-- Run from patton after converting any clients to dreampoints.
-- update dim_rniprocessingparameter_value to use /programfiles/ 
-- instead of /oldurl/


select p.sid_dbprocessinfo_dbnumber, p.dim_rniprocessingparameter_value 
from RN1.Rewardsnow.dbo.convertedtodreampoints as c
join RNIProcessingParameter as p on c.tipfirst = p.sid_dbprocessinfo_dbnumber
where
dim_rniprocessingparameter_key = 'css_header_image'
and dim_rniprocessingparameter_value not like '%programfiles%';


-- if you find a program has a css_header_image that isn't using the /programfiles/ directory...
update RNIProcessingParameter
set
	dim_rniprocessingparameter_value = REPLACE(dim_rniprocessingparameter_value,'/oldurl/','/programfiles/')
where
	sid_dbprocessinfo_dbnumber = ''
	and dim_rniprocessingparameter_key = 'css_header_image';