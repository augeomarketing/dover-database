
update catalog
set
	sid_status_id = 1
where
	sid_catalog_id in (
	
select distinct c.sid_catalog_id
from
	catalog as c
	join catalogcategory as cc on c.sid_catalog_id = cc.sid_catalog_id
	join categorygroupinfo as cgi on cc.sid_category_id = cgi.sid_category_id
where
	cgi.sid_groupinfo_id = 21
	and c.sid_status_id = 1)