-- disable all herobanners but the first one
UPDATE HeroBanner
SET
	dim_herobanner_active = 0
WHERE
	sid_herobanner_id > 1;
	
INSERT INTO HeroBanner
	(sid_dbprocessinfo_dbnumber, dim_herobanner_src, dim_herobanner_alt)
VALUES
	('RNI', './images/additional/SMS_Hero_Dec2015_A.jpg', 'Holiday shopping, now more powerful!'),
	('RNI', './images/additional/SMS_Hero_Dec2015_B.jpg', 'Holiday shopping, now more powerful!');
	
select * from HeroBanner;