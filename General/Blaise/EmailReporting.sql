DECLARE @TipFirst VARCHAR(3) = 'C03';
DECLARE @CampaignID INT;
DECLARE @CampaignName VARCHAR(100);
DECLARE @SendDate VARCHAR(10);

-- Find the Campaign ID via TipFirst
SELECT 
	@CampaignID = sid_emailcampaign_id,
	@CampaignName = dim_emailcampaign_name
FROM 
	EmailCampaign 
WHERE 
	TipFirst = @TipFirst;
	
	--PRINT '@CampaignID = ' + CAST(@CampaignID AS VARCHAR);
	
-- Find the last send date
SELECT 
	@SendDate = MAX(dim_emailopenlog_senddate)
FROM
	EmailOpenLog
WHERE
	sid_emailcampaign_id = @CampaignID;
	
	--PRINT '@SendDate = ' + @SendDate;
	
-- Open Rate
SELECT
	@CampaignName as 'CampaignName', 
	@SendDate as 'SendDate',
	COUNT(dim_emailopenlog_tipnumber) AS OpenRate
FROM
	EmailOpenLog
WHERE
	sid_emailcampaign_id = @CampaignID
	AND dim_emailopenlog_senddate = @SendDate;

-- Click Report
SELECT 
	@CampaignName as 'CampaignName', 
	@SendDate as 'SendDate',
	EL.dim_emaillink_href as 'Link', 
	COUNT(ECL.dim_emailclicklog_tipnumber) as 'Clickthroughs'
FROM
	EmailClickLog as ECL
	JOIN EmailLink as EL on EL.sid_emaillink_id = ECL.sid_emaillink_id
WHERE
	sid_emailcampaign_id = @CampaignID
	AND ECL.dim_emailclicklog_senddate = @SendDate
GROUP BY
	EL.dim_emaillink_href;
	
-- Opt Out Report
SELECT
	COUNT(*) as 'OptoutTotalCount'
FROM
	emailoptout;