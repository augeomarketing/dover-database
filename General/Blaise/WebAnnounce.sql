use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime

set @tipfirst = '531';
set @message =	'<p style="text-align: center;padding:20px;line-height: 1.4em;font-size:1.2em;">' 
				+ 'Bank of Lake Mills has discontinued our Points 2U Rewards program, as of July 21, 2016. Points earned through July 21, 2016 will be available for redemption through September 30, 2016. Points not redeemed by this date will be forfeited. Please contact a Points2U representative at 1-866-764-6872 or a Bank of Lake Mills Personal banker at 920-648-8336, if you have any questions.' 
				+ '</p>';
set @firstday = '05/16/2016' + ' 00:00:00';
set @lastday = '05/16/2017' + ' 00:00:00';

if @tipfirst <> 'RNI'
	update RewardsNOW.dbo.webannounce set dim_webannounce_active = 0 where dim_webannounce_tipprefix = @tipfirst

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	values (@tipfirst, @message, @firstday, @lastday);
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc;