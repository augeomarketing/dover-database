select s.TipNumber, isnull(s.username,'') as username, s.Email, c.Name1, a.MemberNumber, a.MemberID as 'Supervisor''s MemberNumber'
from Coop.dbo.[1Security] as s
	join Coop.dbo.customer as c on c.tipnumber = s.tipnumber
	join Coop.dbo.Account as a on a.TipNumber = s.TipNumber
where LEFT(s.tipnumber,3) = '6EB'
	and s.TipNumber in
('6EB000000000646',	
'6EB000000000721',
'6EB000000000637',
'6EB000000000582',
'6EB000000000715',
'6EB000000000624',
'6EB000000000765',
'6EB000000000881',
'6EB000000001017')

-- sample: 1A7CC5F71737BC22383DC8FE3FC64C2F8F9B4F4639B4A8D3F8B341F6C1AC5C68787882CD14A3671E3EAA8B2D269FA7656C7CBB51631501607FE4EF154EF47E12
-- orig: 

/* SOUTHFIELD BRANCH USERS:
tipnumber	username	email	name	supervisor_id	id
6EB000000000582     	pmr1956	Paula.Roberts@co-opfs.org	PAULA ROBERTS	9644A8A1-505B-4020-B6EC-27E001817428	8974D10A-AD91-4131-98B4-8C34D3A4D1A8
6EB000000000624     	sconyer	Suzanne.Conyer@co-opfs.org	SUZANNE CONYER	8974D10A-AD91-4131-98B4-8C34D3A4D1A8	C588F3B2-A687-4E0C-A966-1BEC99FB46AA
6EB000000000637     	disneygirl	Kelly.Thomas@co-opfs.org	KELLY THOMAS	FECF33DB-34E2-42D1-84C4-EBA33080770A	8063D616-31EB-4491-BDDA-43E457A0CA75
6EB000000000646     	apritchard	Antonio.Pritchard@co-opfs.org	ANTONIO PRITCHARD	FECF33DB-34E2-42D1-84C4-EBA33080770A	7A8B3921-B00F-4E72-A348-5A4E7DEAF63D
6EB000000000715     	tjr1022	LaTasha.Reed@co-opfs.org	LATASHA REED	8974D10A-AD91-4131-98B4-8C34D3A4D1A8	FECF33DB-34E2-42D1-84C4-EBA33080770A
6EB000000000721     	beverly.greenia	Beverly.Greenia@co-opfs.org	BEVERLY GREENIA	FECF33DB-34E2-42D1-84C4-EBA33080770A	0973700F-7FF4-4C70-B89B-F5F4560EC348
6EB000000000765     	NULL	Crystal.Brown@co-opfs.org	CRYSTAL BROWN	FECF33DB-34E2-42D1-84C4-EBA33080770A	0FAE9B3F-1994-4754-BE75-2069145E30EC
6EB000000000881     	Ashley07	Ashley.Schultz@co-opfs.org	ASHLEY SCHULTZ	FECF33DB-34E2-42D1-84C4-EBA33080770A	B4A2F2CE-BF77-4E16-BDB0-806FAF08C958
6EB000000001017     	NULL	joreen.james@co-opfs.org	JOREEN JAMES	FECF33DB-34E2-42D1-84C4-EBA33080770A	26F9713A-B296-42A3-B9D3-BF3ACE05ED90
*/

/* get all unapproved awards for a tip */
select cvs.sid_corevaluesubmission_id, cvs.dim_corevaluesubmission_tipnumber, A_by.TipNumber as 'By', A_for.TipNumber as 'For', A_supervisor.TipNumber as 'Supervisor', cvs.dim_corevaluesubmission_readyForApproval, cvs.dim_corevaluesubmission_approved, cvs.dim_corevaluesubmission_created
from corevaluesubmission as cvs
left join
	Coop.dbo.Account as A_by
		on A_by.MemberNumber = cvs.dim_corevaluesubmission_by
left join
	Coop.dbo.Account as A_for
		on A_for.MemberNumber = cvs.dim_corevaluesubmission_acct
left join
	Coop.dbo.Account as A_supervisor
		on A_supervisor.MemberNumber = A_for.MemberID
where dim_corevaluesubmission_acct = '7A8B3921-B00F-4E72-A348-5A4E7DEAF63D'
and ISNULL(dim_corevaluesubmission_approved, 0) = 0;

/* GET PENDING APPROVALS (manager) */
SELECT DISTINCT 
	sid_corevaluesubmission_id, Name1 as Employee, dim_corevalue_description AS coreValue, dim_corevaluesubmission_description as Description, dim_corevaluesubmission_created AS [Date], ISNULL(dim_corevaluesubmission_approved, -1) AS Approved, dim_corevaluesubmission_points AS Points, (SELECT TOP 1 name1 FROM [COOP].dbo.customer cust INNER JOIN [COOP].dbo.account acct ON acct.tipnumber = cust.tipnumber WHERE acct.membernumber = cvs.dim_corevaluesubmission_by) AS submittedbyname
FROM [COOP].dbo.customer c 
INNER JOIN [COOP].dbo.account s 
	ON c.tipnumber = s.tipnumber 
INNER JOIN rewardsnow.dbo.corevaluesubmission cvs 
	ON s.MemberNumber =  cvs.dim_corevaluesubmission_acct 
INNER JOIN rewardsnow.dbo.corevalue cv 
	ON cvs.sid_corevalue_id = cv.sid_corevalue_id 
WHERE 1=1 
	AND LEFT(c.Tipnumber,3) = '6EB' 
	AND cvs.dim_corevaluesubmission_readyForApproval = 1 
	AND cvs.dim_corevaluesubmission_approved IS NULL 
	AND cvs.dim_corevaluesubmission_acct 
		IN ( SELECT DISTINCT 
				MemberNumber 
			FROM [COOP].dbo.[account] 
			WHERE MemberId = (SELECT DISTINCT 
								MemberNumber 
							  FROM [COOP].dbo.[account] 
							  WHERE Tipnumber ='6EB000000000715')) 
ORDER BY cvs.dim_corevaluesubmission_created DESC;

/* get supervisor */
select a.TipNumber as 'me', myBoss.TipNumber as 'my boss'
from Account as a
join Account as myBoss
	on myBoss.MemberNumber = a.MemberID
where a.TipNumber = '6EB000000000624';

/* awards missing a coop account */
select *
from rewardsnow.dbo.corevaluesubmission as cvs
	LEFT JOIN Coop.dbo.Account as A
		on A.MemberNumber = cvs.dim_corevaluesubmission_by
where
	A.TipNumber IS NULL;
	
/* awards missing a coop account (by tip)*/
/* 14 additional awards missing. */
/* 16 if joining to 1security instead of account */
select *
from rewardsnow.dbo.corevaluesubmission as cvs
	LEFT JOIN Coop.dbo.account as A
		on A.TipNumber = cvs.dim_corevaluesubmission_bytipnumber
where
	A.TipNumber IS NULL;


