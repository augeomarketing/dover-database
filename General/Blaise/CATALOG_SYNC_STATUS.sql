-- Patton

-- SYNC STATUS...the ID returned from each pair of queries should be the same.  meaning, the max(id)  on patton should equal the max(id) on RN1.
SELECT MAX(sid_catalog_id) FROM [catalog];
SELECT MAX(sid_catalog_id) FROM [rn1].[catalog].[dbo].[catalog];

SELECT MAX(sid_catalogdescription_id) FROM [catalogdescription];
SELECT MAX(sid_catalogdescription_id) FROM [rn1].[catalog].[dbo].[catalogdescription];

SELECT MAX(sid_catalogcategory_id) FROM catalogcategory;
SELECT MAX(sid_catalogcategory_id) FROM [rn1].[catalog].[dbo].[catalogcategory];

SELECT MAX(sid_loyaltycatalog_id) FROM [loyaltycatalog];
SELECT MAX(sid_loyaltycatalog_id) FROM [rn1].[catalog].[dbo].[loyaltycatalog];

SELECT MAX(dwid_loyaltycatalog_id) FROM [loyaltycatalog];
SELECT MAX(dwid_loyaltycatalog_id) FROM [rn1].[catalog].[dbo].[loyaltycatalog];

