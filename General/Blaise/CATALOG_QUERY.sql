-- GET ALL ACTIVE CATALOG ITEMS FOR SPECIFIC PROGRAMS
-- //////////////////////////////////////////////////




select distinct 
	c.sid_catalog_id, lc.sid_loyalty_id,
	LT.dim_loyaltytip_prefix, 
	C.dim_catalog_code, 
	CD.dim_catalogdescription_name, 
	LC.dim_loyaltycatalog_pointvalue, 
	GI.dim_groupinfo_name, 
	CASE C.sid_status_id
	 WHEN 1 THEN 'In-Stock'
	 WHEN 2 THEN 'Out of Stock'
	 WHEN 3 THEN 'Discontinued'
	 ELSE 'Status Unknown'
	END as 'Status'
from loyaltytip as LT
join loyalty as L on L.sid_loyalty_id = LT.sid_loyalty_id
JOIN loyaltycatalog as LC on LC.sid_loyalty_id = L.sid_loyalty_id
JOIN [catalog] as C on C.sid_catalog_id = LC.sid_catalog_id
JOIN catalogcategory as CC on CC.sid_catalog_id = C.sid_catalog_id
JOIN categorygroupinfo as CGI on CGI.sid_category_id = CC.sid_category_id
JOIN groupinfo as GI on GI.sid_groupinfo_id = CGI.sid_groupinfo_id
JOIN catalogdescription as CD on CD.sid_catalog_id = C.sid_catalog_id
JOIN category as CAT on CAT.sid_category_id = CC.sid_category_id
where
	LT.dim_loyaltytip_prefix in ('52J')
	and dim_catalogcategory_active = 1
	and C.dim_catalog_active = 1
	and C.sid_status_id = 1
	and LC.dim_loyaltycatalog_pointvalue > 0
order by
	LT.dim_loyaltytip_prefix, dim_catalog_code;
  

  
  
-- GET ALL ITEMS FOR A SPECIFIC GROUP
-- ///////////////////////////////////////////////////////////////////////////////// 
select distinct 
	C.dim_catalog_code, 
	CD.dim_catalogdescription_name, 
	GI.dim_groupinfo_name, 
	CASE C.sid_status_id
	 WHEN 1 THEN 'In-Stock'
	 WHEN 2 THEN 'Out of Stock'
	 WHEN 3 THEN 'Discontinued'
	 ELSE 'Status Unknown'
	END as 'Status'
from [catalog] as C 
JOIN catalogcategory as CC on CC.sid_catalog_id = C.sid_catalog_id
JOIN categorygroupinfo as CGI on CGI.sid_category_id = CC.sid_category_id
JOIN groupinfo as GI on GI.sid_groupinfo_id = CGI.sid_groupinfo_id
JOIN catalogdescription as CD on CD.sid_catalog_id = C.sid_catalog_id
JOIN category as CAT on CAT.sid_category_id = CC.sid_category_id
where
	GI.sid_groupinfo_id IN (5,10,12,18,21);