-- PATTON

/* LOCAL PARAMS*/
DECLARE @newCatalogID INT;
DECLARE @newCatalogDescriptionID INT;
DECLARE @newLoyaltyCatalogID_Patton INT;
DECLARE @newLoyaltyCatalogID_RN1 INT;
DECLARE @newCatalogCategoryID INT;

/* EXTERNAL PARAMS */
DECLARE @code VARCHAR(20);
DECLARE @trancode VARCHAR(2);  /* RM = Merchandise */
DECLARE @dollars decimal(10,2); /* TODO: is this field even used? */
DECLARE @image VARCHAR(1024); /* Image goes here: \\web2\C$\inetpub\catalog\images\products\ */
DECLARE @status INT;  /*1 = in stock, 2 = out of stock, 3 = discontinued */
DECLARE @cost decimal(18,2);  /* req'd */
DECLARE @msrp decimal(18,2); /* req'd */
DECLARE @shipping decimal(18,2); /* req'd */
DECLARE @hishipping decimal(18,2); /* optional */
DECLARE @handling decimal(18,2); /* optional */
DECLARE @weight decimal(18,2); /* optional */
DECLARE @cashvalue decimal(18,2); /* optional (certs and giftcards)*/
DECLARE @name VARCHAR(1024); /* req'd */
DECLARE @description VARCHAR(8000); /* req'd */

DECLARE @pointvalue INT; /* -1 = deactivated, 0 = unintialized, >0 = active */
DECLARE @loyaltyid INT;
DECLARE @categoryid INT; /* todo: make this a list for multiple categories */

	/* TESTING DATA */
	SET @code = 'BDS-1234-20150604-C';
	SET @trancode = 'BS';
	SET @dollars = 0;
	SET @image = 'test.png';
	SET @status = 1;
	SET @cost = 2.22;
	SET @msrp = 2.00;
	SET @shipping = 3.33;
	SET @hishipping = 0.00;
	SET @handling = 4.44;
	SET @weight = 0.00;
	SET @cashvalue = 0.00;
	SET @name = 'BDS-1234-20150604-C';
	SET @description = 'BDS-1234-20150604-C';
	
	set @pointvalue = 0
	set @loyaltyid = 165 /* reb*/
	set @categoryid = 9; /* style */
	
/* START: INSERT CATALOG ITEM */
	INSERT INTO [catalog] (
		dim_catalog_code, 
		dim_catalog_trancode, 
		dim_catalog_dollars, 
		dim_catalog_imagelocation, 
		dim_catalog_imagealign, 
		dim_catalog_active, 
		dim_catalog_parentid, 
		sid_status_id, 
		dim_catalog_cost, 
		dim_catalog_msrp, 
		dim_catalog_shipping, 
		dim_catalog_hishipping, 
		dim_catalog_handling, 
		dim_catalog_weight, 
		dim_catalog_cashvalue
	)VALUES(
		  @code,
		  @trancode,
		  @dollars,
		  @image,
		  'right',
		  @status,
		  -1,
		  1,
		  @cost,
		  @msrp,
		  @shipping,
		  @hishipping,
		  @handling,
		  @weight,
		  @cashvalue
		  );

	SELECT @newCatalogID = SCOPE_IDENTITY();

	PRINT 'New sid_catalog_id: ' + CONVERT(VARCHAR(20),@newCatalogID);
	  
	/* Catalog Description */
	INSERT INTO catalogdescription (
		sid_catalog_id, 
		dim_catalogdescription_name, 
		dim_catalogdescription_description, 
		sid_languageinfo_id, 
		dim_catalogdescription_active
	)VALUES(
		@newCatalogID,
		@name,
		@description,
		1,
		1
	);

	SELECT @newCatalogDescriptionID = SCOPE_IDENTITY();

	PRINT 'New description id: ' + CONVERT(VARCHAR(20),@newCatalogDescriptionID);

	/* Web Side - Catalog */
	INSERT INTO [RN1].[catalog].[dbo].[catalog] (
		sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars,dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_active, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_cost, dim_catalog_msrp, dim_catalog_shipping, dim_catalog_hishipping, dim_catalog_handling, dim_catalog_weight, dim_catalog_cashvalue)
	SELECT 
		sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_active, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_cost, dim_catalog_msrp, dim_catalog_shipping, dim_catalog_hishipping, dim_catalog_handling, dim_catalog_weight, dim_catalog_cashvalue
	FROM 
		[catalog]
	WHERE 
		sid_catalog_id = @newCatalogID;

	PRINT 'Web Catalog Inserted.';	

	/* Web Side - CatalogDescription */
	INSERT INTO [RN1].[catalog].[dbo].[catalogdescription] (
		sid_catalogdescription_id, sid_catalog_id, dim_catalogdescription_name, dim_catalogdescription_description, sid_languageinfo_id, dim_catalogdescription_active)
	SELECT
		sid_catalogdescription_id, sid_catalog_id, dim_catalogdescription_name, dim_catalogdescription_description, sid_languageinfo_id, dim_catalogdescription_active
	FROM
		[catalogdescription]
	WHERE
		sid_catalog_id = @newCatalogID;
		
	PRINT 'Web Description Inserted.';	
	
/* END: INSERT CATALOG ITEM */

/* START: INSERT CATALOGCATEGORY */
	INSERT INTO catalogcategory 
		(sid_catalog_id, sid_category_id, dim_catalogcategory_active)
	VALUES 
		(@newCatalogID, @categoryid, 1);
		
	SELECT @newCatalogCategoryID = SCOPE_IDENTITY();

	PRINT 'Patton catalogcategory Insert: ' + CONVERT(VARCHAR(20),@newCatalogCategoryID);

	INSERT INTO RN1.catalog.dbo.catalogcategory 
		(sid_catalogcategory_id ,sid_catalog_id, sid_category_id, dim_catalogcategory_active)
	VALUES 
		(@newCatalogCategoryID, @newCatalogID, @categoryid, 1);
		
	PRINT 'RN1 catalogcategory INSERT.';
/* END: INSERT CATALOGCATEGORY */

/* START: INSERT LOYALTYCATALOG */
	/* INSERT INTO AT LEAST ONE PROGRAM SO IT WILL SHOW UP IN CATALOG EDITOR */
	
	-- Patton side
	SELECT @newLoyaltyCatalogID_Patton = MAX(sid_loyaltycatalog_id) + 1 FROM loyaltycatalog;

	insert into loyaltycatalog 
		(sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
	values
		(@newLoyaltyCatalogID_Patton, @loyaltyid, @newCatalogID, @pointvalue);
		
	PRINT 'Patton LoyaltyCatalog Insert: ' + CONVERT(VARCHAR(20),@newLoyaltyCatalogID_Patton);

	-- RN1 side
	SELECT @newLoyaltyCatalogID_RN1 = MAX(dwid_loyaltycatalog_id) + 1 FROM RN1.catalog.dbo.loyaltycatalog;

	insert into RN1.catalog.dbo.loyaltycatalog 
		(dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
	values
		(@newLoyaltyCatalogID_RN1, @newLoyaltyCatalogID_Patton, @loyaltyid, @newCatalogID, @pointvalue);	
	
	PRINT 'RN1 LoyaltyCatalog Insert: ' + CONVERT(VARCHAR(20),@newLoyaltyCatalogID_Patton);
/* END: INSERT LOYALTYCATALOG */

/* Check Results */
SELECT * FROM catalog WHERE sid_catalog_id = @newCatalogID
SELECT * FROM catalogdescription WHERE sid_catalog_id = @newCatalogID
SELECT * FROM loyaltycatalog WHERE sid_catalog_id = @newCatalogID
SELECT * FROM RN1.catalog.dbo.catalog WHERE sid_catalog_id = @newCatalogID
SELECT * FROM RN1.catalog.dbo.catalogdescription WHERE sid_catalog_id = @newCatalogID
SELECT * FROM RN1.catalog.dbo.loyaltycatalog WHERE sid_catalog_id = @newCatalogID

