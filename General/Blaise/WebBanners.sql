use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime

set @tipfirst = '240'
set @message = '<img style="display: block; margin: 0 auto; width: 680px;" src="./images/additional/240_ESCU_Web Banner_Nov2015.jpg">'
set @firstday = '10/22/2015' + ' 00:00:00'
set @lastday = '12/01/2015' + ' 00:00:00'

if @tipfirst <> 'RNI'
	update RewardsNOW.dbo.webannounce set dim_webannounce_active = 0 where dim_webannounce_tipprefix = @tipfirst

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	values (@tipfirst, @message, @firstday, @lastday)
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc