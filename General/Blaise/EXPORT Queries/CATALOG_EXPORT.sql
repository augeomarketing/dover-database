select distinct 
	C.dim_catalog_code, 
	CD.dim_catalogdescription_name as 'PRODUCT NAME', 
	C.dim_catalog_msrp AS 'MSRP',
	C.dim_catalog_cost AS 'AUGEO COST',  
	C.dim_catalog_handling + C.dim_catalog_shipping AS 'AUGEO S&H', 
	replace(replace(CD.dim_catalogdescription_description,char(10),' '),char(13),' ') as 'DESCRIPTION',
	CASE LEFT(C.dim_catalog_imagelocation,4)
		WHEN 'http' THEN C.dim_catalog_imagelocation
		ELSE 'https://www.rewardsnow.com/catalog/images/products/' + C.dim_catalog_imagelocation
	END as 'Image_URL',
	GI.dim_groupinfo_name as 'Group_Name', 
	dbo.getCategoriesByCatalogID(C.sid_catalog_id) as 'Categories',
	dbo.getTipsByCatalogID(C.sid_catalog_id) as 'TipFirsts',
	C.sid_routing_id as 'Routing ID'
from loyaltytip as LT
join loyalty as L on L.sid_loyalty_id = LT.sid_loyalty_id
JOIN loyaltycatalog as LC on LC.sid_loyalty_id = L.sid_loyalty_id
JOIN [catalog] as C on C.sid_catalog_id = LC.sid_catalog_id
JOIN catalogcategory as CC on CC.sid_catalog_id = C.sid_catalog_id
JOIN categorygroupinfo as CGI on CGI.sid_category_id = CC.sid_category_id
JOIN groupinfo as GI on GI.sid_groupinfo_id = CGI.sid_groupinfo_id
JOIN catalogdescription as CD on CD.sid_catalog_id = C.sid_catalog_id
JOIN category as CAT on CAT.sid_category_id = CC.sid_category_id
JOIN RewardsNow.dbo.dbprocessinfo as DB on DB.DBNumber = LT.dim_loyaltytip_prefix
where
	/* active flags */
	LT.dim_loyaltytip_active = 1 
	AND L.dim_loyalty_active = 1
	AND LC.dim_loyaltycatalog_active = 1
	AND C.dim_catalog_active = 1
	AND CC.dim_catalogcategory_active = 1
	-- AND CD.dim_catalogdescription_active = 1
	and CAT.dim_category_active = 1
	/* has point value */
	and LC.dim_loyaltycatalog_pointvalue > 0
	/* is in stock */
	and C.sid_status_id = 1
	and DB.sid_FiProdStatus_statuscode IN ('P','V')
	/* exlude per john */
	and GI.sid_groupinfo_id NOT IN (1,2,3,12,21,25)
	and dim_catalog_code not in ('QG10','QG25','QG50','GROUPON25','CCSUB_ANNUALLY','CG25','CG50')
group by
	C.sid_catalog_id,
	C.dim_catalog_code, 
	CD.dim_catalogdescription_name, 
	GI.dim_groupinfo_name,
	CAT.sid_category_id,
	C.dim_catalog_cost, C.dim_catalog_handling, C.dim_catalog_shipping, C.dim_catalog_msrp,
	C.dim_catalog_imagelocation, CD.dim_catalogdescription_description, C.sid_routing_id
order by
	dim_groupinfo_name asc,
	dim_catalog_code asc;