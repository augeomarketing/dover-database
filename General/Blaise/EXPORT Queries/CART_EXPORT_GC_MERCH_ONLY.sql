-- AUTHORIZED USERS

select 
	dim_authusers_tipnumber, 
	dim_authusers_users, 
	dim_authusers_created, 
	dim_authusers_lastmodified
from 
	authusers 
where 
	dim_authusers_tipnumber like '636%'
	and dim_authusers_active = 1;

-- PINS 

SELECT DISTINCT
	o.TipNumber, o.transid,
	c.dim_catalog_code,
	cd.dim_catalogdescription_name,
	RTRIM(p.PIN) as pin, p.Issued, p.Expire,
	cat.dim_category_name
FROM
	coop.dbo.OnlHistory AS o
	JOIN PINs.dbo.PINs as p on o.TipNumber = p.TIPNumber AND O.TransID = p.transid
	INNER JOIN catalog.dbo.catalog c 
		ON p.progid = c.dim_catalog_code 
	INNER JOIN catalog.dbo.catalogdescription cd
		ON c.sid_catalog_id = cd.sid_catalog_id
	INNER JOIN Catalog.dbo.catalogcategory cc
		ON cc.sid_catalog_id = c.sid_catalog_id
	INNER JOIN Catalog.dbo.categorygroupinfo cgi
		ON cc.sid_category_id = cgi.sid_category_id
	INNER JOIN Catalog.dbo.groupinfo gi
		ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
	INNER JOIN Catalog.dbo.category as cat
		on cat.sid_category_id = cc.sid_category_id
WHERE 
	expire > getdate()
	AND dim_pins_effectivedate < getdate()
	AND cgi.sid_groupinfo_id <> 16
	AND o.TipNumber like '636%'
ORDER BY
	o.TipNumber,dim_catalog_code;

-- CARTS WISHLISTS

select 
	cart.sid_tipnumber, c.dim_catalog_code, cart.dim_cart_wishlist
from
	cart
	join Catalog.dbo.catalog as c on c.sid_catalog_id = cart.sid_catalog_id
where
	dim_cart_active = 1
	and (c.dim_catalog_code like 'PRE-%' OR c.dim_catalog_code like 'X-AUG%')
	and cart.sid_tipnumber like '636%'
order by
	cart.sid_tipnumber, cart.dim_cart_wishlist, c.dim_catalog_code;
