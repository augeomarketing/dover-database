select top 100 
	o.TipNumber,
	o.Email,
	o.CatalogCode,
	o.CatalogDesc,
	cst.dim_cashstartracking_rnitransid, 
	cst.dim_cashstartracking_ordernumber,
	cst.dim_cashstartracking_transid,
	cst.dim_cashstartracking_egccode,
	cst.dim_cashstartracking_egcnumber,
	cst.dim_cashstartracking_accesscode,
	cst.dim_cashstartracking_url
from
	cashstartracking as cst
	join Enterprise.dbo.OnlHistory as o on o.TransID = cst.dim_cashstartracking_rnitransid
where
	cst.dim_cashstartracking_active = 1
order by
	o.TipNumber asc, cst.dim_cashstartracking_created desc;