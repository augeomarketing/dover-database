SELECT 
	sc.sid_subscriptioncustomer_tipnumber
	,  sc.dim_subscriptioncustomer_startdate
	,  sc.dim_subscriptioncustomer_enddate
	, ISNULL(sc.dim_subscriptioncustomer_canceldate, '') AS dim_subscriptioncustomer_canceldate
	, sc.sid_subscriptionstatus_id
	, ss.dim_subscriptionstatus_description
	, st.sid_subscriptiontype_id
	, st.dim_subscriptiontype_description
	, sc.dim_subscriptioncustomer_ccexpire
	, sp.dim_subscriptionproduct_description
	, st.dim_subscriptiontype_points
FROM rewardsnow.dbo.subscriptioncustomer sc
INNER JOIN rewardsnow.dbo.subscriptiontype st
	ON sc.sid_subscriptiontype_id = st.sid_subscriptiontype_id
INNER JOIN rewardsnow.dbo.subscriptionstatus ss
	ON sc.sid_subscriptionstatus_id = ss.sid_subscriptionstatus_id
INNER JOIN rewardsnow.dbo.subscriptionproducttype spt
	ON spt.sid_subscriptiontype_id = sc.sid_subscriptiontype_id
INNER JOIN rewardsnow.dbo.subscriptionproduct sp
	ON sp.sid_subscriptionproduct_id = spt.sid_subscriptionproduct_id
WHERE 
	LEFT(sid_subscriptioncustomer_tipnumber,3) IN ('109','114','135','150','267') 
	AND sc.dim_subscriptioncustomer_active = 1
	AND sc.dim_subscriptioncustomer_enddate > GETDATE()
	AND ss.sid_subscriptionstatus_id = 1
