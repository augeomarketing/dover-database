SELECT TOP 100 *
FROM PINS.dbo.PINs
order by dim_pins_lastmodified desc;


SELECT DISTINCT
	o.TipNumber, o.transid,
	c.dim_catalog_code,
	cd.dim_catalogdescription_name,
	RTRIM(p.PIN) as pin, p.Issued, p.Expire
FROM
	coop.dbo.OnlHistory AS o
	JOIN PINs.dbo.PINs as p on o.TipNumber = p.TIPNumber AND O.TransID = p.transid
	INNER JOIN catalog.dbo.catalog c 
		ON p.progid = c.dim_catalog_code 
	INNER JOIN catalog.dbo.catalogdescription cd
		ON c.sid_catalog_id = cd.sid_catalog_id
	--INNER JOIN catalog.dbo.cataloginstruction ci 
		--ON c.sid_catalog_id = ci.sid_catalog_id 
	--INNER JOIN catalog.dbo.instruction i 
		--ON ci.sid_instruction_id = i.sid_instruction_id 
	INNER JOIN Catalog.dbo.catalogcategory cc
		ON cc.sid_catalog_id = c.sid_catalog_id
	INNER JOIN Catalog.dbo.categorygroupinfo cgi
		ON cc.sid_category_id = cgi.sid_category_id
	INNER JOIN Catalog.dbo.groupinfo gi
		ON gi.sid_groupinfo_id = cgi.sid_groupinfo_id
WHERE 
	expire > getdate()
	AND dim_pins_effectivedate < getdate()
	AND cgi.sid_groupinfo_id <> 16
	AND o.TipNumber like '636%'
ORDER BY
	o.TipNumber,dim_catalog_code;
	


/* which items require a pin? */	
SELECT DISTINCT
	c.dim_catalog_code,
	cd.dim_catalogdescription_name
FROM
	PINs.dbo.PINs as p 
	INNER JOIN catalog.dbo.catalog c 
		ON p.progid = c.dim_catalog_code 
	INNER JOIN catalog.dbo.catalogdescription cd
		ON c.sid_catalog_id = cd.sid_catalog_id;
		
		