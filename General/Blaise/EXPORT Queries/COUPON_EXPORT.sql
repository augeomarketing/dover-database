/*select 
	c.dim_coupons_points, 
	c.dim_coupons_amount, 
	cs.dim_couponstatus_desc, 
	c.dim_coupons_dateissued 
from 
	onlinehistorywork.dbo.coupons c, 
	onlinehistorywork.dbo.couponstatus cs, 
	account a 
where 
	c.sid_couponstatus_id = cs.sid_couponstatus_id 
	and a.MemberNumber = c.dim_coupons_account 
	and a.tipnumber = ? 
	and c.sid_couponstatus_id = 3;*/
	
	-- NOT SURE WE NEED TO DO COUPONS.
	
SELECT top 100
	a.TipNumber,
	c.dim_coupons_points, 
	c.dim_coupons_amount, 
	cs.dim_couponstatus_desc, 
	c.dim_coupons_dateissued 
FROM
	coop.dbo.Account as a
	JOIN OnlineHistoryWork.dbo.coupons as c on a.MemberNumber = c.dim_coupons_account
	JOIN OnlineHistoryWork.dbo.couponstatus as cs on c.sid_couponstatus_id = cs.sid_couponstatus_id
WHERE
	c.sid_couponstatus_id = 3
ORDER BY
	a.TipNumber, c.dim_coupons_created;
	