DECLARE @TipFirst VarChar(3) = '267';

select 
	dim_authusers_tipnumber, 
	dim_authusers_users, 
	dim_authusers_created, 
	dim_authusers_lastmodified
from 
	authusers 
where 
	LEFT(dim_authusers_tipnumber,3) IN ('605',
'611',
'615',
'617',
'624',
'633',
'635',
'638',
'643',
'644',
'645',
'646',
'647',
'648',
'649',
'655',
'657',
'654')
	and dim_authusers_active = 1
order by
	dim_authusers_tipnumber;