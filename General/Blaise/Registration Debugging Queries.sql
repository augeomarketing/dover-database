-- RN1

/* 
  You will need the TipNumber of the account attempting to register.
  
  You will also need to know what the member was entering into the registration form.
  
  Most (but not all) programs register with the following: Full Name, Last Six, and Zip Code.  
*/

/* Almost every attempted registration will be logged here */
select *
from
	registrationtracking
where
	(dim_registrationtracking_tipnumber = '651000000048481'	or dim_registrationtracking_cardname like '%JOHN SMITH%')
	and dim_registrationtracking_created > '2016-02-01';
	
/* If not found above, try checking here. */
/* Inserts here happen in signup.asp */
select *
from
	BatchDebugLog with(nolock)
where
	(dim_batchdebuglog_logentry like '%651000000048481%' or dim_batchdebuglog_logentry like '%JOHN SMITH%')
	and dim_batchdebuglog_dateadded > '2016-02-01';

-- ==================================
/* Look up the member's data on RN1 */
/* Using data found in these tables, attempt to register the user yourself */
select * from webinit where dim_webinit_defaulttipprefix = '651';
	
select *
from
	michigan1st.dbo.[1Security]
where
	TipNumber = '651000000048481';
select *
from
	michigan1st.dbo.Customer
where
	TipNumber = '651000000048481';
select *
from
	michigan1st.dbo.Account
where
	TipNumber = '651000000048481';
-- ==================================
/* Still not working?  Run this sproc after passing it the appropriate parameters. */
/* If successful, it will return a single record.  If two records are found, a combine is likely needed. */
EXEC usp_webGetTipnumberFromUserEntry
	@database = 'NewTNB',
	@name = 'KENNETH G ROZNOWSKI',
	@lastname = '',
	@username = '',
	@lastsix = '809344',
	@ssn = '',
	@memberid = '',
	@membernumber = '',
	@maidenname = '',
	@zipcode = '48706',
	@signup = 1,
	@url = 'www.rewardsnow.com/dcecu/'
	

