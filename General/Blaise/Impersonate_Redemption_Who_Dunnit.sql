-- Find the time of the redemption in question

select *
from
	Redwood.dbo.OnlHistory
where
	TipNumber = '213000000074016';
	
-- redemption date: 2015-11-30 19:37:00

-- find the login time that corresponds with the redemption
select *
from
	loginhistory
where
	dim_loginhistory_tipnumber = '213000000074016';

-- login time: 2015-11-30 19:23:48.970

-- find the isadmin session var in batchdebuglog
select *
from
	BatchDebugLog with(nolock)
where
	dim_batchdebuglog_process = 'login_ss.asp'
	and dim_batchdebuglog_logentry like '%213000000074016%'
order by
	dim_batchdebuglog_dateadded desc;

/*

tipfirst: 213: tipnumber: 213000000074016: ip: 12.233.91.251:: ::,{tipnumber = 213000000074016},{epochsec = 1448937186},{authhash = 2F/mhGhOwGTt70T5G56b/skHwkc=},{IsAdmin = 5026},{CFID = 290455},{CFTOKEN = 52797053}::,{Start = 11/30/2015 8:26:22 PM},{signupform = False},{baseURL = www.rewardsnow.com/redwood/},{beta = 0},{loginHistoryID = -1},{webInitId = 75},{tipfirst = 213},{defaulttipfirst = 213},{Database = Redwood},{cssStyle = 213style.css},{cssMenu = 213menu.css},{cssStructure = 213style_structure.css},{cssStylePrint = stylePrint.css},{cssMenuPrint = menuPrint.css},{cssStructurePrint = style_structurePrint.css},{ProgramName = Redwood Rewards},{SiteTitle = Redwood Rewards},{termspage = 213terms.asp},{FAQPage = 213faq.asp},{earnpage = 213earning.asp},{javascript = 213custom.js},{captchaid = 5},{ssoonly = 0},{sfname = ShoppingFLING},{sfurl = https://www.shoppingfling.org/welcome.aspx},{sfemail = rewards@shoppingfling.org},{IsAdmin = 5026},{landing = },{carttoken = {58FE8306-DF75-49DB-BA0B-FB1543229DF8}},{loginmethod = 2},{loggedin = True},{weblogin = False},{sessionCode = {26896C61-2D2F-414B-98C5-B7E2A7926FA2}},{tipnumber = 213000000074016},{EmailAddr = purebell@airpost.net},{EmailStm = Y},{EmailStatement = Y},{EmailOther = N},{firstSessionLogin = 1},{display_sweepstakes_reminder = 0},{ClientName = Redwood CU},{ClientCode = Redwood},{PointsUpdated = 10/31/2015},{BookingFee = 10},{TravelMin = 2500},{TravelBottom = 5000},{logo = },{CSPhone = 1-877-532-7785},{Landingpage = },{CashMin = 2500},{AcctName1 = ANITA DUNAJSKY BELL},{AcctName2 = },{Address1 = 815 RIVER STREET},{Address2 = },{Address4 = SANTA CRUZ CA 95060},{City = SANTA CRUZ},{State = CA},{Zipcode = 95060},{segment = },{status = A},{firstname = ANITA},{RunAvailable = 390},{ValidForm = {F9641F66-2DD3-4CD8-A93A-0D66710117B8}},{regdate_diff = 25},{travelRatio = 100},{phonenumber = },{phonecontactstmt = 0},{phonecontactmarketing = 0},{ASPFIXATION = D8HZFbY1HSL},{setmenu = 1},{checkedtip = 213},{haspayforpoints = 0},{SubscriptionDaysRemaining = -9999},{displayredemptions = 0},{displayBonus = 0},{displayTravelRebate = 0},{displayShoppingFling = 1},{displayCouponCents = 0},{displaySMS = 0},{displayTravelPrepackage = 1},{displayMerch = 1},{displayGiftCard = 1},{displayDownload = 1},{displayQuickgift = 1},{displayCharity = 1},{displayFees = 0},{displayCashback = 1},{displayCollege = 0},{displayLocalGiftCard = 0},{displayContests = 0},{displayRNIContests = 0},{displayEGiftCard = 1},{displaySpecial = 0},{hasUsePoints = 1},{attempts = 0},{subscriptionReminderDisplay = 0},{emailnag = True},{hasHistoryStatement = 51}
tipfirst: 213: tipnumber: 213000000074016: ip: 12.233.91.251:: ::,{tipnumber = 213000000074016},{epochsec = 1448936781},{authhash = tiOSjVtdpVqvJCDVRJSeMHtjFTs=},{IsAdmin = 5026},{CFID = 290455},{CFTOKEN = 52797053}::,{Start = 11/30/2015 8:26:22 PM},{signupform = False},{baseURL = www.rewardsnow.com/redwood/},{beta = 0},{loginHistoryID = 0},{webInitId = 75},{tipfirst = 213},{defaulttipfirst = 213},{Database = Redwood},{cssStyle = 213style.css},{cssMenu = 213menu.css},{cssStructure = 213style_structure.css},{cssStylePrint = stylePrint.css},{cssMenuPrint = menuPrint.css},{cssStructurePrint = style_structurePrint.css},{ProgramName = Redwood Rewards},{SiteTitle = Redwood Rewards},{termspage = 213terms.asp},{FAQPage = 213faq.asp},{earnpage = 213earning.asp},{javascript = 213custom.js},{captchaid = 5},{ssoonly = 0},{sfname = ShoppingFLING},{sfurl = https://www.shoppingfling.org/welcome.aspx},{sfemail = rewards@shoppingfling.org},{IsAdmin = 5026},{landing = },{carttoken = {016AEE8C-1E71-4BFA-B305-F287170D81E6}}
tipfirst: 213: tipnumber: 213000000074016: ip: 12.233.91.251:: ::,{tipnumber = 213000000074016},{epochsec = 1448933641},{authhash = ce0ebvIMxfNekS1kP6ru1snB+dM=},{IsAdmin = 4819},{CFID = 290443},{CFTOKEN = 57354547}::,{Start = 11/30/2015 7:34:02 PM},{signupform = False},{baseURL = www.rewardsnow.com/redwood/},{beta = 0},{loginHistoryID = 0},{webInitId = 75},{tipfirst = 213},{defaulttipfirst = 213},{Database = Redwood},{cssStyle = 213style.css},{cssMenu = 213menu.css},{cssStructure = 213style_structure.css},{cssStylePrint = stylePrint.css},{cssMenuPrint = menuPrint.css},{cssStructurePrint = style_structurePrint.css},{ProgramName = Redwood Rewards},{SiteTitle = Redwood Rewards},{termspage = 213terms.asp},{FAQPage = 213faq.asp},{earnpage = 213earning.asp},{javascript = 213custom.js},{captchaid = 5},{ssoonly = 0},{sfname = ShoppingFLING},{sfurl = https://www.shoppingfling.org/welcome.aspx},{sfemail = rewards@shoppingfling.org},{IsAdmin = 4819},{landing = },{carttoken = {FACC88CD-6758-4556-8CCD-1472342F2FE4}}

*/

select *
from
	management.dbo.userinfo
where
	sid_userinfo_id = 4819;


