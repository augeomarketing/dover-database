select *
from
	webbanner
order by
	dim_webbanner_banner1image;
	
update webbanner
set
	dim_webbanner_banner3link = '',
	dim_webbanner_banner3image = '',
	dim_webbanner_banner4link = '',
	dim_webbanner_banner4image = ''
where
	dim_webbanner_tipprefix IN (
		select dbnumber 
		from dbprocessinfo 
		where LocalMerchantParticipant = 'Y' 	
	)
	AND dim_webbanner_tipprefix NOT IN ('650','276');