/*
    run this sproc.  it will generate output in the <messages> tab.
    copy/paste all of this text into a new query window and run it.
    that will create the views
*/


declare @SQL		nvarchar(4000)
declare @DB		nvarchar(50)


declare csrDB cursor FAST_FORWARD for
    select quotename(dbnamepatton)
    from rewardsnow.dbo.dbprocessinfo dbpi join master.dbo.sysdatabases sdb
	   on dbnamepatton = sdb.name
    order by dbnumber

open csrDB

fetch next from csrDB into @DB
while @@FETCH_STATUS = 0
BEGIN



-------------------------------------------------------------------------------------------------
--  CUSTOMER
-------------------------------------------------------------------------------------------------

    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwCustomer'') 
				drop view dbo.vwCustomer

			 '

--    print @sql
    exec sp_executesql @sql


    set @sql = 'use ' + @db + ';
			 GO

			 create view [dbo].[vwCustomer]
			 as
			 select *
			 from ' + @db + '.dbo.customer'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL



    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwCustomer_stage'') 
				drop view dbo.vwCustomer_stage

			 '

--    print @sql

    exec sp_executesql @SQL


    set @sql = 'use ' + @db + '; 
			 GO

			 create view [dbo].[vwCustomer_stage]
			 as
			 Select *
			  from ' + @db + '.dbo.Customer_stage'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL




    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwCustomerdeleted'') 
				drop view dbo.vwCustomerdeleted'

--    print @sql

    exec sp_executesql @SQL

    set @sql = 'use ' + @db + '; 
			 GO

			 create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from ' + @db + '.dbo.Customerdeleted'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL



-------------------------------------------------------------------------------------------------
--  AFFILIAT
-------------------------------------------------------------------------------------------------



    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwAffiliat'') 
				drop view dbo.vwaffiliat'

--    print @sql
    exec sp_executesql @sql


    set @sql = 'use ' + @db + ';
			 GO

			 create view [dbo].[vwAffiliat]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			 from ' + @db + '.dbo.affiliat'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL



    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwAffiliat_stage'') 
				drop view dbo.vwaffiliat_stage'

--    print @sql

    exec sp_executesql @SQL

    set @sql = 'use ' + @db + '; 
			 GO

			 create view [dbo].[vwAffiliat_stage]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			  from ' + @db + '.dbo.affiliat_stage'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL




    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwAffiliatdeleted'') 
				drop view dbo.vwaffiliatdeleted'

--    print @sql

    exec sp_executesql @SQL

    set @sql = 'use ' + @db + '; 
			 GO

			 create view [dbo].[vwAffiliatdeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID, datedeleted
			  from ' + @db + '.dbo.affiliatdeleted'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL



-------------------------------------------------------------------------------------------------
--  HISTORY
-------------------------------------------------------------------------------------------------




    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwHistory'') 
				drop view dbo.vwHistory '

    exec sp_executesql @SQL

    set @SQL = 'use ' + @db + '
			  GO 

			 create view [dbo].[vwHistory]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from ' + @db + '.dbo.history'
    print @sql
    print '
		  GO
		  '

    --exec sp_executesql @SQL




    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwHistory_Stage'') 
				   drop view dbo.vwHistory_Stage'
    exec sp_executesql @sql

    set @sql = 'use ' + @db + '
			 GO

			 create view [dbo].[vwHistory_Stage]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from ' + @db + '.dbo.history_Stage'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL



    set @SQL = 'use ' + @db + '; 
			 if exists(select 1 from dbo.sysobjects where name = ''vwHistorydeleted'') 
				   drop view dbo.vwHistorydeleted '
    exec sp_executesql @sql


    set @sql = 'use ' + @db + '
			 GO

			 create view [dbo].[vwHistorydeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
			 from ' + @db + '.dbo.historydeleted'
    print @sql
    print '
		  GO
		  '

--    exec sp_executesql @SQL



    fetch next from csrDB into @DB
END

close csrDB
deallocate csrDB
