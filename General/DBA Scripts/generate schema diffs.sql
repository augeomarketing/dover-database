--**************************************************************************************************************--
--                                                                                                              --
-- Schema duplication tool.  Make sure this script is run from the database used as the @SrcDB                  --
-- Change the @SrcDB and @TblToCompare to the appropriate values, then run.  This will add any columns that     --
-- do not exist in a table in each database that exist in the source table.                                     --
--                                                                                                              --
--**************************************************************************************************************--


declare @db			   nvarchar(50)
declare @sql			   nvarchar(4000)
declare @altersql		   nvarchar(4000)

declare @objectid		   int

declare @tblSrcCols		   table
    (ColumnNm	 		   varchar(50) primary key,
     DataType			   varchar(255),
     columnwidth		   bigint)

declare @srcDB			   nvarchar(50)
declare @tbltocompare	   varchar(50)
declare @NbrColumnsdiff	   int

declare @loopctr		   int
declare @column		   varchar(50)
declare @type			   varchar(50)
declare @width			   int

-- Set Source database & table.  These are the template on which the other dbs will be compared against
set @srcDB = 'Advantage'
set @tbltocompare = 'Account'

-- Get the SQL Object ID for the table
set @sql = 'select @objectid = (select object_id from ' + @srcdb + '.sys.objects where name = ' + CHAR(39) + @tbltocompare + CHAR(39) + ')'
exec sp_executesql @sql, N'@objectid int OUTPUT', @objectid = @objectid OUTPUT

--
-- load in the columns, data types and widths into the temp table
insert into @tblSrcCols
(ColumnNm, DataType, columnwidth)
select sc.name as columnnm, st.name as datatype, sc.max_length as columnwidth
from sys.columns sc join sys.types st
    on sc.system_type_id = st.system_type_id
where sc.object_id = @objectid
order by column_id


--
-- Drop temp tables if they exist
if exists(select 1 from tempdb.sys.objects where LEFT(name, 8) = '#tblcols')
    drop table #tblcols

if exists(select 1 from tempdb.sys.objects where LEFT(name, 15) = '#missingColumns')
    drop table #missingColumns


create table #tblcols
    (ColumnNm	 		   varchar(50) primary key,
     DataType			   varchar(255),
     columnwidth		   bigint)

create table #missingColumns
(ColumnNm	 		   varchar(50) primary key,
 DataType			   varchar(255),
 columnwidth		   bigint)



--
-- Cursor through sysdatabases, except for those listed.  These databases will have the table synced with the source table defined above
declare csrDB cursor fast_forward for
    select name
    from master.dbo.sysdatabases
    where name not in ('master', 'model', 'msdb', 'tempdb', 'management', 'onlinehistorywork', 'piknclik', 'pins', 
				    'carlson', 'fullfillment', 'tunes', 'principal', 'rewardsnow', @srcdb)
    and name not like 'lite%'
    and name not like 'catal%'
    and name not like 'logi%'
    and name not like 'testclass%'
    order by name

open csrdb

fetch next from csrdb into @db
while @@FETCH_STATUS = 0
BEGIN

    -- Flush temp tables
    truncate table #tblcols
    truncate table #missingColumns

    -- Get objectid for the database's 1Security table
    set @sql = 'select @objectid = (select object_id from ' + @db + '.sys.objects where name = ' + CHAR(39) + @tbltocompare + CHAR(39) + ')'
    --print @sql
    exec sp_executesql @sql, N'@objectid int OUTPUT', @objectid = @objectid OUTPUT

    -- Get columns in the database being processed
    set @sql = '
	   insert into #tblCols (Columnnm, DataType, ColumnWidth)
	   select sc.name as columnnm, st.name as datatype, sc.max_length as columnwidth
	   from ' + @db + '.sys.columns sc join ' + @db + '.sys.types st
		  on sc.system_type_id = st.system_type_id
	   where sc.object_id = @objectid
	   order by column_id'
    --print @sql
    exec sp_executesql @sql, N'@ObjectID int', @objectid = @objectid


    -- Now, what columns are missing?
    insert into #missingColumns
    select src.*
    from @tblSrcCols src left outer join #tblcols tbl
	   on src.ColumnNm = tbl.ColumnNm
    where tbl.ColumnNm is null

    -- Get row count.  If row count = 0, the database/table being processed has the same columns as the source above.
    set @NbrColumnsdiff = @@ROWCOUNT

    if @NbrColumnsdiff > 0
    BEGIN

	   -- Build the SQL Statement to do the ALTER TABLE
	   set @sql = 'ALTER TABLE ' + quotename(@db) + '.dbo.' + quotename(@tbltocompare) + '    ADD  '
	   set @altersql = ''

	   set @loopctr = 1
	   while @loopctr <= @NbrColumnsdiff
	   BEGIN
    	   
		  select top 1 @column = columnnm, @type = datatype, @width = columnwidth from #missingColumns

		  set @altersql = @altersql + @column + '  ' + @type

		  if @type in ('varchar', 'nvarchar', 'char', 'nchar')
			 set @altersql = @altersql + '(' + CAST(@width as nvarchar(10)) + ')'


		  if @loopctr != @NbrColumnsdiff
			 set @altersql = @altersql + ',     '
        

		  delete from #missingColumns where ColumnNm = @column
		  set @loopctr = @loopctr + 1
	   END

	   
	  set @sql = @sql + @altersql

	  print @sql
	  exec sp_executesql @sql

    END


    fetch next from csrdb into @db
END


close csrdb
deallocate csrdb



