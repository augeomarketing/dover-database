--select quotename(name)
--from pattondbases
--where name not in ('tempdb', 'msdb', 'master')


declare @DBName		nvarchar(100)
declare @SQL			nvarchar(4000)
Declare @filenumber		int
declare @GUID			uniqueidentifier

set @GUID = newid()


declare csrDB cursor FAST_FORWARD for
	select  name
	from northwind.dbo.pattondbases
	where name not in ('tempdb', 'msdb', 'master') and name not like 'z%'

print ' '

open csrDB

fetch next from csrDB into @DBName

while @@FETCH_STATUS = 0
BEGIN


	set @SQL = '
	exec master.dbo.xp_restore_database @database = ' + char(39) + @DBName + char(39) + ',
		@filename = ' + char(39) + 'C:\dbrestores\' + @DBName + '.bak' + char(39) + ',
		@filenumber = 1,
		@with = N''REPLACE'',
		@with = N''STATS = 10'',
		@with = N''MOVE '  + char(39) + char(39) +  @DBName + '_Data' + char(39) +char(39) + '  TO  ' + char(39) + char(39) + 'C:\SQLData\' +  @DBName + '_Data.mdf' + char(39) + char(39) + char(39) + ',
		@with = N''MOVE '  + char(39) + char(39) +  @DBName + '_Log'  + char(39) +char(39) + '  TO  ' + char(39) + char(39) + 'C:\SQLData\' +  @DBName + '_Log.ldf' + char(39) + char(39) +  char(39) + ',
		@affinity = 0,
		@logging = 0'



/*
	set @SQL = '
	exec master.dbo.xp_restore_database @database = ' + @DBName + ',
	@filename =  ' + char(39) + 'C:\DBRestores\' + replace(replace(@DBName, '[', ''), ']', '') + '.bak' + char(39) + ', 
	@filenumber = 1,
	@with = N''REPLACE'',
	@with = N''STATS = 10'',
	@affinity = 0,
	@logging = 0'
*/

	print 'Begin RESTORE up database: ' + @dbname
	exec sp_executesql @sql
	print 'Done RESTORING up database: ' + @dbname
	print ' '
	print ' '

	fetch next from csrDB into @DBName
END

close csrDB
deallocate csrDB