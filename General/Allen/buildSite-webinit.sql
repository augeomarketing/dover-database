declare @tipfirst char(3) 
declare @loyaltyid int 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @LIB_template_Sid  int
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @custtravfee int
declare @website varchar(1024)

  
 --Update variables 
set @loyaltyid = 2
set @tipfirst = '217' 
set @finame = 'Callaway Bank' 
set @clientcode = 'Callaway'
set @CustomerServicePhone = '1-877-446-7937'
set @programname = 'Callaway Bank Rewards'
set @travelincrement = 2500
set @travelbottom = 5000
set @cashbackmin = 9999999
set @merch = 1
set @cashback = 0
set @AirFee = 10
set @logo = ''
set @termspage = @tipfirst + 'terms.asp'
set @faqpage = @tipfirst + 'faq.asp'
set @earnpage = @tipfirst + 'earning.asp'
--
-- Check certs down below
--
set @Business = 0
set @statementdefault = 1
set @StmtNum = 2
set @LIB_template_Sid =  2 --(Select top 1 sid_loyalty_id from catalog.dbo.loyalty where dim_loyalty_liab =1 order by sid_loyalty_id)
set @database = 'callaway'
set @liab = 1
set @custtravfee = 1
set @website = 'www.rewardsnow.com/callaway/testing/'


SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst


IF @@ROWCOUNT = 0
BEGIN
	insert into clientaward (TIPFirst, ClientCode) VALUES (@tipfirst,@clientcode)

	insert into client (ClientCode, ClientName, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault, StmtNum, CustomerServicePhone)
	select @clientcode, @finame, @programname, '02/28/2009', @travelincrement, @travelbottom, @cashbackmin, @merch, @AirFee,  @logo,  @termspage, @faqpage, @earnpage, @Business, @statementdefault, @StmtNum, @CustomerServicePhone
END
ELSE
BEGIN 
	SET @clientcode = (SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst)
	UPDATE client 
		SET
		RNProgramName = @programname, 
		TravelMinimum = @travelincrement, 
		TravelBottom = @travelbottom, 
		CashBackMinimum = @cashbackmin,  
		Merch = @merch, 
		AirFee = @AirFee, 
		logo = @logo, 
		termspage = @termspage, 
		faqpage = @faqpage, 
		earnpage = @earnpage, 
		Business = @Business, 
		statementdefault = @statementdefault, 
		StmtNum = @StmtNum, 
		CustomerServicePhone = @CustomerServicePhone
		WHERE clientcode = @clientcode
END

SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec)
	values (@tipfirst, @finame, @database, 'Certs', 1, 1, 1, 1, @cashback, @liab, 1, @custtravfee, 1, 0)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.searchdb 
	SET
		ClientName = @finame, 
		dbase = @database, 
		--certspage = 'Certs', 
		Website = 1, 
		newstyle = 1 , 
		history = 1 , 
		encrypted = 1, 
		CASH = @cashback, 
		LIAB = @liab, 
		QTR = 1, 
		Custtravelfee = @custtravfee, 
		ratio = 1, 
		disp_dec = @cashback
	WHERE tipfirst = @tipfirst
END

select sid_webinit_id FROM rewardsnow.dbo.webinit WHERE dim_webinit_defaulttipprefix = @tipfirst
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO rewardsnow.dbo.webinit (dim_webinit_defaulttipprefix, dim_webinit_database, dim_webinit_baseurl, dim_webinit_javascript, dim_webinit_cssstyle, dim_webinit_cssmenu, dim_webinit_cssstructure, dim_webinit_cssstyleprint, dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning)
	VALUES (@Tipfirst, @database, @website, @tipfirst + 'custom.js', @tipfirst + 'style.css', @tipfirst + 'menu.css', @tipfirst + 'style_structure.css', 'stylePrint.css','menuPrint.css','style_structurePrint.css', @programname, @programname, @termspage, @faqpage, @earnpage)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.webinit 
		SET
			dim_webinit_database = @database, 
			dim_webinit_baseurl = @website, 
			dim_webinit_programname = @programname, 
			dim_webinit_sitetitle = @programname, 
			dim_webinit_defaultterms = @termspage, 
			dim_webinit_defaultfaq = @faqpage, 
			dim_webinit_defaultearning =	@earnpage
		WHERE dim_webinit_defaulttipprefix = @tipfirst
END
