Declare newtip cursor 
for select distinct left(travnum,3) as newtip 
		from statement s
			left outer join rewardsnow.dbo.webstatement WS
				ON left(s.travnum,3) = ws.dim_webstatement_tipfirst
		where dim_webstatement_tipfirst IS NULL

declare @newtip varchar(3)
Declare @sqlcmd nvarchar(4000)
declare @tip_template varchar(3)

set @tip_template = '102'

open newtip
Fetch next from newtip into @newtip
while @@Fetch_Status = 0
	Begin
	  set @sqlcmd =	'
		insert into rewardsnow.dbo.webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst, dim_webstatement_active)
		select sid_webstatementtype_id, dim_webstatement_dbfield, ' + quotename(@newtip,'''') + ' as dim_webstatement_tipfirst, dim_webstatement_active
		from rewardsnow.dbo.webstatement
		where dim_webstatement_tipfirst = ' + quotename(@tip_template,'''')
	  print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	'
		insert into rewardsnow.dbo.webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
		select sid_webstatementtype_id, dim_webstatementtypeorder_order, '+ quotename(@newtip,'''') + ' as dim_webstatementtypeorder_tipfirst
		from rewardsnow.dbo.webstatementtypeorder
		where dim_webstatementtypeorder_tipfirst = ' + quotename(@tip_template,'''')
	  print @sqlcmd
	  print '---'
	  EXECUTE sp_executesql @SqlCmd
      
	  fetch Next from newtip into @newtip
	end
Close newtip
Deallocate newtip
