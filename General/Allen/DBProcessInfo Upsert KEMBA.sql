declare @tipfirst varchar(3) = '254'
declare @pattondb varchar(50) = '254'
declare @rn1db varchar(50) = 'KEMBA'
declare @dbavailable char(1) = 'P'
declare @clientcode varchar(50) = 'KEMBA'
declare @clientname varchar(50) = 'KEMBA Financial CU'
declare @programname varchar(50) = 'MyAdvantage Points' 
declare @expiration_years int = 3
declare @expiration_type varchar(2) = 'ME'
declare @datejoined datetime = getdate()
declare @extract_gcs varchar(1) = 'N'
declare @extract_cash varchar(1) = 'Y'
declare @transfer_history varchar(1) = 'Y'
declare @hasonlinebooking int = 0
declare @vesdia varchar(1) = 'P'
declare @accessdev varchar(1) = 'N'
declare @exp_points_display int = 0
declare @exp_points_offset int = 0

update rewardsnow.dbo.dbprocessinfo
set DBNumber = @tipfirst,
	DBNamePatton = @pattondb,
	DBNameNEXL = @rn1db,
	DBAvailable = @dbavailable,
	ClientCode = @clientcode,
	ClientName = @clientname,
	ProgramName = @programname,
	DBLocationPatton = @pattondb,
	DBLocationNexl = @rn1db,
	PointExpirationYears = @expiration_years,
	PointsExpireFrequencyCd = @expiration_type,
	DateJoined = @datejoined,
	ExtractCashBack = @extract_cash,
	ExtractGiftCards = @extract_gcs,
	RNProgramName = @programname,
	TransferHistToRn1 = @transfer_history,
	hasonlinebooking = @hasonlinebooking,
	VesdiaParticipant = @vesdia,
	AccessDevParticipant = @accessdev,
	ExpPointsDisplay = @exp_points_display,
	ExpPointsDisplayPeriodOffset = @exp_points_offset
where DBNumber = @tipfirst

if @@ROWCOUNT = 0
begin
	insert into RewardsNow.dbo.dbprocessinfo (DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName,
		ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, PointsExpireFrequencyCd, DateJoined, ExtractCashBack, 
		ExtractGiftCards, RNProgramName, TransferHistToRn1, hasonlinebooking, VesdiaParticipant, AccessDevParticipant, ExpPointsDisplay, 
		ExpPointsDisplayPeriodOffset)
	values (@tipfirst, @pattondb, @rn1db, @dbavailable, @clientcode, @clientname, @programname, @pattondb, @rn1db,
			@expiration_years, @expiration_type, @datejoined, @extract_cash, @extract_gcs, @programname, @transfer_history,
			@hasonlinebooking, @vesdia, @accessdev, @exp_points_display, @exp_points_offset)
	print 'Patton insert'
end

update catalog.rewardsnow.dbo.dbprocessinfo
set DBNumber = @tipfirst,
	DBNamePatton = @pattondb,
	DBNameNEXL = @rn1db,
	DBAvailable = @dbavailable,
	ClientCode = @clientcode,
	ClientName = @clientname,
	ProgramName = @programname,
	DBLocationPatton = @pattondb,
	DBLocationNexl = @rn1db,
	PointExpirationYears = @expiration_years,
	PointsExpireFrequencyCd = @expiration_type,
	DateJoined = @datejoined,
	ExtractCashBack = @extract_cash,
	ExtractGiftCards = @extract_gcs,
	RNProgramName = @programname,
	TransferHistToRn1 = @transfer_history,
	hasonlinebooking = @hasonlinebooking,
	VesdiaParticipant = @vesdia,
	AccessDevParticipant = @accessdev,
	ExpPointsDisplay = @exp_points_display,
	ExpPointsDisplayPeriodOffset = @exp_points_offset
where DBNumber = @tipfirst

if @@ROWCOUNT = 0
begin
	insert into catalog.RewardsNow.dbo.dbprocessinfo (DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName,
		ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, PointsExpireFrequencyCd, DateJoined, ExtractCashBack, 
		ExtractGiftCards, RNProgramName, TransferHistToRn1, hasonlinebooking, VesdiaParticipant, AccessDevParticipant, ExpPointsDisplay, 
		ExpPointsDisplayPeriodOffset)
	values (@tipfirst, @pattondb, @rn1db, @dbavailable, @clientcode, @clientname, @programname, @pattondb, @rn1db,
			@expiration_years, @expiration_type, @datejoined, @extract_cash, @extract_gcs, @programname, @transfer_history,
			@hasonlinebooking, @vesdia, @accessdev, @exp_points_display, @exp_points_offset)
	print 'RN1 insert'
end