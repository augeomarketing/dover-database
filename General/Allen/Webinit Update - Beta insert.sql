declare @tipfirst varchar(3)
declare @url varchar(50)
set @tipfirst = '700'
set @url = 'beta.rewardsnow.com/rewards/'

use rewardsnow
insert into webinit (dim_webinit_defaulttipprefix, dim_webinit_database, dim_webinit_baseurl, dim_webinit_javascript, dim_webinit_cssstyle, dim_webinit_cssmenu, dim_webinit_cssstructure, dim_webinit_cssstyleprint, dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning)
select top 1 @tipfirst as dim_webinit_defaulttipprefix, 'testclass990' as dim_webinit_database, @url as dim_webinit_baseurl, dim_webinit_javascript, dim_webinit_cssstyle, dim_webinit_cssmenu, dim_webinit_cssstructure, dim_webinit_cssstyleprint, dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning
from webinit
where dim_webinit_defaulttipprefix = @tipfirst

select top 5 * from webinit
order by sid_webinit_id desc