declare @tipfirst char(3) 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @dynamicCss int
declare @custtravfee int
declare @travelccreqd int
declare @website varchar(1024)
declare @travelratio numeric(18,0)
declare @ssoonly int
declare @payforpoints int

  
 --Update variables 
set @tipfirst = '52Y' 
set @finame = 'Security State Bank'
set @clientcode = 'ADVSSB' 
set @CustomerServicePhone = '1-866-POINTS2U'
set @programname = 'Advantage Network' 
set @travelincrement = 2500
set @travelbottom = 5000
set @cashbackmin = 999999
set @merch = 1
set @cashback = 0
set @AirFee = 0
set @logo = ''
set @termspage = @tipfirst + 'terms.asp'
set @faqpage = @tipfirst + 'faq.asp'
set @earnpage = @tipfirst + 'earning.asp'
set @Business = 0
set @statementdefault = 1
set @StmtNum = 4
set @database = 'Metavante'
set @liab = 0
set @custtravfee = 0
set @travelccreqd = 1
set @website = 'www.points2u.com/advantage/'
set @travelratio = 100 --100 for most, 200 for Omni... points divided by travelratio = dollars
set @dynamicCss = 0
set @ssoonly = 0
set @payforpoints = 0


SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst


IF @@ROWCOUNT = 0
BEGIN
	insert into client (ClientCode, ClientName, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault, StmtNum, CustomerServicePhone)
	select @clientcode, @finame, @programname, GETDATE(), @travelincrement, @travelbottom, @cashbackmin, @merch, @AirFee,  @logo,  @termspage, @faqpage, @earnpage, @Business, @statementdefault, @StmtNum, @CustomerServicePhone
	
	insert into clientaward (TIPFirst, ClientCode) VALUES (@tipfirst,@clientcode)
END
ELSE
BEGIN 
	SET @clientcode = (SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst)
	UPDATE client 
		SET
		RNProgramName = @programname, 
		TravelMinimum = @travelincrement, 
		TravelBottom = @travelbottom, 
		CashBackMinimum = @cashbackmin,  
		Merch = @merch, 
		AirFee = @AirFee, 
		logo = @logo, 
		termspage = @termspage, 
		faqpage = @faqpage, 
		earnpage = @earnpage, 
		Business = @Business, 
		statementdefault = @statementdefault, 
		StmtNum = @StmtNum, 
		CustomerServicePhone = @CustomerServicePhone
		WHERE clientcode = @clientcode
END

SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec, travelratio, TravelCCReqdInProgram)
	values (@tipfirst, @finame, @database, 'Certs', 1, 1, 1, 1, @cashback, @liab, 1, @custtravfee, 1, 0, @travelratio, @travelccreqd)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.searchdb 
	SET
		ClientName = @finame, 
		dbase = @database, 
		--certspage = 'Certs', 
		Website = 1, 
		newstyle = 1 , 
		history = 1 , 
		encrypted = 1, 
		CASH = @cashback, 
		LIAB = @liab, 
		QTR = 1, 
		Custtravelfee = @custtravfee, 
		ratio = 1, 
		disp_dec = 0, 
		travelratio = @travelratio,
		TravelCCReqdInProgram = @travelccreqd
	WHERE tipfirst = @tipfirst
END

IF @payforpoints <> 0
BEGIN
	INSERT INTO RewardsNOW.dbo.payforpoints (dim_payforpoints_tipfirst, dim_payforpoints_minratio, dim_payforpoints_maxratio, dim_payforpoints_minpoints, dim_payforpoints_maxpointsforratio, dim_payforpoints_increment)
	VALUES (@tipfirst, 0.035, 0.020, 250, 5000, 50)
end