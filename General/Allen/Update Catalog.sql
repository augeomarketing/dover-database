use catalog

declare @oldtip char(3)
declare @newtip char(3)
declare @grouplist varchar(20)

set @oldtip = '002'
set @newtip = '515'
set @grouplist = '3, 13, 21'


exec dbo.usp_RebuildCatalog_v2 @oldtip, 
	@newtip, 
	@grouplist

print @newtip