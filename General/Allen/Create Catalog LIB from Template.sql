use catalog

--begin tran

declare @sid_loyaltycatalog_id_seed int
declare @sid_editloyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @loyaltyid	int
declare @LIB_template_Sid  int

------------------------------
--  SET LOYALTY ID HERE!!!
------------------------------
set @loyaltyid = 127

------------------------------
-- get template sid (the one you're copying from)
------------------------------
set @LIB_template_Sid = 24 --(Select top 1 sid_loyalty_id from catalog.dbo.loyalty where dim_loyalty_liab =1 order by sid_loyalty_id)


-- get max sid_loyaltycatalog_id from loyaltycatalog id.  increment by 1.  this
-- becomes the identity seed for the temp tables
set @sid_loyaltycatalog_id_seed = (select max(sid_loyaltycatalog_id) + 1 from catalog.dbo.loyaltycatalog)
set @sid_editloyaltycatalog_id_seed = (select max(sid_editloyaltycatalog_id) + 1 from catalog.dbo.editloyaltycatalog)

-- drop table if it exists.  This is necessary so the identity seed can be set properly
-- truncate won't work
if exists(select 1 from catalog.dbo.sysobjects where [name] = 'tmp_loyaltycatalog')
begin
	drop table catalog.dbo.tmp_loyaltycatalog
END

if exists(select 1 from catalog.dbo.sysobjects where [name] = 'tmp_editloyaltycatalog')
begin
	drop table catalog.dbo.tmp_editloyaltycatalog
END

-- Dynamic SQL to create table.  Using dynamic SQL to allow us to specify the identity seed
set @sql = '
	CREATE TABLE catalog.[dbo].[tmp_loyaltycatalog](
		[sid_loyaltycatalog_id] [int] IDENTITY(' + cast(@sid_loyaltycatalog_id_seed as nvarchar(10)) + ', 1) NOT NULL,
		[sid_loyalty_id] [int] NOT NULL,
		[sid_catalog_id] [int] NOT NULL,
		[dim_loyaltycatalog_pointvalue] [int],
		[dim_loyaltycatalog_bonus] [int],
		[dim_loyaltycatalog_active] [int]
	)'

-- execute sql to create the table
exec sp_executesql @sql

---- Dynamic SQL to create edit table.  Using dynamic SQL to allow us to specify the identity seed
--set @sql = '
--	CREATE TABLE catalog.[dbo].[tmp_editloyaltycatalog](
--		[sid_editloyaltycatalog_id] [int] IDENTITY(' + cast(@sid_editloyaltycatalog_id_seed as nvarchar(10)) + ', 1) NOT NULL,
--		[sid_loyaltycatalog_id] [int] NOT NULL,
--		[sid_loyalty_id] [int] NOT NULL,
--		[sid_catalog_id] [int] NOT NULL,
--		[dim_loyaltycatalog_pointvalue] [int],
--		[dim_loyaltycatalog_bonus] [int],
--		[dim_loyaltycatalog_active] [int]
--	)'

---- execute sql to create the table
--exec sp_executesql @sql

-- insert into temp table list of catalog items to create a list of items for a new
-- "Loyalty in a Box" client
insert into catalog.dbo.tmp_loyaltycatalog
(sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
select  @loyaltyid, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
from catalog.dbo.loyaltycatalog
where sid_loyalty_id = @LIB_template_Sid  
and	dim_loyaltycatalog_active = 1
and sid_catalog_id not in (
	select sid_catalog_id 
		from catalog.dbo.loyaltycatalog 
		where sid_loyalty_id = @loyaltyid)
		
---- insert into temp table edit list of catalog items to create a list of items for a new
---- "Loyalty in a Box" client
--insert into catalog.dbo.tmp_editloyaltycatalog
--(sid_editloyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
--select  @loyaltyid, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
--from catalog.dbo.editloyaltycatalog
--where sid_loyalty_id = @LIB_template_Sid  
----and	dim_loyaltycatalog_active = 1
--and sid_catalog_id not in (
--	select sid_catalog_id 
--		from catalog.dbo.editloyaltycatalog 
--		where sid_loyalty_id = @loyaltyid)


----------------------------------------------------------------------
-- Now insert result set into the catalog.dbo.loyaltycatalog table
----------------------------------------------------------------------

-- Add new loyalty in a box program to PATTON\RN
insert into catalog.dbo.loyaltycatalog
(sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
 dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
select sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
 dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
from catalog.dbo.tmp_loyaltycatalog

-- Add new loyalty in a box program to RN1
insert into [RN1].catalog.dbo.loyaltycatalog
(dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
 dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
select dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
 dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
from catalog.dbo.tmp_loyaltycatalog

---- Add new edits to PATTON\RN
--insert into catalog.dbo.editloyaltycatalog
--(sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
-- dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
--select sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
-- dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
--from catalog.dbo.tmp_editloyaltycatalog

---- Add new edits to RN1
--insert into [RN1].catalog.dbo.editloyaltycatalog
--(dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
-- dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
--select dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue,
-- dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
--from catalog.dbo.tmp_editloyaltycatalog

--rollback tran
--print 'Transaction rolled back!'