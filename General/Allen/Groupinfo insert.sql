-- Run from Patton

declare @name varchar(50) = 'UNH_Apparel'
declare @desc varchar(1024) = 'UNH Apparel'
declare @trancode varchar(2) = 'RM'

insert into catalog.dbo.groupinfo(dim_groupinfo_name, dim_groupinfo_description, dim_groupinfo_trancode)
values (@name, @desc, @trancode)

insert into rn1.catalog.dbo.groupinfo(dim_groupinfo_name, dim_groupinfo_description, dim_groupinfo_trancode)
values (@name, @desc, @trancode)

select top 5 * from catalog.dbo.groupinfo order by sid_groupinfo_id desc
select top 5 * from rn1.catalog.dbo.groupinfo order by sid_groupinfo_id desc