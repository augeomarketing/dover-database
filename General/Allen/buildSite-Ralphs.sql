declare @tipfirst char(3) 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @dynamicCss int
declare @custtravfee int
declare @travelccreqd int
declare @website varchar(1024)
declare @travelratio numeric(18,0)
declare @ssoonly int
declare @payforpoints int

  
 --Update variables 
set @tipfirst = '52R' 
set @finame = 'Ralphs'
set @clientcode = 'Ralph' 
set @CustomerServicePhone = '1-603-516-3440'
set @programname = 'Ralphs' 
set @travelincrement = 0
set @travelbottom = 0
set @cashbackmin = 0
set @merch = 0
set @cashback = 0
set @AirFee = 0
set @logo = ''
set @termspage = @tipfirst + 'terms.asp'
set @faqpage = @tipfirst + 'faq.asp'
set @earnpage = @tipfirst + 'earning.asp'
set @Business = 0
set @statementdefault = 1
set @StmtNum = 4
set @database = 'Metavante'
set @liab = 0
set @custtravfee = 0
set @travelccreqd = 0
set @website = 'www.rewardsnow.com/ralphs/'
set @travelratio = 0 --100 for most, 200 for Omni... points divided by travelratio = dollars
set @dynamicCss = 0
set @ssoonly = 0
set @payforpoints = 0

select sid_webinit_id FROM rewardsnow.dbo.webinit WHERE dim_webinit_defaulttipprefix = @tipfirst
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO rewardsnow.dbo.webinit (dim_webinit_defaulttipprefix, dim_webinit_database, dim_webinit_baseurl, dim_webinit_javascript, dim_webinit_cssstyle, dim_webinit_cssmenu, dim_webinit_cssstructure, dim_webinit_cssstyleprint, dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning, dim_webinit_ssoonly)
	VALUES (@Tipfirst, @database, @website, @tipfirst + 'custom.js', @tipfirst + 'style.css', @tipfirst + 'menu.css', @tipfirst + 'style_structure.css', 'stylePrint.css','menuPrint.css','style_structurePrint.css', @programname, @programname, @termspage, @faqpage, @earnpage, @ssoonly)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.webinit 
		SET
			dim_webinit_database = @database, 
			dim_webinit_baseurl = @website, 
			dim_webinit_programname = @programname, 
			dim_webinit_sitetitle = @programname, 
			dim_webinit_defaultterms = @termspage, 
			dim_webinit_defaultfaq = @faqpage, 
			dim_webinit_defaultearning = @earnpage,
			dim_webinit_ssoonly = @ssoonly
		WHERE dim_webinit_defaulttipprefix = @tipfirst
END

