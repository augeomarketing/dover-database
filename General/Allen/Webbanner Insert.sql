use rewardsnow
--select * from webbanner

declare @tipfirst varchar(3)
declare @banner1image varchar(100)
declare @banner1link varchar(100)
declare @banner2image varchar(100)
declare @banner2link varchar(100)
declare @banner3image varchar(100)
declare @banner3link varchar(100)
declare @banner4image varchar(100)
declare @banner4link varchar(100)

set @tipfirst = '231'
set @banner1image = './images/bannerAds/banner_bestbuy.jpg'
set @banner1link = 'giftcard_category.asp'
set @banner2image = './images/bannerAds/banner_homedepot.jpg'
set @banner2link = 'giftcard_category.asp'
set @banner3image = './images/bannerAds/banner_kohls.jpg'
set @banner3link = 'giftcard_category.asp'
set @banner4image = './images/bannerAds/banner_walmart.jpg'
set @banner4link = 'giftcard_category.asp'

update webbanner
set dim_webbanner_banner1image = @banner1image, 
	dim_webbanner_banner1link = @banner1link, 
	dim_webbanner_banner2image = @banner2image, 
	dim_webbanner_banner2link = @banner2link, 
	dim_webbanner_banner3image = @banner3image, 
	dim_webbanner_banner3link = @banner3link, 
	dim_webbanner_banner4image = @banner4image, 
	dim_webbanner_banner4link = @banner4link
where dim_webbanner_tipprefix = @tipfirst

if @@rowcount = 0
	begin
		print 'Inserted'
		insert into webbanner (dim_webbanner_tipprefix, dim_webbanner_banner1image, dim_webbanner_banner1link, dim_webbanner_banner2image, dim_webbanner_banner2link, dim_webbanner_banner3image, dim_webbanner_banner3link, dim_webbanner_banner4image, dim_webbanner_banner4link)
		values (@tipfirst, @banner1image, @banner1link, @banner2image, @banner2link, @banner3image, @banner3link, @banner4image, @banner4link)
	end

select top 5 * from webbanner where dim_webbanner_tipprefix = @tipfirst