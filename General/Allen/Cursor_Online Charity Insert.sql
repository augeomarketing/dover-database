Declare clientcode cursor 
for select clientcode
		from newtnb.dbo.client c
		where rnprogramname = 'Rewards2U'

declare @clientcode varchar(20)
Declare @sqlcmd nvarchar(4000)
declare @tip_template varchar(3)

open clientcode
Fetch next from clientcode into @clientcode
while @@Fetch_Status = 0
	Begin
	  set @sqlcmd =	'
		insert into charity_client (charity_id, clientcode, points, charity_value) values (
		''1'', ' + quotename(@clientcode, '''') + ', ''3500'', ''25'')'
	  print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	'
		insert into charity_client (charity_id, clientcode, points, charity_value) values (
		''2'', ' + quotename(@clientcode, '''') + ', ''7000'', ''50'')'
	  print @sqlcmd
	  print '---'
	  EXECUTE sp_executesql @SqlCmd
      
	  fetch Next from clientcode into @clientcode
	end
Close clientcode
Deallocate clientcode
