use rewardsnow

declare @oldtip char(3)
declare @newtip char(3)

set @oldtip = '643'
set @newtip = '644'

insert into dbo.webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst, dim_webstatement_active)
select sid_webstatementtype_id, dim_webstatement_dbfield, @newtip as dim_webstatement_tipfirst, dim_webstatement_active
from dbo.webstatement
where dim_webstatement_tipfirst = @oldtip

insert into dbo.webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
select sid_webstatementtype_id, dim_webstatementtypeorder_order, @newtip as dim_webstatementtypeorder_tipfirst
from dbo.webstatementtypeorder
where dim_webstatementtypeorder_tipfirst = @oldtip