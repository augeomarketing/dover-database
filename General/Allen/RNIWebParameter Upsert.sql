declare @tipfirst varchar(3) = 'V01',
		@key varchar(255) = 'LOOKUP_PASSWORD',
		@value varchar(255) = 'DHC83NC1'

update rewardsnow.dbo.RNIWebParameter
set dim_rniwebparameter_value = @value
where sid_dbprocessinfo_dbnumber = @tipfirst
	and dim_rniwebparameter_key = @key
	
if @@ROWCOUNT = 0
begin
	insert into rewardsnow.dbo.RNIWebParameter (sid_dbprocessinfo_dbnumber, dim_rniwebparameter_key, dim_rniwebparameter_value)
	values (@tipfirst, @key, @value)
end

select * from RewardsNOW.dbo.RNIWebParameter where dim_rniwebparameter_key = @key and sid_dbprocessinfo_dbnumber = @tipfirst