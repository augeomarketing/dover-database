begin tran

	Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
	Values ('52J999999999996','52J','999999999996','PERM TEST','100 MAIN ST','SUITE 350','DOVER NH 03820','03820','123','0','123','A') 
	  
	Insert into Account (Tipnumber, Lastname, Lastsix, MemberNumber) 
	Values ('52J999999999996','TEST','000037', '49865002019') 
	  
	INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther) 
	Values ('52J999999999996', '52Jtest2', 'sample', 'N', 'ssmith@rewardsnow.com', 'N') 
	
	Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
	Values ('52J999999999995','52J','999999999995','THAT BUSINESS','100 MAIN ST','SUITE 350','DOVER NH 03820','03820','-456','0','-456','A') 
	  
	Insert into Account (Tipnumber, Lastname, Lastsix, MemberNumber) 
	Values ('52J999999999995','BUSINESS','000045', '49865001446') 
	  
	INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther) 
	Values ('52J999999999995', '52Jtest3', 'sample', 'N', 'ssmith@rewardsnow.com', 'N') 

	Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
	Values ('52J999999999994','52J','999999999994','THAT BUSINESS','100 MAIN ST','SUITE 350','DOVER NH 03820','03820','999999999','0','999999999','A') 
	  
	Insert into Account (Tipnumber, Lastname, Lastsix, MemberNumber) 
	Values ('52J999999999994','BUSINESS','000060', '49865000024') 
	
	insert into Account (Tipnumber, Lastname, Lastsix, MemberNumber) 
	Values ('52J999999999994','BUSINESS','000052', '49865000024') 
	  
	INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther) 
	Values ('52J999999999994', '52Jtes4t', 'sample', 'N', 'ssmith@rewardsnow.com', 'N') 
	
	Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
	Values ('52J999999999993','52J','999999999993','TEST PERM','100 MAIN ST','SUITE 350','DOVER NH 03820','03820','0','0','0','A') 
	  
	Insert into Account (Tipnumber, Lastname, Lastsix, MemberNumber) 
	Values ('52J999999999993','PERM','000094', '49865002174') 
	  
	INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther) 
	Values ('52J999999999993', '52Jtest5', 'sample', 'N', 'ssmith@rewardsnow.com', 'N') 
		
commit tran