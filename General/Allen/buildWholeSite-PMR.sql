declare @tipfirst char(3) 
declare @loyaltyid int 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @LIB_template_Sid  int
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @custtravfee int
declare @website varchar(1024)
declare @travelratio numeric(18,0)

  
 --Update variables 
set @loyaltyid = ''
set @tipfirst = '718' 
set @finame = 'Primax Merchant Rewards' 
set @clientcode = 'PMR'
set @CustomerServicePhone = ''
set @programname = 'Primax Merchant Rewards'
set @travelincrement = 0
set @travelbottom = 0
set @cashbackmin = 0
set @merch = 0
set @cashback = 0
set @AirFee = 0
set @logo = @tipfirst + ''
--set @logo = ''
set @termspage = ''
set @faqpage = ''
set @earnpage = ''
set @Business = 0
set @statementdefault = 1
set @StmtNum = 1
set @database = 'NEBA'
set @liab = 1
set @custtravfee = 1
set @website = ''
set @travelratio = 0 --100 for most, 200 for Omni, 200 for LA DOTD... points divided by travelratio = dollars


SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst


IF @@ROWCOUNT = 0
BEGIN
	insert into client (ClientCode, ClientName, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault, StmtNum, CustomerServicePhone)
	select @clientcode, @finame, @programname, '02/28/2009', @travelincrement, @travelbottom, @cashbackmin, @merch, @AirFee,  @logo,  @termspage, @faqpage, @earnpage, @Business, @statementdefault, @StmtNum, @CustomerServicePhone

	insert into clientaward (TIPFirst, ClientCode) VALUES (@tipfirst,@clientcode)
END
ELSE
BEGIN 
	SET @clientcode = (SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst)
	UPDATE client 
		SET
		RNProgramName = @programname, 
		TravelMinimum = @travelincrement, 
		TravelBottom = @travelbottom, 
		CashBackMinimum = @cashbackmin,  
		Merch = @merch, 
		AirFee = @AirFee, 
		logo = @logo, 
		termspage = @termspage, 
		faqpage = @faqpage, 
		earnpage = @earnpage, 
		Business = @Business, 
		statementdefault = @statementdefault, 
		StmtNum = @StmtNum, 
		CustomerServicePhone = @CustomerServicePhone
		WHERE clientcode = @clientcode
END

SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec, travelratio)
	values (@tipfirst, @finame, @database, 'Certs', 1, 1, 1, 1, @cashback, @liab, 1, @custtravfee, 1, 0, @travelratio)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.searchdb 
	SET
		ClientName = @finame, 
		dbase = @database, 
		--certspage = 'Certs', 
		Website = 1, 
		newstyle = 1 , 
		history = 1 , 
		encrypted = 1, 
		CASH = @cashback, 
		LIAB = @liab, 
		QTR = 1, 
		Custtravelfee = @custtravfee, 
		ratio = 1, 
		disp_dec = 0, 
		travelratio = @travelratio
	WHERE tipfirst = @tipfirst
END

SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec, travelratio)
	values (@tipfirst, @finame, @database, 'Certs', 1, 1, 1, 1, @cashback, @liab, 1, @custtravfee, 1, 0, @travelratio)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.searchdb 
	SET
		ClientName = @finame, 
		dbase = @database, 
		--certspage = 'Certs', 
		Website = 1, 
		newstyle = 1 , 
		history = 1 , 
		encrypted = 1, 
		CASH = @cashback, 
		LIAB = @liab, 
		QTR = 1, 
		Custtravelfee = @custtravfee, 
		ratio = 1, 
		disp_dec = 0, 
		travelratio = @travelratio
	WHERE tipfirst = @tipfirst
END