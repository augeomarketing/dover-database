use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime

set @tipfirst = '615'
set @message = '<p style="font-weight: bold; color: red;">Due to unforeseen circumstances, AdvantageOne&#39;s Premium Perks points earned with purchases made on March 25, 26, 27, 30 and 31 and on April 2nd and 7th will be delayed.<br><br>Points earned from purchases made in the months of March through May have been posted and can be viewed on your Premium Perks Rewards website.<br><br>We apologize for the delay and will keep you posted on the status of the missing transaction point earnings. Thank you for participating in our Premium Perks program.</p>'
set @firstday = '5/3/2014' + ' 00:00:00'
set @lastday = '8/16/2014' + ' 00:00:00'

if @tipfirst <> 'RNI'
	update RewardsNOW.dbo.webannounce set dim_webannounce_active = 0 where dim_webannounce_tipprefix = @tipfirst

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	values (@tipfirst, @message, @firstday, @lastday)
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc