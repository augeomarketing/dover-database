Declare @rc int
Declare @returnval int
DECLARE @object int
DECLARE @Encrypted_Password varchar(250)
DECLARE @Decrypted_Password varchar(250)
Declare @Method_call varchar(4000)
Declare @Secret_code varchar(100)
declare @src varchar(255)
declare @desc varchar(255)
 
----------------------------------------------------------------------------------------------------

declare security_crsr cursor local
for select password
from [1security]
/* CHANGE TIPFIRST HERE */
where password is not null and len(rtrim(password))< 12
for update
/*                                                                            */
open security_crsr
/*                                                                            */
fetch security_crsr into @Decrypted_Password

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error

/*                                                                            */

while @@FETCH_STATUS = 0
begin	

	-- Password for Encryption
	set @secret_code = 'TeStInG'
	print @Decrypted_Password
	 
	--  Encrypt
	EXEC @rc = sp_OACreate 'CAPICOM.EncryptedData', @object OUT
	if @rc <> 0
	begin
	  exec sp_oageterrorinfo @object, @src out, @desc out 
	  select hr1=convert(varbinary(4),@rc), source=@src, description=@desc
	  return
	end
	 
	EXEC @rc = sp_OASetProperty @Object, 'Algorithm.Name', 3 -- 3DES
	if @rc <> 0
	begin
	  exec sp_oageterrorinfo @object, @src out, @desc out 
	  select hr2=convert(varbinary(4),@rc), source=@src, description=@desc
	  return
	end
	 
	set @method_call = 'SetSecret("' + @Secret_code + '")'
	EXEC @rc=sp_OAMethod @Object, @method_call
	if @rc <> 0
	begin
	  exec sp_oageterrorinfo @object, @src out, @desc out 
	  select hr3=convert(varbinary(4),@rc), source=@src, description=@desc
	  return
	end

	EXEC @rc=sp_OASetProperty @Object, 'Content', @Decrypted_Password
	if @rc <> 0
	begin
  		exec sp_oageterrorinfo @object, @src out, @desc out 
  		select hr4=convert(varbinary(4),@rc), source=@src, description=@desc
  		return
	end
 
	EXEC @rc=sp_OAMethod @Object, 'Encrypt(0)', @Encrypted_Password out
	if @rc <> 0
	begin
	  exec sp_oageterrorinfo @object, @src out, @desc out 
	  select hr5=convert(varbinary(4),@rc), source=@src, description=@desc
	  return
	end

	update [1security]	
	set Password = @Encrypted_Password
	where current of security_crsr

	-- Destroy object
	exec @rc = sp_OADestroy @object
	if @rc <> 0
	begin
	    exec sp_OAGetErrorInfo @object
	    return
	end
			

Next_Record:
	fetch security_crsr into @Decrypted_Password
end

Fetch_Error:
close security_crsr
deallocate security_crsr