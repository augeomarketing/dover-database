declare @tipfirst varchar(3) = '216'
-- REB, 216, 707, 717, 704, 138, 644
insert into RewardsNOW.dbo.payforpoints (dim_payforpoints_tipfirst, dim_payforpoints_minratio, dim_payforpoints_maxratio, dim_payforpoints_minpoints, dim_payforpoints_maxpointsforratio, dim_payforpoints_increment)
values (@tipfirst, 0.035, 0.020, 250, 5000, 50)