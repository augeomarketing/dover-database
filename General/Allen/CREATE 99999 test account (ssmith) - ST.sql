begin tran
 Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
 Values ('250999999999998','250','999999999998','JOHN SMITH','383 CENTRAL AVE','SUITE 350','DOVER NH 03820','03820','0','0','0','S') 
  
 Insert into Account (Tipnumber, Lastname, Lastsix, SSNLast4, MemberNumber) 
 Values ('250999999999998','SMITH','1188', '1188', '1188') 
  
 INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther, SecretA) 
 Values ('250999999999998', '1188test', 'sample', 'N', 'ssmith@rewardsnow.com', 'N', 'Testing') 
commit tran

update [1Security]
set Password = (
	select top 1 password
	from neba.dbo.[1security]
	where tipnumber like '%999999%')
where TipNumber like '%9999999%'
and (password is null or rtrim(password) = '' or password = 'sample')