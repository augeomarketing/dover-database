if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUSTOMER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CUSTOMER]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Client]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Client]
GO

CREATE TABLE [dbo].[CUSTOMER] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RunAvailable] [int] NULL ,
	[RUNBALANCE] [int] NULL ,
	[RunRedeemed] [int] NULL ,
	[LastStmtDate] [datetime] NULL ,
	[NextStmtDate] [datetime] NULL ,
	[STATUS] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[LASTNAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPFIRST] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPLAST] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME5] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME6] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HOMEPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WORKPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BusinessFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SegmentCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ComboStmt] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RewardsOnline] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOTES] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BonusFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc3] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc4] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc5] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RunBalanceNew] [int] NULL ,
	[RunAvaliableNew] [int] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Client] (
	[ClientCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipFirst] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zipcode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Phone1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Phone2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactPerson1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactPerson2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactEmail1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactEmail2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateJoined] [datetime] NULL ,
	[RNProgramName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TermsConditions] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsUpdatedDT] [datetime] NULL ,
	[MinRedeemNeeded] [int] NULL ,
	[TravelFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MerchandiseFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TravelIncMinPoints] [decimal](18, 0) NULL ,
	[MerchandiseBonusMinPoints] [decimal](18, 0) NULL ,
	[MaxPointsPerYear] [decimal](18, 0) NULL ,
	[PointExpirationYears] [int] NULL ,
	[ClientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Pass] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ServerName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DbName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UserName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Password] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsExpire] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastTipNumberUsed] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

