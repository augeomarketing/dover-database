/****** Object:  Trigger [trig_CopyTravelOnlHistory]    Script Date: 02/06/2009 11:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- 	select * from ticket_transaction 

/*  **************************************  */
/* Date: 2008/ 03  */
/* Author: Rich T  */
/*  **************************************  */
/* This copies records from the ticket_transaction table to the OnlineHistory table  */

create TRIGGER [dbo].[trig_CopyTravelOnlHistory] ON [dbo].[Ticket_Transaction] 
FOR INSERT
AS

---------- Copy Records to OnlHistory table --------
insert into OnlHistory 
(TipNumber,HistDate,Email,Points,TranDesc,PostFlag,TransID,Trancode,CatalogCode, CatalogDesc,CatalogQtY, Source)
select tipnumber, Trandate, email, points, 'CONF: '+CONF_PIN, 	Postflag, id, 'RV', 'OnlineTravel', 'ONLINE TRAVEL '+Last6, 1, 'WEB' from  
		  	Ticket_Transaction where POSTFLAG   = 0 

---------- Update PostFlag in TicketTransaction--------
Update ticket_transaction SET postflag = 1 where postflag = 0 

-------- Reformat account number to 6 --------
Update ticket_transaction SET CARDNUMBER = RIGHT(RTRIM(CARDNUMBER),6) WHERE LEN(RTRIM(CARDNUMBER)) > 6
Update ticket_transaction SET CVV = 0
GO
