if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[role]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[role]
GO

CREATE TABLE [dbo].[role] (
	[sid_role_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_role_name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_role_description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_role_created] [datetime] NOT NULL ,
	[dim_role_lastmodified] [datetime] NOT NULL ,
	[dim_role_active] [int] NOT NULL 
) ON [PRIMARY]
GO

