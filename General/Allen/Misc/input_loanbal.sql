if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[input_loanbal]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[input_loanbal]
GO

CREATE TABLE [dbo].[input_loanbal] (
	[input_loanbal_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[acctnbr] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[loanid] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[avgbal] [decimal](18, 0) NULL ,
	[loantype] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

