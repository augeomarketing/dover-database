if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Customer]
GO

CREATE TABLE [dbo].[Input_Customer] (
	[Input_Customer_Id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[Acct Nbr] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Br Nbr] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Card Nbr] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Acct Stat Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Mbr Nm] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Card Stat Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Acct Nbr1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Acct Status Flg] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Nm Line 1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Nm Line 2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Nm Line 3] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Addr Line 1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Addr Line 2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[St Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zip Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Close Dt] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last Maint 1 Dt] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last Maint 1 Typ Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Close Reason Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

