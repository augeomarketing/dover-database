DECLARE @clientId CHAR(3)
SET @clientId = '221'

DECLARE @dbName VARCHAR(50)
SET @dbName = (SELECT DBNamePatton from RewardsNOW.dbo.DBProcessInfo WHERE DBNumber = @clientId)

DECLARE @SQL NVARCHAR(2048)
SET @SQL =		  'truncate table [' + @dbName + '].dbo.affiliat;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.affiliat_stage;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.affiliatdeleted;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.beginning_balance_month;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.beginning_balance_table;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.current_month_activity;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.customer;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.customer_stage;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.customerdeleted;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.history;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.history_stage;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.historydeleted;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.historytip;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.input_customer;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.input_customer_error;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.input_purge;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.input_purge_error;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.monthly_audit_errorfile;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.monthly_statement_file;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.onetimebonuses;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.onetimebonuses_stage;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.purge_pending;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.quarterly_audit_errorfile;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.quarterly_statement_file;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.rn1_estatementtip;'
SET @SQL = @SQL + 'truncate table [' + @dbName + '].dbo.welcomekit;' 

exec sp_executesql @SQL

SET @SQL = 'UPDATE [' + @dbName + '].dbo.client SET LastTipNumberUsed = NULL'
exec sp_executesql @SQL

delete from rewardsnow.dbo.rptliability where clientid = @clientId
