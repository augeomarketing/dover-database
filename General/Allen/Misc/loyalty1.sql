if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[loyalty]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[loyalty]
GO

CREATE TABLE [dbo].[loyalty] (
	[sid_loyalty_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_loyalty_name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_loyalty_description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_loyalty_created] [datetime] NOT NULL ,
	[dim_loyalty_lastmodified] [datetime] NOT NULL ,
	[dim_loyalty_active] [int] NOT NULL 
) ON [PRIMARY]
GO

