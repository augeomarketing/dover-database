if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Transaction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Transaction]
GO

CREATE TABLE [dbo].[Input_Transaction] (
	[Card Nbr] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Primary Acct Nbr] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SUM Sum Trn Cnt] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SUM Sum Tran Amt] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Process Typ Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Network Id] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

