if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[input_cardtrans]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[input_cardtrans]
GO

CREATE TABLE [dbo].[input_cardtrans] (
	[input_cardtrans_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[acctnbr] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[firstname] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[address] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[city] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[state] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[zip] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[phone] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[relationshipcode] [int] NULL ,
	[accttype] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[trantype] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[tranamt] [decimal](18, 0) NULL ,
	[trancnt] [int] NULL ,
	[cardnbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[priorcardnbr] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

