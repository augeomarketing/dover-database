--ADDED Primary Key-Autonumber (HISTORYID) to HISTORY table
--ADDED Index to HISTORY table 
--UPDATED all the ACCTID's to a new value (per PCI)
--UPDATE HISTORY SET ACCTID = 'AGB' + CONVERT(VARCHAR(100), CHECKSUM(ACCTID))
--ADDED Statistics to HISTORY table

--use Compass
SELECT TipNumber,  SUM(points*ratio) AS RunAvailable 
FROM HISTORY 
WHERE TipNumber = '360000000611189'
GROUP BY TipNumber
OPTION(MAXDOP 1)
--2:35
