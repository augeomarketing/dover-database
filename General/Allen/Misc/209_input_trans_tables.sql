if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Transaction_Loan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Transaction_Loan]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Transaction_Share]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Transaction_Share]
GO

CREATE TABLE [dbo].[Input_Transaction_Loan] (
	[Input_Transaction_Load_Id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[AccountNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LoanId] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AverageBalance] [decimal](18, 0) NULL ,
	[LoanType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Points] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Transaction_Share] (
	[Input_Transaction_Id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[AccountNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RelationshipCode] [int] NULL ,
	[AccountType] [int] NULL ,
	[AvgDailyBalance] [decimal](18, 0) NULL ,
	[ShareType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Trancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Points] [int] NULL 
) ON [PRIMARY]
GO

