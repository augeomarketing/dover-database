CREATE TABLE [dbo].[Catalog] (
	[CatalogId] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[CatalogCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogTrancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogDollars] [int] NOT NULL ,
	[CatalogImageLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogCreated] [datetime] NOT NULL ,
	[CatalogLastModified] [datetime] NOT NULL ,
	[CatalogActive] [int] NOT NULL ,
	[CatalogParentID] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Catalog] WITH NOCHECK ADD 
	CONSTRAINT [PK_Catalog] PRIMARY KEY  CLUSTERED 
	(
		[CatalogId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Catalog] WITH NOCHECK ADD 
	CONSTRAINT [DF_Catalog_CatalogDollars] DEFAULT ((-1)) FOR [CatalogDollars],
	CONSTRAINT [DF_Catalog_CatalogImageLocation] DEFAULT ('../image') FOR [CatalogImageLocation],
	CONSTRAINT [DF_Catalog_CatalogCreated] DEFAULT (getdate()) FOR [CatalogCreated],
	CONSTRAINT [DF_Catalog_CatalogLastModified] DEFAULT (getdate()) FOR [CatalogLastModified],
	CONSTRAINT [DF_Catalog_CatalogActive] DEFAULT (1) FOR [CatalogActive],
	CONSTRAINT [DF_Catalog_CatalogParentID] DEFAULT ((-1)) FOR [CatalogParentID]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER [dbo].[TRIG_Catalog_UPDATE] ON [dbo].[Catalog] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @CatalogID INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.CatalogID 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @CatalogID 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE Catalog SET CatalogLastModified = getdate() WHERE CatalogID = @CatalogID 
       
              FETCH NEXT FROM UPD_QUERY INTO @CatalogID 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END 




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

