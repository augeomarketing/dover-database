Set datefirst 4
Select 	rtrim(subclient.ClientName) as Client,
	rtrim(Main.Tipnumber) as [Account #],
	rtrim(Main.Name1) as Name,
	CASE
    		WHEN Right(Rtrim(Main.Itemnumber), 2) = '-B' THEN 'Bonus'
    		ELSE rtrim(Main.Trandesc)
  	    END as Type,
	rtrim(Subitem.catalogdesc) as Product,
	rtrim(Main.Catalogqty) as Qty,
	rtrim(Main.Points*Main.catalogqty) as Points,
	left(rtrim(Main.histdate), 11) as Date
From	main left outer join subitem on main.itemnumber = subitem.itemnumber left outer join subclient on main.tipfirst = subclient.tipfirst
Where	Main.TipFirst = SubClient.TipFirst
    and histdate >= left(Getdate() + 1 - datepart(dw, getdate())- 7, 11)
    and histdate < Left(Getdate() + 1 - datepart(dw, getdate()),11)
    and (main.trandesc not in ('Travel', 'Cash', 'Cash Back', 'Cash Rebate', 'Brochure request')
     or (Main.trandesc ='travel' and subclient.clientname = 'Compass')
     or (Main.trandesc in ('Cash', 'Cash Back', 'Cash Rebate') and subclient.clientname = 'Compass')
     or trandesc is null)
Order by Client, type, product, histdate