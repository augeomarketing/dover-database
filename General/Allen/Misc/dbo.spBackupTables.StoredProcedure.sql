/****** Object:  StoredProcedure [dbo].[spBackupTables]    Script Date: 02/06/2009 11:33:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spBackupTables] AS 

TRUNCATE TABLE Customer_Backup
TRUNCATE TABLE Account_backup
TRUNCATE TABLE [1Security_backup]

INSERT INTO Customer_backup SELECT * FROM Customer
INSERT INTO Account_backup SELECT * FROM Account
INSERT INTO [1security_backup] SELECT * FROM [1security]
GO
