CREATE PROCEDURE usp_auditCatalogDescription
    @auditId INT, @catalogId INT, @LanguageID INT, @Description VARCHAR(1024), @Name VARCHAR(1024)
AS 
BEGIN 
	SELECT auditId FROM AuditCatalogDescription WHERE auditId = @auditId AND catalogId = @catalogId
	IF(@@ROWCOUNT > 0)
		UPDATE AuditCatalogDescription SET 
						   CatalogId = @catalogId,
						   AuditCatalogDescription = @Description,
						   AuditCatalogName = @Name
		WHERE AuditId = @auditId
	ELSE
		INSERT INTO AuditCatalogDescription (AuditId, CatalogId, AuditCatalogDescription, AuditCatalogName, LanguageId)
		VALUES (@auditId, @catalogId, @Description, @Name, @Language)
	END IF
END
