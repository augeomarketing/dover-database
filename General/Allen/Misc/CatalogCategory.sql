CREATE TABLE [dbo].[CatalogCategory] (
	[CatalogCategoryID] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[CatalogID] [int] NOT NULL ,
	[CategoryID] [int] NOT NULL ,
	[CatalogCategoryCreated] [datetime] NOT NULL ,
	[CatalogCategoryLastModified] [datetime] NOT NULL ,
	[CatalogCategoryActive] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CatalogCategory] WITH NOCHECK ADD 
	CONSTRAINT [PK_CatalogCategory] PRIMARY KEY  CLUSTERED 
	(
		[CatalogCategoryID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[CatalogCategory] WITH NOCHECK ADD 
	CONSTRAINT [DF_CatalogCategory_CatalogCategoryCreated] DEFAULT (getdate()) FOR [CatalogCategoryCreated],
	CONSTRAINT [DF_CatalogCategory_CatalogCategoryLastModified] DEFAULT (getdate()) FOR [CatalogCategoryLastModified],
	CONSTRAINT [DF_CatalogCategory_CatalogCategoryActive] DEFAULT (1) FOR [CatalogCategoryActive]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER [dbo].[TRIG_CatalogCategory_UPDATE] ON dbo.CatalogCategory 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @CatalogCategoryID INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.CatalogCategoryID 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @CatalogCategoryID 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE CatalogCategory SET CatalogCategoryLastModified = getdate() WHERE CatalogCategoryID = @CatalogCategoryID 
       
              FETCH NEXT FROM UPD_QUERY INTO @CatalogCategoryID 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

