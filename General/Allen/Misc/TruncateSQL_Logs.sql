DECLARE @LogFileName NVARCHAR(100)
,@LogFileSize INT
,@ShrinkToMb INT
,@DBName NVARCHAR(200)

SET @ShrinkToMb = 2

SELECT @LogFileName = LTRIM(RTRIM([name]))
,@LogFileSize = [size]
,@DBName = DB_Name()
FROM sysfiles
WHERE groupid = 0

IF((@ShrinkToMb * 128) < @LogFileSize)
BEGIN
DBCC SHRINKFILE(@LogFileName, @ShrinkToMb)
BACKUP LOG @DBName WITH TRUNCATE_ONLY
DBCC SHRINKFILE(@LogFileName, @ShrinkToMb)
SELECT @LogFileSize = [size]
FROM sysfiles
WHERE groupid = 0
PRINT @DBName + ' log file (' + @LogFileName + ') shrunk to '
+ cast(@LogFileSize AS NVARCHAR)
+ ' pages, you requested a size of '
+ cast(@ShrinkToMb * 128 AS NVARCHAR)
+ ' pages'
END
ELSE
BEGIN
PRINT @DBName + ' log file (' + @LogFileName + ') is smaller than '
+ cast(@ShrinkToMb * 128 AS NVARCHAR)
+ ' pages already - No action taken'
END
