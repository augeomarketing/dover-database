if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[userinfo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[userinfo]
GO

CREATE TABLE [dbo].[userinfo] (
	[sid_userinfo_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_userinfo_fname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_userinfo_lname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_userinfo_email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_userinfo_username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_userinfo_password] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_userinfo_lastlogin] [datetime] NOT NULL ,
	[dim_userinfo_penultimatelogin] [datetime] NOT NULL ,
	[dim_userinfo_created] [datetime] NOT NULL ,
	[dim_userinfo_lastmodified] [datetime] NOT NULL ,
	[dim_userinfo_active] [int] NOT NULL 
) ON [PRIMARY]
GO

