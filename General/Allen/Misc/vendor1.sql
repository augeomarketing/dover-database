if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vendor]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[vendor]
GO

CREATE TABLE [dbo].[vendor] (
	[sid_vendor_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_vendor_name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_vendor_code] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_vendor_contact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_vendor_created] [datetime] NOT NULL ,
	[dim_vendor_lastmodified] [datetime] NOT NULL ,
	[dim_vendor_active] [int] NOT NULL 
) ON [PRIMARY]
GO

