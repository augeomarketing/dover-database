if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[input_customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[input_customer]
GO

CREATE TABLE [dbo].[input_customer] (
	[inputId] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[memberId] [int] NOT NULL ,
	[lastname] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[firstname] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[address1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[address2] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[city] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[state] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[phone] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[email] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[daycareId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[tipnumber] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

