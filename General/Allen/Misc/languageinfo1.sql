if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[languageinfo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[languageinfo]
GO

CREATE TABLE [dbo].[languageinfo] (
	[sid_languageinfo_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_languageinfo_name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_languageinfo_created] [datetime] NOT NULL ,
	[dim_languageinfo_lastmodified] [datetime] NOT NULL ,
	[dim_languageinfo_active] [int] NOT NULL 
) ON [PRIMARY]
GO

