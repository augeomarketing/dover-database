/****** Object:  StoredProcedure [dbo].[spPostToWeb]    Script Date: 02/06/2009 11:33:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Patton to Web   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spPostToWeb]  AS 
-- Null customer address3 if = CityStateZip
UPDATE customer
SET address3 = null WHERE address3 = citystatezip

-- remove orphan Tips from 1Security
DELETE FROM [1security] WHERE tipnumber NOT IN (SELECT tipnumber FROM customer )

-- Add New Tips to 1security 
INSERT INTO [1Security] 
(Tipnumber, EmailStatement) 
SELECT tipnumber, 'N'  FROM customer 
WHERE tipnumber NOT IN (SELECT tipnumber FROM [1security])

-- This recalculates the customer AvailableBal against the OnlHistory.
UPDATE Customer 
SET AvailableBal = AvailableBal - 
(SELECT SUM (OnlHistory.Points * OnlHistory.CatalogQty) 
	FROM  OnlHistory WHERE OnlHistory.tipnumber = Customer.Tipnumber AND OnlHistory.Copyflag IS NULL GROUP BY Tipnumber  )
WHERE tipnumber IN (SELECT tipnumber FROM onlhistory WHERE CopyFlag IS NULL)
GO
