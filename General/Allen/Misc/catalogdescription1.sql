if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[catalogdescription]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[catalogdescription]
GO

CREATE TABLE [dbo].[catalogdescription] (
	[sid_catalogdescription_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[sid_catalog_id] [int] NOT NULL ,
	[dim_catalogdescription_name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_catalogdescription_description] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[sid_languageinfo_id] [int] NOT NULL ,
	[dim_catalogdescription_created] [datetime] NOT NULL ,
	[dim_catalogdescription_lastmodified] [datetime] NOT NULL ,
	[dim_catalogdescription_active] [int] NOT NULL 
) ON [PRIMARY]
GO

