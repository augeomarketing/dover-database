if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Customer]
GO

CREATE TABLE [dbo].[Input_Customer] (
	[Input_Customer_Id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[AccountNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FirstName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[zip] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePhone] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RelationshipCode] [int] NULL ,
	[AccountType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TransactionCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TransactionAmt] [decimal](18, 0) NULL ,
	[TransactionCnt] [int] NULL ,
	[CardNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PriorAcctNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[JointFirstName1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[JointLastName1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[JointFirstName2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[JointLostName2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

