/****** Object:  Table [dbo].[1Security_Backup]    Script Date: 02/06/2009 11:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[1Security_Backup](
	[TipNumber] [char](20) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](10) NULL,
	[RegDate] [datetime] NULL,
	[username] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
