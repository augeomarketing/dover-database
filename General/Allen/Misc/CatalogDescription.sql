CREATE TABLE [dbo].[CatalogDescription] (
	[CatalogDescriptionID] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[CatalogID] [int] NOT NULL ,
	[CatalogName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogDescription] [varchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LanguageID] [int] NOT NULL ,
	[CatalogDescriptionCreated] [datetime] NOT NULL ,
	[CatalogDescriptionLastModified] [datetime] NOT NULL ,
	[CatalogDescriptionActive] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CatalogDescription] WITH NOCHECK ADD 
	CONSTRAINT [PK_CatalogDescription] PRIMARY KEY  CLUSTERED 
	(
		[CatalogDescriptionID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[CatalogDescription] WITH NOCHECK ADD 
	CONSTRAINT [DF_CatalogDescription_CatalogDescriptionCreated] DEFAULT (getdate()) FOR [CatalogDescriptionCreated],
	CONSTRAINT [DF_CatalogDescription_CatalogDescriptionLastModified] DEFAULT (getdate()) FOR [CatalogDescriptionLastModified],
	CONSTRAINT [DF_CatalogDescription_CatalogDescriptionActive] DEFAULT (1) FOR [CatalogDescriptionActive]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER [dbo].[TRIG_CatalogDescription_UPDATE] ON [dbo].[CatalogDescription] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @CatalogDescriptionID INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.CatalogDescriptionID 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @CatalogDescriptionID 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE CatalogDescription SET CatalogDescriptionLastModified = getdate() WHERE CatalogDescriptionID = @CatalogDescriptionID 
       
              FETCH NEXT FROM UPD_QUERY INTO @CatalogDescriptionID 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END 




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

