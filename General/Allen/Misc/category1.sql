if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[category]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[category]
GO

CREATE TABLE [dbo].[category] (
	[sid_category_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_category_name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_category_description] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_category_created] [datetime] NOT NULL ,
	[dim_category_lastmodified] [datetime] NOT NULL ,
	[dim_category_active] [int] NOT NULL 
) ON [PRIMARY]
GO

