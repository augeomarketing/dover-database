if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[status]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[status]
GO

CREATE TABLE [dbo].[status] (
	[sid_status_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_status_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_status_description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[dim_status_image] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_status_active] [int] NOT NULL ,
	[dim_status_created] [datetime] NOT NULL ,
	[dim_status_lastmodified] [datetime] NOT NULL 
) ON [PRIMARY]
GO

