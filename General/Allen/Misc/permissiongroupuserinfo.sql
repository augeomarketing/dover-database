if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[permissiongroupuserinfo]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[permissiongroupuserinfo]
GO

CREATE TABLE [dbo].[permissiongroupuserinfo] (
	[sid_permissiongroupuserinfo_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[sid_permissiongroup_id] [int] NOT NULL ,
	[sid_userinfo_id] [int] NOT NULL ,
	[dim_permissiongroupuserinfo_created] [datetime] NOT NULL ,
	[dim_permissiongroup_lastmodified] [datetime] NOT NULL ,
	[dim_permissiongroupuserinfo_active] [int] NOT NULL 
) ON [PRIMARY]
GO

