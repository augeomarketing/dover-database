if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spBackupTables]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spBackupTables]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[spPostToWeb]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[spPostToWeb]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[1Security]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[1Security]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[1Security_Backup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[1Security_Backup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Account]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Account]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Account_Backup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Account_Backup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Airlines]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Airlines]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Airport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Airport]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Award]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Award]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Certs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Certs]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Client]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Client]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClientAward]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClientAward]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Comb_TipTracking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Comb_TipTracking]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Country]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Country]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Currency]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Currency]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Customer]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Customer_Backup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Customer_Backup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EquipmentCode]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[EquipmentCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Itinerary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Itinerary]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ONLCerts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ONLCerts]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OnlHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OnlHistory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Statement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Statement]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Ticket_Transaction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Ticket_Transaction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TravelCert]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TravelCert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Writeback]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Writeback]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[security]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[security]
GO

CREATE TABLE [dbo].[1Security] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Password] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmailStatement] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EMailOther] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last6] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegDate] [datetime] NULL ,
	[username] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[1Security_Backup] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Password] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmailStatement] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EMailOther] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last6] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegDate] [datetime] NULL ,
	[username] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Account] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LastSix] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SSNLast4] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RecNum] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Account_Backup] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LastSix] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SSNLast4] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RecNum] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Airlines] (
	[airline_code] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[airline_name] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[logo] [image] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Airport] (
	[LocationCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StateProvince] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Country] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Award] (
	[AwardCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AwardName] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AwardDesc] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientAwardPoints] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Certs] (
	[Company] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Dollars] [decimal](18, 0) NOT NULL ,
	[Points] [decimal](18, 0) NOT NULL ,
	[CertNo] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Client] (
	[ClientCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ClientName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Timestamp] [binary] (8) NULL ,
	[ClientTypeCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RNProgramName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Ad1] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ad2] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TermsConditions] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsUpdated] [smalldatetime] NULL ,
	[TravelMinimum] [int] NULL ,
	[TravelBottom] [int] NULL ,
	[Merch] [bit] NOT NULL ,
	[RecNum] [int] NOT NULL ,
	[AirFee] [decimal](18, 0) NULL ,
	[Logo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Termspage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[ClientAward] (
	[TIPFirst] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ClientCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Comb_TipTracking] (
	[NewTIP] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldTip] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TranDate] [datetime] NOT NULL ,
	[OldTipPoints] [int] NULL ,
	[OldTipRedeemed] [int] NULL ,
	[OldTipRank] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Country] (
	[CountryCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CountryName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CountryCode3] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Currency] (
	[CurrencyCode] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CurrencyDiscription] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TipFirst] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TipLast] [char] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CityStateZip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EarnedBalance] [int] NULL ,
	[Redeemed] [int] NULL ,
	[AvailableBal] [int] NULL ,
	[Status] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Segment] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[city] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[state] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Customer_Backup] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TipFirst] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TipLast] [char] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name5] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CityStateZip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EarnedBalance] [int] NULL ,
	[Redeemed] [int] NULL ,
	[AvailableBal] [int] NULL ,
	[Status] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Segment] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[city] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[state] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[EquipmentCode] (
	[code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Itinerary] (
	[ItineraryNumber] [char] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BookedAmount] [money] NULL ,
	[ActualAmount] [money] NULL ,
	[DateAdded] [datetime] NULL ,
	[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusDate] [datetime] NULL ,
	[Points] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ONLCerts] (
	[Company] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Amount] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Points] [decimal](18, 0) NOT NULL ,
	[Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[OnlHistory] (
	[TipNumber] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[HistDate] [smalldatetime] NOT NULL ,
	[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Points] [int] NOT NULL ,
	[TranDesc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PostFlag] [tinyint] NULL ,
	[TransID]  uniqueidentifier ROWGUIDCOL  NOT NULL ,
	[Trancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CatalogCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CatalogDesc] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CatalogQty] [int] NULL ,
	[copyflag] [datetime] NULL ,
	[saddress1] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[saddress2] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[scity] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[sstate] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[szipcode] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[scountry] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[hphone] [char] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[wphone] [char] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[source] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[notes] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[sname] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ordernum] [bigint] NULL ,
	[linenum] [bigint] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Statement] (
	[TRAVNUM] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME1] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PNTBEG] [float] NULL ,
	[PNTEND] [float] NULL ,
	[PNTPRCHS] [float] NULL ,
	[PNTBONUS] [float] NULL ,
	[PNTADD] [float] NULL ,
	[PNTINCRS] [float] NULL ,
	[PNTREDEM] [float] NULL ,
	[PNTRETRN] [float] NULL ,
	[PNTSUBTR] [float] NULL ,
	[PNTDECRS] [float] NULL ,
	[PNTDEBIT] [float] NULL ,
	[PNTMORT] [float] NULL ,
	[PNTHOME] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Ticket_Transaction] (
	[ID] [uniqueidentifier] NOT NULL ,
	[tipnumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[last6] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[points] [decimal](18, 0) NULL ,
	[TranDate] [datetime] NOT NULL ,
	[postflag] [tinyint] NOT NULL ,
	[email] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[conf_pin] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[itinerary_num] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[rnow_fee] [float] NULL ,
	[Cardnumber] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Exp_Date] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CVV] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TravelCert] (
	[TIPNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Amount] [decimal](18, 0) NOT NULL ,
	[Lastsix] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Issued] [smalldatetime] NOT NULL ,
	[Expire] [smalldatetime] NOT NULL ,
	[Received] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Writeback] (
	[TipNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Hardbounce] [decimal](18, 0) NULL ,
	[Softbounce] [decimal](18, 0) NULL ,
	[Email] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Sent] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[security] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmailStatement] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EMailOther] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last6] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegDate] [datetime] NULL 
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE spBackupTables AS 

TRUNCATE TABLE Customer_Backup
TRUNCATE TABLE Account_backup
TRUNCATE TABLE [1Security_backup]

INSERT INTO Customer_backup SELECT * FROM Customer
INSERT INTO Account_backup SELECT * FROM Account
INSERT INTO [1security_backup] SELECT * FROM [1security]

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Patton to Web   */
/*  **************************************  */

CREATE PROCEDURE spPostToWeb  AS 
-- Null customer address3 if = CityStateZip
UPDATE customer
SET address3 = null WHERE address3 = citystatezip

-- remove orphan Tips from 1Security
DELETE FROM [1security] WHERE tipnumber NOT IN (SELECT tipnumber FROM customer )

-- Add New Tips to 1security 
INSERT INTO [1Security] 
(Tipnumber, EmailStatement) 
SELECT tipnumber, 'N'  FROM customer 
WHERE tipnumber NOT IN (SELECT tipnumber FROM [1security])

-- This recalculates the customer AvailableBal against the OnlHistory.
UPDATE Customer 
SET AvailableBal = AvailableBal - 
(SELECT SUM (OnlHistory.Points * OnlHistory.CatalogQty) 
	FROM  OnlHistory WHERE OnlHistory.tipnumber = Customer.Tipnumber AND OnlHistory.Copyflag IS NULL GROUP BY Tipnumber  )
WHERE tipnumber IN (SELECT tipnumber FROM onlhistory WHERE CopyFlag IS NULL)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

