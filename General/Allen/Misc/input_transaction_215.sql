if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[input_transaction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[input_transaction]
GO

CREATE TABLE [dbo].[input_transaction] (
	[transactionId] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[memberid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[merchantid] [int] NULL ,
	[merchantName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[orderId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[transactionDate] [datetime] NULL ,
	[sku] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[sales] [decimal](18, 2) NULL ,
	[commision] [decimal](18, 2) NULL ,
	[processDate] [datetime] NULL ,
	[processTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

