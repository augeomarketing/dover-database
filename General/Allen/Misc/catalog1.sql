CREATE DATABASE [Catalog]  ON (NAME = N'Catalog2_Data', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL$RN\data\Catalog2_Data.MDF' , SIZE = 4, FILEGROWTH = 10%) LOG ON (NAME = N'Catalog2_Log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL$RN\data\Catalog2_Log.LDF' , SIZE = 3, FILEGROWTH = 10%)
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO

exec sp_dboption N'Catalog', N'autoclose', N'false'
GO

exec sp_dboption N'Catalog', N'bulkcopy', N'false'
GO

exec sp_dboption N'Catalog', N'trunc. log', N'false'
GO

exec sp_dboption N'Catalog', N'torn page detection', N'true'
GO

exec sp_dboption N'Catalog', N'read only', N'false'
GO

exec sp_dboption N'Catalog', N'dbo use', N'false'
GO

exec sp_dboption N'Catalog', N'single', N'false'
GO

exec sp_dboption N'Catalog', N'autoshrink', N'false'
GO

exec sp_dboption N'Catalog', N'ANSI null default', N'false'
GO

exec sp_dboption N'Catalog', N'recursive triggers', N'false'
GO

exec sp_dboption N'Catalog', N'ANSI nulls', N'false'
GO

exec sp_dboption N'Catalog', N'concat null yields null', N'false'
GO

exec sp_dboption N'Catalog', N'cursor close on commit', N'false'
GO

exec sp_dboption N'Catalog', N'default to local cursor', N'false'
GO

exec sp_dboption N'Catalog', N'quoted identifier', N'false'
GO

exec sp_dboption N'Catalog', N'ANSI warnings', N'false'
GO

exec sp_dboption N'Catalog', N'auto create statistics', N'true'
GO

exec sp_dboption N'Catalog', N'auto update statistics', N'true'
GO

use [Catalog]
GO

CREATE TABLE [dbo].[catalog] (
	[sid_catalog_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[dim_catalog_code] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_catalog_trancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_catalog_dollars] [int] NOT NULL ,
	[dim_catalog_imagelocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dim_catalog_created] [datetime] NOT NULL ,
	[dim_catalog_lastmodified] [datetime] NOT NULL ,
	[dim_catalog_active] [int] NOT NULL ,
	[dim_catalog_parentid] [int] NOT NULL ,
	[sid_status_id] [int] NOT NULL 
) ON [PRIMARY]
GO

