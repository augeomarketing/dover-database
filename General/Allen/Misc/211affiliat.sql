if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AFFILIAT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AFFILIAT]
GO

CREATE TABLE [dbo].[AFFILIAT] (
	[ACCTID] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AcctType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[SECID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctTypeDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[YTDEarned] [real] NULL ,
	[CustID] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

