if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[loyaltytip]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[loyaltytip]
GO

CREATE TABLE [dbo].[loyaltytip] (
	[sid_loyaltytip_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[sid_loyalty_id] [int] NOT NULL ,
	[dim_loyaltytip_prefix] [int] NOT NULL ,
	[dim_loyaltytip_created] [datetime] NOT NULL ,
	[dim_loyaltytip_lastmodified] [datetime] NOT NULL ,
	[dim_loyaltytip_active] [int] NOT NULL 
) ON [PRIMARY]
GO

