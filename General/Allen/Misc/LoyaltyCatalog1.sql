if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[loyaltycatalog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[loyaltycatalog]
GO

CREATE TABLE [dbo].[loyaltycatalog] (
	[sid_loyaltycatalog_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[sid_loyalty_id] [int] NOT NULL ,
	[sid_catalog_id] [int] NOT NULL ,
	[dim_loyaltycatalog_pointvalue] [int] NOT NULL ,
	[dim_loyaltycatalog_bonus] [int] NOT NULL ,
	[dim_loyaltycatalog_created] [datetime] NOT NULL ,
	[dim_loyaltycatalog_lastmodified] [datetime] NOT NULL ,
	[dim_loyaltycatalog_active] [int] NOT NULL 
) ON [PRIMARY]
GO

