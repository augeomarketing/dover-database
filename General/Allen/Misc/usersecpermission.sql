if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TRIG_UserSecPermission_UPDATE]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TRIG_UserSecPermission_UPDATE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UserSecPermission]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[UserSecPermission]
GO

CREATE TABLE [dbo].[UserSecPermission] (
	[UserSecPermissionID] [int] IDENTITY (1, 1) NOT NULL ,
	[UserSecID] [int] NOT NULL ,
	[PermissionID] [int] NOT NULL ,
	[UserSecPermissionCreated] [datetime] NOT NULL ,
	[UserSecPermissionLastModified] [datetime] NOT NULL ,
	[UserSecPermissionActive] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserSecPermission] WITH NOCHECK ADD 
	CONSTRAINT [PK_UserSecPermission] PRIMARY KEY  CLUSTERED 
	(
		[UserSecPermissionID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[UserSecPermission] WITH NOCHECK ADD 
	CONSTRAINT [DF_UserSecPermission_UserSecPermissionCreated] DEFAULT (getdate()) FOR [UserSecPermissionCreated],
	CONSTRAINT [DF_UserSecPermission_UserSecPermssionLastModified] DEFAULT (getdate()) FOR [UserSecPermissionLastModified],
	CONSTRAINT [DF_UserSecPermission_UserSecPermssionActive] DEFAULT (1) FOR [UserSecPermissionActive]
GO

 CREATE  INDEX [UserSecID_PermissionID] ON [dbo].[UserSecPermission]([UserSecID], [PermissionID]) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER [dbo].[TRIG_UserSecPermission_UPDATE] ON [dbo].[UserSecPermission] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @UserSecPermissionID INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.UserSecPermissionID 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @UserSecPermissionID 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE UserSecPermission SET UserSecPermissionLastModified = getdate() WHERE UserSecPermissionID = @UserSecPermissionID 
       
              FETCH NEXT FROM UPD_QUERY INTO @UserSecPermissionID 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

