if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AFFILIAT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AFFILIAT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AFFILIAT_Stage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AFFILIAT_Stage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AccountDeleteInput]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AccountDeleteInput]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AcctType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AcctType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AffiliatDeleted]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AffiliatDeleted]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Beginning_Balance_Table]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Beginning_Balance_Table]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUSTOMER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CUSTOMER]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUSTOMER_Stage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CUSTOMER_Stage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUSTOMERdeleted]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CUSTOMERdeleted]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Client]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Client]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Current_Month_Activity]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Current_Month_Activity]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DateforAudit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DateforAudit]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HISTORY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[HISTORY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HISTORYTIP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[HISTORYTIP]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HISTORY_Stage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[HISTORY_Stage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[HistoryDeleted]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[HistoryDeleted]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Customer]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Customer_error]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Customer_error]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Purge]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Purge]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Purge_Pending]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Purge_Pending]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Transaction]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Transaction]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Input_Transaction_Error]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Input_Transaction_Error]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Monthly_Audit_ErrorFile]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Monthly_Audit_ErrorFile]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Monthly_Statement_File]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Monthly_Statement_File]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OneTimeBonuses]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OneTimeBonuses]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OneTimeBonuses_Stage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OneTimeBonuses_Stage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Quarterly_Audit_ErrorFile]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Quarterly_Audit_ErrorFile]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Quarterly_Statement_File]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Quarterly_Statement_File]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Results]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Results]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Status]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Status]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TranCode_Factor]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TranCode_Factor]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TranType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TranType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TransStandard]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TransStandard]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WelcomeKit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[WelcomeKit]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[beginning_balance_month]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[beginning_balance_month]
GO

CREATE TABLE [dbo].[AFFILIAT] (
	[ACCTID] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AcctType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[SECID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctTypeDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[YTDEarned] [real] NULL ,
	[CustID] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AFFILIAT_Stage] (
	[ACCTID] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AcctType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[SECID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctTypeDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[YTDEarned] [real] NULL ,
	[CustID] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AccountDeleteInput] (
	[acctid] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[dda] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AcctType] (
	[AcctType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AcctTypeDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Acctmultiplier] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AffiliatDeleted] (
	[TipNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateAdded] [datetime] NULL ,
	[SecID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctTypeDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[YTDEarned] [real] NULL ,
	[CustID] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateDeleted] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Beginning_Balance_Table] (
	[Tipnumber] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MonthBeg1] [int] NULL ,
	[MonthBeg2] [int] NULL ,
	[MonthBeg3] [int] NULL ,
	[MonthBeg4] [int] NULL ,
	[MonthBeg5] [int] NULL ,
	[MonthBeg6] [int] NULL ,
	[MonthBeg7] [int] NULL ,
	[MonthBeg8] [int] NULL ,
	[MonthBeg9] [int] NULL ,
	[MonthBeg10] [int] NULL ,
	[MonthBeg11] [int] NULL ,
	[MonthBeg12] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CUSTOMER] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RunAvailable] [int] NULL ,
	[RUNBALANCE] [int] NULL ,
	[RunRedeemed] [int] NULL ,
	[LastStmtDate] [datetime] NULL ,
	[NextStmtDate] [datetime] NULL ,
	[STATUS] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[LASTNAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPFIRST] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPLAST] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME5] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME6] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HOMEPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WORKPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BusinessFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SegmentCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ComboStmt] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RewardsOnline] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOTES] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BonusFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc3] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc4] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc5] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RunBalanceNew] [int] NULL ,
	[RunAvaliableNew] [int] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[CUSTOMER_Stage] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RunAvailable] [int] NULL ,
	[RUNBALANCE] [int] NULL ,
	[RunRedeemed] [int] NULL ,
	[LastStmtDate] [datetime] NULL ,
	[NextStmtDate] [datetime] NULL ,
	[STATUS] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[LASTNAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPFIRST] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPLAST] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME5] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME6] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HOMEPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WORKPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BusinessFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SegmentCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ComboStmt] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RewardsOnline] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOTES] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BonusFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc3] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc4] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc5] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RunBalanceNew] [int] NULL ,
	[RunAvaliableNew] [int] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[CUSTOMERdeleted] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RunAvailable] [int] NULL ,
	[RUNBALANCE] [int] NULL ,
	[RunRedeemed] [int] NULL ,
	[LastStmtDate] [datetime] NULL ,
	[NextStmtDate] [datetime] NULL ,
	[STATUS] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DATEADDED] [datetime] NOT NULL ,
	[LASTNAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPFIRST] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIPLAST] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME5] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME6] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HOMEPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WORKPHONE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BusinessFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SegmentCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ComboStmt] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RewardsOnline] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOTES] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BonusFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc1] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc2] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc3] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc4] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Misc5] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RunBalanceNew] [int] NULL ,
	[RunAvaliableNew] [int] NULL ,
	[DateDeleted] [datetime] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Client] (
	[ClientCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipFirst] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address4] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zipcode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Phone1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Phone2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactPerson1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactPerson2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactEmail1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactEmail2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateJoined] [datetime] NULL ,
	[RNProgramName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TermsConditions] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsUpdatedDT] [datetime] NULL ,
	[MinRedeemNeeded] [int] NULL ,
	[TravelFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MerchandiseFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TravelIncMinPoints] [decimal](18, 0) NULL ,
	[MerchandiseBonusMinPoints] [decimal](18, 0) NULL ,
	[MaxPointsPerYear] [decimal](18, 0) NULL ,
	[PointExpirationYears] [int] NULL ,
	[ClientID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Pass] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ServerName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DbName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UserName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Password] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsExpire] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastTipNumberUsed] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Current_Month_Activity] (
	[Tipnumber] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[EndingPoints] [int] NULL ,
	[Increases] [int] NULL ,
	[Decreases] [int] NULL ,
	[AdjustedEndingPoints] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[DateforAudit] (
	[Datein] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HISTORY] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HISTDATE] [datetime] NULL ,
	[TRANCODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCount] [int] NULL ,
	[POINTS] [decimal](18, 0) NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SECID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ratio] [float] NULL ,
	[Overage] [decimal](5, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HISTORYTIP] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HISTDATE] [datetime] NULL ,
	[TRANCODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCount] [int] NULL ,
	[POINTS] [numeric](18, 0) NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SECID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ratio] [float] NULL ,
	[Overage] [numeric](9, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HISTORY_Stage] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HISTDATE] [datetime] NULL ,
	[TRANCODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCount] [int] NULL ,
	[POINTS] [decimal](18, 0) NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SECID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ratio] [float] NULL ,
	[Overage] [decimal](5, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[HistoryDeleted] (
	[TipNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HistDate] [datetime] NULL ,
	[TranCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCount] [int] NULL ,
	[Points] [decimal](18, 0) NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ratio] [float] NULL ,
	[Overage] [decimal](5, 0) NULL ,
	[DateDeleted] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Customer] (
	[MemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldMemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [char] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zip] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePhone] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CardType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last4] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Customer_error] (
	[MemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldMemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [char] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zip] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePhone] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CardType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last4] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Purge] (
	[MemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldMemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [char] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zip] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePhone] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CardType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last4] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Purge_Pending] (
	[MemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldMemberNum] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [char] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zip] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePhone] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CardType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StatusCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last4] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmployeeFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Transaction] (
	[MemberNum] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AccountNum] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldAccountNum] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PurchAmt] [money] NULL ,
	[PurchCnt] [int] NULL ,
	[trandate] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NewCard] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNUmber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Points] [numeric](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Input_Transaction_Error] (
	[MemberNum] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AccountNum] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OldAccountNum] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCode] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PurchAmt] [money] NULL ,
	[PurchCnt] [int] NULL ,
	[trandate] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NewCard] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TipNUmber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Points] [numeric](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Monthly_Audit_ErrorFile] (
	[Tipnumber] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsBegin] [decimal](18, 0) NULL ,
	[PointsEnd] [decimal](18, 0) NULL ,
	[PointsPurchasedCR] [decimal](18, 0) NULL ,
	[PointsPurchasedDB] [decimal](18, 0) NULL ,
	[PointsBonus] [decimal](18, 0) NULL ,
	[PointsAdded] [decimal](18, 0) NULL ,
	[PointsIncreased] [decimal](18, 0) NULL ,
	[PointsRedeemed] [decimal](18, 0) NULL ,
	[PointsReturnedCR] [decimal](18, 0) NULL ,
	[PointsReturnedDB] [decimal](18, 0) NULL ,
	[PointsSubtracted] [decimal](18, 0) NULL ,
	[PointsDecreased] [decimal](18, 0) NULL ,
	[Errormsg] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Currentend] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Monthly_Statement_File] (
	[Tipnumber] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Acctname1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Acctname2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CityStateZip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsBegin] [decimal](18, 0) NULL ,
	[PointsEnd] [decimal](18, 0) NULL ,
	[PointsPurchasedCR] [decimal](18, 0) NULL ,
	[PointsPurchasedDB] [decimal](18, 0) NULL ,
	[PointsBonus] [decimal](18, 0) NULL ,
	[PointsAdded] [decimal](18, 0) NULL ,
	[PointsIncreased] [decimal](18, 0) NULL ,
	[PointsRedeemed] [decimal](18, 0) NULL ,
	[PointsReturnedCR] [decimal](18, 0) NULL ,
	[PointsReturnedDB] [decimal](18, 0) NULL ,
	[PointsSubtracted] [decimal](18, 0) NULL ,
	[PointsDecreased] [decimal](18, 0) NULL ,
	[acctid] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cardseg] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[lastfour] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[pointfloor] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsExpire] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[OneTimeBonuses] (
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Trancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctID] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateAwarded] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[OneTimeBonuses_Stage] (
	[TipNumber] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Trancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctID] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DateAwarded] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile] (
	[Tipnumber] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsBegin] [decimal](18, 0) NULL ,
	[PointsEnd] [decimal](18, 0) NULL ,
	[PointsPurchasedCR] [decimal](18, 0) NULL ,
	[PointsPurchasedDB] [decimal](18, 0) NULL ,
	[PointsBonus] [decimal](18, 0) NULL ,
	[PointsAdded] [decimal](18, 0) NULL ,
	[PointsIncreased] [decimal](18, 0) NULL ,
	[PointsRedeemed] [decimal](18, 0) NULL ,
	[PointsReturnedCR] [decimal](18, 0) NULL ,
	[PointsReturnedDB] [decimal](18, 0) NULL ,
	[PointsSubtracted] [decimal](18, 0) NULL ,
	[PointsDecreased] [decimal](18, 0) NULL ,
	[Errormsg] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Currentend] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Quarterly_Statement_File] (
	[Tipnumber] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Acctname1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Acctname2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CityStateZip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsBegin] [decimal](18, 0) NULL ,
	[PointsEnd] [decimal](18, 0) NULL ,
	[PointsPurchasedCR] [decimal](18, 0) NULL ,
	[PointsPurchasedDB] [decimal](18, 0) NULL ,
	[PointsBonus] [decimal](18, 0) NULL ,
	[PointsAdded] [decimal](18, 0) NULL ,
	[PointsIncreased] [decimal](18, 0) NULL ,
	[PointsRedeemed] [decimal](18, 0) NULL ,
	[PointsReturnedCR] [decimal](18, 0) NULL ,
	[PointsReturnedDB] [decimal](18, 0) NULL ,
	[PointsSubtracted] [decimal](18, 0) NULL ,
	[PointsDecreased] [decimal](18, 0) NULL ,
	[acctid] [char] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cardseg] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[lastfour] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[pointfloor] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PointsExpire] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Results] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HISTDATE] [smalldatetime] NULL ,
	[TRANCODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCount] [int] NULL ,
	[POINTS] [decimal](18, 0) NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SECID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ratio] [float] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Status] (
	[Status] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[StatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TranCode_Factor] (
	[Trancode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PointFactor] [float] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TranType] (
	[TranCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[IncDec] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CntAmtFxd] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Points] [float] NOT NULL ,
	[Ratio] [float] NOT NULL ,
	[TypeCode] [nvarchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TransStandard] (
	[TIP] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranDate] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcctNum] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranCode] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranNum] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranAmt] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TranType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Ratio] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CrdActvlDt] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[WelcomeKit] (
	[TIPNUMBER] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ACCTNAME2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACCTNAME4] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS3] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[beginning_balance_month] (
	[monthbegin] [datetime] NOT NULL ,
	[beginbalance] [decimal](18, 0) NULL 
) ON [PRIMARY]
GO

