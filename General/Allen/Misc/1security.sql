if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[1Security]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[1Security]
GO

CREATE TABLE [dbo].[1Security] (
	[TipNumber] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Password] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretQ] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecretA] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmailStatement] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EMailOther] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Last6] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegDate] [datetime] NULL ,
	[Nag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

