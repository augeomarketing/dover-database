SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/******************************************************************************/
/*    Purge Customers FROM Staged or Production TABLEs.  */
/* BY:  Allen Barriere (stolen from R.Tremblay)  */
/* DATE: 10/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted CHAR(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read input_purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.


/******************************************************************************/
CREATE  PROCEDURE spPurgeClosedCustomers @Production_Flag CHAR(1), @DateDeleted CHAR(10) AS

DECLARE @SQLDynamic NVARCHAR(2000)
DECLARE @Tipnumber 	CHAR(15)
DECLARE @spErrMsgr VARCHAR(255)
----------- Production TABLE Processing ----------
IF @Production_Flag = 'P'
BEGIN

	-- copy any input_purge_pending INTO input_purge 
	INSERT INTO Input_Purge SELECT * FROM Input_Purge_Pending

	-- Clear Input_Purge_Pending 
	TRUNCATE TABLE Input_Purge_Pending

	-- Copy any customers FROM input_purge to input_purge_pending if they have History activity greater than the delete date
	INSERT INTO input_Purge_Pending 
		SELECT * FROM input_Purge  
		WHERE tipnumber IN ( SELECT DISTINCT tipnumber FROM history WHERE histdate > @DateDeleted and TranCode <> 'RQ' )

	-- Remove any customers FROM input_purge if they have current activity in history
	DELETE FROM input_Purge 
		WHERE tipnumber IN ( SELECT DISTINCT tipnumber FROM history WHERE histdate > @DateDeleted and TranCode <> 'RQ' )

	-------------- purge remainging input_purge records. 
	-- INSERT customer to customerdeleted 
	INSERT INTO CustomerDeleted 
		SELECT c.*, @DateDeleted  FROM Customer c WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- INSERT affiliat to affiliatdeleted 
	INSERT INTO AffiliatDeleted 
		SELECT AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LAStName, YTDEarned, CustId, @DateDeleted AS DateDeleted 
		FROM Affiliat  WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- copy history to historyDeleted 
	INSERT INTO HistoryDeleted 
		SELECT H.* , @DateDeleted AS DateDeleted FROM History H WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- DELETE FROM customer 
	DELETE FROM Customer
		WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- DELETE records FROM affiliat 
	DELETE FROM Affiliat   
		WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- DELETE records FROM History 
	DELETE FROM History 
		WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- flag all Undeleted Customers "C" that have an input_purge_pending record 
	UPDATE Customer SET status = 'C' 
		WHERE tipnumber IN (SELECT tipnumber FROM Input_Purge_Pending)

End
----------- Stage TABLE Processing ----------
ELSE IF @Production_Flag = 'S'
BEGIN
	TRUNCATE TABLE Input_Purge 
-- need to not delete open items

	INSERT INTO Input_Purge 
	SELECT * FROM Input_Customer WHERE [Acct Stat Cd] = '4'	

	DELETE FROM Input_Customer 	WHERE [Acct Stat Cd] = '4'
	DELETE FROM Customer_stage 	WHERE TipNumber IN     (SELECT TipNumber FROM Input_Purge)
	DELETE FROM Affiliat_Stage 	WHERE TipNumber NOT IN (SELECT TipNumber FROM Customer_Stage)
	DELETE FROM History_stage 	WHERE TipNumber NOT IN (SELECT TipNumber FROM Customer_Stage)
END
ELSE
BEGIN
	SET @spErrMsgr = 'Production Flag Not Recognized' 
	RETURN -100
END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

