if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TRIG_PERMISSION_UPDATE]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TRIG_PERMISSION_UPDATE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Permission]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Permission]
GO

CREATE TABLE [dbo].[Permission] (
	[PermissionID] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[PermissionName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PermissionDesc] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PermssionCreated] [datetime] NOT NULL ,
	[PermissionLastModified] [datetime] NOT NULL ,
	[PermissionActive] [int] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Permission] WITH NOCHECK ADD 
	CONSTRAINT [PK_Permission] PRIMARY KEY  CLUSTERED 
	(
		[PermissionID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Permission] WITH NOCHECK ADD 
	CONSTRAINT [DF_Permission_PermssionCreated] DEFAULT (getdate()) FOR [PermssionCreated],
	CONSTRAINT [DF_Permission_PermissionLastModified] DEFAULT (getdate()) FOR [PermissionLastModified],
	CONSTRAINT [DF_Permission_PermissionActive] DEFAULT (1) FOR [PermissionActive]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE TRIGGER [dbo].[TRIG_PERMISSION_UPDATE] ON [dbo].[permission] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @PERMISSIONID INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.PermissionID 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @PERMISSIONID 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE permission SET PermissionLastModified = getdate() WHERE PermissionID = @PERMISSIONID 
       
              FETCH NEXT FROM UPD_QUERY INTO @PERMISSIONID 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

