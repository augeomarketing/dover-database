if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[input_sharebal]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[input_sharebal]
GO

CREATE TABLE [dbo].[input_sharebal] (
	[input_sharebal_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[acctnbr] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[lastname] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[relationshipcode] [int] NULL ,
	[accttype] [int] NULL ,
	[avgbalance] [decimal](18, 0) NULL ,
	[sharetype] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

