
CREATE TABLE [dbo].[editcatalogcategory] (
	[sid_editcatalogcategory_id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[sid_catalogcategory_id] [int] NOT NULL ,
	[sid_catalog_id] [int] NOT NULL ,
	[sid_category_id] [int] NOT NULL ,
	[dim_catalogcategory_created] [datetime] NOT NULL ,
	[dim_catalogcategory_lastmodified] [datetime] NOT NULL ,
	[dim_catalogcategory_active] [int] NOT NULL 
	[dim_editcatalogcategory_created] [datetime] NOT NULL ,
	[dim_editcatalogcategory_lastmodified] [datetime] NOT NULL

) ON [PRIMARY]
GO

