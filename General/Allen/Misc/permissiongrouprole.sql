if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[permissiongrouprole]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[permissiongrouprole]
GO

CREATE TABLE [dbo].[permissiongrouprole] (
	[sid_permissiongrouprole_id] [int] NOT NULL ,
	[sid_permissiongroup_id] [int] NOT NULL ,
	[sid_role_id] [int] NOT NULL ,
	[dim_permissiongrouprole_created] [datetime] NOT NULL ,
	[dim_permissiongrouprole_lastmodified] [datetime] NOT NULL ,
	[dim_permissiongrouprole_active] [int] NOT NULL 
) ON [PRIMARY]
GO

