use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime
declare @enddate datetime

set @tipfirst = '260'
set @enddate = '12/17/2013'
set @message = '<p align="center"><b>Notice of Change - Travel Redemption </b></p><p><b>Effective ' + DATENAME("month", @enddate) + ' ' + CAST(DAY(@enddate) AS VARCHAR) + ', 2013, the Travel Rebate Certificate option will no longer be available. You can still apply a credit towards the purchase of any airline ticket to any destination, on any airline, in any class of service, at any time... with no restrictions! Or select from the fixed-point ticket options with no additional out of pocket fees. You can also book with our professional agents. Our Travel Resource Center Specialists will help you plan your trip or vacation—everything from your flight to unique adventure packages. We also offer eGift Cards such as Marriott, Celebrity Cruises and more! If you have already redeemed for a travel rebate certificate, your certificate is valid for one year from the issue date. Go to Use Points/ Travel to visit the Travel Resource Center today!</b></p>'
set @firstday = GETDATE()
set @lastday = DATEADD("day", 3, @enddate)

update RewardsNOW.dbo.webannounce set dim_webannounce_active = 0 where dim_webannounce_tipprefix = @tipfirst

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	values (@tipfirst, @message, @firstday, @lastday)
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc