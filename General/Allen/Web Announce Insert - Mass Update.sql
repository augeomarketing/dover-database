use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime

-- Set criteria for Tipfirsts here
-- select * from rewardsnow.dbo.dbprocessinfo where localmerchantparticipant <> 'N'

set @message = '<a href="shopmainstreet.asp"><img src="images/additional/sms_ma_banner_july2014.jpg" /></a>'
set @firstday = '5/3/2014' + ' 00:00:00'
set @lastday = '8/16/2014' + ' 00:00:00'


update RewardsNOW.dbo.webannounce 
set dim_webannounce_active = 0 
where dim_webannounce_tipprefix in (
	select dbnumber 
	from rewardsnow.dbo.dbprocessinfo 
	where localmerchantparticipant <> 'N')

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	select dbnumber, @message, @firstday, @lastday
	from rewardsnow.dbo.dbprocessinfo 
	where localmerchantparticipant <> 'N'
commit tran

select top 50 * 
from webannounce
order by dim_webannounce_lastmodified desc