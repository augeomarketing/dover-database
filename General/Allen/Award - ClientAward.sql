
select top 10 * from award
select distinct AwardCode FROM award
select top 10 * from clientaward

select top 10 * from Catalog.dbo.catalogTip
select top 10 * from Catalog.dbo.catalog

INSERT INTO CatalogTip (CatalogID, TipPrefix, CatalogTipPointValue)
SELECT (SELECT TOP 1 CatalogID FROM Catalog.dbo.Catalog WHERE CatalogCode = a.CatalogCode) AS CatalogID, b.TipFirst, ClientAwardPoints
FROM [RewardsNOW!].dbo.Award a
INNER JOIN [RewardsNOW!].dbo.ClientAward b
	ON a.AwardCode = b.ClientCode
WHERE b.TipFirst IN (360,362)
--WHERE (SELECT TOP 1 CatalogID FROM Catalog.dbo.Catalog WHERE CatalogCode = a.CatalogCode) IS Null

INSERT INTO Catalog (CatalogCode, CatalogTranCode,CatalogDollars)
SELECT DISTINCT CatalogCode , 'RM',0
FROM newTNB.dbo.Award a
INNER JOIN newTNB.dbo.ClientAward b
	ON a.AwardCode = b.ClientCode
WHERE (SELECT TOP 1 CatalogID FROM Catalog.dbo.Catalog WHERE CatalogCode = a.CatalogCode) IS Null
