use catalog

declare @tipfirst char(3) 
declare @loyaltyid int 
declare @finame varchar(50) 
  
 --Update variables 
 set @loyaltyid = 155 
 set @tipfirst = '930' 
 set @finame = 'US Road Sports Demo' 
  
-- begin tran
 
	 --Insert into Patton 
	 insert into catalog.dbo.loyaltytip (sid_loyalty_id, dim_loyaltytip_prefix, dim_loyaltytip_finame) values   
	   (@loyaltyid, @tipfirst, @finame)  
	    
	 --Insert into web DB 
	 insert into catalog.catalog.dbo.loyaltytip (sid_loyalty_id, dim_loyaltytip_prefix, dim_loyaltytip_finame) values  
	   (@loyaltyid, @tipfirst, @finame) 
	  
	 select top 5 * from catalog.dbo.loyaltytip 
	 order by sid_loyaltytip_id desc 
	  
	 select top 5 * from rn1.catalog.dbo.loyaltytip 
	 order by sid_loyaltytip_id desc
 
-- commit tran