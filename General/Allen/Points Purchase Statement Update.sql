use rewardsnow

/*
insert into webstatementtype (dim_webstatementtype_name, dim_webstatementtype_ratio)
values ('Points Purchased', 1)

insert into webstatementtype (dim_webstatementtype_name, dim_webstatementtype_ratio)
values ('Points Purchase Reversal', -1)
*/

-- REB, 216, 707, 717, 704, 138, 644
declare @tipfirst varchar(3) = '216'
declare @ppadd int
declare @ppsub int
declare @ppaddwst int
declare @ppsubwst int

select 1 from RewardsNOW.dbo.webstatementtrantype where sid_webstatement_id in (select sid_webstatement_id from webstatement where dim_webstatement_tipfirst = @tipfirst)
if @@ROWCOUNT <> 0
	begin
		set @ppaddwst = (select sid_webstatementtype_id from webstatementtype where dim_webstatementtype_name = 'Points Purchased')
		set @ppsubwst = (select sid_webstatementtype_id from webstatementtype where dim_webstatementtype_name = 'Points Purchase Reversal')

		-- add
		insert into dbo.webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst)
		values (@ppaddwst, 'PNTBONUSPRCHS', @tipfirst)
		set @ppadd = SCOPE_IDENTITY()

		insert into webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
		values (@ppaddwst, 99, @tipfirst)

		--subtract
		insert into dbo.webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst)
		values (@ppsubwst, 'PNTBONUSRETRN', @tipfirst)
		set @ppsub = SCOPE_IDENTITY()

		insert into webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
		values (@ppsubwst, 99, @tipfirst)
		
		insert into dbo.webstatementtrantype (sid_webstatement_id, sid_trantype_trancode)
		select @ppadd, TranCode
		from TranType
		where Ratio = 1
		and TranCode like 'p%'
		
		insert into dbo.webstatementtrantype (sid_webstatement_id, sid_trantype_trancode)
		select @ppsub, TranCode
		from TranType
		where Ratio = -1
		and TranCode like 'p%'

		select * from webstatementtrantype where sid_webstatement_id in (select sid_webstatement_id from webstatement where dim_webstatement_tipfirst = @tipfirst)
		select * from webstatement where dim_webstatement_tipfirst = @tipfirst
	end
	else
		print 'Nothing to add'
		select 'Nothing to add'

