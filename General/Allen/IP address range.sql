declare @lowip nvarchar(20) = '65.113.217.1'
declare @highip nvarchar(20) = '65.113.217.254'

--declare @lowip nvarchar(20) = '67.129.193.1'
--declare @highip nvarchar(20) = '67.129.193.254'

--declare @tipfirst varchar(3) = 'RNI'
declare @tipfirst varchar(3) = '266'
declare @application INT = 1

declare @lowoctet int = ParseName(@lowip, 1)
declare @highoctet int = ParseName(@highip, 1)

declare @currentoctet varchar(3) = @lowoctet
declare @currentip varchar(20)
while @currentoctet <= @highoctet
begin
	set @currentip = ParseName(@lowip, 4) + '.' + ParseName(@lowip, 3) + '.' + ParseName(@lowip, 2) + '.' + @currentoctet
	print @currentip
	insert into RewardsNOW.dbo.ssoip (dim_ssoip_ip, dim_ssoip_remotehost, dim_ssoip_tipprefix, dim_ssoip_application)
	values (@currentip, @currentip, @tipfirst, @application)
	set @currentoctet = @currentoctet + 1
end