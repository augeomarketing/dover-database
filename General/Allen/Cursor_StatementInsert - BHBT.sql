use bhbt
--Cursor through DBs
Declare tipnumber cursor 
--for Select name as dbname from master.dbo.sysdatabases
--where (name like '1%') and name not like '111%'
for select distinct rtrim(tipnumber) as tipnumber from customer
where tipnumber like '%999999%' and tipnumber not in (select travnum from statement)

Declare @tipnumber varchar(15)
Declare @sqlcmd nvarchar(4000)

open tipnumber
Fetch next from tipnumber into @tipnumber
while @@Fetch_Status = 0
	Begin
	  set @sqlcmd =	
		'insert into statement 
		(Travnum, Acctname1, PntBeg, PntEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
		PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased)
		values
		(''' + @tipnumber + ''', ''Shawn Smith'', ''0'', ''0'', ''0'', ''0'', ''0'', ''0'',
		''0'', ''0'', ''0'', ''0'', ''0'', ''0'')'
	  EXECUTE sp_executesql @SqlCmd
      print @sqlcmd
	  print '---'
	  fetch Next from tipnumber into @tipnumber
	end
Close tipnumber
Deallocate tipnumber