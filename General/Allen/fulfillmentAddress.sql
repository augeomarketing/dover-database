declare @SQL nvarchar(1000)
declare @dbname varchar(50)
declare @tipfirst varchar(3)

DECLARE gettips CURSOR FAST_FORWARD FOR 
SELECT DISTINCT(LEFT(tipnumber,3)) FROM fullfillment.dbo.main
WHERE (saddress1 IS NULL or saddress1 = '') AND redstatus NOT IN (2,3,4,5,6,7,8)

OPEN gettips
FETCH NEXT FROM gettips INTO @tipfirst
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @dbname = dbNamePatton 
		FROM rewardsnow.dbo.dbprocessinfo 
		WHERE dbnumber= @tipfirst	
	
	SET @SQL = 'UPDATE f SET saddress1 = c.address1, saddress2 = c.address2 FROM ' + quotename(@dbname) + '.dbo.customer c join fullfillment.dbo.main f on c.tipnumber = f.tipnumber WHERE (f.saddress1 IS NULL or f.saddress1 = '''') AND f.redstatus NOT IN (2,3,4,5,6,7,8) AND c.address1 IS NOT NULL AND c.address1 <> '''''
	--print @SQL
	exec sp_executesql @SQL
	FETCH NEXT FROM gettips INTO @tipfirst
END
CLOSE gettips
DEALLOCATE gettips
