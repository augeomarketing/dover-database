USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_centcatalogselect]    Script Date: 03/19/2009 16:34:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[usp_centcatalogselect]

@tipfirst char(3),
@group int = 0,
@category int = 0, 
@catalogcode varchar(20) = '0'

AS

declare @sqlcmd nvarchar(4000)
declare @sqlcmdorder nvarchar(1000)

set @sqlcmd = 
	'select distinct dim_catalog_code as catalogcode, dim_loyaltycatalog_pointvalue as points, dim_catalogdescription_name as name
		  FROM loyaltycatalog lc INNER JOIN loyaltytip l ON l.sid_loyalty_id = lc.sid_loyalty_id  
	      INNER JOIN catalog c on LC.sid_catalog_id = c.sid_catalog_id  
	      INNER JOIN catalogdescription cd on cd.sid_catalog_id = c.sid_catalog_id  
	      INNER JOIN catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id 
	      INNER JOIN categorygroupinfo cg ON cc.sid_category_id = cg.sid_category_id 
	      WHERE dim_loyaltytip_prefix = ''' + @tipfirst + '''
	      AND sid_status_id = 1
		  AND dim_catalog_active = 1
	      AND sid_languageinfo_id = 1
	      AND dim_catalogcategory_active = 1
		  AND dim_loyaltycatalog_pointvalue > -1
		  '
if @category <> 0
	set @sqlcmd = @sqlcmd + 'AND cc.sid_category_id = ' + convert(varchar(7), @category) + ' 
		  '
if @group <> 0
	set @sqlcmd = @sqlcmd + 'AND sid_groupinfo_id = ' + convert(varchar(7), @group) + ' 
		  '
if @catalogcode <> '0'
	set @sqlcmd = @sqlcmd + 'AND dim_catalog_code = ''' + convert(varchar(20), @catalogcode) + '''
		  '
set @sqlcmdorder = 'ORDER BY dim_loyaltycatalog_pointvalue, dim_catalogdescription_name'
set @sqlcmd = @sqlcmd + @sqlcmdorder

EXECUTE sp_executesql @sqlcmd