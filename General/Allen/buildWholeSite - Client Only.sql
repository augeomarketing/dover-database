declare @tipfirst char(3) 
declare @loyaltyid int 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @LIB_template_Sid  int
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @custtravfee int
declare @website varchar(1024)
declare @travelratio numeric(18,0)

  
 --Update variables 
set @loyaltyid = 8
set @tipfirst = '406' 
set @finame = 'United Catholics FCU' 
set @clientcode = 'UCFCU'
set @CustomerServicePhone = '1-866-P2P-REWD (1-866-727-7393)'
set @programname = 'Point2Point Rewards'
set @travelincrement = 2500
set @travelbottom = 2500
set @cashbackmin = 9999999
set @merch = 1
set @cashback = 0
set @AirFee = 10
set @logo = @tipfirst + 'logo.jpg'
-- set @logo = ''
set @termspage = @tipfirst + 'terms.asp'
set @faqpage = @tipfirst + 'faq.asp'
set @earnpage = @tipfirst + 'earning.asp'
--
-- Check certs down below
--
set @Business = 0
set @statementdefault = 1
set @StmtNum = 1
set @LIB_template_Sid =  8 --(Select top 1 sid_loyalty_id from catalog.dbo.loyalty where dim_loyalty_liab =1 order by sid_loyalty_id)
set @database = 'OneBridge'
set @liab = 1
set @custtravfee = 1
set @website = 'www.point2pointrewards.com'


SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst


IF @@ROWCOUNT = 0
BEGIN
	insert into clientaward (TIPFirst, ClientCode) VALUES (@tipfirst,@clientcode)

	insert into client (ClientCode, ClientName, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault, StmtNum, CustomerServicePhone)
	select @clientcode, @finame, @programname, '02/28/2009', @travelincrement, @travelbottom, @cashbackmin, @merch, @AirFee,  @logo,  @termspage, @faqpage, @earnpage, @Business, @statementdefault, @StmtNum, @CustomerServicePhone
END
ELSE
BEGIN 
	SET @clientcode = (SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst)
	UPDATE client 
		SET
		RNProgramName = @programname, 
		TravelMinimum = @travelincrement, 
		TravelBottom = @travelbottom, 
		CashBackMinimum = @cashbackmin,  
		Merch = @merch, 
		AirFee = @AirFee, 
		logo = @logo, 
		termspage = @termspage, 
		faqpage = @faqpage, 
		earnpage = @earnpage, 
		Business = @Business, 
		statementdefault = @statementdefault, 
		StmtNum = @StmtNum, 
		CustomerServicePhone = @CustomerServicePhone
		WHERE clientcode = @clientcode
END
