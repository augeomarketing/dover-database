declare @tipfirst char(3) 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @dynamicCss int
declare @custtravfee int
declare @travelccreqd int
declare @website varchar(1024)
declare @travelratio numeric(18,0)
declare @ssoonly int
declare @payforpoints int

  
 --Update variables 
set @tipfirst = '264' 
set @finame = 'Seasons FCU'
set @clientcode = 'SeasonsSig' 
set @CustomerServicePhone = '855-4 SS RWDS (855-477-7937)'
set @programname = 'Signature' 
set @travelincrement = 0
set @travelbottom = 0
set @cashbackmin = 0
set @merch = 0
set @cashback = 0
set @AirFee = 0
set @logo = ''
set @termspage = @tipfirst + 'terms.asp'
set @faqpage = @tipfirst + 'faq.asp'
set @earnpage = @tipfirst + 'earning.asp'
set @Business = 0
set @statementdefault = 1
set @StmtNum = 2
set @database = 'Seasons'
set @liab = 0
set @custtravfee = 0
set @travelccreqd = 0
set @website = 'www.rewardsnow.com/seasons/signature/'
set @travelratio = 0 --100 for most, 200 for Omni... points divided by travelratio = dollars
set @dynamicCss = 0
set @ssoonly = 0
set @payforpoints = 1

SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into client (ClientCode, ClientName, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault, StmtNum, CustomerServicePhone)
	select @clientcode, @finame, @programname, GETDATE(), @travelincrement, @travelbottom, @cashbackmin, @merch, @AirFee,  @logo,  @termspage, @faqpage, @earnpage, @Business, @statementdefault, @StmtNum, @CustomerServicePhone
	
	insert into clientaward (TIPFirst, ClientCode) VALUES (@tipfirst,@clientcode)
END
ELSE
BEGIN 
	SET @clientcode = (SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst)
	UPDATE client 
		SET
		RNProgramName = @programname, 
		TravelMinimum = @travelincrement, 
		TravelBottom = @travelbottom, 
		CashBackMinimum = @cashbackmin,  
		Merch = @merch, 
		AirFee = @AirFee, 
		logo = @logo, 
		termspage = @termspage, 
		faqpage = @faqpage, 
		earnpage = @earnpage, 
		Business = @Business, 
		statementdefault = @statementdefault, 
		StmtNum = @StmtNum, 
		CustomerServicePhone = @CustomerServicePhone
		WHERE clientcode = @clientcode
END

SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec, travelratio, TravelCCReqdInProgram)
	values (@tipfirst, @finame, @database, 'Certs', 1, 1, 1, 1, @cashback, @liab, 1, @custtravfee, 1, 0, @travelratio, @travelccreqd)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.searchdb 
	SET
		ClientName = @finame
	WHERE tipfirst = @tipfirst
END

select sid_webinit_id FROM rewardsnow.dbo.webinit WHERE dim_webinit_defaulttipprefix = @tipfirst
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO rewardsnow.dbo.webinit (dim_webinit_defaulttipprefix, dim_webinit_database, dim_webinit_baseurl, dim_webinit_javascript, dim_webinit_cssstyle, dim_webinit_cssmenu, dim_webinit_cssstructure, dim_webinit_cssstyleprint, dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning, dim_webinit_ssoonly)
	VALUES (@Tipfirst, @database, @website, @tipfirst + 'custom.js', @tipfirst + 'style.css', @tipfirst + 'menu.css', @tipfirst + 'style_structure.css', 'stylePrint.css','menuPrint.css','style_structurePrint.css', @programname, @programname, @termspage, @faqpage, @earnpage, @ssoonly)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.webinit 
		SET
			dim_webinit_database = @database, 
			dim_webinit_baseurl = @website, 
			dim_webinit_programname = @programname, 
			dim_webinit_sitetitle = @programname, 
			dim_webinit_defaultterms = @termspage, 
			dim_webinit_defaultfaq = @faqpage, 
			dim_webinit_defaultearning = @earnpage,
			dim_webinit_ssoonly = @ssoonly
		WHERE dim_webinit_defaulttipprefix = @tipfirst
END

IF @dynamicCss = 1
BEGIN
	UPDATE RewardsNOW.dbo.webinit 
		SET dim_webinit_cssmenu = 'dynamiccss.asp', 
			dim_webinit_cssstructure = '',
			dim_webinit_cssstyle = ''
		WHERE dim_webinit_defaulttipprefix = @tipfirst
END

IF @payforpoints <> 0
BEGIN
	INSERT INTO RewardsNOW.dbo.payforpoints (dim_payforpoints_tipfirst, dim_payforpoints_minratio, dim_payforpoints_maxratio, dim_payforpoints_minpoints, dim_payforpoints_maxpointsforratio, dim_payforpoints_increment)
	VALUES (@tipfirst, 0.035, 0.020, 250, 5000, 50)
end

IF @travelratio <> 100
BEGIN
	UPDATE RewardsNOW.dbo.RNIWebParameter
	SET dim_rniwebparameter_value = @travelratio
		WHERE sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniwebparameter_key = 'TRAVELRATIO'
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO RewardsNOW.dbo.RNIWebParameter (sid_dbprocessinfo_dbnumber, dim_rniwebparameter_key, dim_rniwebparameter_value)
		VALUES (@tipfirst, 'TRAVELRATIO', @travelratio)
	END
END

UPDATE RewardsNOW.dbo.RNIWebParameter
SET dim_rniwebparameter_value = @travelccreqd
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst
	AND dim_rniwebparameter_key = 'TRAVELCCREQD'
	
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO RewardsNOW.dbo.RNIWebParameter (sid_dbprocessinfo_dbnumber, dim_rniwebparameter_key, dim_rniwebparameter_value)
	VALUES (@tipfirst, 'TRAVELCCREQD', @travelccreqd)
END