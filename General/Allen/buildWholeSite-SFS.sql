declare @tipfirst char(3) 
declare @finame varchar(50) 
declare @clientcode varchar(50) 
declare @programname varchar(50)
declare @travelincrement int
declare @travelbottom int
declare @cashbackmin int
declare @merch int
declare @AirFee int
declare @logo varchar(50)
declare @termspage varchar(50)
declare @faqpage varchar(50)
declare @earnpage varchar(50)
declare @Business int
declare @statementdefault int
declare @StmtNum int
declare @CustomerServicePhone varchar(50)
declare @sid_loyaltycatalog_id_seed int
declare @sql nvarchar(4000)
declare @database varchar(50)
declare @cashback int
declare @liab int
declare @custtravfee int
declare @travelccreqd int
declare @website varchar(1024)
declare @travelratio numeric(18,0)
declare @dynamiccss int
declare @ssoonly int

  
 --Update variables 
set @tipfirst = '699' 
set @finame = 'ShoppingFLING' 
set @clientcode = 'SFS'
set @CustomerServicePhone = '603-516-3440'
set @programname = 'ShoppingFLING'
set @travelincrement = 2500
set @travelbottom = 2500
set @cashbackmin = 9999999
set @merch = 1
set @cashback = 0
set @AirFee = 0
set @logo = ''
set @termspage = @tipfirst + 'terms.asp'
set @faqpage = @tipfirst + 'faq.asp'
set @earnpage = @tipfirst + 'earning.asp'
set @Business = 0
set @statementdefault = 1
set @StmtNum = 4
set @database = 'testclass990'
set @liab = 1
set @custtravfee = 0
set @travelccreqd = 0
set @website = 'www.rewardsnow.com/shoppingfling/'
set @travelratio = 100 --100 for most, 200 for Omni, 200 for LA DOTD... points divided by travelratio = dollars
set @dynamicCss = 0
set @ssoonly = 0


SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst


IF @@ROWCOUNT = 0
BEGIN
	insert into client (ClientCode, ClientName, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, logo, termspage, faqpage, earnpage, Business, statementdefault, StmtNum, CustomerServicePhone)
	select @clientcode, @finame, @programname, '02/28/2010', @travelincrement, @travelbottom, @cashbackmin, @merch, @AirFee,  @logo,  @termspage, @faqpage, @earnpage, @Business, @statementdefault, @StmtNum, @CustomerServicePhone
	
	insert into clientaward (TIPFirst, ClientCode) VALUES (@tipfirst,@clientcode)
END
ELSE
BEGIN 
	SET @clientcode = (SELECT clientcode FROM clientaward WHERE tipfirst = @tipfirst)
	UPDATE client 
		SET
		RNProgramName = @programname, 
		TravelMinimum = @travelincrement, 
		TravelBottom = @travelbottom, 
		CashBackMinimum = @cashbackmin,  
		Merch = @merch, 
		AirFee = @AirFee, 
		logo = @logo, 
		termspage = @termspage, 
		faqpage = @faqpage, 
		earnpage = @earnpage, 
		Business = @Business, 
		statementdefault = @statementdefault, 
		StmtNum = @StmtNum, 
		CustomerServicePhone = @CustomerServicePhone
		WHERE clientcode = @clientcode
END

SELECT clientname FROM rewardsnow.dbo.searchdb WHERE tipfirst = @tipfirst

IF @@ROWCOUNT = 0
BEGIN
	insert into rewardsnow.dbo.searchdb (TIPFirst, ClientName, dbase, certspage, Website, newstyle, history, encrypted, CASH, LIAB, QTR, Custtravelfee, ratio, disp_dec, travelratio, TravelCCReqdInProgram)
	values (@tipfirst, @finame, @database, 'Certs', 1, 1, 1, 1, @cashback, @liab, 1, @custtravfee, 1, 0, @travelratio, @travelccreqd)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.searchdb 
	SET
		ClientName = @finame, 
		dbase = @database, 
		--certspage = 'Certs', 
		Website = 1, 
		newstyle = 1 , 
		history = 1 , 
		encrypted = 1, 
		CASH = @cashback, 
		LIAB = @liab, 
		QTR = 1, 
		Custtravelfee = @custtravfee, 
		ratio = 1, 
		disp_dec = 0, 
		travelratio = @travelratio,
		TravelCCReqdInProgram = @travelccreqd
	WHERE tipfirst = @tipfirst
END

select sid_webinit_id FROM rewardsnow.dbo.webinit WHERE dim_webinit_defaulttipprefix = @tipfirst
IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO rewardsnow.dbo.webinit (dim_webinit_defaulttipprefix, dim_webinit_database, dim_webinit_baseurl, dim_webinit_javascript, dim_webinit_cssstyle, dim_webinit_cssmenu, dim_webinit_cssstructure, dim_webinit_cssstyleprint, dim_webinit_cssmenuprint, dim_webinit_cssstructureprint, dim_webinit_programname, dim_webinit_sitetitle, dim_webinit_defaultterms, dim_webinit_defaultfaq, dim_webinit_defaultearning)
	VALUES (@Tipfirst, @database, @website, @tipfirst + 'custom.js', @tipfirst + 'style.css', @tipfirst + 'menu.css', @tipfirst + 'style_structure.css', 'stylePrint.css','menuPrint.css','style_structurePrint.css', @programname, @programname, @termspage, @faqpage, @earnpage)
END
ELSE
BEGIN
	UPDATE rewardsnow.dbo.webinit 
		SET
			dim_webinit_database = @database, 
			dim_webinit_baseurl = @website, 
			dim_webinit_programname = @programname, 
			dim_webinit_sitetitle = @programname, 
			dim_webinit_defaultterms = @termspage, 
			dim_webinit_defaultfaq = @faqpage, 
			dim_webinit_defaultearning =	@earnpage
		WHERE dim_webinit_defaulttipprefix = @tipfirst
END
