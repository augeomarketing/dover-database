-- Run from Patton
-- Category 1 is merch
-- Category 2 is travel
-- Category 3 is GCs

declare @loyaltyid varchar(3)
declare @groupinfo varchar(3)

set @loyaltyid = '11'
set @groupinfo = '2'

update catalog.dbo.loyaltycatalog
set dim_loyaltycatalog_pointvalue = -1
where sid_loyalty_id = @loyaltyid AND sid_catalog_id IN (
SELECT sid_catalog_id FROM catalog.dbo.catalogcategory cc INNER JOIN catalog.dbo.categorygroupinfo cg
ON cc.sid_category_id = cg.sid_category_id WHERE sid_groupinfo_id = @groupinfo)

update catalog.dbo.editloyaltycatalog
set dim_loyaltycatalog_pointvalue = -1
where sid_loyalty_id = @loyaltyid AND sid_catalog_id IN (
SELECT sid_catalog_id FROM catalog.dbo.catalogcategory cc INNER JOIN catalog.dbo.categorygroupinfo cg
ON cc.sid_category_id = cg.sid_category_id WHERE sid_groupinfo_id = @groupinfo)

update rn1.catalog.dbo.loyaltycatalog
set dim_loyaltycatalog_pointvalue = -1
where sid_loyalty_id = @loyaltyid AND sid_catalog_id IN (
SELECT sid_catalog_id FROM catalog.dbo.catalogcategory cc INNER JOIN catalog.dbo.categorygroupinfo cg
ON cc.sid_category_id = cg.sid_category_id WHERE sid_groupinfo_id = @groupinfo)

update rn1.catalog.dbo.editloyaltycatalog
set dim_loyaltycatalog_pointvalue = -1
where sid_loyalty_id = @loyaltyid AND sid_catalog_id IN (
SELECT sid_catalog_id FROM catalog.dbo.catalogcategory cc INNER JOIN catalog.dbo.categorygroupinfo cg
ON cc.sid_category_id = cg.sid_category_id WHERE sid_groupinfo_id = @groupinfo)