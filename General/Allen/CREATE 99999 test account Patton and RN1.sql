-- RUN FROM PATTON

declare @points INT = 25000,
		@tipnumber varchar(20) = '6ZZ000000001000',
		@firstname varchar(50) = 'Linda',
		@lastname varchar(50) = 'Pettit',
		@address1 varchar(50) = '2777 E. Guasti Road, Suite 6',
		@company varchar(10) = 'CalCenter CU',
		@city varchar(50) = 'Ontario',
		@state varchar(2) = 'CA',
		@zip varchar(10) = '91761',
		@email varchar(50) = 'lpettit@calcentercu.org'
		
-- RNI side
Insert into rn1.coop.dbo.Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, city, state) 
Values (@tipnumber ,LEFT(@tipnumber, 3), RIGHT(@tipnumber, 12),@firstname + ' ' + @lastname,@company,@address1,@city + ' ' + @state + ' ' + @zip,@zip, @points,'0', @points, 'A', @city, @state) 

Insert into rn1.coop.dbo.Account (Tipnumber, Lastname, Lastsix, SSNLast4, MemberNumber) 
Values (@tipnumber ,@lastname,'0' + @zip, '', '') 

INSERT into rn1.coop.dbo.[1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther, SecretA) 
Values (@tipnumber , LEFT(@firstname,1) + @lastname, 'sample', 'N', @email, 'N', 'Testing')

update rn1.coop.dbo.[1Security]
set Password = (
select top 1 password
from rn1.neba.dbo.[1security]
where tipnumber like '%999999%')
where TipNumber = @tipnumber

-- Patton side
insert into [990].dbo.customer (TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, STATUS, DATEADDED, ACCTNAME1, ADDRESS1, ADDRESS2, address4, City, State, ZipCode)
VALUES (@tipnumber, @points, @points, 0, 'A', GETDATE(), @firstname + ' ' + @lastname, @company, @address1, @city + ' ' + @state + ' ' + @zip, @city, @state, @zip)

insert into [990].dbo.AFFILIAT (ACCTID, TIPNUMBER, AcctType, DATEADDED)
values (@zip, @tipnumber, 'Zipcode', GETDATE())
