use RewardsNOW

declare @tipfirst varchar(3) = '119'
declare @fieldname varchar(30) = 'PNTEXPIRE'

insert into webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst)
values (11, @fieldname, @tipfirst)

insert into webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
values (11, 100, @tipfirst)