use rewardsnow

declare @oldtip char(3)
declare @newtip char(3)

set @oldtip = '52N'
set @newtip = '233'

delete from webstatement where dim_webstatement_tipfirst = @newtip
delete from webstatementtypeorder where dim_webstatementtypeorder_tipfirst = @newtip

insert into dbo.webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst, dim_webstatement_active)
select sid_webstatementtype_id, dim_webstatement_dbfield, @newtip as dim_webstatement_tipfirst, dim_webstatement_active
from dbo.webstatement
where dim_webstatement_tipfirst = @oldtip

insert into dbo.webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
select sid_webstatementtype_id, dim_webstatementtypeorder_order, @newtip as dim_webstatementtypeorder_tipfirst
from dbo.webstatementtypeorder
where dim_webstatementtypeorder_tipfirst = @oldtip

select * from webstatement where dim_webstatement_tipfirst = @newtip
select * from webstatementtypeorder where dim_webstatementtypeorder_tipfirst = @newtip