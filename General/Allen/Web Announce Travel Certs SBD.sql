use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime
declare @enddate datetime

set @tipfirst = '232'
set @enddate = '12/17/2013'
set @message = '<p><b>We hope that you are enjoying the rewards you�ve received by participating in the SBD Rewards Program. We�d like to let you know about a slight modification to the SBD Rewards Program.  Effective Tuesday, December 17th, 2013, the option to redeem points for Travel Rebate Certificates will no longer be available. However, you may still redeem points for travel by booking online or calling a Travel Resource Center agent. You still have the option of paying with a combination of points and your debit or credit card. Please visit the Travel Resource Center for more information and to learn about other ways you can use SBD Rewards points to see the world! If you have further questions about the SBD Rewards Program, please contact an SBD Rewards Service Specialist, available 24 hours a day, 7 days a week, at 1-877-SBD-RWDS (877-723-7937).</b></p>'
set @firstday = GETDATE()
set @lastday = DATEADD("day", 3, @enddate)

update RewardsNOW.dbo.webannounce set dim_webannounce_active = 0 where dim_webannounce_tipprefix = @tipfirst

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	values (@tipfirst, @message, @firstday, @lastday)
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc