use RewardsNow
/*
select dbnumber
from dbprocessinfo
where DBNumber in (
'615',
'150',
'003',
'002',
'621',
'713',
'214',
'625',
'164',
'219',
'240',
'206',
'160',
'617',
'644',
'A01',
'611',
'636',
'109',
'163',
'603',
'233',
'637',
'241',
'A02',
'REB')
and DBNumber in (select dim_webbanner_tipprefix from webbanner)
*/

insert into webbanner (dim_webbanner_tipprefix, dim_webbanner_banner1image, dim_webbanner_banner1link, dim_webbanner_banner2image, dim_webbanner_banner2link, dim_webbanner_banner3image, dim_webbanner_banner3link, dim_webbanner_banner4image, dim_webbanner_banner4link)
select dbnumber, './images/bannerAds/shoppingfling_holiday.jpg', 'shopping.asp', '', '', '', '', '', ''
from dbprocessinfo
where DBNumber in (
'615',
'150',
'003',
'002',
'621',
'713',
'214',
'625',
'164',
'219',
'240',
'206',
'160',
'617',
'644',
'A01',
'611',
'636',
'109',
'163',
'603',
'233',
'637',
'241',
'A02',
'REB')
and DBNumber not in (select dim_webbanner_tipprefix from webbanner)