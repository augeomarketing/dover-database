--Run from Patton

use catalog

declare @Category_name varchar(50)
declare @Category_desc varchar(50)
declare @ImageName varchar(50)

set @Category_desc = 'Chef''s Catalogue'
set @Category_name = replace(replace(@Category_desc, '''', ''), ' ', '_')
set @ImageName = 'icon_' + lower(replace(@Category_name, '.', '_')) + '.jpg'

print @category_desc
print @category_name
print @imageName

insert into catalog.dbo.category (dim_category_name, dim_category_description, dim_category_imagelocation)
	values (@Category_name, @Category_desc, @ImageName)

insert into catalog.catalog.dbo.category (dim_category_name, dim_category_description, dim_category_imagelocation)
	values (@Category_name, @Category_desc, @ImageName)

select top 20 * from catalog.dbo.category
order by sid_category_id desc

select top 20 * from catalog.catalog.dbo.category
order by sid_category_id desc

-- Add to categorygroupinfo
/*
insert into catalog.dbo.categorygroupinfo (sid_category_id, sid_groupinfo_id)
values (129, 4)

insert into catalog.catalog.dbo.categorygroupinfo (sid_category_id, sid_groupinfo_id)
values (120, 4)

select top 20 * from catalog.dbo.categorygroupinfo
order by sid_category_id desc

select top 20 * from catalog.catalog.dbo.categorygroupinfo
order by sid_category_id desc
*/
