declare @oldtip varchar(3)
declare @newtip varchar(3)

set @oldtip = '211'
set @newtip = '911'

update affiliat
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update customer
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update history
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update beginning_balance_table
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update historytip
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update monthly_audit_errorfile
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update monthly_statement_file
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update OneTimeBonuses
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update quarterly_audit_errorfile
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update quarterly_statement_file
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update Results
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update RN1_EstatementTip
set tipnumber = replace(tipnumber, @oldtip, @newtip)
update TransStandard
set TIP = replace(TIP, @oldtip, @newtip)
update WelcomeKit
set tipnumber = replace(tipnumber, @oldtip, @newtip)

update client
set tipfirst = replace(tipfirst, @oldtip, @newtip),
	lasttipnumberused = replace(lasttipnumberused, @oldtip, @newtip)
