--
-- Run from Patton
--
use catalog

declare @tipfirst char(3)
declare @newid int
declare @loyalty_name varchar(50)
declare @loyalty_desc varchar(250)
declare @FIName varchar(50)
declare @loyalty_liab int

set @tipfirst = 'V01'
set @loyalty_name = 'Vive Plus'
set @loyalty_desc = ''
set @loyalty_liab = '0'
set @FIName = 'Santander'

insert into catalog.dbo.loyalty (dim_loyalty_name, dim_loyalty_description, dim_loyalty_liab)
values (@loyalty_name, @loyalty_desc, @loyalty_liab)
set @newid = (select scope_identity())
--Keep above line directly below insert

insert into catalog.catalog.dbo.loyalty (dim_loyalty_name, dim_loyalty_description, dim_loyalty_liab)
values (@loyalty_name, @loyalty_desc, @loyalty_liab)

update catalog.dbo.loyaltytip
set sid_loyalty_id = @newid
where dim_loyaltytip_prefix = @tipfirst

if @@rowcount = 0
begin
	insert into catalog.dbo.loyaltytip (sid_loyalty_id, dim_loyaltytip_prefix, dim_loyaltytip_finame)
	values (@newid, @tipfirst, @FIName)
end

update catalog.catalog.dbo.loyaltytip
set sid_loyalty_id = @newid
where dim_loyaltytip_prefix = @tipfirst

if @@rowcount = 0
begin
	insert into catalog.catalog.dbo.loyaltytip (sid_loyalty_id, dim_loyaltytip_prefix, dim_loyaltytip_finame)
	values (@newid, @tipfirst, @FIName)
end

select top 10 *
from catalog.dbo.loyalty
order by sid_loyalty_id desc

select top 10 *
from catalog.dbo.loyaltytip
order by sid_loyalty_id desc

select top 10 *
from catalog.catalog.dbo.loyalty
order by sid_loyalty_id desc

select top 10 *
from catalog.catalog.dbo.loyaltytip
order by sid_loyalty_id desc
