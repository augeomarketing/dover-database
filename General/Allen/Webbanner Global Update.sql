use rewardsnow
--select * from webbanner

declare @banner1image varchar(100)
declare @banner1link varchar(100)
declare @banner2image varchar(100)
declare @banner2link varchar(100)
declare @banner3image varchar(100)
declare @banner3link varchar(100)
declare @banner4image varchar(100)
declare @banner4link varchar(100)

begin tran

set @banner1image = 'anchor'
set @banner1link = '196'
set @banner2image = 'keurig'
set @banner2link = '4091'
set @banner3image = 'mixer'
set @banner3link = '4193'
set @banner4image = 'slowcooker'
set @banner4link = '1926'

update RewardsNOW.dbo.webbanner
set dim_webbanner_banner1image = REPLACE(dim_webbanner_banner1image, 'bose', @banner1image),
	dim_webbanner_banner1link = REPLACE(dim_webbanner_banner1link, '2731', @banner1link),
	dim_webbanner_banner2image = REPLACE(dim_webbanner_banner2image, 'bluray', @banner2image),
	dim_webbanner_banner2link = REPLACE(dim_webbanner_banner2link, '4117', @banner2link),
	dim_webbanner_banner3image = REPLACE(dim_webbanner_banner3image, 'cobra', @banner3image),
	dim_webbanner_banner3link = REPLACE(dim_webbanner_banner3link, '3837', @banner3link)--,
	--dim_webbanner_banner4image = REPLACE(dim_webbanner_banner4image, 'hamilton', @banner4image),
	--dim_webbanner_banner4link = REPLACE(dim_webbanner_banner4image, '2731', @banner4link),

select * from webbanner

--rollback tran
--commit tran