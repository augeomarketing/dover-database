use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime
declare @enddate datetime

set @message = '<p align="center"><b>Important Notice Regarding Travel Rebate Certificates<br /><br />Effective April 15, 2014, the option to redeem for Travel Rebate Certificates will be discontinued.  If you have already redeemed points for a travel rebate certificate, it is valid for one year from the issue date.  You may still redeem points for travel by booking online or calling a Travel Resource Center Specialist.  Our Travel Resource Center Specialists will help you plan your trip or vacation—everything from your flight to unique adventure packages. Visit the Travel Resource Center today and see how far your points will take you!</b></p>'
set @firstday = '3/3/2014 01:00:00'
set @lastday = '4/19/2014 01:00:00'

update RewardsNOW.dbo.webannounce
set dim_webannounce_active = 0 
where dim_webannounce_tipprefix in ('51D', '51M', '52S', '52Y', '579', '580', '582', '585')

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	select dbnumber, @message, @firstday, @lastday
	from RewardsNOW.dbo.dbprocessinfo dbpi
	where DBNumber in ('51D', '51M', '52S', '52Y', '579', '580', '582', '585')
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc