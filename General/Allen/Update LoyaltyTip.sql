--
-- Run from Patton
-- select * from catalog.dbo.loyaltytip
--
use catalog

declare @tipfirst char(3)
declare @newid int
declare @FIName varchar(50)

set @tipfirst = '910'
set @newid = 27
set @FIName = (select clientname from rn1.rewardsnow.dbo.searchdb where tipfirst = @tipfirst)


print @tipfirst
update catalog.dbo.loyaltytip
set sid_loyalty_id = @newid
where dim_loyaltytip_prefix = @tipfirst

if @@rowcount = 0
begin
	insert into catalog.dbo.loyaltytip (sid_loyalty_id, dim_loyaltytip_prefix, dim_loyaltytip_finame)
	values (@newid, @tipfirst, @FIName)
end

update catalog.catalog.dbo.loyaltytip
set sid_loyalty_id = @newid
where dim_loyaltytip_prefix = @tipfirst

if @@rowcount = 0
begin
	insert into catalog.catalog.dbo.loyaltytip (sid_loyalty_id, dim_loyaltytip_prefix, dim_loyaltytip_finame)
	values (@newid, @tipfirst, @FIName)
end

select top 5 * from rn1.catalog.dbo.loyaltytip
order by sid_loyaltytip_id desc

select top 5 * from loyaltytip
order by sid_loyaltytip_id desc