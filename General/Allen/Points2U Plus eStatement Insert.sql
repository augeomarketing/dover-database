declare @tipfirst varchar(3)

set @tipfirst = '51S'

use rewardsnow
insert into webstatement (sid_webstatementtype_id, dim_webstatement_dbfield, dim_webstatement_tipfirst)
values ('13', 'PntBonusMer', @tipfirst)

insert into webstatementtypeorder (sid_webstatementtype_id, dim_webstatementtypeorder_order, dim_webstatementtypeorder_tipfirst)
values ('13', '40', @tipfirst)

print @tipfirst