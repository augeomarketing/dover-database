select *
from dbo.webinit

begin tran

update wi
set	dim_webinit_defaulttipprefix = wi2.dim_webinit_defaulttipprefix, 
	dim_webinit_database = wi2.dim_webinit_database, 
	dim_webinit_javascript = wi2.dim_webinit_javascript , 
	dim_webinit_cssstyle = wi2.dim_webinit_cssstyle , 
	dim_webinit_cssmenu = wi2.dim_webinit_cssmenu , 
	dim_webinit_cssstructure = wi2.dim_webinit_cssstructure , 
	dim_webinit_cssstyleprint = wi2.dim_webinit_cssstyleprint , 
	dim_webinit_cssmenuprint = wi2.dim_webinit_cssmenuprint , 
	dim_webinit_cssstructureprint = wi2.dim_webinit_cssstructureprint , 
	dim_webinit_programname = wi2.dim_webinit_programname , 
	dim_webinit_sitetitle = wi2.dim_webinit_sitetitle , 
	dim_webinit_defaultterms = wi2.dim_webinit_defaultterms , 
	dim_webinit_defaultfaq = wi2.dim_webinit_defaultfaq , 
	dim_webinit_defaultearning = wi2.dim_webinit_defaultearning 

from rewardsnow.dbo.webinit wi, rewardsnow.dbo.webinit wi2
where wi.dim_webinit_baseurl = '167394-web2.rewardsnow.com/testing999/'
and wi2.dim_webinit_baseurl = 'www.rewardsnow.com/occu/'

select *
from rewardsnow.dbo.webinit
rollback tran
commit tran