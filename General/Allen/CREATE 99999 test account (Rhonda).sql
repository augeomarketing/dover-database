begin tran
 Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
 Values ('98000000000999','980','000000000999','RHONDA MCCOY','3777 WEST ROAD','','EAST LANSING MI 48826','48826','15000','0','15000','A') 
  
 Insert into Account (Tipnumber, Lastname, Lastsix, SSNLast4, MemberNumber) 
 Values ('98000000000999','MCCOY','048826', '048826', '048826') 
  
 INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther, SecretA) 
 Values ('98000000000999', 'rmccoy', 'sample', 'N', 'rmccoy@msufcu.org', 'N', 'Testing') 
commit tran

update [1Security]
set Password = (
	select top 1 password
	from neba.dbo.[1security]
	where tipnumber like '%999999%')
where TipNumber like '%9999999%'
and (password is null or rtrim(password) = '' or password = 'sample')