begin tran
 Insert into Customer (Tipnumber, Tipfirst, TIPLast, Name1, Address1, Address2, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status) 
 Values ('919999999999999','919','999999999999','SHAWN SMITH','383 CENTRAL AVE','SUITE 350','DOVER NH 03820','03820','20012','0','20012','S') 
  
 Insert into Account (Tipnumber, Lastname, Lastsix, SSNLast4, MemberNumber) 
 Values ('919999999999999','SMITH','919919', '9919', '123456') 
  
 INSERT into [1Security] (Tipnumber, Username, Password, EmailStatement, Email, EmailOther, SecretA) 
 Values ('919999999999999', '919test', 'sample', 'N', 'ssmith@rewardsnow.com', 'N', 'Testing') 
commit tran

update [1Security]
set Password = (
	select top 1 password
	from neba.dbo.[1security]
	where tipnumber like '%999999%')
where TipNumber like '%9999999%'
and (password is null or rtrim(password) = '' or password = 'sample')