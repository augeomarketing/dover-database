use RewardsNOW

declare @tipfirst char(3)
declare @message nvarchar(2048)
declare @firstday datetime
declare @lastday datetime

set @tipfirst = 'RNI'
set @message = '<p><b>Please Note:</b> On Tuesday, April 29, 2014, internet access to your rewards account information will be unavailable from 4:00 AM EDT to approximately 8:00 AM EDT due to planned, scheduled system maintenance. We apologize for any inconvenience this may cause.</p>'
set @firstday = '4/21/2014' + ' 00:00:00'
set @lastday = '4/29/2014' + ' 10:00:00'

if @tipfirst <> 'RNI'
	update RewardsNOW.dbo.webannounce set dim_webannounce_active = 0 where dim_webannounce_tipprefix = @tipfirst

begin tran
	insert into webannounce (dim_webannounce_tipprefix, dim_webannounce_message, dim_webannounce_firstday, dim_webannounce_lastday)
	values (@tipfirst, @message, @firstday, @lastday)
commit tran

select top 10 * 
from webannounce
order by dim_webannounce_lastmodified desc