declare @histdate smalldatetime
set @histdate = '6/1/2008'

use fullfillment

select tipfirst, sum(points) as monthred, (
	Select sum(points) as Total from Main m
	JOIN travelcert on Main.transid = travelcert.transid 
	where m.tipfirst = main.tipfirst
	AND Trancode in ('ru', 'rv', 'rt') 
	and tipfirst in ('557', '559', '561', '563')
	and month(@histdate) = month(histdate)
	and year(@histdate) = year(histdate)
	AND TCStatus = '2'  
	AND Redstatus = '7' 
	group by tipfirst)
from main
where tipfirst in ('557', '559', '561', '563')
and trancode in ('rt', 'ru', 'rv')
and month(@histdate) = month(histdate)
and year(@histdate) = year(histdate)
group by tipfirst

/*Select Tipfirst,  sum(points) as Total from Main 
JOIN travelcert on Main.transid = travelcert.transid 
where tipfirst = '557' 
AND Trancode in ('ru', 'rv', 'rt') 
and month(@histdate) = month(histdate)
and year(@histdate) = year(histdate)
AND TCStatus = '2'  
AND Redstatus = '7' 
group by tipfirst  */