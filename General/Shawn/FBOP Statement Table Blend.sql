use rewardsnow
select wst.sid_webstatementtype_id, ws.dim_webstatement_dbfield, ws.dim_webstatement_tipfirst, 
	wst.dim_webstatementtype_name
from webstatement ws, webstatementtype wst
where ws.dim_webstatement_tipfirst in (
	select tipfirst
	from metavante.dbo.clientaward ca, metavante.dbo.client c
	where rnprogramname = 'bankrewards'
	and c.clientcode = ca.clientcode)
and ws.sid_webstatementtype_id = wst.sid_webstatementtype_id
order by dim_webstatement_tipfirst