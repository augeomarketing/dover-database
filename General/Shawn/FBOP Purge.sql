declare @histdate smalldatetime
set @histdate = left(getdate() - day(getdate()),11)
set @histdate = @histdate + 1
--print @histdate

Select 'Park National' as Client, left(cd.tipnumber,3) as Tipfirst, cd.Tipnumber, AcctName1 as 'Name', Address1 as 'Address 1', Address4 as 'City, State, Zip', zipcode as 'ZIP', runavailable as 'Points', ad.secid as 'DDA', Right(acctid,6) as [Last 6], cd.datedeleted as 'Date Deleted', (select isnull(sum(points*ratio),0) from historydeleted where year(datedeleted) = year(@histdate)) as YTDDel, (select isnull(sum(points*ratio),0) from historydeleted where year(datedeleted) = year(@histdate) and month(datedeleted) = year(datedeleted)) as MonthDel
from [557ParkNationalConsumer].dbo.customerdeleted cd, [557ParkNationalConsumer].dbo.affiliatdeleted ad
where cd.Tipnumber = ad.tipnumber and cd.datedeleted < @histdate
union
Select 'California National' as Client, left(cd.tipnumber,3) as Tipfirst, cd.Tipnumber, AcctName1 as 'Name', Address1 as 'Address 1', Address4 as 'City, State, Zip', zipcode as 'ZIP', runavailable as 'Points', ad.secid as 'DDA', Right(acctid,6) as [Last 6], cd.datedeleted as 'Date Deleted'
from [559CaliforniaNationalConsumer].dbo.customerdeleted cd, [559CaliforniaNationalConsumer].dbo.affiliatdeleted ad
where cd.Tipnumber = ad.tipnumber and cd.datedeleted < @histdate
union
Select 'San Diego National' as Client, left(cd.tipnumber,3) as Tipfirst, cd.Tipnumber, AcctName1 as 'Name', Address1 as 'Address 1', Address4 as 'City, State, Zip', zipcode as 'ZIP', runavailable as 'Points', ad.secid as 'DDA', Right(acctid,6) as [Last 6], cd.datedeleted as 'Date Deleted'
from [561SanDiegoNationalConsumer].dbo.customerdeleted cd, [561SanDiegoNationalConsumer].dbo.affiliatdeleted ad
where cd.Tipnumber = ad.tipnumber and cd.datedeleted < @histdate
union
Select 'Pacific National' as Client, left(cd.tipnumber,3) as Tipfirst, cd.Tipnumber, AcctName1 as 'Name', Address1 as 'Address 1', Address4 as 'City, State, Zip', zipcode as 'ZIP', runavailable as 'Points', ad.secid as 'DDA', Right(acctid,6) as [Last 6], cd.datedeleted as 'Date Deleted'
from [563PacificNationalConsumer].dbo.customerdeleted cd, [563PacificNationalConsumer].dbo.affiliatdeleted ad
where cd.Tipnumber = ad.tipnumber and cd.datedeleted < @histdate
union
Select 'BankUSA' as Client, left(cd.tipnumber,3) as Tipfirst, cd.Tipnumber, AcctName1 as 'Name', Address1 as 'Address 1', Address4 as 'City, State, Zip', zipcode as 'ZIP', runavailable as 'Points', ad.secid as 'DDA', Right(acctid,6) as [Last 6], left(cd.datedeleted,11) as 'Date Deleted', (select isnull(sum(points*ratio),0) from historydeleted where year(datedeleted) = year(@histdate)) as YTDDel, (select isnull(sum(points*ratio),0) from historydeleted where year(datedeleted) = year(@histdate) and month(datedeleted) = year(datedeleted)) as MonthDel
from [50ABankUSA].dbo.customerdeleted cd, [50ABankUSA].dbo.affiliatdeleted ad
where cd.Tipnumber = ad.tipnumber and cd.datedeleted < @histdate
order by left(cd.tipnumber,3), cd.datedeleted, cd.tipnumber 
