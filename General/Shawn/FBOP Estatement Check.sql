declare @ldlm smalldatetime
set @ldlm = getdate() - day(getdate())
set @ldlm = left(@ldlm,11)
set @ldlm = dateadd(hh, 23, @ldlm)
set @ldlm = dateadd(mi, 59, @ldlm)
set @ldlm = dateadd(ss, 58, @ldlm)

use metavante
select clientname, left(tipnumber,3) as Inst, month(regdate) as Month, year(regdate) as year, count(left(tipnumber,3)) as stmtcounts, (
	select count(left(tipnumber,3)) as stmtcount 
	from [1security] sec 
	where password is not null
	and left(sec.tipnumber,3) = left([1security].tipnumber,3)
	and month(sec.regdate) = month([1security].regdate)
	and year(sec.regdate) = year([1security].regdate)
) as regcounts
from [1security], clientaward, client
where password is not null and emailstatement = 'y' and left(tipnumber,3) in 
	(select tipfirst from clientaward where clientcode in 
		(select clientcode from client where rnprogramname = 'bankrewards')) 
	and month(regdate) is not null
	and left([1security].tipnumber,3) = clientaward.tipfirst
	and clientaward.clientcode = client.clientcode
	and regdate < @ldlm
group by left(tipnumber,3), month(regdate), year(regdate), clientname
order by left(tipnumber,3), year(regdate) desc, month(regdate) desc


/*select clientname, left(tipnumber,3) as Inst, month(regdate) as Month, year(regdate) as year, count(left(tipnumber,3)) as regcounts 
from [1security], clientaward, client
where password is not null and left(tipnumber,3) in 
	(select tipfirst from clientaward where clientcode in 
		(select clientcode from client where rnprogramname = 'bankrewards')) 
	and month(regdate) is not null
	and left([1security].tipnumber,3) = clientaward.tipfirst
	and clientaward.clientcode = client.clientcode
group by left(tipnumber,3), month(regdate), year(regdate), clientname
order by left(tipnumber,3), year(regdate) desc, month(regdate) desc*/