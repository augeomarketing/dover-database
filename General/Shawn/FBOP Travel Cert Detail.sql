--Determine previous full month end date in relation to current day
declare @today datetime
declare @monthend datetime

set @today = getdate()
set @monthend = cast(year(@today) as varchar(4)) + '/' + right('00' + cast(month(@today) as varchar(2)), 2) + '/' + '01'  + ' 00:00:00'
set @monthend = convert(nvarchar(25),(Dateadd(minute, -1, @monthend)),121)

Select	clientname,
		tipnumber, 
		rtrim(name1) as Name1,
		isnull(rtrim(name2),'') as Name2,
		TCID as Certificate_Number,
		points/100 as CashValue,
		histdate as Redemption_Date
from main join travelcert on main.transid = travelcert.transid, login.dbo.client
where (month(histdate) = month(@monthend) and year(histdate) = year(@monthend)) and points <> 0
and left(tipnumber,3) in (Select tipfirst from [Workops].dbo.FBOPtips) and
login.dbo.client.tipfirst = left(tipnumber,3)
order by login.dbo.client.tipfirst, histdate desc