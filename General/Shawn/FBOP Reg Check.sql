select left(tipnumber,3) as Inst, month(regdate) as Month, year(regdate) as year, count(left(tipnumber,3)) as counts 
from [1security] 
where password is not null and left(tipnumber,3) in 
	(select tipfirst from clientaward where clientcode in 
		(select clientcode from client where rnprogramname = 'bankrewards')) 
	and month(regdate) is not null
group by left(tipnumber,3), month(regdate), year(regdate) 
order by left(tipnumber,3), month(regdate), year(regdate) 