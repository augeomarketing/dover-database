select 
	left(tipnumber,3) as Inst, 
	Clientname, 
	month(regdate) as [Month], 
	year(regdate) as [year] 
	,
	(select count(left(tipnumber,3)) FROM [1security] where left(tipnumber,3) = left(a.tipnumber,3) AND [password] IS NOT NULL
		AND month(regdate) = month(a.regdate) AND year(regdate) = year(a.regdate)
	) as regcounts
	
	--,(select count(left(tipnumber,3)) FROM [1security] where left(tipnumber,3) = left(a.tipnumber,3) AND emailstatement = 'Y') as statementcounts
	--,(select count(left(tipnumber,3)) FROM [1security] where left(tipnumber,3) = left(a.tipnumber,3) AND emailstatement = 'Y') / (select count(left(tipnumber,3)) FROM [1security] where left(tipnumber,3) = left(a.tipnumber,3) AND [password] IS NOT NULL) as percentage 

	from [1security] a, clientaward b, client c
	where 
	a.[password] is not null 
	and left(a.tipnumber,3) = b.tipfirst 
	and b.clientcode = c.clientcode 
	and rnprogramname = 'bankrewards'
--	group by left(tipnumber,3), clientname, month(regdate), year(regdate)
--	order by left(tipnumber,3), month(regdate), year(regdate)

/*
--select left(tipnumber,3) as Inst, Clientname, month(regdate) as Month, year(regdate) as year, count(left(tipnumber,3)) as stmtcounts
select count(left(tipnumber,3)) as stmtcounts 
from [1security] a, clientaward b, client c
where password is not null 
and left(a.tipnumber,3) = b.tipfirst 
and b.clientcode = c.clientcode 
and rnprogramname = 'bankrewards'
and emailstatement = 'Y'
group by left(tipnumber,3), month(regdate), year(regdate), clientname
order by left(tipnumber,3), month(regdate), year(regdate)
*/