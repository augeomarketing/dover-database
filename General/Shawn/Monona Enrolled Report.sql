truncate table zzzshawn.dbo.monona

--CREATE TABLE #monona(
--	[tipnumber] [varchar](15) NOT NULL,
--	[acctname1] [varchar](40) NOT NULL,
--	[address1] [varchar](40) NULL,
--	[address2] [varchar](40) NULL,
--	[city] [varchar](40) NULL,
--	[state] [varchar](2) NULL,
--	[zipcode] [varchar](15) NULL,
--	[acctid] [varchar] (16) NOT NULL,
--) ON [PRIMARY]
--SET ANSI_PADDING OFF
--GO


insert into zzzshawn.dbo.monona
select tipnumber, acctname1, address1, address2, city, state, zipcode, (select top 1 acctid from [527MononaConsumer].dbo.affiliat where tipnumber = [527MononaConsumer].dbo.customer.tipnumber order by dateadded desc) as acctid
from [527MononaConsumer].dbo.customer
where getdate() - dateadded > 90
and tipnumber not in (
	select tipnumber
	from rn1.metavante.dbo.[1security]
	where password is not null)
go

insert into zzzshawn.dbo.monona
select tipnumber, acctname1, address1, address2, city, state, zipcode, (select top 1 acctid from [593MononaCommercial].dbo.affiliat where tipnumber = [593MononaCommercial].dbo.customer.tipnumber order by dateadded desc) as acctid
from [593MononaCommercial].dbo.customer
where getdate() - dateadded > 90
and tipnumber not in (
	select tipnumber
	from rn1.metavante.dbo.[1security]
	where password is not null)
go

select * from zzzshawn.dbo.monona