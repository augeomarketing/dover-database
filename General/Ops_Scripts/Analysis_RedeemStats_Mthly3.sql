--Returns monthly statistical data on redemption levels based on Onlinehistorywork records
--This should be run against the Patton DB for greater accuracy

Select	left(tipnumber, 3) as TIPFirst,
	ClientName as ClientName,
	Year(histdate)	as [Year],
	month(histdate)	as [Month],
	Count(points)	as Count_Redeem,
	Avg(points)	as Average_Redeem,
	Sum(points)	as Total_Redeem,
	Max(points)	as Max_Redeem,
	Min(points)	as Min_Redeem
From	onlinehistorywork.dbo.onlhistory o left outer join fullfillment.dbo.subclient c
		on left(o.tipnumber, 3) = c.tipfirst
where	points > '0' and left(tipnumber,1 ) <> '9' and tipnumber not like '%999999%' and histdate >= '1/1/2007'
group by	left(tipnumber, 3), clientname, Year(Histdate), month(histdate)
order by	left(tipnumber, 3), Year(Histdate), month(histdate)

--Returns monthly statistical data on redemption levels based on Onlinehistorywork records
--This should be run against the Patton DB for greater accuracy

Select
	t.description		as Redemption_Type,
	Count(m.points)		as Count_Redeem,
	Sum(m.points)		as Points_Redeem,
	Avg(m.points)		as Average_Redeem
From	Fullfillment.dbo.main m
			left outer join rewardsnow.dbo.trantype t on m.trancode = t.trancode
where	Month(histdate) = '8' and year(histdate) = '2009' and left(m.trancode,1) = 'R'
group by	t.description
order by	t.description

Select
	trancode			as Trancode,
	trandesc			as Redemption_Type,
	Count(m.points)		as Count_Redeem,
	Sum(m.points)		as Points_Redeem,
	Avg(m.points)		as Average_Redeem
From	Fullfillment.dbo.main m
where	Month(histdate) = '8' and year(histdate) = '2009'
group by	trancode, trandesc
order by	trancode, trandesc

Select * from Fullfillment.dbo.main 
where	Month(histdate) = '11' and year(histdate) = '2008'
order by histdate desc

sELECT * FROM REWARDSNOW.DBO.TRANTYPE