--Determine previous full month end date in relation to current day
declare @today datetime
declare @monthend datetime
declare @tipfirst varchar(3)

set @today = getdate()
set @monthend = cast(year(@today) as varchar(4)) + '/' + right('00' + cast(month(@today) as varchar(2)), 2) + '/' + '01'  + ' 00:00:00'
set @monthend = convert(nvarchar(25),(Dateadd(millisecond, -3, @monthend)),121)
set @tipfirst = '561'

--Select @today, @monthend, (Select Min(histdate) from history) as Prog_Start
/*
Select	tipnumber, 
		rtrim(name1) as Name1,
		isnull(rtrim(name2),'') as Name2,
		TCID as Certificate_Number,
		points/100 as CashValue,
		histdate as Redemption_Date
from main join travelcert on main.transid = travelcert.transid 
where tipfirst = @tipfirst and histdate <= @monthend
order by tipfirst, histdate desc
*/
Select	tipnumber, 
		rtrim(name1) as Name1,
		isnull(rtrim(name2),'') as Name2,
		TCID as Certificate_Number,
		points/100 as CashValue,
		histdate as Redemption_Date
from main join travelcert on main.transid = travelcert.transid 
where tipfirst = @tipfirst and (month(histdate) = month(@monthend) and year(histdate) = year(@monthend))
order by tipfirst, histdate desc


/*
Select	*
from main join travelcert on main.transid = travelcert.transid 
where tipfirst = '557'
order by histdate desc
*/