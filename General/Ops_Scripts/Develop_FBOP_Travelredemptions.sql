--Create table [Workops].dbo.FBOPtips (TIPFirst varchar(3))
--insert into [Workops].dbo.FBOPtips Select '557'
--insert into [Workops].dbo.FBOPtips Select '559'
--insert into [Workops].dbo.FBOPtips Select '561'
--insert into [Workops].dbo.FBOPtips Select '563'

--Create table [Workops].dbo.FBOPreport (TIPFirst varchar(3), Points_Total int, Points_Processed int, Points_Outstanding int)
truncate table [Workops].dbo.FBOPreport

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select tipfirst from [Workops].dbo.FBOPtips
Declare @TIPFirst varchar(3)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @TIPFirst
while @@Fetch_Status = 0
	Begin
	Insert into [Workops].dbo.FBOPreport
		 Select @TIPFirst, null, null, null
	Update [Workops].dbo.FBOPreport
	Set Points_Total = 
		(Select sum(points)
		 from Main 
		 where tipfirst = @TIPFirst
		 AND Trancode in ('ru', 'rv', 'rt') 
--		 AND (month(Histdate)='6' and year(histdate) = '2008')  
		 group by tipfirst)
	Where Tipfirst = @TIPFirst
	Update [Workops].dbo.FBOPreport
	set Points_Processed = 
		(Select sum(points)
		 from Main JOIN travelcert on Main.transid = travelcert.transid 
		 where tipfirst = @TIPFirst
		 AND Trancode in ('ru', 'rv', 'rt') 
--		 AND (month(Histdate)='6' and year(histdate) = '2008') 
		 AND TCStatus = '2'  
		 AND Redstatus = '7'  
		 group by tipfirst)
	Where Tipfirst = @TIPFirst
	Update [Workops].dbo.FBOPreport
	Set Points_Outstanding = Points_Total - Points_Processed
	Where Tipfirst = @TIPFirst

	  fetch Next from curDBName into @TIPFirst
	end
Close curDBName
Deallocate curDBName

Select *
From [Workops].dbo.FBOPreport
order by TIPFirst