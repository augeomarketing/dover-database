Select clientname as Client, tcstatusname as Status, Count(points) as Count_redeems, Sum(points) as Points 
from main m join travelcert t on m.transid = t.transid
		join subtravelcertstatus s on t.tcstatus = s.tcstatus
		join subclient c on m.tipfirst = c.tipfirst
where m.tipfirst in ('557','559','561','563')
group by clientname, tcstatusname
order by clientname, tcstatusname

Select * from subtravelcertstatus
Select * from subclient