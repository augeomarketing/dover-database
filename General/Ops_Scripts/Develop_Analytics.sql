Drop table #tmpresults

Create table #tmpresults 
	(Y int, M int,Points_Earned int, Point_Count int, Points_Average int, Customer_Count int, Customer_Points_Avg int)

insert into #tmpresults
Select	Year(histdate)																	as Y,
		month(histdate)																	as M, 
		isnull(sum((points+isnull(overage, 0))*ratio), 0)								as Points_Earned,
		isnull(count(distinct tipnumber), 0)											as Point_Count, 
		0																				as Points_Average,
		(Select Count(tipnumber) from customer 
		 where isnull(Statusdescription, 'Blank') not like '%combine%' and
		 (year(dateadded) < Year(histdate) or 
		  (year(dateadded) = Year(histdate) and month(dateadded) <= month(histdate))))	as Customer_Count,
		0																				as Customer_Points_Avg
from history
where trancode like '6%'
group by Year(histdate), month(histdate)
order by Year(histdate), month(histdate)

Update #tmpresults
set points_earned = points_earned +
(Select isnull(sum((points+isnull(overage, 0))*ratio), 0) as Points_Earned from historydeleted where trancode like '6%'
 and Year(histdate) = #tmpresults.Y and month(histdate) = #tmpresults.M)

Update #tmpresults
set point_count = point_count +
(Select count(distinct tipnumber) as Point_Count from historydeleted where trancode like '6%'
 and Year(histdate) = #tmpresults.Y and month(histdate) = #tmpresults.M)

Update #tmpresults
set Customer_Count = Customer_Count +
(Select Count(tipnumber) from customerdeleted 
		 where isnull(Statusdescription, 'Blank') not like '%combine%' and tipnumber not in (Select tipnumber from customer) and
		 (year(dateadded) < #tmpresults.Y or 
		  (year(dateadded) = #tmpresults.Y and month(dateadded) <= #tmpresults.M)) and
		 (year(datedeleted) > #tmpresults.Y or 
		  (year(datedeleted) = #tmpresults.Y and month(datedeleted) >= #tmpresults.M)))

Update #tmpresults
set Points_Average = Convert(numeric(8,0),Points_Earned/Point_Count)

Update #tmpresults
set Customer_Points_Avg = Convert(numeric(8,0),Points_Earned/Customer_Count)


Select * from #tmpresults order by Y,M
