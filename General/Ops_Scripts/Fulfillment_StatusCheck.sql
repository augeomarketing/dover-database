--Generates a list of all redemptions in Fulfillment still listed as "IN PROCESS"

Select rtrim(tipfirst)as TipFirst, 
Rtrim(tipnumber) as TipNumber,
rtrim(Name1) as Name1,
rtrim(Name2) as Name2,
rtrim(Trancode) as TranCode,
rtrim(trandesc) as TranDesc,
rtrim(Itemnumber) as CatalogCode,
rtrim(points) as Points,
rtrim(OrderID) as Order_ID,
Histdate as HistDate,
Cast((Getdate()-histdate)as smallint) as Days_inProcess
From main
where redstatus = '0'
Order by Days_inProcess Desc