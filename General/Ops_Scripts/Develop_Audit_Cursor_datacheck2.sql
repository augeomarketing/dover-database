exec [workops].dbo.spdatacheck '201'

--Show results
--Diagnostics: Balances/Orphaned History/Excess TIPFirsts
Select
(Select isnull(count(tipnumber),0) from customer where runbalance - runredeemed <> runavailable) as Balance_Err,
(Select isnull(Count(*),0) from customer where tipnumber in (
 Select customer.Tipnumber from customer join history on customer.tipnumber = history.tipnumber
 Group by customer.tipnumber, runavailable Having Sum(points*ratio) <> runavailable)) as HistAvail_Err,
(Select isnull(count(h.tipnumber),0) from history h left outer join customer c on h.tipnumber = c.tipnumber where c.tipnumber is null) as Orphans,
(Select isnull(count(distinct left(tipnumber, 3)),0) from affiliat) as Affiliat_TIP,
(Select isnull(count(distinct left(tipnumber, 3)),0) from customer) as Customer_TIP,
(Select isnull(count(distinct left(tipnumber, 3)),0) from history) as History_TIP,
(Select isnull(count(distinct left(tipnumber, 3)),0) from monthly_statement_file) as Statement_TIP

--Show historical point allocations
Select * from [workops].dbo.hist_datacheck order by yr, mo	

--Select * from history where tipnumber not in (select tipnumber from customer)