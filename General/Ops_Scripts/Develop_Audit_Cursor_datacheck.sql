--Drop table [workops].dbo.hist_datacheck
--Create table [workops].dbo.hist_datacheck (yr int, mo int, Net bigint, Returns bigint, Bonus bigint, R_Bonus bigint, New_Cust int, Del_Cust int, New_Acct int, Del_Acct int)
Truncate table [workops].dbo.hist_datacheck
create table #tmprslts (yr int, mo int, points bigint)
create table #tmprslts2 (yr int, mo int, points bigint)

--Set Month and Year
insert into #tmprslts
Select year(histdate)as yr ,month(histdate) as mo,'0' as points
from history
group by year(histdate),month(histdate)
union
Select year(histdate)as yr ,month(histdate) as mo,'0' as points
from historydeleted
group by year(histdate),month(histdate)

insert into [workops].dbo.hist_datacheck
Select yr, mo, '0' as Net, '0' as returns, '0' as bonus, '0' as r_bonus, '0' as new_cust, '0' as del_cust, '0' as new_acct, '0' as del_acct
from #tmprslts
group by yr ,mo

--Set Net Points
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from history
where left(trancode,1) in ('6')
group by year(histdate),month(histdate)
union
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from historydeleted
where left(trancode,1) in ('6')
group by year(histdate),month(histdate)
insert into #tmprslts2
Select yr, mo, sum(points) as points
from #tmprslts
group by yr ,mo

Update [workops].dbo.hist_datacheck
set Net = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set Return Points
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from history
where left(trancode,1) in ('3')
group by year(histdate),month(histdate)
union
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from historydeleted
where left(trancode,1) in ('3')
group by year(histdate),month(histdate)
insert into #tmprslts2
Select yr, mo, sum(points) as points
from #tmprslts
group by yr ,mo

Update [workops].dbo.hist_datacheck
set Returns = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set Bonus Points
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from history
where left(trancode,1) in ('0','B','N','F') and trancode <> 'BE'
group by year(histdate),month(histdate)
union
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from historydeleted
where left(trancode,1) in ('0','B','N','F') and trancode <> 'BE'
group by year(histdate),month(histdate)
insert into #tmprslts2
Select yr, mo, sum(points) as points
from #tmprslts
group by yr ,mo

Update [workops].dbo.hist_datacheck
set Bonus = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set RewardsNOW Bonus Points
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from history
where trancode = 'BE'
group by year(histdate),month(histdate)
union
Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points
from historydeleted
where trancode = 'BE'
group by year(histdate),month(histdate)
insert into #tmprslts2
Select yr, mo, sum(points) as points
from #tmprslts
group by yr ,mo

Update [workops].dbo.hist_datacheck
set R_Bonus = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set New Customer Count
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts
Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points
from customer
where status <> 'X'
group by year(dateadded),month(dateadded)
union
Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points
from customerdeleted
where status <> 'X'
group by year(dateadded),month(dateadded)
insert into #tmprslts2
Select yr, mo, sum(points) as points
from #tmprslts
group by yr ,mo

Update [workops].dbo.hist_datacheck
set new_cust = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set Deleted Customer Count
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts2
Select year(datedeleted)as yr ,month(datedeleted) as mo,count(tipnumber) as points
from customerdeleted
where status <> 'X'
group by year(datedeleted),month(datedeleted)

Update [workops].dbo.hist_datacheck
set del_cust = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set New Account Count
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts
Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points
from affiliat
where acctstatus <> 'X'
group by year(dateadded),month(dateadded)
union
Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points
from affiliatdeleted
where acctstatus <> 'X'
group by year(dateadded),month(dateadded)
insert into #tmprslts2
Select yr, mo, sum(points) as points
from #tmprslts
group by yr ,mo

Update [workops].dbo.hist_datacheck
set new_acct = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

--Set Deleted Account Count
truncate table #tmprslts
truncate table #tmprslts2
insert into #tmprslts2
Select year(datedeleted)as yr ,month(datedeleted) as mo,count(tipnumber) as points
from affiliatdeleted
where acctstatus <> 'X'
group by year(datedeleted),month(datedeleted)

Update [workops].dbo.hist_datacheck
set del_acct = points 
from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo

drop table #tmprslts
drop table #tmprslts2


--Show results
--Diagnostics: Balances/Orphaned History/Excess TIPFirsts
Select
(Select count(tipnumber) from customer where runbalance - runredeemed <> runavailable) as Balance_Err,
(Select Count(*) from customer where tipnumber in (
 Select customer.Tipnumber from customer join history on customer.tipnumber = history.tipnumber
 Group by customer.tipnumber, runavailable Having Sum(points*ratio) <> runavailable)) as HistAvail_Err,
(Select count(h.tipnumber) from history h left outer join customer c on h.tipnumber = c.tipnumber where c.tipnumber is null) as Orphans,
(Select count(distinct left(tipnumber, 3)) from affiliat) as Affiliat_TIP,
(Select count(distinct left(tipnumber, 3)) from customer) as Customer_TIP,
(Select count(distinct left(tipnumber, 3)) from history) as History_TIP,
(Select count(distinct left(tipnumber, 3)) from monthly_statement_file) as Statement_TIP

/*
--Display any possible duplicated active history records
Select Tipnumber, trancode, histdate, points, Count(Tipnumber) as dup_count
from history
where trancode not like 'R%' and histdate >= (getdate()-60)
group by Tipnumber, trancode, histdate, acctid, description, points
having Count(tipnumber) > '1'
Order by histdate desc, tipnumber

Select Tipnumber, trancode, acctid, description, points, Count(Tipnumber) as dup_count
from history
where trancode not like 'R%' and histdate >= (getdate()-60)
group by Tipnumber, trancode, acctid, description, points
having Count(tipnumber) > '1'
Order by Tipnumber asc, acctid
*/

--Show historical point allocations
Select * from [workops].dbo.hist_datacheck order by yr, mo	