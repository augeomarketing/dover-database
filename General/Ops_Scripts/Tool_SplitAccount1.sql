--Script to Split out a TIPnumber, losing the associated history
declare @tip varchar(15)
set @tip = '525000000056854'

Select * from affiliat where tipnumber = @tip
Select * from customer where tipnumber = @tip
Select * from history where tipnumber = @tip order by histdate desc

Select lasttipnumberused from rewardsnow.dbo.dbprocessinfo where dbnumber = '525'
Select lasttipnumberused from client

Begin tran
--Adjust Affiliat records
insert into affiliatdeleted
Select tipnumber, accttype, dateadded, secid, acctid, 'C' as AcctStatus, accttypedesc, lastname, ytdearned, custid, '2009-10-27 00:00:00.000' as datedeleted
from affiliat where tipnumber = '525000000016105'
Update affiliat set tipnumber = '525000000056853', dateadded = '2009-10-27 00:00:00.000' where acctid = '4474520419597956'
Update affiliat set tipnumber = '525000000056854', dateadded = '2009-10-27 00:00:00.000' where acctid = '4474520419598210'
Delete from affiliat where tipnumber = '525000000016105'

--Adjust Customer records
Insert into customerdeleted
Select tipnumber, tipfirst, tiplast, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, 
	address4, city, state, zipcode, lastname, 'C' as Status, 'Closed[C] Account split' as statusdescription, homephone, workphone, 
	runbalance, runredeemed, runavailable, laststmtdate, nextstmtdate, dateadded, notes, combostmt, rewardsonline, employeeflag,
	businessflag, segmentcode, misc1, misc2, misc3, misc4, misc5, runbalancenew, runavaliablenew, '2009-10-27 00:00:00.000' as datedeleted
from customer where tipnumber = '525000000016105'
insert into customer
select '525000000056853' as tipnumber, '0' as runavailable, '0' as runbalance, '0' as runredeemed, laststmtdate,nextstmtdate, Status, 
	'2009-10-27 00:00:00.000' as dateadded, lastname, tipfirst, '000000056853' as tiplast, acctname1, null as acctname2,
	acctname3, acctname4, acctname5, acctname6, address1, address2, address3, address4, city, state, zipcode, statusdescription,
	homephone,workphone, businessflag, employeeflag, segmentcode, combostmt, rewardsonline, notes, bonusflag, misc1, misc2, misc3, misc4,
	misc5, runbalancenew, runavaliablenew
from customer where tipnumber = '525000000016105'
update customer set tipnumber = '525000000056854', dateadded = '2009-10-27 00:00:00.000', tiplast = '000000056854', acctname1 = acctname2,
	acctname2 = null, runbalance = '0', runredeemed = '0', runavailable = '0'
where tipnumber = '525000000016105'

--Adjust History records
insert into historydeleted
Select tipnumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, '2009-10-27 00:00:00.000' as datedeleted
from history where tipnumber = '525000000016105'
delete from history where tipnumber = '525000000016105'

--Adjust Begin_bal records
insert into beginning_balance_table select '525000000056853',0,0,0,0,0,0,0,0,0,0,0,0
insert into beginning_balance_table select '525000000056854',0,0,0,0,0,0,0,0,0,0,0,0
Delete from beginning_balance_table where tipnumber not in (select tipnumber from customer) and tipnumber not like '%999999%'

--Adjust One time bonus table
/*
insert into onetimebonuses
select '575000000004728' as tipnumber, 'BA' as trancode, null as acctid, '2008-09-30 00:00:00.000' as dateawarded
update onetimebonuses set tipnumber = '575000000004729' where tipnumber = '575000000003466'
*/

--Adjust LastTIP records
update client set lasttipnumberused = '525000000056854'
update rewardsnow.dbo.dbprocessinfo set lasttipnumberused = '525000000056854' where dbnumber = '525'

--re-Post to web
--Process adjustments through CLASS

--commit tran
--rollback tran

Select * from metavantework.dbo.packagecodes where tipfirst = '525'

