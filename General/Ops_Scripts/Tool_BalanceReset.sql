--Balance Checks
Select * from customer where runbalance - runredeemed <> runavailable
Select * from customer where tipnumber in
	(Select customer.Tipnumber from customer join history on customer.tipnumber = history.tipnumber
	 Group by customer.tipnumber, runavailable Having Sum(points*ratio) <> runavailable)

--The following script will reset Balance, Redeemed, and Available to match history

--Begin Tran
--Update customer 
--set runbalance = isnull((Select sum(points*ratio) from history where (left(trancode, 1) <> 'R' and trancode not in ('DR','IR')) and history.tipnumber = customer.tipnumber),0),
--	runredeemed = isnull((Select sum(points*ratio)*-1 from history where (left(trancode, 1) = 'R' or trancode in ('DR','IR')) and history.tipnumber = customer.tipnumber),0),
--	runavailable = isnull((Select sum(points*ratio) from history where history.tipnumber = customer.tipnumber),0)
--rollback tran
--commit tran

Select * from customer where runbalance - runredeemed <> runavailable
Select * from customer order by tipnumber
Select sum(points*ratio) from history where tipnumber = '51J000000000011'

--The following script will reset the beginning_balance_table balances to a specified month

Declare @date datetime
set @date = '09/01/2009'
--Begin Tran
--Update beginning_balance_table 
--set monthbeg9 = isnull((Select sum(points*ratio) from history where history.tipnumber = beginning_balance_table.tipnumber and histdate < @date),0)
--rollback tran
--commit tran

Select * from beginning_balance_table order by tipnumber
Select sum(points*ratio) from history where tipnumber = '51J000000000011'