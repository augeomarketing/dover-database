--SELECT * FROM REWARDSNOW.DBO.RPTLIABILITY

DECLARE @startdate NVARCHAR(25)
DECLARE @enddate NVARCHAR(25)
DECLARE @deletedate NVARCHAR(25)
DECLARE @nextmonthstartdate NVARCHAR(25)
DECLARE @dbname VARCHAR(50)
DECLARE @tipfirst VARCHAR(3)
SELECT clientname as clientname FROM Rewardsnow.dbo.dbprocessinfo where dbnumber = (Select top 1 left(tipnumber, 3) from customer)

--Change Dates for the Month Being Reviewed
--Start date is entered manually, all other dates calculated FROM startdate.
--CONVERTs are included so that generated dates can be printed to confirm accuracy.
--1/31/2008 RBM
SET @startdate = '2009-11-01 00:00:00:000'
SET @tipfirst = (Select top 1 left(tipnumber, 3) from customer)
--SET @tipfirst = '211'
SET @enddate = DATEADD(month, 1, @startdate)
SET @enddate = CONVERT(NVARCHAR(25),(DATEADD(MILLISECOND, -3, @enddate)),121)
SET @deletedate = DATEADD(month, 1, @startdate)
SET @deletedate = CONVERT(NVARCHAR(25),(DATEADD(DAY, -1, @deletedate)),121)
--Used to obtain EOMBalance for the Month Being Processed
--SET @nextmonthstartdate = '2007-12-01 00:00:00:000'
SET @nextmonthstartdate = CONVERT(NVARCHAR(25),(DATEADD(MONTH, 1, @startdate)),121) -- RDT
--SELECT @dbname = DBNamePatton FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst


--Display relevant dates to confirm appropriate dates are being used in calculations
--1/31/2008 RBM
SELECT
  @startdate as Start_Date,
  @enddate as End_Date,
  @deletedate as Delete_Date,
  @nextmonthstartdate as Next_Month_Start_Date

--SELECT The Previous End Of Month Balance                        ***SUMMARY - BEGINNING OUTSTANDING POINTS
--This should match the Beginning Points Outstanding for this Month FROM the Liability Report
SELECT 
  COALESCE( (SELECT SUM (points*ratio) 
              FROM history 
              WHERE histdate < @startdate), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted 
              WHERE (histdate < @startdate) 
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted 
              WHERE (histdate < @startdate) 
                AND (datedeleted > @enddate)),0)
AS Beginning_Points_Outstanding

--SELECT the Total Points earned During the Currents Month�s Run  ***SUMMARY - PRODUCT TOTAL POINTS
--Modified 1/16/2007 RBM   Will now calculate with null values in overage.  Assumes overage value = 0
SELECT
  COALESCE( (SELECT SUM((points + COALESCE(overage, 0)) * ratio) 
              FROM history 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (left(trancode, 1) = '6')),0)
+
  COALESCE( (SELECT SUM((points + COALESCE(overage, 0)) * ratio) 
              FROM historydeleted 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (left(trancode, 1) = '6') 
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM((points + COALESCE(overage, 0)) * ratio) 
              FROM historydeleted 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (left(trancode, 1) = '6') 
                AND (datedeleted > @enddate)), 0)
AS Product_Total_Points
--SELECT the Total estatement Points awarded during the Currents Month�s Run  ***SUMMARY - BONUS POINTS
SELECT
  COALESCE( (SELECT SUM(points * ratio)  
              FROM history 
              WHERE (histdate BETWEEN @startdate AND @enddate)  
                AND (LEFT(trancode,1 ) IN ('0', 'B', 'F', 'G', 'N'))
                AND (trancode <> 'BE')), 0)
+
  COALESCE ( (SELECT SUM(points*ratio) 
                FROM historydeleted 
                WHERE (histdate BETWEEN @startdate AND @enddate) 
                  AND (LEFT(trancode,1 ) IN ('0', 'B', 'F', 'G', 'N'))
                  AND (trancode <> 'BE') 
                  AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode,1 ) IN ('0', 'B', 'F', 'G', 'N'))
                AND (trancode <> 'BE') 
                AND(datedeleted > @enddate)), 0)
as Bonus_Points_Added
--SELECT the Total estatement Points awarded during the Currents Month�s Run  ***SUMMARY - REWARDSNOW BONUS POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio) 
              FROM history 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode = 'BE')),0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM  historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode = 'BE')
                AND (datedeleted BETWEEN @startdate AND @enddate)),0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode = 'BE')
                AND (datedeleted > @enddate)),0)
as Rewards_Bonus_Added
--SELECT the Total Points with an Increase During the Currents Month�s Run    ***SUMMARY - ADJ INCREASE POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio) 
              FROM history 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('IE','TP'))), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('IE','TP'))
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted 
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('IE','TP'))
                AND (datedeleted > @enddate)), 0)
as Increase_Earned
--SELECT the Total Points with an Increase During the Currents Month�s Run    ***SUMMARY - ADJ DECREASE POINTS
/* 6/18/2010: RBM: Added new Trancode (TD) to where clause logic */
SELECT
 COALESCE( (SELECT SUM(points*ratio) 
              FROM history
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('DE','IR','TD'))), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('DE','IR','TD'))
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate)
                AND (trancode in ('DE','IR','TD'))
                AND (datedeleted > @enddate)), 0)
as Decrease_Earned
--SELECT the Total Points Expired During the Currents Month�s Run ***SUMMARY - EXPIRED POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio)
              FROM history
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode,1) = 'X')), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode,1) = 'X')
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode,1) = 'X')
                AND (datedeleted > @enddate)), 0)
as Expired_Points
--SELECT the Total Points Redeemed During the Currents Month�s Run      ***SUMMARY - REDEEMED POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio)
              FROM history
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND LEFT(trancode,1) = 'R'), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode,1) = 'R')
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode,1) = 'R')
                AND (datedeleted > @enddate)), 0)
as Redemptions
--SELECT the Total redemption Points Returned During the Currents Month�s Run      ***SUMMARY - REDEEMED_RETURNED POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio)
              FROM history
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode IN ('DZ','DR'))), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('DZ','DR'))
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio) 
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (trancode in ('DZ','DR'))
                AND (datedeleted > @enddate)), 0)
as Redemption_Returns
--SELECT the Total Points Returned During the Currents Month�s Run      ***SUMMARY - RETURNED POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio)
              FROM history
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode, 1) = '3')), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode, 1) = '3')
                AND (datedeleted BETWEEN @startdate AND @enddate)), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM  historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate) 
                AND (LEFT(trancode, 1) = '3')
                AND (datedeleted > @enddate)),0)
as [Returns]
--SELECT the Total Points Over Max Allowed During the Currents Month�s Run    ***SUMMARY - OVERAGE POINTS
SELECT
  COALESCE( (SELECT SUM(overage)
              FROM  history
              WHERE (histdate BETWEEN @startdate AND @enddate)),0)
+
  COALESCE( (SELECT SUM(overage)
              FROM  historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate)
                AND (datedeleted BETWEEN @startdate AND @enddate)),0)
+
  COALESCE( (SELECT SUM(overage)
              FROM  historydeleted
              WHERE (histdate BETWEEN @startdate AND @enddate)
                AND (datedeleted > @enddate)),0)
as Overage
--SELECT the Total Points Purged During the Currents Month�s Run  ***SUMMARY - PURGED POINTS
SELECT
  COALESCE( (SELECT SUM(points*ratio)
              FROM  historydeleted
              WHERE (histdate < @enddate)
                AND (datedeleted BETWEEN @startdate AND @enddate)),0)
as Purged_Points
--SELECT The Current End Of Month Balance                         ***SUMMARY - NET OUTSTANDING POINTS
--This should match the Net Points Outstanding for this Month FROM the Liability Report
SELECT
 COALESCE( (SELECT SUM(points*ratio)
              FROM  history
              WHERE (histdate < @nextmonthstartdate)),0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM  historydeleted
              WHERE (histdate < @nextmonthstartdate)
                AND (datedeleted > @enddate)),0)
as Net_Points_Outstanding
--SELECT The Current Redeemable Points Outstanding                ***SUMMARY - REDEEMABLE OUTSTANDING POINTS
--This should match the Redeemable Points Outstanding for this Month FROM the Liability Report
--Added 1/4/2007 - RBM
SELECT
  COALESCE( (SELECT SUM(points*ratio)
              FROM  history
              WHERE histdate < @enddate
                AND tipnumber IN (SELECT tipnumber 
                                    FROM history 
                                    WHERE histdate <= @enddate 
                                      GROUP BY tipnumber 
                                      HAVING SUM(points*ratio) >= (SELECT COALESCE(minRedeemNeeded, 750) FROM RewardsNOW.dbo.dbprocessinfo where dbnumber = @tipfirst))), 0)
+
  COALESCE( (SELECT SUM(points*ratio)
              FROM  historydeleted
              WHERE (histdate < @enddate)
                AND (datedeleted > @enddate)
                AND tipnumber IN (SELECT tipnumber 
                                    FROM historydeleted 
                                    WHERE histdate <= @enddate 
                                    GROUP BY tipnumber 
                                    HAVING SUM(points*ratio) >= (SELECT COALESCE(minRedeemNeeded, 750) FROM RewardsNOW.dbo.dbprocessinfo where dbnumber = @tipfirst))),0)
as Redeemable_Points_Outstanding
--SELECT The Total number of Debit/Credit accounts                ***SUMMARY - ACCOUNT_COUNTS
--Added 1/4/2007 - RBM
SELECT accttype, COUNT(DISTINCT acctid) as AcctCount 
  FROM affiliat 
  WHERE DATEADDed < @nextmonthstartdate 
  GROUP BY accttype

--Deleted accounts - This number should be added to the active counts to determine a historic value
SELECT accttype as accttype_Del, COUNT(DISTINCT acctid) as acctcount_Del
  FROM affiliatdeleted 
  WHERE DATEADDed < @nextmonthstartdate 
    AND datedeleted >= @enddate
  GROUP BY accttype

/*
--SELECT The Total number of eligible participants based on the Affiliat table            ***SUMMARY - ELIGIBLE_AFFILIAT
--Added 1/7/2007 - RBM
SELECT
(     SELECT      count (distinct tipnumber)
            FROM  affiliat
            WHERE DATEADDed < @nextmonthstartdate
              AND       tipnumber in      (SELECT tipnumber 
                               FROM history 
                               WHERE histdate <= @enddate 
                               group by tipnumber 
                               having SUM(points*ratio)>=(SELECT isnull(minRedeemNeeded, 750) FROM client)))
+
(     SELECT      count (distinct tipnumber)
            FROM  affiliatdeleted
            WHERE (DATEADDed < @nextmonthstartdate)
              AND       tipnumber not in (SELECT distinct tipnumber FROM affiliat)
              AND (datedeleted >= @enddate)
              AND       tipnumber in      (SELECT tipnumber 
                               FROM historydeleted 
                               WHERE histdate <= @enddate 
                               group by tipnumber 
                               having SUM(points*ratio)>=(SELECT isnull(minRedeemNeeded, 750) FROM client)))
as Eligible_Affiliat
*/
--SELECT The Total number of eligible participants based on the Customer table            ***SUMMARY - ELIGIBLE_CUSTOMER
--Added 1/7/2007 - RBM
SELECT
  COALESCE( (SELECT COUNT (DISTINCT tipnumber)
              FROM  customer
              WHERE DATEADDed < @nextmonthstartdate
                AND tipnumber IN (SELECT tipnumber 
                                    FROM history 
                                    WHERE histdate <= @enddate 
                                    GROUP BY tipnumber 
                                    HAVING SUM(points*ratio) >= (SELECT COALESCE(minRedeemNeeded, 750) FROM RewardsNOW.dbo.dbprocessinfo where dbnumber = @tipfirst))), 0)
+
  COALESCE( (SELECT COUNT(DISTINCT tipnumber)
              FROM  customerdeleted
              WHERE (DATEADDed < @nextmonthstartdate)
                AND  tipnumber NOT IN (SELECT DISTINCT tipnumber FROM customer)
			          AND  LEFT(statusdescription, 3) <> 'Pri'
                AND (datedeleted >= @enddate)
                AND tipnumber IN (SELECT tipnumber 
                                    FROM historydeleted 
                                    WHERE histdate <= @enddate 
                                    GROUP BY tipnumber 
                                    HAVING SUM(points*ratio) >= (SELECT COALESCE(minRedeemNeeded, 750) FROM RewardsNOW.dbo.dbprocessinfo where dbnumber = @tipfirst))), 0)
as Eligible_Customer
/*
--SELECT The Total number of participants based on the Affiliat table         ***SUMMARY - PARTICIPANTS_AFFILIAT
--Added 1/4/2007 - RBM
SELECT
(     SELECT      count (distinct tipnumber)
            FROM  affiliat
            WHERE DATEADDed < @nextmonthstartdate)
+
(     SELECT      count (distinct tipnumber)
            FROM  affiliatdeleted
            WHERE (DATEADDed < @nextmonthstartdate)
              AND       tipnumber not in (SELECT distinct tipnumber FROM affiliat)
              AND (datedeleted >= @enddate))
as Participants_Affiliat
*/
--SELECT The Total number of participants based on the Customer table         ***SUMMARY - PARTICIPANTS_CUSTOMER
--Added 1/4/2007 - RBM
SELECT
  COALESCE( (SELECT COUNT(DISTINCT tipnumber)
              FROM  customer
              WHERE DATEADDed < @nextmonthstartdate
			          AND status <> 'X'), 0)
+
  COALESCE( (SELECT COUNT(DISTINCT tipnumber)
              FROM  customerdeleted
              WHERE (DATEADDed < @nextmonthstartdate)
			          AND status <> 'X'
                AND tipnumber NOT IN (SELECT tipnumber FROM customer)
			          AND LEFT(statusdescription, 3) <> 'Pri'
                AND (datedeleted >= @enddate)), 0)
as Participants_Customer


--SELECT The total of this Month�s Points FROM HistoryDeleted           ***SUMMARY - DELETED POINTS
SELECT SUM(points*ratio) AS Total_Deleted_Points 
  FROM  historydeleted 
  WHERE histdate BETWEEN @startdate AND @enddate
    AND datedeleted = @deletedate
--SELECT This Months Detail FROM History Deleted                  ***DETAIL - DELETED POINTS

SELECT TRANCODE, SUM(points*ratio) as Deleted_Points 
  FROM  historydeleted 
  WHERE histdate BETWEEN @startdate AND @enddate
    AND datedeleted = @deletedate
    GROUP BY TRANCODE
    ORDER BY TRANCODE

--SELECT The transaction Detail Totals for the Current Month            ***DETAIL - EARNED/USED POINTS
SELECT TRANCODE, SUM(points*ratio) as EarnedUsed_Points 
  FROM  history 
  WHERE histdate BETWEEN @startdate AND @enddate
  GROUP BY TRANCODE
  ORDER BY TRANCODE

-- RDT 12/27/08
--SELECT count(tipnumber) as Participants FROM customer
--SELECT count (distinct tipnumber) as Participants FROM affiliat WHERE DATEADDed < @nextmonthstartdate
--SELECT accttype, count(accttype) FROM affiliat WHERE DATEADDed < @nextmonthstartdate group by accttype
