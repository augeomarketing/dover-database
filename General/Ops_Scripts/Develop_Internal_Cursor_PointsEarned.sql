--Drop table [WorkOps].dbo.Hist_EarnedPoints
--Create table [WorkOps].dbo.Hist_EarnedPoints (DBName varchar(100) not null, Point_Year int, Point_Month int,Points_Earned bigint)
truncate table [WorkOps].dbo.Hist_EarnedPoints
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)
--Drop table #tempresult1
Create table #tmpresult1 (Point_Year int, Point_Month int,Points_Earned bigint)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
		Truncate table #tmpresult1
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into #tmpresult1
		 Select year(histdate) as Point_Year, month(histdate) as Point_Month, Sum(points*ratio) Points_Earned
		 from ['+@DBName+'].dbo.history
		 where left(trancode, 1) <> ''R'' and trancode not in (''IR'',''DR'')-- and histdate >= ''1/1/2007''
		 group by year(histdate), month(histdate)
			union all
		 Select year(histdate) as Point_Year, month(histdate) as Point_Month, Sum(points*ratio) Points_Earned
		 from ['+@DBName+'].dbo.historydeleted
		 where left(trancode, 1) <> ''R'' and trancode not in (''IR'',''DR'')-- and histdate >= ''1/1/2007''
		 group by year(histdate), month(histdate)'
	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Hist_EarnedPoints
		 Select '''+@DBName+''' as DBName, Point_Year, Point_Month, sum(Points_Earned)
		 from #tmpresult1
		 group by Point_Year, Point_Month
		 order by Point_Year, Point_Month'
	  EXECUTE sp_executesql @SqlCmd

	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName


/*
Select *
From [WorkOps].dbo.Hist_EarnedPoints
order by DBName

Select Count(*)
from [WorkOps].dbo.Hist_EarnedPoints
where dbname <> 'BQTest' and customer_count > '0'

Select Sum(customer_count)
from [WorkOps].dbo.Hist_EarnedPoints
where dbname not like '9%'
 and dbname not in ('594HarrisConsumer', 'testclass990')

Select Point_Year,Point_Month, Sum(Points_Earned) as Points_Earned
from [WorkOps].dbo.Hist_EarnedPoints
where dbname not in ('BQTest','TESTCLASS990') and left(dbname, 1) <> '9'
group by Point_Year,Point_Month
order by Point_Year,Point_Month
*/