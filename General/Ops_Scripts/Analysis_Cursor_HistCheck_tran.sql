--Drop table [WorkOps].dbo.Hist_RecordCount_tran
--Create table [WorkOps].dbo.Hist_RecordCount_tran (DBName varchar(100) not null, trancode varchar(2), date_year int, Hist_Count int null, Hist_Sum bigint null)
truncate table [WorkOps].dbo.Hist_RecordCount_tran
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Hist_RecordCount_tran
		 Select '''+@DBName+''' as DBName, trancode, year(histdate) as date_year, count(tipnumber) as Hist_Count, sum(points*ratio) as Hist_Sum
		 from ['+@DBName+'].dbo.History
		 group by trancode, year(histdate)'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Hist_RecordCount_tran
order by DBName

Select trancode, sum(hist_count) as hist_count
from [WorkOps].dbo.Hist_RecordCount_tran
where date_year = '2009' and trancode in ('63')
group by trancode
