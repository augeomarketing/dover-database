--Returns monthly statistical data on redemption levels based on Onlinehistorywork records
--This should be run against the Patton DB for greater accuracy

Select	Year(histdate)	as [Year],
	month(histdate)	as [Month],
	Count(points)	as Count_Redeem,
	Avg(points)	as Average_Redeem,
	Sum(points)	as Total_Redeem,
	Max(points)	as Max_Redeem,
	Min(points)	as Min_Redeem
From	onlinehistorywork.dbo.onlhistory
where	points > '0' and left(tipnumber,1 ) <> '9' and tipnumber not like '%999999%'
group by	Year(Histdate), month(histdate)
order by	Year(Histdate), month(histdate)

Select top 5
	Year(histdate)	as [Year],
	month(histdate)	as [Month],
	Count(points)	as Count_Redeem,
	Avg(points)	as Average_Redeem,
	Sum(points)	as Total_Redeem,
	Max(points)	as Max_Redeem,
	Min(points)	as Min_Redeem
From	onlinehistorywork.dbo.onlhistory
where	points > '0' and left(tipnumber,1 ) <> '9' and tipnumber not like '%999999%'
group by	Year(Histdate), month(histdate)
order by	Count(points) desc

Select top 5
	Year(histdate)	as [Year],
	month(histdate)	as [Month],
	Count(points)	as Count_Redeem,
	Avg(points)	as Average_Redeem,
	Sum(points)	as Total_Redeem,
	Max(points)	as Max_Redeem,
	Min(points)	as Min_Redeem
From	onlinehistorywork.dbo.onlhistory
where	points > '0' and left(tipnumber,1 ) <> '9' and tipnumber not like '%999999%'
group by	Year(Histdate), month(histdate)
order by	Sum(points) desc