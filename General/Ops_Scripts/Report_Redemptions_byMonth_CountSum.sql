--Report Script:  By month report on all redemptions placed
Declare @date datetime
set		@date = '12/1/2009'

--First script:  Summary report: By month/trancode show Count/Sum of redemptions
Select	month(m.histdate) as Mo, Year(m.histdate) as Yr, m.trancode, t.Description,
		count(m.points) as Transaction_Count, Sum(m.catalogqty) as Redemption_Count, SUM(m.Points*m.CatalogQty) as Points
from	fullfillment.dbo.Main m join RewardsNOW.dbo.TranType t	on m.TranCode = t.trancode
			join fullfillment.dbo.SubRouting r					on m.Routing = r.Routing
			join RewardsNOW.dbo.dbprocessinfo dbpi				on m.TipFirst = dbpi.DBNumber
where	HistDate >= @date
group by month(m.histdate), Year(m.histdate), m.trancode, t.description
order by Year(m.histdate), month(m.histdate), m.trancode, t.description

--Second script:  Detail report: By month/Client/trancode/Routing show Count/Sum of redemptions
Select	month(m.histdate) as Mo, Year(m.histdate) as Yr, m.Tipfirst, dbpi.ClientName, m.TranCode, t.Description, r.RoutingName,
		count(m.points) as Transaction_Count, Sum(m.catalogqty) as Redemption_Count, SUM(m.Points*m.CatalogQty) as Points
from	fullfillment.dbo.Main m join RewardsNOW.dbo.TranType t	on m.TranCode = t.trancode
			join fullfillment.dbo.SubRouting r					on m.Routing = r.Routing
			join RewardsNOW.dbo.dbprocessinfo dbpi				on m.TipFirst = dbpi.DBNumber
where	HistDate >= @date
group by month(m.histdate), Year(m.histdate), m.Tipfirst, dbpi.ClientName, m.TranCode, t.description, r.RoutingName
order by Year(m.histdate), month(m.histdate), m.Tipfirst, dbpi.ClientName, m.TranCode, t.description,  r.RoutingName
