--Drop table [WorkOps].dbo.Cust_RecordCount
--Create table [WorkOps].dbo.Cust_RecordCount (DBName varchar(100) not null, customer_count int null)
Truncate table [WorkOps].dbo.Cust_RecordCount

Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where left (dbnumber,1)<> '9'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @DBNameRN varchar(100)
Declare @TIPfirst varchar (3)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
  	  set @dbnamern = (Select dbnameNEXL from rewardsnow.dbo.dbprocessinfo where dbnamePatton = @dbname)
	  set @tipfirst = (Select dbnumber from rewardsnow.dbo.dbprocessinfo where dbnamePatton = @dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_RecordCount
		 Select '''+@DBName+''' as DBName, count(*) as customer_count
		 from [rn1].['+@DBNameRN+'].dbo.[1security]
		 where password is not null and left(tipnumber,3) = '''+@tipfirst+''' and tipnumber not like ''%999999%'''
--	Print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_RecordCount
order by DBName

Select sum(customer_count)
From [WorkOps].dbo.Cust_RecordCount
where dbname not like '9%'
 and dbname not in ('testclass990')