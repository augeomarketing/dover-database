Declare	@NEWTIP varchar(15)
Declare @OLDTIP varchar(15)
Declare @ACCTID varchar(16)

Set @NEWTIP =  cast((Select lasttipnumberused from client) as bigint)+1
--print @NEWTIP
Set @OLDTIP = '<<TIP NUMBER>>'
Set @ACCTID = '<<ACCTID>>'

Update affiliat
set tipnumber = @NEWTIP
where acctid = @ACCTID

Update history
set tipnumber = @NEWTIP
where acctid = @ACCTID

Insert into customer
  (Tipnumber, Runbalance, runredeemed, runavailable, dateadded, acctname1, acctname2)
Select 
	@NEWTIP,
	isnull((Select sum(Points*ratio) from history where tipnumber = @NEWTIP and trancode not like 'R%'),0),
	isnull((Select sum(Points*ratio) from history where tipnumber = @NEWTIP and trancode like 'R%'),0),
	isnull((Select sum(Points*ratio) from history where tipnumber = @NEWTIP),0),
	(Select Min(Dateadded) from affiliat where tipnumber = @NEWTIP),
	(Select acctname2 from customer where tipnumber = @OLDTIP), (Select acctname3 from customer where tipnumber = @OLDTIP)
--	(Select acctname3 from customer where tipnumber = @OLDTIP), (Select acctname4 from customer where tipnumber = @OLDTIP)

Update customer
Set	runbalance = isnull((Select sum(Points*ratio) from history where tipnumber = @OLDTIP and trancode not like 'R%'),0),
	runredeemed = isnull((Select sum(Points*ratio) from history where tipnumber = @OLDTIP and trancode like 'R%'),0),
	runavailable = isnull((Select sum(Points*ratio) from history where tipnumber = @OLDTIP),0),
	acctname2 = null, acctname3 = null
--	acctname3 = null, acctname4 = null
Where tipnumber = @OLDTIP

Update beginning_Balance_table
set  monthbeg3 = isnull((select runavailable from customer where tipnumber = @OLDTIP), 0)
where tipnumber = @OLDTIP

insert into beginning_Balance_table
(tipnumber,monthbeg1,monthbeg2,monthbeg3,monthbeg4,monthbeg5,monthbeg6,monthbeg7,monthbeg8,monthbeg9,monthbeg10,monthbeg11,monthbeg12)
Select	@NEWTIP, '0','0',
	isnull((Select runavailable from customer where tipnumber = @NEWTIP), 0),
	'0','0','0','0','0','0','0','0','0'

Update Client
Set lasttipnumberused = @NEWTIP
