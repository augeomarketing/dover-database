--Drop table [WorkOps].dbo.Pointaccrual_bymonth
--Create table [workops].dbo.Pointaccrual_bymonth (DBName varchar(100), yr int, mo int, Earned bigint, Redeemed bigint, Expired bigint, Purged bigint, New_Cust int, Del_Cust int, New_Acct int, Del_Acct int)
Truncate Table [workops].dbo.Pointaccrual_bymonth

Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where left(dbnumber,1) <> '9'

Declare @TIP varchar (3)
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @TIP = (Select dbnumber from rewardsnow.dbo.dbprocessinfo where dbnamepatton = @dbname)
	  
	  EXECUTE [workops].dbo.sppointaccrual_bymonth @TIP

	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Pointaccrual_bymonth
		 Select '''+@DBName+''' as DBName, yr, mo, Earned, Redeemed, Expired, Purged, New_Cust, Del_Cust, New_Acct, Del_Acct
		 from [workops].dbo.hist_datacheck2'
	  EXECUTE sp_executesql @SqlCmd

	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Pointaccrual_bymonth
where yr = '2009' and mo = '4' and purged <> 0
order by purged

Select distinct dbname
From [WorkOps].dbo.Pointaccrual_bymonth
order by dbname

Select	yr, mo, count(dbname) as FI_Count, Sum(Earned) as Earned, sum(Redeemed) as Redeemed, sum(Expired) as Expired,
		Sum(Purged) as Purged, Sum(New_Cust) as New_Cust, Sum(Del_Cust) as Del_Cust, Sum(New_Acct) as New_Acct,
		Sum(Del_Acct) as Del_Acct
from [WorkOps].dbo.Pointaccrual_bymonth
group by yr, mo
order by yr, mo