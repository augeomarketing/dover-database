--Breakage Report --> This report gives a breakout by FI of the status of all points awarded
--This is broken out into points awarded, redeemed and forfeited.  Also displays points still available
--Developed 4/15/2008 RBM

--Drop table [WorkOps].dbo.Hist_Breakage
--Create table [WorkOps].dbo.Hist_Breakage (DBName varchar(100) not null, First_Entry datetime, Points_Awarded bigint null, Points_Redeemed bigint null, Points_Expired bigint null, Points_Closed bigint null, Points_Forfeit bigint null, Points_Available bigint null)
Truncate Table [WorkOps].dbo.Hist_Breakage
--Cursor through DBProcessinfo and get Breakage Totals based on History
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
	
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Hist_Breakage
		 Select
			'''+@DBName+''' as DBName,
			CASE
			 WHEN (Select Min(histdate) from ['+@DBName+'].dbo.History) > (Select Min(histdate) from ['+@DBName+'].dbo.Historydeleted) 
				THEN (Select Min(histdate) from ['+@DBName+'].dbo.Historydeleted)
    			 ELSE (Select Min(histdate) from ['+@DBName+'].dbo.History)
  			END as First_Entry,
			(isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.History where left(trancode, 1) <> ''R'' and trancode not in (''IR'', ''DR'',''XF'',''XP'')),0) +
			isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.Historydeleted where left(trancode, 1) <> ''R'' and trancode not in (''IR'', ''DR'',''XF'',''XP'')),0))
			as Points_Awarded,
			((isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.History where left(trancode, 1) = ''R'' or trancode in (''IR'', ''DR'')),0) +
			isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.Historydeleted where left(trancode, 1) = ''R'' or trancode in (''IR'', ''DR'')),0))*-1)
			as Points_Redeemed,
			((isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.History where trancode in (''XF'', ''XP'')),0) +
			isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.Historydeleted where trancode in (''XF'', ''XP'')),0))*-1)
			as Points_Expired,
			(isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.Historydeleted),0))
			as Points_Closed,
			((isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.History where trancode in (''XF'', ''XP'')),0) +
			isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.Historydeleted where trancode in (''XF'', ''XP'')),0))*-1) +
			isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.Historydeleted),0)
			as Points_Forfeit,
			(isnull((Select Sum(points*ratio) From ['+@DBName+'].dbo.History),0))
			as Points_Available'
--Print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Hist_Breakage
order by DBName


Select *
From [WorkOps].dbo.Hist_Breakage
order by first_entry

Select	dbname,
		first_entry,
		cast(cast(points_redeemed as decimal(18,3))/cast(points_awarded as decimal(18,3)) as decimal(4,3)) as Known_RedeemRate,
		cast(cast(points_forfeit as decimal(18,3))/cast(points_awarded as decimal(18,3)) as decimal(4,3)) as Known_ForfeitRate,
		cast(cast(points_redeemed as decimal(18,3))/cast((points_awarded-points_available) as decimal(18,3)) as decimal(4,3)) as Adj_RedeemRate,
		cast(cast(points_forfeit as decimal(18,3))/cast((points_awarded-points_available) as decimal(18,3)) as decimal(4,3)) as Adj_ForfeitRate,
		1-cast(cast(points_available as decimal(18,3))/cast(points_awarded as decimal(18,3)) as decimal(4,3)) as Committed_PointRate,
		cast(cast(points_available as decimal(18,3))/cast(points_awarded as decimal(18,3)) as decimal(4,3)) as Active_PointRate
From [WorkOps].dbo.Hist_Breakage
where	points_awarded-points_available <> 0
		and 1-cast(cast(points_available as decimal(18,3))/cast(points_awarded as decimal(18,3)) as decimal(4,3)) >= 0.5
order by adj_forfeitrate