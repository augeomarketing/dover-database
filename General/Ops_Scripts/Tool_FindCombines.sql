--This script will return a list of all TIPNumbers combined within a given daterange
--Script will only work for those FIs putting the message 'Pri combined to ###...' in the StatusDescription field

Select	Tipnumber, 
	rtrim(Acctname1) as Name1,
	rtrim(Acctname2) as Name2,
	rtrim(StatusDescription) as StatusDescription,
	Datedeleted
from	customerdeleted 
where 	statusdescription like '%combined%'
  and 	datedeleted >= '4/28/2008'
--and	datedeleted <  ''
order by Right(rtrim(statusdescription), 15), statusdescription, tipnumber

Select * from customer where tipnumber in 
(Select Tipnumber from customerdeleted 
where statusdescription like '%combined%' and datedeleted >= '4/28/2008')

Select * from affiliat where tipnumber in 
(Select Tipnumber from customerdeleted 
where statusdescription like '%combined%' and datedeleted >= '4/28/2008')

Select * from history where tipnumber in 
(Select Tipnumber from customerdeleted 
where statusdescription like '%combined%' and datedeleted >= '4/28/2008')