Drop table [WorkOps].dbo.Cust_RecordCount
Create table [WorkOps].dbo.Cust_RecordCount (DBName varchar(100) not null, customer_Count int null)
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
--where DBNameNEXL = 'NewTNB' or DBNameNEXL = 'Indiana'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_RecordCount
		 Select '''+@DBName+''' as DBName, 
			(select isnull(count (distinct tipnumber),0) from ['+@DBName+'].dbo.customer where dateadded < ''1/1/2006'')
		+
			(select isnull(count (distinct tipnumber),0) from  ['+@DBName+'].dbo.customerdeleted 
				where (dateadded < ''1/1/2008'') and  tipnumber not in (Select distinct tipnumber from ['+@DBName+'].dbo.customer) and  statusdescription not like ''Pri%'' and (datedeleted >= ''1/1/2006''))
'--		 from ['+@DBName+'].dbo.customer'
--print @SqlCmd
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select distinct *
From [WorkOps].dbo.Cust_RecordCount
order by DBName

Select Count(*)
from [WorkOps].dbo.Cust_RecordCount
where dbname <> 'BQTest' and customer_count > '0'

Select Sum(customer_count)
from [WorkOps].dbo.Cust_RecordCount
where dbname not like '9%'
 and dbname not in ('594HarrisConsumer', 'testclass990')