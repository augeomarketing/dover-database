--Create storage table for search
--Drop table [WorkOps].dbo.Cust_BalanceError
--Create table [WorkOps].dbo.Cust_BalanceError (DBName varchar(100) not null, Hist_Avail_Errors int null, Bal_Avail_Errors int null)
Truncate table [WorkOps].dbo.Cust_BalanceError

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_BalanceError
		 Select '''+@DBName+''' as DBName, 
			(Select count(*)
			 From ['+@DBName+'].dbo.customer
		 	 where tipnumber in (
				Select ['+@DBName+'].dbo.customer.Tipnumber from ['+@DBName+'].dbo.customer join ['+@DBName+'].dbo.history on ['+@DBName+'].dbo.customer.tipnumber = ['+@DBName+'].dbo.history.tipnumber
				Group by ['+@DBName+'].dbo.customer.tipnumber, runavailable
				Having Sum(points*ratio) <> runavailable)) as Hist_Avail_Errors,
			(Select count(*)
			 From ['+@DBName+'].dbo.customer
		 	 where runbalance-runredeemed <> runavailable) as Bal_Avail_Errors'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From WorkOps.dbo.Cust_BalanceError
where Hist_Avail_Errors > 0
   or Bal_Avail_Errors > 0
order by DBName

--Select * from customer where tipnumber in (
----Select Count(*) from customer where tipnumber in (
--Select customer.Tipnumber from customer join history on customer.tipnumber = history.tipnumber
--Group by customer.tipnumber, runavailable
--Having Sum(points*ratio) <> runavailable)