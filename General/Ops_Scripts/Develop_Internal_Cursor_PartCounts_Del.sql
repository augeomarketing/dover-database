--Drop table [WorkOps].dbo.Cust_PartCounts_Del
--Create table [WorkOps].dbo.Cust_PartCounts_Del (DBName varchar(100) not null, Part_Year int, Part_Month int,Part_Count bigint)
truncate table [WorkOps].dbo.Cust_PartCounts_Del
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_PartCounts_Del
		 Select '''+@DBName+''' as DBName, year(datedeleted) as Part_Year, month(datedeleted) as Part_Month, Count(tipnumber) as Part_Count
		 from ['+@DBName+'].dbo.customerdeleted
		 group by year(datedeleted), month(datedeleted)'
	  EXECUTE sp_executesql @SqlCmd

	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

/*
Select *
From [WorkOps].dbo.Cust_PartCounts_Del
order by DBName

Select Part_Year,Part_Month, Sum(Part_Count) as Part_Count
from [WorkOps].dbo.Cust_PartCounts_Del
where dbname not in ('BQTest','TESTCLASS990') and left(dbname, 1) <> '9'
group by Part_Year,Part_Month
order by Part_Year,Part_Month
*/
