Declare @month int
Declare @year int
set @month = '9'
set @year = '2009'

Select
	t.description		as Redemption_Type,
	Count(m.points)		as Count_Redeem,
	Sum(m.points)		as Points_Redeem,
	Avg(m.points)		as Average_Redeem
From	Fullfillment.dbo.main m
			left outer join rewardsnow.dbo.trantype t on m.trancode = t.trancode
where	Month(histdate) = @month and year(histdate) = @year and left(m.trancode,1) = 'R'
group by	t.description
order by	t.description

Select 
	'VISA Gift Cards'	as Redemption_Type,
	Count(points)		as Count_Redeem,
	Sum(points)		as Points_Redeem,
	Avg(points)		as Average_Redeem
from Fullfillment.dbo.main 
where routing = '2' and trancode = 'RC' and (itemnumber like '%VISA%' or catalogdesc like ('%VISA%')) 
	and Month(histdate) = @month and year(histdate) = @year
	
Select count(*) as Development_Clients from rewardsnow.dbo.dbprocessinfo where sid_fiprodstatus_statuscode = 'V' and dbnamepatton in (select name from master.dbo.sysdatabases) and left(dbnumber,1) <> '9'
Select count(*) as Active_Clients from rewardsnow.dbo.dbprocessinfo where sid_fiprodstatus_statuscode = 'P' and dbnamepatton in (select name from master.dbo.sysdatabases) and left(dbnumber,1) <> '9'
Select count(*) as Inactive_Clients from rewardsnow.dbo.dbprocessinfo where sid_fiprodstatus_statuscode = 'I' and dbnamepatton in (select name from master.dbo.sysdatabases) and left(dbnumber,1) <> '9'
