--Drop table [WorkOps].dbo.Cust_NegAvail
--Create table [WorkOps].dbo.Cust_NegAvail (DBName varchar(100) not null,LessThan_0 int null,LessThan_100 int null,LessThan_1000 int null,LessThan_5000 int null,LessThan_10000 int null)
Truncate Table [WorkOps].dbo.Cust_NegAvail
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_NegAvail
		 Select '''+@DBName+''' as DBName, 
			(Select count(tipnumber) from ['+@DBName+'].dbo.customer where runavailable < ''0'') as LessThan_0,
			(Select count(tipnumber) from ['+@DBName+'].dbo.customer where runavailable < ''-100'') as LessThan_100,
			(Select count(tipnumber) from ['+@DBName+'].dbo.customer where runavailable < ''-1000'') as LessThan_1000,
			(Select count(tipnumber) from ['+@DBName+'].dbo.customer where runavailable < ''-5000'') as LessThan_5000,
			(Select count(tipnumber) from ['+@DBName+'].dbo.customer where runavailable < ''-10000'') as LessThan_10000'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_NegAvail
order by DBName

Select *
From [WorkOps].dbo.Cust_NegAvail
where lessthan_0 > '0'
order by DBName

Select *
From [WorkOps].dbo.Cust_NegAvail
where lessthan_100 > '0'
order by DBName

Select *
From [WorkOps].dbo.Cust_NegAvail
where lessthan_1000 > '0'
order by DBName

Select *
From [WorkOps].dbo.Cust_NegAvail
where lessthan_5000 > '0'
order by DBName

Select *
From [WorkOps].dbo.Cust_NegAvail
where lessthan_10000 > '0'
order by DBName