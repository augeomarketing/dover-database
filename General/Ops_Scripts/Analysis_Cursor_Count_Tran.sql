--Populates table with list and count of all trancodes, by database

--Drop table [WorkOps].dbo.Cust_TranCount
--Create table [WorkOps].dbo.Cust_TranCount (DBName varchar(100) not null, Trancode varchar(4) null, Trandesc varchar(100) null, Count_Tran int null)
Truncate table [WorkOps].dbo.Cust_TranCount

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_TranCount
		 Select '''+@DBName+''' as DBName, h.Trancode as Trancode, h.Description as Trandesc, Count(h.trancode) as Count_Tran
		 from ['+@DBName+'].dbo.history h --join ['+@DBName+'].dbo.trantype t on h.trancode = t.trancode
		 group by h.Trancode, h.description'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From WorkOps.dbo.Cust_tranCount
order by DBName

Select *
From WorkOps.dbo.Cust_tranCount
order by trancode, trandesc

Select trancode, trandesc, sum(count_tran)
From WorkOps.dbo.Cust_tranCount
group by trancode, trandesc
order by trancode, trandesc