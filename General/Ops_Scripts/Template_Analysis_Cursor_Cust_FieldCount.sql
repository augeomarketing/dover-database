--This script populates a table in Workops with a list of accttypes and counts, by FI

--Drop table [WorkOps].dbo.Cust_FieldCount
--Create table [WorkOps].dbo.Cust_FieldCount (DBName varchar(100) not null, FieldValue varchar(50) null, FieldCount int null)
Truncate table [WorkOps].dbo.Cust_FieldCount
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_FieldCount
		 Select '''+@DBName+''' as DBName, Status as Fieldvalue, count(status) as FieldCount
		 from ['+@DBName+'].dbo.customer
		 group by status'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_FieldCount
order by DBName

Select *
From [WorkOps].dbo.Cust_FieldCount
where fieldvalue not in ('A','C')
order by DBName

Select dbname, sum(fieldcount)
From [WorkOps].dbo.Cust_FieldCount
group by dbname
order by DBName
