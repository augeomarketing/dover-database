Declare @month varchar(2)
Declare @day varchar(2)
Declare @year varchar(4)

set @month = '05'
set @day = '15'
set @year = '2008'

Select Left(tipnumber, 3) as TipFirst, count(Points) as Redeem_Count, Sum(Points*catalogqty) as Points
from [fullfillment].dbo.main
where month(histdate) = @month and day(histdate) = @day and year(histdate) = @year and tipnumber not like '%999999%'
group by Left(Tipnumber, 3)
order by tipfirst

Select Left(tipnumber, 3) as TipFirst, count(Points) as Redeem_Count, Sum(Points*catalogqty) as Points
from [onlinehistorywork].dbo.onlhistory
where month(histdate) = @month and day(histdate) = @day and year(histdate) = @year and tipnumber not like '%999999%'
group by Left(Tipnumber, 3)
order by tipfirst