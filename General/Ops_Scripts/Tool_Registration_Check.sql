--Two part script to allow viewing of data relevant to registration and to reset a specified registration
--Section that resets registration is currently commented out to prevent accidental erasure

Declare @Tip varchar(15)

Set @Tip = '641000000000064'

--Section is used to examine data on RN1 for any possible registration issues

Select * from [1security] where tipnumber = @Tip
Select * from [account] where tipnumber = @Tip
Select * from [customer] where tipnumber = @Tip

--Section is used to erase registration data for listed TIP

/*
Update [1security]
Set	username = null, 
	[password] = null, 
	secretq = null, 
	secreta = null, 
	emailstatement = 'Y',
	email = null, 
	email2 = null, 
	emailother = null, 
	[Last6] = null,
	regdate = null,
	Hardbounce = null,
	SoftBounce = null,
	nag = null,
	Paperstatement = null,
	ThisLogin = null,
	LastLogin = null
where	tipnumber = @Tip
*/