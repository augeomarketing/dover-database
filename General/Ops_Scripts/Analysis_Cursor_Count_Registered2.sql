--Drop table [RewardsNOW].dbo.Security_Registered
--Create table [RewardsNOW].dbo.Security_Registered (DBName varchar(100) not null, customer_count int null, registered_count int null, estatement_count int null)
Truncate table RewardsNOW.dbo.Security_Registered

Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @DBNameRN varchar(100)
Declare @TIPfirst varchar (3)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
  	  set @dbnamern = (Select dbnameNEXL from rewardsnow.dbo.dbprocessinfo where dbnamePatton = @dbname)
	  set @tipfirst = (Select dbnumber from rewardsnow.dbo.dbprocessinfo where dbnamePatton = @dbname)
	  set @sqlcmd =	
		'Insert into [RewardsNOW].dbo.Security_Registered
		 Select '''+@DBName+''' as DBName, count(*) as customer_count, 0 as registered_count, 0 as estatement_count
		 from ['+@DBNameRN+'].dbo.[1security]
		 where left(tipnumber,3) = '''+@tipfirst+''' and tipnumber not like ''%999999%'''
--	Print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  set @sqlcmd =
	    'Update [RewardsNOW].dbo.Security_Registered
	     Set registered_count = 
	       (Select count(*) from ['+@DBNameRN+'].dbo.[1security] 
	        where password is not null and left(tipnumber,3) = '''+@tipfirst+''' and tipnumber not like ''%999999%'')
	     where DBName = '''+@DBName+''''
--	Print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  set @sqlcmd =
	    'Update [RewardsNOW].dbo.Security_Registered
	     Set estatement_count = 
	       (Select count(*) from ['+@DBNameRN+'].dbo.[1security] 
	        where password is not null and emailstatement = ''Y'' and left(tipnumber,3) = '''+@tipfirst+''' and tipnumber not like ''%999999%'')
	     where DBName = '''+@DBName+''''
--	Print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  	     
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [RewardsNOW].dbo.Security_Registered
order by DBName

Select sum(customer_count)
From [RewardsNOW].dbo.Security_Registered
where dbname not like '9%'
 and dbname not in ('testclass990')