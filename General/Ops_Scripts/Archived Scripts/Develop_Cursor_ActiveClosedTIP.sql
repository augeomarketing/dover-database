Drop table [WorkOps].dbo.Cust_RecordCount
Create table [WorkOps].dbo.Cust_RecordCount (DBName varchar(100) not null, customer_Count int null)
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
--where DBNameNEXL = 'NewTNB' or DBNameNEXL = 'Indiana'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_RecordCount
		 Select '''+@DBName+''' as DBName, count(*) as customer_count
		 from ['+@DBName+'].dbo.customer
		 where tipnumber in (Select tipnumber from customerdeleted)'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_RecordCount
order by DBName