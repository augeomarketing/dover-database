--Create storage table for search
/*
Drop table [WorkOps].dbo.Redemptions_DailyCount
Create table [WorkOps].dbo.Redemptions_DailyCount	(DBName varchar(100) not null, 
							 TipFirst varchar(3) null,
							 Redeem_Count int null,
							 Points int null)
*/
Truncate Table [WorkOps].dbo.Redemptions_DailyCount
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor
for Select [name] from master.dbo.sysdatabases
--where DBNameNEXL = 'NewTNB' or DBNameNEXL = 'Indiana'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @month varchar(2)
Declare @day varchar(2)
Declare @year varchar(4)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @month = '05'
	  set @day = '15'
	  set @year = '2008'
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Redemptions_DailyCount
		 Select '''+@DBName+''' as DBName, Left(tipnumber, 3) as TipFirst, count(Points) as Redeem_Count, Sum(Points) as Points
		 from ['+@DBName+'].dbo.history
		 where month(histdate) = '''+@month+''' and day(histdate) = '''+@day+''' and year(histdate) = '''+@year+''' and tipnumber not like ''%999999%'' and trancode like ''R%''
		 group by Left(Tipnumber, 3)' 
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName


Select *
From [WorkOps].dbo.Redemptions_DailyCount
--where Error_Count > 0
order by tipfirst

Select TipFirst, Redeem_Count, Points
From [WorkOps].dbo.Redemptions_DailyCount
where DBName <> 'Onlinehistorywork'
order by tipfirst

Select Sum(Redeem_Count), SUm(Points)
From [WorkOps].dbo.Redemptions_DailyCount