--Script to delete all unnecessary zero point balance records from the data system.
--Removed data will be stored in two locations in case retrieval becomes necessary
--Location 1: Patton - [RewardsNOW].dbo.ZeroPointStorage - This will store all records removed from FI history tables
--Location 2: NEXL - [RewardsNOW].dbo.ZeroPointStorage - This will store all records removed from FI onlhistory tables
--							 This can be used to repop onlinehistorywork and main as well



--First step
--Server: Patton Database: Fullfillment Table: Main

--Delete from Fullfillment.dbo.main
Where	points = '0'
  And	(tipnumber not like '203%' and tipnumber not like '4%' and tipnumber not like '7%')
  And	trancode <> 'RQ'



--Second step
--Server: Patton Database: OnlineHistoryWork Table: OnlHistory

--Delete from OnlineHistoryWork.dbo.Onlhistory
Where	points = '0'
  And	(tipnumber not like '203%' and tipnumber not like '4%' and tipnumber not like '7%')
  And	trancode <> 'RQ'



--Third step (Make this into Stored Proc?)
--Server: Patton Database(s): All FI DBs Table: History
--This step records the records about to be deleted in a storage table in case they need to be recovered at a later date
--and then deletes the records from the individual FI databases

Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
--Remove databases managed by Dan Foster from cursor run
where DBNameNEXL <> 'NJFCU' and DBNameNEXL <> 'Onebridge' and DBNameNEXL <> 'NEBA'

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [RewardsNOW].dbo.ZeroPointStorage
		 Select	tipnumber, acctid, histdate, trancode, trancount, points, [description], secid, ratio,
			overage, recnum
		 from 	['+@DBName+'].dbo.history
		 Where	points = ''0''
    		   And	(tipnumber not like ''203%'' and tipnumber not like ''4%'' and tipnumber not like ''7%'')
    		   And	trancode <> ''RQ'''

--	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	
		'Delete From ['+@DBName+'].dbo.history
		 Where	points = ''0''
    		   And	(tipnumber not like ''203%'' and tipnumber not like ''4%'' and tipnumber not like ''7%'')
    		   And	trancode <> ''RQ'''

--	  EXECUTE sp_executesql @SqlCmd

	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName


--Fourth step
--Server: RN1 Database: OnlineHistoryWork Table: OnlHistory

--Delete from [RN1].OnlineHistoryWork.dbo.Onlhistory
Where	points = '0'
  And	(tipnumber not like '203%' and tipnumber not like '4%' and tipnumber not like '7%')
  And	trancode <> 'RQ'



--Fifth step (Make this into Stored Proc?)
--Server: Patton Database(s): All FI DBs Table: History
--This step records the records about to be deleted in a storage table in case they need to be recovered at a later date
--and then deletes the records from the individual FI databases

Declare curDBName cursor 
for Select Distinct(dbnameNEXL) from rewardsnow.dbo.dbprocessinfo
--Remove databases managed by Dan Foster from cursor run
where DBNameNEXL <> 'NJFCU' and DBNameNEXL <> 'Onebridge' and DBNameNEXL <> 'NEBA'

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [RN1].[RewardsNOW].dbo.ZeroPointStorage
		 Select	tipnumber, histdate, email, points, trandesc, postflag, transid, trancode, catalogcode,
			catalogdesc, catalogqty, source, copyflag, saddress1, saddress2, scity, sstate, szipcode,
			scountry, hphone, wphone, notes, sname, ordernum, linenum, collegeacctname
		 from 	[RN1].['+@DBName+'].dbo.onlhistory
		 Where	points = ''0''
    		   And	(tipnumber not like ''203%'' and tipnumber not like ''4%'' and tipnumber not like ''7%'')
    		   And	trancode <> ''RQ'''
Print @sqlcmd
--	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	
		'Delete From [RN1].['+@DBName+'].dbo.onlhistory
		 Where	points = ''0''
    		   And	(tipnumber not like ''203%'' and tipnumber not like ''4%'' and tipnumber not like ''7%'')
    		   And	trancode <> ''RQ'''
Print @sqlcmd
--	  EXECUTE sp_executesql @SqlCmd

	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName