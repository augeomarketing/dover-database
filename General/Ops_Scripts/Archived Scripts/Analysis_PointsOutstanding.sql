--Drop table [WorkOps].dbo.Cust_PointsOut_PIT
--Create table [WorkOps].dbo.Cust_PointsOut_PIT (DBName varchar(100) not null, Points_Outstanding int null, Rundate datetime)
Truncate table [WorkOps].dbo.Cust_PointsOut_PIT

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where dbnamePatton <> 'BQTest'

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_PointsOut_PIT
		 Select '''+@DBName+''' as DBName, sum(runavailable) as Points_Outstanding, getdate() as rundate
		 from ['+@DBName+'].dbo.customer'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_PointsOut_PIT
order by DBName
