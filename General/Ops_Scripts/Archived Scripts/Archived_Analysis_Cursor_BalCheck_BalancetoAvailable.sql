--Returns a record count by database of the number of customers whose 
--Runbalance minus Runredeemed does not match runavailable

--Create storage table for search
--Drop table [WorkOps].dbo.Cust_BalanceError
--Create table [WorkOps].dbo.Cust_BalanceError (DBName varchar(100) not null, Error_Count int null)
Truncate table [WorkOps].dbo.Cust_BalanceError

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
--where DBNameNEXL = 'NewTNB' or DBNameNEXL = 'Indiana'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_BalanceError
		 Select '''+@DBName+''' as DBName, count(*) as error_count
		 from ['+@DBName+'].dbo.customer
		 where runbalance - runredeemed <> runavailable'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From WorkOps.dbo.Cust_BalanceError
Where error_count > 0
order by DBName