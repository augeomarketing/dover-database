Select * from selection
where Clientcode = 'ASB' and submitdate >= '1/15/2009' and datefulfilled is null

Select
Upper(rtrim(Name))	as Name,
  Case
     WHEN 
	RIGHT(RTRIM(catalogcode), 2) = '-B' 
		THEN CONVERT(INT, SUBSTRING(RTRIM(catalogcode), LEN(RTRIM(catalogcode))-4, 3))
     ELSE RIGHT(RTRIM(catalogcode), 3)
  END		AS Amount,
RTRIM(Itemname)	AS Gift_Card_Type,
Submitdate	AS Redemption_Date,
  CASE
    WHEN address2 is null THEN rtrim(Address1)
    ELSE rtrim(Address1)+' '+ rtrim(address2)
  END AS address,
rtrim(city)	as City,
rtrim(state)	as State,
rtrim(zip)	as Zip
FROM 
	selection 
where 
	Clientcode = 'ASB' 
	and submitdate >= '1/15/2009'
	and datefulfilled is null
order by submitdate


/*
Update selection 
set datefulfilled = getdate()
where 	Clientcode = 'ASB' 	and submitdate >= '1/15/2009' and datefulfilled is null
*/
