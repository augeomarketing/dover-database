--Declare variables and establish date to run procedure for
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)
Declare @month varchar(2)
Declare @day varchar(2)
Declare @year varchar(4)

set @month	= '02'
set @day	= '04'
set @year	= '2008'

--Create storage table for search
Drop table [Fullfillment].dbo.Redemptions_DailyCount
Create table [Fullfillment].dbo.Redemptions_DailyCount
(	TipFirst varchar(3) null,
	Count_RN1 int null,
	Points_RN1 int null,
	Count_ONL_RN1 int null,
	Points_ONL_RN1 int null,
	Count_ONL_Patton int null,
	Points_ONL_Patton int null,
	Count_Patton int null,
	Points_Patton int null,
	Count_Fulfillment int null,
	Points_Fulfillment int null,
)

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor
for Select [name] from [RN1].master.dbo.sysdatabases
where [name] not in (--'Carlson', 'Master', 'RewardsNOW', 'model', 'MSDB', 'PikNClik', 'PINs', 'Principal', 
'onlinehistorywork')

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'If (SELECT Count(name) FROM [RN1].'+@dbname+'.dbo.sysobjects Where Name = ''OnlHistory'' AND xType= ''U'') > ''0''
	Begin
		 Insert into [Fullfillment].dbo.Redemptions_DailyCount
		 Select Left(tipnumber, 3) as TipFirst, 
			count(Points) as Count_RN1, 
			Sum(Points*CatalogQty) as Points_RN1,
			NULL as Count_ONL_RN1,
			NULL as Points_ONL_RN1,
			NULL as Count_ONL_Patton,
			NULL as Points_ONL_Patton,
			NULL as Count_Patton,
			NULL as Points_Patton,
			NULL as Count_Fulfillment,
			NULL as Points_Fulfillment
		 from [RN1].['+@DBName+'].dbo.onlhistory
		 where month(histdate) = '''+@month+''' and day(histdate) = '''+@day+''' and year(histdate) = '''+@year+''' and tipnumber not like ''%999999%''
		 group by Left(Tipnumber, 3)
	end
else
	begin
		Print ''skipped database '+@dbname+'''
	end' 
Print @sqlcmd
		 EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName


--Select *
--From [Fullfillment].dbo.Redemptions_DailyCount
--order by tipfirst