--Select * from onlhistory where tipnumber like '596%' order by histdate desc

Declare @TIP varchar(15)
Declare @email varchar(50)
Declare @qty int
Declare @sad1 varchar(50)
Declare @sad2 varchar(50)
Declare @scity varchar(50)
Declare @sstate varchar(5)
Declare @szipcode varchar(10)
Declare @hphone varchar(12)

Set @TIP = '596000000019928'
set @email = 'Debra.Smack@rcu.org'
Set @qty = '2'
Set @sad1 = '18281 57TH AVE'
Set @sad2 = ''
Set @scity = 'CHIPPEWA FALLS'
Set @sstate = 'WI'
Set @szipcode = '54729'
Set @hphone = '715-720-6039'


--Insert into onlhistory
(tipnumber, histdate, email, points, trandesc, 
 trancode, catalogcode, catalogdesc, catalogqty,
 source, saddress1, saddress2, scity,
 sstate, szipcode, scountry, hphone, wphone,
 notes)
values 
(
@TIP, --as tipnumber, 
getdate(), --as histdate, 
@email, --as email, 
'7000', --as points, 
'Redeem for Gift Cert', --as trandesc,
'RC', --as trancode, 
'GCR-RCUPGC-050', --as catalogcode, 
'$50 RCU Prepaid Visa Card', --as catalogdesc, 
@qty, --as catalogqty,
'web', --as source, 
@sad1, --as saddress1, 
@sad2, --as saddress2, 
@scity, --as scity,
@sstate, --as sstate, 
@szipcode, --as szipcode, 
'United States', --as scountry, 
@hphone,--as hphone, 
'715-833-8284', --as wphone,
'Entry to update dropped Visa Card redemption(s)' --as notes
)


Select * from onlhistory where tipnumber = @TIP order by histdate desc

Select * from onlhistory where notes = 'Entry to update dropped Visa Card redemption(s)'