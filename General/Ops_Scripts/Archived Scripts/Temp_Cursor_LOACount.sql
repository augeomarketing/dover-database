Drop table #tempresults
Create table #tempresults (DBName varchar(100) not null, Yr int null, Mo int null, Ct int null)
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where DBnumber like '5%'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into #tempresults
		 Select '''+@DBName+''' as DBName, Year(dateadded) Yr, Month(dateadded) Mo, Count(dateadded) Ct
		 from ['+@DBName+'].dbo.customer
		 where acctname2 like ''%corporate account%''
		 group by Year(dateadded), Month(dateadded)'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From #tempresults
order by DBName

Select yr, mo, sum(ct)
From #tempresults
group by yr, mo
order by yr desc, mo desc

Select distinct dbname
from #tempresults
