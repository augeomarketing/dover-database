declare @startdate nvarchar(25)
declare @enddate nvarchar(25)
declare @deletedate nvarchar(25)
declare @nextmonthstartdate nvarchar(25)
select clientname as clientname from client
--Change Dates for the Month Being Reviewed
--Start date is entered manually, all other dates calculated from startdate.
--Converts are included so that generated dates can be printed to confirm accuracy.
--1/31/2008 RBM
set @startdate = '2009-01-01 00:00:00:000'
set @enddate = Dateadd(month, 1, @startdate)
set @enddate = convert(nvarchar(25),(Dateadd(millisecond, -3, @enddate)),121)
set @deletedate = Dateadd(month, 1, @startdate)
set @deletedate = convert(nvarchar(25),(Dateadd(day, -1, @deletedate)),121)
--Used to obtain EOMBalance for the Month Being Processed
--set @nextmonthstartdate = '2007-12-01 00:00:00:000'
set @nextmonthstartdate = convert(nvarchar(25),(Dateadd(month, 1, @startdate)),121) -- RDT

--Display relevant dates to confirm appropriate dates are being used in calculations
--1/31/2008 RBM
Select
@startdate as Start_Date,
@enddate as End_Date,
@deletedate as Delete_Date,
@nextmonthstartdate as Next_Month_Start_Date

--Select The Previous End Of Month Balance                        ***SUMMARY - BEGINNING OUTSTANDING POINTS
--This should match the Beginning Points Outstanding for this Month from the Liability Report
Select
Case  When (      select      sum(points*ratio)
            from  history
            where histdate < @startdate)
is null then '0'
      Else (      select      sum(points*ratio)
            from  history
            where histdate < @startdate)
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @startdate)
              and (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @startdate)
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @startdate)
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @startdate)
              and (datedeleted > @enddate))
end
as Beginning_Points_Outstanding
--Select the Total Points earned During the Currents Month�s Run  ***SUMMARY - PRODUCT TOTAL POINTS
--Modified 1/16/2007 RBM   Will now calculate with null values in overage.  Assumes overage value = 0
Select
Case  When (      select      sum((points+isnull(overage, 0))*ratio)
            from  history
            where (histdate between @startdate and @enddate) and (trancode like '6%'))
is null then '0'
      Else (      select      sum((points+isnull(overage, 0))*ratio)
            from  history
            where (histdate between @startdate and @enddate) and (trancode like '6%'))
end
+
Case  When (      select      sum((points+isnull(overage, 0))*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '6%')
              and (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum((points+isnull(overage, 0))*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '6%')
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum((points+isnull(overage, 0))*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '6%')
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum((points+isnull(overage, 0))*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '6%')
              and (datedeleted > @enddate))
end
as Product_Total_Points
--Select the Total estatement Points awarded during the Currents Month�s Run  ***SUMMARY - BONUS POINTS
Select
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode like '0%' or trancode like 'B%' or trancode like 'F%' or trancode like 'N%') and (trancode <> 'BE'))
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode like '0%' or trancode like 'B%' or trancode like 'F%' or trancode like 'N%') and (trancode <> 'BE'))
end
+
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '0%' or trancode like 'B%' or trancode like 'F%' or trancode like 'N%') and (trancode <> 'BE') 
              and (datedeleted between @startdate and @enddate)) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '0%' or trancode like 'B%' or trancode like 'F%' or trancode like 'N%') and (trancode <> 'BE') 
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '0%' or trancode like 'B%' or trancode like 'F%' or trancode like 'N%') and (trancode <> 'BE') 
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '0%' or trancode like 'B%' or trancode like 'F%' or trancode like 'N%') and (trancode <> 'BE') 
              and (datedeleted > @enddate))
end
as Bonus_Points_Added
--Select the Total estatement Points awarded during the Currents Month�s Run  ***SUMMARY - REWARDSNOW BONUS POINTS
Select
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode = 'BE')) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode = 'BE'))
end
+
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'BE')
              and (datedeleted between @startdate and @enddate)) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'BE')
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'BE')
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'BE')
              and (datedeleted > @enddate))
end
as Rewards_Bonus_Added
--Select the Total Points with an Increase During the Currents Month�s Run    ***SUMMARY - ADJ INCREASE POINTS
Select
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode in ('IE','TP'))) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode in ('IE','TP')))
end
+
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode in ('IE','TP'))
              and (datedeleted between @startdate and @enddate)) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode in ('IE','TP'))
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode in ('IE','TP'))
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode in ('IE','TP'))
              and (datedeleted > @enddate))
end
as Increase_Earned
--Select the Total Points with an Increase During the Currents Month�s Run    ***SUMMARY - ADJ DECREASE POINTS
Select
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode = 'DE')) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  history
            where (histdate between @startdate and @enddate) and (trancode = 'DE'))
end
+
Case  When (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'DE')
              and (datedeleted between @startdate and @enddate)) 
is null then '0'
      Else (      select      sum(points*ratio) as Adj_Decrease
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'DE')
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'DE')
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode = 'DE')
              and (datedeleted > @enddate))
end
as Decrease_Earned
--Select the Total Points Expired During the Currents Month�s Run ***SUMMARY - EXPIRED POINTS
Select
Case  When (      select      sum(points*ratio)
            from  history
            where (histdate between @startdate and @enddate) and (trancode like 'X%'))
is null then '0'
      Else (      select      sum(points*ratio)
            from  history
            where (histdate between @startdate and @enddate) and (trancode like 'X%'))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like 'X%')
              and (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like 'X%')
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like 'X%')
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like 'X%')
              and (datedeleted > @enddate))
end
as Expired_Points
--Select the Total Points Redeemed During the Currents Month�s Run      ***SUMMARY - REDEEMED POINTS
Select
Case  When (      select      sum(points*ratio)
            from  history
            where (histdate between @startdate and @enddate) and ((trancode like 'R%') or (trancode in ('IR','DR'))))
is null then '0'
      Else (      select      sum(points*ratio)
            from  history
            where (histdate between @startdate and @enddate) and ((trancode like 'R%') or (trancode in ('IR','DR'))))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and ((trancode like 'R%') or (trancode in ('IR','DR')))
              and (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and ((trancode like 'R%') or (trancode in ('IR','DR')))
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and ((trancode like 'R%') or (trancode in ('IR','DR')))
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and ((trancode like 'R%') or (trancode in ('IR','DR')))
              and (datedeleted > @enddate))
end
as Redemptions
--Select the Total Points Returned During the Currents Month�s Run      ***SUMMARY - RETURNED POINTS
Select
Case  When (      select      sum(points*ratio)
            from  history
            where (histdate between @startdate and @enddate) and (trancode like '3%'))
is null then '0'
      Else (      select      sum(points*ratio)
            from  history
            where (histdate between @startdate and @enddate) and (trancode like '3%'))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '3%')
              and (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '3%')
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '3%')
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate between @startdate and @enddate) and (trancode like '3%')
              and (datedeleted > @enddate))
end
as [Returns]
--Select the Total Points Over Max Allowed During the Currents Month�s Run    ***SUMMARY - OVERAGE POINTS
Select
Case  When (      select      sum(overage)
            from  history
            where (histdate between @startdate and @enddate))
is null then '0'
      Else (      select      sum(overage)
            from  history
            where (histdate between @startdate and @enddate))
end
+
Case  When (      select      sum(overage)
            from  historydeleted
            where (histdate between @startdate and @enddate)
              and (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum(overage)
            from  historydeleted
            where (histdate between @startdate and @enddate)
              and (datedeleted between @startdate and @enddate))
end
+
Case  When (      select      sum(overage)
            from  historydeleted
            where (histdate between @startdate and @enddate)
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(overage)
            from  historydeleted
            where (histdate between @startdate and @enddate)
              and (datedeleted > @enddate))
end
as Overage
--Select the Total Points Purged During the Currents Month�s Run  ***SUMMARY - PURGED POINTS
Select
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @enddate)
               and      (datedeleted between @startdate and @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @enddate)
               and      (datedeleted between @startdate and @enddate))
end

as Purged_Points
--Select The Current End Of Month Balance                         ***SUMMARY - NET OUTSTANDING POINTS
--This should match the Net Points Outstanding for this Month from the Liability Report
Select
Case  When (      select      sum(points*ratio)
            from  history
            where (histdate < @nextmonthstartdate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  history
            where (histdate < @nextmonthstartdate))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @nextmonthstartdate)
              and (datedeleted > @enddate))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @nextmonthstartdate)
              and (datedeleted > @enddate))
end
as Net_Points_Outstanding
--Select The Current Redeemable Points Outstanding                ***SUMMARY - REDEEMABLE OUTSTANDING POINTS
--This should match the Redeemable Points Outstanding for this Month from the Liability Report
--Added 1/4/2007 - RBM
Select
Case  When (      select      sum(points*ratio)
            from  history
            where histdate < @enddate
            and   tipnumber in      (Select tipnumber 
                               from history 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
is null then '0'
      Else (      select      sum(points*ratio)
            from  history
            where histdate < @enddate
            and   tipnumber in      (Select tipnumber 
                               from history 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
end
+
Case  When (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @enddate)
              and (datedeleted > @enddate)
            and   tipnumber in      (Select tipnumber 
                               from historydeleted 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
is null then '0'
      Else (      select      sum(points*ratio)
            from  historydeleted
            where (histdate < @enddate)
              and (datedeleted > @enddate)
            and   tipnumber in      (Select tipnumber 
                               from historydeleted 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
end
as Redeemable_Points_Outstanding
--Select The Total number of Debit/Credit accounts                ***SUMMARY - ACCOUNT_COUNTS
--Added 1/4/2007 - RBM
select accttype, count(distinct acctid) as acctcount 
from affiliat 
where dateadded < @nextmonthstartdate 
group by accttype
--Deleted accounts - This number should be added to the active counts to determine a historic value
select accttype as accttype_Del, count(distinct acctid) as acctcount_Del
from affiliatdeleted 
where dateadded < @nextmonthstartdate and datedeleted >= @enddate
group by accttype
/*
--Select The Total number of eligible participants based on the Affiliat table            ***SUMMARY - ELIGIBLE_AFFILIAT
--Added 1/7/2007 - RBM
Select
(     select      count (distinct tipnumber)
            from  affiliat
            where dateadded < @nextmonthstartdate
              and       tipnumber in      (Select tipnumber 
                               from history 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
+
(     select      count (distinct tipnumber)
            from  affiliatdeleted
            where (dateadded < @nextmonthstartdate)
              and       tipnumber not in (Select distinct tipnumber from affiliat)
              and (datedeleted >= @enddate)
              and       tipnumber in      (Select tipnumber 
                               from historydeleted 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
as Eligible_Affiliat
*/
--Select The Total number of eligible participants based on the Customer table            ***SUMMARY - ELIGIBLE_CUSTOMER
--Added 1/7/2007 - RBM
Select
(     select      count (distinct tipnumber)
            from  customer
            where dateadded < @nextmonthstartdate
              and       tipnumber in      (Select tipnumber 
                               from history 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
+
(     select      count (distinct tipnumber)
            from  customerdeleted
            where (dateadded < @nextmonthstartdate)
              and  tipnumber not in (Select distinct tipnumber from customer)
			  and  statusdescription not like 'Pri%'
              and (datedeleted >= @enddate)
              and       tipnumber in      (Select tipnumber 
                               from historydeleted 
                               where histdate <= @enddate 
                               group by tipnumber 
                               having Sum(points*ratio)>=(Select isnull(minRedeemNeeded, 750) from client)))
as Eligible_Customer
/*
--Select The Total number of participants based on the Affiliat table         ***SUMMARY - PARTICIPANTS_AFFILIAT
--Added 1/4/2007 - RBM
Select
(     select      count (distinct tipnumber)
            from  affiliat
            where dateadded < @nextmonthstartdate)
+
(     select      count (distinct tipnumber)
            from  affiliatdeleted
            where (dateadded < @nextmonthstartdate)
              and       tipnumber not in (Select distinct tipnumber from affiliat)
              and (datedeleted >= @enddate))
as Participants_Affiliat
*/
--Select The Total number of participants based on the Customer table         ***SUMMARY - PARTICIPANTS_CUSTOMER
--Added 1/4/2007 - RBM
Select
(     select      count (distinct tipnumber)
            from  customer
            where dateadded < @nextmonthstartdate)
+
(     select      count (distinct tipnumber)
            from  customerdeleted
            where (dateadded < @nextmonthstartdate)
              and  tipnumber not in (Select distinct tipnumber from customer)
			  and  statusdescription not like 'Pri%'
              and (datedeleted >= @enddate))
as Participants_Customer


--Select The total of this Month�s Points from HistoryDeleted           ***SUMMARY - DELETED POINTS
select      sum(points*ratio)as Total_Deleted_Points 
from  historydeleted 
where histdate between @startdate and @enddate
      and datedeleted = @deletedate
--Select This Months Detail From History Deleted                  ***DETAIL - DELETED POINTS
select      TRANCODE, sum(points*ratio) as Deleted_Points 
from  historydeleted 
where histdate between @startdate and @enddate
      and datedeleted = @deletedate
Group by TRANCODE
Order by TRANCODE
--Select The transaction Detail Totals for the Current Month            ***DETAIL - EARNED/USED POINTS
select      TRANCODE, sum(points*ratio) as EarnedUsed_Points 
from  history 
where histdate between @startdate and @enddate
Group by TRANCODE
Order by TRANCODE

-- RDT 12/27/08
--select count(tipnumber) as Participants from customer
--select count (distinct tipnumber) as Participants from affiliat where dateadded < @nextmonthstartdate
--select accttype, count(accttype) from affiliat where dateadded < @nextmonthstartdate group by accttype
