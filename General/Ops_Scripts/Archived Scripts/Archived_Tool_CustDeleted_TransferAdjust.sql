--Updates the deleted records of a customer to reflect points transferred from a closed to an open account

Declare @tipnumber varchar(15), @points int, @LiveTip varchar(15), @datetime varchar(25)

Set	@tipnumber = '360000000190084'
Set	@points = '31663'
Set	@LiveTip = '360000000821747'
Set	@datetime = getdate()

--Inserts a record into the history table to record transfer of points to an active TIPnumber
--If command inserted to prevent history record being created if script is looking at wrong database (RBM 2/15/08)

If	Left(@TIPnumber, 3) = (Select top 1 Left(tipnumber, 3) from customer)
Begin
	Insert into historydeleted
	(tipnumber,Acctid,Histdate,trancode,trancount,Points,[Description],Secid,ratio,Overage,Datedeleted)
	Select
		@tipnumber,
		(Select top 1 acctid from historydeleted where tipnumber = @tipnumber),
		getdate(),
		'DE',
		'1',
		@points,
		'Pts to ' + @LiveTip + ' on ' + @datetime,
		(Select top 1 secid from historydeleted where tipnumber = @tipnumber),
		'-1',
		'0',
		(Select top 1 datedeleted from historydeleted where tipnumber = @tipnumber)

--Updates the customer record to reflect transfer of opints to an active TIPnumber

	Update	customerdeleted
	Set	runbalance = (runbalance - @points), runavailable = (runavailable - @points)
	where	tipnumber = @tipnumber

--Displays the customerdeleted and historydeleted records for confirmation that data was updated correctly

	Select * from customerdeleted where tipnumber = @tipnumber
	Select * from Historydeleted where tipnumber = @tipnumber order by histdate desc
end

else
Begin
	Print 'You are in the wrong database'
end