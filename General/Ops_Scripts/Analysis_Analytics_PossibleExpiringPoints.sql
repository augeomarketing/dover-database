--Create table #temp (tipnumber varchar(15), Total_points int, Possible_expire int, Used_points int, To_expire int)
Declare @expdate datetime
Declare @expire smallint

set @expire = 3

set @expire = @expire - 1
set @expdate = Cast(year(getdate())-@expire as varchar(4)) + '-01-01 00:00:00.000'

truncate table #temp

Insert into #temp 
select tipnumber, 0,0,0,0 from customer

update #temp set total_points = (Select isnull(sum(points*ratio),0) from history where history.tipnumber = #temp.tipnumber)
update #temp set possible_expire = (Select isnull(sum(points*ratio),0) from history where history.tipnumber = #temp.tipnumber and histdate < @expdate and left(trancode, 1) not in ('R','X'))
update #temp set used_points = (Select isnull(sum(points*ratio),0)*-1 from history where history.tipnumber = #temp.tipnumber and left(trancode, 1) in ('R','X'))
update #temp set to_expire = possible_expire - used_points
update #temp set to_expire = 0 where to_expire < 0


Select count(tipnumber)Cust_toexpire from #temp where to_expire > 0
Select sum(to_expire) Points_toexpire from #temp where to_expire > 0

Select * from #temp where to_expire > 0 order by tipnumber
