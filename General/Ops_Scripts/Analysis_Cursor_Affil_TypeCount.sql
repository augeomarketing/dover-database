--This script populates a table in Workops with a list of accttypes and counts, by FI

--Drop table [WorkOps].dbo.Affil_TypeCount
--Create table [WorkOps].dbo.Affil_TypeCount (DBName varchar(100) not null, accttype varchar(50) null, TypeCount int null)
Truncate table [WorkOps].dbo.Affil_TypeCount
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Affil_TypeCount
		 Select '''+@DBName+''' as DBName, accttype, count(Accttype) as TypeCount
		 from ['+@DBName+'].dbo.affiliat
		 group by accttype'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Affil_TypeCount
order by DBName

Select *
From [WorkOps].dbo.Affil_TypeCount
where accttype not like 'Credit%' and accttype not like 'Debit%'
	and dbname not like '9%' and dbname not in ('594Harrisconsumer','testclass990')
order by DBName

Select dbname, sum(typecount)
From [WorkOps].dbo.Affil_TypeCount
where dbname not like '9%' and dbname not in ('594Harrisconsumer','testclass990')
group by dbname
order by DBName

Select accttype, sum(typecount)
from [WorkOps].dbo.Affil_TypeCount
group by accttype
order by accttype

Select accttype, sum(typecount)
from [WorkOps].dbo.Affil_TypeCount
where dbname not in ('594Harrisconsumer')
group by accttype
order by accttype
