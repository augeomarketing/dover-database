Select * from metavantework.dbo.autoprocessdetail	order by tipfirst
Select * from metavantework.dbo.packagecodes		order by tipfirst							--Master table for what subpackage gets run for each FI
Select * from metavantework.dbo.packagename			order by type, id							--Control table for determining which subpackage to run for DTS step
Select * from metavantework.dbo.packagerunconfirm	order by datamonth desc, tipfirst, type		--Confirmation table for processing
Select * from metavantework.dbo.packagebonus		order by tipfirst							--Bonus TIPs
Select * from metavantework.dbo.packageerror		order by rundate desc						--Failed Audits


--Display TIPFirsts queued for Processing
Select tipfirst, dbname1, Monthbeginingdate, Monthendingdate,  CA_id, PW_id, UB_id, WK_id, NM_id, [Debit/Credit]
from autoprocessdetail  
where ca_ID <> 0 OR PW_id <> 0 or UB_id <> 0 or WK_id <> 0 or NM_id <> 0 
order by tipfirst

--Display all TIPFirsts not run for current month
Select apd.tipfirst
from autoprocessdetail apd 
	join packagecodes pc					on apd.tipfirst = pc.tipfirst
	left outer join packagerunconfirm prc   on apd.tipfirst = prc.tipfirst and apd.monthbeginingdate = prc.datamonth and prc.type <> 'CA'
  where prc.tipfirst is null and pc.PW_ID <> 0 and pc.UB_id <> 0
order by apd.tipfirst




--Populate autoprocessdetail from DBprocessinfo
--Missing Data Elements:   <<debit/credit>>
/*
Insert into metavantework.dbo.autoprocessdetail
Select	d.dbnumber as tipfirst, dbnamepatton as dbname1, dbnamepatton as dbname2, 'O:\5xx\Input\'+DebitFileName as Sourcefilenamestring, 'C:\SQLBackups\Processing_Backups\'+dbnamepatton+'.bak' as backupnamestring,
		null as monthbeginingdate, null as monthendingdate, c.clientcode as RNClientCode, 0 as PW_id, 0 as UB_id, 0 as WK_id, 0 as NM_id, null as [debit/credit],
		'O:\5xx\Output\Errorfiles\MissingNames\'+dbnumber+'MissingNames.csv' as MissingNameOut, null as quarterbeginingdate, null as quarterendingdate
from rewardsnow.dbo.dbprocessinfo d left outer join metavantework.dbo.autoprocessdetail apd on d.dbnumber = apd.tipfirst
	left outer join metavantework.dbo.sourcefilecheck sfc on d.dbnumber = sfc.tipprefix
	left outer join rn1.metavante.dbo.clientaward c on d.dbnumber = c.tipfirst
where left(d.dbnumber, 1) = '5' and apd.tipfirst is null and d.sid_fiprodstatus_statuscode not in ('X','V','I')
order by d.dbnumber
*/

--Shows all TIPFirsts set up in autoprocessdetail that have not had packagecodes assigned yet
Select * from metavantework.dbo.autoprocessdetail apd left outer join metavantework.dbo.packagecodes pc on apd.tipfirst = pc.tipfirst where pc.tipfirst is null




--Setup for new subpackage
/*
insert into metavantework.dbo.packagename
(type,id,path)
values
('UB',1,'5xx_Sub_UB01_UpdateBeginningBalance')
*/

--Code setup for new TIPFirst
/*
insert into metavantework.dbo.packagecodes
(tipfirst,PW_id,UB_id,WK_id,NM_id)
values
('5xx',0,0,0,0)
*/

--Update codes for retired tipfirst
/*
Update metavantework.dbo.packagecodes
set PW_id = 0,
	UB_id = 0,
	WK_id = 0,
	NM_id = 0
where TipFirst = '5xx'
*/