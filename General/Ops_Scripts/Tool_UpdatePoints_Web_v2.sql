--This script is used to update the points on a single account from Patton to RN1
--DO NOT USE THIS SCRIPT IF THE CLIENT IS IN MONTHLY PROCESSING!!!
Declare @sql nvarchar(4000)
Declare @tip varchar(15)
Declare @PattonDB varchar(50)
Declare @RNDB varchar(50)
Declare	@Balance varchar(10)
Declare	@Redeemed varchar(10)
Declare	@Available varchar(10)
Declare	@RedeemedOutstanding varchar(10)
Declare	@AdjustmentsOutstanding varchar(10)
Declare	@CLASSRedeemOutstanding varchar(10)

declare @rc		int

Set	@tip = '213000000013280'
-- PattonDB and RNDB will be assigned based on TIP selected
Set @pattonDB =	(Select rtrim(DBNamePatton) from rewardsnow.dbo.dbprocessinfo where DBNumber = Left(@tip, 3))
Set @RNDB =		(Select rtrim(DBNameNEXL) from rewardsnow.dbo.dbprocessinfo where DBNumber = Left(@tip, 3))

--/*  SECTION 1 - ANALYSIS
--This will generate a sql statement that will return the TIP, Name and point values for the record being adjusted
--This should be used to determine that the correct account is being updated
	Set @sql = 
	'Select	p.tipnumber,
			rtrim(p.acctname1) as acctname1,
			runbalance, 
			runredeemed, 
			runavailable, 
			earnedbalance, 
			redeemed, 
			availablebal, 
			(Select sum(points) from [RN1].['+@RNDB+'].dbo.onlhistory where tipnumber = ' + char(39) +@tip+ char(39) + ' and copyflag is null) as Outstanding_redeems,
			(Select sum(points*ratio) from [RN1].[onlinehistorywork].dbo.portal_adjustments where tipnumber = ' + char(39) +@tip+ char(39) + ' and copyflag is null) as Outstanding_CLASS
	from 	['+@PattonDB+'].dbo.customer p join [RN1].['+@RNDB+'].dbo.customer r
				on p.tipnumber = r.tipnumber
	where	p.tipnumber = ' + char(39) +@tip+ char(39) + ''

--	Print @sql
	EXECUTE sp_executesql @Sql
/**/


-- IF statement will prevent adjustment from being made if Database is flagged unavailable in DBProcessinfo
If (Select DBavailable from rewardsnow.dbo.dbprocessinfo where DBNumber = Left(@tip, 3)) = 'Y'
BEGIN


-- Balance, Redeemed and Available will be set to the Patton values for requested TIP
Set @sql = N'Set	@Balance = (Select runbalance from ['+@PattonDB+'].dbo.customer where tipnumber = ' + char(39) +@tip+ char(39) + ')'
	EXECUTE sp_executesql @Sql, N'@Balance int OUTPUT', @Balance = @Balance output
Set @sql = N'Set	@Redeemed = (Select runredeemed from ['+@PattonDB+'].dbo.customer where tipnumber = ' + char(39) +@tip+ char(39) + ')'
	EXECUTE sp_executesql @Sql, N'@Redeemed int OUTPUT', @Redeemed = @Redeemed output
Set @sql = 'Set	@Available = (Select runavailable from ['+@PattonDB+'].dbo.customer where tipnumber = ' + char(39) +@tip+ char(39) + ')'
	EXECUTE sp_executesql @Sql, N'@Available int OUTPUT', @Available = @Available output
-- Calculate all outstanding adjustments and redemptions on RN1 for the requested TIP
Set @sql = N'Set	@RedeemedOutstanding = (isnull((Select sum(points) from [RN1].['+@RNDB+'].dbo.onlhistory where tipnumber = ' + char(39) +@tip+ char(39) + ' and copyflag is null), 0))'
	EXECUTE sp_executesql @Sql, N'@RedeemedOutstanding int OUTPUT', @RedeemedOutstanding = @RedeemedOutstanding output
Set @sql = 'Set	@AdjustmentsOutstanding = (isnull((Select sum(points*ratio) from [RN1].[onlinehistorywork].dbo.portal_adjustments where tipnumber = ' + char(39) +@tip+ char(39) + ' and (trancode not like ''R%'' and trancode not in (''DR'',''DZ'')) and copyflag is null), 0))'
	EXECUTE sp_executesql @Sql, N'@AdjustmentsOutstanding int OUTPUT', @AdjustmentsOutstanding = @AdjustmentsOutstanding output
Set @sql = 'Set	@CLASSRedeemOutstanding = (isnull((Select sum(points*ratio) from [RN1].[onlinehistorywork].dbo.portal_adjustments where tipnumber = ' + char(39) +@tip+ char(39) + ' and (trancode like ''R%'' or trancode in (''DR'',''DZ'')) and copyflag is null), 0))'
	EXECUTE sp_executesql @Sql, N'@CLASSRedeemOutstanding int OUTPUT', @CLASSRedeemOutstanding = @CLASSRedeemOutstanding output


--	SECTION 2 - TRACKING
--This will insert a record into [WorkOps].dbo.Cust_PointSynch to maintain a transaction history

--BEGIN TRAN

	set @sql =	
	'Insert into [WorkOps].dbo.Cust_PointSynch
	 Select tipnumber, Acctname1, Getdate()
	 from ['+@PattonDB+'].dbo.customer
	 where tipnumber = ' + char(39) +@tip+ char(39) + ''

--	Print @sql
	EXECUTE @rc = sp_executesql @Sql

	if @rc = 0
	BEGIN


--	SECTION 3 - SYNCHRONIZATION
--This will generate a sql statement that will match RN1 values to Patton values for the requested TIP
		Set @sql = 
		'Update	[RN1].['+@RNDB+'].dbo.customer
		Set		earnedbalance = ('+@Balance+' + '+@AdjustmentsOutstanding+'),
				redeemed = ('+@Redeemed+' + '+@RedeemedOutstanding+' - '+@CLASSRedeemOutstanding+'),
				availablebal = ('+@Available+' - '+@RedeemedOutstanding+' + '+@AdjustmentsOutstanding+' + '+@CLASSRedeemOutstanding+')
		where	tipnumber = ' + char(39) +@tip+ char(39) + ''

	/*  --Old format of script.  Extracted and replaced with new format
		Set @sql = 
		'Update [RN1].['+@RNDB+'].dbo.customer
		 Set	earnedbalance = ((Select runbalance from ['+@PattonDB+'].dbo.customer where tipnumber = '''+@tip+''') +
				    isnull((Select sum(points*ratio) from [RN1].[onlinehistorywork].dbo.portal_adjustments where tipnumber = '''+@tip+''' and copyflag is null), 0)),
			redeemed = ((Select runredeemed from ['+@PattonDB+'].dbo.customer where tipnumber = '''+@tip+''') +
				    isnull((Select sum(points) from [RN1].['+@RNDB+'].dbo.onlhistory where tipnumber = '''+@tip+''' and copyflag is null), 0)),
			availablebal = ((Select runavailable from ['+@PattonDB+'].dbo.customer where tipnumber = '''+@tip+''') -
					isnull((Select sum(points) from [RN1].['+@RNDB+'].dbo.onlhistory where tipnumber = '''+@tip+''' and copyflag is null), 0) +
					isnull((Select sum(points*ratio) from [RN1].[onlinehistorywork].dbo.portal_adjustments where tipnumber = '''+@tip+''' and copyflag is null), 0))
		where	tipnumber = '''+@tip+''''
	*/

--		Print @sql
		EXECUTE @rc = sp_executesql @Sql
	END

--	if @rc = 0
--		COMMIT TRAN
--	else
--		ROLLBACK TRAN
end

else
Begin
	Print 'Database is in process, do not run this script'
end

--Intial development RBM 03/20/2008
--Updated script to adjust for any CLASS adjustments not pushed to Patton RBM 03/21/2008
--Updated script to count null results as zeros in calculating point totals for update RBM 03/21/2008
--Updated script to eliminate error in script and to reduce runtime needed RBM 07/15/2008