--Returns a count by database of customers on Patton that are not on RN1

--Create storage table for search
--Drop table [WorkOps].dbo.Cust_BalanceError
--Create table [WorkOps].dbo.Cust_BalanceError (DBName varchar(100) not null, Error_Count int null)
Truncate table [WorkOps].dbo.Cust_BalanceError

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
Declare curDBName2 cursor 
for Select dbnameNEXL from rewardsnow.dbo.dbprocessinfo
Declare @DBName varchar(100)
Declare @DBName2 varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
open curDBName2
Fetch next from curDBName2 into @DBName2
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @dbname2 = rtrim(@dbname2)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_BalanceError
		 Select '''+@DBName+''' as DBName, count(*) as error_count
		 from ['+@DBName+'].dbo.customer
		 where tipnumber not in (Select tipnumber from [RN1].['+@DBName2+'].dbo.customer)'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	  fetch Next from curDBName2 into @DBName2
	end
Close curDBName
Deallocate curDBName
Close curDBName2
Deallocate curDBName2

Select *
From WorkOps.dbo.Cust_BalanceError
where Error_Count > 0
order by DBName

--Select Count(*) from customer where tipnumber in (
--Select customer.Tipnumber from customer join history on customer.tipnumber = history.tipnumber
--Group by customer.tipnumber, runavailable
--Having Sum(points*ratio) <> runavailable)