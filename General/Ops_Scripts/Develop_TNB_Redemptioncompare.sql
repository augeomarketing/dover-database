-- Agent = report type 'R'
-- Processed = report type not 'R'
/*
Select * from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')

Select	trancode, trandesc, count(trandesc) 
from	main 
where	tipfirst in (Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
group by trancode, trandesc
order by trandesc

Select trancode, trandesc, count(trandesc) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
group by trancode, trandesc
order by trandesc
*/
--Establish timeframe for comparison run

declare @today datetime
declare @startdate datetime
declare @enddate datetime

set @today = getdate()

set @startdate = cast( year(@today) -1 as varchar(4)) + '/' + right('00' + cast( month(@today) as varchar(2)), 2) + '/' +
                        right('00' +cast( day(@today) as varchar(2)), 2) + ' 00:00:00'

set @enddate = cast( year(@today) as varchar(4)) + '/' + right('00' + cast( month(@today) as varchar(2)), 2) + '/' +
                        right('00' +cast( day(@today) as varchar(2)), 2) + ' 00:00:00'

select @today as rundate, @startdate as startdate, @enddate as enddate


--Declare container fields for all counts

Declare @bonusa varchar(10),@bonusp varchar(10),@bonusa_points varchar(10),@bonusp_points varchar(10)
Declare @mercha varchar(10),@merchp varchar(10),@mercha_points varchar(10),@merchp_points varchar(10)
Declare @charitya varchar(10),@charityp varchar(10),@charitya_points varchar(10),@charityp_points varchar(10)
Declare @gifta varchar(10),@giftp varchar(10),@gifta_points varchar(10),@giftp_points varchar(10)
Declare @brochurea varchar(10),@brochurep varchar(10),@brochurea_points varchar(10),@brochurep_points varchar(10)
Declare @downloada varchar(10),@downloadp varchar(10),@downloada_points varchar(10),@downloadp_points varchar(10)
Declare @travela varchar(10),@travelp varchar(10),@travela_points varchar(10),@travelp_points varchar(10)

--Calculate Redemption counts for all categories of redemption, by Agent or Processed

Set @bonusa =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'bonus')

Set @bonusp =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'bonus')

Set @mercha =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'merch')

Set @merchp =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'merch')

Set @charitya =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RG')

Set @charityp =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RG')

Set @gifta =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RC')

Set @giftp =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RC')

Set @brochurea =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RQ')

Set @brochurep =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RQ')

Set @downloada =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RD')

Set @downloadp =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RD')

Set @travela =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode in ('RT','RU','RV'))

Set @travelp =
(Select Count(tipnumber) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode in ('RT','RU','RV'))

--Calculate Redemption point totals for all categories of redemption, by Agent or Processed

Set @bonusa_points =
(Select sum(points) 
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'bonus')

Set @bonusp_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'bonus')

Set @mercha_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'merch')

Set @merchp_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trandesc = 'merch')

Set @charitya_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RG')

Set @charityp_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RG')

Set @gifta_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RC')

Set @giftp_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RC')

Set @brochurea_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RQ')

Set @brochurep_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RQ')

Set @downloada_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RD')

Set @downloadp_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode = 'RD')

Set @travela_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype = 'R')
  and	histdate between @startdate and @enddate
  and	trancode in ('RT','RU','RV'))

Set @travelp_points =
(Select sum(points)  
from Fullfillment.dbo.main where tipfirst in 
(Select tipfirst from tnb.dbo.assoc where reporttype <> 'R')
  and	histdate between @startdate and @enddate
  and	trancode in ('RT','RU','RV'))


--Drop table #tempredeemcounts
Create table #tempredeemcounts (Type varchar(25), Agent_Count varchar(10), Processed_Count varchar(10), Agent_Points varchar(10), Processed_Points varchar(10))
Insert into #tempredeemcounts Select 'Bonus', @bonusa, @bonusp, @bonusa_points, @bonusp_points
Insert into #tempredeemcounts Select 'Merchandise', @mercha, @merchp, @mercha_points, @merchp_points
Insert into #tempredeemcounts Select 'Charity', @charitya, @charityp, @charitya_points, @charityp_points
Insert into #tempredeemcounts Select 'Gift Card', @gifta, @giftp, @gifta_points, @giftp_points
Insert into #tempredeemcounts Select 'Brochure', @brochurea, @brochurep, @brochurea_points, @brochurep_points
Insert into #tempredeemcounts Select 'Downloadable', @downloada, @downloadp, @downloada_points, @downloadp_points
Insert into #tempredeemcounts Select 'Travel', @travela, @travelp, @travela_points, @travelp_points

Select * from #tempredeemcounts order by Type