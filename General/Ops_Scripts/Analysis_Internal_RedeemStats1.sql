Select
	'TRAVEL'			as Type,
	year(histdate)		as Red_Year,
	month(histdate)		as Red_Month,
	Sum(catalogqty)		as Count_Redeem,
	Sum(m.points*catalogqty)		as Points_Redeem
From	Fullfillment.dbo.main m 
where	Trancode in ('RT','RU','RV') and histdate >= '1/1/2007'
group by	year(histdate),month(histdate)
order by	year(histdate),month(histdate)

Select
	'GIFT CARD'			as Type,
	year(histdate)		as Red_Year,
	month(histdate)		as Red_Month,
	Sum(catalogqty)		as Count_Redeem,
	Sum(m.points*catalogqty)		as Points_Redeem
From	Fullfillment.dbo.main m 
where	Trancode in ('RC') and histdate >= '1/1/2007'
group by	year(histdate),month(histdate)
order by	year(histdate),month(histdate)

Select
	'MERCHANDISE'		as Type,
	year(histdate)		as Red_Year,
	month(histdate)		as Red_Month,
	Sum(catalogqty)		as Count_Redeem,
	Sum(m.points*catalogqty)		as Points_Redeem
From	Fullfillment.dbo.main m 
where	Trancode in ('RM') and histdate >= '1/1/2007'
group by	year(histdate),month(histdate)
order by	year(histdate),month(histdate)

Select
	'OTHER'			as Type,
	year(histdate)		as Red_Year,
	month(histdate)		as Red_Month,
	Sum(catalogqty)		as Count_Redeem,
	Sum(m.points*catalogqty)		as Points_Redeem
From	Fullfillment.dbo.main m 
where	Trancode not in ('RC','RM','RT','RU','RV','RQ') and left(m.trancode, 1) <> 'D' and histdate >= '1/1/2007'
group by	year(histdate),month(histdate)
order by	year(histdate),month(histdate)