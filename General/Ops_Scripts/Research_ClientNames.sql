Select * from rewardsnow.dbo.rptctlclients where clientnum like '3%' order by clientnum
Select * from rewardsnow.dbo.clientdata where tipfirst like '3%' order by tipfirst
Select * from rewardsnow.dbo.dbprocessinfo where dbnumber like '3%' order by dbnumber
Select * from login.dbo.client where tipfirst like '3%' order by tipfirst
Select * from [rn1].rewardsnow.dbo.searchdb where tipfirst like '3%' order by tipfirst

Select	i.dbnumber		as tipfirst,
		c.clientcode	as c_clientcode,	c.clientname	as c_clientname,	c.description as c_clientdescription,
		r.rnname		as r_clientcode,	r.FormalName	as r_clientname,
		d.clientname	as d_clientcode,
		s.clientname	as s_clientcode
from				rewardsnow.dbo.dbprocessinfo	i
	left outer join	login.dbo.client				c	on i.dbnumber = c.tipfirst
	left outer join	rewardsnow.dbo.rptctlclients	r	on i.dbnumber = r.clientnum
	left outer join	rewardsnow.dbo.clientdata		d	on i.dbnumber = d.tipfirst
	left outer join	[rn1].rewardsnow.dbo.searchdb	s	on i.dbnumber = s.tipfirst
where	i.dbnumber <> 'BQT'
order by i.dbnumber

Select	i.dbnumber		as tipfirst,
		r.FormalName	as FormalName_rptctlclients,
		d.clientname	as clientname_clientdata,
		l.clientname	as clientname_login,
		s.clientname	as cllientname_searchdb
from				rewardsnow.dbo.dbprocessinfo	i
	left outer join	rewardsnow.dbo.rptctlclients	r	on i.dbnumber = r.clientnum
	left outer join	rewardsnow.dbo.clientdata		d	on i.dbnumber = d.tipfirst
	left outer join	login.dbo.client				l	on i.dbnumber = l.tipfirst
	left outer join	[rn1].rewardsnow.dbo.searchdb	s	on i.dbnumber = s.tipfirst
where	i.dbnumber <> 'BQT'
order by i.dbnumber

Select * from	rewardsnow.dbo.clienttemp