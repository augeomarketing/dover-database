--Checks customer submitted TIP and Acctid and returns information needed to send to FI for confirmation of request

Declare	@TIP varchar (15)
Declare	@Acct varchar (6)

Set	@TIP	= '601000000001934'
Set	@Acct	= '792209'


Select	Tipnumber, rtrim(acctname1) as Name1, rtrim(acctname2) as Name2,
	rtrim(address1) as address, rtrim(address4) as CityStateZip, rtrim(homephone) as phone
from	customer 
where	tipnumber = @TIP

Select	c.Tipnumber, right(rtrim(acctid), 6) as lastsix, rtrim(acctname1) as Name1, rtrim(acctname2) as Name2,
	rtrim(address1) as address, rtrim(address4) as CityStateZip, rtrim(homephone) as phone
from	customer c join affiliat a on c.tipnumber = a.tipnumber 
where	right(acctid, 6) = @Acct

