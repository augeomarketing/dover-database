--Returns monthly statistical data on redemption levels based on Onlinehistorywork records
--This should be run against the Patton DB for greater accuracy

Select	left(tipnumber, 3) as TIPFirst,
	ClientName as ClientName,
	Year(histdate)	as [Year],
	month(histdate)	as [Month],
	Count(points)	as Count_Redeem,
	Avg(points)	as Average_Redeem,
	Sum(points)	as Total_Redeem,
	Max(points)	as Max_Redeem,
	Min(points)	as Min_Redeem
From	onlinehistorywork.dbo.onlhistory o left outer join fullfillment.dbo.subclient c
		on left(o.tipnumber, 3) = c.tipfirst
where	points > '0' and left(tipnumber,1 ) <> '9' and tipnumber not like '%999999%' and histdate >= '1/1/2007'
group by	left(tipnumber, 3), clientname, Year(Histdate), month(histdate)
order by	left(tipnumber, 3), Year(Histdate), month(histdate)
