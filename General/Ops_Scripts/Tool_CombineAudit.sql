--Script used to confirm that customer accounts were closed correctly and new account has proper point totals
--for a combine

Select *
From customerdeleted
where tipnumber in ('402000000002863','402000000002630')

Select Sum(Runavailable)
From customerdeleted
where tipnumber in ('402000000002863','402000000002630')

Select *
From customer
where tipnumber = '402000000018690'

Select *
From [RN1].Onebridge.dbo.customer
where tipnumber in ('402000000002863','402000000002630','402000000018690')