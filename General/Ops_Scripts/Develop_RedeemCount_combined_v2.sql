--Create storage table for search
/*
Drop table [Workops].dbo.Redemptions_DailyCount
Create table [Workops].dbo.Redemptions_DailyCount	(DBName_Patton varchar(100) null,
													 TipFirst varchar(3) null,
													 r_redeempoint int null,
													 r_redeemcount int null,
													 r_decreasepoint int null,
													 r_decreasecount int null,
													 f_redeempoint int null,
													 f_redeemcount int null,
													 f_decreasepoint int null,
													 f_decreasecount int null,
													 h_redeempoint int null,
													 h_redeemcount int null,
													 h_decreasepoint int null,
													 h_decreasecount int null)
*/
Truncate Table [Workops].dbo.Redemptions_DailyCount
--Cursor through DBProcessinfo and get number of customer records
Declare curtipfirst cursor
for Select [dbnumber] from rewardsnow.dbo.dbprocessinfo

Declare @TIPfirst varchar (3)
Declare @DBNamePatton varchar(100)
Declare @DBNameNEXL varchar(100)

Declare @month varchar(2)
Declare @day varchar(2)
Declare @year varchar(4)

Declare @sqlcmd nvarchar(4000)

Declare @r_redeempoint int
Declare @r_redeemcount int
Declare @r_decreasepoint int
Declare @r_decreasecount int
Declare @f_redeempoint int
Declare @f_redeemcount int
Declare @f_decreasepoint int
Declare @f_decreasecount int
Declare @h_redeempoint int
Declare @h_redeemcount int
Declare @h_decreasepoint int
Declare @h_decreasecount int


open curtipfirst
Fetch next from curtipfirst into @TIPfirst
while @@Fetch_Status = 0
	Begin
	  set @dbnamepatton = (Select rtrim(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)
	  set @dbnamenexl = (Select rtrim(dbnamenexl) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)
	  set @month = '01'
	  set @day = '19'
	  set @year = '2009'
--Set FI Points and Count in ONLhistory
	  Set @sqlcmd = N'Set	@r_redeempoint = (Select isnull(Sum(points*catalogqty), 0) from [167395-db1].['+@dbnamenexl+'].dbo.onlhistory where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@r_redeempoint int OUTPUT', @r_redeempoint = @r_redeempoint output
	  Set @sqlcmd = N'Set	@r_redeemcount = (Select isnull(Count(points), 0) from [167395-db1].['+@dbnamenexl+'].dbo.onlhistory where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@r_redeemcount int OUTPUT', @r_redeemcount = @r_redeemcount output
	  Set @r_decreasepoint = 0
	  Set @r_decreasecount = 0
--Add in FI Points and Counts from CLASS
	  Set @r_redeempoint =	@r_redeempoint + (Select	isnull(Sum(points), 0) from [167395-db1].onlinehistorywork.dbo.portal_adjustments where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode like 'R%' and tipnumber not like '%999999%')
	  Set @r_redeemcount =	@r_redeemcount + (Select	isnull(Count(points), 0) from [167395-db1].onlinehistorywork.dbo.portal_adjustments	where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode like 'R%' and tipnumber not like '%999999%')
	  Set @r_decreasepoint = @r_decreasepoint + (Select	isnull(Sum(points), 0) from [167395-db1].onlinehistorywork.dbo.portal_adjustments where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode in ('DR','DZ') and tipnumber not like '%999999%')
	  Set @r_decreasecount = @r_decreasecount + (Select	isnull(Count(points), 0) from [167395-db1].onlinehistorywork.dbo.portal_adjustments where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode in ('DR','DZ') and tipnumber not like '%999999%')
--Set FI Points and Counts for Fulfillment
	  Set @f_redeempoint =	(Select isnull(Sum(points*catalogqty), 0) from fullfillment.dbo.main where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode like 'R%' and tipnumber not like '%999999%')
	  Set @f_redeemcount =	(Select isnull(Count(points), 0) from fullfillment.dbo.main	where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode like 'R%' and tipnumber not like '%999999%')
	  Set @f_decreasepoint = (Select isnull(Sum(points*catalogqty), 0) from fullfillment.dbo.main where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode in ('DR','DZ') and tipnumber not like '%999999%')
	  Set @f_decreasecount = (Select isnull(Count(points), 0) from fullfillment.dbo.main where day(histdate) = @day and month(histdate) = @month and year(histdate) = @year and tipfirst = @tipfirst and trancode in ('DR','DZ') and tipnumber not like '%999999%')
--Set FI Points and Counts for History
	  Set @sqlcmd = N'Set	@h_redeempoint = (Select isnull(Sum(points), 0) from ['+@dbnamepatton+'].dbo.history where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode like ''R%'' and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_redeempoint int OUTPUT', @h_redeempoint = @h_redeempoint output
	  Set @sqlcmd = N'Set	@h_redeemcount = (Select isnull(Count(points), 0) from ['+@dbnamepatton+'].dbo.history where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode like ''R%'' and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_redeemcount int OUTPUT', @h_redeemcount = @h_redeemcount output
	  Set @sqlcmd = N'Set	@h_decreasepoint = (Select isnull(Sum(points), 0) from ['+@dbnamepatton+'].dbo.history where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode in (''DR'',''DZ'') and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_decreasepoint int OUTPUT', @h_decreasepoint = @h_decreasepoint output
	  Set @sqlcmd = N'Set	@h_decreasecount = (Select isnull(Count(points), 0) from ['+@dbnamepatton+'].dbo.history where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode in (''DR'',''DZ'') and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_decreasecount int OUTPUT', @h_decreasecount = @h_decreasecount output
--add FI Points and Counts for Historydeleted
	  Set @sqlcmd = N'Set	@h_redeempoint = @h_redeempoint + (Select isnull(Sum(points), 0) from ['+@dbnamepatton+'].dbo.historydeleted where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode like ''R%'' and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_redeempoint int OUTPUT', @h_redeempoint = @h_redeempoint output
	  Set @sqlcmd = N'Set	@h_redeemcount = @h_redeemcount + (Select isnull(Count(points), 0) from ['+@dbnamepatton+'].dbo.historydeleted where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode like ''R%'' and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_redeemcount int OUTPUT', @h_redeemcount = @h_redeemcount output
	  Set @sqlcmd = N'Set	@h_decreasepoint = @h_decreasepoint + (Select isnull(Sum(points), 0) from ['+@dbnamepatton+'].dbo.historydeleted where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode in (''DR'',''DZ'') and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_decreasepoint int OUTPUT', @h_decreasepoint = @h_decreasepoint output
	  Set @sqlcmd = N'Set	@h_decreasecount = @h_decreasecount + (Select isnull(Count(points), 0) from ['+@dbnamepatton+'].dbo.historydeleted where day(histdate) = '+@day+' and month(histdate) = '+@month+' and year(histdate) = '+@year+' and left(tipnumber, 3) = '''+@tipfirst+''' and trancode in (''DR'',''DZ'') and tipnumber not like ''%999999%'')'
		EXECUTE sp_executesql @Sqlcmd, N'@h_decreasecount int OUTPUT', @h_decreasecount = @h_decreasecount output

Insert into [Workops].dbo.Redemptions_DailyCount
(DBName_Patton,TipFirst,
r_redeempoint,r_redeemcount,r_decreasepoint,r_decreasecount,
f_redeempoint,f_redeemcount,f_decreasepoint,f_decreasecount,
h_redeempoint,h_redeemcount,h_decreasepoint,h_decreasecount)
Values
(@DBNamePatton, @TIPfirst,
@r_redeempoint,@r_redeemcount,@r_decreasepoint,@r_decreasecount,
@f_redeempoint,@f_redeemcount,@f_decreasepoint,@f_decreasecount,
@h_redeempoint,@h_redeemcount,@h_decreasepoint,@h_decreasecount)
	  fetch Next from curtipfirst into @TIPfirst
	end
Close curtipfirst
Deallocate curtipfirst

/*

Select * from [Workops].dbo.Redemptions_DailyCount
where (r_redeempoint <> f_redeempoint or r_redeempoint <> h_redeempoint or f_redeempoint <> h_redeempoint)
or (r_redeemcount <> f_redeemcount or r_redeemcount <> h_redeemcount or f_redeemcount <> h_redeemcount)
or (r_decreasepoint <> f_decreasepoint or r_decreasepoint <> h_decreasepoint or f_decreasepoint <> h_decreasepoint)
or (r_decreasecount <> f_decreasecount or r_decreasecount <> h_decreasecount or f_decreasecount <> h_decreasecount)
order by tipfirst



Select * from [Workops].dbo.Redemptions_DailyCount
order by tipfirst

*/