Declare @tipfirst varchar(3)
set @tipfirst = '634'

--Begin tran
--Populate the PointsNOW Client Table from the FI client table
Insert into [login].dbo.client
(ClientCode,ClientName,Description,TipFirst,Address1,Address2,Address3,Address4,City,State,Zipcode,CountryCode,Phone1,
Phone2,Fax,ContactPerson1,ContactPerson2,ContactEmail1,ContactEmail2,DateJoined,ClientTypeCode,RNProgramName,Ad1,Ad2,
TermsConditions,PointsUpdatedDT,MinRedeemNeeded,TravelFlag,MerchandiseFlag,TravelIncMinPoints,MerchandiseBonusMinPoints,
MaxPointsPerYear,PointExpirationYears,ClientID,Pass,ClientLogo,ServerName,DbName,UserName,Password,PointExpire)

Select ClientCode,ClientName,Description,TipFirst,Address1,Address2,Address3,Address4,City,State,Zipcode,
null as CountryCode,Phone1,Phone2,null as Fax,ContactPerson1,ContactPerson2,ContactEmail1,ContactEmail2,DateJoined,
null as ClientTypeCode,RNProgramName,null as Ad1,null as Ad2,TermsConditions,PointsUpdatedDT,MinRedeemNeeded,TravelFlag,
MerchandiseFlag,TravelIncMinPoints,MerchandiseBonusMinPoints,MaxPointsPerYear,PointExpirationYears,ClientID,Pass,
null as ClientLogo,ServerName,DbName,UserName,Password,PointsExpire as pointexpire
From client
where tipfirst = @tipfirst

--Populate the PointsNOW Database_Table Table
insert into [login].dbo.database_table
(DatabseId,ServerName,DbName,UserName,Password,ClientCode)
Select (Select max(cast(databseid as int))+1 from [login].dbo.database_table) as DatabaseId, servername, DbName, username, password, clientcode
from [login].dbo.client
where tipfirst = @tipfirst


--Populate the PointsNOW UserClient Table
insert into [login].dbo.userclient
(UserId, ClientId, databaseId)
Select 'IT-DEPT' as UserId, clientid, null as databaseid
from [login].dbo.client
where tipfirst = @tipfirst

insert into [login].dbo.userclient
(UserId, ClientId, databaseId)
Select 'CUST-SERV' as UserId, clientid, null as databaseid
from [login].dbo.client
where tipfirst = @tipfirst

--commit tran
--rollback tran
