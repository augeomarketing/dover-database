--Script to check errorredemption for outstanding issues
/* --Check errorredemption for specified date
Select tipnumber,histdate,points,trandesc,catalogcode,catalogdesc,catalogqty,source,notes,transid
from onlinehistorywork.dbo.errorredemption 
where	Day(histdate) = '' 
	and	Month(histdate) = '' 
	and Year(histdate) = '' 
	and tipnumber not like '%999999%' 
order by histdate desc
*/

--Check errorredemption for previous day
Select tipnumber,histdate,points,trandesc,catalogcode,catalogdesc,catalogqty,source,notes,transid
from onlinehistorywork.dbo.errorredemption 
where	Day(histdate) = Day(getdate() -1) 
	and	Month(histdate) = Month(getdate() -1) 
	and Year(histdate) = Year(getdate() -1) 
	and tipnumber not like '%999999%' 
order by histdate desc

--This script will identify redemptions failed due to closed account or expired points
Declare @TIP varchar(15)
Set @TIP = ''

Select e.tipnumber, 'TIP was closed',e.histdate as redeem_date, c.datedeleted
from onlinehistorywork.dbo.errorredemption e join customerdeleted c on e.tipnumber = c.tipnumber
where	e.tipnumber = @TIP

Select	e.Tipnumber, e.histdate as err_date, e.trancode as err_trancode, catalogcode, catalogdesc, catalogqty, e.points as err_points,
		acctname1, runbalance, runredeemed, runavailable, 
		h.histdate as exp_date, h.points as exp_points,
		(e.points*catalogqty) - runavailable as points_short
from	onlinehistorywork.dbo.errorredemption e
	join	customer c	on e.tipnumber = c.tipnumber
	join	history h	on e.tipnumber = h.tipnumber
where	e.tipnumber = @TIP and h.trancode = 'xp' and h.histdate >= e.histdate - 60--31
order by e.histdate desc
