--Drop table [WorkOps].dbo.Cust_PointsOut
--Create table [WorkOps].dbo.Cust_PointsOut (DBName varchar(100) not null, Points_Outstanding bigint null, Rundate datetime)
--Truncate table [WorkOps].dbo.Cust_PointsOut

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where dbnumber not like '9%'

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)
Declare @startdate varchar(25)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @startdate = '09/01/2009 00:00:00.000'
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_PointsOut
		 Select '''+@DBName+''' as DBName, 
				((select isnull(sum(points*ratio), 0) from ['+@DBName+'].dbo.history where histdate < '''+@startdate+''') + (select	isnull(sum(points*ratio), 0) from ['+@DBName+'].dbo.historydeleted where (histdate < '''+@startdate+''') and (datedeleted >= '''+@startdate+'''))) as Points_Outstanding, 
				convert(nvarchar(25),(Dateadd(millisecond, -3, '''+@startdate+''')),121) as rundate'
--Print @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_PointsOut
order by DBName

Select rundate, sum(Points_Outstanding) as Points_Outstanding
from [WorkOps].dbo.Cust_PointsOut
group by rundate
order by rundate

Select * 
from [WorkOps].dbo.Cust_PointsOut
where rundate = '2008-05-31 23:59:59.997'