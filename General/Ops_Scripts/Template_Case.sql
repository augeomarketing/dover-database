Select
Tipnumber as TIP,
rtrim(Name1) as Name1,
rtrim(Name2) as Name2,
(Right(rtrim(main.itemnumber), 3)*Catalogqty) as Amount,
rtrim(ItemDesc) as Gift_Card_Type,
Catalogqty as Quantity,
Histdate as Redemption_Date,
  CASE
    WHEN saddress2 is null THEN rtrim(sAddress1)
    ELSE rtrim(sAddress1)+' '+ rtrim(saddress2)
  END as address,
rtrim(scity) as City,
rtrim(sstate) as State,
rtrim(szip) as Zip
from main join subitem on main.itemnumber = subitem.itemnumber
where tipfirst in (002, 003) and main.trancode = 'RC' and redstatus = '0'
   and main.itemnumber not like '%Ringtone%' and main.itemnumber not like '%Tune%'
   and main.itemnumber not like '%QG%' and main.itemnumber not like '%quickgift%'
order by histdate