--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Update [Fullfillment].dbo.Main

		 set	[Fullfillment].dbo.Main.saddress1 = Upper(['+@DBName+'].dbo.customer.address1),
			[Fullfillment].dbo.Main.saddress2 = Upper(['+@DBName+'].dbo.customer.address2),
			[Fullfillment].dbo.Main.scity = Upper(['+@DBName+'].dbo.customer.city),
			[Fullfillment].dbo.Main.sstate = Upper(['+@DBName+'].dbo.customer.state),
			[Fullfillment].dbo.Main.szip = Upper(['+@DBName+'].dbo.customer.zipcode)

		From	['+@DBName+'].dbo.customer

		 where	[Fullfillment].dbo.Main.tipnumber = ['+@DBName+'].dbo.customer.tipnumber
		    and	[Fullfillment].dbo.Main.trancode = ''RQ''
		    and (['+@DBName+'].dbo.customer.state is not null and ['+@DBName+'].dbo.customer.state <> '''')
		    and([Fullfillment].dbo.Main.sstate is null or [Fullfillment].dbo.Main.sstate = '''')'
  	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName