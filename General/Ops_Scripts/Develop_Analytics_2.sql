Select Year(dateadded)Y, month(dateadded) M, count(distinct tipnumber) New_TIPs
from customer
group by Year(dateadded), month(dateadded)
union
Select Year(dateadded)Y, month(dateadded) M, count(distinct tipnumber) New_TIPs
from customerdeleted
where statusdescription not like '%combine%'
group by Year(dateadded), month(dateadded)
order by Year(dateadded), month(dateadded)

create table #tempcounts (Y int, M int, Points_Earned int, Point_Count int, Points_Average int, Customer_Count int)
--truncate table #tempcounts
insert into #tempcounts
Select	Year(histdate) Y, month(histdate) M, 
		isnull(sum((points+isnull(overage, 0))*ratio), 0) as Points_Earned, 
		count(distinct tipnumber) as Point_Count, 
		Convert(numeric(8,0),isnull(sum((points+isnull(overage, 0))*ratio), 0)/count(distinct tipnumber)) as Points_Average,
		(Select Count(tipnumber) from customer 
		 where isnull(Statusdescription,'Blank') not like '%combine%' and
		 (year(dateadded) < Year(histdate) or 
		  (year(dateadded) = Year(histdate) and month(dateadded) <= month(histdate)))) as Customer_Count
from history
where trancode like '6%' --or trancode like '3%'
group by Year(histdate), month(histdate)
union
Select	Year(histdate) Y, month(histdate) M, 
		isnull(sum((points+isnull(overage, 0))*ratio), 0) as Points_Earned, 
		count(distinct tipnumber) as Point_Count, 
		Convert(numeric(8,0),isnull(sum((points+isnull(overage, 0))*ratio), 0)/count(distinct tipnumber)) as Points_Average,
		(Select Count(tipnumber) from customerdeleted 
		 where isnull(Statusdescription,'Blank') not like '%combine%' and
		 (year(dateadded) < Year(histdate) or 
		  (year(dateadded) = Year(histdate) and month(dateadded) <= month(histdate)))) as Customer_Count
from historydeleted
where trancode like '6%' --or trancode like '3%'
group by Year(histdate), month(histdate)

Select	Y, M, 
		Sum(Points_Earned) as Points_Earned,
		Sum(Point_Count) as Point_Count,
		Sum(Points_Earned)/Sum(Point_Count) as Points_Average,
		Sum(Customer_Count) as Customer_Count
From	#tempcounts
Group by Y, M
order by Y, M



Select year(datedeleted), month(datedeleted), count(Datedeleted)
from customerdeleted
where statusdescription not like '%combine%'
group by year(datedeleted), month(datedeleted)
order by year(datedeleted), month(datedeleted)

Select year(datedeleted), month(datedeleted), sum(points*ratio)
from historydeleted
group by year(datedeleted), month(datedeleted)
order by year(datedeleted), month(datedeleted)
