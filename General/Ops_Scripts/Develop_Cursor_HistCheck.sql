--Drop table [WorkOps].dbo.Hist_RecordCount
--Create table [WorkOps].dbo.Hist_RecordCount (DBName varchar(100) not null, Hist_Count int null, Hist_Sum bigint null)
truncate table [WorkOps].dbo.Hist_RecordCount
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
--where DBNameNEXL = 'NewTNB' or DBNameNEXL = 'Indiana'
--for Select name from master.dbo.sysdatabases
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Hist_RecordCount
		 Select '''+@DBName+''' as DBName, count(tipnumber) as Hist_Count, sum(points*ratio) as Hist_Sum
		 from ['+@DBName+'].dbo.History'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Hist_RecordCount
order by DBName

Select Count(*)
from [WorkOps].dbo.Hist_RecordCount
where dbname <> 'BQTest' and customer_count > '0'

Select Sum(customer_count)
from [WorkOps].dbo.Hist_RecordCount
where dbname not like '9%'
 and dbname not in ('594HarrisConsumer', 'testclass990')