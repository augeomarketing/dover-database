Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [50ABankUSA].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [50BMadisonvilleState].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [50CNorthHouston].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [50DCitizensNational].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [557ParkNationalConsumer].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [559CaliforniaNationalConsumer].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [561SanDiegoNationalConsumer].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate

UNION

Select	m.tipfirst, m.tipnumber, rtrim(m.name1) as Name1, isnull(rtrim(m.name2),'') as Name2, tc.TCID, m.points/100 as CashValue, 
		m.RedReqFulDate, stc.TCStatusName as Status, a.ACCTID 
from	fullfillment.dbo.main m join fullfillment.dbo.travelcert tc		on m.transid = tc.transid 
		join fullfillment.dbo.SubTravelCertStatus stc	on tc.TCStatus = stc.TCStatus
		join [563PacificNationalConsumer].dbo.AFFILIAT	a on m.TipNumber = a.TIPNUMBER
where	tc.tcstatus in ('1','2','3','4','5','6')
		and tipfirst in ('50A','50B','50C','50D','557','559','561','563')
		-- and redreqfuldate is not null
--order by tipfirst, histdate
order by tipfirst, RedReqFulDate

Select * from affiliat where tipnumber = '561000000002916'