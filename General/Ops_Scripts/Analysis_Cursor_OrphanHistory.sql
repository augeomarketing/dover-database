--Returns a record count by database of the number of History records that do not match a corresponding customer record

--Create storage table for search
--Drop table [WorkOps].dbo.Cust_OrphanedHistory
--Create table [WorkOps].dbo.Cust_OrphanedHistory (DBName varchar(100) not null, Error_Count int null)
Truncate table [WorkOps].dbo.Cust_OrphanedHistory

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_OrphanedHistory
		 Select '''+@DBName+''' as DBName, count(*) as error_count
		 from ['+@DBName+'].dbo.history
		 where tipnumber not in
			(Select tipnumber from ['+@DBName+'].dbo.customer)'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

--Finds which databases have orphaned records in them
Select *
From WorkOps.dbo.Cust_OrphanedHistory
Where error_count > 0
order by DBName