--Drop table #tmpresult
Select Distinct
		'582' as TIPFirst,
		c.acctname1 as NAME1, a.tipnumber as TIP_PRI, 
		d.acctname1 as NAME2, b.tipnumber as TIP_SEC,
		Getdate() as Histdate,
		'330' as USID,
		null as copyflagtransfered, null as copyflagcompleted
into #tmpresult
from affiliat a join affiliat b on a.custid = b.custid
	join customer c on a.tipnumber = c.tipnumber
	join customer d on b.tipnumber = d.tipnumber
where a.custid <> '00000000000' and a.tipnumber < b.tipnumber
--order by TIP_Pri, TIP_Sec
/*
Insert into #tmpresult
Select Distinct
		'582' as TIPFirst,
		c.acctname1 as NAME1, a.tipnumber as TIP_PRI, 
		d.acctname1 as NAME2, b.tipnumber as TIP_SEC,
		Getdate() as Histdate,
		'330' as USID,
		null as copyflagtransfered, null as copyflagcompleted
from affiliat a join affiliat b on a.secid = b.secid
	join customer c on a.tipnumber = c.tipnumber
	join customer d on b.tipnumber = d.tipnumber
where a.secid <> '000000000' and a.custid <> b.custid and a.tipnumber < b.tipnumber
--order by TIP_Pri, TIP_Sec
*/
declare @histdate datetime
set @histdate = getdate()

Update #tmpresult
set histdate = @histdate

Delete from #tmpresult where TIP_PRI in (Select TIP_SEC from #tmpresult)

Select count(*) from #tmpresult 
Select distinct * from #tmpresult order by TIP_PRI, TIP_SEC

Select * from [RN1].onlinehistorywork.dbo.portal_combines order by histdate desc
--Select * from [RN1].rewardsnow.dbo.portal_users

/*
Insert into [RN1].onlinehistorywork.dbo.portal_combines
(tipfirst,name1,tip_pri,name2,tip_sec,histdate,usid,copyflagtransfered,copyflagcompleted,transid)
select distinct tipfirst,name1,tip_pri,name2,tip_sec,histdate,usid,copyflagtransfered,copyflagcompleted,newid() as transid from #tmpresult
*/