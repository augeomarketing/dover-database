--Populates table with the Max TIPNumber from the customer table and the LastTIPUsed from the client table for comparison

--Drop table [WorkOps].dbo.Cust_LastTip
--Create table [WorkOps].dbo.Cust_LastTip (DBName varchar(100) not null, Cust varchar(15) null, CusDel varchar(15) null, Client_RecordCount int, Cust_Max varchar(15) null, Client_Last varchar(15) null)
Truncate table [WorkOps].dbo.Cust_LastTip

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_LastTip
		 Select
		 	(Select '''+@DBName+''') as DBName, 
			(Select max(tipnumber) from ['+@DBName+'].dbo.customer) as Cust,
			(Select max(tipnumber) from ['+@DBName+'].dbo.customerdeleted) as CustDel,
			(Select Count(*) from ['+@DBName+'].dbo.client) as Client_RecordCount,
		 	(Case
				When (Select max(tipnumber) from ['+@DBName+'].dbo.customer)>=(Select Isnull(max(tipnumber), 0) from ['+@DBName+'].dbo.customerdeleted)
					then (Select max(tipnumber) from ['+@DBName+'].dbo.customer)
				Else (Select max(tipnumber) from ['+@DBName+'].dbo.customerdeleted)
			end) as Cust_Max,
		 	(Select lasttipnumberused from Rewardsnow.dbo.dbprocessinfo where dbnamepatton = '''+@DBName+''') as Client_Last'
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

--Analysis scripts for table
Select *
From WorkOps.dbo.Cust_LastTip
order by DBName

Select *
From WorkOps.dbo.Cust_LastTip
where Cust_Max = Client_Last
order by DBName

Select dbname, Cust_Max, Client_Last
From WorkOps.dbo.Cust_LastTip
where Cust_Max > isnull(Client_Last, 0)
order by DBName

Select * from rewardsnow.dbo.DBProcessinfo where dbnamepatton not in 
(Select DBname from WorkOps.dbo.Cust_LastTip)