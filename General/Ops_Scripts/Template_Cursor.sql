Drop table [WorkOps].dbo.Cust_RecordCount
Create table [WorkOps].dbo.Cust_RecordCount (DBName varchar(100) not null, customer_count int null)

Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.dbo.sysdatabases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_RecordCount
		 Select '''+@DBName+''' as DBName, count(*) as customer_count
		 from ['+@DBName+'].dbo.customer
		 where status <> ''X'''
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Select *
From [WorkOps].dbo.Cust_RecordCount
order by DBName

Select sum(customer_count)
From [WorkOps].dbo.Cust_RecordCount
