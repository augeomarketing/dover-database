--Drop table [WorkOps].dbo.Cust_PartCounts
--Create table [WorkOps].dbo.Cust_PartCounts (DBName varchar(100) not null, Part_Year int, Part_Month int,Part_Count bigint)
truncate table [WorkOps].dbo.Cust_PartCounts
--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor 
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)
--Drop table #tempresult1
Create table #tmpresult1 (Part_Year int, Part_Month int,Part_Count bigint)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
		Truncate table #tmpresult1
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into #tmpresult1
		 Select year(dateadded) as Part_Year, month(dateadded) as Part_Month, Count(tipnumber) as Part_Count
		 from ['+@DBName+'].dbo.customer
--		 where dateadded >= ''1/1/2007''
		 group by year(dateadded), month(dateadded)
			union all
		 Select year(dateadded) as Part_Year, month(dateadded) as Part_Month, Count(tipnumber) as Part_Count
		 from ['+@DBName+'].dbo.customerdeleted
--		 where dateadded >= ''1/1/2007''
		 group by year(dateadded), month(dateadded)'
	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_PartCounts
		 Select '''+@DBName+''' as DBName, Part_Year, Part_Month, sum(Part_Count)
		 from #tmpresult1
		 group by Part_Year, Part_Month
		 order by Part_Year, Part_Month'
	  EXECUTE sp_executesql @SqlCmd

	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

/*
Select *
From [WorkOps].dbo.Cust_PartCounts
order by DBName

Select Part_Year,Part_Month, Sum(Part_Count) as Part_Count
from [WorkOps].dbo.Cust_PartCounts
where dbname not in ('BQTest','TESTCLASS990') and left(dbname, 1) <> '9'
group by Part_Year,Part_Month
order by Part_Year,Part_Month
*/
