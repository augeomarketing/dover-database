Select h.tipnumber,
	(Select isnull(sum(points*ratio), 0) from history where ratio > '0' and tipnumber = h.tipnumber) as Adds,
	(Select isnull(sum(points*ratio), 0) from history where ratio < '0' and trancode not like 'R%' and tipnumber = h.tipnumber)as Subtracts,
	(Select isnull(sum(points*ratio), 0) from history where trancode like 'R%' and tipnumber = h.tipnumber) as Redeems
From history h
group by h.tipnumber
Having	(Select isnull(sum(points*ratio), 0) from history where ratio > '0' and tipnumber = h.tipnumber) <
	(Select isnull(sum(points), 0) from history where trancode like 'R%' and tipnumber = h.tipnumber)

/*
having	(Select isnull(sum(points*ratio), 0) from history where trancode not like 'R%' and tipnumber = h.tipnumber) > '0'
   and	(Select isnull(sum(points*ratio), 0) from history where tipnumber = h.tipnumber) < '0'
*/
