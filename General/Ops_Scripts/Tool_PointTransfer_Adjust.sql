--Updates the records of a customer to reflect points transferred from a closed to an open account

Declare @TIPLive varchar(15), @points int, @TipDeleted varchar(15)

Set	@TipLive = '511000000000810'
Set	@TIPDeleted = '511000000000672'
Set	@points = '379'

--Inserts a record into the history table to record transfer of points from a closed TIPnumber
--If command inserted to prevent history record being created if script is looking at wrong database (RBM 2/15/08)

If	Left(@TipLive, 3) = (Select top 1 Left(tipnumber, 3) from customer)
Begin
	Insert into history
	(tipnumber,Acctid,Histdate,trancode,trancount,Points,[Description],Secid,ratio,Overage)
	Select
		@TipLive,
		null,--(Select top 1 acctid from history where tipnumber = @TipLive),
		getdate(),
		'IE',
		'1',
		@points,
		'Point Transfer from ' + @TIPDeleted,
		null,--(Select top 1 secid from history where tipnumber = @TipLive),
		'1.0',
		'0'

--Updates the customer record to reflect transfer of points from a closed TIPnumber

	Update	customer
	Set	runbalance = (runbalance + @points), runavailable = (runavailable + @points)
	where	tipnumber = @TipLive

--Displays the customer and history records for confirmation that data was updated correctly

	Select * from customer where tipnumber = @TipLive
	Select * from History where tipnumber = @TipLive order by histdate desc
end

else
Begin
	Print 'You are in the wrong database'
end