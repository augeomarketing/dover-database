#!perl

##ppm install File-Copy
##ppm install File-Path
##ppm install File-Type
##ppm install Archive-Extract

#Parameters:
#source - File path or directory path to source files.  Wild cards are acceptable.  UNC path is preferable. Required.
#targetDir - Path to destination directory. Required
#archiveDir - Path to archiveDir directory.  Optional.  If none provided, source files will be archived in source directory.
#decryptDir - Path to directory where automated decrypt routine runs.  Optional.  Default= \\patton\ops\ops_decrypt_pgp.
#copyonly - Flag setting to prevent moving of files.  Files are only copied.  Optional.  Default = N (accepts y|n|Y|N)
#noarchive - Flag setting to prevent the archive step.  Files will not be archived.  Optional. Default = N (accepts y|n|Y|N)
#unpack - Flag setting to tell script to try to unpack archive files. Optional. Default = Y (accepts y|n|Y|N)
#         archive types handled: tar, tgz, tbz, txz, gz, zip, bz2, Z, lzma, xz
#automated - Program is called from an automated process.  No human interaction.  Optional.  Default = Y (accepts y|n|Y|N)
#debug - Run program in debug mode. Optional. Default = N (accepts y|n|Y|N)

#TODO:
#  Better error output
#  Output results
#  Handle files in bulk instead of file by file
#  Parameterize decrypt name format
#  Fix ambiguous comments
#  Add usage instructions if ?



##For moving files to decrypt directory, waiting for them to decrypt, and then moving
##them to a destination directory for further processing

use strict;
use warnings;
use File::Copy;
use File::Path 2.07 qw(make_path);
use File::Basename;
use Time::localtime;
use CGI;

# Declare constants for booleans
use constant false => 0;
use constant true => 1;

my $cgi = CGI->new;

my $source = $cgi->param("source") // "";
$source =~ s/\\$//;
$source = (-d $source) ? ($source . "/*") : $source;
$source =~ s!\\!/!g;

my $targetDir =  $cgi->param("targetDir") // "";
$targetDir = SetPathEnding($targetDir);

my $archiveDir =  $cgi->param("archiveDir") // "";
$archiveDir = SetPathEnding($archiveDir);

my $decryptDir = $cgi->param("decryptDir") // "//patton/ops/ops-decrypt-pgp";
$decryptDir = SetPathEnding($decryptDir);

my $copyOnly = $cgi->param("copyOnly") // "N";
my $skipArchive = $cgi->param("noArchive") // "N";
my $unpack = $cgi->param("unpack") // "Y";
my $ap = $cgi->param("automated") // "Y";
my $dbg = $cgi->param("debug") // "N";

my $debug = (uc($dbg) eq "Y") ? true : false;
my $autoProcess = (uc($ap) eq "Y") ? true : false;
my $noMove = (uc($copyOnly) eq "Y") ? true : false;
my $noArchive = (uc($skipArchive) eq "Y") ? true : false;
my $noUnpack = (uc($unpack) eq "N") ? true : false;

my @files = glob($source);
my $fileCount = scalar(@files);
my $fileNum = 0;
my $errCount = 1;

# Check data input
if ($targetDir eq "/") {
} else
{
  make_path($targetDir) if (! -d $targetDir);
}

unless($noArchive)
{
  $archiveDir =  $targetDir if ($archiveDir eq "/");
  make_path($archiveDir) if ((! -d $archiveDir) && ($archiveDir ne "/"));
}

my $dateStamp = GetDate();

# Each discrete step resets the filename and file values so that the next step
# can process it independently regardless of which step executed previously.

foreach my $file (@files) {
  $fileNum++;
  print "Processing " . basename($file) . "( " . $fileNum . " of " . $fileCount . ")\n" if ($autoProcess == false);
  if(-e $file) {
    my $fileName = basename($file);
    my $originalFile = $file;
    my $decryptFile = $decryptDir . $fileName;
    my $decryptedFile = $decryptDir . GetDecryptFileName($fileName);

    # Handle Archiving
    # Unless specifically told not to archive at all, the original file will be
    # copied to the specified directory with the extention .archive.  If no
    # directory is specified, the archive will happen in place.
    unless($noArchive) {
      my $archiveFile = $archiveDir . $fileName . ".archive";
      print "--> (File $fileNum of $fileCount) Archiving file\n" if ($autoProcess == false);
      copy ($file, $archiveFile);
    }

    # Handle Decryption
    # Unless specifically told not to do the decryption step. The file will be either
    # copied or moved (depending on the flag setting) into a folder (default: ops_decrypt_pgp)
    # to await decryption.  There must be an automated process that decrypts files that land
    # in the decrypt directory.  The file must be decrypted and named with the following structure
    # YYYYMMDD--FILENAME.EXTENSION(S) Note: the .pgp|.gpg must be removed from the result.
    # The script will wait for 8 minutes (sleeping in 30 second intervals) for the decryption.  After
    # the time is up, an error is noted and the next file in the list is attempted.
    if( $file =~ /\.g?pgp?$/i ) {

      print "--> (File $fileNum of $fileCount) Copying from source.\n" if ($autoProcess == false);
      copy ($file, $decryptFile);

      my $waitCounter = 1;
      my $waitLimit = 16; #16 = 8 minutes.  Decrypt script runs every 5 minutes and may take some time to decrypt data.
      my $keepWaiting = true;
      while ($keepWaiting) {
        print "--> (File $fileNum of $fileCount) Waiting for decrypt. (Pass #${waitCounter})\n" if (autoProcess == false);
        sleep 30;
        $keepWaiting = ((-e $decryptedFile) || $waitCounter == $waitLimit + 1 ) ? false : true;

        $waitCounter++;
      }
      if (-e $decryptedFile) {
        $file = $decryptedFile;
        $fileName = basename($decryptedFile);
      } else {
        #$contact, $body, $subject;
        #SendEmail("itops@rewardsnow.com", "Decryption of data in $decryptDir does not appear to be working.", "Decrypt Timeout: RNIFileHandling_FTP2Processing.pl");
        $errCount++;
        next;
      }
    }

    # Handle Final file copy
    if (-e $file) {
    $fileName =~ s/^${dateStamp}--//;

      my $targetFile = $targetDir . $fileName;
      print "--> (File $fileNum of $fileCount) Moving file\n" if (autoProcess == false);
      copy ($file, $targetFile);
      if (-e $targetFile) {
        $file = $targetFile;
        $fileName = basename($targetFile);
      } else {
        $errCount++;
        next;
      }
    }

    if (-e $file) {
     if ($noUnpack == false) {
      UnpackFile($file);
     }
      CleanupFiles($originalFile,  $decryptFile, $decryptedFile);
    }

  }
}

if ($errCount > 1) {
	if ($autoProcess = false) {
		die "Errors in processing";
	}	
	return $errCount;
}

die "Errors in processing" if ($errCount && $autoProcess == false);

sub CleanupFiles {
  my ($originalFile,  $decryptFile, $decryptedFile) = @_;
  unless($debug == true) {
    unless($noMove == true) { unlink $originalFile; }
    if (-e $decryptFile) { unlink $decryptFile; }
    if (-e $decryptedFile) {unlink $decryptedFile; }
  }
}


sub SetPathEnding {
  my $p = shift;
  $p =~ s!\\!/!g;
  unless ($p eq "") {
    $p = ($p =~ m!/$!) ? $p : $p . "/";
  }
  return $p;
}

sub GetDecryptFileName {
  my $f = shift;
  my $dateStamp = GetDate();

  $f = substr($f, 0, (length($f) - 4));

  return "${dateStamp}--$f";
}

sub GetDate {
  my $tm = localtime;
  my ($sec, $min, $hour, $day, $month, $year) = ($tm->sec, $tm->min, $tm->hour, $tm->mday, $tm->mon, $tm->year);
  $year = $year + 1900;
  $month = $month + 1;
  $month = "0" . $month if $month < 10;
  $day = "0" . $day if $day < 10;
  my $stamp = "${year}${month}${day}";
  return $stamp;
}

sub GetArchiveType {
  my ($fileName) = @_;

  use File::Type;

  my $ft = File::Type->new();
  my $fileType = $ft->checktype_filename($fileName);

  if (($fileName =~ m/\.tar$/i) || ($fileType =~ m/x-tar$/i)) {
    return "tar";
  } elsif (($fileName =~ m/\.tar\.gz$|\.tgz$/i) || ($fileType =~ m/x-gtar$/i)) {
    return "tgz";
  } elsif ($fileName =~ m/\.tar\.bz2$|\.tbz$/i) {
    return "tbz";
  } elsif ($fileName =~ m/\.tar\.xz$|\.txz$/i) {
    return "txz";
  } elsif (($fileName =~ m/\.gz$/i) || ($fileType =~ m/x-gzip$/i)) {
    return "gz";
  } elsif (($fileName =~ m/\.zip$|\.jar$|\.par$/i) || ($fileType =~ m/zip$/i)) {
    return "zip";
  } elsif (($fileName =~ m/\.bz2$/i) || ($fileType =~ m/x-bzip2$/i)) {
    return "bz2";
  } elsif ($fileName =~ m/\.z$/i) {
    return "Z";
  } elsif ($fileName =~ m/\.lzma$/i) {
    return "lzma";
  } elsif ($fileName =~ m/\.xz$/i) {
    return "xz";
  } else {
    return "none";
  }
}
sub UnpackFile{
  my $file = shift;

  use Archive::Extract;

  my $archiveType = GetArchiveType($file);

  if ($archiveType ne "none") {
    my $ae = Archive::Extract->new( archive => $file, type => $archiveType );
    $ae->extract( to => $targetDir );
  unlink $file || warn "Could not delete source archive";
  }
}

sub SendEmail{
  my ($contact, $body, $subject) = @_;

  $body =~ s/'/''/ig;
  $subject =~ s/'/''/ig;

  my $dbh = new Win32::ODBC("DSN=PATTON;");
  my $sql = "INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from ) VALUES ('$subject', '$body', '$contact', 'opslogs\@rewardsnow.com')";
  executesql($sql, $dbh);
}
