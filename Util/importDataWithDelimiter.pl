#!perl

use strict;
use warnings;

use CGI;
use Text::CSV::Encoded;
use Win32::ODBC;

my $dbh1 = new Win32::ODBC("DSN=PATTON;");

my $cgi = new CGI;
my $file = $cgi->param("filename");
my $filetype = $cgi->param("filetype") // 1;
my $tip = $cgi->param("tip") // "990";
my $enddate = $cgi->param("enddate") // "01/01/2000";
my $delimiter = $cgi->param("delimiter") // ',';

open(FILE, "<" . $file);

my $rowcount = 1;
while(my $line = <FILE>){
  my $csv = Text::CSV::Encoded->new({encoding=>"utf-8", sep_char=>${delimiter}});
#  $csv->setDelimiter($delimiter);
  my $fileToImport = $file;
  $fileToImport =~ s/'/''/g;
  my $insertSQL = "INSERT INTO RewardsNOW.dbo.RNIRawImport (sid_rniimportfiletype_id, dim_rnirawimport_source, dim_rnirawimport_sourcerow, sid_dbprocessinfo_dbnumber, dim_rnirawimport_processingenddate ";
  my $valuesSQL = "VALUES ($filetype, '$fileToImport', $rowcount, '$tip', '$enddate' ";
  if ($csv->parse($line)) {
    my @data = $csv->fields();
    my $fieldNumber = "01";
    foreach my $fieldData (@data){
      $fieldData =~ s/'/''/g;
      $insertSQL .= ", dim_rnirawimport_field" . $fieldNumber . " ";
      $valuesSQL .= ", '" . $fieldData . "'";
      $fieldNumber++;
    }
  }
  $insertSQL .= ")";
  $valuesSQL .= ")";
  my $sql = $insertSQL . $valuesSQL;
  #print $sql . "\n";
  executesql( $sql, $dbh1 );

  $rowcount ++;
}
close FILE;



sub executesql {
  my ($request, $data) = @_;
  if ($data->Sql($request)) {
    my $value = $data->Error();
    print "SQL Statement: " . $request . "\n";
    print "SQL Error: " . $value . "\n";
    $data->Close();
    open(SQLERROR, ">>sqllog.txt");
    print SQLERROR "SQL Statement: " . $request . "\n";
    print SQLERROR "SQL Error: " . $value . "\n\n";
    close SQLERROR;
  }
  return $data;
}
