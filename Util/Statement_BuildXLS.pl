#!c:\perl64\bin\perl.exe
#ppm install Spreadsheet-WriteExcel
use strict;
use warnings;

use CGI;
use Spreadsheet::WriteExcel;
use Win32::ODBC;

my $cgi = CGI->new();

my $outdir = $cgi->param("outputdir") // "";
my $outfile = $cgi->param("outputfilename") // "";
my $jobid = $cgi->param("jobid") // 0;

my $DSN = "PATTON";

unless ($outdir eq '' || $outfile eq '' || $jobid == 0) {
	
	#clean up file path
	$outdir =~ s|\$||;
	#error out if file path not found
	if (!-d $outdir) {
		my $sqlerr1 = "EXEC Rewardsnow.dbo.usp_LogProcessingJobError " . $jobid . ", 'Output Directory Not Does not Exist', NULL";

		my $cnx = new Win32::ODBC("DSN=Patton;") or die "Could not open connection to DSN because of [$!]";
		
		if ($cnx->Sql($sqlerr1)){
		  print "I could not execute the following statement:\n $sqlerr1\n";
		  print "I encountered this error:\n";
		  print $cnx->Error() . "\n";
		}
		
		$cnx->Close();
		
		die
	}
	#generate full output path
	my $file = $outdir . "\\" . $outfile;
	
	#delete file if exists
	unlink $file if (-e $file);
	
	#initialize worksheet
	my $workbook = Spreadsheet::WriteExcel->new($file);
	$workbook->compatibility_mode();
	my $worksheet = $workbook->add_worksheet();
	#extract data from database
	
	my $sqlextract = "SELECT dim_stmtvalue_column, dim_stmtvalue_row, convert(varchar(255),dim_stmtvalue_value) as dim_stmtvalue_value " .
	                 " FROM RewardsNow.dbo.stmtValue " .
	                 " WHERE sid_processingjob_id = " . $jobid;
	                 

	my $cnxextract = new Win32::ODBC("DSN=Patton;") or die "Could not open connection to DSN because of [$!]";

	if ($cnxextract->Sql($sqlextract)){
  	print "I could not execute the following statement:\n $sqlextract\n";
  	print "I encountered this error:\n";
  	print $cnxextract->Error() . "\n";
  	$cnxextract->Close();
	}
	#populate spreadsheet
	
	while ( $cnxextract->FetchRow() ) {
		my %datarow = $cnxextract->DataHash();
		$worksheet->write_string($datarow{dim_stmtvalue_row},$datarow{dim_stmtvalue_column},$datarow{dim_stmtvalue_value});
	}	
	$cnxextract->Close();
	$workbook->close();
}
