#!perl

use strict;
use warnings;

use CGI;
use Win32::ODBC;

my $dbh1 = new Win32::ODBC("DSN=PATTON;");

my $cgi = new CGI;
my $file = $cgi->param("filename");
my $filetype = $cgi->param("filetype") // 1;
my $tip = $cgi->param("tip") // "990";
my $enddate = $cgi->param("enddate") // "01/01/2000";
#my $datestamp = getdate();

open(FILE, "<" . $file);

my $rowcount = 1;
while(my $line = <FILE>){
  my $fileToImport = $file;
  $fileToImport =~ s/'/''/g;
  $line =~ s/'/''/g;
  my $insertSQL = "INSERT INTO RewardsNOW.dbo.RNIRawImport (sid_rniimportfiletype_id, dim_rnirawimport_source, dim_rnirawimport_sourcerow, sid_dbprocessinfo_dbnumber, dim_rnirawimport_processingenddate, dim_rnirawimport_field01) ";
  my $valuesSQL = "VALUES ($filetype, '$fileToImport', $rowcount, '$tip', '$enddate', '$line') ";
  my $sql = $insertSQL . $valuesSQL;
  executesql( $sql, $dbh1 );

  $rowcount ++;
}
close FILE;

sub executesql {
  my ($request, $data) = @_;
  if ($data->Sql($request)) {
    my $value = $data->Error();
    print "SQL Statement: " . $request . "\n";
    print "SQL Error: " . $value . "\n";
    $data->Close();
    open(SQLERROR, ">>importDataFixed_ErrLog.txt");
    print SQLERROR "SQL Statement: " . $request . "\n";
    print SQLERROR "SQL Error: " . $value . "\n\n";
    close SQLERROR;
	
	die "SQL Error: " . $value . "\n";
  }
  return $data;
}

#sub getdate {
#  my $tm = localtime(time);
#  my ($sec, $min, $hour, $day, $month, $year) = ($tm->sec, $tm->min, $tm->hour, $tm->mday, $tm->mon, $tm->year);
#  $year = $year + 1900;
#  $month = "0". $month + 1;
#  $month = substr($month, -2);
#
#  $day = "0" . $day if $day < 10;
#  my $stamp = "${year}${month}${day}";
#  return $stamp;
#}