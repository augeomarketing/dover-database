#!c:\perl\bin\perl.exe

#TODO (2.0): REMOVE HARDCODED VALUES AND PUT IN A TABLE 
#TODO (1.5): ADD FILE CHECKS

use strict;
use warnings;

use Time::localtime;
use Win32::ODBC;
use chilkat();

#Declare Objects
my $cnx = new Win32::ODBC("DSN=PATTON;");
my $sftp = new chilkat::CkSFtp();

#General Settings\Global Vars (incl FilePath)
my $flagDeleteFileOnUpload = 1;
my $success = 0;
my $datestamp = getdate();
my $pathToFile = "\\\\patton\\ops\\AccessDevelopment\\Output\\";

my @filesToSend;

#Encryption Settings
my $pathToGpg = "C:\\Progra~1\\GNU\\GnuPG";
my $itOpsKey = "E0CCA6E4";
my $clientKey = "53885F0E";

#Email Settings
my $msgTo = 'web@rewardsnow.com, dirish@rewardsnow.com, opslogs@rewardsnow.com';
my $msgFrom = 'opslogs@rewardsnow.com';
my $msgSubj = 'SFTP Send - AccessDevelopment ' . $datestamp; 
my $msgBody = 'SFTP Send - AccessDevelopment ' . $datestamp . "<br />";

#SFTP Settings

my $hostname = 'ftp.adchosted.com';
my $uid      = 'rewards-now';
my $pwd      = 'sgm!9tFGK$$9';
my $port     = 22;
$sftp->put_ConnectTimeoutMs(15000);
$sftp->put_IdleTimeoutMs(15000);

$msgBody .= "Parameter Values: <br />";
$msgBody .= "flagDeleteFileOnUpload = ${flagDeleteFileOnUpload} <br />";
$msgBody .= " datestamp = ${datestamp} <br />";
$msgBody .= " pathToFile = ${pathToFile} <br />"; 
$msgBody .= " pathToGpg = ${pathToGpg} <br />"; 
$msgBody .= " itOpsKey = ${itOpsKey } <br />";
$msgBody .= " clientKey = ${clientKey} <br />"; 
$msgBody .= " msgTo = ${msgTo} <br />"; 
$msgBody .= " msgFrom = ${msgFrom} <br />";
$msgBody .= " hostname = ${hostname} <br />";
$msgBody .= " uid = ${uid} <br />";
$msgBody .= " pwd = Not Displayed via Email <br />";
$msgBody .= " port = ${port} <br />";
$msgBody .= "Processing Messages: <br />";



###################################
#          Begin Main             #
###################################

print "Validating Chilkat Components : Start \n";
$msgBody .= "---> Validating Chilkat Components : Start <br />";

#Validate Chilkat Components
$success = $sftp->UnlockComponent("REWNOWSSH_Qck7DUkt1Rz1");
if ($success != 1) {
	$msgBody .= "<b>Error Unlocking Chilkat Component</b><br />";
	$msgBody .= $sftp->lastErrorText() . "<br />";
	sendresults();
	exit;
} else {
	print "Validating Chilkat Components : Success \n";
	$msgBody .= "---> Validating Chilkat Components : Success <br />";
}


print "Encrypting Unencrypted Files : Start \n";
$msgBody .= "---> Encrypting Unencrypted Files : Start <br />";

#  Encrypt any Unencrypted Files
opendir(DIR, $pathToFile);
my @files = readdir(DIR);
closedir DIR;
foreach my $file (@files) {
	if ($file =~ m/^RN-AD/i) {
		#Test if file hasn't been encrypted
		if ($file !~ m/\.(p)?gp(g)?$/ ) {  
			my $encryptOutput = `$pathToGpg\\gpg.exe --batch -r ${itOpsKey} -r ${clientKey} -e ${pathToFile}${file}`;
			system("move", "${pathToFile}${file}.gpg", "${pathToFile}${file}.pgp");
			
			if ($flagDeleteFileOnUpload == 1) {
				unlink "${pathToFile}${file}";			
			}
		
			print "--->--->Encryption Output for file ${file}: ${encryptOutput} \n";
			$msgBody .=  "--->--->Encryption Output for file ${file}: ${encryptOutput} <br />";
		}
	}
}	

print "Encrypting Unencrypted Files : End \n";
$msgBody .= "---> Encrypting Unencrypted Files : End <br />";

print "Getting Files to Send : Start \n";
$msgBody .= "---> Getting Files to Send : Start <br />";


# Get Files to Send         
opendir(EDIR, $pathToFile);
my @sendFiles = readdir(EDIR);
closedir (EDIR);
foreach my $sendFile (@sendFiles) {
	$sendFile =~ m/RN-AD(.*)\.pgp/i || next;
	print "--->--->Adding file to list: ${sendFile} \n";
	$msgBody .= "--->--->Adding file to list: ${sendFile}<br />";
	push(@filesToSend, $sendFile);
}

print "Getting Files to Send : End \n";
$msgBody .= "---> Getting Files to Send : End <br />";


# Send Files
ftpFiles($pathToFile, \@filesToSend );

# Send Results
sendresults();

print "Processing Complete";


exit;
###################################
#           End Main              #
###################################

###################################################################################################################

                                      ###################################
                                      #          Subroutines            #
                                      ###################################

###################################################################################################################

sub getdate {
	my $tm = localtime;
	(my $sec, my $min, my $hour, my $day, my $month, my $year) = ($tm->sec, $tm->min, $tm->hour, $tm->mday, $tm->mon, $tm->year);
	$year = $year + 1900;
	$month = $month + 1;
	$month = "0" . $month if $month < 10;
	$day = "0" . $day if $day < 10;
	my $stamp = "${year}${month}${day}${hour}${min}${sec}";
	return $stamp;
}

sub executesql {
	my ($request, $connection) = @_;
	if ($connection->Sql($request)) {
		my $err = $connection->Error();
		print "SQL Statement: " . $request . "\n";
		print "SQL Error: " . $err . "\n";
		$connection->Close();
		open(SQLERROR, ">>AccessDevelopment_SQLError_" . $datestamp . ".txt");
		print SQLERROR "SQL Statement: " . $request . "\n";
		print SQLERROR "SQL Error: " . $err . "\n\n";
		close SQLERROR;
	}
	return $connection;
}

sub sendresults {
	print "--->Sending Results \n";
	$msgBody .= "---> Sending Results <br />";

	$msgSubj =~ s/'/''/g;
	$msgTo   =~ s/'/''/g;
	$msgFrom =~ s/'/''/g;
	$msgBody =~ s/'/''/g;

	my $insertSQL = "INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_to, dim_perlemail_from, dim_perlemail_body) ";
	my $valuesSQL = "VALUES ('$msgSubj', '$msgTo', '$msgFrom', '$msgBody'); ";
	my $sql = $insertSQL . $valuesSQL;
	executesql( $sql, $cnx );
	return 1;
}


sub ftpFiles{
	my ($path, $filenames) = @_;
	
	print "--->Sending Files : Start \n";
	$msgBody .= "---> Sending Files : Start <br />";

	print "--->---> Connecting to FTP Server : Start \n";
	$msgBody .= "--->---> Connecting to FTP Server : Start <br />";

	#Validate Connection, Authenticate SSH, Initialize SFTP
	$success = $sftp->Connect($hostname,$port);
	if ($success != 1) {
		$msgBody .= "<b>Error Connecting to ${hostname} on port ${port}</b><br />";
		$msgBody .= $sftp->lastErrorText() . "<br />";
		sendresults();
		exit;
	} else {
		print "--->---> Connecting to FTP Server : Success \n";
		$msgBody .= "--->---> Connecting to FTP Server : Success <br />";
	}

	
	print "--->---> Authenticating User : Start \n";
	$msgBody .= "--->---> Authenticating User : Start <br />";

	$success = $sftp->AuthenticatePw($uid,$pwd);
	if ($success != 1) {
		$msgBody .= "<b>Error Authenticating User ${uid}</b><br />";
		$msgBody .= $sftp->lastErrorText() . "<br />";
		sendresults();
		exit;
	} else {
		print "--->---> Authenticating User : Success \n";
		$msgBody .= "--->---> Authenticating User : Success <br />";
	}

	
	print "--->---> Initializing FTP : Start \n";
	$msgBody .= "--->---> Initializing FTP : Start <br />";

	$success = $sftp->InitializeSftp();
	if ($success != 1) {
		$msgBody .= "<b>Error Initializing SFTP</b><br />";
		$msgBody .= $sftp->lastErrorText() . "<br />";
		sendresults();
		exit;
	} else {
		print "--->---> Initializing FTP : Success \n";
		$msgBody .= "--->---> Initializing FTP : Success <br />";
	}

	
	print "--->---> File Send : Start \n";
	$msgBody .= "--->---> File Send : Start <br />";
	
	foreach my $filename (@$filenames){
		my $remoteFilePath = "/to_access/${filename}";
		my $localFilePath = "${path}${filename}";
	
		$success = $sftp->UploadFileByName($remoteFilePath,$localFilePath);
		if ($success != 1) {
			$msgBody .= "<b>Error Uploading ${localFilePath}</b><br />";
			$msgBody .= $sftp->lastErrorText() . "<br />";
			sendresults();
			exit;
		} else {
			$msgBody .= "--->--->--->${localFilePath} successfully uploaded to ${remoteFilePath}<br />";
			print "--->--->--->${localFilePath} successfully uploaded to ${remoteFilePath} \n";
			if ($flagDeleteFileOnUpload == 1)
			{
				$msgBody .= "--->--->--->--->Deleting ${localFilePath}<br />";
				print "--->--->--->--->Deleting ${localFilePath} \n";
				unlink $localFilePath;
			}
		}
	}

	print "--->---> File Send : End \n";
	$msgBody .= "--->---> File Send : End <br />";

	print "--->Sending Files : End (Disconnecting FTP) \n";
	$msgBody .= "---> Sending Files : End (Disconnecting FTP) <br />";
	
	$sftp->Disconnect();
	return 1;
} 