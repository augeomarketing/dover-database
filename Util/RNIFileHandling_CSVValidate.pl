#perl
# right now this routine only does validation and minor manipulation of files.
# it will output valid lines to back to the original file and lines in error 
# to the file name with .err appended.  The original file will be archived to
# the file name with .archive appended for safe keeping
# if the archivedir parameter is filled, it will move the original to the
# specified directory (which must be fully qualified at this time)

#TODO Get MD5 of source before anything is touched
#TODO Get MD5 of load file when done
#TODO Store stats at end of run:  src file md5 [if not existing at start of process] 
#     (dim_rniimportcontrol_srcfilemd5) ErrFile name (dim_rniimportcontrol_errfile), validation date (dim_rniimportcontrol_validationdate), 
#     srcerrcount (dim_rniimportcontrol_srcerrcount), loadfile MD5 (dim_rniimportcontrol_loadfilemd5)
#TODO add parameters ($srcMD5, $tranSum)
#TODO override parameters from control file if id supplied 
#  $inFile dim_rniimportcontrol_sourcefilepath + dim_rniimportcontrol_filename
#  $srcMD5 dim_rniimportcontrol_sourcefilemd5
#  $lineCount dim_rniimportcontrol_rowcount
#  $tranSum dim_
#TODO verify src MD5 against supplied value (if exists)
#TODO add row number to load file output
#TODO add control id to load file output
#TODO add sql handling code

use strict;
use warnings;
use Text::CSV::Encoded;
use CGI;
use File::Copy;
use File::Path;
use File::Basename;
use Switch 'Perl5', 'Perl6';
use WIN32::ODBC;

# Declare constants for booleans
use constant false => 0;
use constant true => 1;

# Initialize Objects
my $CGI = CGI->new;

# Set Default Values for missing parameters
my $inFile = $CGI->param("infile") // "NOFILE";
my $errFile = $CGI->param("errfile") // $inFile . ".err";
my $archiveDir = $CGI->param("archivedir") // "NOARCHIVEDIR";
my $lineCount = $CGI->param("linecount") // -1;
my $fi = $CGI->param("fi") // "999";
my $delimiter = $CGI->param("delimiter") // ",";
my $textSeparator = $CGI->param("textseparator") // '"';
my $fileType = $CGI->param("filetype") // 1;  #1-Account, 2-Transaction, 3-Account Purge, 4-Transaction/Card Purge, 5-Bonus
my $trimErrors = $CGI->param("trimerrors") // "Y";
my $trimHeader = $CGI->param("trimheader") // "N";
my $allowEmbeddedQuotes = $CGI->param("allowembeddedquotes") // "N";
my $fileVersion = $CGI->param("fileversion") // 225
my $controlId = $CGI->param("controlid") // 0


$fileType = uc($fileType);

$trimHeader = (uc($trimHeader) eq "Y") ? true : false;
$trimErrors = (uc($trimErrors) eq "Y") ? true : false;
$allowEmbeddedQuotes = (uc($allowEmbeddedQuotes) eq "Y") ? true : false;

if (($inFile ne "NOFILE") && (-e $inFile)) {

	my $loadFileRow = 0;

	my $escapeChar = ($allowEmbeddedQuotes == true) ? "\\" : '"';

	my ( $errCount, $lineCounter);
	$errCount = $lineCounter = 0;

	my ( $output, $errOutput);
	$output = $errOutput = "";

	my $columnCount = SetColumnCount($fileVersion, $fileType);
	my $validationType = SetValidationType($fi); #Initialize $validationType
	my $runDate = SetRunDate(); #Initialize $runDate;

	# handle archive directory and file archiving before anything else
	copy ($inFile, $inFile . ".archive");
	
	unless ($archiveDir eq "NOARCHIVEDIR") {
		mkpath($archiveDir) unless (-d $archiveDir);
		my $archiveFileName = basename($inFile) . ".archive";
		move ($inFile . ".archive", $archiveDir . "\\" . $archiveFileName);
	}

	my $CSV = Text::CSV::Encoded->new( {
		quote_char=> $textSeparator
		, sep_char=> $delimiter
		, escape_char=> $escapeChar
		, allow_loose_quotes => $allowEmbeddedQuotes
	} );
	
	open (MYFILE, "<" . $inFile);
	my @rows = <MYFILE>;
	close MYFILE;
	
	my @pChar = ("\\", "|", "/", "-");
	my $cIndex = 0;		

	my $totalRows= scalar(@rows);
	foreach my $line (@rows)
	{
		$cIndex++;
		if ($cIndex == 4) {
			$cIndex = 0;
		}
		print "\r" . $pChar[$cIndex];
		#if we are supposed to skip headers and we are on line 1, decrement the linecount and loop
		next if ($trimHeader == true && $lineCounter == 1);
		$lineCounter++;
		
		if ($CSV->parse($line)) {
			my @columns = $CSV->fields();
			if (ValidateColumnCount($columnCount, @columns) && ValidateRequiredColumns($validationType, $fileType, @columns) ){
				#build out missing columns for output rows
				
				
				$output .= $line; 
			}
			else { 
				$errOutput .= $line;
				$errCount++;  
			}

		} else {
			$errOutput .= $line;
			$errCount++;
			#failed to parse line...		
		}
	}
#	close CSV;
	
	if ($lineCount != $lineCounter && $lineCount != -1)  {
		$errCount++;
	}
	
	if (length($output) != 0) {
		open (OUTFILE, ">" . $inFile );
		print OUTFILE $output;
		close OUTFILE;
	}
	
	if (length($errOutput) != 0) {
		open (ERRFILE, ">" . $errFile );
		print ERRFILE $errOutput;
		close ERRFILE;
	}
	
	#this should be replaced with the database output...
	open (VALFILE, ">" . $inFile . ".validation");
	print VALFILE basename($inFile) . "\t" . $runDate . "\t" . $errCount . "\t" . $inFile . "\.err";
	close VALFILE;
	
}

sub SetColumnCount {
	my ($fv, $ft) = @_;

	my %cols;
	$cols{224}{1} = 23;
	$cols{224}{2} = 16;
	$cols{225}{1} = 23;
	$cols{225}{2} = 17;
	$cols{225}{3} = 5;
	$cols{225}{4} = 6;
	$cols{225}{5} = 7;

	return (defined $cols{$fv}{$ft}) ? $cols{$fv}{$ft} : -1;
}

sub SetRunDate {
	my($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime();
	my $year = 1900 + $yearOffset;
	my $pad_len = 2;

	$month = sprintf("%0${pad_len}d", $month);
	$dayOfMonth = sprintf("%0${pad_len}d", $dayOfMonth);
	$second = sprintf("%0${pad_len}d", $second);
	$hour = sprintf("%0${pad_len}d", $hour);
	$minute = sprintf("%0${pad_len}d", $minute);

	return "$year$month$dayOfMonth $hour:$minute:$second";
}

sub ValidateColumnCount {
	#takes columncount and array containing the columns
	my ($cc, @cc_cols) = @_;
	($cc == scalar(@cc_cols)) ? return true : return false;
}


sub ValidateRequiredColumns {
 # takes the version, validation type, file type, and an array containing the columns
 # 0 is the type for the default validation
 # 1 is the type for RIS Media (tipfirst =~ /^a/i)
	my ($v, $t, $f, @rc_cols) = @_;
	my $c = $v . "_" . $f . "_" . $t;
	# v = version
	# t = validation type (tied to fi)
	# f = file type

	my $set = $rc_cols[0] . $rc_cols[1] . $rc_cols[2];
	return false if ($set eq "");

	given ($c) {
		when /(224|225)_1_0/ { #for versions 224 or 225, account, default
			($rc_cols[4] ne "" && $rc_cols[8] ne "" && $rc_cols[11] ne ""
				&& $rc_cols[12] ne "" && $rc_cols[14] ne "" && $rc_cols[17] ne "" ) ? return true : return false;
		}
		when /(224|225)_1_1/ { #for versions 224 or 225, account, RIS Media (type 1)
			($rc_cols[22] ne "") ? return true : return false;
		}
		when /^224_2_/ { #for version 224, transaction, all types
			($rc_cols[3] ne "" && $rc_cols[7] ne "" && $rc_cols[9] ne "") ? return true : return false;
		}
		when /^225_2_/ { #for version 225, transaction, all types
			($rc_cols[3] ne "" && $rc_cols[7] ne "" && $rc_cols[9] ne "" && $rc_cols[16] ne "") ? return true : return false;
		}
		when /^225_(3|4)_/ { #for version 225, purge customers (3) or purge card (4), all types
			($rc_cols[4] ne "") ? return true : return false;
		}
		when /^225_5_/ { #for version 225, bonus (5), all types
			($rc_cols[4] ne "" && $rc_cols[6] ne "") ? return true : return false;
		}
		default { return false; }
	}
	
#	given ($v) {
#		when (224) {
#			given ($f) {
#				when (1) { #224 - Account
#					given ($t) { # Validation Type
#						when ([0]) { 
#							($rc_cols[4] ne "" && $rc_cols[8] ne "" && $rc_cols[11] ne ""
#								&& $rc_cols[12] ne "" && $rc_cols[14] ne "" && $rc_cols[17] ne "" ) ? return true : return false;
#						}
#						when ([1]) { 
#							($rc_cols[22] ne "") ? return true : return false;
#						}
#						default { return false; }
#					}
#				}
#				when (2) { #224 - Transaction
#					given ($t) {
#						when ([0,1]) { 
#							($rc_cols[3] ne "" && $rc_cols[7] ne "" && $rc_cols[9] ne "") ? return true : return false;
#						}
#						default { return false; }
#					}
#				}
#				default { return false; }
#			}
#		}
#		when (225) {
#			given ($f) {
#				when (1) { #225 - Account
#					given ($t) {
#						when ([0]) { 
#							($rc_cols[4] ne "" && $rc_cols[8] ne "" && $rc_cols[11] ne ""
#								&& $rc_cols[12] ne "" && $rc_cols[14] ne "" && $rc_cols[17] ne "" ) ? return true : return false;
#						}
#						when ([1]) { 
#							($rc_cols[22] ne "") ? return true : return false;
#						default { return false; }
#					}
#				}
#				when (2) { #225 - Transaction
#					given ($t) {
#						when ([0,1]) { 
#							($rc_cols[3] ne "" && $rc_cols[7] ne "" && $rc_cols[9] ne "" && $rc_cols[16] ne "") ? return true : return false;
#						}
#						default { return false; }
#					}
#				}
#				when (3) { #225 - Account Purge
#					given ($t) {
#						when ([0,1]) { 
#							($rc_cols[4] ne "") ? return true : return false;
#						}
#						default { return false; }
#					}
#				}
#				when (4) { #225 - Transaction/Card Purge
#					given ($t) {
#						when ([0,1]) { 
#							($rc_cols[4] ne "") ? return true : return false;
#						}
#						default { return false; }
#					}
#				}
#				when (5) { #225 - Bonus
#					given ($t) {
#						when ([0,1]) {
#							($rc_cols[4] ne "" && $rc_cols[6] ne "") ? return true : return false;
#						}
#						default { return false; }
#					}
#				}
#				default { return false; }
#			}
#		}
#		default { return false; }
#	}
	
}
sub SetValidationType {
  my $type = shift;
#0 default
#1 A01, etc - RIS Media - Validate 
	given ($type) {
		when (/^a/i) { return 1; }
		default { return 0; }
	}
}


sub PopulateParamsFromControlTable {
	my $ctrlId = shift;
	#pull data from database
	#hash row
	#apply to params if they are empty
	
	
}


sub UpdateStats {
	my $DBH = new Win32::ODBC("DSN=PATTON;");
	my $sql = "";
    my $updateQry = executesql ($sql, $DBH);
}


sub executesql {
  my ($request, $DATA) = @_;
  if ($DATA->Sql($request)) {
    my $value = $DATA->Error();
    print "SQL Statement: " . $request . "\n";
    print "SQL Error: " . $value . "\n";
    $DATA->Close();
    exit;
  }
  return $DATA;
}
