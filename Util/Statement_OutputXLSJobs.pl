#!perl

use strict;
use warnings;
use Win32::ODBC;

my $sqlgetjobs = "EXEC Rewardsnow.dbo.usp_GetJobsAwaitingOutput";
my $cnxgetjobs = new Win32::ODBC("DSN=Patton;") or die "Unable to open database connection: [$!]";

if ($cnxgetjobs->Sql($sqlgetjobs)) {
  print "Unable to execute: \n $sqlgetjobs \n";
  print "Error: \n";
  print $cnxgetjobs->Error() . "\n";
  $cnxgetjobs->Close();
}

while ( $cnxgetjobs->FetchRow() ) {
  my ($jobid, $outputdir, $outputfilename, $outputtype) = $cnxgetjobs->Data("jobid", "outputDir", "outputFileName", "outputType");

  if ($outputtype =~ m|XLS|i) {
	if ($outputtype =~ m|XLSX|i) {
		my $commandline = "\\\\patton\\ops\\util\\Statement_BuildXLSX.pl outputdir=" . $outputdir . " outputfilename=" . $outputfilename . " jobid=" . $jobid;
	} else {
		my $commandline = "\\\\patton\\ops\\util\\Statement_BuildXLS.pl outputdir=" . $outputdir . " outputfilename=" . $outputfilename . " jobid=" . $jobid;
	} 
    
    system ($commandline);
    unless ( $? == -1 ) {
    
      my $dbnumber = "";
      my $stepid = "";
      my $statusid = "";

      #if output was production welcome kit dir, notify production
      if ($outputdir =~ m/\\\\ftp\\ike\\staff\\production\\welcomekit$/i) { #todo: check syntax of this regex (and actual location of directory)
        my $sqlproduction = "EXEC rewardsnow.dbo.usp_stmtEmailProductionWelcomeKitsReady_UseFilename '" . $outputfilename . "'";
        my $cnxproduction = new Win32::ODBC("DSN=Patton;") or die "Unable to open database connection: [$!]";

        if ($cnxproduction->Sql($sqlproduction)) {
          print "Unable to execute: \n $sqlproduction \n";
          print "Error: \n";
          print $cnxproduction->Error() . "\n";
        } #production sql error

        $cnxproduction->Close();

      } #if outputdir = producton

      #mark job as done in processingjob
      my $sqlupdatejobstatus = "EXEC Rewardsnow.dbo.usp_BNW_UpdateJobStatus " . $jobid . ", 3";
      my $cnxupdatejobstatus = new Win32::ODBC("DSN=Patton;") or die "Unable to open database connection: [$!]";

      if ($cnxupdatejobstatus->Sql($sqlupdatejobstatus)) {
        print "Unable to execute: \n $sqlupdatejobstatus \n";
        print "Error: \n";
        print $cnxupdatejobstatus->Error() . "\n";
      } #jobstatus sql error

      $cnxupdatejobstatus->Close();

      #get job info
      my $sqljobinfo = "SELECT sid_dbprocessinfo_dbnumber, sid_processingstep_id, sid_processingjobstatus_id from rewardsnow.dbo.processingjob where sid_processingjob_id = " . $jobid;
      my $cnxjobinfo = new Win32::ODBC("DSN=Patton;") or die "Unable to open database connection: [$!]";

      if ($cnxjobinfo->Sql($sqljobinfo)) {
        print "Unable to execute: \n $sqljobinfo \n";
        print "Error: \n";
        print $cnxjobinfo->Error() . "\n";
        $cnxjobinfo->Close();
      } #getjob info sql error

      if ($cnxjobinfo->FetchRow()) {
        ($dbnumber, $stepid, $statusid) = $cnxjobinfo->Data("sid_dbprocessinfo_dbnumber", "sid_processingstep_id", "sid_processingjobstatus_id");
      }
      $cnxjobinfo->Close();

      unless ($statusid == 7) {
        #notify processor of status
        my $sqlnotifyprocessor = "EXEC rewardsnow.dbo.usp_BNWEmailProcessor '" . $dbnumber . "', " . $stepid . ", " . $statusid;
        my $cnxnotifyprocessor = new Win32::ODBC("DSN=Patton;") or die "Unable to open database connection: [$!]";

        if ($cnxnotifyprocessor->Sql($sqlnotifyprocessor)) {
          print "Unable to execute: \n $sqlnotifyprocessor \n";
          print "Error: \n";
          print $cnxnotifyprocessor->Error() . "\n";
        } #notify processor info sql error
        $cnxnotifyprocessor->Close();

      } #unless statusid = 7
    } #unless build xls failed
  } #if  output type is xls
} #while fetch row

$cnxgetjobs->Close();
