USE [660]
GO
/****** Object:  Table [dbo].[wrkstatement]    Script Date: 12/09/2014 14:48:37 ******/
ALTER TABLE [dbo].[wrkstatement] DROP CONSTRAINT [DF_wrkstatement_PointsAdded]
GO
ALTER TABLE [dbo].[wrkstatement] DROP CONSTRAINT [DF_wrkstatement_PointsEnd]
GO
ALTER TABLE [dbo].[wrkstatement] DROP CONSTRAINT [DF_wrkstatement_PointsBegin]
GO
ALTER TABLE [dbo].[wrkstatement] DROP CONSTRAINT [DF_wrkstatement_PointsDecreased]
GO
DROP TABLE [dbo].[wrkstatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkstatement](
	[tipnumber] [varchar](15) NOT NULL,
	[PAN] [varchar](19) NOT NULL,
	[PointsAdded] [nvarchar](11) NULL,
	[PointsEnd] [nvarchar](10) NULL,
	[startdate] [varchar](8) NULL,
	[enddate] [varchar](8) NULL,
	[PointsBegin] [nvarchar](10) NULL,
	[PointsDecreased] [nvarchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[wrkstatement] ADD  CONSTRAINT [DF_wrkstatement_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[wrkstatement] ADD  CONSTRAINT [DF_wrkstatement_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[wrkstatement] ADD  CONSTRAINT [DF_wrkstatement_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[wrkstatement] ADD  CONSTRAINT [DF_wrkstatement_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO
