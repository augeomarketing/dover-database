USE [660]
GO
/****** Object:  StoredProcedure [dbo].[usp_MthStmtFile_Extract]    Script Date: 12/09/2014 14:49:46 ******/
DROP PROCEDURE [dbo].[usp_MthStmtFile_Extract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Create statement file that must be exported to the FI
-- =============================================
-- SEB 1/14/2015 removed the ABS from the update statements.

CREATE PROCEDURE [dbo].[usp_MthStmtFile_Extract]
	-- Add the parameters for the stored procedure here
	@MonthBeginningDate datetime
	, @MonthEndingDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @StartdateOut varchar(8), @EnddateOut varchar(8)
	
    -- Insert statements for procedure here
	set @StartdateOut = substring(convert(varchar(8),@MonthBeginningDate,112),5,4)+substring(convert(varchar(8),@MonthBeginningDate,112),1,4)
	set @EnddateOut = substring(convert(varchar(8),@MonthEndingDate,112),5,4)+substring(convert(varchar(8),@MonthEndingDate,112),1,4)

	truncate table wrkstatement

	insert into wrkstatement (tipnumber, PAN, startdate, enddate)
	select tipnumber, acctid, @StartdateOut, @EnddateOut
	from AFFILIAT_Stage
	where AcctTypeDesc='cardnumber'
		and AcctStatus='A'
	order by TIPNUMBER

	update wrkstatement
	set PointsBegin = msf.PointsBegin
			, PointsEnd = msf.PointsEnd
			, PointsAdded = msf.PointsIncreased
			, PointsDecreased = msf.PointsDecreased
	from wrkstatement wks join Monthly_Statement_File msf on wks.tipnumber=msf.Tipnumber

END
GO
