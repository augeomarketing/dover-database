USE [50HBeverly]
GO
/****** Object:  Table [dbo].[AugEnd]    Script Date: 09/24/2009 10:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AugEnd](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AugEnd] ADD  DEFAULT (0) FOR [PNTEND]
GO
