USE [50HBeverly]
GO
/****** Object:  Table [dbo].[FBOPQuarterlyStatement]    Script Date: 09/24/2009 10:39:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FBOPQuarterlyStatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedBUS] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusBUS] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedBUS] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
 CONSTRAINT [PK_FBOPQuarterlyStatement] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__7FEAFD3E]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__00DF2177]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__01D345B0]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__02C769E9]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__03BB8E22]  DEFAULT (0) FOR [PointsPurchasedHE]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__04AFB25B]  DEFAULT (0) FOR [PointsPurchasedBUS]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__05A3D694]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0697FACD]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__078C1F06]  DEFAULT (0) FOR [PointsBonusHE]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0880433F]  DEFAULT (0) FOR [PointsBonusBUS]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__09746778]  DEFAULT (0) FOR [PointsBonusEmpCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0A688BB1]  DEFAULT (0) FOR [PointsBonusEmpDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0B5CAFEA]  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0C50D423]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0D44F85C]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0E391C95]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0F2D40CE]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__10216507]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__11158940]  DEFAULT (0) FOR [PointsReturnedHE]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__1209AD79]  DEFAULT (0) FOR [PointsReturnedBUS]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__12FDD1B2]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__13F1F5EB]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__PNTDE__14E61A24]  DEFAULT (0) FOR [PNTDEBIT]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__PNTMO__15DA3E5D]  DEFAULT (0) FOR [PNTMORT]
GO
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__PNTHO__16CE6296]  DEFAULT (0) FOR [PNTHOME]
GO
