USE [577Wheaton_GlennEllyn]
GO
/****** Object:  Table [dbo].[FixPoints]    Script Date: 09/25/2009 13:56:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FixPoints](
	[tipnumber] [varchar](15) NOT NULL,
	[PointsGiven] [numeric](38, 0) NULL,
	[Dollars] [numeric](38, 0) NULL,
	[CorrectPoints] [numeric](38, 0) NULL,
	[PointsDue] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
