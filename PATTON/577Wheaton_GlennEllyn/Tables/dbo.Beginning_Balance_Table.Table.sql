USE [577Wheaton_GlennEllyn]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 09/25/2009 13:56:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__30F848ED]  DEFAULT (0) FOR [MonthBeg1]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__31EC6D26]  DEFAULT (0) FOR [MonthBeg2]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__32E0915F]  DEFAULT (0) FOR [MonthBeg3]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__33D4B598]  DEFAULT (0) FOR [MonthBeg4]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__34C8D9D1]  DEFAULT (0) FOR [MonthBeg5]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__35BCFE0A]  DEFAULT (0) FOR [MonthBeg6]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__36B12243]  DEFAULT (0) FOR [MonthBeg7]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__37A5467C]  DEFAULT (0) FOR [MonthBeg8]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__38996AB5]  DEFAULT (0) FOR [MonthBeg9]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__398D8EEE]  DEFAULT (0) FOR [MonthBeg10]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__3A81B327]  DEFAULT (0) FOR [MonthBeg11]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__3B75D760]  DEFAULT (0) FOR [MonthBeg12]
GO
