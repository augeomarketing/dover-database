USE [512NEBATCommercial]
GO
/****** Object:  Table [dbo].[Missingnames]    Script Date: 09/24/2009 09:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Missingnames](
	[ssn] [nvarchar](9) NULL,
	[bank] [nvarchar](4) NULL,
	[agent] [nvarchar](4) NULL,
	[ddanum] [nvarchar](11) NULL,
	[acctnum] [nvarchar](25) NULL,
	[addr1] [nvarchar](40) NULL,
	[citystate] [nvarchar](40) NULL,
	[zip] [nvarchar](10) NULL
) ON [PRIMARY]
GO
