USE [512NEBATCommercial]
GO
/****** Object:  Table [dbo].[fixname]    Script Date: 09/24/2009 09:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixname](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[lastname] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
