USE [593MononaCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spFlagWelcomeKit]    Script Date: 09/25/2009 14:38:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spFlagWelcomeKit] @Enddate char(10)
as

drop table workkits

select distinct tipnumber, left(acctid,6) as bin
into workkits
from affiliat 
where dateadded=@Enddate 

update welcomekit
set flag='1'
where tipnumber in (select tipnumber from workkits where bin='447011')

update welcomekit
set flag='3'
where tipnumber in (select tipnumber from workkits where bin='418289')
GO
