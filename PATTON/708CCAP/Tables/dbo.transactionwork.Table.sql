USE [708CCAP]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 11/17/2010 10:44:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](25) NULL,
	[SystemBankIdentifier] [varchar](4) NULL,
	[PrincipleBankIdentifier] [varchar](3) NULL,
	[AgentBankIdentifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[AddPoints] [decimal](18, 2) NULL,
	[ReturnPoints] [decimal](18, 2) NULL,
	[BonusPoints] [decimal](18, 2) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
