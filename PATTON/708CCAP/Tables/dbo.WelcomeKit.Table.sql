USE [708CCAP]
GO
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 11/17/2010 10:44:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WelcomeKit](
	[AcctNbr] [varchar](15) NULL,
	[MbrName] [varchar](40) NULL,
	[JointMbrName] [varchar](40) NULL,
	[MbrNameFill] [varchar](160) NULL,
	[StatusCode] [char](1) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[MbrAddress1] [varchar](40) NULL,
	[MbrAddress2] [varchar](40) NULL,
	[MbrAddress3] [varchar](40) NULL,
	[Mbraddress4] [varchar](40) NULL,
	[MbrZipCode] [varchar](15) NULL,
	[lastname] [char](40) NULL,
	[MbrTelephone] [varchar](10) NULL,
	[WorkTelephone] [varchar](10) NULL,
	[DateAdded] [datetime] NULL,
	[Open1] [varchar](96) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[MbrCity] [varchar](40) NULL,
	[MbrState] [varchar](2) NULL,
	[CustID] [varchar](9) NULL,
	[Misc7] [varchar](8) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
