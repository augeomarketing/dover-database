USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 11/17/2010 10:42:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @pagect  nvarchar(3)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @pagect = (select count(*) from welcomekit)

set @filename='W' + @TipPrefix + @currentdate + '-' + @pagect + '.xls'
 
set @newname='O:\708\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='O:\708\Output\WelcomeKits\' + @filename
GO
