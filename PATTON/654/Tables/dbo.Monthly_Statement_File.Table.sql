USE [654]
GO
/******. Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 12/02/2013 14:59:39 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__07C12930]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__07C12930]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__07C12930]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__08B54D69]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__08B54D69]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__08B54D69]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__09A971A2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__09A971A2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__09A971A2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0A9D95DB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0A9D95DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0A9D95DB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0B91BA14]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0B91BA14]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0B91BA14]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0C85DE4D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0C85DE4D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0C85DE4D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0D7A0286]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0D7A0286]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0D7A0286]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0E6E26BF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0E6E26BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0E6E26BF]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0F624AF8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0F624AF8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0F624AF8]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__10566F31]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__10566F31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__10566F31]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__114A936A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__114A936A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__114A936A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__123EB7A3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__123EB7A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__123EB7A3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__1332DBDC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__1332DBDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1332DBDC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[AcctID] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__07C12930]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__07C12930]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__07C12930]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__08B54D69]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__08B54D69]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__08B54D69]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__09A971A2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__09A971A2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__09A971A2]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0A9D95DB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0A9D95DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__0A9D95DB]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0B91BA14]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0B91BA14]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__0B91BA14]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0C85DE4D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0C85DE4D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__0C85DE4D]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0D7A0286]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0D7A0286]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__0D7A0286]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0E6E26BF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0E6E26BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__0E6E26BF]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__0F624AF8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0F624AF8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__0F624AF8]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__10566F31]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__10566F31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__10566F31]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__114A936A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__114A936A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__114A936A]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__123EB7A3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__123EB7A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__123EB7A3]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__1332DBDC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__1332DBDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1332DBDC]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
END


End
GO
