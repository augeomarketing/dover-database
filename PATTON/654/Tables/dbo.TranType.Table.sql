USE [654]
GO
/******. Object:  Table [dbo].[TranType]    Script Date: 12/02/2013 14:43:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranType]') AND type in (N'U'))
DROP TABLE [dbo].[TranType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
END
GO
