USE [654]
GO
/******. Object:  Table [dbo].[TRANSsTANDARD]    Script Date: 12/02/2013 14:43:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARD]') AND type in (N'U'))
DROP TABLE [dbo].[TRANSsTANDARD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARD]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TRANSsTANDARD](
	[TFNO] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANNUM] [nvarchar](4) NULL,
	[TRANAMT] [nchar](15) NULL,
	[TRANTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[CRDACTVLDT] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
