USE [654]
GO
/******. Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 12/02/2013 14:43:03 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__74AE54BC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__74AE54BC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__74AE54BC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__75A278F5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__75A278F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__75A278F5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__76969D2E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__76969D2E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__76969D2E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__778AC167]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__778AC167]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__778AC167]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__787EE5A0]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__787EE5A0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__787EE5A0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonusDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__797309D9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__797309D9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__7A672E12]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7B5B524B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__7B5B524B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7C4F7684]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__7C4F7684]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7D439ABD]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7D439ABD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__7D439ABD]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_Currentend]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_Currentend]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF_Monthly_Audit_ErrorFile_Currentend]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__74AE54BC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__74AE54BC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__74AE54BC]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__75A278F5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__75A278F5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__75A278F5]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__76969D2E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__76969D2E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__76969D2E]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__778AC167]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__778AC167]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__778AC167]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__787EE5A0]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__787EE5A0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__787EE5A0]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonusDB]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__797309D9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__797309D9]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7A672E12]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7B5B524B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7B5B524B]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7C4F7684]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7C4F7684]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7D439ABD]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7D439ABD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7D439ABD]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Audit_ErrorFile_Currentend]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Audit_ErrorFile_Currentend]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_Currentend]  DEFAULT (0) FOR [Currentend]
END


End
GO
