USE [654]
GO
/******. Object:  Table [dbo].[Account_Reference]    Script Date: 12/02/2013 14:43:03 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_Reference_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account_Reference]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_Reference_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account_Reference] DROP CONSTRAINT [DF_Account_Reference_DateAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateAdded] [date] NOT NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_Reference_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account_Reference]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_Reference_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account_Reference] ADD  CONSTRAINT [DF_Account_Reference_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
END


End
GO
