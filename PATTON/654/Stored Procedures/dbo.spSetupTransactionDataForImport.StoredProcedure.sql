USE [654]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 11/09/2015 14:09:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO

USE [654]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 11/09/2015 14:09:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Changes to speed up process bring in only trans for this bin and current month  */
-- SEB 2/7/14 added process codes for credit cards
-- =============================================

-- S Blanchette 7/2013 Change to use RMNITransaction view
CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] 
	@StartDate char(10)
	, @EndDate char(10)
	, @TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table transwork
	truncate table transstandard


	/****************************************************************************************************/
	/*  If FI wants to reward for payment transactions then you must include the 500xxx series codes as seen below  */
	/*   */
	/*  If not to reward for payments then the 500xxx codes must be removed */                                   

	select *
	into #tempTrans 
	from Rewardsnow.dbo.vw_654_TRAN_SOURCE_1 with (nolock)
	where Trandate>=cast(@StartDate as DATE) and Trandate<=cast(@EndDate as DATE) /* SEB005 */


	insert into transwork ( TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID )
	select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID 
	 from #tempTrans
	-- seb 2/7/14 where sic<>'6011' and processingcode in ('000000', '002000', '200020', '200040', '500000', '500020', '500010') and (left(msgtype,2) in ('02', '04')) 
	where  sic<>'6011' and processingcode in ('000000', '001000', '002000', '003000', '200000', '200020', '200030', '200040', '500000', '500020', '500010', '503000') and (left(msgtype,2) in ('02', '04')) 

	/****************************************************************************************/
	/*  The next section controls the awarding of points for signature and pin transactions */
	drop table #temptrans

	update TRANSWORK
	set TIPNUMBER = afs.TIPNUMBER
	from TRANSWORK tw join AFFILIAT_Stage afs on tw.PAN = afs.ACCTID

	delete from TRANSWORK
	where TIPNUMBER is null

	update transwork
	set TypeCard=tfr.TypeCard
	from transwork trwk join COOPWork.dbo.TipFirstReference tfr on rtrim(pan)like rtrim(tfr.bin)+'%'

	-- Credit Card
	update transwork
	set points=ROUND(((amounttran/100)), 10, 0)
	where TypeCard='C'

	-- Signature Debit
	update transwork
	set points=ROUND(((amounttran/100)/2), 10, 0)
	where netid in('MCI', 'VNT')
		and TypeCard='D' 
		
	-- PIN Debit
	update transwork
	set points='0'
	where netid not in('MCI', 'VNT')
		and TypeCard='D' 
	
	--Credit transactions
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' 
		from transwork
		where typecard = 'C' and processingcode in ('000000', '001000', '003000', '500000', '503000') and left(msgtype,2) in ('02') 		        	
		group by tipnumber, Pan
		
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' 
		from transwork
		where typecard = 'C' and (processingcode in ('200030', '200000') or left(msgtype,2) in ('04'))
		group by tipnumber, Pan
		
	--HOLIDAY BONUS AWARD
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		SELECT	tipnumber, @enddate, Pan, 'G2', sum(NumberOfTrans), sum(points), 'Double Point Bonus', '1', ' ' 
		FROM	transwork
		WHERE	typecard = 'C' and processingcode in ('000000', '001000', '003000', '500000', '503000') and left(msgtype,2) in ('02') 
			AND	PAN like '528240%'
			AND	((TRANDATE >= '20151125' and TRANDATE <= '20151130') OR (TRANDATE >= '20151220' and TRANDATE <= '20151225'))    	
		GROUP BY	tipnumber, Pan	
	
	--Put to standard transtaction file format purchases.
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'Debit Purchase', '1', ' ' from transwork
	where typecard = 'D' and processingcode in ('000000', '001000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02')  		        	
	group by tipnumber, Pan
		
	--Put to standard transtaction file format returns.
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'Debit Return', '-1', ' ' from transwork
	where typecard = 'D' and (processingcode in ('200020', '200040') or left(msgtype,2) in ('04'))
	group by tipnumber, Pan

	delete from Transstandard
	where TRANAMT='0'

END


GO


