USE [654]
GO
/****** Object:  StoredProcedure [dbo].[spGetandGenerateTipNumbers_Stage]    Script Date: 12/17/2013 14:01:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetandGenerateTipNumbers_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetandGenerateTipNumbers_Stage]
	@EndDate char(10)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @SSN nchar(16), @PAN nchar(16), @PrimDDA nchar(16), @1stDDA nchar(16), @2ndDDA nchar(16), @3rdDDA nchar(16), @4thDDA nchar(16), @5thDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15)
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare DEMO_crsr cursor
for select TipFirst, Tipnumber, SSN, Pan, [Prim DDA], [1st DDA], [2nd DDA], [3rd DDA], [4th DDA], [5th DDA] 
from Demographicin
for update
/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber=''0''
		
fetch DEMO_crsr into @tipFirst, @Tipnumber, @SSN, @PAN, @PrimDDA, @1stDDA, @2ndDDA, @3rdDDA, @4thDDA, @5thDDA
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
set @worktip=''0''
		
-- Check if SSN already assigned to a tip number.
if @SSN is not null and left(@SSN,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
Begin
	set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
	If @workTip<>''0'' and @worktip is not null
	Begin
		if @Pan is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @Pan, left(@worktip,3), @EndDate)
		End
		if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
		End
		if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
		End
		if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
		End
		if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
		End
		if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
		End
		if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
		End
	End
END
ELSE
	-- Check if PAN already assigned to a tip number.
	if @PAN is not null and left(@PAN,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
	Begin
		set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
		If @workTip<>''0'' and @worktip is not null
		Begin
			if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @SSN, left(@worktip,3), @EndDate)
			End
			if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
			End
			if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
			End
			if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
			End
			if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
			End
			if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
			End
			if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
			Begin
				insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
				values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
			End
		End
	END
	ELSE
		-- Check if Primary DDA already assigned to a tip number.
		if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
		Begin
			set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
			If @workTip<>''0'' and @worktip is not null
			Begin
				if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @SSN, left(@worktip,3), @EndDate)
				End
				if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @PAN, left(@worktip,3), @EndDate)
				End
				if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
				End
				if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
				End
				if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
				End
				if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
				End
				if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
				Begin
					insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
					values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
				End
			End
		END
		ELSE
			-- Check if 1st DDA already assigned to a tip number.
			if @1stDDA is not null and left(@1stDDA,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
			Begin
				set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
				If @workTip<>''0'' and @worktip is not null
				Begin
					if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @SSN, left(@worktip,3), @EndDate)
					End
					if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @PAN, left(@worktip,3), @EndDate)
					End
					if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
					End
					if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
					End
					if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
					End
					if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
					End
					if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
					Begin
						insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
						values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
					End
				End
			END
			ELSE
				-- Check if 2nd DDA already assigned to a tip number.
				if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
				Begin
					set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
					If @workTip<>''0'' and @worktip is not null
					Begin
						if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @SSN, left(@worktip,3), @EndDate)
						End
						if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @PAN, left(@worktip,3), @EndDate)
						End
						if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
						End
						if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
						End
						if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
						End
						if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
						End
						if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
						Begin
							insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
							values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
						End
					End
				END
				ELSE
					-- Check if 3rd DDA already assigned to a tip number.
					if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
					Begin
						set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
						If @workTip<>''0'' and @worktip is not null
						Begin
							if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @SSN, left(@worktip,3), @EndDate)
							End
							if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @PAN, left(@worktip,3), @EndDate)
							End
							if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
							End
							if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
							End
							if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
							End
							if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
							End
							if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
							End
						End
					END
					ELSE
					-- Check if 4th DDA already assigned to a tip number.
					if @4thDDA is not null and left(@4thDDA,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
					Begin
						set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
						If @workTip<>''0'' and @worktip is not null
						Begin
							if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @SSN, left(@worktip,3), @EndDate)
							End
							if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @PAN, left(@worktip,3), @EndDate)
							End
							if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
							End
							if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
							End
							if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
							End
							if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
							End
							if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
							Begin
								insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
								values(@Worktip, @5thDDA, left(@worktip,3), @EndDate)
							End
						End
					END
					ELSE
						-- Check if 5th DDA already assigned to a tip number.
						if @5thDDA is not null and left(@5thDDA,1)<> '' '' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
						Begin
							set @Worktip=(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
							If @workTip<>''0'' and @worktip is not null
							Begin
								if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @SSN, left(@worktip,3), @EndDate)
								End
								if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @PAN, left(@worktip,3), @EndDate)
								End
								if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
								End
								if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @1stDDA, left(@worktip,3), @EndDate)
								End
								if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @2ndDDA, left(@worktip,3), @EndDate)
								End
								if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @3rdDDA, left(@worktip,3), @EndDate)
								End
								if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
								Begin
									insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
									values(@Worktip, @4thDDA, left(@worktip,3), @EndDate)
								End
							End
						END
	If @workTip=''0'' or @worktip is null
	Begin
	/*********  Start SEB002  *************************/
		--set @newtipnumber=(select max(tipnumber) from Account_Reference_Stage)				
		--if @newtipnumber is null or left(@newtipnumber,1)='' '' 
		--begin
		--	set @newtipnumber=@tipfirst+''000000000000''		
		--end				
		--set @newtipnumber=@newtipnumber + ''1''
	
		declare @LastTipUsed char(15)
		
		exec rewardsnow.dbo.spGetLastTipNumberUsed ''654'', @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @newtipnumber = cast(@LastTipUsed as bigint) + 1  
	/*********  End SEB002  *************************/
		set @UpdateTip=@newtipnumber
		
		if @SSN is not null and left(@SSN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @SSN, left(@UpdateTip,3), @EndDate)
		End
		if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PAN)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @PAN, left(@UpdateTip,3), @EndDate)
		End
		if @PrimDDA is not null and left(@PrimDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@PrimDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @PrimDDA, left(@UpdateTip,3), @EndDate)
		End
		if @1stDDA is not null and left(@1stDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@1stDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @1stDDA, left(@UpdateTip,3), @EndDate)
		End
		if @2ndDDA is not null and left(@2ndDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@2ndDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @2ndDDA, left(@UpdateTip,3), @EndDate)
		End
		if @3rdDDA is not null and left(@3rdDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@3rdDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @3rdDDA, left(@UpdateTip,3), @EndDate)
		End
		if @4thDDA is not null and left(@4thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@4thDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @4thDDA, left(@UpdateTip,3), @EndDate)
		End
		if @5thDDA is not null and left(@5thDDA,1)<> '' '' and not exists(select tipnumber from Account_Reference_Stage where acctnumber=@5thDDA)
		Begin
			insert into Account_Reference_Stage (Tipnumber, Acctnumber, TipFirst, DateAdded)
			values(@UpdateTip, @5thDDA, left(@UpdateTip,3), @EndDate)
		End

		exec RewardsNOW.dbo.spPutLastTipNumberUsed ''654'', @newtipnumber  /*SEB002 */
	
	End
	Else
	If @workTip<>''0'' and @worktip is not null
	Begin
		set @updateTip=@worktip
	End	
	update demographicin	
	set tipnumber = @UpdateTip 
	where current of demo_crsr 
	goto Next_Record
Next_Record:
		fetch DEMO_crsr into @tipFirst, @Tipnumber, @SSN, @PAN, @PrimDDA, @1stDDA, @2ndDDA, @3rdDDA, @4thDDA, @5thDDA
end
Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr
END
' 
END
GO
