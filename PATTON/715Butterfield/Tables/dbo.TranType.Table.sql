/****** Object:  Table [dbo].[TranType]    Script Date: 02/12/2009 13:48:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TranType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
END
GO
