/****** Object:  Table [dbo].[Input_Customer]    Script Date: 02/12/2009 13:47:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_Customer]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardNumber] [varchar](25) NULL,
	[SystembankID] [float] NULL,
	[Principlebankid] [float] NULL,
	[AgentBankID] [float] NULL,
	[AcctName1] [varchar](50) NULL,
	[AcctName2] [varchar](50) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[HomePhone] [varchar](20) NULL,
	[StatusCode] [varchar](2) NULL,
	[External Status Code] [varchar](2) NULL,
	[CustID] [varchar](16) NULL,
	[Secondary Social Security Number Text] [varchar](10) NULL,
	[Account Identifier2] [varchar](10) NULL,
	[External Status Code Last Change Date] [varchar](2) NULL,
	[Miscellaneous Seventh Text] [varchar](8) NULL,
	[TipNumber] [varchar](15) NULL,
	[Lastname] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[StatusDescription] [varchar](50) NULL,
	[Misc5] [varchar](4) NULL CONSTRAINT [DF_Input_Customer_Misc5]  DEFAULT (0)
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
