/****** Object:  Table [dbo].[lstnamework]    Script Date: 02/12/2009 13:47:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[lstnamework]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[lstnamework](
	[tipnumber] [varchar](15) NULL,
	[acctnbr] [varchar](15) NULL,
	[lastname] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
