/****** Object:  Table [dbo].[Bonus Table]    Script Date: 02/12/2009 13:46:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Bonus Table]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Bonus Table](
	[TipNumber] [varchar](15) NULL,
	[CustID] [varchar](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
