/****** Object:  Table [dbo].[PendingPurge]    Script Date: 02/12/2009 13:47:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[PendingPurge]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
