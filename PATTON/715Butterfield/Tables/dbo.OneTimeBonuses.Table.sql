/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 02/12/2009 13:47:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[CustID] [varchar](9) NULL,
	[misc7] [varchar](5) NULL,
	[DateAwarded] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
