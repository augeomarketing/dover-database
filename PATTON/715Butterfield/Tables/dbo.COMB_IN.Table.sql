/****** Object:  Table [dbo].[COMB_IN]    Script Date: 02/12/2009 13:46:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[COMB_IN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[COMB_IN](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[LAST6_PRI] [varchar](6) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[LAST6_SEC] [varchar](6) NULL,
	[POINT_SEC] [float] NULL,
	[POINT_TOT] [float] NULL,
	[TANDATE] [varchar](8) NULL,
	[ERRMSG] [varchar](80) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
