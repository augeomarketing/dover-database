/****** Object:  Table [dbo].[COMB_err]    Script Date: 02/12/2009 13:46:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[COMB_err]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[COMB_err](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[TRANDATE] [datetime] NULL,
	[ERRMSG] [varchar](80) NULL,
	[ERRMSG2] [varchar](80) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
