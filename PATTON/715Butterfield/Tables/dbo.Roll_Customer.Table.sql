/****** Object:  Table [dbo].[Roll_Customer]    Script Date: 02/12/2009 13:48:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Roll_Customer]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Roll_Customer](
	[CardNumber] [char](25) NULL,
	[CompanyID] [varchar](6) NULL,
	[SystemBankID] [char](10) NULL,
	[PrincipleBankID] [char](10) NULL,
	[AgentBankID] [char](10) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[InternalStatusCode] [char](1) NULL,
	[ExternalStatusCode] [char](1) NULL,
	[CustID] [char](9) NULL,
	[AcctID2] [char](40) NULL,
	[Misc7] [char](8) NULL,
	[Misc8] [char](8) NULL,
	[CR3] [varchar](25) NULL,
	[TipNumber] [varchar](15) NULL,
	[Address4] [varchar](50) NULL,
	[LstName] [char](40) NULL,
	[SegmentCode] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
