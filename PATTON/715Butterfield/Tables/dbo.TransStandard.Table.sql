/****** Object:  Table [dbo].[TransStandard]    Script Date: 02/12/2009 13:48:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TransStandard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TransStandard](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TIP] [nvarchar](15) NULL,
	[TranDate] [nvarchar](10) NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](50) NULL,
	[Ratio] [nvarchar](4) NULL
) ON [PRIMARY]
END
GO
