/****** Object:  StoredProcedure [dbo].[spTransactionScrub]    Script Date: 02/12/2009 13:46:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spTransactionScrub]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spTransactionScrub]
 AS

--update input_transactions set custid = b.custid
--from input_transactions a, roll_commercial b
--where a.tipnumber = b.tipnumber and a.custid is null  


--------------- Input Transaction table

Insert into Input_Transactions_error 
	select * from Input_Transactions 
	where (cardnumber is null or cardnumber = '' '') or
	      (tipnumber is null or tipnumber = '' '') or
	      (purchase is null  or [returns] is null or bonus is null)
	       
Delete from Input_Transactions
where (cardnumber is null or cardnumber = '' '') or
      (tipnumber is null or tipnumber = '' '') or
      (purchase is null  or (purchase =  0 and [Returns] = 0 and Bonus = 0))


/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transactions 
set  purchase = round(purchase,0),  [Returns] =  round([returns],0), bonus = round(Bonus,0)' 
END
GO
