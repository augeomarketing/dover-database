/****** Object:  StoredProcedure [dbo].[spRollAccountNames]    Script Date: 02/12/2009 13:46:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spRollAccountNames]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRollAccountNames]  AS

update input_customer  set acctname2 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
(a.acctname2 is null or a.acctname2 = '' '') and a.acctname1 != b.acctname1 

update input_customer  set acctname3 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and (a.acctname3 is null or a.acctname3 = '' '')
and (a.acctname1 != b.acctname1 and a.acctname2 != b.acctname1)  

update input_customer  set acctname4 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
(a.acctname4 is null or a.acctname4 = '' '')and (a.acctname1 != b.acctname1 and a.acctname2 != b.acctname1
and a.acctname3 != b.acctname1)    

update input_customer  set acctname5 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and (a.acctname5 is null or a.acctname5 = '' '') and
(a.acctname1 != b.acctname1 and a.acctname2 != b.acctname1
and a.acctname3 != b.acctname1 and a.acctname4 != b.acctname1)

update input_customer  set acctname6 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and rtrim(b.acctname1) != rtrim(a.acctname5) and
(a.acctname6 is null or a.acctname6 = '' '') and (a.acctname1 != b.acctname1 and a.acctname2 != b.acctname1
and a.acctname3 != b.acctname1 and a.acctname4 != b.acctname1 and a.acctname5 != b.acctname1)' 
END
GO
