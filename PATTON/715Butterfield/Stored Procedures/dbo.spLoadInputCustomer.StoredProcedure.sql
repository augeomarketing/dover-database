/****** Object:  StoredProcedure [dbo].[spLoadInputCustomer]    Script Date: 02/12/2009 13:46:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spLoadInputCustomer]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadInputCustomer] @trandate varchar(10)
AS

truncate table input_customer

INSERT INTO Input_Customer  (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Customer

--INSERT INTO Input_Customer (tipnumber)
--SELECT DISTINCT tipnumber FROM Roll_Commercial

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_customer b
WHERE     a.tipnumber = b.tipnumber

--UPDATE    input_customer
--SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
--                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
--                     principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
--FROM         input_customer a, roll_commercial b
--WHERE     a.tipnumber = b.tipnumber

update input_customer set statusdescription = ''Active[A]'' where statuscode = ''A''

UPDATE    Input_Customer SET dateadded =  (SELECT dateadded FROM CUSTOMER_Stage
WHERE  customer_stage.tipnumber = input_customer.tipnumber)

UPDATE   input_CUSTOMER SET dateadded = CONVERT(datetime, @trandate)
WHERE     (dateadded  IS NULL) or dateadded = '' ''

update input_customer set misc5 = ''5000''
where systembankid = ''4520'' and dateadded = @trandate

update input_customer set misc5 = ''3000''
where systembankid = ''8698'' and dateadded = @trandate' 
END
GO
