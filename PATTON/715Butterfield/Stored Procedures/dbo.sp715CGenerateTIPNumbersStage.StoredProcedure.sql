USE [715ButterfieldBank]
GO
/****** Object:  StoredProcedure [dbo].[sp715CGenerateTIPNumbersStage]    Script Date: 02/12/2009 13:46:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp715CGenerateTIPNumbersStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN

EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp715CGenerateTIPNumbersStage]'
AS 

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = '' '')

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = '' '')

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = '' '')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed ''715'', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 715000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = '' ''

exec RewardsNOW.dbo.spPutLastTipNumberUsed ''715'', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and a.tipnumber is null or a.tipnumber = '' ''' 
END
GO
