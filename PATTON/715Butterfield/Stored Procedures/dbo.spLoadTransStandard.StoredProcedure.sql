/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 02/12/2009 13:46:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10)
as

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1), @BonusPoints varchar(4)

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], ''BI'', ''1'', convert(char(15), [bonus])  from Input_Transactions 
where (bonus <> ''0'')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], ''63'', ''1'', convert(char(15), [purchase])  from Input_Transactions
where (purchase > ''0'')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], ''33'',''1'', convert(char(15), [returns])  from Input_Transactions
where ([returns] > ''0'')

/******************************************************************************/
-- Load the TransStandard table with  NEW Customer BONUS
/******************************************************************************/
set @Trandate=@dateadded

	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select  tipnumber, misc5
from input_customer
where misc5 > ''0''

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber,@BonusPoints

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @acctid = (select acctnum from transstandard
				where rowid  = (select max(rowid) from transstandard where tip = @tipnumber and (acctnum is not null or acctnum <> '' '')))

		INSERT INTO transstandard (Tip,tranDate,acctnum,TranCode,Trannum,tranamt,Ratio,trantype)
        		Values(@Tipnumber, @Trandate, @acctid,''BA'', ''1'', @BonusPoints, ''1'', ''Bonus for New Customer'') 
 
		fetch tip_crsr into @tipnumber,@BonusPoints
End

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr' 
END
GO
