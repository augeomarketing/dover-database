/****** Object:  StoredProcedure [dbo].[spRemovePunctuation]    Script Date: 02/12/2009 13:46:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spRemovePunctuation]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRemovePunctuation] AS

update roll_customer
set acctname1=replace(acctname1,char(39), '' ''),acctname2=replace(acctname2,char(39), '' ''), address1=replace(address1,char(39), '' ''),
address4=replace(address4,char(39), '' ''),   city=replace(city,char(39), '' ''),  lstname=replace(lstname,char(39), '' '')
 
update roll_customer
set acctname1=replace(acctname1,char(140), '' ''),acctname2=replace(acctname2,char(140), '' ''), address1=replace(address1,char(140), '' ''),
address4=replace(address4,char(140), '' ''),   city=replace(city,char(140), '' ''),  lstname=replace(lstname,char(140), '' '')

update roll_customer
set acctname1=replace(acctname1,char(44), '' ''),acctname2=replace(acctname2,char(44), '' ''), address1=replace(address1,char(44), '' ''),
address4=replace(address4,char(44), '' ''),   city=replace(city,char(44), '' ''),  lstname=replace(lstname,char(44), '' '')
  
update roll_customer
set acctname1=replace(acctname1,char(46), '' ''),acctname2=replace(acctname2,char(46), '' ''), address1=replace(address1,char(46), '' ''),
address4=replace(address4,char(46), '' ''),   city=replace(city,char(46), '' ''),  lstname=replace(lstname,char(46), '' '')

/*
update roll_commercial
set acctname1=replace(acctname1,char(39), '' ''),acctname2=replace(acctname2,char(39), '' ''), address1=replace(address1,char(39), '' ''),
address4=replace(address4,char(39), '' ''),   city=replace(city,char(39), '' ''),  lstname=replace(lstname,char(39), '' '')
 
update roll_commercial
set acctname1=replace(acctname1,char(140), '' ''),acctname2=replace(acctname2,char(140), '' ''), address1=replace(address1,char(140), '' ''),
address4=replace(address4,char(140), '' ''),   city=replace(city,char(140), '' ''),  lstname=replace(lstname,char(140), '' '')

update roll_commercial
set acctname1=replace(acctname1,char(44), '' ''),acctname2=replace(acctname2,char(44), '' ''), address1=replace(address1,char(44), '' ''),
address4=replace(address4,char(44), '' ''),   city=replace(city,char(44), '' ''),  lstname=replace(lstname,char(44), '' '')
  
update roll_commercial
set acctname1=replace(acctname1,char(46), '' ''),acctname2=replace(acctname2,char(46), '' ''), address1=replace(address1,char(46), '' ''),
address4=replace(address4,char(46), '' ''),   city=replace(city,char(46), '' ''),  lstname=replace(lstname,char(46), '' '')

*/' 
END
GO
