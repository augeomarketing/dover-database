USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_cardtrans_error]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_cardtrans_error](
	[input_cardtrans_error_id] [int] NOT NULL,
	[acctnbr] [varchar](20) NULL,
	[firstname] [varchar](40) NULL,
	[lastname] [varchar](40) NULL,
	[address] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](40) NULL,
	[zip] [varchar](40) NULL,
	[phone] [varchar](10) NULL,
	[relationshipcode] [int] NULL,
	[accttype] [char](2) NULL,
	[trantype] [varchar](50) NULL,
	[tranamt] [decimal](18, 0) NULL,
	[trancnt] [int] NULL,
	[cardnbr] [varchar](10) NULL,
	[priorcardnbr] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[tipnumber] [varchar](15) NULL,
	[points] [int] NULL,
	[ImportType] [char](1) NULL,
	[errorreason] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
