USE [209Oregon]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 02/11/2011 11:27:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [float] NULL,
 CONSTRAINT [PK_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
