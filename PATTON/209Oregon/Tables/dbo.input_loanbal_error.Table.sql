USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_loanbal_error]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_loanbal_error](
	[input_loanbal_error_id] [int] NOT NULL,
	[acctnbr] [varchar](20) NULL,
	[loanid] [varchar](20) NULL,
	[avgbal] [decimal](18, 0) NULL,
	[loantype] [varchar](30) NULL,
	[tipnumber] [varchar](15) NULL,
	[trancode] [varchar](2) NULL,
	[points] [int] NULL,
	[errorreason] [varchar](30) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
