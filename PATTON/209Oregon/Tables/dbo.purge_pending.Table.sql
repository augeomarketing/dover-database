USE [209Oregon]
GO
/****** Object:  Table [dbo].[purge_pending]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[purge_pending](
	[purge_pending_id] [int] IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[purgedate] [datetime] NOT NULL,
 CONSTRAINT [PK_purge_pending] PRIMARY KEY CLUSTERED 
(
	[purge_pending_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
