USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_joint]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_joint](
	[input_joint_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[acctnbr] [varchar](20) NOT NULL,
	[cardnbr] [varchar](20) NOT NULL,
	[firstname] [varchar](40) NOT NULL,
	[lastname] [varchar](40) NOT NULL,
	[tipnumber] [char](15) NULL,
 CONSTRAINT [PK_input_joint] PRIMARY KEY CLUSTERED 
(
	[input_joint_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
