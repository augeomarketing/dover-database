USE [209Oregon]
GO
/****** Object:  Table [dbo].[aux_trancode]    Script Date: 02/11/2011 11:27:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aux_trancode](
	[aux_trancode_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[aux_trancode_oregon] [varchar](10) NOT NULL,
	[aux_trancode_rewardsnow] [varchar](2) NOT NULL,
	[aux_trancode_importtype] [char](1) NOT NULL,
 CONSTRAINT [PK_aux_trancode] PRIMARY KEY CLUSTERED 
(
	[aux_trancode_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[aux_trancode] ADD  CONSTRAINT [DF_aux_trancode_aux_trancode_importtype]  DEFAULT ('C') FOR [aux_trancode_importtype]
GO
