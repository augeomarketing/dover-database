USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_loanbal]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_loanbal](
	[input_loanbal_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[acctnbr] [varchar](20) NULL,
	[loanid] [varchar](20) NULL,
	[avgbal] [decimal](18, 0) NULL,
	[loantype] [varchar](30) NULL,
	[tipnumber] [varchar](15) NULL,
	[trancode] [varchar](2) NULL,
	[points] [int] NULL,
 CONSTRAINT [PK_input_loanbal] PRIMARY KEY CLUSTERED 
(
	[input_loanbal_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
