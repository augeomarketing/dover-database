USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_sharebal_error]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_sharebal_error](
	[input_sharebal_error_id] [int] NOT NULL,
	[acctnbr] [varchar](20) NULL,
	[lastname] [varchar](40) NULL,
	[relationshipcode] [int] NULL,
	[accttype] [int] NULL,
	[avgbalance] [decimal](18, 0) NULL,
	[sharetype] [varchar](20) NULL,
	[tipnumber] [varchar](15) NULL,
	[trancode] [varchar](2) NULL,
	[points] [int] NULL,
	[errorreason] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
