USE [209Oregon]
GO
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 02/11/2011 11:27:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WelcomeKit](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](50) NOT NULL,
	[ACCTNAME2] [varchar](50) NULL,
	[ACCTNAME3] [varchar](50) NULL,
	[ACCTNAME4] [varchar](50) NULL,
	[ADDRESS1] [varchar](50) NULL,
	[ADDRESS2] [varchar](50) NULL,
	[ADDRESS3] [varchar](50) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[DDA] [varchar](50) NULL,
	[CARD] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
