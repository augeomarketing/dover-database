USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_cardtrans]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_cardtrans](
	[input_cardtrans_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[acctnbr] [varchar](20) NULL,
	[firstname] [varchar](40) NULL,
	[lastname] [varchar](40) NULL,
	[address] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](40) NULL,
	[zip] [varchar](40) NULL,
	[phone] [varchar](10) NULL,
	[relationshipcode] [int] NULL,
	[accttype] [char](2) NULL,
	[trantype] [varchar](50) NULL,
	[tranamt] [decimal](18, 0) NULL,
	[trancnt] [int] NULL,
	[cardnbr] [varchar](10) NULL,
	[priorcardnbr] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[tipnumber] [varchar](15) NULL,
	[points] [int] NULL,
	[importType] [char](1) NULL,
 CONSTRAINT [PK_cardtrans] PRIMARY KEY CLUSTERED 
(
	[input_cardtrans_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
