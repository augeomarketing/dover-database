USE [209Oregon]
GO
/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 02/11/2011 11:27:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_beginning_balance_month] PRIMARY KEY CLUSTERED 
(
	[monthbegin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[beginning_balance_month] ADD  CONSTRAINT [DF_beginning_balance_month_beginbalance]  DEFAULT (0) FOR [beginbalance]
GO
