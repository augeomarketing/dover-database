USE [209Oregon]
GO
/****** Object:  Table [dbo].[RN1_EstatementTip]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RN1_EstatementTip](
	[RN1_EstatementTip_Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TipNumber] [varchar](16) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
