USE [209Oregon]
GO
/****** Object:  Table [dbo].[input_transactionreport]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_transactionreport](
	[input_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[trantype] [varchar](50) NOT NULL,
	[tranamt] [decimal](18, 0) NOT NULL,
	[trancnt] [int] NOT NULL,
	[points] [int] NOT NULL,
 CONSTRAINT [PK_input_transactionreport] PRIMARY KEY CLUSTERED 
(
	[input_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[input_transactionreport] ADD  CONSTRAINT [DF_input_transactionreport_tranamt]  DEFAULT (0) FOR [tranamt]
GO
ALTER TABLE [dbo].[input_transactionreport] ADD  CONSTRAINT [DF_input_transactionreport_trancnt]  DEFAULT (0) FOR [trancnt]
GO
ALTER TABLE [dbo].[input_transactionreport] ADD  CONSTRAINT [DF_input_transactionreport_points]  DEFAULT (0) FOR [points]
GO
