USE [209Oregon]
GO
/****** Object:  UserDefinedFunction [dbo].[fnLinkCheckingNames]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnLinkCheckingNames] (@MemberNumber nvarchar(16))

RETURNS @LinkedNames table(Checking nvarchar(16) primary key, Name1 nvarchar(50), Name2 nvarchar(50), 
						Name3 nvarchar(50), Name4 nvarchar(50), Name5 nvarchar(50),
						Name6 nvarchar(50) )

as

BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(50))

	-- Get distinct names from NAME1 column for rows that match the @Checking parm
	insert into @names
	SELECT acctname FROM (
		select distinct rtrim(ltrim(firstname)) + ' ' + rtrim(ltrim(lastname)) as acctname
		from dbo.input_cardtrans
		where acctnbr = @MemberNumber 
		union
		select distinct rtrim(ltrim(firstname)) + ' ' + rtrim(ltrim(lastname)) as acctname
		from dbo.input_joint
		where acctnbr = @MemberNumber 
	) as temp
	ORDER BY acctname
	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
	select	@MemberNumber as AcctNbr, 
			(select AcctName from @names where id=1) as Name1,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END
GO
