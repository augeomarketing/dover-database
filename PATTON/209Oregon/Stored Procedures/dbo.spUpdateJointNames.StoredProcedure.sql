USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateJointNames]    Script Date: 02/11/2011 11:27:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spUpdateJointNames]

as

set nocount on

declare @memberNumber nvarchar(16)

select distinct custid
into #Members
from dbo.affiliat_stage src2 
where custid is not null and custid != ''
AND custid in (select acctnbr from input_joint)
--order by custid


declare csr cursor fast_forward /* fast_forward for better performance */
      for select custid
		from #Members
		--order by custid

open csr

fetch next from csr into @memberNumber

while @@FETCH_Status = 0
BEGIN

      update c
            set
				ACCTNAME1 = fn.name1,
                ACCTNAME2 = fn.name2, 
                ACCTNAME3 = fn.name3, 
                ACCTNAME4 = fn.name4, 
                ACCTNAME5 = fn.name5, 
                ACCTNAME6 = fn.name6 
	      from dbo.customer_stage c 
		join dbo.affiliat_stage a
			on c.tipnumber = a.tipnumber
		join dbo.fnLinkCheckingNames(@memberNumber) fn
        	    on a.custid = fn.checking
	
      fetch next from csr into @memberNumber
END

close csr
deallocate csr
GO
