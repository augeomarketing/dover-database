USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spUniqueDDA]    Script Date: 02/11/2011 11:27:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 11/21/07 */
/* Author: Allen B  */
/*  **************************************  */
/*  **************************************  */

CREATE     PROCEDURE [dbo].[spUniqueDDA]
AS 

TRUNCATE TABLE aux_uniqueacct

INSERT INTO aux_uniqueacct (acctid) 
SELECT DISTINCT acctnbr FROM Input_CardTrans
WHERE acctnbr NOT IN (SELECT acctid FROM aux_uniqueacct)

INSERT INTO aux_uniqueacct (acctid) 
SELECT DISTINCT RTRIM(LTRIM(acctnbr)) FROM Input_sharebal
WHERE RTRIM(LTRIM(acctnbr)) NOT IN (SELECT acctid FROM aux_uniqueacct)

INSERT INTO aux_uniqueacct (acctid) 
SELECT DISTINCT RTRIM(LTRIM(acctnbr)) FROM Input_loanbal
WHERE RTRIM(LTRIM(acctnbr)) NOT IN (SELECT acctid FROM aux_uniqueacct)


INSERT INTO aux_uniqueacct (acctid) 
SELECT DISTINCT RTRIM(LTRIM(acctnbr)) FROM input_joint
WHERE RTRIM(LTRIM(acctnbr)) NOT IN (SELECT acctid FROM aux_uniqueacct)
GO
