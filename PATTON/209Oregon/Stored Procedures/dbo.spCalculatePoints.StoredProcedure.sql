USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**************************************
Calculates points in input_transaction
**************************************  
*/
CREATE       PROCEDURE [dbo].[spCalculatePoints] AS   

UPDATE input_cardtrans
SET Points = ROUND(I.[tranamt] * F.pointFactor , 0)
FROM input_cardtrans I 
INNER JOIN aux_trancodeFactor F ON
I.Trancode = F.Trancode

UPDATE input_sharebal
SET Points = ROUND((I.[avgbalance] - (CONVERT(int,I.[avgbalance]) % 100)) * F.pointFactor , 0)
FROM input_sharebal I 
INNER JOIN aux_trancodeFactor F ON
I.Trancode = F.Trancode

UPDATE input_loanbal
SET Points = ROUND((I.[avgbal] - (CONVERT(int,I.[avgbal]) % 100))  * F.pointFactor , 0)
FROM input_loanbal I 
INNER JOIN aux_trancodeFactor F ON
I.Trancode = F.Trancode
GO
