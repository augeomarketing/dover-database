USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
****************************************************************************
   This imports data from input_custTran into the customer_STAGE  table
   it only updates the customer demographic data   

   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  
****************************************************************************
*/	

CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

UPDATE Customer_Stage
SET 
LASTNAME 	= LEFT(RTRIM(input_cardtrans.lastname),40)
,ACCTNAME1 	= LEFT(RTRIM(input_cardtrans.[firstname]),40 ) + ' ' + LEFT(RTRIM(input_cardtrans.[lastname]),40 )
,ADDRESS1 	= input_cardtrans.[address]
,ADDRESS4       = LEFT(LTRIM(RTRIM( input_cardtrans.[city])) + ' ' + LTRIM(RTRIM( input_cardtrans.[state])) + ' ' + LTRIM( RTRIM( input_cardtrans.[zip])) , 40 )
,CITY 		= input_cardtrans.[city]
,STATE		= LEFT(input_cardtrans.[state],2)
,ZIPCODE 	= LTRIM(input_cardtrans.[zip])
FROM input_cardtrans
WHERE input_cardtrans.TIPNUMBER = Customer_Stage.TIPNUMBER 

/*Add New Customers  -- based on INPUT_TRANSACTION                                                     */
INSERT INTO Customer_Stage
(
	TIPNUMBER, 
	TIPFIRST, 
	TIPLAST, 
	LASTNAME,
	ACCTNAME1, 
	ADDRESS1, 
	ADDRESS4,
	CITY, 
	STATE, 
	ZIPCODE , 
	DATEADDED, 
	Status,
	RUNAVAILABLE, 
	RUNBALANCE, 
	RUNREDEEMED, 
	RunAvaliableNew
)
SELECT 
	DISTINCT TIPNUMBER, 
	LEFT(TIPNUMBER,3), 
	RIGHT(RTRIM(TIPNUMBER),12), 
	LEFT(RTRIM(lastname),40),
	LEFT(RTRIM([firstname]),40 ) + ' ' + LEFT(RTRIM([lastname]),40 ),
	LEFT(RTRIM([address]),40), 
	LEFT( LTRIM(RTRIM(city)) + ' ' + LTRIM(RTRIM([state])) + ' ' + LTRIM( RTRIM([zip])),40),
	[city], 
	LEFT([state],2), 
	RTRIM([zip]),
	@EndDate, 
	'A'
	,0
	,0
	,0
	,0
	FROM  input_cardtrans 
	WHERE input_cardtrans.tipnumber NOT IN (SELECT TIPNUMBER FROM Customer_Stage)

/* set Default status to A */
UPDATE Customer_Stage
SET STATUS = 'A' 
WHERE STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
UPDATE Customer_Stage
SET StatusDescription = 
S.StatusDescription 
FROM status S INNER JOIN Customer_Stage C on S.Status = C.Status

/* Move Address2 to address1 if address1 is null */
UPDATE Customer_Stage 
SET 
Address1 = Address2, 
Address2 = NULL
WHERE address1 IS NULL
GO
