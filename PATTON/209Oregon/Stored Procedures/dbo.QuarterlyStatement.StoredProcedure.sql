USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[QuarterlyStatement]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[QuarterlyStatement] @StartDateParm CHAR(10), @EndDateParm CHAR(10)
AS 

DECLARE  @MonthBegin CHAR(2),  @SQLUpdate NVARCHAR(1000)
DECLARE @StartDate DATETIME
DECLARE @EndDate DATETIME

SET @Startdate = CONVERT(DATETIME, @StartDateParm + ' 00:00:00:001')
SET @Enddate = CONVERT(DATETIME, @EndDateParm + ' 23:59:59:990' )

SET @MonthBegin = MONTH(CONVERT(DATETIME, @StartDate) )

/* Load the statement file FROM the customer table  */
TRUNCATE TABLE Quarterly_Statement_File

INSERT INTO Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
SELECT tipnumber, acctname1, acctname2, address1, address2, address3, (RTRIM(city) + ' ' + RTRIM(state) + ' ' + zipcode)
FROM customer WHERE tipnumber NOT IN (SELECT tipnumber FROM RN1_EstatementTip)

UPDATE Quarterly_Statement_File
SET pointspurchasedCR =
	(SELECT SUM(points) FROM History 
		WHERE tipnumber = Quarterly_Statement_File.tipnumber 
			AND histdate >= @startdate 
			AND histdate<=@enddate AND trancode='63')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='63')

	--Load the statmement file  with CREDIT returns            
UPDATE Quarterly_Statement_File
	SET pointsreturnedCR = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='33')
	WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='33')

/* Load the statmement file with DEBIT purchases          */
UPDATE Quarterly_Statement_File
SET pointspurchasedDB = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '67')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '67')

/* Load the statmement file with DEBIT  returns            */
UPDATE Quarterly_Statement_File
SET pointsreturnedDB = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='37')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '37')

/* Load the statmement file with Loan Points          */
UPDATE Quarterly_Statement_File
SET PointsLoan = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = 'FP')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = 'FP')

/* Load the statmement file with Share Points          */
UPDATE Quarterly_Statement_File
SET PointsShare = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = 'FS')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = 'FS')


/* Load the statmement file with bonuses            */
UPDATE Quarterly_Statement_File
SET pointsbonus = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode LIKE 'B_')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode LIKE 'B_')

/* Load the statmement file with plus adjustments    */
UPDATE Quarterly_Statement_File
SET pointsadded = (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='IE')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
UPDATE Quarterly_Statement_File
SET pointsadded = pointsadded + (SELECT SUM(points) FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='DR')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber = Quarterly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = 'DR')


/* Load the statmement file with total point increases */
UPDATE Quarterly_Statement_File
SET pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsShare + pointsLoan

/* Load the statmement file with redemptions          */
UPDATE Quarterly_Statement_File
SET pointsredeemed=(SELECT SUM(points) FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode like 'R%')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode like 'R%')

/* Load the statmement file with minus adjustments    */
UPDATE Quarterly_Statement_File
SET pointssubtracted=(SELECT SUM(points) FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='DE')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='DE')

/* Add EP to  minus adjustments    */
UPDATE Quarterly_Statement_File
SET pointssubtracted= pointssubtracted + (SELECT SUM(points) FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='EP')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='EP')

/* Add expired Points */
UPDATE Quarterly_Statement_File
SET PointsExpire = (SELECT SUM(points) FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='XP')
WHERE EXISTS(SELECT * FROM History WHERE tipnumber=Quarterly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='XP')

/* Load the statmement file with total point decreases */
UPDATE Quarterly_Statement_File
SET pointsdecreased = pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire

/* Load the statmement file with the Beginning balance for the Month */
SET @SQLUpdate=N'UPDATE Quarterly_Statement_File
SET pointsbegin = (SELECT monthbeg'+ @MonthBegin + N' FROM Beginning_Balance_Table WHERE tipnumber = Quarterly_Statement_File.tipnumber)
WHERE EXISTS(SELECT * FROM Beginning_Balance_Table WHERE tipnumber=Quarterly_Statement_File.tipnumber)'

EXEC sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
UPDATE Quarterly_Statement_File
SET pointsend = pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the Quarterly statement table */
UPDATE Quarterly_Statement_file 
SET acctid = (SELECT TOP 1 a.acctid FROM affiliat a WHERE Quarterly_Statement_file.tipnumber = a.tipnumber)
GO
