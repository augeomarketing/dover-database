USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[usp_OptoutSweepAndProcess]    Script Date: 07/18/2013 16:21:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OptoutSweepAndProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_OptoutSweepAndProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OptoutSweepAndProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2013
-- Description:	To get opt out request from LRC and Process
-- =============================================
CREATE PROCEDURE [dbo].[usp_OptoutSweepAndProcess] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Tipnumber char(15), @DBNameNEXL varchar(50), @SQLDelete nvarchar(max), @SQLUpdate nvarchar(max)
	
	exec rewardsnow.dbo.usp_LRCOptOutUpdate @TipFirst, ''C''

	set @DBNameNEXL=(SELECT  rtrim(DBNameNEXL) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber= @TipFirst)
				
-- PURGE RECORDS ON WEB SIDE
	set @SQLDelete = N''delete [doolittle\web].'' + QuoteName(@DBNameNEXL) + N''.dbo.[1security] 
						from [doolittle\web].'' + QuoteName(@DBNameNEXL) + N''.dbo.[1security] sec join rewardsnow.dbo.optouttracking oot on sec.tipnumber = oot.tipnumber
						where sec.tipnumber is not null and oot.optoutposted is null''
	exec sp_executesql @SQLDelete
	
	set @SQLDelete = N''delete [doolittle\web].'' + QuoteName(@DBNameNEXL) + N''.dbo.Customer 
						from [doolittle\web].'' + QuoteName(@DBNameNEXL) + N''.dbo.Customer cs join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.tipnumber
						where cs.tipnumber is not null and oot.optoutposted is null''
	exec sp_executesql @SQLDelete

	set @SQLDelete = N''delete [doolittle\web].'' + QuoteName(@DBNameNEXL) + N''.dbo.Account 
						from [doolittle\web].'' + QuoteName(@DBNameNEXL) + N''.dbo.Account act join rewardsnow.dbo.optouttracking oot on act.tipnumber = oot.tipnumber
						where act.tipnumber is not null and oot.optoutposted is null''
	exec sp_executesql @SQLDelete

-- SET STATUS TO C ON PATTON
	update customer
	set status = ''O'', StatusDescription = ''Opted Out''
	from dbo.Customer cs join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.tipnumber
	where cs.tipnumber is not null and oot.optoutposted is null

	update AFFILIAT
	set AcctStatus = ''O''
	from dbo.AFFILIAT af join rewardsnow.dbo.optouttracking oot on af.tipnumber = oot.tipnumber
	where af.tipnumber is not null and oot.optoutposted is null

END

' 
END
GO
