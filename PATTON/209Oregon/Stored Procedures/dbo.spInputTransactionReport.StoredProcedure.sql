USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spInputTransactionReport]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**************************************
Sums totals for Input Transaction Report
**************************************  
*/
CREATE       PROCEDURE [dbo].[spInputTransactionReport] AS   

INSERT INTO input_transactionreport (trantype, tranamt, trancnt, points)
SELECT tt.Description, isnull(sum(tranamt),0), isnull(sum(trancnt),0), isnull(sum(ic.points),0)
FROM input_cardtrans ic
join RewardsNow.dbo.TranType tt
on ic.trancode = tt.TranCode
GROUP BY tt.Description 
ORDER BY tt.Description

INSERT INTO input_transactionreport (trantype, tranamt, trancnt, points)
SELECT 'Loan Balance', isnull(SUM(avgbal),0), isnull(COUNT(loanid),0), isnull(sum(points),0)
FROM input_loanbal

INSERT INTO input_transactionreport (trantype, tranamt, trancnt, points)
SELECT 'Share Balance', isnull(SUM(avgbalance),0), isnull(COUNT(tipnumber),0), isnull(sum(points),0)
FROM input_sharebal
GO
