USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 11/09/2015 15:23:30 ******/
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
    Load the TransStandard Table from the Transaction_input table
****************************************************************************
*/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded CHAR(10) AS

-- Clear TransStandard 
TRUNCATE TABLE TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
INSERT TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
SELECT  [TipNumber], @DateAdded , [cardnbr], [TranCode], Sum( Convert(int, [TranCnt] ) ) , CONVERT(CHAR(15), Sum(Convert ( int, [Points])) ) 
FROM input_cardtrans
GROUP BY [TipNumber], [cardnbr], [TranCode]

INSERT TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
SELECT  [TipNumber], @DateAdded , [acctnbr], [TranCode], 1 , CONVERT(CHAR(15), Sum(Convert ( int, [Points])) ) 
FROM input_loanbal
GROUP BY [TipNumber], [acctnbr], [TranCode]

INSERT TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
SELECT  [TipNumber], @DateAdded , [acctnbr], [TranCode], 1 , CONVERT(CHAR(15), Sum(Convert ( int, [Points])) ) 
FROM input_sharebal
GROUP BY [TipNumber], [acctnbr], [TranCode]

-- Set the TranType to the Description found in the RewardsNow.TranCode table
UPDATE TransStandard 
SET TranType = R.[Description] 
FROM RewardsNow.dbo.Trantype R 
INNER JOIN TransStandard T 
ON R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
UPDATE TransStandard 
SET Ratio = R.Ratio  
FROM RewardsNow.dbo.Trantype R 
INNER JOIN TransStandard T 
ON R.TranCode = T.Trancode
GO
