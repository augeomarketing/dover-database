USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 02/11/2011 11:27:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/* 

This retrieves all customers that were added in the previous month
It does not do an exact date match on the custom date added column

*/
CREATE   PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 
TRUNCATE TABLE Welcomekit 

INSERT INTO Welcomekit 
SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode, '', ''
FROM customer WHERE (YEAR(DATEADDED) = YEAR(@EndDate) AND MONTH(DATEADDED) = MONTH(@EndDate) AND UPPER(STATUS) <> 'C') 

UPDATE DateforAudit SET DateIn = @EndDate
GO
