USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[QuarterlyAuditValidation]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[QuarterlyAuditValidation] 
AS

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/

DECLARE @Tipnumber NCHAR(15), @pointsbegin NUMERIC(9), @pointsend NUMERIC(9), @pointspurchasedCR NUMERIC(9),@pointspurchasedDB NUMERIC(9), @pointsbonus NUMERIC(9), @pointsadded NUMERIC(9),
 @pointsincreased NUMERIC(9), @pointsredeemed NUMERIC(9), @pointsreturnedCR NUMERIC(9), @pointsreturnedDB NUMERIC(9), @pointssubtracted NUMERIC(9), @pointsdecreased NUMERIC(9), @errmsg VARCHAR(50), @currentend NUMERIC(9), @pointsShare NUMERIC(9), @pointsLoan NUMERIC(9)

TRUNCATE TABLE Quarterly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Quarterly Statement TABLE                     */
/*                                                                            */

DECLARE tip_crsr CURSOR
FOR SELECT Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased, pointsShare, pointsloan
FROM Quarterly_Statement_File
OPEN tip_crsr

FETCH tip_crsr INTO @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased, @pointsShare, @pointsLoan

IF @@FETCH_STATUS = 1
	GOTO Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @errmsg = NULL
	SET @currentend = '0'
	IF @pointsend <> (SELECT AdjustedEndingPoints FROM Current_Month_Activity WHERE tipnumber = @tipnumber)
		BEGIN
			SET @errmsg='Ending Balances do not match'
			SET @currentend=(SELECT AdjustedEndingPoints FROM Current_Month_Activity WHERE tipnumber = @tipnumber)		
			INSERT INTO Quarterly_Audit_ErrorFile
			VALUES(  @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased, @pointsLoan, @pointsShare, @errmsg, @currentend )
			GOTO Next_Record
		END
		
	GOTO Next_Record

	Next_Record:
	FETCH tip_crsr INTO @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased, @pointsShare, @pointsLoan
END
GOTO Fetch_Error

Fetch_Error:
CLOSE  tip_crsr
DEALLOCATE tip_crsr
GO
