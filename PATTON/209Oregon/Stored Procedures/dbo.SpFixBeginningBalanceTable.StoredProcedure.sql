USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[SpFixBeginningBalanceTable]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SpFixBeginningBalanceTable]
	@month INT
AS
BEGIN
	DECLARE @MonthBucket CHAR(10), @MonthBegin CHAR(2), @YearBegin CHAR(4), @SQLInsert NVARCHAR(1000), @SQLUpdate NVARCHAR(1000)

	SET @YearBegin = CONVERT(CHAR(4), YEAR(GetDate()))

	SET @MonthBegin = @month + 1
	IF CONVERT( INT , @MonthBegin) = '13' 
		BEGIN
			SET @monthbegin='1'
			SET @YearBegin = CONVERT(CHAR(4), YEAR(GetDate()) + 1)
		END	

	SET @MonthBucket='MonthBeg' + @monthbegin

	set @SQLUpdate=N'UPDATE Beginning_Balance_Table SET ' + Quotename(LTRIM(RTRIM(@MonthBucket))) + N' =(SELECT SUM(points * ratio) FROM history WHERE tipnumber = Beginning_Balance_Table.tipnumber AND histdate < ''' + LTRIM(RTRIM(@MonthBegin)) + '/01/' + LTRIM(RTRIM(@YearBegin)) + ''' ) WHERE EXISTS(SELECT 1 FROM history WHERE tipnumber = Beginning_Balance_Table.tipnumber)'
	SET @SQLInsert=N'INSERT INTO Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') SELECT tipnumber, COALESCE(SUM(points * ratio), 0) FROM history WHERE histdate < ''' + LTRIM(RTRIM(@MonthBegin)) + '/01/' + LTRIM(RTRIM(@YearBegin)) + ''' AND NOT EXISTS(SELECT 1 FROM Beginning_Balance_Table WHERE tipnumber = history.tipnumber AND histdate < ''' + LTRIM(RTRIM(@MonthBegin)) + '/01/' + LTRIM(RTRIM(@YearBegin)) + ''' ) GROUP BY history.tipnumber'
		

	EXEC sp_executesql @SQLUpdate
	EXEC sp_executesql @SQLInsert

END
GO
