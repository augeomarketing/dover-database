USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  
**************************************************************
Copies data from input_transaction to the Affiliat_stage table
  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
**************************************************************  
*/

CREATE      PROCEDURE [dbo].[spLoadAffiliatStage] @MonthEnd CHAR(20) AS 

/************ Insert New Accounts into Affiliat Stage  ***********/
INSERT INTO Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, YTDEarned, CustID, lastname)
SELECT Distinct t.[cardnbr], t.TipNumber, A.AcctTypeDesc, @MonthEnd, t.[relationshipcode], TT.[Description] , 
 0, rtrim(ltrim(t.[acctnbr])), rtrim(ltrim(t.lastname))
FROM input_cardtrans t 
INNER JOIN AcctType A
ON t.Trancode = A.AcctType
INNER JOIN TranType TT
ON TT.Trancode = T.Trancode 
-- 7/11/08 WHERE t.[cardnbr] NOT IN ( SELECT ACCTID FROM Affiliat_Stage) 
WHERE ltrim(rtrim(t.acctnbr))+ltrim(rtrim(t.CardNbr)) not in ( select ltrim(rtrim(custid)) + ltrim(rtrim(acctid))  from Affiliat_Stage )


INSERT INTO Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, YTDEarned, CustID, lastname)
SELECT Distinct RIGHT(RTRIM(t.[cardnbr]),6), t.TipNumber, 'CREDIT', @MonthEnd, '', 'Credit Purchase' , 0, LTRIM(RTRIM(t.[acctnbr])), ltrim(rtrim(t.lastname))
FROM input_joint t 
--  7/11/08 WHERE RIGHT(RTRIM(t.[cardnbr]),6) NOT IN ( SELECT ACCTID FROM Affiliat_Stage)
WHERE ltrim(rtrim(t.acctnbr))+Right( ltrim(rtrim(t.CardNbr)),6)  not in ( select ltrim(rtrim(custid)) + ltrim(rtrim(acctid))  from Affiliat_Stage )
GO
