USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 02/14/2011 16:17:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER  PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname varchar(100) output, @ConnectionString varchar(100) output, @filename varchar(50) output, @prodFile varchar(50) OUTPUT
AS

DECLARE @currentdate CHAR(4), @endingDate VARCHAR(10), @totalNumber INT

SET @endingDate=(SELECT TOP 1 CONVERT(date,RTRIM(LTRIM(datein))) FROM DateforAudit)
SET @currentdate = CONVERT(VARCHAR(2),rewardsnow.dbo.ufn_pad(MONTH(@endingDate), 'L',2,'0')) + CONVERT(VARCHAR(4),RIGHT(YEAR(@endingdate),2))

SELECT 	@totalNumber = count(*)
FROM customer 
WHERE (YEAR(DATEADDED) = YEAR(@endingDate) 
AND MONTH(DATEADDED) = MONTH(@endingDate) 
AND UPPER(STATUS) <> 'C') 


SET @filename = LTRIM(RTRIM('W' + @TipPrefix + @currentdate + '_' + CONVERT(VARCHAR(10),@totalNumber) + '.xls'))
SET @newname = LTRIM(RTRIM('\\doolittle\ops\' + @TipPreFix + '\Output\WelcomeKit\Welcome.bat ' + @filename))
SET @ConnectionString = LTRIM(RTRIM('\\doolittle\ops\' + @TipPrefix + '\Output\WelcomeKit\' + @filename))
SET @prodFile = LTRIM(RTRIM('\\ftp\ike\staff\Production\WelcomeKit\' + @filename))
