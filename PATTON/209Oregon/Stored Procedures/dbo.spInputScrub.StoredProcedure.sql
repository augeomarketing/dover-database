USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 02/11/2011 11:27:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
****************************************************************************
    This scrubs (cleans up) the input_ tables
**************************************************************************** 
*/	

CREATE   PROCEDURE [dbo].[spInputScrub] AS

TRUNCATE TABLE  input_cardtrans_error
TRUNCATE TABLE  input_loanbal_error
TRUNCATE TABLE  input_sharebal_error

/* Remove input_customer records with Member Name null */
INSERT INTO input_cardtrans_error 
SELECT *, 'Member Name not Specified' 
FROM input_cardtrans 
WHERE [acctnbr] IS NULL

DELETE 
FROM input_cardtrans 
WHERE [acctnbr] IS NULL

INSERT INTO Input_cardtrans_error
SELECT *, 'Customer Opt Out Request'
FROM Input_cardtrans a
WHERE [AcctNbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = '209')

DELETE FROM Input_cardtrans 
WHERE [AcctNbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = '209')

INSERT INTO input_loanbal_error
SELECT *, 'Customer Opt Out Request'
FROM input_loanbal a
WHERE [AcctNbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = '209')

DELETE FROM input_loanbal 
WHERE [AcctNbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = '209')

INSERT INTO input_sharebal_error
SELECT *, 'Customer Opt Out Request'
FROM input_sharebal a
WHERE [AcctNbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = '209')

DELETE FROM input_sharebal 
WHERE [AcctNbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = '209')

/* remove leading and trailing spaces in names and addresses */ 
UPDATE 
	input_cardtrans SET
	
	[acctnbr] = LTRIM(RTRIM([acctnbr])) , 
	[firstname] = LTRIM(RTRIM([firstname])) , 
	[address] = LTRIM(RTRIM([address])) , 
	[city] = LTRIM(RTRIM([city])) , 
	[state] = LTRIM(RTRIM([state])) , 
	[zip] = LTRIM(RTRIM([zip])) , 
	[phone] = LTRIM(RTRIM([phone])) , 
	[relationshipcode] = LTRIM(RTRIM([relationshipcode])),
	[accttype] = LTRIM(RTRIM([accttype])),
	[trantype] = LTRIM(RTRIM([trantype])),
	[tranamt] = LTRIM(RTRIM([tranamt])),
	[trancnt] = LTRIM(RTRIM([trancnt])),
	[cardnbr] = LTRIM(RTRIM([cardnbr])),
	[priorcardnbr] = LTRIM(RTRIM([priorcardnbr]))

UPDATE input_loanbal
SET acctnbr = rtrim(ltrim(acctnbr)),
	loanid = rtrim(ltrim(loanid)),
	avgbal = rtrim(ltrim(avgbal)),
	loantype = rtrim(ltrim(loantype))

UPDATE input_sharebal
SET acctnbr = rtrim(ltrim(acctnbr)),
	lastname = rtrim(ltrim(lastname)),
	relationshipcode = rtrim(ltrim(relationshipcode)),
	avgbalance = rtrim(ltrim(avgbalance)),
	sharetype = rtrim(ltrim(sharetype))

UPDATE input_joint
SET acctnbr = rtrim(ltrim(acctnbr)),
	cardnbr = rtrim(ltrim(cardnbr)),
	firstname = rtrim(ltrim(firstname)),
	lastname = rtrim(ltrim(lastname))

INSERT INTO input_loanbal_error
SELECT *, 'Customer Not In Member File' 
FROM input_loanbal 
WHERE [acctnbr] NOT IN (SELECT [acctnbr] FROM input_cardtrans union SELECT [custid] FROM affiliat_stage) 

DELETE 
FROM input_loanbal 
WHERE [acctnbr] NOT IN (SELECT [acctnbr] FROM input_cardtrans union SELECT [custid] FROM affiliat_stage) 

INSERT INTO input_sharebal_error
SELECT *, 'Customer Not In Member File' 
FROM input_sharebal 
WHERE [acctnbr] NOT IN (SELECT [acctnbr] FROM input_cardtrans union SELECT [custid] FROM affiliat_stage) 

DELETE 
FROM input_sharebal 
WHERE [acctnbr] NOT IN (SELECT [acctnbr] FROM input_cardtrans union SELECT [custid] FROM affiliat_stage) 

/* Replace null with 0 on purhase and trancounts in transaction */
UPDATE input_cardtrans 
SET [trancnt]  = 0 
WHERE [trancnt] IS NULL

/* Round the amounts */
UPDATE input_cardtrans 
SET [tranamt] = ROUND([tranamt],0)

-- Convert Trancodes to Product Codes

UPDATE input_cardtrans 
	SET TranCode = aux_trancode.aux_trancode_rewardsnow
	FROM
		aux_trancode INNER JOIN input_cardtrans
		ON aux_trancode.aux_trancode_oregon = input_cardtrans.trantype
		  AND aux_trancode.aux_trancode_importtype = input_cardtrans.importtype

UPDATE input_loanbal
	SET TranCode = 'FP'

UPDATE input_sharebal
	SET TranCode = 'FS'
GO
