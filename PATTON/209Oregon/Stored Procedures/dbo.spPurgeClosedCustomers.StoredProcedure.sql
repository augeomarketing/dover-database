USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 07/18/2013 16:21:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  A. Barriere  */
/* DATE: 7/2008   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 

-- read input_purge
-- staging tables don''t need to have records moved to delete tables
-- Production tables need to have pending purge processing.

-- S Blanchette 7/2013 added code to update the OptOutPosted field
/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted char(10) AS

DECLARE @TipFirst CHAR(3)
SELECT @TipFirst = tipfirst FROM Client

INSERT INTO input_purge (purgedate, custid)
SELECT @DateDeleted, acctid FROM rewardsnow.dbo.optouttracking
WHERE tipPrefix = @tipFirst

-- find tipnumbers for purge accounts
UPDATE ip
SET ip.tipnumber = a.tipnumber
FROM affiliat a INNER JOIN input_purge ip ON a.custid = ip.custid

TRUNCATE TABLE input_purge_error

--exception report
INSERT INTO input_purge_error (acctnbr, reason)
SELECT custid, ''No Record Found'' FROM input_purge WHERE tipnumber is null

DELETE FROM input_purge WHERE tipnumber is null

-- load pending purges into purge file
INSERT INTO Input_Purge (tipnumber, purgedate)
SELECT tipnumber, purgedate FROM Purge_Pending 

TRUNCATE TABLE Purge_Pending

-- any tip that has history after purge date is pending
INSERT INTO purge_pending (tipnumber, purgedate)
SELECT tipnumber, @DateDeleted FROM input_purge
WHERE tipnumber in (SELECT tipnumber FROM history WHERE histdate > @DateDeleted AND TranCode <> ''RQ'' )

DELETE FROM input_purge
WHERE tipnumber IN (SELECT tipnumber FROM history WHERE histdate > @DateDeleted AND TranCode <> ''RQ'' )


----------- Stage Table Processing ----------
IF @Production_Flag = ''S''
----------- Stage Table Processing ----------
BEGIN
	DELETE FROM Customer_stage WHERE TipNumber IN (SELECT TipNumber FROM Input_Purge)
	DELETE FROM Affiliat_Stage WHERE TipNumber NOT IN (SELECT TipNumber FROM Customer_Stage)
	DELETE FROM History_stage 	WHERE TipNumber NOT IN (SELECT TipNumber FROM Customer_Stage)
END

----------- Production Table Processing ----------
IF @Production_Flag = ''P''
----------- Production Table Processing ----------
BEGIN
	-------------- purge remainging input_purge records. 
	-- Insert customer to customerdeleted 
	INSERT INTO CustomerDeleted (TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
	SELECT TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	FROM Customer WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- Insert affiliat to affiliatdeleted 
	INSERT INTO AffiliatDeleted (TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
	SELECT TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, @DateDeleted
	FROM Affiliat WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- copy history to historyDeleted 
	INSERT INTO HistoryDeleted (tipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	SELECT TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, @DateDeleted
	FROM History WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- Delete from customer 
	DELETE FROM Customer
	WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- Delete records from affiliat 
	DELETE FROM Affiliat   
	WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- Delete records from History 
	DELETE FROM History 
	WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- flag all Undeleted Customers "C" that have an input_purge_pending record 
	UPDATE customer SET status = ''C'' 
	WHERE tipnumber IN (SELECT Tipnumber FROM Purge_Pending)

	-- Set date in optouttracking
	update rewardsnow.dbo.optouttracking
	set OPTOUTPOSTED = ip.PurgeDate
	from rewardsnow.dbo.optouttracking oot join dbo.input_purge ip on oot.TIPNUMBER = ip.tipnumber
	
	TRUNCATE TABLE Input_Purge
END
' 
END
GO
