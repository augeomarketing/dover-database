USE [209Oregon]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 02/11/2011 11:27:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
********************************************************
 This updates the TIPNUMBER in the input_CustTran table   
********************************************************
*/

CREATE           PROCEDURE [dbo].[spLoadTipNumbers] AS

DECLARE @NewTip bigint
DECLARE @TipPrefix char(3)

UPDATE aux_uniqueacct
SET TIPNUMBER = AFFILIAT_Stage.TIPNUMBER
FROM AFFILIAT_Stage
	INNER JOIN aux_uniqueacct
	ON AFFILIAT_Stage.[custid] = aux_uniqueacct.[acctid]
WHERE aux_uniqueacct.TIPNUMBER IS NULL

SELECT @TipPrefix = TipFirst FROM Client
EXEC rewardsnow.dbo.[spGetLastTipNumberUsed] @TipPrefix, @NewTip OUTPUT

If @NewTip is NULL Set @NewTip = @TipPrefix + '000000000000'

/* Assign Get New Tips to new customers */
UPDATE aux_uniqueacct
SET @NewTip = ( @NewTip + 1 ),
	TIPNUMBER =  @NewTip 
	WHERE TipNumber IS NULL

-- Update LastTip -- 
--UPDATE Client SET LastTipNumberUsed = @NewTip  
EXEC rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip

/* Load Tipnumbers to Input_Transaction AND Input_customer Table */

UPDATE Input_cardtrans
SET Input_cardtrans.TIPNUMBER = aux_uniqueacct.TIPNUMBER
FROM Input_cardtrans
	INNER JOIN aux_uniqueacct
	ON Input_cardtrans.[AcctNbr] = aux_uniqueacct.[acctid]
WHERE Input_cardtrans.TIPNUMBER IS NULL

UPDATE Input_loanbal 
SET Input_loanbal.TIPNUMBER = aux_uniqueacct.TIPNUMBER
FROM Input_loanbal
	INNER JOIN aux_uniqueacct
	ON Input_loanbal.[acctnbr] = aux_uniqueacct.[acctid]
WHERE Input_loanbal.TIPNUMBER IS NULL

UPDATE Input_sharebal 
SET Input_sharebal.TIPNUMBER = aux_uniqueacct.TIPNUMBER
FROM Input_sharebal
	INNER JOIN aux_uniqueacct
	ON Input_sharebal.[acctnbr] = aux_uniqueacct.[acctid]
WHERE Input_sharebal.TIPNUMBER IS NULL

UPDATE Input_joint
SET Input_joint.TIPNUMBER = aux_uniqueacct.TIPNUMBER
FROM Input_joint
	INNER JOIN aux_uniqueacct
	ON Input_joint.[acctnbr] = aux_uniqueacct.[acctid]
WHERE Input_joint.TIPNUMBER IS NULL
GO
