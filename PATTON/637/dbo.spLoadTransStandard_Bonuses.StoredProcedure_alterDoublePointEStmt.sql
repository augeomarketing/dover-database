use [637Wyandotte]

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[spLoadTransStandard_Bonuses] @EndDate char(10)
AS
Declare @E_Stmt bit
--do a select from a BonusTypes table to conditionally add 
---------------------------------------------------------------------------------------
/* E-Statement Bonuses */
--for May through July processing point value for E-statement bonus doubled to 600 from 300-RWL
Insert into transstandard_Bonuses (tfno,Trandate, Acct_num, Trancode, Trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @EndDate,NULL,'BE',1,600, 'Bonus Email Statements', 1,null from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode='BE')

Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tipnumber, 'BE',null,@EndDate from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode='BE')

-----------------------------------------------------------------------------------------------------------------------
/* ADD MORE BONUSES HERE */
------------------------------------------------------------------------------------------------------------------------
/* Copy records to TransStandard*/
insert into transstandard 
	Select * from TransStandard_Bonuses
GO
