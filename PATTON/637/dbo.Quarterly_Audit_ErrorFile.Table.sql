SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[TOT_Begin] [numeric](18, 0) NULL,
	[TOT_End] [numeric](18, 0) NULL,
	[TOT_Add] [numeric](18, 0) NULL,
	[TOT_Subtract] [numeric](18, 0) NULL,
	[TOT_Net] [numeric](18, 0) NULL,
	[TOT_Bonus] [numeric](18, 0) NULL,
	[TOT_Redeem] [numeric](18, 0) NULL,
	[TOT_ToExpire] [numeric](18, 0) NULL,
	[CRD_Purch] [numeric](18, 0) NULL,
	[CRD_Return] [numeric](18, 0) NULL,
	[DBT_Purch] [numeric](18, 0) NULL,
	[DBT_Return] [numeric](18, 0) NULL,
	[ADJ_Add] [numeric](18, 0) NULL,
	[ADJ_Subtract] [numeric](18, 0) NULL,
	[ErrorMsg] [nvarchar](50) NULL,
	[CurrentEnd] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]  DEFAULT (0) FOR [TOT_Begin]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]  DEFAULT (0) FOR [TOT_End]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]  DEFAULT (0) FOR [TOT_Add]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]  DEFAULT (0) FOR [TOT_Subtract]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]  DEFAULT (0) FOR [TOT_Net]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]  DEFAULT (0) FOR [TOT_Bonus]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]  DEFAULT (0) FOR [TOT_Redeem]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]  DEFAULT (0) FOR [TOT_ToExpire]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]  DEFAULT (0) FOR [CRD_Purch]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]  DEFAULT (0) FOR [CRD_Return]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]  DEFAULT (0) FOR [DBT_Purch]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]  DEFAULT (0) FOR [DBT_Return]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]  DEFAULT (0) FOR [ADJ_Add]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]  DEFAULT (0) FOR [ADJ_Subtract]
GO
