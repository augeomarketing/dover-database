USE [637Wyandotte]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 07/30/2014 09:48:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransToStage]
GO

USE [637Wyandotte]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 07/30/2014 09:48:27 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst char(3), @enddate char(10)

AS 

DECLARE	@MaxPointsPerYear decimal(9) = ( Select MaxPointsPerYear from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )
	,	@YTDEarned numeric(9)
	,	@AmtToPost numeric (9)
	,	@Overage numeric(9)
	,	@dbName varchar(50) = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )
	,	@SQLStmt nvarchar(MAX) 
/***************************** HISTORY_STAGE *****************************/
----  Insert TransStandard into History_stage 
INSERT INTO	history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
SELECT	TFNO, Acct_num, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 From TransStandard
/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
IF	@MaxPointsPerYear > 0 
	BEGIN 
		UPDATE	History_Stage
		SET		Overage = (H.Points * H.Ratio) - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM	History_Stage H JOIN Affiliat_Stage A ON H.Tipnumber = A.Tipnumber AND A.AcctID = H.AcctID
		WHERE	A.YTDEarned + H.Points > @MaxPointsPerYear 
	END
	
---------SUMMER 2014 SPEND BONUS AWARD-------------------------------------------------
INSERT INTO HISTORY_Stage
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT	TIPNUMBER, ACCTID, HISTDATE, 'G2' as trancode, '1' as trancount, POINTS, 
		'Double point promotion award' as description, 'NEW' as secid, Ratio, Overage
FROM	HISTORY_Stage
WHERE	MONTH(histdate) = MONTH(CAST(@enddate as DATE))
	AND	MONTH(histdate) in (7,8,9)
	AND	YEAR(histdate) = 2014
	AND TRANCODE like '6%'
	AND POINTS > 0
	AND	LEFT(Acctid, 6) in ('441897','441898','454863')
---------------------------------------------------------------------------------------	
	
/***************************** AFFILIAT_STAGE -ROLLUP BY TIP*****************************/
/*
-- Update Affiliat YTDEarned 
Update Affiliat_Stage
	set YTDEarned  = A.YTDEarned  + H.Points 
	FROM HISTORY_STAGE H JOIN AFFILIAT_Stage A on H.Tipnumber = A.Tipnumber
-- Update History_Stage Points = Points - Overage
Update History_Stage 
	Set Points = Points - Overage where Points > Overage
*/
/***************************** AFFILIAT_STAGE -ROLLUP BY CARD (AcctID)*****************************/
-- Update Affiliat_stage YTDEarned 
SELECT	A.ACCTID, A.Tipnumber, SUM(H.POINTS*H.Ratio) AS AcctIDPoints
INTO	#AP
FROM	AFFILIAT_stage A 
	INNER JOIN
		HISTORY_stage H 
	ON	A.ACCTID = H.ACCTID AND A.TIPNUMBER = H.TIPNUMBER
WHERE	H.SecID = 'NEW'
GROUP BY A.ACCTID, A.Tipnumber
---Join on the temp table and update 
UPDATE	Affiliat_stage
SET		YTDEarned = YTDEarned +  AP.AcctIDPoints
FROM	AFFILIAT_stage A 
	INNER JOIN
		#AP AP 
	ON	A.ACCTID = AP.ACCTID AND A.TIPNUMBER = AP.TIPNUMBER
DROP TABLE #AP
---?????? why 
UPDATE	History_Stage 
SET		Points = Points - Overage 
WHERE	Points > Overage
/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
UPDATE Customer_Stage SET RunAvaliableNew  = 0 WHERE RunAvaliableNew  is null
UPDATE Customer_Stage SET RunAvailable  = 0 WHERE RunAvailable is null

IF exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_histpoints]') and OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[vw_histpoints]
/* Create View */
SET		@SQLStmt =	'
			CREATE VIEW vw_histpoints as select tipnumber, sum(points*ratio) as points from history_stage where secid = ''NEW''group by tipnumber
					'
EXEC	sp_executesql @SQLStmt

UPDATE	customer_stage 
SET		RunAvailable  = RunAvailable + v.Points 
FROM	Customer_Stage C JOIN vw_histpoints V ON C.Tipnumber = V.Tipnumber

UPDATE	customer_stage 
SET		RunAvaliableNew  = v.Points 
FROM	Customer_Stage C JOIN vw_histpoints V ON C.Tipnumber = V.Tipnumber

DROP VIEW	[dbo].[vw_histpoints]

GO


