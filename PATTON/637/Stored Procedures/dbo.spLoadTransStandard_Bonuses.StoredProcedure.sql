USE [637Wyandotte]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard_Bonuses]    Script Date: 09/16/2010 14:30:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard_Bonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadTransStandard_Bonuses] @EndDate char(10)
AS
Declare @E_Stmt bit
--do a select from a BonusTypes table to conditionally add 
---------------------------------------------------------------------------------------
/* E-Statement Bonuses */
-- Temporyary increase in points from May THRU July 2010 form 300 to 800 for e-statements
-- Points brought back down to 300 9/16/2010
-- Points set to 1000  1/11/2012 SEB001
Insert into transstandard_Bonuses (tfno,Trandate, Acct_num, Trancode, Trannum, tranamt, trantype, ratio, crdactvldt)
select tipnumber, @EndDate,NULL,''BE'',1,1000, ''Bonus Email Statements'', 1,null from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BE'')

Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tipnumber, ''BE'',null,@EndDate from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BE'')

-----------------------------------------------------------------------------------------------------------------------
/* ADD MORE BONUSES HERE */
------------------------------------------------------------------------------------------------------------------------
/* Copy records to TransStandard*/
insert into transstandard 
	Select * from TransStandard_Bonuses' 
END
GO
