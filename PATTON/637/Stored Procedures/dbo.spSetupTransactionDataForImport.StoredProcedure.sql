USE [637Wyandotte]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 05/06/2014 10:18:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO

USE [637Wyandotte]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 05/06/2014 10:18:15 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3), @DebitCreditFlag nvarchar(1)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */

/********************************/
/* S Blanchette                 */
/* 11/2010                      */
/* SEB003                       */
/* chage ratio from 2 to 1 on   */
/* awarding points they want    */
/* double points for Nov Dec    */
/* must be removed after DEC    */
/* processing                   */
/********************************/
             
/********************************/
/* S Blanchette                 */
/* 1/2011                      */
/* SEB004                       */
/* chage ratio back to normal   */
/********************************/


/*									       */
/* BY:  S.Blanchette  */
/* DATE: 05/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Change  Set tip to null and add date criteria  */

-- S Blanchete change to use RNITransaction view
  
truncate table transwork
truncate table transstandard

select *
into #tempTrans
from Rewardsnow.dbo.vw_637_TRAN_SOURCE_1 with (nolock)
where Trandate<=cast(@EndDate as DATE) /* SEB005 */

insert into transwork ( TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID )
--select * from COOPWork.dbo.TransDetailHold
		select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID 
from #tempTrans
where sic<>'6011' and processingcode in ('000000', '001000', '002000', '200020', '200040', '500000', '500020', '500010', '003000', '200030', '500030') and (left(msgtype,2) in ('02', '04')) 

drop table #temptrans

update TRANSWORK
set TIPNUMBER = afs.TIPNUMBER
from TRANSWORK tw join AFFILIAT_Stage afs on tw.PAN = afs.ACCTID

delete from TRANSWORK
where TIPNUMBER is null

---CALC SIG DEBIT POINT VALUES
-- Signature Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0) --SEB003 SEB004
--set points=ROUND(((amounttran/100)/1), 10, 0) --SEB003  SEB004********must remove after december processing
where netid in('MCI', 'VNT') AND LEFT(PAN,6) IN ('529705','536063','544330')	
---CALC PIN DEBIT POINT VALUES
-- PIN Debit NO AWARD FOR PIN

--CALCULATE CREDIT POINT EARNINGS (1/$1)
	UPDATE	transwork
	SET		POINTS = ROUND(((amounttran/100)), 10, 0)
	WHERE	LEFT(PAN,6) IN ('441897','441898','454863')



/*
update transwork
set points=ROUND(((amounttran/100)/4), 10, 0)
where netid not in('MCI', 'VNT') 
--Put to standard transtaction file format purchases.
*/
if @DebitCreditFlag='D'    ---if this is a COOP DEBIT ONLY FI
Begin
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' 
	from transwork
	where processingcode in ('000000', '001000', '002000', '500000', '500020', '500010', '003000', '500030') and left(msgtype,2) in ('02') AND LEFT(PAN, 6) IN ('529705','536063','544330')	        	
	group by tipnumber, Pan
	
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' 
	from transwork
	where (processingcode in ('200020', '200040', '200030') or left(msgtype,2) in ('04')) AND LEFT(PAN, 6) IN ('529705','536063','544330')
	group by tipnumber, Pan

--CREATE CREDIT TRANSACTION RECORDS
	INSERT INTO TransStandard
		(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	SELECT	tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' 
	FROM	transwork
	WHERE	processingcode in ('000000', '001000', '002000', '500000', '500020', '500010', '003000', '500030') and left(msgtype,2) in ('02') AND LEFT(PAN, 6) IN ('441897','441898','454863')	        	
	GROUP BY	tipnumber, Pan
		
	-- SEB001
	INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' 
		from transwork
		where (processingcode in ('200020', '200040', '200030') or left(msgtype,2) in ('04')) AND LEFT(PAN, 6) IN ('441897','441898','454863')
		group by tipnumber, Pan


END


GO


