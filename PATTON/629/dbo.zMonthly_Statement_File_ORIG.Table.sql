SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zMonthly_Statement_File_ORIG](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__07C12930]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__08B54D69]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__09A971A2]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__0A9D95DB]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__0B91BA14]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__0C85DE4D]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__0D7A0286]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__0E6E26BF]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__0F624AF8]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__10566F31]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__114A936A]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__123EB7A3]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[zMonthly_Statement_File_ORIG] ADD  CONSTRAINT [DF__Monthly_S__Point__1332DBDC]  DEFAULT (0) FOR [PointsDecreased]
GO
