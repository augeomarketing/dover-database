SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[TOT_Begin] [numeric](18, 0) NULL,
	[TOT_End] [numeric](18, 0) NULL,
	[TOT_Add] [numeric](18, 0) NULL,
	[TOT_Subtract] [numeric](18, 0) NULL,
	[TOT_Net] [numeric](18, 0) NULL,
	[TOT_Bonus] [numeric](18, 0) NULL,
	[TOT_Redeem] [numeric](18, 0) NULL,
	[TOT_ToExpire] [numeric](18, 0) NULL,
	[CRD_Purch] [numeric](18, 0) NULL,
	[CRD_Return] [numeric](18, 0) NULL,
	[DBT_Purch] [numeric](18, 0) NULL,
	[DBT_Return] [numeric](18, 0) NULL,
	[ADJ_Add] [numeric](18, 0) NULL,
	[ADJ_Subtract] [numeric](18, 0) NULL,
	[ErrorMsg] [nvarchar](50) NULL,
	[CurrentEnd] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
