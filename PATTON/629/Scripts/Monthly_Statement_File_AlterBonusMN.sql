use [629SouthWestern]
go

/*
   Monday, October 08, 201211:43:47 AM
   User: 
   Server: doolittle\rn
   Database: 629SouthWestern
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Monthly_Statement_File ADD
	PointsBonusMN decimal(18, 0) NULL
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsBonusMN DEFAULT (0) FOR PointsBonusMN
GO
ALTER TABLE dbo.Monthly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
