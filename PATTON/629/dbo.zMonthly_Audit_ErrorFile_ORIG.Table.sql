SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__74AE54BC]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__75A278F5]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__76969D2E]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__778AC167]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__787EE5A0]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonusDB]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__797309D9]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__7A672E12]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__7B5B524B]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__7C4F7684]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF__Monthly_A__Point__7D439ABD]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[zMonthly_Audit_ErrorFile_ORIG] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_Currentend]  DEFAULT (0) FOR [Currentend]
GO
