SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 8/28/2007  Added expired Points 
-- RDT 2/4/2008 Added FullSpectrum 

*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[spQuarterlyStatement]  @StartDateParm char(10), @EndDateParm char(10)

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)
Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
Truncate Table Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber,  acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer


/* Load the statmement file with CREDIT purchases          */
update Quarterly_Statement_File
set CRD_Purch =(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='63')



/* Load the statmement file  with CREDIT returns            */
update Quarterly_Statement_File
set CRD_Return=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber
			 and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with DEBIT purchases          */
update Quarterly_Statement_File
set DBT_Purch=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='67')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='67')

/* Load the statmement file with DEBIT  returns            */
update Quarterly_Statement_File
set DBT_Return=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='37')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			 and histdate>=@startdate and histdate<=@enddate and trancode='37')





-- Load the statmement file with plus adjustments    
update Quarterly_Statement_File
set ADJ_Add=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode='IE')
--Load the statmement file with minus adjustments    
update Quarterly_Statement_File
set ADJ_Subtract=(select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DE')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DE')


/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set TOT_Bonus=(select sum(points*ratio) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and ( trancode like 'B%' OR trancode like 'F%' )  )
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and ( trancode like 'B%' OR trancode like 'F%' )  )


--Load the statmement file with total point increases 
update Quarterly_Statement_File
set TOT_Add= CRD_Purch + DBT_Purch +  ADJ_Add  + TOT_Bonus 

-- Load the statmement file with redemptions          
update Quarterly_Statement_File
set TOT_Redeem=(select sum(points*ratio*-1) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')



-- Add expired Points 
update Quarterly_Statement_File
set TOT_ToExpire = (select sum(points) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='XP')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='XP')


-- Load the statmement file with total point decreases 
update Quarterly_Statement_File
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
set TOT_Subtract=TOT_Redeem + CRD_Return + DBT_Return + ADJ_Subtract + TOT_ToExpire


-- Load the statmement file with the Beginning balance for the Month 
set @SQLUpdate=N'update Quarterly_Statement_File
set TOT_Begin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


--Load the statmement file with beginning points 
update Quarterly_Statement_File
set TOT_End=TOT_Begin + TOT_Add - TOT_Subtract



/* Load the acctid to the monthly statement table */
-- RDT Use CIFNum -- Update Quarterly_Statement_File  set acctid = a.acctid from affiliat_Stage a where Quarterly_Statement_File.tipnumber = a.tipnumber
GO
