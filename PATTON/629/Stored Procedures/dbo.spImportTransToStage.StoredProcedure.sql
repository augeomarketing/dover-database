USE [629SouthWestern]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 10/09/2014 08:13:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransToStage]
GO

USE [629SouthWestern]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 10/09/2014 08:13:57 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*   */
/*  **************************************  */
/* Date: 2/2011                           */
/* S Blanchette                           */
/* SEB001                                 */
/* Modify to get MaxpointsperYear from dbprocessinfo */
/*  **************************************  */
/*  **************************************  */
/* Date: 1/2012                           */
/* S Blanchette                           */
/* SEB002                                 */
/* Modify to only use current trans in   */
/* calc for YTDEARNED                     */
/*  **************************************  */

/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst CHAR(3), @enddate CHAR(10)

AS 

DECLARE	@MaxPointsPerYear	DECIMAL(9) = (select MaxPointsPerYear from RewardsNow.dbo.dbprocessinfo where dbnumber = @TipFirst) 
	,	@dbName				VARCHAR(50 )= (Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst) 
	,	@SQLStmt			NVARCHAR(max) 

/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
INSERT INTO	history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
SELECT	TFNO, Acct_num, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 
FROM	TransStandard
/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
IF	@MaxPointsPerYear > 0 
	BEGIN 
		UPDATE	History_Stage
		SET		Overage = H.Points - (@MaxPointsPerYear - A.ytdearned) 
		FROM	History_Stage H INNER JOIN Affiliat_Stage A ON H.Tipnumber = A.Tipnumber
		WHERE	A.YTDEarned + H.Points > @MaxPointsPerYear 
			AND	H.SECID = 'NEW'
	END

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
/***************************/
/* Start SEB002            */
/***************************/

/* Replaced SEB 1/13/2012 Update Affiliat_Stage
	set YTDEarned  = A.YTDEarned  + H.Points 
	FROM HISTORY_STAGE H JOIN AFFILIAT_Stage A on H.Tipnumber = A.Tipnumber */
	
-- Added below SEB 1/13/2012	
SELECT	A.ACCTID, A.Tipnumber, SUM(H.POINTS*H.Ratio) AS AcctIDPoints
INTO	#AP
FROM	AFFILIAT_stage A INNER JOIN HISTORY_stage H ON A.ACCTID = H.ACCTID AND A.TIPNUMBER = H.TIPNUMBER
WHERE	H.SecID = 'NEW'
GROUP BY	A.ACCTID, A.Tipnumber
		
---Join on the temp table and update 
UPDATE	Affiliat_stage
SET		YTDEarned = YTDEarned + AP.AcctIDPoints
FROM	AFFILIAT_stage A INNER JOIN #AP AP ON A.ACCTID = AP.ACCTID AND A.TIPNUMBER = AP.TIPNUMBER

DROP TABLE	#AP
/***************************/
/* End SEB002              */
/***************************/

-- Update History_Stage Points = Points - Overage
UPDATE History_Stage	SET Points = Points - Overage	WHERE Points >= Overage
	
/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
UPDATE Customer_Stage	SET RunAvaliableNew = 0	WHERE RunAvaliableNew is null
UPDATE Customer_Stage	SET RunAvailable  = 0	WHERE RunAvailable is null

IF exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_histpoints]') and OBJECTPROPERTY(id, N'IsView') = 1)
	BEGIN	DROP VIEW [dbo].[vw_histpoints]	END
/* Create View */
SET	@SQLStmt = 'Create view vw_histpoints as select tipnumber, sum(points*ratio) as points from history_stage where secid = ''NEW''group by tipnumber'
EXEC	sp_executesql @SQLStmt

UPDATE	customer_stage 
SET		RunAvailable  = RunAvailable + v.Points 
FROM	Customer_Stage C INNER JOIN vw_histpoints V ON C.Tipnumber = V.Tipnumber

UPDATE	customer_stage 
SET		RunAvaliableNew  = v.Points 
FROM	Customer_Stage C INNER JOIN vw_histpoints V ON C.Tipnumber = V.Tipnumber

DROP VIEW [dbo].[vw_histpoints]

GO
