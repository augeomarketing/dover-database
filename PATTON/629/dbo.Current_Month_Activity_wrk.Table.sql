SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Current_Month_Activity_wrk](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [decimal](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Current_Month_Activity_wrk] ADD  CONSTRAINT [DF_Current_Month_Activity_wrk_points]  DEFAULT (0) FOR [points]
GO
