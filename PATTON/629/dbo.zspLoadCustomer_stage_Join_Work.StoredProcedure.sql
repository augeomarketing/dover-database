SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspLoadCustomer_stage_Join_Work] @TipFirst char(3), @EndDate DateTime 
AS 
--this needs work as there are multiple rows per tip. See about doing it with a function maybe
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(CustIn.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(CustIn.NAMEACCT1),40 )
,ACCTNAME2 	= left(rtrim(CustIn.NAMEACCT2),40 )
,ACCTNAME3 	= left(rtrim(CustIn.NAMEACCT3),40 )
,ACCTNAME4 	= left(rtrim(CustIn.NAMEACCT4),40 )
,ACCTNAME5 	= left(rtrim(CustIn.NAMEACCT5),40 )
,ACCTNAME6 	= left(rtrim(CustIn.NAMEACCT6),40 )
,ADDRESS1 	= CustIn.ADDRESS1
,ADDRESS2  	= CustIn.ADDRESS2
,ADDRESS4      = left(ltrim(rtrim( CustIn.CITY))+' ' +ltrim(rtrim( CustIn.STATE))+' ' +ltrim( rtrim( CustIn.ZIP)) , 40 )
,CITY 		= CustIn.CITY
,STATE		= left(CustIn.STATE,2)
,ZIPCODE 	= ltrim(CustIn.ZIP)
,HOMEPHONE 	= left(CustIn.HOMEPHONE,10)
,WORKPHONE 	= left(CustIn.WORKPHONE,10)
,BusinessFlag	= Custin.BUSINESFLG
,STATUS	= CustIn.STATUS
,MISC2		= CustIn.Misc2
,MISC3		= CustIn.Misc3
From CustIn
Where CustIn.TIPNUMBER = Customer_Stage.TIPNUMBER 
/*Add New Customers                                                      */
	Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ACCTNAME2,  ACCTNAME3,  ACCTNAME4,  ACCTNAME5,  ACCTNAME6,   ADDRESS1, -- ADDRESS2, 
	ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE,  DATEADDED, STATUS, MISC2, Misc3,
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, 
	left(TIPNUMBER,3), 
	right(rtrim(TIPNUMBER),6), 
	left(rtrim(LASTNAME),40),
	left(rtrim(CustIn.NameAcct1),40) , 
	left(rtrim(CustIn.NameAcct2),40) , 
	left(rtrim(CustIn.NameAcct3),40) , 
	left(rtrim(CustIn.NameAcct4),40) , 
	left(rtrim(CustIn.NameAcct5),40) , 
	left(rtrim(CustIn.NameAcct6),40) , 
	Left(rtrim(ADDRESS1),40), 
--	Left(rtrim(ADDRESS2),40), 
	left( ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIP)  ),40),
	CITY, left(STATE,2), rtrim(ZIP),
	left(HOMEPHONE,10),  @EndDate, STATUS,  Misc2, Misc3
	,0, 0, 0, 0
from  CustIn 
	where CustIn.tipnumber not in (select TIPNUMBER from Customer_Stage)
/* set Default status to A */
Update Customer_Stage
Set STATUS = 'A' 
Where STATUS IS NULL 
/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null
GO
