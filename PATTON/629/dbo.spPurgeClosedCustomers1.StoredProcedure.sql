SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 

-- !!!!! calls procedure spFlagCustomerClosedAccount !!!!!!
-- !!!!! calls procedure spFlagCustomerClosedAccount !!!!!!

-- RDT 10/06/2008 Add code to hold purge for 2 months. 
-- Input_Purge_hold 

-- This will delete all customers with a status of 'C'losed OR 'H'ot card.


/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers1]  @Production_Flag char(1), @DateDeleted char(10) AS

Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)

-- RDT 10/06/2008
Declare @MonthsToHold int
declare @DateDeletedDatetime char(10) 
declare @DatetoPurge datetime 

---Not for why not using Pending Purge) 
---PUT NOTE about spLoadCustomerAndAffiliat upping CustClosed.DateTodelete by one month


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin
	Truncate Table CustomerDeleted_stage 	
	Truncate Table AffiliatDeleted_Stage 	
	Truncate Table HistoryDeleted_stage 	



--shouldn't this Select be based on criteria from the Customer_Closed table joined by tipnumber where @DateDeleted(the parameter_= Customer_Closed.DateToDelete?
	Insert into CustomerDeleted_stage 
		select *, @DateDeleted from   Customer_stage 	
			where Status <> 'A' 	
	Insert into AffiliatDeleted_Stage 	
		select *, @DateDeleted from Affiliat_Stage 	
			where TipNumber not in (select TipNumber from Customer_Stage) 
					or AcctStatus <> 'A'
	Insert into HistoryDeleted_stage 	
		select *, @DateDeleted from History_stage 	
			where TipNumber not in (select TipNumber from Customer_Stage)
--delete unless the tip has hd transactions affter the datedeleted OR it's a request for brochure
	Delete from Customer_stage 	where Status ='C' AND TipNumber in (Select Tipnumber from customer_closed where DateToDelete <=@DateDeleted )
		AND tipnumber not in (select tipnumber from history where Histdate>=@DateDeleted AND Trancode<>'RQ')  

--------------'where status=C instead
	Delete from Affiliat_Stage 	where TipNumber not in (select TipNumber from Customer_Stage) 

	Delete from History_stage 	where TipNumber not in (select TipNumber from Customer_Stage)




End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin
	Begin Tran 

	-- flag all Undeleted Customers "A" that have an input_purge_pending record 
	Update customer set status = 'A' 
		where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)




	-- Insert customer to customerdeleted 
	Insert Into CustomerDeleted 
		Select c.* , @DateDeleted      ---GET FORM CUSTOMER_CLOSED
		From Customer c 
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@DateDeleted)

	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn't work. I keep getting a datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
		 LastName, YTDEarned, CustId, DateDeleted )
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
			   LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		From Affiliat  
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@DateDeleted)

	-- copy history to historyDeleted 
	Insert Into HistoryDeleted 
		Select H.* , @DateDeleted as DateDeleted 
		From History H 
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@DateDeleted)

	-- Delete records from History 
	Delete from History 
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@DateDeleted)

	-- Delete records from affiliat 
	Delete from Affiliat   
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@DateDeleted)

	-- Delete from customer 
	Delete from Customer
		Where Tipnumber  in (select Tipnumber from Customer_Closed  where DateToDelete<=@DateDeleted)



	Commit Transaction 

End
GO
