SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 
--load transwork and transStandard 
/*ORIGINAL FROM SOUTH FLORIDA BELOW*/
/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select t.AccountNum, t.TipNumber, t.TranCode, @MonthEnd, c.StatusCode, t.TranCode, c.LastName, 0, t.MemberNum
	from input_Customer c join Input_Transaction t on c.Tipnumber = t.Tipnumber
	where t.AccountNum not in ( Select acctid from Affiliat_Stage)
/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
GO
