USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[spSetAuditFileName]    Script Date: 07/08/2010 15:37:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='M708' + @currentdate + '.xls'
 
set @newname='O:\708\Output\AuditFiles\Audit.bat ' + @filename
set @ConnectionString='O:\708\Output\AuditFiles\' + @filename
GO
