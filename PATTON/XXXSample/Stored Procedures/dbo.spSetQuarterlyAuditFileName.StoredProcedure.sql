USE [709BeaconCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetQuarterlyAuditFileName]    Script Date: 05/04/2010 16:48:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetQuarterlyAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @pagect  nvarchar(5)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @pagect = (select count(*) from quarterly_statement_file)

set @filename='S709' + @currentdate + '-' + @pagect + '.xls'
 
set @newname='O:\709\Output\QuarterlyFiles\Audit.bat ' + @filename
set @ConnectionString='O:\709\Output\QuarterlyFiles\' + @filename
GO
