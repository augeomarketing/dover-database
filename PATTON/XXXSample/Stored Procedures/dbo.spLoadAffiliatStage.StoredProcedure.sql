USE [709BeaconCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 05/04/2010 16:48:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend, right(c.custid,4),  c.Statuscode, 'Credit Card', c.LastName, 0, c.custid
	from roll_Customer c where c.cardnumber not in ( Select acctid from Affiliat_Stage)


/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

update affiliat_stage
set lastname = b.lastname 
from input_customer b, affiliat_stage a
where   b.tipnumber = a.tipnumber

update affiliat_stage
set acctstatus = b.statuscode
from input_customer b, affiliat_stage a
where   b.tipnumber = a.tipnumber
GO
