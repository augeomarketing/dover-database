USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[spSetPrimary_Tipnumber]    Script Date: 07/08/2010 15:37:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetPrimary_Tipnumber] 
AS 


declare @wtip_cnt numeric

set @wtip_cnt = (Select max(tip_cnt) from comb_in ) 
 
update comb_in set tip_pri = (select tip_sec from comb_in where
tip_cnt = @wtip_cnt)

delete comb_in where tip_cnt = @wtip_cnt

delete comb_in where tip_pri = tip_sec
GO
