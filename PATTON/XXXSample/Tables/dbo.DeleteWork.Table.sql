USE [709BeaconCU]
GO
/****** Object:  Table [dbo].[DeleteWork]    Script Date: 05/04/2010 16:49:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeleteWork](
	[TIPNUMBER] [varchar](15) NULL,
	[AcctID] [varchar](16) NULL,
	[CustID] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
