USE [709BeaconCU]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 05/04/2010 16:49:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
