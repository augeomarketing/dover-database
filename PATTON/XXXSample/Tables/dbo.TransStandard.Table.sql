USE [709BeaconCU]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 05/04/2010 16:49:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TIP] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[AcctNum] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [varchar](4) NULL,
	[TranAmt] [decimal](18, 2) NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
