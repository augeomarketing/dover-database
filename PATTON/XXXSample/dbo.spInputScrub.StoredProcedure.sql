USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 07/08/2010 15:37:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrub] AS

Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

update Input_Customer
set [acctname1]=replace([acctname1],char(39), ' '),  [acctname2]=replace([acctname2],char(39), ' '), address1=replace(address1,char(39), ' '),
address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update Input_Customer
set [acctname1]=replace([acctname1],char(140), ' '), [acctname2]=replace([acctname2],char(140), ' '), address1=replace(address1,char(140), ' '),
address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update Input_Customer
set [acctname1]=replace([acctname1],char(44), ' '), [acctname2]=replace([acctname2],char(44), ' '), address1=replace(address1,char(44), ' '),
address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update Input_Customer
set [acctname1]=replace([acctname1],char(46), ' '), [acctname2]=replace([acctname2],char(46), ' '), address1=replace(address1,char(46), ' '),
address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update Input_Customer
set [acctname1]=replace([acctname1],char(34), ' '), [acctname2]=replace([acctname2],char(34), ' '), address1=replace(address1,char(34), ' '),
address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update Input_Customer
set [acctname1]=replace([acctname1],char(35), ' '), [acctname2]=replace([acctname2],char(35), ' '), address1=replace(address1,char(35), ' '),
address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')

update Input_Customer set zipcode = left(zipcode,5) + '-' + right(zipcode,4)
where len(zipcode) > 5


--------------- Input Customer table

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (custid is null or custid = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (acctname1 is null or acctname1 = ' ')

delete from Input_customer 
where (custid is null or custid = ' ') or  
           (cardnumber is null or cardnumber = ' ') or
           (acctname1 is null or acctname1 = ' ')

update input_customer set homephone =  Left(Input_Customer.homephone,3) + substring(Input_Customer.homephone,5,3) + right(Input_Customer.homephone,4)
--update input_customer set workphone  = Left(Input_Customer.workphone,3) + substring(Input_Customer.workphone,5,3) + right(Input_Customer.workphone,4)

--UPDATE  input_customer SET misc2 = CustID

--------------- Input Transaction table

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where 	(cardnumber is null or cardnumber = ' ') or 
		(purchase is null and [returns] is null and bonus is null) or 
		(purchase = '0' and [returns] = '0' and bonus = '0') 
	       
Delete from Input_Transaction
where 	(cardnumber is null or cardnumber = ' ') or 
	(purchase is null and [returns] is null and bonus is null)  or 
	(purchase = '0' and [returns] = '0' and bonus = '0')
GO
