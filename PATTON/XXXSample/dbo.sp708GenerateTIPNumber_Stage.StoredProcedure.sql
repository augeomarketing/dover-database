USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[sp708GenerateTIPNumber_Stage]    Script Date: 07/08/2010 15:37:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp708GenerateTIPNumber_Stage]
AS 

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,AFFILIAT_stage b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update ROLL_CUSTOMER
set TIPNUMBER = a.tipnumber
from ROLL_CUSTOMER r join AFFILIAT_Stage a
on CompanyID = SECID where Misc8 = 'Y' and (r.tipnumber is null or r.tipnumber = ' ')


DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber as acctid , tipnumber	
from roll_customer where (tipnumber is null or tipnumber = ' ') and misc8 = 'N'

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 708, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 708000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '708', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Assigning Tipnumbers combining on CompanyIdentifier */

-- declare @newnum bigint, @LastTipUsed char(15)


DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct companyid as custid, tipnumber	
from roll_customer where (tipnumber is null or tipnumber = ' ') and misc8 = 'Y'

	/*    Create new tip          */
 
exec rewardsnow.dbo.spGetLastTipNumberUsed 708, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 708000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '708', @newnum
update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.companyid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
