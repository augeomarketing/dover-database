USE [708CCAP]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 07/08/2010 15:36:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [varchar](50) NULL,
	[DeletionDate] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
