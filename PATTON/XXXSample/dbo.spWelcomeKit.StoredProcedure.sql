USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 07/08/2010 15:37:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName= '708CCAP'  --(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode FROM ' + QuoteName(@DBName) + N' .dbo.customer WHERE (DATEADDED = @EndDate AND STATUS <> ''C'') '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate
GO
