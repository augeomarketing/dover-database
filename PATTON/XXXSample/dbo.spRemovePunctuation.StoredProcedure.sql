USE [708CCAP]
GO
/****** Object:  StoredProcedure [dbo].[spRemovePunctuation]    Script Date: 07/08/2010 15:37:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRemovePunctuation] AS

update commercialwork
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), address1=replace(address1,char(39), ' '),
address2=replace(address2,char(39), ' '), address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update commercialwork
set acctname1=replace(acctname1,char(140), ' '),acctname2=replace(acctname2,char(140), ' '), address1=replace(address1,char(140), ' '),
 address2=replace(address2,char(140), ' '), address4=replace(address4,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')

update commercialwork
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), address1=replace(address1,char(44), ' '),
address2=replace(address2,char(44), ' '), address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update commercialwork
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), address1=replace(address1,char(46), ' '),
address2=replace(address2,char(46), ' '), address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')
GO
