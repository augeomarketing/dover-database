USE [Fullfillment]
GO

if object_ID('usp_AugeoGiftCardOrders_Update') is not null
    drop procedure dbo.usp_AugeoGiftCardOrders_Update
GO

/****** Object:  StoredProcedure [dbo].[usp_AugeoGiftCardOrders_Update]    Script Date: 4/15/2015 12:52:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 4.9.2015
-- Description:	Mass main table update for 
--              Augeo gift card process.
--
-- Example of XML passed:
-- <Main>
--    <Row orderid="4432" redstatus="17" tipnumber="12345678912345" redreqfuldate="12/25/2015" comment="Augeo Order ID: 26577825" />
--    <Row orderid="5534" redstatus="17" tipnumber="12345678912340" redreqfuldate="1/25/2012" comment="Augeo Order ID: 26577844" />
-- </Main>
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_AugeoGiftCardOrders_Update] 
	-- Add the parameters for the stored procedure here
	@MainTableUpdates XML
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    CREATE TABLE #MainUpdateTable
    (
	OrderID BIGINT,
	TipNumber CHAR(15),
	RedReqFulDate SMALLDATETIME,
	Comment VARCHAR(255),
	RedStatus TINYINT
    )
   
    INSERT INTO #MainUpdateTable
    (
	OrderID,
	TipNumber,
	RedReqFulDate,
	Comment,
	RedStatus
    )
    SELECT
	Main.n.value('@orderid', 'bigint') AS OrderID, 
	Main.n.value('@tipnumber', 'char(15)') AS TipNumber,
	Main.n.value('@redreqfuldate', 'smalldatetime') AS RedReqFulDate,
	Main.n.value('@comment', 'varchar(255)') AS Comment,
	Main.n.value('@redstatus', 'tinyint') AS RedStatus
    FROM
	@MainTableUpdates.nodes('/Main/Row') as Main(n)
		

    UPDATE M SET M.RedReqFulDate = F.RedReqFulDate, M.RedStatus = F.RedStatus, M.Notes = ISNULL(M.Notes, '') + F.Comment
    FROM dbo.Main AS M 
    INNER JOIN #MainUpdateTable AS F ON M.OrderID = F.OrderID AND M.TipNumber = F.TipNumber

    SELECT
	T.OrderID AS FileOrderID,
	T.TipNumber AS FileTipNumber,
	T.Comment AS FileAugeoOrderID,
	M.TipNumber,
	M.OrderID
    FROM
	#MainUpdateTable AS T
    LEFT OUTER JOIN 
	dbo.Main AS M ON T.OrderID = M.OrderID AND T.TipNumber = M.TipNumber

END



GO


