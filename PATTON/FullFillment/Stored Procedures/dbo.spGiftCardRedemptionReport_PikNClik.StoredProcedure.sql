USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spGiftCardRedemptionReport_PikNClik]    Script Date: 11/18/2014 11:08:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGiftCardRedemptionReport_PikNClik]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGiftCardRedemptionReport_PikNClik]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spGiftCardRedemptionReport_PikNClik]    Script Date: 11/18/2014 11:08:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGiftCardRedemptionReport_PikNClik]
	@StartDate			DATETIME,
	@EndDate			DATETIME

AS

SELECT	rtrim(ltrim(catalogcode))	AS Catalogcode,
		count(catalogcode)			AS Card_count
FROM	selection
WHERE	cast(submitdate as DATE) between @StartDate and @EndDate
	AND	(catalogcode like 'GCR%' or itemname like '%VISA%')
	AND	itemname not like '%Quickgifts%'
GROUP BY	catalogcode
ORDER BY	catalogcode

GO


