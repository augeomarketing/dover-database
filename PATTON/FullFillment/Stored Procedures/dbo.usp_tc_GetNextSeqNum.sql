USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_GetNextSeqNum]    Script Date: 12/17/2010 13:57:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_GetNextSeqNum]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_GetNextSeqNum]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_GetNextSeqNum]    Script Date: 12/17/2010 13:57:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Bob Lawliss>
-- Create date: <3/15/10>
-- =============================================
CREATE PROCEDURE [dbo].[usp_tc_GetNextSeqNum] 
	@DebitCredit varchar(6),
	@NextSeqNum varchar(6) output
AS
BEGIN
	SET NOCOUNT ON;

declare @NumRecs int, @date datetime, @FileCreationDate varchar(7),  @FileCreationTime varchar(7), 
@LastSeqNum varchar(6), @NextSeqID int ,@TmpInt int

				
set @date = GETDATE()

if upper(@DebitCredit)='CREDIT'
	Begin
			--make sure that the MAXIMUM number of times that you can create a new Sequence
			--number for any given day is 9
			select @NumRecs =COUNT(*) from tc_FileSeqNumbers where CONVERT(varchar(10),CreationDateTime)=CONVERT(varchar(10),@Date)
			if @NumRecs =9 
				begin 
					print 'FAIL'
					set @NextSeqNum='000000'
					return
				end
				
			
			if @NumRecs =0 
				begin
						set @NextSeqNum='000001'
						insert into tc_FileSeqNumbers (SeqNum, CreationDateTime, DebitCredit)
						values(@NextSeqNum, @date, @DebitCredit)
				end
			else
			begin
				SELECT @FileCreationDate=cast( year(@date) as varchar(4)) + RIGHT('000' + CAST(DATEPART(dy, @date) AS varchar(3)),3) 
				select  @FileCreationTime=replace(convert(varchar(100),@date,108),':','')
				--  select @FileCreationDate
				
				print 'THIS'
				select @LastSeqNum=isnull(SeqNum,0) from tc_FileSeqNumbers where CreationDateTime=(Select max(CreationDateTime) from tc_FileSeqNumbers)
				print convert(varchar(30),@LastSeqNum)
				
				
				set @NextSeqNum=convert(varchar(6),CONVERT(int,@LastSeqNum)+1 )
				set @NextSeqNum= REPLICATE('0', (6-len(convert(varchar(6),@NextSeqNum)))) + @NextSeqNum
				print 'THAT'
				print 'HERE:' + @NextSeqNum

				insert into tc_FileSeqNumbers (SeqNum, CreationDateTime, DebitCredit)
						values(@NextSeqNum, @date, @DebitCredit)
			end
	end
	return
	
	
	
	
END


GO

