/****** Object:  StoredProcedure [dbo].[spLoadCCLastSix]    Script Date: 03/12/2009 09:50:40 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spLoadCCLastSix]
as

Declare @DBName varchar(100), @SQLIf nvarchar(1000)

Delete from LastSixMerge
Delete from CCLastSix

DECLARE cur_databases CURSOR FOR
	select name from master.dbo.sysdatabases where name not in ('tempdb') and (name like '0%' or name like '1%' or name like '2%' or name like '3%' or name like '4%' or name like '5%' or name like '6%' or name like '7%' or name like '8%' or name like '9%' or name like 'ASB%')
	OPEN cur_databases 
	fetch next  from cur_databases into @DBName
	
WHILE (@@FETCH_STATUS=0)
	BEGIN
		set @SQLIF='If exists ( SELECT * FROM ' + Quotename(@DBNAME) + N'.dbo.sysobjects where name=''affiliat'')
		BEGIN
			INSERT INTO CCLastSix (TipNumber, LastSix, LastSixID)
				SELECT RTRIM(tipnumber), Right(rtrim(acctid),6), (RTRIM(tipnumber) + Right(rtrim(acctid),6))
				FROM ' + Quotename(@DBNAME) + N'.dbo.affiliat
				WHERE (tipnumber IN (SELECT DISTINCT TipNumber FROM Main)) and (RTRIM(tipnumber) + Right(rtrim(acctid),6)) not in (select lastsixid from cclastsix)
				
		END'
		exec sp_executesql @SQLIF
		
		fetch next  from cur_databases into @DBName
	END

close cur_databases
deallocate cur_databases


insert into lastsixmerge (lastsixid, transid)
SELECT CCLastSix.LastSixID, Main.TransID
FROM CCLastSix INNER JOIN Main ON CCLastSix.TipNumber = Main.TipNumber
where transid not in (select transid from lastsixmerge)
GO
