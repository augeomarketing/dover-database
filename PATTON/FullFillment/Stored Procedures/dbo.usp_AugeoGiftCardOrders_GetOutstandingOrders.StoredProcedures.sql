USE [fullfillment]
GO

if object_ID('usp_AugeoGiftCardOrders_GetOutstandingOrders') is not null
    drop procedure dbo.usp_AugeoGiftCardOrders_GetOutstandingOrders
GO

/****** Object:  StoredProcedure [dbo].[usp_AugeoGiftCardOrders_GetOutstandingOrders]    Script Date: 4/23/2015 12:59:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:  Nicholas T. Parsons
-- Create date: 4.20.2015
-- Description:	Obtains sent orders that have been outstanding 
--              for @DaysToCheck days.
--
-- History:
--    5/4/2015 NAPT - Added @DaysToCheck parameter 
--                    to be used instead of the hard
--                    coded 7 value.
-- =============================================
CREATE PROCEDURE [dbo].[usp_AugeoGiftCardOrders_GetOutstandingOrders] 
	-- Add the parameters for the stored procedure here
	@DaysToCheck INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @CurrentDate SMALLDATETIME = GETDATE()

    SELECT
	TipNumber,
	OrderId,
	ItemNumber,
	HistDate
    FROM
	dbo.Main
    WHERE
	RedStatus = 1 AND
	TranCode = 'RC' AND
	ItemNumber LIKE '%-AUG-%' AND
	Routing = 3 AND
	DATEADD(DAY, @DaysToCheck, HistDate) <= @CurrentDate
END


GO
