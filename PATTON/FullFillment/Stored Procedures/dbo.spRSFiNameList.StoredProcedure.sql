USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSFiNameList]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSFiNameList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSFiNameList] 
				(@TipsToInclude	nvarchar(4000))

as

select clientname
from rewardsnow.dbo.dbprocessinfo
where dbnumber in ( select item from dbo.Split ( @TipsToInclude, ',') )
order by clientname
GO
