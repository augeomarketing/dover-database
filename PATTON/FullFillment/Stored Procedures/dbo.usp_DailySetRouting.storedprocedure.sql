USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_DailySetRouting]    Script Date: 06/16/2010 15:34:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DailySetRouting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DailySetRouting]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_DailySetRouting]    Script Date: 06/16/2010 15:34:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_DailySetRouting]

AS

--------------------------------------------------------------------------------------------------------------------------------
--Set the Routing for all redemptions fulfilled by a 3rd party vendor
--------------------------------------------------------------------------------------------------------------------------------
/*RBM - 06/16/2010 - Added logic for SCU gift cards (SCUHN/GCSCUHN*/
Update	fullfillment.dbo.Main 
Set		Routing = '3'
where	Routing is null
	and	(
		trancode in ('RC','RM')
	or	Left(itemnumber, 3) in ('HIN','PSA','CAF','TB-','LPN','GMT','BUL','GTN','SVM')	--3rd Party Vendor items
	or	Left(itemnumber, 5) in ('GCSVM','SCUHN')										--3rd Party Vendor Gas/Gift Cards
	or	LEFT(itemnumber, 7) in ('GCSCUHN')												--3rd Party Vendor Gift Cards
	or	(left(itemnumber,3) = 'WPH' and tipfirst = '594')								--Harris Branded Items
	or	itemnumber like '%SHOPLET%'														--Shoplet Gift Cards  (RBM - Added Shoplet Routing - 7/16/10)
		)

--------------------------------------------------------------------------------------------------------------------------------
--Set the Routing for all redemptions fulfilled by a Financial Institution
--------------------------------------------------------------------------------------------------------------------------------
Update	fullfillment.dbo.Main
Set		Routing = '2'
where	Routing is null
	and	(
		(trancode in ('RB','RF','RG','RK','RR','RT','RU'))			--All Cash Back/Contest/Fee/Charity/Travel Cert Redemptions
	or	(itemnumber = 'OnlineTravel')							--Online Travel Booking
	or	(trancode = 'RC' and tipfirst in ('002','003')and left(itemnumber,8) in ('GCR-ALII','GCR-ALOH','GCR-BEAR','GCR-CITY','GCR-COFF','GCR-FOOD','GCR-MACY','GCR-ZIPP','GCR-MM-0'))		--ASB Gift Cards
	or	(left(catalogdesc,4) = 'Kids' and tipfirst = '217')		--Callaway Kids Cards
	or	(trancode = 'RC' and TIPfirst not in ('231') and (Trandesc = 'SVC' or itemnumber like '%VISA%' or catalogdesc like '%VISA%'))		--VISA gift cards
	or	(left(itemnumber, 8) = 'GCR-KERN' and tipfirst in ('226','230'))		--Kern Gift Cards
	or	(trancode = 'RC' and (left(itemnumber, 4) = 'GCX-' or left(itemnumber, 2) = 'X-'))		--FI Fulfilled Gift Cards
		)

--------------------------------------------------------------------------------------------------------------------------------
--Set the Routing for any unassigned redemptions
--------------------------------------------------------------------------------------------------------------------------------
Update	fullfillment.dbo.Main
Set		Routing = '1'
where	Routing is null

GO


