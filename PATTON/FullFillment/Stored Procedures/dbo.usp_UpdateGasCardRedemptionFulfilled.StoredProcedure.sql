use fullfillment
GO

if object_id('usp_UpdateGasCardRedemptionFulfilled') is not null
    drop procedure dbo.usp_UpdateGasCardRedemptionFulfilled
GO

create procedure  dbo.usp_UpdateGasCardRedemptionFulfilled

AS

update m
    set redreqfuldate = getdate(),
        redstatus = 1
from fullfillment.dbo.main m join fullfillment.dbo.gas_temp tmp
    on m.transid = tmp.transid
    

update s 
    set datefulfilled = getdate()
from fullfillment.dbo.selection s join fullfillment.dbo.gas_temp tmp
    on s.transid = tmp.transid

