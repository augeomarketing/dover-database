USE [fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateRaffleREDStatus]    Script Date: 02/23/2011 14:40:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spUpdateRaffleREDStatus]  AS
declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)
DECLARE @DateToday DATETIME
DECLARE @Datework nvarchar(19)

SET @DateToday = getdate()
--print '@DateToday'
--print @DateToday
set @Datework = left(@DateToday,11)
SET @DateToday = @Datework
--print '@DateToday'
--print @DateToday



--Update the status to '13' (TO BE SENT TO FI) in MAIN and the File Holding the transactions. This is done because the process
-- will run every week but delivery may be only once per Month
 
Update  .dbo.main set redstatus = '13' where TRANSID in (select TRANSID from dbo.Raffle_Redemptions)
GO
