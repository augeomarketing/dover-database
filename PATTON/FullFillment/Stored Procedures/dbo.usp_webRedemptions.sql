USE [fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[usp_webRedemptions]    Script Date: 11/01/2011 14:52:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110930
-- Description:	Get Redemptions for LRC
-- =============================================
ALTER PROCEDURE [dbo].[usp_webRedemptions]
	@tipfirst varchar(3),
	@trancode varchar(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(*) as redemptionCount
	FROM fullfillment.dbo.main
	WHERE LEFT(tipnumber,3) = @tipfirst
		AND TranCode IN (SELECT ITEM FROM rewardsnow.dbo.ufn_split(@trancode, ','))
END

--exec fullfillment.dbo.usp_webRedemptions '002', 're, rc'