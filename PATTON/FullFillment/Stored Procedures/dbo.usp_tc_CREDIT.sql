USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_CREDIT]    Script Date: 12/17/2010 13:56:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_CREDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_CREDIT]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_CREDIT]    Script Date: 12/17/2010 13:56:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Bob Lawliss>
-- Create date: <3/15/10>
--
-- Modified:  Barriere,Allen --2010/12/15
-- =============================================
/******************************************/
/* S Blanchette                           */
/* 3/2011                                 */
/* SEB003                                 */
/* Carry forward Tipnumber and TCID       */
/******************************************/

/******************************************/
/* S Blanchette                           */
/* 6/2011                                 */
/* SEB004                                 */
/* Fix record type on the detail records  */
/******************************************/

/******************************************/
/* S Blanchette                           */
/* 1/2012                                 */
/* SEB005                                 */
/* Add Name1 and Name2                    */
/******************************************/


CREATE PROCEDURE [dbo].[usp_tc_CREDIT] 
 	@NextSeqNum varchar(6)
AS
BEGIN
SET NOCOUNT ON;

	--begin Header Rec  CHANGE M099 to M000 for non-test  see spec
	--get the file creation date YYYYDDD [Julian Dates]
	declare @date datetime, 
	@FileCreationDate varchar(7),  
	@FileCreationTime varchar(7), 
	@NextSeqID int = 0 , @TmpInt int = 0, @ctr int = 0


	DECLARE @Rec_Len CHAR(5) = '00512',
			@Rec_Type CHAR(4) = 'M000',
			@Client_Id CHAR(5) = '15001',
			@SourceId CHAR(3) =  '825',
			@Rec_TypeD CHAR(4) = 'M100' /* SEB004 */
			
	set @date = GETDATE()
	
	SELECT	@FileCreationDate = cast( year(CreationDateTime) as varchar(4)) + RIGHT('000' + CAST(DATEPART(dy, CreationDateTime) AS varchar(3)),3),
			@FileCreationTime = replace(convert(varchar(100),CreationDateTime,108),':','')  
		from tc_FileSeqNumbers where SeqNum = @NextSeqNum

	insert into tc_CREDIT_Hdr (Rec_Len, Rec_Type, Rec_ID, Client_ID, Source, Filler, File_Creation_Date, Creation_Time, File_Seq_Num, Filler1)
	
	SELECT	@Rec_Len as Rec_Len, 
			@Rec_Type as Rec_Type, 
			REPLICATE(' ', 24) as Rec_ID, 
			@Client_Id as Client_ID,
			@SourceId  as source, 
			REPLICATE(' ', 50) as filler,
			@FileCreationDate as File_Creation_Date, 
			@FileCreationTime as Creation_Time,
			@NextSeqNum as File_Seq_Number,
			REPLICATE(' ', 402) as filler
			
	select	m.tipnumber, 
			tc.tcid, 
			tc.transid, 
			m.RedStatus , 
			tc.TCStatus, 
			srs.redstatusname, 
			stcs.TCStatusName, 
			tc.TCLastSix , 
			case isnull(tc.TCActValRed, 0) 
				WHEN 0 THEN m.cashvalue 
				ELSE tc.TCActValRed
			end	AS TCActValRed, 
			tc.TCRedDate, 
			tc.TCCreditDate,
			m.cashvalue as CV,
			tc.TCActValRed as AVR,
			m.Name1, /* SEB005 */
			m.Name2  /* SEB005 */
			into #tmp
		from 
			main m 
				join TravelCert tc on m.TransID =tc.TransID 
				join SubRedemptionStatus srs on srs.RedStatus = m.RedStatus 
				join SubTravelCertStatus stcs on stcs.TCStatus  = tc.TCStatus 
		where  1=1
			and stcs.TCStatusName='Returned'
			and srs.RedstatusName='Completed' --THIS NEEDS TO BE REMMED OUT IN TEST IN ORDER TO GET ANY RECORDS BACK
			and m.TipFirst in ('002','003')  -- BRITTANY AND DEE SAID THAT BOTH FIs get sent together

	TRUNCATE TABLE tc_Credit_wrk
	TRUNCATE TABLE tc_CREDIT_OUT_FINAL

	insert into tc_Credit_wrk 
	SELECT	transid as transid, 
			MAX(tclastsix) as tclastsix, 
			MAX(TCActValRed) as TcActValRed,
			MAX(ACCTID) as acctid ,
			MAX(TIPNUMBER) as tipnumber ,
			MAX(AcctType) as accttype ,
			MAX(lastname) as lastname ,
			MAX(CustID) as custid,
			MAX(tcid) as tcid,  /* SEB003 */
			MAX(Name1) as Name1, /* SEB005 */
			MAX(Name2) as Name2  /* SEB005 */
	FROM (				
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID ,
				#tmp.tcid,   /* SEB003 */ 
				#tmp.Name1, /* SEB005 */
				#tmp.Name2 /* SEB005 */											
			from #tmp 
				join asb.dbo.AFFILIAT a on a.TIPNUMBER = #tmp.tipnumber
					and right(a.acctid,4) = RIGHT(rtrim(#tmp.tcLastSix),4)
			where (a.AcctType ='Credit' AND LEFT(a.acctid,6) <> '485107')

		UNION ALL
		
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID ,
				#tmp.tcid,   /* SEB003 */ 
				#tmp.Name1, /* SEB005 */
				#tmp.Name2 /* SEB005 */												
			from #tmp join asbcorp.dbo.AFFILIAT a on a.TIPNUMBER=#tmp.tipnumber
				and right(a.acctid,4)=RIGHT(rtrim(#tmp.tcLastSix),4)
			where (a.AcctType ='Credit' AND LEFT(a.acctid,6) <> '485107')
		) AS temp
		group by transid
		having COUNT(transid) = 1
		order by TIPNUMBER 

	insert into tc_error 
	SELECT	transid as transid,
			'DUPLICATE TRANSID' as reason 
	FROM (				
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID 											
			from #tmp 
				join asb.dbo.AFFILIAT a on a.TIPNUMBER = #tmp.tipnumber
					and right(a.acctid,4) = RIGHT(rtrim(#tmp.tcLastSix),4)
			where (a.AcctType ='Credit' AND LEFT(a.acctid,6) <> '485107')

		UNION ALL
		
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID 											
			from #tmp join asbcorp.dbo.AFFILIAT a on a.TIPNUMBER=#tmp.tipnumber
				and right(a.acctid,4)=RIGHT(rtrim(#tmp.tcLastSix),4)
			where (a.AcctType ='Credit' AND LEFT(a.acctid,6) <> '485107')
		) AS temp
		group by transid
		having COUNT(transid) <> 1



	drop table #tmp

	insert into tc_CREDIT_OUT (	SeqNum, 
								TransID, 
								Rec_Len, 
								Rec_Type,  
								Rec_ID, 
								Client_ID,  
								Source, 
								Filler, 
								Org, 
								Acct_Nbr, 
								Tran_Amt, 
								Txn_Code, 
								Txn_Code_Ind, 
								Eff_Date, 
								SKU_NBR, 
								Descr, 
								Credit_Plan, 
								Auth_Code,
								Ticket_nbr,
								PO_Nbr,
								Pmt_Ref_Nbr, 
								Item_Nbr,
								Store_Nbr, 
								Dept,
								SalesClerk, 
								Quantity,
								Unit_Price, 
								Ref_nbr,
								Plan_Seq, 
								FS_Points,
								FS_Pgm_Nbr, 
								Merchant_Org,
								Merchant_Store, 
								Category_Code,
								Visa_Tran_ID, 
								Intch_Free,
								Qual_Ind, 
								Card_Nbr,
								Card_Seq, 
								Card_Blk_Code,
								Ins_Prod, 
								Merch_ID,
								Trm_Actn_Flag, 
								Merch_Country_Code,
								User_Filler,Filler_1,
								Tipnumber,  /* SEB003 */
								TCID,    /* SEB003 */
								Name1,   /* SEB005 */
								Name2 )  /* SEB005 */
				select	@NextSeqNum, 
						transid,		
						@Rec_Len as Rec_Len,	
		/* SEB004		@Rec_Type as Rec_Type,	*/
		/* SEB004 */	@Rec_TypeD as Rec_Type,	
						REPLICATE(' ', 24) as Rec_ID,	
						@Client_Id as Client_ID,
						@SourceId  as Source,
						REPLICATE(' ',50) as Filler,
						'570' as Org,
						'000' + acctid as Acct_Nbr,
						REPLICATE('0', (18-len(replace(convert(varchar(18),isnull(TCActValRed,0)),'.','') ))) + replace(convert(varchar(18),isnull(TCActValRed,0)),'.','') as Tran_Amt, 
						'285' as Txn_Code, 
						'C' as Txn_Code_Ind , 
						@FileCreationDate as Eff_Date, 
						REPLICATE('0', 9 ) as SKU_NBR,
						REPLICATE (' ', 40) as Descr, 
						'10002' as Credit_Plan ,
						REPLICATE(' ', 6) as Auth_Code, 
						REPLICATE(' ', 15) as Ticket_nbr, 
						REPLICATE(' ', 16) as PO_Nbr,
						REPLICATE(' ', 15) as Pmt_Ref_Nbr, 
						REPLICATE(' ', 5) as Item_Nbr,
						'999999998' as Store_Nbr, 
						REPLICATE(' ', 4) as Dept,
						REPLICATE(' ', 4) as SalesClerk, 
						REPLICATE(' ', 4) as Quantity,
						REPLICATE(' ', 18) as Unit_Price, 
						REPLICATE(' ', 23) as Ref_nbr,
						REPLICATE(' ', 2) as Plan_Seq, 
						REPLICATE(' ', 10) as FS_Points,
						' ' as FS_Pgm_Nbr, 
						'000' as Merchant_Org,
						REPLICATE(' ', 9) as Merchant_Store, 
						'0000' as Category_Code,
						REPLICATE(' ', 15) as Visa_Tran_ID, 
						REPLICATE(' ', 10) as Intch_Free,
						' ' as Qual_Ind, 
						REPLICATE(' ', 19) as Card_Nbr,
						REPLICATE(' ', 4) as Card_Seq, 
						' ' as Card_Blk_Code,
						REPLICATE(' ', 2) as Ins_Prod, 
						REPLICATE(' ', 15) as Merch_ID,
						REPLICATE(' ', 2) as Trm_Actn_Flag, 
						REPLICATE(' ', 3) as Merch_Country_Code,
						REPLICATE(' ', 20) as User_Filler,					
						REPLICATE(' ', 68) as Filler_1,
						Tipnumber, /* SEB003 */
						TCID,  /* SEB003 */
						Name1,   /* SEB005 */
						Name2   /* SEB005 */
				from tc_Credit_wrk



--============================================================================
--FINAL OUTPUT BELOW


declare @RecCount int = 0,
		@TotalAmt int = 0, 
		@strRecCount varchar(10) = '0', 
		@strTotalAmt varchar(17) = '0'
		
select @RecCount = COUNT(*) from tc_CREDIT_OUT where SeqNum=@NextSeqNum
select @TotalAmt = ISNULL( SUM(convert(int, tran_amt) ),0 ) from tc_CREDIT_OUT where SeqNum=@NextSeqNum
set @strRecCount = REPLICATE('0', (10-len(convert(varchar(10),@RecCount ) ))) + convert(varchar(10),@RecCount )
set @strTotalAmt = REPLICATE('0', (17-len(convert(varchar(17),@TotalAmt ) ))) + convert(varchar(17),@TotalAmt )

--print 'RecCount:'  + CONVERT(VARCHAR(MAX), @reccount )
--print 'TotalAmt:'  + CONVERT(VARCHAR(MAX), @TotalAmt )


insert into tc_CREDIT_trailer (SeqNum, Rec_len, Rec_Type, Rec_ID, Client_ID, Source, filler, Tot_Rec_Count, Tot_Amt, Debit_Tran_Count, Debit_Total_Amt, Credit_Tran_Count, Credit_Total_Amt, filler1)
	Select	@NextSeqNum,  
			@Rec_Len,  
			'M999',
			REPLICATE(' ', 24),
			@Client_Id,
			@SourceId ,
			REPLICATE(' ',50), 
			@strRecCount,
			@strTotalAmt,
			REPLICATE(' ', 17),
			REPLICATE(' ', 10), 
			@strRecCount,
			@strTotalAmt,
			REPLICATE(' ', 340)

insert into tc_CREDIT_OUT_FINAL (FullRecord, SeqChar)
select  Rec_Len +  Rec_Type + Rec_ID + Client_ID + Source + Filler + File_Creation_Date + Creation_Time + File_Seq_Num + Filler1,
		'A'
	from tc_CREDIT_Hdr 
	where File_Seq_Num=@NextSeqNum									
									
insert into tc_CREDIT_OUT_FINAL (FullRecord, SeqChar)
	select   Rec_Len + Rec_Type + Rec_ID + Client_ID + Source + Filler + Org + Acct_Nbr + Tran_Amt + Txn_Code + Txn_Code_Ind + Eff_Date + SKU_NBR + Descr + Credit_Plan + Auth_Code + Ticket_nbr + PO_Nbr + Pmt_Ref_Nbr + Item_Nbr + Store_Nbr + Dept + SalesClerk + Quantity + Unit_Price + Ref_nbr + Plan_Seq + FS_Points + FS_Pgm_Nbr + Merchant_Org + Merchant_Store + Category_Code + Visa_Tran_ID + Intch_Free + Qual_Ind + Card_Nbr + Card_Seq + Card_Blk_Code + Ins_Prod + Merch_ID + Trm_Actn_Flag + Merch_Country_Code + User_Filler + Filler_1  ,
		'B'
	from tc_CREDIT_OUT 
	where SeqNum=@NextSeqNum
	order by 1	

insert into tc_CREDIT_OUT_FINAL (FullRecord, SeqChar)
select Rec_len + Rec_Type + Rec_ID + Client_ID + Source + filler + Tot_Rec_Count + Tot_Amt + Debit_Tran_Count + Debit_Total_Amt + Credit_Tran_Count + Credit_Total_Amt + filler1,
		'C' 
 from tc_CREDIT_trailer  where SeqNum=@NextSeqNum

END




GO

