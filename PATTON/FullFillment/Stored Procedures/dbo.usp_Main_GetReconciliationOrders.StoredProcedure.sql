USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_Main_GetReconciliationOrders]    Script Date: 02/15/2016 13:55:01 ******/
SET ANSI_NULLS ON
GO

if object_ID('dbo.usp_Main_GetReconciliationOrders') is not null
    drop procedure dbo.usp_Main_GetReconciliationOrders
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 2/15/2016
-- Description:	Gets all reconciliation 
--              dreampoints orders from latest
--              order date. 
-- =============================================
CREATE PROCEDURE [dbo].[usp_Main_GetReconciliationOrders] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentOrderDate DATETIME = (SELECT TOP 1 HistDate FROM dbo.Main WHERE RedStatus = 1 AND [Source] LIKE 'DREAMPOI%' ORDER BY HistDate DESC)

	SELECT
		TipNumber,
		HistDate AS OrderDate,
		OrderID AS OrderNumber,
		TranCode AS OrderType,
		CatalogDesc AS CatalogDescription,
		CatalogQty AS Quantity,
		Points
	FROM
		dbo.Main
	WHERE
		RedStatus = 1 AND
		Source LIKE 'DREAMPOI%' AND
		CONVERT(DATE, HistDate) = CONVERT(DATE, @CurrentOrderDate)
		
END


GO



