USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_PremcoMerchandiseOrders_Get]    Script Date: 6/5/2015 4:06:13 PM ******/
if object_ID('dbo.usp_PremcoMerchandiseOrders_Get') is not null
    drop procedure dbo.usp_PremcoMerchandiseOrders_Get
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 6.3.2015
-- Description:	Gets all open Premco merchandise 
--              orders.
-- 
-- History:
--      7/2/2015 NAPT - Removed cost from being retrieved
--                      since Premco doesn't need this data 
--                      and returned codes from 
--                      extended table due to length.
-- =============================================
CREATE PROCEDURE [dbo].[usp_PremcoMerchandiseOrders_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT
		ISNULL(OrderId, 0) AS PONumber,
		ISNULL(Name1, '') AS Name1,
		ISNULL(Name2, '') AS Name2,
		ISNULL(long_catalog_code, ItemNumber) AS CatalogCode,
		ISNULL(CatalogQty, '') AS Qty,
		ISNULL(SAddress1, '') AS Address1,
		ISNULL(SAddress2, '') AS Address2,
		ISNULL(SCity, '') AS City,
		ISNULL(SState, '') AS State,
		ISNULL(SZip, '') AS Zip,
		ISNULL(Phone1, '') AS Phone
	FROM
		dbo.Main
	LEFT OUTER JOIN
		[Catalog].[dbo].[CatalogCode] ON dim_catalog_code = ItemNumber
	WHERE
		RedStatus = 0 AND
		TranCode = 'RM' AND
		ItemNumber LIKE 'PRE-%' 

	
END

GO    
