use fullfillment
GO

if object_id('usp_ExtractGasCardRedemptions') is not null
    drop procedure dbo.usp_ExtractGasCardRedemptions
GO

create procedure  dbo.usp_ExtractGasCardRedemptions

AS

-- Clear work table
truncate table fullfillment.dbo.GAS_Temp

-- Populate work table with Gas Card redemptions from Main as well as Pik-n-Clik
insert into  fullfillment.dbo.GAS_Temp
(transid, Ship_To_Name, Ship_To_Address1, Ship_To_Address2, Ship_To_City, Ship_To_State, Ship_To_Zip, Ship_To_Phone, GAS_Part_Number, GAS_Amount, QTY_Ordered)
Select  transid,                                -- Main redemptions
        isnull(Name1, 'Unknown Addressee')      as Ship_To_Name,
        isnull(Saddress1, '')                   as Ship_To_Address1,
        isnull(saddress2, '')	                as Ship_To_Address2,
        isnull(Scity, '')                       as Ship_To_City,
        isnull(Sstate, '')                  	as Ship_To_State,
        isnull(Szip, '')                    	as Ship_To_Zip,
        isnull(phone1, '')                  	as Ship_To_Phone,
        isnull(ItemNumber, '')              	as GAS_Part_Number,
        isnull(Cashvalue, '')               	as GAS_Amount,
        isnull(CatalogQty, '')              	as QTY_Ordered
From	fullfillment.dbo.Main
Where	(Itemnumber like 'GCSVM%'  or itemnumber like 'SVM%')
    and	Redstatus = '0'

UNION ALL

Select  transid,                                -- Pik-n-Clik redemptions
        isnull(Name, '')                    	as Ship_To_Name,
        isnull(address1, '')                	as Ship_To_Address1,
        isnull(address2, '')                	as Ship_To_Address2,
        isnull(city, '')                    	as Ship_To_City,
        isnull(state, '')                   	as Ship_To_State,
        isnull(zip, '')                     	as Ship_To_Zip,
        isnull(phone, '')                   	as Ship_To_Phone,
        isnull(Catalogcode, '')             	as GAS_Part_Number,
        isnull(Right(rtrim(catalogcode), 3), '') as GAS_Amount,
        '1'		                                as QTY_Ordered
From fullfillment.dbo.Selection
where	(Catalogcode like 'GCSVM%'  or Catalogcode like 'SVM%')
   and	DateFulfilled is null
