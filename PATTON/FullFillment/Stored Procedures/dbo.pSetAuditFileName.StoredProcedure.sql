USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[pSetAuditFileName]    Script Date: 03/12/2009 09:50:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pSetAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @datesend char(13)

--Create Date and Time Stamp for filename

set @datesend = convert (varchar (11), getdate(), 112)+'_'+convert (varchar (2), getdate(), 8)
		+ Right(convert (varchar (5), getdate(), 8), 2)

--Put Filename together

set @filename='ASB_GiftCards_' + @datesend + '.xls'

 
set @newname='O:\5xx\Output\AuditFiles\Audit.bat ' + @filename
set @ConnectionString='O:\5xx\Output\AuditFiles\' + @filename
GO
