USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSMetavanteFIDropDownList]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSMetavanteFIDropDownList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spRSMetavanteFIDropDownList]

as

select dbnumber as tipfirst, '(' + dbnumber + ') ' + clientname as clientname
from rewardsnow.dbo.dbprocessinfo
where dbnamenexl = 'metavante'
and sid_fiprodstatus_statuscode not in ('I', 'X', 'V')
order by clientname
GO
