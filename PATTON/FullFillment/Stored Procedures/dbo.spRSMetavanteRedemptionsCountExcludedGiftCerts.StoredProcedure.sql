USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSMetavanteRedemptionsCountExcludedGiftCerts]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSMetavanteRedemptionsCountExcludedGiftCerts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSMetavanteRedemptionsCountExcludedGiftCerts]
	(@ReportStartDate		datetime,
	 @ReportEndDate		datetime,
	 @TipsToInclude		nvarchar(4000))

as

-- Set enddate so it has 23:59:59 for the time
set @reportenddate = dateadd(d, 1, @reportenddate) -- add one day to end date
set @reportenddate = dateadd(s, -1, @reportenddate) -- now back off one second.

-- get count of gift certs for metavante only, that have a null
-- redreqfuldate
select count(*) as GiftCertsExcludedCount
from dbo.Main rdm join RewardsNow.dbo.DBProcessInfo db
	on rdm.tipfirst = db.dbnumber
	and db.dbnumber in ( select item from dbo.Split ( @TipsToInclude, ',') )

where rdm.histdate between @reportstartdate and @reportenddate
and rdm.trandesc = 'gift cert'
and rdm.redreqfuldate is null
GO
