USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_CREDIT_master]    Script Date: 12/17/2010 13:56:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_CREDIT_master]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_CREDIT_master]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_CREDIT_master]    Script Date: 12/17/2010 13:56:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Bob Lawliss>
-- Create date: <3/15/10>
-- =============================================
CREATE PROCEDURE [dbo].[usp_tc_CREDIT_master] 
 

AS

SET NOCOUNT ON;
	declare @nsn char(6) = '000000'
	exec usp_tc_GetNextSeqNum 'CREDIT' , @NextSeqNum = @nsn output
	--print '@NSN' + @NSN
	
	if @NSN <> '000000'
		exec usp_tc_CREDIT @nsn

GO

