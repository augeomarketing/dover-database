USE [fullfillment]
GO

if object_ID('spExtractHinda_Giftcard_US') is not null
    drop procedure dbo.spExtractHinda_Giftcard_US
GO

/****** Object:  StoredProcedure [dbo].[spExtractHinda_GiftCard_US]    Script Date: 08/09/2010 11:39:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                                                                            */
/* BQuinn          */
/* DATE: 08/2010   */
/* REVISION: 0 */
/* THIS PROC WILL INSERT INTO THE TABLE Hinda_Temp Gift cards for US delivery */
/******************************************************************************/
create PROCEDURE [dbo].[spExtractHinda_GiftCard_US]  AS

declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)
DECLARE @DateToday DATETIME

 		set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''Hinda_Temp'')
		Begin
			TRUNCATE TABLE .dbo.Hinda_Temp 
		End '
		exec sp_executesql @SQLIf



insert into  dbo.Hinda_Temp
Select
'R' + cast(OrderID as varchar(15))	as ClientOrderNumber,
rtrim(Name1)		as ShiptoFirstName,
rtrim(Saddress1)	as ShiptoAddressLine1,
rtrim(Saddress2)	as ShiptoAddressLine2,
rtrim(Scity)		as ShiptoCity,
rtrim(Sstate)		as ShiptoState,
rtrim(Szip)		as ShiptoPostalCode,
--rtrim(Scountry)	as ShiptoCountry,
rtrim(Phone1)		as ShiptoPhone,
rtrim(email)		as ShiptoEmail,
--Replace(rtrim(Itemnumber), 'HIN-', '')	as Catalognumber,
right(Itemnumber,12) as Catalognumber,
rtrim(Catalogqty)	as Quantity,
Histdate		as [TimeStamp]
From fullfillment.dbo.Main
where	itemnumber like 'GCSCUHN-%' 
and	redstatus = '0'
and	scity <>  'APO'
and tipfirst = '231'
GO
