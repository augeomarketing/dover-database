use Fullfillment

if exists(select 1 from dbo.sysobjects where name = 'sproc_InvoiceType' and xtype = 'P')
	drop procedure dbo.sproc_Invoicetype
GO

-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,22,2009>
-- Description:	<Queries invoice detail for type record set>
-- =============================================
CREATE PROCEDURE dbo.[sproc_InvoiceType]
@tipprefix char(3),
@typequery varchar(50)

AS
CREATE TABLE #Invoicetemp(
tipfirst char(3),
client varchar(255),
Accountnum char(15),
Name varchar(50),
type varchar(30),
product varchar(300),
Qty int,
Points int,
Date smalldatetime,
source varchar(10),
cashvalue smallmoney
)

Insert INTO #Invoicetemp 
exec fullfillment.dbo.sproc_Invoicedetail  @tipprefix

Select * from  #Invoicetemp 
where Type = @typequery

drop table #Invoicetemp


/*

declare @typequery varchar(50)
declare @tipprefix char(3)

 EXEC sproc_InvoiceType  '002', qg
select @costaspoints



*/