USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_SaveDebitCredit_History]    Script Date: 12/17/2010 13:57:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_SaveDebitCredit_History]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_SaveDebitCredit_History]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_SaveDebitCredit_History]    Script Date: 12/17/2010 13:57:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Bob Lawliss>
-- Create date: <3/15/10>
-- =============================================
CREATE PROCEDURE [dbo].[usp_tc_SaveDebitCredit_History] 


AS
BEGIN
	SET NOCOUNT ON;

declare @CurrdateTime datetime =getdate()
--insert the records that were extracted into the history table
insert into tc_DebitCredit_wrk_History	(transid, tclastsix, TCActValRed, ACCTID, TIPNUMBER, AcctType, LastName, CustID, DateRun)
	select transid, tclastsix, TCActValRed, ACCTID, TIPNUMBER, AcctType, LastName, CustID, @CurrdateTime  from dbo.tc_Credit_wrk
	union 
	select transid, tclastsix, TCActValRed, ACCTID, TIPNUMBER, AcctType, LastName, CustID, @CurrdateTime  from dbo.tc_Debit_wrk	



--update tables  travelcert and Main for those records with TransIDs
--that were extracted for the run. 
UPDATE Fullfillment.dbo.travelcert 
	SET tccreditdate = @CurrdateTime 
	WHERE TransID in
	(Select transid from tc_DebitCredit_wrk_History where DateRun=@CurrDateTime)

UPDATE Fullfillment.dbo.Main 
	SET redstatus = 7 
	WHERE TransID in
	(Select transid from tc_DebitCredit_wrk_History where DateRun=@CurrDateTime)

END



GO

