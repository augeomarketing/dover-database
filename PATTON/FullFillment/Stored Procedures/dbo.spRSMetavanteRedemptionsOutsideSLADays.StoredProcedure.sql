USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSMetavanteRedemptionsOutsideSLADays]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSMetavanteRedemptionsOutsideSLADays]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSMetavanteRedemptionsOutsideSLADays]
	(@ReportStartDate		smalldatetime,
	 @ReportEndDate		smalldatetime,
	 @SLADays				int)

as

-- Build temp table of TIPFirsts for all of Metavante
select dbnumber
into #metavante_tips
from rewardsnow.dbo.dbprocessinfo
where dbnamenexl like '%metavante%'


select sts.RedStatusName, ful.trandesc, count(*) as NbrRedemptions
from fullfillment.dbo.[main] ful join #metavante_tips tips
	on ful.tipfirst = tips.dbnumber

join dbo.SubRedemptionStatus sts
	on ful.redstatus = sts.redstatus

where	histdate between @ReportStartDate and @ReportEndDate
	and  trancode != 'RQ' -- don't include brochures
	and  datediff(day, histdate, isnull(Redreqfuldate, getdate())) > @SLADays

group by ful.trandesc, sts.RedStatusName

order by ful.trandesc, sts.RedStatusName
GO
