USE [Fullfillment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* BQuinn          */
/* DATE: 08/2009   */
/* REVISION: 0 */
/* THIS PROC WILL INSERT INTO THE TABLE GiftCardsInProcess AND UPDATE THE REDSTATUS IN MAIN TO '13' (TO BE SENT)*/
/******************************************************************************/
ALTER PROCEDURE [dbo].[spUpdateFeeExtractREDStatus]  AS

--Update the status to '13' (TO BE SENT TO FI) in MAIN and the File Holding the transactions. This is done because the process
-- will run every week but delivery may be only once per Month
	UPDATE m
		set RedStatus = '13', RedReqFulDate = GETDATE()
		from Fullfillment.dbo.main m join Fullfillment.dbo.FeeRedemptionProcessed f
			on m.TransID = f.dim_FeeRedemptionProcessed_Transid
		where RedStatus <> '13'

	UPDATE f
		set dim_FeeRedemptionProcessed_RedStatus = '13'
		from Fullfillment.dbo.FeeRedemptionProcessed f join Fullfillment.dbo.Fee_Redemptions fr
			on fr.dim_Fee_Redemptions_Transid = f.dim_FeeRedemptionProcessed_Transid
		where dim_FeeRedemptionProcessed_RedStatus <> '13'
