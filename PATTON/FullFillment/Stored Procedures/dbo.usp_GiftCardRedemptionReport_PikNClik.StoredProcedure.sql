USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_GiftCardRedemptionReport_PikNClik]    Script Date: 07/26/2011 10:21:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GiftCardRedemptionReport_PikNClik]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GiftCardRedemptionReport_PikNClik]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_GiftCardRedemptionReport_PikNClik]    Script Date: 07/26/2011 10:21:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_GiftCardRedemptionReport_PikNClik]
	@StartDate			datetime,
	@EndDate				datetime

AS

declare @rptStartDate		datetime
declare @rptEndDate			datetime

set @rptStartDate = cast(year(@StartDate) as varchar(4)) + '/' +
				right('00' + cast(month(@StartDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@StartDate) as varchar(2)), 2) + ' 00:00:00'

set @rptEndDate = cast(year(@EndDate) as varchar(4)) + '/' +
				right('00' + cast(month(@EndDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@EndDate) as varchar(2)), 2) + ' 23:59:59'


select  rtrim(ltrim(s.catalogcode)) as Catalogcode, count(s.catalogcode)as Card_count
from fullfillment.dbo.selection s join catalog.dbo.catalog c
	on s.catalogcode = c.dim_catalog_code
join catalog.dbo.catalogcategory cc
	on c.sid_catalog_id = cc.sid_catalog_id
join catalog.dbo.categorygroupinfo cgi
	on cc.sid_category_id = cgi.sid_category_id
join catalog.dbo.groupinfo gi
	on gi.sid_groupinfo_id = cgi.sid_groupinfo_id
	
where sid_routing_id = 1   -- in-house fulfilled
and dim_groupinfo_trancode = 'RC' -- redeem for cards
and submitdate between @rptStartDate and @rptEndDate
and clientcode <> 'ASB'
group by catalogcode
order by catalogcode
GO


