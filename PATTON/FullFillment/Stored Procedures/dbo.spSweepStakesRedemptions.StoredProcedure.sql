USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spSweepStakesRedemptions]    Script Date: 08/02/2011 17:19:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSweepStakesRedemptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSweepStakesRedemptions]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spSweepStakesRedemptions]    Script Date: 08/02/2011 17:19:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spSweepStakesRedemptions]  AS
DECLARE @DateToday DATE = CAST(GETDATE() AS DATE)

  --Truncate work tables

DECLARE @PINS TABLE
(
	tipnumber varchar(15)
	, PIN varchar(30)
	, expire datetime
	, issued datetime
	, ProgID varchar (20)
	, dim_pins_created datetime
	, dim_pins_lastmodified datetime
	, TransID varchar(40)
)	

DECLARE @SweepstakesTipsToExtract TABLE
(
	sid_sweepstakestipstoextract_tipfirst varchar(3) primary key
	, dim_sweepstakestipstoextract_dbname varchar(40)
)


DECLARE @temp_sweepstakesredemptions TABLE
(
	dim_Temp_SweepStakesRedemptions_tipnumber varchar (15)
	, dim_Temp_SweepStakesRedemptions_name1 varchar (50)
	, dim_Temp_SweepStakesRedemptions_catalogqty int NOT NULL default(0)
	, dim_Temp_SweepStakesRedemptions_catalogdesc varchar (50)
	, dim_Temp_SweepStakesRedemptions_PointsPerItem  int NOT NULL default(0)
	, dim_Temp_SweepStakesRedemptions_totval int NOT NULL default(0)
	, dim_Temp_SweepStakesRedemptions_cashvalue smallmoney NOT NULL default(0)
	, dim_Temp_SweepStakesRedemptions_name2 varchar (50)
	, dim_Temp_SweepStakesRedemptions_histdate datetime
	, dim_Temp_SweepStakesRedemptions_PointsTotal int NOT NULL default(0)
	, dim_Temp_SweepStakesRedemptions_saddress1 varchar (50)
	, dim_Temp_SweepStakesRedemptions_saddress2 varchar (50)
	, dim_Temp_SweepStakesRedemptions_scity varchar (50)
	, dim_Temp_SweepStakesRedemptions_sstate varchar (5)
	, dim_Temp_SweepStakesRedemptions_szip varchar (15)
	, dim_Temp_SweepStakesRedemptions_DateExtracted   datetime  NOT NULL default(getdate())
	, dim_Temp_SweepStakesRedemptions_RedStatus int
	, dim_Temp_SweepStakesRedemptions_Transid varchar (40)
	, dim_Temp_SweepStakesRedemptions_Entry_id varchar (40)
	, dim_Temp_SweepStakesRedemptions_ItemNumber varchar (20)
)

BEGIN TRY

	INSERT INTO  @Temp_SweepStakesRedemptions
				(
				dim_Temp_SweepStakesRedemptions_tipnumber,dim_Temp_SweepStakesRedemptions_name1,dim_Temp_SweepStakesRedemptions_catalogqty,
				dim_Temp_SweepStakesRedemptions_catalogdesc, dim_Temp_SweepStakesRedemptions_PointsPerItem,
       			dim_Temp_SweepStakesRedemptions_totval,dim_Temp_SweepStakesRedemptions_cashvalue,
       			dim_Temp_SweepStakesRedemptions_name2,dim_Temp_SweepStakesRedemptions_histdate,
       			dim_Temp_SweepStakesRedemptions_PointsTotal, dim_Temp_SweepStakesRedemptions_saddress1, 
       			dim_Temp_SweepStakesRedemptions_saddress2, dim_Temp_SweepStakesRedemptions_scity, 
       			dim_Temp_SweepStakesRedemptions_sstate, dim_Temp_SweepStakesRedemptions_szip,
				DIM_Temp_SweepStakesRedemptions_Transid, dim_Temp_SweepStakesRedemptions_Entry_id, dim_Temp_SweepStakesRedemptions_DateExtracted,
				dim_Temp_SweepStakesRedemptions_RedStatus , dim_Temp_SweepStakesRedemptions_ItemNumber 
				)
	       		select  m.tipnumber,m.name1,m.catalogqty as qty,left(catalogdesc,50) as catalogdesc, m.points,
	    		totval = (m.cashvalue*m.catalogqty),cashvalue,name2,
       			m.histdate, pointstotal =(m.points * m.catalogqty) , m.saddress1, m.saddress2, m.scity, m.sstate, m.szip,
				m.transid, p.pin , @DateToday, m.RedStatus, m.ItemNumber 
				from main m
				inner join rn1.pins.dbo.pins p
					  on m.TransID = p.transid
				inner join catalog.dbo.Catalog c
					  on dim_catalog_code = m.ItemNumber
				inner join Catalog.dbo.catalogcategory cc
					  on c.sid_catalog_id = cc.sid_catalog_id
				inner join Catalog.dbo.categorygroupinfo cgi
					  on cgi.sid_category_id = cc.sid_category_id
				where cgi.sid_groupinfo_id = 16 -- for FI
					  --or cgi.sid_groupinfo_id = 17 -- for RNI
					  and m.RedStatus = '0'
					  
	INSERT INTO @PINs
	(
		TIPNumber, PIN , Expire, Issued, ProgID, dim_pins_created , dim_pins_lastmodified , TransID
	)
	select 
		m.TIPNumber, p.PIN, p.Expire, p.Issued, p.ProgID, p.dim_pins_created, p.dim_pins_lastmodified, m.TransID
	from 
		rn1.pins.dbo.pins p 
	inner join fullfillment.dbo.main m
		on p.tipnumber = m.TipNumber and p.progid = m.ItemNumber and p.transid = m.transid
	INNER JOIN @Temp_SweepstakesRedemptions t
		ON m.TransID = t.dim_temp_sweepstakesredemptions_transid
	where p.expire > GETDATE()
		and m.RedStatus = '0'		 
		and m.TransID is not null

	IF (SELECT COUNT(1) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME IN ('SweepStakesRedemptions', 'SweepStakesRedemptionTotal', 'SweepStakesRedemptionProcessed')) = 3
	BEGIN
		DELETE FROM SweepStakesRedemptions
	
		INSERT INTO SweepStakesRedemptions
		(
			dim_SweepStakesRedemptions_tipnumber
			,dim_SweepStakesRedemptions_name1
			, dim_SweepStakesRedemptions_catalogqty
			, dim_SweepStakesRedemptions_catalogdesc
			, dim_SweepStakesRedemptions_PointsPerItem 
			, dim_SweepStakesRedemptions_totval
			, dim_SweepStakesRedemptions_cashvalue
			, dim_SweepStakesRedemptions_name2
			, dim_SweepStakesRedemptions_histdate
			, dim_SweepStakesRedemptions_PointsTotal
			, dim_SweepStakesRedemptions_saddress1
			, dim_SweepStakesRedemptions_saddress2
			, dim_SweepStakesRedemptions_scity
			, dim_SweepStakesRedemptions_sstate
			, dim_SweepStakesRedemptions_szip
			, DIM_SweepStakesRedemptions_Transid
			, dim_SweepStakesRedemptions_Entry_id
			, dim_SweepStakesRedemptions_DateExtracted
			, dim_SweepStakesRedemptions_RedStatus 
			, dim_SweepStakesRedemptions_ItemNumber 
		)
		select 	
			t.dim_Temp_SweepStakesRedemptions_tipnumber as dim_SweepStakesRedemptions_tipnumber
			, t.dim_Temp_SweepStakesRedemptions_name1 as dim_SweepStakesRedemptions_name1
			, '1' as dim_SweepStakesRedemptions_catalogqty
			, t.dim_Temp_SweepStakesRedemptions_catalogdesc as dim_SweepStakesRedemptions_catalogdesc
			, t.dim_Temp_SweepStakesRedemptions_PointsPerItem as dim_SweepStakesRedemptions_PointsPerItem
			, (dim_Temp_SweepStakesRedemptions_cashvalue * t.dim_Temp_SweepStakesRedemptions_catalogqty) as dim_SweepStakesRedemptions_totval
			, t.dim_Temp_SweepStakesRedemptions_cashvalue as dim_SweepStakesRedemptions_cashvalue
			, t.dim_Temp_SweepStakesRedemptions_name2 as dim_SweepStakesRedemptions_name2
			, t.dim_Temp_SweepStakesRedemptions_histdate as dim_SweepStakesRedemptions_histdate
			, (t.dim_Temp_SweepStakesRedemptions_PointsPerItem * t.dim_Temp_SweepStakesRedemptions_catalogqty) as dim_SweepStakesRedemptions_PointsTotal
			, t.dim_Temp_SweepStakesRedemptions_saddress1 as dim_SweepStakesRedemptions_saddress1
			, t.dim_Temp_SweepStakesRedemptions_saddress2 as dim_SweepStakesRedemptions_saddress2
			, t.dim_Temp_SweepStakesRedemptions_scity as dim_SweepStakesRedemptions_scity
			, t.dim_Temp_SweepStakesRedemptions_sstate as dim_SweepStakesRedemptions_sstate
			, t.dim_Temp_SweepStakesRedemptions_szip as dim_SweepStakesRedemptions_szip
			, t.dIM_Temp_SweepStakesRedemptions_Transid as DIM_SweepStakesRedemptions_Transid
			, t.dim_Temp_SweepStakesRedemptions_Entry_id as dim_SweepStakesRedemptions_Entry_id
			, t.dim_Temp_SweepStakesRedemptions_DateExtracted as dim_SweepStakesRedemptions_DateExtracted
			, t.dim_Temp_SweepStakesRedemptions_RedStatus  as dim_SweepStakesRedemptions_RedStatus
			, t.dim_Temp_SweepStakesRedemptions_ItemNumber as dim_SweepStakesRedemptions_ItemNumber 
		from @Temp_SweepStakesRedemptions as t
		left outer join SweepstakesRedemptionProcessed p
			on t.dIM_Temp_SweepStakesRedemptions_Transid = p.dim_sweepstakesredemptionprocessed_transid 
		where p.dim_SweepStakesRedemptionProcessed_Transid is null
		order by dim_Temp_SweepStakesRedemptions_ItemNumber,t.dim_Temp_SweepStakesRedemptions_tipnumber


		--Aggregate Totals (Ideally, this should be a view of SweepstakesRedemptions
		INSERT INTO  .dbo.SweepStakesRedemptionTotal
		(
			sid_SweepStakesRedemptionTotal_tipfirst
			, dim_SweepStakesRedemptionTotal_totrecords
			, dim_SweepStakesRedemptionTotal_QTYReds
			, dim_SweepStakesRedemptionTotal_TotalPoints
			, dim_SweepStakesRedemptionTotal_totalvalue
			, dim_SweepStakesRedemptionTotal_totcashvalue
			, dim_SweepStakesRedemptionTotal_TotalItemPoints
			, dim_SweepStakesRedemptionTotal_dateextracted
		)
		SELECT
			ISNULL(srt.sid_SweepStakesRedemptionTotal_tipfirst, ec.SID_EXTRACT_CONTROL_TIPFIRST) as sid_SweepStakesRedemptionTotal_tipfirst
			, isnull(srt.dim_SweepStakesRedemptionTotal_totrecords, 0) as dim_SweepStakesRedemptionTotal_totrecords
			, isnull(srt.dim_SweepStakesRedemptionTotal_QTYReds, 0) as dim_SweepStakesRedemptionTotal_QTYReds
			, isnull(srt.dim_SweepStakesRedemptionTotal_TotalPoints, 0) as dim_SweepStakesRedemptionTotal_TotalPoints
			, isnull(srt.dim_SweepStakesRedemptionTotal_totalvalue, 0) as dim_SweepStakesRedemptionTotal_totalvalue
			, isnull(srt.dim_SweepStakesRedemptionTotal_totcashvalue, 0) as dim_SweepStakesRedemptionTotal_totcashvalue
			, isnull(srt.TotalItemPoints, 0) as TotalItemPoints
			, CONVERT(date, getdate()) as dim_SweepStakesRedemptionTotal_dateextracted
		FROM EXTRACT_CONTROL ec
		LEFT OUTER JOIN 
		(
				select 
					left(dim_SweepStakesRedemptions_tipnumber,3) as sid_SweepStakesRedemptionTotal_tipfirst
					, count(dim_SweepStakesRedemptions_Tipnumber) as dim_SweepStakesRedemptionTotal_totrecords
					, sum(isnull(dim_SweepStakesRedemptions_catalogqty,0)) as dim_SweepStakesRedemptionTotal_QTYReds
					, sum(isnull(dim_SweepStakesRedemptions_PointsPerItem,0)) as dim_SweepStakesRedemptionTotal_TotalPoints
					, sum(isnull(dim_SweepStakesRedemptions_totval,0)) as dim_SweepStakesRedemptionTotal_totalvalue
					, sum(isnull(dim_SweepStakesRedemptions_cashvalue * dim_SweepStakesRedemptions_catalogqty,0)) as dim_SweepStakesRedemptionTotal_totcashvalue
					, sum(isnull(dim_SweepStakesRedemptions_PointsPerItem * dim_SweepStakesRedemptions_catalogqty,0)) as TotalItemPoints
					, CONVERT(date, getdate()) as dim_SweepStakesRedemptionTotal_dateextracted
				from SweepStakesRedemptions
				group by left(dim_SweepStakesRedemptions_tipnumber,3) --order by left(dim_SweepStakesRedemptions_tipnumber,3)
		) srt
		ON ec.SID_EXTRACT_CONTROL_TIPFIRST = srt.sid_SweepStakesRedemptionTotal_tipfirst
		WHERE ec.DIM_EXTRACT_CONTROL_TRANCODE = 'RR'

		INSERT INTO  .dbo.SweepStakesRedemptionProcessed
		( 
			dim_SweepStakesRedemptionProcessed_tipnumber, dim_SweepStakesRedemptionProcessed_name1, dim_SweepStakesRedemptionProcessed_catalogqty,
			dim_SweepStakesRedemptionProcessed_catalogdesc, dim_SweepStakesRedemptionProcessed_PointsPerItem, dim_SweepStakesRedemptionProcessed_totval, 
			dim_SweepStakesRedemptionProcessed_cashvalue, dim_SweepStakesRedemptionProcessed_name2, dim_SweepStakesRedemptionProcessed_histdate,
			dim_SweepStakesRedemptionProcessed_PointsTotal, dim_SweepStakesRedemptionProcessed_saddress1,dim_SweepStakesRedemptionProcessed_saddress2 ,
			dim_SweepStakesRedemptionProcessed_scity , dim_SweepStakesRedemptionProcessed_sstate, dim_SweepStakesRedemptionProcessed_szip,
			dim_SweepStakesRedemptionProcessed_Transid, dim_SweepStakesRedemptionProcessed_DateExtracted, dim_SweepStakesRedemptionProcessed_RedStatus,
			dim_SweepStakesRedemptionProcessed_Entry_id,dim_SweepStakesRedemptionProcessed_ItemNumber
		)
		select 
			dim_SweepStakesRedemptions_tipnumber, dim_SweepStakesRedemptions_name1, dim_SweepStakesRedemptions_catalogqty,
			dim_SweepStakesRedemptions_catalogdesc, dim_SweepStakesRedemptions_PointsPerItem, dim_SweepStakesRedemptions_totval, dim_SweepStakesRedemptions_cashvalue,
			dim_SweepStakesRedemptions_name2, dim_SweepStakesRedemptions_histdate, dim_SweepStakesRedemptions_PointsTotal, 
 			dim_SweepStakesRedemptions_saddress1,dim_SweepStakesRedemptions_saddress2 ,dim_SweepStakesRedemptions_scity ,
			dim_SweepStakesRedemptions_sstate, dim_SweepStakesRedemptions_szip, DIM_SweepStakesRedemptions_Transid,
			dim_SweepStakesRedemptions_DateExtracted, 1 as dim_SweepstakesRedemptions_REDStatus,dim_SweepStakesRedemptions_Entry_id, dim_SweepStakesRedemptions_itemnumber
		FROM .dbo.SweepStakesRedemptions ssr
		LEFT OUTER JOIN SweepStakesRedemptionProcessed ssp
			ON ssr.dim_SweepStakesRedemptions_Transid = ssp.dim_SweepStakesRedemptionProcessed_Transid
		where ssp.dim_SweepStakesRedemptionProcessed_Transid is null
				
	END
	ELSE
	BEGIN
		RAISERROR('ERROR:  Required Table missing: SweepstakesRedemptions or SweepstakesRedemptionsTotal', 16, 1)
	END
END TRY
BEGIN CATCH
	DECLARE @err VARCHAR(MAX) = ERROR_MESSAGE()
	DECLARE @sev INT = ERROR_SEVERITY()
	DECLARE @sta INT = ERROR_STATE()
	EXEC RewardsNow.dbo.spPerlemail_insert 'Error in Sweepstakes Processing', @err, 'cheit@rewardsnow.com, itops@rewardsnow.com', '@itops@rewardsnow.com', 0, null 	
	
	RAISERROR(@err,@sev,@sta)
END CATCH



GO


