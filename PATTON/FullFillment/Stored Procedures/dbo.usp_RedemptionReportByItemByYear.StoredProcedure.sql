use fullfillment
go

if object_id('fullfillment.dbo.usp_RedemptionReportByItemByYear') is not null
    drop procedure dbo.usp_RedemptionReportByItemByYear
GO


create procedure dbo.usp_RedemptionReportByItemByYear
    @Year				   varchar(5),
    @CatalogCode		   varchar(20),
    @trancode			   varchar(5)
AS

--select @year, @catalogcode, @trancode

select itemnumber, itemdesc, yr, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12]
from (select itemnumber, dim_catalogdescription_name itemdesc, yr, mo, isnull(sum(catalogqty), 0) catalogqty 
	   from dbo.vw_main_aggregate_catalogqty_avgpoints_totalredeem vw 
	   where trancode = case
						when @trancode = '<all>' then trancode
						else @trancode
					 end
	   group by itemnumber, dim_catalogdescription_name, yr,  mo) st

pivot
( sum( catalogqty )
  for mo in ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12] )
) as pivottable

where yr = case
			 when @year = '<all>' then yr
			 else @year
		  end
and
    itemnumber = case
				when @catalogcode = '<all>' then itemnumber
				else @catalogcode
			  end

order by itemnumber, yr


/*

exec dbo.usp_RedemptionReportByItemByYear '2010', 'GCR-BORDERS-025', '<all>'

*/