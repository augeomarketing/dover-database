SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Robert McKeagney>
-- Create date: <Dec 18, 2009>
-- Description:	<Generate a list of current definition records in the subcashbackvalue table formatted for reporting purposes>
-- =============================================
CREATE PROCEDURE dbo.usp_subcashbackvalueSelectForRecordReport 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select	left(c.tipfirstpoints, 3) as TipFirst,
		d.clientname as Name,
		right(c.tipfirstpoints, len(c.tipfirstpoints) - 3) as points,
		cashvalue as Value
From fullfillment.dbo.subcashbackvalue c join rewardsnow.dbo.dbprocessinfo d on left(c.tipfirstpoints, 3) = d.dbnumber
Order by left(c.tipfirstpoints, 3), cashvalue

END
GO
