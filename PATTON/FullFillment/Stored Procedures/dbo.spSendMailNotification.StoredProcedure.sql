USE [fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spSendMailNotification]    Script Date: 10/15/2010 14:59:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update The Pearle mail table when The ASB Gift Card File is extracted   */
/* BY:  B.QUINN  */
/* DATE: 3/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/


CREATE PROCEDURE [dbo].[spSendMailNotification]     AS
declare @Errorcount char(1)



	begin
		insert into [Maintenance].[dbo].[PerleMail] 
	(
	 dim_perlemail_subject
	,dim_perlemail_body
	,dim_perlemail_to
	,dim_perlemail_from
	,dim_perlemail_bcc
	)
	values
	(
	  'ASB Gift Cards Extracted'
	 ,'The File is in the ASB directory Ready To Send'
	 ,'bcolbath@rewardsnow.com;brollins@rewardsnow.com;dhayes@rewardsnow.com'
	 ,'bquinn@rewardsnow.com'
	 ,1
	)
	end
GO
