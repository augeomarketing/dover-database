USE [Fullfillment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                                                                            */
/* BQuinn          */
/* DATE: 08/2009   */
/* REVISION: 0 */
/* THIS PROC WILL INSERT INTO THE TABLE GiftCardsInProcess AND UPDATE THE REDSTATUS IN MAIN TO '13' (TO BE SENT)*/
/******************************************************************************/
ALTER PROCEDURE [dbo].[spUpdateGiftCardsREDStatus]  AS
-- This TABLE will be used to update the final status once the transaction has been verified and sent to the FI

	INSERT INTO  .dbo.GiftCardsInProcess( tipnumber, name1, qty, catalogdesc, points, totval, cashvalue, name2, histdate, pointsperitem, saddress1, saddress2, scity, sstate, szip, transid, DateExtracted, REDSTATUS  )
     select VGC.tipnumber, VGC.name1, VGC.qty, VGC.catalogdesc, VGC.points, VGC.totval, VGC.cashvalue, VGC.name2, VGC.histdate, VGC.pointsperitem, VGC.saddress1, VGC.saddress2, VGC.scity, VGC.sstate, VGC.szip, VGC.transid, VGC.DateExtracted, VGC.RedStatus  
     FROM Fullfillment.dbo.VisagftCRDNEW VGC LEFT OUTER JOIN Fullfillment.dbo.GiftCardsInProcess GCP
		ON VGC.transid = GCP.transid
	WHERE GCP.transid IS NULL

--Update the status to '13' (TO BE SENT TO FI) in MAIN and the File Holding the transactions. This is done because the process
-- will run every week but delivery may be only once per Month
 
	Update  m
	set redstatus = '13' 
	FROM Fullfillment.dbo.main m join Fullfillment.dbo.VisagftCRDNEW v
	on m.TransID = v.transid
	where m.RedStatus <> '13'

	Update gcp
	set redstatus = '13' 
	from Fullfillment.dbo.GiftCardsInProcess gcp join Fullfillment.dbo.VisagftCRDNEW v
	on gcp.transid = v.transid
	where gcp.RedStatus <> '13'
