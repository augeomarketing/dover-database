USE [fullfillment]
GO

if object_ID('usp_AugeoGiftCardOrders_Get') is not null
    drop procedure dbo.usp_AugeoGiftCardOrders_Get
GO

/****** Object:  StoredProcedure [dbo].[usp_AugeoGiftCardOrders_Get]    Script Date: 04/10/2015 16:52:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 4.8.2015
-- Description:	Gets all Augeo open gift card orders.
-- =============================================
CREATE PROCEDURE [dbo].[usp_AugeoGiftCardOrders_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT
	'12345' AS ORDER_NUM,
	'1' AS SUB_NUM,
	ItemNumber AS MERCHANT_DENOMINATION_NUM,
	ISNULL(CatalogQty, '') AS QUANTITY,
	ISNULL(Name1, '') AS CUST_WHOLE_NAME,
	RTRIM(ISNULL(Name2, '')) AS SECONDARY_NAME,
	RTRIM(ISNULL(SAddress1, '')) AS ADDRESS_LINE1,
	RTRIM(ISNULL(SAddress2, '')) AS ADDRESS_LINE2,
	ISNULL(SCity, '') AS CUST_CITY,
	ISNULL(SState, '') AS CUST_STATE,
	ISNULL(SZip, '') AS CUST_ZIP,
	'US' AS CUST_COUNTRY,
	TipNumber,
	CONVERT(VARCHAR(255), ISNULL(OrderId, 0)) + '|' + ISNULL(TipNumber, '0') AS CUST_PHONE_NUM,
	ISNULL(SCity, '') AS RECIP_CITY,
	ISNULL(SState, '') AS RECIP_STATE,
	ISNULL(SZip, '') AS RECIP_ZIP,
	'US' AS RECIP_COUNTRY,
	CONVERT(VARCHAR(255), ISNULL(OrderId, 0)) + '|' + ISNULL(TipNumber, '0') AS RECIP_PHONE_NUM,
	'USPOST' AS SHIPPING,
	ISNULL(HistDate, '') AS ORDER_DATE,
	ISNULL(Email, '') AS EMAIL,
	ISNULL(CatalogDesc, '') AS CAT_DESC
    FROM
	dbo.Main
    WHERE
	RedStatus = 0 AND
	TranCode = 'RC' AND
	ItemNumber LIKE '%-AUG-%' AND
	Routing = 3
    ORDER BY
	SAddress1, SCity, SState, SZip, TipNumber

END

    
