USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadInvoiceDetail]    Script Date: 02/03/2014 09:07:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadInvoiceDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadInvoiceDetail]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadInvoiceDetail]    Script Date: 02/03/2014 09:07:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Rich T
-- Create date: 3/15/2011
-- Description:	This code was moved from the DTS 
-- (inline sql ) to this sproc. 
-- Added parameters
-- @RunDate = (do you really need an explaination of this parameter?)
-- @EOM = 0 Calculate Weekly dates
--        1 Calculate End-O-Month dates
-- A view (vw_InvoiceDetail) is used to "rename" the columns for Accounting 
-- NOTE: The end date is assumed to be the first day of the next week/month.
--       For EOM runs use the first day of the next month, not the last day of the reporting month.
-- =============================================
CREATE PROCEDURE [dbo].[usp_LoadInvoiceDetail] @RunDate datetime, @EOM Integer AS 

BEGIN

--Determine previous full week in relation to current day
DECLARE	@startdate datetime
	,	@enddate datetime
	,	@endpart int
	,	@sqlcmd	nvarchar(max)
	,	@dbname nvarchar(100)

Truncate Table InvoiceDetail 

IF DAY(@rundate)> 24
	BEGIN 
	SELECT	@startdate = CAST(CAST(MONTH(@rundate) as varchar(2))+'/17/'+CAST(YEAR(@rundate) as varchar(4)) as date)
	,		@enddate = CAST(CAST(MONTH(@rundate) as varchar(2))+'/25/'+CAST(YEAR(@rundate) as varchar(4)) as date)
	END
ELSE
	IF	DAY(@rundate)> 16
		BEGIN
		SELECT	@startdate = CAST(CAST(MONTH(@rundate) as varchar(2))+'/09/'+CAST(YEAR(@rundate) as varchar(4)) as date)
		,		@enddate = CAST(CAST(MONTH(@rundate) as varchar(2))+'/17/'+CAST(YEAR(@rundate) as varchar(4)) as date)
		END
	ELSE
		IF	DAY(@rundate)> 8
			BEGIN
			SELECT	@startdate = CAST(CAST(MONTH(@rundate) as varchar(2))+'/01/'+CAST(YEAR(@rundate) as varchar(4)) as date)
			,		@enddate = CAST(CAST(MONTH(@rundate) as varchar(2))+'/09/'+CAST(YEAR(@rundate) as varchar(4)) as date)
			END
		ELSE
			BEGIN
			SELECT	@startdate = CAST(CAST(MONTH(DATEADD(month,-1,@rundate)) as varchar(2))+'/25/'+CAST(YEAR(DATEADD(month,-1,@rundate)) as varchar(4)) as date)
			,		@enddate = CAST(DATEADD(DAY,-DAY(@rundate)+1,@rundate) as DATE)
			END

Insert into InvoiceDetail 
SELECT
	Main.TransID as sid_InvoiceDetail_TransID,
	RTRIM(SubClient.ClientName) AS dim_InvoiceDetail_Client,
	RTRIM(Main.Tipnumber) AS dim_InvoiceDetail_TipNumber,
	RTRIM(Main.Name1) AS dim_InvoiceDetail_Name,
	CASE
		--Modified [AGB] 20100305
		WHEN (SELECT TOP 1 ISNULL(dim_loyaltycatalog_bonus,1) FROM catalog.dbo.loyaltycatalog WHERE sid_catalog_id = (select TOP 1 sid_catalog_id from catalog.dbo.catalog where dim_catalog_active = 1 and dim_catalog_code = case when RIGHT(RTRIM(Main.itemnumber),2) = '-B' then LEFT(main.itemnumber, LEN(RTRIM(main.itemnumber)) - 2 ) else RTRIM(main.itemnumber) end ) AND sid_loyalty_id = (SELECT TOP 1 sid_loyalty_id FROM catalog.dbo.loyaltytip where dim_loyaltytip_active = 1 AND dim_loyaltytip_prefix = LEFT(LTRIM(Main.TipNumber), 3))) = 1 THEN 'Bonus'
		--End Modification
		WHEN RIGHT(RTRIM(Main.Itemnumber), 2) = '-B' THEN 'Bonus'
		WHEN UPPER(RTRIM(Main.Trandesc)) in ('GIFT CERT', 'Redeem Cards', 'Redeem eGift Cards') THEN '$' + RIGHT(RTRIM(Main.ItemNumber), 3)
		WHEN UPPER(RTRIM(Main.Trandesc)) = 'ONLINE PROD OR SVC' THEN '$' + RIGHT(RTRIM(Main.ItemNumber), 3)
		WHEN UPPER(RTRIM(Main.Trandesc)) = 'SVC' THEN '$' + RIGHT(RTRIM(Main.ItemNumber), 3)
		ELSE RTRIM(Main.Trandesc)
	END AS dim_InvoiceDetail_Type,
RTRIM(Main.catalogdesc) AS dim_InvoiceDetail_Product,
RTRIM(Main.Catalogqty) AS dim_InvoiceDetail_Qty,
RTRIM(Main.Points*Main.catalogqty) AS dim_InvoiceDetail_Points,
LEFT(RTRIM(Main.histdate), 11) AS dim_InvoiceDetail_Date,
RTRIM(Itemnumber) AS dim_InvoiceDetail_ItemCode,
RTRIM(Source) AS dim_InvoiceDetail_Source,
null,
Main.OrderID AS dim_InvoiceDetail_OrderID
FROM
	main
	LEFT OUTER JOIN rewardsnow.dbo.dbprocessinfo subclient
	ON main.tipfirst = subclient.DBNumber
	WHERE
		histdate >= @startdate
		AND histdate < @enddate
		AND	left(itemnumber,3) <> 'WPH'  --line added to remove Harris-Branded Items from Invoicing file: RBM 04/13/2011 
		AND (main.trandesc NOT IN ('Travel', 'Cash', 'Cash Back', 'Cash Rebate', 'Brochure request')
		OR (Main.trandesc ='travel' )
		OR (Main.trandesc in ('Cash', 'Cash Back', 'Cash Rebate') )
		OR (Main.catalogdesc like '%Carlson%')
		OR trandesc IS NULL
		)
ORDER BY dim_InvoiceDetail_Client, dim_InvoiceDetail_type, dim_InvoiceDetail_product, histdate

--- Update the dim_InvoiceDetail_Type with dollar values 
Update InvoiceDetail 
	set dim_InvoiceDetail_Type =  m.cashvalue  
	from InvoiceDetail i join Main m on i.sid_InvoiceDetail_TransID = m.TransID 
	where m.TranCode in( 'RV','RC','RG' , 'RD') and m.cashvalue <> 0 


------------------------------------
-- FOR ASB ONLY -- add card type to last column
--------------ASB --------------
update InvoiceDetail  
	set dim_InvoiceDetail_CardType = AcctType 
	from InvoiceDetail id join 
	( select TIPNUMBER , min ( LEFT(acctType,1)) as AcctType 
	  from ASB.dbo.AFFILIAT af group by TIPNUMBER ) af 
		on id.dim_InvoiceDetail_TipNumber = af.TIPNUMBER 
	Where dim_InvoiceDetail_CardType is null 

--------------ASB  deleted --------------
update InvoiceDetail  
	set dim_InvoiceDetail_CardType = AcctType 
	from InvoiceDetail id join 
	( select TIPNUMBER , min ( LEFT(acctType,1)) as AcctType 
	  from ASB.dbo.AffiliatDeleted af group by TIPNUMBER ) af 
		on id.dim_InvoiceDetail_TipNumber = af.TIPNUMBER 
	Where dim_InvoiceDetail_CardType is null 

--------------ASB CORP--------------
update InvoiceDetail  
	set dim_InvoiceDetail_CardType = AcctType 
	from InvoiceDetail id join 
	( select TIPNUMBER , min ( LEFT(acctType,1)) as AcctType 
	  from ASBCorp.dbo.AFFILIAT af group by TIPNUMBER ) af 
		on id.dim_InvoiceDetail_TipNumber = af.TIPNUMBER 
	Where dim_InvoiceDetail_CardType is null 

--------------ASB CORP Deleted --------------
update InvoiceDetail  
	set dim_InvoiceDetail_CardType = AcctType 
	from InvoiceDetail id join 
	( select TIPNUMBER , min ( LEFT(acctType,1)) as AcctType 
	  from ASBCorp.dbo.AffiliatDeleted  af group by TIPNUMBER ) af 
		on id.dim_InvoiceDetail_TipNumber = af.TIPNUMBER 
	Where dim_InvoiceDetail_CardType is null 
--------------206 Fidelity Organization--------------
UPDATE	id
SET		id.dim_InvoiceDetail_Client += ': ' + CASE 
												WHEN Misc1 IS NOT NULL THEN CAST(c.Misc1 as VARCHAR(20))
												ELSE CAST(c.City as VARCHAR(20))
											  END	
FROM	InvoiceDetail id INNER JOIN [206Fidelity].dbo.Customer c on id.dim_InvoiceDetail_TipNumber = c.TipNumber
WHERE	id.dim_InvoiceDetail_TipNumber like '206%' AND id.dim_InvoiceDetail_CardType IS NULL
------------------------------------
-- FOR FIS ONLY -- add card type to last column
DECLARE CurDBName CURSOR
FOR		Select dbnamepatton from rewardsnow.dbo.dbprocessinfo
		Where dbnumber like '5%'

OPEN	CurDBName
FETCH NEXT FROM CurDBName INTO @dbname
WHILE @@FETCH_STATUS = 0
	BEGIN
	SET @sqlcmd =	'
		UPDATE	id  
		SET		dim_InvoiceDetail_CardType = af.AcctType 
		FROM	InvoiceDetail id 
			JOIN
				(Select tipnumber, Max(LEFT(accttype, 1)) as AcctType 
				 From [<<DBNAME>>].dbo.AFFILIAT 
				 Where AcctStatus = ''A'' and LEFT(accttype, 1) in (''C'',''D'') 
				 Group By TIPNUMBER) af
				ON	id.dim_InvoiceDetail_TipNumber = af.TIPNUMBER	
		where dim_InvoiceDetail_CardType is null

		UPDATE	id  
		SET		dim_InvoiceDetail_CardType = afd.AcctType 
		FROM	InvoiceDetail id 
			JOIN
				(Select tipnumber, Max(LEFT(accttype, 1)) as AcctType 
				 From [<<DBNAME>>].dbo.AffiliatDeleted 
				 Where AcctStatus = ''A'' and LEFT(accttype, 1) in (''C'',''D'') 
				 Group By TIPNUMBER) afd
				ON	id.dim_InvoiceDetail_TipNumber = afd.TIPNUMBER	
		where dim_InvoiceDetail_CardType is null
					'
					
	SET @sqlcmd = REPLACE(@sqlcmd, '<<DBNAME>>', @dbname)
	--PRINT @sqlcmd
	EXECUTE sp_executesql @sqlcmd			
	FETCH NEXT FROM CurDBName INTO @dbname
	END
		
CLOSE		CurDBName
DEALLOCATE	CurDBName


END

GO


