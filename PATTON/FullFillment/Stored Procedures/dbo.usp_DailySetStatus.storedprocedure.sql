USE [fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[usp_DailySetStatus]    Script Date: 03/25/2011 13:37:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[usp_DailySetStatus]

as

--Update Status Download/CLASS:  
--Set Date of Completion to match Date of Redemption
--Set status on Downloads and CLASS redemptions to Completed
Update	main 
set		redreqfuldate = histdate, redstatus = '5'
where	redstatus = '0'
	and	((trancode = 'RC' and trandesc = 'Online Prod or Svc') or trancode in ('RD', 'RS','RZ','DR','DZ'))

--Add applicable Redemptions into CFR process
UPDATE	a
SET		a.RedStatus = b.dim_cfrtipfirsttrancodemap_initialstatus
FROM	fullfillment.dbo.Main a JOIN RewardsNow.dbo.cfrTipfirstTrancodeMap b 
	ON	a.TipFirst = b.sid_dbprocessinfo_dbnumber AND a.TranCode = b.sid_trantype_trancode
WHERE	a.Routing = '2' 
	AND RedStatus = '0'

--Set status on CASHBACK redemptions to Completed
Update	main 
set		redstatus = '5'
where	redstatus = '0'
	and	trancode = 'RB'

--[AB]
--Set status on Shoplet Gift Card redemptions to Completed
--Set Date of Completion to match Date of Redemption

Update	main 
set		redreqfuldate = histdate, redstatus = '5'
where	redstatus = '0'
	and	LEFT(ItemNumber, 11) = 'GCR-SHOPLET'

UPDATE	Main
SET		RedStatus = '1'
WHERE	RedStatus = '0'
	AND	Source like '%DREAMPOINT%'

--UPDATE	Main
--SET		RedStatus = '1'
--WHERE	Routing = '3'
--	AND	Source like '%DREAMPOINT%'

--[AB]


--Update TCStatus:  
--Replaces sp_updateqtrtravel stored procedure
--Set QTRs to Pending to prevent printing
Update	travelcert 
set		tcstatus = '1' 
where	tcstatus = '0' 
	and	transid in 
		(select transid from main where trancode in ('RT','RU','RV') and (itemnumber = 'TRAVEL-QTR' or catalogdesc like 'QTR%'))

--Set the tcstatus to Void for any travel cert redemption made for 0 points
update	travelcert
set		tcstatus = '4'
from	main m join travelcert t on m.transid = t.transid
where	m.points = 0
	and	t.tcstatus <> '4'

--Set travel certs pending more than 13 months to Expired
update	travelcert
set		tcstatus = '3'
from	main m join travelcert t on m.transid = t.transid
where	m.histdate < cast(floor(cast(DATEADD(month, -13, GETDATE()) as float)) as datetime) 
	and	m.trancode in ('RT','RU') --This line not strictily necessary as already bound by join
	and	t.tcstatus in ('1')

