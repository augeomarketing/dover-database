USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSMetavanteTravelCertificatesSLA_Credit]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSMetavanteTravelCertificatesSLA_Credit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSMetavanteTravelCertificatesSLA_Credit]
			(@ReportStartDate			datetime,
			 @ReportEndDate			datetime,
			 @SLADays					int,
			 @TipsToInclude			nvarchar(4000))

AS

create table #sla_travelcerts (TotalTravelCertsRedeemed	bigint,
						 TravelCertsWithinSLA		bigint,
						 TravelCertsOutsideSLA		bigint)

create table #travelcerts (DaysElapsed		int)


-- Set enddate so it has 23:59:59 for the time
set @reportenddate = dateadd(d, 1, @reportenddate) -- add one day to end date
set @reportenddate = dateadd(s, -1, @reportenddate) -- now back off one second.

-- Create temp table of travel certs days elapsed between redemption date and credit date
insert into #travelcerts
select rewardsnow.dbo.fnCalcBusinessdayselapsed(tcreddate, tccreditdate) as DaysElapsed
from dbo.main rdm join dbo.travelcert tc
	on rdm.transid  = tc.transid

join rewardsnow.dbo.dbprocessinfo db
	on rdm.tipfirst = db.dbnumber
where dbnumber in ( select item from dbo.Split ( @TipsToInclude, ',') )
and tcreddate between @ReportStartDate and @ReportEndDate
and tcstatus = 2  -- 2 = returned

-- Populate temp table with counts - total certs returned, count within sla days, count outside sla days
insert into #sla_travelcerts
select (select count(*) from #travelcerts) as TotalTravelCertsRedeemed,
	  (select count(*) from #travelcerts where dayselapsed <= @SLADays) as TravelCertsWithinSLA,
	  (select count(*) from #travelcerts where dayselapsed > @SLADays) as travelCertsOutsideSLA

select TotaltravelCertsRedeemed, TravelCertsWithinSLA, TraveLCertsOutsideSLA
from #sla_travelcerts
GO
