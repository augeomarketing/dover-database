USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSPik_n_Clik_RedemptionForCommissions]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSPik_n_Clik_RedemptionForCommissions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSPik_n_Clik_RedemptionForCommissions]
	(@StartDate				datetime,
	 @EndDate					datetime,
	 @FiNamesToInclude			varchar(1000) = null)

as

declare @SqlStartDate	nvarchar(10)
declare @sqlEndDate		nvarchar(10)

set @SqlStartDate = cast( datepart(year, @StartDate) as nvarchar(4)) + '/' +
				cast( datepart(month, @StartDate) as nvarchar(2)) + '/' +
				cast( datepart(day, @StartDate) as nvarchar(2))

set @SqlEndDate =	cast( datepart(year, @EndDate) as nvarchar(4)) + '/' +
				cast( datepart(month, @EndDate) as nvarchar(2)) + '/' +
				cast( datepart(day, @EndDate) as nvarchar(2))

Select left(ltrim(rtrim(Submitdate)), 11) as RedemptionDate,
	ltrim(rtrim(ClientCode)) as FIName,
	ltrim(rtrim(Name)) as CustomerName,
	ltrim(rtrim(Address1)) as AddressLine1,
	ltrim(rtrim(Address2)) as AddressLine2,
	ltrim(rtrim(City)) as City,
	ltrim(rtrim(State)) as State,
	ltrim(rtrim(Zip)) as ZipCode,
	ltrim(rtrim(EmpName)) as EmployeeName,
	ltrim(rtrim(sel.catalogcode)) as CatalogCode,
	ltrim(rtrim(ItemName)) as ItemName,
	1 as Quantity,	
	isnull(ItemCost, 0.00) as ItemCost, 
	isnull(EstimatedShippingCost, 0.00) as estimatedShippingCost, 
	isnull(SellingPrice, 0.00) as SellingPrice

from dbo.Selection sel left outer join dbo.CatalogItemCosts cic
	on ltrim(rtrim(sel.catalogcode)) = cic.catalogcode

where submitdate between  @StartDate  and  @EndDate
	and ltrim(rtrim(clientcode)) in ( select item from dbo.Split ( @FiNamesToInclude, ',') )

order by ClientCode, itemname, submitdate
GO
