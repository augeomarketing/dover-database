USE Fullfillment
GO

IF OBJECT_ID(N'usp_UpdateGiftCardSentStatusByTipfirst_SentToVendor') IS NOT NULL
	DROP PROCEDURE usp_UpdateGiftCardSentStatusByTipfirst_SentToVendor
GO

CREATE PROCEDURE usp_UpdateGiftCardSentStatusByTipfirst_SentToVendor
	@tipfirst VARCHAR(3)
	, @debug BIT = 0
AS
BEGIN
	BEGIN TRY
		DECLARE @newStatus TINYINT
		
		SELECT @newStatus = RedStatus 
		FROM SubRedemptionStatus 
		WHERE RewardsNow.dbo.ufn_Trim(UPPER(RedStatusName)) = 'SENT TO VENDOR'
		--Need to use Trim because someone in their infinite wisdom thought a fixed width char(30) field was a good idea...
		
		IF @debug = 1
		BEGIN
			RAISERROR('Processing TIP: %s.  RedStatusID: %d',0,0,@tipfirst, @newStatus) WITH NOWAIT
		END
	
		UPDATE m
		SET 
			RedStatus = @newStatus
			, RedReqFulDate = getdate()
		FROM
			Main m
		INNER JOIN
			VisagftCRDNEW v
		ON
			m.TransID = v.TransID
		WHERE
			UPPER(LEFT(m.TipNumber, 3)) = UPPER(@tipfirst) 
	END TRY
	BEGIN CATCH
		DECLARE
			@errMsg NVARCHAR(2048) = ERROR_MESSAGE()
			, @errNum INT = ERROR_NUMBER()
			, @errSeverity INT = ERROR_SEVERITY()
			, @errState INT = ERROR_STATE()
			, @errLine INT = ERROR_LINE()
			, @errProc NVARCHAR(200) = ISNULL(ERROR_PROCEDURE(), '-');
			
		RAISERROR('Error %d, Level %d, State %d Procedure %s, Line %s, Message: %s'
			, @errSeverity
			, 1
			, @errNum
			, @errSeverity
			, @errState
			, @errProc
			, @errLine
		);
	END CATCH
END

	
GO

