USE [Fullfillment]
GO

IF OBJECT_ID(N'spExtractVisaGiftCardsNEW') IS NOT NULL
BEGIN
	DROP PROCEDURE spExtractVisaGiftCardsNEW
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spExtractVisaGiftCardsNEW]  AS

BEGIN

	DECLARE 
		@SQLSet NVARCHAR(4000)
		, @SQLInsert NVARCHAR(4000)
		, @SQLUpdate NVARCHAR(4000)
		, @SQLSelect NVARCHAR(4000)
		, @SQLDelete NVARCHAR(4000)
		, @SQLIf NVARCHAR(4000)

	DECLARE @DateToday DATETIME

	DECLARE @Datework NVARCHAR(19)

	SET @DateToday = GETDATE()
	SET @Datework = LEFT(@DateToday,11)
	SET @DateToday = @Datework


--  Truncate work tables

	IF OBJECT_ID(N'Fullfillment.dbo.TipsToExtract') IS NOT NULL
	BEGIN
		TRUNCATE TABLE Fullfillment.dbo.TipsToExtract 
	END 

	IF OBJECT_ID(N'Fullfillment.dbo.VisagftCRDNEW') IS NOT NULL
	BEGIN
		TRUNCATE TABLE Fullfillment.dbo.VisagftCRDNEW 
	END 

	IF OBJECT_ID(N'Fullfillment.dbo.VisagftCRDtotal') IS NOT NULL
	BEGIN
		TRUNCATE TABLE Fullfillment.dbo.VisagftCRDtotal 
	END


-- Insert new Gift Card Requests RedStatus = '0' Routing Code of '2' and not in CFR

	INSERT INTO  Fullfillment.dbo.VisagftCRDNEW
	(
		tipnumber
		, name1
		, qty
		, catalogdesc
		, points
		, totval
		, cashvalue
		, name2
		, histdate
		, pointsperitem
		, saddress1
		, saddress2
		, scity
		, sstate
		, szip
		, transid
		, DateExtracted
		, RedStatus
		, ItemNumber
	)
	SELECT  
		mn.tipnumber
		, mn.name1
		, catalogqty AS qty
		, LEFT(mn.catalogdesc,50) AS catalogdesc
		, mn.points
		, (mn.cashvalue*mn.catalogqty) AS totval 
		, mn.cashvalue
		, mn.name2
		, mn.histdate
		, (mn.points * mn.catalogqty) AS pointsperitem  
		, mn.saddress1
		, mn.saddress2
		, mn.scity
		, mn.sstate
		, mn.szip
		, mn.transid
		, @DateToday
		, mn.RedStatus
		, ItemNumber 
	FROM  Fullfillment.dbo.MAIN mn
	LEFT OUTER JOIN RewardsNow.dbo.cfrTipfirstTrancodeMap tcm
		ON mn.TipFirst = tcm.sid_dbprocessinfo_dbnumber
			AND mn.TranCode = tcm.sid_trantype_trancode
	LEFT OUTER JOIN Fullfillment.dbo.GiftCardsInProcess gcp
		ON mn.TransID = gcp.transid
	WHERE  
		mn.redstatus = '0' 
		AND mn.Routing = '2' 
		AND mn.trancode = 'RC' 
		AND mn.points <> '0' 
		AND mn.histdate < @DateToday 
		AND gcp.TRANSID IS NULL
		AND tcm.sid_dbprocessinfo_dbnumber IS NULL
	ORDER BY mn.histdate ASC

-- Group Totals By TipFirst for presentation to Finance OnLine

	INSERT INTO  Fullfillment.dbo.VisagftCRDtotal
	(
		tipfirst
		, totrecords
		, QTYCards
		, TotalPoints
		, totalvalue
		, totcashvalue
		, TotalItemPoints
	)
	SELECT 
		LEFT(tipnumber,3) AS tipfirst
		, COUNT(Tipnumber) AS totrecords
		, SUM(QTY) AS QTYCards
		, SUM(points) AS TotalPoints
		, SUM(totval) AS totalvalue
		, SUM(cashvalue* qty) AS totcashvalue
		, SUM(points * qty) AS TotalItemPoints 
	FROM VisagftCRDNEW
	GROUP BY 
		LEFT(tipnumber,3)
	ORDER BY 
		LEFT(tipnumber,3)

-- Collect Clients That Have No Transactions This Week

	INSERT INTO  Fullfillment.dbo.TipsToExtract
	(
		dbnumber
		,clientname
	) 
	SELECT 
		dbpi.DBNumber
		, dbpi.clientname
	FROM rewardsnow.dbo.dbprocessinfo dbpi
	LEFT OUTER JOIN Fullfillment.dbo.VisagftCRDtotal gct
		ON dbpi.DBNumber = gct.tipfirst
	WHERE 
		extractgiftcards = 'y'
		AND gct.tipfirst IS NULL

-- Insert Into The Totals Table Entries for FI's Without Transactions
-- This will allow for the Fi's Tip to Appear in The Display For Finance

	INSERT INTO Fullfillment.dbo.VisagftCRDtotal
	(
		tipfirst
		, totrecords
		, QTYCards
		, TotalPoints
		, totalvalue
		, totcashvalue
		, TotalItemPoints
	)
	SELECT 
		dbnumber AS tipfirst
		, '0' AS totrecords
		, '0' AS QTYCards
		, '0' AS TotalPoints
		, '0' AS totalvalue
		, '0' AS totcashvalue
		, '0' AS TotalItemPoints 
	FROM TipsToExtract tte
	LEFT OUTER JOIN VisagftCRDtotal gct
		ON tte.DBNumber = gct.tipfirst
	WHERE
		gct.tipfirst IS NULL

		 
-- Set The DateExtracted To The Current Date/Time 
	UPDATE Fullfillment.dbo.VisagftCRDtotal
	SET Dateextracted = @DateToday		 

END


GO


