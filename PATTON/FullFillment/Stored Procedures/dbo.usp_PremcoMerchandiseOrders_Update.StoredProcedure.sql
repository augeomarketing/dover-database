USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_PremcoMerchandiseOrders_Update]    Script Date: 6/5/2015 4:17:01 PM ******/
if object_ID('usp_PremcoMerchandiseOrders_Update') is not null
    drop procedure dbo.usp_PremcoMerchandiseOrders_Update
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 6.3.2015
-- Description:	Updates Premcro merchandise main records.
--
-- Example of XML passed:
-- <Main>
--    <Row orderid="4432" redstatus="17" redreqfuldate="12/25/2015" />
--    <Row orderid="5534" redstatus="17" redreqfuldate="1/25/2012" />
-- </Main>
--
-- =============================================
CREATE PROCEDURE [dbo].[usp_PremcoMerchandiseOrders_Update] 
	-- Add the parameters for the stored procedure here
	@MainTableUpdates XML
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #MainUpdateTable
    (
		OrderID BIGINT,
		RedReqFulDate SMALLDATETIME,
		RedStatus TINYINT
    )
   
	INSERT INTO #MainUpdateTable
	(
		OrderID,
		RedReqFulDate,
		RedStatus
	)
	SELECT
		Main.n.value('@orderid', 'bigint') AS OrderID, 
		Main.n.value('@redreqfuldate', 'smalldatetime') AS RedReqFulDate,
		Main.n.value('@redstatus', 'tinyint') AS RedStatus
	FROM
		@MainTableUpdates.nodes('/Main/Row') as Main(n)
		

	UPDATE M SET M.RedReqFulDate = F.RedReqFulDate, M.RedStatus = F.RedStatus
	FROM dbo.Main AS M 
	INNER JOIN #MainUpdateTable AS F ON M.OrderID = F.OrderID

	-- Check to make sure all records from original get SPR were updated
	SELECT
		T.OrderID AS FileOrderID,
		M.OrderID
	FROM
		#MainUpdateTable AS T
	LEFT OUTER JOIN 
		dbo.Main AS M ON T.OrderID = M.OrderID
	WHERE 
		TranCode = 'RM' AND
		ItemNumber LIKE 'PRE-%' AND 
		M.RedStatus = 0
END

GO



