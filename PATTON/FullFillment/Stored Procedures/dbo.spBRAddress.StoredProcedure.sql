USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spBRAddress]    Script Date: 08/18/2009 08:54:39 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spBRAddress]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spBRAddress]
GO

USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spBRAddress]    Script Date: 08/18/2009 08:54:39 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE procedure [dbo].[spBRAddress]
as 

--This stored procedure runs through all current FIs as determined by the DBnamePatton field in DBProcessinfo and adds address information in to all Brochure Requests in Fulfillment.
--It will only add address info to a Brochure Request record that does not already contain address info, and only if the information exists in the individual customer record in the correct format.
--Script was developed and implemented by Rob McKeagney on 10/22/2007.

Declare curDBName cursor FAST_FORWARD for
    Select dbnamePatton 
    from rewardsnow.dbo.dbprocessinfo
    where sid_fiProdStatus_statuscode != 'X'  -- Don't do dbs that have been deleted

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Update [Fullfillment].dbo.Main

		 set	[Fullfillment].dbo.Main.saddress1 = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.address1))),
			[Fullfillment].dbo.Main.saddress2 = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.address2))),
			[Fullfillment].dbo.Main.scity = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.city))),
			[Fullfillment].dbo.Main.sstate = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.state))),
			[Fullfillment].dbo.Main.szip = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.zipcode)))

		From	['+@DBName+'].dbo.customer

		 where	[Fullfillment].dbo.Main.tipnumber = ['+@DBName+'].dbo.customer.tipnumber
			and([Fullfillment].dbo.Main.redstatus not in (2,3,4,5,6,7,8))
		    and([Fullfillment].dbo.Main.saddress1 is null or [Fullfillment].dbo.Main.saddress1 = '''')
		    and([Fullfillment].dbo.Main.saddress2 is null or [Fullfillment].dbo.Main.saddress2 = '''')
		    and([Fullfillment].dbo.Main.scity is null or [Fullfillment].dbo.Main.scity = '''')
		    and([Fullfillment].dbo.Main.sstate is null or [Fullfillment].dbo.Main.sstate = '''')
		    and([Fullfillment].dbo.Main.szip is null or [Fullfillment].dbo.Main.szip = '''')'
--Print @sqlcmd
  	  EXECUTE sp_executesql @SqlCmd

	  set @sqlcmd =	
		'Update [Fullfillment].dbo.Main

		 set	[Fullfillment].dbo.Main.saddress4 = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.address4))),
			[Fullfillment].dbo.Main.scity = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.city))),
			[Fullfillment].dbo.Main.sstate = Upper(LTRIM(RTRIM(['+@DBName+'].dbo.customer.state)))

		From	['+@DBName+'].dbo.customer
			INNER JOIN [Fullfillment].dbo.Main
				ON [Fullfillment].dbo.Main.tipnumber = ['+@DBName+'].dbo.customer.tipnumber
					AND [Fullfillment].dbo.Main.szip = ['+@DBName+'].dbo.customer.zipcode
		 where	([Fullfillment].dbo.Main.redstatus not in (2,3,4,5,6,7,8))
		    and(([Fullfillment].dbo.Main.scity is null or [Fullfillment].dbo.Main.scity = '''')
		    or ([Fullfillment].dbo.Main.sstate is null or [Fullfillment].dbo.Main.sstate = ''''))'
--Print @sqlcmd
  	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName
GO


