USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_errors]    Script Date: 12/17/2010 14:54:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_errors]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_errors]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_errors]    Script Date: 12/17/2010 14:54:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20101217
-- =============================================
CREATE PROCEDURE [dbo].[usp_tc_errors] 
 
AS
BEGIN


	select m.tipnumber, name1, HistDate, Points, Catalogdesc, cashvalue, TCLastSix, coalesce(a.acctid, ac.acctid) as Account, reason
	from tc_error e
	join main m on 
	E.transid = m.TransID
	join TravelCert t
	on E.transid = t.TransID
	left outer join asb.dbo.AFFILIAT a
	on m.TipNumber = a.TIPNUMBER
	left outer join ASBCorp.dbo.AFFILIAT ac
	on m.TipNumber = ac.tipnumber
	where LTRIM(rtrim(TCLastSix)) = coalesce(right(rtrim(a.ACCTID), len(TCLastSix)), right(rtrim(ac.ACCTID), len(TCLastSix)))
	order by m.transid, HistDate desc

END 
GO

