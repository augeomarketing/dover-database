USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateSweepStakesREDStatus]    Script Date: 12/19/2011 10:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spUpdateSweepStakesREDStatus]  AS


--Update the status to '01' (SENT TO VENDOR) in MAIN and the File Holding the transactions. This is done because the process
-- will run every week but delivery may be only once per Month

	Update  m
	set redstatus = '1', REDREQFULDATE = GETDATE() 
	from Fullfillment.dbo.main m join Fullfillment.dbo.SweepStakesRedemptions s
	on m.TransID = s.dim_SweepStakesRedemptions_Transid
	and RedStatus = '0'
 

