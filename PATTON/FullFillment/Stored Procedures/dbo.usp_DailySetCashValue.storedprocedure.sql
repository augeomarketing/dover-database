USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_DailySetCashValue]    Script Date: 01/10/2011 09:44:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DailySetCashValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DailySetCashValue]
GO

USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_DailySetCashValue]    Script Date: 01/10/2011 09:44:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[usp_DailySetCashValue]

AS

--------------------------------------------------------------------------------------------------------------------------------
--Set the cashvalue for CashBack redemptions
--------------------------------------------------------------------------------------------------------------------------------
update main
set main.cashvalue = subcashbackvalue.cashvalue
from dbo.main join dbo.subcashbackvalue
 on convert(varchar(3), tipfirst) + convert(Varchar(8), points) = tipfirstpoints
where trancode = 'RB' and main.cashvalue = 0.00 and itemnumber not like '%CLASS%'
--------------------------------------------------------------------------------------------------------------------------------
--Set the cashvalue for Gift Card redemptions
--------------------------------------------------------------------------------------------------------------------------------
update main
set main.cashvalue = subgccashvalue.cashvalue
from dbo.main join dbo.subgccashvalue
 on right(rtrim(itemnumber), 3) = gccv
where trancode in ('RC','RD') and main.cashvalue = 0.00

update main
set main.cashvalue = subgccashvalue.cashvalue
from dbo.main join dbo.subgccashvalue
 on right(rtrim(itemnumber), 5) = gccv
where trancode in ('RC','RD') and main.cashvalue = 0.00
--------------------------------------------------------------------------------------------------------------------------------
--Set the cashvalue for Travel redemptions
--------------------------------------------------------------------------------------------------------------------------------

--CARLSON LIMITER COMMENTED OUT AS PROGRAM NOT ACTIVE SINCE 2010


--Golden Years Travel Rebates
UPDATE	main
SET		cashvalue = (points * 0.7) /100
WHERE	trancode = 'RU' and tipfirst in ('511','512')/* and catalogdesc not like '%Carlson%'*/ and cashvalue = 0.00

UPDATE	main
SET		cashvalue = points/200
WHERE	trancode in ('RT','RU','RV') and tipfirst in ('161','248','617','650')/* and catalogdesc not like '%Carlson%'*/ and cashvalue = 0.00

UPDATE	Main
SET		cashvalue = Points/180
WHERE	TranCode in ('RT','RU','RV') and tipfirst in ('603')/* and catalogdesc not like '%Carlson%'*/ and cashvalue = 0.00

UPDATE	main
SET		cashvalue = points/100
WHERE	trancode in ('RT','RU','RV') and tipfirst not in ('161','248','603','617','650')/* and catalogdesc not like '%Carlson%'*/ and cashvalue = 0.00


/*
Update main
set cashvalue = points/140
where trancode in ('RT','RU','RV') and tipfirst in ('161') and catalogdesc not like '%Carlson%' and cashvalue = 0.00
*/

--------------------------------------------------------------------------------------------------------------------------------
--These redemption types are not currently having a cashvalue set in Fullfillment
--RD	Redeem Downloaded Items
--RF	Redeem for Fee
--RG	Redeem Giving
--RK	Redeem Coupon
--RM	Redeem Merchandise
--RZ	CLASS FI FULLFILLED REDEMPTION
--------------------------------------------------------------------------------------------------------------------------------

GO


