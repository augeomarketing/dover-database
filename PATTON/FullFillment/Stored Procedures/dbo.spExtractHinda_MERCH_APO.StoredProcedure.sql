USE [fullfillment]
GO

if object_id('spExtractHinda_MERCH_APO') is not null
    drop procedure dbo.spExtractHinda_MERCH_APO
GO


/****** Object:  StoredProcedure [dbo].[spExtractHinda_MERCH_APO]    Script Date: 08/09/2010 11:52:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                                                                            */
/* BQuinn          */
/* DATE: 08/2010   */
/* REVISION: 0 */
/* THIS PROC WILL INSERT INTO THE TABLE Hinda_Temp MERCH for APO delivery */
/******************************************************************************/
create PROCEDURE [dbo].[spExtractHinda_MERCH_APO]  AS

declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)
DECLARE @DateToday DATETIME

 		set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''Hinda_Temp'')
		Begin
			TRUNCATE TABLE .dbo.Hinda_Temp 
		End '
		exec sp_executesql @SQLIf



insert into  fullfillment.dbo.Hinda_Temp
Select
'R' + cast(OrderID as varchar(15))	as ClientOrderNumber,
rtrim(Name1)		as ShiptoFirstName,
rtrim(Saddress1)	as ShiptoAddressLine1,
rtrim(Saddress2)	as ShiptoAddressLine2,
rtrim(Scity)		as ShiptoCity,
rtrim(Sstate)		as ShiptoState,
rtrim(Szip)		as ShiptoPostalCode,
--rtrim(Scountry)	as ShiptoCountry,
rtrim(Phone1)		as ShiptoPhone,
rtrim(email)		as ShiptoEmail,
Replace(rtrim(Itemnumber), 'HIN-', '')	as Catalognumber,
rtrim(Catalogqty)	as Quantity,
Histdate		as [TimeStamp]
From fullfillment.dbo.Main
where	itemnumber like 'HIN%' 
and	redstatus = '0'
and	scity =  'APO'
and tipfirst = '231'
GO
