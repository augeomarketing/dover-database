USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_ClearTempdata]    Script Date: 12/17/2010 13:56:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_ClearTempdata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_ClearTempdata]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_ClearTempdata]    Script Date: 12/17/2010 13:56:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Bob Lawliss>
-- Create date: <3/15/10>
-- =============================================
CREATE PROCEDURE [dbo].[usp_tc_ClearTempdata] 
 
AS
BEGIN

	truncate table [fullfillment].dbo.tc_CREDIT_Hdr
	truncate table [fullfillment].dbo.tc_CREDIT_OUT
	truncate table [fullfillment].dbo.tc_CREDIT_OUT_FINAL
	truncate table [fullfillment].dbo.tc_CREDIT_trailer
	truncate table [fullfillment].dbo.tc_Credit_wrk
	truncate table [fullfillment].dbo.tc_DEBIT_OUT
	truncate table [fullfillment].dbo.tc_Debit_wrk
	truncate table [fullfillment].dbo.tc_FileSeqNumbers
	truncate table [fullfillment].dbo.tc_error
END


GO

