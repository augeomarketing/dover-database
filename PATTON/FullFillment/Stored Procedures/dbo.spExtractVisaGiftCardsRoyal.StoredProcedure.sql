/****** Object:  StoredProcedure [dbo].[spExtractVisaGiftCardsRoyal]    Script Date: 03/12/2009 09:50:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                                                                            */
/******************************************************************************/
/*    This  Process will Extract the GiftCard Transactions for Compass Bank   */
/* This Process will be re written in TSQL at some point in the not to distant future  */
/*    and stored in the RewardsNow database */
/* BQuinn          */
/* DATE: 04/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spExtractVisaGiftCardsRoyal]  @TipFirst char(3) AS

--declare @TipFirst char(3)
--set @TipFirst = '596'
declare @dtest nvarchar(11)
declare @dteend nvarchar(11)


/* Drop The Work Table */
drop table RoyalgftCRDwk
/* Set the Date Parameter  */
set @dteend = getdate()
--print @dteend
set @dtest = convert(nvarchar(25),(Dateadd(day, -7, @dteend)),121)
--print @dtest
select	 tipnumber,name1,catalogqty as qty,left(catalogdesc,30) as catalogdesc,points ,totval    = (cast( SubString(Catalogdesc, patindex ('%$%', catalogdesc)+1,3) as int )* catalogqty),name2,
 histdate,(points * catalogqty) as pointsperitem,saddress1,saddress2,scity,sstate,szip
into RoyalgftCRDwk 
from main where  left(tipfirst,2) = left(@TipFirst,2) and  left(itemnumber,7) =  'GCR-RCU' and histdate >= @dtest and histdate < @dteend and points <> '0' order by histdate asc
GO
