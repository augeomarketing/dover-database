USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSMetavanteRedemptions]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSMetavanteRedemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSMetavanteRedemptions]
	(@ReportStartDate		datetime,
	 @ReportEndDate		datetime,
	 @SLADays				int,
	 @TipsToInclude		nvarchar(4000))

as
declare @Today			datetime

-- Set enddate so it has 23:59:59 for the time
set @reportenddate = dateadd(d, 1, @reportenddate) -- add one day to end date
set @reportenddate = dateadd(s, -1, @reportenddate) -- now back off one second.

set @Today = getdate()

-- Build temp table of TIPFirsts for all of Metavante
create table #metavante_tips (dbnumber nchar(3) primary key)

insert into #metavante_tips
select dbnumber
from rewardsnow.dbo.dbprocessinfo
where dbnumber in ( select item from dbo.Split ( @TipsToInclude, ',') )



-- Build temp table of redemption detail
create table #redemptions
	(sid				bigint identity(1,1) primary key,
	 TranDesc			varchar(100),
	 TranCode			varchar(2),
	 RedStatus		tinyint,
	 RedStatusName		varchar(30),
	 HistDate			smalldatetime,
	 RedReqFulDate		smalldatetime null)

create index ix_tmp_redemptions on #redemptions (trandesc, trancode, redstatus, redstatusname, histdate, redreqfuldate)

insert into #redemptions
(trandesc, trancode, redstatus, redstatusname, histdate, redreqfuldate)
select trandesc, trancode, ful.redstatus, RedStatusName, histdate, redreqfuldate
from fullfillment.dbo.[main] ful join #metavante_tips tips
	on ful.tipfirst = tips.dbnumber

join dbo.SubRedemptionStatus sts
	on ful.redstatus = sts.redstatus

where	histdate between @ReportStartDate and @ReportEndDate
	and  trancode not in ('RQ', 'DE', 'DR') -- don't include brochures, decrease earned or decreased redeemed
	and trandesc not like 'College%'

-- Delete gift certs that do not have a recorded redemption date.  these will be accounted for in a different process/sproc
--delete from #redemptions
--where trandesc = 'Gift Cert' and redreqfuldate is null

-- PHB 2009-09-17
-- If REDREQFULDATE is NULL for Gift Certs, put in the current date for purposes of
-- Calculating the SLA only. 
--
update #redemptions
	set redreqfuldate = getdate()
where trandesc = 'Gift Cert' and redreqfuldate is null



-- Create summary table used for output
create table #SLA
	(RedStatusName				varchar(30),
	 TranDesc					varchar(100),
	 Redemptions_Total			bigint DEFAULT 0,
	 Redemptions_InsideSLA		bigint DEFAULT 0,
	 Redemptions_OutsideSLA		bigint DEFAULT 0)

create index ix_tmpSLA on #SLA (TranDesc, RedStatusName)

---------------------------------------------------------
--Load temp table with total redemptions
---------------------------------------------------------
insert into #SLA
(RedStatusName, TranDesc, Redemptions_Total)
select RedStatusName, case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end as trandesc, count(*) as NbrRedemptions
from #redemptions  
group by case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end , RedStatusName

order by case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end , RedStatusName

---------------------------------------------------------
-- Update temp table with redemptions within specified SLA
---------------------------------------------------------
select RedStatusName, case
					when trandesc like 'Conf%' then 'Travel'
					when trandesc like 'Merch%' then 'Merchandise'
					else trandesc
				  end as trandesc, count(*) as NbrRedemptions
into #SLA_Within
from #redemptions 
where rewardsnow.dbo.fnCalcBusinessDaysElapsed(histdate, isnull(RedReqFuldate, @Today)) <= @SLADays

group by case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end , RedStatusName

order by case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end , RedStatusName


-- Now update the temp table with the totals
update sla
	set Redemptions_InsideSLA = tmp.NbrRedemptions
from #SLA sla join #SLA_Within tmp
	on sla.redstatusname = tmp.redstatusname
	and case
			when sla.trandesc like 'Conf%' then 'Travel'
			when sla.trandesc like 'Merch%' then 'Merchandise'
			else sla.trandesc 
		end = tmp.trandesc

---------------------------------------------------------
-- Update temp table with redemptions outside specified SLA
---------------------------------------------------------
select RedStatusName, case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end as trandesc, count(*) as NbrRedemptions
into #SLA_Outside
from #redemptions 

where	rewardsnow.dbo.fnCalcBusinessDaysElapsed(histdate, isnull(RedReqFuldate, @Today)) > @SLADays

group by case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end , RedStatusName

order by case
			when trandesc like 'Conf%' then 'Travel'
			when trandesc like 'Merch%' then 'Merchandise'
			else trandesc 
		end , RedStatusName

-- Now update the temp table with the totals
update sla
	set Redemptions_OutsideSLA = tmp.NbrRedemptions
from #SLA sla join #SLA_Outside tmp
	on sla.redstatusname = tmp.redstatusname
	and case
			when sla.trandesc like 'Conf%' then 'Travel'
			when sla.trandesc like 'Merch%' then 'Merchandise'
			else sla.trandesc 
		end  = tmp.trandesc

-----
-- KLUGE TO Recalc gift certs & online
-----
/*
update #sla
	set Redemptions_InsideSLA = Redemptions_InsideSLA + Redemptions_OutsideSLA
where 
--trandesc like 'gift%' or
--trandesc like 'online%' or
--trandesc like 'travel%'

update #sla
	set Redemptions_OutsideSLA = 0
where 
--trandesc like 'gift%' or
--trandesc like 'online%' or
--trandesc like 'travel%'
*/

-------
-- Set redemptions inside SLA time window to 0 for all those with a status of PROCESSING
-- or On Hold, or On Order
-------
/*
update #sla
	set Redemptions_Total = Redemptions_Total - Redemptions_InsideSLA
where RedStatusName = 'Processing'
or    RedStatusName like 'On_Order%'
or    RedStatusName like 'On_Hold%'
or    RedStatusName like 'Backordered%'

update #sla
	set Redemptions_InsideSLA = 0
where RedStatusName = 'Processing'
or    RedStatusName like 'On_Order%'
or    RedStatusName like 'On_Hold%'
or    RedStatusName like 'Backordered%'
*/

--------
-- Now return the results
--------
select RedStatusName, TranDesc, Redemptions_Total, Redemptions_InsideSLA, Redemptions_OutsideSLA
from #SLA
order by trandesc, redstatusname
GO
