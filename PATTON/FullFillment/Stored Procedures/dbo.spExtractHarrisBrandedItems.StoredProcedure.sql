USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spExtractHarrisBrandedItems]    Script Date: 03/24/2011 14:05:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spExtractHarrisBrandedItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spExtractHarrisBrandedItems]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spExtractHarrisBrandedItems]    Script Date: 03/24/2011 14:05:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*                                                                            */
/******************************************************************************/
/*    This  Process will Extract the GiftCard Transactions for Compass Bank   */
/* This Process will be re written in TSQL at some point in the not to distant future  */
/*    and stored in the RewardsNow database */
/* BQuinn          */
/* DATE: 04/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/

--Revised process to pull redemptions for other Harris FIs (51J, 51K) RBM 3/25/11

CREATE PROCEDURE [dbo].[spExtractHarrisBrandedItems]  
--@TipFirst char(3) 
AS
		DECLARE @DateToday DATETIME

--declare	@SQLSet nvarchar(4000),		@SQLInsert nvarchar(4000),	@SQLUpdate nvarchar(4000),
--			@SQLSelect nvarchar(4000),	@SQLDelete nvarchar(4000),	@SQLIf nvarchar(4000)
--declare	@dtest nvarchar(11)
--declare	@dteend nvarchar(11)
--declare	@TipFirst char(3)
--set		@TipFirst = '594'

--  Truncate work tables (removed dynamic code - RBM 3/25/11)

 		if exists(select * from  .dbo.sysobjects where xtype='u' and name = 'ZZZITEMwk')
		Begin
			TRUNCATE TABLE [fullfillment].[dbo].[ZZZITEMwk] 
		end
		Else
		Begin
			CREATE TABLE	[fullfillment].[dbo].[ZZZITEMwk](
							[tipnumber] [char](15) NULL, [name1] [varchar](50) NULL, [qty] [int] NULL, [catalogdesc] [varchar](30) NULL,
							[points] [int] NULL, [totval] [int] NULL, [name2] [varchar](50) NULL, [histdate] [smalldatetime] NULL,
							[totpoints] [int] NULL, [saddress1] [varchar](50) NULL, [saddress2] [varchar](50) NULL, [scity] [varchar](50) NULL,
							[sstate] [varchar](5) NULL, [szip] [varchar](15) NULL, [transid] [nvarchar](40) NULL, [dateextracted] [datetime] NULL,
							[redstatus] [nvarchar](2) NULL, [routing] [nvarchar](2) NULL, [itemnumber] [nvarchar](20) NULL
			) ON [PRIMARY]	
		End 


/* Set the Date Parameter  */
		set	@DateToday = getdate()
--print @DateToday
--set @DateToday = convert(nvarchar(25),(Dateadd(day, -14, @DateToday)),121)
--print @dtest
--print '@DateToday'
--print @DateToday

		INSERT INTO  [fullfillment].[dbo].[ZZZITEMwk]
			(
			tipnumber,name1, qty,catalogdesc, points,
       		totval,name2,histdate,totpoints, saddress1, saddress2, scity, sstate, szip,
			transid, DateExtracted, RedStatus, ItemNumber, routing
			)
			select  tipnumber,name1,catalogqty as qty,left(catalogdesc,30) as catalogdesc, points,
  					'0',name2, histdate, (points * catalogqty) , saddress1, saddress2, scity, sstate, szip,
					transid, @DateToday, RedStatus, ItemNumber, routing
			from	[fullfillment].[dbo].[MAIN] 
			where	points <> '0' and Routing = '3' and left(itemnumber,3) = 'WPH' and  redstatus = '0'
					and TipFirst in ('51J','51K','594')
		
		
--Update RedStatus in Main (removed dynamic code - RBM 3/25/11)

	   	Update  .dbo.main set redstatus = '13' where TRANSID in (select TRANSID from .dbo.ZZZITEMwk)



GO


