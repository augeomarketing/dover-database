USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSMetavanteRedemptionsStats]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSMetavanteRedemptionsStats]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSMetavanteRedemptionsStats]
	(@ReportStartDate		smalldatetime,
	 @ReportEndDate		smalldatetime)

as

-- Build temp table of TIPFirsts for all of Metavante
select dbnumber
into #metavante_tips
from rewardsnow.dbo.dbprocessinfo
where dbnamenexl like '%metavante%'


select cast(year(histdate) as nvarchar(4)) + '/' +
	  right('00' + cast(month(histdate) as nvarchar(2)), 2)+ '/' +
	  right('00' + cast(day(histdate) as nvarchar(2)), 2) as histdate,
	  sts.RedStatusName,  count(*) as NbrRedemptions

from fullfillment.dbo.[main] ful join #metavante_tips tips
	on ful.tipfirst = tips.dbnumber

join dbo.SubRedemptionStatus sts
	on ful.redstatus = sts.redstatus

where	histdate between @ReportStartDate and @ReportEndDate
	and  trancode != 'RQ' -- don't include brochures

group by cast(year(histdate) as nvarchar(4)) + '/' +
	  right('00' + cast(month(histdate) as nvarchar(2)), 2)+ '/' +
	  right('00' + cast(day(histdate) as nvarchar(2)), 2),  sts.RedStatusName 

order by cast(year(histdate) as nvarchar(4)) + '/' +
	  right('00' + cast(month(histdate) as nvarchar(2)), 2)+ '/' +
	  right('00' + cast(day(histdate) as nvarchar(2)), 2),  sts.RedStatusName
GO
