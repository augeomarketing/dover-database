USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spExtractFeeRedemptions]    Script Date: 01/06/2011 11:01:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spExtractFeeRedemptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spExtractFeeRedemptions]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[spExtractFeeRedemptions]    Script Date: 01/06/2011 11:01:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

----set ansi_padding on
--/*                                                                            */
--/* BQuinn          */
--/* DATE: 02/2009   */
--/* REVISION: 0 */
-- Modified extract_control statement to look for Trancode 'RF' which replaces Flag for Fee extraction. Extract_Control was modified to be used in
-- a more efficient manner
--/******************************************************************************/
create PROCEDURE [dbo].[spExtractFeeRedemptions]  AS

declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)
DECLARE @DateToday DATETIME
DECLARE @Datework nvarchar(19)

SET @DateToday = getdate()

set @Datework = left(@DateToday,11)
SET @DateToday = @Datework

  --Truncate work tables

		
		if OBJECT_ID('tempdb..#FeeTipsToExtract') is not null
		drop table #FeeTipsToExtract
	
		create table #FeeTipsToExtract
		(SID_EXTRACT_CONTROL_TIPFIRST		varchar(15) primary key,
		 DIM_EXTRACT_CONTROL_CLIENTNAME		varchar(40))

--		print @SQLIf

 		if exists(select * from  .dbo.sysobjects where xtype='u' and name = 'Fee_Redemptions')
		Begin
			TRUNCATE TABLE .dbo.Fee_Redemptions 
		End 


		if exists(select * from .dbo.sysobjects where xtype='u' and name = 'FeeRedemptionTotal')
		Begin
			TRUNCATE TABLE .dbo.FeeRedemptionTotal 
		End 



 --Insert new Gift Card Requests RedStatus = '0' Routing Code of '2'

            INSERT INTO  .dbo.Fee_Redemptions
			(
			SID_Fee_Redemptions_tipnumber,dim_Fee_Redemptions_name1, dim_Fee_Redemptions_catalogqty,
			dim_Fee_Redemptions_catalogdesc, dim_Fee_Redemptions_PointsPerItem ,
       		dim_Fee_Redemptions_totval,dim_Fee_Redemptions_cashvalue,
       		dim_Fee_Redemptions_name2,dim_Fee_Redemptions_histdate,
       		dim_Fee_Redemptions_PointsTotal, dim_Fee_Redemptions_saddress1, 
       		dim_Fee_Redemptions_saddress2, dim_Fee_Redemptions_scity, 
       		dim_Fee_Redemptions_sstate, dim_Fee_Redemptions_szip,
			DIM_Fee_Redemptions_Transid, dim_Fee_Redemptions_DateExtracted,
			dim_Fee_Redemptions_RedStatus , dim_Fee_Redemptions_ItemNumber 
			)
	       	select  tipnumber,name1,catalogqty as qty,left(catalogdesc,50) as catalogdesc, points,
	    	totval = (cashvalue*catalogqty),cashvalue,name2,
       		histdate, pointstotal =(points * catalogqty) , saddress1, saddress2, scity, sstate, szip,
			transid, @DateToday, RedStatus, ItemNumber 
			FROM  .dbo.MAIN 
			where  redstatus = '0' and Routing = '2' and trancode = 'RF' 
		    and points <> '0' and histdate < @DateToday 



-- Group Totals By TipFirst for presentation to Finance OnLine

            INSERT INTO  .dbo.FeeRedemptionTotal
			(
			sid_FeeRedemptionTotal_tipfirst, dim_FeeRedemptionTotal_totrecords, dim_FeeRedemptionTotal_QTYReds,
			 dim_FeeRedemptionTotal_TotalPoints,  dim_FeeRedemptionTotal_totalvalue, 
			 dim_FeeRedemptionTotal_totcashvalue, dim_FeeRedemptionTotal_TotalItemPoints
			)
	       	select left(SID_Fee_Redemptions_tipnumber,3)as sid_FeeRedemptionTotal_tipfirst,
	       	count(SID_Fee_Redemptions_Tipnumber) as dim_FeeRedemptionTotal_totrecords,
	       	sum(dim_Fee_Redemptions_catalogqty) as dim_FeeRedemptionTotal_QTYReds,
			sum(dim_Fee_Redemptions_PointsPerItem) as dim_FeeRedemptionTotal_TotalPoints, 
			sum(dim_Fee_Redemptions_totval) as dim_FeeRedemptionTotal_totalvalue,
			sum(dim_Fee_Redemptions_cashvalue * dim_Fee_Redemptions_catalogqty) as dim_FeeRedemptionTotal_totcashvalue,
			sum(dim_Fee_Redemptions_PointsPerItem * dim_Fee_Redemptions_catalogqty) as TotalItemPoints 
			from dbo.Fee_Redemptions
			group by left(SID_Fee_Redemptions_tipnumber,3) 
			order by left(SID_Fee_Redemptions_tipnumber,3)



-- Collect Clients That Have No Transactions This Week
		
		INSERT INTO #FeeTipsToExtract 
			(SID_EXTRACT_CONTROL_TIPFIRST,DIM_EXTRACT_CONTROL_CLIENTNAME) 
			select SID_EXTRACT_CONTROL_TIPFIRST,DIM_EXTRACT_CONTROL_CLIENTNAME
			from  dbo.EXTRACT_CONTROL  ec left outer join dbo.feeredemptiontotal frt
			    on ec.sid_extract_control_tipfirst = sid_feeredemptiontotal_tipfirst
			where DIM_EXTRACT_CONTROL_TRANCODE = 'RF'
			and frt.sid_feeredemptiontotal_tipfirst is null
		


-- Insert Into The Totals Table Entries for FI's Without Transactions
-- This will allow for the Fi's Tip to Appear in The Display For Finance

		INSERT INTO  .dbo.FeeRedemptionTotal
			(
			sid_FeeRedemptionTotal_tipfirst, dim_FeeRedemptionTotal_totrecords, dim_FeeRedemptionTotal_QTYReds,
			 dim_FeeRedemptionTotal_TotalPoints,  dim_FeeRedemptionTotal_totalvalue, dim_FeeRedemptionTotal_totcashvalue,
			  dim_FeeRedemptionTotal_TotalItemPoints
			)
	       	select SID_EXTRACT_CONTROL_TIPFIRST, 0 as totrecords, 0 as QTYCards,
			0 as TotalPoints, 0 as totalvalue,
			0 as totcashvalue, 0 as TotalItemPoints 
			from #FeeTipsToExtract tmp left outer join dbo.feeredemptiontotal frt
			    on tmp.sid_extract_control_tipfirst = sid_feeredemptiontotal_tipfirst
			where sid_feeredemptiontotal_tipfirst is null

		 
		 
-- Set The DateExtracted To The Current Date/Time 
		 
	   	Update  .dbo.FeeRedemptionTotal set dim_FeeRedemptionTotal_dateextracted = @DateToday


 
 -- This TABLE will be used to update the final status once the transaction has been verified and sent to the FI

		INSERT INTO  .dbo.FeeRedemptionProcessed
		( dim_FeeRedemptionProcessed_tipnumber, dim_FeeRedemptionProcessed_name1, dim_FeeRedemptionProcessed_catalogqty,
		  dim_FeeRedemptionProcessed_catalogdesc, dim_FeeRedemptionProcessed_PointsPerItem, dim_FeeRedemptionProcessed_totval, 
		  dim_FeeRedemptionProcessed_cashvalue, dim_FeeRedemptionProcessed_name2, dim_FeeRedemptionProcessed_histdate,
		  dim_FeeRedemptionProcessed_PointsTotal, dim_FeeRedemptionProcessed_saddress1,dim_FeeRedemptionProcessed_saddress2 ,
		  dim_FeeRedemptionProcessed_scity , dim_FeeRedemptionProcessed_sstate, dim_FeeRedemptionProcessed_szip,
		  dim_FeeRedemptionProcessed_Transid, dim_FeeRedemptionProcessed_DateExtracted, dim_FeeRedemptionProcessed_RedStatus  )
		  
	      select SID_Fee_Redemptions_tipnumber, dim_Fee_Redemptions_name1, dim_Fee_Redemptions_catalogqty,
		  dim_Fee_Redemptions_catalogdesc, dim_Fee_Redemptions_PointsPerItem, dim_Fee_Redemptions_totval, dim_Fee_Redemptions_cashvalue,
		  dim_Fee_Redemptions_name2, dim_Fee_Redemptions_histdate, dim_Fee_Redemptions_PointsTotal, 
 		  dim_Fee_Redemptions_saddress1,dim_Fee_Redemptions_saddress2 ,dim_Fee_Redemptions_scity ,
		  dim_Fee_Redemptions_sstate, dim_Fee_Redemptions_szip, DIM_Fee_Redemptions_Transid,
		  dim_Fee_Redemptions_DateExtracted, dim_Fee_Redemptions_RedStatus  
		  FROM .dbo.Fee_Redemptions  fr left outer join dbo.feeredemptionprocessed frp
		    on fr.dim_Fee_Redemptions_Transid = frp.dim_FeeRedemptionProcessed_Transid
		  where frp.dim_FeeRedemptionProcessed_Transid is null

		 




GO

