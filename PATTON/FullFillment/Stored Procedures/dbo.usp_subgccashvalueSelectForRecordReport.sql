SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Robert McKeagney>
-- Create date: <Dec 18, 2009>
-- Description:	<Generate a list of current definition records in the subgccashvalue table formatted for reporting purposes>
-- =============================================
CREATE PROCEDURE dbo.usp_subgccashvalueSelectForRecordReport 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select	gccv as [Last three/five of catalogcode],
		cashvalue as Value
from fullfillment.dbo.subgccashvalue


END
GO
