USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_errors_sent]    Script Date: 12/17/2010 14:53:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_errors_sent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_errors_sent]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_errors_sent]    Script Date: 12/17/2010 14:53:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20101217
-- =============================================
CREATE PROCEDURE [dbo].[usp_tc_errors_sent] 
 
AS
BEGIN

declare @currdateTime datetime = getdate()

UPDATE t
	SET tccreditdate = @CurrdateTime
	from fullfillment.dbo.TravelCert t
	join fullfillment.dbo.tc_error tc
	on t.TransID = tc.transid


UPDATE m
	SET redstatus = 7
	from fullfillment.dbo.main m
	join fullfillment.dbo.tc_error tc
	on m.TransID = tc.transid

END 
GO

