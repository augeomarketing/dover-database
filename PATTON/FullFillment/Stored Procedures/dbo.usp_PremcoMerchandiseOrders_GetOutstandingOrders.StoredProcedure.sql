USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_PremcoMerchandiseOrders_GetOutstandingOrders]    Script Date: 7/6/2015 12:14:49 PM ******/
SET ANSI_NULLS ON
GO

if object_ID('usp_PremcoMerchandiseOrders_GetOutstandingOrders') is not null
    drop procedure dbo.usp_PremcoMerchandiseOrders_GetOutstandingOrders
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.6.2015
-- Description:	Gets all outstanding premco orders
-- =============================================
CREATE PROCEDURE [dbo].[usp_PremcoMerchandiseOrders_GetOutstandingOrders] 
	-- Add the parameters for the stored procedure here
	@DaysToCheck INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate SMALLDATETIME = GETDATE()

	SELECT
		OrderId,
		ISNULL(long_catalog_code, ItemNumber) AS ItemNumber,
		HistDate
	FROM
		dbo.Main
	LEFT OUTER JOIN 
		[Catalog].[dbo].[CatalogCode] ON dim_catalog_code = ItemNumber
	WHERE
		RedStatus = 1 AND
		TranCode = 'RM' AND
		ItemNumber LIKE 'PRE-%' AND
		DATEADD(DAY, @DaysToCheck, HistDate) <= @CurrentDate

END
