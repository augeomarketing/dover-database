/****** Object:  StoredProcedure [dbo].[sp_updateqtrtravel]    Script Date: 03/12/2009 09:50:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[sp_updateqtrtravel]
as
update travelcert 
set tcstatus = '1' 
where tcstatus = '0' and transid in (select transid from main where trancode in ('RT','RU','RV') and saddress1 is null)
GO
