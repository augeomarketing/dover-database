USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_Main_GetDoverFulfilledOrders]    Script Date: 3/7/2016 2:26:16 PM ******/
SET ANSI_NULLS ON
GO

if object_ID('usp_Main_GetDoverFulfilledOrders') is not null
    drop procedure dbo.usp_Main_GetDoverFulfilledOrders
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 2/15/2016
-- Description:	Gets all Dover
--              fulfilled orders that have not 
--              benn sent to Naperville office. 
-- =============================================
CREATE PROCEDURE [dbo].[usp_Main_GetDoverFulfilledOrders] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT
		TipNumber,
		HistDate AS OrderDate,
		ISNULL(OrderID, '') AS OrderNumber,
		TranCode AS OrderType,
		CatalogDesc AS CatalogDescription,
		CatalogQty AS Quantity,
		Points
	FROM
		dbo.Main
	WHERE
		RedStatus = 23 AND
		IsSentToAugeoYN = 0
END


GO



