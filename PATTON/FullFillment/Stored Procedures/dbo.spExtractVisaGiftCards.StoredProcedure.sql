/****** Object:  StoredProcedure [dbo].[spExtractVisaGiftCards]    Script Date: 03/12/2009 09:50:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*                                                                            */
/******************************************************************************/
/*    This  Process will Extract the GiftCard Transactions for Compass Bank   */
/* This Process will be re written in TSQL at some point in the not to distant future  */
/*    and stored in the RewardsNow database */
/* BQuinn          */
/* DATE: 04/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/
--alter PROCEDURE [dbo].[spExtractVisaGiftCards]  @TipFirst char(3) AS

declare @dtest nvarchar(11)
declare @dteend nvarchar(11)
declare @TipFirst char(3)
set @tipfirst = '360'
/* Drop The Work Table */
if exists(select * from Fullfillment.dbo.sysobjects where xtype='u' and name = 'ZZZgftCRDwk')
begin
truncate table gftCRDwk360
end
 
if exists(select * from Fullfillment.dbo.sysobjects where xtype='u' and name = 'ZZZgftCRDtotal')
begin
drop table ZZZgftCRDtotal
end
/* Set the Date Parameter  */
set @dteend = 'jun 18 2009'
--set @dteend = getdate()
print @dteend
set @dtest = convert(nvarchar(25),(Dateadd(day, -7, @dteend)),121)
--print @dtest

insert into gftCRDwk360
select  tipnumber,name1,catalogqty as qty,left(catalogdesc,30) as catalogdesc, points,
        totval = (cast( SubString(Catalogdesc, patindex ('%$%', catalogdesc)+1,3) as int )* catalogqty),
  	    cashvalue,name2,
        histdate,(points * catalogqty) as pointsperitem, saddress1, saddress2, scity, sstate, szip, transid
from main 
--where  redstatus = '0'
where  left(tipfirst,2) = left(@TipFirst,2) 
    and (trandesc in ('SVC','Gift Cert') and (itemnumber like '%VISA%' or catalogdesc like '%VISA%'))
    and histdate >= @dtest and histdate < @dteend 
    and points <> '0' 
order by histdate asc
 
select left(tipnumber,3)as tipfirst,count(Tipnumber) as totrecords,sum(QTY) as QTYCards,
sum(points) as TotalPoints, sum(totval) as totalvalue,
sum(cashvalueeach * qty) as totcashvalue,
sum(pointsperitem) as TotalItemPoints
into ZZZgftCRDtotal 
from gftCRDwk360
group by left(tipnumber,3)

--update main set redstatus = '13' where transid in (select transid from gftCRDwk360)

--select * from main where transid in (select transid from ZZZgftCRDwk)
GO
