USE [fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[usp_webRedemptionYears]    Script Date: 02/15/2012 17:16:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20110930
-- Description:	Get Redemptions for LRC
-- =============================================
ALTER PROCEDURE [dbo].[usp_webRedemptionYears]
	@tipfirst varchar(3)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @thisyear INT
	SET @thisyear = (SELECT YEAR(GETDATE() - DAY(getdate())))

	SELECT [year] FROM 
	(
	SELECT @thisyear as [year]
	UNION 
	SELECT DISTINCT YEAR(histdate) as [year]
		FROM fullfillment.dbo.main
		WHERE LEFT(tipnumber,3) = @tipfirst
			AND YEAR(Histdate) >= (@thisyear - 1)
			AND YEAR(histdate) <= @thisyear

	) tmp
	ORDER BY [year] DESC
END

--exec fullfillment.dbo.usp_webRedemptionYears '255'