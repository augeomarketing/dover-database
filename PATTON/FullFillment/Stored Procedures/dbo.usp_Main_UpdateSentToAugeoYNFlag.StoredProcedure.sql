USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_Main_UpdateSentToAugeoYNFlag]    Script Date: 2/16/2016 11:11:49 AM ******/
SET ANSI_NULLS ON
GO

if object_ID('usp_Main_UpdateSentToAugeoYNFlag') is not null
    drop procedure dbo.usp_Main_UpdateSentToAugeoYNFlag
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 2/15/2016
-- Description:	Updates sent to augeo flag for all
--              sent items.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Main_UpdateSentToAugeoYNFlag] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Main SET IsSentToAugeoYN = 1 WHERE RedStatus = 23 AND IsSentToAugeoYN <> 1
END

GO



