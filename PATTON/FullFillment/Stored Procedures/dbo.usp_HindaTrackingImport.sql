USE [Fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_HindaTrackingImport]    Script Date: 09/13/2010 10:42:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HindaTrackingImport]') AND type in (N'P', N'PC'))
    DROP PROCEDURE [dbo].[usp_HindaTrackingImport]
GO

/****** Object:  StoredProcedure [dbo].[usp_HindaTrackingImport]    Script Date: 09/13/2010 10:42:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20100202
-- Description:	Update Fulfillment with Tracking Numbers from Hinda
-- =============================================
CREATE PROCEDURE [dbo].[usp_HindaTrackingImport]
    @type CHAR, 
    @orderId INT,
    @trackingNumber varchar(255), 
    @shipdate varchar(50), 
    @shippedBy int = 4
AS
BEGIN
  DECLARE @transid uniqueidentifier
  IF @type = 'P'
  BEGIN
    SELECT @transid = transid FROM Selection WHERE OrderNum = @orderId
    UPDATE Selection SET TransactNum = @trackingNumber WHERE OrderNum = @orderId
  END
  IF @type = 'R' OR @type = 'M'
  BEGIN
    SELECT @transid = transid FROM Main WHERE OrderId = @orderId
  END
  UPDATE Shipping 
	SET ShipTrack = @trackingNumber, 
		ShipDate = @shipdate, 
		ShippedBy = @shippedBy  --Hinda
    WHERE TransID = @transid

  UPDATE Main
    SET RedStatus = 5  --Complete
    WHERE TransID = @transid

--Foreign Key Constraint, adding TransId to Shipping (from Selection)
--		
--  IF @@ROWCOUNT = 0
--  BEGIN
--    INSERT INTO Shipping (TransId, ShipTrack, ShipDate, shippedBy)
--    VALUES (@transid, @trackingNumber, @shipdate, 4)
--  END
END

GO

