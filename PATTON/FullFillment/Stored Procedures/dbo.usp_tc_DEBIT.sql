USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_DEBIT]    Script Date: 12/17/2010 13:56:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_tc_DEBIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_tc_DEBIT]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_tc_DEBIT]    Script Date: 12/17/2010 13:56:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Bob Lawliss>
-- Create date: <3/15/10>
-- =============================================
-- =============================================
-- Author:		S Blanchette
-- Create date: 8/2010
-- Reason:  Change txn code and descr per ASB
-- SCAN:  SEB001
-- =============================================
-- =============================================
-- Author:		A Barriere
-- Create date: 12/2010
-- =============================================
-- =============================================
-- Author:		S Blanchette
-- Create date: 3/2011
-- Reason:  Change txn code and descr per ASB
-- SCAN:  SEB002
-- =============================================
-- =============================================
-- Author:		S Blanchette
-- Create date: 3/2011
-- Reason:  Include Tip and TCID in output
-- SCAN:  SEB004
-- =============================================

/******************************************/
/* S Blanchette                           */
/* 1/2012                                 */
/* SEB005                                 */
/* Add Name1 and Name2                    */
/******************************************/

CREATE PROCEDURE [dbo].[usp_tc_DEBIT] 
AS
BEGIN
	SET NOCOUNT ON;

--get the database name for later use
	declare @DBName varchar(50), @SQLSelect nvarchar(2000), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @SQLCursor nvarchar(2000), @SQLIF nvarchar(2000), @ErrorFlag char(1)
	declare @ctr int = 0

	select 
		m.tipnumber, 
		tc.tcid, 
		tc.transid, 
		m.RedStatus , 
		tc.TCStatus, 
		srs.redstatusname, 
		stcs.TCStatusName, 
		tc.TCLastSix , 
		case isnull(tc.TCActValRed, 0) 
			WHEN 0 THEN m.cashvalue 
			ELSE tc.TCActValRed
		end	as TCActValRed, 
		tc.TCRedDate, 
		tc.TCCreditDate,
		m.cashvalue as CV,
		tc.TCActValRed as AVR,
		m.Name1, /* SEB005 */
		m.Name2  /* SEB005 */
	into #Tmp
	from 
		main m 
			join TravelCert tc on m.TransID =tc.TransID 
			join SubRedemptionStatus srs on srs.RedStatus = m.RedStatus 
			join SubTravelCertStatus stcs on stcs.TCStatus  = tc.TCStatus 
	where  stcs.TCStatusName='Returned'
		and srs.RedstatusName='Completed'  --THIS NEEDS TO BE REMMED OUT IN TEST IN ORDER TO GET ANY RECORDS BACK
		and m.TipFirst in ('002','003')

	truncate table tc_Debit_wrk
	truncate table tc_DEBIT_OUT

--insert ASB
		insert into tc_Debit_wrk 
		
	SELECT	transid as transid, 
			MAX(tclastsix) as tclastsix, 
			MAX(TCActValRed) as TcActValRed,
			MAX(ACCTID) as acctid ,
			MAX(TIPNUMBER) as tipnumber ,
			MAX(AcctType) as accttype ,
			MAX(lastname) as lastname ,
			MAX(CustID) as custid,
			MAX(tcid) as tcid,  /* SEB004 */
			MAX(Name1) as Name1, /* SEB005 */
			MAX(Name2) as Name2  /* SEB005 */
	FROM (				
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID , 											
				#tmp.tcid,   /* SEB004 */ 
				#tmp.Name1, /* SEB005 */
				#tmp.Name2 /* SEB005 */											
			from #tmp 
				join asb.dbo.AFFILIAT a on a.TIPNUMBER = #tmp.tipnumber
					and right(a.acctid,4) = RIGHT(rtrim(#tmp.tcLastSix),4)
			where ((UPPER(a.AcctType) ='DEBIT') OR (UPPER(a.AcctType) ='CREDIT' AND LEFT(a.acctid,6)='485107'))

		UNION ALL
		
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID , 											
				#tmp.tcid,   /* SEB004 */ 
				#tmp.Name1, /* SEB005 */
				#tmp.Name2 /* SEB005 */											
			from #tmp join asbcorp.dbo.AFFILIAT a on a.TIPNUMBER=#tmp.tipnumber
				and right(a.acctid,4)=RIGHT(rtrim(#tmp.tcLastSix),4)
			where ((UPPER(a.AcctType) ='DEBIT') OR (UPPER(a.AcctType) ='CREDIT' AND LEFT(a.acctid,6)='485107'))
		) AS temp
		group by transid
		having COUNT(transid) = 1
		order by TIPNUMBER 
		
	insert into tc_error 
	SELECT	transid as transid,
			'DUPLICATE TRANSID' as reason 
	FROM (				
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID  											
			from #tmp 
				join asb.dbo.AFFILIAT a on a.TIPNUMBER = #tmp.tipnumber
					and right(a.acctid,4) = RIGHT(rtrim(#tmp.tcLastSix),4)
			where ((UPPER(a.AcctType) ='DEBIT') OR (UPPER(a.AcctType) ='CREDIT' AND LEFT(a.acctid,6)='485107'))

		UNION ALL
		
		select	#tmp.transid, 
				#tmp.tclastsix, 
				#tmp.TCActValRed,
				a.ACCTID ,
				a.TIPNUMBER ,
				a.AcctType ,
				a.lastname ,
				a.CustID 											
			from #tmp join asbcorp.dbo.AFFILIAT a on a.TIPNUMBER=#tmp.tipnumber
				and right(a.acctid,4)=RIGHT(rtrim(#tmp.tcLastSix),4)
			where ((UPPER(a.AcctType) ='DEBIT') OR (UPPER(a.AcctType) ='CREDIT' AND LEFT(a.acctid,6)='485107'))
		) AS temp
		group by transid
		having COUNT(transid) <> 1

	drop table #Tmp

/*********************************************/
/*                                           */
/* OUTPUT DEBIT CARD CHARGES                 */
/*                                           */
/*********************************************/

		--insert into the Output table
		insert into tc_DEBIT_OUT (PEBK, PEBLK, PEBTCH, PESEQ, PESSEQ, PESER, PERTR, PEACCT, PETRAN, PEAMT, PEEFDT, PECOST, PEDESC, PEEMPM, PEEMPA, PEDEL, RECNUM, SortChar, Tipnumber,  /* SEB004 */ TCID, /* SEB004 */ Name1, /* SEB005 */ Name2 /*SEB005 */)
		Select	'570' as PEBK,	
				'000' as PEBLK,	
				'0000' as PEBTCH,	
				'00000' as PESEQ,
				'00' as PESSEQ,
				'000000000000000' as PESSER,
				'321370765' as PERTR,
				'00' + custid as PEAACT,
				'000225' as PETRAN,
				REPLICATE('0', (13-len(replace(convert(varchar(13),isnull(TCActValRed,0)),'.','') ))) + replace(convert(varchar(13),isnull(TCActValRed,0)),'.','') as PEAMT, 
				'      ' as PEEFDT ,
				'01700' as PECOST, --WAITING FOR RESPONSE FROM LORRAINE
				'Rewards Rebate Debit          ' as PEDESC,
				'     ' as PEEMPM, /* SEB001 */
				'     ' as PEEMPA ,' ' as PEDEL, 
				'       ' as RecNum, 'A' as SortChar,
				Tipnumber, /* SEB004 */
				TCID,  /* SEB004 */
				Name1, /* SEB005 */
				Name2 /* SEB005 */											
		from tc_Debit_wrk
		WHERE LEFT(AcctType,1) = 'D'
/*********************************************/
/*    whole section added seb001             */
/* OUTPUT EEX	 CARD CHARGES                */
/*                                           */
/*********************************************/
/*           */
		--insert into the Output table
		insert into tc_DEBIT_OUT (PEBK, PEBLK, PEBTCH, PESEQ, PESSEQ, PESER, PERTR, PEACCT, PETRAN, PEAMT, PEEFDT, PECOST, PEDESC, PEEMPM, PEEMPA, PEDEL, RECNUM, SortChar, Tipnumber,  /* SEB004 */ TCID, /* SEB004 */ Name1, /* SEB005 */ Name2 /*SEB005 */)
		Select	'570' as PEBK,	
				'000' as PEBLK,	
				'0000' as PEBTCH,
				'00000' as PESEQ,
				'00' as PESSEQ,
				'000000000000000' as PESSER,
				/* seb002 '321370765' as PERTR, */
				'557050082' as PERTR, /* SEB002 */
				'00' + custid as PEAACT,
				'000082' as PETRAN,
				REPLICATE('0', (13-len(replace(convert(varchar(13),isnull(TCActValRed,0)),'.','') ))) + replace(convert(varchar(13),isnull(TCActValRed,0)),'.','') as PEAMT, /* SEB001 */
				'      ' as PEEFDT ,
				'01700' as PECOST, --WAITING FOR RESPONSE FROM LORRAINE
				'Rewards Rebate Credit         ' as PEDESC, /* SEB002 */ '     ' as PEEMPM, /* SEB001 */
				'     ' as PEEMPA ,' ' as PEDEL, 
				'       ' as RecNum, 'A' as SortChar,
				Tipnumber, /* SEB004 */
				TCID,  /* SEB004 */
				Name1, /* SEB005 */
				Name2 /* SEB005 */											
			from tc_Debit_wrk
			WHERE LEFT(AcctType,1) = 'C'

--=======================================================================================

declare @SumDebit decimal(18,0)
select @SumDebit = ISNULL(SUM(convert(float,PEAMT)),0) from tc_DEBIT_OUT
--add the 'trailer' rec in
	insert into tc_DEBIT_OUT (PEBK, PEBLK, PEBTCH, PESEQ, PESSEQ, PESER, PERTR, PEACCT, PETRAN, PEAMT, PEEFDT, PECOST, PEDESC, PEEMPM, PEEMPA, PEDEL, RECNUM, SortChar)
	Select	'570' as PEBK,	
			'000' as PEBLK,	
			'0000' as PEBTCH,	
			'00000' as PESEQ,
			'00' as PESSEQ,
			'000000000000000' as PESSER,
			'557030081' as PERTR,
			'0000000766050',
			'000081' as PETRAN,
			REPLICATE('0', (13-len(replace(convert(varchar(13),isnull(@SumDebit,0)),'.','') ))) + replace(convert(varchar(13),isnull(@SumDebit,0)),'.','') as PEAMT, 
			'      ' as PEEFDT ,
			'01700' as PECOST, --WAITING FOR RESPONSE FROM LORRAINE
			'Rewards Rebate Debit          ' as PEDESC,
			'     ' as PEEMPM, /* SEB001 */
			'     ' as PEEMPA ,' ' as PEDEL, 
			'       ' as RecNum, 'B' as SortChar
--end 'trailer rec								
print '@SumDebit' + cast(@SumDebit as varchar) 
print '@SumDebit' + REPLICATE('0', (13-len(replace(convert(varchar(13),isnull(@SumDebit,0)),'.','') ))) + replace(convert(varchar(13),isnull(@SumDebit,0)),'.','') 
--=======================================								
	--Below updates the RecNumFiELD

declare @newRecNum varchar(7) = '0000000'

UPDATE tc_DEBIT_OUT
SET @newRecNum = right('0000000' + convert(varchar(7),( @newRecNum + 1 )), 7),
	RecNum =  @newRecNum 

END


GO

