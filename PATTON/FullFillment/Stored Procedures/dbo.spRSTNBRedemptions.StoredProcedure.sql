USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spRSTNBRedemptions]    Script Date: 12/17/2009 09:04:03 ******/
DROP PROCEDURE [dbo].[spRSTNBRedemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spRSTNBRedemptions]
	(@ReportStartDate		smalldatetime,
	 @ReportEndDate		smalldatetime,
	 @SLADays				int)

as

-- Build temp table of TIPFirsts for all of Metavante
select dbnumber
into #tips
from rewardsnow.dbo.dbprocessinfo
where dbnamenexl like '%tnb%'

create table #SLA
	(RedStatusName				varchar(15),
	 TranDesc					varchar(100),
	 Redemptions_Total			bigint DEFAULT 0,
	 Redemptions_InsideSLA		bigint DEFAULT 0,
	 Redemptions_OutsideSLA		bigint DEFAULT 0)

create index ix_tmpSLA on #SLA (TranDesc, RedStatusName)

---------------------------------------------------------
--Load temp table with total redemptions
---------------------------------------------------------
insert into #SLA
(RedStatusName, TranDesc, Redemptions_Total)
select sts.RedStatusName, case
			when ful.trandesc like 'Conf%' then 'Travel'
			else ful.trandesc 
		end as trandesc, count(*) as NbrRedemptions
from fullfillment.dbo.[main] ful join #tips tips
	on ful.tipfirst = tips.dbnumber

join dbo.SubRedemptionStatus sts
	on ful.redstatus = sts.redstatus

where	histdate between @ReportStartDate and @ReportEndDate
	and  trancode != 'RQ' -- don't include brochures
-- Take below clause out - this is just a check
--	and trandesc not like 'Gift%'
--	and trandesc not like 'online%'
	and trandesc not like 'College%'

group by case
			when ful.trandesc like 'Conf%' then 'Travel'
			else ful.trandesc 
		end , sts.RedStatusName

order by case
			when ful.trandesc like 'Conf%' then 'Travel'
			else ful.trandesc 
		end , sts.RedStatusName

---------------------------------------------------------
-- Update temp table with redemptions within specified SLA
---------------------------------------------------------
select sts.RedStatusName, case
						when ful.trandesc like 'Conf%' then 'Travel'
						else ful.trandesc
					 end as trandesc, count(*) as NbrRedemptions
into #SLA_Within
from fullfillment.dbo.[main] ful join #tips tips
	on ful.tipfirst = tips.dbnumber

join dbo.SubRedemptionStatus sts
	on ful.redstatus = sts.redstatus

where	histdate between @ReportStartDate and @ReportEndDate
	and  trancode != 'RQ' -- don't include brochures
	and  datediff(day, histdate, isnull(Redreqfuldate, getdate())) <= @SLADays
group by case
			when trandesc like 'Conf%' then 'Travel'
			else trandesc 
		end , sts.RedStatusName

order by case
			when trandesc like 'Conf%' then 'Travel'
			else trandesc 
		end , sts.RedStatusName

-- Now update the temp table with the totals
update sla
	set Redemptions_InsideSLA = tmp.NbrRedemptions
from #SLA sla join #SLA_Within tmp
	on sla.redstatusname = tmp.redstatusname
	and case
			when sla.trandesc like 'Conf%' then 'Travel'
			else sla.trandesc 
		end = tmp.trandesc

---------------------------------------------------------
-- Update temp table with redemptions outside specified SLA
---------------------------------------------------------
select sts.RedStatusName, case
			when ful.trandesc like 'Conf%' then 'Travel'
			else ful.trandesc 
		end as trandesc, count(*) as NbrRedemptions
into #SLA_Outside
from fullfillment.dbo.[main] ful join #tips tips
	on ful.tipfirst = tips.dbnumber

join dbo.SubRedemptionStatus sts
	on ful.redstatus = sts.redstatus

where	histdate between @ReportStartDate and @ReportEndDate
	and  trancode != 'RQ' -- don't include brochures
	and  datediff(day, histdate, isnull(Redreqfuldate, getdate())) > @SLADays
group by case
			when ful.trandesc like 'Conf%' then 'Travel'
			else ful.trandesc 
		end , sts.RedStatusName

order by case
			when ful.trandesc like 'Conf%' then 'Travel'
			else ful.trandesc 
		end , sts.RedStatusName

-- Now update the temp table with the totals
update sla
	set Redemptions_OutsideSLA = tmp.NbrRedemptions
from #SLA sla join #SLA_Outside tmp
	on sla.redstatusname = tmp.redstatusname
	and case
			when sla.trandesc like 'Conf%' then 'Travel'
			else sla.trandesc 
		end  = tmp.trandesc

-----
-- KLUGE TO Recalc gift certs & online
-----
update #sla
	set Redemptions_InsideSLA = Redemptions_InsideSLA + Redemptions_OutsideSLA
where trandesc like 'gift%'
or trandesc like 'online%'
or trandesc like 'travel%'

update #sla
	set Redemptions_OutsideSLA = 0
where trandesc like 'gift%'
or trandesc like 'online%'
or trandesc like 'travel%'

-------
-- Set redemptions inside SLA time window to 0 for all those with a status of PROCESSING
-- or On Hold, or On Order
-------
update #sla
	set Redemptions_Total = Redemptions_Total - Redemptions_InsideSLA
where RedStatusName = 'Processing'
or    RedStatusName like 'On_Order%'
or    RedStatusName like 'On_Hold%'
or    RedStatusName like 'Backordered%'

update #sla
	set Redemptions_InsideSLA = 0
where RedStatusName = 'Processing'
or    RedStatusName like 'On_Order%'
or    RedStatusName like 'On_Hold%'
or    RedStatusName like 'Backordered%'


--------
-- Now return the results
--------
select RedStatusName, TranDesc, Redemptions_Total, Redemptions_InsideSLA, Redemptions_OutsideSLA
from #SLA
order by trandesc, redstatusname
GO
