USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_GiftCardRedemptionReport]    Script Date: 07/26/2011 10:21:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GiftCardRedemptionReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GiftCardRedemptionReport]
GO

USE [fullfillment]
GO

/****** Object:  StoredProcedure [dbo].[usp_GiftCardRedemptionReport]    Script Date: 07/26/2011 10:21:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_GiftCardRedemptionReport]
	@StartDate			datetime,
	@EndDate				datetime

AS

declare @rptStartDate		datetime
declare @rptEndDate			datetime

set @rptStartDate = cast(year(@StartDate) as varchar(4)) + '/' +
				right('00' + cast(month(@StartDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@StartDate) as varchar(2)), 2) + ' 00:00:00'

set @rptEndDate = cast(year(@EndDate) as varchar(4)) + '/' +
				right('00' + cast(month(@EndDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@EndDate) as varchar(2)), 2) + ' 23:59:59'


--Select      rtrim(ltrim(itemnumber)) as Catalogcode,
--            count(catalogqty)as Redeem_count,
--            sum(catalogqty) as Card_count
--from  dbo.main
--where 
--	 histdate between @rptStartDate and @rptEndDate
--      and   trancode = 'RC'
----      and   tipfirst not in ('002','003')  Removed 7/16/2009
--      and routing = 1
--      and (catalogdesc not like '%VISA%' or (tipfirst in ('231') and catalogdesc like '%visa%')) -- added for 231 Service credit union where RewardsNOW will fulfill Visa Gift Cards for them
--      and catalogdesc not like '%Quickgifts%'
--group by itemnumber
--order by itemnumber


select  rtrim(ltrim(m.ItemNumber)) as Catalogcode, count(m.ItemNumber)as Card_count
from fullfillment.dbo.Main m join catalog.dbo.catalog c
	on m.ItemNumber = c.dim_catalog_code
join catalog.dbo.catalogcategory cc
	on c.sid_catalog_id = cc.sid_catalog_id
join catalog.dbo.categorygroupinfo cgi
	on cc.sid_category_id = cgi.sid_category_id
join catalog.dbo.groupinfo gi
	on gi.sid_groupinfo_id = cgi.sid_groupinfo_id
	
where sid_routing_id = 1   -- in-house fulfilled
and dim_groupinfo_trancode = 'RC' -- redeem for cards
and m.HistDate between @rptStartDate and @rptEndDate
and (catalogdesc not like '%VISA%' or (tipfirst in ('231') and catalogdesc like '%visa%')) -- added for 231 Service credit union where RewardsNOW will fulfill Visa Gift Cards for them
and catalogdesc not like '%Quickgifts%'

group by m.ItemNumber
order by m.ItemNumber
GO


