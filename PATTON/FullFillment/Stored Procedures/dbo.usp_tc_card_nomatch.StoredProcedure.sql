USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[usp_tc_card_nomatch]    Script Date: 02/04/2011 13:41:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20101217
-- =============================================
ALTER PROCEDURE [dbo].[usp_tc_card_nomatch] 
 
AS
BEGIN
	declare @rc int = 0

	update travelcert
		set TCStatus = 7   -- Set of OFA  (see Dee Hayes for explanation)
	from travelcert 
		left outer join (

			select t.transid, m.tipnumber 
			from TravelCert t
			join main m on t.TransID = m.TransID
			inner join asb.dbo.AFFILIAT a
			 on m.TipNumber = a.TIPNUMBER
				AND rtrim(tclastsix) = RIGHT(RTRIM(a.ACCTID),LEN(rtrim(TCLastSix)))

			union

			select t.transid, m.tipnumber from TravelCert t
			join main m on t.TransID = m.TransID
			inner join asbcorp.dbo.AFFILIAT ac
			 on m.TipNumber = ac.TIPNUMBER
				AND rtrim(tclastsix) = RIGHT(RTRIM(ac.ACCTID),LEN(rtrim(TCLastSix)))
	) as temp
	on travelcert.transid = temp.transid
	join main on TravelCert.TransID = main.transid
	where LEFT(main.tipnumber,3) in ('002','003')
	and TCStatus = 2   -- Returned
	AND main.RedStatus = 5  -- Completed
	and temp.TipNumber is null

	set @rc = @@ROWCOUNT
	IF @rc > 0
	BEGIN
		INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_body,dim_perlemail_from,dim_perlemail_subject,dim_perlemail_to)
		VALUES ( convert(varchar(10),@rc) + ' ASB Travel Cert(s) have been set to OFA -- Cardnumber on file does not match submission', 'bcolbath@rewardsnow.com', 'ASB Travel Cert Set to OFA', 'request@rewardsnow.com')
	END

	
END

