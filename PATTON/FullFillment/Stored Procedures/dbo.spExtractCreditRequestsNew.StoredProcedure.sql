USE [Fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spExtractCreditRequestsNew]    Script Date: 10/06/2009 11:09:53 ******/
SET ANSI_NULLS ON
GO
DROP PROCEDURE [dbo].[spExtractCreditRequestsNew]
GO
SET QUOTED_IDENTIFIER ON
GO
--
create PROCEDURE [dbo].[spExtractCreditRequestsNew] AS
  
declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)
DECLARE @DateToday DATETIME
DECLARE @STARTDATE DATETIME
DECLARE @Datework nvarchar(19)
declare @TipFirst varchar(3)
SET @DateToday = getdate()
--print '@DateToday'
--print @DateToday
set @Datework = left(@DateToday,11)
SET @DateToday = @Datework
--print '@DateToday'
--print @DateToday
declare @NewRecordNumber as numeric
declare @RecordNumber as numeric
declare @recnumber int
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100) 
DECLARE @strStmt NVARCHAR(2500) 

set @NewRecordNumber = 0
truncate table CreditRequest

set @STARTDATE = convert(nvarchar(25),(Dateadd(day, -7, @DateToday)),121)
PRINT '@STARTDATE'
PRINT @STARTDATE

--  Truncate work tables

 		set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''CreditRequest'')
		Begin
			TRUNCATE TABLE .dbo.CreditRequest 
		End '
		exec sp_executesql @SQLIf

		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''CreditRequesttotal'')
		Begin
			TRUNCATE TABLE .dbo.CreditRequesttotal 
		End '
		exec sp_executesql @SQLIf

 		set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''DBNumberTable'')
		Begin
			Drop TABLE .dbo.DBNumberTable 
		End '
		exec sp_executesql @SQLIf

/* Load CreditRequest Table with  rows from History */
INSERT INTO  .dbo.CreditRequest
			(
			tipnumber,name1,name2,Qty,Catalogdesc,Points,Email,Cashvalue,Trancode,histdate,cardlast4 ,
			saddress1, scity, sstate, szip,dateextracted, TRANSID)			
	       	select  tipnumber,name1,name2,CatalogQty,Catalogdesc,Points,Email,cashvalue,Trancode,histdate,right(catalogdesc,4),
	       	saddress1,  scity, sstate, szip,@DateToday,TRANSID
			FROM  .dbo.MAIN where  redstatus = '0' and Routing = '2' and trancode = 'RB' 
			and points <> '0' and histdate < @DateToday 
			and TRANSID not IN (SELECT  TRANSID from .dbo.CreditRequestInProcess) order by histdate asc

		 
		 
		 
INSERT INTO  .dbo.CreditRequesttotal
			(
			tipfirst, totrecords, QTYCards,
			 TotalPoints,  totalvalue, totcashvalue, TotalItemPoints
			)
	       		select left(tipnumber,3)as tipfirst,count(Tipnumber) as totrecords,sum(QTY) as QTYCards,
			sum(points) as TotalPoints, sum(cashvalue) as totalvalue,
			sum(cashvalue* qty) as totcashvalue,
			sum(points * qty) as TotalItemPoints from CreditRequest
			group by left(tipnumber,3)order by left(tipnumber,3)






select distinct(left(tipnumber,3)) as TipFirst into DBNumberTable
from CreditRequest

Declare RB_crsr cursor
for Select *
From DBNumberTable   

Open RB_crsr

Fetch RB_crsr  
into  	@tipfirst

IF @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
BEGIN

SET @strDBName = (SELECT DBNamePatton FROM Rewardsnow.[dbo].[DBProcessinfo] 
                WHERE (dbnumber = @tipfirst)) 


set @sqlupdate=N'Update CreditRequest  set CreditRequest.cardlast4 = right(af.acctid,4), ' 
set @sqlupdate=@sqlupdate + N' CreditRequest.custid = isnull(af.custid,0) '
set @sqlupdate=@sqlupdate + N' from  ['      +  (rtrim(@strDBName))   + '].dbo.affiliat as af ' 
set @sqlupdate=@sqlupdate + N'inner join CreditRequest as cr '
set @sqlupdate=@sqlupdate + N'on af.tipnumber = cr.tipnumber '  
set @sqlupdate=@sqlupdate + N'where cr.tipnumber in (select af.tipnumber from ['  +  (rtrim(@strDBName)) + '].dbo.affiliat)'  
print '@sqlupdate'
print @sqlupdate
print ' '
exec sp_executesql @SQLUpdate, N'@strDBName nchar(30)', @strDBName=@strDBName

-- Update the History record in the individual DB's with the date sent

set @sqlupdate=N'Update ['      +  (rtrim(@strDBName))   + '].dbo.history  ' 
set @sqlupdate=@sqlupdate + N' set ['      +  (rtrim(@strDBName))   + '].dbo.history.acctid = cr.dateextracted' 
set @sqlupdate=@sqlupdate + N' from  dbo.CreditRequest as cr ' 
set @sqlupdate=@sqlupdate + N' inner join ['      +  (rtrim(@strDBName))   + '].dbo.history as hst '
set @sqlupdate=@sqlupdate + N' on hst.tipnumber = cr.tipnumber '  
set @sqlupdate=@sqlupdate + N' where cr.tipnumber in (select hst.tipnumber from ['     +  (rtrim(@strDBName))   + '].dbo.history)'  
set @sqlupdate=@sqlupdate + N' and trancode = ''RB'' and hst.acctid is null' 
print '@sqlupdate'
print @sqlupdate
print ' '
--exec sp_executesql @SQLUpdate, N'@strDBName nchar(30),@DateToday datetime', @strDBName=@strDBName, @DateToday=@DateToday


FETCH_NEXT:
	
	Fetch RB_crsr  
	into  	@tipfirst


END /*while */

GOTO EndPROC



Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  RB_crsr
deallocate  RB_crsr
Finish:
GO
