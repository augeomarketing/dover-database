USE [Fullfillment]
GO

if exists(select 1 from dbo.sysobjects where name = 'spGasCardRedemptionReport' and xtype = 'P')
    drop procedure dbo.spgasCardRedemptionReport
GO


/****** Object:  StoredProcedure [dbo].[spGasCardRedemptionReport]    Script Date: 07/24/2009 10:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[spGasCardRedemptionReport]
	@StartDate			datetime,
	@EndDate				datetime

AS

declare @rptStartDate		datetime
declare @rptEndDate			datetime

set @rptStartDate = cast(year(@StartDate) as varchar(4)) + '/' +
				right('00' + cast(month(@StartDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@StartDate) as varchar(2)), 2) + ' 00:00:00'

set @rptEndDate = cast(year(@EndDate) as varchar(4)) + '/' +
				right('00' + cast(month(@EndDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@EndDate) as varchar(2)), 2) + ' 23:59:59'


Select      rtrim(ltrim(itemnumber)) as Catalogcode,
            count(catalogqty)as Redeem_count,
            sum(catalogqty) as Card_count
from	   dbo.main
where 
	 histdate between @rptStartDate and @rptEndDate
      and   trancode = 'RC'
--      and   tipfirst not in ('002','003')  Removed 7/16/2009
      and routing = 3
      and catalogdesc not like '%VISA%'
      and catalogdesc not like '%Quickgifts%'
group by itemnumber
order by itemnumber

GO


