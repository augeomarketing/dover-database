/****** Script for SelectTopNRows command from SSMS  ******/
use [fullfillment] 
declare @nsn varchar(6)
exec usp_tc_GetNextSeqNum 'CREDIT' , @NextSeqNum=@nsn output
print '@NSN' + @NSN

if @NSN<>'000000'
	exec usp_tc_CREDIT @nsn

/*
	select * from tc_FileSeqNumbers
	select isnull(SeqNum,0) from tc_FileSeqNumbers where CreationDateTime=(Select '2010-04-15 13:21:17.543' )

	delete tc_FileSeqNumbers where seqID>1
	delete tc_CREDIT_Hdr where File_Seq_Num>'000001'
	delete tc_CREDIT_OUT where SeqNum>'000001'
	delete tc_CREDIT_trailer where SeqNum>'000001'	

	truncate table tc_FileSeqNumbers 
	truncate table tc_CREDIT_Hdr 
	truncate table tc_CREDIT_OUT 
	truncate table tc_CREDIT_trailer 

*/