USE [fullfillment]
GO
/****** Object:  Table [dbo].[VisagftCRDwk]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VisagftCRDwk](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[VisagftCRDwk] ADD [catalogdesc] [varchar](30) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [points] [int] NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [totval] [int] NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [cashvalue] [smallmoney] NOT NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[VisagftCRDwk] ADD [name2] [varchar](50) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [histdate] [smalldatetime] NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [pointsperitem] [int] NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [saddress1] [varchar](50) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [saddress2] [varchar](50) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [scity] [varchar](50) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [sstate] [varchar](5) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [szip] [varchar](15) NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [transid] [char](40) NOT NULL
ALTER TABLE [dbo].[VisagftCRDwk] ADD [DateExtracted] [datetime] NOT NULL
GO
SET ANSI_PADDING OFF
GO
