USE [fullfillment]
GO
/****** Object:  Table [dbo].[ZZZgftCRDNEW]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZZZgftCRDNEW](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL,
	[catalogdesc] [varchar](30) NULL,
	[points] [int] NULL,
	[totval] [int] NULL,
	[name2] [varchar](50) NULL,
	[histdate] [smalldatetime] NULL,
	[pointsperitem] [int] NULL,
	[saddress1] [varchar](50) NULL,
	[saddress2] [varchar](50) NULL,
	[scity] [varchar](50) NULL,
	[sstate] [varchar](5) NULL,
	[szip] [varchar](15) NULL,
	[ItemNumber] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
