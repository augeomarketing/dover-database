USE [fullfillment]
GO
/****** Object:  Table [dbo].[FISERV_CASHBACK_TRAILER]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FISERV_CASHBACK_TRAILER](
	[REC_LENGTH] [nvarchar](5) NULL,
	[REC_TYPE] [nvarchar](4) NULL,
	[REC_ID] [nvarchar](24) NULL,
	[CLIENT_ID] [nvarchar](5) NULL,
	[SOURCE] [nvarchar](3) NULL,
	[FILL_1] [nvarchar](50) NULL,
	[TOT_REC_COUNT] [nvarchar](10) NULL,
	[TOT_AMOUNT] [nvarchar](17) NULL,
	[DEBIT_TRANS_COUNT] [nvarchar](10) NULL,
	[DEBIT_TOTAL_AMOUNT] [nvarchar](17) NULL,
	[CREDIT_TRANS_COUNT] [nvarchar](10) NULL,
	[CREDIT_TOTAL_AMOUNT] [nvarchar](17) NULL,
	[FILL_2] [nvarchar](340) NULL
) ON [PRIMARY]
GO
