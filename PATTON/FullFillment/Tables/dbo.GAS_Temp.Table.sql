USE [fullfillment]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_Name]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_Name]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_Address1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_Address1]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_Address2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_Address2]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_City]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_City]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_State]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_State]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_Zip]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_Zip]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_Ship_To_Phone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_Ship_To_Phone]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_GAS_Part_Number]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_GAS_Part_Number]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_GAS_Amount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_GAS_Amount]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GAS_Temp_QTY_Ordered]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GAS_Temp] DROP CONSTRAINT [DF_GAS_Temp_QTY_Ordered]
END

GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[GAS_Temp]    Script Date: 11/09/2011 08:38:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GAS_Temp]') AND type in (N'U'))
DROP TABLE [dbo].[GAS_Temp]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[GAS_Temp]    Script Date: 11/09/2011 08:38:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GAS_Temp](
	[transid] [varchar](40) NOT NULL,
	[Ship_To_Name] [varchar](50) NOT NULL,
	[Ship_To_Address1] [varchar](50) NOT NULL,
	[Ship_To_Address2] [varchar](50) NOT NULL,
	[Ship_To_City] [varchar](50) NOT NULL,
	[Ship_To_State] [varchar](5) NOT NULL,
	[Ship_To_Zip] [varchar](15) NOT NULL,
	[Ship_To_Phone] [varchar](12) NOT NULL,
	[GAS_Part_Number] [varchar](20) NOT NULL,
	[GAS_Amount] [int] NOT NULL,
	[QTY_Ordered] [int] NOT NULL,
 CONSTRAINT [PK_GAS_Temp] PRIMARY KEY CLUSTERED 
(
	[transid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1815 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Name'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2115 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address1'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2115 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Address2'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_City'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_City'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_City'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1620 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_City'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_City'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_State'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_State'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_State'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1740 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_State'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_State'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Zip'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Zip'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Zip'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1545 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Zip'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Zip'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Phone'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Phone'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Phone'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1860 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Phone'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'Ship_To_Phone'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Part_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Part_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Part_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2145 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Part_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Part_Number'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1650 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'GAS_Amount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'QTY_Ordered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'QTY_Ordered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'QTY_Ordered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1665 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'QTY_Ordered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp', @level2type=N'COLUMN',@level2name=N'QTY_Ordered'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DefaultView', @value=0x02 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Filter', @value=NULL , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_FilterOnLoad', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_HideNewField', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_OrderBy', @value=NULL , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOn', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_OrderByOnLoad', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Orientation', @value=0x00 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TableMaxRecords', @value=10000 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_TotalsRow', @value=0 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GAS_Temp'
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_Name]  DEFAULT ('') FOR [Ship_To_Name]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_Address1]  DEFAULT ('') FOR [Ship_To_Address1]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_Address2]  DEFAULT ('') FOR [Ship_To_Address2]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_City]  DEFAULT ('') FOR [Ship_To_City]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_State]  DEFAULT ('') FOR [Ship_To_State]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_Zip]  DEFAULT ('') FOR [Ship_To_Zip]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_Ship_To_Phone]  DEFAULT ('') FOR [Ship_To_Phone]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_GAS_Part_Number]  DEFAULT ('') FOR [GAS_Part_Number]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_GAS_Amount]  DEFAULT ((0)) FOR [GAS_Amount]
GO

ALTER TABLE [dbo].[GAS_Temp] ADD  CONSTRAINT [DF_GAS_Temp_QTY_Ordered]  DEFAULT ((0)) FOR [QTY_Ordered]
GO


