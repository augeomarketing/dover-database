USE [fullfillment]
GO
/****** Object:  Table [dbo].[CreditRequesttotal]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditRequesttotal](
	[tipfirst] [nvarchar](3) NULL,
	[totrecords] [int] NULL,
	[QTYCards] [int] NULL,
	[TotalPoints] [int] NULL,
	[totalvalue] [int] NULL,
	[totcashvalue] [int] NULL,
	[TotalItemPoints] [int] NULL,
	[dateextracted] [datetime] NULL
) ON [PRIMARY]
GO
