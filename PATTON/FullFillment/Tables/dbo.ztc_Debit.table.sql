USE [fullfillment]
GO
/****** Object:  Table [dbo].[ztc_Debit]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ztc_Debit](
	[transid] [char](40) NOT NULL,
	[tclastsix] [char](6) NULL,
	[TCActValRed] [smallmoney] NULL,
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
