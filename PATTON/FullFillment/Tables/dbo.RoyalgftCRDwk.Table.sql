USE [fullfillment]
GO
/****** Object:  Table [dbo].[RoyalgftCRDwk]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoyalgftCRDwk](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [catalogdesc] [varchar](30) NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [points] [int] NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [totval] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [name2] [varchar](50) NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [histdate] [smalldatetime] NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [pointsperitem] [int] NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [saddress1] [varchar](50) NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [saddress2] [varchar](50) NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [scity] [varchar](50) NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [sstate] [varchar](5) NULL
ALTER TABLE [dbo].[RoyalgftCRDwk] ADD [szip] [varchar](15) NULL
GO
SET ANSI_PADDING OFF
GO
