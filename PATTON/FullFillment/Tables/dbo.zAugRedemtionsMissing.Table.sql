USE [fullfillment]
GO
/****** Object:  Table [dbo].[zAugRedemtionsMissing]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zAugRedemtionsMissing](
	[TipNumber] [char](15) NULL,
	[TipFirst] [char](3) NULL,
	[Source] [varchar](10) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[TransID] [char](40) NOT NULL,
	[OrderID] [nchar](6) NULL,
	[HistDate] [smalldatetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranDesc] [varchar](100) NULL,
	[Points] [int] NULL,
	[Vendor] [char](3) NULL,
	[ItemNumber] [varchar](20) NULL,
	[Catalogdesc] [varchar](300) NULL,
	[Segment] [char](2) NULL,
	[CatalogQty] [int] NULL,
	[SName] [varchar](50) NULL,
	[SAddress1] [varchar](50) NULL,
	[SAddress2] [varchar](50) NULL,
	[SAddress3] [varchar](50) NULL,
	[SAddress4] [varchar](50) NULL,
	[SCity] [varchar](50) NULL,
	[SState] [varchar](5) NULL,
	[SZip] [varchar](15) NULL,
	[SCountry] [varchar](50) NULL,
	[Phone1] [varchar](12) NULL,
	[Phone2] [varchar](12) NULL,
	[Email] [varchar](50) NULL,
	[RedStatus] [tinyint] NULL,
	[RedReqFulDate] [smalldatetime] NULL,
	[InvoiceDate] [smalldatetime] NULL,
	[Notes] [varchar](2000) NULL,
	[cashvalue] [smallmoney] NOT NULL,
	[Routing] [tinyint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
