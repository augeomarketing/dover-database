USE [fullfillment]
GO

/****** Object:  Table [dbo].[FeeRedemptionTotal]    Script Date: 01/06/2011 11:07:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FeeRedemptionTotal]') AND type in (N'U'))
DROP TABLE [dbo].[FeeRedemptionTotal]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[FeeRedemptionTotal]    Script Date: 01/06/2011 11:07:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FeeRedemptionTotal](
	[sid_FeeRedemptionTotal_tipfirst] [varchar](3) NULL,
	[dim_FeeRedemptionTotal_totrecords] [int] NOT NULL,
	[dim_FeeRedemptionTotal_QTYReds] [int] NOT NULL,
	[dim_FeeRedemptionTotal_TotalPoints] [int] NULL,
	[dim_FeeRedemptionTotal_totalvalue] [int] NOT NULL,
	[dim_FeeRedemptionTotal_totcashvalue] [money] NOT NULL,
	[dim_FeeRedemptionTotal_TotalItemPoints] [int] NOT NULL,
	[dim_FeeRedemptionTotal_dateextracted] [datetime] NULL
) ON [PRIMARY]

GO

