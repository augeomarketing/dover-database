USE [fullfillment]
GO
/****** Object:  Table [dbo].[GiftCardDeliveryReference]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftCardDeliveryReference](
	[TipFirst] [nvarchar](3) NULL,
	[Delivery_Method] [nvarchar](10) NULL,
	[FtpDirectory] [nvarchar](50) NULL,
	[Contact_Name] [nvarchar](255) NULL
) ON [PRIMARY]
GO
