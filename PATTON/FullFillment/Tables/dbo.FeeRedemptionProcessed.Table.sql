USE [fullfillment]
GO

/****** Object:  Table [dbo].[FeeRedemptionProcessed]    Script Date: 01/06/2011 11:06:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FeeRedemptionProcessed]') AND type in (N'U'))
DROP TABLE [dbo].[FeeRedemptionProcessed]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[FeeRedemptionProcessed]    Script Date: 01/06/2011 11:06:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FeeRedemptionProcessed](
	[dim_FeeRedemptionProcessed_tipnumber] [char](15) NULL,
	[dim_FeeRedemptionProcessed_name1] [varchar](50) NULL,
	[dim_FeeRedemptionProcessed_catalogqty] [int] NULL,
	[dim_FeeRedemptionProcessed_catalogdesc] [varchar](50) NULL,
	[dim_FeeRedemptionProcessed_PointsPerItem] [int] NULL,
	[dim_FeeRedemptionProcessed_totval] [int] NOT NULL,
	[dim_FeeRedemptionProcessed_cashvalue] [smallmoney] NOT NULL,
	[dim_FeeRedemptionProcessed_name2] [varchar](50) NULL,
	[dim_FeeRedemptionProcessed_histdate] [smalldatetime] NULL,
	[dim_FeeRedemptionProcessed_PointsTotal] [int] NULL,
	[dim_FeeRedemptionProcessed_saddress1] [varchar](50) NULL,
	[dim_FeeRedemptionProcessed_saddress2] [varchar](50) NULL,
	[dim_FeeRedemptionProcessed_scity] [varchar](50) NULL,
	[dim_FeeRedemptionProcessed_sstate] [varchar](5) NULL,
	[dim_FeeRedemptionProcessed_szip] [varchar](15) NULL,
	[dim_FeeRedemptionProcessed_DateExtracted] [datetime] NOT NULL,
	[dim_FeeRedemptionProcessed_RedStatus] [tinyint] NULL,
	[dim_FeeRedemptionProcessed_Transid] [char](40) NULL,
	[dim_FeeRedemptionProcessed_ItemNumber] [varchar](20) NULL
) ON [PRIMARY]

GO

