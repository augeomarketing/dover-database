USE [fullfillment]
GO
/****** Object:  Table [dbo].[GiftCardtracker]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GiftCardtracker](
	[TransID] [char](40) NULL,
	[GCCFSDate] [smalldatetime] NULL,
	[GCID] [varchar](125) NULL,
	[GCFulfilledBy] [varchar](25) NULL,
	[sid_gct_id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](100) NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[created] [datetime] NOT NULL,
 CONSTRAINT [PK_GiftCardtracker] PRIMARY KEY CLUSTERED 
(
	[sid_gct_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[GiftCardtracker] ADD  CONSTRAINT [DF_GiftCardtracker_Username]  DEFAULT (suser_sname()) FOR [Username]
GO
ALTER TABLE [dbo].[GiftCardtracker] ADD  CONSTRAINT [DF_GiftCardtracker_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[GiftCardtracker] ADD  CONSTRAINT [DF_GiftCardtracker_created]  DEFAULT (getdate()) FOR [created]
GO
