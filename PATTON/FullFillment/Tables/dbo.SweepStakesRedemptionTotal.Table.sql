USE [fullfillment]
GO
/****** Object:  Table [dbo].[SweepStakesRedemptionTotal]    Script Date: 03/09/2011 09:24:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SweepStakesRedemptionTotal](
	[sid_SweepStakesRedemptionTotal_tipfirst] [varchar](3) NULL,
	[dim_SweepStakesRedemptionTotal_totrecords] [int] NULL,
	[dim_SweepStakesRedemptionTotal_QTYReds] [int] NULL,
	[dim_SweepStakesRedemptionTotal_TotalPoints] [int] NULL,
	[dim_SweepStakesRedemptionTotal_totalvalue] [int] NULL,
	[dim_SweepStakesRedemptionTotal_totcashvalue] [money] NULL,
	[dim_SweepStakesRedemptionTotal_TotalItemPoints] [int] NULL,
	[dim_SweepStakesRedemptionTotal_dateextracted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_totrecords]  DEFAULT ((0)) FOR [dim_SweepStakesRedemptionTotal_totrecords]
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_QTYReds]  DEFAULT ((0)) FOR [dim_SweepStakesRedemptionTotal_QTYReds]
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_TotalPoints]  DEFAULT ((0)) FOR [dim_SweepStakesRedemptionTotal_TotalPoints]
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_totalvalue]  DEFAULT ((0)) FOR [dim_SweepStakesRedemptionTotal_totalvalue]
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_totcashvalue]  DEFAULT ((0)) FOR [dim_SweepStakesRedemptionTotal_totcashvalue]
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_TotalItemPoints]  DEFAULT ((0)) FOR [dim_SweepStakesRedemptionTotal_TotalItemPoints]
GO
ALTER TABLE [dbo].[SweepStakesRedemptionTotal] ADD  CONSTRAINT [DF_SweepStakesRedemptionTotal_dim_SweepStakesRedemptionTotal_dateextracted]  DEFAULT (getdate()) FOR [dim_SweepStakesRedemptionTotal_dateextracted]
GO
