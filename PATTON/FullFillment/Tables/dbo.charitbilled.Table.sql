USE [fullfillment]
GO
/****** Object:  Table [dbo].[charitbilled]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[charitbilled](
	[sid_charitbilled_id] [int] NOT NULL,
	[dim_charitbilled_transid] [nvarchar](150) NULL,
	[dim_charitbilled_date] [datetime] NULL,
	[dim_charitbilled_ceated] [datetime] NULL,
	[dim_charitbilled_lastmodified] [datetime] NULL,
	[dim_charitbilled_active] [nvarchar](50) NULL,
 CONSTRAINT [PK_charitbilled] PRIMARY KEY CLUSTERED 
(
	[sid_charitbilled_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[charitbilled] ADD  CONSTRAINT [DF_charitbilled_dim_charitbilled_ceated]  DEFAULT (getdate()) FOR [dim_charitbilled_ceated]
GO
ALTER TABLE [dbo].[charitbilled] ADD  CONSTRAINT [DF_charitbilled_dim_charitbilled_lastmodified]  DEFAULT (getdate()) FOR [dim_charitbilled_lastmodified]
GO
ALTER TABLE [dbo].[charitbilled] ADD  CONSTRAINT [DF_charitbilled_dim_charitbilled_active]  DEFAULT (1) FOR [dim_charitbilled_active]
GO
