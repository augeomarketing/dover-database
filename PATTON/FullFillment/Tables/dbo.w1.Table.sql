USE [fullfillment]
GO
/****** Object:  Table [dbo].[w1]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[w1](
	[tipnumber] [char](15) NULL,
	[count] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
