USE [fullfillment]
GO
/****** Object:  Table [dbo].[DBL_Temp]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBL_Temp](
	[DBL_Customer_Number] [varchar](7) NULL,
	[PO_Number] [varchar](15) NULL,
	[PO_Date] [datetime] NULL,
	[Cancel_Date] [varchar](1) NULL,
	[Order_Reference] [varchar](20) NULL,
	[Ship_Via_Code] [varchar](20) NULL,
	[Ship_Complete] [varchar](20) NULL,
	[Signature_Required] [varchar](20) NULL,
	[Ship_To_Name] [varchar](50) NULL,
	[Ship_To_Address1] [varchar](50) NULL,
	[Ship_To_Address2] [varchar](50) NULL,
	[Ship_To_City] [varchar](50) NULL,
	[Ship_To_State] [varchar](5) NULL,
	[Ship_To_Zip] [varchar](10) NULL,
	[Ship_To_Phone] [varchar](12) NULL,
	[Line_Number] [varchar](20) NULL,
	[DBL_Part_Number] [varchar](20) NULL,
	[QTY_Ordered] [int] NULL,
	[Line_Reference] [varchar](20) NULL,
	[UPC_Code] [varchar](20) NULL,
	[Comment] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
