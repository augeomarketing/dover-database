USE [fullfillment]
GO
/****** Object:  Table [dbo].[ZZZITEMwk]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZZZITEMwk](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ZZZITEMwk] ADD [catalogdesc] [varchar](30) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [points] [int] NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [totval] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[ZZZITEMwk] ADD [name2] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [histdate] [smalldatetime] NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [totpoints] [int] NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [saddress1] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [saddress2] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [scity] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [sstate] [varchar](5) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [szip] [varchar](15) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [transid] [nvarchar](40) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [dateextracted] [datetime] NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [redstatus] [nvarchar](2) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [routing] [nvarchar](2) NULL
ALTER TABLE [dbo].[ZZZITEMwk] ADD [itemnumber] [nvarchar](20) NULL
GO
SET ANSI_PADDING OFF
GO
