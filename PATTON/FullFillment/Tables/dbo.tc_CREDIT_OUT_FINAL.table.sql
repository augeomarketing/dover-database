USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_OUT_FINAL]    Script Date: 12/17/2010 14:00:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_CREDIT_OUT_FINAL]') AND type in (N'U'))
DROP TABLE [dbo].[tc_CREDIT_OUT_FINAL]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_OUT_FINAL]    Script Date: 12/17/2010 14:00:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_CREDIT_OUT_FINAL](
	[FullRecord] [char](512) NULL,
	[SeqChar] [char](1) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

