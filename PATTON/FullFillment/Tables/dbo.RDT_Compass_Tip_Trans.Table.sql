/****** Object:  Table [dbo].[RDT_Compass_Tip_Trans]    Script Date: 02/20/2009 15:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RDT_Compass_Tip_Trans](
	[Tipnumber] [char](15) NOT NULL,
	[transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
