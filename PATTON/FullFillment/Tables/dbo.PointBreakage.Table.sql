USE [fullfillment]
GO
/****** Object:  Table [dbo].[PointBreakage]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PointBreakage](
	[DBName] [varchar](100) NOT NULL,
	[LiveEarned] [int] NULL,
	[ClosedEarned] [int] NULL,
	[LiveRedeemed] [int] NULL,
	[ClosedRedeemed] [int] NULL,
	[LiveOutstanding] [int] NULL,
	[ClosedOutstanding] [int] NULL,
	[LiveDead] [int] NULL,
	[ClosedDead] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
