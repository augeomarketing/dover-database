USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_Credit_wrk]    Script Date: 12/17/2010 14:00:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_Credit_wrk]') AND type in (N'U'))
DROP TABLE [dbo].[tc_Credit_wrk]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_Credit_wrk]    Script Date: 12/17/2010 14:00:30 ******/
/*******************************/
/* S Blanchette                */
/* 3/2011                      */
/* SEB001                      */
/* Add Tip and TCID            */
/*******************************/
/*******************************/
/* S Blanchette                */
/* 1/2012                      */
/* SEB002                      */
/* Add Name1 and Name2         */
/*******************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_Credit_wrk](
	[transid] [char](40) NOT NULL,
	[tclastsix] [char](6) NULL,
	[TCActValRed] [varchar](18) NULL,
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[LastName] [varchar](40) NULL,
	[CustID] [char](13) NULL,
	[TCID] [varchar] (6) NULL,  /* SEB001 */
	[Name1] [varchar] (50) NULL, /* SEB002 */
	[Name2] [varchar] (50) NULL,  /* SEB002 */
 CONSTRAINT [PK_tc_Credit_wrk] PRIMARY KEY CLUSTERED 
(
	[transid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

