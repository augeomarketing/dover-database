USE [fullfillment]
GO
/****** Object:  Table [dbo].[w2]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w2](
	[tipnumber] [nvarchar](20) NULL,
	[count] [int] NULL
) ON [PRIMARY]
GO
