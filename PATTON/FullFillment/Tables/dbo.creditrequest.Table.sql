USE [fullfillment]
GO
/****** Object:  Table [dbo].[creditrequest]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[creditrequest](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL,
	[catalogdesc] [varchar](30) NULL,
	[points] [int] NULL,
	[Email] [varchar](50) NULL,
	[cashvalue] [smallmoney] NOT NULL,
	[name2] [varchar](50) NULL,
	[histdate] [smalldatetime] NULL,
	[pointstotperitem] [int] NULL,
	[saddress1] [varchar](50) NULL,
	[saddress2] [varchar](50) NULL,
	[scity] [varchar](50) NULL,
	[sstate] [varchar](5) NULL,
	[szip] [varchar](15) NULL,
	[transid] [char](40) NOT NULL,
	[dateextracted] [datetime] NULL,
	[RedStatus] [tinyint] NULL,
	[Trancode] [varchar](2) NULL,
	[Trandesc] [varchar](100) NULL,
	[ItemNumber] [varchar](20) NULL,
	[cardlast4] [varchar](4) NULL,
	[rebatetype] [varchar](1) NOT NULL,
	[custid] [varchar](13) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
