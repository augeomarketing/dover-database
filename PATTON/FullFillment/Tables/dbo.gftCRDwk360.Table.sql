USE [fullfillment]
GO
/****** Object:  Table [dbo].[gftCRDwk360]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gftCRDwk360](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[gftCRDwk360] ADD [catalogdesc] [varchar](30) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [points] [int] NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [totval] [int] NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [cashvalueEach] [smallmoney] NOT NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[gftCRDwk360] ADD [name2] [varchar](50) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [histdate] [smalldatetime] NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [pointsperitem] [int] NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [saddress1] [varchar](50) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [saddress2] [varchar](50) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [scity] [varchar](50) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [sstate] [varchar](5) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [szip] [varchar](15) NULL
ALTER TABLE [dbo].[gftCRDwk360] ADD [transid] [char](40) NOT NULL
GO
SET ANSI_PADDING OFF
GO
