USE [fullfillment]
GO
/****** Object:  Table [dbo].[FI_Pricing]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FI_Pricing](
	[Institution] [nvarchar](50) NULL,
	[Tipfirst] [varchar](5) NOT NULL,
	[Merch] [nvarchar](50) NULL,
	[Bonus] [nvarchar](50) NULL,
	[RetGiftCardFee] [nvarchar](50) NULL,
	[Tunesperpt] [nvarchar](50) NULL,
	[RTperpt] [nvarchar](50) NULL,
	[ONLineGCFee] [nvarchar](50) NULL,
 CONSTRAINT [PK_FI_Pricing] PRIMARY KEY CLUSTERED 
(
	[Tipfirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
