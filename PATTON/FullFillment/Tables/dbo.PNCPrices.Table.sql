USE [fullfillment]
GO
/****** Object:  Table [dbo].[PNCPrices]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PNCPrices](
	[Prod Numbers] [nvarchar](255) NULL,
	[Manufacturer] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Model Number] [nvarchar](255) NULL,
	[Item Id] [nvarchar](255) NULL,
	[Drop_Ship] [nvarchar](255) NULL,
	[MSRP] [money] NULL,
	[Street_Retail] [money] NULL,
	[Merch] [money] NULL,
	[Shipping] [money] NULL,
	[Handling] [money] NULL,
	[Delivered] [money] NULL,
	[18% Margin] [money] NULL,
	[TOTAL ] [money] NULL,
	[7% Tax AVG] [money] NULL,
	[TOTAL TO _CHARGE] [money] NULL
) ON [PRIMARY]
GO
