USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_DEBIT_OUT]    Script Date: 12/17/2010 14:01:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_DEBIT_OUT]') AND type in (N'U'))
DROP TABLE [dbo].[tc_DEBIT_OUT]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_DEBIT_OUT]    Script Date: 12/17/2010 14:01:00 ******/
/*******************************/
/* S Blanchette                */
/* 3/2011                      */
/* SEB001                      */
/* Add Tip and TCID            */
/*******************************/
/*******************************/
/* S Blanchette                */
/* 1/2012                      */
/* SEB002                      */
/* Add Name1 and Name2         */
/*******************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_DEBIT_OUT](
	[PEBK] [char](3) NULL,
	[PEBLK] [char](3) NULL,
	[PEBTCH] [char](4) NULL,
	[PESEQ] [char](5) NULL,
	[PESSEQ] [char](2) NULL,
	[PESER] [char](15) NULL,
	[PERTR] [char](9) NULL,
	[PEACCT] [char](13) NULL,
	[PETRAN] [char](6) NULL,
	[PEAMT] [char](13) NULL,
	[PEEFDT] [char](6) NULL,
	[PECOST] [char](5) NULL,
	[PEDESC] [char](30) NULL,
	[PEEMPM] [char](5) NULL,
	[PEEMPA] [char](5) NULL,
	[PEDEL] [char](1) NULL,
	[RECNUM] [char](7) NULL,
	[SortChar] [char](1) NULL,
	[TipNumber] [varchar] (15) NULL,  /* SEB001 */
	[TCID] [varchar] (6) NULL,  /* SEB001 */
	[Name1] [varchar] (50) NULL, /* SEB002 */
	[Name2] [varchar] (50) NULL  /* SEB002 */

) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

