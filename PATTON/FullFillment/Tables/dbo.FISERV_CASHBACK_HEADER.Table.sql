USE [fullfillment]
GO
/****** Object:  Table [dbo].[FISERV_CASHBACK_HEADER]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FISERV_CASHBACK_HEADER](
	[REC_LENGTH] [nvarchar](5) NULL,
	[REC_TYPE] [nvarchar](4) NULL,
	[REC_ID] [nvarchar](24) NULL,
	[CLIENT_ID] [nvarchar](5) NULL,
	[SOURCE] [nvarchar](3) NULL,
	[FILL_1] [nvarchar](50) NULL,
	[FILE_CREATE_DATE] [nvarchar](7) NULL,
	[FILE_CREATE_TIME] [nvarchar](6) NULL,
	[FILE_SEQUENCE_NUM] [nvarchar](6) NULL,
	[FILL_2] [nvarchar](402) NULL
) ON [PRIMARY]
GO
