USE [fullfillment]
GO
/****** Object:  Table [dbo].[permissiongroupuserinfo]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissiongroupuserinfo](
	[sid_archivepermissiongroupuserinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_permissiongroupuserinfo_id] [int] NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_permissiongroupuserinfo_created] [datetime] NOT NULL,
	[dim_permissiongroup_lastmodified] [datetime] NOT NULL,
	[dim_permissiongroupuserinfo_active] [int] NOT NULL,
	[dim_archivepermissiongroupuserinfo_created] [datetime] NOT NULL,
	[dim_archivepermissiongroup_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
