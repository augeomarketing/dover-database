USE [fullfillment]
GO
/****** Object:  Table [dbo].[SweepStakesRedemptions]    Script Date: 03/09/2011 09:24:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SweepStakesRedemptions](
	[dim_SweepStakesRedemptions_tipnumber] [char](15) NULL,
	[dim_SweepStakesRedemptions_name1] [varchar](50) NULL,
	[dim_SweepStakesRedemptions_catalogqty] [int] NULL,
	[dim_SweepStakesRedemptions_catalogdesc] [varchar](50) NULL,
	[dim_SweepStakesRedemptions_PointsPerItem] [int] NULL,
	[dim_SweepStakesRedemptions_totval] [int] NULL,
	[dim_SweepStakesRedemptions_cashvalue] [smallmoney] NOT NULL,
	[dim_SweepStakesRedemptions_name2] [varchar](50) NULL,
	[dim_SweepStakesRedemptions_histdate] [smalldatetime] NULL,
	[dim_SweepStakesRedemptions_PointsTotal] [int] NULL,
	[dim_SweepStakesRedemptions_saddress1] [varchar](50) NULL,
	[dim_SweepStakesRedemptions_saddress2] [varchar](50) NULL,
	[dim_SweepStakesRedemptions_scity] [varchar](50) NULL,
	[dim_SweepStakesRedemptions_sstate] [varchar](5) NULL,
	[dim_SweepStakesRedemptions_szip] [varchar](15) NULL,
	[dim_SweepStakesRedemptions_DateExtracted] [datetime] NOT NULL,
	[dim_SweepStakesRedemptions_RedStatus] [tinyint] NULL,
	[dim_SweepStakesRedemptions_Transid] [char](40) NULL,
	[dim_SweepStakesRedemptions_Entry_id] [char](40) NULL,
	[dim_SweepStakesRedemptions_ItemNumber] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
