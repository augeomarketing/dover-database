USE [fullfillment]
GO
/****** Object:  Table [dbo].[Redemptions_DailyCount]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemptions_DailyCount](
	[TipFirst] [varchar](3) NULL,
	[Count_RN1] [int] NULL,
	[Points_RN1] [int] NULL,
	[Count_ONL_RN1] [int] NULL,
	[Points_ONL_RN1] [int] NULL,
	[Count_ONL_Patton] [int] NULL,
	[Points_ONL_Patton] [int] NULL,
	[Count_Patton] [int] NULL,
	[Points_Patton] [int] NULL,
	[Count_Fulfillment] [int] NULL,
	[Points_Fulfillment] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
