USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_OUT]    Script Date: 12/17/2010 13:59:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_CREDIT_OUT]') AND type in (N'U'))
DROP TABLE [dbo].[tc_CREDIT_OUT]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_OUT]    Script Date: 12/17/2010 13:59:09 ******/
/*******************************/
/* S Blanchette                */
/* 3/2011                      */
/* SEB001                      */
/* Add Tip and TCID            */
/*******************************/
/*******************************/
/* S Blanchette                */
/* 1/2012                      */
/* SEB002                      */
/* Add Name1 and Name2         */
/*******************************/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_CREDIT_OUT](
	[SeqNum] [char](6) NULL,
	[TransID] [char](50) NULL,
	[Rec_Len] [char](5) NULL,
	[Rec_Type] [char](4) NULL,
	[Rec_ID] [char](24) NULL,
	[Client_ID] [char](5) NULL,
	[Source] [char](3) NULL,
	[Filler] [char](50) NULL,
	[Org] [char](3) NULL,
	[Acct_Nbr] [char](19) NULL,
	[Tran_Amt] [char](18) NULL,
	[Txn_Code] [char](3) NULL,
	[Txn_Code_Ind] [char](1) NULL,
	[Eff_Date] [char](7) NULL,
	[SKU_NBR] [char](9) NULL,
	[Descr] [char](40) NULL,
	[Credit_Plan] [char](5) NULL,
	[Auth_Code] [char](6) NULL,
	[Ticket_nbr] [char](15) NULL,
	[PO_Nbr] [char](16) NULL,
	[Pmt_Ref_Nbr] [char](15) NULL,
	[Item_Nbr] [char](5) NULL,
	[Store_Nbr] [char](9) NULL,
	[Dept] [char](4) NULL,
	[SalesClerk] [char](12) NULL,
	[Quantity] [char](4) NULL,
	[Unit_Price] [char](18) NULL,
	[Ref_nbr] [char](23) NULL,
	[Plan_Seq] [char](2) NULL,
	[FS_Points] [char](10) NULL,
	[FS_Pgm_Nbr] [char](1) NULL,
	[Merchant_Org] [char](3) NULL,
	[Merchant_Store] [char](9) NULL,
	[Category_Code] [char](4) NULL,
	[Visa_Tran_ID] [char](15) NULL,
	[Intch_Free] [char](10) NULL,
	[Qual_Ind] [char](1) NULL,
	[Card_Nbr] [char](19) NULL,
	[Card_Seq] [char](4) NULL,
	[Card_Blk_Code] [char](1) NULL,
	[Ins_Prod] [char](2) NULL,
	[Merch_ID] [char](15) NULL,
	[Trm_Actn_Flag] [char](2) NULL,
	[Merch_Country_Code] [char](3) NULL,
	[User_Filler] [char](20) NULL,
	[Filler_1] [char](68) NULL,
	[TipNumber] [varchar] (15) NULL,  /* SEB001 */
	[TCID] [varchar] (6) NULL,  /* SEB001 */
	[Name1] [varchar] (50) NULL, /* SEB002 */
	[Name2] [varchar] (50) NULL  /* SEB002 */
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

