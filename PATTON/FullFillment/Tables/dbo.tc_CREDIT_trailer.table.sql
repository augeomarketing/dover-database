USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_trailer]    Script Date: 12/17/2010 14:00:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_CREDIT_trailer]') AND type in (N'U'))
DROP TABLE [dbo].[tc_CREDIT_trailer]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_trailer]    Script Date: 12/17/2010 14:00:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_CREDIT_trailer](
	[SeqNum] [char](6) NULL,
	[Rec_len] [char](5) NOT NULL,
	[Rec_Type] [char](4) NULL,
	[Rec_ID] [char](24) NULL,
	[Client_ID] [char](5) NULL,
	[Source] [char](3) NULL,
	[filler] [char](50) NULL,
	[Tot_Rec_Count] [char](10) NULL,
	[Tot_Amt] [char](17) NULL,
	[Debit_Tran_Count] [char](10) NULL,
	[Debit_Total_Amt] [char](17) NULL,
	[Credit_Tran_Count] [char](10) NULL,
	[Credit_Total_Amt] [char](17) NULL,
	[filler1] [char](340) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

