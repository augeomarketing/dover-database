USE [fullfillment]
GO
/****** Object:  Table [dbo].[subcashbackvalue]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[subcashbackvalue](
	[tipfirstpoints] [varchar](10) NULL,
	[CashValue] [smallmoney] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
