USE [fullfillment]
GO
/****** Object:  Table [dbo].[ztc_Debit_Output]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ztc_Debit_Output](
	[BN_SN] [varchar](32) NOT NULL,
	[Routing_Transit] [varchar](9) NOT NULL,
	[AcctNum] [varchar](15) NULL,
	[trancode] [varchar](6) NOT NULL,
	[ActAmtRedeemed] [varchar](13) NULL,
	[EffectiveDate] [varchar](6) NOT NULL,
	[costCtr] [varchar](5) NOT NULL,
	[descript] [varchar](30) NOT NULL,
	[MadeByEmp] [varchar](5) NOT NULL,
	[OfficerApproval] [varchar](5) NOT NULL,
	[DeleteField] [varchar](1) NOT NULL,
	[RecNum] [varchar](7) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
