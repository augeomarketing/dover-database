USE [fullfillment]
GO
/****** Object:  Table [dbo].[TravelCerttracker]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TravelCerttracker](
	[TransID] [char](40) NOT NULL,
	[TCID] [varchar](6) NULL,
	[TCLastSix] [char](6) NULL,
	[TCStatus] [tinyint] NULL,
	[TCActValRed] [smallmoney] NULL,
	[TCRedDate] [smalldatetime] NULL,
	[TCCreditDate] [smalldatetime] NULL,
	[TCTranDate] [smalldatetime] NULL,
	[username] [varchar](100) NOT NULL,
	[TCTID] [int] IDENTITY(1,1) NOT NULL,
	[DateModified] [datetime] NOT NULL,
 CONSTRAINT [PK_TravelCerttracker] PRIMARY KEY CLUSTERED 
(
	[TCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TravelCerttracker] ADD  CONSTRAINT [DF_TravelCerttracker_DateModified]  DEFAULT (getdate()) FOR [DateModified]
GO
