USE [fullfillment]
GO
/****** Object:  Table [dbo].[Subclientlkup]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subclientlkup](
	[SID_subclientlkup_id] [int] NOT NULL,
	[dim_subclientlkup_name] [nvarchar](50) NOT NULL,
	[dim_subclientlkup_phone] [nvarchar](50) NOT NULL,
	[dim_subclientlkup_lastmodified] [datetime] NOT NULL,
	[dim_subclientlkup_created] [datetime] NOT NULL,
	[dim_subclientlkup_active] [int] NOT NULL
) ON [PRIMARY]
GO
