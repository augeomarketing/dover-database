USE [fullfillment]
GO
/****** Object:  Table [dbo].[SweepStakesRedemptionProcessed]    Script Date: 03/09/2011 09:24:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SweepStakesRedemptionProcessed](
	[dim_SweepStakesRedemptionProcessed_tipnumber] [char](15) NULL,
	[dim_SweepStakesRedemptionProcessed_name1] [varchar](50) NULL,
	[dim_SweepStakesRedemptionProcessed_catalogqty] [int] NULL,
	[dim_SweepStakesRedemptionProcessed_catalogdesc] [varchar](50) NULL,
	[dim_SweepStakesRedemptionProcessed_PointsPerItem] [int] NULL,
	[dim_SweepStakesRedemptionProcessed_totval] [int] NULL,
	[dim_SweepStakesRedemptionProcessed_cashvalue] [smallmoney] NOT NULL,
	[dim_SweepStakesRedemptionProcessed_name2] [varchar](50) NULL,
	[dim_SweepStakesRedemptionProcessed_histdate] [smalldatetime] NULL,
	[dim_SweepStakesRedemptionProcessed_PointsTotal] [int] NULL,
	[dim_SweepStakesRedemptionProcessed_saddress1] [varchar](50) NULL,
	[dim_SweepStakesRedemptionProcessed_saddress2] [varchar](50) NULL,
	[dim_SweepStakesRedemptionProcessed_scity] [varchar](50) NULL,
	[dim_SweepStakesRedemptionProcessed_sstate] [varchar](5) NULL,
	[dim_SweepStakesRedemptionProcessed_szip] [varchar](15) NULL,
	[dim_SweepStakesRedemptionProcessed_DateExtracted] [datetime] NOT NULL,
	[dim_SweepStakesRedemptionProcessed_RedStatus] [tinyint] NULL,
	[dim_SweepStakesRedemptionProcessed_Transid] [char](40) NULL,
	[dim_SweepStakesRedemptionProcessed_Entry_id] [char](40) NULL,
	[dim_SweepStakesRedemptionProcessed_ItemNumber] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
