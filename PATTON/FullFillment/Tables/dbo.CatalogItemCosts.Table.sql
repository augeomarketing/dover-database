USE [fullfillment]
GO
/****** Object:  Table [dbo].[CatalogItemCosts]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CatalogItemCosts](
	[CatalogCode] [varchar](20) NOT NULL,
	[ItemCost] [decimal](10, 2) NOT NULL,
	[EstimatedShippingCost] [decimal](10, 2) NOT NULL,
	[SellingPrice] [decimal](10, 2) NOT NULL,
	[LastUpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CatalogItemCosts] PRIMARY KEY CLUSTERED 
(
	[CatalogCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CatalogItemCosts] ADD  CONSTRAINT [DF_CatalogItemCosts_ItemCost]  DEFAULT (0.00) FOR [ItemCost]
GO
ALTER TABLE [dbo].[CatalogItemCosts] ADD  CONSTRAINT [DF_CatalogItemCosts_EstimatedShippingCost]  DEFAULT (0.00) FOR [EstimatedShippingCost]
GO
ALTER TABLE [dbo].[CatalogItemCosts] ADD  CONSTRAINT [DF_CatalogItemCosts_SellingPrice]  DEFAULT (0.00) FOR [SellingPrice]
GO
ALTER TABLE [dbo].[CatalogItemCosts] ADD  CONSTRAINT [DF_CatalogItemCosts_LastUpdateDate]  DEFAULT (getdate()) FOR [LastUpdateDate]
GO
