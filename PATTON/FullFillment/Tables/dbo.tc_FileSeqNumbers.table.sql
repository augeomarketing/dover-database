USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_FileSeqNumbers]    Script Date: 12/17/2010 14:02:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_FileSeqNumbers]') AND type in (N'U'))
DROP TABLE [dbo].[tc_FileSeqNumbers]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_FileSeqNumbers]    Script Date: 12/17/2010 14:02:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_FileSeqNumbers](
	[SeqID] [int] IDENTITY(1,1) NOT NULL,
	[SeqNum] [varchar](6) NOT NULL,
	[CreationDateTime] [datetime] NOT NULL,
	[DebitCredit] [varchar](6) NOT NULL,
 CONSTRAINT [PK_tc_FileSeqNumbers] PRIMARY KEY CLUSTERED 
(
	[SeqNum] ASC,
	[CreationDateTime] ASC,
	[DebitCredit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

