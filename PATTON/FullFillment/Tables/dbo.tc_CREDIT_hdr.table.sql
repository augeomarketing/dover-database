USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_Hdr]    Script Date: 12/17/2010 13:58:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_CREDIT_Hdr]') AND type in (N'U'))
DROP TABLE [dbo].[tc_CREDIT_Hdr]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_CREDIT_Hdr]    Script Date: 12/17/2010 13:58:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_CREDIT_Hdr](
	[Rec_Len] [char](5) NULL,
	[Rec_Type] [char](4) NULL,
	[Rec_ID] [char](24) NULL,
	[Client_ID] [char](5) NULL,
	[Source] [char](3) NULL,
	[Filler] [char](50) NULL,
	[File_Creation_Date] [char](7) NULL,
	[Creation_Time] [char](6) NULL,
	[File_Seq_Num] [char](6) NULL,
	[Filler1] [char](402) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

