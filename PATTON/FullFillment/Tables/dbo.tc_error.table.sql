USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_error]    Script Date: 12/17/2010 14:02:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_error]') AND type in (N'U'))
DROP TABLE [dbo].[tc_error]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_error]    Script Date: 12/17/2010 14:02:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_error](
	[sid] [int] IDENTITY(1,1) NOT NULL,
	[transid] [varchar](40) NOT NULL,
	[reason] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_tc_CREDIT_error] PRIMARY KEY CLUSTERED 
(
	[sid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

