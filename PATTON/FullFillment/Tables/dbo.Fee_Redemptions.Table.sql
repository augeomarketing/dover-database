USE [fullfillment]
GO
/****** Object:  Table [dbo].[Fee_Redemptions]    Script Date: 05/11/2010 15:42:10 ******/
SET ANSI_NULLS ON
GO
DROP TABLE [dbo].[Fee_Redemptions]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fee_Redemptions](
	[sid_Fee_Redemptions_tipnumber] [char](15) NULL,
	[dim_Fee_Redemptions_name1] [varchar](50) NULL,
	[dim_Fee_Redemptions_catalogqty] [int] NULL,
	[dim_Fee_Redemptions_catalogdesc] [varchar](50) NULL,
	[dim_Fee_Redemptions_PointsPerItem] [int] NULL,
	[dim_Fee_Redemptions_totval] [int] NOT NULL,
	[dim_Fee_Redemptions_cashvalue] [smallmoney] NOT NULL,
	[dim_Fee_Redemptions_name2] [varchar](50) NULL,
	[dim_Fee_Redemptions_histdate] [smalldatetime] NULL,
	[dim_Fee_Redemptions_PointsTotal] [int] NULL,
	[dim_Fee_Redemptions_saddress1] [varchar](50) NULL,
	[dim_Fee_Redemptions_saddress2] [varchar](50) NULL,
	[dim_Fee_Redemptions_scity] [varchar](50) NULL,
	[dim_Fee_Redemptions_sstate] [varchar](5) NULL,
	[dim_Fee_Redemptions_szip] [varchar](15) NULL,
	[dim_Fee_Redemptions_DateExtracted] [datetime] NOT NULL,
	[dim_Fee_Redemptions_RedStatus] [tinyint] NULL,
	[dim_Fee_Redemptions_Transid] [char](40) NULL,
	[dim_Fee_Redemptions_ItemNumber] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
