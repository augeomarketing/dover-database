USE [fullfillment]
GO
/****** Object:  Table [dbo].[ZZZgftCRDwk]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZZZgftCRDwk](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [catalogdesc] [varchar](30) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [points] [int] NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [totval] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [name2] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [histdate] [smalldatetime] NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [pointsperitem] [int] NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [saddress1] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [saddress2] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [scity] [varchar](50) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [sstate] [varchar](5) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [szip] [varchar](15) NULL
ALTER TABLE [dbo].[ZZZgftCRDwk] ADD [ItemNumber] [varchar](20) NULL
GO
SET ANSI_PADDING OFF
GO
