USE fullfillment
GO

IF OBJECT_ID(N'NullRoutingTracking') IS NOT NULL
DROP TABLE NullRoutingTracking
GO


CREATE TABLE NullRoutingTracking
(
	sid_nullroutingtracking_id INT IDENTITY(1,1) PRIMARY KEY
	, dim_nullroutingtracking_tipnumber VARCHAR(15)
	, dim_nullroutingtracking_trancode VARCHAR(2)
	, dim_nullroutingtracking_itemnumber VARCHAR(20)
	, dim_nullroutingtracking_transid CHAR(40)
	, dim_nullroutingtracking_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
)
GO