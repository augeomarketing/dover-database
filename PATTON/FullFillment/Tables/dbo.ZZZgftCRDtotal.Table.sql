USE [fullfillment]
GO
/****** Object:  Table [dbo].[ZZZgftCRDtotal]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[ZZZgftCRDtotal](
	[tipfirst] [varchar](3) NULL,
	[totrecords] [int] NULL,
	[QTYCards] [int] NULL,
	[TotalPoints] [int] NULL,
	[totalvalue] [int] NULL,
	[totcashvalue] [money] NULL,
	[TotalItemPoints] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
