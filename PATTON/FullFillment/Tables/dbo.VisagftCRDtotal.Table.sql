USE [fullfillment]
GO
/****** Object:  Table [dbo].[VisagftCRDtotal]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VisagftCRDtotal](
	[VGCRDTkey] [int] IDENTITY(1,1) NOT NULL,
	[tipfirst] [varchar](3) NULL,
	[totrecords] [int] NULL,
	[QTYCards] [int] NULL,
	[TotalPoints] [int] NULL,
	[totalvalue] [int] NULL,
	[totcashvalue] [money] NULL,
	[TotalItemPoints] [int] NULL,
	[deliverystatus] [int] NOT NULL,
	[Dateextracted] [datetime] NOT NULL,
 CONSTRAINT [PK_VisagftCRDtotal] PRIMARY KEY CLUSTERED 
(
	[VGCRDTkey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[VisagftCRDtotal] ADD  CONSTRAINT [DF_VisagftCRDtotal_deliverystatus]  DEFAULT (1) FOR [deliverystatus]
GO
ALTER TABLE [dbo].[VisagftCRDtotal] ADD  CONSTRAINT [DF_VisagftCRDtotal_apprvldate]  DEFAULT (getdate()) FOR [Dateextracted]
GO
