USE [fullfillment]
GO
/****** Object:  Table [dbo].[Subprog]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subprog](
	[sid_subprog_id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramName] [nvarchar](100) NOT NULL,
	[PhoneNumber1] [nvarchar](50) NOT NULL,
	[Marker] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Subprog] PRIMARY KEY CLUSTERED 
(
	[sid_subprog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
