USE [fullfillment]
GO

/****** Object:  Table [dbo].[Hinda_Temp]    Script Date: 05/08/2012 10:27:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Hinda_Temp]') AND type in (N'U'))
DROP TABLE [dbo].[Hinda_Temp]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[Hinda_Temp]    Script Date: 05/08/2012 10:27:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Hinda_Temp](
	[ClientOrderNumber] [varchar](16) NULL,
	[ShiptoFirstName] [varchar](50) NULL,
	[ShiptoAddressLine1] [varchar](50) NULL,
	[ShiptoAddressLine2] [varchar](50) NULL,
	[ShiptoCity] [varchar](50) NULL,
	[ShiptoState] [varchar](5) NULL,
	[ShiptoPostalCode] [varchar](10) NULL,
	[ShiptoPhone] [varchar](12) NULL,
	[ShiptoEmail] [varchar](50) NULL,
	[Catalognumber] [varchar](20) NULL,
	[Quantity] [varchar](12) NULL,
	[TimeStamp] [smalldatetime] NULL
) ON [PRIMARY]

GO

