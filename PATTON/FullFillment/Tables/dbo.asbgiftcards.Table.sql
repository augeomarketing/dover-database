USE [fullfillment]
GO
/****** Object:  Table [dbo].[asbgiftcards]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[asbgiftcards](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[name2] [varchar](50) NULL,
	[Amount] [smallmoney] NULL,
	[Gift_Card_Type] [varchar](50) NULL,
	[qty] [int] NULL,
	[Redemption_date] [smalldatetime] NULL,
	[sAddress1] [varchar](50) NULL,
	[sAddress2] [varchar](50) NULL,
	[sCity] [varchar](50) NULL,
	[sState] [varchar](5) NULL,
	[szip] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
