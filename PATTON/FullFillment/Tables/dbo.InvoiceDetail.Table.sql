USE [fullfillment]
SET ANSI_PADDING ON
GO


/****** Object:  Table [dbo].[InvoiceDetail]    Script Date: 03/22/2011 16:29:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceDetail]') AND type in (N'U'))
DROP TABLE [dbo].[InvoiceDetail]
GO

/****** Object:  Table [dbo].[InvoiceDetail]    Script Date: 03/22/2011 16:29:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[InvoiceDetail](
	[sid_InvoiceDetail_TransID] [char](40) NOT NULL,
	[dim_InvoiceDetail_Client] [varchar](256) NULL,
	[dim_InvoiceDetail_TipNumber] [varchar](15) NULL,
	[dim_InvoiceDetail_Name] [varchar](50) NULL,
	[dim_InvoiceDetail_Type] [varchar](100) NULL,
	[dim_InvoiceDetail_Product] [varchar](300) NULL,
	[dim_InvoiceDetail_Qty] int  NULL,
	[dim_InvoiceDetail_Points] int NULL,
	[dim_InvoiceDetail_Date] [varchar](11) NULL,
	[dim_InvoiceDetail_ItemCode] [varchar](20) NULL,
	[dim_InvoiceDetail_Source] [varchar](10) NULL,
	[dim_InvoiceDetail_CardType] [char](1) NULL,
	[dim_InvoiceDetail_OrderID] [Bigint] NULL,
 CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED 
(
	[sid_InvoiceDetail_TransID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


