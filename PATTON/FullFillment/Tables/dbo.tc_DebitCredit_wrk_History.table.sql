USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_DebitCredit_wrk_History]    Script Date: 12/17/2010 14:02:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tc_DebitCredit_wrk_History]') AND type in (N'U'))
DROP TABLE [dbo].[tc_DebitCredit_wrk_History]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[tc_DebitCredit_wrk_History]    Script Date: 12/17/2010 14:02:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tc_DebitCredit_wrk_History](
	[transid] [char](40) NOT NULL,
	[tclastsix] [char](6) NULL,
	[TCActValRed] [smallmoney] NULL,
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[LastName] [varchar](40) NULL,
	[CustID] [char](13) NULL,
	[DateRun] [datetime] NOT NULL,
 CONSTRAINT [PK_tc_DebitCredit_wrk_History] PRIMARY KEY CLUSTERED 
(
	[transid] ASC,
	[DateRun] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

