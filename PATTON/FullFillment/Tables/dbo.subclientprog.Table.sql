USE [fullfillment]
GO
/****** Object:  Table [dbo].[subclientprog]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subclientprog](
	[sid_subclientprog_id] [int] IDENTITY(1,1) NOT NULL,
	[Clients] [nvarchar](255) NULL,
	[Phoneloginnum] [nvarchar](100) NULL,
	[Ringtonumber] [nvarchar](255) NULL,
	[phonenumber] [nvarchar](255) NULL,
	[phrasenumber] [nvarchar](255) NULL,
	[forwardnumber] [nvarchar](255) NULL,
	[FI] [nvarchar](255) NULL
) ON [PRIMARY]
GO
