USE [fullfillment]
GO
/****** Object:  Table [dbo].[GiftCardsInProcessBJQ]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GiftCardsInProcessBJQ](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[qty] [int] NULL,
	[catalogdesc] [varchar](30) NULL,
	[points] [int] NULL,
	[totval] [int] NULL,
	[cashvalue] [smallmoney] NOT NULL,
	[name2] [varchar](50) NULL,
	[histdate] [smalldatetime] NULL,
	[pointsperitem] [int] NULL,
	[saddress1] [varchar](50) NULL,
	[saddress2] [varchar](50) NULL,
	[scity] [varchar](50) NULL,
	[sstate] [varchar](5) NULL,
	[szip] [varchar](15) NULL,
	[transid] [char](40) NOT NULL,
	[DateExtracted] [datetime] NOT NULL,
	[RedStatus] [char](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
