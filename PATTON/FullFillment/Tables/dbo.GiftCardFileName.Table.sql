USE [fullfillment]
GO
/****** Object:  Table [dbo].[GiftCardFileName]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiftCardFileName](
	[TipFirst] [nvarchar](3) NOT NULL,
	[FileName] [nvarchar](255) NULL,
	[FileSentToFI] [nvarchar](1) NULL,
	[DateSent] [datetime] NULL,
	[GCFKey] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_GiftCardFileName] PRIMARY KEY CLUSTERED 
(
	[GCFKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
