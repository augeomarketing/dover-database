USE [fullfillment]
GO
/****** Object:  Table [dbo].[zzzgiftcardcounts]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[zzzgiftcardcounts](
	[Client] [varchar](50) NULL,
	[Qty] [int] NULL,
	[Points] [int] NULL,
	[dbname] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
