USE [fullfillment]
GO
/****** Object:  Table [dbo].[tcwork3]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tcwork3](
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[tcid] [varchar](6) NULL,
	[tccreditdate] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
