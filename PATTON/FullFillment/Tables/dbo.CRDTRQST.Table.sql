USE [fullfillment]
GO
/****** Object:  Table [dbo].[CRDTRQST]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CRDTRQST](
	[NAME1] [nvarchar](50) NULL,
	[NAME2] [nvarchar](50) NULL,
	[ADDRESS1] [nvarchar](50) NULL,
	[ADDRESS2] [nvarchar](50) NULL,
	[CITY] [nvarchar](35) NULL,
	[STATE] [nvarchar](2) NULL,
	[ZIPCODE] [nvarchar](9) NULL,
	[ACCOUNT16] [nvarchar](16) NULL,
	[TIPNUMBER] [nvarchar](20) NULL,
	[TRANSDATE] [nvarchar](10) NULL,
	[CERTVALUE] [nvarchar](10) NULL,
	[PRODCODE] [nvarchar](2) NULL,
	[PRODTYPE] [nvarchar](2) NULL,
	[DDANUMBER] [nvarchar](13) NULL,
	[STATECODE] [nvarchar](3) NULL,
	[CERTNUM] [nvarchar](5) NULL,
	[crdtrqst_key_sid] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
