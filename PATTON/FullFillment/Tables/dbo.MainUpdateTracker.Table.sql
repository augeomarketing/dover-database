USE [fullfillment]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MainUpdateTracker_Modified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MainUpdateTracker] DROP CONSTRAINT [DF_MainUpdateTracker_Modified]
END

GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[MainUpdateTracker]    Script Date: 05/08/2012 11:04:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MainUpdateTracker]') AND type in (N'U'))
DROP TABLE [dbo].[MainUpdateTracker]
GO

USE [fullfillment]
GO

/****** Object:  Table [dbo].[MainUpdateTracker]    Script Date: 05/08/2012 11:04:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MainUpdateTracker](
	[TipNumber] [char](15) NULL,
	[TipFirst] [char](3) NULL,
	[Source] [varchar](10) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[TransID] [char](40) NOT NULL,
	[OrderID] [bigint] NULL,
	[HistDate] [smalldatetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranDesc] [varchar](100) NULL,
	[Points] [int] NULL,
	[Vendor] [char](3) NULL,
	[ItemNumber] [varchar](20) NULL,
	[Catalogdesc] [varchar](300) NULL,
	[Segment] [char](2) NULL,
	[CatalogQty] [int] NULL,
	[SName] [varchar](50) NULL,
	[SAddress1] [varchar](50) NULL,
	[SAddress2] [varchar](50) NULL,
	[SAddress3] [varchar](50) NULL,
	[SAddress4] [varchar](50) NULL,
	[SCity] [varchar](50) NULL,
	[SState] [varchar](5) NULL,
	[SZip] [varchar](15) NULL,
	[SCountry] [varchar](50) NULL,
	[Phone1] [varchar](12) NULL,
	[Phone2] [varchar](12) NULL,
	[Email] [varchar](50) NULL,
	[RedStatus] [tinyint] NULL,
	[RedReqFulDate] [smalldatetime] NULL,
	[InvoiceDate] [smalldatetime] NULL,
	[Routing] [tinyint] NULL,
	[MainID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](100) NOT NULL,
	[Modified] [datetime] NOT NULL,
 CONSTRAINT [PK_MainUpdateTracker] PRIMARY KEY CLUSTERED 
(
	[MainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [fullfillment]
/****** Object:  Index [idx_mut_transid_inc_redreqfuldate]    Script Date: 05/08/2012 11:04:51 ******/
CREATE NONCLUSTERED INDEX [idx_mut_transid_inc_redreqfuldate] ON [dbo].[MainUpdateTracker] 
(
	[TransID] ASC
)
INCLUDE ( [RedReqFulDate]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MainUpdateTracker] ADD  CONSTRAINT [DF_MainUpdateTracker_Modified]  DEFAULT (getdate()) FOR [Modified]
GO

