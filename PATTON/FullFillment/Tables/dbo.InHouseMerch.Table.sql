USE [fullfillment]
GO
/****** Object:  Table [dbo].[InHouseMerch]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InHouseMerch](
	[Unit Cost] [money] NOT NULL,
	[Catalog#] [nvarchar](255) NOT NULL
) ON [PRIMARY]
GO
