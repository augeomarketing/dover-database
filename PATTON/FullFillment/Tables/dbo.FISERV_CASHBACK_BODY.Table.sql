USE [fullfillment]
GO
/****** Object:  Table [dbo].[FISERV_CASHBACK_BODY]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FISERV_CASHBACK_BODY](
	[REC_LENGTH] [nvarchar](5) NULL,
	[REC_TYPE] [nvarchar](4) NULL,
	[REC_ID] [nvarchar](24) NULL,
	[CLIENT_ID] [nvarchar](5) NULL,
	[SOURCE] [nvarchar](3) NULL,
	[FILL_1] [nvarchar](50) NULL,
	[ORG] [nvarchar](3) NULL,
	[ACCT_NBR] [nvarchar](19) NULL,
	[TRAN_AMT] [nvarchar](18) NULL,
	[TRAN_CODE] [nvarchar](3) NULL,
	[TRAN_CODE_IND] [nvarchar](1) NULL,
	[EFF_DATE] [nvarchar](7) NULL,
	[SKU_NUMBER] [nvarchar](9) NULL,
	[TRANS_DESC] [nvarchar](40) NULL,
	[CREDIT_PLAN] [nvarchar](5) NULL,
	[AUTH_CODE] [nvarchar](6) NULL,
	[TICKET_NUM] [nvarchar](15) NULL,
	[PO_NUMBER] [nvarchar](16) NULL,
	[PMT_REF_NUM] [nvarchar](15) NULL,
	[ITEM_NUM] [nvarchar](5) NULL,
	[STORE_NUM] [nvarchar](9) NULL,
	[DEPT] [nvarchar](4) NULL,
	[SALES_CLERK] [nvarchar](12) NULL,
	[QUANTITY] [nvarchar](4) NULL,
	[UNIT_PRICE] [nvarchar](18) NULL,
	[REF_NUM] [nvarchar](23) NULL,
	[PLAN_SEQ] [nvarchar](2) NULL,
	[FS_POINTS] [nvarchar](10) NULL,
	[FS_PGM_NUM] [nvarchar](1) NULL,
	[MERCHANT_ORG] [nvarchar](3) NULL,
	[MERCHANT_STORE] [nvarchar](9) NULL,
	[CATEGORY_CODE] [nvarchar](4) NULL,
	[VISA_TRAN_ID] [nvarchar](15) NULL,
	[INTCHG_FEE] [nvarchar](10) NULL,
	[QUAL_INF] [nvarchar](1) NULL,
	[CARD_NUM] [nvarchar](19) NULL,
	[CARD_SEQ] [nvarchar](4) NULL,
	[CARD_BLOCK_CODE] [nvarchar](1) NULL,
	[INS_PROD] [nvarchar](2) NULL,
	[MERCHANT_ID] [nvarchar](15) NULL,
	[TRM_ACTN_FLAG] [nvarchar](2) NULL,
	[MERCHANT_COUNTRY_CODE] [nvarchar](3) NULL,
	[USER_FILLER] [nvarchar](20) NULL,
	[FILL_2] [nvarchar](68) NULL
) ON [PRIMARY]
GO
