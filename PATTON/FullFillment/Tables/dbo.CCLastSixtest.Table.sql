USE [fullfillment]
GO
/****** Object:  Table [dbo].[CCLastSixtest]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CCLastSixtest](
	[TipNumber] [char](15) NOT NULL,
	[LastSix] [char](6) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
