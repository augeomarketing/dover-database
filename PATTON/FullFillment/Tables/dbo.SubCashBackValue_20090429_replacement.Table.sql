USE [fullfillment]
GO
/****** Object:  Table [dbo].[SubCashBackValue_20090429_replacement]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCashBackValue_20090429_replacement](
	[TipFirstPoints] [int] NOT NULL,
	[CashValue] [smallmoney] NOT NULL
) ON [PRIMARY]
GO
