USE [fullfillment]
GO
/****** Object:  Table [dbo].[Dropship_Temp]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dropship_Temp](
	[PO_Number] [varchar](7) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[Catalog_Code] [varchar](20) NULL,
	[Item_Description] [varchar](300) NULL,
	[Qty] [varchar](12) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](5) NULL,
	[Zip] [varchar](15) NULL,
	[Phone] [varchar](12) NULL,
	[TimeStamp] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
