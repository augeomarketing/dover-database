USE [fullfillment]
GO
/****** Object:  Table [dbo].[tcwork1]    Script Date: 03/11/2010 09:44:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tcwork1](
	[TransID] [char](40) NOT NULL,
	[TCID] [varchar](6) NULL,
	[TCLastSix] [char](6) NULL,
	[TCStatus] [tinyint] NULL,
	[TCActValRed] [smallmoney] NULL,
	[TCRedDate] [smalldatetime] NULL,
	[TCCreditDate] [smalldatetime] NULL,
	[TCTranDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
