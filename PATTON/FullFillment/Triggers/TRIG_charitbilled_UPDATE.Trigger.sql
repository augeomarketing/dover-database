/****** Object:  Trigger [TRIG_charitbilled_UPDATE]    Script Date: 02/20/2009 15:13:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_charitbilled_UPDATE] ON [dbo].[charitbilled] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_charitbilled_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_charitbilled_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_charitbilled_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE charitbilled SET dim_charitbilled_lastmodified = getdate() WHERE sid_charitbilled_id = @sid_charitbilled_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_charitbilled_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
