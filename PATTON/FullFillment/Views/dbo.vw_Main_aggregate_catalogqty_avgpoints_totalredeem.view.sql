use fullfillment
GO

if object_id('fullfillment.dbo.vw_Main_aggregate_Catalogqty_Avgpoints_TotalRedeem') is not null
    drop view dbo.vw_Main_aggregate_Catalogqty_Avgpoints_TotalRedeem
GO

create view dbo.vw_Main_aggregate_Catalogqty_Avgpoints_TotalRedeem
as

select itemnumber, cd.dim_catalogdescription_name, trancode, year(histdate) yr, month(histdate) mo, catalogqty , points, (points * catalogqty) total_redeem
from fullfillment.dbo.main m join (select dim_catalog_code, dim_catalogdescription_name
							from catalog.dbo.catalog c join catalog.dbo.catalogdescription cd
								on c.sid_catalog_id = cd.sid_catalog_id
								--and c.dim_catalog_active = 1
								--and cd.dim_catalogdescription_active = 1
							 ) cd
	   on m.itemnumber = CASE
					   when left(m.itemnumber,2) = 'GC' then 'GC' + cd.dim_catalog_code
					   else cd.dim_catalog_code
				      END

where   left(tipnumber,1) <> '9' and
	   tipnumber not like '%999999%' and
	   points > 0
