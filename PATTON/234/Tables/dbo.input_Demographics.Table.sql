USE [234]
GO
/****** Object:  Table [dbo].[input_Demographics]    Script Date: 08/13/2010 14:54:00 ******/
DROP TABLE [dbo].[input_Demographics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[input_Demographics](
	[DDA] [varchar](20) NULL,
	[HomePhone] [varchar](20) NULL,
	[junk] [varchar](3) NULL,
	[SSN] [varchar](11) NULL,
	[AcctFullName] [varchar](50) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[St] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL
) ON [PRIMARY]
GO
