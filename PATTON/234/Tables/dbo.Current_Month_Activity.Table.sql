USE [234]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/13/2010 14:54:00 ******/
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_EndingPoints]
GO
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Increases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Decreases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]
GO
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [varchar](15) NOT NULL,
	[EndingPoints] [int] NOT NULL,
	[Increases] [int] NOT NULL,
	[Decreases] [int] NOT NULL,
	[AdjustedEndingPoints] [int] NOT NULL,
 CONSTRAINT [PK_Current_Month_Activity] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT ((0)) FOR [EndingPoints]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT ((0)) FOR [Increases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT ((0)) FOR [Decreases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT ((0)) FOR [AdjustedEndingPoints]
GO
