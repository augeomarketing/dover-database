USE [234]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 08/13/2010 14:54:00 ******/
ALTER TABLE [dbo].[TranType] DROP CONSTRAINT [DF_TranType_Points]
GO
ALTER TABLE [dbo].[TranType] DROP CONSTRAINT [DF_TranType_Ratio]
GO
DROP TABLE [dbo].[TranType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [varchar](2) NOT NULL,
	[Description] [varchar](40) NULL,
	[IncDec] [varchar](1) NOT NULL,
	[CntAmtFxd] [varchar](1) NOT NULL,
	[Points] [int] NOT NULL,
	[Ratio] [int] NOT NULL,
	[TypeCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Points]  DEFAULT ((1)) FOR [Points]
GO
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Ratio]  DEFAULT ((1)) FOR [Ratio]
GO
