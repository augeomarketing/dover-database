USE [234]
GO
/****** Object:  UserDefinedFunction [dbo].[fnTotalPointsFromHistoryForTranCode]    Script Date: 08/13/2010 14:54:01 ******/
DROP FUNCTION [dbo].[fnTotalPointsFromHistoryForTranCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fnTotalPointsFromHistoryForTranCode] (@TipNumber nvarchar(16), @StartDate datetime, @EndDate datetime, @TranCode nvarchar(2))

returns int

AS

BEGIN
declare @points	int

	set @points = 0

	set @points = (select sum(points) 
					from dbo.History_Stage
					where tipnumber=@TipNumber
					and histdate between @StartDate and @EndDate
					and trancode=@TranCode)

	return isnull(@points, 0)

END
GO
