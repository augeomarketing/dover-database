USE [234]
GO
/****** Object:  Table [dbo].[input_Transactions]    Script Date: 08/13/2010 14:54:00 ******/
DROP TABLE [dbo].[input_Transactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[input_Transactions](
	[DDA] [varchar](20) NULL,
	[Cardnumber] [varchar](20) NULL,
	[Junka] [varchar](50) NULL,
	[TranCount_Sig] [varchar](20) NULL,
	[TranAmt_Sig] [varchar](20) NULL,
	[Junkb] [varchar](50) NULL,
	[TranCount_PIN] [varchar](20) NULL,
	[TranAmt_PIN] [varchar](20) NULL
) ON [PRIMARY]
GO
