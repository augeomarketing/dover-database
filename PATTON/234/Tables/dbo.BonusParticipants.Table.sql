USE [234]
GO
/****** Object:  Table [dbo].[BonusParticipants]    Script Date: 08/13/2010 14:54:00 ******/
ALTER TABLE [dbo].[BonusParticipants] DROP CONSTRAINT [FK_BonusParticipants_BonusType]
GO
ALTER TABLE [dbo].[BonusParticipants] DROP CONSTRAINT [DF_BonusParticipants_dim_BonusParticipants_DateAdded]
GO
DROP TABLE [dbo].[BonusParticipants]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BonusParticipants](
	[sid_BonusType_Id] [int] NOT NULL,
	[dim_BonusParticipants_TipNumber] [varchar](15) NOT NULL,
	[dim_BonusParticipants_EffectiveDate] [datetime] NULL,
	[dim_BonusParticipants_ExpirationDate] [datetime] NULL,
	[dim_BonusParticipants_DateAdded] [datetime] NULL,
	[dim_BonusParticipants_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusParticipants] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC,
	[dim_BonusParticipants_TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BonusParticipants]  WITH NOCHECK ADD  CONSTRAINT [FK_BonusParticipants_BonusType] FOREIGN KEY([sid_BonusType_Id])
REFERENCES [dbo].[BonusType] ([sid_BonusType_Id])
GO
ALTER TABLE [dbo].[BonusParticipants] CHECK CONSTRAINT [FK_BonusParticipants_BonusType]
GO
ALTER TABLE [dbo].[BonusParticipants] ADD  CONSTRAINT [DF_BonusParticipants_dim_BonusParticipants_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusParticipants_DateAdded]
GO
