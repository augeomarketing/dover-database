USE [234]
GO
/****** Object:  View [dbo].[vw_histpoints]    Script Date: 08/13/2010 14:54:02 ******/
DROP VIEW [dbo].[vw_histpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vw_histpoints] 

as 

select tipnumber, sum(points * ratio) as points 
from dbo.history_stage 
where secid = 'NEW' 
group by tipnumber
GO
