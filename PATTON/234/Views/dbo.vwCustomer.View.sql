USE [234]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 08/13/2010 14:54:02 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [219DeanBank].dbo.customer
GO
