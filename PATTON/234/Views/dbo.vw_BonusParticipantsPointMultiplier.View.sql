USE [234]
GO
/****** Object:  View [dbo].[vw_BonusParticipantsPointMultiplier]    Script Date: 08/13/2010 14:54:02 ******/
DROP VIEW [dbo].[vw_BonusParticipantsPointMultiplier]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_BonusParticipantsPointMultiplier]
as

select dim_BonusParticipants_TipNumber, dim_BonusProgram_PointMultiplier, dim_BonusProgram_EffectiveDate, dim_BonusProgram_ExpirationDate
from dbo.BonusParticipants bp join dbo.BonusProgram pgm
	on bp.sid_BonusType_Id = pgm.sid_BonusType_Id
GO
