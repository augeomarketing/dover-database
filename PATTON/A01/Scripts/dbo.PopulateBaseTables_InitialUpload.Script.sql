USE [A01];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[CUSTOMER]([TIPNUMBER], [RunAvailable], [RUNBALANCE], [RunRedeemed], [LastStmtDate], [NextStmtDate], [STATUS], [DATEADDED], [LASTNAME], [TIPFIRST], [TIPLAST], [ACCTNAME1], [ACCTNAME2], [ACCTNAME3], [ACCTNAME4], [ACCTNAME5], [ACCTNAME6], [ADDRESS1], [ADDRESS2], [ADDRESS3], [ADDRESS4], [City], [State], [ZipCode], [StatusDescription], [HOMEPHONE], [WORKPHONE], [BusinessFlag], [EmployeeFlag], [SegmentCode], [ComboStmt], [RewardsOnline], [NOTES], [BonusFlag], [Misc1], [Misc2], [Misc3], [Misc4], [Misc5], [RunBalanceNew], [RunAvaliableNew])
SELECT N'A01000000000001', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'1142', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000002', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'3749', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000003', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'2038551234', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'13414', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000004', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'33314', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000005', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'2038551234', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'46795', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000006', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'46803', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000007', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'2038551234', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'91117', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000008', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'91360', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000009', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'x', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'2038551234', N'2038551234', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'93482', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000010', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'94571', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000011', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'2038551234', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'101838', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000012', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'2038551234', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'109957', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000013', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Patty Sinisko', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'1234546789', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'110716', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000014', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'111369', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000015', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'124050', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000016', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'124160', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000017', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Dave ', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'362416', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000018', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Zoe Eisenberg', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434108', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000019', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Sheila Featherston', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434109', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000020', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Randi Vannuchi', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434110', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000021', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Paris Cheffer', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434111', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000022', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Nick Caruso', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434112', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000023', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Michael Schindel', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434113', NULL, NULL, 0, 0 UNION ALL
SELECT N'A01000000000024', 0, 0, 0, NULL, NULL, N'A', '20110518 00:00:00.000', NULL, N'A01', NULL, N'Anne Kraft', N'', N'', N'', NULL, NULL, N'69 East Avenue', N'', N'', NULL, N'Norwalk', N'CT', N'06851', N'Active[A]', N'', N'', N'N', N'N', NULL, NULL, NULL, NULL, NULL, N'1', N'0', N'434114', NULL, NULL, 0, 0
COMMIT;
RAISERROR (N'[dbo].[CUSTOMER]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO


BEGIN TRANSACTION;
INSERT INTO [dbo].[Client]([ClientCode], [ClientName], [Description], [TipFirst], [Address1], [Address2], [Address3], [Address4], [City], [State], [Zipcode], [Phone1], [Phone2], [ContactPerson1], [ContactPerson2], [ContactEmail1], [ContactEmail2], [DateJoined], [RNProgramName], [TermsConditions], [PointsUpdatedDT], [MinRedeemNeeded], [TravelFlag], [MerchandiseFlag], [TravelIncMinPoints], [MerchandiseBonusMinPoints], [MaxPointsPerYear], [PointExpirationYears], [ClientID], [Pass], [ServerName], [DbName], [UserName], [Password], [PointsExpire], [LastTipNumberUsed], [PointsExpireFrequencyCd], [ClosedMonths], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [logo], [landing], [termspage], [faqpage], [earnpage], [Business], [StatementDefault], [StmtNum], [CustomerServicePhone])
SELECT N'RISMedia', N'RIS Media', N'RIS Media', N'A01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20110430 00:00:00.000', N'RewardsNOW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, N'A01', N'Test', NULL, NULL, NULL, NULL, NULL, N'A01000000000024', N'YE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[Client]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO


BEGIN TRANSACTION;
INSERT INTO [dbo].[PointsExpireFrequency]([PointsExpireFrequencyCd], [PointsExpireFrequencyNm])
SELECT N'ME', N'Month End' UNION ALL
SELECT N'MM', N'Mid Month' UNION ALL
SELECT N'YE', N'Year End'
COMMIT;
RAISERROR (N'[dbo].[PointsExpireFrequency]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO
