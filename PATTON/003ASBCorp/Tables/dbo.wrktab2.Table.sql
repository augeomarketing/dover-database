USE [ASBCorp]
GO
/****** Object:  Table [dbo].[wrktab2]    Script Date: 01/12/2010 08:19:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab2]') AND type in (N'U'))
DROP TABLE [dbo].[wrktab2]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktab2](
	[DDA_NUMBER] [nvarchar](15) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
