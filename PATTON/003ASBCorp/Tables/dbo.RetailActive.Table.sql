USE [ASBCorp]
GO
/****** Object:  Table [dbo].[RetailActive]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailActive]') AND type in (N'U'))
DROP TABLE [dbo].[RetailActive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailActive]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RetailActive](
	[Tipnumber] [varchar](15) NOT NULL,
	[Segmentcode] [char](2) NULL,
	[histdate] [varchar](10) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[Purchases] [int] NOT NULL,
	[Bonuses] [int] NOT NULL,
	[Runavailable] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
