USE [ASBCorp]
GO
/****** Object:  Table [dbo].[CombineWithBIN]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CombineWithBIN]') AND type in (N'U'))
DROP TABLE [dbo].[CombineWithBIN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CombineWithBIN]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CombineWithBIN](
	[PRIMARYTip] [char](15) NULL,
	[SECONDARYTip] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
