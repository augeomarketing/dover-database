USE [ASBCorp]
GO
/****** Object:  Table [dbo].[CorpFlashDouble]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorpFlashDouble]') AND type in (N'U'))
DROP TABLE [dbo].[CorpFlashDouble]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorpFlashDouble]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CorpFlashDouble](
	[ACCT_NBR] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[CRDHLDR_NME] [nvarchar](255) NULL,
	[REW_PLN] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
