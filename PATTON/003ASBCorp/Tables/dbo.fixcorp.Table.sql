USE [ASBCorp]
GO
/****** Object:  Table [dbo].[fixcorp]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixcorp]') AND type in (N'U'))
DROP TABLE [dbo].[fixcorp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixcorp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixcorp](
	[acctname1] [varchar](40) NOT NULL,
	[number] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
