USE [ASBCorp]
GO
/****** Object:  Table [dbo].[OnlHistory]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__OnlHistor__Trans__71D1E811]') AND parent_object_id = OBJECT_ID(N'[dbo].[OnlHistory]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__OnlHistor__Trans__71D1E811]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OnlHistory] DROP CONSTRAINT [DF__OnlHistor__Trans__71D1E811]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND type in (N'U'))
DROP TABLE [dbo].[OnlHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OnlHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[TransID] [int] NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](15) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__OnlHistor__Trans__71D1E811]') AND parent_object_id = OBJECT_ID(N'[dbo].[OnlHistory]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__OnlHistor__Trans__71D1E811]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[OnlHistory] ADD  CONSTRAINT [DF__OnlHistor__Trans__71D1E811]  DEFAULT (0) FOR [TransID]
END


End
GO
