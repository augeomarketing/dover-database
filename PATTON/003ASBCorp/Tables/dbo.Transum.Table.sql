USE [ASBCorp]
GO
/****** Object:  Table [dbo].[Transum]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transum]') AND type in (N'U'))
DROP TABLE [dbo].[Transum]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transum]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transum](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[NUMPURCH] [nchar](6) NULL,
	[AMTPURCH] [decimal](8, 0) NULL,
	[NUMCR] [nchar](6) NULL,
	[AMTCR] [decimal](8, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [decimal](5, 0) NULL,
	[cardtype] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
