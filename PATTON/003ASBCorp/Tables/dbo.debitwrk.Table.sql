USE [ASBCorp]
GO
/****** Object:  Table [dbo].[debitwrk]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[debitwrk]') AND type in (N'U'))
DROP TABLE [dbo].[debitwrk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[debitwrk]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[debitwrk](
	[DDANUM] [nvarchar](11) NULL,
	[STATUS] [nvarchar](1) NULL
) ON [PRIMARY]
END
GO
