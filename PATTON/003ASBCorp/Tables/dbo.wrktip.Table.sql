USE [ASBCorp]
GO
/****** Object:  Table [dbo].[wrktip]    Script Date: 01/12/2010 08:19:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktip]') AND type in (N'U'))
DROP TABLE [dbo].[wrktip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktip]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktip](
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
