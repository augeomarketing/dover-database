USE [ASBCorp]
GO
/****** Object:  Table [dbo].[BUS0606M$]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BUS0606M$]') AND type in (N'U'))
DROP TABLE [dbo].[BUS0606M$]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BUS0606M$]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BUS0606M$](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
END
GO
