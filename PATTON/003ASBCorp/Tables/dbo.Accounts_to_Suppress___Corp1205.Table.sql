USE [ASBCorp]
GO
/****** Object:  Table [dbo].[Accounts_to_Suppress___Corp1205]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts_to_Suppress___Corp1205]') AND type in (N'U'))
DROP TABLE [dbo].[Accounts_to_Suppress___Corp1205]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Accounts_to_Suppress___Corp1205]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Accounts_to_Suppress___Corp1205](
	[TRAVNUM] [nvarchar](255) NULL,
	[ACCTNAME1] [nvarchar](255) NULL,
	[ACCTNAME2] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[LASTLINE] [nvarchar](255) NULL,
	[STDATE] [nvarchar](255) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHS] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[ZIP] [nvarchar](255) NULL,
	[ACCT_NUM] [nvarchar](255) NULL,
	[LASTFOUR] [nvarchar](255) NULL,
	[CR_RTG] [nvarchar](255) NULL,
	[AGENT] [nvarchar](255) NULL,
	[NOTONTSYS] [nvarchar](255) NULL,
	[INVLDCR] [nvarchar](255) NULL,
	[INVLDAGNT] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
