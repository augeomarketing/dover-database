USE [ASBCorp]
GO
/****** Object:  Table [dbo].[testsarah]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[testsarah]') AND type in (N'U'))
DROP TABLE [dbo].[testsarah]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[testsarah]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[testsarah](
	[tipnumber] [varchar](15) NULL,
	[trancode] [varchar](2) NULL,
	[points] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
