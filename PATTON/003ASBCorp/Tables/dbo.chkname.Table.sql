USE [ASBCorp]
GO
/****** Object:  Table [dbo].[chkname]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkname]') AND type in (N'U'))
DROP TABLE [dbo].[chkname]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkname]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkname](
	[na1] [nvarchar](40) NULL,
	[tip] [varchar](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
