USE [ASBCorp]
GO
/****** Object:  Table [dbo].[CardsinRollUp]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CardsinRollUp]') AND type in (N'U'))
DROP TABLE [dbo].[CardsinRollUp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CardsinRollUp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CardsinRollUp](
	[na1] [nvarchar](40) NULL,
	[acctnum] [nvarchar](25) NULL,
	[numpurch] [int] NULL,
	[amtpurch] [float] NULL,
	[numcr] [int] NULL,
	[amtcr] [float] NULL
) ON [PRIMARY]
END
GO
