USE [ASBCorp]
GO
/****** Object:  Table [dbo].[fixexpiredpoints]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixexpiredpoints]') AND type in (N'U'))
DROP TABLE [dbo].[fixexpiredpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixexpiredpoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixexpiredpoints](
	[tipnumber] [nchar](15) NULL,
	[acctname1] [varchar](40) NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[address3] [varchar](40) NULL,
	[citystatezip] [varchar](50) NULL,
	[pointsshowntoexpire] [decimal](18, 0) NULL,
	[correctpointstoexpire] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
