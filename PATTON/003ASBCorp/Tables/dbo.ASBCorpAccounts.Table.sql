USE [ASBCorp]
GO
/****** Object:  Table [dbo].[ASBCorpAccounts]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASBCorpAccounts]') AND type in (N'U'))
DROP TABLE [dbo].[ASBCorpAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASBCorpAccounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASBCorpAccounts](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[zipcode] [varchar](15) NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
