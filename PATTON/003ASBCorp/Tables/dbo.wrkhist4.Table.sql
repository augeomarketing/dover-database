USE [ASBCorp]
GO
/****** Object:  Table [dbo].[wrkhist4]    Script Date: 01/12/2010 08:19:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist4]') AND type in (N'U'))
DROP TABLE [dbo].[wrkhist4]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist4]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkhist4](
	[tipnumber] [varchar](15) NULL,
	[acctname1] [char](40) NULL,
	[histdate] [datetime] NULL,
	[Deltipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
