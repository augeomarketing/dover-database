USE [ASBCorp]
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORYTIP_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORYTIP]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORYTIP_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORYTIP] DROP CONSTRAINT [DF_HISTORYTIP_Overage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORYTIP]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORYTIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORYTIP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Recnum] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Overage] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORYTIP_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORYTIP]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORYTIP_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORYTIP] ADD  CONSTRAINT [DF_HISTORYTIP_Overage]  DEFAULT (0) FOR [Overage]
END


End
GO
