USE [ASBCorp]
GO
/****** Object:  Table [dbo].[Customer_Closed_STAGE]    Script Date: 05/28/2010 14:17:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_Closed_STAGE]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_Closed_STAGE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_Closed_STAGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_Closed_STAGE](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
END
GO
