USE [ASBCorp]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg10]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg11]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT (0) FOR [MonthBeg1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT (0) FOR [MonthBeg2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT (0) FOR [MonthBeg3]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT (0) FOR [MonthBeg4]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT (0) FOR [MonthBeg5]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT (0) FOR [MonthBeg6]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT (0) FOR [MonthBeg7]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg8]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT (0) FOR [MonthBeg8]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT (0) FOR [MonthBeg9]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg10]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT (0) FOR [MonthBeg10]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg11]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT (0) FOR [MonthBeg11]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Beginning_Balance_Table_MonthBeg12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT (0) FOR [MonthBeg12]
END


End
GO
