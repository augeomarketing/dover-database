USE [ASBCorp]
GO
/****** Object:  Table [dbo].[CorpRollUp]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorpRollUp]') AND type in (N'U'))
DROP TABLE [dbo].[CorpRollUp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorpRollUp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CorpRollUp](
	[NA2] [nvarchar](25) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[numpurch] [int] NULL,
	[amtpurch] [float] NULL,
	[numcr] [int] NULL,
	[amtcr] [float] NULL
) ON [PRIMARY]
END
GO
