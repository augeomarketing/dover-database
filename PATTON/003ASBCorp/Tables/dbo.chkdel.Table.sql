USE [ASBCorp]
GO
/****** Object:  Table [dbo].[chkdel]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkdel]') AND type in (N'U'))
DROP TABLE [dbo].[chkdel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkdel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkdel](
	[tipnumber] [nvarchar](15) NULL,
	[acctnum] [nvarchar](25) NULL,
	[status] [nvarchar](1) NULL,
	[affilstatus] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
