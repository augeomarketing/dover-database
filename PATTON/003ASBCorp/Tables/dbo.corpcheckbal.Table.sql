USE [ASBCorp]
GO
/****** Object:  Table [dbo].[corpcheckbal]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corpcheckbal]') AND type in (N'U'))
DROP TABLE [dbo].[corpcheckbal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[corpcheckbal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[corpcheckbal](
	[f1] [nvarchar](255) NULL,
	[f2] [nvarchar](255) NULL,
	[F3] [nvarchar](255) NULL,
	[F4] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL,
	[f6] [nvarchar](255) NULL,
	[F7] [nvarchar](255) NULL,
	[F8] [nvarchar](255) NULL,
	[AIEA] [nvarchar](255) NULL,
	[HI] [nvarchar](255) NULL,
	[96701] [nvarchar](255) NULL,
	[D] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
