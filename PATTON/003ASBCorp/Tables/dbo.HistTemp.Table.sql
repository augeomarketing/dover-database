USE [ASBCorp]
GO
/****** Object:  Table [dbo].[HistTemp]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistTemp]') AND type in (N'U'))
DROP TABLE [dbo].[HistTemp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistTemp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistTemp](
	[SlNo] [decimal](18, 0) NULL,
	[TipNumber] [varchar](15) NULL,
	[Histdate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL,
	[TranCode] [varchar](2) NULL,
	[IncDec] [nvarchar](1) NULL,
	[TypeCode] [nvarchar](1) NULL,
	[ExpiryFlag] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
