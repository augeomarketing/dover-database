USE [ASBCorp]
GO
/****** Object:  Table [dbo].[Closed]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Closed]') AND type in (N'U'))
DROP TABLE [dbo].[Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Closed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Closed](
	[Col001] [char](3) NULL,
	[BANK] [char](4) NULL,
	[Col003] [char](2) NULL,
	[AGENT] [char](4) NULL,
	[ACCTNUM] [char](16) NULL,
	[Col006] [char](56) NULL,
	[Col007] [char](90) NULL,
	[Col008] [char](25) NULL,
	[Col009] [char](5) NULL,
	[Col010] [char](11) NULL,
	[Col011] [char](11) NULL,
	[Col012] [char](19) NULL,
	[Col013] [char](22) NULL,
	[Col014] [char](24) NULL,
	[Col015] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
