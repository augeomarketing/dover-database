USE [ASBCorp]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORY_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY] DROP CONSTRAINT [DF_HISTORY_Overage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND name = N'idx_history_tip_points_ratio')
CREATE NONCLUSTERED INDEX [idx_history_tip_points_ratio] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC,
	[POINTS] ASC,
	[Ratio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND name = N'IX_HISTORY')
CREATE NONCLUSTERED INDEX [IX_HISTORY] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND name = N'ix_history_trancode_ratio_points_tipnumber_histdate')
CREATE NONCLUSTERED INDEX [ix_history_trancode_ratio_points_tipnumber_histdate] ON [dbo].[HISTORY] 
(
	[TRANCODE] ASC,
	[Ratio] ASC,
	[POINTS] ASC,
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HISTORY_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HISTORY]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HISTORY_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HISTORY] ADD  CONSTRAINT [DF_HISTORY_Overage]  DEFAULT (0) FOR [Overage]
END


End
GO
