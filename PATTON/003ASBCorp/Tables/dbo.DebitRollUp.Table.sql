USE [ASBCorp]
GO
/****** Object:  Table [dbo].[DebitRollUp]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitRollUp]') AND type in (N'U'))
DROP TABLE [dbo].[DebitRollUp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitRollUp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DebitRollUp](
	[na1] [nvarchar](40) NULL,
	[acctnum] [nvarchar](25) NULL,
	[numpurch] [int] NULL,
	[amtpurch] [float] NULL,
	[numcr] [int] NULL,
	[amtcr] [float] NULL
) ON [PRIMARY]
END
GO
