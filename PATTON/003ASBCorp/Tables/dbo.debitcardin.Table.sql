USE [ASBCorp]
GO
/****** Object:  Table [dbo].[debitcardin]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[debitcardin]') AND type in (N'U'))
DROP TABLE [dbo].[debitcardin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[debitcardin]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[debitcardin](
	[MATCH1] [nvarchar](1) NULL,
	[MATCH2] [nvarchar](1) NULL,
	[MATCH3] [nvarchar](1) NULL,
	[RUN] [nvarchar](1) NULL,
	[TIPFIRST] [nvarchar](3) NULL,
	[TIPMID] [nvarchar](7) NULL,
	[TIPLAST] [nvarchar](5) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[TYPE] [nvarchar](2) NULL,
	[SSN] [nvarchar](9) NULL,
	[DDANUM] [nvarchar](11) NULL,
	[SAVNUM] [nvarchar](11) NULL,
	[BANK] [nvarchar](4) NULL,
	[CLIENT] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[OLDCC] [nvarchar](16) NULL,
	[NA1] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](30) NULL,
	[PERIOD] [nvarchar](14) NULL,
	[NA2] [nvarchar](40) NULL,
	[NA3] [nvarchar](40) NULL,
	[NA4] [nvarchar](40) NULL,
	[NA5] [nvarchar](40) NULL,
	[NA6] [nvarchar](40) NULL,
	[ADDR1] [nvarchar](40) NULL,
	[ADDR2] [nvarchar](40) NULL,
	[ADDR3] [nvarchar](40) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[ZIP] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[NUMPURCH] [nvarchar](9) NULL,
	[PURCH] [float] NULL,
	[NUMRET] [nvarchar](9) NULL,
	[AMTRET] [float] NULL,
	[RECORDER] [float] NULL,
	[TRANCODE] [char](3) NULL,
	[POINTS] [char](9) NULL,
	[Addr1Ext] [char](4) NULL,
	[Addr2Ext] [char](4) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
