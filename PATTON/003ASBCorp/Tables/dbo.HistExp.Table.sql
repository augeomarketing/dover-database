USE [ASBCorp]
GO
/****** Object:  Table [dbo].[HistExp]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistExp]') AND type in (N'U'))
DROP TABLE [dbo].[HistExp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistExp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistExp](
	[TIPNumber] [varchar](15) NOT NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Zipcode] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[TotEarned] [int] NULL,
	[RunRedemed] [int] NULL,
	[Available] [int] NULL,
	[LastStmtDT] [smalldatetime] NULL,
	[AcctID] [varchar](25) NULL,
	[DateAdded] [smalldatetime] NULL,
	[CardType] [varchar](20) NULL,
	[Descriptio] [nvarchar](40) NULL,
	[TranCode] [nvarchar](2) NULL,
	[Points] [decimal](18, 0) NULL,
	[HistDate] [smalldatetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
