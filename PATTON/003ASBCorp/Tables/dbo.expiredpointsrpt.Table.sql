USE [ASBCorp]
GO
/****** Object:  Table [dbo].[expiredpointsrpt]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[expiredpointsrpt]') AND type in (N'U'))
DROP TABLE [dbo].[expiredpointsrpt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[expiredpointsrpt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[expiredpointsrpt](
	[tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [char](40) NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[DateOfExpire] [nvarchar](12) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
