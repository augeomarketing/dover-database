USE [ASBCorp]
GO
/****** Object:  Table [dbo].[newactivitycheck]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[newactivitycheck]') AND type in (N'U'))
DROP TABLE [dbo].[newactivitycheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[newactivitycheck]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[newactivitycheck](
	[tipnumber] [varchar](15) NOT NULL,
	[dateadded] [datetime] NULL,
	[typecode] [char](10) NULL,
	[name1] [char](40) NULL,
	[name2] [char](40) NULL,
	[balance] [int] NULL,
	[historydate] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
