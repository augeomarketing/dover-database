USE [ASBCorp]
GO
/****** Object:  Table [dbo].[fixtrans]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixtrans]') AND type in (N'U'))
DROP TABLE [dbo].[fixtrans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixtrans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixtrans](
	[acctid] [varchar](25) NULL,
	[trancode] [varchar](2) NULL,
	[awarded] [decimal](38, 0) NULL,
	[shouldbe] [decimal](17, 0) NULL,
	[delta] [decimal](17, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
