USE [ASBCorp]
GO
/****** Object:  Table [dbo].[HistoryDeleted_Stage]    Script Date: 06/13/2010 11:39:57 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HistoryDeleted_Stage_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HistoryDeleted_Stage_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HistoryDeleted_Stage] DROP CONSTRAINT [DF_HistoryDeleted_Stage_Overage]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistoryDeleted_Stage](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](18, 0) NULL,
	[datedeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HistoryDeleted_Stage_Overage]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HistoryDeleted_Stage_Overage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HistoryDeleted_Stage] ADD  CONSTRAINT [DF_HistoryDeleted_Stage_Overage]  DEFAULT ((0)) FOR [Overage]
END


End
GO
