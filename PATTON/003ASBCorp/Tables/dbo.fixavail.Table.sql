USE [ASBCorp]
GO
/****** Object:  Table [dbo].[fixavail]    Script Date: 01/12/2010 08:19:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixavail]') AND type in (N'U'))
DROP TABLE [dbo].[fixavail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixavail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixavail](
	[tipnumber] [varchar](15) NULL,
	[avail] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
