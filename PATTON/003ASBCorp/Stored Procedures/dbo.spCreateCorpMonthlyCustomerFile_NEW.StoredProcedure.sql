USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spCreateCorpMonthlyCustomerFile_NEW]    Script Date: 04/23/2010 15:20:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateCorpMonthlyCustomerFile_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateCorpMonthlyCustomerFile_NEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateCorpMonthlyCustomerFile_NEW]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/**************************************************/
/* S Blanchette                                   */
/* 6/2010                                         */
/* Update status check to reflect new codes       */
/*                                                */
/**************************************************/

/***********************************************/
/* S Blanchette                                */
/* 11/10                                       */
/* Issue with Blocked code                     */
/* SEB001                                            */
/***********************************************/


CREATE PROCEDURE [dbo].[spCreateCorpMonthlyCustomerFile_NEW] 
	-- Add the parameters for the stored procedure here
	@DateIn varchar(10)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

DELETE FROM ASBIMP5CORP
WHERE NA2=''CORPORATE ACCOUNT''

--DELETE FROM ASBimp5Corp
--/* WHERE    BANK in ( ''0302'',''0311'',''0611'',''0620'',''0621'',''0622'',''0623'',''1621'') REMOVED 2/20/06 */
--where not exists(select * from ValidAgent where agent=asbimp5corp.bank)

update ASBIMP5Corp
set status = '' '' 
where left(status,1) in ( ''T'', ''F'', ''V'', ''M'', ''A'') /* SEB001 */
--where left(status,1) in ( ''T'',''L'',''F'', ''V'', ''M'', ''A'') SEB001
--where status in ( ''AV'',''LG'',''DR'', ''F1'')

DELETE FROM ASBIMP5Corp
WHERE    status <>'' '' and status is not null

update ASBIMP5Corp
set OLDCCNUM = null
where LEN(ltrim(rtrim(oldccnum))) = ''14''

update ASBIMP5Corp
set oldccnum = substring(acct_num, 1, 6) + substring(ltrim(oldccnum), 1,10)
where LEN(ltrim(rtrim(oldccnum))) = ''10''


/*                                         	                */
/*                                              	        */
/*  Begin logic to roll up multiple records into one by NA2     */
/*                                                         	*/
/*                                                        	*/
declare @TFNO nvarchar(19), @BANK nvarchar(4), @ACCT_NUM nvarchar(25), @OLDCCNUM nvarchar(16), @NA1 nvarchar(25), @NA2 nvarchar(25), @STATUS nvarchar(14), @NA3 nvarchar(32), @NA4 nvarchar(32), @NA5 nvarchar(32), @NA6 nvarchar(32), @CITYSTATE nvarchar(29), @ZIP nvarchar(9), @N12 nvarchar(32), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @NUMPURCH nvarchar(9), @AMTPURCH float, @NUMCR nvarchar(9), @AMTCR float, @STMTDATE nvarchar(14), @LASTNAME nvarchar(30), @oldpre char(8), @oldpost char(10)
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float

drop table CorpRollUp
DELETE FROM CorpRollUpa

SELECT NA2, ACCT_NUM, sum(cast (NUMPURCH as int)) as numpurch, SUM(AMTPURCH) as amtpurch, sum(cast (NUMCR as int)) as numcr, SUM(AMTCR) as amtcr into CorpRollUp 
FROM asbimp5corp
GROUP BY NA2, ACCT_NUM
ORDER BY NA2, ACCT_NUM desc 

/*                                                                            */
/* Setup Cursor for processing                                                */
declare asbimp5_crsr cursor
for select TFNO, BANK, ACCT_NUM, OLDCCNUM, NA1, NA2, STATUS, NA3, NA4, NA5, NA6, CITYSTATE, ZIP, N12, HOMEPHONE, WORKPHONE, NUMPURCH, AMTPURCH, NUMCR, AMTCR, STMTDATE, LASTNAME, OLDPRE, OLDPOST
from asbimp5CORP	
/*                                                                            */
open asbimp5_crsr
/*                                                                            */
fetch asbimp5_crsr into @TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @OLDPRE, @OLDPOST 

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if NOT EXISTS (SELECT * FROM CORPROLLUPA WHERE NA2 = @NA2 and ACCT_NUM=@acct_num)
	begin
		SELECT @sNUMPURCH=NUMPURCH, @sAMTPURCH=AMTPURCH, @sNUMCR=NUMCR, @sAMTCR=AMTCR  FROM CORPROLLUP WHERE NA2=@NA2 and ACCT_NUM=@acct_num
		set @NUMPURCH=@sNUMPURCH
		set @NUMCR=@sNUMCR 
		set @AMTPURCH=@sAMTPURCH
		set @AMTCR=@sAMTCR
		
		INSERT INTO corprollupa (TFNO, BANK, ACCT_NUM, OLDCCNUM, NA1, NA2, STATUS, NA3, NA4, NA5, NA6, CITYSTATE, ZIP, N12, HOMEPHONE, WORKPHONE, NUMPURCH, AMTPURCH, NUMCR, AMTCR, STMTDATE, LASTNAME, OLDPRE, OLDPOST)
		VALUES(@TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @OLDPRE, @OLDPOST )		        	
	end		        	

Next_Record:
	fetch asbimp5_crsr into @TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @OLDPRE, @OLDPOST 
	end

Fetch_Error:
close asbimp5_crsr
deallocate asbimp5_crsr

DELETE FROM asbimp5corp

insert into asbimp5corp
select *, '''' from corprollupa
order by na2
/*                                         	                */
/*  End logic to roll up multiple records into one by NA2       */
/*                                                        	*/

update ASBIMP5Corp
set NA1='''', na2=replace(na2,char(39), '' '')

/*  Logic gets account numbers from Customer table     */
--drop table chkname

--select acctname1, (select top 1 acctid from affiliat where tipnumber=customer.tipnumber and accttype<>''Debit'') as acctid
--into chkname
--from customer
--where tipnumber like ''003%'' and segmentcode=''C''

--update ASBIMP5Corp
--set acct_num=(select acctid from chkname where acctname1 = asbimp5corp.na2), OLDCCNUM=(select acctid from chkname where acctname1 = asbimp5corp.na2)

/*  Logic gets Month ending date and posts it to table */
declare @DTE varchar(10)
--set @DTE = @DateIn
set @DTE = @Datein

update ASBIMP5Corp
set stmtdate= @DTE, status=''A'', na1=na2, na2=''''

--delete from MissingAccountCorp
--insert into MissingAccountCorp
--elect * from ASBIMP5Corp
--where acct_num is null or acct_num = '' ''
END
' 
END
GO
