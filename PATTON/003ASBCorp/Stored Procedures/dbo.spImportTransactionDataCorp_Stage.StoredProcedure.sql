USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransactionDataCorp_Stage]    Script Date: 05/14/2010 14:58:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataCorp_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionDataCorp_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataCorp_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2010
-- Description:	Import transactions to stage
-- =============================================

/****************************************************/
/*  S Blanchette                                    */
/*  6/10                                            */
/*  Added code to reverse sign of AMTCR is negative */
/*  SCAN: SEB001                                    */
/****************************************************/
/***************************************************************/
/*  S Blanchette                                               */
/*  9/10                                                       */
/*  Added code to make sure tip exist before adding to history */
/*  SCAN: SEB002                                               */
/***************************************************************/

CREATE PROCEDURE [dbo].[spImportTransactionDataCorp_Stage]  
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

delete from transum
delete from cardsin2 where tipnumber is null

update cardsin2
set numpurch=''0''
where numpurch is null

update cardsin2
set purch=''0''
where purch is null

update cardsin2
set numret=''0''
where numret is null

update cardsin2
set amtret=''0''
where amtret is null

insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype)
select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype	
from cardsin2
group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype

/* SEB001 */
update transum
set amtcr = amtcr * -1
where amtcr<0

/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''63'', description=''Credit Card Purchase'', amtpurch=ROUND(AMTPURCH/100, 0)
where amtpurch>0 and cardtype=''C''

insert into history_Stage
select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, ''NEW'', ratio, overage
from transum
where trancode=''63'' and tipnumber in (select tipnumber from customer_stage) /* SEB002 */

/*********************************************************************/
/*  Process Credit Cards Returns                                     */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''33'', description=''Credit Card Returns'', amtcr=ROUND(AMTcr/100, 0)
where amtcr>0 and cardtype=''C''

insert into history_Stage
select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, ''NEW'', ratio, overage
from transum
where trancode=''33'' and tipnumber in (select tipnumber from customer_stage) /* SEB002 */

/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''67'', description=''Debit Card Purchase'', amtpurch=round(AMTPURCH/100,0)
where amtpurch>0 and cardtype=''D''

insert into history_Stage
select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, ''NEW'', ratio, overage
from transum
where trancode=''67'' and tipnumber in (select tipnumber from customer_stage) /* SEB002 */

/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''37'', description=''Debit Card Returns'', amtcr=ROUND(AMTcr/100, 0)
where amtcr>0 and cardtype=''D''

insert into history_Stage
select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, ''NEW'', ratio, overage
from transum
where trancode=''37'' and tipnumber in (select tipnumber from customer_stage) /* SEB002 */

Update dbo.CUSTOMER_STAGE 
	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
		RunAvailable		= isnull(RunAvailable, 0),
		runbalance		= isnull(runbalance, 0)

update dbo.Customer_Stage 
	set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber=dbo.Customer_Stage.tipnumber),	
	runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber=dbo.Customer_Stage.tipnumber),
	RunAvaliableNew = (select sum(amtpurch-amtcr) from transum where tipnumber=dbo.Customer_Stage.tipnumber)
where exists(select * from transum where tipnumber=dbo.Customer_Stage.tipnumber)

END
' 
END
GO
