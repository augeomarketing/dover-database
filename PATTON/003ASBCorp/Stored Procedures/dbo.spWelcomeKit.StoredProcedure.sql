USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 01/12/2010 08:19:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spWelcomeKit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spWelcomeKit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spWelcomeKit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10)
AS 

delete from WelcomeKit

insert into Welcomekit
SELECT     TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5), SegmentCode, (select top 1 custid from affiliat where tipnumber=customer.tipnumber) as DDA
FROM         CUSTOMER
WHERE     (DATEADDED = @EndDate AND STATUS <> ''c'')
' 
END
GO
