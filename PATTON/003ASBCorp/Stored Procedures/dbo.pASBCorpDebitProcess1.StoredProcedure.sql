USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pASBCorpDebitProcess1]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBCorpDebitProcess1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBCorpDebitProcess1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBCorpDebitProcess1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[pASBCorpDebitProcess1]
as  

/*           Set Tip First                                                       */
update DCIn
set tipfirst=''003''
where left(DEBIT_CARD_NUMBER,6) in (''414578'') 

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/


delete from DCIN
where DDA_NUMBER is Null or DDA_NUMBER=''00000000000''

/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
declare @DDANUM nchar(11), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @ADDR1 nvarchar(40)

update dcin
set [name_2]=null

delete from ddaname

declare debitcardin_crsr cursor
for select DDA_NUMBER, [NAME_1]
from DCIN
/* where ddanum is not null */
/* for update */ 
/*                                                                            */
open debitcardin_crsr
/*                                                                            */
fetch debitcardin_crsr into @DDANUM, @NA1
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from ddaname where DDANUM=@ddanum)
	begin
		insert ddaname(ddanum, na1) values(@ddanum, @na1)
		goto Next_Record1
	end
	
	else
		if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na2))=''0'')
		begin
			update ddaname
			set na2=@na1
			where ddanum=@ddanum
			goto Next_Record1
		end
		else
			if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na3))=''0'')
			begin
				update ddaname
				set na3=@na1
				where ddanum=@ddanum
				goto Next_Record1
			end
			else
				if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na4))=''0'')
				begin
					update ddaname
					set na4=@na1
					where ddanum=@ddanum
					goto Next_Record1
				end
				else
					if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na5))=''0'')
					begin
						update ddaname
						set na5=@na1
						where ddanum=@ddanum
						goto Next_Record1
					end
					else
						if exists(select * from ddaname where ddanum=@ddanum and len(rtrim(na6))=''0'')
						begin
							update ddaname
							set na6=@na1
							where ddanum=@ddanum
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch debitcardin_crsr into @DDANUM, @NA1
end

Fetch_Error1:
close debitcardin_crsr
deallocate debitcardin_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update ddaname
set na2=null
where na2=na1

update ddaname
set na3=null
where na3=na1 or na3=na2

update ddaname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update ddaname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update ddaname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update ddaname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or len(rtrim(na1))=''0''

	update ddaname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or len(rtrim(na2))=''0''

	update ddaname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or len(rtrim(na3))=''0''

	update ddaname
	set na4=na5, na5=na6, na6=null
	where na4 is null or len(rtrim(na4))=''0''

	update ddaname
	set na5=na6, na6=null
	where na5 is null or len(rtrim(na5))=''0''

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

update DCIN
set DCIN.[NAME_1]=ddaname.na1, DCIN.[NAME_2]=ddaname.na2, DCIN.na3=ddaname.na3, DCIN.na4=ddaname.na4, DCIN.na5=ddaname.na5, DCIN.na6=ddaname.na6 
from DCIN, ddaname
where DCIN.dda_NUMBER=ddaname.ddanum


/*                                                                                 */
/* End of New code to replace cursor processing for rolling acct demographic data  */
/*                                                                                 */


/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update DCIN
set joint=''J''
where [NAME_2] is not null and len(rtrim([NAME_2]))<>''0''

update DCIN
set joint=''S''
where [NAME_2] is null or len(rtrim([NAME_2]))=''0''' 
END
GO
