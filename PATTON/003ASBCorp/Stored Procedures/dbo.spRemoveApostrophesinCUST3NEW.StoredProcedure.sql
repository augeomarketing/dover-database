USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveApostrophesinCUST3NEW]    Script Date: 01/12/2010 08:19:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveApostrophesinCUST3NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveApostrophesinCUST3NEW]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveApostrophesinCUST3NEW]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRemoveApostrophesinCUST3NEW] AS
update cust3new
set NA1=replace(na1,char(39), '' ''),na2=replace(na2,char(39), '' ''), NAmeacct3=replace(NAmeacct3,char(39), '' ''),NAmeacct4=replace(NAmeacct4,char(39), '' ''), NAmeacct5=replace(NAmeacct5,char(39), '' ''),NAmeacct6=replace(NAmeacct6,char(39), '' ''), lastname=replace(lastname,char(39), '' ''), na3=replace(na3,char(39), '' ''), na4=replace(na4,char(39), '' ''), na5=replace(na5,char(39), '' ''), citystate=replace(citystate,char(39), '' '')' 
END
GO
