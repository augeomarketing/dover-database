USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePointsExtract]    Script Date: 01/12/2010 08:19:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spExpirePointsExtract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spExpirePointsExtract]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spExpirePointsExtract]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spExpirePointsExtract] @DateOfExpire NVARCHAR(10)  AS   

--declare @DateOfExpire NVARCHAR(10) 
--SET @DateOfExpire = ''12/31/2007''
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME


set @ExpireDate = cast(@DateOfExpire as datetime)

SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 7
set @expirationdate = (rtrim(@intYear) + ''-'' + rtrim(@intmonth) + ''-'' + rtrim(@intday) +   '' 23:59:59.997'')


/*print ''year''
print @intYear
print ''month''
print @intmonth
print ''day''
print @intday
print ''@expirationdate''
print @expirationdate */

	TRUNCATE TABLE ExpiredPoints

	INSERT   into ExpiredPoints
	 SELECT tipnumber, sum(points * ratio) as addpoints,
	''0'' AS REDPOINTS, ''0'' AS POINTSTOEXPIRE, ''0'' as PREVEXPIRED, @ExpireDate as dateofexpire
	from history
	where histdate < @expirationdate and (trancode not like(''R%'') and
	     trancode <> ''IR''and trancode not like(''XP''))
	group by tipnumber


	UPDATE ExpiredPoints  
	SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  =''XP'') 
	  	 AND TIPNUMBER = ExpiredPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		  trancode  =''XP''
	  	 AND TIPNUMBER = ExpiredPoints.TIPNUMBER) 

	UPDATE ExpiredPoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  like(''R%'') or
   		  trancode = ''IR'')  
	  	 AND TIPNUMBER = ExpiredPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 (trancode  like(''R%'') or
   		  trancode = ''IR'')  
	  	 AND TIPNUMBER = ExpiredPoints.TIPNUMBER)


	UPDATE ExpiredPoints  
	SET POINTSTOEXPIRE = (ADDPOINTS + REDPOINTS + PREVEXPIRED),
	    dateofexpire = @ExpireDate


	UPDATE ExpiredPoints  
	SET POINTSTOEXPIRE = ''0''
	WHERE
	POINTSTOEXPIRE IS NULL 

	UPDATE ExpiredPoints  
	SET POINTSTOEXPIRE = ''0''
	WHERE
	POINTSTOEXPIRE < ''0'' 

	delete from ExpiredPoints 	
	WHERE
	POINTSTOEXPIRE = ''0''' 
END
GO
