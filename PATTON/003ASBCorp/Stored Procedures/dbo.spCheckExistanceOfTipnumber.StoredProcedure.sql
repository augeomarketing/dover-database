USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spCheckExistanceOfTipnumber]    Script Date: 01/12/2010 08:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckExistanceOfTipnumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCheckExistanceOfTipnumber]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckExistanceOfTipnumber]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spCheckExistanceOfTipnumber] @DBNAME char(100)
AS 

declare @TipNumber char(15), @SQLIf nvarchar(2000)

drop table chktip

set @SQLIF=''select tipnumber
		into chktip
		from cardsin
		where tipnumber is not null and not exists( SELECT * FROM '' + Quotename(@DBNAME) + N''.dbo.customer where tipnumber=cardsin.Tipnumber) and Status=''''A'''' ''
exec sp_executesql @SQLIF

declare cardsin_crsr cursor
for select tipnumber
from chktip
order by tipnumber

open cardsin_crsr
/*                                                                            */
fetch cardsin_crsr into @tipnumber
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
		exec spreactivatecustomerdata @Tipnumber, @DBNAME			
				
	fetch cardsin_crsr into @tipnumber
END

Fetch_Error:
close cardsin_crsr
deallocate cardsin_crsr' 
END
GO
