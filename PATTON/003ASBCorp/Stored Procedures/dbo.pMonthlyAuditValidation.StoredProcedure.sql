USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pMonthlyAuditValidation]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pMonthlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMonthlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @errmsg varchar(50) 

set @errmsg=''Ending Balances do not match''

delete from Monthly_Audit_ErrorFile

INSERT INTO Monthly_Audit_ErrorFile
select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsbonusMN, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
from Monthly_Statement_File a, 	Current_Month_Activity b
where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints' 
END
GO
