USE [ASBCorp]
GO

/****** Object:  StoredProcedure [dbo].[pASBDebitProcess1_NEW]    Script Date: 06/05/2013 07:38:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBDebitProcess1_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBDebitProcess1_NEW]
GO

USE [ASBCorp]
GO

/****** Object:  StoredProcedure [dbo].[pASBDebitProcess1_NEW]    Script Date: 06/05/2013 07:38:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/***************************************************/
/* S Blanchette                                    */
/* 6/2010                                          */
/* Change to handle multiple transaction records   */
/* SCAN SEB001                                     */
/***************************************************/

CREATE PROCEDURE [dbo].[pASBDebitProcess1_NEW] 
	-- Add the parameters for the stored procedure here
@Datein char(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

/*********  Begin SEB002  *************************/
update debitcardin
set addr1=rtrim(addr1) + rtrim(Addr1Ext), addr2=rtrim(addr2) + rtrim(Addr2Ext)
/*********  End SEB002  *************************/
 
/*           Set Tip First                                                       */


update DebitCardIn
set tipfirst='003'
--where left(AcctNum,6) in ('414579', '414580') 
where left(acctnum,6) in (select BIN from asbwork.dbo.ASB_BIN_Xref where Retail_Corp_Flag='C' and Type_Card_Flag='D' )  

delete from DebitCardIn
where tipfirst<>'003'  

/*           Convert Trancode                                                     */
update DebitCardIn
set Trancode='67 '
where trancode='001' 

update DebitCardIn
set Trancode='37 '
where trancode='004' 

/*    Put points in proper fields                                                    */
update debitcardin	
set numpurch='1', purch=points
where trancode='67'

update debitcardin	
set numret='1', amtret=points
where trancode='37'

/*    Replace DDANUM                                                    */
update debitcardin
set ddanum=savnum
where ddanum='00000000000'

/*    Set Status                                                 */
update debitcardin
set status='A'
where status in('A', 'I')

update debitcardin
set status='C'
where status in('C', 'D', 'R')

--ZERO OUT DEBIT EARNINGS FOR DEBIT DECONVERSION 05/01/2013
UPDATE	debitcardin
SET		PURCH = 0, AMTRET = 0
WHERE	TRANCODE in ('67','37')

/*    Remove records with no DDANUM                                             */
delete from debitcardin
where DDANUM is Null or DDANUM='00000000000'

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/
update debitcardin
--set period='10/31/2006', TIPMID='0000000'
set period=@Datein, TIPMID='0000000'


/******************************************************/
/* Section to make status code the same for like DDA# */
/******************************************************/

drop table DebitRollUp
DELETE FROM DebitRollUpA

update debitcardin
set numpurch='0'
where numpurch is null

update debitcardin
set purch='0'
where purch is null

update debitcardin
set numret='0'
where numret is null

update debitcardin
set amtret='0'
where amtret is null

/*************************************/
/* START SEB001                      */
/*************************************/

--SELECT na1, acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr 
--into DebitRollUp 
--FROM debitcardin
--GROUP BY na1, acctnum
--ORDER BY na1, acctnum

SELECT na1, acctnum, trancode, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr 
into DebitRollUp 
FROM debitcardin
GROUP BY na1, acctnum, trancode
ORDER BY na1, acctnum, trancode

/*************************************/
/* END SEB001                        */
/*************************************/

declare @OLDCCNUM nvarchar(16), @NA1 nvarchar(40), @NA2 nvarchar(40), @STATUS nvarchar(1), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @N12 nvarchar(32), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @NUMCR nvarchar(9), @AMTCR float 
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float, @ddanum char(11), @AMTPURCH float
declare @TIPFIRST nvarchar (3), @TIPMID nvarchar (7), @TIPLAST nvarchar (5), @TIPNUMBER nvarchar (15), @TYPE nvarchar (2), @SSN nvarchar (9), @SAVNUM nvarchar (11), @BANK nvarchar (4), @CLIENT nvarchar (11), @ACCTNUM nvarchar (25), @OLDCC nvarchar (16), @LASTNAME nvarchar (30), @PERIOD nvarchar (14), @ADDR1 nvarchar (40), @ADDR2 nvarchar (40), @ADDR3 nvarchar (40), @CITYSTATE nvarchar (40), @ZIP nvarchar (10), @PHONE1 nvarchar (10), @PHONE2 nvarchar (10), @NUMPURCH nvarchar (9), @PURCH float, @NUMRET nvarchar (9), @AMTRET float, @TRANCODE char (3), @POINTS char (6)

/*                                                                            */
/* Setup Cursor for processing                                                */
declare asbimp5_crsr cursor
for select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS
from debitcardin	
/*                                                                            */
open asbimp5_crsr
/*                                                                            */
fetch asbimp5_crsr into @TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @TYPE, @SSN, @DDANUM, @SAVNUM, @BANK, @CLIENT, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @TRANCODE, @POINTS

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
/*************************************/
/* START SEB001                      */
/*************************************/

	--if NOT EXISTS (SELECT * FROM DebitRollUpA WHERE NA1 = @NA1 and acctnum=@acctnum)
	--begin
		--SELECT @sNUMPURCH=NUMPURCH, @sAMTPURCH=AMTPURCH, @sNUMCR=NUMCR, @sAMTCR=AMTCR  FROM DebitRollUp WHERE NA1=@NA1 and acctnum=@acctnum
		--set @NUMPURCH=@sNUMPURCH
		--set @NUMCR=@sNUMCR 
		--set @AMTPURCH=@sAMTPURCH
		--set @AMTCR=@sAMTCR
		
		--INSERT INTO DebitRollUpa (TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
		--VALUES(@TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @TYPE, @SSN, @DDANUM, @SAVNUM, @BANK, @CLIENT, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @TRANCODE, @POINTS)		        	
	--end		        	

	if NOT EXISTS (SELECT * FROM DebitRollUpA WHERE NA1 = @NA1 and acctnum=@acctnum and trancode=@trancode)
	begin
		SELECT @sNUMPURCH=NUMPURCH, @sAMTPURCH=AMTPURCH, @sNUMCR=NUMCR, @sAMTCR=AMTCR  FROM DebitRollUp WHERE NA1=@NA1 and acctnum=@acctnum and trancode=@trancode
		set @NUMPURCH=@sNUMPURCH
		set @NUMCR=@sNUMCR 
		set @AMTPURCH=@sAMTPURCH
		set @AMTCR=@sAMTCR
		
		INSERT INTO DebitRollUpa (TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
		VALUES(@TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @TYPE, @SSN, @DDANUM, @SAVNUM, @BANK, @CLIENT, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @TRANCODE, @POINTS)		        	
	end		        	

/*************************************/
/* END SEB001                        */
/*************************************/

Next_Record:
	fetch asbimp5_crsr into @TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @TYPE, @SSN, @DDANUM, @SAVNUM, @BANK, @CLIENT, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @TRANCODE, @POINTS
	end

Fetch_Error:
close asbimp5_crsr
deallocate asbimp5_crsr

/*  Bring combined data back into DEBITCARDIN2 table */
truncate table debitcardin2

insert into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS 
from debitrollupa
order by na1

/* Clean up */
update debitcardin2
set NA2='', NA3='', NA4='', NA5='', NA6='', na1=replace(na1,char(39), ' '), addr1=replace(addr1,char(39), ' '), addr2=replace(addr2,char(39), ' '), addr3=replace(addr3,char(39), ' '), citystate=replace(citystate,char(39), ' ')

/*  Logic gets account numbers from Customer table     */
--drop table chkname

--select acctname1, (select top 1 acctid from affiliat where tipnumber=customer.tipnumber) as acctid
--into chkname
--from customer

/* load Account number into debitcardin */
--update debitcardin2
--set acctnum=(select acctid from chkname where acctname1 = debitcardin2.na1), OLDCC=(select acctid from chkname where acctname1 = debitcardin2.na1)
--where exists (select acctid from chkname where acctname1 = debitcardin2.na1)
END

GO


