USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spVendorAwardBonusFileProcessCorp_Stage]    Script Date: 05/14/2010 14:58:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spVendorAwardBonusFileProcessCorp_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spVendorAwardBonusFileProcessCorp_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spVendorAwardBonusFileProcessCorp_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2010
-- Description:	Add Bonus for Mall Networks trans
-- =============================================
CREATE PROCEDURE [dbo].[spVendorAwardBonusFileProcessCorp_Stage] 
	-- Add the parameters for the stored procedure here
	@startDateParm varchar(10), 
	@EndDateParm varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

	delete from asbwork.dbo.transumbonus

	insert into asbwork.dbo.transumbonus (tipnumber, amtdebit)
	select [dim_rollup_tip], [dim_rollup_amount]	
	from MallNetworksFTP.dbo.rollup
	where [dim_rollup_created]>=@Startdate and dim_rollup_created<=@Enddate and [dim_rollup_tip] like ''003%''

	update asbwork.dbo.transumbonus
	set ratio=''1'', trancode=''FA'', description=''Merchant Reward'', histdate=@EndDateParm, numdebit=''1'', overage=''0''

	insert into dbo.HISTORY_STAGE
	select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ''NEW'', ratio, overage
	from asbwork.dbo.transumbonus

	Update dbo.Customer_Stage 
		Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
			RunAvailable		= isnull(RunAvailable, 0),
			runbalance		= isnull(runbalance, 0)

	update dbo.Customer_Stage
		set runavailable = runavailable + (select sum(amtdebit) from asbwork.dbo.transumbonus where tipnumber=dbo.Customer_Stage.tipnumber), 
		runbalance = runbalance + (select sum(amtdebit) from asbwork.dbo.transumbonus where tipnumber=dbo.Customer_Stage.tipnumber),
		RunAvaliableNew = RunAvaliableNew + (select sum(amtdebit) from asbwork.dbo.transumbonus where tipnumber=dbo.Customer_Stage.tipnumber)
	where exists(select tipnumber from asbwork.dbo.transumbonus where tipnumber=CUSTOMER_STAGE.tipnumber)

END

' 
END
GO
