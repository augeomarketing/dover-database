USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spLoad_Debitcardin]    Script Date: 04/23/2010 15:20:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoad_Debitcardin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoad_Debitcardin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoad_Debitcardin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 4/2010
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoad_Debitcardin]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		Truncate table [dbo].[debitcardin]

	INSERT INTO [dbo].[debitcardin]
           ([MATCH1]
           ,[MATCH2]
           ,[MATCH3]
           ,[RUN]
           ,[TIPFIRST]
           ,[TIPMID]
           ,[TIPLAST]
           ,[TIPNUMBER]
           ,[TYPE]
           ,[SSN]
           ,[DDANUM]
           ,[SAVNUM]
           ,[BANK]
           ,[CLIENT]
           ,[ACCTNUM]
           ,[OLDCC]
           ,[NA1]
           ,[LASTNAME]
           ,[PERIOD]
           ,[NA2]
           ,[NA3]
           ,[NA4]
           ,[NA5]
           ,[NA6]
           ,[ADDR1]
           ,[ADDR2]
           ,[ADDR3]
           ,[CITYSTATE]
           ,[ZIP]
           ,[STATUS]
           ,[PHONE1]
           ,[PHONE2]
           ,[NUMPURCH]
           ,[PURCH]
           ,[NUMRET]
           ,[AMTRET]
           ,[RECORDER]
           ,[TRANCODE]
           ,[POINTS]
           ,[Addr1Ext]
           ,[Addr2Ext])
     SELECT 
			 right(rtrim(Bank_Card_Type),1)
            ,'' ''
            ,'' ''
            ,'' ''
            ,'' ''
            ,'' ''
            ,'' ''
            ,'' ''
            ,'' ''
            ,[SSN]
		    ,right(rtrim([Account_Number]),11)
		    ,'' ''
		    ,'' ''
		    ,[Client-BIN]
			,[PAN]
			,[Old_PAN]
			,(rtrim([First_Name]) + '' '' + RTRIM([Last_Name]) )
			,'' ''
			,'' ''
			,'' ''
			,'' ''
			,'' ''
			,'' ''
			,'' ''
			,[Address1]
			,[Address2]
			,'' ''
	      ,(rtrim([City]) + '' '' + RTRIM([State]) )
		  ,[Zip]
		  ,[Status]
		  ,[Home_Phone]
		  ,[Work_Phone]
		  ,'' ''
		  ,'' ''
		  ,'' ''
		  ,'' ''
		  ,'' ''
	      ,[Tran_Code]
	      ,[Dollar_Amount]
          ,[Address1_Extension]
          ,[Address2_Extension]
      FROM [ASBWORK].[dbo].[ASB_Debit_Detail]

END

' 
END
GO
