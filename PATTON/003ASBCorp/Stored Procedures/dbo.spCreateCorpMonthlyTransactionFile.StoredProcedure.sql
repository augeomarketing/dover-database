USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spCreateCorpMonthlyTransactionFile]    Script Date: 01/12/2010 08:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateCorpMonthlyTransactionFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateCorpMonthlyTransactionFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateCorpMonthlyTransactionFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spCreateCorpMonthlyTransactionFile] @DateIn varchar(10) 
AS
/******************************************************************************/
/*                                                                            */
/*                   SQL TO PROCESS ASB Transactions                          */
/*                                                                            */
/* BY:  S.BLANCHETTE                                                          */
/* DATE: 3/2005                                                               */
/* REVISION: 0                                                                */
/*                                                                            */
/*                                                                            */
/******************************************************************************/	
declare @TFNO varchar(19), @BANK varchar(4), @ACCT_NUM varchar(25), @NUMPURCH varchar(9), @AMTPURCH float(8), @NUMCR varchar(9), @AMTCR float(8)
declare @oTFNO varchar(15), @oTRANDATE varchar(10), @oACCT_NUM varchar(25), @oTRANCODE varchar(2), @oTRANNUM varchar(4), @oTRANAMT nchar(15), @oNUMCR varchar(9), @oAMTCR float(8), @oTRANTYPE varchar(20), @oRATIO varchar(4), @oCRDACTVLDT varchar(10)
declare @wrkamt float(8)
/*                                                                            */
DELETE Tran3NewCorp
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING COMB_IN TABLE                               */
/*                                                                            */

declare asbimp5_crsr cursor
for select TFNO, BANK, ACCT_NUM, NUMPURCH, AMTPURCH, NUMCR, AMTCR
from asbimp5Corp
for update
/*                                                                            */
open asbimp5_crsr
/*                                                                            */
fetch asbimp5_crsr into @TFNO, @BANK, @ACCT_NUM, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/* purchases */
	set @oTFNO=rtrim(@TFNO)
	set @oACCT_NUM=@ACCT_NUM
	set @oTRANNUM=rtrim(@NUMPURCH)
	set @wrkamt=ROUND(@AMTPURCH/100, 0)
	set @oTRANAMT=str(@wrkamt,15,0)
	set @oTRANDATE= @DateIn 
	set @oTRANCODE=''63''
	set @oTRANTYPE=''CREDIT''
	set @oRATIO=''1''
	set @oCRDACTVLDT=''''
	if @wrkamt>0
	begin
		INSERT INTO Tran3NewCorp (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		VALUES(@oTFNO, @oTRANDATE, @oACCT_NUM, @oTRANCODE, @oTRANNUM, @oTRANAMT, @oTRANTYPE, @oRATIO, @oCRDACTVLDT)		        	
	end		        	
/* credits */
	set @oTRANNUM=rtrim(@NUMCR)
	set @wrkamt=ROUND(@AMTCR/100, 0)
	set @oTRANAMT=str(@wrkamt,15,0)
	set @oTRANCODE=''33''
	set @oRATIO=''-1''
	
	if @wrkamt>0
	begin
	INSERT INTO Tran3NewCorp (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		VALUES(@oTFNO, @oTRANDATE, @oACCT_NUM, @oTRANCODE, @oTRANNUM, @oTRANAMT, @oTRANTYPE, @oRATIO, @oCRDACTVLDT)	
	end	        	

/******************************************************************************/	

Next_Record:
	fetch asbimp5_crsr into @TFNO, @BANK, @ACCT_NUM, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR 
	end

Fetch_Error:
close asbimp5_crsr
deallocate asbimp5_crsr

delete tran3newCorp where TRANAMT=0' 
END
GO
