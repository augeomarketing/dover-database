USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spMoveDeletedRecordstoHoldfiles]    Script Date: 01/12/2010 08:19:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveDeletedRecordstoHoldfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMoveDeletedRecordstoHoldfiles]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveDeletedRecordstoHoldfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[spMoveDeletedRecordstoHoldfiles] @Enddate char(10)
as

declare @datedeleted datetime
set  @datedeleted = @Enddate

INSERT INTO CustomerDeleted
       	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @datedeleted 
	FROM Customer where status=''C''

INSERT INTO AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, Datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, @datedeleted 
	FROM affiliat where exists(select * from customer where tipnumber=affiliat.tipnumber and status=''C'')

INSERT INTO historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, @datedeleted 
	FROM History where exists(select * from customer where tipnumber=history.tipnumber and status=''C'')

delete from affiliat
where exists(select * from customer where tipnumber=affiliat.tipnumber and status=''C'')

delete from history
where exists(select * from customer where tipnumber=history.tipnumber and status=''C'')

delete from customer 
where status=''C''' 
END
GO
