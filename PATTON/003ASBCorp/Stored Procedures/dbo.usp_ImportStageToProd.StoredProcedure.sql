USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImportStageToProd]    Script Date: 05/14/2010 14:58:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ImportStageToProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ImportStageToProd]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ImportStageToProd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/*************************************************/
/* S Blanchette                                  */
/* 6/2010                                        */
/* Add Custid and Secid to affiliat update       */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 7/2010                                        */
/* Add Custid and Secid to affiliat insert       */
/* SEB002                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 3/2011                                        */
/* Added code to zero out RunBalance in stage    */
/* SEB003                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 11/2011                                        */
/* Added code to remove customer closed records  */
/*   that are not in customer_closed_stage       */
/* SEB004                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 12/2011                                        */
/* Added code to 0 out the RunavailableNew field */
/*  in CUSTOMER after the points have been       */
/*  updated to take care of the inserted recs    */
/* SEB005                                        */
/*************************************************/

CREATE PROCEDURE [dbo].[usp_ImportStageToProd] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Declare @dbName varchar(50) 
	Declare @SQLStmt nvarchar(2000) 

	/*    Get Database name                                      */
	set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )


	------------------------- Customer ----------------------- 
	---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
	Update c
		Set 
		Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
		Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
		Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
		Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
		City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
		Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
		Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
		Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
	From dbo.Customer_Stage S Join dbo.Customer C 
	On S.Tipnumber = C.Tipnumber

	-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
/***************************/
/* START SEB003            */
/***************************/
	--Update dbo.Customer_Stage 	
	--	Set RunAvailable = 0 

	Update dbo.Customer_Stage 	
		Set RunAvailable = 0, RunBalance=0
/***************************/
/* END SEB003            */
/***************************/

	--	Insert New Customers from Customers_Stage
	Insert into Customer 
		select stg.*
		from Customer_Stage stg left outer join dbo.Customer cus
		on stg.tipnumber = cus.tipnumber
		where cus.Tipnumber is null

	--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
	Update C 
		set RunBalance = C.RunBalance + S.RunAvaliableNew,
			  RunAvailable = C.RunAvailable + S.RunAvaliableNew
	From Customer_Stage S Join Customer C 
		On S.Tipnumber = C.Tipnumber



	----------------------- Affiliat ----------------------- 
	--	Update Existing accounts with YTDEarned from Affiliat_Stage
	Update a
		set YTDEarned = S.YTDEarned,
			custid = s.custid,
			secid = s.secid,
			AcctStatus = s.acctstatus  
	from dbo.Affiliat_Stage S join dbo.Affiliat A 
		on S.Tipnumber = A.Tipnumber and s.acctid = a.acctid

	--	Insert New Affiliat accounts
	Insert into dbo.Affiliat 
		select stg.* 
		from dbo.Affiliat_Stage stg left outer join dbo.Affiliat aff
			on stg.tipnumber = aff.tipnumber and stg.acctid = aff.acctid /* SEB002 */
		where aff.Tipnumber is null



	----------------------- HISTORY ----------------------- 
	-- History  Insert New Records from History_Stage to History  .
	Insert Into dbo.History 
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	from dbo.History_Stage 
	where SecID = ''NEW''

	-- Set SecID in History_Stage so it doesn''t get double posted. 
	Update dbo.History_Stage  
		set SECID = ''Imported to Production ''+ convert(char(20), GetDate(), 120)  
	where SecID = ''NEW''

	----------------------- OneTimeBonuses ----------------------- 
	-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
	insert into dbo.OneTimeBonuses 
	select stg.* 
	from dbo.OneTimeBonuses_stage stg left outer join dbo.OneTimeBonuses otb
		on stg.TipNumber = otb.tipnumber
		and stg.trancode = otb.trancode
	where otb.tipnumber is null

	---------------------- Customer Closed -------------------------------
		--	Insert New Customer Closed accounts
	Insert into  dbo.Customer_Closed
		select stg.* 
		from dbo.Customer_Closed_Stage stg left outer join dbo.Customer_Closed cls
			on stg.tipnumber = cls.tipnumber
		where cls.Tipnumber is null

	-- Truncate Stage Tables so''s we don''t double post.
	delete from dbo.OneTimeBonuses_stage
	delete from dbo.History_Stage
	delete from dbo.Affiliat_Stage
	delete from dbo.Customer_Stage

	declare @datedeleted datetime
	
	set @datedeleted=(select top 1 datedeleted from dbo.customerpurge)

	INSERT INTO dbo.CustomerDeleted (TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, datedeleted)
	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @datedeleted 
	FROM dbo.Customer 
	where Tipnumber in (Select Tipnumber from dbo.customerPurge)
	/* */
	/* */
	INSERT INTO dbo.AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
	FROM dbo.affiliat 
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	INSERT INTO dbo.historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, @datedeleted
	FROM dbo.history
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	Delete from dbo.affiliat 
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	Delete from dbo.history 
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	Delete from dbo.customer 
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	Delete from dbo.[Beginning_Balance_Table]
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	Delete from dbo.OneTimeBonuses 
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
	/* */
	/* */
	Delete from dbo.Customer_Closed
	where Tipnumber in (Select Tipnumber from dbo.customerPurge) 
/***************************/
/* START SEB004            */
/***************************/
	delete from dbo.customer_closed
	where tipnumber not in (select tipnumber from dbo.Customer_Closed_Stage)
/***************************/
/* END SEB004            */
/***************************/
	/* */
	--Delete from dbo.customerPurge
	/* */
	
/***************************/
/* START SEB005            */
/***************************/
	Update dbo.Customer 	
		Set RunAvaliableNew = 0
/***************************/
/* END SEB005            */
/***************************/

END

' 
END
GO
