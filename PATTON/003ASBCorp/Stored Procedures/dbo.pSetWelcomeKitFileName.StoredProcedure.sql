USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pSetWelcomeKitFileName]    Script Date: 01/12/2010 08:19:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetWelcomeKitFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(100), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(4), @endingDate char(10)
declare @NumRecs numeric, @SQLSelect nvarchar(1000)

set @endingDate=(select top 1 datein from asbwork.dbo.DateforAudit)


set @SQLSelect=''set @NumRecs=(select count(*) from Welcomekit) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

--set @filename=''W'' + @TipPrefix + @currentdate + ''.xls''
/* SEB001 */set @filename=''W-Kit003_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''
 
set @newname=''O:\'' + @TipPrefix + ''\Output\Welcomekits\Welcome.bat '' + @filename
set @ConnectionString=''O:\'' + @TipPrefix + ''\Output\Welcomekits\'' + @filename' 
END
GO
