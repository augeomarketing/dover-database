USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransactionDataCorp]    Script Date: 01/12/2010 08:19:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataCorp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionDataCorp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataCorp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spImportTransactionDataCorp] 
AS 

delete from transum
delete from cardsin2 where tipnumber is null

update cardsin2
set numpurch=''0''
where numpurch is null

update cardsin2
set purch=''0''
where purch is null

update cardsin2
set numret=''0''
where numret is null

update cardsin2
set amtret=''0''
where amtret is null

insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype)
select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype	
from cardsin2
group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype


/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''63'', description=''Credit Card Purchase'', amtpurch=ROUND(AMTPURCH/100, 0)
where amtpurch>0 and cardtype=''C''

insert into history
select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
from transum
where trancode=''63''

/*********************************************************************/
/*  Process Credit Cards Returns                                     */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''33'', description=''Credit Card Returns'', amtcr=ROUND(AMTcr/100, 0)
where amtcr>0 and cardtype=''C''

insert into history
select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
from transum
where trancode=''33''

/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''67'', description=''Debit Card Purchase'', amtpurch=round(AMTPURCH/100,0)
where amtpurch>0 and cardtype=''D''

insert into history
select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
from transum
where trancode=''67''

/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''37'', description=''Debit Card Returns'', amtcr=ROUND(AMTcr/100, 0)
where amtcr>0 and cardtype=''D''

insert into history
select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
from transum
where trancode=''37''

update customer
set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber=customer.tipnumber), runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber=customer.tipnumber)
where exists(select * from transum where tipnumber=customer.tipnumber)' 
END
GO
