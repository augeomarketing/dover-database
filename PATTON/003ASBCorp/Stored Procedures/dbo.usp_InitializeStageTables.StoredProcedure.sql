USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[usp_InitializeStageTables]    Script Date: 05/14/2010 14:58:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InitializeStageTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InitializeStageTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InitializeStageTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InitializeStageTables]
	-- Add the parameters for the stored procedure here
	@StartDate		datetime
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

delete from dbo.history_stage
delete from dbo.affiliat_stage
delete from dbo.customer_stage
delete from dbo.OneTimeBonuses_Stage
delete from dbo.customer_closed_stage
delete from dbo.customerPurge

insert into dbo.customer_stage
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
select TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
from dbo.customer

insert into dbo.customer_closed_stage
(Tipnumber, DateClosed, DateToDelete)
select Tipnumber, DateClosed, DateToDelete
from dbo.customer_closed

insert into dbo.affiliat_stage
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
from dbo.affiliat

insert into onetimebonuses_stage
(TipNumber, Trancode, AcctID, DateAwarded)
select TipNumber, Trancode, AcctID, DateAwarded
from dbo.onetimebonuses

insert into dbo.history_stage
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, ''OLD'', Ratio, Overage
from dbo.history
where histdate >= @startdate


END
' 
END
GO
