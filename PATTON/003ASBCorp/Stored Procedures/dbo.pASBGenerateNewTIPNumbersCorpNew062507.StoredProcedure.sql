USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pASBGenerateNewTIPNumbersCorpNew062507]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBGenerateNewTIPNumbersCorpNew062507]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBGenerateNewTIPNumbersCorpNew062507]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBGenerateNewTIPNumbersCorpNew062507]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to employ last tip used instead of max tip
--
CREATE PROCEDURE [dbo].[pASBGenerateNewTIPNumbersCorpNew062507] 
AS

declare @newnum bigint, @DBName varchar(50), @a_TipPrefix nchar(3), @newtip char(15)

/* Process RETAIL cards */
--SELECT @newnum = max(TIPNUMBER) from affiliat /* SCAN: SEB001 */
--if @newnum is null/* SCAN: SEB001 */
--	begin
--	set @newnum=''003000000000000''/* SCAN: SEB001 */
--	end/* SCAN: SEB001 */
--set @newnum = @newnum + 1/* SCAN: SEB001 */

/*********  Begin SEB001  *************************/
set @DBName=''ASBCorp''
set @a_TipPrefix=''003''

declare @LastTipUsed char(15)

exec rewardsnow.dbo.spGetLastTipNumberUsed @a_TipPrefix, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @Newnum = cast(@LastTipUsed as bigint) + 1  
/*********  End SEB001  *************************/

drop table chkname

select distinct na1, ''               '' as tip
into chkname
from cardsin2
where tipnumber is null or len(rtrim(tipnumber)) = 0 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tip
from chkname
where left(tip,1)='' '' 
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @NewTip = Cast(@Newnum as char) /*SEB001 */
		set @NewTip = replicate(''0'',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip /*SEB001*/
           					
		update chkname
		--set tipnumber = ''00'' + @newnum 
		--set tip=''00'' + substring(convert(varchar(15),@newnum),1,15) /* SCAN: SEB001 */	
		set tip=@NewTip /*SEB001 */
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update cardsin2
set tipnumber=b.tip
from cardsin2 a, chkname b
where a.na1=b.na1

exec RewardsNOW.dbo.spPutLastTipNumberUsed @a_TipPrefix, @NewTip  /*SEB001 */' 
END
GO
