USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pDealWithClosedCardsASB]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pDealWithClosedCardsASB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pDealWithClosedCardsASB]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pDealWithClosedCardsASB]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pDealWithClosedCardsASB] @datedeleted char(10)
AS 

--update affiliat
--set acctstatus=''C''
--where exists(select acctnum from closed where acctnum=affiliat.acctid)

update customer
set status=''C''
where status=''A'' and not exists(select * from affiliat where tipnumber=customer.tipnumber and acctstatus=''A'')

INSERT INTO AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
	FROM affiliat where acctstatus=''C''

delete from affiliat
where acctstatus=''C''' 
END
GO
