USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pMergeCARDINrecords]    Script Date: 01/12/2010 08:19:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMergeCARDINrecords]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pMergeCARDINrecords]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMergeCARDINrecords]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[pMergeCARDINrecords]
as  

drop table CardsinRollUp
truncate table cardsinRollUpA


SELECT na1,acctnum,  sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr 
into CardsinRollUp
FROM Cardsin
GROUP BY na1, acctnum
ORDER BY na1

declare @OLDCCNUM nvarchar(16), @NA1 nvarchar(40), @NA2 nvarchar(40), @STATUS nvarchar(1), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @N12 nvarchar(32), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @NUMCR nvarchar(9), @AMTCR float 
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float, @ddanum char(11), @AMTPURCH float, @cardtype char(1)
declare @TIPFIRST nvarchar (3), @TIPMID nvarchar (7), @TIPLAST nvarchar (5), @TIPNUMBER nvarchar (15), @TYPE nvarchar (2), @SSN nvarchar (9), @SAVNUM nvarchar (11), @BANK nvarchar (4), @CLIENT nvarchar (11), @ACCTNUM nvarchar (25), @OLDCC nvarchar (16), @LASTNAME nvarchar (30), @PERIOD nvarchar (14), @ADDR1 nvarchar (40), @ADDR2 nvarchar (40), @ADDR3 nvarchar (40), @CITYSTATE nvarchar (40), @ZIP nvarchar (10), @PHONE1 nvarchar (10), @PHONE2 nvarchar (10), @NUMPURCH nvarchar (9), @PURCH float, @NUMRET nvarchar (9), @AMTRET float, @TRANCODE char (3), @POINTS char (6)

/*                                                                            */
/* Setup Cursor for processing                                                */
declare asbimp5_crsr cursor
for select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, SSN, DDANUM, BANK, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, cardtype
from cardsin	
order by na1, cardtype
/*                                                                            */
open asbimp5_crsr
/*                                                                            */
fetch asbimp5_crsr into @TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @SSN, @DDANUM, @BANK, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @cardtype

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if NOT EXISTS (SELECT * FROM cardsinRollUpA WHERE NA1 = @NA1 and acctnum=@acctnum)
	begin
		SELECT @sNUMPURCH=NUMPURCH, @sAMTPURCH=AMTPURCH, @sNUMCR=NUMCR, @sAMTCR=AMTCR  FROM cardsinRollUp WHERE NA1=@NA1 and acctnum=@acctnum
		set @NUMPURCH=@sNUMPURCH
		set @NUMCR=@sNUMCR 
		set @AMTPURCH=@sAMTPURCH
		set @AMTCR=@sAMTCR
		
		INSERT INTO cardsinRollUpa (TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, SSN, DDANUM, BANK, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, Cardtype)
		VALUES(@TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @SSN, @DDANUM, @BANK, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @cardtype)		        	
	end		        	

Next_Record:
	fetch asbimp5_crsr into @TIPFIRST, @TIPMID, @TIPLAST, @TIPNUMBER, @SSN, @DDANUM, @BANK, @ACCTNUM, @OLDCC, @NA1, @LASTNAME, @PERIOD, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CITYSTATE, @ZIP, @STATUS, @PHONE1, @PHONE2, @cardtype
	end

Fetch_Error:
close asbimp5_crsr
deallocate asbimp5_crsr

/*  Bring combined data back into DEBITCARDIN2 table */
truncate table cardsin2

insert into cardsin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, SSN, DDANUM, BANK, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype)
select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, SSN, DDANUM, BANK, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, Cardtype 
from cardsinrollupa
order by na1, cardtype

/* Clean up */
update cardsin2
set NA2='''', NA3='''', NA4='''', NA5='''', NA6='''', na1=replace(na1,char(39), '' ''), addr1=replace(addr1,char(39), '' ''), addr2=replace(addr2,char(39), '' ''), addr3=replace(addr3,char(39), '' ''), citystate=replace(citystate,char(39), '' '')

/*  Logic gets account numbers from Customer table     */
--drop table chkname

--select acctname1, (select top 1 acctid from affiliat where tipnumber=customer.tipnumber) as acctid
--into chkname
--from customer

/* load Account number into debitcardin */
--update cardsin2
--set acctnum=(select acctid from chkname where acctname1 = cardsin2.na1), OLDCC=(select acctid from chkname where acctname1 = cardsin2.na1)
--where exists (select acctid from chkname where acctname1 = cardsin2.na1)' 
END
GO
