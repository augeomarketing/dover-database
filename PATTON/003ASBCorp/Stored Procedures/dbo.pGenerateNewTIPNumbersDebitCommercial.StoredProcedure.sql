USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateNewTIPNumbersDebitCommercial]    Script Date: 01/12/2010 08:19:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGenerateNewTIPNumbersDebitCommercial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGenerateNewTIPNumbersDebitCommercial]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGenerateNewTIPNumbersDebitCommercial]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGenerateNewTIPNumbersDebitCommercial] @tipin char(3)
AS 

declare @newnum bigint

SELECT @newnum = max(TIPNUMBER) from affiliat where left(tipnumber,3)=@tipin
if @newnum is null
	begin
	set @newnum=@Tipin + ''000000000000''
	end
set @newnum = @newnum + 1

delete from DCIN2

insert into DCIN2 
select * from DCIN
order by tipfirst, DDA_NUMBER, NAME_1, NAME_2, joint

drop table wrktab2 

select distinct DDA_NUMBER, tipnumber
into wrktab2
from DCIN2
where tipfirst=@tipin and tipnumber is null 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from wrktab2
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update wrktab2	
		set tipnumber = ''00'' + substring(convert(char(13), @newnum),1,13) 
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update DCIN2
set tipnumber=(select tipnumber from wrktab2 where DDA_NUMBER=DCIN2.DDA_NUMBER)
where DDA_NUMBER is not null and tipnumber is null

delete from DCIN

insert into DCIN 
select * from DCIN2
order by tipfirst, tipnumber, DDA_NUMBER, joint' 
END
GO
