USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pASBLoadCardsinDEBIT]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBLoadCardsinDEBIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBLoadCardsinDEBIT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBLoadCardsinDEBIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pASBLoadCardsinDEBIT]
as 


/* Debit cards */
insert into Cardsin(tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, cardtype)
select tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, ''D'' from debitcardin2' 
END
GO
