USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pASBGenerateNewTIPNumbersCorp]    Script Date: 01/12/2010 08:19:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBGenerateNewTIPNumbersCorp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBGenerateNewTIPNumbersCorp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBGenerateNewTIPNumbersCorp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pASBGenerateNewTIPNumbersCorp] 
AS

declare @newnum bigint

/* Process RETAIL cards */
SELECT @newnum = max(TIPNUMBER) from affiliat 
if @newnum is null
	begin
	set @newnum=''003000000000000''
	end
set @newnum = @newnum + 1

drop table chkname

select distinct na1, ''               '' as tip
into chkname
from cardsin2
where tipnumber is null or len(rtrim(tipnumber)) = 0 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tip
from chkname
where left(tip,1)='' '' 
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update chkname
		--set tipnumber = ''00'' + @newnum 
		set tip=''00'' + substring(convert(varchar(15),@newnum),1,15)	
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update cardsin2
set tipnumber=b.tip
from cardsin2 a, chkname b
where a.na1=b.na1' 
END
GO
