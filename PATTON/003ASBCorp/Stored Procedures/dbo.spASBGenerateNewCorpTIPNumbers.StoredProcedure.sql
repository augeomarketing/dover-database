USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spASBGenerateNewCorpTIPNumbers]    Script Date: 01/12/2010 08:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBGenerateNewCorpTIPNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spASBGenerateNewCorpTIPNumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBGenerateNewCorpTIPNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spASBGenerateNewCorpTIPNumbers]
 AS

drop table affil
select * into affil from affiliat where tipnumber like ''003%''

update asbimp5Corp
set tfno = (select TIPNUMBER from affil where acctid = acct_num)

update asbimp5Corp
set tfno = (select TIPNUMBER from affil where acctid = oldccnum)
where tfno is null or len(rtrim(tfno)) = 0 

delete from asbimp5Corp 
where (tfno is null or len(rtrim(tfno)) = 0) and status = ''C''

declare @newnum bigint
SELECT @newnum = max(TIPNUMBER) from affil 
set @newnum = @newnum + 1
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING ASBIMP5Corp TABLE                         */
/*                                                                            */
declare tip_crsr cursor
for select tfno
from asbimp5Corp
WHERE (tfno is null or len(rtrim(tfno)) = 0)
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update asbimp5Corp	
		set tfno = ''00'' + substring(convert(char(13), @newnum),1,13) 
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr' 
END
GO
