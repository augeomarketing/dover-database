USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[spImportCustomerDataCorp]    Script Date: 01/12/2010 08:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerDataCorp]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportCustomerDataCorp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerDataCorp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spImportCustomerDataCorp]
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CUSTOMER AND TRANSACTION DATA                         */
/*                                                                            */
/******************************************************************************/


/***********************************************************************/
/*  BY: S Blanchette                                                   */
/*  DATE: 6/10                                                         */
/*  REASON:  Added SSN to the update statement for the affiliat table  */
/*                                                                     */
/***********************************************************************/

declare @NA1 char(40), @NA2 char(40), @ADDR1 char(40), @ADDR2 char(40), @ADDR3 char(40), @CityState char(40),
 @CityLength nchar(2), @CSSTAT char(2), @ZIP nchar(9), @CSHFON nchar(10), @CSBFON nchar(10), 
 @DMSTATUS char(1), @ACCTNO nchar(16), @PRODCT char(20), @CARDNO char(16), @NUMCREDIT nchar(6), @AMTCREDIT float(8), @NUMDEBIT nchar(6), @AMTDEBIT float(8), @Tipnumber varchar(15), @dateadded nchar(10), @lastname varchar(40),
 @NA3 varchar(40), @NA4 varchar(40), @NA5 varchar(40), @NA6 varchar(40), @proddesc varchar(50), @ssn char(9), @DDANUM char(11), @Ctype char(1), @StatusDesc varchar(50)

delete from cardsin2 where tipnumber is null

update cardsin2
set citystate=replace(citystate,char(44), '''')

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING RNFILE TABLE                         */
/*                                                                            */
declare RNFILE_crsr cursor
for select TIPNUMBER, SSN, DDANUM, ACCTNUM, NA1, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, PERIOD, LASTNAME, Cardtype
from cardsin2
order by tipnumber, cardtype asc, status desc
/*                                                                            */
open RNFILE_crsr
/*                                                                            */
fetch RNFILE_crsr into @Tipnumber, @ssn, @ddanum, @cardno, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CityState, @zip, @DMstatus, @CSHFON, @CSBFON, @NUMDEBIT, @AMTDEBIT, @NUMCREDIT, @AMTCREDIT, @dateadded, @lastname, @Ctype
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
	if @ctype=''C''
	Begin
		Set @prodct=''Credit''
	end
	else
		Begin
			set @prodct=''Debit''
		end
	set @proddesc=(select Accttypedesc from accttype where accttype=@prodct)
	set @StatusDesc=(select StatusDescription from Status where Status=@DMStatus)
 		if not exists (select tipnumber from customer where tipnumber=@tipnumber)
			begin
			INSERT INTO Customer (TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, Status, StatusDescription, HomePhone, WorkPhone, SegmentCode, DateAdded, Lastname)
       			values(@tipnumber, ''0'', ''0'', ''0'', left(@tipnumber,3), right(@tipnumber,12), @NA1, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, (rtrim(@CityState) + '' '' + rtrim(@zip)), left(@citystate,(len(@citystate)-3)), right(rtrim(@citystate),2), @ZIP, ''A'', ''Active[A]'', @CSHFON, @CSBFON, @Ctype, @DateAdded, @lastname) 
			end
 		else 
			begin
			update customer
			--set AcctName1=@NA1, AcctName2=@NA2, AcctName3=@NA3, AcctName4=@NA4, AcctName5=@NA5, AcctName6=@NA6, Address1=@ADDR1, Address2=@ADDR2, Address4=@CityState, City=left(@citystate,(len(@citystate)-3)), State=right(rtrim(@citystate),2), Zipcode=@ZIP, HomePhone=@CSHFON, WorkPhone=@CSBFON, lastname= @lastname, status=@DMstatus, StatusDescription=@StatusDesc 
			set AcctName1=@NA1, AcctName2=@NA2, AcctName3=@NA3, AcctName4=@NA4, AcctName5=@NA5, AcctName6=@NA6, Address1=@ADDR1, Address2=@ADDR2, Address4=(rtrim(@CityState) + '' '' + rtrim(@zip)), City=left(@citystate,(len(@citystate)-3)), State=right(rtrim(@citystate),2), Zipcode=@ZIP, HomePhone=@CSHFON, WorkPhone=@CSBFON, lastname= @lastname 
			where tipnumber=@tipnumber
			end	
/*   AFFILIAT TABLE                      */
/*                                       */
/* Will populate the following           */
/*  DDA#(Account#)  goes into CUSTID               */
/*  SSN goes into  SECID      */
/*  Card# goes into ACCTID    */
/*                                       */
		if not exists (select tipnumber from affiliat where acctid= @cardno)
			begin
			INSERT INTO affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, CustID)
       			values(@cardno, @tipnumber, @prodct, @dateadded, @ssn, ''A'', @proddesc, @lastname, @ddanum) 
			end
 		else 
			begin
			update affiliat
			set lastname=@lastname, AcctStatus=@DMstatus, SECID = @ssn
			where tipnumber=@tipnumber and acctid=@cardno
			end	
				
goto Next_Record
Next_Record:
		fetch RNFILE_crsr into @Tipnumber, @ssn, @ddanum, @cardno, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CityState, @zip, @DMstatus, @CSHFON, @CSBFON, @NUMDEBIT, @AMTDEBIT, @NUMCREDIT, @AMTCREDIT, @dateadded, @lastname, @Ctype
end
Fetch_Error:
close  rnfile_crsr
deallocate  rnfile_crsr' 
END
GO
