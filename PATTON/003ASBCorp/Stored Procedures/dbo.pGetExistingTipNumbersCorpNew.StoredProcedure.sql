USE [ASBCorp]
GO
/****** Object:  StoredProcedure [dbo].[pGetExistingTipNumbersCorpNew]    Script Date: 01/12/2010 08:19:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetExistingTipNumbersCorpNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetExistingTipNumbersCorpNew]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetExistingTipNumbersCorpNew]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGetExistingTipNumbersCorpNew] 
AS

update cardsin2
set na2='' ''
where na2 is null

update cardsin2
set na3='' ''
where na3 is null

update cardsin2
set na4='' ''
where na4 is null

update cardsin2
set na5='' ''
where na5 is null

update cardsin2
set na6='' ''
where na6 is null

/*  Get tipnumber based on account number  CORP */
update cardsin2
set tipnumber=affiliat.tipnumber
from cardsin, affiliat
where (cardsin2.Tipnumber is null or len(rtrim(cardsin2.tipnumber)) = 0) and cardsin2.acctnum is not null and left(cardsin2.acctnum,1) not in(''0'', '' '', ''9'') and cardsin2.acctnum=affiliat.acctid 

/*  Get tipnumber based on old account number  CORP */
update cardsin2
set tipnumber=affiliat.tipnumber
from cardsin2, affiliat
where (cardsin2.Tipnumber is null or len(rtrim(cardsin2.tipnumber)) = 0) and cardsin2.oldcc is not null and left(cardsin2.oldcc,1) not in(''0'', '' '', ''9'')  and cardsin2.oldcc=affiliat.acctid

/*  Get tipnumber based on account number  CORP */
update cardsin2
set tipnumber=customer.tipnumber
from cardsin, customer
where (cardsin2.Tipnumber is null or len(rtrim(cardsin2.tipnumber)) = 0) and cardsin2.na1 is not null  and cardsin2.na1=customer.acctname1 

/*************************************************************************************/
/*                                                                                   */
/*  ADDED  3/13/2007  SEB  to check deleted tables if not tipnumber found in the     */
/*                         Normal tables                                             */
/*                                                                                   */
/*************************************************************************************/
/* Removed below code SEB001 because of problems */

/*  Get tipnumber based on account number  CORP */
/*update cardsin2
set tipnumber=affiliatdeleted.tipnumber
from cardsin, affiliatdeleted
where (cardsin2.Tipnumber is null or len(rtrim(cardsin2.tipnumber)) = 0) and cardsin2.acctnum is not null and left(cardsin2.acctnum,1) not in(''0'', '' '', ''9'') and cardsin2.acctnum=affiliatdeleted.acctid  */

/*  Get tipnumber based on old account number  CORP */
/* update cardsin2
set tipnumber=affiliatdeleted.tipnumber
from cardsin2, affiliatdeleted
where (cardsin2.Tipnumber is null or len(rtrim(cardsin2.tipnumber)) = 0) and cardsin2.oldcc is not null and left(cardsin2.oldcc,1) not in(''0'', '' '', ''9'')  and cardsin2.oldcc=affiliatdeleted.acctid */

/*  Get tipnumber based on account number  CORP */
/* update cardsin2
set tipnumber=customerdeleted.tipnumber
from cardsin, customerdeleted
where (cardsin2.Tipnumber is null or len(rtrim(cardsin2.tipnumber)) = 0) and cardsin2.na1 is not null  and cardsin2.na1=customerdeleted.acctname1  */

delete from cardsin2
where tipnumber is null and status<>''A''' 
END
GO
