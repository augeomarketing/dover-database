USE [540DanversITunes]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPCombineTips]    Script Date: 09/25/2009 11:20:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                   SQL TO PROCESS  COMBINES                          */
/*                                                                         */
/* BY:  R.Tremblay                                          */
/* DATE: 8/2006                                                               */
/* REVISION: 1                                                                */
/* This creates a new tip. Copies the pri customer to customerdeleted.
 Copies sec customer to customerdeleted. Changes  pri customer tip  to new tip
 Changes all pri affiliat records to new Tip. Changes all pri history records to new Tip.
 Changes all sec affiliat records to new Tip. Changes all sec history records to new Tip.
 Add secondary TIP values (runavailable, runbalance, etc) to NEW TIP 
 Deletes sec customer                                                           
 Added code to call spCombineBeginningBalance
 Added code to combine Bonus tale transactions 10/10/06
 Altered Table by adding OldTipPoints column to track the number of points transferred from the old tip
 Added code to update the points transferred 
 10/3/06 - Added OldTipRank = P = Primary S = Secondary
 Input file must be ordered by PRITIP to check for duplicates. 
 NEW Tables needed 
	metavantework.dbo.workcombine
	COMB_err
THE FOLLOWING TABLE NEEDS TO BE CREATED ON PATTON AND RN1
	Comb_TipTracking
BRAEK LOGIC REPEATED AFTER WHILE STATEMENT TO PROCESS LAST RECORD AFTER EOF    BJQ 12/27/2006
WRITE TO COMB_ERR CHANGED TO ADD NAME1 AND NAME2 TO THE OUTPUT REC             BJQ 12/28/2006
                                                                                                 */
/******************************************************************************/	
/******************************************************************************/	
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to employ last tip used instead of max tip
/******************************************************************************/	
 CREATE PROCEDURE [dbo].[spFBOPCombineTips] @a_TipPrefix nchar(3) /* SEB001 */AS    
declare @a_TIPPRI char(15), @a_TIPSEC char(15), @a_TIPBREAK char(15), @a_errmsg char(80)
declare @a_namep char(40), @a_namep1 char(40), @a_names char(40), @a_names1 char(40), @a_addrp char(40), @a_addrs char(40)     
declare @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int
declare @DateToday datetime
declare @NewTip as char(15), @NewTipValue bigint, @DeleteDescription as char(40)
declare @Old_TipPri nvarchar(15)
declare @newnum bigint, @DBName varchar(50) /* SEB001 */
SET @DateToday = GetDate()
SET @a_TIPBREAK = '0'
set @Old_TipPri = '0'
/******************************************************************************/	
/*  Delete null tips                                */
delete from metavantework.dbo.workcombine where PrimaryTip is null
delete from metavantework.dbo.workcombine where SecondaryTip is null
/******************************************************************************/	
/*  Delete tips  from metavantework.dbo.workcombine if there is an error message               */
--delete from metavantework.dbo.workcombine where Len( RTrim( errmsg) ) > 0 
/******************************************************************************/	
/*  DECLARE CURSOR FOR PROCESSING metavantework.dbo.workcombine TABLE                               */
declare combine_crsr cursor for 
	select PrimaryTip, SecondaryTip, ERRMSG
	from metavantework.dbo.workcombine 
	order by PrimaryTip, SecondaryTip
open combine_crsr
fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg 
/******************************************************************************/	
/* MAIN PROCESSING                                                */
/******************************************************************************/	
if @@FETCH_STATUS = 1
	goto Fetch_Error
while @@FETCH_STATUS = 0
begin	
	IF @a_TIPPRI <> @Old_TipPri
	   set @NewTip = '0'
	/* Check for existance of  Primary and secondary tips. */ 
	IF @a_TIPPRI <> @Old_TipPri
	 begin
	  IF  not EXISTS ( SELECT tipnumber FROM Customer WHERE TIPnumber = @a_TIPPRI )	  
		begin
		Insert into comb_err (TIP_PRI, TIP_SEC, TRANDATE, errmsg, errmsg2)
		Values (@a_TIPPRI, @a_TIPSEC, @DateToday,  @a_errmsg, 'Primary Tip not in Customer Table' )
		goto Next_Record
		end	   
	end
	IF  NOT EXISTS ( SELECT tipnumber FROM Customer WHERE TIPnumber = @a_TIPSEC )
	begin
		Insert into comb_err (TIP_PRI, TIP_SEC, TRANDATE, errmsg, errmsg2)
		Values (@a_TIPPRI, @a_TIPSEC,  @DateToday,  @a_errmsg, 'Secondary Tip not in Customer Table.' )
		goto Next_Record
	end
	/* Check for new primary tip. If new then create a new tip else use the same "new" tip */
	IF @a_TIPPRI <> @a_TIPBREAK 
	Begin
		/*    Create new tip          */
		/*********  Begin SEB001  *************************/
		--set @NewTipValue = ( select cast(max(tipnumber)as bigint) + 1 from customer )
		declare @LastTipUsed char(15)
		exec rewardsnow.dbo.spGetLastTipNumberUsed @a_TipPrefix, @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @NewTipValue = cast(@LastTipUsed as bigint) + 1  
		/*********  End SEB001  *************************/
		/*  Make sure the tipnumber is 15 character long by adding zeros to the begining of the Tip*/
		/* clients with tip prefix starting zeros will have max tips less than 15 characters */
/* 	Code replaced with Code Sarah recreated to pad with leading zeros where approiate  BJQ 12/2006 */
/*		set @NewTipValue = replicate('0',15 - Len( RTrim( @NewTipValue ) ) ) +  @NewTipValue */
/*		set @NewTip = Cast(@NewTipValue as char)                                              */ 
       		 set @NewTip = Cast(@NewTipValue as char)
           	 set @NewTip = replicate('0',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip 
		exec RewardsNOW.dbo.spPutLastTipNumberUsed @a_TipPrefix, @NewTip  /*SEB001 */
		/******************************************************************************/	
		/*  copy PRI customer to customerdeleted set description to new tipnumber     */
		/* IF the PRI tip is not repeated */
 
		set @DeleteDescription = 'Pri Combined to '  + @NewTip
		INSERT INTO CustomerDeleted
		(
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
		)
       		SELECT 
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
		FROM Customer where tipnumber = @a_TIPPRI
		/******************************************************************************/	
		/* Add Get PRI tip values from Customer  */
 
		select 	@a_RunAvailable =RunAvailable,  @a_RunRedeemed=RunRedeemed from customer where tipnumber = @a_TIPPRI
		/*  Insert record into Comb_TipTracking table  */
 
		Insert into Comb_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank) Values (@NewTip, @a_TIPPRI, @DateToday, @a_RunAvailable, @a_RunRedeemed,'P' )
	End --@a_TIPPRI <> @a_TIPBREAK 
	/******************************************************************************/	
	/*  copy SEC customer to customerdeleted set description to new tipnumber         */
	set @DeleteDescription = 'Sec Combined to '  + @NewTip
 
	INSERT INTO CustomerDeleted
	(
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	  
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
	)
       	SELECT 
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	  
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
	FROM Customer where tipnumber = @a_TIPSEC
	/******************************************************************************/	
	/* Add Get SEC  tip values from Customer  */
 
	select 	@a_RunAvailable =RunAvailable,  @a_RunRedeemed=RunRedeemed from customer where tipnumber = @a_TIPSEC
 
	/******************************************************************************/	
	/*  Insert record into Comb_TipTracking table  */
 
	Insert into Comb_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank) Values (@NewTip, @a_TIPSEC, @DateToday, @a_RunAvailable, @a_RunRedeemed,'S' )
	/******************************************************************************/	
	/*   change  PRI tip in customer to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK
	   begin
	   Update Customer set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI
	   SET @Old_TipPri = @a_TIPPRI
	   end 
	/* OLD_TIPPRI ADDED for check of next record in Fetch Statement. If The nhext Tipnumber to be combined */
        /* equals a_TIPPRI the next time thru thw while statement the program will not have to select for the */
	/* Customer Record because it has just been processed   BJQ 12/2006                */
	/******************************************************************************/	
	/*         Change all PRI affiliat records to new Tip              */
 
	IF @a_TIPPRI <> @a_TIPBREAK  Update AFFILIAT  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
	
	/******************************************************************************/	
	/*         Change all SEC affiliat records to new Tip              */
 
	update AFFILIAT set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPSEC
	/******************************************************************************/	
	/*       Change all PRI history records to new Tip          */
 
	IF @a_TIPPRI <> @a_TIPBREAK  Update HISTORY Set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPPRI
	/******************************************************************************/	
	/*       Change all SEC history records to new Tip          */
 
	update HISTORY set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPSEC
	/******************************************************************************/	
	/* Get and Add SEC tip values to Customer NEW TIP */
 
	select 
		@a_RunAvailable =RunAvailable, 
		@a_RunBalance=RunBalance, 
		@a_RunRedeemed=RunRedeemed 
	from customer where tipnumber = @a_TIPSEC
 
	update customer	
	set RunAvailable = RunAvailable + @a_RunAvailable, 
	     RunBalance=RunBalance + @a_RunBalance, 
 	     RunRedeemed=RunRedeemed + @a_RunRedeemed  
	where tipnumber = @NEWTIP
	
	/******************************************************************************/	
	/*    change  PRI tip in BEGINNING_BALANCE_TABLE to new tip number  (this avoids an insert)      */
 
	IF @a_TIPPRI <> @a_TIPBREAK  Update Beginning_Balance_Table set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI
	/******************************************************************************/	
	/* Add SEC tip values to Beginning_Balance_Table NEW TIP for each month */
 
	exec spCombineBeginningBalance @NewTip, @a_TIPSEC
	/******************************************************************************/	
	/* Delete SEC Beginning_Balance_Table                  */
 
	delete from Beginning_Balance_Table where tipnumber = @a_TIPSEC
	/******************************************************************************/	
	/* Delete SEC Customer                  */
 
	delete from CUSTOMER where tipnumber = @a_TIPSEC
	/******************************************************************************/	
	/*         Change all PRI & SEC BONUS records to new Tip              */
 
	If exists ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[OneTimeBonuses]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 )
	BEGIN
		Update OneTimeBonuses  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		Update OneTimeBonuses  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC
	END
Next_Record:
 
	Set @a_TIPBREAK = @a_TIPPRI
	fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg
	
end --while @@FETCH_STATUS = 0
Fetch_Error:
close combine_crsr
deallocate combine_crsr
GO
