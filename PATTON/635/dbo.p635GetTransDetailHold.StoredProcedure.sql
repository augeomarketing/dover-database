SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[p635GetTransDetailHold] @StartDate char(10), @EndDate char(10)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */


Truncate table TransDetailHold


Insert Into TransDetailHold (tdhID, TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID )
	select tdhID, TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID 
	from  COOPWork.dbo.TransDetailHold where PAN like '514324%' and trandate between @Startdate and @EndDate
GO
