USE [635SeaAir]
GO
/****** Object:  Table [dbo].[CreditWorkIn]    Script Date: 10/28/2010 11:10:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreditWorkIn]') AND type in (N'U'))
DROP TABLE [dbo].[CreditWorkIn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreditWorkIn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CreditWorkIn](
	[SID] [bigint] IDENTITY(1,1) NOT NULL,
	[TFNO] [nvarchar](19) NULL,
	[BANK] [nvarchar](4) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[OLDCCNUM] [nvarchar](16) NULL,
	[NA1] [nvarchar](25) NULL,
	[NA2] [nvarchar](25) NULL,
	[STATUS] [nvarchar](14) NULL,
	[NA3] [nvarchar](32) NULL,
	[NA4] [nvarchar](32) NULL,
	[NA5] [nvarchar](32) NULL,
	[NA6] [nvarchar](32) NULL,
	[CITYSTATE] [nvarchar](29) NULL,
	[ZIP] [nvarchar](9) NULL,
	[N12] [nvarchar](32) NULL,
	[HOMEPHONE] [nvarchar](10) NULL,
	[WORKPHONE] [nvarchar](10) NULL,
	[NUMPURCH] [nvarchar](9) NULL,
	[AMTPURCH] [float] NULL,
	[NUMCR] [nvarchar](9) NULL,
	[AMTCR] [float] NULL,
	[STMTDATE] [nvarchar](14) NULL,
	[FirstName1] [nvarchar](30) NULL,
	[LASTNAME1] [nvarchar](30) NULL,
	[FirstName2] [nvarchar](30) NULL,
	[LASTNAME2] [nvarchar](30) NULL,
	[oldpre] [char](8) NULL,
	[oldpost] [char](10) NULL,
	[SSN] [char](9) NULL,
 CONSTRAINT [PK_CreditWorkIn_1] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
