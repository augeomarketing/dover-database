USE [635SeaAir]
GO
/****** Object:  Table [dbo].[Credit_Detail]    Script Date: 10/28/2010 11:10:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credit_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Credit_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credit_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Credit_Detail](
	[PAN] [char](16) NULL,
	[Primary_Name] [char](25) NULL,
	[Secondary_Name] [char](25) NULL,
	[Address1] [char](36) NULL,
	[Address2] [char](36) NULL,
	[City_State] [char](27) NULL,
	[Zip] [char](9) NULL,
	[Cycle] [char](2) NULL,
	[Bank] [char](4) NULL,
	[Credit_Rating] [char](2) NULL,
	[Home_Phone] [char](10) NULL,
	[Work_Phone] [char](10) NULL,
	[Number_Purchases] [char](2) NULL,
	[Amount_Purchases] [char](8) NULL,
	[Number_Returns] [char](2) NULL,
	[Amount_Returns] [char](8) NULL,
	[Last_Statement_Date] [char](8) NULL,
	[Bank1] [char](4) NULL,
	[Transfer_Account] [char](14) NULL,
	[SSN] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
