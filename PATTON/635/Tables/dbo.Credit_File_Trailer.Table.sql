USE [635SeaAir]
GO
/****** Object:  Table [dbo].[Credit_File_Trailer]    Script Date: 10/28/2010 11:10:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credit_File_Trailer]') AND type in (N'U'))
DROP TABLE [dbo].[Credit_File_Trailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credit_File_Trailer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Credit_File_Trailer](
	[File_Type] [char](4) NULL,
	[Debit_Record_Count] [char](9) NULL,
	[Debit_Amount] [char](11) NULL,
	[Credit_Record_Count] [char](9) NULL,
	[Credit_Amount] [char](11) NULL,
	[Filler] [char](214) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
