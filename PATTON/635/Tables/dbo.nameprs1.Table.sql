USE [635SeaAir]
GO
/****** Object:  Table [dbo].[nameprs1]    Script Date: 11/01/2010 10:17:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nameprs1]') AND type in (N'U'))
DROP TABLE [dbo].[nameprs1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nameprs1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[nameprs1](
	[sid] [bigint] NULL,
	[CSNA1] [nvarchar](40) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
