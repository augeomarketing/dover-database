USE [635SeaAir]
GO
/****** Object:  Table [dbo].[Credit_Source]    Script Date: 10/28/2010 11:10:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credit_Source]') AND type in (N'U'))
DROP TABLE [dbo].[Credit_Source]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Credit_Source]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Credit_Source](
	[Col001] [varchar](500) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
