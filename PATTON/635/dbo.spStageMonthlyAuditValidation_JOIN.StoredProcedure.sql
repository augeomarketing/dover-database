SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spStageMonthlyAuditValidation_JOIN]
AS

TRUNCATE TABLE Monthly_Audit_ErrorFile

INSERT INTO Monthly_Audit_ErrorFile (Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsBonusCR, PointsAdded, PointsPurchasedDB, PointsBonusDB,  PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Errormsg, Currentend)
SELECT Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsBonusCR, PointsAdded, PointsPurchasedDB, PointsBonusDB,  PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased,  'Ending Balances do not match', (SELECT SUM(AdjustedEndingPoints) FROM Current_Month_Activity WHERE tipnumber = msf.tipnumber)
  FROM Monthly_Statement_File msf
  WHERE pointsend <> (SELECT SUM(AdjustedEndingPoints) FROM Current_Month_Activity WHERE tipnumber = msf.tipnumber)
GO
