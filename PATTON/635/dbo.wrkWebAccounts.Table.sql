SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkWebAccounts](
	[AcctID] [varchar](25) NULL,
	[Tipnumber] [varchar](25) NULL,
	[Lastname] [varchar](40) NULL,
	[Last6] [varchar](6) NULL,
	[DDA] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
