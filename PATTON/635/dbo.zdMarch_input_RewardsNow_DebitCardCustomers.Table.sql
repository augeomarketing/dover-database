SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zdMarch_input_RewardsNow_DebitCardCustomers](
	[DDA] [nvarchar](20) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LastName] [nvarchar](40) NULL,
	[Address1] [nvarchar](40) NULL,
	[Address2] [nvarchar](40) NULL,
	[City] [nvarchar](40) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](5) NULL,
	[Zip4] [nvarchar](4) NULL,
	[HomePhone] [nvarchar](10) NULL,
	[SSN] [nvarchar](11) NULL,
	[CardNumber] [nvarchar](20) NULL
) ON [PRIMARY]
GO
