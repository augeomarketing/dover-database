SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_DebitTrans_Sig](
	[DDA] [nvarchar](20) NULL,
	[CardNumber] [nvarchar](20) NULL,
	[SHRTYPE] [nvarchar](2) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TransAmt] [float] NULL,
	[CAN] [int] NULL,
	[TipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
