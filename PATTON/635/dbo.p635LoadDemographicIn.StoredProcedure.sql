SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[p635LoadDemographicIn] AS

truncate table DemographicIn

insert into DemographicIn
(Pan, [Prim DDA], [Address #1], [Address #2], City , ST, Zip, [First], [Last], SSN, [Home Phone], TipFirst, TipNumber,  LastName)

Select right(Cardnumber,16), DDA, Address1,Address2,City, State , Zip, Firstname, LastName, SSN, HomePhone, '635', NULL, LastName
from input_RewardsNow_DebitCardCustomers


delete DemographicIn where Lastname is null
GO
