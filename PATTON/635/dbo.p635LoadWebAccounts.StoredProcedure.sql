SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[p635LoadWebAccounts] AS


truncate table wrkWebAccounts   

-------------------------------------------
insert into wrkWebAccounts (Tipnumber,AcctID)
	select distinct Tipnumber, Cardnumber 
	from dbo.input_DebitTrans_Sig 
	union
	select Tipnumber,Pan from dbo.DemographicIn 
	order by tipnumber

Update WA set WA.Lastname=DI.[Last], WA.[Last6]=Substring(rtrim(WA.AcctID),11,6  )
from wrkWebAccounts WA join DemographicIn DI on WA.AcctID=DI.PAN

Update WA set WA.DDA=I.[DDA]
from wrkWebAccounts WA join dbo.input_RewardsNow_DebitCardCustomers I on WA.AcctID=I.Cardnumber

delete wrkWebAccounts where DDA  in (Select acctid from affiliat where acctstatus<>'A')
GO
