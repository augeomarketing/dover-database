USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport_DebitSIG]    Script Date: 06/21/2011 10:32:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_DebitSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport_DebitSIG]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_DebitSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport_DebitSIG] @StartDate char(10), @EndDate char(10), @TipFirst char(3), @DebitCreditFlag nvarchar(1)
AS 

/**********************************************/
/* S Blanchette                               */
/* 6/2011                                     */
/* SEB001                                     */
/* Calculation for points is incorrect it     */
/* divides the amounttran by 100 as if the    */
/* Decimal point is assumed in the field but  */
/* in this case the field contains whole      */
/* dollars and should not be divided by the   */
/* 100.                                       */
/*                                            */
/**********************************************/

delete input_DebitTrans_SIG where cardnumber is null

truncate table TRANSWORK_DebitSIG


update input_DebitTrans_Sig 
set input_DebitTrans_Sig.Tipnumber=Account_Reference.tipnumber
from input_DebitTrans_Sig, Account_Reference
where input_DebitTrans_Sig.DDA=Account_Reference.AcctNumber 
--where input_DebitTrans_Sig.Cardnumber=Account_Reference.AcctNumber 

/*
--NEW 03/09 rwl
declare @Bin varchar(9)
Select @Bin=Bin from TipFirstReference where tipfirst=@TipFirst
update COOPWork.dbo.TransDetailHold set tipfirst =@Tipfirst where Pan like @Bin + ''%''
*/

insert into TRANSWORK_DebitSIG (TIPNUMBER, TRANDATE, ACCTID, AMOUNTTRAN, RATIO, NUMBEROFTRANS, TRANTYPE, POINTS)
--select * from COOPWork.dbo.TransDetailHold

select  TIPNUMBER,  @EndDate, DDA, Avg(TransAmt), 
case
	when Avg(TransAmt) > 0 then -1
	else 1
end as ratio
,avg(Can) , ''DEBIT'', null
from input_DebitTrans_sig
Group By Tipnumber,DDA

update TRANSWORK_DebitSIG set AmountTran=abs(AmountTran)


---CALC SIG DEBIT POINT VALUES
-- Signature Debit
update TRANSWORK_DebitSIG
/* SEB001 set points=ROUND(((amounttran/100)/2), 10, 0) */
set points=ROUND((amounttran/2), 10, 0) /* SEB001 */



--Put to standard transtaction file format purchases.

if @DebitCreditFlag=''D''    ---if this is a COOP DEBIT SIG ONLY 
Begin
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, 
@enddate, 
AcctID, 
case
	when Ratio =1 then ''67''

	else ''37''
end as ratio, 
NumberOfTrans, points, 
TranType, 
Ratio, 
'' '' 
from TRANSWORK_DebitSIG

	

END' 
END
GO
