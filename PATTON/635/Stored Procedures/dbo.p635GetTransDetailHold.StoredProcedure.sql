USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[p635GetTransDetailHold]    Script Date: 07/30/2013 13:39:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p635GetTransDetailHold]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p635GetTransDetailHold]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p635GetTransDetailHold]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[p635GetTransDetailHold] @StartDate char(10), @EndDate char(10)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */

-- S Blanchete change to use RNITransaction view

Truncate table TransDetailHold

select *
into #tempTrans
from Rewardsnow.dbo.vw_635_TRAN_SOURCE_1 with (nolock)
where Trandate<=cast(@EndDate as DATE) /* SEB005 */


Insert Into TransDetailHold (tdhID, TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID )
	select '''', (substring(TRANDATE,5,2)+''/''+substring(TRANDATE,7,2)+''/''+substring(TRANDATE,1,4)), MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID 
	from  #tempTrans where PAN like ''514324%'' 

drop table #temptrans

' 
END
GO
