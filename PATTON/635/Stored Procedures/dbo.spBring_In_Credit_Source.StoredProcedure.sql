USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spBring_In_Credit_Source]    Script Date: 10/28/2010 11:18:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBring_In_Credit_Source]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBring_In_Credit_Source]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBring_In_Credit_Source]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spBring_In_Credit_Source]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Truncate table dbo.Credit_File_Header
	Truncate table dbo.Credit_File_Trailer
	Truncate table dbo.Credit_Detail
	
	-- Insert statements for procedure here
	INSERT INTO [dbo].[Credit_File_Header]
           ([File_Type]
           ,[Client_ID]
           ,[Creation_Date]
           ,[Creation_Time]
           ,[Filler])
 	 select 
		substring(col001,1,4), 
		substring(col001, 5, 5),
		substring(col001, 10, 8),
		substring(col001, 18, 6),
		substring(col001, 24, 235)
	from dbo.Credit_Source
	where substring(col001, 1, 4) = ''F000'' 
	
	-- Insert statements for procedure here
	
	INSERT INTO [dbo].[Credit_File_Trailer]
           ([File_Type]
           ,[Debit_Record_Count]
           ,[Debit_Amount]
           ,[Credit_Record_Count]
           ,[Credit_Amount]
           ,[Filler])
    select 
		substring(col001,1,4), 
		substring(col001, 5, 9),
		substring(col001, 14, 11),
		substring(col001, 25, 9),
		substring(col001, 34, 11),
		substring(col001, 45, 214)
	from dbo.Credit_Source
	where substring(col001, 1, 4) = ''F999''
	
	
	-- Insert statements for procedure here

	INSERT INTO [dbo].[Credit_Detail]
           ([PAN]
           ,[Primary_Name]
           ,[Secondary_Name]
           ,[Address1]
           ,[Address2]
           ,[City_State]
           ,[Zip]
           ,[Cycle]
           ,[Bank]
           ,[Credit_Rating]
           ,[Home_Phone]
           ,[Work_Phone]
           ,[Number_Purchases]
           ,[Amount_Purchases]
           ,[Number_Returns]
           ,[Amount_Returns]
           ,[Last_Statement_Date]
           ,[Bank1]
           ,[Transfer_Account]
           ,[SSN])
	select 
		substring(col001, 1, 16), 
		substring(col001, 17, 25),
		substring(col001, 42, 25),
		substring(col001, 67, 36),
		substring(col001, 103, 36),
		substring(col001, 139, 27),
		substring(col001, 166, 9), 
		substring(col001, 175, 2),
		substring(col001, 177, 4),
		substring(col001, 181, 2),
		substring(col001, 183, 10),
		substring(col001, 193, 10),
		substring(col001, 203, 2), 
		substring(col001, 205, 8),
		substring(col001, 213, 2),
		substring(col001, 215, 8),
		substring(col001, 223, 8),
		substring(col001, 231, 4),
		substring(col001, 235, 14), 
		substring(col001, 249, 9)
	from dbo.Credit_Source
	where substring(col001, 1, 1) not in (''F'')

END
' 
END
GO
