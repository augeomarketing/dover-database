USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spCheck_Credit_Batch_Totals]    Script Date: 10/28/2010 11:18:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheck_Credit_Batch_Totals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCheck_Credit_Batch_Totals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheck_Credit_Batch_Totals]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCheck_Credit_Batch_Totals] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Debitamount numeric (18,2), @creditamount numeric (18,0)
	declare @BatchDebitamount numeric (18,2), @Batchcreditamount numeric (18,0)
	
	truncate table dbo.Credit_Batch_Errors

	set @DebitAmount = (select sum(cast(Amount_Purchases as int)) from dbo.Credit_Detail)
	set @CreditAmount = (select sum(cast(Amount_Returns as int)) from dbo.Credit_Detail)

	set @BatchDebitAmount = (select cast(Debit_Amount as numeric(11,0))/100 from dbo.Credit_File_Trailer)
	set @BatchCreditAmount = (select cast(Credit_Amount as numeric(11,0))/100 from dbo.Credit_File_Trailer)

	if @DebitAmount<>@BatchDebitAmount or @CreditAmount<>@BatchCreditAmount
	Begin
		insert into dbo.Credit_Batch_Errors
		values(''1'')
	end

END
' 
END
GO
