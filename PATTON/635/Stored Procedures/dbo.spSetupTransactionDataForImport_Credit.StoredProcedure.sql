USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport_Credit]    Script Date: 11/03/2010 13:14:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_Credit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport_Credit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_Credit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
/*********************************************/
/*  S Blanchette                             */
/*  11/10                                    */
/*  SEB001                                   */
/*  Add a divide by two                      */
/*********************************************/

CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport_Credit]
	-- Add the parameters for the stored procedure here
	@EndDate char(10), 
	@DebitCreditFlag nvarchar(1)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


truncate table TransStandardCredit


if @DebitCreditFlag=''C''    ---if this is a COOP Credit
	Begin

		INSERT INTO TransStandardCredit(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
			select tfno, @enddate, ACCT_NUM, ''63'', sum(cast(NUMPURCH as int)), sum(cast(AMTPURCH as bigint)), ''CREDIT'', ''1'', '' '' 
			from creditworkin
			group by tfno, ACCT_NUM
			

		INSERT INTO TransStandardCredit(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select tfno, @enddate, ACCT_NUM, ''33'', sum(cast(NUMCR as int)), sum(cast(AMTCR as bigint)), ''CREDIT'', ''-1'', '' '' 
			from creditworkin
			group by tfno, ACCT_NUM
			
		update TransStandardCredit
		/* SEB001  set tranamt=ROUND(((tranamt/100)), 10, 0) */
		set tranamt=ROUND(((tranamt/100)/2), 10, 0) /* SEB001 */

	END
END
' 
END
GO
