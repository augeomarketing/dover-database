USE [635SeaAir]
GO

/****** Object:  StoredProcedure [dbo].[spBringInCreditFile]    Script Date: 11/11/2014 09:57:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBringInCreditFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBringInCreditFile]
GO

USE [635SeaAir]
GO

/****** Object:  StoredProcedure [dbo].[spBringInCreditFile]    Script Date: 11/11/2014 09:57:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spBringInCreditFile] 

AS

BEGIN
	SET NOCOUNT ON;

	INSERT INTO [635SeaAir].[dbo].[DemographicIn]
	(
		[Pan]
	,	[Address #1]
	,	[Address #2]
	,	[City ]
	,	[ST]
	,	[Zip]
	,	[First]
	,	[Last]
	,	[First2]
	,	[Last2]
	,	[SSN]
	,	[Home Phone]
	,	[Work Phone]
	,	[TipFirst]
	,	[TipNumber]
	)
	SELECT
		[ACCT_NUM]
	,	[NA3]
	,	[NA4]
	,	CASE
			WHEN	LEN(CITYSTATE) > 3
			THEN	LEFT(CITYSTATE,(LEN(CITYSTATE)-3))
			ELSE	CITYSTATE
		END		
	,	RIGHT(RTRIM(CITYSTATE),2)
	,	[ZIP]
	,	FirstName1
	,	LASTNAME1
	,	FirstName2
	,	LASTNAME2
	,	[SSN]
	,	[HOMEPHONE]
	,	[WORKPHONE]
	,	LEFT(tfno,3)
	,	tfno
	FROM	[635SeaAir].[dbo].[CreditWorkIn]

END

GO


