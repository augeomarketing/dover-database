USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spGetandGenerateTipNumbers_CREDIT]    Script Date: 11/02/2010 14:13:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers_CREDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetandGenerateTipNumbers_CREDIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers_CREDIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGetandGenerateTipNumbers_CREDIT] 
	-- Add the parameters for the stored procedure here
	@TipFirstParm varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @LastTipUsed char(15)
	declare  @PAN nchar(16), @SSN varchar(16), @acct nchar(16), @1stDDA nchar(16), @2ndDDA nchar(16), @3rdDDA nchar(16), @4thDDA nchar(16), @5thDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)

	update CreditWorkIn set [SSN]=null where len(rtrim(ltrim([SSN])))=0


	-------------BEGIN update Input_Demographics to Assign Tips to recs via DDA first, then SSN
	--pass 1 (update by DDA)
	update id
		  set TFNO = ar.tipnumber
	from dbo.CreditWorkIn id join dbo.account_reference ar
		  on id.ACCT_NUM = ar.acctnumber      
		  where id.TFNO is null and id.ACCT_NUM is not null /********** SEB001 ***********/
	      
	     
	--pass 2 (update by SSN)      
	      
	update id
		  set TFNO = ar.tipnumber
		from dbo.CreditWorkIn id join dbo.account_reference ar
		  on id.[SSN] = ar.acctnumber
		  where id.TFNO is null and id.[SSN] is not null and id.[SSN]<>''000000000''/************** SEB001 *************/

	-----------END update Input_Demographics to Assign Tips to recs via SSN first, then DDA      



	---------==============================================
	--create a temp table based on Distinct SSNs and DDAs for recs still w/o tipnumber
	select distinct [SSN], ACCT_NUM
	into #tmpSSN_DDA
	from CreditWorkIn 
	where TFNO is null  
	--where [Prim DDA] not in (select acctnumber from Account_Reference)


	declare csrSSN_DDA cursor FAST_FORWARD for
	select SSN, ACCT_NUM from #tmpSSN_DDA
	open csrSSN_DDA
	------------------
		  exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstParm, @LastTipUsed output
		  select @LastTipUsed as LastTipUsed
		  set @newtipnumber=@LastTipUsed
		  set @newtipnumber = cast(@newtipnumber as bigint) + 1  
		  --set the value of the first new tipnumber
	---------------     



	fetch next from csrSSN_DDA
		  into  @SSN, @acct

	while @@FETCH_STATUS = 0
	BEGIN
		  -- Get new tip#

			
		  -- if Neither SSN OR acct are in account_reference, get a new tip and add both
	if @SSN is not null
		begin
			if NOT exists(select acctnumber from account_reference where acctnumber=@SSN) AND NOT exists(select acctnumber from account_reference where acctnumber=@acct)
				begin
					set @newtipnumber = cast(@newtipnumber as bigint) + 1  
					set @UpdateTip=@newtipnumber
					--
					insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
						values(@UpdateTip, @SSN, @TipFirstParm)	
					insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
						values(@UpdateTip, @Acct, @TipFirstParm) 

				end
					--begin
					--	set @newtipnumber = cast(@newtipnumber as bigint) + 1  
					--	set @UpdateTip=@newtipnumber
					--end
			--if the SSN isn''t in AcctRef but acct IS, Lookup tip associated with acct  and add this in with the SSN
			if not exists(select acctnumber from account_reference where acctnumber=@SSN) and EXISTS(select acctnumber from account_reference where acctnumber=@acct)	
				Begin
					Select @UpdateTip=Tipnumber from account_reference where acctnumber=@acct
					insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
						values(@UpdateTip, @SSN, @TipFirstParm) 		
				End

			--if the acct isn''t in AcctRef but SSN IS, Lookup tip associated with SSN  and add this in with the DDA
			if not exists(select acctnumber from account_reference where acctnumber=@acct) and EXISTS(select acctnumber from account_reference where acctnumber=@SSN)	
				Begin
					Select @UpdateTip=Tipnumber from account_reference where acctnumber=@SSN
					insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
						values(@UpdateTip, @Acct, @TipFirstParm) 		
				End			
		End	
	else    --if the ssn IS null,
		begin

			
			--if the Acct isn''t in AcctRef
			if not EXISTS(select acctnumber from account_reference where acctnumber=@acct)	
				Begin

					set @newtipnumber = cast(@newtipnumber as bigint) + 1  
					set @UpdateTip=@newtipnumber

					insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
						values(@UpdateTip, @acct, @TipFirstParm) 		
				End		
		end				


	                     
	                     
		  fetch next from csrSSN_DDA
		  into  @SSN, @acct
	END

	close csrSSN_DDA
	deallocate csrSSN_DDA




	-- Update last Tip Number Used
	exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstParm, @newtipnumber 
	---===============================================

	--re-update input_Demographic in to give tips to the NEW card numbers by DDA
	update id
		  set TFNO = ar.tipnumber
	from dbo.CreditWorkIn id join dbo.account_reference ar
		  on id.ACCT_NUM = ar.acctnumber
		  where id.TFNO is null and id.ACCT_NUM is not null /*********************** SEB001 *****************/
	-----------==================================================    
	--re-update input_Demographic in to give tips to the NEW card numbers by SSN
	update id
		  set TFNO = ar.tipnumber
	from dbo.CreditWorkIn id join dbo.account_reference ar
		  on id.[SSN] = ar.acctnumber
		  where id.TFNO is null and id.[SSN] is not null and id.[SSN]<>''000000000''/******************SEB001 **************/



END
' 
END
GO
