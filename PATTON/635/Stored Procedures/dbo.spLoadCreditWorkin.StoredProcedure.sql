USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCreditWorkin]    Script Date: 10/28/2010 11:18:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCreditWorkin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCreditWorkin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCreditWorkin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCreditWorkin] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table dbo.[CreditWorkIn]

	INSERT INTO dbo.[CreditWorkIn]
			   ([TFNO]
			   ,[BANK]
			   ,[ACCT_NUM]
			   ,[OLDCCNUM]
			   ,[NA1]
			   ,[NA2]
			   ,[STATUS]
			   ,[NA3]
			   ,[NA4]
			   ,[NA5]
			   ,[NA6]
			   ,[CITYSTATE]
			   ,[ZIP]
			   ,[N12]
			   ,[HOMEPHONE]
			   ,[WORKPHONE]
			   ,[NUMPURCH]
			   ,[AMTPURCH]
			   ,[NUMCR]
			   ,[AMTCR]
			   ,[STMTDATE]
			   ,[FirstName1]
			   ,[LASTNAME1]
			   ,[FirstName2]
			   ,[LASTNAME2]
			   ,[oldpre]
			   ,[oldpost]
			   ,[SSN]
		 )
	SELECT 
			null 
		  ,[Bank]
		  ,[PAN]
		  ,[Transfer_Account]
		  ,[Primary_Name]
		  ,[Secondary_Name]
		  ,[Credit_Rating]
		  ,left([Address1],32)
		  ,left([Address2],32)
		  ,'' ''
		  ,'' ''
		  ,[City_State]
		  ,[Zip]
		  ,'' ''
		  ,[Home_Phone]
		  ,[Work_Phone]
		  ,[Number_Purchases]
		  ,cast([Amount_Purchases] as int)
		  ,[Number_Returns]
		  ,cast([Amount_Returns] as int)
		  ,[Last_Statement_Date]
		  ,''''
		  ,''''
		  ,''''
		  ,''''
		  ,'' ''
		  ,'' ''
		  ,[SSN]
	FROM [dbo].[Credit_Detail]
	where [Credit_Rating] is null or LEN([Credit_Rating])=''0''
	
	update dbo.[CreditWorkIn]
	set NA1=replace(na1,char(39), '' ''),na2=replace(na2,char(39), '' ''),citystate=replace(citystate,char(39), '' '')

END
' 
END
GO
