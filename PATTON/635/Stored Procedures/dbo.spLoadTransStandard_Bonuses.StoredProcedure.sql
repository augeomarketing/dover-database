USE [635SeaAir]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard_Bonuses]    Script Date: 04/24/2012 11:42:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard_Bonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadTransStandard_Bonuses] @EndDate char(10)
AS
Declare @E_Stmt bit
--do a select from a BonusTypes table to conditionally add 
/* SEB001 Change points from 100 to 500 */
---------------------------------------------------------------------------------------
/* E-Statement Bonuses */
declare @Trancode nvarchar(2), @Description nvarchar(40)
Set @Trancode=''BE''
Select @Description= [Description] from tranType where TranCode=@TranCode

Insert into transstandard_Bonuses (tfno,Trandate, Acct_num, Trancode, Trannum, tranamt, trantype, ratio, crdactvldt)
/* SEB001 select tipnumber, @EndDate,NULL,@Trancode,1,100, @Description, 1,null from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BE'') */
/* SEB001 */select tipnumber, @EndDate,NULL,@Trancode,1,500, @Description, 1,null from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BE'') 

Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tipnumber, ''BE'',null,@EndDate from EStmt_Work where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BE'')
-----------------------------------------------------------------------------------------------------------------------
/* ADD MORE BONUSES HERE */
------------------------------------------------------------------------------------------------------------------------
/* Copy records to TransStandard*/
insert into transstandard 
	Select * from TransStandard_Bonuses' 
END
GO
