USE [52ACrescentStateBank]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 09/24/2009 16:53:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL
) ON [PRIMARY]
GO
