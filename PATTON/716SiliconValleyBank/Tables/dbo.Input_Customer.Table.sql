USE [716SiliconValleyBank]
GO

/****** Object:  Table [dbo].[Input_Customer]    Script Date: 08/14/2009 12:51:51 ******/
DROP TABLE [dbo].[Input_Customer]
GO

USE [716SiliconValleyBank]
GO

/****** Object:  Table [dbo].[Input_Customer]    Script Date: 08/14/2009 12:51:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardNumber] [varchar](50) NULL,
	[SystembankID] [varchar](50) NULL,
	[Principlebankid] [varchar](50) NULL,
	[AgentBankID] [varchar](50) NULL,
	[AcctName1] [varchar](50) NULL,
	[AcctName2] [varchar](50) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[ZipCode] [varchar](50) NULL,
	[HomePhone] [varchar](50) NULL,
	[StatusCode] [varchar](50) NULL,
	[External Status Code] [varchar](50) NULL,
	[CustID] [varchar](50) NULL,
	[Secondary Social Security Number Text] [varchar](50) NULL,
	[Account Identifier2] [varchar](50) NULL,
	[External Status Code Last Change Date] [varchar](50) NULL,
	[Miscellaneous Seventh Text] [varchar](50) NULL,
	[TipNumber] [varchar](50) NULL,
	[Lastname] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[StatusDescription] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


