use [716SiliconValleyBank] 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO


/****** Object:  Table [dbo].[Client]    Script Date: 04/13/2012 14:34:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) default 0 null,
	[PointsEnd] [decimal](18, 0) default 0 null,
	[PointsPurchased] [decimal](18, 0) default 0 null,
	[PointsBonus] [decimal](18, 0) default 0 null,
	[PointsAdded] [decimal](18, 0) default 0 null,
	[PointsIncreased] [decimal](18, 0) default 0 null,
	[PointsRedeemed] [decimal](18, 0) default 0 null,
	[PointsReturned] [decimal](18, 0) default 0 null,
	[PointsSubtracted] [decimal](18, 0) default 0 null,
	[PointsDecreased] [decimal](18, 0) default 0 null,
	[ExpPts] [Decimal](18,0) default 0 null,
	[PointsBonusMN][decimal](18,0) default 0 null 
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
