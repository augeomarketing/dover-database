/****** Object:  Table [dbo].[TransStandard]    Script Date: 06/23/2009 10:31:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TIP] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[AcctNum] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [numeric](18, 0) NULL,
	[TranAmt] [varchar](15) NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
