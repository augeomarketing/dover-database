/****** Object:  Table [dbo].[CommercialWORK]    Script Date: 03/19/2009 10:55:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommercialWORK](
	[AcctID] [varchar](25) NULL,
	[CompanyName] [varchar](50) NULL,
	[CompanyID] [varchar](10) NULL,
	[SystemBankID] [varchar](4) NULL,
	[PrincipleBankID] [varchar](4) NULL,
	[AgentBankID] [varchar](4) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[acctname2] [varchar](40) NULL,
	[acctname3] [varchar](40) NULL,
	[acctname4] [varchar](40) NULL,
	[acctname5] [varchar](40) NULL,
	[acctname6] [varchar](40) NULL,
	[Status] [char](1) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[address3] [varchar](40) NULL,
	[address4] [varchar](40) NULL,
	[ZipCode] [varchar](15) NULL,
	[lastname] [varchar](40) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[fld4] [varchar](10) NULL,
	[dateadded] [datetime] NULL,
	[Open2] [varchar](1) NULL,
	[SegmentCode] [varchar](2) NULL,
	[open1] [char](87) NULL,
	[misc1] [varchar](20) NULL,
	[misc2] [varchar](20) NULL,
	[misc3] [varchar](20) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NOT NULL,
	[BonusFlag] [char](1) NULL,
	[LastChangeDate] [datetime] NULL,
	[CustID] [varchar](9) NULL,
	[Misc7] [varchar](8) NULL,
	[Misc8] [varchar](8) NULL,
	[CR3] [varchar](25) NULL,
	[OldCardNum] [varchar](25) NULL,
	[OldYTD] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
