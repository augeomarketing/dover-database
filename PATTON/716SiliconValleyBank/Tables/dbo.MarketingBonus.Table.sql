/****** Object:  Table [dbo].[MarketingBonus]    Script Date: 06/23/2009 10:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MarketingBonus](
	[Tipnumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[Custid] [varchar](20) NULL,
	[Misc7] [varchar](8) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
