/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 06/23/2009 10:31:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[CustID] [varchar](9) NULL,
	[misc7] [varchar](5) NULL,
	[DateAwarded] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
