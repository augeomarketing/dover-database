/****** Object:  Table [dbo].[SVBWork]    Script Date: 06/23/2009 10:31:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SVBWork](
	[AcctId] [varchar](255) NULL,
	[CompanyName] [varchar](255) NULL,
	[CompanyId] [varchar](255) NULL,
	[Misc7] [varchar](255) NULL,
	[Misc8] [varchar](255) NULL,
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
