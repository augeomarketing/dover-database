/****** Object:  Table [dbo].[Input_Transactions]    Script Date: 06/23/2009 10:30:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transactions](
	[CardNumber] [varchar](25) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](3) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 2) NULL CONSTRAINT [DF_Input_Transactions_Purchase]  DEFAULT (0),
	[Returns] [decimal](18, 2) NULL CONSTRAINT [DF_Input_Transactions_Returns]  DEFAULT (0),
	[Bonus] [decimal](18, 2) NULL CONSTRAINT [DF_Input_Transactions_Bonus]  DEFAULT (0),
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
