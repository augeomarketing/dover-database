/****** Object:  Table [dbo].[NameFloatTable]    Script Date: 06/23/2009 10:31:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NameFloatTable](
	[Rowid] [char](10) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Acctname] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
