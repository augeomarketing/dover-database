/****** Object:  Table [dbo].[lstnamework]    Script Date: 06/23/2009 10:30:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lstnamework](
	[tipnumber] [varchar](15) NULL,
	[acctnbr] [varchar](15) NULL,
	[lastname] [char](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
