/****** Object:  Table [dbo].[Quarterly_Mail_Table]    Script Date: 06/23/2009 10:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Mail_Table](
	[Tipnumber] [varchar](15) NOT NULL,
	[EMailStmtInd] [char](1) NULL CONSTRAINT [DF_QrterlyMailTable_EMailStmtInd]  DEFAULT ('Y')
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
