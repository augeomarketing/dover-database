/****** Object:  Table [dbo].[Worktable]    Script Date: 06/23/2009 10:31:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Worktable](
	[Rowid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Card_Number] [varchar](50) NULL,
	[CompanyID] [varchar](50) NULL,
	[Tipnumber] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
