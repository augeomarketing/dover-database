USE [716SiliconValleyBank]
GO

/****** Object:  StoredProcedure [dbo].[spScrub_Customer_Data]    Script Date: 11/19/2010 10:08:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spScrub_Customer_Data]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spScrub_Customer_Data]
GO

USE [716SiliconValleyBank]
GO

/****** Object:  StoredProcedure [dbo].[spScrub_Customer_Data]    Script Date: 11/19/2010 10:08:39 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spScrub_Customer_Data] @monthend varchar(10)
AS

/*****************************************************************************/
/*  Changes: 1) update address4 processing for foreign addresses             */
/*****************************************************************************/   

truncate table input_customer_error

insert input_customer_error (cardnumber,acctname1,acctname2,address1,address2,city,state,zipcode)
select cardnumber,acctname1,acctname2,address1,address2,city,state,zipcode from roll_commercial
where tipnumber is null or tipnumber = ' '

delete roll_commercial
where tipnumber is null or tipnumber = ' '

/*****************************************************/
/*    Set Roll_Commercial LstName column             */
/*****************************************************/   

UPDATE  roll_commercial
SET lstname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE roll_commercial SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE roll_commercial SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

/*****************************************************/
/*    Scrub Consumer Customer Information            */
/*****************************************************/   
INSERT INTO Input_Customer(tipnumber)
SELECT DISTINCT tipnumber
FROM Roll_Commercial

UPDATE    input_customer SET custid = b.custid, cardnumber = b.cardnumber, lastname = b.lstname,
address1 = b.address1, address2 = b.address2, address4 = b.address4, city = b.city, state = b.state, zipcode = b.zipcode,
homephone = b.homephone, systembankid = b.systembankid, principlebankid = b.principlebankid,
agentbankid = b.agentbankid, acctname1 = b.acctname1, acctname2 = b.acctname2, acctname3 = b.acctname3
from input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber and b.misc7 = 'Z'

UPDATE    input_customer SET custid = b.custid, cardnumber = b.cardnumber, lastname = b.lstname,
address1 = b.address1, address2 = b.address2,address4 = b.address4, city = b.city, state = b.state, zipcode = b.zipcode,
homephone = b.homephone, systembankid = b.systembankid, principlebankid = b.principlebankid,
agentbankid = b.agentbankid, acctname1 = b.acctname1, acctname2 = b.acctname2, acctname3 = b.acctname3
from input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber and a.cardnumber is null

update input_customer  set acctname2 =  b.acctname1 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
(a.acctname2 is null or a.acctname2 = ' ') 

update input_customer  set acctname3 =  b.acctname1 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and (a.acctname3 is null or a.acctname3 = ' ')

update input_customer  set acctname4 =  b.acctname1 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
(a.acctname4 is null or a.acctname4 = ' ')

update input_customer  set acctname5 =  b.acctname1 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and (a.acctname5 is null or a.acctname5 = ' ')

update input_customer  set acctname6 =  b.acctname1 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and rtrim(b.acctname1) != rtrim(a.acctname5) and
(a.acctname6 is null or a.acctname6 = ' ')

UPDATE input_customer SET statuscode = 'A'

--update input_customer set segmentcode = 'CM'

UPDATE input_customer
SET lastname = acctname1 
WHERE len(lastname) <= 3

UPDATE input_customer SET lastname = ACCTNAME1
WHERE (lastname = 'BankCard') OR  (lastname = 'Payable') OR  (lastname = 'Fool') OR  (lastname = 'Corp')
OR  (lastname = 'Tech')or (lastname = ' ') or (lastname = 'INC') OR  (lastname = 'Card')
OR  (lastname = 'LTD') or (lastname is null)

update input_customer set statusdescription = 'Active[A]' where statuscode = 'A'

UPDATE    Input_Customer SET dateadded =  (SELECT dateadded FROM CUSTOMER_Stage 
WHERE  customer_Stage.tipnumber = input_customer.tipnumber)

UPDATE   input_CUSTOMER
SET dateadded = CONVERT(datetime, @monthend)
WHERE     (dateadded  IS NULL) or dateadded = ' '

update input_customer
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), acctname3=replace(acctname3,char(39), ' '), 
acctname4=replace(acctname4,char(39), ' '),acctname5=replace(acctname5,char(39), ' '), acctname6=replace(acctname6,char(39), ' '),
address1=replace(address1,char(39), ' '), address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update  input_customer
set acctname1=replace(acctname1,char(96), ' '),acctname2=replace(acctname2,char(96), ' '), acctname3=replace(acctname3,char(96), ' '),
acctname4=replace(acctname4,char(96), ' '),acctname5=replace(acctname5,char(96), ' '), acctname6=replace(acctname6,char(96), ' '),
address1=replace(address1,char(96), ' '), address4=replace(address4,char(96), ' '),   city=replace(city,char(96), ' '),  lastname=replace(lastname,char(96), ' ')

update  input_customer
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), acctname3=replace(acctname3,char(44), ' '), 
acctname4=replace(acctname4,char(44), ' '),acctname5=replace(acctname5,char(44), ' '), acctname6=replace(acctname6,char(44), ' '),
address1=replace(address1,char(44), ' '), address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update  input_customer
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), acctname3=replace(acctname3,char(46), ' '), 
acctname4=replace(acctname4,char(46), ' '),acctname5=replace(acctname5,char(46), ' '), acctname6=replace(acctname6,char(46), ' '),
address1=replace(address1,char(46), ' '), address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

-- CHANGE (1)
update input_customer set address4 = city + ' ' + state + ' ' + left(zipcode,5) + '-' + right(zipcode,4)
where len(zipcode) > 5

update input_customer set address4 = city + ' ' + state + ' ' + zipcode
where len(zipcode) = 5

update input_customer set address4 = city + ' ' + state
where zipcode is null

GO


