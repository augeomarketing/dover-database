/****** Object:  StoredProcedure [dbo].[spLoadInputCustomer]    Script Date: 06/23/2009 10:29:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadInputCustomer] @monthend varchar(10)
AS

truncate table input_customer

insert into input_customer_error (cardnumber,acctname1,acctname2)
select cardnumber,acctname1,acctname2 from roll_commercial where tipnumber is null

--delete from roll_commercial where tipnumber is null

INSERT INTO Input_Customer  (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Commercial

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber and (PATINDEX('%Z%', Misc7) > 0)

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.internalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber and a.cardnumber is null

update input_customer
set acctname1 = acctname2
where acctname1 = address1

UPDATE input_customer
set acctname2 = ' '
where acctname1 = acctname2

update input_customer  set acctname2 =  b.acctname2 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  ((a.acctname1 != b.acctname2) and 
(a.acctname2 is null or a.acctname2 = ' ')) 

update input_customer  set acctname3 =  b.acctname2 
from input_customer a, roll_commercial b 
where a.tipnumber = b.tipnumber and  ((a.acctname1 != b.acctname2) and (a.acctname2 != b.acctname2))
and (a.acctname3 is null or a.acctname3 = ' ') 

update input_customer set statusdescription = 'Active[A]' where statuscode = 'A'

UPDATE    Input_Customer SET dateadded =  (SELECT dateadded FROM CUSTOMER_Stage
WHERE  customer_stage.tipnumber = input_customer.tipnumber)

UPDATE   input_CUSTOMER
SET dateadded = CONVERT(datetime, @monthend)
WHERE     (dateadded  IS NULL) or dateadded = ' '
GO
