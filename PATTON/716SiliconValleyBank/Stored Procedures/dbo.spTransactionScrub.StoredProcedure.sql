/****** Object:  StoredProcedure [dbo].[spTransactionScrub]    Script Date: 06/23/2009 10:29:10 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTransactionScrub]
 AS

--------------- Input Transaction table

Insert into Input_Transactions_error 
	select * from Input_Transactions 
	where (cardnumber is null or cardnumber = ' ') or
	      (tipnumber is null or tipnumber = ' ') 

Delete from Input_Transactions
where (cardnumber is null or cardnumber = ' ') or
      (tipnumber is null or tipnumber = ' ') or
      (purchase = '0' and [Returns] = '0' and Bonus = '0')


/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

--Update input_transactions 
--set  purchase = round(purchase,0),  [Returns] =  round([returns],0), bonus = round(Bonus,0)
GO
