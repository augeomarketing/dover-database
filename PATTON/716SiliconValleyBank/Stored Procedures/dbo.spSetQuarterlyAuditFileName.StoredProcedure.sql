USE [716SiliconValleyBank]
GO

/****** Object:  StoredProcedure [dbo].[spSetQuarterlyAuditFileName]    Script Date: 04/17/2010 14:40:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetQuarterlyAuditFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetQuarterlyAuditFileName]
GO

USE [716SiliconValleyBank]
GO

/****** Object:  StoredProcedure [dbo].[spSetQuarterlyAuditFileName]    Script Date: 04/17/2010 14:40:53 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spSetQuarterlyAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @pagect  varchar(6)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

-- JIRA SVB-8  Start
-- set @pagect = (select count(*) from quarterly_statement_file)
set @pagect = ( Select COUNT(*) 
							From Quarterly_Statement_File qtr Join [1Security] sec
								on qtr.Tipnumber = sec.TipNumber 
								Where sec.RegDate is not null and sec.EmailStatement = 'N' ) 
-- JIRA SVB-8  end


set @filename='S716' + @currentdate + '-' + @pagect + '.xls'
 
set @newname='O:\716\Output\QuarterlyFiles\Audit.bat ' + @filename
set @ConnectionString='O:\716\Output\QuarterlyFiles\' + @filename

GO


