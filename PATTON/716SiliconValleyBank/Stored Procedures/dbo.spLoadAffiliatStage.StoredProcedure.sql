USE [716SiliconValleyBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 08/14/2009 11:43:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

ALTER PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


--update roll_commercial set lstname = b.lastname
--from roll_commercial a, customer_stage b
--where a.tipnumber = b.tipnumber

/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend, misc8,'A', 'Credit Card', c.LstName, 0, c.companyid
	from roll_Commercial c where c.cardnumber not in ( Select acctid from Affiliat_Stage)


/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypedesc = T.AcctTypedesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType