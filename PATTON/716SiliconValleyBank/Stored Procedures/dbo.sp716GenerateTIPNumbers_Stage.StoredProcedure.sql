/****** Object:  StoredProcedure [dbo].[sp716GenerateTIPNumbers_Stage]    Script Date: 06/24/2009 13:45:12 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp716GenerateTIPNumbers_Stage]
AS 

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.companyid = b.custid and (a.misc8 = 'Y' and b.secid = 'Y')  and (a.tipnumber is null or a.tipnumber = ' ') 


DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber, tipnumber	
from roll_commercial where (tipnumber is null or tipnumber = ' ') 
and (misc8 = 'N' or misc8 is null)

insert into gentip (custid, tipnumber)
select   distinct companyid as custid, tipnumber	
from roll_commercial where tipnumber is null and misc8 = 'Y'


declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '716', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 716000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '716', @newnum

update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Assigning Tipnumbers combining on CompanyIdentifier */

update roll_commercial
set Tipnumber = b.tipnumber
from roll_commercial a,gentip b
where a.companyid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
