USE [525UnionBankConsumer]
GO
/****** Object:  Table [dbo].[wrkaffiliat]    Script Date: 09/25/2009 10:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkaffiliat](
	[tipnumber] [varchar](15) NOT NULL,
	[custid] [char](13) NULL,
	[secid] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
