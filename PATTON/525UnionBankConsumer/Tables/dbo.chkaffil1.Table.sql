USE [525UnionBankConsumer]
GO
/****** Object:  Table [dbo].[chkaffil1]    Script Date: 09/25/2009 10:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chkaffil1](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[secid] [varchar](10) NULL,
	[custid] [char](13) NULL,
	[dateadded] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
