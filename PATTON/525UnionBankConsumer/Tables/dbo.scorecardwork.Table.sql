USE [525UnionBankConsumer]
GO
/****** Object:  Table [dbo].[scorecardwork]    Script Date: 09/25/2009 10:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[scorecardwork](
	[tipnumber] [char](15) NULL,
	[f7] [nvarchar](255) NULL,
	[points] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
