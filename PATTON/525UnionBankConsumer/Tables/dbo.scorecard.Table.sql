USE [525UnionBankConsumer]
GO
/****** Object:  Table [dbo].[scorecard]    Script Date: 09/25/2009 10:34:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[scorecard](
	[OLD Card Number] [float] NULL,
	[NEW Card Number] [float] NULL,
	[Name Line 1] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[F7] [nvarchar](255) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
