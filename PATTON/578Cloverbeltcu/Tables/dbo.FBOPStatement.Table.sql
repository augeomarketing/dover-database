USE [578Cloverbeltcu]
GO
/****** Object:  Table [dbo].[FBOPStatement]    Script Date: 09/25/2009 13:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FBOPStatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedBUS] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusBUS] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedBUS] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
