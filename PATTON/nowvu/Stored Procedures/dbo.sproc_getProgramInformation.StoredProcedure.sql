USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getProgramInformation]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Retreives program information, based on tipnumber>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getProgramInformation]
@tipNumber varChar(15)

AS

DECLARE @tipFirst varchar(3)

SET @tipFirst = Left(LTrim(RTrim(@tipNumber)), 3)

SELECT LTrim(Rtrim(DBNamePatton)) As dbNamePatton, LTrim(Rtrim(DBNameNEXL)) As dbNameNEXL,
	LTrim(Rtrim(ClientCode)) As ClientCode, LTrim(Rtrim(ClientName)) As ClientName, 
	LTrim(Rtrim(ProgramName)) As ProgramName
FROM rewardsNow.dbo.dbprocessInfo
WHERE dbNumber = @tipFirst

/*
EXECUTE sproc_getProgramInformation '00200000041623'
*/
GO
