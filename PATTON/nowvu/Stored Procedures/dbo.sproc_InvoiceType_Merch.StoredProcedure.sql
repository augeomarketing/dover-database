USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_InvoiceType_Merch]    Script Date: 09/25/2009 14:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO 
-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,22,2009>
-- Description:	<Queries invoice detail for type record set>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_InvoiceType_Merch]
@tipprefix char(3),
@typename varchar(50)

AS

declare @multiplyer decimal(10,2)
SET @multiplyer = (Select dim_billingprogram_Merch from nowvu.dbo.billingprogram
WHERE sid_BillingProgram_id = (Select sid_BillingProgram_id from nowvu.dbo.programtiplookup where dbnumber = @tipprefix))

CREATE TABLE #Invoicetemp(
tipfirst char(3),
client varchar(255),
Accountnum char(15),
Name varchar(50),
type varchar(30),
product varchar(300),
Qty int,
Points int,
Date smalldatetime,
source varchar(10),
cashvalue smallmoney)

Insert INTO #Invoicetemp 
exec NowVu.dbo.sproc_Invoicedetail  @tipprefix

SELECT tipfirst, client, Accountnum, Name, type, product, qty, points, date, source, cashvalue, (points * @multiplyer) as Price
FROM #Invoicetemp 
WHERE Type = @typename

drop table #Invoicetemp


/*
declare @typequery varchar(50)
declare @tipprefix char(3)
EXEC sproc_InvoiceType_Merch '557', [Redeem Merchandise]
select @costaspoints
*/
GO
