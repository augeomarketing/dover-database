USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setShippingTrackingNumber]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_setShippingTrackingNumber]
@transID varChar(50),
@newTrackingNumber varChar(100)

AS

UPDATE fullfillment.dbo.shipping 
SET ShipTrack = @newTrackingNumber
WHERE Transid = @transID  

/*
EXECUTE sproc_setShippingTrackingNumber @transID, @newTrackingNumber
*/
GO
