USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getPickList]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu db and catalog db to retreive packinglist, based on the sid_package_id >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getPickList]
@cardQuantity int OUTPUT,
@itemDesc varChar(35) OUTPUT 

AS

DECLARE @sql NVARCHAR(2000)
DECLARE @reportNumber int
DECLARE @startDate DATETIME
DECLARE @endDate DATETIME
DECLARE @start DATETIME
DECLARE @end DATETIME
DECLARE @havingClause char(1000)

set @reportNumber = 1

set @startDate = '10/27/09'

set @endDate = '10/28/09'

set @start = cast(year(@startDate) as varchar(4)) + '/' +
				right('00' + cast(month(@StartDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@StartDate) as varchar(2)), 2) + ' 00:00:00'
				
set @end = cast(year(@endDate) as varchar(4)) + '/' +
				right('00' + cast(month(@endDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@endDate) as varchar(2)), 2) + ' 00:00:00'

					
IF (@reportNumber = 1)
	SET @havingClause = ' HAVING P.dim_package_value BETWEEN 0.01 AND 75 AND Sum(PC.dim_packagecontent_catalogQTY) < 9 ORDER BY P.sid_package_id, P.dim_package_value'
ELSE IF (@reportNumber = 2)
	SET @havingClause = ' HAVING P.dim_package_value BETWEEN 75.01 AND 199.99 AND Sum(PC.dim_packagecontent_catalogQTY) < 9 ORDER BY P.sid_package_id, P.dim_package_value'
ELSE IF (@reportNumber = 3)
	SET @havingClause = ' HAVING P.dim_package_value >= 200 AND Sum(PC.dim_packagecontent_catalogQTY) < 9 ORDER BY P.sid_package_id, P.dim_package_value'
ELSE IF (@reportNumber = 4)
	SET @havingClause = ' HAVING Sum(PC.dim_packagecontent_catalogQTY) > 8 ORDER BY P.sid_package_id, P.dim_package_value'
			
SET @sql = 'SELECT sum(PC.dim_packagecontent_catalogQTY), CD.dim_catalogdescription_name, P.sid_package_id, P.dim_package_value
	FROM nowvu.dbo.package P
	LEFT JOIN nowvu.dbo.packageContent PC
	ON P.sid_package_id = PC.sid_package_id
	LEFT JOIN catalog.dbo.catalog C
	ON PC.dim_packagecontent_itemnumber = (''GC'' + C.dim_catalog_code)
	LEFT JOIN catalog.dbo.catalogdescription CD
	ON C.sid_catalog_id = CD.sid_catalog_id
	WHERE CONVERT(char(8), P.dim_package_created, 112)
	BETWEEN DATEADD(DAY, DateDiff(Day, 0, @start), 0) AND DATEADD(DAY, DateDiff(Day, 0, @end), 0) 
	GROUP BY P.sid_package_id, CD.dim_catalogdescription_name, P.dim_package_value' +
			@havingClause

--EXEC sp_executesql @SQL, N'@cardQuantity int, @itemDesc varChar(35) ', @cardQuantity=@cardQuantity, @itemDesc=@itemDesc
--print @sql
GO
