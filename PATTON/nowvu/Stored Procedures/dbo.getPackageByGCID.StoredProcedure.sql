USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[getPackageByGCID]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu.dbo.packageCardNum to retreive gift card IDs, based on the sid_package_id >
-- =============================================
CREATE PROCEDURE [dbo].[getPackageByGCID]
@giftCardID int

AS

SELECT sid_package_id AS pkgID
FROM nowvu.dbo.packageCardNum
WHERE dim_packagecardnum_number = @giftCardID


/*
EXECUTE sproc_getPackageByGCID 6037808831213395
*/
GO
