USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getGCIDs]    Script Date: 12/07/2009 09:08:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu.dbo.packageCardNum to retreive gift card IDs, based on the sid_package_id >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_getGCIDs]
@packageID int

AS

SELECT sid_packageCardNum_ID , dim_packageCardNum_number AS cardID, dim_packageCardNum_active AS cardStatus
FROM nowvu.dbo.packageCardNum
WHERE sid_package_id = @packageID
AND dim_packageCardNum_active = 1
ORDER BY dim_packageCardNum_active

/*
EXECUTE sproc_getGCIDs 3481
*/