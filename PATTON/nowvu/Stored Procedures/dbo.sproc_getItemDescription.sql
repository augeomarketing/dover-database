USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getItemDescription]    Script Date: 10/27/2009 14:44:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 27,2009>
-- Description:	<Queries catalog db to retreive item description, based on the sid_packagecontent_id number >
-- =============================================
CREATE  PROCEDURE [dbo].[sproc_getItemDescription]
@pc_id int

AS
DECLARE @packagecontent_id int

SET @packagecontent_id = @pc_id

SELECT dim_catalogdescription_name AS dim_catalogdescription_name
FROM catalog.dbo.catalogdescription CD
WHERE CD.sid_catalog_id =
(SELECT C.sid_catalog_id 
FROM catalog.dbo.catalog C
WHERE ('GC'+ C.dim_catalog_code) =
 (SELECT dim_packagecontent_itemnumber
	FROM nowvu.dbo.packagecontent
	WHERE sid_packagecontent_id = @packagecontent_id))

/*
EXECUTE nowvu.dbo.sproc_getItemDescription 1 

*/