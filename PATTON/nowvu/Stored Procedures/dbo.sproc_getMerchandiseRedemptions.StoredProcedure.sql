USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getMerchandiseRedemptions]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 27,2009>
-- Description:	<Queries catalog db to retreive item description, based on the sid_packagecontent_id number >
-- =============================================
CREATE  PROCEDURE [dbo].[sproc_getMerchandiseRedemptions]
@tipNumber varChar(15)

AS

SELECT M.RedReqFulDate, M.Points, M.TransID, M.cashvalue, M.RedStatus, M.CatalogDesc, M.HistDate,
		M.source, S.ItemNumber
FROM fullfillment.dbo.Main M
LEFT OUTER JOIN fullfillment.dbo.SubItem S
ON M.ItemNumber = S.ItemNumber
WHERE TipNumber = @tipNumber
AND (M.TranCode = 'RM') AND (M.Vendor = 'RN-')
ORDER BY M.HistDate DESC

/*
EXECUTE sproc_getMerchandiseRedemptions 002000000005538
*/
GO
