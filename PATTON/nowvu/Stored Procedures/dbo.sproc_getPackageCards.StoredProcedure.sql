USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getPackageCards]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries Main for sorter data to identify packages>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getPackageCards]

AS

DECLARE @start  DATETIME
DECLARE @end  DATETIME

--CREATE TABLE #dates(
--startdate DATETIME,
--enddate DATETIME)

--INSERT INTO #dates
EXECUTE nowvu.dbo.sproc_getdates @start output, @end output

--SET @start= (Select startdate from #dates)
--SET @end = (Select enddate from #dates)
--DROP TABLE #dates


Select sum(catalogqty) as Packagecount from fullfillment.dbo.main 
WHERE histdate >= @start 
AND histdate <= @end
AND trancode = 'RC'
AND routing = 1



/* 
use nowvu

EXEC nowvu.dbo.sproc_getPackageCards
*/
GO
