USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_TypeFeeTotals]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries invoice detail for type record set>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_TypeFeeTotals]
	@tipprefix char(3),
	@typequery varchar(50),
	@Qtytotal int OUTPUT,
	@Pointstotal int OUTPUT,
	@FeeTotal money OUTPUT,
	@Cashtotal money OUTPUT,
	@Total money OUTPUT

	
AS
/* */
Declare @Fee money
IF  @typequery = 'redeem cards'
	BEGIN
		SET @FEE = 
		(Select distinct dim_billingProgram_gcfee from nowvu.dbo.billingprogram as BP
		INNER JOIN nowvu.dbo.ProgramTipLookup as PTL
		ON PTL.sid_billingProgram_id = BP.sid_billingProgram_id
		WHERE dbnumber = @tipprefix)
	END
IF @typequery = 'QG'
	BEGIN
		SET @FEE = 
		(Select distinct dim_BillingProgram_Onlinegcfee from nowvu.dbo.billingprogram as BP
		INNER JOIN nowvu.dbo.ProgramTipLookup as PTL
		ON PTL.sid_billingProgram_id = BP.sid_billingProgram_id
		WHERE dbnumber = @tipprefix)
	END

		CREATE TABLE #typetotal(
		transid char(40),
		tipfirst char(3),
		client varchar(255),
		Accountnum char(15),
		Name varchar(50),
		[type] varchar(30),
		product varchar(300),
		Qty int,
		Points int,
		[Date] smalldatetime,
		[source] varchar(10),
		cashvalue smallmoney)

	Insert INTO #typetotal
	--EXEC dbo.sproc_Invoicedetail  '002'
	EXEC dbo.sproc_Invoicedetail  @tipprefix
	
	SET @Qtytotal = coalesce((Select sum(qty) FROM #Typetotal WHERE type = @typequery),0)
	SET @PointsTotal = coalesce((Select sum(points) FROM #Typetotal WHERE type = @typequery),0)
	SET @FeeTotal = coalesce((SELECT sum(@fee * qty)FROM #Typetotal WHERE type = @typequery),0)
	SET @Cashtotal =coalesce((SELECT sum(cashvalue * qty) FROM #Typetotal WHERE type = @typequery),0)
	
	SET @Total = coalesce(
	(SELECT  SUM((cashvalue * qty)  + (@fee * qty)) as Total
	FROM #Typetotal 
	WHERE type = @typequery),0)
	

		
	drop table #typetotal


/*
use nowvu
Declare @F money
Declare @Ct money
declare @T money
Declare @P int
Declare @Q int
EXEC nowvu.dbo.sproc_TypeFeeTotals '504', [redeem cards],@FeeTotal = @F OUTput, @cashtotal = @CT OUTPUT, @Total =  @T output, @pointstotal = @P OUTPUT , @qtytotal = @Q OUTPUT
select @F as [FeeT], @CT AS [CashT], @t AS [Total], @P as [Points], @Q AS qty

----
	@FeeTotal money OUTPUT,
	@Cashtota
EXEC sproc_InvoiceType 504,  [qg]
*/
GO
