USE [NowVU]
GO
/****** Object:  StoredProcedure [dbo].[sproc_updateGiftCardNumber]    Script Date: 04/02/2010 13:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Apr 08, 2010>
-- Description:	<Updates a gift card number, given the record ID and new card number>
-- Arguments: transID, newDate
-- =============================================
IF EXISTS (SELECT 1 
           FROM sys.objects
           WHERE name = 'sproc_updateGiftCardNumber'
           AND TYPE = 'P')
   DROP PROCEDURE dbo.sproc_updateGiftCardNumber
   
GO

CREATE PROCEDURE [dbo].[sproc_updateGiftCardNumber]
@newCardNumber varChar(50),
@giftCardID INT

AS

UPDATE nowvu.dbo.packageCardNum
SET dim_packageCardnum_number = @newCardNumber
WHERE sid_packagecardnum_id = @giftCardID

