USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getSuperSearch]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 18, 2009>
-- Description:	<Queries Fullfillment.Main for redemptions based on a 'magic search' term>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getSuperSearch]
	@term varChar(100),
	@startDate DATETIME,
	@endDate DATETIME
AS

SET @term = LTRIM(RTRIM(@term))

SELECT  distinct
		Ltrim(Rtrim(M.Tipnumber)) AS Tipnumber,
		Ltrim(Rtrim(M.TipFirst)) AS TipFirst,
		Ltrim(Rtrim(M.Name1)) AS NameOne,
		ISNULL(Ltrim(Rtrim(M.Name2)), '') AS NameTwo,
		Ltrim(Rtrim(M. sAddress1)) AS AddressOne,
		Ltrim(Rtrim(M. sAddress2)) AS AddressTwo,
		Ltrim(Rtrim(M.sCity)) AS City,
		Ltrim(Rtrim(M.sState)) AS State,
		ISNULL(Ltrim(Rtrim(M.sCountry)), '') AS Country, 
		LEFT(Ltrim(Rtrim(M.sZIP)), 5) AS ZIP,
		ISNULL(Replace(Ltrim(RTrim(M.Phone1)),'-',''), '') AS phoneOne,
		ISNULL(Replace(Ltrim(RTrim(M.Phone2)),'-',''), '') AS phoneTwo,
		ISNULL(Ltrim(Rtrim(M.email)), '') AS emailAddress,
		Ltrim(Rtrim(M.catalogDesc)) AS catalogDescription,
		Ltrim(Rtrim(M.catalogQTY)) AS catalogQTY,
		Ltrim(Rtrim(M.routing)) AS routingCode,
		Ltrim(Rtrim(SRS.redStatusName)) AS status,
		ISNULL(Ltrim(Rtrim(M.itemNumber)), '') AS itemNum,
		ISNULL(Ltrim(Rtrim(M.vendor)), '') AS vendor,
		ISNULL(Ltrim(Rtrim(GC.gcID)), '') AS gcID,
		ISNULL(Ltrim(Rtrim(GC.gcFulfilledby)), '') AS gcFilledby,
		ISNULL(Ltrim(Rtrim(TC.TCID)), '') AS tcid,
		ISNULL(Ltrim(Rtrim(STC.TCStatusName)), '') AS tcstatus,
		TC.TCactValRed AS tcValRed,
		TC.TCredDate AS credDate,
		ISNULL(Ltrim(Rtrim(TC.TCCreditDate)), '') AS TCCreditDate,
		ISNULL(Ltrim(Rtrim(TC.TCTranDate)), '') AS tctranDate,
		CB.CBcreditDate AS cbcreditDate,
		M.histDate AS redemptionDate,
		M.redreqfuldate AS fulfilledDate,
		M.invoiceDate AS invoiceDate,
		PT.sid_package_id AS packageNumber,
		P.dim_package_redStatus AS packageStatus
	FROM fullfillment.dbo.main M
		LEFT OUTER JOIN fullfillment.dbo.subRedemptionStatus SRS
			ON M.redStatus = SRS.redStatus
		LEFT OUTER JOIN fullfillment.dbo.giftCard GC
			ON M.transID = GC.transID
		LEFT OUTER JOIN fullfillment.dbo.TravelCert TC
			ON M.transID = TC.transID
		LEFT OUTER JOIN fullfillment.dbo.SubTravelCertStatus STC
			ON TC.tcstatus = STC.tcstatus
		LEFT OUTER JOIN fullfillment.dbo.cashBack CB
			ON M.transID = CB.TransID
		LEFT OUTER JOIN nowvu.dbo.packagetransID PT
			ON M.transID = PT.transID
		LEFT OUTER JOIN nowvu.dbo.package P
			ON PT.sid_package_id = P.sid_package_id
		LEFT OUTER JOIN nowvu.dbo.packagecardNum PN
			ON P.sid_package_id = PN.sid_package_id
	WHERE (
			M.Tipnumber LIKE '%' + @term + '%'
			OR M.Tipfirst LIKE '%' + @term + '%'
			OR SRS.RedStatusName LIKE '%' + @term + '%'
			OR M.source LIKE '%' + @term + '%'
			OR M.sAddress1 LIKE '%' + @term + '%'
			OR M.sCity LIKE '%' + @term + '%'
			OR M.sZIP LIKE '%' + @term + '%'
			OR M.sState LIKE '%' + @term + '%'
			OR M.tipNumber LIKE '%' + @term + '%'
			OR M.name1 LIKE '%' + @term + '%'
			OR M.name2 LIKE '%' + @term + '%'
			OR M.Email LIKE '%' + @term + '%'
			OR M.phone1 LIKE '%' + @term + '%'
			OR M.vendor LIKE '%' + @term + '%'
			OR M.catalogDesc LIKE '%' + @term + '%'
			OR M.notes LIKE '%' + @term + '%'
			OR M.itemNumber LIKE '%' + @term + '%'
			OR GC.gcID LIKE '%' + @term + '%'
			OR TC.tcID LIKE '%' + @term + '%'
			OR PN.dim_packagecardNum_number LIKE '%' + @term + '%'
			OR PT.sid_package_id LIKE '%' + @term + '%'
		)
		AND M.histDate BETWEEN DateAdd(Day, DateDiff(day, 0, @startDate), 0) AND DateAdd(Day, DateDiff(day, 0, @endDate), 0)
	ORDER BY M.HistDate , Tipnumber DESC

/*
Execute sproc_getSuperSearch '525000000032538', '12/01/09', '12/16/09'
*/

/*
OR Convert(varChar(20), PT.sid_package_id) LIKE COALESCE('%' + @term + '%',PT.sid_package_id)
*/
GO
