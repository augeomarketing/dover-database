USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setRedemptionStatus]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_setRedemptionStatus]
@transID varChar(50),
@newStatus INT

AS

UPDATE fullfillment.dbo.main 
SET RedStatus = @newStatus 
WHERE Transid = @transID  

/*
EXECUTE sproc_setRedemptionStatus @transID, @newDate
*/
GO
