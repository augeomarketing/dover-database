USE [NowVU]
GO

/****** Object:  StoredProcedure [dbo].[sproc_clearShippedDate]    Script Date: 05/17/2010 15:52:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT 1 
           FROM sys.objects
           WHERE name = 'sproc_clearShippedDate'
           AND TYPE = 'P')
   DROP PROCEDURE dbo.sproc_clearShippedDate
   
GO

CREATE PROCEDURE [dbo].[sproc_sproc_clearShippedDate]
@packageID varChar(50)

AS

BEGIN TRANSACTION

UPDATE NowVU.dbo.package 
SET dim_package_shippedDate = Null
WHERE sid_package_id = @packageID 


UPDATE fullfillment.dbo.main 
SET redreqfuldate = Null
WHERE Transid in  
(SELECT transID
 FROM nowvu.dbo.packagetransid PT
 WHERE PT.sid_package_id = @packageID)
 
COMMIT TRANSACTION
GO

