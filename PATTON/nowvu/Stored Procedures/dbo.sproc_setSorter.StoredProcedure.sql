USE [NowVu]
GO

/****** Object:  StoredProcedure [dbo].[sproc_setSorter]    Script Date: 07/17/2012 08:32:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sproc_setSorter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sproc_setSorter]
GO

USE [NowVu]
GO

/****** Object:  StoredProcedure [dbo].[sproc_setSorter]    Script Date: 07/17/2012 08:32:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Oman> - Oh man, I have to look at this code again? - rofl
-- Create date: <Oct,20,2009>
-- Description:	<Queries Main for sorter data to identify packages>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_setSorter]
AS

DECLARE @start  DATETIME
DECLARE @end  DATETIME

EXECUTE NowVU.dbo.sproc_getdates @start Output, @end output
print @start
print @end

-- RT# 275900 - PHB
-- chged to do max() on address4, thereby removing it from the grouping
-- Also added where clause on the insert into packagetransid to look at address3.
-- Finally, prettied the code up to make it more readable

INSERT INTO Nowvu.dbo.package
    (dim_package_histdate, DIM_PACKAGE_TipNumber, DIM_PACKAGE_Name1, DIM_PACKAGE_Name2,  DIM_PACKAGE_SAddress1, DIM_PACKAGE_SAddress2, 
     DIM_PACKAGE_SAddress3, DIM_PACKAGE_SAddress4,DIM_PACKAGE_SCity, DIM_PACKAGE_SState, DIM_PACKAGE_SZip, DIM_PACKAGE_SCountry, 
     DIM_package_value)

SELECT CONVERT(CHAR(8), F.histdate, 1)  ,LTRIM(RTRIM(F.TipNumber)), LTRIM(RTRIM(F.Name1)), LTRIM(RTRIM(F.Name2)), LTRIM(RTRIM(F.SAddress1)), 
	   LTRIM(RTRIM(F.SAddress2)), LTRIM(RTRIM(F.SAddress3)), max(LTRIM(RTRIM(F.SAddress4))),LTRIM(RTRIM(F.SCity)), LTRIM(RTRIM(F.SState)), 
	   LTRIM(RTRIM(F.SZip)), LTRIM(RTRIM(F.SCountry)), SUM(F.Cashvalue * F.Catalogqty) AS Value
FROM fullfillment.dbo.main F 
	INNER JOIN Catalog.dbo.catalog c ON (F.ItemNumber = C.dim_catalog_code OR F.ItemNumber = 'GC' + C.dim_catalog_code)
	INNER JOIN Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN Catalog.dbo.categorygroupinfo cgi ON cc.sid_category_id = cgi.sid_category_id
	INNER JOIN Catalog.dbo.groupinfo g ON cgi.sid_groupinfo_id = g.sid_groupinfo_id
WHERE histdate >= @start 
    AND histdate <= @end
    AND f.TranCode = 'RC'
    AND g.dim_groupinfo_trancode = 'RC'
    AND c.sid_routing_id = 1
GROUP BY CONVERT(CHAR(8), F.histdate, 1) ,LTRIM(RTRIM(F.TipNumber)), LTRIM(RTRIM(F.Name1)), LTRIM(RTRIM(F.Name2)), LTRIM(RTRIM(F.SAddress1)), 
	    LTRIM(RTRIM(F.SAddress2)), LTRIM(RTRIM(F.SAddress3)), LTRIM(RTRIM(F.SCity)), LTRIM(RTRIM(F.SState)), 
	    LTRIM(RTRIM(F.SZip)), LTRIM(RTRIM(F.SCountry))

INSERT INTO nowvu.dbo.packagetransid (sid_package_id, transid)
SELECT distinct P.sid_package_id, F.transid 
	FROM fullfillment.dbo.main F 
		INNER JOIN nowvu.dbo.package P ON P.dim_package_tipnumber= F.tipnumber
		INNER JOIN Catalog.dbo.catalog C ON (F.ItemNumber = C.dim_catalog_code OR F.ItemNumber = 'GC' + C.dim_catalog_code)
		INNER JOIN Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
		INNER JOIN Catalog.dbo.categorygroupinfo cgi ON cc.sid_category_id = cgi.sid_category_id
		INNER JOIN Catalog.dbo.groupinfo g ON cgi.sid_groupinfo_id = g.sid_groupinfo_id
	WHERE 
		LTRIM(RTRIM(P.dim_package_tipnumber))= LTRIM(RTRIM(F.tipnumber))
		AND ISNULL(LTRIM(RTRIM(P.dim_package_name1)),'')= ISNULL(LTRIM(RTRIM(F.name1)),'')
		AND ISNULL(LTRIM(RTRIM(P.dim_package_name2)),'')=  ISNULL(LTRIM(RTRIM(F.name2)),'')
		AND ISNULL(LTRIM(RTRIM(P.dim_package_saddress1)),'')= ISNULL(LTRIM(RTRIM(F.saddress1)),'')
		AND ISNULL(LTRIM(RTRIM(P.dim_package_saddress2)),'')= ISNULL(LTRIM(RTRIM(F.saddress2)),'')
		AND ISNULL(ltrim(rtrim(p.dim_package_saddress3)),'') = ISNULL(ltrim(rtrim(f.saddress3)),'')   --<< Added for RT# 275900
		AND LTRIM(RTRIM(P.dim_package_sstate))= LTRIM(RTRIM(F.sstate))
		AND LTRIM(RTRIM(P.dim_package_scity))= LTRIM(RTRIM(F.scity))
		AND LTRIM(RTRIM(P.dim_package_szip))= LTRIM(RTRIM(F.szip))
		AND isnull(LTRIM(RTRIM(P.dim_package_scountry)), '') = isnull(LTRIM(RTRIM(F.scountry)), '')  --<< Uncommented 20110527 PHB.   RT#380914 & RT# 381366
		AND CONVERT(CHAR(8), F.histdate, 1) = CONVERT(CHAR(8), P.dim_package_histdate, 1)
		AND F.histdate >= @start 
		AND F.histdate <= @end
	    AND f.TranCode = 'RC'
		AND g.dim_groupinfo_trancode = 'RC'
		AND C.sid_routing_id = 1
		AND f.transid NOT IN (SELECT transid FROM nowvu.dbo.packagetransid)
	ORDER BY sid_package_id

INSERT INTO nowvu.dbo.packagecontent (sid_package_id, dim_packagecontent_itemnumber, dim_packagecontent_CatalogQty)
SELECT PT.sid_package_id, F.Itemnumber, SUM(F.catalogqty) AS catalogqty 
	FROM fullfillment.dbo.main F INNER JOIN  nowvu.dbo.packagetransid PT ON F.transid = PT.transid
	WHERE pt.sid_package_id NOT IN (SELECT sid_package_id FROM nowvu.dbo.packagecontent)
	GROUP BY  PT.sid_package_id, F.Itemnumber, F.transid
	ORDER BY PT.sid_package_id


------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--PIK N CLIK
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

INSERT INTO Nowvu.dbo.package
    (dim_package_histdate, DIM_PACKAGE_TipNumber, DIM_PACKAGE_Name1, DIM_PACKAGE_Name2,  DIM_PACKAGE_SAddress1, DIM_PACKAGE_SAddress2, 
     DIM_PACKAGE_SAddress3, DIM_PACKAGE_SAddress4,DIM_PACKAGE_SCity, DIM_PACKAGE_SState, DIM_PACKAGE_SZip, DIM_PACKAGE_SCountry, 
     DIM_package_value)

SELECT CONVERT(CHAR(8), SubmitDate, 1),'P' + REPLICATE('0', 14 - LEN(LTRIM(RTRIM(ORDERNUM)))) + LTRIM(RTRIM(ORDERNUM)), LTRIM(RTRIM(Name)), '', LTRIM(RTRIM(Address1)), 
	   LTRIM(RTRIM(Address2)), '', '' ,LTRIM(RTRIM(City)), LTRIM(RTRIM([State])), 
	   LTRIM(RTRIM(Zip)), '', SUM(c.dim_catalog_cashvalue * 1) --PNC always has qty 1
	FROM fullfillment.dbo.selection F 
		JOIN Catalog.dbo.catalog c ON (F.CatalogCode = C.dim_catalog_code OR F.CatalogCode = 'GC' + C.dim_catalog_code)
		INNER JOIN Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
		INNER JOIN Catalog.dbo.categorygroupinfo cgi ON cc.sid_category_id = cgi.sid_category_id
		INNER JOIN Catalog.dbo.groupinfo g ON cgi.sid_groupinfo_id = g.sid_groupinfo_id
	WHERE SubmitDate >= @start 
		AND SubmitDate <= @end
		AND g.dim_groupinfo_trancode = 'RC'
		AND c.sid_routing_id = 1
	GROUP BY CONVERT(CHAR(8), SubmitDate, 1),'P' + REPLICATE('0', 14 - LEN(LTRIM(RTRIM(ORDERNUM)))) + LTRIM(RTRIM(ORDERNUM)), LTRIM(RTRIM(Name)), LTRIM(RTRIM(Address1)),
				LTRIM(RTRIM(Address2)),LTRIM(RTRIM(City)), LTRIM(RTRIM([State])),
				LTRIM(RTRIM(Zip))

INSERT INTO nowvu.dbo.packagetransid (sid_package_id, transid)
SELECT distinct P.sid_package_id, F.transid 
	FROM fullfillment.dbo.selection F 
		INNER JOIN nowvu.dbo.package P ON P.dim_package_tipnumber = 'P' + REPLICATE('0', 14 - LEN(LTRIM(RTRIM(ORDERNUM)))) + LTRIM(RTRIM(ORDERNUM))
		INNER JOIN Catalog.dbo.catalog C ON (F.CatalogCode = C.dim_catalog_code OR F.CatalogCode = 'GC' + C.dim_catalog_code)
		INNER JOIN Catalog.dbo.catalogcategory cc ON c.sid_catalog_id = cc.sid_catalog_id
		INNER JOIN Catalog.dbo.categorygroupinfo cgi ON cc.sid_category_id = cgi.sid_category_id
		INNER JOIN Catalog.dbo.groupinfo g ON cgi.sid_groupinfo_id = g.sid_groupinfo_id
	WHERE 
		LTRIM(RTRIM(P.dim_package_tipnumber))= 'P' + REPLICATE('0', 14 - LEN(LTRIM(RTRIM(ORDERNUM)))) + LTRIM(RTRIM(ORDERNUM))
		AND ISNULL(LTRIM(RTRIM(P.dim_package_name1)),'')= ISNULL(LTRIM(RTRIM(F.name)),'')
		AND ISNULL(LTRIM(RTRIM(P.dim_package_saddress1)),'')= ISNULL(LTRIM(RTRIM(F.address1)),'')
		AND ISNULL(LTRIM(RTRIM(P.dim_package_saddress2)),'')= ISNULL(LTRIM(RTRIM(F.address2)),'')
		AND LTRIM(RTRIM(P.dim_package_sstate))= LTRIM(RTRIM(F.state))
		AND LTRIM(RTRIM(P.dim_package_scity))= LTRIM(RTRIM(F.city))
		AND LTRIM(RTRIM(P.dim_package_szip))= LTRIM(RTRIM(F.zip))
		AND CONVERT(CHAR(8), F.SubmitDate, 1) = CONVERT(CHAR(8), P.dim_package_histdate, 1)
		AND F.SubmitDate >= @start 
		AND F.SubmitDate <= @end
		AND g.dim_groupinfo_trancode = 'RC'
		AND C.sid_routing_id = 1
		AND f.transid NOT IN (SELECT transid FROM nowvu.dbo.packagetransid)
	ORDER BY sid_package_id

INSERT INTO nowvu.dbo.packagecontent (sid_package_id, dim_packagecontent_itemnumber, dim_packagecontent_CatalogQty)
SELECT PT.sid_package_id, F.CatalogCode, 1  --PNC always has qty 1
	FROM fullfillment.dbo.Selection F INNER JOIN  nowvu.dbo.packagetransid PT ON F.transid = PT.transid
	WHERE pt.sid_package_id NOT IN (SELECT sid_package_id FROM nowvu.dbo.packagecontent)
	GROUP BY  PT.sid_package_id, F.CatalogCode, F.transid
	ORDER BY PT.sid_package_id

/* 
use nowvu
TRUNCATE TABLE Nowvu.dbo.package
TRUNCATE TABLE nowvu.dbo.packagetransid
TRUNCATE TABLE nowvu.dbo.packagecontent
Select * from  Nowvu.dbo.package
Select * from nowvu.dbo.packagetransid
Select * from nowvu.dbo.packagecontent
EXEC nowvu.dbo.sproc_setSorter
*/


GO


