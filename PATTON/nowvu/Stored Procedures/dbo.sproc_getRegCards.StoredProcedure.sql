USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getRegCards]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Dec 29, 2009>
-- Description:	<Retrieves a user's online registration CARD information, given a tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getRegCards]
(
@tipNumber varChar(15),
@status int OUTPUT
)

AS

DECLARE @tipFirst varChar(3)
DECLARE @dbWebName varChar(50)
DECLARE @sql NVARCHAR(2000)

SET @tipFirst = Left(LTrim(Rtrim(@tipNumber)), 3)

BEGIN

SELECT @dbWebName = dbnameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @tipFirst

IF @dbWebName IS NULL RETURN
ELSE SET @dbWebName = QUOTENAME(@dbWebName)

SET @sql = 'SELECT lastname, Right(Ltrim(Rtrim(lastsix)), 4) AS LastFour, Right(Ltrim(Rtrim([SSNLast4])), 4) AS SSNNum,
			Len(Ltrim(Rtrim(lastsix))) As reqLen
			FROM [RN1].'+@dbWebName+'.dbo.[account]
			WHERE tipNumber = '''+ @tipNumber+ '''' 

EXEC sp_executesql @sql

SET @status = @@ROWCOUNT

END


/*
EXECUTE sproc_getOnlineRegistration '402000000009235', 1
*/
GO
