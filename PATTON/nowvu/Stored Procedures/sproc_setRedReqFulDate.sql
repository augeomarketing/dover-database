USE [NowVU]
GO

/****** Object:  StoredProcedure [dbo].[sproc_setRedReqFulDate]    Script Date: 04/13/2010 15:16:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Apr 13, 2010>
-- Description:	<Updates RedReqFulDate (fulfillment Date) in fulfillment.Main >
-- Arguments: transID, newDate
-- =============================================

IF EXISTS (SELECT 1 
           FROM sys.objects
           WHERE name = 'sproc_setRedReqFulDate'
           AND TYPE = 'P')
   DROP PROCEDURE dbo.sproc_setRedReqFulDate
   
GO

CREATE PROCEDURE [dbo].[sproc_setRedReqFulDate]
@transID varChar(50),
@newDate DATETIME

AS

UPDATE fullfillment.dbo.main 
SET RedReqFulDate = @newDate 
WHERE Transid = @transID  

/*
EXECUTE sproc_setRedReqFulDate @transID, @newDate
*/
GO

