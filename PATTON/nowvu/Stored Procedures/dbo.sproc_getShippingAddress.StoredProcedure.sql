USE [NowVU]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getShippingAddress]    Script Date: 12/14/2010 10:08:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu db to retreive shipping address, based on the sid_package_id >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_getShippingAddress]
@packageID int

AS

DECLARE @pID int

SET @pID = @packageID

SELECT
  LTrim(RTrim(P.sid_package_id)) AS packageID,
  LTrim(RTrim(P.dim_package_name1)) As name1,
  ISNULL(P.dim_package_name2, '') As name2,
  LTrim(RTrim(P.dim_package_sAddress1)) As address1,
  LTrim(RTrim(P.dim_package_sAddress2)) As address2,
  LTrim(RTrim(P.dim_package_created)) As createdDate,
  LTrim(RTrim(P.dim_package_histDate)) As redemptionDate,
  LTrim(RTrim(P.dim_package_shippedDate)) As shippedDate,
  LTrim(RTrim(P.dim_package_tipNumber)) As tipNumber,
  LTrim(RTrim(P.dim_package_sCity)) As City,
  LTrim(RTrim(P.dim_package_sState)) As State,
  Left(LTrim(RTrim(P.dim_package_sZip)), 5) As Zip,
  LTrim(RTrim(P.dim_package_sCountry)) AS Country,
  LTrim(RTrim(P.dim_package_value)) AS value,
  LTrim(RTrim(P.dim_package_redstatus)) AS statusCode,
  LTrim(RTrim(SRS.redStatusName)) AS pStatus,
  LTrim(RTrim(P.dim_package_lastmodified)) AS statusDate
FROM nowvu.dbo.package P
LEFT JOIN fullfillment.dbo.subRedemptionStatus SRS
ON P.dim_package_redStatus = SRS.redStatus
WHERE P.sid_package_id = @pID
AND P.dim_package_active = 1
ORDER BY P.sid_package_id

/*
EXECUTE nowvu.dbo.sproc_getShippingAddress 4599

*/