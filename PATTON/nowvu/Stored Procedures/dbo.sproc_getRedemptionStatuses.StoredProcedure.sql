USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getRedemptionStatuses]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 06,2009>
-- Description:	<Queries fulfillment db for list of redemption statuses (to fill combo box)>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getRedemptionStatuses]

AS

SELECT RedStatus AS redemptionStatus, RedStatusName AS statusName
FROM fullfillment.dbo.subredemptionstatus 
WHERE 1=1

/*
execute sproc_getRedemptionStatuses
*/
GO
