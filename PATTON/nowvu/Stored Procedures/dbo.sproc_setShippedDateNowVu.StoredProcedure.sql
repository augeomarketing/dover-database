USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setShippedDateNowVu]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_setShippedDateNowVu]
@packageID varChar(50),
@shippedDate DATETIME

AS

UPDATE nowvu.dbo.package 
SET dim_Package_shippedDate = @shippedDate
WHERE sid_package_id = @packageID
GO
