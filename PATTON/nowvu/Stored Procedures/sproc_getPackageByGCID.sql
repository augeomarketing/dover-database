USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getPackageByGCID]    Script Date: 12/07/2009 14:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu.dbo.packageCardNum to retreive package ID, based on a gift card number (GCID) >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_getPackageByGCID]
@giftCardID varchar(20)

AS

SELECT sid_package_id AS pkgID, dim_packageCardNum_active
FROM nowvu.dbo.packageCardNum
WHERE dim_packagecardnum_number = @giftCardID

/*
EXECUTE sproc_getPackageByGCID '6037808831213395'
*/