USE [NowVU]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setRedemptionStatus]    Script Date: 04/02/2010 13:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Apr 08, 2010>
-- Description:	<Updates Redemption Status in fulfillment.Main.>
-- Arguments: transID, newDate
-- =============================================
IF EXISTS (SELECT 1 
           FROM sys.objects
           WHERE name = 'sproc_setRedemptionStatus'
           AND TYPE = 'P')
   DROP PROCEDURE dbo.sproc_setRedemptionStatus
   
GO

CREATE PROCEDURE [dbo].[sproc_setRedemptionStatus]
@transID varChar(50),
@newStatus INT

AS

UPDATE fullfillment.dbo.main 
SET RedStatus = @newStatus 
WHERE Transid = @transID  

/*
EXECUTE sproc_setRedemptionStatus @transID, @newDate
*/