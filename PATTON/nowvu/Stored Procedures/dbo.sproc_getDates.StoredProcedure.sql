USE [NowVu]
GO

if object_id('sproc_getDates') is not null
    drop procedure dbo.sproc_getDates
GO

/****** Object:  StoredProcedure [dbo].[sproc_getDates]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries Main for sorter data to identify packages>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getDates] 
	@start datetime output,
	@end datetime output

AS

DECLARE @startmod int
DECLARE @endmod   int
--DECLARE @start  DATETIME
--DECLARE @end  DATETIME
--DECLARE @start1  DATETIME
--DECLARE @end1  DATETIME

--set @startmod= '-2'
--set @endmod = '-2'
set @startmod = '-1'
set @endmod   = '-1'
			 
--set @start1 = (Select CAST(CONVERT(VARCHAR(20), DATEADD(dd, @startmod, GETDATE()), 101) AS DATETIME))
--set @end1= (Select CAST(CONVERT(VARCHAR(20), DATEADD(dd, @endmod, GETDATE()), 101) AS DATETIME))

set @start = convert(date, dateadd(dd,@startmod, getdate()))
set @start = @start + '00:00:00.000'

set @end = convert(date, dateadd(dd,@endmod, getdate())) 
set @end = @end + '23:59:59.998'

select @start, @end
--set @start = cast(year(@start1) as varchar(4)) + '/' +
--				right('00' + cast(month(@Start1) as varchar(2)), 2) + '/' +
--				right('00' + cast(day(@Start1) as varchar(2)), 2) + ' 00:00:00'

--set @end = cast(year(@End1) as varchar(4)) + '/' +
--				right('00' + cast(month(@End1) as varchar(2)), 2) + '/' +
--				right('00' + cast(day(@End1) as varchar(2)), 2) + ' 23:59:59'
				
--Select @start as Startdate, @end as enddate
GO
