USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getOnlineRegistration]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Retrieves a user's online registration information, given a tipNumber >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getOnlineRegistration]
(
@tipNumber varChar(15),
@status int OUTPUT
)

AS

DECLARE @tipFirst varChar(3)
DECLARE @dbWebName varChar(50)
DECLARE @sql NVARCHAR(2000)

SET @tipFirst = Left(LTrim(Rtrim(@tipNumber)), 3)

BEGIN

SELECT @dbWebName = dbnameNEXL FROM rewardsnow.dbo.dbprocessinfo WHERE DBNumber = @tipFirst

IF @dbWebName IS NULL RETURN
ELSE SET @dbWebName = QUOTENAME(@dbWebName)

SET @sql = 'SELECT Ltrim(Rtrim(C.Name1)) AS Name1, Ltrim(Rtrim(SD.userName)) AS userName, Len(SD.password) AS password,
				Ltrim(Rtrim(SD.secretQ)) AS secretQ, Ltrim(Rtrim(SD.secretA)) AS secretA, Ltrim(Rtrim(SD.EmailStatement)) AS emailStatement,
				SD.regDate, Ltrim(Rtrim(SD.email)) AS email
			FROM [RN1].'+@dbWebName+'.dbo.[1security] SD
			INNER JOIN [RN1].'+@dbWebName+'.dbo.[customer] C
			ON SD.tipNumber = C.tipNumber
			WHERE SD.tipNumber = '''+ @tipNumber+ ''''
			
EXEC sp_executesql @sql

SET @status = @@ROWCOUNT

END


/*
EXECUTE sproc_getOnlineRegistration '545000000013300', 1
*/
GO
