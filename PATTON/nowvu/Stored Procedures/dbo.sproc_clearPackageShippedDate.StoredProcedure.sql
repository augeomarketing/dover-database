USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_clearPackageShippedDate]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_clearPackageShippedDate]
@packageID varChar(50)

AS

BEGIN TRANSACTION

BEGIN TRY
	UPDATE NowVU.dbo.package 
	SET dim_package_shippedDate = Null
	WHERE sid_package_id = @packageID

	UPDATE fullfillment.dbo.main 
	SET redreqfuldate = Null
	WHERE Transid in (SELECT transID
					  FROM nowvu.dbo.packagetransid PT
					  WHERE PT.sid_package_id = @packageID)
					  
	COMMIT TRANSACTION
END TRY

BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION
END CATCH

/*
EXECUTE sproc_clearPackageShippedDate 10141
*/
GO
