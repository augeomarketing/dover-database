USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getGCPickList]    Script Date: 12/07/2009 09:09:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Retreives list of package IDs that are used to generate the pick cards >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_getGCPickList]
@reportID int,
@startDate DATETIME,
@endDate DATETIME

AS

IF @reportID = 1			
	SELECT P.sid_package_id
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_created, 112)
			BETWEEN DATEADD(DAY, DateDiff(Day, 0, @startDate), 0) AND DATEADD(DAY, DateDiff(Day, 0, @endDate), 0)
			AND P.dim_package_value BETWEEN 0.01 AND 75
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
			ORDER BY P.sid_package_id
			
Else IF @reportID = 2
	SELECT P.sid_package_id
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_created, 112)
			BETWEEN DATEADD(DAY, DateDiff(Day, 0, @startDate), 0) AND DATEADD(DAY, DateDiff(Day, 0, @endDate), 0)
			AND P.dim_package_value BETWEEN 75.01 AND 199.99
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
			ORDER BY P.sid_package_id
			
Else IF @reportID = 3
	SELECT P.sid_package_id
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_created, 112)
			BETWEEN DATEADD(DAY, DateDiff(Day, 0, @startDate), 0) AND DATEADD(DAY, DateDiff(Day, 0, @endDate), 0)
			AND P.dim_package_value >= 200
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
			ORDER BY P.sid_package_id
			
Else IF @reportID = 4
	SELECT P.sid_package_id
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_created, 112)
			BETWEEN DATEADD(DAY, DateDiff(Day, 0, @startDate), 0) AND DATEADD(DAY, DateDiff(Day, 0, @endDate), 0)
			GROUP BY P.sid_package_id 
			HAVING (Sum(PC.dim_packagecontent_catalogQTY) > 8)
			ORDER BY P.sid_package_id
			
/*
EXECUTE sproc_getGCPickList 1, '11/15/2009', '11/20/2009'
*/