USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_updateGiftCardNumber]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_updateGiftCardNumber]
@giftCardID INT,
@newCardNumber varChar(50)

AS

UPDATE nowvu.dbo.packageCardNum
SET dim_packageCardnum_number = @newCardNumber
WHERE sid_packagecardnum_id = @giftCardID
GO
