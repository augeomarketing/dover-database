USE [NowVU]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setShippedDateNowVu]    Script Date: 04/23/2010 10:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 06,2009>
-- Description:	<Posts date value to nowvu.dbo.package.dim_package_shippedDate, given the package ID and the shipping date.>
-- =============================================
IF EXISTS (SELECT 1 
           FROM sys.objects
           WHERE name = 'sproc_setShippedDateNowVu'
           AND TYPE = 'P')
   DROP PROCEDURE dbo.sproc_setShippedDateNowVu
   
GO

CREATE PROCEDURE [dbo].[sproc_setShippedDateNowVu]
@packageID varChar(50),
@shippedDate DATETIME

AS

UPDATE nowvu.dbo.package 
SET dim_Package_shippedDate = @shippedDate
WHERE sid_package_id = @packageID