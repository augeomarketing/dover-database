USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getTravelCertList]    Script Date: 12/07/2009 10:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries Fullfillment.dbo.Main for travel certificates, by redemption date. >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_getTravelCertList]
@startDate DATETIME,
@endDate DATETIME

AS

SELECT
M.TipFirst AS tipFirst,
Ltrim(Rtrim(M.TipNumber)) AS tipNumber,
Ltrim(Rtrim(M.Name1)) AS name1,
ISNULL(Ltrim(Rtrim(M.Name2)), '') AS name2,
M.TransID AS transID,
M.Points AS points,
M.cashvalue AS cashValue,
M.histDate AS redemptionDate,
M.RedReqFulDate AS fulfillmentDate,
TC.TCRedDate AS tcRedemptionDate,
TCS.TCStatusName AS tcStatusName,
M.redStatus AS mainStatusVal,
SRS.redstatusName AS mainStatus, 
ISNULL(Ltrim(Rtrim(M.Notes)), '') AS notes,
TC.TCID AS tcID,
TC.TCStatus AS travelCertStatus,
ISNULL(Ltrim(Rtrim(TC.TCLastSix)), '') AS tcLastFour,
TC.TCActValRed AS redeemedValue,
TC.TCCreditDate AS creditDate,
TC.TCTranDate AS TransDate
FROM fullfillment.dbo.main M
INNER JOIN fullfillment.dbo.TravelCert TC
ON M.TransID = TC.TransID
INNER JOIN fullfillment.dbo.subTravelCertStatus TCS
ON TC.tcstatus = TCS.TCStatus
INNER JOIN fullfillment.dbo.subRedemptionStatus SRS
ON M.redStatus = SRS.redstatus
WHERE CONVERT(char(8), M.histDate, 112) BETWEEN DATEADD(DAY, DateDiff(Day, 0, @startDate), 0) AND DATEADD(DAY, DateDiff(Day, 0, @endDate), 0)
ORDER BY CONVERT(char(8), M.histDate, 112), M.tipFirst, TC.tcID

/*
EXECUTE sproc_getTravelCertList '11/20/2009', '11/25/2009'
*/