USE [NowVU]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setShippingTrackingNumber]    Script Date: 04/02/2010 13:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Nov 12,2009>
-- Description:	<Updates a UPS/USPS tracking number in patton.fullfillment.dbo.shipping.shipTrack>
-- Arguments: transID, newTrackingNumber
-- =============================================
IF EXISTS (SELECT 1 
           FROM sys.objects
           WHERE name = 'sproc_setShippingTrackingNumber'
           AND TYPE = 'P')
   DROP PROCEDURE dbo.sproc_setShippingTrackingNumber
   
GO

CREATE PROCEDURE [dbo].[sproc_setShippingTrackingNumber]
@transID varChar(50),
@newTrackingNumber varChar(100)

AS

UPDATE fullfillment.dbo.shipping 
SET ShipTrack = @newTrackingNumber
WHERE Transid = @transID  

/*
EXECUTE sproc_setShippingTrackingNumber @transID, @newTrackingNumber
*/
