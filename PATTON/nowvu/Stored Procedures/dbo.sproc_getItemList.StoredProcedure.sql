USE [NowVU]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getItemList]    Script Date: 08/15/2011 16:46:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu db and catalog db to retreive packinglist, based on the sid_package_id >
-- =============================================
ALTER PROCEDURE [dbo].[sproc_getItemList]
@packageID int

AS

DECLARE @pID int

SET @pID = @packageID

SELECT sum(PC.dim_packagecontent_catalogQTY) AS itemQuantity, CD.dim_catalogdescription_name AS itemDescription
FROM nowvu.dbo.package P
	LEFT JOIN nowvu.dbo.packageContent PC
		ON P.sid_package_id = PC.sid_package_id
	LEFT JOIN catalog.dbo.catalog C
		ON (PC.dim_packagecontent_itemnumber = 'GC' + C.dim_catalog_code OR PC.dim_packagecontent_itemnumber = C.dim_catalog_code  )
	LEFT JOIN catalog.dbo.catalogdescription CD
		ON C.sid_catalog_id = CD.sid_catalog_id
WHERE P.sid_package_id = @pID
AND C.dim_catalog_active = 1
GROUP BY CD.dim_catalogdescription_name
ORDER BY CD.dim_catalogdescription_name