USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_setRedReqFulDate]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_setRedReqFulDate]
@transID varChar(50),
@newDate DATETIME

AS

UPDATE fullfillment.dbo.main 
SET RedReqFulDate = @newDate 
WHERE Transid = @transID  

/*
EXECUTE sproc_setRedReqFulDate @transID, @newDate
*/
GO
