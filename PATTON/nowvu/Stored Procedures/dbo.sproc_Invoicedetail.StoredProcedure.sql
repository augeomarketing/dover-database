USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_Invoicedetail]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,22,2009>
-- Description:	<creates invoice detail>
-- =============================================
ALTER PROCEDURE [dbo].[sproc_Invoicedetail]
@tipprefix char(3)


AS
DECLARE @startdate DATETIME
DECLARE @enddate DATETIME
SET @enddate = '2009-11-19 23:59:59'
SET @startdate = '2009-11-12 00:00:00'
--Determine previous full week in relation to current day 
--I just ran this query successfully
--Determine previous full week in relation to current day
/*Set datefirst 4
DECLARE @today DATETIME
DECLARE @startdate DATETIME
DECLARE @enddate DATETIME
DECLARE @endpart INT
SET @today = GETDATE()
SET @enddate = CAST(YEAR(@today) AS VARCHAR(4)) + '/' + RIGHT('00' + CAST(MONTH(@today) AS VARCHAR(2)), 2) + '/' + RIGHT('00' + CAST(DAY(@today) AS VARCHAR(2)), 2) + ' 00:00:00'
SET @endpart = DATEPART(dw, @enddate) - 1
SET @enddate = @enddate - @endpart
SET @startdate = @enddate - 7 */

--print 'start:' + CONVERT(varchar(20),@startdate) + ' end:' + CONVERT(varchar(20),@enddat

--SET @enddate = '2009-11-19 23:59:59'
--SET @startdate = '2009-11-12 00:00:00' 

SELECT DISTINCT
m.transid,
m.tipfirst,
lt.dim_loyaltytip_finame AS Client,
RTRIM(m.Tipnumber) AS [Accountnum],
RTRIM(m.Name1) AS [Name],
CASE
WHEN lc.dim_loyaltycatalog_bonus = '1' THEN 'Bonus'
WHEN g.sid_groupinfo_id in (3,13) THEN 'Redeem Cards' -- Gift cards (3), Local GCs (9), Gas cards (13) and FI Cards (14)
WHEN g.sid_groupinfo_id in (1) THEN 'Redeem Merchandise' -- Merchandise (1) and FI merch (15)
WHEN g.sid_groupinfo_id in (11) THEN 'Ringtone'
WHEN g.sid_groupinfo_id in (10) THEN 'QG'
WHEN g.sid_groupinfo_id in (12) THEN 'Tunes'
ELSE RTRIM(m.Trandesc)
END AS Type,

--RTRIM(dim_groupinfo_name) As Groupname,
RTRIM(m.catalogdesc) AS Product,
RTRIM(m.Catalogqty) AS Qty,
RTRIM(m.Points * m.catalogqty) AS Points,
LEFT(RTRIM(m.histdate), 11) AS [Date],
RTRIM(Source) AS Source,
Cashvalue

FROM fullfillment.dbo.main m
INNER JOIN catalog.dbo.loyaltytip lt
ON lt.dim_loyaltytip_prefix = LEFT (m.Tipnumber, 3)
AND dim_loyaltytip_active = 1
INNER JOIN catalog.dbo.loyaltycatalog lc
ON lt.sid_loyalty_id = lc.sid_loyalty_id
INNER JOIN catalog.dbo.catalogcategory cc
ON cc.sid_catalog_id = lc.sid_catalog_id
INNER JOIN catalog.dbo.categorygroupinfo cg
ON cc.sid_category_id = cg.sid_category_id
INNER JOIN catalog.dbo.groupinfo g
ON g.sid_groupinfo_id = cg.sid_groupinfo_id
LEFT OUTER JOIN catalog.dbo.catalog c
ON c.sid_catalog_id = lc.sid_catalog_id
AND c.dim_catalog_code =
CASE
WHEN LEFT(m.itemnumber, 2) = 'GC'
THEN SUBSTRING( RTRIM(LTRIM(m.itemnumber)), 3, LEN(RTRIM(LTRIM(m.itemnumber))) - 2 )
ELSE RTRIM(LTRIM(m.itemnumber))
END

WHERE
histdate >= @startdate
AND histdate < @enddate
AND dim_loyaltytip_prefix = @tipprefix
AND dim_loyaltycatalog_pointvalue > 0
AND dim_catalogcategory_active = 1
AND dim_groupinfo_name != 'travel'
AND m.trancode != 'DR'
AND m.source != 'CLASS'
AND g.sid_groupinfo_id not in(14,15)
and (c.sid_catalog_id is not null or (c.sid_catalog_id is null AND source like '%class%' and dim_loyaltycatalog_bonus = 0))

UNION

SELECT
m.transid,
m.tipfirst,
RTRIM(s.ClientName) AS Client,
RTRIM(m.Tipnumber) AS [Accountnum],
RTRIM(m.Name1) AS [Name],
CASE
WHEN M.Trancode = 'DR' THEN 'Decrease Redeem'
ELSE RTRIM(m.Trandesc)
END AS Type,
RTRIM(m.catalogdesc) AS Product,
RTRIM(m.Catalogqty) AS Qty,
CASE
WHEN M.Trancode = 'DR' THEN '-' + RTRIM(m.Points*m.catalogqty)
ELSE RTRIM(m.Points*m.catalogqty)
END AS Points,
LEFT(RTRIM(m.histdate), 11) AS [Date],
RTRIM(Source) AS Source,
Cashvalue
FROM fullfillment.dbo.main m
LEFT OUTER JOIN rewardsnow.dbo.dbprocessinfo s
ON m.tipfirst = s.DBNumber
WHERE trancode = 'dr'
AND LEFT(Tipnumber, 3) = @tipprefix
AND histdate >= @startdate
AND histdate < @enddate
Order by Type




/*


declare @tipprefix char(3)
@tipprefix = 002
declare @i int
 EXEC dbo.sproc_Invoicedetail  '713'
select @costaspoints



*/
GO
