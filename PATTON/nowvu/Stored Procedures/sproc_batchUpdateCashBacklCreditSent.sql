USE [NowVU]
GO
/****** Object: StoredProcedure [dbo].[sproc_batchUpdateCashBackCreditSent] Script Date: 05/21/2010 10:49:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: <DSeavey>
-- Create date: <May 21, 2010>
-- Description: <
-- Updates the cashback creditDate in fullfillment.dbo.cashback
-- and redstatus in fullfillment.dbo.Main
-- >
-- =============================================
IF EXISTS (SELECT 1
			FROM sys.objects
			WHERE name = 'sproc_batchUpdateCashBackCreditSent'
			AND TYPE = 'P')
DROP PROCEDURE dbo.sproc_batchUpdateCashBackCreditSent

GO

CREATE PROCEDURE [dbo].[sproc_batchUpdateCashBackCreditSent]

AS

BEGIN TRANSACTION

BEGIN TRY
	UPDATE fullfillment.dbo.cashback
		SET cbcreditdate = getdate()
	FROM fullfillment.dbo.main M
	INNER JOIN fullfillment.dbo.cashback CB
		ON M.TransID = CB.TransID
	WHERE M.redstatus = 5


	UPDATE fullfillment.dbo.Main
		SET redstatus = 7
	FROM fullfillment.dbo.Main M
	INNER JOIN fullfillment.dbo.Cashback CB
		ON M.TransID = CB.TransID
	WHERE M.redstatus = 5

	COMMIT TRANSACTION

END TRY

BEGIN CATCH
	if @@TRANCOUNT > 0
	rollback transaction
END CATCH

