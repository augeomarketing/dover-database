USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getOpenAccountDetail]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <DSeavey>
-- Create date: <Oct 27,2009>
-- Description:   <Queries catalog db to retreive item description, based on the sid_packagecontent_id number >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getOpenAccountDetail]
@tipNumber varChar(15)
AS

DECLARE @dbNamePatton varChar(50)

set @DBNamePatton=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=Left(@tipNumber, 3))
				

BEGIN

SELECT DISTINCT TipNumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, RunAvailable As PointsAvail, StatusDescription,
LTrim(RTrim(Address1)) AS AddressOne, homephone As Phone,
(LTrim(RTrim(City)) + '  ' + LTrim(RTrim(State)) + ', ' + Left(LTrim(RTrim(ZipCode)), 5)) AS AddressTwo,
                              dateAdded, zipCode
FROM [@dbNamePatton].dbo.customer
WHERE 1=1
            AND
TipNumber = '@TipNumber'

END

/*
Execute dbo.sproc_getOpenAccountDetail '217000000002263'
*/
GO
