USE [fullfillment]
GO
/****** Object:  StoredProcedure [dbo].[spGiftCardRedemptionReport]    Script Date: 07/28/2010 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spGiftCardRedemptionReport]
	@StartDate			datetime,
	@EndDate				datetime

AS

declare @rptStartDate		datetime
declare @rptEndDate			datetime

set @rptStartDate = cast(year(@StartDate) as varchar(4)) + '/' +
				right('00' + cast(month(@StartDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@StartDate) as varchar(2)), 2) + ' 00:00:00'

set @rptEndDate = cast(year(@EndDate) as varchar(4)) + '/' +
				right('00' + cast(month(@EndDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@EndDate) as varchar(2)), 2) + ' 23:59:59'


Select      rtrim(ltrim(itemnumber)) as Catalogcode,
            count(catalogqty)as Redeem_count,
            sum(catalogqty) as Card_count
from  dbo.main
where 
	 histdate between @rptStartDate and @rptEndDate
      and   trancode = 'RC'
--      and   tipfirst not in ('002','003')  Removed 7/16/2009
      and routing = 1
      and (catalogdesc not like '%VISA%' or (tipfirst in ('231') and catalogdesc like '%visa%')) -- added for 231 Service credit union where RewardsNOW will fulfill Visa Gift Cards for them
      and catalogdesc not like '%Quickgifts%'
group by itemnumber
order by itemnumber
GO
