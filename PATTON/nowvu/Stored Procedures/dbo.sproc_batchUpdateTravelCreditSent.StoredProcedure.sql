USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_batchUpdateTravelCreditSent]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_batchUpdateTravelCreditSent]

AS

BEGIN TRANSACTION

  UPDATE Fullfillment.dbo.travelcert
  SET tccreditdate = getdate()
  FROM fullfillment.dbo.Main M
    INNER JOIN fullfillment.dbo.TravelCert TC
    ON M.TransID = TC.TransID
  WHERE M.TipFirst NOT IN ('002', '003') -- ASB travel certs are processed seperately with fullfillment.dbo.-- ASB Travel Certs are processed seperately with fullfillment.dbo.usp_tc_SaveDebitCredit_History
  AND (TC.TCStatus = 2)
  AND (M.redstatus = 5)

  UPDATE Fullfillment.dbo.Main
  SET redstatus = 7
  FROM Fullfillment.dbo.Main M
    INNER JOIN Fullfillment.dbo.TravelCert TC
    ON M.TransID = TC.TransID
  WHERE M.TipFirst NOT IN ('002', '003') -- ASB travel certs are processed seperately with fullfillment.dbo.-- ASB Travel Certs are processed seperately with fullfillment.dbo.usp_tc_SaveDebitCredit_History
  AND (TC.TCStatus = 2) 
  AND (M.redstatus = 5)
  
  IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
	
COMMIT TRANSACTION
GO
