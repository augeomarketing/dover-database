USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_generatePickList]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu db and catalog db to retreive packinglist, based on the sid_package_id >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_generatePickList]
@reportNumber int,
@redemptionDate DATETIME

AS

IF (@reportNumber = 1)	
	SELECT Convert(INT, P.sid_package_id) AS packageID
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_histDate, 112)
			= DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
			AND P.dim_package_active = 1
			AND P.dim_package_value BETWEEN 0.01 AND 75
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
			ORDER BY P.sid_package_id

IF (@reportNumber = 2)	
	SELECT Convert(INT, P.sid_package_id) AS packageID
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_histDate, 112)
			= DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
			AND P.dim_package_active = 1
			AND P.dim_package_value BETWEEN 75.01 AND 199.99
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
			ORDER BY P.sid_package_id
			
IF (@reportNumber = 3)	
	SELECT Convert(INT, P.sid_package_id) AS packageID
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_histDate, 112)
			= DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
			AND P.dim_package_active = 1
			AND P.dim_package_value >= 200
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) < 9
			ORDER BY P.sid_package_id

IF (@reportNumber = 4)	
	SELECT Convert(INT, P.sid_package_id) AS packageID
			FROM nowvu.dbo.package P
			LEFT JOIN nowvu.dbo.packageContent PC
			ON P.sid_package_id = PC.sid_package_id
			LEFT JOIN catalog.dbo.catalog C
			ON PC.dim_packagecontent_itemnumber = ('GC' + C.dim_catalog_code)
			LEFT JOIN catalog.dbo.catalogdescription CD
			ON C.sid_catalog_id = CD.sid_catalog_id
			WHERE CONVERT(char(8), P.dim_package_histDate, 112)
			= DATEADD(DAY, DateDiff(Day, 0, @redemptionDate), 0)
			AND P.dim_package_active = 1
			GROUP BY P.sid_package_id
			HAVING Sum(PC.dim_packagecontent_catalogQTY) > 8
			ORDER BY P.sid_package_id
GO
