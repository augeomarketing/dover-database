USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_TypeMerchTotals]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Oman>
-- Create date: <Oct,20,2009>
-- Description:	<Queries invoice detail for type record set>
-- =============================================
CREATE PROCEDURE [dbo].[sproc_TypeMerchTotals]
	@tipprefix char(3),
	@typequery varchar(50),
	@points int OUTPUT,
	@qty int OUTPUT,
	@Total money OUTPUT

	
AS

Declare @Multiplier Decimal(20,10)
IF  @typequery = 'Bonus'
	BEGIN
	SET @Multiplier = 
	(Select distinct coalesce(dim_BillingProgram_Bonus,0) as dim_billingprogram_bonus from nowvu.dbo.billingprogram as BP
	INNER JOIN nowvu.dbo.ProgramTipLookup as PTL
	ON PTL.sid_billingProgram_id = BP.sid_billingProgram_id
	WHERE dbnumber = @tipprefix)
	END 
IF  @typequery = 'Redeem Merchandise'
	BEGIN
	SET @Multiplier = 
	(Select distinct coalesce(dim_BillingProgram_Merch,0) as dim_BillingProgram_Merch from nowvu.dbo.billingprogram as BP
	INNER JOIN nowvu.dbo.ProgramTipLookup as PTL
	ON PTL.sid_billingProgram_id = BP.sid_billingProgram_id
	WHERE dbnumber = @tipprefix)
	END 
IF  @typequery = 'Tunes'
	BEGIN
	SET @Multiplier = 
	(Select distinct coalesce(dim_BillingProgram_tunesperpoint,0) as dim_BillingProgram_tunesperpoint from nowvu.dbo.billingprogram as BP
	INNER JOIN nowvu.dbo.ProgramTipLookup as PTL
	ON PTL.sid_billingProgram_id = BP.sid_billingProgram_id
	WHERE dbnumber = @tipprefix)
	END  
IF  @typequery = 'Ringtone'
	BEGIN
	SET @Multiplier = 
	(Select distinct coalesce(dim_BillingProgram_RTperpoint, 0) as dim_BillingProgram_RTperpoint from nowvu.dbo.billingprogram as BP
	INNER JOIN nowvu.dbo.ProgramTipLookup as PTL
	ON PTL.sid_billingProgram_id = BP.sid_billingProgram_id
	WHERE dbnumber = @tipprefix)
	END 

	
		CREATE TABLE #typetotal(
		transid char(40),
		tipfirst char(3),
		client varchar(255),
		Accountnum char(15),
		Name varchar(50),
		[type] varchar(30),
		product varchar(300),
		Qty int,
		Points int,
		Date smalldatetime,
		source varchar(10),
		cashvalue smallmoney)

	Insert INTO #typetotal
	EXEC dbo.sproc_Invoicedetail  @tipprefix
	
    SET @qty = coalesce((Select sum(coalesce(qty,0)) FROM #Typetotal WHERE type = @typequery),0)
	SET @Points = coalesce((Select sum(coalesce(points,0)) FROM #Typetotal WHERE type = @typequery),0)
	SET @Total =coalesce(
	(SELECT  SUM( coalesce(@Multiplier,0) * coalesce(points,0) ) as Total
	FROM #Typetotal 
	WHERE type = @typequery),0)

		
	drop table #typetotal


/*
use nowvu

declare @T money
Declare @P int
Declare @Q int
EXEC nowvu.dbo.sproc_TypeMerchTotals 209, [bonus], @Total =  @T output,  @points = @P OUTPUT, @qty = @q OUTPUT
SELECT @t AS [Total], @P AS [Points], @Q as Qty

Exec nowvu.dbo.sproc_Invoicedetail 209
Select * from nowvu.dbo.billingprogram
select * from nowvu.dbo.programtiplookup
*/
GO
