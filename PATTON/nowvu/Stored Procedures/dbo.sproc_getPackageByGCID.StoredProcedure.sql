USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_getPackageByGCID]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<DSeavey>
-- Create date: <Oct 28,2009>
-- Description:	<Queries nowvu.dbo.packageCardNum to retreive package ID, based on a gift card number (GCID) >
-- =============================================
CREATE PROCEDURE [dbo].[sproc_getPackageByGCID]
@giftCardID varchar(40)

AS

SELECT sid_package_id AS pkgID, dim_packageCardNum_active
FROM nowvu.dbo.packageCardNum
WHERE dim_packagecardnum_number = @giftCardID

/*
EXECUTE sproc_getPackageByGCID '04130041441999907900112167'
*/
GO
