USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_sproc_clearShippedDate]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_sproc_clearShippedDate]
@packageID varChar(50)

AS

BEGIN TRANSACTION

UPDATE NowVU.dbo.package 
SET dim_package_shippedDate = Null
WHERE sid_package_id = @packageID 


UPDATE fullfillment.dbo.main 
SET redreqfuldate = Null
WHERE Transid in  
(SELECT transID
 FROM nowvu.dbo.packagetransid PT
 WHERE PT.sid_package_id = @packageID)
 
COMMIT TRANSACTION
GO
