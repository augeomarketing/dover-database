USE [NowVu]
GO
/****** Object:  StoredProcedure [dbo].[sproc_batchUpdateCashBackCreditSent]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_batchUpdateCashBackCreditSent]

AS

BEGIN TRANSACTION

    BEGIN TRY
	   UPDATE fullfillment.dbo.cashback
		  SET cbcreditdate = getdate()
	   FROM fullfillment.dbo.main M
	   INNER JOIN fullfillment.dbo.cashback CB
		  ON M.TransID = CB.TransID
	   WHERE M.redstatus = 5

		
	   UPDATE fullfillment.dbo.main
		  SET redstatus = 7
	   FROM fullfillment.dbo.main M
	   INNER JOIN fullfillment.dbo.cashback CB
		  ON M.TransID = CB.TransID
	   WHERE M.redstatus = 5

	   COMMIT TRANSACTION

    END TRY

    BEGIN CATCH
	   if @@TRANCOUNT > 0
		  rollback transaction
    END CATCH
GO
