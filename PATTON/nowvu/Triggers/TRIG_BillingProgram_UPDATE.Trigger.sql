USE [NowVu]
GO
/****** Object:  Trigger [TRIG_BillingProgram_UPDATE]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRIG_BillingProgram_UPDATE] ON [dbo].[BillingProgram] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_BillingProgram_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_BillingProgram_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_BillingProgram_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE BillingProgram SET dim_BillingProgram_lastmodified = getdate() WHERE sid_BillingProgram_id = @sid_BillingProgram_id 
              FETCH NEXT FROM UPD_QUERY INTO @sid_BillingProgram_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
