
USE [fullfillment]
GO
CREATE NONCLUSTERED INDEX [idx_main_histdate_includes]
ON [dbo].[Main] ([HistDate])
INCLUDE ([TipNumber],[TipFirst],[Source],[Name1],[Name2],[TransID],[Vendor],[ItemNumber],[Catalogdesc],[CatalogQty],[SAddress1],[SAddress2],[SCity],[SState],[SZip],[SCountry],[Phone1],[Phone2],[Email],[RedStatus],[RedReqFulDate],[InvoiceDate],[Notes],[Routing])
GO
