USE [NowVu]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_transIDInPKG]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_transIDInPKG](@transID varChar(50) )
RETURNS int
AS
BEGIN
	Return(SELECT COUNT(*)
			FROM NowVu.dbo.packagetransid
			WHERE transid = @transID)
			
END
GO
