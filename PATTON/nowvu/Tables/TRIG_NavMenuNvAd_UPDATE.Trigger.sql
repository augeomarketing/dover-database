USE [Nowvu]
GO
/****** Object:  Trigger [TRIG_NavMenuNvAd_UPDATE]    Script Date: 02/01/2010 10:16:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_NavMenuNvAd_UPDATE] ON [dbo].[NavMenuNvAd] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 

	INSERT INTO dbo.archiveNavMenuNvAd
	SELECT *, getdate(), getdate()
	FROM deleted

	UPDATE c
		SET dim_NavMenuNvAd_lastmodified = getdate()
	FROM dbo.NavMenuNvAd c JOIN deleted del
		ON c.sid_NavMenuNvAd_id = del.sid_NavMenuNvAd_id

 END
GO
