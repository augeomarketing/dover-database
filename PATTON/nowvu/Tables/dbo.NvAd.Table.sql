USE [NowVu]
GO
/****** Object:  Table [dbo].[NvAd]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NvAd](
	[sid_nvad_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_nvad_group] [varchar](50) NOT NULL,
	[dim_nvad_description] [varchar](50) NOT NULL,
	[dim_nvad_created] [datetime] NOT NULL,
	[dim_NvAd_lastmodified] [datetime] NOT NULL,
	[dim_nvad_active] [int] NOT NULL,
	[sid_navmenu_id] [int] NULL,
 CONSTRAINT [PK_NvAd] PRIMARY KEY CLUSTERED 
(
	[sid_nvad_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[NvAd] ADD  CONSTRAINT [DF_NvAd_dim_navad_created]  DEFAULT (getdate()) FOR [dim_nvad_created]
GO
ALTER TABLE [dbo].[NvAd] ADD  CONSTRAINT [DF_NvAd_dim_navad_lastmodfied]  DEFAULT (getdate()) FOR [dim_NvAd_lastmodified]
GO
ALTER TABLE [dbo].[NvAd] ADD  CONSTRAINT [DF_NvAd_dim_navad_active]  DEFAULT (1) FOR [dim_nvad_active]
GO
