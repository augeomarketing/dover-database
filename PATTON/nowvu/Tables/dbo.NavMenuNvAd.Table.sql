USE [NowVu]
GO
/****** Object:  Table [dbo].[NavMenuNvAd]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NavMenuNvAd](
	[sid_NavMenuNvAd_id] [int] NOT NULL,
	[sid_Navmenu_sid] [int] NOT NULL,
	[sid_NvAd_id] [int] NOT NULL,
	[dim_NavMenuNvAd_lastmodified] [datetime] NOT NULL,
	[dim_NavMenuNvAd_created] [datetime] NOT NULL
) ON [PRIMARY]
GO
