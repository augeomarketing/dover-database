USE [NowVu]
GO
/****** Object:  Table [dbo].[ProgramTipLookup]    Script Date: 07/28/2010 16:19:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProgramTipLookup](
	[sid_ProgramTipLookup_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_BillingProgram_id] [int] NOT NULL,
	[DBNumber] [varchar](50) NOT NULL,
	[sid_ProgramGroupLookup_id] [int] NULL,
	[dim_ProgramTipLookup_active] [int] NOT NULL,
	[dim_ProgramTipLookup_created] [datetime] NOT NULL,
	[dim_ProgramTipLookup_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_ProgramTipLookup] PRIMARY KEY CLUSTERED 
(
	[sid_ProgramTipLookup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ProgramTipLookup] ADD  CONSTRAINT [DF_ProgramTipLookup_dim_ProgramTipLookup_active]  DEFAULT (1) FOR [dim_ProgramTipLookup_active]
GO
ALTER TABLE [dbo].[ProgramTipLookup] ADD  CONSTRAINT [DF_ProgramTipLookup_dim_ProgramTipLookup_created]  DEFAULT (getdate()) FOR [dim_ProgramTipLookup_created]
GO
ALTER TABLE [dbo].[ProgramTipLookup] ADD  CONSTRAINT [DF_ProgramTipLookup_dim_ProgramTipLookup_lastmodified]  DEFAULT (getdate()) FOR [dim_ProgramTipLookup_lastmodified]
GO
