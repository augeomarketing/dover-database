USE [Nowvu]
GO
/****** Object:  Trigger [TRIG_NvAd_UPDATE]    Script Date: 02/01/2010 10:16:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRIG_NvAd_UPDATE] ON [dbo].[NvAd] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_NvAd_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_NvAd_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_NvAd_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE NvAd SET dim_NvAd_lastmodified = getdate() WHERE sid_NvAd_id = @sid_NvAd_id 
              FETCH NEXT FROM UPD_QUERY INTO @sid_NvAd_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
