USE [rewardsnow]
GO
/****** Object:  Table [dbo].[dbprocessinfo]    Script Date: 07/28/2010 16:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dbprocessinfo](
	[DBNumber] [varchar](50) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[DBAvailable] [char](1) NOT NULL,
	[ClientCode] [varchar](50) NULL,
	[ClientName] [varchar](256) NULL,
	[ProgramName] [varchar](256) NULL,
	[DBLocationPatton] [varchar](50) NULL,
	[DBLocationNexl] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[DateJoined] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[LastTipNumberUsed] [varchar](15) NULL,
	[PointsExpireFrequencyCd] [varchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[WelcomeKitGroupName] [varchar](50) NULL,
	[GenerateWelcomeKit] [varchar](1) NOT NULL,
	[sid_FiProdStatus_statuscode] [varchar](1) NULL,
	[IsStageModel] [int] NULL,
	[ExtractGiftCards] [varchar](1) NOT NULL,
	[ExtractCashBack] [varchar](1) NOT NULL,
	[RNProgramName] [varchar](50) NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [int] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Landing] [varchar](255) NULL,
	[TermsPage] [varchar](50) NULL,
	[FaqPage] [varchar](20) NULL,
	[EarnPage] [varchar](20) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StatementNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactPhone1] [varchar](12) NULL,
	[ContactPhone2] [varchar](12) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[ServerName] [varchar](50) NULL,
	[TransferHistToRn1] [varchar](1) NOT NULL,
	[CalcDailyExpire] [varchar](1) NOT NULL,
	[hasonlinebooking] [int] NOT NULL,
 CONSTRAINT [PK_dbprocessinfo_1] PRIMARY KEY CLUSTERED 
(
	[DBNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IDX_dbnumber_dbnamepatton] ON [dbo].[dbprocessinfo] 
(
	[DBNumber] ASC,
	[DBNamePatton] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_dbnumber_prodstatus] ON [dbo].[dbprocessinfo] 
(
	[DBNumber] ASC,
	[sid_FiProdStatus_statuscode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dbprocessinfo]  WITH NOCHECK ADD  CONSTRAINT [FK_dbprocessinfo_FiProdStatus] FOREIGN KEY([sid_FiProdStatus_statuscode])
REFERENCES [dbo].[FiProdStatus] ([sid_FiProdStatus_statuscode])
GO
ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [FK_dbprocessinfo_FiProdStatus]
GO
ALTER TABLE [dbo].[dbprocessinfo]  WITH NOCHECK ADD  CONSTRAINT [CK_dbprocessinfo_ExtractCashBack] CHECK  (([ExtractCashBack] = 'N' or [ExtractCashBack] = 'Y'))
GO
ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_ExtractCashBack]
GO
ALTER TABLE [dbo].[dbprocessinfo]  WITH CHECK ADD  CONSTRAINT [CK_dbprocessinfo_hasonlinebooking] CHECK  (([hasonlinebooking]=(1) OR [hasonlinebooking]=(0)))
GO
ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_hasonlinebooking]
GO
ALTER TABLE [dbo].[dbprocessinfo]  WITH NOCHECK ADD  CONSTRAINT [CK_dbprocessinfo_VGC] CHECK  (([ExtractGiftCards] = 'N' or [ExtractGiftCards] = 'Y'))
GO
ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_VGC]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_GenerateWelcomeKit]  DEFAULT ('Y') FOR [GenerateWelcomeKit]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_IsStageModel]  DEFAULT (0) FOR [IsStageModel]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ExtractGiftCards]  DEFAULT ('N') FOR [ExtractGiftCards]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ExtractCashBack]  DEFAULT ('N') FOR [ExtractCashBack]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_TransferHistToRn1]  DEFAULT ('N') FOR [TransferHistToRn1]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_CalcDailyExpire]  DEFAULT ('N') FOR [CalcDailyExpire]
GO
ALTER TABLE [dbo].[dbprocessinfo] ADD  DEFAULT ((0)) FOR [hasonlinebooking]
GO
