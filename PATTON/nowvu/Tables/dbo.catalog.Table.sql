USE [catalog]
GO
/****** Object:  Table [dbo].[catalog]    Script Date: 07/28/2010 16:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catalog](
	[sid_catalog_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_catalog_code] [varchar](20) NOT NULL,
	[dim_catalog_trancode] [char](2) NOT NULL,
	[dim_catalog_dollars] [decimal](10, 2) NOT NULL,
	[dim_catalog_imagelocation] [varchar](1024) NOT NULL,
	[dim_catalog_imagealign] [varchar](50) NOT NULL,
	[dim_catalog_created] [datetime] NOT NULL,
	[dim_catalog_lastmodified] [datetime] NOT NULL,
	[dim_catalog_active] [int] NOT NULL,
	[dim_catalog_parentid] [int] NOT NULL,
	[sid_status_id] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_catalog_brochure] [int] NOT NULL,
 CONSTRAINT [PK_Catalog] PRIMARY KEY CLUSTERED 
(
	[sid_catalog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalog_179_1607676775__K11_K9_K1_K2] ON [dbo].[catalog] 
(
	[sid_status_id] ASC,
	[dim_catalog_active] ASC,
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalog_179_1607676775__K9_K1_K2] ON [dbo].[catalog] 
(
	[dim_catalog_active] ASC,
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalog_179_1607676775__K9_K1_K2_K11] ON [dbo].[catalog] 
(
	[dim_catalog_active] ASC,
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC,
	[sid_status_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catalog_code_trancode_dollars_imagelocation_imagealign_active_status_userinfo_brochure] ON [dbo].[catalog] 
(
	[dim_catalog_code] ASC,
	[dim_catalog_trancode] ASC,
	[dim_catalog_dollars] ASC,
	[dim_catalog_imagelocation] ASC,
	[dim_catalog_imagealign] ASC,
	[dim_catalog_active] ASC,
	[sid_status_id] ASC,
	[sid_userinfo_id] ASC,
	[dim_catalog_brochure] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catalog_trancode_dollars_imagelocation_imagealign_active_status_userinfo_brochure_id] ON [dbo].[catalog] 
(
	[sid_catalog_id] ASC,
	[dim_catalog_trancode] ASC,
	[dim_catalog_dollars] ASC,
	[dim_catalog_imagelocation] ASC,
	[dim_catalog_imagealign] ASC,
	[dim_catalog_active] ASC,
	[sid_status_id] ASC,
	[sid_userinfo_id] ASC,
	[dim_catalog_brochure] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_catalog_sid_catalog_id_dim_catalog_code] ON [dbo].[catalog] 
(
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogCode]  DEFAULT ('ABC') FOR [dim_catalog_code]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogTrancode]  DEFAULT ('RM') FOR [dim_catalog_trancode]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogDollars]  DEFAULT (0.0) FOR [dim_catalog_dollars]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogImageLocation]  DEFAULT ('../image') FOR [dim_catalog_imagelocation]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_imagealign]  DEFAULT ('right') FOR [dim_catalog_imagealign]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogCreated]  DEFAULT (getdate()) FOR [dim_catalog_created]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogLastModified]  DEFAULT (getdate()) FOR [dim_catalog_lastmodified]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogActive]  DEFAULT (1) FOR [dim_catalog_active]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogParentID]  DEFAULT ((-1)) FOR [dim_catalog_parentid]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_sdi_userinfo_id]  DEFAULT (0) FOR [sid_userinfo_id]
GO
ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_brochure]  DEFAULT (0) FOR [dim_catalog_brochure]
GO
