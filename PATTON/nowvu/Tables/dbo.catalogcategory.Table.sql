USE [catalog]
GO
/****** Object:  Table [dbo].[catalogcategory]    Script Date: 07/28/2010 16:19:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catalogcategory](
	[sid_catalogcategory_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_catalogcategory_created] [datetime] NOT NULL,
	[dim_catalogcategory_lastmodified] [datetime] NOT NULL,
	[dim_catalogcategory_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_CatalogCategory] PRIMARY KEY CLUSTERED 
(
	[sid_catalogcategory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalogcategory_179_331148225__K3_K6_K2_K1] ON [dbo].[catalogcategory] 
(
	[sid_category_id] ASC,
	[dim_catalogcategory_active] ASC,
	[sid_catalog_id] ASC,
	[sid_catalogcategory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catalogcategory_catalog_category] ON [dbo].[catalogcategory] 
(
	[sid_category_id] ASC,
	[sid_catalog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[catalogcategory] ADD  CONSTRAINT [DF_CatalogCategory_CatalogCategoryCreated]  DEFAULT (getdate()) FOR [dim_catalogcategory_created]
GO
ALTER TABLE [dbo].[catalogcategory] ADD  CONSTRAINT [DF_CatalogCategory_CatalogCategoryLastModified]  DEFAULT (getdate()) FOR [dim_catalogcategory_lastmodified]
GO
ALTER TABLE [dbo].[catalogcategory] ADD  CONSTRAINT [DF_CatalogCategory_CatalogCategoryActive]  DEFAULT (1) FOR [dim_catalogcategory_active]
GO
ALTER TABLE [dbo].[catalogcategory] ADD  CONSTRAINT [DF_catalogcategory_sid_userinfo_id]  DEFAULT (0) FOR [sid_userinfo_id]
GO
