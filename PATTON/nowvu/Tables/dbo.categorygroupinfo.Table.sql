USE [catalog]
GO
/****** Object:  Table [dbo].[categorygroupinfo]    Script Date: 07/28/2010 16:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categorygroupinfo](
	[sid_categorygroupinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[sid_groupinfo_id] [int] NOT NULL,
	[dim_categorygroupinfo_created] [datetime] NOT NULL,
	[dim_categorygroupinfo_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_categorygroupinfo] PRIMARY KEY CLUSTERED 
(
	[sid_categorygroupinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catid_groupid] ON [dbo].[categorygroupinfo] 
(
	[sid_category_id] ASC,
	[sid_groupinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[categorygroupinfo] ADD  CONSTRAINT [DF_categorygroupinfo_dim_categorygroupinfo_created]  DEFAULT (getdate()) FOR [dim_categorygroupinfo_created]
GO
ALTER TABLE [dbo].[categorygroupinfo] ADD  CONSTRAINT [DF_categorygroupinfo_dim_categorygroupinfo_lastmodified]  DEFAULT (getdate()) FOR [dim_categorygroupinfo_lastmodified]
GO
