USE [catalog]
GO
/****** Object:  Table [dbo].[loyaltycatalog]    Script Date: 07/28/2010 16:19:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[loyaltycatalog](
	[dwid_loyaltycatalog_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_loyaltycatalog_id] [int] NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_loyaltycatalog_pointvalue] [int] NOT NULL,
	[dim_loyaltycatalog_bonus] [int] NOT NULL,
	[dim_loyaltycatalog_created] [datetime] NOT NULL,
	[dim_loyaltycatalog_lastmodified] [datetime] NOT NULL,
	[dim_loyaltycatalog_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_loyaltycatalog_1] PRIMARY KEY CLUSTERED 
(
	[dwid_loyaltycatalog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_loyaltycatalog_179_1204199340__K3_K4_K5_K6] ON [dbo].[loyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC,
	[dim_loyaltycatalog_pointvalue] ASC,
	[dim_loyaltycatalog_bonus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_loyaltycatalog_lid_cid_active_point] ON [dbo].[loyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC,
	[dim_loyaltycatalog_active] ASC,
	[dim_loyaltycatalog_pointvalue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_loyaltycatlaog_sid_loyalty_id_sid_catalog_id] ON [dbo].[loyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC,
	[dim_loyaltycatalog_pointvalue] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[loyaltycatalog] ADD  CONSTRAINT [DF_LoyaltyCatalog_LoyaltyCatalogTipPointValue]  DEFAULT (0) FOR [dim_loyaltycatalog_pointvalue]
GO
ALTER TABLE [dbo].[loyaltycatalog] ADD  CONSTRAINT [DF_LoyaltyCatalog_LoyaltyCatalogBonus]  DEFAULT (0) FOR [dim_loyaltycatalog_bonus]
GO
ALTER TABLE [dbo].[loyaltycatalog] ADD  CONSTRAINT [DF_LoyaltyCatalog_LoyaltyCatalogCreated]  DEFAULT (getdate()) FOR [dim_loyaltycatalog_created]
GO
ALTER TABLE [dbo].[loyaltycatalog] ADD  CONSTRAINT [DF_LoyaltyCatalog_LoyaltyCatalogLastModified]  DEFAULT (getdate()) FOR [dim_loyaltycatalog_lastmodified]
GO
ALTER TABLE [dbo].[loyaltycatalog] ADD  CONSTRAINT [DF_LoyaltyCatalog_LoyaltyCatalogActive]  DEFAULT (1) FOR [dim_loyaltycatalog_active]
GO
ALTER TABLE [dbo].[loyaltycatalog] ADD  CONSTRAINT [DF_loyaltycatalog_sid_userinfo_id]  DEFAULT (0) FOR [sid_userinfo_id]
GO
