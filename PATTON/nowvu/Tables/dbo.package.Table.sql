USE [NowVu]
GO
/****** Object:  Table [dbo].[package]    Script Date: 07/28/2010 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[package](
	[sid_package_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_package_tipnumber] [char](15) NOT NULL,
	[dim_package_name1] [varchar](50) NULL,
	[dim_package_name2] [varchar](50) NULL,
	[dim_package_saddress1] [varchar](50) NULL,
	[dim_package_saddress2] [varchar](50) NULL,
	[dim_package_saddress3] [varchar](50) NULL,
	[dim_package_saddress4] [varchar](50) NULL,
	[dim_package_sstate] [varchar](50) NULL,
	[dim_package_scity] [varchar](50) NULL,
	[dim_package_szip] [varchar](15) NULL,
	[dim_package_scountry] [varchar](50) NULL,
	[dim_package_value] [money] NOT NULL,
	[dim_package_created] [datetime] NOT NULL,
	[dim_package_lastmodified] [datetime] NOT NULL,
	[dim_package_active] [int] NOT NULL,
	[dim_package_redstatus] [int] NOT NULL,
	[dim_package_histdate] [datetime] NULL,
	[dim_package_shippedDate] [datetime] NULL,
 CONSTRAINT [PK_package] PRIMARY KEY CLUSTERED 
(
	[sid_package_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[package] ADD  CONSTRAINT [DF_package_dim_package_tipnumber]  DEFAULT (1) FOR [dim_package_tipnumber]
GO
ALTER TABLE [dbo].[package] ADD  CONSTRAINT [DF_package_dim_package_Value]  DEFAULT (0) FOR [dim_package_value]
GO
ALTER TABLE [dbo].[package] ADD  CONSTRAINT [DF_package_dim_package_created]  DEFAULT (getdate()) FOR [dim_package_created]
GO
ALTER TABLE [dbo].[package] ADD  CONSTRAINT [DF_package_dim_package_lastmodified]  DEFAULT (getdate()) FOR [dim_package_lastmodified]
GO
ALTER TABLE [dbo].[package] ADD  CONSTRAINT [DF_package_dim_package_active]  DEFAULT (1) FOR [dim_package_active]
GO
ALTER TABLE [dbo].[package] ADD  CONSTRAINT [DF_package_dim_package_redstatus]  DEFAULT (13) FOR [dim_package_redstatus]
GO
