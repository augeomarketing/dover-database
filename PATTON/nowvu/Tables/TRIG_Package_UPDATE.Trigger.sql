USE [Nowvu]
GO
/****** Object:  Trigger [TRIG_Package_UPDATE]    Script Date: 02/01/2010 10:16:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create TRIGGER [dbo].[TRIG_Package_UPDATE] ON [dbo].[package] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_Package_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_Package_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_Package_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE Package SET dim_Package_lastmodified = getdate() WHERE sid_Package_id = @sid_Package_id 
              FETCH NEXT FROM UPD_QUERY INTO @sid_Package_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
