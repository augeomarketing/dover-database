USE [598FirstWesternConsumer]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 09/25/2009 14:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Quarterly_Audit_ErrorFile] ON [dbo].[Quarterly_Audit_ErrorFile] 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__5DCAEF64]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__5EBF139D]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__5FB337D6]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__60A75C0F]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__619B8048]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__628FA481]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__6383C8BA]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__6477ECF3]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__656C112C]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__66603565]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__6754599E]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__68487DD7]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__693CA210]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Curre__6A30C649]  DEFAULT (0) FOR [Currentend]
GO
