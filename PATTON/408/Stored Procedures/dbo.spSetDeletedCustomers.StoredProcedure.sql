USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetDeletedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetDeletedCustomers]
GO
CREATE PROCEDURE [dbo].[spSetDeletedCustomers]   AS

update customer set status = 'P',statusdescription = 'Pending Deletion'
where status = 'D'
GO
