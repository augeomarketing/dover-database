USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO
CREATE  PROCEDURE [dbo].[spPendingPurgePN]  AS    

/* Testing Parameters */

declare @purgedate varchar(10)
--set @purgedate = '05/30/2009'

/* Declarations */

declare @tipnumber nvarchar(15)
declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge
(tipnumber)
select 
tipnumber
from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare PendingPurge_crsr cursor
for Select *
From PendingPurge

Open PendingPurge_crsr
/*                  */

set @PostPurgeActivity = ' '


Fetch PendingPurge_crsr  
into   @tipnumber
IF @@FETCH_STATUS = 1
	goto Fetch_Error

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	select @PostPurgeActivity = 'y'
	from history where @tipnumber = tipnumber and histdate > @purgedate

	if @PostPurgeActivity <> 'y'
	   begin
	     update customer
	     set status = 'C', statusdescription = 'Closed'
	     where tipnumber = @tipnumber

	     delete from PendingPurge
	     where tipnumber = @tipnumber
	   end
		


 
set @PostPurgeActivity = ' '

FETCH_NEXT:
	
	Fetch PendingPurge_crsr  
        into   @tipnumber
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:






close  PendingPurge_crsr
deallocate  PendingPurge_crsr
GO
