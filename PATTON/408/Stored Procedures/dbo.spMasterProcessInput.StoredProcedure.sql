USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMasterProcessInput]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMasterProcessInput]
GO
CREATE PROCEDURE [dbo].[spMasterProcessInput] 
	@Tip						Char(3), 
	@StartDateInput	VarChar(10) ,
	@EndDateInput     VarChar(10)
	AS   

declare @SprocReturn  int 
Exec  @SprocReturn = spGenTIPNumbersStage                       
If @SprocReturn = 0	 Exec  @SprocReturn  = sprollcustomerscrub		@EndDateInput
If @SprocReturn = 0  Exec  @SprocReturn  = spInputScrub					@EndDateInput     
If @SprocReturn = 0  Exec  @SprocReturn  = sploadlastname                             
If @SprocReturn = 0  Exec  @SprocReturn  = sploadinputcustomer                        
If @SprocReturn = 0  Exec  @SprocReturn  = spLoadCustomerStage	@EndDateInput
If @SprocReturn = 0  Exec  @SprocReturn  = spLoadAffiliatStage		@EndDateInput      
If @SprocReturn = 0  Exec  @SprocReturn  = spLoadTransStandard		@EndDateInput             
If @SprocReturn = 0  Exec  @SprocReturn = spImportTransToStage @Tip, @EndDateInput
If @SprocReturn = 0  Exec  @SprocReturn = spStageMonthlyStatement @StartDateInput, @EndDateInput
If @SprocReturn = 0  Exec  @SprocReturn = spStageCurrentMonthActivity @EndDateInput 
If @SprocReturn = 0  Exec  @SprocReturn = spStageMonthlyAuditValidation 
return @SprocReturn  

GO
