USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInsertHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInsertHistory]
GO
CREATE PROCEDURE [dbo].[spInsertHistory] 
as
/****************************************************************************/
/*                                                                          */
/* Procedure to insert monthly transactions based on the input transactions */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

	  insert history (tipnumber,acctid,histdate,trancode,trancount,points,[description],secid,ratio,overage)
	       select tipnumber,cardnumber as acctid,transdate as histdate,trancode,trancount,points,[description],secid,ratio,overage from transactions

	update 	cus
		set runavailable = dbo.fnSumPointsByTip(tipnumber),
		runbalance = dbo.fnSumPointsByTip(tipnumber) + runredeemed
	from dbo.customer cus 
	where cus.tipnumber in (select distinct tipnumber from dbo.transactions)
GO
