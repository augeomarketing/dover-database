USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyReportSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyReportSummary]
GO
CREATE PROCEDURE [dbo].[spMonthlyReportSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table ReportSummary

insert ReportSummary (dateadded)
values (@enddate)

update  ReportSummary set purchases = (select SUM(CONVERT(int, pointspurchasedCR)) FROM monthly_statement_file)

update  ReportSummary set returned =(select SUM(CONVERT(int, pointsreturnedCR)) FROM monthly_statement_file)

update ReportSummary set bonuses = (select sum(convert(int,pointsbonus)) from  monthly_statement_file)

update BegBalanceCheck set newbegbalance = (select sum(convert(int,pointsbegin)) from monthly_statement_file)
GO
