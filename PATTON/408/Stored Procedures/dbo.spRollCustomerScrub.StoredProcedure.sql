USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollCustomerScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRollCustomerScrub]
GO
CREATE PROCEDURE [dbo].[spRollCustomerScrub] @enddate varchar(10)
 AS

/********************************************************************************************/
-- CHANGES:
-- RDT 4/15/2011 Changed 1Bridge Codes to Card Bin numbers. 
/********************************************************************************************/

update roll_customer  set accttype = 'Credit', accttypedesc = 'Credit Card'
where left(acctid,6) = '407958' 

/*update roll_customer  set accttype = 'Debit', accttypedesc = 'Debit Card'
where left(acctid,6) = ''  */

update roll_customer set DateAdded =  (select dateadded from  CUSTOMER_stage
where customer_stage.tipnumber = roll_customer.tipnumber)

update roll_customer set dateadded = CONVERT(datetime, @enddate)
where  (dateadded is NULL)
GO
