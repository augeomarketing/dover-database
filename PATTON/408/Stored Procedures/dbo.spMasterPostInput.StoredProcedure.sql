USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMasterPostInput]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMasterPostInput]
GO
CREATE PROCEDURE [dbo].[spMasterPostInput] 
	@Tip						Char(3), 
	@StartDateInput	VarChar(10) ,
	@EndDateInput     VarChar(10)
	AS   

declare @SprocReturn  int 
Exec  @SprocReturn = spImportStageToProd @Tip
If @SprocReturn = 0	 Exec  @SprocReturn  = RewardsNow.dbo.spRemoveSpecialCharactersFromCustomer	@Tip
If @SprocReturn = 0  Exec  @SprocReturn  = RewardsNow.dbo.spRemoveSpecialCharactersFromAffiliat		@Tip
If @SprocReturn = 0  Exec  @SprocReturn  = RewardsNow.dbo.usp_AddFIPostToWeb									@Tip,@StartDateinput, @EndDateInput
If @SprocReturn = 0  Exec  @SprocReturn  = RewardsNow.dbo.usp_GenerateStandardWelcomeKitForFI		@Tip,@StartDateinput, @EndDateInput
If @SprocReturn = 0  Exec  @SprocReturn  = RewardsNow.dbo.pnotifypostedtoweb											@Tip
If @SprocReturn = 0  Exec  @SprocReturn  = RewardsNow.dbo.spmonthlyparticipantcounts								@Tip,@EndDateInput

UPDATE Rewardsnow.dbo.dbprocessinfo 	SET        DBAvailable = 'Y'	WHERE  (DBNumber = @Tip )

execute maintenance.dbo.AddToReportQueue @Tip,@StartDateinput, @EndDateInput,'rtremblay@rewardsnow.com, itops@rewardsnow.com'

return @SprocReturn  

GO
