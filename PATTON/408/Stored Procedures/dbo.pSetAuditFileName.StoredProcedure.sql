USE [408]   
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetAuditFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetAuditFileName]
GO

CREATE PROCEDURE [dbo].[pSetAuditFileName] @newname nchar(100)  output
AS

declare @Filename char(50), @currentdate nchar(6), @workmonth nchar(2), @workyear nchar(4), @endingDate char(10)

set @endingDate=(select end_date from rewardsnow.dbo.monthly_process_dates where monthly_cycle = month(getdate())-1
					and fi_name = 'default')

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,4)

set @currentdate=@workmonth + @workyear

set @filename='M408_' + @currentdate + '.csv'
print @filename
 
set @newname=@filename
GO
