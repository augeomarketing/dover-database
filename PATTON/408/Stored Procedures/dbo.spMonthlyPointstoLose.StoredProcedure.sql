USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyPointstoLose]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyPointstoLose]
GO
CREATE PROCEDURE [dbo].[spMonthlyPointstoLose]
AS

update monthly_statement_file set exppnts = d.POINTSTOEXPIRE
from monthly_statement_file m join [rewardsnow].dbo.DailyPointsToExpire d on m.tipnumber = D.tipnumber
where POINTSTOEXPIRE > '0'

update monthly_statement_file set exppnts = '0' where exppnts is null
GO
