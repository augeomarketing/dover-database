USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyStatement]
GO

CREATE PROCEDURE [dbo].[spStageMonthlyStatement] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS

Declare @StartDate DateTime     
Declare @EndDate DateTime     


print 'start date parm'
print @StartDateparm

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    
set @Enddate = convert(datetime, @EndDateParm +' 23:59:59:990' )  


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin= Month( @StartDateParm)
set @MonthBucket='MonthBeg' + @monthbegin

/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, city, state, zipcode,status)
select tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, rtrim(city) , rtrim(state) , left(zipcode,5), status
from customer_stage

/* Load the statmement file with purchases          */
update Monthly_Statement_File
set PointsPurchasedCR =(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='63' )

/* Load the statmement file with bonuses            */
update Monthly_Statement_File
set pointsbonus=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode like'B%'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%'))
/* */ 
-- ADD PointsBonusMN 
update Monthly_Statement_File
set PointsBonusMN =(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode in ( 'F0','G0','F9','G9')))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode in ( 'F0','G0','F9','G9'))) 


/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode='IE' or trancode = 'XF'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode='IE' or trancode = 'XF'))

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased = PointsPurchasedCR  + PointsBonus + PointsAdded + PointsBonusMN

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with decrease redemptions          */
update Monthly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

update Monthly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode ='DZ')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode ='DZ')

/* Load the statmement file with returns            */
update Monthly_Statement_File
set PointsReturnedCR =(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='33')


/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode='XP' or trancode='DE'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and (trancode='XP' or trancode='DE'))

update Monthly_Statement_File
set ExpPnts=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='XP' )
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='XP' )


/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + PointsReturnedCR  + pointssubtracted + ExpPnts

/* Load the statmement file with the Beginning balance for the Month */
/*update Monthly_Statement_File
set pointsbegin=(select BeginningPoints from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber) */
set @SQLUpdate=N'update Monthly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

-- CHANGE(1) add code to update expiring points for customers 
--update monthly_statement_file set exppnts = d.dim_ExpiringPointsProjection_PointsToExpireThisPeriod
--from monthly_statement_file m join [rewardsnow].dbo.expiringpointsprojection d on m.tipnumber = D.sid_ExpiringPointsProjection_Tipnumber

update monthly_statement_file set exppnts = '0' where exppnts is null or ExpPnts < 0
GO
