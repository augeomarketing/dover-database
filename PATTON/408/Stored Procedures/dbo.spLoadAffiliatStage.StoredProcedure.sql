USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO
CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, DateAdded, Accttype, secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.acctid, c.TipNumber, @monthend, accttype, c.last6,  'A', accttypedesc, c.LastName, 0, c.custid
	from roll_Customer c where c.acctid not in ( Select acctid from Affiliat_Stage)

/*** Change(1) Update the Affiliat_Stage secid with any new replacement cards (last6)  ****/

  Update AFS Set secid = last6 
  from affiliat_stage afs Join Roll_customer rc on afs.acctid = rc.Acctid
  where (afs.acctid = rc.Acctid) and (SECID != last6)
GO
