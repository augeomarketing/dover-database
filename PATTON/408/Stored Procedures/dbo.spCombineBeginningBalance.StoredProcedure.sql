USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCombineBeginningBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCombineBeginningBalance]
GO
CREATE   PROCEDURE [dbo].[spCombineBeginningBalance] @TipPrimary char(15), @TipSecondary char(15)
AS 

Declare @MonthBucket char(10), @Month int, @SQLUpdate nvarchar(1000)

Set @Month = 1

While @Month < 13 
Begin 
	set @MonthBucket='MonthBeg' + cast(@Month as char)

	set @SQLUpdate = N'update Beginning_Balance_Table set '+ Quotename(@MonthBucket) + 
	   N'= ( select sum(' + Quotename(@MonthBucket) +' ) from  beginning_balance_table 
		where tipnumber = ''' +@TipPrimary + ''' or tipnumber = '''  + @TipSecondary + ' '') where tipnumber = ''' + @TipPrimary + ''''
--print @SQLUpdate 
exec sp_executesql @SQLUpdate

	
	set @Month = @Month + 1 

End
GO
