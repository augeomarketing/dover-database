USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetWelcomeKitFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetWelcomeKitFileName]
GO
CREATE PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @pagect varchar(6)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @pagect = (select count(*) from welcomekit)

set @filename='W' + @TipPrefix + @currentdate + '-' + @pagect + '.xls'
 
set @newname='\\patton\ops\408\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='\\patton\ops\408\Output\WelcomeKits\' + @filename
GO
