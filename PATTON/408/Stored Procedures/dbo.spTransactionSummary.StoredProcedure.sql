USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTransactionSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spTransactionSummary]
GO
CREATE PROCEDURE [dbo].[spTransactionSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table TransactionSummary

insert TransactionSummary (dateadded)
values (@enddate)

update  TransactionSummary set crpurchases = (select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '63' )
update  TransactionSummary set dbpurchases = (select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '67')
update  TransactionSummary set plpurchases = (select SUM(CONVERT(int, points)) FROM transactions  WHERE trancode = '61')

update   TransactionSummary set crreturned =(select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '33')
update   TransactionSummary set dbreturned =(select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '37')
update   TransactionSummary set plreturned =(select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '31')

update reportsummary set purchases = (select (crpurchases + dbpurchases + plpurchases) from transactionsummary)
update reportsummary set returned = (select (crreturned + dbreturned + plreturned) from transactionsummary)
GO
