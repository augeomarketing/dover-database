USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InitializeInput]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InitializeInput]
GO

-- =============================================
CREATE PROCEDURE [dbo].[usp_InitializeInput]
AS
BEGIN
	truncate table  input_customer
	truncate table roll_customer
	truncate table input_transactions
	truncate table deletework
	truncate table newlastname
END
GO
