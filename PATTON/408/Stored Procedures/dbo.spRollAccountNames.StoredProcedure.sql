USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollAccountNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRollAccountNames]
GO
CREATE PROCEDURE [dbo].[spRollAccountNames]  AS

update input_customer  set acctname2 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
(a.acctname2 is null or a.acctname2 = ' ') 

update input_customer  set acctname3 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and (a.acctname3 is null or a.acctname3 = ' ')

update input_customer  set acctname4 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
(a.acctname4 is null or a.acctname4 = ' ')

update input_customer  set acctname5 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and (a.acctname5 is null or a.acctname5 = ' ')

update input_customer  set acctname6 =  b.acctname1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and rtrim(b.acctname1) != rtrim(a.acctname5) and
(a.acctname6 is null or a.acctname6 = ' ')
GO
