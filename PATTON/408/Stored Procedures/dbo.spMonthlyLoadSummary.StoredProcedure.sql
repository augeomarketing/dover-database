USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyLoadSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyLoadSummary]
GO
CREATE PROCEDURE [dbo].[spMonthlyLoadSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table MonthlySummary

insert MonthlySummary (dateadded)
values (@enddate)

update  MonthlySummary set crpurchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '63'  and histdate = @enddate)
update  MonthlySummary set dbpurchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '67'  and histdate = @enddate)
update  MonthlySummary set plpurchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '61'  and histdate = @enddate)

update   MonthlySummary set crreturned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '33' and histdate =  @enddate)
update   MonthlySummary set dbreturned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '37' and histdate =  @enddate)
update   MonthlySummary set plreturned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '31' and histdate =  @enddate)

update   MonthlySummary set bonuses =(select SUM(CONVERT(int, points)) FROM history WHERE (trancode = 'BO' or trancode = 'BI') and histdate =  @enddate)
GO
