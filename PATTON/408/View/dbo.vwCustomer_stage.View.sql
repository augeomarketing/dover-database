USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
DROP VIEW [dbo].[vwCustomer_stage]
GO
create view [dbo].[vwCustomer_stage]
			 as
			 Select *
			  from dbo.Customer_stage
GO
