USE [408]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 03/29/2013 13:36:27 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_histpoints]'))
DROP VIEW [dbo].[vw_histpoints]
GO

USE [408]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 03/29/2013 13:36:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[vw_histpoints] as select tipnumber, sum(points*ratio) as points from history_stage where secid = 'NEW'group by tipnumber



GO


