USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer]'))
DROP VIEW [dbo].[vwCustomer]
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from dbo.customer
GO
