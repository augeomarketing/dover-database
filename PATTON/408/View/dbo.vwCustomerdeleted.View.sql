USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomerdeleted]'))
DROP VIEW [dbo].[vwCustomerdeleted]
GO
create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from dbo.Customerdeleted
GO
