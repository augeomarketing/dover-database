USE [407]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 03/25/2013 11:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[AcctID] [varchar](25) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL,
	[Last6] [varchar](6) NULL,
	[Bonus] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
