USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses_Stage]
GO

CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
