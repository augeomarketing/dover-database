USE [408] 
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
DROP TABLE [dbo].[TransStandard]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[TIP] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[AcctNum] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [varchar](4) NULL,
	[TranAmt] [varchar](15) NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
