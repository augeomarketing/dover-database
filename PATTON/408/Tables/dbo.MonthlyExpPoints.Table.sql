USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlyExpPoints]') AND type in (N'U'))
DROP TABLE [dbo].[MonthlyExpPoints]
GO

CREATE TABLE [dbo].[MonthlyExpPoints](
	[Tipnumber] [varchar](15) NOT NULL,
	[EligiblePoints] [decimal](18, 0) NULL,
	[UsedPoints] [decimal](18, 0) NULL,
	[PointstoLose] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
