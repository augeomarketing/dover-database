USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlySummary]') AND type in (N'U'))
DROP TABLE [dbo].[MonthlySummary]
GO


CREATE TABLE [dbo].[MonthlySummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[CRPurchases] [decimal](18, 2) NULL,
	[CRReturned] [decimal](18, 2) NULL,
	[DBPurchases] [decimal](18, 2) NULL,
	[DBReturned] [decimal](18, 0) NULL,
	[PLPurchases] [decimal](18, 0) NULL,
	[PLReturned] [decimal](18, 0) NULL,
	[Bonuses] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_CRPurchases]  DEFAULT (0) FOR [CRPurchases]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_CRReturned]  DEFAULT (0) FOR [CRReturned]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_DBPurchases]  DEFAULT (0) FOR [DBPurchases]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_DBReturned]  DEFAULT (0) FOR [DBReturned]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_PLPurchases]  DEFAULT (0) FOR [PLPurchases]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_PLReturned]  DEFAULT (0) FOR [PLReturned]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_Bonuses]  DEFAULT (0) FOR [Bonuses]
GO
