USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses]
GO

CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
