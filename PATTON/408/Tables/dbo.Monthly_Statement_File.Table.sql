USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO

CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [char](40) NULL,
	[Acctname4] [char](40) NULL,
	[Acctname5] [char](40) NULL,
	[Acctname6] [char](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[PointsBegin] [decimal](18, 0) Default 0 ,
	[PointsEnd] [decimal](18, 0) Default 0 ,
	[PointsPurchasedCR] [decimal](18, 0)  Default 0,
	[PointsBonus] [decimal](18, 0)  Default 0,
	[PointsAdded] [decimal](18, 0)  Default 0,
	[PointsIncreased] [decimal](18, 0)  Default 0,
	[PointsRedeemed] [decimal](18, 0)  Default 0,
	[PointsReturnedCR] [decimal](18, 0)  Default 0,
	[PointsSubtracted] [decimal](18, 0)  Default 0,
	[PointsDecreased] [decimal](18, 0)  Default 0,
	[PointsBonusMN] [decimal](18, 0)  Default 0,
	[ExpPnts] [decimal](18, 0)  Default 0,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
