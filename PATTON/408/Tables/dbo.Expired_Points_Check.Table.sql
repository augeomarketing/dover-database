USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]') AND type in (N'U'))
DROP TABLE [dbo].[Expired_Points_Check]
GO

CREATE TABLE [dbo].[Expired_Points_Check](
	[tipnumber] [varchar](15) NULL,
	[h_exp_pts] [decimal](18, 0) NOT NULL,
	[a_exp_pts] [decimal](18, 0) NOT NULL,
	[point_difference] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Expired_Points_Check] ADD  CONSTRAINT [DF_Expired_Points_Check_h_exp_pts]  DEFAULT ((0)) FOR [h_exp_pts]
GO
ALTER TABLE [dbo].[Expired_Points_Check] ADD  CONSTRAINT [DF_Expired_Points_Check_a_exp_pts]  DEFAULT ((0)) FOR [a_exp_pts]
GO
ALTER TABLE [dbo].[Expired_Points_Check] ADD  CONSTRAINT [DF_Expired_Points_Check_point_difference]  DEFAULT ((0)) FOR [point_difference]
GO
