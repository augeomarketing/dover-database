USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HERTRAN]') AND type in (N'U'))
DROP TABLE [dbo].[HERTRAN]
GO

CREATE TABLE [dbo].[HERTRAN](
	[TFNO] [char](15) NULL,
	[TranDate] [char](10) NULL,
	[AcctID] [char](25) NULL,
	[TranCode] [char](2) NULL,
	[Col005] [char](4) NULL,
	[TranAmt] [char](15) NULL,
	[Col007] [char](20) NULL,
	[Col008] [char](4) NULL,
	[Col009] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
