USE [408]
GO
/****** Object:  Table [dbo].[BegBalanceCheck]    Script Date: 03/25/2013 11:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BegBalanceCheck]') AND type in (N'U'))
DROP TABLE [dbo].[BegBalanceCheck]
GO
CREATE TABLE [dbo].[BegBalanceCheck](
	[OldBegBalance] [decimal](18, 0) NULL,
	[NewBegBalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
