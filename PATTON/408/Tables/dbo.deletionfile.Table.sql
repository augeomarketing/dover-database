USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[deletionfile]') AND type in (N'U'))
DROP TABLE [dbo].[deletionfile]
GO

CREATE TABLE [dbo].[deletionfile](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[RunAvailable] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
