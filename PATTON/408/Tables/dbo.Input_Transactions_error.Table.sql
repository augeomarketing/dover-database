USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_error]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transactions_error]
GO

CREATE TABLE [dbo].[Input_Transactions_error](
	[CardNumber] [varchar](19) NULL,
	[Unused] [varchar](19) NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [int] NULL,
	[TipNumber] [varchar](15) NULL,
	[TransDate] [varchar](10) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
