USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewLastName]') AND type in (N'U'))
DROP TABLE [dbo].[NewLastName]
GO

CREATE TABLE [dbo].[NewLastName](
	[AcctID] [varchar](25) NOT NULL,
	[Tipnumber] [varchar](15) NULL,
	[AcctName1] [varchar](50) NULL,
	[ReversedName] [varchar](50) NULL,
	[LastSpace] [int] NULL,
	[Lastname] [varchar](50) NULL,
 CONSTRAINT [PK_NewLastName] PRIMARY KEY CLUSTERED 
(
	[AcctID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
