USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportSummary]') AND type in (N'U'))
DROP TABLE [dbo].[ReportSummary]
GO

CREATE TABLE [dbo].[ReportSummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[Purchases] [decimal](18, 2) NULL,
	[Returned] [decimal](18, 2) NULL,
	[Bonuses] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
