USE [407]
GO
/****** Object:  Table [dbo].[TEMP_EXP_HST]    Script Date: 03/25/2013 11:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TEMP_EXP_HST](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[POINTS] [decimal](18, 0) NULL,
	[histdate] [datetime] NULL,
	[prevexp] [varchar](5) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
