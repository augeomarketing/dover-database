USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reg]') AND type in (N'U'))
DROP TABLE [dbo].[Reg]
GO
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
