USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DateforAudit]') AND type in (N'U'))
DROP TABLE [dbo].[DateforAudit]
GO

CREATE TABLE [dbo].[DateforAudit](
	[Datein] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
