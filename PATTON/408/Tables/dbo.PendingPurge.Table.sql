USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PendingPurge]') AND type in (N'U'))
DROP TABLE [dbo].[PendingPurge]
GO
CREATE TABLE [dbo].[PendingPurge](
	[Tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
