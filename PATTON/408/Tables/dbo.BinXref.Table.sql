USE [408]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[BinXref]    Script Date: 04/11/2013 15:59:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BinXref]') AND type in (N'U'))
DROP TABLE [dbo].[BinXref]
GO


CREATE TABLE [dbo].[BinXref](
	[dim_BinXref_Bin] [int] NOT NULL,
	[dim_BinXref_FITrancode] [char](1) NOT NULL,
	[dim_BinXref_Trancode] [char](2) NOT NULL,
	[dim_BinXref_EffectiveDate] [date] NOT NULL
) ON [PRIMARY]

GO


