USE [408]
GO
/****** Object:  Table [dbo].[CertificateDeleted]    Script Date: 03/25/2013 11:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[rniPurge]    Script Date: 07/19/2012 16:02:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CertificateDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[CertificateDeleted]
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [decimal](18, 0) NULL,
	[RedeemedNow] [decimal](18, 0) NOT NULL,
	[RunAvailable] [decimal](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
