USE [408]
GO
/****** Object:  UserDefinedFunction [dbo].[fnSumPointsByTip]    Script Date: 03/25/2013 11:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fnSumPointsByTip] (@TipNumber nvarchar(15))
	returns bigint

as

BEGIN
declare @Points		bigint

set @Points = (select sum(points*ratio) from history where history.tipnumber = @tipnumber)

return @Points

END
GO
