USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations]
where ConfigurationFilter like '261%'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'261', N'261', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'261', N'Data Source=patton\rn;Initial Catalog=261;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{B888AFBB-6540-403C-87AE-CD821B44B6FE}patton\rn.261;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-BackupFI_Pre_MoveToProd-{9391F4A7-DCFE-409C-B859-31CBF15B9DA2}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261', N'Nov  1 2013 12:00AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'261', N'Nov 30 2013 12:00AM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'261_ArchiveRNITransactionsUpdateDates', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PackagePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_ArchiveRNITransactionsUpdateDates', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\Flag RNIRawImport Txn Rows as Processed.dtsx', N'\Package.Connections[Flag RNIRawImport Txn Rows as Processed.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_ArchiveRNITransactionsUpdateDates', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNIRawImportArchiveInsertFromRNIRawimport.dtsx', N'\Package.Connections[RNIRawImportArchiveInsertFromRNIRawImport.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_ArchiveRNITransactionsUpdateDates', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNITransactionArchive.dtsx', N'\Package.Connections[RNITransactionArchive.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_ArchiveRNITransactionsUpdateDates', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\Update SSIS Start and End Dates.dtsx', N'\Package.Connections[Update SSIS Start and End Dates.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PathToPackage].Properties[Value]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\261_OPS_W01_ImportCIF.dtsx', N'\Package.Connections[261_OPS_W01_ImportCIF.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\261_OPS_W02_ImportEmail.dtsx', N'\Package.Connections[261_OPS_W02_ImportEmail.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\GenerateWelcomeKits.dtsx', N'\Package.Connections[GenerateWelcomeKits.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\InitializeStage_Affiliat.dtsx', N'\Package.Connections[InitializeStage_Affiliat.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\InitializeStage_Customer.dtsx', N'\Package.Connections[InitializeStage_Customer.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\PostToWeb.dtsx', N'\Package.Connections[PostToWeb.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomer Archive.dtsx', N'\Package.Connections[RNICustomer Archive.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomerAssignTipNumbers.dtsx', N'\Package.Connections[RNICustomerAssignTipNumbers.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomerInsertAffiliat.dtsx', N'\Package.Connections[RNICustomerInsertAffiliat.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomerSetPrimary.dtsx', N'\Package.Connections[RNICustomerSetPrimary.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomerUpsertFICustomerFromRNICustomer.dtsx', N'\Package.Connections[RNICustomerUpsertFICustomerFromRNICustomer.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomerUpsertFromSource.dtsx', N'\Package.Connections[RNICustomerUpsertFromSource.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNICustomerValidateRequiredSourceFields.dtsx', N'\Package.Connections[RNICustomerValidateRequiredSourceFields.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNIRawImport Archive.dtsx', N'\Package.Connections[RNIRawImport Archive.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\Upsert Customer.dtsx', N'\Package.Connections[Upsert Customer.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\UpsertAffiliat.dtsx', N'\Package.Connections[Upsert Affiliat.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_Generate_Audit_File', N'\\patton\ops\261\Output\Audit\MSFAudit.xls', N'\Package.Connections[MSFAudit.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_Generate_Audit_File', N'\\patton\ops\261\Output\Audit\MSFAudit_Template.xls', N'\Package.Connections[MSFAudit_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_Generate_Audit_File', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\patton\ops\261\Output\Audit\MSFAudit.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[MSFAuditRename.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_Generate_Audit_File', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\patton\ops\261\Output\Audit\MSFAudit.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[MSFAudit XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PackagePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\261_OPS_D01_ImportPOSTMON.dtsx', N'\Package.Connections[261_OPS_D01_ImportPOSTMON.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\CalcCustomerStageBalances.dtsx', N'\Package.Connections[CalcCustomerStageBalances.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\Flag RNIRawImport Txn Rows as Processed.dtsx', N'\Package.Connections[Flag RNIRawImport Txn Rows as Processed.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\Generate 261Schools Audit.dtsx', N'\Package.Connections[Generate 261Schools Audit.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNIRawImportArchiveInsertFromRNIRawImport.dtsx', N'\Package.Connections[RNIRawImportArchiveInsertFromRNIRawImport.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNITransactionArchive.dtsx', N'\Package.Connections[RNITransactionArchive.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNITransactionAssignTip.dtsx', N'\Package.Connections[RNITransactionAssignTip.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNITransactionInsert.dtsx', N'\Package.Connections[RNITransactionInsert.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNITransactionInsertHistoryStage_Summarize.dtsx', N'\Package.Connections[RNITransactionInsertHistoryStage_Summarize.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\RNITransactionValidateRequiredSourceFields.dtsx', N'\Package.Connections[RNITransactionValidateRequiredSourceFields.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\Update SSIS Start and End Dates.dtsx', N'\Package.Connections[Update SSIS Start and End Dates.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_W01_ImportCIF', N'\\patton\ops\261\input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W01_ImportCIF', N'<none>', N'\Package.Connections[CIF].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'261_OPS_W01_ImportCIF', N'CIF*.*', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'', N'\Package.Connections[Email].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'\\patton\ops\261\input', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'261', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'261_OPS_W02_ImportEmail', N'261_Email_*.*', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'Data Source=patton\rn;Initial Catalog=261;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-261_OPS_W02_ImportEmail-{8092CC14-6891-4782-9E27-2A6181750677}patton\rn.261;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-261_OPS_W02_ImportEmail-{84E3D636-5F51-4B45-BEF8-38B9EDA30FF4}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_Post_Audit_Approval', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PackagePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_ProcessJobID', N'0', N'\Package.Variables[User::ProcessJobID].Properties[Value]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

