USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'261_QuarterlyStatements', N'\\patton\ops\261\Output\Statements\STATEMENT_STD_V1_Template.XLS', N'\Package.Connections[QuarterlyStmt_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'\\patton\ops\261\Output\Statements\STATEMENT_STD_V1_Tmp.XLS', N'\Package.Connections[QuarterlyStmt.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'Data Source=rn1;Initial Catalog=Schools;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-QuarterlyStatementFile-{1E47CC2A-0F40-4B67-9C8F-EB930F850DF9}WEB_DeanBank;', N'\Package.Connections[WEB_Schools].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-QuarterlyStatementFile-{4B0C7A6A-9692-441B-BE25-76C1E9629D7D}doolittle\rn.RewardsNow;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'Data Source=patton\rn;Initial Catalog=261;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{B888AFBB-6540-403C-87AE-CD821B44B6FE}doolittle\rn.261;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'True', N'\Package.Connections[Excel QuarterlyStmt.xls].Properties[FirstRowHasColumnName]', N'Boolean' UNION ALL
SELECT N'261_QuarterlyStatements', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\patton\ops\261\Output\Statements\STATEMENT_STD_V1_Tmp.XLS;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Excel QuarterlyStmt.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'261', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'261_QuarterlyStatements', N'\\patton\ops\261\output\Statements\', N'\Package.Variables[User::ProdStmtPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

