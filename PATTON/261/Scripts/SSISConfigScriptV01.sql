USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete from RewardsNow.dbo.[SSIS Configurations]
where ConfigurationFilter like '261%'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'261', N'Data Source=patton\rn;Initial Catalog=261;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{B888AFBB-6540-403C-87AE-CD821B44B6FE}patton\rn.261;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-BackupFI_Pre_MoveToProd-{9391F4A7-DCFE-409C-B859-31CBF15B9DA2}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261', N'10/31/2013 10:05:40 AM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'261', N'10/1/2013 10:05:19 AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'261', N'261', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'261_DemographicParent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PathToPackage].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_Transaction_Parent', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PackagePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W01_ImportCIF', N'<none>', N'\Package.Connections[CIF].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'261_OPS_W01_ImportCIF', N'CIF*.*', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W01_ImportCIF', N'\\patton\ops\261\input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'', N'\Package.Connections[Email].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'Data Source=patton\rn;Initial Catalog=261;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-261_OPS_W02_ImportEmail-{8092CC14-6891-4782-9E27-2A6181750677}patton\rn.261;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-261_OPS_W02_ImportEmail-{84E3D636-5F51-4B45-BEF8-38B9EDA30FF4}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'261_Email_*.*', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'\\patton\ops\261\input', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'10/31/2013 9:21:40 AM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'10/1/2013 9:21:20 AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'261_OPS_W02_ImportEmail', N'261', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'261_Post_Audit_Approval', N'\\patton\ops\SSIS_Packages\261SchoolsFinancialCU\261SchoolsFinancialCU\', N'\Package.Variables[User::PackagePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

