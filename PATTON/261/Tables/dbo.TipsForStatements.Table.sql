USE [261]
GO
/****** Object:  Table [dbo].[TipsForStatements]    Script Date: 02/25/2014 08:34:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipsForStatements]') AND type in (N'U'))
DROP TABLE [dbo].[TipsForStatements]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipsForStatements]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipsForStatements](
	[TipNumber] [varchar](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
