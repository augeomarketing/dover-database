USE [261]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 11/13/2013 14:39:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyStatementFileLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyStatementFileLoad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- SEB 11/13/2013 Changed PointsBegin to use calculation instead of lookup
-- SEB 11/2013 Added code H%

CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]  @StartDate datetime, @EndDate datetime

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(max)


set @MonthBegin = cast(month(@StartDate) as varchar(2))

/* Load the statement file from the customer table  */
delete from dbo.Monthly_Statement_File

insert into dbo.Monthly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
from dbo.customer_Stage
--where status = ''A''

/* Load the statmement file with CREDIT purchases          */
update dbo.Monthly_Statement_File 
	set	pointspurchasedCR = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''63''), 0),
							
		pointsreturnedCR =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''33''), 0),
							
		pointspurchasedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''67''), 0),
		pointsreturnedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''37''), 0),

		pointsadded =		isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''IE''), 0) +
							isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''DR''), 0) +
							isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''TP''), 0),

		pointssubtracted =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''DE''), 0) +
							isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''EP''), 0),

		PointsExpire =		isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''XP''), 0)
		

/* Load the statmement file with bonuses            */
--
update Monthly_Statement_File
	set pointsbonus=(select sum(points*ratio) 
				    from dbo.History_Stage 
				    where tipnumber=Monthly_Statement_File.tipnumber and 
						  histdate>=@startdate and histdate<=@enddate and (trancode like ''B%'' or trancode like ''F%'' or trancode like ''G%'' or trancode like ''H%'') )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''B%'' or trancode like ''F%'' or trancode like ''G%'' or trancode like ''H%''))


/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')

/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
update Monthly_Statement_File 
set pointsbegin = (select isnull(SUM(points * ratio),0)
					from history 
					where Tipnumber = Monthly_Statement_File.TIPNUMBER
					and histdate < @Startdate)

/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased




' 
END
GO
