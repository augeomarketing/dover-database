USE [OnlineHistoryWork]

CREATE TABLE Portal_Combines_Tablelist 
(
		sid_Portal_Combines_Tablelist_id		INT				IDENTITY(1,1) PRIMARY KEY
	,	dim_Portal_Combines_Tablelist_dbname	VARCHAR(255)
	,	dim_Portal_Combines_Tablelist_tablename	VARCHAR(255)
	,	dim_Portal_Combines_Tablelist_fieldname	VARCHAR(255)
)

INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	NULL, 'HISTORY', 'TIPNUMBER'	
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	NULL, 'HISTORY_Stage', 'TIPNUMBER'	
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	NULL, 'OneTimeBonuses', 'TIPNUMBER'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	NULL, 'Account_Reference', 'TIPNUMBER'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	NULL, 'Tip_DDA_Reference', 'TIPNUMBER'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	NULL, 'SelfEnroll', 'dim_selfenroll_tipnumber'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	'fullfillment', 'Main', 'TIPNUMBER'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	'OnlineHistoryWork', 'OnlHistory', 'TIPNUMBER'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	'RewardsNow', 'RNICustomer', 'dim_rnicustomer_rniid'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	'RewardsNow', 'RNITransaction', 'dim_rnitransaction_rniid'
INSERT INTO Portal_Combines_Tablelist (dim_Portal_Combines_Tablelist_dbname, dim_Portal_Combines_Tablelist_tablename, dim_Portal_Combines_Tablelist_fieldname)
SELECT	'RewardsNow', 'RNITransactionArchive', 'dim_rnitransaction_rniid'


--DROP TABLE Portal_Combines_Tablelist