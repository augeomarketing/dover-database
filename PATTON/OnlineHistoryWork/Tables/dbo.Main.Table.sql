/****** Object:  Table [dbo].[Main]    Script Date: 02/26/2009 12:19:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Main](
	[TipNumber] [char](15) NULL,
	[TipFirst] [char](3) NULL,
	[Source] [varchar](10) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[TransID] [char](40) NOT NULL,
	[OrderID] [nchar](6) NULL,
	[HistDate] [smalldatetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranDesc] [varchar](100) NULL,
	[Points] [int] NULL,
	[Vendor] [char](3) NULL,
	[ItemNumber] [varchar](20) NULL,
	[Catalogdesc] [varchar](300) NULL,
	[Segment] [char](2) NULL,
	[CatalogQty] [int] NULL,
	[SName] [varchar](50) NULL,
	[SAddress1] [varchar](50) NULL,
	[SAddress2] [varchar](50) NULL,
	[SAddress3] [varchar](50) NULL,
	[SAddress4] [varchar](50) NULL,
	[SCity] [varchar](50) NULL,
	[SState] [varchar](5) NULL,
	[SZip] [varchar](15) NULL,
	[SCountry] [varchar](50) NULL,
	[Phone1] [varchar](12) NULL,
	[Phone2] [varchar](12) NULL,
	[Email] [varchar](50) NULL,
	[RedStatus] [tinyint] NULL,
	[RedReqFulDate] [smalldatetime] NULL,
	[InvoiceDate] [smalldatetime] NULL,
	[Notes] [varchar](2000) NULL,
 CONSTRAINT [PK_Main] PRIMARY KEY CLUSTERED 
(
	[TransID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [_dta_index_Main_169_752721734__K12_K9_K13] ON [dbo].[Main] 
(
	[Vendor] ASC,
	[TranCode] ASC,
	[ItemNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_Main_169_752721734__K6_K13_K2_K1_K4_K30_K29_K5_K11_K8_K18_K19_K22_K23_K24_K25] ON [dbo].[Main] 
(
	[TransID] ASC,
	[ItemNumber] ASC,
	[TipFirst] ASC,
	[TipNumber] ASC,
	[Name1] ASC,
	[RedReqFulDate] ASC,
	[RedStatus] ASC,
	[Name2] ASC,
	[Points] ASC,
	[HistDate] ASC,
	[SAddress1] ASC,
	[SAddress2] ASC,
	[SCity] ASC,
	[SState] ASC,
	[SZip] ASC,
	[SCountry] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_Main_169_752721734__K6_K2_K1_K4_K30_K29_K5_K11] ON [dbo].[Main] 
(
	[TransID] ASC,
	[TipFirst] ASC,
	[TipNumber] ASC,
	[Name1] ASC,
	[RedReqFulDate] ASC,
	[RedStatus] ASC,
	[Name2] ASC,
	[Points] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_Main_169_752721734__K6_K2_K1_K8_K30_K31_K13_K14] ON [dbo].[Main] 
(
	[TransID] ASC,
	[TipFirst] ASC,
	[TipNumber] ASC,
	[HistDate] ASC,
	[RedReqFulDate] ASC,
	[InvoiceDate] ASC,
	[ItemNumber] ASC,
	[Catalogdesc] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_fullfillment_redstatus] ON [dbo].[Main] 
(
	[RedStatus] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_main_trancode] ON [dbo].[Main] 
(
	[TranCode] ASC,
	[TranDesc] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Main_HistDate_TipFirst_TipNumber] ON [dbo].[Main] 
(
	[HistDate] ASC,
	[TipFirst] ASC,
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Main_TipFirst_TipNumber_HistDate] ON [dbo].[Main] 
(
	[TipFirst] ASC,
	[TipNumber] ASC,
	[HistDate] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Main_TransID_Tip_Name_Points_HistDate] ON [dbo].[Main] 
(
	[TransID] ASC,
	[TipNumber] ASC,
	[Name1] ASC,
	[Name2] ASC,
	[Points] ASC,
	[HistDate] DESC
) ON [PRIMARY]
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1755 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipFirst'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipFirst'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=2 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipFirst'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=495 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipFirst'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TipFirst'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=3 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=495 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Source'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=4 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2940 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=5 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2940 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Name2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TransID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TransID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=6 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TransID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1050 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TransID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TransID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=7 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=840 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'HistDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'HistDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=8 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'HistDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2265 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'HistDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'HistDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=9 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1005 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranCode'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranDesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranDesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=10 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranDesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1800 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranDesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'TranDesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=11 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=735 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Points'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Vendor'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Vendor'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=12 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Vendor'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=795 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Vendor'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Vendor'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=13 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=3585 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'ItemNumber'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Catalogdesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Catalogdesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=14 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Catalogdesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=4920 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Catalogdesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Catalogdesc'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Segment'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Segment'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=15 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Segment'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=960 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Segment'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Segment'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'CatalogQty'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'CatalogQty'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=16 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'CatalogQty'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1155 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'CatalogQty'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'CatalogQty'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=17 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1185 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SName'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=18 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=4545 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=19 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=3180 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=20 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1140 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress3'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=21 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1140 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SAddress4'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=22 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2685 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCity'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=23 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=795 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SState'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=24 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1005 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SZip'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCountry'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCountry'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=32 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCountry'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCountry'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'SCountry'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=25 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1350 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone1'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=26 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1350 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Phone2'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=27 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=3120 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DisplayControl', @value=N'109' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Format', @value=N'' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_IMEMode', @value=N'0' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Email'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedStatus'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedStatus'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=28 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedStatus'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1095 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedStatus'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedStatus'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedReqFulDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedReqFulDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=29 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedReqFulDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1545 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedReqFulDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'RedReqFulDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'InvoiceDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'InvoiceDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=30 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'InvoiceDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=1170 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'InvoiceDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'InvoiceDate'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_AggregateType', @value=-1 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnHidden', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnOrder', @value=31 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_ColumnWidth', @value=2130 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TextAlign', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main', @level2type=N'COLUMN',@level2name=N'Notes'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DatasheetBackColor', @value=16777215 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DatasheetBackColor12', @value=16777215 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DefaultView', @value=0x02 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Filter', @value=NULL , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_FilterOnLoad', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_HideNewField', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_OrderBy', @value=N'[Main].[HistDate] DESC' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_OrderByOn', @value=True , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_OrderByOnLoad', @value=True , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Orientation', @value=0x00 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TableMaxRecords', @value=10000000 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_TotalsRow', @value=False , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Main'
GO
