USE [OnlineHistoryWork]
GO

/****** Object:  Table [dbo].[Portal_Combines_Tablelist]    Script Date: 07/15/2015 07:53:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal_Combines_Tablelist]') AND type in (N'U'))
DROP TABLE [dbo].[Portal_Combines_Tablelist]
GO

USE [OnlineHistoryWork]
GO

/****** Object:  Table [dbo].[Portal_Combines_Tablelist]    Script Date: 07/15/2015 07:53:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Portal_Combines_Tablelist](
	[sid_Portal_Combines_Tablelist_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_Portal_Combines_Tablelist_dbname] [varchar](255) NULL,
	[dim_Portal_Combines_Tablelist_tablename] [varchar](255) NULL,
	[dim_Portal_Combines_Tablelist_fieldname] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_Portal_Combines_Tablelist_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


