/****** Object:  Table [dbo].[OPTOUTTracking]    Script Date: 02/26/2009 12:19:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OPTOUTTracking](
	[TipPrefix] [nvarchar](3) NULL,
	[TIPNUMBER] [nvarchar](15) NOT NULL,
	[ACCTID] [nvarchar](6) NOT NULL,
	[FIRSTNAME] [nvarchar](40) NOT NULL,
	[LASTNAME] [nvarchar](40) NOT NULL,
	[OPTOUTDATE] [datetime] NOT NULL,
	[OPTOUTSOURCE] [nvarchar](40) NOT NULL,
	[OPTOUTPOSTED] [datetime] NULL,
 CONSTRAINT [PK_OPTOUTTracking] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
