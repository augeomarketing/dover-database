/****** Object:  Table [dbo].[ErrorRedemption]    Script Date: 02/26/2009 12:18:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ErrorRedemption](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](20) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL,
	[CopyFlag] [datetime] NULL,
	[Source] [char](10) NULL,
	[DBNum] [varchar](3) NULL,
	[saddress1] [char](50) NULL,
	[saddress2] [char](50) NULL,
	[scity] [char](50) NULL,
	[sstate] [char](5) NULL,
	[szipcode] [char](10) NULL,
	[scountry] [varchar](50) NULL,
	[hphone] [char](12) NULL,
	[wphone] [char](12) NULL,
	[notes] [varchar](250) NULL,
	[sname] [char](50) NULL,
	[ordernum] [bigint] NULL,
	[linenum] [bigint] NULL,
	[FullfillmentFlag] [datetime] NULL,
	[CreditRequestFlag] [datetime] NULL,
	[DateDiscovered] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
