/****** Object:  Table [dbo].[RDT_Compass_Tip_Trans]    Script Date: 02/26/2009 12:19:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RDT_Compass_Tip_Trans](
	[Tipnumber] [char](15) NOT NULL,
	[transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
