/****** Object:  Table [dbo].[SarahFixOverRedemptions]    Script Date: 02/26/2009 12:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SarahFixOverRedemptions](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[runavailable] [int] NULL,
	[runbalance] [int] NULL,
	[runredeemed] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
