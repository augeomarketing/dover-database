/****** Object:  Table [dbo].[Portal_Adjustments]    Script Date: 02/26/2009 12:19:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Adjustments](
	[TipFirst] [char](3) NULL,
	[tipnumber] [char](15) NOT NULL,
	[lastsix] [char](20) NULL,
	[Histdate] [smalldatetime] NOT NULL CONSTRAINT [DF__Portal_Ad__Histd__09A971A2]  DEFAULT (getdate()),
	[trancode] [char](2) NOT NULL,
	[TranDesc] [varchar](50) NULL,
	[points] [int] NOT NULL,
	[Ratio] [float] NOT NULL,
	[usid] [int] NOT NULL,
	[CopyFlag] [datetime] NULL,
	[transID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_portal_adjustments_TipNumber] ON [dbo].[Portal_Adjustments] 
(
	[tipnumber] ASC
) ON [PRIMARY]
GO
