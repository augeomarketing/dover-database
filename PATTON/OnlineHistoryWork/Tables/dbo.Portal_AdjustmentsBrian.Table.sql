/****** Object:  Table [dbo].[Portal_AdjustmentsBrian]    Script Date: 02/26/2009 12:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_AdjustmentsBrian](
	[TipFirst] [char](3) NULL,
	[tipnumber] [char](15) NOT NULL,
	[lastsix] [char](20) NULL,
	[Histdate] [smalldatetime] NOT NULL,
	[trancode] [char](2) NOT NULL,
	[TranDesc] [varchar](50) NULL,
	[points] [int] NOT NULL,
	[Ratio] [float] NOT NULL,
	[usid] [int] NOT NULL,
	[CopyFlag] [datetime] NULL,
	[transID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
