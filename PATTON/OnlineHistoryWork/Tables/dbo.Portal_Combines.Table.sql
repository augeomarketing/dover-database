/****** Object:  Table [dbo].[Portal_Combines]    Script Date: 02/26/2009 12:19:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Portal_Combines](
	[TipFirst] [char](3) NULL,
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NOT NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NOT NULL,
	[HistDate] [datetime] NULL CONSTRAINT [DF_Portal_Combines_HistDate]  DEFAULT (getdate()),
	[usid] [int] NOT NULL,
	[CopyFlagTransfered] [datetime] NULL,
	[CopyFlagCompleted] [datetime] NULL,
	[transid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Portal_Combines_transid]  DEFAULT (newid())
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
