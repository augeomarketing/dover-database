/****** Object:  Table [dbo].[DBProcessInfoRedemptions]    Script Date: 02/26/2009 12:18:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBProcessInfoRedemptions](
	[DBName] [varchar](50) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
