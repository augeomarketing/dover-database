/****** Object:  Table [dbo].[testredemptions]    Script Date: 02/26/2009 12:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[testredemptions](
	[Message] [varchar](8) NOT NULL,
	[MsgDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
