/****** Object:  Table [dbo].[portal_combines_test]    Script Date: 02/26/2009 12:19:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[portal_combines_test](
	[TipFirst] [char](3) NULL,
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NOT NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NOT NULL,
	[HistDate] [datetime] NULL,
	[usid] [int] NOT NULL,
	[CopyFlagTransfered] [datetime] NULL,
	[CopyFlagCompleted] [datetime] NULL,
	[transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
