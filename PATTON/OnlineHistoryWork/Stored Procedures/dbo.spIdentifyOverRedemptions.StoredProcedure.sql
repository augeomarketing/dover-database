/****** Object:  StoredProcedure [dbo].[spIdentifyOverRedemptions]    Script Date: 02/26/2009 12:18:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create Procedure [dbo].[spIdentifyOverRedemptions]
AS

Declare @SQLInsert nvarchar(1000), @DBName varchar(50)

DECLARE cDBName CURSOR FOR
	SELECT  distinct rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName

WHILE (@@FETCH_STATUS=0)
	BEGIN
		if not exists (select name from master.dbo.sysdatabases where name not in ('tempdb') and name like @DBName) 
		Begin
			goto nextrecord
		end

		set @SQLInsert='insert into onlinehistorywork.dbo.FixOverRedemptions (tipnumber, acctname1, runavailable, runbalance, runredeemed)
			select tipnumber, acctname1, runavailable, runbalance, runredeemed 
			from ' + QuoteName(@DBName) + N'.dbo.customer
			where runavailable<''0'' and runredeemed>''0'' '
		exec sp_executesql @SQLInsert 

NEXTRECORD:	
		FETCH NEXT FROM cDBName INTO @DBName
	END

Fetch_Error:
CLOSE cDBName
DEALLOCATE cDBName
GO
