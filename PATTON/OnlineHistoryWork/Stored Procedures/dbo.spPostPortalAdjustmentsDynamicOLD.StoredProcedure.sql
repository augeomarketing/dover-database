/****** Object:  StoredProcedure [dbo].[spPostPortalAdjustmentsDynamicOLD]    Script Date: 02/26/2009 12:18:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- THIS IS THE VERSION BEFORE CHANGES WERE MADE FOR 'RB','RZ' AND 'DZ' TRANSACTIONS BJQ 10/29/2008
-- RDT 07/27/2007 Changed LastSix char(6) to LastSix char(20). Some clients have more than 6 in the last 6. (member numbers, etc.) 

/*  RDT 07/27/2007 */ -- RDT 07/27/2007 Added checks for DBAvailable flag in dbo.DBProcessInfo 
/* RDT 07/30/2007 */ -- Removed checks for DBAvailable flag in dbo.DBProcessInfo 

CREATE PROCEDURE [dbo].[spPostPortalAdjustmentsDynamicOLD] AS   

	Declare 
	@TipFirst char(3),	
	@TipNum char(15),
-- RDT 07/27/2007 @LastSix char(6),
	@LastSix char(20),
	@HistDate DateTime,
	@TranCode char(2),
	@Transdesc char(50),
	@TotalPoints int,
	@Points int,
	@Ratio float,
	@USID char(4),
	@TransID uniqueidentifier,
	@SQLInsert nvarchar(1000), 
	@SQLUpdate nvarchar(1000),
	@DBNum varchar(3),	
        	@DBName varchar(50),
        	@CopyDateTime datetime, 
	@hldDB char(3)

--convert to Midnight today
	Set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

	Update Portal_Adjustments
	set copyflag='01/01/1900'
	where copyflag is null and not exists(select * from RewardsNOW.dbo.DBProcessInfo where DBNumber=Portal_Adjustments.Tipfirst) 

	set @hldDB=' '		
	
	--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
	DECLARE cRecs CURSOR FOR
		SELECT  TipFirst, TipNumber, Lastsix, HistDate, TranCode, TranDesc, Points, Ratio, USID, TransID  
			from OnlineHistoryWork.dbo.Portal_Adjustments 
			Where CopyFlag is null
			order by tipnumber
		OPEN cRecs 
		FETCH NEXT FROM cRecs INTO @DBNum, @TipNum, @LastSix, @HistDate, @TranCode, @Transdesc, @Points, @Ratio, @USID, @TransID
		WHILE (@@FETCH_STATUS=0)
		BEGIN
			--Update the Customer table
		set @TotalPoints = (@Points * @Ratio)
		if @DBNum<>@hldDB
		Begin		
			if not exists(SELECT * from RewardsNOW.dbo.DBProcessInfo
			where DBNumber=@DBNum)
				Begin
					Goto Next_Record
				End
/*  RDT 07/27/2007 */ --RDT 07/30/2007   	If (Select DBAvailable from RewardsNOW.dbo.DBProcessInfo 	
/*  RDT 07/27/2007 */ --RDT 07/30/2007  		where DBNumber=@DBNum) <> 'Y' 
/*  RDT 07/27/2007 */ --RDT 07/30/2007   		Begin 
/*  RDT 07/27/2007 */ --RDT 07/30/2007   			Goto Next_Record
/*  RDT 07/27/2007 */ --RDT 07/30/2007   		End
			Else
				Begin			
					set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
					where DBNumber=@DBNum)
					set @hldDB=@DBNum
				End		
		End		

		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set RunAvailable = RunAvailable + @TotalPoints, RunBalance=RunBalance + @TotalPoints where TipNumber=@TipNum1'
		exec sp_executesql @SQLUpdate, N'@Tipnum1 nchar(15), @TotalPoints int', @Tipnum1=@Tipnum, @TotalPoints=@TotalPoints


		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
				values 	( @TipNum, @LastSix, @HistDate, @TranCode, 1, @Points, @Transdesc, '' '', @Ratio, 0 )'
-- RDT 07/27/2007 exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @LastSix char(6), @HistDate datetime, @TranCode char(2), @Points int, @Transdesc char(50), @Ratio float ', 
-- RDT 07/27/2007 	@Tipnum= @Tipnum, @LastSix=@LastSix, @HistDate=@HistDate, @TranCode=@TranCode, @Points=@Points, @Transdesc=@Transdesc, @Ratio=@Ratio
		exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @LastSix char(20), @HistDate datetime, @TranCode char(2), @Points int, @Transdesc char(50), @Ratio float ', 
					@Tipnum= @Tipnum, @LastSix=@LastSix, @HistDate=@HistDate, @TranCode=@TranCode, @Points=@Points, @Transdesc=@Transdesc, @Ratio=@Ratio
	
-----Update the copyflag for the record
		Update OnlineHistoryWork.dbo.Portal_Adjustments  set CopyFlag= @CopyDateTime where TransID=@TransID and TipFirst=@DBNum 

Next_Record:
		FETCH NEXT FROM cRecs INTO @DBNum, @TipNum, @LastSix, @HistDate, @TranCode, @Transdesc, @Points, @Ratio, @USID, @TransID
END

CLOSE cRecs 
DEALLOCATE	cRecs
GO
