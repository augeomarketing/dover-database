/****** Object:  StoredProcedure [dbo].[pRepostRedemptions]    Script Date: 02/26/2009 12:18:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pRepostRedemptions] @StartDate varchar(10), @EndDate varchar(10), @DBNum char(3) 
AS 


	Declare 
	@TipNum varchar(15),
	@Points int,
	@CatalogQty int,
	@TotalPoints int,
	@HistDate DateTime,
	@TranCode char(2),
	@CatalogDesc varchar(50),
	@TransID uniqueidentifier,
	@SQLInsert nvarchar(1000), 
	@SQLUpdate nvarchar(1000),
        @DBName varchar(50),
        @Transdescr char(10)
	
	set @Transdescr='DTS REDEEM'
		
	set @DBName=(select dbname from RewardsNOW.dbo.DBProcessInfo where DBnumber=@DBNum)

	--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
	DECLARE cRecs CURSOR FOR
		SELECT  TipNumber,substring(TipNumber,1,3)  as DBNum, Points,CatalogQty, HistDate, TransID, TranCode, substring(CatalogDesc,1,50) as CD  
			from OnlineHistoryWork.dbo.OnlHistory 
			Where histdate>=@Startdate and histdate<=@Enddate and left(tipnumber,3)=@DBNum
			order by tipnumber
		OPEN cRecs 
		FETCH NEXT FROM cRecs INTO @TipNum, @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogDesc
		WHILE (@@FETCH_STATUS=0)
		BEGIN
			--Update the Customer table
		set @TotalPoints = (@Points * @CatalogQty)

		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set RunRedeemed=RunRedeemed + @TotalPoints, RunAvailable=RunAvailable-@TotalPoints where TipNumber=@TipNum1'
		exec sp_executesql @SQLUpdate, N'@Tipnum1 nchar(15), @TotalPoints int', @Tipnum1=@Tipnum, @TotalPoints=@TotalPoints


		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
				values 	( @TipNum, NULL, @HistDate, @TranCode, 1, @TotalPoints, @CatalogDesc, @Transdescr, -1, 0 )'
		exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @HistDate datetime, @TranCode char(2), @TotalPoints int, @CatalogDesc varchar(50), @Transdescr char(10)', 
					@Tipnum= @Tipnum, @HistDate=@HistDate, @TranCode=@TranCode, @TotalPoints=@TotalPoints, @CatalogDesc=@CatalogDesc, @Transdescr=@Transdescr
	
		FETCH NEXT FROM cRecs INTO @TipNum,  @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogDesc
		END
		CLOSE cRecs 
		DEALLOCATE	cRecs
GO
