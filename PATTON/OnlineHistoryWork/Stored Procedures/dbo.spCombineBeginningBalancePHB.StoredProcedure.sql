/****** Object:  StoredProcedure [dbo].[spCombineBeginningBalancePHB]    Script Date: 02/26/2009 12:18:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO update beginning_balance_table with COMBINES                          */
/*                                                                            */
/* BY:  R.Tremblay                                          */
/* DATE: 9/2006                                                               */
/* REVISION: 1                                                                */
/* */
/* 	CAUTION 	CAUTION 	CAUTION */
/* Run this ONCE because the Secondary TIP values are ADDED to the primary tip values. */
/* 	CAUTION 	CAUTION 	CAUTION */
/*  MODIFICATIONS                                                             */
/*  PHB001	Paul H. Butler   25 June 2008                                  */
/*             If FI being processed belongs to TNB, use the TNB database     */
/*             version of beginning_balance_table, not the one in the FI's    */
/*             database - because it doesn't exist                            */
/* */


CREATE   PROCEDURE [dbo].[spCombineBeginningBalancePHB] @TipPrimary char(15), @TipSecondary char(15)
AS 

Declare @MonthBucket char(10), @Month int, @SQLUpdate nvarchar(1000), @DBName char(50), @DBNameNEXL varchar(50)

/* Below lines modified ->  PHB001  */
SELECT  @DBName = ltrim(rtrim(DBNamePatton)), @DBNameNEXL = ltrim(rtrim(DBNameNEXL)) -- PHB001
from RewardsNOW.dbo.DBProcessInfo   -- PHB001
where DBNumber=left(@TipPrimary,3) -- PHB001

if @DBNameNEXL = 'NewTNB'  -- PHB001
	set @DBName = 'TNB'   -- PHB001
/* Above lines modified ->  PHB001  */

Set @Month = 1

While @Month < 13 
Begin 
	set @MonthBucket='MonthBeg' + cast(@Month as char)

	set @SQLUpdate = N'update '+ Quotename(@DBName) + N'.dbo.Beginning_Balance_Table set '+ Quotename(@MonthBucket) + 
	   N'= ( select sum(' + Quotename(@MonthBucket) +' ) from  '+ Quotename(@DBName) + N'.dbo.beginning_balance_table 
		where tipnumber = ''' +@TipPrimary + ''' or tipnumber = '''  + @TipSecondary + ' '') where tipnumber = ''' + @TipPrimary + ''''
--print @SQLUpdate 
exec sp_executesql @SQLUpdate

	
	set @Month = @Month + 1 

End
GO
