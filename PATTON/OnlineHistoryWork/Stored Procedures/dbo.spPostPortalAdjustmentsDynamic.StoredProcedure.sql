USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[spPostPortalAdjustmentsDynamic]    Script Date: 02/04/2013 12:56:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPostPortalAdjustmentsDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPostPortalAdjustmentsDynamic]
GO

USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[spPostPortalAdjustmentsDynamic]    Script Date: 02/04/2013 12:56:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[spPostPortalAdjustmentsDynamic] AS   

SET NOCOUNT ON

DECLARE @process VARCHAR(50) = 'spPostPortalAdjustmentsDynamic'
	, @msg VARCHAR(MAX)
	
DELETE FROM RewardsNow.dbo.BatchDebugLog WHERE dim_batchdebuglog_process = @process	
	
SET @msg = 'BEGIN PROCESS'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

DECLARE
	@TipNumber varchar(15)
	, @TranCode varchar(2)
	, @Transdesc varchar(255)
	, @CopyDateTime datetime
	, @sid_redadjust_id INT = 1
	, @max_sid_redadjust_id INT
	, @sid_pa_id INT = 1
	, @max_sid_pa_id INT
	, @sid_nonredadjust_id INT = 1
	, @max_sid_nonredadjust_id INT
	, @sid_db_id INT = 1
	, @max_sid_db_id INT
	, @dbnamepatton varchar(50)
	, @SQLInsert nvarchar(max)
	, @SQLUpdate NVARCHAR(MAX)
	, @sqlFulfillment NVARCHAR(MAX)
		

IF OBJECT_ID(N'tempdb..#pa') IS NOT NULL
	DROP TABLE #pa
	
CREATE TABLE #pa
(
	sid_pa_id INT IDENTITY(1,1) PRIMARY KEY
	, tipfirst varchar(3)
	, tipnumber varchar(15)
	, lastsix varchar(20)
	, histdate smalldatetime
	, trancode varchar(2)
	, trandesc varchar(255)
	, points int
	, ratio int
	, usid int
	, copyflag datetime
	, transid uniqueidentifier
	, dbnamepatton varchar(50)
	, status INT
	, cashvalue money
)

IF OBJECT_ID(N'tempdb..#redtc') IS NOT NULL
	DROP TABLE #redtc
	
CREATE TABLE #redtc
(
	trancode varchar(2) PRIMARY KEY
)


--convert to Midnight today
Set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

--Flagging New Test Transactions so that they will not be pulled
SET @msg = '---> Flagging New Test Transactions in RN1 Portal Adjustments'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

UPDATE RN1.OnlineHistoryWork.dbo.Portal_Adjustments
SET CopyFlag = '1/1/1900'
WHERE CopyFlag IS NULL
	AND Tipnumber LIKE '%999999%'  --I REALLY hate this way of checking for "test" accounts -CWH


SET @msg = '---> Pulling Data From RN1 Portal Adjustments'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

--Pull Portal Adjustments from Web that haven't been pulled yet
INSERT INTO OnlineHistoryWork.dbo.Portal_Adjustments
(
	TipFirst, tipnumber, lastsix, Histdate
	, trancode, TranDesc, points, Ratio, usid
	, transID
)
SELECT 
	TipFirst, tipnumber, lastsix, Histdate
	, trancode, TranDesc, points, Ratio, usid
	, transID
FROM RN1.OnlineHistoryWork.dbo.Portal_Adjustments
WHERE CopyFlag IS NULL

SET @msg = '--->--->' + CONVERT(VARCHAR, @@ROWCOUNT) + ' Rows Pulled'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg


SET @msg = '---> Updating Copyflag on RN1'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

--Update CopyFlag on Web
UPDATE wpa
SET CopyFlag = @CopyDateTime
FROM RN1.OnlineHistoryWork.dbo.Portal_Adjustments wpa
INNER JOIN OnlineHistoryWork.dbo.Portal_Adjustments ppa
ON wpa.transid = ppa.transID
WHERE ppa.CopyFlag IS NULL
	AND wpa.CopyFlag IS NULL


BEGIN TRY

	SET @msg = '---> Updating Orphaned Entries'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg


	UPDATE pa
	SET CopyFlag = '01/01/1900'
	FROM Portal_Adjustments pa
	LEFT OUTER JOIN RewardsNow.dbo.dbprocessinfo dbpi
		ON pa.TipFirst = dbpi.DBNumber
	WHERE dbpi.DBNumber IS NULL
		AND pa.CopyFlag IS NULL
		
	DECLARE @pbc TABLE
	(
		bnid INT PRIMARY KEY
		, tipfirst VARCHAR(4)
		, trancode VARCHAR(2)
		, descr VARCHAR(100)
	)

	
	SET @msg = '---> Pulling data from Portal_Bonus_Codes'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg

	INSERT INTO @pbc (bnid, tipfirst, trancode, descr)
	SELECT bnid, tipfirst, trancode, descr
	FROM RN1.Rewardsnow.dbo.portal_bonus_codes 
	WHERE descr IS NOT NULL

		
	SET @msg = '---> Pulling data from Patton Portal_Adjustments'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg

	--FLAG ADJUSTMENTS TO CLOSED/NONEXISTENT TIPNUMBERS
	--NONEXISTENT TIPNUMBERS FLAGGED AS 08/08/1908
	--CLOSED TIPNUMBERS FLAGGED AS 09/09/1909
	SELECT	@SQLUpdate =	'
		IF ''?'' IN (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo)
			AND ''?'' IN (select name from sys.databases)
			BEGIN
				USE [?]
					IF EXISTS(SELECT name FROM sys.tables WHERE name = ''customerdeleted'') 
					and EXISTS(SELECT name FROM sys.tables WHERE name = ''customer'')
				BEGIN 
					UPDATE	OnlineHistoryWork.dbo.Portal_Adjustments 
					SET		CopyFlag = ''08/08/1908''
					FROM	OnlineHistoryWork.dbo.Portal_Adjustments pa left outer join CustomerDeleted cd on pa.tipnumber = cd.TIPNumber
															left outer join CUSTOMER c			on pa.tipnumber = c.TIPNUMBER 
					WHERE	CopyFlag is null
						and	cd.TIPNUMBER is null	
						and c.TIPNUMBER is null		
						and left(pa.tipnumber, 3) in (Select dbnumber from rewardsnow.dbo.dbprocessinfo where dbnamepatton = ''?'')
				END;	
			END 
							'

	EXEC sp_MSforeachdb @SQLUpdate

	SELECT	@SQLUpdate =	'
		IF ''?'' IN (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo)
			AND ''?'' IN (select name from sys.databases)
			BEGIN
				USE [?]
					IF EXISTS(SELECT name FROM sys.tables WHERE name = ''customerdeleted'') 
					and EXISTS(SELECT name FROM sys.tables WHERE name = ''customer'')
				BEGIN 
					UPDATE	OnlineHistoryWork.dbo.Portal_Adjustments 
					SET		CopyFlag = ''09/09/1909''
					FROM	OnlineHistoryWork.dbo.Portal_Adjustments pa join CustomerDeleted cd on pa.tipnumber = cd.TIPNumber
															left outer join CUSTOMER c			on pa.tipnumber = c.TIPNUMBER 
					WHERE	CopyFlag is null
						and	c.TIPNUMBER is null			
						and left(pa.tipnumber, 3) in (Select dbnumber from rewardsnow.dbo.dbprocessinfo where dbnamepatton = ''?'')
				END;	
			END 
							'

	EXEC sp_MSforeachdb @SQLUpdate	

	--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
	INSERT INTO #pa (dbnamepatton, tipfirst, tipnumber, lastsix, histdate, trancode, points, ratio, usid, transid
					, trandesc, status, cashvalue)
	SELECT  dbpi.DBNamePatton, pa.TipFirst, pa.TipNumber, pa.Lastsix, pa.HistDate, pa.TranCode, pa.Points, pa.Ratio, pa.USID, pa.TransID
		,	CASE	WHEN tcm.sid_trantype_trancode is not null 
					THEN pa.TranDesc + ' - Account:' + ISNULL(pa.lastsix, '')
					ELSE	CASE	WHEN RTRIM(ISNULL(pbc.descr, tt.description)) = RTRIM(pa.TranDesc) 
									THEN RTRIM(pa.TranDesc) 						
									ELSE RTRIM(ISNULL(pbc.descr, tt.description)) + ':' + RTRIM(pa.TranDesc) 
							END 
			END	AS trandesc
		,	isnull(tcm.dim_cfrtipfirsttrancodemap_initialstatus, 5)
		,	CASE	WHEN tcm.sid_trantype_trancode is not null 
						AND ISNUMERIC(REPLACE(SUBSTRING(pa.TranDesc, 1, CHARINDEX('-', pa.TranDesc)),'-','')) = 1
					THEN convert(money, SUBSTRING(pa.TranDesc, 1, CHARINDEX('-', pa.TranDesc) -1))
					ELSE 0
			END
	FROM	OnlineHistoryWork.dbo.Portal_Adjustments pa
		INNER JOIN	RewardsNow.dbo.dbprocessinfo dbpi --Only databases that are in DBProcessInfo
			ON	pa.TipFirst = dbpi.DBNumber
		INNER JOIN	sys.databases sdb --Only databases that actually exist
			ON	dbpi.DBNamePatton = sdb.name
		INNER JOIN	RewardsNow.dbo.TranType tt 
			ON	pa.trancode = tt.TranCode
		LEFT OUTER JOIN	@pbc pbc
			ON	pa.trancode = pbc.trancode AND pa.TipFirst = pbc.tipfirst
		LEFT OUTER JOIN	Rewardsnow.dbo.cfrTipfirstTrancodeMap tcm
			ON	pa.TipFirst = tcm.sid_dbprocessinfo_dbnumber AND pa.trancode = tcm.sid_trantype_trancode
	WHERE	pa.CopyFlag is null
	ORDER BY	pa.tipnumber
	
	UPDATE #pa
	SET trandesc = REPLACE(trandesc, '''', '')
	
	
	
	

	SELECT @max_sid_pa_id = MAX(sid_pa_id) FROM #pa
	
	SET @msg = '--->---> Patton Portal_Adjustment Records: ' + CONVERT(VARCHAR, @max_sid_pa_id)
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg

	SET @msg = '---> Pre-populating Redemption Trancode Reference table to speed processing '
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg

	INSERT INTO #redtc (trancode)
	SELECT DISTINCT m.sid_trantype_trancode
	FROM RewardsNow.dbo.vw_RNITrancodeMap m
	WHERE m.dim_rnitrancodegroup_name IN ('DECREASE_REDEMPTION', 'REDEMPTION_ADJUSTMENT')

	SET @msg = '---> Processing Adjustments By Database'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg

	DECLARE @db TABLE
	(
		sid_db_id INT IDENTITY(1,1) PRIMARY KEY
		, dbnamepatton VARCHAR(50)
	)
	
	INSERT INTO @db (dbnamepatton)
	SELECT DISTINCT dbnamepatton FROM #pa
	
	SELECT @max_sid_db_id = MAX(sid_db_id) FROM @db	

	WHILE @sid_db_id <= @max_sid_db_id
	BEGIN
	
		SELECT @dbnamepatton = dbnamepatton FROM @db WHERE sid_db_id = @sid_db_id
		
		SET @msg = '--->---> Record Begin (' + @dbnamepatton + ')'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg	

		-- HISTORY INSERT

		SELECT @SQLInsert = REPLACE(
			'INSERT INTO [<DBNAME>].dbo.history (Tipnumber, HISTDATE, TRANCODE, TRANCOUNT, POINTS, DESCRIPTION, SECID, RATIO, OVERAGE) 
			SELECT tipnumber, histdate, trancode, 1, points, trandesc, null, ratio, 0 
			FROM #pa
			WHERE dbnamepatton = ''<DBNAME>'''
			, '<DBNAME>', dbnamepatton)
		FROM @db
		WHERE dbnamepatton = @dbnamepatton
		
		SET @msg = '--->--->---> History Insert: ' + @SQLInsert
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
		
		EXEC sp_executesql @SQLInsert
		
		-- REBALANCE AFFECTED CUSTOMERS
		
		SELECT 
			@SQLUpdate = REPLACE(
			'UPDATE C 
			SET 
				runbalance = isnull((Select sum(points*ratio) from [<DBNAME>].dbo.history where trancode not in (SELECT sid_trantype_trancode FROM Rewardsnow.dbo.vw_RNITrancodeMap WHERE dim_rnitrancodegroup_name IN (''DECREASE_REDEMPTION'', ''REDEMPTION_ADJUSTMENT'')) and history.tipnumber = c.tipnumber),0) 
				, runredeemed = isnull((Select sum(points*ratio)*-1 from [<DBNAME>].dbo.history where trancode in (SELECT sid_trantype_trancode FROM Rewardsnow.dbo.vw_RNITrancodeMap WHERE dim_rnitrancodegroup_name IN (''DECREASE_REDEMPTION'', ''REDEMPTION_ADJUSTMENT'')) and history.tipnumber = c.tipnumber),0) 
				, runavailable = isnull((Select sum(points*ratio) from [<DBNAME>].dbo.history where history.tipnumber = c.tipnumber),0) 
			FROM [<DBNAME>].dbo.Customer C
			INNER JOIN (SELECT DISTINCT TIPNUMBER FROM #pa) T
				ON C.tipnumber = T.tipnumber
			'
			, '<DBNAME>', dbnamepatton)
		FROM @db
		WHERE sid_db_id = @sid_db_id

		SET @msg = '--->--->---> Updating Balances: ' + @sqlUpdate
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
		
		EXEC sp_executesql @SQLUpdate

		--FULFILLMENT UPDATE

		SELECT @sqlFulfillment = REPLACE(
			'INSERT INTO Fullfillment.dbo.Main (tipNumber, TipFirst, Source, Name1, Name2, TransID, OrderID 
				, HistDate, TranDesc, Points, Vendor, ItemNumber, CatalogQty, SName, SAddress1, SAddress2  
				, SCity, SState, SZip, SCountry, Phone1, Phone2, Email, RedStatus, Notes, TranCode, Catalogdesc 
				, RedReqFulDate, Routing, CashValue) 
			SELECT	
				c.TipNumber, c.TipFirst, ''CLASS'', c.ACCTNAME1, c.ACCTNAME2, p.transid, null as OrderID
				, p.histdate, p.trandesc, p.points, ''CAS'', ''CLASS FI FULFILLED'', 1, UPPER(c.ACCTNAME1), UPPER(c.ADDRESS1), UPPER(c.ADDRESS2) 
				, UPPER(c.CITY), UPPER(c.STATE), c.ZipCode, ''UNITED STATES'', c.HOMEPHONE, c.WORKPHONE, '''', p.status, '''', p.trancode, p.trandesc 
				, p.histdate, 2, p.cashvalue 
			FROM 
				[<DBNAME>].dbo.CUSTOMER c 
			INNER JOIN #pa p 
				ON c.tipnumber = p.tipnumber 
			INNER JOIN #redtc tc 
				ON p.trancode = tc.trancode 
			LEFT OUTER JOIN Fullfillment.dbo.Main mn 
				ON p.transid = mn.transid 
			WHERE 
				mn.transid IS NULL '
			, '<DBNAME>', dbnamepatton)
		FROM @db
		WHERE sid_db_id = @sid_db_id

		SET @msg = '--->--->---> Fullfillment Update: ' + @sqlFulfillment
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
		
		EXEC sp_executesql @sqlFulfillment			

		--COPYFLAG UPDATE ON PATTON

		SELECT @SQLUpdate =REPLACE(REPLACE(
			'UPDATE ppa 
			SET CopyFlag = ''<COPYDATETIME>'' 
			FROM Onlinehistorywork.dbo.Portal_Adjustments ppa
			INNER JOIN #pa tpa
				ON ppa.TRANSID = tpa.TRANSID
			INNER JOIN 
			( SELECT TIPNUMBER FROM [<DBNAME>].dbo.Customer
				UNION SELECT TIPNUMBER FROM [<DBNAME>].dbo.CustomerDeleted ) c
			ON ppa.tipnumber = c.tipnumber	
			WHERE tpa.dbnamepatton = ''<DBNAME>''
			
			'
			, '<COPYDATETIME>', CONVERT(VARCHAR, @CopyDateTime, 121))
			, '<DBNAME>', dbnamepatton)
		FROM @db
		WHERE sid_db_id = @sid_db_id

		SET @msg = '--->--->---> Copyflag update on Patton: ' + @SQLUpdate
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		EXEC sp_executesql @sqlUpdate	 

	
		SET @msg = '--->---> Record Complete ( ' + @dbnamepatton + ')'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
		
		SET @sid_db_id = @sid_db_id + 1
		
	END

	SET @msg = 'Updating Promo Point Ledgers'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg
	
	EXEC RewardsNow.dbo.usp_UpsertPromoPointDetailForAllTips

	SET @msg = 'END PROCESSING'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg
	
END TRY
BEGIN CATCH
	SET @msg = ERROR_MESSAGE()
	SET @msg = '******* ERROR IN PROCESS: ' + @msg
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @process, @msg
END CATCH






GO


