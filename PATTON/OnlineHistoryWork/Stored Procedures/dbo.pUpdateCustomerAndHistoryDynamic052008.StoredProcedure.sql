/****** Object:  StoredProcedure [dbo].[pUpdateCustomerAndHistoryDynamic052008]    Script Date: 02/26/2009 12:18:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                   SQL TO POST AUTO REDEMPTIONS                            */
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 5/2007                                                              */
/* REVISION: 1                                                               */
/*                                                                           */
/* Added logic to move redemptions with an invalid tipnumber                 */
/* to the ERRORREDEMPTION table and remove it from ONLHISTORY                */
/*                                                                           */
/*                                                                           */
/* SCAN:  SEB001	                                                     */
/*        5/22/2007  logic for invalid tipnumber                             */
/*                                                                           */
/* SCAN:  SEB002	                                                     */
/*        8/9/2007  add field "scountry" to logic                            */
/*						*/
/* SCAN: SRS001 */
/*	11/16/2007 update where clause for DBs to ignore CO-OPs */


CREATE PROCEDURE [dbo].[pUpdateCustomerAndHistoryDynamic052008] 
AS   

Declare 
@TipNum nchar(15),
@DBNum nchar(3),
@Points int,
@CatalogQty int,
@TotalPoints int,
@HistDate DateTime,
@TranCode nchar(2),
@CatalogCode nchar(20),
@CatalogDesc nchar(50),
@TransID uniqueidentifier,
@SQLInsert nvarchar(1000), 
@SQLUpdate nvarchar(1000),
@SQLIf nvarchar(1000),
@DBName nchar(50),
@trandescr nchar(10),
@email nchar(50),
@trandesc nvarchar(100), 
@postflag tinyint, 
@CatalogDesc1 nvarchar(150), 
@Copyflag datetime, 
@Source nchar(10), 
@saddress1 nchar(50), 
@saddress2 nchar(50), 
@scity nchar(50), 
@sstate nchar(5), 
@szipcode nchar(10), 
@scountry nvarchar(50), 
@hphone nchar(12), 
@wphone nchar(12), 
@notes nvarchar(250), 
@sname nchar(50), 
@ordernum bigint, 
@linenum bigint, 
@FullfillmentFlag datetime, 
@CreditRequestFlag datetime,
@hldDB char(3),
@Delcheck nchar(1),


--convert to Midnight today
@CopyDateTime datetime 

set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

Update onlhistory
set copyflag='01/01/1900'
where copyflag is null and not exists(select * from RewardsNOW.dbo.DBProcessInfo where DBNumber=left(onlhistory.TipNumber,3)) 

set @trandescr='DTS REDEEM'
set @hldDB=' '		

--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
DECLARE cRecs CURSOR FOR
	SELECT  TipNumber,substring(TipNumber,1,3)  as DBNum, Points,CatalogQty, HistDate, TransID, TranCode, CatalogCode, email, trandesc, postflag, substring(CatalogDesc,1,50) as CD, CatalogDesc, Copyflag, Source, saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, ordernum, linenum, FullfillmentFlag, CreditRequestFlag
	from OnlineHistoryWork.dbo.OnlHistory 
	Where CopyFlag is null
	--Follow line inserted by SRS on 11/16/2007
	--Following line removed by SEB on 12/3/2007
	--Where CopyFlag is null and substring(tipnumber,1,3) not in ('601','619')
	order by tipnumber
	
	OPEN cRecs 
	FETCH NEXT FROM cRecs INTO @TipNum, @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogCode, @email, @trandesc, @postflag, @CatalogDesc, @CatalogDesc1, @Copyflag, @Source, @saddress1, @saddress2, @scity, @sstate, @szipcode, @scountry, @hphone, @wphone, @notes, @sname, @ordernum, @linenum, @FullfillmentFlag, @CreditRequestFlag

	WHILE (@@FETCH_STATUS=0)
	BEGIN
		--Update the Customer table
	set @TotalPoints = (@Points * @CatalogQty)
	if @DBNum<>@hldDB
	Begin		
		if not exists(SELECT * from RewardsNOW.dbo.DBProcessInfo
			where DBNumber=@DBNum)
			Begin
				Goto Next_Record
			End
		else
			Begin			
				set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@DBNum)
				set @hldDB=@DBNum
			End		
	End		
	
/* SEB001 */	set @Delcheck='N'

/* SEB001 */	set @sqlif=N'if not exists(SELECT * from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber=@tipnum)
		Begin 
			set @Delcheck=''Y''
		End '
/* SEB001 */	exec sp_executesql @SQLIf, N'@TipNum nchar(15), @Delcheck nchar(1) output', @TipNum=@TipNum, @Delcheck=@Delcheck output		
	
/* SEB001 */	If @delcheck='Y'
/* SEB001 */	Begin		
/* SEB001 */		INSERT INTO OnlineHistoryWork.dbo.ErrorRedemption ( TipNumber, histdate, email, Points, trandesc, postflag, TransID, TranCode, CatalogCode, CatalogDesc, CatalogQty, Copyflag, Source, dbnum, saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, ordernum, linenum, FullfillmentFlag, CreditRequestFlag, DateDiscovered )
/* SEB001 */		values 	( @TipNum, @HistDate, @email, @Points, @trandesc, @postflag, @TransID, @TranCode, @CatalogCode, @CatalogDesc1, @CatalogQty, @Copyflag, @Source, @DBNum, @saddress1, @saddress2, @scity, @sstate, @szipcode, @scountry, @hphone, @wphone, @notes, @sname, @ordernum, @linenum, @FullfillmentFlag, @CreditRequestFlag, @CopyDateTime )
			
/* SEB001 */		Delete from OnlineHistoryWork.dbo.onlhistory
/* SEB001 */		where transid=@TransID
	
/* SEB001 */		Goto Next_Record
/* SEB001 */	End
		
	set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set RunRedeemed=RunRedeemed + @TotalPoints, RunAvailable=RunAvailable-@TotalPoints where TipNumber=@TipNum1'
	exec sp_executesql @SQLUpdate, N'@Tipnum1 nchar(15), @TotalPoints int', @Tipnum1=@Tipnum, @TotalPoints=@TotalPoints

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
			values 	( @TipNum, NULL, @HistDate, @TranCode, 1, @TotalPoints, @CatalogDesc, @trandescr, -1, 0 )'
	exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @HistDate datetime, @TranCode char(2), @TotalPoints int, @CatalogDesc varchar(50), @trandescr char(10)', 
				@Tipnum= @Tipnum, @HistDate=@HistDate, @TranCode=@TranCode, @TotalPoints=@TotalPoints, @CatalogDesc=@CatalogDesc, @trandescr=@trandescr

	-----Update the copyflag for the record
	Update OnlineHistoryWork.dbo.OnlHistory set CopyFlag= @CopyDateTime where TransID=@TransID and DBNum=@DBNum 

	Next_Record:
--		FETCH NEXT FROM cRecs INTO @TipNum,  @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogDesc
		FETCH NEXT FROM cRecs INTO @TipNum, @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogCode, @email, @trandesc, @postflag, @CatalogDesc, @CatalogDesc1, @Copyflag, @Source, @saddress1, @saddress2, @scity, @sstate, @szipcode, @scountry, @hphone, @wphone, @notes, @sname, @ordernum, @linenum, @FullfillmentFlag, @CreditRequestFlag
	END

CLOSE cRecs 
DEALLOCATE	cRecs
GO
