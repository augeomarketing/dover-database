USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[pUpdateCustomerAndHistoryDynamic]    Script Date: 04/23/2014 05:08:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pUpdateCustomerAndHistoryDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pUpdateCustomerAndHistoryDynamic]
GO

USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[pUpdateCustomerAndHistoryDynamic]    Script Date: 04/23/2014 05:08:14 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pUpdateCustomerAndHistoryDynamic] 

AS   

DECLARE 
		@dbname				VARCHAR(50)
	,	@tipfirst			VARCHAR(3)	
	,	@sql				NVARCHAR(max)	
	,	@trandescr			VARCHAR(10)		= 'DTS REDEEM'
	,	@CopyDateTime		DATE			= GetDate()

--FLAGE REDEMPTIONS WITH NO CORRESPONDING DATABASE
------------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE	o
SET		o.CopyFlag = '01/01/1900'
FROM	OnlineHistoryWork.dbo.OnlHistory o
	LEFT OUTER JOIN
		RewardsNOW.dbo.DBProcessInfo d
		ON	LEFT(o.TipNumber, 3) = d.DBNumber
WHERE	copyflag is null and d.dbnumber is null
------------------------------------------------------------------------------------------------------------------------------------------------------

--GET LIST OF DATABASES WITH REDEMPTIONS TO PROCESS
DECLARE crdbname CURSOR FOR
	SELECT	d.dbnumber, d.DBNamePatton
	FROM	RewardsNow.dbo.dbprocessinfo d 
		JOIN	(Select distinct LEFT(tipnumber, 3) as dbnumber  from OnlineHistoryWork.dbo.OnlHistory where CopyFlag is null) h
			ON	d.DBNumber = h.dbnumber 
		JOIN	master.dbo.sysdatabases s
			ON	d.DBNamePatton = s.name
	ORDER BY	d.DBNamePatton
	
OPEN crdbname 
FETCH NEXT FROM crdbname INTO @tipfirst, @dbname

WHILE (@@FETCH_STATUS=0)
	BEGIN

--REMOVE BAD REDEMPTIONS (REDEMPTIONS GREATER THAN BALANCE / TIPNUMBER NOT ACTIVE)
------------------------------------------------------------------------------------------------------------------------------------------------------
		SET @sql =	'
			INSERT INTO OnlineHistoryWork.dbo.ErrorRedemption 
				(TipNumber, histdate, email, Points, trandesc, postflag, TransID, TranCode, CatalogCode, CatalogDesc, CatalogQty, 
				 Copyflag, Source, dbnum, saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, 
				 ordernum, linenum, FullfillmentFlag, CreditRequestFlag, DateDiscovered)
			SELECT	onl.TipNumber,  onl.histdate,  onl.email, onl.Points,  onl.trandesc,  onl.postflag,  onl.TransID,  onl.TranCode,  onl.CatalogCode, 
					onl.CatalogDesc,  onl.CatalogQty,  onl.Copyflag,  onl.Source,  onl.dbnum,  onl.saddress1,  onl.saddress2,  onl.scity,  onl.sstate,  
					onl.szipcode,  onl.scountry,  onl.hphone,  onl.wphone,  onl.notes,  onl.sname,  onl.ordernum,  onl.linenum,  onl.FullfillmentFlag, 
					onl.CreditRequestFlag, ''<<COPYDATE>>''
			FROM	OnlineHistoryWork.dbo.onlhistory onl			 
				JOIN 
					(Select o.TipNumber, SUM(o.Points*o.CatalogQty) as points 
					 From OnlineHistoryWork.dbo.onlhistory o 
						Join	(Select tipnumber, sum(points*ratio) as points From [<<DBNAME>>].dbo.HISTORY Group by tipnumber) h
							On	o.TipNumber = h.tipnumber	
					 Where o.CopyFlag is null and o.Points > h.points 
					 Group by o.TipNumber
					) t
					ON	onl.TipNumber = t.TipNumber 
			;		
			INSERT INTO OnlineHistoryWork.dbo.ErrorRedemption 
				(TipNumber, histdate, email, Points, trandesc, postflag, TransID, TranCode, CatalogCode, CatalogDesc, CatalogQty, 
				 Copyflag, Source, dbnum, saddress1, saddress2, scity, sstate, szipcode, scountry, hphone, wphone, notes, sname, 
				 ordernum, linenum, FullfillmentFlag, CreditRequestFlag, DateDiscovered)
			SELECT	onl.TipNumber,  onl.histdate,  onl.email, onl.Points,  onl.trandesc,  onl.postflag,  onl.TransID,  onl.TranCode,  onl.CatalogCode, 
					onl.CatalogDesc,  onl.CatalogQty,  onl.Copyflag,  onl.Source,  onl.dbnum,  onl.saddress1,  onl.saddress2,  onl.scity,  onl.sstate,  
					onl.szipcode,  onl.scountry,  onl.hphone,  onl.wphone,  onl.notes,  onl.sname,  onl.ordernum,  onl.linenum,  onl.FullfillmentFlag, 
					onl.CreditRequestFlag, ''<<COPYDATE>>''
			FROM	OnlineHistoryWork.dbo.onlhistory onl
				LEFT OUTER JOIN	(Select distinct tipnumber from [<<DBNAME>>].dbo.CUSTOMER) c
					ON			onl.TipNumber = c.TIPNUMBER
			WHERE	c.TIPNUMBER is null AND onl.CopyFlag is null AND onl.tipnumber LIKE ''<<TIPFIRST>>%''		
			;
			DELETE onl
			FROM	OnlineHistoryWork.dbo.onlhistory onl
				JOIN
					OnlineHistoryWork.dbo.ErrorRedemption err
					ON onl.TransID = err.TransID 
			WHERE	onl.CopyFlag is null			
					'
		SET @sql = REPLACE(@sql, '<<DBNAME>>',@dbname)
		SET @sql = REPLACE(@sql, '<<COPYDATE>>',@CopyDateTime)
		SET @sql = REPLACE(@sql, '<<TIPFIRST>>',@tipfirst)
		EXEC sp_executesql @sql			
------------------------------------------------------------------------------------------------------------------------------------------------------
--INSERT REDEMPTIONS INTO HISTORY
------------------------------------------------------------------------------------------------------------------------------------------------------
		SET	@sql =	'
			INSERT INTO	[<<DBNAME>>].dbo.history 
				(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage)
			SELECT	o.TipNumber, NULL, o.HistDate, o.TranCode, 1, o.Points*o.CatalogQty, 
					CASE WHEN o.CatalogQty = 1 THEN o.Catalogdesc ELSE rtrim(o.CatalogDesc) + '' Qty('' + cast(o.CatalogQty as varchar(3)) + '')'' END, 
					''<<TRANDESCR>>'', -1, 0
			FROM 	OnlineHistoryWork.dbo.OnlHistory o
				JOIN	[<<DBNAME>>].dbo.CUSTOMER c
					ON	o.TipNumber = c.TIPNUMBER 
			WHERE o.CopyFlag is null
			'
		SET @sql = REPLACE(@sql, '<<DBNAME>>',@dbname)
		SET @sql = REPLACE(@sql, '<<TRANDESCR>>',@trandescr)
		EXEC sp_executesql @sql			

------------------------------------------------------------------------------------------------------------------------------------------------------
--FLAG REDEMPTIONS AS COMPLETED IN ONLINEHISTORYWORK
------------------------------------------------------------------------------------------------------------------------------------------------------
		SET @sql =	'
			UPDATE	o 
			SET		o.CopyFlag = ''<<COPYDATE>>'' 
			FROM	OnlineHistoryWork.dbo.OnlHistory o
				JOIN
					[<<DBNAME>>].dbo.CUSTOMER c
					ON	o.TipNumber = c.TIPNUMBER
			WHERE	o.CopyFlag is null
					'
		SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)	
		SET @sql = REPLACE(@sql, '<<COPYDATE>>',@CopyDateTime)
		EXECUTE sp_executesql @Sql
------------------------------------------------------------------------------------------------------------------------------------------------------


		NEXT_RECORD:
		FETCH NEXT FROM crdbname INTO @dbname
	END

CLOSE crdbname 
DEALLOCATE	crdbname


GO


