/****** Object:  StoredProcedure [dbo].[pUpdateCustomerAndHistoryDynamicold]    Script Date: 02/26/2009 12:18:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pUpdateCustomerAndHistoryDynamicold] AS   

	Declare 
	@TipNum varchar(15),
	@DBNum varchar(3),
	@Points int,
	@CatalogQty int,
	@TotalPoints int,
	@HistDate DateTime,
	@TranCode char(2),
	@CatalogDesc varchar(50),
	@TransID uniqueidentifier,
	@SQLInsert nvarchar(1000), 
	@SQLUpdate nvarchar(1000),
        @DBName varchar(50),
        @Transdescr char(10),
	--convert to Midnight today
	@CopyDateTime datetime, @hldDB char(3)
	set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

DECLARE cDBName CURSOR FOR
	SELECT  rtrim(DBName), dbnumber from OnlineHistoryWork.dbo.DBProcessInfoRedemptions
	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName, @DBNum

WHILE (@@FETCH_STATUS=0)
	BEGIN
		set @SQLUpdate=N'Update onlhistory 
				set copyflag=''01/01/1900''
				where copyflag is null and left(tipnumber,3)=@DBNUM and not exists(select tipnumber from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber= onlhistory.tipnumber)' 
		
		exec sp_executesql @SQLUpdate, N'@DBNum varchar(3)', @DBNum=@DBNum  

		FETCH NEXT FROM cDBName INTO @DBName, @DBNum
	END

Fetch_Error1:
CLOSE cDBName
DEALLOCATE cDBName


	set @Transdescr='DTS REDEEM'
	set @hldDB=' '		
	
	--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
	DECLARE cRecs CURSOR FOR
		SELECT  TipNumber,substring(TipNumber,1,3)  as DBNum, Points,CatalogQty, HistDate, TransID, TranCode, substring(CatalogDesc,1,50) as CD  
			from OnlineHistoryWork.dbo.OnlHistory 
			Where CopyFlag is null
			order by tipnumber
		OPEN cRecs 
		FETCH NEXT FROM cRecs INTO @TipNum, @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogDesc
		WHILE (@@FETCH_STATUS=0)
		BEGIN
			--Update the Customer table
		set @TotalPoints = (@Points * @CatalogQty)
		if @DBNum<>@hldDB
		Begin		
			if not exists(SELECT * from OnlineHistoryWork.dbo.DBProcessInfoRedemptions
				where DBNumber=@DBNum)
				Begin
					Goto Next_Record
				End
			else
				Begin			
					set @DBName=(SELECT  rtrim(DBName) from OnlineHistoryWork.dbo.DBProcessInfoRedemptions
					where DBNumber=@DBNum)
					set @hldDB=@DBNum
				End		
		End		

		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set RunRedeemed=RunRedeemed + @TotalPoints, RunAvailable=RunAvailable-@TotalPoints where TipNumber=@TipNum1'
		exec sp_executesql @SQLUpdate, N'@Tipnum1 nchar(15), @TotalPoints int', @Tipnum1=@Tipnum, @TotalPoints=@TotalPoints


		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
				values 	( @TipNum, NULL, @HistDate, @TranCode, 1, @TotalPoints, @CatalogDesc, @Transdescr, -1, 0 )'
		exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @HistDate datetime, @TranCode char(2), @TotalPoints int, @CatalogDesc varchar(50), @Transdescr char(10)', 
					@Tipnum= @Tipnum, @HistDate=@HistDate, @TranCode=@TranCode, @TotalPoints=@TotalPoints, @CatalogDesc=@CatalogDesc, @Transdescr=@Transdescr
	



		-----Update the copyflag for the record
		Update OnlineHistoryWork.dbo.OnlHistory set CopyFlag= @CopyDateTime where TransID=@TransID and DBNum=@DBNum 

Next_Record:
		FETCH NEXT FROM cRecs INTO @TipNum,  @DBNum, @Points, @CatalogQty, @HistDate, @TransID, @TranCode, @CatalogDesc
END

CLOSE cRecs 
DEALLOCATE	cRecs
GO
