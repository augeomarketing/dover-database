USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[spLoadFullfillmentDB]    Script Date: 03/09/2012 10:20:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadFullfillmentDB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadFullfillmentDB]
GO

USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[spLoadFullfillmentDB]    Script Date: 03/09/2012 10:20:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*                     LOAD DATA TO FULLFILLMENT DB                               	*/
/* BY: S.Blanchette                                                               	*/
/* DATE: 3/2007                                                                		*/
/* REVISION: 0                                                                    	*/
/* Reads the ONLINEHISTORYWORK.DBO.ONLHISTORY TABLE AND POPULATES THE 			*/
/* FULLFILLMENT DB							           	*/ 
/*                                                                                      */
/* REVISION: 1      									*/
/* BY: S.Blanchette                                                               	*/
/* DATE: 4/2007                                                                         */
/* REASON:  TO GET NAME FROM CUSTOMERDELETED TABLE IF NOT EXISTS IN CUSTOMER TABLE	*/
/* SCAN CODE:  SEB0407001                                                               */
/*                                                                                      */
/*                                                                                      */
/* REVISION: 21      									*/
/* BY: S.Blanchette                                                               	*/
/* DATE: 8/9/2007                                                                       */
/* REASON:  add field "scountry" to the logic                                           */
/* SCAN CODE:  SEB0407001                                                               */
/*                                                                                      */
/*                                                                                      */
CREATE  PROCEDURE [dbo].[spLoadFullfillmentDB] AS  
   Declare
   @TipNum char(15),
   @TipFirst char(3),
   @Points int,
   @CatalogQty int,
   @TotalPoints int,
   @HistDate DateTime,
   @TranCode char(2),
   @Catalogcode char(20),
   @CatalogDesc varchar(50),
   @Email varchar(50),
   @TransDesc varchar(100),
   @TransID uniqueidentifier,
   @Source char(10),
   @Saddress1 char(50),
   @Saddress2 char(50),
   @SCity char(50),
   @SState char(5),
   @SZipcode char(10),
   @Scountry varchar(50),
   @Hphone char(12),
   @Wphone char(12),
   @Notes varchar(250),
   @Sname char(50),
   @Ordernum bigint,
   @Linenum bigint,
   @SQLInsert nvarchar(1000),
   @SQLUpdate nvarchar(1000),
   @SQLSelect nvarchar(1000),
   @SQLRUN nvarchar(1000),
       @DBName varchar(100),
       @Transdescr char(10),
   @DBCheck varchar(50),
   @HldDBCheck varchar(50),
   @Name1 char(40),
   @name2 char(40),
   @nameout char(40),
--convert to Midnight today
   @CopyDateTime datetime, @hldDB char(3)
   set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
drop table FullFillWork

select *
into FullFillWork
from OnlineHistoryWork.dbo.OnlHistory
Where FullfillmentFlag is null and tipnumber not like '%999999%' -- PHB 3/18/2009 Added exclusion of tip#s with 999999
order by tipnumber

create index ix_FullFilLWork_TipNumber on dbo.FullFilLWork (TipNumber) -- PHB 3/18/2009 Added for performance

set @HldDBCheck=' '

DECLARE cRecs CURSOR FOR
   SELECT  TipNumber, substring(TipNumber,1,3) as TipFirst, HistDate, Email, Points, Trandesc, TransID, TranCode, CatalogCode, CatalogDesc, CatalogQty, Source, Saddress1, Saddress2, Scity, SState, SZipcode, scountry, Hphone, Wphone, notes, SName, Ordernum, Linenum       
   from dbo.FullFillWork
     order by tipnumber
     
OPEN cRecs

FETCH NEXT FROM cRecs INTO @TipNum, @TipFirst, @HistDate, @Email, @Points, @TransDesc, @TransID, @TranCode, @Catalogcode, @CatalogDesc, @CatalogQty, @Source, @Saddress1, @Saddress2, @SCity, @SState, @SZipcode, @Scountry, @Hphone, @Wphone, @Notes, @SName, @Ordernum, @Linenum
WHILE @@FETCH_STATUS=0
Begin           
	if @tipnum like '%999999%'
    begin
        goto Next_Record
       end
       
    SELECT @DBName = DBNamePatton FROM RewardsNow.dbo.dbprocessinfo where DBNumber = @TipFirst   

--CWH - commented out.  DBProcessinfo is the standard for tying a tipfirst to a database
   --set @DBCheck=@TipFirst + '%'
   --   If @DBCheck like '002%'
   --    Begin
   --        set @DBCheck='ASB'
   --    End
   --    Else
   --        if @DBCheck like '003%'
   --        Begin
   --            set @DBCheck='ASBCorp'
   --        End
   --       if @DBCheck<>@HldDBCheck
   --    Begin                   set @DBName=(select rtrim(name) from master.dbo.sysdatabases where name not in ('tempdb') and name like @DBCheck)
   --        set @hldDBCheck=@DBCheck
   --    End       

   /* Remove "Redeemed For " from TRANSDESC string   */
   set @TransDesc= REPLACE(@TransDesc, 'Redeem For ' , '' )

   set @SQLSelect=N'set @Nameout=(select acctname1 from ' + QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber=@Tipnum)'
   exec sp_executesql @SQLSelect, N'@Nameout char(40) output, @Tipnum char(15)', @tipnum=@tipnum, @Nameout=@Name1 output

   set @SQLSelect=N'set @Nameout=(select acctname2 from ' + QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber=@Tipnum)'
   exec sp_executesql @SQLSelect, N'@Nameout char(40) output, @Tipnum char(15)', @tipnum=@tipnum, @Nameout=@Name2 output

/****************************************************************************************************************/
/*														*/
/* SEB0407001  FOLLOWING SECTION INSERTED             								*/
/*														*/
/*														*/
/****************************************************************************************************************/
--
-- PHB 03/18/2009
-- Commented out.  Should not be any reason to check CustomerDeleted for an Active Redemption
--
	--IF @NAME1 IS NULL OR LEFT(@NAME1,3)='   '
	--BEGIN
	--	set @SQLSelect=N'set @Nameout=(select acctname1 from ' + QuoteName(@DBNAME) + N' .dbo.CustomerDELETED where tipnumber=@Tipnum)'
 --  		exec sp_executesql @SQLSelect, N'@Nameout char(40) output, @Tipnum char(15)', @tipnum=@tipnum, @Nameout=@Name1 output
	--END

	--IF @NAME2 IS NULL OR LEFT(@NAME2,3)='   '
	--BEGIN
	--   	set @SQLSelect=N'set @Nameout=(select acctname2 from ' + QuoteName(@DBNAME) + N' .dbo.CustomerDELETED where tipnumber=@Tipnum)'
 --  		exec sp_executesql @SQLSelect, N'@Nameout char(40) output, @Tipnum char(15)', @tipnum=@tipnum, @Nameout=@Name2 output
	--END
/****************************************************************************************************************/
/*														*/
/* SEB0407001  END OF INSERTED SECTION             								*/
/*														*/
/*														*/
/****************************************************************************************************************/

   /* Load record to the Main Table    */
   INSERT INTO Fullfillment.dbo.Main( TipNumber, Tipfirst, Source, Name1, Name2, Transid, OrderID, Histdate, Trandesc, Points, Vendor, Itemnumber, CatalogQty, SName, Saddress1, Saddress2, Scity, SState, SZip, Scountry,Phone1, Phone2, Email, RedStatus, Notes, TranCode, CatalogDesc )
       VALUES     ( @TipNum, @TipFirst, @Source, @Name1, @name2, @Transid, @Linenum, @HistDate, @TransDesc, @Points, left(@Catalogcode,3), @Catalogcode, @CatalogQty, Upper(@SName), UPPER(@Saddress1), UPPER(@Saddress2), UPPER(@SCity), UPPER(@SState), @SZipcode, UPPER(@Scountry), @Hphone, @Wphone, @Email, '0', @Notes, @TranCode, @CatalogDesc )

   /* INSERT into TRAVELCERT if Trancode='RT' TCStatus=0     */
   If @Trancode='RT'
   Begin           INSERT INTO Fullfillment.dbo.TravelCert( TransId, TCID, TCStatus)
       values     ( @TransID, right(@CatalogDesc,6), '0')
   End

   /* INSERT into TRAVELCERT if Trancode='RU' TCStatus=1     */
   /* values     ( @TransID, right(@CatalogDesc,6), '1')  Replaced 4/2/07 JWH */
   If @Trancode='RU'
   Begin           INSERT INTO Fullfillment.dbo.TravelCert( TransId, TCID, TCStatus, TCLastSix)
       values     ( @TransID, right(@CatalogDesc,6), '1', right(left(@CatalogDesc,12),6)) /* Added 4/2/07 JWH */
   End

   /* Insert into SHIPPING Table if Trancode=RB, RM, RC    */
   /* If @Trancode='RM' or @Transdesc like '%Bonus%' or @Transdesc like '%Gift%'  Replaced 3/22/07 SEB */
   If @Trancode='RM' or @Transdesc like '%Bonus%' or (@Transdesc ='RC' and @Catalogcode not like 'GCR-VISASVC%')  /* Added 3/22/07 SEB */
   Begin           INSERT INTO Fullfillment.dbo.Shipping( TransId)
       values     ( @TransID)
   End


   /* Insert into GIFTCARD Table if TransDesc is like GIFT  */
   /* If @Transdesc like '%Gift%'  Replaced 3/22/07 SEB */
   If @Trancode ='RC' and @Catalogcode not like 'GCR-VISASVC%'  /* Added 3/22/07 SEB */
   Begin           INSERT INTO Fullfillment.dbo.GiftCard(TransId)
       values     ( @TransID)
   End

   /* INSERT into CASHBACK if Trancode='RB'     */
   /* Added 4/2/07 JWH */
   If @Trancode='RB'
   Begin           INSERT INTO Fullfillment.dbo.CashBack( TransId, CBLastSix)
       values     ( @TransID, right(@CatalogDesc,6))
   End

   /*  Set the FullfillmentFlag  */
   update OnlineHistoryWork.dbo.OnlHistory
      set FullfillmentFlag=getdate()
     where tipnumber=@TipNum and TransID=@TransID
        
   goto Next_Record
      Next_Record:
   FETCH NEXT FROM cRecs INTO @TipNum, @TipFirst, @HistDate, @Email, @Points, @TransDesc, @TransID, @TranCode, @Catalogcode, @CatalogDesc, @CatalogQty, @Source, @Saddress1, @Saddress2, @SCity, @SState, @SZipcode, @Scountry, @Hphone, @Wphone, @Notes, @SName, @Ordernum, @Linenum
END

CLOSE cRecs
DEALLOCATE    cRecs

--CWH

UPDATE	M 
SET		Routing = CASE WHEN Routing IS NULL THEN C.sid_routing_id ELSE Routing END
	,	cashvalue = CASE WHEN cashvalue = 0 THEN C.dim_catalog_cashvalue ELSE cashvalue END
FROM	fullfillment.dbo.Main M INNER JOIN Catalog.dbo.catalog C
	ON	M.ItemNumber = C.dim_catalog_code OR M.ItemNumber = 'GC' + C.dim_catalog_code
WHERE	Routing is null OR (cashvalue = 0 and C.dim_catalog_cashvalue <> 0)


IF (SELECT COUNT(*) FROM fullfillment.INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'NullRoutingTracking') > 0
BEGIN
	INSERT INTO fullfillment.dbo.NullRoutingTracking
	(
		dim_nullroutingtracking_tipnumber
		, dim_nullroutingtracking_trancode
		, dim_nullroutingtracking_itemnumber
		, dim_nullroutingtracking_transid
	)
	SELECT 
		TipNumber
		, TranCode
		, ItemNumber
		, TransID
	FROM fullfillment.dbo.Main
	WHERE Routing is null
END



GO


