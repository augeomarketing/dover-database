USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProcessCombineRequests]    Script Date: 08/07/2015 11:20:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ProcessCombineRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ProcessCombineRequests]
GO

USE [OnlineHistoryWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProcessCombineRequests]    Script Date: 07/15/2015 07:49:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE	[dbo].[usp_ProcessCombineRequests]  @runlog BIT = 1

AS

-----------------WORK TABLE PREPARATION--------------------------------------------------------------------
IF object_id('tempdb..#newtiptracking') IS NOT NULL		BEGIN	DROP TABLE #newtiptracking	END
IF object_id('tempdb..#tipdistinct') IS NOT NULL		BEGIN	DROP TABLE #tipdistinct		END
IF object_id('tempdb..#thisAffiliat') IS NOT NULL		BEGIN	DROP TABLE #thisAffiliat	END
-----------------------------------------------------------------------------------------------------------
-----------------VARIABLES---------------------------------------------------------------------------------
CREATE TABLE #newtiptracking
(
		newtip				VARCHAR(15)
	,	oldtip				VARCHAR(15)
	,	trandate			DATETIME
	,	oldtippoints		INT
	,	oldtipredeemed		INT
	,	oldtiprank			VARCHAR(1)
	,	transid				UNIQUEIDENTIFIER			
)

CREATE TABLE #tipdistinct
(
		newtip				VARCHAR(15)
	,	oldtip				VARCHAR(15)
)

CREATE TABLE #thisAffiliat 
(
		ACCTID				VARCHAR(MAX)
	,	AcctType			VARCHAR(MAX)
	,	TIPNUMBER			VARCHAR(MAX)
	,	DATEADDED			DATETIME
	,	SECID				VARCHAR(MAX)
	,	AcctStatus			VARCHAR(MAX)
	,	AcctTypeDesc		VARCHAR(MAX)
	,	LastName			VARCHAR(MAX)
	,	YTDEarned			INT
	,	CustID				VARCHAR(MAX)	
)

DECLARE 
		@sql				NVARCHAR(MAX)
	,	@increment			INT	
	,	@maxincrement		INT					= (Select MAX(sid_Portal_Combines_Tablelist_id) From OnlineHistoryWork.dbo.Portal_Combines_Tablelist)
	,	@tipfirst			VARCHAR(3)
	,	@tiplast			VARCHAR(12)
	,	@dbname				VARCHAR(255)
	,	@databasename		VARCHAR(255)
	,	@tablename			VARCHAR(255)
	,	@fieldname			VARCHAR(255)
	,	@bbdate				DATETIME
	,	@bbfield			VARCHAR(10)
	,	@Process			VARCHAR(255)		= 'usp_ProcessCombineRequests'
	,	@Msg				VARCHAR(MAX)		= ''
-----------------------------------------------------------------------------------------------------------
-----------------COMBINE PROCESSING:	DATA CLEANING AND PREPARATION--------------------------------------
IF @runlog = 1
	BEGIN
		DELETE FROM RewardsNow.dbo.BatchDebugLog WHERE dim_batchdebuglog_process = @Process
		SET @Msg = 'Begin Process'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)

		SET @Msg = '--->1.Scrub Combine Requests'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
	END

----DELETE REQUESTS WITH NULL TIPs in PRIMARY OR SECONDARY
DELETE FROM	OnlineHistoryWork.dbo.Portal_Combines WHERE TIP_PRI is null or TIP_SEC is null
----FLAG REQUESTS WHERE PRIMARY TIP IS LISTED AS A SECONDARY ELSEWHERE
UPDATE	pc1
SET pc1.CopyFlagCompleted = '01/01/1901' 
FROM OnlineHistoryWork.dbo.Portal_Combines pc1 INNER JOIN OnlineHistoryWork.dbo.Portal_Combines pc2 ON pc1.TIP_PRI = pc2.TIP_SEC
where pc1.CopyFlagCompleted is null
----FLAG REQUESTS WHERE SECONDARY TIP IS IN MULTIPLE REQUESTS WITH DIFFERENT PRIMARIES (ONE REQUEST WILL REMAIN ACTIVE)
UPDATE	pc1
SET pc1.CopyFlagCompleted = '02/02/1902' 
FROM OnlineHistoryWork.dbo.Portal_Combines pc1 INNER JOIN OnlineHistoryWork.dbo.Portal_Combines pc2 ON pc1.TIP_SEC = pc2.TIP_SEC
where pc1.CopyFlagCompleted is null and pc2.CopyFlagCompleted is null and pc1.TIP_PRI > pc2.TIP_PRI

----PULL COMBINE REQUESTS INTO WORK TABLE
IF @runlog = 1
	BEGIN
		SET @Msg = '--->2.Load Combine Work Table'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
	END
	
INSERT INTO #newtiptracking
SELECT	'' as newtip, TIP_PRI as oldtip, GETDATE() as trandate, '0' as oldtippoints, '0' as oldtipredeemed, 'P' as oldtiprank, transid as transid
FROM	OnlineHistoryWork.dbo.Portal_Combines pc INNER JOIN RewardsNow.dbo.dbprocessinfo dbp on LEFT(pc.TIP_PRI, 3) = dbp.DBNumber
WHERE	CopyFlagCompleted is null and dbp.DBAvailable = 'Y'
UNION
SELECT	'' as newtip, TIP_SEC as oldtip, GETDATE() as trandate, '0' as oldtippoints, '0' as oldtipredeemed, 'S' as oldtiprank, transid as transid
FROM	OnlineHistoryWork.dbo.Portal_Combines pc INNER JOIN RewardsNow.dbo.dbprocessinfo dbp on LEFT(pc.TIP_PRI, 3) = dbp.DBNumber
WHERE	CopyFlagCompleted is null and dbp.DBAvailable = 'Y'
	
----ASSIGN NEW TIPNUMBERS TO COMBINE REQUESTS
DECLARE tipassign_cursor CURSOR FOR
	SELECT DISTINCT	left(ntt.oldtip, 3) 
	FROM	#newtiptracking ntt
		INNER JOIN	RewardsNow.dbo.dbprocessinfo dbp ON left(ntt.oldtip, 3) = dbp.dbnumber
		INNER JOIN	sys.databases sd ON dbp.DBNamePatton = sd.name
OPEN	tipassign_cursor
FETCH NEXT FROM tipassign_cursor into @tipfirst
WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @runlog = 1
			BEGIN
				SET @Msg = '--->3.'+@tipfirst+': Flag and Remove Requests with Inactive TIPs'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END

		SET @dbname = (Select DBNamePatton From RewardsNow.dbo.dbprocessinfo Where DBNumber = @tipfirst)
		SET	@sql =	N'
			UPDATE	ntt
			SET		ntt.newtip = ''XXXXXXXXXXXXXXX''	
			from	#newtiptracking ntt LEFT OUTER JOIN [<<DBNAME>>].dbo.CUSTOMER c ON ntt.oldtip = c.TIPNUMBER 
			where	c.TIPNUMBER is NULL AND ntt.oldtip LIKE ''<<TIPFIRST>>'' + ''%''
					'
		SET	@sql =	REPLACE(@sql, '<<DBNAME>>',@dbname)
		SET	@sql =	REPLACE(@sql, '<<TIPFIRST>>',@tipfirst)
		EXEC	sp_executesql @sql

		DELETE pc FROM OnlineHistoryWork.dbo.Portal_Combines pc INNER JOIN #newtiptracking ntt ON pc.transid = ntt.transid Where ntt.newtip = 'XXXXXXXXXXXXXXX'
		DELETE ntt1 FROM #newtiptracking ntt1 INNER JOIN #newtiptracking ntt2 on ntt1.transid = ntt2.transid  Where ntt2.newtip = 'XXXXXXXXXXXXXXX'

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->4.'+@tipfirst+': Assign New TIPs'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END		

		INSERT INTO #tipdistinct (newtip, oldtip) SELECT DISTINCT '', oldtip FROM #newtiptracking WHERE oldtiprank = 'P'
		SET @tiplast = (Select RIGHT(LastTipNumberUsed, 12) From RewardsNow.dbo.dbprocessinfo Where DBNumber = @tipfirst)	

		UPDATE	#tipdistinct
		SET		@Tiplast = (cast(@Tiplast as BIGINT) + 1),
				newtip = @tipfirst + RewardsNow.dbo.ufn_Pad(@Tiplast, 'L', 12, '0')
		WHERE	newtip = '' and	oldtip like @tipfirst + '%'

		UPDATE	ntt
		SET		ntt.newtip = td.newtip
		FROM	#newtiptracking ntt INNER JOIN #tipdistinct td on ntt.oldtip = td.oldtip

		UPDATE	ntt1
		SET		ntt1.newtip = ntt2.newtip
		FROM	#newtiptracking ntt1 join (Select newtip, transid from #newtiptracking where newtip <> '') ntt2 on ntt1.transid = ntt2.transid 
		WHERE	ntt1.newtip = ''
	
		FETCH NEXT FROM tipassign_cursor INTO @tipfirst
	END	
CLOSE 		tipassign_cursor
DEALLOCATE	tipassign_cursor

IF @runlog = 1
	BEGIN
		SET @Msg = '--->5.Update Table Fields and Flags'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
	END	

---SET COPYFLAGCOMPLETED TO IN-PROCESS (05/05/1955)	IN PORTAL_COMBINES
UPDATE	pc
SET		pc.CopyFlagCompleted = '05/05/1955'
FROM	OnlineHistoryWork.dbo.Portal_Combines pc
	INNER JOIN	#newtiptracking ntt
		ON	pc.transid = ntt.transid
---UPDATE LASTTIPNUMBERUSED IN DBPROCESSINFO
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
UPDATE	dbp
SET		dbp.LastTipNumberUsed = ntt.newtip
FROM	RewardsNow.dbo.dbprocessinfo dbp 
	INNER JOIN (Select LEFT(newtip, 3) as tipfirst, MAX(newtip) as newtip From #newtiptracking group by LEFT(newtip, 3)) ntt
		ON	dbp.DBNumber = ntt.tipfirst

-----------------------------------------------------------------------------------------------------------
-----------------COMBINE PROCESSING:	PROCESSING---------------------------------------------------------
IF @runlog = 1
	BEGIN
		SET @Msg = '--->6.Insert into New_TipTracking Table'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
	END
---INSERT NEW RECORDS INTO NEW_TIPTRACKING TABLE
INSERT INTO OnlineHistoryWork.dbo.New_TipTracking
	(NewTIP, OldTip, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank, CopyFlagTransfered, Transid)
SELECT	NewTIP, OldTip, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank, /*CAST(GETDATE() as DATE)*/ NULL as CopyFlagTransfered, Transid
FROM	#newtiptracking	
		
IF @runlog = 1
	BEGIN
		SET @Msg = '--->7.Update Database Tables'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
	END
---UPDATE NEW_TIPTRACKING TABLE

DECLARE combine_cursor CURSOR FOR
	SELECT distinct left(ntt.oldtip, 3)
	FROM	#newtiptracking ntt
		INNER JOIN	RewardsNow.dbo.dbprocessinfo dbp ON left(ntt.oldtip, 3) = dbp.dbnumber
		INNER JOIN	sys.databases sd ON dbp.DBNamePatton = sd.name
OPEN	combine_cursor
FETCH NEXT FROM combine_cursor into @tipfirst
WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @dbname = (Select DBNamePatton From RewardsNow.dbo.dbprocessinfo Where DBNumber = @tipfirst)
		IF @runlog = 1
			BEGIN
				SET @Msg = '--->8.'+@tipfirst+': Update New_TipTracking with point earnings'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE AVAILABLE AND REDEEMED IN NEW_TIPTRACKING
		SET	@sql =	N'
			UPDATE	ntt
			SET		ntt.OldTipPoints = ISNULL(ha.points, 0), ntt.OldTipRedeemed = ISNULL(hr.points, 0)
			FROM	OnlineHistoryWork.dbo.New_TipTracking ntt	
				INNER JOIN		#newtiptracking ntt1 ON ntt.OldTip = ntt1.oldtip
				LEFT OUTER JOIN	(Select tipnumber, SUM(points*ratio) as points from [<<DBNAME>>].dbo.HISTORY group by TIPNUMBER) ha ON ntt.OldTip = ha.TIPNUMBER
				LEFT OUTER JOIN	(Select tipnumber, SUM(points*ratio)* -1 as points from [<<DBNAME>>].dbo.HISTORY where TRANCODE like ''R%'' or trancode in (''IR'',''DR'') group by TIPNUMBER) hr ON ntt.OldTip = hr.TIPNUMBER				
					'
		SET	@sql =	REPLACE(@sql, '<<DBNAME>>',@dbname)
		EXEC	sp_executesql @sql

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->9.'+@tipfirst+': Update Customer/Customerdeleted'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE CUSTOMER/CUSTOMERDELETED RECORDS
		SET	@sql =	N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''CUSTOMER'')
				BEGIN
					INSERT INTO [<<DBNAME>>].dbo.CustomerDeleted
						(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST,
						 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode,
						 StatusDescription, 
						 HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, Misc1, Misc2, Misc3, Misc4, Misc5,
						 RunBalanceNew, RunAvaliableNew, DateDeleted)
					SELECT 
						TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST,
						ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode,
						CASE WHEN ntt.oldtiprank = ''P'' THEN ''Pri Combined to '' + ntt.newtip WHEN ntt.oldtiprank = ''S'' THEN ''SEC Combined to '' + ntt.newtip END, 
						HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, Misc1, Misc2, Misc3, Misc4, Misc5,
						RunBalanceNew, RunAvaliableNew, CAST(GETDATE() as DATE)  
					FROM [<<DBNAME>>].dbo.Customer c INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt ON c.TIPNUMBER = ntt.oldtip

					UPDATE	c
					SET		c.TIPNUMBER = ntt.newtip, c.TIPLAST = CAST(RIGHT(ntt.newtip, 12) as VARCHAR(12))
					FROM	[<<DBNAME>>].dbo.Customer c INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt ON c.TIPNUMBER = ntt.oldtip
					WHERE	ntt.oldtiprank = ''P''
					
					DELETE c 
					FROM [<<DBNAME>>].dbo.Customer c INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt ON c.TIPNUMBER = ntt.oldtip 
					WHERE ntt.oldtiprank = ''S''
				END	
					'
		SET	@sql =	REPLACE(@sql, '<<DBNAME>>',@dbname)
		EXEC	sp_executesql @sql

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->10.'+@tipfirst+': Update Customer_Stage'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE CUSTOMER_STAGE RECORDS
		SET	@sql =	N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''Customer_Stage'')
				BEGIN
					UPDATE	c
					SET		c.TIPNUMBER = ntt.newtip, c.TIPLAST = CAST(RIGHT(ntt.newtip, 12) as VARCHAR(12))
					FROM	[<<DBNAME>>].dbo.Customer_Stage c INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt ON c.TIPNUMBER = ntt.oldtip
					WHERE	ntt.oldtiprank = ''P''
					
					DELETE c 
					FROM [<<DBNAME>>].dbo.Customer_Stage c INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt ON c.TIPNUMBER = ntt.oldtip 
					WHERE ntt.oldtiprank = ''S''
				END	
					'
		SET	@sql =	REPLACE(@sql, '<<DBNAME>>',@dbname)
		EXEC	sp_executesql @sql

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->11.'+@tipfirst+': Update Affiliat'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE AFFILIAT RECORDS
		SET	@sql =	N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''AFFILIAT'')
				BEGIN
					DELETE FROM	#thisAffiliat
					
					INSERT INTO #thisAffiliat
						(ACCTID, AcctType, TIPNUMBER, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					SELECT	a.ACCTID, a.AcctType, ntt.newtip, a.DATEADDED, a.SECID, a.AcctStatus, a.AcctTypeDesc, a.LastName, a.YTDEarned, a.CustID
					FROM	[<<DBNAME>>].dbo.AFFILIAT a INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt on a.TIPNUMBER = ntt.oldtip
					
					DELETE a FROM [<<DBNAME>>].dbo.AFFILIAT a INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt on a.TIPNUMBER = ntt.oldtip

					INSERT INTO [<<DBNAME>>].dbo.AFFILIAT
						(ACCTID, AcctType, TIPNUMBER, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					SELECT	ACCTID, AcctType, TIPNUMBER, MIN(DATEADDED), MIN(SECID), MIN(AcctStatus), MIN(AcctTypeDesc),
							ISNULL(MIN(LastName),''''), SUM(YTDEarned), MIN(CustID)
					FROM	#thisAffiliat
					GROUP BY	ACCTID, AcctType, TIPNUMBER
				END	
					'
		SET	@sql =	REPLACE(@sql, '<<DBNAME>>',@dbname)
		EXEC	sp_executesql @sql

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->12.'+@tipfirst+': Update Affiliat_Stage'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE AFFILIAT_STAGE RECORDS
		SET	@sql =	N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''AFFILIAT_Stage'')
				BEGIN
					DELETE FROM	#thisAffiliat
					
					INSERT INTO #thisAffiliat
						(ACCTID, AcctType, TIPNUMBER, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					SELECT	a.ACCTID, a.AcctType, ntt.newtip, a.DATEADDED, a.SECID, a.AcctStatus, a.AcctTypeDesc, a.LastName, a.YTDEarned, a.CustID
					FROM	[<<DBNAME>>].dbo.AFFILIAT_Stage a INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt on a.TIPNUMBER = ntt.oldtip
					
					DELETE a FROM [<<DBNAME>>].dbo.AFFILIAT_Stage a INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt on a.TIPNUMBER = ntt.oldtip

					INSERT INTO [<<DBNAME>>].dbo.AFFILIAT_Stage
						(ACCTID, AcctType, TIPNUMBER, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					SELECT	ACCTID, AcctType, TIPNUMBER, MIN(DATEADDED), MIN(SECID), MIN(AcctStatus), MIN(AcctTypeDesc),
							ISNULL(MIN(LastName),''''), SUM(YTDEarned), MIN(CustID)
					FROM	#thisAffiliat
					GROUP BY	ACCTID, AcctType, TIPNUMBER
				END	
					'
		SET	@sql =	REPLACE(@sql, '<<DBNAME>>',@dbname)
		EXEC	sp_executesql @sql

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->13.'+@tipfirst+': SET Beginning_Balance_Table'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE BEGINNING_BALANCE_TABLE RECORDS
		SET	@bbdate	= (Select PointsUpdated From RewardsNow.dbo.dbprocessinfo where DBNumber = @tipfirst)
		SET	@bbfield	= 'MonthBeg' + cast(month(@bbdate) as varchar(2))
		SET	@sql = N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''Beginning_Balance_Table'')
				BEGIN
					INSERT INTO	[<<DBNAME>>].dbo.Beginning_Balance_Table
					SELECT	c.TIPNUMBER,0,0,0,0,0,0,0,0,0,0,0,0
					FROM	[<<DBNAME>>].dbo.CUSTOMER c LEFT OUTER JOIN [<<DBNAME>>].dbo.Beginning_Balance_Table b ON c.TIPNUMBER = b.Tipnumber
					WHERE	b.Tipnumber is null

					DELETE bbt FROM [<<DBNAME>>].dbo.Beginning_Balance_Table bbt 
						LEFT OUTER JOIN [<<DBNAME>>].dbo.CUSTOMER c on bbt.Tipnumber = c.TIPNUMBER WHERE c.TIPNUMBER IS NULL

					UPDATE	bbt
					SET		<<FIELD>> = his.hispoints
					FROM	[<<DBNAME>>].dbo.Beginning_Balance_Table bbt 
						INNER JOIN
							(select	tipnumber, isnull(SUM(Points*ratio), 0) as hispoints
							 from	[<<DBNAME>>].dbo.HISTORY 
							 where	HISTDATE < ''<<DATE>>''
							 group by	TIPNUMBER
							) his
							ON	bbt.Tipnumber = his.TIPNUMBER
				END				
					'	
		SET	@sql = REPLACE(@sql, '<<FIELD>>', @bbfield)
		SET	@sql = REPLACE(@sql, '<<DATE>>', @bbdate)
		SET	@sql = REPLACE(@sql, '<<DBNAME>>', @dbname)
		EXEC	sp_executesql @sql

		IF @runlog = 1
			BEGIN
				SET @Msg = '--->14.'+@tipfirst+': Update Optional Tables'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
			END
		---UPDATE ALL OPTIONAL TABLES
		SET @increment = 1
		WHILE @increment <= @maxincrement
			BEGIN
				SELECT	@databasename	= ISNULL(dim_Portal_Combines_Tablelist_dbname, @dbname) 
					,	@tablename		= dim_Portal_Combines_Tablelist_tablename
					,	@fieldname		= dim_Portal_Combines_Tablelist_fieldname
				FROM	OnlineHistoryWork.dbo.Portal_Combines_Tablelist
				WHERE	sid_Portal_Combines_Tablelist_id = @increment
				
				SET	@sql =	N'
					IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''<<TABLENAME>>'')
						BEGIN
							UPDATE	tn 
							SET		tn.<<FIELDNAME>> = ntt.NewTip 
							FROM	[<<DBNAME>>].dbo.<<TABLENAME>> tn INNER JOIN (Select distinct newtip, oldtip, oldtiprank from #newtiptracking) ntt ON tn.<<FIELDNAME>> = ntt.oldtip
							WHERE	ntt.oldtip LIKE ''<<TIPFIRST>>'' + ''%''
						END
							'
				SET	@sql =	REPLACE(@sql, '<<DBNAME>>', @databasename)
				SET	@sql =	REPLACE(@sql, '<<TABLENAME>>',@tablename)
				SET	@sql =	REPLACE(@sql, '<<FIELDNAME>>',@fieldname)
				SET	@sql =	REPLACE(@sql, '<<TIPFIRST>>',@tipfirst)
				EXEC	sp_executesql @sql

				SET @increment = @increment + 1
			END	

		EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory @tipfirst
	
		FETCH NEXT FROM combine_cursor INTO @tipfirst
	END	
CLOSE 	combine_cursor
DEALLOCATE combine_cursor
		
IF @runlog = 1
	BEGIN
		SET @Msg = '--->15.Update Completion Flag'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry) VALUES (@Process, @Msg)
	END
---SET COPYFLAGCOMPLETED TO COMPLETE (CURRENT DATE)	IN PORTAL_COMBINES
UPDATE	pc
SET		pc.CopyFlagCompleted = CAST(GETDATE() as DATE)
FROM	OnlineHistoryWork.dbo.Portal_Combines pc
	INNER JOIN	#newtiptracking ntt ON pc.transid = ntt.transid
	INNER JOIN	RewardsNow.dbo.dbprocessinfo dbp ON left(ntt.oldtip, 3) = dbp.dbnumber
	INNER JOIN	sys.databases sd ON dbp.DBNamePatton = sd.name


-----------------------------------------------------------------------------------------------------------
----TESTING CODE
--Select * from #newtiptracking
--order by left(oldtip, 3), transid, oldtiprank
GO


