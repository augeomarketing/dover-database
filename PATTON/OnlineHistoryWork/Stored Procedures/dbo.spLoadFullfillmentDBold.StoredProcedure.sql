/****** Object:  StoredProcedure [dbo].[spLoadFullfillmentDBold]    Script Date: 02/26/2009 12:18:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadFullfillmentDBold] AS   

	Declare 
	@TipNum char(15),
	@TipFirst char(3),
	@Points int,
	@CatalogQty int,
	@TotalPoints int,
	@HistDate DateTime,
	@TranCode char(2),
	@Catalogcode char(20),
	@CatalogDesc varchar(50),
	@Email varchar(50),
	@TransDesc varchar(100),
	@TransID uniqueidentifier,
	@Source char(10),
	@Saddress1 char(50),
	@Saddress2 char(50),
	@SCity char(50),
	@SState char(5),
	@SZipcode char(10),
	@Hphone char(12),
	@Wphone char(12),
	@Notes varchar(250),
	@Sname char(50),
	@Ordernum bigint,
	@Linenum bigint,
	@SQLInsert nvarchar(1000), 
	@SQLUpdate nvarchar(1000),
	@SQLSelect nvarchar(1000),
	@SQLRUN nvarchar(1000),
        @DBName varchar(100),
        @Transdescr char(10),
	@DBCheck varchar(50),
	@HldDBCheck varchar(50),
	@Name1 char(40),
	@name2 char(40),
	@nameout char(40),
--convert to Midnight today
	@CopyDateTime datetime, @hldDB char(3)
	set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
drop table FullFillWork

select *
into FullFillWork
from OnlineHistoryWork.dbo.OnlHistory
Where FullfillmentFlag is null
order by tipnumber

set @HldDBCheck=' '

DECLARE cRecs CURSOR FOR
		SELECT  TipNumber, substring(TipNumber,1,3) as TipFirst, HistDate, Email, Points, Trandesc, TransID, TranCode, CatalogCode, CatalogDesc, CatalogQty, Source, Saddress1, Saddress2, Scity, SState, SZipcode, Hphone, Wphone, notes, SName, Ordernum, Linenum  
			from FullFillWork 
			order by tipnumber
OPEN cRecs 
		FETCH NEXT FROM cRecs INTO @TipNum, @TipFirst, @HistDate, @Email, @Points, @TransDesc, @TransID, @TranCode, @Catalogcode, @CatalogDesc, @CatalogQty, @Source, @Saddress1, @Saddress2, @SCity, @SState, @SZipcode, @Hphone, @Wphone, @Notes, @SName, @Ordernum, @Linenum
	WHILE (@@FETCH_STATUS=0)
	Begin	
		if @tipnum like '%999999%'
		begin
			goto Next_Record
		end	


	set @DBCheck=@TipFirst + '%'
	
	If @DBCheck like '002%'
		Begin
			set @DBCheck='ASB'
		End
		Else
			if @DBCheck like '003%'
			Begin
				set @DBCheck='ASBCorp'
			End
		
	if @DBCheck<>@HldDBCheck
		Begin		
			set @DBName=(select rtrim(name) from master.dbo.sysdatabases where name not in ('tempdb') and name like @DBCheck) 
			set @hldDBCheck=@DBCheck
		End		


	/* Remove "Redeemed For " from TRANSDESC string   */
	set @TransDesc= REPLACE(@TransDesc, 'Redeem For ' , '' )

	set @SQLSelect=N'set @Nameout=(select acctname1 from ' + QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber=@Tipnum)'
	exec sp_executesql @SQLSelect, N'@Nameout char(40) output, @Tipnum char(15)', @tipnum=@tipnum, @Nameout=@Name1 output

	set @SQLSelect=N'set @Nameout=(select acctname2 from ' + QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber=@Tipnum)'
	exec sp_executesql @SQLSelect, N'@Nameout char(40) output, @Tipnum char(15)', @tipnum=@tipnum, @Nameout=@Name2 output

	/* Load record to the Main Table    */
	INSERT INTO Fullfillment.dbo.Main( TipNumber, Tipfirst, Source, Name1, Name2, Transid, OrderID, Histdate, Trandesc, Points, Vendor, Itemnumber, CatalogQty, SName, Saddress1, Saddress2, Scity, SState, SZip, Phone1, Phone2, Email, RedStatus, Notes, TranCode )
		values 	( @TipNum, @TipFirst, @Source, @Name1, @name2, @Transid, @Linenum, @HistDate, @TransDesc, @Points, left(@Catalogcode,3), @Catalogcode, @CatalogQty, Upper(@SName), UPPER(@Saddress1), UPPER(@Saddress2), UPPER(@SCity), UPPER(@SState), @SZipcode, @Hphone, @Wphone, @Email, '0', @Notes, @TranCode )

	/* INSERT into TRAVELCERT if Trancode='RT' TCStatus=0     */
	If @Trancode='RT'
	Begin	
		INSERT INTO Fullfillment.dbo.TravelCert( TransId, TCID, TCStatus)
		values 	( @TransID, right(@CatalogDesc,6), '0')
	End

	/* INSERT into TRAVELCERT if Trancode='RU' TCStatus=1     */
	If @Trancode='RU'
	Begin	
		INSERT INTO Fullfillment.dbo.TravelCert( TransId, TCID, TCStatus)
		values 	( @TransID, right(@CatalogDesc,6), '1')
	End

	/* Insert into SHIPPING Table if Trancode=RB, RM, RC    */
	/* If @Trancode='RM' or @Transdesc like '%Bonus%' or @Transdesc like '%Gift%'  Replaced 3/22/07 SEB */ 
	If @Trancode='RM' or @Transdesc like '%Bonus%' or (@Transdesc ='RC' and @Catalogcode not like 'GCR-VISASVC%')  /* Added 3/22/07 SEB */ 
	Begin	
		INSERT INTO Fullfillment.dbo.Shipping( TransId)
		values 	( @TransID)
	End

	/* Insert into GIFTCARD Table if TransDesc is like GIFT  */
	/* If @Transdesc like '%Gift%'  Replaced 3/22/07 SEB */
	If @Transdesc ='RC' and @Catalogcode not like 'GCR-VISASVC%'  /* Added 3/22/07 SEB */
	Begin	
		INSERT INTO Fullfillment.dbo.GiftCard(TransId)
		values 	( @TransID)
	End

	/*  Set the FullfillmentFlag  */
	update OnlineHistoryWork.dbo.OnlHistory
 	  set FullfillmentFlag=getdate() 
	  where tipnumber=@TipNum and TransID=@TransID
	  	
	goto Next_Record
	
	
Next_Record:
	FETCH NEXT FROM cRecs INTO @TipNum, @TipFirst, @HistDate, @Email, @Points, @TransDesc, @TransID, @TranCode, @Catalogcode, @CatalogDesc, @CatalogQty, @Source, @Saddress1, @Saddress2, @SCity, @SState, @SZipcode, @Hphone, @Wphone, @Notes, @SName, @Ordernum, @Linenum
END

CLOSE cRecs 
DEALLOCATE	cRecs
GO
