/****** Object:  StoredProcedure [dbo].[spPostPortalAdjustmentsDynamicBJQ]    Script Date: 02/26/2009 12:18:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
-- RDT 07/27/2007 Changed LastSix char(6) to LastSix char(20). Some clients have more than 6 in the last 6. (member numbers, etc.) 

/*  RDT 07/27/2007 */ -- RDT 07/27/2007 Added checks for DBAvailable flag in dbo.DBProcessInfo 
/* RDT 07/30/2007 */ -- Removed checks for DBAvailable flag in dbo.DBProcessInfo 

CREATE PROCEDURE [dbo].[spPostPortalAdjustmentsDynamicBJQ] AS   

	Declare 
	@TipFirst char(3),	
	@TipNum char(15),
-- RDT 07/27/2007 @LastSix char(6),
	@LastSix char(20),
	@HistDate DateTime,
	@TranCode char(2),
	@Transdesc char(50),
	@TotalPoints int,
	@Points int,
	@Ratio float,
	@USID char(4),
	@TransID uniqueidentifier,
	@SQLInsert nvarchar(1000),
	@SQLSelect nvarchar(1000), 
	@SQLUpdate nvarchar(1000),
	@DBNum varchar(3),	
        @DBName varchar(50),
       	@CopyDateTime datetime, 
	@hldDB char(3),
-- BJQ 07/31/2008 fields added to supply fullfilment db fields for RB,RD AND RZ (RZ = FI REDEEMED ITEM) transactions thru class
	@acctname1 varchar(40),
	@acctname2 varchar(40),
	@address1 varchar(40),
	@address2 varchar(40),
	@city varchar(40),
	@state char(2),
	@zip Varchar(15),
	@homephone varchar(10),
	@workPhone varchar(10)

--convert to Midnight today
	Set @CopyDateTime=convert(smalldatetime,convert(varchar(10),GetDate(),101))

-- BJQ 7/31/2008 added begin and end transaction Statements

Begin transaction ProcessPortalAdjustments;

	Update Portal_Adjustments
	set copyflag='01/01/1900'
	where copyflag is null and not exists(select * from RewardsNOW.dbo.DBProcessInfo where DBNumber=Portal_Adjustments.Tipfirst) 

	set @hldDB=' '		
	
	--get the tip numbers of the records that haven't been processed yet (CopyFlag is null)
	DECLARE cRecs CURSOR FOR
		SELECT  TipFirst, TipNumber, Lastsix, HistDate, TranCode, TranDesc, Points, Ratio, USID, TransID  
			from OnlineHistoryWork.dbo.Portal_Adjustments 
			Where CopyFlag is null
			order by tipnumber
		OPEN cRecs 
		FETCH NEXT FROM cRecs INTO @DBNum, @TipNum, @LastSix, @HistDate, @TranCode, @Transdesc, @Points, @Ratio, @USID, @TransID
		WHILE (@@FETCH_STATUS=0)
		BEGIN
			--Update the Customer table
 		set @TotalPoints = (@Points * @Ratio)
		if @DBNum<>@hldDB
		Begin		
			if not exists(SELECT * from RewardsNOW.dbo.DBProcessInfo
			where DBNumber=@DBNum)
				Begin
					Goto Next_Record
				End
/*  RDT 07/27/2007 */ --RDT 07/30/2007   	If (Select DBAvailable from RewardsNOW.dbo.DBProcessInfo 	
/*  RDT 07/27/2007 */ --RDT 07/30/2007  		where DBNumber=@DBNum) <> 'Y' 
/*  RDT 07/27/2007 */ --RDT 07/30/2007   		Begin 
/*  RDT 07/27/2007 */ --RDT 07/30/2007   			Goto Next_Record
/*  RDT 07/27/2007 */ --RDT 07/30/2007   		End
			Else
				Begin			
					set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
					where DBNumber=@DBNum)
					set @hldDB=@DBNum
				End		
		End

/* BJQ 8/1/2008 */
-- RunRedeemed must be updated if the transaction is a Redemption or a redeem decrease
-- If tran type is not a type of redemption update as before
--

--/* BJQ001 */	SELECT @transdesc=description from  RewardsNOW.dbo.TranType where trancode = @trancode 
 
		if (@TranCode like 'R%' or
		   @TranCode = 'DZ')
		   begin	
		       begin transaction UpdateCustandHistforREDS; 

			set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set RunAvailable = RunAvailable + @TotalPoints,  RunRedeemed=RunRedeemed +  @Points   where TipNumber=@TipNum1'
			exec sp_executesql @SQLUpdate, N'@Tipnum1 nchar(15),  @points int, @TotalPoints int', @Tipnum1=@Tipnum, @TotalPoints=@TotalPoints, @Points=@Points

		        set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
				values 	( @TipNum, @LastSix, @HistDate, @TranCode, 1, @Points, @Transdesc, ''CLASS'', @Ratio, 0 )'
-- RDT 07/27/2007 exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @LastSix char(6), @HistDate datetime, @TranCode char(2), @Points int, @Transdesc char(50), @Ratio float ', 
-- RDT 07/27/2007 	@Tipnum= @Tipnum, @LastSix=@LastSix, @HistDate=@HistDate, @TranCode=@TranCode, @Points=@Points, @Transdesc=@Transdesc, @Ratio=@Ratio
		        exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @LastSix char(20), @HistDate datetime, @TranCode char(2), @Points int, @Transdesc char(50), @Ratio float ', 
					@Tipnum= @Tipnum, @LastSix=@LastSix, @HistDate=@HistDate, @TranCode=@TranCode, @Points=@Points, @Transdesc=@Transdesc, @Ratio=@Ratio

		       Commit transaction UpdateCustandHistforREDS;
		   end
		else
		   begin
		       begin transaction UpdateCustandHist;

		       set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set RunAvailable = RunAvailable + @TotalPoints, RunBalance=RunBalance + @TotalPoints where TipNumber=@TipNum1'
		       exec sp_executesql @SQLUpdate, N'@Tipnum1 nchar(15), @TotalPoints int', @Tipnum1=@Tipnum, @TotalPoints=@TotalPoints
	


		       set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history ( TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage )
				values 	( @TipNum, @LastSix, @HistDate, @TranCode, 1, @Points, @Transdesc, ''CLASS'', @Ratio, 0 )'
-- RDT 07/27/2007 exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @LastSix char(6), @HistDate datetime, @TranCode char(2), @Points int, @Transdesc char(50), @Ratio float ', 
-- RDT 07/27/2007 	@Tipnum= @Tipnum, @LastSix=@LastSix, @HistDate=@HistDate, @TranCode=@TranCode, @Points=@Points, @Transdesc=@Transdesc, @Ratio=@Ratio
		      exec sp_executesql @SQLInsert, N'@Tipnum nchar(15), @LastSix char(20), @HistDate datetime, @TranCode char(2), @Points int, @Transdesc char(50), @Ratio float ', 
					@Tipnum= @Tipnum, @LastSix=@LastSix, @HistDate=@HistDate, @TranCode=@TranCode, @Points=@Points, @Transdesc=@Transdesc, @Ratio=@Ratio

		       Commit transaction UpdateCustandHist;
		   end	
-- BJQ 7/31/2008
-- This logic has been added to allow cashback redemptions (Trancode 'RB') to be inserted into the fullfillment
-- data base as completed when entered through class by the fi
-- The select from the customer record is necessary to complete the record in the Main table of the Fullfillment DB
		if (@TranCode like 'R%' or
		   @TranCode = 'DZ')
		   begin

		       begin transaction UpdateFullFillmentMain;



--/* BJQ001 */	       SELECT @transdesc=description from  RewardsNOW.dbo.TranType where trancode = @trancode 

         	       set @SQLSelect=N'SELECT @acctname1=acctname1,@acctname2=acctname2,@address1=address1,@address2=address2,@city=city,@state=state,@zip=zipcode,'  
		       SET @SQLSelect = @SQLSelect + N' @homephone=homephone,@workphone=workphone from ' + QuoteName(@DBName)
		       SET @SQLSelect = @SQLSelect + N' .dbo.customer where tipnumber=@tipnum' 
	               exec sp_executesql @SQLSelect, N'@acctname1 varchar(40)output,@acctname2 varchar(40)output,@address1 varchar(40)output,@address2 varchar(40)output,
							@city varchar(40)output,@state char(2)output,@zip Varchar(15)output,@homephone varchar(10)output,
							@workPhone varchar(10)output,@TipNum nchar(15)',
							@acctname1=@acctname1 output,@acctname2=@acctname2 output,@address1=@address1 output,@address2 =@address2 output, 
							@city =@city output,@state=@state output,@zip=@zip output,@homephone=@homephone output,@workPhone=@workPhone output,@TipNum=@TipNum    

			   INSERT INTO Fullfillment.dbo.Main ( TipNumber, Tipfirst, Source, Name1, Name2, Transid,
				       OrderID, Histdate, Trandesc, Points, Vendor, Itemnumber, CatalogQty, SName,
				       Saddress1, Saddress2, Scity, SState, SZip, Scountry,Phone1, Phone2, Email,
				       RedStatus, Notes, TranCode, CatalogDesc, RedReqFulDate )
       			   values     ( @TipNum, left(@TipNum,3), 'CLASS', @acctname1, @acctname2, @Transid,
					' ', @HistDate, @TransDesc, @Points, 'CAS', 'CLASS FI FULLFILLED', '1', Upper(@acctname1),
					UPPER(@address1), UPPER(@address2), UPPER(@City), UPPER(@State), @Zip,
					'UNITED STATES', @homephone, @workPhone, ' ', '5', ' ', @TranCode, @transdesc, @HistDate )

		       Commit transaction UpdateFullFillmentMain;

/* BJQ001 */	  end


	
-----Update the copyflag for the record
		Update OnlineHistoryWork.dbo.Portal_Adjustments  set CopyFlag= @CopyDateTime where TransID=@TransID and TipFirst=@DBNum 

Next_Record:
		FETCH NEXT FROM cRecs INTO @DBNum, @TipNum, @LastSix, @HistDate, @TranCode, @Transdesc, @Points, @Ratio, @USID, @TransID
END

Commit transaction ProcessPortalAdjustments;

CLOSE cRecs 
DEALLOCATE	cRecs
GO
