/****** Object:  StoredProcedure [dbo].[spCombineTipsNewTEST]    Script Date: 02/26/2009 12:18:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO PROCESS  COMBINES                          */
/*                                                                         */
/* BY:  R.Tremblay                                          */
/* DATE: 8/2006                                                               */
/* REVISION: 1                                                                */
/* This creates a new tip. Copies the pri customer to customerdeleted.
 Copies sec customer to customerdeleted. Changes  pri customer tip  to new tip
 Changes all pri affiliat records to new Tip. Changes all pri history records to new Tip.
 Changes all sec affiliat records to new Tip. Changes all sec history records to new Tip.
 Add secondary TIP values (runavailable, runbalance, etc) to NEW TIP 
 Deletes sec customer                                                           

 Added code to call spCombineBeginningBalance
 Added code to combine Bonus tale transactions 10/10/06

 Altered Table by adding OldTipPoints column to track the number of points transferred from the old tip
 Added code to update the points transferred 

 10/3/06 - Added OldTipRank = P = Primary S = Secondary

 Input file must be ordered by PRITIP to check for duplicates. 
 NEW Tables needed 
	COMB_IN
	COMB_err
THE FOLLOWING TABLE NEEDS TO BE CREATED ON PATTON AND RN1
	Comb_TipTracking
BRAEK LOGIC REPEATED AFTER WHILE STATEMENT TO PROCESS LAST RECORD AFTER EOF    BJQ 12/27/2006
WRITE TO COMB_ERR CHANGED TO ADD NAME1 AND NAME2 TO THE OUTPUT REC             BJQ 12/28/2006
                                                                                                 */

/*                     Modified to deal with Portal Combines                      */
/* BY: S.Blanchette                                                               */
/* DATE: 3/27/2007                                                                */
/* REVISION: 1                                                                    */
/* Reads the Portal_Combine table and processes the Combines on Patton            */ 

/*                     Modified to check DBAVAILABLE so as not to allow           */
/*    combines if the DB is in monthly process                                    */
/* BY: S.Blanchette                                                               */
/* DATE: 5/25/2007                                                                */
/* REVISION: 2                                                                    */
/* SCAN: SEB002                                                                   */
/* Gets the DBAVAILABLE flag from DBPROCESSINFO table if "Y" process if           */
/* "N" the bypass that combine                                                    */ 

/* BY: S.Blanchette                                                               */
/* DATE: 12/4/2007                                                                */
/* REVISION: 3                                                                    */
/* SCAN: SEB003                                                                   */
/* Add code to check for null tip when getting last tip used.  If null get out    */
/*                                                                                */ 

/******************************************************************************/	
CREATE PROCEDURE [dbo].[spCombineTipsNewTEST] AS    

/* SEB003 */  Truncate table combineerror

declare @a_TIPPRI char(15), @a_TIPSEC char(15), @a_TIPBREAK char(15), @a_errmsg char(80), @a_Transid uniqueidentifier, @a_TipPrefix char(3) 
declare @a_namep char(40), @a_namep1 char(40), @a_names char(40), @a_names1 char(40), @a_addrp char(40), @a_addrs char(40)     
declare @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int
declare @DateToday datetime, @DBName char(50), @DBAvailable char(1)
declare @NewTip as char(15), @NewTipValue bigint, @DeleteDescription as char(40)
declare @Old_TipPri nvarchar(15)
declare @SQLSet nvarchar(1000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000), @SQLDelete nvarchar(1000), @SQLIf nvarchar(1000)

SET @DateToday = convert(smalldatetime,convert(varchar(10),GetDate(),101))
SET @a_TIPBREAK = '0'
set @Old_TipPri = '0'
/******************************************************************************/	
/*  Delete null tips                                */
delete from OnlineHistoryWork.dbo.Portal_Combines where tip_pri is null
delete from OnlineHistoryWork.dbo.Portal_Combines where tip_sec is null

/******************************************************************************/	
/*  Delete tips  from Comb_IN if there is an error message               */
--delete from comb_in where Len( RTrim( errmsg) ) > 0 

/******************************************************************************/	
/*  DECLARE CURSOR FOR PROCESSING COMB_IN TABLE                               */
declare combine_crsr cursor for 
	select TIP_PRI, TIP_SEC, Transid
	from OnlineHistoryWork.dbo.Portal_Combines
	where CopyFlagCompleted is null 
	order by TIP_PRI, TIP_SEC

open combine_crsr

fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_Transid

/******************************************************************************/	
/* MAIN PROCESSING                                                */
/******************************************************************************/	
if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin --Top of loop	

	IF @a_TIPPRI <> @Old_TipPri
	   set @NewTip = '0'
	
	set @a_TipPrefix=left(@a_TIPPRI,3)

	/* Check for new primary tip. If new then create a new tip else use the same "new" tip */
	IF @a_TIPPRI <> @a_TIPBREAK 
	Begin
		if not exists(SELECT * from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@a_TipPrefix)
		Begin
/* SEB002 */		set @DBAvailable = 'N'			
			Goto Next_Record
		End
/* SEB002 */	else
/* SEB002 */		set @DBAvailable=(SELECT  DBAvailable from RewardsNOW.dbo.DBProcessInfo
/* SEB002 */					where DBNumber=@a_TipPrefix)

/* SEB002 */		if @DBAvailable<>'Y'
/* SEB002 */		Begin
/* SEB002 */			Goto Next_Record
/* SEB002 */		End
			else
				Begin			
					set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
						where DBNumber=@a_TipPrefix)
				End		
print '@dbname'
print @dbname

		/*    Create new tip          */
	declare @LastTipUsed char(15)
	exec rewardsnow.dbo.spGetLastTipNumberUsed @a_TipPrefix, @LastTipUsed output
	select @LastTipUsed as LastTipUsed

/* Begin code add SEB003           */
	if @LastTipUsed is null
	Begin
		insert into combineerror (Error)
		values ('ERROR Last tipnumber used')
		
		goto Fetch_Error1
	end
/* End code add SEB003             */

	set @NewTipValue = cast(@LastTipUsed as bigint) + 1  
print 'Old Tip'
print @LastTipUsed
print 'New Tip'
print @NewTipValue 

		/*  Make sure the tipnumber is 15 character long by adding zeros to the begining of the Tip*/
		/* clients with tip prefix starting zeros will have max tips less than 15 characters */

/* 	Code replaced with Code Sarah recreated to pad with leading zeros where approiate  BJQ 12/2006 */
/*		set @NewTipValue = replicate('0',15 - Len( RTrim( @NewTipValue ) ) ) +  @NewTipValue */
/*		set @NewTip = Cast(@NewTipValue as char)                                              */ 

       		 set @NewTip = Cast(@NewTipValue as char)
           	 set @NewTip = replicate('0',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip 
print 'New Tip2'
print @NewTipValue 
		/******************************************************************************/	
		/*  copy PRI customer to customerdeleted set description to new tipnumber     */
		/* IF the PRI tip is not repeated */
 
		set @DeleteDescription = 'Pri Combined to '  + @NewTip

		set @SQLInsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.CustomerDeleted
			(
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
			)
	       		SELECT 
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
			FROM ' + QuoteName(@DBName) + N' .dbo.Customer where tipnumber = @a_TIPPRI'
print @SQLInsert 
-- RDT		exec sp_executesql @SQLInsert, N'@a_TIPPRI char(15), @DeleteDescription as char(40), @DateToday datetime', @a_TIPPRI=@a_TIPPRI, @DeleteDescription=@DeleteDescription, @DateToday=@DateToday 

		/******************************************************************************/	
		/* Add Get PRI tip values from Customer  */
 		set @SQLSelect=N'select	@a_RunAvailable =RunAvailable, @a_RunRedeemed=RunRedeemed from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber = @a_TIPPRI'
		exec sp_executesql @SQLSelect, N'@a_RunAvailable int output, @a_RunBalance int output, @a_RunRedeemed int output, @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @a_RunAvailable=@a_RunAvailable output, @a_RunBalance=@a_RunBalance output, @a_RunRedeemed=@a_RunRedeemed output

		/*  Insert record into Comb_TipTracking table  */

--RDT 		Insert into OnlineHistoryWork.dbo.New_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank, Transid) Values (@NewTip, @a_TIPPRI, @DateToday, @a_RunAvailable, @a_RunRedeemed,'P', @a_Transid )

		/******************************************************************************/	
		/*  Output Last Tipnumber Used    */
print '@a_TipPrefix' 
print @a_TipPrefix
print ' @NewTip'
print @NewTip
		exec RewardsNOW.dbo.spPutLastTipNumberUsed @a_TipPrefix, @NewTip

	End --@a_TIPPRI <> @a_TIPBREAK 

	/******************************************************************************/	
	/*  copy SEC customer to customerdeleted set description to new tipnumber         */
	set @DeleteDescription = 'Sec Combined to '  + @NewTip
 
	set @SQLInsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.CustomerDeleted
			(
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
			)
	       		SELECT 
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
			FROM ' + QuoteName(@DBName) + N' .dbo.Customer where tipnumber = @a_TIPSEC'
print @SQLInsert
--RDT 	exec sp_executesql @SQLInsert, N'@a_TIPSEC char(15), @DeleteDescription as char(40), @DateToday datetime', @a_TIPSEC=@a_TIPSEC, @DeleteDescription=@DeleteDescription, @DateToday= @DateToday 

	/******************************************************************************/	
	/* Add Get SEC  tip values from Customer  */
 		set @SQLSelect=N'select	@a_RunAvailable =RunAvailable, @a_RunRedeemed=RunRedeemed from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber = @a_TIPSEC'
		exec sp_executesql @SQLSelect, N'@a_RunAvailable int output, @a_RunBalance int output, @a_RunRedeemed int output, @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @a_RunAvailable=@a_RunAvailable output, @a_RunBalance=@a_RunBalance output, @a_RunRedeemed=@a_RunRedeemed output
 
	/******************************************************************************/	
	/*  Insert record into Comb_TipTracking table  */
print 'Insert record into Comb_TipTracking table'

--RDT 	Insert into OnlineHistoryWork.dbo.New_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank, Transid) Values (@NewTip, @a_TIPSEC, @DateToday, @a_RunAvailable, @a_RunRedeemed,'S', @a_Transid )

	/******************************************************************************/	
	/*   change  PRI tip in customer to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK
	   begin
	   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
print @SQLUpdate
--RDT	   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 
		SET @Old_TipPri = @a_TIPPRI
 	   end 

	/* OLD_TIPPRI ADDED for check of next record in Fetch Statement. If The nhext Tipnumber to be combined */
        /* equals a_TIPPRI the next time thru thw while statement the program will not have to select for the */
	/* Customer Record because it has just been processed   BJQ 12/2006                */

	/******************************************************************************/	
	/*         Change all PRI affiliat records to new Tip              */
	IF @a_TIPPRI <> @a_TIPBREAK  
	   begin
	   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.AFFILIAT set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
print @SQLUpdate
--RDT exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 	   end 
 
	/******************************************************************************/	
	/*         Change all SEC affiliat records to new Tip              */
   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.AFFILIAT set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC'
print @SQLUpdate
--RDT    	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @NewTip=@NewTip

	/******************************************************************************/	
	/*       Change all PRI history records to new Tip          */
	IF @a_TIPPRI <> @a_TIPBREAK  
	   begin
	   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.HISTORY set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
print @SQLUpdate
--RDT	   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 	   end 

	/******************************************************************************/	
	/*       Change all SEC history records to new Tip          */
   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.HISTORY set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC'
print @SQLUpdate
--RDT   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @NewTip=@NewTip

	/******************************************************************************/	
	/* Get and Add SEC tip values to Customer NEW TIP */
/*	set @SQLSelect=N'select 
		@a_RunAvailable =RunAvailable, 
		@a_RunBalance=RunBalance, 
		@a_RunRedeemed=RunRedeemed
		from ' + QuoteName(@DBName) + N'.dbo.customer where tipnumber = @a_TIPSEC'
	exec sp_executesql @SQLSelect, N'@a_RunAvailable int output, @a_RunBalance int output, @a_RunRedeemed int output, @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @a_RunAvailable=@a_RunAvailable output, @a_RunBalance=@a_RunBalance output, @a_RunRedeemed=@a_RunRedeemed output
 
 	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.customer	
		set RunAvailable = RunAvailable + @a_RunAvailable, 
	     	RunBalance=RunBalance + @a_RunBalance, 
 	     	RunRedeemed=RunRedeemed + @a_RunRedeemed  
		where tipnumber = @NEWTIP'
   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int ', @a_RunAvailable=@a_RunAvailable, @a_RunBalance=@a_RunBalance, @a_RunRedeemed=@a_RunRedeemed, @NewTip=@NewTip
*/
	/******************************************************************************/	
	/*    change  PRI tip in BEGINNING_BALANCE_TABLE to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK  
		begin
	   		set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
print @SQLUpdate
--RDT 	   		exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 	   	end 
	/******************************************************************************/	
	/* Add SEC tip values to Beginning_Balance_Table NEW TIP for each month */
--RDT	exec spCombineBeginningBalance @NewTip, @a_TIPSEC

	/******************************************************************************/	
	/* Delete SEC Beginning_Balance_Table                  */
 	set @SQLDelete=N'Delete ' + QuoteName(@DBName) + N' .dbo.Beginning_Balance_Table where TIPNUMBER = @a_TIPSEC'
print @SQLUpdate
--RDT	exec sp_executesql @SQLDelete, N'@a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC
 	   	
	/******************************************************************************/	
	/* Delete SEC Customer                  */
 	set @SQLDelete=N'Delete ' + QuoteName(@DBName) + N' .dbo.CUSTOMER where TIPNUMBER = @a_TIPSEC'
print @SQLUpdate
--RDT	exec sp_executesql @SQLDelete, N'@a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC

	/******************************************************************************/	
	/*       Change all ONLHISTORY records to new Tip          */
	IF @a_TIPPRI <> @a_TIPBREAK  
	   begin
		print 'Tip Break'
--RDT	   	Update OnlineHistoryWork.dbo.OnlHistory set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI
	   end 

	/******************************************************************************/	
	/*       Change all ONLHISTORY records to new Tip          */
--RDT   	Update OnlineHistoryWork.dbo.OnlHistory set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC


	/******************************************************************************/	
	/*         Change all PRI & SEC BONUS records to new Tip              */
/*	
	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Onetimebonuses'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.OneTimeBonuses  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		End '
	exec sp_executesql @SQLif, N'@NewTip char(15), @a_TIPPRI char(15)', @NewTip=@NewTip, @a_TIPPRI=@a_TIPPRI

	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Onetimebonuses'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.OneTimeBonuses  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC 
		End '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPSEC char(15)', @NewTip=@NewTip, @a_TIPSEC=@a_TIPSEC

*/
	/******************************************************************************/	
	/*         Change all PRI & SEC ACCOUNT_REFERENCE records to new Tip              */
/*
	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.Account_Reference  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		End '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPPRI char(15)', @NewTip=@NewTip, @a_TIPPRI=@a_TIPPRI

	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.Account_Reference  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC 
		End '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPSEC char(15)', @NewTip=@NewTip, @a_TIPSEC=@a_TIPSEC

*/	
Next_Record:
 	Update OnlineHistoryWork.dbo.Portal_Combines set CopyFlagCompleted=@DateToday
/* SEB002	where transid=@a_Transid */
/* SEB002 */	where transid=@a_Transid and @DBAvailable='Y'

	Set @a_TIPBREAK = @a_TIPPRI
	fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_Transid
	
end --while @@FETCH_STATUS = 0

Fetch_Error1:
Fetch_Error:
close combine_crsr
deallocate combine_crsr
GO
