
USE [OnlineHistoryWork]
GO

if object_id('dbo.spCombineTipsNew') is not null
	drop procedure dbo.spCombineTipsNew
GO


/****** Object:  StoredProcedure [dbo].[spCombineTipsNew]    Script Date: 11/27/2012 12:53:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/******************************************************************************/
/*                   SQL TO PROCESS  COMBINES                          */
/*                                                                         */
/* BY:  R.Tremblay                                          */
/* DATE: 8/2006                                                               */
/* REVISION: 1                                                                */
/* This creates a new tip. Copies the pri customer to customerdeleted.
 Copies sec customer to customerdeleted. Changes  pri customer tip  to new tip
 Changes all pri affiliat records to new Tip. Changes all pri history records to new Tip.
 Changes all sec affiliat records to new Tip. Changes all sec history records to new Tip.
 Add secondary TIP values (runavailable, runbalance, etc) to NEW TIP 
 Deletes sec customer                                                           

 Added code to call spCombineBeginningBalance
 Added code to combine Bonus tale transactions 10/10/06

 Altered Table by adding OldTipPoints column to track the number of points transferred from the old tip
 Added code to update the points transferred 

 10/3/06 - Added OldTipRank = P = Primary S = Secondary

 Input file must be ordered by PRITIP to check for duplicates. 
 NEW Tables needed 
	COMB_IN
	COMB_err
THE FOLLOWING TABLE NEEDS TO BE CREATED ON PATTON AND RN1
	Comb_TipTracking
BRAEK LOGIC REPEATED AFTER WHILE STATEMENT TO PROCESS LAST RECORD AFTER EOF    BJQ 12/27/2006
WRITE TO COMB_ERR CHANGED TO ADD NAME1 AND NAME2 TO THE OUTPUT REC             BJQ 12/28/2006
                                                                                                 */

/*                     Modified to deal with Portal Combines                      */
/* BY: S.Blanchette                                                               */
/* DATE: 3/27/2007                                                                */
/* REVISION: 1                                                                    */
/* Reads the Portal_Combine table and processes the Combines on Patton            */ 
/*                                                                                */ 
/*                     Modified to check DBAVAILABLE so as not to allow           */
/*    combines if the DB is in monthly process                                    */
/* BY: S.Blanchette                                                               */
/* DATE: 5/25/2007                                                                */
/* REVISION: 2                                                                    */
/* SCAN: SEB002                                                                   */
/* Gets the DBAVAILABLE flag from DBPROCESSINFO table if "Y" process if           */
/* "N" the bypass that combine                                                    */ 
/*                                                                                */ 
/* BY: S.Blanchette                                                               */
/* DATE: 12/4/2007                                                                */
/* REVISION: 3                                                                    */
/* SCAN: SEB003                                                                   */
/* Add code to check for null tip when getting last tip used.  If null get out    */
/*                                                                                */ 
/* BY: S.Blanchette                                                               */
/* DATE: 2/2008                                                                   */
/* REVISION: 4                                                                    */
/* SCAN: SEB004                                                                   */
/* Set lastipnumber to null to prevent previous tip from being used in event of   */
/* error                                                                          */ 
/*                                                                                */ 
/* BY: S.Blanchette                                                               */
/* DATE: 2/2008		                                                         */
/* REVISION: 5                                                                    */
/* SCAN: SEB005                                                                   */
/* Change logic to update copyflag as part of the cursor loop                     */
/*                                                                                */ 
/*                                                                                */ 
/*                                                                                */ 
/* BY: S.BQuinn                                                                   */
/* DATE: 6/2008                                                                   */
/* REVISION: 6                                                                    */
/* SCAN: BJQ006                                                                   */
/* Change logic to Check for existance of tippri and tipsec                       */
/* if they do not exist write to the error table (INCOMPLETECOMBINES)             */
/*                                                                                */ 
/*                                                                                */ 
/* BY: Paul H. Butler                                                             */
/* DATE: 6/2008                                                                   */
/* REVISION: 7                                                                    */
/* SCAN: PHB007                                                                   */
/* Added the DBNameNEXL to logic.  This is used to check if the FI is a TNB       */
/* account.  If it is a TNB account, the table inserts/updates against the        */
/* beginning_balance_table must be done in the TNB database, not the FI database  */
/* BJQ008                                                                         */
/* BY: BQuinn                                                                     */
/* DATE: 9/2008                                                                   */
/* REVISION: 8                                                                    */
/* SCAN: BJQ008                                                                   */
/* Added logic to update .dbo.Tip_DDA_Reference where it exists                   */
/*                                                                                */
/* Date: 01/22/2009 Paul H. Butler                                                */
/* SCAN: PHB009                                                                   */
/* Changed calculation on next tip# used to allow for non-numeric tip numbers     */
/*                                                                                */
/* Date: 03/20/2009 Brian J. Quinn                                                */
/* SCAN: BJQ010                                                                   */
/* Added updates to SelfEnroll (Sarah) ,                                          */ 
/* Main and RBPTLedger Table Tipnumbers (Allen and Paul)                          */
/* and chech for null or zero newtip                                              */
/*                                                                                */
/* Date: 11/19/2012 Chris W. Heit                                                 */
/* SCAN: CWH011                                                                   */
/* Added Logging                                                                  */
/* Added Handling for Central Affliate Table changes                              */
/**********************************************************************************/	
CREATE Procedure [dbo].[spCombineTipsNew]  as

/* CWH011 */	IF object_id('tempdb..#thisAffiliat') IS NOT NULL
/* CWH011 */	BEGIN
/* CWH011 */	   DROP TABLE #thisAffiliat
/* CWH011 */	END
	
/* CWH011 */	
	CREATE TABLE #thisAffiliat 
	(
		ACCTID varchar(25)
		, AcctType varchar(20)
		, TIPNUMBER VARCHAR(15)
		, DATEADDED datetime
		, SECID varchar(10)
		, AcctStatus varchar(1)
		, AcctTypeDesc varchar(50)
		, LastName varchar(40)
		, YTDEarned int
		, CustID varchar(20)	
	)
DECLARE @err INT


/* CWH011 */

--/* SEB003 */  Truncate table combineerror

declare @a_TIPPRI char(15), @a_TIPSEC char(15), @a_TIPBREAK char(15), @a_errmsg char(80), @a_Transid uniqueidentifier, @a_TipPrefix char(3) 
declare @a_namep char(40), @a_namep1 char(40), @a_names char(40), @a_names1 char(40), @a_addrp char(40), @a_addrs char(40)     
declare @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int
declare @DateToday datetime, @DBName char(50), @DBAvailable char(1), @DBNameNexl varchar(50)
declare @NewTip as char(15), @NewTipValue bigint, @DeleteDescription as char(40)
declare @Old_TipPri nvarchar(15)
DECLARE @CUSTOMER_FOUND_PRI CHAR(1)
DECLARE @CUSTOMER_FOUND_SEC CHAR(1)
declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)

/* CWH011 */	DECLARE @Process VARCHAR(255) = 'spCombineTipsNew'
/* CWH011 */	DECLARE @Msg VARCHAR(MAX) = ''
/* CWH011 */	DECLARE @a_TIPFIRST VARCHAR(3)

/* CWH011 */	DELETE FROM RewardsNow.dbo.BatchDebugLog WHERE dim_batchdebuglog_process = @Process

/* CWH011 */	SET @Msg = 'Begin Process'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)

SET @DateToday = convert(smalldatetime,convert(varchar(10),GetDate(),101))
SET @a_TIPBREAK = '0'
set @Old_TipPri = '0'
/******************************************************************************/	
/*  Delete null tips                                */

/* CWH011 */	SET @Msg = '--->Delete Null Tips'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)

delete from OnlineHistoryWork.dbo.Portal_Combines where tip_pri is null
delete from OnlineHistoryWork.dbo.Portal_Combines where tip_sec is null

/******************************************************************************/	
/*  Delete tips  from Comb_IN if there is an error message               */
--delete from comb_in where Len( RTrim( errmsg) ) > 0 

/******************************************************************************/	
/*  DECLARE CURSOR FOR PROCESSING COMB_IN TABLE                               */

/* CWH011 */	SET @Msg = '--->Build Cursor'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)

declare combine_crsr  cursor  FAST_FORWARD for 
	select TIP_PRI, TIP_SEC, Transid
	from OnlineHistoryWork.dbo.Portal_Combines
	where CopyFlagCompleted is null 
	order by TIP_PRI, TIP_SEC

open combine_crsr

fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_Transid

/******************************************************************************/	
/* MAIN PROCESSING                                                */
/******************************************************************************/	
if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin --Top of loop	

/* CWH011 */	
/* CWH011 */	DELETE FROM #thisAffiliat

/* CWH011 */	SET @Msg = '--->--->Combining Primary: ' + @a_TIPPRI + ' AND Secondary: ' + @a_TIPSEC + ' (' + CONVERT(VARCHAR(64), @a_Transid) + ')'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)
/* CWH011 */	

--print @a_TIPPRI

	IF @a_TIPPRI <> @Old_TipPri
	   set @NewTip = '0'
	
	set @a_TipPrefix=left(@a_TIPPRI,3)

	/* Check for new primary tip. If new then create a new tip else use the same "new" tip */
	IF @a_TIPPRI <> @a_TIPBREAK 
	Begin
		if not exists(SELECT * from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@a_TipPrefix)
		Begin
/* SEB002 */		set @DBAvailable = 'N'			
				Goto Next_Record
		End
/* SEB002 */	else
/* SEB002 */		set @DBAvailable=(SELECT  DBAvailable from RewardsNOW.dbo.DBProcessInfo
/* SEB002 */					where DBNumber=@a_TipPrefix)

/* SEB002 */		if @DBAvailable<>'Y'
/* SEB002 */		Begin
/* SEB002 */			Goto Next_Record
/* SEB002 */		End
			else
				Begin
/* PHB007 */			--set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
/* PHB007 */			--	where DBNumber=@a_TipPrefix)

/* PHB007 */			select @DBName = ltrim(rtrim(DBNamePatton)), @dbnameNEXL = ltrim(rtrim(DBNameNEXL))
/* PHB007 */			from rewardsnow.dbo.dbprocessinfo
/* PHB007 */			where dbnumber = @a_TipPrefix

				End		

		/*    Create new tip          */
	declare @LastTipUsed char(15)
	set @lasttipused=null /* SEB004 */
	exec rewardsnow.dbo.spGetLastTipNumberUsed @a_TipPrefix, @LastTipUsed output
	select @LastTipUsed as LastTipUsed

--print 'last tip'
--print @LastTipUsed

/* Begin code add SEB003           */
	if @LastTipUsed is null 	   
	Begin
		insert into combineerror (Error)
		values ('ERROR Last tipnumber used')
		
/* CWH011 */	
/* CWH011 */	SET @Msg = '--->--->--->ERROR: Cannot find LastTipnumberUsed'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)
/* CWH011 */	

		goto Fetch_Error1
	end
/* End code add SEB003             */

--	set @NewTipValue = cast(@LastTipUsed as bigint) + 1  

--print 'Old Tip'
--print @LastTipUsed
--print 'New Tip'
--print @NewTipValue 

		/*  Make sure the tipnumber is 15 character long by adding zeros to the begining of the Tip*/
		/* clients with tip prefix starting zeros will have max tips less than 15 characters */

/* 	Code replaced with Code Sarah recreated to pad with leading zeros where approiate  BJQ 12/2006 */
/*		set @NewTipValue = replicate('0',15 - Len( RTrim( @NewTipValue ) ) ) +  @NewTipValue */
/*		set @NewTip = Cast(@NewTipValue as char)                                              */ 

--       		 set @NewTip = Cast(@NewTipValue as char)
--           	 set @NewTip = replicate('0',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip 
/* PHB009 */ 	 set @NewTip = left(@LastTipUsed,3) +  right('000000000000' + cast(cast(right(@LastTipUsed,12)as bigint) + 1 as varchar(12)), 12)
--print 'New Tip2'
--print @NewTipValue 


/* SCAN: BJQ010 */
	if @NewTip is null or
	   @NewTip = '0'
	Begin
		insert into combineerror (Error)
		values ('ERROR assiging @NewTip ')		
		
/* CWH011 */	SET @Msg = '--->--->---> ERROR: Cannot assign @NewTip'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)


		goto Fetch_Error1
	end
/* SCAN: BJQ010 */

/* CWH011 */		SET @Msg = '--->--->---> NewTip: ' + @NewTip
/* CWH011 */		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */		VALUES (@Process, @Msg)

/* SCAN: BJQ006                                                                   */
/* Begin Code scan006     BJQ 06/24/08                                                         */

/* BJQ006 */	/* CHECK TO DETERMINE IF THE PRI_TIP AND SEC_TIP EXIST BEFORE PROCEEDING                            */
/* BJQ006 */	/* IF THE PRI OR SEC DOES NOT EXIST WRITE TO THE NEW ERROR TABLE (INCOMPLETECOMBINES) AND           */
/* BJQ006 */	/* PROCEED TO THE NEXT RECORD / THIS CONDITION WILL NOT CAUSE A JOB FAILURE                         */


/* BJQ006 */	/* CHECK TIP_PRI */

/* BJQ006 */	set @CUSTOMER_FOUND_PRI = 'N'

/* BJQ006 */	set @SQLSelect=N'select	@CUSTOMER_FOUND_PRI = ''Y'' from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber = @a_TIPPRI'
/* BJQ006 */	exec sp_executesql @SQLSelect, N' @CUSTOMER_FOUND_PRI CHAR(1)OUTPUT, @a_TIPPRI char(15)', @CUSTOMER_FOUND_PRI = @CUSTOMER_FOUND_PRI output, @a_TIPPRI=@a_TIPPRI

/* BJQ006 */	if @CUSTOMER_FOUND_PRI <> 'Y' 
/* BJQ006 */	 BEGIN

/* CWH011 */	SET @Msg = '--->--->---> ERROR: Primary TIP not found: (' + @a_TIPPRI + ')'
/* CWH011 */	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
/* CWH011 */	VALUES (@Process, @Msg)
				
/* CWH011 */	INSERT INTO INCOMPLETECOMBINES (TIPPRI, TIPSEC, TRANSID, MESSAGE, DTEPROCESSED)
/* CWH011 */	VALUES (@a_TIPPRI, @a_TIPSEC, @a_Transid, 'ACCOUNTS NOT COMBINED BAD TIPPRI', @DateToday)
/* CWH011 */
				
/* CWH011 */	               --NO NEED FOR DYNAMIC SQL HERE
--CWH011                            
/* BJQ006 */--CWH011	   set @SQLInsert=N'INSERT INTO INCOMPLETECOMBINES (TIPPRI, TIPSEC, TRANSID, MESSAGE, DTEPROCESSED) 
/* BJQ006 */--CWH011                        values ( @a_TIPPRI , @a_TIPSEC , @a_Transid   ,@MESSAGE, @DATE )'
/* BJQ006 */--CWH011	   exec sp_executesql @SQLInsert, N'@a_TIPPRI nchar(15), @a_TIPSEC nchar(15), @a_Transid uniqueidentifier, @MESSAGE VARCHAR(50), @Date DATETIME',
/* BJQ006 */--CWH011					@a_TIPPRI=@a_TIPPRI, @a_TIPSEC=@a_TIPSEC, @a_Transid=@a_Transid, @MESSAGE='ACCOUNTS NOT COMBINED BAD TIPPRI', @Date=@DateToday 

/* BJQ006 */	   Goto Next_Record

/* BJQ006 */	 END

/* BJQ006 *//* CHECK TIP_SEC */

/* BJQ006 */	set @CUSTOMER_FOUND_SEC = 'N'

/* BJQ006 */	set @SQLSelect=N'select	@CUSTOMER_FOUND_SEC = ''Y'' from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber = @a_TIPSEC'
/* BJQ006 */	exec sp_executesql @SQLSelect, N' @CUSTOMER_FOUND_SEC CHAR(1)OUTPUT, @a_TIPsec char(15)', @CUSTOMER_FOUND_SEC = @CUSTOMER_FOUND_sec output, @a_TIPsec=@a_TIPsec

/* BJQ006 */	IF @CUSTOMER_FOUND_SEC <> 'y'
/* BJQ006 */	 BEGIN

				--CWH011
				SET @Msg = '--->--->---> ERROR: Secondary TIP not found: (' + @a_TIPSEC + ')'
				INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
				VALUES (@Process, @Msg)
				
				INSERT INTO INCOMPLETECOMBINES (TIPPRI, TIPSEC, TRANSID, MESSAGE, DTEPROCESSED)
				VALUES (@a_TIPPRI, @a_TIPSEC, @a_Transid, 'ACCOUNTS NOT COMBINED BAD TIPSEC', @DateToday)
				--CWH011
				
--CWH011                            NO NEED FOR DYNAMIC SQL HERE
--CWH011                            



/* BJQ006 */--CWH011	   set @SQLInsert=N'INSERT INTO INCOMPLETECOMBINES (TIPPRI, TIPSEC, TRANSID, MESSAGE, DTEPROCESSED) 
/* BJQ006 */--CWH011                        values ( @a_TIPPRI , @a_TIPSEC , @a_Transid   ,@MESSAGE, @DATE )'
/* BJQ006 */--CWH011	   exec sp_executesql @SQLInsert, N'@a_TIPPRI nchar(15), @a_TIPSEC nchar(15), @a_Transid uniqueidentifier, @MESSAGE VARCHAR(50), @Date DATETIME',
/* BJQ006 */--CWH011					@a_TIPPRI=@a_TIPPRI, @a_TIPSEC=@a_TIPSEC, @a_Transid=@a_Transid, @MESSAGE='ACCOUNTS NOT COMBINED BAD TIPSEC', @Date=@DateToday 
/* BJQ006 */	   Goto Next_Record

/* BJQ006 */	 END

/* End Code scan006     BJQ 06/24/08                                                        */

		/******************************************************************************/	
		/*  copy PRI customer to customerdeleted set description to new tipnumber     */
		/* IF the PRI tip is not repeated */
 
 
		--CWH011
		SET @Msg = '--->--->---> Adding Primary Customer Record to Deleted Table'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@Process, @Msg)
		--CWH011
 
		set @DeleteDescription = 'Pri Combined to '  + @NewTip

		set @SQLInsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.CustomerDeleted
			(
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
			)
	       		SELECT 
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
			FROM ' + QuoteName(@DBName) + N' .dbo.Customer where tipnumber = @a_TIPPRI'
		exec sp_executesql @SQLInsert, N'@a_TIPPRI char(15), @DeleteDescription as char(40), @DateToday datetime', @a_TIPPRI=@a_TIPPRI, @DeleteDescription=@DeleteDescription, @DateToday=@DateToday 

		/******************************************************************************/	
		/* Add Get PRI tip values from Customer  */
 		set @SQLSelect=N'select	@a_RunAvailable =RunAvailable, @a_RunRedeemed=RunRedeemed from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber = @a_TIPPRI'
		exec sp_executesql @SQLSelect, N'@a_RunAvailable int output, @a_RunBalance int output, @a_RunRedeemed int output, @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @a_RunAvailable=@a_RunAvailable output, @a_RunBalance=@a_RunBalance output, @a_RunRedeemed=@a_RunRedeemed output

		/*  Insert record into Comb_TipTracking table  */
 		Insert into OnlineHistoryWork.dbo.New_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank, Transid) Values (@NewTip, @a_TIPPRI, @DateToday, @a_RunAvailable, @a_RunRedeemed,'P', @a_Transid )

		/******************************************************************************/	
		/*  Output Last Tipnumber Used    */
		exec RewardsNOW.dbo.spPutLastTipNumberUsed @a_TipPrefix, @NewTip

	End --@a_TIPPRI <> @a_TIPBREAK 


	/******************************************************************************/	
	/*  copy SEC customer to customerdeleted set description to new tipnumber         */
	--CWH011
	SET @Msg = '--->--->---> Adding Secondary Customer Record to Deleted Table'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
	--CWH011
 
	set @DeleteDescription = 'Sec Combined to '  + @NewTip
 
	set @SQLInsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.CustomerDeleted
			(
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
			)
	       		SELECT 
			TIPNUMBER,  
			 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
			 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
			 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
			 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
			 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
			 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 	 
			 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
			 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
			FROM ' + QuoteName(@DBName) + N' .dbo.Customer where tipnumber = @a_TIPSEC'
	exec sp_executesql @SQLInsert, N'@a_TIPSEC char(15), @DeleteDescription as char(40), @DateToday datetime', @a_TIPSEC=@a_TIPSEC, @DeleteDescription=@DeleteDescription, @DateToday= @DateToday 

	/******************************************************************************/	
	/* Add Get SEC  tip values from Customer  */
 		set @SQLSelect=N'select	@a_RunAvailable =RunAvailable, @a_RunRedeemed=RunRedeemed from ' + QuoteName(@DBName) + N' .dbo.customer where tipnumber = @a_TIPSEC'
		exec sp_executesql @SQLSelect, N'@a_RunAvailable int output, @a_RunBalance int output, @a_RunRedeemed int output, @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @a_RunAvailable=@a_RunAvailable output, @a_RunBalance=@a_RunBalance output, @a_RunRedeemed=@a_RunRedeemed output
 
	/******************************************************************************/	
	/*  Insert record into Comb_TipTracking table  */
 	Insert into OnlineHistoryWork.dbo.New_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank, Transid) Values (@NewTip, @a_TIPSEC, @DateToday, @a_RunAvailable, @a_RunRedeemed,'S', @a_Transid )

	--CWH011
	SET @Msg = '--->--->---> Changing PRI tipnumber in customer to NEWTIP'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
	--CWH011

	/******************************************************************************/	
	/*   change  PRI tip in customer to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK
	   begin
	   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
	   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 
		SET @Old_TipPri = @a_TIPPRI
 	   end 

	/* OLD_TIPPRI ADDED for check of next record in Fetch Statement. If The nhext Tipnumber to be combined */
        /* equals a_TIPPRI the next time thru thw while statement the program will not have to select for the */
	/* Customer Record because it has just been processed   BJQ 12/2006                */


/* 

	CWH011 - If TIPFIRST is part of the Central Process, the Affiliat table keying will make the 'regular' process
	fail.  Instead of updating Affilit entries, new unique entries for the new tip should be added to the Affiliat 
	table after the old entries have been deleted.

*/


--CWH011
SET @Msg = '--->--->---> Processing Affiliat Data'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
VALUES (@Process, @Msg)
--CWH011

--CWH011
IF EXISTS (SELECT sid_dbprocessinfo_dbnumber FROM RewardsNow.dbo.RNICustomerLoadSource WHERE sid_dbprocessinfo_dbnumber = @a_TipPrefix)
BEGIN

	--CWH011
	SET @Msg = '--->--->---> Affiliat for Central Processing'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
	--CWH011

	DECLARE @sql NVARCHAR(MAX)

	
	--CWH011

	SET @sql = REPLACE(REPLACE(REPLACE(
	'
		INSERT INTO #thisAffiliat (ACCTID, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
		SELECT ACCTID, AcctType, MIN(dateadded), min(secid), min(ACCTSTATUS), Min(AcctTypeDesc), ISNULL(MIN(LastName), ''''), SUM(YTDEarned), MIN(custid)
		FROM [<DBNAME>].dbo.AFFILIAT
		WHERE TIPNUMBER IN (''<PRIMARYTIP>'', ''<SECONDARYTIP>'')
		GROUP BY ACCTID, ACCTTYPE
	'
	, '<DBNAME>', @DBName)
	, '<PRIMARYTIP>', @a_TIPPRI)
	, '<SECONDARYTIP>', @a_TIPSEC)
	
	EXECUTE @err = sp_executesql @sql
--	SET @err = @@ERROR

	IF @err <> 0 
	BEGIN
		SET @Msg = '--->--->---> <---<---<---  ERROR IN PROCESSING AFFILIAT RECORDS FOR THIS COMBINE!!!!!!!'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@Process, @Msg)
	END

	

	SET @Msg = '--->--->---> Insert and Group Temp Records: ' + @sql
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	SET @Msg = '--->--->---> Adding New Tip to Temp Records'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	UPDATE #thisAffiliat SET TIPNUMBER = @NewTip

	--
	-- Remove rows from Affilat.  These are going to be replaced by the contents of the #thisAffiliat temp table
	SET @sql = REPLACE(REPLACE(REPLACE(
	'	
		DELETE FROM [<DBNAME>].dbo.AFFILIAT WHERE TIPNUMBER IN (''<PRIMARYTIP>'', ''<SECONDARYTIP>'')
	'
	, '<DBNAME>', @DBName)
	, '<PRIMARYTIP>', @a_TIPPRI)
	, '<SECONDARYTIP>', @a_TIPSEC)

	--CWH011
	SET @Msg = '--->--->---> Removing Source Records From Affiliat'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
	--CWH011
	
	EXEC sp_executesql @sql
	if @@rowcount = 0
	BEGIN
		SET @Msg = '--->--->---> WARNING!!  No rows deleted in AFFILIAT from the old PRI/SEC tipnumbers.  Process continues....'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@Process, @Msg)
	END


	set @sql = REPLACE(
	'
	INSERT INTO [<DBNAME>].dbo.AFFILIAT (ACCTID, AcctType, tipnumber, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	SELECT ACCTID, AcctType, TIPNUMBER, dateadded, secid, ACCTSTATUS, AcctTypeDesc, LastName, YTDEarned, custid
	FROM #thisAffiliat
	'
	, '<DBNAME>', @DBName)

	--CWH011
	SET @Msg = '--->--->---> Inserting Temp Records into Affiliat: ' + @sql
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
	--CWH011

	EXECUTE  @err = sp_executesql @sql
--	SET @err = @@ERROR
	
	IF @err <> 0 
	BEGIN
		SET @Msg = '--->--->---> <---<---<---  ERROR IN INSERTING THE NEW REPLACEMENT AFFILIAT RECORDS FOR THIS COMBINE!!!!!!!'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		VALUES (@Process, @Msg)
	END
END
ELSE
BEGIN	
--CWH011
	--CWH011
	/******************************************************************************/	
	/*         Change all PRI affiliat records to new Tip              */

	SET @sql = REPLACE(REPLACE(REPLACE(
	'
	UPDATE [<DBNAME>].dbo.AFFILIAT 
	SET TIPNUMBER = RewardsNow.dbo.ufn_GetCurrentTip(TIPNUMBER)
	WHERE TIPNUMBER IN (''<PRIMARYTIP>'', ''<SECONDARYTIP>'')
	'
	, '<DBNAME>', @DBName)
	, '<PRIMARYTIP>', @a_TIPPRI)
	, '<SECONDARYTIP>', @a_TIPSEC)
	

	SET @Msg = '--->--->---> Affiliat for Legacy FIs: ' + @sql
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
	--CWH011

	EXEC sp_executesql @sql

--CWH011	IF @a_TIPPRI <> @a_TIPBREAK  
--CWH011	   begin
--CWH011	   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.AFFILIAT set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
--CWH011	   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
--CWH011	end 
 
	/******************************************************************************/	
	/*         Change all SEC affiliat records to new Tip              */
--CWH011   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.AFFILIAT set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC'
--CWH011   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @NewTip=@NewTip


END
	/******************************************************************************/	
	/*       Change all PRI history records to new Tip          */


	SET @sql = REPLACE(REPLACE(REPLACE(
	'
	UPDATE [<DBNAME>].dbo.HISTORY
	SET TIPNUMBER = REWARDSNOW.dbo.ufn_GetCurrentTip(TIPNUMBER)
	WHERE TIPNUMBER IN (''<PRIMARYTIP>'', ''<SECONDARYTIP>'')
	'
	, '<DBNAME>', @DBName)
	, '<PRIMARYTIP>', @a_TIPPRI)
	, '<SECONDARYTIP>', @a_TIPSEC)

	SET @Msg = '--->--->---> History for Legacy FIs: ' + @sql
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	EXEC sp_executesql @sql
	--CWH011

	/******************************************************************************/	
	/*       Change all SEC history records to new Tip          */

/* CWH011

	IF @a_TIPPRI <> @a_TIPBREAK  
	   begin
	   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N' .dbo.HISTORY set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'
	  	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 	   end 

   	set @SQLUpdate=N'Update ' + QuoteName(@DBName) + N'.dbo.HISTORY set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC'
   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @NewTip=@NewTip
*/


	/******************************************************************************/	
	/* Get and Add SEC tip values to Customer NEW TIP */
	SET @Msg = '--->--->---> Add SEC values to New TIP'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	set @SQLSelect=N'select 
		@a_RunAvailable =RunAvailable, 
		@a_RunBalance=RunBalance, 
		@a_RunRedeemed=RunRedeemed
		from ' + QuoteName(@DBName) + N'.dbo.customer where tipnumber = @a_TIPSEC'
	exec sp_executesql @SQLSelect, N'@a_RunAvailable int output, @a_RunBalance int output, @a_RunRedeemed int output, @a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC, @a_RunAvailable=@a_RunAvailable output, @a_RunBalance=@a_RunBalance output, @a_RunRedeemed=@a_RunRedeemed output
 
 	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.customer	
		set RunAvailable = RunAvailable + @a_RunAvailable, 
	     	RunBalance=RunBalance + @a_RunBalance, 
 	     	RunRedeemed=RunRedeemed + @a_RunRedeemed  
		where tipnumber = @NEWTIP'
   	exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int ', @a_RunAvailable=@a_RunAvailable, @a_RunBalance=@a_RunBalance, @a_RunRedeemed=@a_RunRedeemed, @NewTip=@NewTip

	/******************************************************************************/	
	/*    change  PRI tip in BEGINNING_BALANCE_TABLE to new tip number  (this avoids an insert)      */
	SET @Msg = '--->--->---> Update Beginning_Balance_Table'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)
 	
 	IF @a_TIPPRI <> @a_TIPBREAK  
		begin
	  		set @SQLUpdate=N'Update ' + QuoteName(@DBName) + 
						N'.dbo.Beginning_Balance_Table set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI'

	   		exec sp_executesql @SQLUpdate, N'@NewTip char(15), @a_TIPPRI char(15)', @a_TIPPRI=@a_TIPPRI, @NewTip=@NewTip
 	   	end 
	/******************************************************************************/	
	/* Add SEC tip values to Beginning_Balance_Table NEW TIP for each month */
	exec spCombineBeginningBalance @NewTip, @a_TIPSEC

	/******************************************************************************/	
	/* Delete SEC Beginning_Balance_Table                  */

	set @SQLDelete=N'Delete ' + QuoteName(@DBName) + N' .dbo.Beginning_Balance_Table where TIPNUMBER = @a_TIPSEC'

	exec sp_executesql @SQLDelete, N'@a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC
 	   	
	/******************************************************************************/	
	/* Delete SEC Customer                  */
	SET @Msg = '--->--->---> Delete SEC Customer'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

 	set @SQLDelete=N'Delete ' + QuoteName(@DBName) + N' .dbo.CUSTOMER where TIPNUMBER = @a_TIPSEC'
	exec sp_executesql @SQLDelete, N'@a_TIPSEC char(15)', @a_TIPSEC=@a_TIPSEC

	/******************************************************************************/	
	/*       Change all ONLHISTORY records to new Tip          */
	SET @Msg = '--->--->---> Change ONLHistory'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	UPDATE OnlineHistoryWork.dbo.OnlHistory
	SET TIPNUMBER = @NewTip
	WHERE (TipNumber = @a_TIPPRI AND TipNumber <> @a_TIPBREAK)
		OR (TipNumber = @a_TIPSEC)

--CWH011	IF @a_TIPPRI <> @a_TIPBREAK  
--CWH011	   begin
--CWH011	   	Update OnlineHistoryWork.dbo.OnlHistory set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI
--CWH011	   end 

--CWH011   	Update OnlineHistoryWork.dbo.OnlHistory set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC


	/******************************************************************************/	
	/*         Change all PRI & SEC BONUS records to new Tip              */

	SET @Msg = '--->--->---> Update OneTimeBonuses'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)


	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Onetimebonuses'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.OneTimeBonuses  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		End '
	exec sp_executesql @SQLif, N'@NewTip char(15), @a_TIPPRI char(15)', @NewTip=@NewTip, @a_TIPPRI=@a_TIPPRI

	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Onetimebonuses'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.OneTimeBonuses  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC 
		End '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPSEC char(15)', @NewTip=@NewTip, @a_TIPSEC=@a_TIPSEC


	/******************************************************************************/	
	/*         Change all PRI & SEC ACCOUNT_REFERENCE records to new Tip              */
	SET @Msg = '--->--->---> Change Account_Reference'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.Account_Reference  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		End '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPPRI char(15)', @NewTip=@NewTip, @a_TIPPRI=@a_TIPPRI



	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
		Begin
			Update ' + QuoteName(@DBName) + N' .dbo.Account_Reference  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC 
		End '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPSEC char(15)', @NewTip=@NewTip, @a_TIPSEC=@a_TIPSEC


	/******************************************************************************/	
	/*         Change all PRI & SEC Tip_DDA_Reference records to new Tip              */

	SET @Msg = '--->--->---> Change Tip_DDA_Reference'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

--	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where id = object_id(N''[dbo].Tip_DDA_Reference'') and OBJECTPROPERTY(id, N''IsUserTable'') = ''1'') 
	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Tip_DDA_Reference'') 
	BEGIN
		Update ' + QuoteName(@DBName) + N' .dbo.Tip_DDA_Reference  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		Update ' + QuoteName(@DBName) + N' .dbo.Tip_DDA_Reference  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC
	END '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPPRI char(15), @a_TIPSEC char(15)', @NewTip=@NewTip, @a_TIPPRI=@a_TIPPRI, @a_TIPSEC = @a_TIPSEC
	
/******************************************** Begin SelfEnroll Table ****************************************************************/
/* BJQ010 */
	SET @Msg = '--->--->---> Change SelfEnroll'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

set @SQLUpdate=N'if exists(select * from '+ QuoteName(rtrim(@DBName)) + N'.dbo.sysobjects where xtype=''u'' and name = ''selfenroll'')
		Begin
			Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.selfenroll 
			set dim_selfenroll_tipnumber = c.NewTip 
			from '+ QuoteName(rtrim(@DBName)) + N'.dbo.selfenroll as h, OnlineHistoryWork.dbo.New_tiptracking as c 
			where c.oldtip = h.dim_selfenroll_tipnumber and c.OldTipRank =''P''
		End '

exec sp_executesql @SQLUpdate

set @SQLUpdate=N'if exists(select * from '+ QuoteName(rtrim(@DBName)) + N'.dbo.sysobjects where xtype=''u'' and name = ''selfenroll'')
		Begin
			Update '+ QuoteName(rtrim(@DBName)) + N'.dbo.selfenroll 
			set dim_selfenroll_tipnumber = c.NewTip 
			from '+ QuoteName(rtrim(@DBName)) + N'.dbo.selfenroll as h, OnlineHistoryWork.dbo.New_tiptracking as c 
			where c.oldtip = h.dim_selfenroll_tipnumber and c.OldTipRank = ''S''
		End '

exec sp_executesql @SQLUpdate
/******************************************** End SelfEnroll Table  ********************************************************************/
/******************************************** Begin Main Table Update ******************************************************************/

	SET @Msg = '--->--->---> Change Main'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)

	set @SQLIf=N'if exists(select * from ' + QuoteName(@DBName) + N' .dbo.sysobjects where xtype=''u'' and name = ''Main'') 
	BEGIN
		Update ' + QuoteName(@DBName) + N' .dbo.Main  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		Update ' + QuoteName(@DBName) + N' .dbo.Main  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC
	END '
	exec sp_executesql @SQLIf, N'@NewTip char(15), @a_TIPPRI char(15), @a_TIPSEC char(15)', @NewTip=@NewTip, @a_TIPPRI=@a_TIPPRI, @a_TIPSEC = @a_TIPSEC


/******************************************** End Main Table Update ******************************************************************/	

/******************************************** Begin RBPTLedger Table Update ******************************************************************/
--	 CALL TO Rewardsnow.dbo.spRBPTCombineTips Creates New Table entry by SUPPLYING OLDTIPPRI OLDTIPSEC AND NEWTIP

--exec rewardsnow.dbo.spRBPTCombineTips  @a_TIPPRI char(15), @a_TIPSEC char(15), @NewTip=@NewTip


/******************************************** End RBPTLedger Table Update ******************************************************************/	
/* END BJQ010  */


	SET @Msg = '--->--->---> Update Portal Combines'
	INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	VALUES (@Process, @Msg)


/* SEB005 */ 	Update OnlineHistoryWork.dbo.Portal_Combines set CopyFlagCompleted=@DateToday
/* SEB005 */	where transid=@a_Transid 



Next_Record:
/* seb005 	Update OnlineHistoryWork.dbo.Portal_Combines set CopyFlagCompleted=@DateToday */
/* SEB002-5	where transid=@a_Transid */
/* SEB002-5 	where transid=@a_Transid and @DBAvailable='Y'         */

	Set @a_TIPBREAK = @a_TIPPRI
	fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_Transid
	
end --while @@FETCH_STATUS = 0

Fetch_Error1:
Fetch_Error:

--CWH011
SET @Msg = '--->ERROR IN PROCESS (Fetch Error)'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
VALUES (@Process, @Msg)
--CWH011


close combine_crsr
deallocate combine_crsr



GO


