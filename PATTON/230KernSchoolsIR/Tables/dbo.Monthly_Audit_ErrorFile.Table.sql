USE [230KernSchoolsIR]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 02/10/2011 15:48:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
-- RDT20121003 -- added PointsExpire Column
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[DBPointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsBonusMN] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[PointsExpire] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_DBPointsPurchasedDB]  DEFAULT (0) FOR [DBPointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonus]  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_Currentend]  DEFAULT (0) FOR [Currentend]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsExpire]  DEFAULT (0) FOR [PointsExpire]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Monthly_Audit_ErrorFile_PointsBonusMN]  DEFAULT (0) FOR [PointsBonusMN]
GO
