USE [230KernSchoolsIR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Input_Customer]    Script Date: 07/08/2011 16:39:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Customer]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Customer]
GO

/****** Object:  Table [dbo].[Input_Customer]    Script Date: 09/23/2009 15:55:10 ******/

CREATE TABLE [dbo].[Input_Customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[SSN] [varchar](10) NULL,
	[AcctNumber] [varchar](20) NULL,
	[TrailerNumber] [varchar](2) NULL,
	[AcctName1] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[HomePhone] [varchar](15) NULL,
	[WorkPhone] [varchar](15) NULL,
	[AccountInd] [varchar](1) NULL,
	[PurchaseCnt] [decimal](9, 0) NULL,
	[NetPurchases] [decimal](18, 2) NULL,
	[Tipnumber] [varchar](50) NULL,
	[AcctName2] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[LastName] [varchar](50) NULL,
	[AcctID] [varchar](20) NULL,
	[MICR] [varchar] (25) null
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
