USE [230KernSchoolsIR]
GO
/****** Object:  Table [dbo].[Summary]    Script Date: 09/23/2009 15:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Summary](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TranDate] [nvarchar](11) NULL,
	[Input_Participants] [decimal](10, 0) NULL,
	[Participants_Processed] [decimal](18, 0) NULL,
	[Input_Transactions] [decimal](18, 0) NULL,
	[Input_Purchases] [decimal](18, 2) NULL,
	[Input_Returns] [decimal](18, 2) NULL,
	[Stmt_Purchases] [decimal](18, 2) NULL,
	[Stmt_Returns] [decimal](18, 2) NULL,
	[AP_Current_Bonus] [numeric](9, 0) NULL,
	[AP_Conversion_Bonus] [numeric](9, 0) NULL,
	[AP_New_Member_Bonus] [numeric](9, 0) NULL,
	[Bonus_Totals] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Input_Participants]  DEFAULT (0) FOR [Input_Participants]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Participants_Processed]  DEFAULT (0) FOR [Participants_Processed]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Input_Transactions]  DEFAULT (0) FOR [Input_Transactions]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Input_Purchases]  DEFAULT (0) FOR [Input_Purchases]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Input_Returns]  DEFAULT (0) FOR [Input_Returns]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Stmt_Purchases]  DEFAULT (0) FOR [Stmt_Purchases]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Stmt_Returns]  DEFAULT (0) FOR [Stmt_Returns]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_AP_Current_Bonus]  DEFAULT (0) FOR [AP_Current_Bonus]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_AP_Conversion_Bonus]  DEFAULT (0) FOR [AP_Conversion_Bonus]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_AP_New_Member_Bonus]  DEFAULT (0) FOR [AP_New_Member_Bonus]
GO
ALTER TABLE [dbo].[Summary] ADD  CONSTRAINT [DF_Summary_Bonus_Totals]  DEFAULT (0) FOR [Bonus_Totals]
GO
