USE [230KernSchoolsIR]
GO

/****** Object:  Table [dbo].[Account_Xref]    Script Date: 05/25/2011 11:12:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Xref]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Xref]
GO

USE [230KernSchoolsIR]
GO

/****** Object:  Table [dbo].[Account_Xref]    Script Date: 05/25/2011 11:12:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Account_Xref](
	[AccountNumber] [varchar](50) NULL,
	[Old_Trailer] [varchar](50) NULL,
	[New_Trailer] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


