USE [230KernSchoolsIR]
GO
/****** Object:  Table [dbo].[Pending_Purge_Accts]    Script Date: 09/23/2009 15:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pending_Purge_Accts](
	[RowID] [decimal](18, 0) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[DateAdded] [varchar](10) NULL,
	[acctname] [varchar](50) NULL,
	[CycleNumber] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Pending_Purge_Accts] ADD  CONSTRAINT [DF_Pending_Purge_Accts_RowID]  DEFAULT (0) FOR [RowID]
GO
