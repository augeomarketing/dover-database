USE [230KernSchoolsIR]
GO
/****** Object:  Table [dbo].[TransStandard_Error]    Script Date: 09/23/2009 15:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard_Error](
	[TIP] [varchar](50) NOT NULL,
	[TranDate] [datetime] NULL,
	[AcctNum] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [varchar](4) NULL,
	[TranAmt] [decimal](18, 0) NULL,
	[TranType] [varchar](20) NULL,
	[Ratio] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
