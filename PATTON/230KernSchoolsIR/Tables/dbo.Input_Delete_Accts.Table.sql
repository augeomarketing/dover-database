USE [230KernSchoolsIR]
GO
/****** Object:  Table [dbo].[Input_Delete_Accts]    Script Date: 09/23/2009 15:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Delete_Accts](
	[RowID] [decimal](18, 0) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[CustID] [varchar](10) NULL,
	[Name] [varchar](50) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
