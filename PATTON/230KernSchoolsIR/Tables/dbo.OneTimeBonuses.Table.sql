USE [230KernSchoolsIR]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 09/23/2009 15:55:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [varchar](16) NOT NULL,
	[AcctInd] [varchar](1) NULL,
	[Cycle] [numeric](1, 0) NOT NULL,
	[TranCnt] [numeric](9, 0) NULL,
	[DateAwarded1] [datetime] NULL,
	[DateAwarded2] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OneTimeBonuses] ADD  CONSTRAINT [DF_OneTimeBonuses_Cycle]  DEFAULT (0) FOR [Cycle]
GO
ALTER TABLE [dbo].[OneTimeBonuses] ADD  CONSTRAINT [DF_OneTimeBonuses_TranCnt]  DEFAULT (0) FOR [TranCnt]
GO
