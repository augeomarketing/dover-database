USE [230KernSchoolsIR]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/21/2010 14:29:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrub]
GO

USE [230KernSchoolsIR]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/21/2010 14:29:57 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/* Comments, Notes, etc. 
RDT 2011/07/07 Added support for MICR
RDT 2011/07/14 removed check for MICR
RDT 2013/01/15 added code to account for one name in the last name field. 

*/
CREATE  PROCEDURE [dbo].[spInputScrub]  @TranDate as datetime
as

Truncate Table  Input_Customer_error

--------------- Input Customer table

update Input_Customer
set acctname1=replace(acctname1,char(39), ' '), address1=replace(address1,char(39), ' '),address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(140), ' '),  address1=replace(address1,char(140), ' '),address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update Input_Customer
set acctname1=replace(acctname1,char(44), ' '),  address1=replace(address1,char(44), ' '),address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update Input_Customer
set acctname1=replace(acctname1,char(46), ' '), address1=replace(address1,char(46), ' '),address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(34), ' '), address1=replace(address1,char(34), ' '),address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update Input_Customer
set acctname1=replace(acctname1,char(35), ' '), address1=replace(address1,char(35), ' '),address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')

update input_customer set lastname = reverse(ACCTNAME1)

update input_customer set lastname = right(lastname,len(lastname) - 3)
where left(lastname,2) = 'rj' or left(lastname,2) = 'rs' or left(lastname,2) = 'VI'

update input_customer set lastname = right(lastname,len(lastname) - 4)
where left(lastname,4) like 'II%'

--- RDT 2013/01/15 Start
-- update input_customer set lastname = reverse(left(lastname,(CHARINDEX(' ', lastname) -1))) 
-- update input_customer set lastname = acctname1 where lastname is null 
update input_customer set lastname = reverse ( lastname )  
	where (CHARINDEX(' ', lastname) )  = 0 
update input_customer set lastname = reverse(left(lastname,(CHARINDEX(' ', lastname) -1)))  
	where (CHARINDEX(' ', lastname) )  > 0 
--- RDT 2013/01/15 End 

update Input_Customer set address4 = rtrim(ltrim(city + ' ' + state + ' ' + zipcode))

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (ssn is null or ssn = ' ') or  
	      (AcctNumber is null or AcctNumber = ' ') or
	      (acctname1 is null or acctname1 = ' ') or
	      (TrailerNumber is null or TrailerNumber = ' ') 
	  -- RDT 2011/07/14   or (MICR is null or MICR = ' ')  

delete from Input_customer 
where (ssn is null or ssn = ' ') or  
      (AcctNumber is null or AcctNumber = ' ') or
      (acctname1 is null or acctname1 = ' ') or
	  (TrailerNumber is null or TrailerNumber = ' ')
	  -- RDT 2011/07/14 or (MICR is null or MICR = ' ')  

update input_customer set acctnumber = replicate('0',10 - len(acctnumber)) + acctnumber where len(acctnumber) < 10 

update input_customer set ssn = replicate('0',9- len(ssn)) + ssn where len(ssn) < 9

update Input_Customer set [AcctID] = AcctNumber + TrailerNumber

insert summary (trandate)
	Select @TranDate
	
update summary set input_participants = (select count(*)from input_customer)
	where rowid = (select max(rowid) from summary)

GO


