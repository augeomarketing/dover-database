USE [230KernSchoolsIR]
GO
/****** Object:  StoredProcedure [dbo].[spLoad_Calculate_Bonuses]    Script Date: 10/05/2009 16:13:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spLoad_Calculate_Bonuses] @processdate datetime
as

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--================================================================================
--  Insertion of Bonus tracking using AccountInd Codes "C" and "E"
--================================================================================
	if @processdate < '12/01/2009'
		Begin
			update dbo.OneTimeBonuses_Stage set cycle = cycle + 1, 
			 trancnt = trancnt + (select purchasecnt from input_customer where
			 dbo.OneTimeBonuses_Stage.acctid = input_customer.acctid)
				where (acctind = 'E' and dateawarded1 is null) or (acctind = 'C' and dateawarded2 is null)

			Insert dbo.OneTimeBonuses_Stage (tipnumber,acctid,acctind,cycle,trancnt)
			select tipnumber,acctid,accountind,'1',purchasecnt from input_customer 
				where accountind in ('C','E') and acctid not in(select acctid from onetimebonuses_stage)

			insert into transstandard (Tip,trandate,acctnum,trancode,trannum,tranamt)
			select tipnumber,@processdate,acctid,'BC','1',500 from dbo.OneTimeBonuses_Stage
				   where acctind = 'C' and cycle = '1' and dateawarded1 is null
						
			update dbo.OneTimeBonuses_Stage set dateawarded1 = @processdate, trancode = 'BC'
					where acctind = 'C' and cycle = '1' and dateawarded1 is null
	
			insert into transstandard (Tip,trandate,acctnum,trancode,trannum,tranamt)
			select tipnumber,@processdate,acctid,'BC','1',500 from dbo.OneTimeBonuses_Stage
				   where acctind = 'C' and (trancnt >= '5' and cycle <= '3') and dateawarded2 is null 
						
			update dbo.OneTimeBonuses_Stage set dateawarded2 = @processdate
					where acctind = 'C' and (trancnt >= '5' and cycle <= '3') and dateawarded2 is null
		
			insert into transstandard (Tip,trandate,acctnum,trancode,trannum,tranamt)
			select tipnumber,@processdate,acctid,'BI','1',1000 from dbo.OneTimeBonuses_Stage
				   where acctind = 'E' and (trancnt >= '5' and cycle <= '3') and dateawarded1 is null 
						
			update dbo.OneTimeBonuses_Stage set dateawarded1 = @processdate, trancode = 'BI'
					where acctind = 'E' and (trancnt >= '5' and cycle <= '3') and dateawarded1 is null 
					
			--delete dbo.OneTimeBonuses_Stage where (trancnt < '5' and cycle >= '3') or trancnt >= '5'
		End

	update summary set ap_new_member_bonus = (select sum(tranamt) from transstandard where trancode = 'BF')
	where rowid = (select max(rowid) from summary)
	
	update summary set ap_current_bonus = (select sum(tranamt) from transstandard where trancode = 'BI')
	where rowid = (select max(rowid) from summary)
	
	update summary set ap_conversion_bonus = (select sum(tranamt) from transstandard where trancode = 'BC')
	where rowid = (select max(rowid) from summary)
	
	update summary set Bonus_Totals = (select sum(tranamt) from transstandard where trancode in ('BC','BI','BF'))
	where rowid = (select max(rowid) from summary)

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
	Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode where t.trantype is null

--Set the Ratio to the Ratio found in the RewardsNow.TranCode table
	Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode where t.ratio is null

 END
 
