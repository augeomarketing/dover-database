USE [230KernSchoolsIR]
GO
/****** Object:  StoredProcedure [dbo].[spSelectPendingDeletes]    Script Date: 11/06/2009 09:44:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================
-- Author:		Dan Foster
-- Create date: 8/26/2009
-- Description:	Select customers for Pending Delete
-- ================================================
ALTER PROCEDURE [dbo].[spSelectPendingDeletes]	@dateadded datetime
as

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

insert into dbo.Pending_Purge_Accts (tipnumber,dateadded,acctname)
		select tipnumber, @dateadded, acctname1 from CUSTOMER_Stage
			where TIPNUMBER not in (select TIPNUMBER from Pending_Purge_Accts)
				and customer_stage.status = 'I'
				
update dbo.Pending_Purge_Accts set CycleNumber = CycleNumber + 1

update customer_stage set status = 'P',statusdescription = 'Pending Deletion'
where tipnumber in(select tipnumber from Pending_Purge_Accts where CycleNumber >= 3)

delete Pending_Purge_Accts where CycleNumber >= 3 or tipnumber = (select tipnumber from customer_stage 
where status = 'A' and tipnumber = Pending_Purge_Accts.tipnumber)


END
