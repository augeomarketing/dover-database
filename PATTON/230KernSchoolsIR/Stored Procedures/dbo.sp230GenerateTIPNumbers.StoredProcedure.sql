USE [230KernSchoolsIR]
GO

/****** Object:  StoredProcedure [dbo].[sp230GenerateTIPNumbers]    Script Date: 06/21/2010 14:27:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp230GenerateTIPNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp230GenerateTIPNumbers]
GO

USE [230KernSchoolsIR]
GO

/****** Object:  StoredProcedure [dbo].[sp230GenerateTIPNumbers]    Script Date: 06/21/2010 14:27:45 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp230GenerateTIPNumbers]
AS 
/****************************************************************************************/
/*	Change 1: One Tipnumber for every member regardless of SSN or Trailer               */
/****************************************************************************************/
update input_customer set Tipnumber = b.tipnumber
from input_customer a join affiliat_stage b on a.AcctID = b.acctid 
where (a.tipnumber is null or a.tipnumber = ' ')

update input_customer set Tipnumber = b.tipnumber
from input_customer a join affiliat_stage b on left(a.AcctID,10) = left(b.acctid,10) 
where (a.tipnumber is null or a.tipnumber = ' ')

Truncate table GenTip

insert into gentip (acctid, tipnumber)
select acctid, tipnumber	
from input_customer where tipnumber is null or tipnumber = ' '

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '230', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 230000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '230', @newnum

update input_customer set Tipnumber = b.tipnumber
from input_customer a join gentip b on a.AcctID = b.acctid 
where (a.tipnumber is null or a.tipnumber = ' ')

GO


