USE [230KernSchoolsIR]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 09/23/2009 15:54:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	

Update Customer_Stage set status = 'I'

Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.ACCTNAME1),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( Input_Customer.ADDRESS2),40)
,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIPcode)) , 40 )
,CITY 		= Input_Customer.CITY
,STATE		= left(Input_Customer.STATE,2)
,ZIPCODE 	= ltrim(Input_Customer.ZIPcode)
,HOMEPHONE 	= Ltrim(Input_Customer.homephone)
,WORKPHONE  = Ltrim(Input_Customer.workphone) 
,STATUS	= 'A'
,MISC2		= ' '
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage
(
	TIPNUMBER, [STATUS], DATEADDED, LASTNAME, tipfirst, TIPLAST,  ACCTNAME1,
	ADDRESS1,  ADDRESS2, ADDRESS4, CITY, [STATE], ZIPCODE , HOMEPHONE, 
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	distinct TIPNUMBER, 'A' as [status], @enddate, left(rtrim(LASTNAME),40), left(TIPNUMBER,3),
	right(rtrim(TIPNUMBER),6), left(rtrim(Input_Customer.ACCTNAME1),40), Left(rtrim(ADDRESS1),40), 
  	Left(rtrim(ADDRESS2),40), Left(rtrim(Address4),40),	CITY, [State], rtrim(ZIPcode),
	Ltrim(Input_Customer.homephone) ,0, 0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null

update summary set participants_processed = (select count(*) from customer_stage)
	where rowid = (select max(rowid) from summary)
GO
