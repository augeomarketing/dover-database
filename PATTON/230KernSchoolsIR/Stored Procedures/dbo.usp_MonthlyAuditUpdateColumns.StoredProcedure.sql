USE [230KernSchoolsIR] 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyAuditUpdateColumns]    Script Date: 01/25/2012 14:33:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyAuditUpdateColumns]') )
DROP PROCEDURE [dbo].[usp_MonthlyAuditUpdateColumns]
GO
-- =================================================
-- Author:		Rich T
-- Description: Update additional columns in Monthly_Statement_File
-- =================================================
CREATE PROCEDURE [dbo].[usp_MonthlyAuditUpdateColumns] 
AS

Update Monthly_Statement_File 
Set 
	Acctname2 = RTRIM(cs.Acctname2) ,
	Address2  =  RTRIM(cs.ADDRESS2 ) ,
	Address3  =  RTRIM(cs.ADDRESS3 ) ,
	City		= RTRIM( cs.City) ,
	State	= RTRIM( cs.State) , 
	Zip		= RTRIM(cs.ZipCode ) 
	From CUSTOMER_Stage cs 
		join Monthly_Statement_File ms 
			on cs.TIPNUMBER = ms.Tipnumber
GO
