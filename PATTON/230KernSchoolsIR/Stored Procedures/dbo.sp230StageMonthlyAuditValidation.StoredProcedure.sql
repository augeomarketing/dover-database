USE [230KernSchoolsIR]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyAuditUpdateColumns]    Script Date: 01/25/2012 14:33:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp230StageMonthlyAuditValidation]') )
DROP PROCEDURE [dbo].[sp230StageMonthlyAuditValidation]
GO

CREATE PROCEDURE [dbo].[sp230StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare 
	@Tipnumber nchar(15),					@pointsBegin numeric(9),				@pointsEnd numeric(9), 
	@pointspurchasedDB numeric(9), 	@pointsBonus numeric(9),				@PointsBonusMN numeric(9), 
	@PointsAdded numeric(9),				@pointsIncreased numeric(9),		@pointsRedeemed numeric(9),		
	@pointsReturnedDB numeric(9),	@pointsSubtracted numeric(9),	@pointsDecreased numeric(9),		
	@PointsExpire numeric(9),				@errmsg varchar(50),					@currentend numeric(9)

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
--declare tip_crsr cursor
--for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, 
--pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased 
--from Monthly_Statement_File

declare tip_crsr cursor
for select 	Tipnumber, pointsBegin,	pointsEnd, pointspurchasedDB, pointsBonus,	PointsBonusMN , PointsAdded, pointsIncreased, 
					pointsRedeemed, pointsReturnedDB,	pointsSubtracted,	pointsDecreased , PointsExpire
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

--fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend,  @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
--@pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased
fetch tip_crsr into 	@Tipnumber, @pointsBegin,	@pointsEnd, @pointspurchasedDB, @pointsBonus,	@PointsBonusMN , @PointsAdded, @pointsIncreased, 
								@pointsRedeemed, @pointsReturnedDB,	@pointsSubtracted,	@pointsDecreased , @PointsExpire 


/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
       			values( @Tipnumber, @pointsBegin,	@pointsEnd, @pointspurchasedDB, @pointsBonus,	@PointsBonusMN ,	@PointsAdded, @pointsIncreased, 
							@pointsRedeemed, @pointsReturnedDB,	@pointsSubtracted,	@pointsDecreased , @PointsExpire, @errmsg, @currentend )
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into 
					@Tipnumber, @pointsBegin,	@pointsEnd, @pointspurchasedDB, @pointsBonus,	@PointsBonusMN ,	@PointsAdded, @pointsIncreased, 
					@pointsRedeemed, @pointsReturnedDB,	@pointsSubtracted,	@pointsDecreased , @PointsExpire 


	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
