USE [230KernSchoolsIR]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 08/04/2010 10:56:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

USE [230KernSchoolsIR]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 08/04/2010 10:56:52 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/**************************************************************************************/
--  Description: Copies data from input_transaction to the Affiliat_stage table   
--  Revisions:Change 1  Adjust code to check input_customer acctid not account_number  
-- RDT 20110711 move MICR to SECID (replaces TrailerNumber)
/**************************************************************************************/

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
--Change 1
insert into AFFILIAT_Stage (ACCTID,TIPNUMBER)
select distinct AcctID,tipnumber from Input_Customer where acctid not in (select acctid from AFFILIAT_Stage)

--Change 1
update  AFS 
	set  accttype = 'Debit',
	 -- RDT 20110711 secid = c.TrailerNumber, 
	LastName	= c.LastName, 
	YTDEarned	= 0, 
	CustID		= c.SSN, 
	DATEADDED	= @MonthEnd 
	from AFFILIAT_Stage AFS	join Input_Customer C on  c.Acctid = afs.acctid 
		where DATEADDED is null or DATEADDED = ' '

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypeDesc = T.AcctTypeDesc
	from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

--Change 1
update AFFILIAT_Stage 
	set AcctStatus = b.AccountInd,
		SECID = b.MICR  -- RDT 20110711 
	from affiliat_stage a join input_customer b on a.acctid = b.Acctid

update affiliat_stage 
	set acctstatus = 'A'
	where AcctStatus is null or AcctStatus = ' '

-- RDT 20110711  ( moved up) update AFFILIAT_Stage set DATEADDED = @MonthEnd where DATEADDED is null or DATEADDED = ' '


GO


