USE [230KernSchoolsIR]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionScrub]    Script Date: 09/23/2009 15:54:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTransactionScrub]
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Insert into transstandard_error 
	select * from transstandard
	where (tip is null or tip = ' ') or
	      (acctnum is null or acctnum = ' ') or
	      (tranamt is null )or
	      (trancode is null or trancode = ' ')
	       
Delete from transstandard
where (tip is null or tip = ' ') or
	      (acctnum is null or acctnum = ' ') or
	      (tranamt is null )or
	      (trancode is null or trancode = ' ')
END
GO
