USE [230KernSchoolsIR]
GO
/****** Object:  StoredProcedure [dbo].[spLoadBeginningBalance]    Script Date: 09/23/2009 15:54:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadBeginningBalance] @MonthBeginDate datetime
AS 

/*Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)  chg 6/15/2006   */
Declare @MonthBucket char(10), @MonthBegin char(2), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)

set @MonthBegin = month(Convert(datetime, @MonthBeginDate) ) + 1

if CONVERT( int , @MonthBegin)='13' 
	begin
		set @monthbegin='1'
	end	

set @MonthBucket='MonthBeg' + @monthbegin

set @SQLUpdate=N'update Beginning_Balance_Table set '+ Quotename(@MonthBucket) + N'= (select pointsend from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber) where exists(select * from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber)'

set @SQLInsert=N'insert into Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') select tipnumber, pointsend from Monthly_Statement_File where not exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert
GO
