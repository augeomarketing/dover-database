USE [230KernSchoolsIR]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 09/23/2009 15:54:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded datetime
AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], TranDate, [AcctNum], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [AcctID], [PurchaseCnt], [NetPurchases]  from Input_Customer

update TransStandard set TranCode = '67'
	where TranAmt > 0

update TransStandard set TranCode = '37'
	where TranAmt < 0

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

--Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

delete from TransStandard where trancode is null

update summary set input_purchases = (select sum(tranamt) from transstandard where trancode = '67')
	where rowid = (select max(rowid) from summary)

update summary set input_returns = (select sum(tranamt) from transstandard where trancode = '37')
	where rowid = (select max(rowid) from summary)

--update transstandard set tranamt = round(round(tranamt,0)/2,0)
update transstandard set tranamt = round(tranamt/2,0)


update summary set stmt_purchases = (select sum(tranamt) from transstandard where trancode = '67')
	where rowid = (select max(rowid) from summary)

update summary set stmt_returns = (select sum(tranamt) from transstandard where trancode = '37')
	where rowid = (select max(rowid) from summary)

update summary set Input_Transactions = (select count(*) from transstandard)
	where rowid = (select max(rowid) from summary)
GO
