USE [102BCM]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 10/01/2009 15:41:12 ******/
DROP TABLE [dbo].[TranType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
