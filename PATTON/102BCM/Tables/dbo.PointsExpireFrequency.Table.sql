USE [102BCM]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 10/01/2009 15:41:12 ******/
DROP TABLE [dbo].[PointsExpireFrequency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
