USE [102BCM]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 10/01/2009 15:41:12 ******/
DROP TABLE [dbo].[Location]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
