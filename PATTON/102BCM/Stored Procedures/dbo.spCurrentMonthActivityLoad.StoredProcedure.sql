USE [102BCM]
GO
/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivityLoad]    Script Date: 10/01/2009 15:41:11 ******/
DROP PROCEDURE [dbo].[spCurrentMonthActivityLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spCurrentMonthActivityLoad] @EndDate varchar(20)
AS

delete from Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints)
select tipnumber, runavailable 
from Customer


/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
