USE [50DCitizensNational]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 09/24/2009 10:27:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
