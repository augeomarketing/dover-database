USE [596RoyalCUConsumer]
GO
/****** Object:  Table [dbo].[transfer_work]    Script Date: 09/25/2009 14:41:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transfer_work](
	[ACCT #] [nvarchar](255) NULL,
	[CONV PTS] [float] NULL,
	[yr2003] [float] NULL,
	[yr2004] [float] NULL,
	[yr2005] [float] NULL,
	[yr2006] [float] NULL,
	[yr2007] [float] NULL,
	[yr2008] [float] NULL
) ON [PRIMARY]
GO
