USE [596RoyalCUConsumer]
GO
/****** Object:  Table [dbo].[Spend Update]    Script Date: 09/25/2009 14:41:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Spend Update](
	[card] [nvarchar](255) NULL,
	[amount] [float] NULL,
	[flag] [nvarchar](255) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
