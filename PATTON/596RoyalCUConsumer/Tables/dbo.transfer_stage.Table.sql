USE [596RoyalCUConsumer]
GO
/****** Object:  Table [dbo].[transfer_stage]    Script Date: 09/25/2009 14:41:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transfer_stage](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[trancount] [int] NULL,
	[points] [numeric](18, 0) NULL,
	[description] [varchar](50) NULL,
	[secid] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [numeric](5, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
