USE [596RoyalCUConsumer]
GO
/****** Object:  Table [dbo].[ExtraRecords]    Script Date: 09/25/2009 14:41:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExtraRecords](
	[Account #] [nvarchar](255) NULL,
	[Converting Points] [float] NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
