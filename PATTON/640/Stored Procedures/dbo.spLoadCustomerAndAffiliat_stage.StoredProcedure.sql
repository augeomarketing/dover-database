USE [640GraniteState]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerAndAffiliat_stage]    Script Date: 11/24/2010 12:38:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerAndAffiliat_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerAndAffiliat_stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage] @TipFirst char(3), @EndDate DateTime 
AS 
declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20)
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CUSTOMER DATA                                         */
/*                                                                            */
/******************************************************************************/
/* Update all of the Customer_Stage records to a status of ''C'' .... As new records are inserted they have a status of A from the Custin table, 
As records are udated, they already have  a status of A in Custin too 
Anything remaining means that this is the first month that they HAVEN''T appeared, their status is already C and they will be added 
to the Customer_Closed table with MonthEndToDelete set to Two months beyond the EndDate passed in
*/
/*************************************/
/* S Blanchette                      */
/* 11/2010                           */
/* SEB001                            */
/* delete from customer closed if    */
/* tip gets back to a status of ''''A''''  */
/*************************************/

--Get the dateToDelete (x months out based on Client.ClosedMonths)
declare @tmpDate datetime,@NumMonths smallint, @DateClosed datetime, @DateToDelete datetime
set @NumMonths =(SELECT ClosedMonths from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst) 
select @tmpDate=dateadd(d,1,@Enddate)
select @tmpDate=dateadd(m,@NumMonths,@tmpDate)
select @DateToDelete=dateadd(d,-1,@tmpDate)   --usually 2 months beyond the Close date
set @DateClosed=@EndDate
Update customer_stage set status=''C''
Update Affiliat_stage set acctstatus=''C''
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/* 
                                                                           */
declare CUSTIN_crsr cursor
for select ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3
from CUSTIN
order by tipnumber, status desc
/*                                                                            */
open CUSTIN_crsr
fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	if (not exists (select tipnumber from customer_stage where tipnumber=@tipnumber) and @tipnumber is not null and left(@tipnumber,1)<>'' '')
		begin
			-- Add to CUSTOMER TABLE
			INSERT INTO Customer_stage (TIPNumber, Runavailable, Runbalance, Runredeemed, Status, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode,  HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3)
			values(@tipnumber, ''0'', ''0'', ''0'', @Status, LEFT(@tipnumber,3), right(@tipnumber,12), @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @ADDRESS1, @ADDRESS2, @ADDRESS4, rtrim(@City), rtrim(@state), rtrim(@zip),  @HomePhone, @WorkPhone, cast(@DateAdded as datetime), @LastName, @Misc1, @Misc2, @Misc3) 
		end
	else
		begin
			-- Update CUSTOMER TABLE
			update customer_stage
			set Status=@Status, AcctName1=@NAMEACCT1, AcctName2=@NAMEACCT2, AcctName3=@NAMEACCT3, AcctName4=@NAMEACCT4, AcctName5=@NAMEACCT5, AcctName6=@NAMEACCT6, Address1=@Address1, Address2=@Address2, Address4=@Address4, City=rtrim(@City), State=rtrim(@STATE), Zipcode=rtrim(@zip), HomePhone=@HomePhone, WorkPhone=@WorkPhone, lastname=@lastname, misc1=@misc1, misc2=@misc2, misc3=@misc3
			where tipnumber=@tipnumber 
		end
	--load affilitat_stage
	if (not exists (select acctid from affiliat_stage where acctid= @ACCT_NUM) and @ACCT_NUM is not null and left(@ACCT_NUM,1)<>'' '')
		begin
			-- Add to AFFILIAT TABLE
			INSERT INTO affiliat_stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
			values(@ACCT_NUM, @tipnumber, ''Debit'', cast(@DateAdded as datetime), ''A'', ''Debit Card'', @lastname, ''0'') 
		end
	else
		begin
			-- Update AFFILIAT TABLE
			update AFFILIAT_stage	
			set 	lastname=@lastname, 
				acctstatus=@status 
			where acctid=@acct_num
		End
	
goto Next_Record
Next_Record:
		fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
end
Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr
/*  removed 03/09-already have a status CODE
update customer
set StatusDescription=(select statusdescription from status where status=customer.status)
*/
--Any Customer_Stage records that STILL have a status=''C'' get added to the Customer_Closed table if they don''t already exist in it
INSERT into Customer_Closed 
	select Tipnumber,@DateClosed,@DateToDelete from Customer_stage where Status=''C'' AND tipnumber not in (select tipnumber from Customer_closed) 
--see if any of the closed customers have transactions for > DateToDelete...if so , bump their DateToDelete value up by a month
Update Customer_Closed set DateToDelete=dateadd(d,-1,dateadd(m,1,dateadd(d,1,DateToDelete))) where tipnumber in (select Tipnumber from History_stage where histdate >=@DateToDelete)

/****************/
/* START SEB001 */
/****************/

delete from Customer_Closed
where TipNumber in (select TipNumber from CUSTOMER_Stage where STATUS=''A'')

/*****************/
/*  END SEB001   */
/*****************/
' 
END
GO
