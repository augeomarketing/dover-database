SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zspReversals] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Reversals                           */

/*                                                                            */
/******************************************************************************/

--update MonthEndDate or the newly imported records 
update wrkReversals set MonthEndDate=@DateIn where MonthEndDate is null



--truncate the totals table
truncate table wrkReversals_Totals


--rollup the records by tip and sum the Reversals. 
Insert Into wrkReversals_Totals (Tipnumber, SumNetReconAmt)
	select Tipnumber, Sum(NetReconAmt) from  wrkReversals where monthEndDate = @DateIn group by tipnumber

--Update the points field
Update wrkReversals_Totals set Points = round((SumNetReconAmt/2),10,0)





--insert the recs into history_stage
insert into history_stage
select tipnumber, ' ', @DateIn, 'IE', 1, Points, 'Reversal', 'NEW ', 1, 0
from wrkReversals_Totals
where Points is not null and tipnumber is not null



--update the customer_stage fields
update customer_Stage
set runavailable=runavailable + b.Points, runbalance=runbalance + b.Points
from customer_stage a, wrkReversals_Totals b          
where a.tipnumber=b.tipnumber and b.Points is not null
GO
