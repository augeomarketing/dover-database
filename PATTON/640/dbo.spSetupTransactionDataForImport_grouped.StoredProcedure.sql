SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport_grouped] @StartDate char(10), @EndDate char(10), @TipFirst char(3), @DebitCreditFlag nvarchar(1)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */
truncate table TransDetailHold_Grouped
truncate table TransDetailHold_Grouped_Orphans_NoTipnumber
truncate table transwork
truncate table transstandard

--get ALL of the records for this Tipfirst from REGARDLESS of 
insert into TransDetailHold_Grouped (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID, TipFirst, OrigDataElem)

	select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID,TipFirst, OrigDataElem
	from COOPWork.dbo.TransDetailHold_Grouped
	where TipFirst=@tipfirst 

update TransDetailHold_Grouped
set TransDetailHold_Grouped.tipnumber=Affiliat_stage.tipnumber
from TransDetailHold_Grouped, Affiliat_stage
where TransDetailHold_Grouped.pan=Affiliat_stage.acctid 
and TransDetailHold_Grouped.tipnumber is null

--CHECK FOR ORPHANS HERE
--insert the records that didn't get a tip and delete from the table
insert into TransDetailHold_Grouped_Orphans_NoTipnumber
	select * from TransDetailHold_Grouped where tipnumber is null
delete TransDetailHold_Grouped where tipnumber is null
--END CHECK FOR ORPHANS HERE


--criteria for select below is 
insert into transwork (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID, OrigDataElem  )
--select * from COOPWork.dbo.TransDetailHold
select  TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID,OrigDataElem
from TransDetailHold_Grouped
where sic<>'6011' and processingcode in ('000000', '002000', '200020', '200040', '500000', '500020', '500010') and (left(msgtype,2) in ('02', '04')) and Trandate>=@StartDate and Trandate<=@Enddate


---CALC SIG DEBIT POINT VALUES
-- Signature Debit
update transwork
set points=ISNULL(ROUND(((amounttran/100)/2), 10, 0),0)
where netid in('MCI', 'VNT') 
update transwork set points=0 where Points is null



---CALC PIN DEBIT POINT VALUES
-- PIN Debit NO AWARD FOR PIN
/*
update transwork
set points=ROUND(((amounttran/100)/4), 10, 0)
where netid not in('MCI', 'VNT') 
--Put to standard transtaction file format purchases.
*/
if @DebitCreditFlag='D'    ---if this is a COOP DEBIT ONLY FI
Begin
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
	select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' 
	from transwork
	where processingcode in ('000000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02') 		        	
	group by tipnumber, Pan
	
-- SEB001

INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' 
	from transwork
	where 
	--processingcode in ('200020', '200040') or left(msgtype,2) in ('04')    --need to add 200000 to processing codes    --old code

processingcode in ('200020', '200040')   
OR (processingcode = '000000' AND  left(msgtype,2) = '04'  AND OrigDataElem='02')
OR (processingcode <>'000000' AND left(msgtype,2) ='04') 
	group by tipnumber, Pan
END
GO
