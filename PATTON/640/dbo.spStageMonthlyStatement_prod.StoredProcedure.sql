SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
*/
/*******************************************************************************/
CREATE PROCEDURE [dbo].[spStageMonthlyStatement_prod]  @StartDateParm char(10), @EndDateParm char(10)
AS 
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)
Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 
set @MonthBegin = month(Convert(datetime, @StartDate) )
/* Load the statement file from the customer table  */
delete from Monthly_Statement_File
insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from Customer
/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File
set pointspurchasedCR =(select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='63' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
and histdate>=@startdate and histdate<=@enddate and trancode='63'  )
/* Load the statmement file  with CREDIT returns            */
update Monthly_Statement_File
set pointsreturnedCR=(select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='33' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='33' )
/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File
set pointspurchasedDB=(select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='67' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='67' )
/* Load the statmement file with DEBIT  returns            */
update Monthly_Statement_File
set pointsreturnedDB=(select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='37' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='37' )
/* Load the statmement file with DEBITbonuses            */
update Monthly_Statement_File
set pointsbonusDB=(select sum(points*ratio) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and ((trancode like 'B%' and trancode <> 'BX')or trancode= 'NW' or (trancode like 'F%' and trancode<>'FJ' ))  )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and ((trancode like 'B%' and trancode <> 'BX')or trancode= 'NW' or (trancode like 'F%' and trancode<>'FJ' )))
/* Load the statmement file with CREDIT bonuses            */
update Monthly_Statement_File
set pointsbonusCR=(select sum(points*ratio) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='FJ'  )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='FJ'  )
/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='IE' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='IE' )
/* Add  DECREASED REDEEMED to adjustments     */
update Monthly_Statement_File
set pointsadded=pointsadded + (select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DR' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DR' )
/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonusDB +  pointsbonusCR + pointsadded
/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from History where tipnumber=Monthly_Statement_File.tipnumber
and histdate>=@startdate and histdate<=@enddate and trancode like 'R%' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
and histdate>=@startdate and histdate<=@enddate and trancode like 'R%' )
/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DE' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DE' )
/* Add EP to  minus adjustments    */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='EP' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='EP' )
-- RDT 8/28/2007  Added expired Points 
/* Add expired Points */
update Monthly_Statement_File
set PointsExpire = (select sum(points) from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='XP' )
where exists(select * from History where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='XP' )
/* Load the statmement file with total point decreases */
update Monthly_Statement_File
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire
/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate
/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
/* Load the acctid to the monthly statement table */
Update Monthly_Statement_file 
set acctid = a.acctid from Affiliat a where Monthly_Statement_file.tipnumber = a.tipnumber
GO
