SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkReversals](
	[Tipnumber] [varchar](15) NULL,
	[MonthEndDate] [varchar](10) NULL,
	[Type] [nvarchar](255) NULL,
	[New] [nvarchar](255) NULL,
	[Hide] [nvarchar](255) NULL,
	[PAN] [nvarchar](255) NULL,
	[DeviceID] [nvarchar](255) NULL,
	[TranDate] [nvarchar](255) NULL,
	[TranTime] [nvarchar](255) NULL,
	[NetReconAmt] [money] NULL,
	[ActionCode] [float] NULL,
	[LocalDate] [nvarchar](255) NULL,
	[LocalTime] [nvarchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
