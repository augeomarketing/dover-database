SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbers_DDA] @TipFirstParm varchar(3)
AS 




declare @LastTipUsed char(15)
declare @SSN nchar(16), @PAN nchar(16), @PrimDDA nchar(16), @1stDDA nchar(16), @2ndDDA nchar(16), @3rdDDA nchar(16), @4thDDA nchar(16), @5thDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)

update demographicIn set pan=rtrim(ltrim(pan))
update demographicIn set [Prim DDA]=null where len(rtrim(ltrim([Prim DDA])))=0
update demographicIn set [1st DDA]=null where len(rtrim(ltrim([1st DDA])))=0
update demographicIn set [2nd DDA]=null where len(rtrim(ltrim([2nd DDA])))=0
update demographicIn set [3rd DDA]=null where len(rtrim(ltrim([3rd DDA])))=0
update demographicIn set [4th DDA]=null where len(rtrim(ltrim([4th DDA])))=0
update demographicIn set [5th DDA]=null where len(rtrim(ltrim([5th DDA])))=0


--create a temp table based on Distinct Primary DDAs
select distinct [Prim DDA] as PrimDDA
into #tmpPrimDDA
from DemographicIn 
where [Prim DDA] not in (select acctnumber from Account_Reference)




declare csrDDA cursor FAST_FORWARD for
select PrimDDA from #tmpPrimDDA
      

open csrDDA

fetch next from csrDDA
      into  @PrimDDA

while @@FETCH_STATUS = 0
BEGIN
      --
      -- Get new tip#
      --
      exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstParm, @LastTipUsed output
      select @LastTipUsed as LastTipUsed

      set @newtipnumber = cast(@LastTipUsed as bigint) + 1  

      set @UpdateTip=@newtipnumber

      --
      -- Add new tip/dda references to the account_reference table
      --


if @PrimDDA is not null
      insert into dbo.account_reference
      values(@UpdateTip, @PrimDDA, @TipFirstParm)


      --
      -- Update last Tip Number Used
      exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstParm, @newtipnumber 

                                 
      fetch next from csrDDA
      into  @PrimDDA
END

close csrDDA
deallocate csrDDA
------------------------------------------------------
--re-update Demographic in to give tips to the NEW card numbers
update di
      set TipNumber = ar.tipnumber,
            tipfirst = left(ar.tipnumber,3)
from dbo.DemographicIn di join dbo.account_reference ar
      on di.[Prim DDA] = ar.acctnumber
GO
