SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkReversals_Totals](
	[Tipnumber] [varchar](15) NULL,
	[SumNetReconAmt] [money] NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
