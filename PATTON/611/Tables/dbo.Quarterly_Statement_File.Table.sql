USE [611RivertownCFCUConsumer]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 04/22/2011 10:06:55 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__151B244E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__151B244E]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__160F4887]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__160F4887]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__17036CC0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__17036CC0]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__17F790F9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__17F790F9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__18EBB532]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__18EBB532]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__19DFD96B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__19DFD96B]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1AD3FDA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1AD3FDA4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1BC821DD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1BC821DD]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1CBC4616]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1CBC4616]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1DB06A4F]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1EA48E88]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1F98B2C1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__208CD6FA]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMN]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__151B244E]  DEFAULT (0),
	[PointsEnd] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__160F4887]  DEFAULT (0),
	[PointsPurchasedCR] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__17036CC0]  DEFAULT (0),
	[PointsBonusCR] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__17F790F9]  DEFAULT (0),
	[PointsAdded] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__18EBB532]  DEFAULT (0),
	[PointsPurchasedDB] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__19DFD96B]  DEFAULT (0),
	[PointsBonusDB] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__1AD3FDA4]  DEFAULT (0),
	[PointsIncreased] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__1BC821DD]  DEFAULT (0),
	[PointsRedeemed] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__1CBC4616]  DEFAULT (0),
	[PointsReturnedCR] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__1DB06A4F]  DEFAULT (0),
	[PointsSubtracted] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__1EA48E88]  DEFAULT (0),
	[PointsReturnedDB] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__1F98B2C1]  DEFAULT (0),
	[PointsDecreased] [numeric](18, 0) NULL CONSTRAINT [DF__Quarterly__Point__208CD6FA]  DEFAULT (0),
	[Zip] [char](15) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMN]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
