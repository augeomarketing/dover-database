SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pzzzDELETE_GenerateOnlineRegistrationBonus] @EndDate varchar(10)
AS 
/*************************************************************/
/*                                                           */
/*   Procedure to generate the Online sign up bonus          */
/*                                                           */
/*                                                           */
/*************************************************************/
declare @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10)
set @Trandate=@EndDate
set @bonuspoints='2500'

insert into zzz_Delete values (@TranDate)
GO
