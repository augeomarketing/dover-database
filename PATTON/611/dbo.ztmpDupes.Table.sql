SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ztmpDupes](
	[tipnumber] [varchar](15) NOT NULL,
	[TranCode] [varchar](2) NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
