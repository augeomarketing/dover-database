/*
   Monday, September 17, 20123:31:35 PM
   User: 
   Server: doolittle\rn
   Database: 603TLCCommunityConsumer
   Application: 
*/
use [611RivertownCFCUConsumer]
go

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.Monthly_Statement_File.Acctnum', N'Tmp_AcctID', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.Monthly_Statement_File.Tmp_AcctID', N'AcctID', 'COLUMN' 
GO
ALTER TABLE dbo.Monthly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
