USE [51CHelmBankBusiness]
GO
/****** Object:  StoredProcedure [dbo].[spBusinessOnLineRegistrationReport]    Script Date: 09/24/2009 14:33:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spBusinessOnLineRegistrationReport] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table Business_Online_Registration

    -- Insert statements for procedure here
	Insert into dbo.Business_Online_Registration
	select a.Tipnumber, b.name1, b.availablebal, c.lastsix
	from [rn1].metavante.dbo.[1security] a, [rn1].metavante.dbo.customer b, [rn1].metavante.dbo.account c
	where left(a.tipnumber,3) = '51C' and a.tipnumber not like '%999999999%'     
		and a.username is not null and a.password is not null 
		and a.tipnumber=b.tipnumber
		and a.tipnumber=C.tipnumber
END
GO
