USE [51CHelmBankBusiness]
GO
/****** Object:  Table [dbo].[Business_Online_Registration]    Script Date: 09/24/2009 14:33:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Business_Online_Registration](
	[Tipnumber] [char](15) NULL,
	[Name] [char](40) NULL,
	[Availablebal] [numeric](18, 0) NULL,
	[CardLastSix] [char](6) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
