USE [51KHarrisArizona]
GO
/****** Object:  Table [dbo].[FBOPStatement]    Script Date: 01/12/2010 09:16:48 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__18EBB532]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__18EBB532]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__18EBB532]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__19DFD96B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__19DFD96B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__19DFD96B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1AD3FDA4]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1AD3FDA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__1AD3FDA4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1BC821DD]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1BC821DD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__1BC821DD]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1CBC4616]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1CBC4616]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__1CBC4616]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__662B2B3B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__662B2B3B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__662B2B3B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1DB06A4F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__1DB06A4F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1EA48E88]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__1EA48E88]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1F98B2C1]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__1F98B2C1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__69FBBC1F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__69FBBC1F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__69FBBC1F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPStatement_PointsBonusEmpCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPStatement_PointsBonusEmpCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF_FBOPStatement_PointsBonusEmpCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPStatement_PointsBonusEmpDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPStatement_PointsBonusEmpDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF_FBOPStatement_PointsBonusEmpDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__208CD6FA]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__208CD6FA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__2180FB33]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__2180FB33]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__22751F6C]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__22751F6C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__236943A5]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__236943A5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__245D67DE]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__245D67DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__245D67DE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__25518C17]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__25518C17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__25518C17]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__2645B050]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__2645B050]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__73852659]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__73852659]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__73852659]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__2739D489]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__2739D489]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__282DF8C2]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__282DF8C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__Point__282DF8C2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__PNTDE__29221CFB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__PNTDE__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__PNTDE__29221CFB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__PNTMO__2A164134]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__PNTMO__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__PNTMO__2A164134]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__PNTHO__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__PNTHO__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] DROP CONSTRAINT [DF__FBOPState__PNTHO__2B0A656D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPStatement]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPStatement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPStatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedBUS] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusBUS] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedBUS] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
 CONSTRAINT [PK_FBOPStatement] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__18EBB532]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__18EBB532]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__18EBB532]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__19DFD96B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__19DFD96B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__19DFD96B]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1AD3FDA4]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1AD3FDA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1AD3FDA4]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1BC821DD]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1BC821DD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1BC821DD]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1CBC4616]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1CBC4616]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1CBC4616]  DEFAULT (0) FOR [PointsPurchasedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__662B2B3B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__662B2B3B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__662B2B3B]  DEFAULT (0) FOR [PointsPurchasedBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1DB06A4F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1DB06A4F]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1EA48E88]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1EA48E88]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__1F98B2C1]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1F98B2C1]  DEFAULT (0) FOR [PointsBonusHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__69FBBC1F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__69FBBC1F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__69FBBC1F]  DEFAULT (0) FOR [PointsBonusBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPStatement_PointsBonusEmpCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPStatement_PointsBonusEmpCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF_FBOPStatement_PointsBonusEmpCR]  DEFAULT (0) FOR [PointsBonusEmpCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPStatement_PointsBonusEmpDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPStatement_PointsBonusEmpDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF_FBOPStatement_PointsBonusEmpDB]  DEFAULT (0) FOR [PointsBonusEmpDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__208CD6FA]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__208CD6FA]  DEFAULT (0) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__2180FB33]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__2180FB33]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__22751F6C]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__22751F6C]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__236943A5]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__236943A5]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__245D67DE]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__245D67DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__245D67DE]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__25518C17]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__25518C17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__25518C17]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__2645B050]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__2645B050]  DEFAULT (0) FOR [PointsReturnedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__73852659]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__73852659]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__73852659]  DEFAULT (0) FOR [PointsReturnedBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__2739D489]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__2739D489]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__Point__282DF8C2]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__Point__282DF8C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__282DF8C2]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__PNTDE__29221CFB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__PNTDE__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__PNTDE__29221CFB]  DEFAULT (0) FOR [PNTDEBIT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__PNTMO__2A164134]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__PNTMO__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__PNTMO__2A164134]  DEFAULT (0) FOR [PNTMORT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPState__PNTHO__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPState__PNTHO__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__PNTHO__2B0A656D]  DEFAULT (0) FOR [PNTHOME]
END


End
GO
