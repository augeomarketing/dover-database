USE [51KHarrisArizona]
GO
/****** Object:  Table [dbo].[AugEnd]    Script Date: 01/12/2010 09:16:48 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AugEnd__PNTEND__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[AugEnd]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AugEnd__PNTEND__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AugEnd] DROP CONSTRAINT [DF__AugEnd__PNTEND__7A672E12]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AugEnd]') AND type in (N'U'))
DROP TABLE [dbo].[AugEnd]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AugEnd]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AugEnd](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AugEnd__PNTEND__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[AugEnd]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AugEnd__PNTEND__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AugEnd] ADD  CONSTRAINT [DF__AugEnd__PNTEND__7A672E12]  DEFAULT (0) FOR [PNTEND]
END


End
GO
