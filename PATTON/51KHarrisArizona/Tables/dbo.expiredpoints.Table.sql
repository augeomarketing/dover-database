USE [51KHarrisArizona]
GO
/****** Object:  Table [dbo].[expiredpoints]    Script Date: 01/12/2010 09:16:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[expiredpoints]') AND type in (N'U'))
DROP TABLE [dbo].[expiredpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[expiredpoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[expiredpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateOfExpire] [nvarchar](12) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
