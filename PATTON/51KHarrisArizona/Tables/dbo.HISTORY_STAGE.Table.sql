USE [51KHarrisArizona]
GO
/****** Object:  Table [dbo].[HISTORY_STAGE]    Script Date: 01/12/2010 09:16:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_STAGE]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY_STAGE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_STAGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY_STAGE](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
