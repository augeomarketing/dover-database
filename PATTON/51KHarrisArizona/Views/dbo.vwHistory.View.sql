USE [51KHarrisArizona]
GO
/****** Object:  View [dbo].[vwHistory]    Script Date: 01/12/2010 09:16:51 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwHistory]'))
DROP VIEW [dbo].[vwHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwHistory]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwHistory]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from [51KHarrisArizona].dbo.history

'
GO
