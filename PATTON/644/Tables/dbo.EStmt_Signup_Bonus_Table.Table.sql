USE [644]
GO
/****** Object:  Table [dbo].[EStmt_Signup_Bonus_Table]    Script Date: 05/04/2011 11:00:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Signup_Bonus_Table]') AND type in (N'U'))
DROP TABLE [dbo].[EStmt_Signup_Bonus_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Signup_Bonus_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EStmt_Signup_Bonus_Table](
	[Tipnumber] [nchar](15) NULL,
	[Awarded] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
