USE [644]
GO
/****** Object:  Table [dbo].[Current_Month_Activity_wrk]    Script Date: 05/04/2011 11:00:10 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_wrk_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_wrk] DROP CONSTRAINT [DF_Current_Month_Activity_wrk_Points]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity_wrk]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity_wrk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity_wrk]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity_wrk](
	[Tipnumber] [nvarchar](15) NULL,
	[Points] [numeric](18, 0) NULL CONSTRAINT [DF_Current_Month_Activity_wrk_Points]  DEFAULT (0)
) ON [PRIMARY]
END
GO
