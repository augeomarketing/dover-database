USE [644]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 05/04/2011 11:00:09 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg10]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Beginning_Balance_Table_MonthBeg12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT ((0)),
	[MonthBeg2] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT ((0)),
	[MonthBeg3] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT ((0)),
	[MonthBeg4] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT ((0)),
	[MonthBeg5] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT ((0)),
	[MonthBeg6] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT ((0)),
	[MonthBeg7] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT ((0)),
	[MonthBeg8] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT ((0)),
	[MonthBeg9] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT ((0)),
	[MonthBeg10] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT ((0)),
	[MonthBeg11] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT ((0)),
	[MonthBeg12] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT ((0)),
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
