USE [644]
GO
/****** Object:  Table [dbo].[Monthly_Statement_Summary]    Script Date: 05/17/2011 10:27:09 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_Summary_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_Summary] DROP CONSTRAINT [DF_Monthly_Statement_Summary_PointsBonusMN]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_Summary]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_Summary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_Summary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_Summary](
	[NumAccounts] [decimal](18, 0) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[EndDate] [datetime] NULL,
	[PointsBonusMN] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_Summary_PointsBonusMN]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
