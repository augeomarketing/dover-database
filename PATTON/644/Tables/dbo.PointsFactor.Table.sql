USE [644]
GO
/****** Object:  Table [dbo].[PointsFactor]    Script Date: 05/04/2011 11:00:12 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PointsFactor_Factor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] DROP CONSTRAINT [DF_PointsFactor_Factor]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsFactor]') AND type in (N'U'))
DROP TABLE [dbo].[PointsFactor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsFactor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PointsFactor](
	[PAN] [nchar](16) NOT NULL,
	[Factor] [numeric](18, 2) NOT NULL CONSTRAINT [DF_PointsFactor_Factor]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
