USE [644]
GO
/****** Object:  Table [dbo].[Relationship]    Script Date: 05/04/2011 11:00:13 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Relationship_Factor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [DF_Relationship_Factor]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND type in (N'U'))
DROP TABLE [dbo].[Relationship]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Relationship](
	[Acct] [nchar](16) NOT NULL,
	[Factor] [numeric](4, 2) NOT NULL CONSTRAINT [DF_Relationship_Factor]  DEFAULT ((1.00)),
 CONSTRAINT [PK_Relationship] PRIMARY KEY CLUSTERED 
(
	[Acct] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
