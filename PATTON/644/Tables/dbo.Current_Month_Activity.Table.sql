USE [644]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 05/04/2011 11:00:10 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_EndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_EndingPoints]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Increases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Increases]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Decreases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Decreases]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_AdjustedEndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT (0),
	[Increases] [int] NULL CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT (0),
	[Decreases] [int] NULL CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT (0),
	[AdjustedEndingPoints] [int] NULL CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT (0)
) ON [PRIMARY]
END
GO
