USE [644]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_UniqueCustInRecord]    Script Date: 01/23/2013 16:40:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_UniqueCustInRecord]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_UniqueCustInRecord]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_UniqueCustInRecord]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'



CREATE function [dbo].[ufn_UniqueCustInRecord]() 
returns @UniqueCustIn table
	(	
		NAMEACCT1 nvarchar(40), NAMEACCT2 nvarchar(40)
		, NAMEACCT3 nvarchar(40), NAMEACCT4 nvarchar(40), NAMEACCT5 nvarchar(40), NAMEACCT6 nvarchar(40)
		, STATUS nvarchar(1), TIPNUMBER nvarchar(15), TIPFIRST nvarchar(3), TIPLAST nvarchar(12)
		, ADDRESS1 nvarchar(40), ADDRESS2 nvarchar(40), ADDRESS4 nvarchar(40), CITY nvarchar(38)
		, STATE char(2), ZIP nvarchar(15), LASTNAME nvarchar(40), HOMEPHONE nvarchar(10)
		, WORKPHONE nvarchar(10), DATEADDED nvarchar(10), MISC1 nvarchar(20), MISC2 nvarchar(20)
		, MISC3 nvarchar(20)
	)
as
begin
	declare @tmpCustIn table
	(	
		MyID INT IDENTITY(1,1) NOT NULL, NAMEACCT1 nvarchar(40), NAMEACCT2 nvarchar(40)
		, NAMEACCT3 nvarchar(40), NAMEACCT4 nvarchar(40), NAMEACCT5 nvarchar(40), NAMEACCT6 nvarchar(40)
		, STATUS nvarchar(1), TIPNUMBER nvarchar(15), TIPFIRST nvarchar(3), TIPLAST nvarchar(12)
		, ADDRESS1 nvarchar(40), ADDRESS2 nvarchar(40), ADDRESS4 nvarchar(40), CITY nvarchar(38)
		, STATE char(2), ZIP nvarchar(15), LASTNAME nvarchar(40), HOMEPHONE nvarchar(10)
		, WORKPHONE nvarchar(10), DATEADDED nvarchar(10), MISC1 nvarchar(20), MISC2 nvarchar(20)
		, MISC3 nvarchar(20)
	)

	INSERT INTO @tmpCustIn
	(
		NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6
		, STATUS, TIPNUMBER, TIPFIRST, TIPLAST, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME
		, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3
	)
	select 
		NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6
		, STATUS, TIPNUMBER, left(TIPNUMBER,3) as TIPFIRST, right(rtrim(TIPNUMBER),12) as TIPLAST, ADDRESS1
		, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2
		, MISC3
	 from CUSTIN
	 where 1=1
		AND Status = ''A''
	  order by --Make sure the one you want to keep is the first into the table
		TIPNUMBER asc, NAMEACCT6 DESC, NAMEACCT5 DESC, NAMEACCT4 DESC
		, NAMEACCT3 DESC, NAMEACCT2 DESC, NAMEACCT1 DESC
 
	INSERT INTO @UniqueCustIn
	(
		NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6
		, STATUS, TIPNUMBER, TIPFIRST, TIPLAST, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME
		, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3
	)
	select 
		NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6
		, STATUS, tci.TIPNUMBER, TIPFIRST, TIPLAST, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME
		, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3
	from @tmpCustIn tci
	inner join
	(
		select MIN(myId) MinID, TIPNUMBER
		from @tmpCustIn
		group by TIPNUMBER
	) m_tci
	on tci.MyID = m_tci.MinID

	return;
end


' 
END
GO
