USE [644]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard_Bonuses_Registration]    Script Date: 05/04/2011 11:01:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses_Registration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard_Bonuses_Registration]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses_Registration]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadTransStandard_Bonuses_Registration] 
	-- Add the parameters for the stored procedure here
	@EndDate char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Declare @E_Stmt bit
	--do a select from a BonusTypes table to conditionally add 


	---------------------------------------------------------------------------------------
	/* E-Statement Bonuses */

	Insert into transstandard_Bonuses (tfno,Trandate, Acct_num, Trancode, Trannum, tranamt, trantype, ratio, crdactvldt)
	select tipnumber, @EndDate,NULL,''BR'',1,1000, ''Bonus On-Line Rewards Registration'', 1,null from dbo.Signon_Bonus_Table where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BR'')

	Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
	select tipnumber, ''BR'',null,@EndDate from dbo.Signon_Bonus_Table where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BR'')
	-----------------------------------------------------------------------------------------------------------------------

	/* ADD MORE BONUSES HERE */

	------------------------------------------------------------------------------------------------------------------------
	/* Copy records to TransStandard*/
	insert into transstandard 
		Select * from TransStandard_Bonuses
END
' 
END
GO
