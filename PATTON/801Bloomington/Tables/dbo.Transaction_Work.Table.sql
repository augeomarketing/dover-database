USE [801Bloomington]
GO
/****** Object:  Table [dbo].[Transaction_Work]    Script Date: 09/07/2010 16:55:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaction_Work](
	[Bank_Num] [varchar](4) NULL,
	[AcctID] [varchar](12) NULL,
	[Record_Type] [varchar](1) NULL,
	[Type_Acct] [varchar](1) NULL,
	[Name1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[Addr1] [varchar](40) NULL,
	[Addr2] [varchar](40) NULL,
	[Addr3] [varchar](40) NULL,
	[TIN] [varchar](9) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[Num_Bill_Paid] [varchar](3) NULL,
	[Email_Enabled] [varchar](1) NULL,
	[Sig_Amt] [numeric](18, 2) NULL,
	[Sig_CT] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
