USE [801Bloomington]
GO
/****** Object:  Table [dbo].[OlD_Cust_YTD_Transaction]    Script Date: 09/07/2010 16:55:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OlD_Cust_YTD_Transaction](
	[Customer_Id] [varchar](16) NOT NULL,
	[Current_Month_Transaction] [numeric](18, 2) NULL,
	[Current_Month_Count] [int] NULL,
	[Current_YTD_Total] [numeric](18, 2) NULL,
	[Prev_YTD_Total] [numeric](18, 2) NULL,
	[Current_YTD_Count] [int] NULL,
	[Prev_YTD_Count] [int] NULL,
	[Last_Date_Processed] [date] NULL,
 CONSTRAINT [PK_Old_Cust_YTD_Transaction] PRIMARY KEY CLUSTERED 
(
	[Customer_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
