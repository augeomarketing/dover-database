USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [dbo].[SSIS Configurations]
where [ConfigurationFilter] = '801_PushPattonToWeb' or 
[ConfigurationFilter] = '801_Patton_Default'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'801_PushPattonToWeb', N'Data Source=rn1;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-801_PushPattonToWeb-{D6BE7126-1AC5-4DFE-BA46-53DCFA6EB4F9}rn1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[RewardsNow WEB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'801_Patton_Default', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-801_PushPattonToWeb-{F0AF083B-DA16-4A90-8654-854F3B37DB14}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'801_Patton_Default', N'Data Source=patton\rn;Initial Catalog=801Bloomington;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-801_PushPattonToWeb-{DBC6E719-E791-479E-9808-F73EADE4E15A}patton\rn.801Bloomington;Auto Translate=False;', N'\Package.Connections[801Bloomington].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'801_PushPattonToWeb', N'Data Source=rn1;Initial Catalog=HorizonBank;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-801_PushPattonToWeb-{121E8CFE-F763-4320-85E4-EFD7E1383CCB}rn1.HorizonBank;Auto Translate=False;', N'\Package.Connections[HorizonBank_Web].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

