USE [801Bloomington]
GO
/****** Object:  StoredProcedure [dbo].[spEditTransStandard]    Script Date: 09/07/2010 16:54:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    Edit the TransStandard Table                                            */
/* BY:  Dan Foster                                                            */
/* DATE: 07/26/2010                                                           */
/* Changes:                                                                   */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spEditTransStandard] @DateAdded varchar(10) AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 

UPDATE TS SET tipnumber = Cp.tipnumber
FROM TransStandard TS join customerprep Cp on TS.Acctid = Cp.account_number

Update TransStandard set TranCode = '64'
where TranAmt > 0

update TransStandard set TranCode = '34'
where TranAmt <= 0

delete from TransStandard
where (tranamt > -2 and TranAmt < 2)

update TransStandard set  tranamt = abs( round(tranamt/2,0)) ,TranDate = @dateadded

 
/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode
GO
