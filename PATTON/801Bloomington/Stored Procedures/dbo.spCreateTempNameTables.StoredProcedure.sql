USE [801Bloomington]
GO
/****** Object:  StoredProcedure [dbo].[spCreateTempNameTables]    Script Date: 09/07/2010 16:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================
-- Author:		Dan Foster		
-- Create date: <07/07/2010>
-- Description:	<Allocate temp Name work tables>
-- ===============================================
CREATE PROCEDURE [dbo].[spCreateTempNameTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #newlastname(
	[Cardnumber] [varchar](25) NOT NULL,
	[AcctName1] [varchar](50) NULL,
	[ReversedName] [varchar](50) NULL,
	[AcctName2] [varchar](50) null,
	[LastSpace] [int] NULL,
	[Lastname] [varchar](50) NULL
)

truncate table #newlastname

Insert #newlastname (cardnumber, acctname1)
select distinct(cardnumber),acctname1 from hold_customer
where (PATINDEX('%,%', acctname1) = 0)

update #newlastname set reversedname = rtrim(ltrim(reverse(acctname1)))
		
update #newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	
update #newlastname set lastname = reverse(substring(reversedname,1,lastspace))
where lastspace >= 0
	
update #newlastname set reversedname = reverse(left(acctname1, len(acctname1) -3))
where lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'LP'
	
update #newlastname set reversedname = reverse(left(acctname1, len(acctname1) -4))
where lastname = 'INC' or lastname = 'III' or rtrim(ltrim(lastname)) = 'LLC' or lastname = 'DBA' or lastname = 'LTD' or
lastname = 'SSB'
	 
update #newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
where lastspace <= 3
	
update #newlastname set lastname = reverse(substring(reversedname,1,lastspace))
where (lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'III')
and lastspace > 0
	
update #newlastname set lastname = acctname1 where lastname = 'INC' or lastname = 'CO'
	
update #newlastname set lastname = reverse(reversedname) where lastspace <= 1

update HC set hc.AcctName1 = nln.acctname1,hc.Lastname=NLN.lastname
from Hold_Customer HC join #newlastname NLN on HC.CardNumber = NLN.CardNumber
	
truncate table #newlastname

Insert #newlastname (cardnumber, acctname1,acctname2)
select distinct(cardnumber),acctname1,acctname2 from hold_customer
where (PATINDEX('%,%', acctname1) > 0)

UPDATE  #newlastname SET lastname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

update #newlastname set reversedname = ltrim(rtrim(REVERSE(acctname1)))

update #newlastname set ReversedName = ltrim(rtrim(SUBSTRING(ReversedName,3,LEN(reversedname))))
where SUBSTRING(ReversedName,1,2) = 'RO'

update #newlastname set AcctName1 = REVERSE(reversedname)

UPDATE #newlastname SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE #newlastname SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

update HC set hc.AcctName1 = nln.acctname1,hc.AcctName2=NLN.acctname2,hc.lastname=NLN.lastname
from Hold_Customer HC join #newlastname NLN on HC.CardNumber = NLN.CardNumber

--drop table #newlastname
END
GO
