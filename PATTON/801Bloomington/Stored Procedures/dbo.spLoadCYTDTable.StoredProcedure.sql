USE [801Bloomington]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCYTDTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCYTDTable]
GO

-- =============================================
-- Author:		Dan Foster	
-- Create date: 8/10/2010
-- Description:	Load CYTD Transaction Table
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCYTDTable]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update Transaction_Work set Sig_Amt = (Sig_Amt/100)
	
	-- RDT20111208 Remove minus sign '000-1' Transaction_Work 
	update Transaction_Work 
		set SIG_CT = replace ( SIG_CT ,'-','0')
		where SIG_CT like '%-%'
			
	insert dbo.Cust_YTD_Transaction (Customer_Id) 
	select distinct acctid from Transaction_work 
	where AcctID not in (select Customer_Id from Cust_YTD_Transaction)

	update dbo.Cust_YTD_Transaction set Current_Month_Transaction = 0, Current_Month_Count = 0, 
	Prev_YTD_Total = Current_YTD_Total, Prev_YTD_Count = Current_YTD_Count 
	
    update dbo.Cust_YTD_Transaction set Current_YTD_Total = 0, Current_YTD_Count = 0 
	
	update dbo.Cust_YTD_Transaction set Current_YTD_Total = SIG_AMT, Current_YTD_Count = SIG_CT
	from dbo.Cust_YTD_Transaction CYTD join Transaction_work CIW on CYTD.Customer_Id = CIW.acctid
	
	update dbo.Cust_YTD_Transaction set Current_Month_Transaction = Current_YTD_Total - Prev_YTD_Total,
	Current_Month_Count = Current_YTD_Count - Prev_YTD_Count
	where Current_YTD_Total <> 0
	
	update dbo.Cust_YTD_Transaction set Current_Month_Transaction = 0 
	where Current_YTD_Total - Prev_YTD_Total = 0 
	
END
GO
