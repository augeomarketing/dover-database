USE [801Bloomington]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/05/2010 09:30:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/05/2010 09:30:05 ******/
/*  *****************************************/
/* Date:  7/26/2010                         */
/* Author:  Dan Foster                      */
/*  *****************************************/
/*  Description: Copies data from input_transaction to the Affiliat_stage table           */
/*  Tables:                                                                               */
/*  Revisions: 1) update using member number changed to add account type MEMBER to        */ 
/*                affiliat_stage                                                          */
/*             2) new insert using card number to add card numbers to affiliat_stage      */ 
/*             3) update added using card number to add account type DEBIT to             */ 
/*                affiliat_stage                                                          */
/* RDT 2012/02/17    */
/******************************************************************************************/

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 

/************ Insert New Accounts into Affiliat Stage  ***********/
insert into AFFILIAT_Stage 
	(ACCTID,TIPNUMBER)
	select distinct account_number,tipnumber from CustomerPrep 
		where Account_Number not in ( select acctid from AFFILIAT_Stage ) 
		and LEN( Account_Number ) <= 12
		and CustomerPrep.TipNumber is not null

/**** Update new account fields ****/
update AFS 
	set AcctType='Member', 
	DateAdded=@MonthEnd, 
	secid = null, 
	AcctStatus='A',
	AcctTypeDesc = 'Member Number', 
	LastName=cs.LastName, 
	YTDEarned=0, 
	custid=tin
 from AFFILIAT_Stage AFS join CustomerPrep CS on AFS.ACCTID=CS.account_number
 where (DateAdded is null or DateAdded = '') 

/*** Insert new cards into affiliat ****/
--- Max Tipnumber is used because FI sends same card on different member numbers. 
insert into AFFILIAT_Stage 
	(ACCTID,tipnumber)
select Card_Number, MAX(tipnumber ) 
		from CustomerPrep 
		where Card_Number not in ( Select acctid from AFFILIAT_Stage) 
		and CustomerPrep.TipNumber is not null
		group by card_Number

/**** UPDATE new card fields ****/
Update AFS 
	set 
	AcctType		='Debit', 
	DateAdded		= @MonthEnd,
	secid				= null, 
	AcctStatus		='A',
	AcctTypeDesc = 'Debit Card', 
	LastName		=cs.LastName, 
	YTDEarned		=0, 
	custid				=tin
 from AFFILIAT_Stage AFS join CustomerPrep CS on AFS.ACCTID=CS.Card_Number 
 where	 (DateAdded is null or DateAdded = '') 
 
GO


