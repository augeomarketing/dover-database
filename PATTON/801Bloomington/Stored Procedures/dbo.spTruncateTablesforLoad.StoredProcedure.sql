USE [801Bloomington]
GO
/****** Object:  StoredProcedure [dbo].[spTruncateTablesforLoad]    Script Date: 09/07/2010 16:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================
-- Author:		Dab Foster
-- Create date: 8/3/2010
-- Description:	Prep Data Base Tables for Initial Load
-- =====================================================
CREATE PROCEDURE [dbo].[spTruncateTablesforLoad]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	truncate table customer
	truncate table customer_Stage
	truncate table affiliat
	truncate table affiliat_Stage
	truncate table history
	truncate table history_Stage
	truncate table OnetimeBonuses
	truncate table OnetimeBonuses_Stage
	truncate Table beginning_balance_table
	truncate Table customerprep
	truncate table cust_ytd_transaction

END
GO
