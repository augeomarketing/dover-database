USE [801Bloomington]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateAffiliat]    Script Date: 09/07/2010 16:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 08/03/2010
-- Description:	Update Affiliat_stage table
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateAffiliat] @end_date varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   update AFFILIAT_Stage set AcctType = 'Debit', AcctStatus = 'A', AcctTypeDesc = 'Debit Card',
   YTDEarned = '0', DATEADDED = @end_date, SECID = RIGHT(secid,4)
   
   update AFS set LastName = cp.lastname,custid = cp.tin
   from AFFILIAT_Stage AFS join CustomerPrep CP on AFS.TIPNUMBER = CP.tipnumber
   
END
GO
