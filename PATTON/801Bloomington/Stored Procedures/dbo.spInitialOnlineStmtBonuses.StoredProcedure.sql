USE [801Bloomington]
GO
/****** Object:  StoredProcedure [dbo].[spInitialOnlineStmtBonuses]    Script Date: 09/07/2010 16:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:		Dan Foster
-- Create date: 08/28/2010
-- Description:	Initialization for online stmat bonuses
-- ======================================================
CREATE PROCEDURE [dbo].[spInitialOnlineStmtBonuses] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update onetimebonuses set Trancode = 'BE', dateawarded = @processdate  

END
GO
