USE [801Bloomington]
GO

/****** Object:  StoredProcedure [dbo].[spAwardBonuses]    Script Date: 09/15/2010 16:04:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAwardBonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAwardBonuses]
GO

USE [801Bloomington]
GO

/****** Object:  StoredProcedure [dbo].[spAwardBonuses]    Script Date: 09/15/2010 16:04:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ======================================================================
-- Author:	Dan Foster
-- Create date: 07/26/2010
-- Description:	Creates First Time Use Bonus and Web registration Bonus
-- ======================================================================
CREATE PROCEDURE [dbo].[spAwardBonuses]  @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Sets up Customer First Time Use Bonus
    
	insert dbo.TransStandard (TipNumber,TranDate,TranCode,TranNum,TranAmt,Ratio)
		select distinct tipnumber,TranDate,'BF','1','500','1' from dbo.TransStandard where
		tipnumber not in (select tipnumber from dbo.OneTimeBonuses_stage where Trancode = 'BF')
		and TranCode = '64'	
		
	insert dbo.OneTimeBonuses_Stage (TipNumber, Trancode, DateAwarded)
		select tipnumber,trancode,trandate from dbo.TransStandard where Trancode = 'BF'
		
	-- Sets up Customer Bonus for Bank Estatements
   
	insert dbo.TransStandard (TipNumber,TranDate,TranCode,TranNum,TranAmt,Ratio)
		select tipnumber,@processdate,'BE','1','1000','1' from dbo.CUSTOMER_Stage where
		tipnumber not in (select tipnumber from dbo.OneTimeBonuses_stage where Trancode = 'BE') and
		tipnumber in (select distinct tipnumber from CustomerPrep where online_stmt = '3')	
		
	insert dbo.OneTimeBonuses_Stage (TipNumber, Trancode, DateAwarded)
		select tipnumber,trancode,trandate from dbo.TransStandard where Trancode = 'BE'
		
	Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
	on R.TranCode = T.Trancode 

END




GO


