USE [801Bloomington]
GO
/****** Object:  StoredProcedure [dbo].[spClearWorkandHoldTables]    Script Date: 09/07/2010 16:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Dan Foster
-- Create date: 6/01/2010
-- Description:	Clear work tables to receive next month's files
-- ================================================================
CREATE PROCEDURE [dbo].[spClearWorkandHoldTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 truncate table dbo.customerprep
 truncate table dbo.affiliat_stage
 truncate table dbo.customer_stage
 truncate table dbo.history_stage
 truncate table dbo.OneTimeBonuses_Stage
 truncate table dbo.welcomekit
 truncate table dbo.transaction_work
 
END
GO
