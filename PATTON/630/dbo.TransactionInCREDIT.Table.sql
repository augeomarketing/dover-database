SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionInCREDIT](
	[RecType] [char](1) NULL,
	[TransCode] [char](2) NULL,
	[CardNumber] [char](16) NULL,
	[TransAmt] [char](10) NULL,
	[AuthNum] [char](5) NULL,
	[PurchaseDate] [char](10) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
