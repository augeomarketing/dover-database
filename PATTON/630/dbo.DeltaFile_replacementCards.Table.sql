SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeltaFile_replacementCards](
	[Pan] [nvarchar](16) NULL,
	[ReplacementCard] [nvarchar](16) NULL,
	[TipNumber] [nvarchar](15) NULL,
	[EndDate] [datetime] NULL
) ON [PRIMARY]
GO
