SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustIn_Combined_DebitCredit](
	[TipNumber] [nchar](15) NULL,
	[Acct_Num] [nchar](16) NULL,
	[AcctName1] [char](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
