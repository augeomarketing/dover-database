SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScorecardPointsIn](
	[Account_Number] [nvarchar](255) NULL,
	[Acct_Name] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[Tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
