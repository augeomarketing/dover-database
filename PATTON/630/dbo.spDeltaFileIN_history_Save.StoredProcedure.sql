SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDeltaFileIN_history_Save] @enddate char(10)
AS 

--delete records (in case of re-running)
delete DeltaFileIN_CR_history where ProcEndDate=@EndDate

insert into dbo.DeltaFileIN_CR_history(Pan, NA1, Address1, Address2, City, ST, Zip, Home_Phone, SSN, LastName, TipNumber, ProcEndDate)
select Pan, NA1, Address1, Address2, City, ST, Zip, Home_Phone, SSN, LastName, TipNumber, @EndDate from DeltaFileIN_CR
GO
