SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pSetupTransferPointsForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSFER POINTS  DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
truncate table wrkTRANSsTANDARDTransferPoints
truncate table ScorecardPointsIn_err

update ScorecardPointsIn
set ScorecardPointsIn.tipnumber=affiliat.tipnumber
from ScorecardPointsIn, affiliat
where ScorecardPointsIn.Account_number=affiliat.acctid and ScorecardPointsIn.tipnumber is null


insert into ScorecardPointsIn_err select * from ScorecardPointsIn where tipnumber is null

delete ScorecardPointsIn where tipnumber is null

--Move and calc from ScorecardPointsIn --> wrkTRANSsTANDARDTransferPoints
-- SEB001
INSERT INTO wrkTRANSsTANDARDTransferPoints(Tipnumber, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select Tipnumber, @enddate, Account_Number, 'TP', Count(Account_Number), sum(cast(Points as decimal)) , 'Transfer Standard', '1', ' ' from ScorecardPointsIn
group by tipnumber, Account_Number
GO
