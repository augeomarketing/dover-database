SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spClosedCards_IN_SaveHistory] @Enddate char(10)
AS 



--get the last day of previous month's processing to put in the History table
declare  @StartDate varchar(10),@LastmonthEndDate datetime
set @StartDate=substring(@EndDate,1,2) + '/01/' + substring(@EndDate,7,4)
set @LastmonthEndDate=cast(@StartDate as datetime)-1

--insert lastmonths values into history
insert into dbo.ClosedCards_In_History
	select CardnumToClose,@LastmonthEndDate from ClosedCards_In
	
	--===========================
TRUNCATE TABLE ClosedCards_IN
GO
