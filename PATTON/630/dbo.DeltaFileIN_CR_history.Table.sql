SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeltaFileIN_CR_history](
	[Pan] [nchar](16) NULL,
	[NA1] [char](40) NULL,
	[Address1] [nchar](40) NULL,
	[Address2] [nchar](40) NULL,
	[City] [nchar](40) NULL,
	[ST] [nchar](2) NULL,
	[Zip] [nchar](9) NULL,
	[Home_Phone] [nchar](10) NULL,
	[SSN] [nchar](16) NULL,
	[LastName] [char](40) NULL,
	[TipNumber] [nchar](15) NULL,
	[ProcEndDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
