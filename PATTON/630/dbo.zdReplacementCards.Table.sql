SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zdReplacementCards](
	[Cardnumber] [nvarchar](255) NULL,
	[REPLACEMENT_CARD_NUMBER] [nvarchar](255) NULL
) ON [PRIMARY]
GO
