SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pDELTA_Process_Replacement_Cards] @Enddate datetime
AS 

delete DeltaFile_replacementCards where Pan is NULL

--update the values of the DemographicInCR table with the replacement card values in the DeltaFile_replacementCards table. 
--this is upstream so that when CustIn_Combined_DebitCredit is created in the DealWithClosedCards630 that the cards and corresponding account won't get closed
update w set PAN=A.ReplacementCard
	from DemographicInCR w
	join DeltaFile_replacementCards a 
	on a.PAN = w.Pan   


--udate the tipnumbers in the DeltaFile_replacementCards table
update w set TipNumber=A.Tipnumber
	from DeltaFile_replacementCards w
	join Affiliat a 
	on a.AcctID = w.Pan   


---------------update all of the working tables with the NEW account number
update a set AcctID=w.ReplacementCard
	from DeltaFile_replacementCards w
	join Affiliat a 
	on w.Pan = a.AcctID

update a set AcctID=w.ReplacementCard
	from DeltaFile_replacementCards w
	join AffiliatDeleted a 
	on w.Pan = a.AcctID

update a set AcctNumber=w.ReplacementCard
	from DeltaFile_replacementCards w
	join Account_Reference a 
	on w.Pan = a.AcctNumber

update a set AcctNumber=w.ReplacementCard
	from DeltaFile_replacementCards w
	join Account_Reference_Deleted a 
	on w.Pan = a.AcctNumber

update a set AcctID=w.ReplacementCard
	from DeltaFile_replacementCards w
	join History a 
	on w.Pan = a.AcctID

update a set AcctID=w.ReplacementCard
	from DeltaFile_replacementCards w
	join HistoryDeleted a 
	on w.Pan = a.AcctID
GO
