SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDealWithClosedCards_630] @Enddate char(10)
AS 
/************************************************************************************/

/************************************************************************************/

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)



declare @datedeleted datetime
set @datedeleted = @Enddate



delete ClosedCards_IN where CardnumToClose is null
--truncate the combined demographic table
	

/* set any affiliat records that have been explicitly closed form the CLosedCard input file*/

Update affiliat set acctstatus = 'C' where acctid in(select CardnumToClose from ClosedCards_IN)


/* set any customers to closed that don't have any active records in the affiliat table*/
Update customer set status = 'C' where status='A' and not exists(select * from affiliat where affiliat.tipnumber= customer.tipnumber and affiliat.acctstatus='A') 


/* set any closed customers to ACTIVE that DOt have active records in the affiliat table*/
Update customer set status = 'A' where status='C' and exists(select * from affiliat where affiliat.tipnumber= customer.tipnumber and affiliat.acctstatus='A')
GO
