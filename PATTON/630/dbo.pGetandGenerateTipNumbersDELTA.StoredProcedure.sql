SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbersDELTA]
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS  for the CREDIT cards from the DELTA file that DON'T are NEW accounts (don't have a value in the replacement card field and are not being closed ('c' in the CloseFlag field)                   */
/*                                                                            */
/******************************************************************************/
-- Changed logic to employ last tip used instead of max tip
declare  @PAN nchar(16),  @SSN nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15)
/************BEGIN SEB 001   ***************/

truncate table DemographicIn_DELTA
delete from DeltaFileIN_CR where Pan is null



insert into DemographicIn_DELTA
(pan,
[Address #1],
[Address #2],
City,
St,
Zip,
NA1,
[Home Phone],
SSN,
Lastname
)
select 
	[Pan],
	[Address1],
	[Address2],
	[City],
	[ST],
	left([Zip],5),
	[NA1],
	right(ltrim(rtrim([Home_Phone])),10),
	[SSN],
	[Lastname]
from DeltaFileIN_CR





/************ END SEB001  ****************/
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare DEMO_crsr cursor
for select Tipnumber, Pan, SSN
from DemographicIn_DELTA /* SEB001 */
for update
/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber='0'
		
fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
set @worktip='0'
		
	-- Check if PAN already assigned to a tip number.
	if @PAN is not null and left(@PAN,1)<> ' ' and exists(select tipnumber from account_reference where acctnumber=@PAN)
	Begin
		set @Worktip=(select tipnumber from account_reference where acctnumber=@PAN)
	END
	If @workTip='0' or @worktip is null
	Begin
		/*********  Start SEB002  *************************/
		--set @newtipnumber=(select max(tipnumber) from account_reference)				
		--if @newtipnumber is null or left(@newtipnumber,1)=' ' 
		--begin
		--	set @newtipnumber=@tipfirst+'000000000000'		
		--end				
		--set @newtipnumber=@newtipnumber + '1'
	
		declare @LastTipUsed char(15)
		
		exec rewardsnow.dbo.spGetLastTipNumberUsed '630', @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @newtipnumber = cast(@LastTipUsed as bigint) + 1  
	/*********  End SEB002  *************************/
		set @UpdateTip=@newtipnumber
		
		if @PAN is not null and left(@PAN,1)<> ' ' and not exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			insert into Account_Reference
			values(@UpdateTip, @PAN, left(@UpdateTip,3))
		End

		
		exec RewardsNOW.dbo.spPutLastTipNumberUsed '630', @Newtipnumber  /*SEB002 */
	End
	Else
	If @workTip<>'0' and @worktip is not null
		Begin
			set @updateTip=@worktip
		End	
	update DemographicIn_DELTA	/* SEB001 */
	set tipnumber = @UpdateTip 
	where current of demo_crsr 
	goto Next_Record
Next_Record:
		fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN
end
Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr

---------------------------------------------------------------
GO
