SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionInCREDIT_orphans_history_Summary](
	[CardNumber] [char](16) NULL,
	[Points] [int] NULL,
	[MonthEndDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
