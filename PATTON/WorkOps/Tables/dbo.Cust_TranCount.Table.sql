USE [WorkOps]
GO
/****** Object:  Table [dbo].[Cust_TranCount]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_TranCount](
	[DBName] [varchar](100) NOT NULL,
	[Trancode] [varchar](4) NULL,
	[Trandesc] [varchar](100) NULL,
	[Count_Tran] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
