USE [WorkOps]
GO
/****** Object:  Table [dbo].[VisaCheck]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VisaCheck](
	[DBName] [varchar](100) NOT NULL,
	[accountnum] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
