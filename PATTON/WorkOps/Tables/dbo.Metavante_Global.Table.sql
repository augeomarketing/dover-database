USE [WorkOps]
GO
/****** Object:  Table [dbo].[Metavante_Global]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Metavante_Global](
	[TIPfirst] [varchar](3) NULL,
	[MonthBeginningDate] [varchar](10) NULL,
	[MonthEndingDate] [varchar](10) NULL,
	[DBName1] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
