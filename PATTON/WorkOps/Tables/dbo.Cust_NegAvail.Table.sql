USE [WorkOps]
GO
/****** Object:  Table [dbo].[Cust_NegAvail]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_NegAvail](
	[DBName] [varchar](100) NOT NULL,
	[LessThan_0] [int] NULL,
	[LessThan_100] [int] NULL,
	[LessThan_1000] [int] NULL,
	[LessThan_5000] [int] NULL,
	[LessThan_10000] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
