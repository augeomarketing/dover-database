USE [WorkOps]
GO
/****** Object:  Table [dbo].[hist_datacheck]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hist_datacheck](
	[yr] [int] NULL,
	[mo] [int] NULL,
	[Net] [bigint] NULL,
	[Returns] [bigint] NULL,
	[Bonus] [bigint] NULL,
	[R_Bonus] [bigint] NULL,
	[New_Cust] [int] NULL,
	[Del_Cust] [int] NULL,
	[New_Acct] [int] NULL,
	[Del_Acct] [int] NULL
) ON [PRIMARY]
GO
