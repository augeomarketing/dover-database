USE [WorkOps]
GO
/****** Object:  Table [dbo].[Cust_PartCounts_Del]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_PartCounts_Del](
	[DBName] [varchar](100) NOT NULL,
	[Part_Year] [int] NULL,
	[Part_Month] [int] NULL,
	[Part_Count] [bigint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
