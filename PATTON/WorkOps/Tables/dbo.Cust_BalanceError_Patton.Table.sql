USE [WorkOps]
GO
/****** Object:  Table [dbo].[Cust_BalanceError_Patton]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_BalanceError_Patton](
	[DBName] [varchar](100) NOT NULL,
	[Hist_Avail_Errors] [int] NULL,
	[Bal_Avail_Errors] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
