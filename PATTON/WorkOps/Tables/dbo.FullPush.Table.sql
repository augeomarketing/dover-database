USE [WorkOps]
GO
/****** Object:  Table [dbo].[FullPush]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FullPush](
	[sid_FullPush_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_FullPush_user] [nvarchar](50) NOT NULL,
	[dim_FullPush_created] [datetime] NOT NULL,
	[dim_FullPush_lastmodified] [datetime] NOT NULL,
	[dim_FullPush_active] [int] NOT NULL,
 CONSTRAINT [PK_FullPush] PRIMARY KEY CLUSTERED 
(
	[sid_FullPush_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FullPush] ADD  CONSTRAINT [DF_FullPush_dim_FullPush_created]  DEFAULT (getdate()) FOR [dim_FullPush_created]
GO
ALTER TABLE [dbo].[FullPush] ADD  CONSTRAINT [DF_FullPush_dim_FullPush_lastmodified]  DEFAULT (getdate()) FOR [dim_FullPush_lastmodified]
GO
ALTER TABLE [dbo].[FullPush] ADD  CONSTRAINT [DF_FullPush_dim_FullPush_active]  DEFAULT (1) FOR [dim_FullPush_active]
GO
