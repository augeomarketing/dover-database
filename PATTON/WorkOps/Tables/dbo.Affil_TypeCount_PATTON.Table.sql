USE [WorkOps]
GO
/****** Object:  Table [dbo].[Affil_TypeCount_PATTON]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Affil_TypeCount_PATTON](
	[DBName] [varchar](100) NOT NULL,
	[accttype] [varchar](50) NULL,
	[TypeCount] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
