USE [WorkOps]
GO

/****** Object:  Table [dbo].[Hist_Breakage]    Script Date: 07/16/2013 10:52:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Hist_Breakage]') AND type in (N'U'))
DROP TABLE [dbo].[Hist_Breakage]
GO

USE [WorkOps]
GO

/****** Object:  Table [dbo].[Hist_Breakage]    Script Date: 07/16/2013 10:52:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Hist_Breakage](
	[DBName] [varchar](100) NOT NULL,
	[ClientName] [varchar](100) NOT NULL,
	[FI_Status] [varchar](3) NULL,
	[First_Entry] [datetime] NULL,
	[Points_Awarded] [bigint] NULL,
	[Points_Redeemed] [bigint] NULL,
	[Points_Expired] [bigint] NULL,
	[Points_Closed] [bigint] NULL,
	[Points_Forfeit] [bigint] NULL,
	[Points_Available] [bigint] NULL
) ON [PRIMARY]

GO
