USE [WorkOps]
GO
/****** Object:  Table [dbo].[pricing]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pricing](
	[sid_pricing_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_catalog_id] [int] NULL,
	[dim_catalog_code] [nvarchar](255) NULL,
	[dim_pricing_cost] [money] NULL,
	[dim_pricing_estcost] [money] NULL,
	[dim_pricing_shipping] [money] NULL,
	[dim_pricing_handling] [money] NULL,
	[dim_pricing_MSRP] [money] NULL,
	[sid_vendor_id] [int] NULL,
	[dim_pricing_weight] [int] NULL,
	[dim_pricing_active] [int] NOT NULL,
	[dim_pricing_created] [datetime] NOT NULL,
	[dim_pricing_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_Pricingimport] PRIMARY KEY CLUSTERED 
(
	[sid_pricing_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[pricing] ADD  CONSTRAINT [DF_pricing_dim_pricing_active]  DEFAULT (1) FOR [dim_pricing_active]
GO
ALTER TABLE [dbo].[pricing] ADD  CONSTRAINT [DF_pricing_dim_pricing_created]  DEFAULT (getdate()) FOR [dim_pricing_created]
GO
ALTER TABLE [dbo].[pricing] ADD  CONSTRAINT [DF_pricing_dim_pricing_lastmodified]  DEFAULT (getdate()) FOR [dim_pricing_lastmodified]
GO
