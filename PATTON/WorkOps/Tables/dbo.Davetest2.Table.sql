USE [WorkOps]
GO
/****** Object:  Table [dbo].[Davetest2]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Davetest2](
	[Update1] [varchar](50) NULL,
	[update2] [datetime] NULL,
	[key2] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Davetest2] PRIMARY KEY CLUSTERED 
(
	[key2] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
