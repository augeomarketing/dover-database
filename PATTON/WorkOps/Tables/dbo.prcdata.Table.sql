USE [WorkOps]
GO
/****** Object:  Table [dbo].[prcdata]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[prcdata](
	[code] [nvarchar](255) NULL,
	[Cost] [float] NULL,
	[Shipping] [float] NULL,
	[handling] [float] NULL,
	[MSRP] [float] NULL,
	[Weight] [float] NULL,
	[sid_catalog_id] [float] NULL,
	[sid_vendor_id] [float] NULL
) ON [PRIMARY]
GO
