USE [WorkOps]
GO

/****** Object:  Table [dbo].[Cust_OrphanedHistory]    Script Date: 09/29/2010 09:59:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cust_OrphanedHistory]') AND type in (N'U'))
DROP TABLE [dbo].[Cust_OrphanedHistory]
GO

USE [WorkOps]
GO

/****** Object:  Table [dbo].[Cust_OrphanedHistory]    Script Date: 09/29/2010 09:59:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Cust_OrphanedHistory](
	[DBName] [varchar](100) NOT NULL,
	[Error_Count] [int] NOT NULL DEFAULT(0),
 CONSTRAINT [PK_Cust_OrphanedHistory] PRIMARY KEY CLUSTERED 
(
	[DBName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


