USE [WorkOps]
GO
/****** Object:  Table [dbo].[FBOPreport]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FBOPreport](
	[TIPFirst] [varchar](3) NULL,
	[Points_Total] [int] NULL,
	[Points_Processed] [int] NULL,
	[Points_Outstanding] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
