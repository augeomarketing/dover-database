USE [WorkOps]
GO

/****** Object:  Table [dbo].[Cust_BalanceError]    Script Date: 09/29/2010 09:58:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cust_BalanceError]') AND type in (N'U'))
DROP TABLE [dbo].[Cust_BalanceError]
GO

USE [WorkOps]
GO

/****** Object:  Table [dbo].[Cust_BalanceError]    Script Date: 09/29/2010 09:58:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Cust_BalanceError](
	[DBName] [varchar](100) NOT NULL,
	[Hist_Avail_Errors] [int] NOT NULL DEFAULT(0),
	[Bal_Avail_Errors] [int] NOT NULL DEFAULT(0),
 CONSTRAINT [PK_Cust_BalanceError] PRIMARY KEY CLUSTERED 
(
	[DBName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


