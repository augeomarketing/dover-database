USE [WorkOps]
GO

/****** Object:  Table [dbo].[NAP_Acct]    Script Date: 01/29/2016 11:00:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NAP_Acct]') AND type in (N'U'))
DROP TABLE [dbo].[NAP_Acct]
GO

USE [WorkOps]
GO

/****** Object:  Table [dbo].[NAP_Acct]    Script Date: 01/29/2016 11:00:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[NAP_Acct](
	[TIPNumber] [char](15) NULL,
	[lastname] [varchar](40) NULL,
	[lastsix] [varchar](50) NULL,
	[SSNlast4] [varchar](50) NULL,
	[memberid] [varchar](100) NULL,
	[membernumber] [varchar](100) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


