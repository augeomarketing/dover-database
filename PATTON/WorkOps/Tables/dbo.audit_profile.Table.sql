USE [WorkOps]
GO
/****** Object:  Table [dbo].[audit_profile]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[audit_profile](
	[RowNumber] [int] IDENTITY(1,1) NOT NULL,
	[EventClass] [int] NULL,
	[TextData] [ntext] NULL,
	[BinaryData] [image] NULL,
	[DatabaseID] [int] NULL,
	[TransactionID] [bigint] NULL,
	[NTUserName] [nvarchar](128) NULL,
	[NTDomainName] [nvarchar](128) NULL,
	[HostName] [nvarchar](128) NULL,
	[ClientProcessID] [int] NULL,
	[ApplicationName] [nvarchar](128) NULL,
	[LoginName] [nvarchar](128) NULL,
	[SPID] [int] NULL,
	[Duration] [bigint] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[CPU] [int] NULL,
	[Permissions] [int] NULL,
	[Severity] [int] NULL,
	[EventSubClass] [int] NULL,
	[ObjectID] [int] NULL,
	[Success] [int] NULL,
	[IndexID] [int] NULL,
	[IntegerData] [int] NULL,
	[ServerName] [nvarchar](128) NULL,
	[ObjectType] [int] NULL,
	[NestLevel] [int] NULL,
	[State] [int] NULL,
	[Error] [int] NULL,
	[Mode] [int] NULL,
	[Handle] [int] NULL,
	[ObjectName] [nvarchar](128) NULL,
	[DatabaseName] [nvarchar](128) NULL,
	[FileName] [nvarchar](128) NULL,
	[OwnerName] [nvarchar](128) NULL,
	[RoleName] [nvarchar](128) NULL,
	[TargetUserName] [nvarchar](128) NULL,
	[DBUserName] [nvarchar](128) NULL,
	[LoginSid] [image] NULL,
	[TargetLoginName] [nvarchar](128) NULL,
	[TargetLoginSid] [image] NULL,
	[ColumnPermissions] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RowNumber] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC dbo.sp_addextendedproperty @name=N'Build', @value=2039 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'audit_profile'
GO
EXEC dbo.sp_addextendedproperty @name=N'MajorVer', @value=8 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'audit_profile'
GO
EXEC dbo.sp_addextendedproperty @name=N'MinorVer', @value=0 , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'audit_profile'
GO
