USE [WorkOps]
GO

/****** Object:  Table [dbo].[Cust_LastTip]    Script Date: 09/29/2010 09:59:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cust_LastTip]') AND type in (N'U'))
DROP TABLE [dbo].[Cust_LastTip]
GO

USE [WorkOps]
GO

/****** Object:  Table [dbo].[Cust_LastTip]    Script Date: 09/29/2010 09:59:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Cust_LastTip](
	[DBName] [varchar](100) NOT NULL,
	[Cust] [varchar](15) NULL,
	[CusDel] [varchar](15) NULL,
	[Client_RecordCount] [int] NOT NULL DEFAULT(0),
	[Cust_Max] [varchar](15) NULL,
	[Client_Last] [varchar](15) NULL,
 CONSTRAINT [PK_Cust_LastTip] PRIMARY KEY CLUSTERED 
(
	[DBName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


