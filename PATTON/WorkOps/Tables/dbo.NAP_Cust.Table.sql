USE [WorkOps]
GO

/****** Object:  Table [dbo].[NAP_Cust]    Script Date: 01/29/2016 11:00:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NAP_Cust]') AND type in (N'U'))
DROP TABLE [dbo].[NAP_Cust]
GO

USE [WorkOps]
GO

/****** Object:  Table [dbo].[NAP_Cust]    Script Date: 01/29/2016 11:00:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[NAP_Cust](
	[TIPNumber] [char](15) NULL,
	[username] [char](50) NULL,
	[password] [varchar](150) NULL,
	[emailstatement] [varchar](1) NULL,
	[email] [char](50) NULL,
	[emailother] [varchar](1) NULL,
	[Name_1] [varchar](40) NULL,
	[Name_2] [varchar](40) NULL,
	[Name_3] [varchar](40) NULL,
	[Name_4] [varchar](40) NULL,
	[Name_5] [varchar](40) NULL,
	[Address_1] [varchar](40) NULL,
	[Address_2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[Country] [varchar](3) NULL,
	[Status] [varchar](1) NULL,
	[BirthDate] [varchar](8) NULL,
	[Gender] [varchar](1) NULL,
	[Radius] [varchar](5) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


