USE [WorkOps]
GO
/****** Object:  Table [dbo].[FbopPurged]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FbopPurged](
	[sid_FbopPurged_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_FbopPurged_tipnumber] [int] NULL,
	[dim_FbopPurged_Acctname1] [nvarchar](50) NULL,
	[dim_FbopPurged_Address1] [nvarchar](100) NULL,
	[dim_FbopPurged_address4] [nvarchar](100) NULL,
	[dim_FbopPurged_Zipcode] [int] NULL,
	[dim_FbopPurged_acctidsix] [int] NULL,
	[dim_FbopPurged_runavailable] [bigint] NULL,
	[dim_FbopPurged_Custid] [bigint] NULL,
 CONSTRAINT [PK_FbopPurged] PRIMARY KEY CLUSTERED 
(
	[sid_FbopPurged_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
