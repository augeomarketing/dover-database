USE [WorkOps]
GO
/****** Object:  Table [dbo].[Hist_EarnedPoints]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hist_EarnedPoints](
	[DBName] [varchar](100) NOT NULL,
	[Point_Year] [int] NULL,
	[Point_Month] [int] NULL,
	[Points_Earned] [bigint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
