USE [WorkOps]
GO
/****** Object:  Table [dbo].[Cust_PointSynch]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_PointSynch](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](50) NOT NULL,
	[Trandate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
