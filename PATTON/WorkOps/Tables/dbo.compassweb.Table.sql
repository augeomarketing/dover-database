USE [WorkOps]
GO
/****** Object:  Table [dbo].[compassweb]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[compassweb](
	[sid_compassweb_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_compassweb_totalhits] [int] NOT NULL,
	[dim_compassweb_logins] [int] NOT NULL,
	[dim_compassweb_failedlogins] [int] NOT NULL,
	[dim_compassweb_redemptions] [int] NOT NULL,
	[dim_compassweb_faq] [int] NOT NULL,
	[dim_compassweb_terms] [int] NOT NULL,
	[dim_compassweb_catalog] [int] NOT NULL,
	[dim_compassweb_combines] [int] NOT NULL,
	[dim_compassweb_password] [int] NOT NULL,
	[dim_compassweb_customerservice] [int] NOT NULL,
	[dim_compassweb_undeliverableemail] [int] NOT NULL,
	[dim_compassweb_enrollment] [int] NOT NULL,
	[dim_compassweb_daterange] [varchar](50) NOT NULL,
	[dim_compassweb_created] [datetime] NOT NULL,
	[dim_compassweb_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_compassweb] PRIMARY KEY CLUSTERED 
(
	[sid_compassweb_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_totalhits]  DEFAULT ((-1)) FOR [dim_compassweb_totalhits]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_logins]  DEFAULT ((-1)) FOR [dim_compassweb_logins]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_failedlogins]  DEFAULT ((-1)) FOR [dim_compassweb_failedlogins]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_redemptions]  DEFAULT ((-1)) FOR [dim_compassweb_redemptions]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_faq]  DEFAULT ((-1)) FOR [dim_compassweb_faq]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_terms]  DEFAULT ((-1)) FOR [dim_compassweb_terms]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_catalog]  DEFAULT ((-1)) FOR [dim_compassweb_catalog]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_combines]  DEFAULT ((-1)) FOR [dim_compassweb_combines]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_password]  DEFAULT ((-1)) FOR [dim_compassweb_password]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_customerservice]  DEFAULT ((-1)) FOR [dim_compassweb_customerservice]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_undeliverableemail]  DEFAULT ((-1)) FOR [dim_compassweb_undeliverableemail]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_enrollment]  DEFAULT ((-1)) FOR [dim_compassweb_enrollment]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_daterange]  DEFAULT ('notset') FOR [dim_compassweb_daterange]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_created]  DEFAULT (getdate()) FOR [dim_compassweb_created]
GO
ALTER TABLE [dbo].[compassweb] ADD  CONSTRAINT [DF_compassweb_dim_compassweb_lastmodified]  DEFAULT (getdate()) FOR [dim_compassweb_lastmodified]
GO
