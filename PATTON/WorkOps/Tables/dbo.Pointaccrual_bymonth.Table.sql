USE [WorkOps]
GO
/****** Object:  Table [dbo].[Pointaccrual_bymonth]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pointaccrual_bymonth](
	[DBName] [varchar](100) NULL,
	[yr] [int] NULL,
	[mo] [int] NULL,
	[Earned] [bigint] NULL,
	[Redeemed] [bigint] NULL,
	[Expired] [bigint] NULL,
	[Purged] [bigint] NULL,
	[New_Cust] [int] NULL,
	[Del_Cust] [int] NULL,
	[New_Acct] [int] NULL,
	[Del_Acct] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
