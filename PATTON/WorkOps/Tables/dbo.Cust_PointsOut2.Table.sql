USE [WorkOps]
GO
/****** Object:  Table [dbo].[Cust_PointsOut2]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_PointsOut2](
	[DBName] [varchar](100) NOT NULL,
	[Points_Outstanding] [bigint] NULL,
	[Rundate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
