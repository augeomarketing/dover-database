USE [WorkOps]
GO
/****** Object:  Table [dbo].[Hist_RecordCount]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hist_RecordCount](
	[DBName] [varchar](100) NOT NULL,
	[Hist_Count] [int] NULL,
	[Hist_Sum] [bigint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
