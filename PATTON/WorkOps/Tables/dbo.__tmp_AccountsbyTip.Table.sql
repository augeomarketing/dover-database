USE [WorkOps]
GO
/****** Object:  Table [dbo].[__tmp_AccountsbyTip]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__tmp_AccountsbyTip](
	[TipNumber] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[CITY] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[BeginningPoints] [int] NULL,
	[EndingPointBalance] [int] NULL,
	[DebitPurchase] [int] NULL,
	[DebitBonus] [int] NULL,
	[DebitReturn] [int] NULL,
	[Redeemed] [int] NULL,
	[Adjustments] [int] NULL,
	[emailaddress] [varchar](40) NULL,
	[emailstatement] [char](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
