USE [WorkOps]
GO
/****** Object:  Table [dbo].[DB_Table_RecCount]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DB_Table_RecCount](
	[DBName] [nvarchar](100) NOT NULL,
	[TBLname] [nvarchar](100) NOT NULL,
	[RecCount] [int] NOT NULL,
	[RunDate] [datetime] NULL
) ON [PRIMARY]
GO
