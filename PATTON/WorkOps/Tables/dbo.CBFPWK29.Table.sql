USE [WorkOps]
GO
/****** Object:  Table [dbo].[CBFPWK29]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CBFPWK29](
	[Col001] [char](11) NULL,
	[Col002] [char](17) NULL,
	[Col003] [char](9) NULL,
	[Col004] [char](52) NULL,
	[Col005] [char](52) NULL,
	[Col006] [char](21) NULL,
	[Col007] [char](35) NULL,
	[Col008] [char](10) NULL,
	[Col009] [char](13) NULL,
	[Col010] [char](26) NULL,
	[Col011] [char](68) NULL,
	[Col012] [char](186) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
