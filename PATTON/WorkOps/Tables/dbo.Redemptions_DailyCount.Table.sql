USE [WorkOps]
GO
/****** Object:  Table [dbo].[Redemptions_DailyCount]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Redemptions_DailyCount](
	[DBName_Patton] [varchar](100) NULL,
	[TipFirst] [varchar](3) NULL,
	[r_redeempoint] [int] NULL,
	[r_redeemcount] [int] NULL,
	[r_decreasepoint] [int] NULL,
	[r_decreasecount] [int] NULL,
	[f_redeempoint] [int] NULL,
	[f_redeemcount] [int] NULL,
	[f_decreasepoint] [int] NULL,
	[f_decreasecount] [int] NULL,
	[h_redeempoint] [int] NULL,
	[h_redeemcount] [int] NULL,
	[h_decreasepoint] [int] NULL,
	[h_decreasecount] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
