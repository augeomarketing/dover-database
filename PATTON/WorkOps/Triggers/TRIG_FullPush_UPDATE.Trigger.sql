USE [WorkOps]
GO
/****** Object:  Trigger [TRIG_FullPush_UPDATE]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_FullPush_UPDATE] ON [dbo].[FullPush] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 

	UPDATE FullPush
      SET dim_FullPush_lastmodified = getdate()
		FROM dbo.FullPush INNER JOIN deleted del
      ON FullPush.sid_FullPush_id = del.sid_FullPush_id

END
GO
