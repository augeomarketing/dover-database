USE [WorkOps]
GO
/****** Object:  Trigger [UPD_compassweb]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UPD_compassweb]
   ON  [dbo].[compassweb] 
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE compassweb
		SET dim_compassweb_lastmodified = getdate()
		FROM dbo.compassweb join deleted del
			ON compassweb.sid_compassweb_id = del.sid_compassweb_id
END
GO
