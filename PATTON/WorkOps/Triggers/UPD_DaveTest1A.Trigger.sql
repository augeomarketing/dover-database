USE [WorkOps]
GO
/****** Object:  Trigger [UPD_DaveTest1A]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[UPD_DaveTest1A]
		ON [dbo].[DaveTest1]
		AFTER UPDATE
AS

IF UPDATE (VALUE1)

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Davetest2
		SET update1 = 5,
		update2 = getdate()
	FROM   DaveTest1 as A JOIN Davetest2 as B ON A.key1 = B.key2
END
GO
