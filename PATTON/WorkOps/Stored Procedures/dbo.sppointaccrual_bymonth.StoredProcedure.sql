USE [WorkOps]
GO
/****** Object:  StoredProcedure [dbo].[sppointaccrual_bymonth]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sppointaccrual_bymonth]
	@tip char(3)

as

Declare @dbname varchar(50)
Declare @sqlcmd nvarchar(4000)

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @dbname = quotename(@dbname) + '.dbo.'

--Drop table [workops].dbo.hist_datacheck2
--Create table [workops].dbo.hist_datacheck2 (yr int, mo int, Earned bigint, Redeemed bigint, Expired bigint, Purged bigint, New_Cust int, Del_Cust int, New_Acct int, Del_Acct int)
Truncate table [workops].dbo.hist_datacheck2
create table #tmprslts (yr int, mo int, points bigint)
create table #tmprslts2 (yr int, mo int, points bigint)

--Set Month and Year
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,''0'' as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,''0'' as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, ''0'' as Earned, ''0'' as Redeemed, ''0'' as Expired, ''0'' as Purged, ''0'' as new_cust, ''0'' as del_cust, ''0'' as new_acct, ''0'' as del_acct '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
--Add null date flag row
--insert into [workops].dbo.hist_datacheck2
--(yr, mo, Earned, Redeemed, Expired, Purged, new_cust, del_cust, new_acct, del_acct)
--values
--(1,1,0,0,0,0,0,0,0,0)

--Set Earned Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) not in (''X'',''R'') '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) not in (''X'',''R'') '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'Set Earned = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Redeemed Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''R'') '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''R'') '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'Set Redeemed = points '
Set @sqlcmd = @sqlcmd + 'From #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Expired Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''X'') '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(histdate,1))as yr ,month(isnull(histdate,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''X'') '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(histdate,1)),month(isnull(histdate,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'set Expired = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Purged Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(datedeleted,1))as yr ,month(isnull(datedeleted,1)) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(datedeleted,1)),month(isnull(datedeleted,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'set Purged = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd  

--Set New Customer Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(dateadded,1))as yr ,month(isnull(dateadded,1)) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customer '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(dateadded,1)),month(isnull(dateadded,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(dateadded,1))as yr ,month(isnull(dateadded,1)) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customerdeleted '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(dateadded,1)),month(isnull(dateadded,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'set new_cust = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Deleted Customer Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(datedeleted,1))as yr ,month(isnull(datedeleted,1)) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customerdeleted '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' and statusdescription not like ''Pri'' '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(datedeleted,1)),month(isnull(datedeleted,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'set del_cust = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set New Account Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(dateadded,1))as yr ,month(isnull(dateadded,1)) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliat '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(dateadded,1)),month(isnull(dateadded,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(dateadded,1))as yr ,month(isnull(dateadded,1)) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliatdeleted '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(dateadded,1)),month(isnull(dateadded,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'set new_acct = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Deleted Account Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(isnull(datedeleted,1))as yr ,month(isnull(datedeleted,1)) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliatdeleted '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(isnull(datedeleted,1)),month(isnull(datedeleted,1))'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck2 '
Set @sqlcmd = @sqlcmd + 'set del_acct = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck2 d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

drop table #tmprslts
drop table #tmprslts2
GO
