USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_FBOP_IncentiveReport]    Script Date: 05/12/2010 14:48:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FBOP_IncentiveReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FBOP_IncentiveReport]
GO

USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_FBOP_IncentiveReport]    Script Date: 05/12/2010 14:48:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[usp_FBOP_IncentiveReport]

@tipfirst		char(3),
@yr				char(4)

AS

Set NOCOUNT ON

Declare @db		varchar(50)
Declare @sql	nvarchar(4000)

Set @db = (select quotename(dbnamepatton) from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tipfirst)

Create table #tmp (tipnumber varchar(15),  Yr int, Outstanding int, Activity int, Incentive int)

Set @sql = '
Insert into #tmp 
select	distinct tipnumber, YEAR(histdate) as Yr, 0, 0, 0
from	'+@db+'.dbo.HISTORY
where	TIPNUMBER in (select TIPNUMBER from '+@db+'.dbo.HISTORY where TRANCODE in (''FE'',''FF'')and year(histdate) >= '''+@yr+''')
	and	year(histdate) >= '''+@yr+'''
order by tipnumber, YEAR(histdate)

Update #tmp 
set Outstanding = (select RunAvailable from '+@db+'.dbo.CUSTOMER where CUSTOMER.TIPNUMBER = #tmp.tipnumber)

Update #tmp 
set Activity = (select isnull(SUM(points*ratio),0) from '+@db+'.dbo.HISTORY 
	where	HISTORY.TIPNUMBER = #tmp.tipnumber 
		and	YEAR(history.histdate) = #tmp.Yr
		and	LEFT(history.trancode, 1) in (''3'',''6'',''B'',''F'')
		and	History.TRANCODE not in (''FE'',''FF''))
		
Update #tmp 
set Incentive = (select isnull(SUM(points*ratio),0) from '+@db+'.dbo.HISTORY 
	where HISTORY.TIPNUMBER = #tmp.tipnumber 
		and YEAR(history.histdate) = #tmp.Yr
		and History.TRANCODE in (''FE'',''FF''))		
'
Exec sp_executesql @sql
		
Select tipnumber, Yr, Outstanding, Activity, Incentive from #tmp

Drop table #tmp
GO


