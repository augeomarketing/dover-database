USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_Report_Custtransactions]    Script Date: 08/31/2010 10:32:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Report_Custtransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Report_Custtransactions]
GO

USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_Report_Custtransactions]    Script Date: 08/31/2010 10:32:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Report on number of transactions per customer for initial customer load, by 1 month increments

CREATE procedure [dbo].[usp_Report_Custtransactions]

	@tip char(3)

AS

Declare @dbname varchar(50)
Declare @sqlcmd nvarchar(4000)
Declare @datemin datetime
Declare @datemax datetime
Declare @datestart datetime
Declare @month int
Declare @year int

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @dbname = quotename(@dbname) + '.dbo.'
Set @sqlcmd = 
'Set @datemin = (Select cast(cast(MIN(dateadded) as DATE)as datetime) from '+@dbname+'CUSTOMER)'
Exec sp_executesql @sqlcmd, N'@datemin datetime output', @datemin = @datemin output
Set @sqlcmd =
'Set @datemax = (Select MAX(histdate) from '+@dbname+'History)'
Exec sp_executesql @sqlcmd, N'@datemax datetime output', @datemax = @datemax output
Set @datestart = @datemin

Create Table #tmp1 (Mo int,Yr int,Mo_D int,Yr_D int,tipnumber varchar(15),Tran_Count int)
Create Table #tmp2 (Mo int,Yr int, Range varchar(20), Tran_Count int)
Create Table #tmp3 (Mo int,Yr int, [0] int, [1-5] int, [6-10] int, [10-15] int, [Over 15] int, Closed int)

	while @datestart <= @datemax
	begin
	set @month = MONTH(@datestart)
	set @year = YEAR(@datestart)

Set @sqlcmd = 
'		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, ''99'' as Mo_D, ''9999'' as Yr_D, tipnumber, ''0'' as Tran_Count
		from '+@dbname+'CUSTOMER
		where TIPNUMBER in (Select tipnumber from '+@dbname+'customer where DATEADDED = '''+cast(@datemin as varchar(20))+''')
		  and TIPNUMBER not in (Select TIPNUMBER from '+@dbname+'HISTORY where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))

		insert into #tmp1
		Select  '''+cast(@month as varchar(2))+''' as Mo, '''+cast(@year as varchar(4))+''' as Yr, MONTH(DateDeleted) as Mo_D, YEAR(DateDeleted) as Yr_D, tipnumber, ''0'' as Tran_Count
		from '+@dbname+'CUSTOMERDELETED
		where TIPNUMBER in (Select tipnumber from '+@dbname+'CUSTOMERDELETED where DATEADDED = '''+cast(@datemin as varchar(20))+''' and StatusDescription not like ''%combine%'')
		  and TIPNUMBER not in (Select TIPNUMBER from '+@dbname+'HISTORYDELETED where MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+' and LEFT(trancode, 1) in (''3'',''6''))

		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,''99'' as Mo_D,''9999'' as Yr_D,TIPNUMBER, SUM(trancount) as Tran_Count 
		from '+@dbname+'HISTORY
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'customer where DATEADDED = '''+cast(@datemin as varchar(20))+''')
			and LEFT(trancode, 1) in (''3'',''6'')
			and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		group by tipnumber
		
		Insert into #tmp1
		Select '''+cast(@month as varchar(2))+''' as Mo,'''+cast(@year as varchar(4))+''' as Yr,MONTH(DateDeleted) as Mo_D,YEAR(DateDeleted) as Yr_D,TIPNUMBER, SUM(trancount) as Tran_Count 
		from '+@dbname+'HistoryDeleted
		where	TIPNUMBER in (Select tipnumber from '+@dbname+'CUSTOMERdeleted where DATEADDED = '''+cast(@datemin as varchar(20))+''' and StatusDescription not like ''%combine%'')
			and LEFT(trancode, 1) in (''3'',''6'')
			and MONTH(HISTDATE) = '+cast(@month as varchar(2))+' and YEAR(HISTDATE) = '+cast(@year as varchar(4))+'
		group by MONTH(DateDeleted),YEAR(DateDeleted),tipnumber
'
EXECUTE sp_executesql @SqlCmd

		insert into #tmp2	Select Mo,Yr,'1. 0', COUNT(Tran_Count) from #tmp1 where mo = @month and yr = @year and Tran_Count = 0 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'2. 1-5', COUNT(Tran_Count) from #tmp1 where mo = @month and yr = @year and Tran_Count > 0 and Tran_Count <= 5 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'3. 6-10', COUNT(Tran_Count) from #tmp1 where mo = @month and yr = @year and Tran_Count > 5 and Tran_Count <= 10 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'4. 10-15', COUNT(Tran_Count) from #tmp1 where mo = @month and yr = @year and Tran_Count > 10 and Tran_Count <= 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'5. Over 15', COUNT(Tran_Count) from #tmp1 where mo = @month and yr = @year and Tran_Count > 15 and ((mo_d > @month and yr_d >= @year) or Yr_D >@year) group by Mo,Yr
		insert into #tmp2	Select Mo,Yr,'6. Closed', COUNT(Tran_Count) from #tmp1 where mo = @month and yr = @year and ((mo_d <= @month and yr_d <= @year) or Yr_D < @year) group by Mo,Yr
		
	set @datestart = (Select DATEADD(month,1,@datestart))
	end		


insert into #tmp3 select distinct Mo, Yr, 0,0,0,0,0,0 from #tmp2
update #tmp3 set [0] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '1. 0'
update #tmp3 set [1-5] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '2. 1-5'
update #tmp3 set [6-10] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '3. 6-10'
update #tmp3 set [10-15] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '4. 10-15'
update #tmp3 set [Over 15] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '5. Over 15'
update #tmp3 set [Closed] = Tran_Count from #tmp2 where #tmp2.mo = #tmp3.Mo and #tmp2.yr = #tmp3.yr and [Range] = '6. Closed'

--Select * from #tmp2
--order by Yr,Mo,[Range]

Select Mo,Yr, sum(Tran_Count) as Total
from #tmp2
group by Mo,Yr
order by Mo,Yr

Select * from #tmp3 order by Yr, Mo

Drop table #tmp1
Drop table #tmp2
Drop table #tmp3



GO


