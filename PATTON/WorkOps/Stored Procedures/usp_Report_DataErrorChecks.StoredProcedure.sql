USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_Report_DataErrorChecks]    Script Date: 09/29/2010 13:53:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Report_DataErrorChecks]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Report_DataErrorChecks]
GO

USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_Report_DataErrorChecks]    Script Date: 09/29/2010 13:53:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create Procedure [dbo].[usp_Report_DataErrorChecks]

AS

Truncate table [WorkOps].dbo.Cust_OrphanedHistory
Truncate table [WorkOps].dbo.Cust_LastTip
Truncate table [WorkOps].dbo.Cust_BalanceError

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor FAST_FORWARD
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where sid_fiprodstatus_statuscode not in ('X') 
	and dbnamepatton in (Select name from master.sys.databases)
	and left(dbnumber,1) <> '9'
Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_OrphanedHistory
		 Select '''+@DBName+''' as DBName, count(tipnumber) as error_count
		 from ['+@DBName+'].dbo.history
		 where tipnumber not in
			(Select tipnumber from ['+@DBName+'].dbo.customer)'
	  EXECUTE sp_executesql @SqlCmd
  	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_LastTip
		 Select
		 	(Select '''+@DBName+''') as DBName, 
			(Select max(tipnumber) from ['+@DBName+'].dbo.customer) as Cust,
			(Select max(tipnumber) from ['+@DBName+'].dbo.customerdeleted) as CustDel,
			(Select Count(*) from ['+@DBName+'].dbo.client) as Client_RecordCount,
		 	(Case
				When (Select max(tipnumber) from ['+@DBName+'].dbo.customer)>=(Select Isnull(max(tipnumber), 0) from ['+@DBName+'].dbo.customerdeleted)
					then (Select max(tipnumber) from ['+@DBName+'].dbo.customer)
				Else (Select max(tipnumber) from ['+@DBName+'].dbo.customerdeleted)
			end) as Cust_Max,
		 	(Select lasttipnumberused from Rewardsnow.dbo.dbprocessinfo where dbnamepatton = '''+@DBName+''') as Client_Last'
	  EXECUTE sp_executesql @SqlCmd
	  set @sqlcmd =	
		'Insert into [WorkOps].dbo.Cust_BalanceError
		 Select '''+@DBName+''' as DBName, 
			(Select count(tipnumber)
			 From ['+@DBName+'].dbo.customer
		 	 where tipnumber in (
				Select ['+@DBName+'].dbo.customer.Tipnumber from ['+@DBName+'].dbo.customer join ['+@DBName+'].dbo.history on ['+@DBName+'].dbo.customer.tipnumber = ['+@DBName+'].dbo.history.tipnumber
				Group by ['+@DBName+'].dbo.customer.tipnumber, runavailable
				Having Sum(points*ratio) <> runavailable)) as Hist_Avail_Errors,
			(Select count(tipnumber)
			 From ['+@DBName+'].dbo.customer
		 	 where runbalance-runredeemed <> runavailable) as Bal_Avail_Errors'
	  EXECUTE sp_executesql @SqlCmd	  
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName
GO


