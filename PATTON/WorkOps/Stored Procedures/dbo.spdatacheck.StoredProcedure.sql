USE [WorkOps]
GO
/****** Object:  StoredProcedure [dbo].[spdatacheck]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spdatacheck]
	@tip char(3)

as

Declare @dbname varchar(50)
Declare @sqlcmd nvarchar(4000)

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @dbname = quotename(@dbname) + '.dbo.'

--Drop table [workops].dbo.hist_datacheck
--Create table [workops].dbo.hist_datacheck (yr int, mo int, Net bigint, Returns bigint, Bonus bigint, R_Bonus bigint, New_Cust int, Del_Cust int, New_Acct int, Del_Acct int)
Truncate table [workops].dbo.hist_datacheck
create table #tmprslts (yr int, mo int, points bigint)
create table #tmprslts2 (yr int, mo int, points bigint)

--Set Month and Year
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,''0'' as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,''0'' as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, ''0'' as Net, ''0'' as returns, ''0'' as bonus, ''0'' as r_bonus, ''0'' as new_cust, ''0'' as del_cust, ''0'' as new_acct, ''0'' as del_acct '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd

--Set Net Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''6'') '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''6'') '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'Set Net = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Return Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''3'') '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''3'') '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'Set Returns = points '
Set @sqlcmd = @sqlcmd + 'From #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Bonus Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''0'',''B'',''N'',''F'') and trancode <> ''BE'' '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where left(trancode,1) in (''0'',''B'',''N'',''F'') and trancode <> ''BE'' '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'set Bonus = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set RewardsNOW Bonus Points
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'where trancode = ''BE'' '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'where trancode = ''BE'' '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'set R_Bonus = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set New Customer Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customer '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customerdeleted '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'set new_cust = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Deleted Customer Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(datedeleted)as yr ,month(datedeleted) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customerdeleted '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(datedeleted),month(datedeleted)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'set del_cust = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set New Account Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliat '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliatdeleted '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'set new_acct = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Deleted Account Count
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(datedeleted)as yr ,month(datedeleted) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliatdeleted '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(datedeleted),month(datedeleted)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.hist_datacheck '
Set @sqlcmd = @sqlcmd + 'set del_acct = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.hist_datacheck d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

drop table #tmprslts
drop table #tmprslts2
GO
