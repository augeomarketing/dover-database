USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_FBOP_IncentiveReport_v2]    Script Date: 06/23/2010 09:57:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FBOP_IncentiveReport_v2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FBOP_IncentiveReport_v2]
GO

USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_FBOP_IncentiveReport_v2]    Script Date: 06/23/2010 09:57:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create procedure [dbo].[usp_FBOP_IncentiveReport_v2]

@tipfirst		char(3)

AS

Create table #tmp
	(TIP varchar(15), Name1 varchar(50), Name2 varchar(50), Address1 varchar(50), Address2 varchar(50),
     City varchar(50), [State] varchar(50), Zip varchar(5), Phone varchar(10), 
	 Current_Point_Balance int, Incentive_Points_Awarded int, Other_Points_Awarded int, Points_Redeemed int, Points_Expired int, 
	 SSN varchar(1000), DDA_nums varchar(1000), Card_nums varchar(1000))
Create table #tmpwork
	(field_value varchar(20))

Declare @db		varchar(50)
Declare @sql	nvarchar(4000)

Set @db = (select quotename(dbnamepatton) from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tipfirst)

Set @sql = '
Insert into #tmp 
select	distinct h.tipnumber as TIP, c.Acctname1 as Name1, isnull(c.Acctname2,'''') as Name2, c.address1 as Address1, 
		c.address2 as Address2,	c.city as City, c.[state] as [State], left(c.ZipCode, 5) as Zip, c.homephone as Phone, 
		c.RunAvailable as Current_Point_Balance, 0 as Incentive_Points_Awarded, 0 as Other_Points_Awarded, 0 as Points_Redeemed, 
		0 as Points_Expired, ''Not Set'' as SSN, ''Not Set'' as DDA_nums, ''Not Set'' as Card_nums
from	'+@db+'.dbo.HISTORY h Join '+@db+'.dbo.CUSTOMER c on h.tipnumber = c.tipnumber
where	h.TIPNUMBER in (select TIPNUMBER from '+@db+'.dbo.HISTORY where TRANCODE in (''FE'',''FF''))
order by h.tipnumber

Update #tmp 
set Incentive_Points_Awarded =	(select	isnull(SUM(points*ratio),0) from '+@db+'.dbo.HISTORY 
								 where	HISTORY.TIPNUMBER = #tmp.TIP 
								   and	History.TRANCODE in (''FE'',''FF''))		
Update #tmp 
set Other_Points_Awarded =		(select isnull(SUM(points*ratio),0) from '+@db+'.dbo.HISTORY 
								 where	HISTORY.TIPNUMBER = #tmp.TIP 
								   and	LEFT(history.trancode, 1) not in (''R'',''X'')
								   and	History.TRANCODE not in (''FE'',''FF''))								   
Update #tmp 
set Points_Redeemed =			(select isnull(SUM(points*ratio*-1),0) from '+@db+'.dbo.HISTORY 
								 where	HISTORY.TIPNUMBER = #tmp.TIP 
								   and	LEFT(history.trancode, 1) = ''R'')
Update #tmp 
set Points_Expired =			(select isnull(SUM(points*ratio*-1),0) from '+@db+'.dbo.HISTORY 
								 where	HISTORY.TIPNUMBER = #tmp.TIP 
								   and	LEFT(history.trancode, 1) = ''X'')	
'
Exec sp_executesql @sql

Set @sql = '
Declare @tipnumber varchar(15)
Declare @value varchar(20)
Declare @valuestring varchar(1000)

set @valuestring = ''''

while (select COUNT(SSN) from #tmp where SSN = ''Not Set'') > 0
	begin
	Set @tipnumber = (Select top 1 TIP from #tmp where SSN = ''Not Set'')
	delete from #tmpwork
	insert into #tmpwork
		Select distinct isnull(secid, '''') from '+@db+'.dbo.AFFILIAT where TIPNUMBER = @tipnumber
	delete from #tmpwork where field_value = ''''	
	while (select COUNT(field_value) from #tmpwork) > 0
		begin
		set @value = (Select top 1 field_value from #tmpwork)
		set @valuestring = @valuestring + rtrim(@value) + '', ''
		delete from #tmpwork where field_value = @value
		end
	set @valuestring = LEFT(@valuestring, len(@valuestring)-1)	
	update #tmp set SSN = @valuestring where TIP = @tipnumber
	set @valuestring = ''''
	set @tipnumber = ''''
	end
	
while (select COUNT(DDA_nums) from #tmp where DDA_nums = ''Not Set'') > 0
	begin
	Set @tipnumber = (Select top 1 TIP from #tmp where DDA_nums = ''Not Set'')
	delete from #tmpwork
	insert into #tmpwork
		Select distinct isnull(CustID, '''') from '+@db+'.dbo.AFFILIAT where TIPNUMBER = @tipnumber
	delete from #tmpwork where field_value = ''''
	while (select COUNT(field_value) from #tmpwork) > 0
		begin
		set @value = (Select top 1 field_value from #tmpwork)
		set @valuestring = @valuestring + rtrim(@value) + '', ''
		delete from #tmpwork where field_value = @value
		end
	set @valuestring = LEFT(@valuestring, len(@valuestring)-1)	
	update #tmp set DDA_nums = @valuestring where TIP = @tipnumber
	set @valuestring = ''''
	set @tipnumber = ''''
	end	

while (select COUNT(Card_nums) from #tmp where Card_nums = ''Not Set'') > 0
	begin
	Set @tipnumber = (Select top 1 TIP from #tmp where Card_nums = ''Not Set'')
	delete from #tmpwork
	insert into #tmpwork
		Select distinct isnull(ACCTID, '''') from '+@db+'.dbo.AFFILIAT where TIPNUMBER = @tipnumber
	delete from #tmpwork where field_value = ''''
	while (select COUNT(field_value) from #tmpwork) > 0
		begin
		set @value = (Select top 1 field_value from #tmpwork)
		set @valuestring = @valuestring + rtrim(@value) + '', ''
		delete from #tmpwork where field_value = @value
		end
	set @valuestring = LEFT(@valuestring, len(@valuestring)-1)	
	update #tmp set Card_nums = @valuestring where TIP = @tipnumber
	set @valuestring = ''''
	set @tipnumber = ''''
	end	
'
Exec sp_executesql @sql

Select	TIP, Name1, Name2, Address1, Address2, City, [State], Zip, Phone, Current_Point_Balance,
		Incentive_Points_Awarded, Other_Points_Awarded, Points_Redeemed, Points_Expired,
		SSN, DDA_nums, Card_nums 
from	#tmp

Drop table #tmpwork
Drop table #tmp

GO


