USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_DataCheck_Dynamic_Rollup]    Script Date: 04/27/2010 08:45:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DataCheck_Dynamic_Rollup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DataCheck_Dynamic_Rollup]
GO

USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_DataCheck_Dynamic_Rollup]    Script Date: 04/27/2010 08:45:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[usp_DataCheck_Dynamic_Rollup]
	@tip char(3)

as

Declare @trancode varchar(2)
Declare @dbname varchar(50)
Declare @dbname2 varchar(50)
Declare @sqlcmd nvarchar(4000)
Declare @sqlcmd2 nvarchar (4000)

set	@dbname = (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)
set @dbname2 = @dbname
set @dbname = quotename(@dbname) + '.dbo.'

--Drop and Create audit worktable in WorkOps
if exists(select * from workops.dbo.sysobjects where xtype='u' and name = 'Datacheck_Dynamic')
	Begin
		Drop table [workops].dbo.Datacheck_Dynamic
	End
Create table [workops].dbo.Datacheck_Dynamic (yr int not null, mo int not null, New_Cust int, Del_Cust int, New_Acct int, Del_Acct int)
ALTER TABLE dbo.Datacheck_Dynamic ADD CONSTRAINT
	PK_Datacheck_Dynamic PRIMARY KEY CLUSTERED 
	(
	yr,
	mo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

--Create Worktables
create table #tmprslts (yr int not null, mo int not null, points bigint)
create table #tmprslts2 (yr int not null, mo int not null, points bigint)


create index ix_tmprslts on #tmprslts (yr, mo, points)
create index ix_tmprslts2 on #tmprslts2 (yr, mo, points)
---------------------------------------------------------------------------------------------------------------------
--Set static fields in audit table (Month/Year/Customers Added/Customers Deleted/Accounts Added/Accounts Deleted)
---------------------------------------------------------------------------------------------------------------------

--Set Month and Year (Insert)
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,''0'' as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'history '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(histdate)as yr ,month(histdate) as mo,''0'' as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'historydeleted '
Set @sqlcmd = @sqlcmd + 'group by year(histdate),month(histdate)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into [workops].dbo.Datacheck_Dynamic '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, ''0'' as new_cust, ''0'' as del_cust, ''0'' as new_acct, ''0'' as del_acct '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd

--Set New Customer Count (Update)
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customer '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customerdeleted '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.Datacheck_Dynamic '
Set @sqlcmd = @sqlcmd + 'set new_cust = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.Datacheck_Dynamic d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Deleted Customer Count (Update)
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(datedeleted)as yr ,month(datedeleted) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'customerdeleted '
Set @sqlcmd = @sqlcmd + 'where status <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(datedeleted),month(datedeleted)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.Datacheck_Dynamic '
Set @sqlcmd = @sqlcmd + 'set del_cust = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.Datacheck_Dynamic d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set New Account Count (Update)
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliat '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts '
Set @sqlcmd = @sqlcmd + 'Select year(dateadded)as yr ,month(dateadded) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliatdeleted '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(dateadded),month(dateadded)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select yr, mo, sum(points) as points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts '
Set @sqlcmd = @sqlcmd + 'group by yr ,mo'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.Datacheck_Dynamic '
Set @sqlcmd = @sqlcmd + 'set new_acct = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.Datacheck_Dynamic d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

--Set Deleted Account Count (Update)
truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd = 'insert into #tmprslts2 '
Set @sqlcmd = @sqlcmd + 'Select year(datedeleted)as yr ,month(datedeleted) as mo,count(tipnumber) as points '
Set @sqlcmd = @sqlcmd + 'from '+@dbname+'affiliatdeleted '
Set @sqlcmd = @sqlcmd + 'where acctstatus <> ''X'' '
Set @sqlcmd = @sqlcmd + 'group by year(datedeleted),month(datedeleted)'
  EXECUTE sp_executesql @SqlCmd
Set @sqlcmd = 'Update [workops].dbo.Datacheck_Dynamic '
Set @sqlcmd = @sqlcmd + 'set del_acct = points '
Set @sqlcmd = @sqlcmd + 'from #tmprslts2 t join [workops].dbo.Datacheck_Dynamic d on t.yr = d.yr and t.mo = d.mo'
  EXECUTE sp_executesql @SqlCmd

---------------------------------------------------------------------------------------------------------------------
--Run Curser to set and populate Dynamic table fields
---------------------------------------------------------------------------------------------------------------------

Set @sqlcmd = 
'Declare curtrancode cursor
for Select distinct left(trancode,1) from '+@dbname+'history union Select distinct left(trancode,1) from '+@dbname+'HistoryDeleted order by left(TRANCODE,1)
Declare @trancode varchar(2)
Declare @sqlcmd2 nvarchar (4000)
open curtrancode
Fetch next from curtrancode into @trancode
while @@Fetch_Status = 0
	Begin
	Set @sqlcmd2 = 
''		ALTER TABLE [WorkOps].dbo.Datacheck_Dynamic ADD ['' + @trancode + ''] int NULL ''
	  EXECUTE sp_executesql @SqlCmd2

truncate table #tmprslts
truncate table #tmprslts2
Set @sqlcmd2 = 
'' insert into #tmprslts 
  Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points 
  from '+@dbname+'history 
  where left(trancode,1) = ''''''+@trancode+'''''' 
  group by year(histdate),month(histdate) ''
EXECUTE sp_executesql @SqlCmd2

Set @sqlcmd2 = 
'' insert into #tmprslts 
  Select year(histdate)as yr ,month(histdate) as mo,sum(points*ratio) as points 
  from '+@dbname+'historydeleted 
  where left(trancode,1) = ''''''+@trancode+'''''' 
  group by year(histdate),month(histdate) ''
EXECUTE sp_executesql @SqlCmd2

 insert into #tmprslts2 
  Select yr, mo, sum(points) as points 
  from #tmprslts 
  group by yr ,mo

Set @sqlcmd2 = 
'' Update [workops].dbo.Datacheck_Dynamic 
 Set ['' + @trancode + ''] = points 
 from #tmprslts2 t join [workops].dbo.Datacheck_Dynamic d on t.yr = d.yr and t.mo = d.mo ''
EXECUTE sp_executesql @SqlCmd2

	  fetch Next from curtrancode into @trancode
	end
Close curtrancode
Deallocate curtrancode'
  EXECUTE sp_executesql @SqlCmd

drop table #tmprslts
drop table #tmprslts2

GO


