USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_FBOP_CreditDebitBreakout]    Script Date: 08/31/2010 10:28:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FBOP_CreditDebitBreakout]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FBOP_CreditDebitBreakout]
GO

USE [WorkOps]
GO

/****** Object:  StoredProcedure [dbo].[usp_FBOP_CreditDebitBreakout]    Script Date: 08/31/2010 10:28:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Create procedure [dbo].[usp_FBOP_CreditDebitBreakout]

@tipfirst		char(3)

AS

Create table #tmp
	(TIP varchar(15), Name1 varchar(50), Name2 varchar(50), Address1 varchar(50), Address2 varchar(50),
     City varchar(50), [State] varchar(50), Zip varchar(5), 
	 Current_Point_Balance int, Cust_type varchar(50), Card_nums varchar(1000))
Create table #tmpwork
	(field_value varchar(20))

Declare @db		varchar(50)
Declare @sql	nvarchar(4000)

Set @db = (select quotename(dbnamepatton) from RewardsNOW.dbo.dbprocessinfo where DBNumber = @tipfirst)

Set @sql = '
Insert into #tmp 
select	distinct c.tipnumber as TIP, c.Acctname1 as Name1, isnull(c.Acctname2,'''') as Name2, c.address1 as Address1, 
		c.address2 as Address2,	c.city as City, c.[state] as [State], left(c.ZipCode, 5) as Zip, 
		c.RunAvailable as Current_Point_Balance, ''Not Set'' as Cust_type, ''Not Set'' as Card_nums
from	'+@db+'.dbo.CUSTOMER c
order by c.tipnumber

Update	#tmp 
set		Cust_type =	''Employee''
where	TIP in (Select Tipnumber from '+@db+'.dbo.affiliat where rtrim(acctid) like ''%123456'' or accttype = ''Dummy'') and cust_type = ''Not Set''

--RBM: 9/9/2010: Changed update to include H acctype cards in Debit Only								   
Update #tmp 
set		Cust_type =	''Debit Only'' 
where	TIP not in (Select Tipnumber from '+@db+'.dbo.affiliat where accttype not in (''Debit'',''H'')) and cust_type = ''Not Set''

Update #tmp 
set		Cust_type =	''Debit/Credit'' 
where	cust_type = ''Not Set''
'
Exec sp_executesql @sql

Set @sql = '
Declare @tipnumber varchar(15)
Declare @value varchar(20)
Declare @valuestring varchar(1000)

set @valuestring = ''''

--RBM:  9/9/2010:  Change where clause on Account selection to exclude closed cards
while (select COUNT(Card_nums) from #tmp where Card_nums = ''Not Set'') > 0
	begin
	Set @tipnumber = (Select top 1 TIP from #tmp where Card_nums = ''Not Set'')
	delete from #tmpwork
	insert into #tmpwork
		Select distinct isnull(ACCTID, '''') from '+@db+'.dbo.AFFILIAT where TIPNUMBER = @tipnumber
	delete from #tmpwork where field_value = ''''
	while (select COUNT(field_value) from #tmpwork) > 0
		begin
		set @value = (Select top 1 field_value from #tmpwork)
		set @valuestring = @valuestring + rtrim(@value) + '',''
		set @valuestring = @valuestring + (select accttype from '+@db+'.dbo.affiliat where acctid = @value) + '',''
		delete from #tmpwork where field_value = @value
		end
	set @valuestring = LEFT(@valuestring, len(@valuestring)-1)	
	update #tmp set Card_nums = @valuestring where TIP = @tipnumber
	set @valuestring = ''''
	set @tipnumber = ''''
	end	
'
Exec sp_executesql @sql

Select	TIP, Name1, Name2, Address1, Address2, City, [State], Zip, 
		Current_Point_Balance, Cust_type, Card_nums 
from	#tmp
where	Cust_type = 'employee'

Select	TIP, Name1, Name2, Address1, Address2, City, [State], Zip, 
		Current_Point_Balance, Cust_type, Card_nums 
from	#tmp
where	Cust_type = 'debit only'

Select	TIP, Name1, Name2, Address1, Address2, City, [State], Zip, 
		Current_Point_Balance, Cust_type, Card_nums 
from	#tmp
where	Cust_type = 'debit/credit'


Drop table #tmpwork
Drop table #tmp



GO


