USE [WorkOps]
GO
/****** Object:  StoredProcedure [dbo].[spmetavanteglobalpop]    Script Date: 07/27/2009 15:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[spmetavanteglobalpop]

@tip nvarchar(3), @mdb nvarchar(10) output, @meb nvarchar(10) output, @dbn nvarchar(50) output

as

Set @mdb = (Select monthbeginningdate from Metavante_Global where tipfirst = @tip)
Set @meb = (Select monthendingdate from Metavante_Global where tipfirst = @tip)
Set @dbn = (Select [dbname1] from Metavante_Global where tipfirst = @tip)
GO
