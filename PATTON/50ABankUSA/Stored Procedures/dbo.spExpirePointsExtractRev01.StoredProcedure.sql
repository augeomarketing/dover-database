USE [50ABankUSA]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePointsExtractRev01]    Script Date: 09/24/2009 10:16:16 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[spExpirePointsExtractRev01] @MonthEndDate NVARCHAR(10) 
AS   

declare @DateOfExpire NVARCHAR(10), @ExpireYear char(4)
set @ExpireYear=right( @MonthEndDate, 4)
SET @DateOfExpire = '12/31/' + @ExpireYear
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME


set @ExpireDate = cast(@DateOfExpire as datetime)

SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 2
set @expirationdate = (rtrim(@intYear) + '-' + rtrim(@intmonth) + '-' + rtrim(@intday) +   ' 23:59:59.997')


/*print 'year'
print @intYear
print 'month'
print @intmonth
print 'day'
print @intday
print '@expirationdate'
print @expirationdate */

	TRUNCATE TABLE dbo.dbo.ExpiredPoints

	INSERT   into dbo.ExpiredPoints
	 SELECT tipnumber, sum(points * ratio) as addpoints,
	'0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as PREVEXPIRED, @ExpireDate as dateofexpire
	from history
	where histdate < @expirationdate and (trancode not like('R%') and
	     trancode <> 'IR'and trancode not like('XP'))
	group by tipnumber


	UPDATE dbo.ExpiredPoints  
	SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  ='XP') 
	  	 AND TIPNUMBER = dbo.ExpiredPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		  trancode  ='XP'
	  	 AND TIPNUMBER = dbo.ExpiredPoints.TIPNUMBER) 

	UPDATE dbo.ExpiredPoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = dbo.ExpiredPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = dbo.ExpiredPoints.TIPNUMBER)


	UPDATE dbo.ExpiredPoints  
	SET POINTSTOEXPIRE = (ADDPOINTS + REDPOINTS + PREVEXPIRED),
	    dateofexpire = @ExpireDate


	UPDATE dbo.ExpiredPoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE IS NULL 

	UPDATE dbo.ExpiredPoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE < '0' 

	delete from dbo.ExpiredPoints 	
	WHERE
	POINTSTOEXPIRE = '0'

--update dbo.FBOPQuarterlyStatement
--set PointsToExpire='0'

--update dbo.FBOPQuarterlyStatement
--set PointsToExpire=b.pointstoexpire
--from dbo.FBOPQuarterlyStatement a, dbo.ExpiredPoints b
--where a.tipnumber=b.tipnumber
GO
