USE [50ABankUSA]
GO
/****** Object:  Table [dbo].[Wrongaddress]    Script Date: 09/24/2009 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Wrongaddress](
	[tipnumber] [char](20) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[name1] [varchar](50) NOT NULL,
	[name2] [varchar](50) NULL,
	[address1] [varchar](50) NOT NULL,
	[citystatezip] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
