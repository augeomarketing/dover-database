USE [50ABankUSA]
GO
/****** Object:  Table [dbo].[fixcredit]    Script Date: 09/24/2009 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixcredit](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[points] [numeric](18, 0) NULL,
	[histdate] [datetime] NULL,
	[pointsshouldbe] [int] NOT NULL,
	[pointsdelta] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
