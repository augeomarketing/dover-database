USE [50ABankUSA]
GO
/****** Object:  Table [dbo].[wrkbal]    Script Date: 09/24/2009 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkbal](
	[tipnumber] [varchar](15) NOT NULL,
	[begbal] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
