USE [50ABankUSA]
GO
/****** Object:  Table [dbo].[wrkcustid2]    Script Date: 09/24/2009 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkcustid2](
	[custid] [char](13) NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[email] [char](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
