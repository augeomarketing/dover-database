USE [50ABankUSA]
GO
/****** Object:  Table [dbo].[chktip]    Script Date: 09/24/2009 10:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chktip](
	[acctid] [varchar](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
