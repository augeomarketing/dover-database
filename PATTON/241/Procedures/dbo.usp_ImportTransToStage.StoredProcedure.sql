USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImportTransToStage]    Script Date: 01/25/2012 14:33:25 ******/
DROP PROCEDURE [dbo].[usp_ImportTransToStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date: 1/08 */
/* Author: Rich T  */
/*  **************************************  */
/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals

*/
/*  **************************************  */

CREATE PROCEDURE [dbo].[usp_ImportTransToStage] @TipFirst char(3), @enddate char(10)
AS 

Declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(max) 

/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear from  RewardsNow.dbo.dbprocessinfo where DBNumber = @TipFirst  ) 

/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
 Insert into history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select TIPNumber , AcctID , Trandate, ts.TranCode , Trannum, TranAmt, tt.Description , tt.Ratio, 'NEW', 0 
		From TransStandard ts 
			Join Rewardsnow.dbo.TranType tt 
				on ts.TranCode = tt.TranCode 

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update History_Stage
		set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM History_Stage H JOIN Affiliat_Stage A on  H.ACCTID = A.ACCTID
		where A.YTDEarned + H.Points > @MaxPointsPerYear 
End

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
Update Affiliat_Stage
	set YTDEarned  = A.YTDEarned  + H.Points 
	FROM HISTORY_STAGE H JOIN AFFILIAT_Stage A on H.TIPNUMBER = A.TIPNUMBER 

-- Update History_Stage Points = Points - Overage
Update History_Stage 
	Set Points = Points - Overage where Points > Overage

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update Customer_Stage Set RunAvaliableNew  = 0 where RunAvaliableNew  is null
Update Customer_Stage Set RunAvailable  = 0 where RunAvailable is null
Update Customer_Stage Set RunBalanceNew   = 0 where RunBalanceNew is null

/* Create View if needed */
IF Not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_histpoints]') and OBJECTPROPERTY(id, N'IsView') = 1)
Begin 
	set @SQLStmt = 'Create view vw_histpoints as select tipnumber, sum(points*ratio) as points from history_stage where secid = ''NEW''group by tipnumber'
	exec sp_executesql @SQLStmt
End 


Update customer_stage 
	Set 
		RunAvailable  = RunAvailable + v.Points , 
		RunBalance = RunBalance  + v.Points 
	From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber


Update customer_stage 
	Set 
		RunAvaliableNew  = v.Points ,
		RunBalanceNew = v.Points 
	From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber
GO
