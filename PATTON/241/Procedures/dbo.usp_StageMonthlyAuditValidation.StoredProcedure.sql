USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_StageMonthlyAuditValidation]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_StageMonthlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare	@Tipnumber nchar(15),			@pointsBegin bigint,				@pointsEnd bigint,				
				@PointsPurchasedCR bigint,	@PointsPurchasedDB bigint, 	@pointsBonus bigint,
				@pointsBonusMN bigint,			@pointsAdded bigint,				@pointsIncreased bigint,	
				@pointsRedeemed bigint, 		@pointsReturnedCR bigint,	@pointsReturnedDB bigint,	
				@pointsSubtracted bigint, 	@pointsDecreased bigint,		@errmsg varchar(50),			
				@currentEnd bigint,				@ExpiringPts bigint

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select 
	Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsBonusMN, PointsAdded, PointsIncreased, 
	PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, ExpiringPts
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into 
	@Tipnumber, @PointsBegin, @PointsEnd, @PointsPurchasedCR, @PointsPurchasedDB, @PointsBonus, @PointsBonusMN, @PointsAdded, @PointsIncreased, 
	@PointsRedeemed, @PointsReturnedCR, @PointsReturnedDB, @PointsSubtracted, @PointsDecreased, @ExpiringPts

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
				(Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsBonusMN, PointsAdded, PointsIncreased, 
				PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, ExpiringPts, Errormsg, Currentend				)
       			values
       			(@Tipnumber, @pointsBegin, @pointsEnd, @PointsPurchasedCR , @PointsPurchasedDB , @pointsBonus , @pointsBonusMN , @pointsAdded , @pointsIncreased, 
       				@pointsRedeemed, @pointsReturnedCR, @pointsReturnedDB,  @pointsSubtracted , @pointsDecreased , @ExpiringPts, @errmsg, @currentEnd )

		goto Next_Record
		end
goto Next_Record

Next_Record:
		fetch tip_crsr into 
			@Tipnumber, @PointsBegin, @PointsEnd, @PointsPurchasedCR, @PointsPurchasedDB, @PointsBonus, @PointsBonusMN, @PointsAdded, @PointsIncreased, 
			@PointsRedeemed, @PointsReturnedCR, @PointsReturnedDB, @PointsSubtracted, @PointsDecreased, @ExpiringPts

	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
