USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_SendEmailWelcomeKitsToRN1]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_SendEmailWelcomeKitsToRN1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SendEmailWelcomeKitsToRN1]
	@beginDate DATE
	, @endDate DATE
AS

BEGIN
	SET @beginDate = ISNULL(@beginDate, rewardsnow.dbo.ufn_getfirstofprevmonth(getdate()))
	SET @endDate = ISNULL(@endDate, rewardsnow.dbo.ufn_getlastofmonth(@beginDate))

	INSERT INTO RN1.rewardsnow.dbo.emailwelcomekit 
	(
		dim_emailwelcomekit_tipnumber
		, dim_emailwelcomekit_email
		, dim_emailwelcomekit_segment
	)
	SELECT distinct 
		fic.TIPNUMBER as dim_emailwelcomekit_tipnumber
		, lower(rnic.dim_RNICustomer_EmailAddress) as dim_emailwelcomekit_email
		, fic.EmployeeFlag dim_emailwelcomekit_segement
	FROM [241].dbo.CUSTOMER fic
	INNER JOIN Rewardsnow.dbo.RNICustomer rnic
		ON fic.TIPNUMBER = rnic.dim_RNICustomer_rniid
	LEFT OUTER JOIN RN1.rewardsnow.dbo.emailwelcomekit wk
		ON fic.TIPNUMBER = wk.dim_emailwelcomekit_tipnumber
	WHERE wk.dim_emailwelcomekit_tipnumber IS NULL
		AND ISNULL(rnic.dim_RNICustomer_EmailAddress, '') <> ''
		AND CONVERT(DATE, fic.DATEADDED) BETWEEN @beginDate and @endDate
	ORDER BY fic.EmployeeFlag, fic.TIPNUMBER	
END
GO
