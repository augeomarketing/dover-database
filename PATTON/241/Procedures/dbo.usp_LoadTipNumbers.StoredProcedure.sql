USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadTipNumbers]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_LoadTipNumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*********** Revisions *********************
 Get existing tip from affiliat table by 
	1. Card number
	2. SSN
	3. DDA  
House Hold on 
	1. SSN
	2. DDA  (if no SSN) 
	If No SSN or DDA write exception record.

 Combine all names with like Tipnumbers
 execute usp_SetAddressToPrimaryRecord
 execute usp_LoadRNICustomer 
	

*****************************************/

CREATE  PROCEDURE [dbo].[usp_LoadTipNumbers] @TipPrefix char(3) AS

Declare @NewTip bigint

/* Update Tip from Affiliat on Card number */ 
Update Input_Customer 
	Set  Input_Customer.TipNumber = A.Tipnumber
	From Affiliat_stage a join Input_Customer i 
		on a.ACCTID  = i.[CardNumber]
		where a.AcctType ='Debit'
		and ( i.TipNumber is NULL or i.TipNumber = '') 

/* Update Tipnumber Based on SSN in Affiliat */ 
Update Input_Customer 
	Set  Input_Customer.TipNumber = A.Tipnumber
	From Affiliat_stage a join Input_Customer i 
		on a.ACCTID  = i.[PrimaryId]
		where a.AcctType ='TaxID'
		and ( i.TipNumber is NULL or i.TipNumber = '') 

/* Update Tipnumber Based on DDA in Affiliat 
	ONLY IF INPUT_CUSTOMER PrimaryID (ssn)  = 000000000 */ 
Update Input_Customer 
	Set  Input_Customer.TipNumber = A.Tipnumber
	From Affiliat_stage a join Input_Customer i 
		on a.ACCTID  = i.DDA
		where a.AcctType ='DDA'
		and ( i.TipNumber is NULL or i.TipNumber = '') 
		and i.PrimaryId = '000000000'


exec rewardsnow.dbo.spGetLastTipNumberUsed @TipPrefix, @NewTip output
-- select @NewTip as LastTipUsed

If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'

/********************* HOUSE HOLD BY SSN *********************/
-- Create Temp table with TipNumber and PrimaryID (household = PrimaryID)
Select distinct TipNumber , PrimaryId 
	Into #HouseHold_SSN
	From Input_Customer 
		Where ( TipNumber is Null or TipNumber = '' ) 
			And ( PrimaryId is not null And PrimaryId <> '000000000' And PrimaryId <> '')

/* Assign Get New Tips to Householded Temp table */
UPDATE  #HouseHold_SSN
	Set  @NewTip = ( @NewTip + 1 ),
	      TipNumber =  @NewTip 
	Where TipNumber is null or TipNumber = '' 

-- Update Input_Customer from HouseHold Table. 
Update Input_Customer 
	Set TipNumber = hh.TipNumber
	From #HouseHold_SSN hh, Input_Customer ic 
		Where hh.PrimaryID = ic.PrimaryID


/********************* HOUSE HOLD BY DDA if no SSN *********************/
-- Create Temp table with TipNumber and DDA (household = PrimaryID)
Select distinct TipNumber , DDA 
	Into #HouseHold_DDA
	From Input_Customer 
		Where ( TipNumber is Null or TipNumber = '' ) 
			And ( PrimaryId is null or PrimaryId = '000000000' or PrimaryId = '')

/* Update the #HouseHold_DDA with tipnumbers joined to Input_Customer by DDA */
update #HouseHold_DDA  
set TipNumber = ic.tipnumber 
from #HouseHold_DDA hh , Input_Customer ic 
		where hh.dda = ic.dda
		and ( ic.TipNumber is not null or ic.TipNumber <> '' ) 
		and hh.TipNumber is null 

/* Assign Get New Tips to Householded Temp table */
UPDATE  #HouseHold_DDA
	Set  @NewTip = ( @NewTip + 1 ),
	      TipNumber =  @NewTip 
		Where TipNumber is null or TipNumber = '' 

-- Update Input_Customer from HouseHold Table. 
Update Input_Customer 
	Set TipNumber = hh.TipNumber
	From #HouseHold_DDA hh, Input_Customer ic 
		Where hh.DDA = ic.DDA 

-- Update LastTip -- 
exec rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip 

--- Link all names with like Tipnumbers
Declare @Tip char(15), @Name1 varchar(40), @Name2 varchar(40), @Name3 varchar(40), @Name4 varchar(40)
Declare tip_crsr cursor
	For select distinct Tipnumber from Input_Customer 
Open tip_crsr
fetch tip_crsr into @Tip

If @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
While @@FETCH_STATUS = 0
	BEGIN	
	
	Update Input_Customer 
	set NAME1 = ln.Name1 ,
		NAME2  = ln.Name2,
		Name3 = ln.Name3,
		Name4 = ln.Name4
	 From  ufn_LinkNames ( 't', @Tip  ) as ln 
	 Where Tipnumber = @Tip 
			
		fetch tip_crsr into @Tip

	END 

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

-- If a Customer has more than one address on a Tipnumber set it to the primary record. 
execute usp_SetAddressToPrimaryRecord

-- ReLoad Rewardsnow.dbo.RNICustomer with 241 tips
execute usp_LoadRNICustomer
GO
