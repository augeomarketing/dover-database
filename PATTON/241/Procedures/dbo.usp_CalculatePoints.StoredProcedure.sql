USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_CalculatePoints]    Script Date: 09/29/2015 08:43:01 ******/
DROP PROCEDURE [dbo].[usp_CalculatePoints]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/*  
	Loads Input_Transaction and 
	Calculates points in input_transaction  
	Calls [241].dbo.usp_PopulateTransStandardFromRawData 
	Calls [241].dbo.usp_RollUpTransStandard 
	Revisions:
		RDT-1: 11/28/2011 Birthday and Estatment bonuses changed to join on SSN Then Member Number - Requested by FI -
		RDT 1/11/2012 Query Optimization - replace status update with new SQL
								Removed ufn_PadSSN -- this was moved into the vew
*/
/*  **************************************  */


/********************************************/
/*  SEB 9/2015                              */
/*  Changes to handle new bonuses and comment out old ones  */
/*  Fix issue where it's matching Member numbers to other FIs */
/********************************************/


CREATE PROCEDURE [dbo].[usp_CalculatePoints]  @EndDate DateTime  AS   


--- This sproc truncates the transStandard table 
	 execute [241].dbo.usp_PopulateTransStandardFromRawData   @EndDate


----- Temp table to hold Primary tipnumber and member number. 
Declare @Tip_Member as TABLE 
(
	dim_rnicustomer_rniid VARCHAR(15)
	, dim_rnicustomer_member VARCHAR(20)
)

--INSERT SINGLES
Insert into @Tip_Member (dim_rnicustomer_rniid, dim_rnicustomer_member)
	Select  dim_RNICustomer_RNIId, dim_RNICustomer_Member 
		From Rewardsnow.dbo.RNICustomer
			where dim_RNICustomer_TipPrefix = '241' 
			and dim_RNICustomer_PrimaryIndicator = 1
		group by 	dim_RNICustomer_Member, dim_RNICustomer_RNIId
		having COUNT(*) = 1

--INSERT MULTIPLE PRIMARY Count > 1 
Insert into @Tip_Member (dim_rnicustomer_rniid, dim_rnicustomer_member)
	SELECT rnic.dim_rnicustomer_rniid, rnic.dim_RNICustomer_Member
		FROM
		Rewardsnow.dbo.rnicustomer rnic
		inner join 
			(
				select  dim_RNICustomer_Member, min(sid_rnicustomer_id) as sid_rnicustomer_id
				from Rewardsnow.dbo.RNICustomer
				where dim_RNICustomer_TipPrefix = '241' 
					and dim_RNICustomer_PrimaryIndicator = 1
				group by 	dim_RNICustomer_Member
								, dim_RNICustomer_RNIId
				having COUNT(*) > 1
			) mult
		on rnic.dim_RNICustomer_Member = mult.dim_RNICustomer_Member
			and rnic.sid_RNICustomer_ID = mult.sid_rnicustomer_id

--INSERT Single PRIMARY 
Insert into @Tip_Member (dim_rnicustomer_rniid, dim_rnicustomer_member)
	SELECT rnic.dim_RNICustomer_RNIId, rnic.dim_RNICustomer_Member
		 FROM Rewardsnow.dbo.RNICustomer rnic
		inner join 
			(
				SELECT rnic.dim_RNICustomer_Member
				FROM
					Rewardsnow.dbo.RNICustomer rnic
				LEFT OUTER JOIN
					@Tip_Member haves
				ON rnic.dim_RNICustomer_Member = haves.dim_RNICustomer_Member
				WHERE haves.dim_RNICustomer_RNIId is null
					and rnic.dim_RNICustomer_TipPrefix = '241'
				GROUP BY rnic.dim_RNICustomer_Member
				HAVING COUNT(*) = 1
			) single0
	on rnic.dim_RNICustomer_Member = single0.dim_RNICustomer_Member
/* SEB 9/2015 */ where rnic.sid_dbprocessinfo_dbnumber='241'

--dupes on non-primary only

Insert into @Tip_Member (dim_rnicustomer_rniid, dim_rnicustomer_member)
	Select dim_rnicustomer_rniid, dim_rnicustomer_member
		from Rewardsnow.dbo.rnicustomer rnic
		inner join
			(
				select MIN(sid_rnicustomer_id) as sid_rnicustomer_id 
				from Rewardsnow.dbo.RNICustomer r
				left outer join @Tip_Member t
				ON r.dim_RNICustomer_Member = t.dim_rnicustomer_member
				where t.dim_rnicustomer_member is null
				and dim_RNICustomer_TipPrefix = '241'
				group by r.dim_rnicustomer_member
			) mindupe0
		on rnic.sid_RNICustomer_ID = mindupe0.sid_rnicustomer_id

--- --- --- --- vw_241_BNS_TARGET_1 = Relationship bonuses --- --- --- --- 
Insert into TransStandard 
(	TIPNumber, 	TranDate, 	AcctID, 	TranCode, 	TranType, 	TranNum, 
	TranAmt, 	Ratio) 
Select  
	tm.dim_rnicustomer_rniid , 	 	@EndDate,	vww.Cardnumber ,	xr.sid_trantype_TranCode, 		tt.Description ,	1, 
	( convert ( integer, vww.AdjustAmount  ) * xr.dim_rnitrantypexref_factor) , 	 tt.Ratio 

/* SEB 9/2015	From RewardsNow.dbo.vw_241_BNS_TARGET_1 vww  */ 
/* SEB 9/2015 */ From RewardsNow.dbo.vw_241_BNS_TARGET_100 vww 

		Join RewardsNow.dbo.rniTrantypeXref xr 
			on vww.BonusFileType = xr.dim_rnitrantypexref_code01 
			and vww.AdjustDescription like xr.dim_rnitrantypexref_code02 

		Join @Tip_Member tm 
			on  vww.AccountID =  tm.dim_rnicustomer_member

		Join Rewardsnow.dbo.TranType tt 
			on tt.TranCode= xr.sid_trantype_trancode 
		where xr.sid_dbprocessinfo_dbnumber = '241'
		and vww.sid_rnirawimportstatus_id = 0 

/* RDT 1/11/2012
----Set the status indicator to "imported" on View
update RewardsNow.dbo.vw_241_BNS_TARGET_1  
	set sid_rnirawimportstatus_id =  1  
	where 
			BonusFileType in ( select  dim_rnitrantypexref_code01 from RewardsNow.dbo.rniTrantypeXref where sid_dbprocessinfo_dbnumber ='241')
		and 
			AccountID in ( select AcctID  from TransStandard  Group by AcctID ) 
		and 
			sid_rnirawimportstatus_id =  0  
- Update the status indicator to "imported" for ALL vw_241_BNS_TARGET_1 Bonuses
*/

/* SEB 9/2015 Update  RewardsNow.dbo.vw_241_BNS_TARGET_1 */ 
/* SEB 9/2015 */ Update  RewardsNow.dbo.vw_241_BNS_TARGET_100 
	Set sid_rnirawimportstatus_id = 1 
		From rewardsnow.dbo.vw_241_bns_target_100 bns join (SELECT distinct dim_rnitrantypexref_code01
				FROM rewardsnow.dbo.rnitrantypexref ttx join [Rewardsnow].[dbo].[vw_RNITrancodeMap] vw
					on ttx.sid_trantype_trancode = vw.sid_trantype_trancode
					where dim_rnitrancodegroup_name = 'INCREASE_BONUS'
						and sid_dbprocessinfo_dbnumber = '241') ttx
	on bns.bonusfiletype = ttx.dim_rnitrantypexref_code01
	
	join (select acctid from dbo.transstandard group by acctid) ts 
	on  AccountID = ts.acctid
	
	where sid_rnirawimportstatus_id =  0  


/* SEB 9/2015 Start here
 --- --- --- --- --- vw_241_BNS_TARGET_2  = Mortgage #2  --- --- --- --- 
Insert into TransStandard 
	( TIPNumber, 	TranDate, 	AcctID,  	TranCode, 	TranType, 	TranNum,
 	TranAmt, 	Ratio ) 
Select  
	afs.TipNumber, 	 @EndDate , 	vww.SSN  , 	xr.sid_trantype_TranCode, 	 tt.Description, 	1, 
	 ( convert ( integer, vww.transactionAmount ) * xr.dim_rnitrantypexref_factor) , 	 tt.Ratio  

	From RewardsNow.dbo.vw_241_BNS_TARGET_2 vww 

		Join RewardsNow.dbo.rniTrantypeXref xr 
			on vww.BonusFileType = xr.dim_rnitrantypexref_code01 

		Join AFFILIAT_Stage afs
			on  vww.SSN  =  afs.ACCTID 

		Join Rewardsnow.dbo.TranType tt 
			on tt.TranCode= xr.sid_trantype_trancode 

	where 	xr.sid_dbprocessinfo_dbnumber = '241'
			and vww.BonusFileType = 2 
			and vww.sid_rnirawimportstatus_id = 0 
-----------------------------------MATCH BY SSN ------------------------------------------------------			
 --- --- --- --- --- vw_241_BNS_TARGET_2  = Estatement #3 - MATCH BY SSN  --- --- --- --- 
Insert into TransStandard 
	( TIPNumber, 	TranDate, 	AcctID, 	TranCode, 	TranType, 	TranNum,
 	TranAmt, 	Ratio ) 
Select  
	afs.TipNumber, 	 @EndDate , 	 vww.SSN  , 	xr.sid_trantype_TranCode, 	 tt.Description, 	1, 
	 ( convert ( integer, vww.transactionAmount ) * xr.dim_rnitrantypexref_factor) , 	 tt.Ratio  

	From RewardsNow.dbo.vw_241_BNS_TARGET_2 vww 
		Join RewardsNow.dbo.rniTrantypeXref xr 
			on vww.BonusFileType = xr.dim_rnitrantypexref_code01 
		Join AFFILIAT_Stage afs
			on  vww.SSN  =  afs.ACCTID 
		Join Rewardsnow.dbo.TranType tt 
			on tt.TranCode= xr.sid_trantype_trancode 
	where 	xr.sid_dbprocessinfo_dbnumber = '241'
			and vww.BonusFileType = 3
			and vww.sid_rnirawimportstatus_id = 0 
			and afs.AcctType = 'TAXID'												

--- --- --- --- --- vw_241_BNS_TARGET_2  = Birthday #4 bonuses MATCH BY SSN --- --- --- --- 
Insert into TransStandard 
	( TIPNumber, 	TranDate, 	AcctID, 	TranCode, 	TranType, 	TranNum, 	
	TranAmt, 	Ratio ) 
Select  
	afs.TipNumber,  @EndDate , vww.SSN  ,	xr.sid_trantype_TranCode,  tt.Description, 	1, 
	 ( convert ( integer, vww.transactionAmount ) * xr.dim_rnitrantypexref_factor) , 	 tt.Ratio  

	From RewardsNow.dbo.vw_241_BNS_TARGET_2 vww 
		Join RewardsNow.dbo.rniTrantypeXref xr 
			on vww.BonusFileType = xr.dim_rnitrantypexref_code01 
		Join AFFILIAT_Stage afs
			on  vww.SSN  =  afs.ACCTID  
		Join Rewardsnow.dbo.TranType tt 
			on tt.TranCode= xr.sid_trantype_trancode 
	where 	xr.sid_dbprocessinfo_dbnumber = '241'
		and vww.BonusFileType = 4
		and vww.sid_rnirawimportstatus_id = 0 
			and afs.AcctType = 'TAXID'												

/* RDT 1/11/2012
Update  RewardsNow.dbo.vw_241_BNS_TARGET_2 
set sid_rnirawimportstatus_id = 1 
where 
		BonusFileType in ( select  dim_rnitrantypexref_code01 from RewardsNow.dbo.rniTrantypeXref where sid_dbprocessinfo_dbnumber ='241')
		and 
		 [241].dbo.ufn_PadSSN(SSN) in ( select acctid from TransStandard  )			
		and 
		sid_rnirawimportstatus_id =  0  
*/
--ss
Update  RewardsNow.dbo.vw_241_BNS_TARGET_2 
	Set sid_rnirawimportstatus_id = 1 
		From rewardsnow.dbo.vw_241_bns_target_2 bns join (SELECT distinct dim_rnitrantypexref_code01
				FROM rewardsnow.dbo.rnitrantypexref ttx join [Rewardsnow].[dbo].[vw_RNITrancodeMap] vw
					on ttx.sid_trantype_trancode = vw.sid_trantype_trancode
					where dim_rnitrancodegroup_name = 'INCREASE_BONUS'
						and sid_dbprocessinfo_dbnumber = '241') ttx
	on bns.bonusfiletype = ttx.dim_rnitrantypexref_code01
	
	join (select acctid from dbo.transstandard group by acctid) ts 
	on ssn = ts.acctid
	
	where sid_rnirawimportstatus_id =  0  
-- End here  */




-----------------------------------MATCH BY MEMBER NUMBER------------------------------------------------------			
--- --- --- --- --- --- vw_241_BNS_TARGET_2  = Estatement #3 - MATCH BY MEMBER NUMBER--- --- --- --- --- 
Insert into TransStandard 
	( TIPNumber, 	TranDate, 	AcctID, 	TranCode, 	TranType, 	TranNum,
	TranAmt, 	Ratio ) 
Select  
	afs.TipNumber, 	 @EndDate , 	vww.MemberNumber ,	xr.sid_trantype_TranCode, 		 tt.Description, 	1, 
	 ( convert ( integer, vww.transactionAmount ) * xr.dim_rnitrantypexref_factor) , 	 tt.Ratio  

	From RewardsNow.dbo.vw_241_BNS_TARGET_2 vww 
		Join RewardsNow.dbo.rniTrantypeXref xr 
			on vww.BonusFileType = xr.dim_rnitrantypexref_code01 
		Join AFFILIAT_Stage afs
			on  vww.MemberNumber  =  afs.ACCTID 
		Join Rewardsnow.dbo.TranType tt 
			on tt.TranCode= xr.sid_trantype_trancode 
	where 	xr.sid_dbprocessinfo_dbnumber = '241'
			and vww.BonusFileType = 3
			and vww.sid_rnirawimportstatus_id = 0 
			and afs.AcctType = 'Member'												


/* SEB 9/2015  Start here
--- --- --- --- --- vw_241_BNS_TARGET_2  = Birthday #4 bonuses MATCH BY MEMBERNUMBER --- --- --- --- --- 
Insert into TransStandard 
	( TIPNumber, 	TranDate, 	AcctID, 	TranCode, 	TranType, 	TranNum, 	
	TranAmt, 	Ratio ) 
Select  
	afs.TipNumber,	 	@EndDate , 	vww.MemberNumber,	xr.sid_trantype_TranCode, 		 tt.Description, 	1, 
	 ( convert ( integer, vww.transactionAmount ) * xr.dim_rnitrantypexref_factor) , 
	 tt.Ratio  
	From RewardsNow.dbo.vw_241_BNS_TARGET_2 vww 
		Join RewardsNow.dbo.rniTrantypeXref xr 
			on vww.BonusFileType = xr.dim_rnitrantypexref_code01 
		Join AFFILIAT_Stage afs
			on  vww.MemberNumber  =  afs.ACCTID 
		Join Rewardsnow.dbo.TranType tt 
			on tt.TranCode= xr.sid_trantype_trancode 
	where 	xr.sid_dbprocessinfo_dbnumber = '241'
		and vww.BonusFileType = 4
		and vww.sid_rnirawimportstatus_id = 0 
			and afs.AcctType = 'DDA'												
			
/*  RDT 01/11/2012 
--- Update the status indicator to "imported" 
Update  RewardsNow.dbo.vw_241_BNS_TARGET_2 
set sid_rnirawimportstatus_id = 1 
where 
				BonusFileType in ( select  dim_rnitrantypexref_code01 from RewardsNow.dbo.rniTrantypeXref where sid_dbprocessinfo_dbnumber ='241')
			and 
				MemberNumber in ( select acctid from TransStandard group by AcctID ) 
			and 
				sid_rnirawimportstatus_id =  0  
*/

--- Update the status indicator to "imported" for ALL vw_241_BNS_TARGET_2 Bonuses
Update  RewardsNow.dbo.vw_241_BNS_TARGET_2 
	Set sid_rnirawimportstatus_id = 1 
		From rewardsnow.dbo.vw_241_bns_target_2 bns join (SELECT distinct dim_rnitrantypexref_code01
				FROM rewardsnow.dbo.rnitrantypexref ttx join [Rewardsnow].[dbo].[vw_RNITrancodeMap] vw
					on ttx.sid_trantype_trancode = vw.sid_trantype_trancode
					where dim_rnitrancodegroup_name = 'INCREASE_BONUS'
						and sid_dbprocessinfo_dbnumber = '241') ttx
	on bns.bonusfiletype = ttx.dim_rnitrantypexref_code01
	
	join (select acctid from dbo.transstandard group by acctid) ts 
	on MemberNumber = ts.acctid
	
	where sid_rnirawimportstatus_id =  0  
-----------------------------	
 End here  */



	--- Roll up the transactions into one per type.
	execute [241].dbo.[usp_RollUpTransStandard]
GO
