USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_SetAddressToPrimaryRecord]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_SetAddressToPrimaryRecord]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*********** Revisions *********************
Customers may have a different address linked 
to each card. This will set all addresses to the primary card. 
If no primary card exists or multiple primary cards exist the
first card is used. First = order of records.
*****************************************/
 
CREATE  PROCEDURE [dbo].[usp_SetAddressToPrimaryRecord] as 

declare @T_Customer as Table ( 
	[RowID] [bigint] IDENTITY(1,1) ,
	[PortfolioId] [varchar](20) ,
	[MemberId] [varchar](20) ,
	[PrimaryId] [varchar](20) ,
	[DDA] [varchar](20) ,
	[PrimaryFlag] [int] ,
	[NAME1] [varchar](40)  ,
	[NAME2] [varchar](40) ,
	[NAME3] [varchar](40) ,
	[NAME4] [varchar](40) ,
	[ADDRESS1] [varchar](40)  ,
	[ADDRESS2] [varchar](40) ,
	[ADDRESS3] [varchar](40) ,
	[City] [varchar](40)  ,
	[State] [varchar](3)  ,
	[Country] [varchar](3)  ,
	[PostalCode] [varchar](20)  ,
	[PrimaryPhone] [varchar](20) ,
	[MobilePhone] [varchar](20) ,
	[CustomerCode] [int]  ,
	[BusinessFlag] [int] ,
	[EmployeeFlag] [int] ,
	[InstitionID] [varchar](20) ,
	[CardNumber] [varchar](16) ,
	[Email] [varchar](254) ,
	[CustomerType] [int] ,
	[TipNumber] [varchar](15) 
) 

Insert  into @T_Customer 
	select ic1.*
	from Input_Customer ic1 
	join Input_Customer ic2 on ic1.TipNumber = ic2.TipNumber 
	where 
		ic1.ADDRESS1 <> ic2.ADDRESS1 OR 
		ic1.ADDRESS2	<> ic2.ADDRESS2 OR  
		ic1.ADDRESS3	<> ic2.ADDRESS3 OR   
		ic1.City				<> ic2.City  OR 
		ic1.State			<> ic2.State  OR 
		ic1.Country		<> ic2.Country  OR 
		ic1.PostalCode	<> ic2.PostalCode  OR 
		ic1.PrimaryPhone	<> ic2.PrimaryPhone  OR 
		ic1.MobilePhone	<> ic2.MobilePhone	OR 
		ic1.CustomerCode	<> ic2.CustomerCode  OR 
		ic1.BusinessFlag	<> ic2.BusinessFlag	OR 	
		ic1.EmployeeFlag	<> ic2.EmployeeFlag  OR 
		ic1.InstitionID		<> ic2.InstitionID 	 OR 
		ic1.Email				<> ic2.Email 
	order by ic1.PrimaryFlag desc


Declare @T_RowTip as Table ( RowId BigInt, Tipnumber char(15) ) 

Insert into @T_RowTip 
	Select 	min ( RowID ) as rowrow , tc.TipNumber 
		From @T_Customer tc 
		Group by tc.TipNumber 
		Order by rowrow
	
Update Input_Customer 
set 
	ADDRESS1 = tc.ADDRESS1 ,	
	ADDRESS2 = tc.ADDRESS2 , 
	ADDRESS3 = tc.ADDRESS3 , 
	City = tc.City , 
	State = tc.State ,	
	Country = tc.Country ,
	PostalCode = tc.PostalCode ,	
	PrimaryPhone = tc.PrimaryPhone ,	
	MobilePhone = tc.MobilePhone ,	
	CustomerCode = tc.CustomerCode , 	
	BusinessFlag = tc.BusinessFlag ,	
	EmployeeFlag = tc.EmployeeFlag ,	
	InstitionID = tc.InstitionID ,	
	Email = tc.Email
	from		Input_Customer ic 
		Join 	@T_Customer tc on tc.Tipnumber = ic.TipNumber 
		Join 	@T_RowTip tr on tr.RowId  = tc.RowID
GO
