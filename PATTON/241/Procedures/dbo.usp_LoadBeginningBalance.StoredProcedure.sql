USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadBeginningBalance]    Script Date: 01/25/2012 14:33:25 ******/
DROP PROCEDURE [dbo].[usp_LoadBeginningBalance]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_LoadBeginningBalance] @MonthBeginDate datetime 
AS 

Declare @MonthBucket char(10), @MonthBegin char(2), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)

Set @MonthBegin= month(@MonthBeginDate) + '1'

If CONVERT( int , @MonthBegin)='13' 
	begin
		set @monthbegin='1'
	end	

set @MonthBucket='MonthBeg' + @monthbegin

set @SQLUpdate=N'update Beginning_Balance_Table set '+ Quotename(@MonthBucket) + N'= (select pointsend from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber) where exists(select * from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber)'

set @SQLInsert=N'insert into Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') select tipnumber, pointsend from Monthly_Statement_File where not exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert
GO
