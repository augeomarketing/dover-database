USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_PostStageToProd]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_PostStageToProd]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[usp_PostStageToProd] @TipFirst char(3)
AS 

Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 


/*    set DbProcessInfo to ='N' */
Update RewardsNow.dbo.dbprocessinfo 
set DBAvailable = 'N' 
where DBNumber = @TipFirst 


----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned, Lastname and CUSTID from Affiliat_Stage
Update Affiliat 
set	YTDEarned = S.YTDEarned,
		LastName	= S.LastName,	
		CustID		= S.CustID
	From Affiliat_Stage S join Affiliat A 
		on S.Tipnumber = A.Tipnumber

--	Insert New Affiliat accounts
Insert into Affiliat 
		( ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID) 
	Select  
		  ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID 
	From Affiliat_Stage 
			where AcctID not in (select AcctID from Affiliat )


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
Update Customer 
Set 
		Laststmtdate			= S.Laststmtdate , 
		Nextstmtdate		= S.Nextstmtdate , 
		Status						= S.Status , 
		Lastname					= S.Lastname , 
		Acctname1 				= S.Acctname1 , 
		Acctname2 				= S.Acctname2 ,
		Acctname3 				= S.Acctname3 , 
		Acctname4 				= S.Acctname4 , 
		Acctname5 				= S.Acctname5 , 
		Acctname6 				=  S.Acctname6 ,
		Address1 				=  S.Address1 , 
		Address2 				= S.Address2 , 
		Address3 				= S.Address3 , 
		Address4 				= RTrim( S.City ) + ' '+ RTrim( S.State ) + ' ' + RTrim( S.ZipCode ),
		City 							= S.City , 
		State 						= S.State , 
		Zipcode 					= S.Zipcode , 
		Statusdescription	= S.Statusdescription , 
		Homephone 				= S.Homephone ,
		Workphone 				= S.Workphone , 
		Businessflag			 = S.Businessflag , 
		Employeeflag 			= S.Employeeflag , 
		Segmentcode 			= S.Segmentcode ,
		Combostmt 				= S.Combostmt , 
		Rewardsonline 		= S.Rewardsonline , 
		Notes 						= S.Notes , 
		Bonusflag 				= S.Bonusflag , 
		Misc1 = S.Misc1 , Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
	From Customer_Stage S Join Customer C 
		On S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update Customer_Stage 	Set RunAvailable = 0 

--	Insert New Customers from Customers_Stage
Insert into Customer 
	( TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, 
	TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, 
	ADDRESS4, 
	City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
	NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew) 
	Select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, 
		TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, 
		RTrim( City ) + ' '+ RTrim( State ) + ' ' + RTrim( ZipCode ), 
		City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
		NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	From Customer_Stage where Tipnumber not in (select Tipnumber from Customer )

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
Update Customer 
	Set RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
	From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
Insert Into History 
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage )
	Select 
		TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	From History_Stage where SecID = 'NEW'

-- Set SecID in History_Stage so it doesn't get double posted. 
Update History_Stage  
	set SECID = 'Posted '+ convert(char(20), GetDate(), 120)  
	where SecID = 'NEW'

----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode are not found in production table.
Insert into OneTimeBonuses 
( TipNumber, Trancode, AcctID, DateAwarded)
Select  
	TipNumber, Trancode, AcctID, DateAwarded
	From OneTimeBonuses_stage 
		Where not exists
			( select * from OneTimeBonuses 
				where OneTimeBonuses_stage.Tipnumber = OneTimeBonuses.Tipnumber and 
						   OneTimeBonuses_stage.Trancode   = OneTimeBonuses.Trancode)

--- Truncate Stage Tables so's we don't double post.
truncate table Customer_Stage
truncate table Affiliat_Stage
truncate table History_Stage
truncate table OneTimeBonuses_stage

execute usp_LoadRNICustomer
GO
