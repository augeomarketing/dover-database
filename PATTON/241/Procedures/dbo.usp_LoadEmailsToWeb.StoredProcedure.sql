USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadEmailsToWeb]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_LoadEmailsToWeb]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/*  
	Loads Emails to web from Input_Customer
*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[usp_LoadEmailsToWeb]  AS   

Update rn1.Enterprise.dbo.[1security] 
Set s1.Email = ic.Email 
	From 
		Input_Customer ic Join rn1.enterprise.dbo.[1security] s1
		on ic.TipNumber = s1.Tipnumber 
			where ( s1.Email is null or len( rTrim( s1.Email )) < 1  ) 
				and (  ic.Email is not null and LEN(rtrim(ic.email)) > 5 )
GO
