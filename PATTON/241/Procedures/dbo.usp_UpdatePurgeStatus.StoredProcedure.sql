USE [241]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[usp_InputScrub]    Script Date: 07/24/2012 15:44:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdatePurgeStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdatePurgeStatus]
GO


/******************************************************************************/
/*    This Updates the status in affiliat and customer based on age of accounts. */
/* BY:  R.Tremblay  2012/07 */
/***
		This should be called AFTER the monthly processing has been posted to the production tables
		and before posting to the web. 
		I didn't add the tipnumber because of combines.
***/ 
/******************************************************************************/	

CREATE  PROCEDURE [dbo].[usp_UpdatePurgeStatus] @tipFirst char(3), @EndDate Date
AS

/********** Load rniPurge with new accounts **********/
Insert into rniPurge
	(dim_rniPurge_TipFirst, dim_rniPurge_AccountType , dim_rniPurge_Account , dim_rniPurge_DateAdded, dim_rniPurge_DateRecent)
Select 
	left(af.TIPNUMBER ,3) , af.AcctType, af.ACCTID , @EndDate , max(dim_rnirawimport_processingenddate) 
	From Rewardsnow.dbo.vw_241_ACCT_TARGET_226 ri left outer join AFFILIAT af on af.ACCTID = ri.CardNumber  
		where af.AcctId is null 
		group by left(af.TIPNUMBER,3) , af.AcctType, af.ACCTID 
	
/**********  Update the rniPurge table with recient dates  **********/
Update rniPurge 
Set dim_rniPurge_DateRecent = hi.MaxHist
	From rniPurge rP 
		join ( Select CardNumber , Max(dim_rnirawimport_processingenddate) as MaxHist 
					From Rewardsnow.dbo.vw_241_ACCT_TARGET_226 --with (rowlock) 
					group by CardNumber )  as hi
		on hi.CardNumber = rP.dim_rniPurge_Account 
		
/**********  set the affiliat status  **********/
Update AFFILIAT 
	Set AcctStatus = 'C' 
	Where ACCTID in 
	( 
		Select dim_rniPurge_account
		From rniPurge rp join affiliat af on rp.dim_rniPurge_Account = af.acctid 
		Where DATEDIFF( month, dim_rniPurge_DateRecent, @EndDate ) > 
			( Select ClosedMonths -1 from RewardsNow.dbo.dbprocessinfo where DBNumber = @tipFirst ) 
	) 
	and AcctStatus <> 'C'

/**********  set the customer status  to the Min status in the Affiliat (A = min) **********/
	Update  cst 
	Set status =  af.MinStat 
	From Customer cst 
		Join 	( select tipnumber , AcctType , MIN (acctstatus) as MinStat from AFFILIAT group by TIPNUMBER , AcctType ) as af 
			on cst.TIPNUMBER = af.TIPNUMBER
	Where af.AcctType = 'debit' 

--- call purge accounts when ready

GO


