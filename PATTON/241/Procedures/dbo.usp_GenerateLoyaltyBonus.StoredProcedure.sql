USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_GenerateLoyaltyBonus]    Script Date: 10/02/2015 09:08:19 ******/
DROP PROCEDURE [dbo].[usp_GenerateLoyaltyBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		SBlanchette
-- Create date: 9/2015
-- Description:	To generate bonus by tipnumber 1000 points for 45 or more trans for the month.
-- =============================================
CREATE PROCEDURE [dbo].[usp_GenerateLoyaltyBonus]
	-- Add the parameters for the stored procedure here
	@TipFirst varchar (3),
	@MonthEndDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select tipnumber
	into #tmpBonus
	from HISTORY_Stage
	where TRANCODE like '6%'
	and HISTDATE = @MonthEndDate
	and SECID = 'NEW'
	group by TIPNUMBER having SUM(trancount)>=45

	INSERT INTO [241].[dbo].[HISTORY_Stage]
			   ([TIPNUMBER]
			   ,[ACCTID]
			   ,[HISTDATE]
			   ,[TRANCODE]
			   ,[TranCount]
			   ,[POINTS]
			   ,[Description]
			   ,[SECID]
			   ,[Ratio]
			   ,[Overage]
				)
	select	TIPNUMBER,NULL,@MonthEndDate,'BI',1,'1000','Loyalty Bonus','NEW',1,0
	from  #tmpBonus 

	update CUSTOMER_Stage
	set RunAvailable = RunAvailable + 1000
		, RunAvaliableNew = RunAvaliableNew + 1000
		, RUNBALANCE = RUNBALANCE + 1000
		, RunBalanceNew = RunBalanceNew + 1000
	where TIPNUMBER in (select TIPNUMBER from #tmpBonus)


END
GO
