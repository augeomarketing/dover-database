USE [241]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadStage]    Script Date: 03/18/2013 10:48:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadStage]
GO

USE [241]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadStage]    Script Date: 03/18/2013 10:48:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/*  **************************************  */
/*  Tables:  
	Input_Customer  - select 
	
	Customer_Stage	- Insert , update 
	Affiliat_Stage	- Insert , update 
	*/
/*  Revisions / Notes: 
Code below assumes only card numbers in the customer file are valid. 
Cards in the transaction file not found in the customer file are ignored.
*/
	
/*  **************************************  */

Create PROCEDURE [dbo].[usp_LoadStage]   @MonthEnd char(20)  AS 

-- Insert NEW customers 
Insert into CUSTOMER_Stage 
	(
	STATUS, 
	DATEADDED, 
	ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4,
	ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode,
	HOMEPHONE, WORKPHONE, 
	BusinessFlag, EmployeeFlag, Misc5, 
	LASTNAME ,
	TIPNUMBER 
	)
	Select
		CustomerCode ,
		@MonthEnd, 	
		NAME1, NAME2, NAME3, NAME4, 
		ADDRESS1, ADDRESS2, ADDRESS3, City, State, PostalCode, 
		PrimaryPhone, MobilePhone, 
		BusinessFlag, EmployeeFlag, 	Max(CustomerType),
		reverse( LEFT(Reverse (rtrim(NAME1)) ,  CHARINDEX(' ', reverse(rtrim(NAME1)))-1) ), 
		TipNumber 
	From Input_Customer  
			where TipNumber not in ( select tipnumber from CUSTOMER_Stage) 
		Group by 	CustomerCode ,
				NAME1, NAME2, NAME3, NAME4, 
				ADDRESS1, ADDRESS2, ADDRESS3, City, State, PostalCode, 
				PrimaryPhone, MobilePhone, 
				BusinessFlag, EmployeeFlag, 	
				reverse( LEFT(Reverse (rtrim(NAME1)) ,  CHARINDEX(' ', reverse(rtrim(NAME1)))-1) ), 
				TipNumber 

-- Update Old Customers 
Update CUSTOMER_Stage 
set 
	STATUS			=	ic.CustomerCode,
	ACCTNAME1		=	ic.Name1,
	ACCTNAME2	=	ic.Name2,
	ACCTNAME3	 =	ic.Name3,
	ACCTNAME4	=	ic.Name4, 
	ADDRESS1		=	ic.Address1 ,
	ADDRESS2		=	ic.Address2,
	ADDRESS3		=  ic.Address3,
	City					=  ic.City,
	State				=	ic.State,
	 ZipCode			=	ic.PostalCode,
	HOMEPHONE =  ic.PrimaryPhone,
	WORKPHONE =  ic.MobilePhone, 
	BusinessFlag	=  ic.BusinessFlag,
	EmployeeFlag	=  ic.EmployeeFlag,
	Misc5				=	ic.CustomerType,
	LASTNAME	= reverse( LEFT(Reverse (rtrim(NAME1)) ,  CHARINDEX(' ', reverse(rtrim(NAME1)))-1) )
From Input_Customer ic join CUSTOMER_Stage cs
				on ic.TipNumber = cs.TIPNUMBER 

/************ Insert Accounts into Affiliat_Stage from  Input_Customer ***********/
/* This is for cards that do not have transcations. */

Insert Into AFFILIAT_Stage
( TipNumber, ACCTID,  AcctType, DATEADDED, AcctStatus , CustID  ) 
	Select distinct Tipnumber, CardNumber,  'Debit', @MonthEnd, 'A' , PortfolioId 
	From Input_Customer ic
			where ic.CardNumber  Not in ( select Acctid from AFFILIAT_Stage ) 

/************ Insert TIN / SSN into Affiliat_Stage from  Input_Customer ***********/

Insert Into AFFILIAT_Stage
( TipNumber, ACCTID,  AcctType, DATEADDED, AcctStatus  ) 
	Select distinct Tipnumber, PrimaryID,  'TaxID', @MonthEnd, 'A' 
	From Input_Customer ic
			where ic.PrimaryID Not in ( select Acctid from AFFILIAT_Stage ) 
			and ic.PrimaryID <> '000000000'

/************ Insert DDA  into Affiliat_Stage from  Input_Customer ***********/
print 'DDA'
Insert Into AFFILIAT_Stage
( TipNumber, ACCTID,  AcctType, DATEADDED, AcctStatus  ) 
	Select  distinct Tipnumber, DDA,  'DDA', @MonthEnd, 'A' 
	From Input_Customer ic
			where ic.DDA Not in ( select Acctid from AFFILIAT_Stage ) 
				and ic.PrimaryFlag = 1

/************ LastName in Affiliat_Stage  ***********/
Update AFFILIAT_Stage 
	Set LastName = Cstg.LastName 
	From CUSTOMER_Stage Cstg Join AFFILIAT_Stage Astg 
		on Cstg.TIPNUMBER	=	Astg.TIPNUMBER 
		
/************ Remove special characters from Stage  tables ***********/
exec Rewardsnow.dbo.spRemoveSpecialCharactersFromCustomerStage '241'
exec Rewardsnow.dbo.spRemoveSpecialCharactersFromAffiliatStage  '241'
				
/************* Reset the status in customer_stage to 'A' ***********/
Update CUSTOMER_Stage 
	set Status = 'A' where Status = '1' 
Update CUSTOMER_Stage 
	set Status = 'C' where Status <> 'A' 


	


GO


