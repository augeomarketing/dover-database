USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_InitializeInput]    Script Date: 01/25/2012 14:33:25 ******/
DROP PROCEDURE [dbo].[usp_InitializeInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rich T>
-- Create date: <8/9/2011>
-- Description:	<Initialize input tables>
-- =============================================
CREATE PROCEDURE [dbo].[usp_InitializeInput]
AS
BEGIN
	Truncate Table Input_Customer

	Truncate Table Input_Transaction

END
GO
