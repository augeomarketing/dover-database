USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_BonusStage]    Script Date: 01/25/2012 14:33:25 ******/
DROP PROCEDURE [dbo].[usp_BonusStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*  Bonuses  for Cards with a date added = date parameter.  */
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType char(2)
-- @CardType char(6) 

/******************************************************************************/
CREATE PROCEDURE [dbo].[usp_BonusStage]  @DateAdded Date,  @BonusAmt int, @TranCode VarChar(2) , @CardType VarChar(6) AS

Declare @BonusDesc varchar(20) 
Set @BonusDesc = (select description from trantype where trancode = @trancode )

Select c.tipnumber , a.ACCTID  into #TNewCard 
	From Customer_Stage c Join Affiliat_Stage a 
	on c.tipnumber = a.tipnumber 
		Where a.DATEADDED = @DateAdded 
			and Upper(a.accttype ) = @CardType
		    and c.TIPNUMBER  not in 
				( Select TIPNUMBER From OneTimeBonuses_Stage Where Trancode = @TranCode ) 

UPDATE Customer_Stage
	set	 RunAvailable = RunAvailable + @BonusAmt, 
			RunBalance=RunBalance + @BonusAmt  
	Where tipnumber in ( Select Tipnumber From #TNewCard) 
	
INSERT INTO history_Stage 
	(TipNumber, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage,SecID)
	Select Tipnumber , @DateAdded , @TranCode , '1', @BonusAmt , '1', @BonusDesc, '0','NEW' 
	From #TNewCard 
	 
INSERT INTO OneTimeBonuses_Stage  
		(Tipnumber, Trancode,  DateAwarded, AcctID )
		Select 	Tipnumber, @Trancode,  @DateAdded , AcctId
		From #TNewCard
GO
