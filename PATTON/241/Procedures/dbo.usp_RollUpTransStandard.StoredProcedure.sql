USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_RollUpTransStandard]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_RollUpTransStandard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/*  
	Rolls Up transactions in the TransStandard File
*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[usp_RollUpTransStandard]  AS   

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard_UnRolled]') AND type in (N'U'))
DROP TABLE [dbo].[TransStandard_UnRolled]

Select * into TransStandard_UnRolled
from TransStandard

select TIPNumber , Max(TranDate)  as Trandate , AcctID, TranCode , Sum(TranNum) as TranNum , SUM(tranAmt) as TranAmt, TranType , Ratio
into #Temp
from TransStandard
group by TIPNumber , AcctID, TranCode, TranType , Ratio

Truncate Table TransStandard

Insert into TransStandard
( TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio) 
select TIPNumber , TranDate , AcctID, TranCode , TranNum , tranAmt , TranType , Ratio
from #Temp
GO
