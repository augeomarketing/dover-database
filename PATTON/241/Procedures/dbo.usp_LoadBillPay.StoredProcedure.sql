USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadBillPay]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_LoadBillPay]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/

/******************************************************************************/
CREATE PROCEDURE [dbo].[usp_LoadBillPay]  @EndDate Date
AS



-- Get Tipnumbers  & Insert into TransStandard
Insert into TransStandard 
( TIPNumber,
 	TranDate,
 	AcctID,
 	TranCode,
 	TranType,
 	TranNum,
 	TranAmt,
 	Ratio ) 
Select  
	afs.TipNumber, 	 
	@EndDate , 
	 [241].dbo.ufn_PadSSN(vww.PrimaryId)  , -- Note Replicate: SSN in the View has leading zeros truncated 
	'FC', 	
	 'Online Bill Pay',
	1, 
	 vww.Points,
	 1 
	From [241].dbo.Input_Transaction vww 
		Join AFFILIAT_Stage afs
			on  [241].dbo.ufn_PadSSN(vww.PrimaryId)  =  afs.ACCTID 
	where 	afs.AcctType = 'TAXID'
GO
