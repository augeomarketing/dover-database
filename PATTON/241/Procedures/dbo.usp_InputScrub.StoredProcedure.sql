USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_InputScrub]    Script Date: 01/25/2012 14:33:25 ******/
DROP PROCEDURE [dbo].[usp_InputScrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This LOADS and scrubs the inport_Customer and flags the RNIRawImport as Imported
*/
/* BY:  R.Tremblay  */
/* REVISION: 0 */
-- RDT 10/13/2011 Lhun Check Digit moved to View.
-- RDT 6/15/2012 One account doesn't have  a name1 
/******************************************************************************/	

CREATE  PROCEDURE [dbo].[usp_InputScrub] 
AS

/* clear input tables */
Truncate Table  Input_CUSTOMER
Truncate Table  Input_CUSTOMER_Error
--------------------Truncate Table Input_Transaction
--------------------Truncate Table Input_Transaction_error

-- Load Input_Customer and Add Luhn Check Digit to Card number of input 
Insert into Input_Customer 
( 
	PortfolioId, MemberId, PrimaryId, DDA, PrimaryFlag, 
	NAME1, NAME2, NAME3, NAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, Country, PostalCode, 
	PrimaryPhone, MobilePhone, CustomerCode, BusinessFlag, EmployeeFlag, InstitionID, CardNumber, Email, CustomerType 
)
Select 
	PortfolioId, MemberId, PrimaryId, DDA, PrimaryFlag,
	NAME1, NAME2, NAME3, NAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, Country, PostalCode,
	PrimaryPhone, MobilePhone, CustomerCode, BusinessFlag, EmployeeFlag, InstitionID, 
-- RDT 10/13/2011 	Rtrim(CardNumber) + Cast( Rewardsnow.dbo.ufn_GetLuhnSum ( CardNumber) as CHAR) , 
	CardNumber , 
	Email, CustomerType
 From RewardsNow.dbo.vw_241_ACCT_TARGET_226

-- Update Import Status to 1 
Update RewardsNow.dbo.vw_241_ACCT_TARGET_226 
Set sid_rnirawimportstatus_id =  1 
	From RewardsNow.dbo.vw_241_ACCT_TARGET_226 vw 
		join input_customer ic 
		on		ic.CardNumber	= vw.CardNumber 
		and	ic.PrimaryId		= vw.PrimaryId 
		and	ic.DDA				= vw.DDA 

Update Input_Customer 
	Set NAME1 = 'No Name' 
	Where NAME1 = '' 		
------------ Scrub Input_Customer 
------------ If a customer has one card flagged as employee TYPE then all cards should be flagged the same. 
----------Update ic1
----------set EmployeeFlag = ic2.Emp
----------	from Input_Customer ic1 join 
----------	( select PrimaryID , Max(EmployeeFlag)  as emp from Input_Customer group by PrimaryId ) ic2
----------	on ic1.PrimaryId = ic2.PrimaryId

------------ If a customer has one card flagged as an employee CARD then all cards should be flagged the same. 
----------Update ic1
----------set CustomerType = ic2.Emp
----------	from Input_Customer ic1 join 
----------	( select PrimaryID , Max(CustomerType)  as emp from Input_Customer group by PrimaryId ) ic2
----------	on ic1.PrimaryId = ic2.PrimaryId

GO
