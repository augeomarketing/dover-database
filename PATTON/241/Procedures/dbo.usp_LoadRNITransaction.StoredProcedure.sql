
USE [241]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadRNICustomer]    Script Date: 01/25/2012 14:33:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadRNITransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadRNITransaction]
GO

/* 
This loads the rnitransaction and the RNITransactionAux tables 
*/ 

CREATE PROCEDURE [dbo].[usp_LoadRNITransaction] @ProcessDate DateTime
AS 
  

truncate table [241].dbo.Input_Customer 

/* load input_customer from archive 
	The input_Customer is used in  [241].dbo.usp_LoadStage 	and [241].dbo.usp_CalculatePoints 
*/
	insert into [241].dbo.Input_Customer 
	(
		PortfolioId	,PrimaryId	,DDA ,PrimaryFlag	
		,NAME1	,NAME2	,NAME3	,NAME4	
		,ADDRESS1	,ADDRESS2	,ADDRESS3	,City	,State	,Country	,PostalCode	
		,PrimaryPhone	,MobilePhone	
		,CustomerCode	,BusinessFlag	,EmployeeFlag	,InstitionID	,CardNumber	,Email	,CustomerType	
		, TipNumber 
	) 
		select 
		dim_RNICustomer_Portfolio	,dim_RNICustomer_PrimaryId	,dim_RNICustomer_Member	,dim_RNICustomer_PrimaryIndicator	
		,dim_RNICustomer_Name1	,dim_RNICustomer_Name2	,dim_RNICustomer_Name3	,dim_RNICustomer_Name4	
		,dim_RNICustomer_Address1	,dim_RNICustomer_Address2	,dim_RNICustomer_Address3	,dim_RNICustomer_City	,dim_RNICustomer_StateRegion	,dim_RNICustomer_CountryCode	,dim_RNICustomer_PostalCode	
		,dim_RNICustomer_PriPhone	,dim_RNICustomer_PriMobilPhone	
		,dim_RNICustomer_CustomerCode	,dim_RNICustomer_BusinessFlag	,dim_RNICustomer_EmployeeFlag	,dim_RNICustomer_InstitutionID	,dim_RNICustomer_CardNumber	,dim_RNICustomer_EmailAddress	,dim_RNICustomer_CustomerType	
		,dim_RNICustomer_RNIId	
		from rewardsnow.dbo.RNICustomer 
		where sid_dbprocessinfo_dbnumber = '241' 

execute [241].dbo.usp_LoadStage @ProcessDate 

execute [241].dbo.usp_CalculatePoints @ProcessDate   ---- This can take a while. Maybe hours.

INSERT INTO Rewardsnow.dbo.RNITransaction
(
	[dim_RNITransaction_TipPrefix] 
	,[sid_rnirawimport_id] 
	,dim_RNITransaction_Member
	,[dim_RNITransaction_RNIId] 
	,[dim_RNITransaction_CardNumber] 
	,[dim_RNITransaction_TransactionDate] 
	,[dim_RNITransaction_TransactionCode] 
	,[dim_RNITransaction_TransactionAmount] 
	,[dim_RNITransaction_TransactionDescription] 
	,[dim_RNITransaction_MerchantID] 
	,[dim_RNITransaction_EffectiveDate] 
	, dim_RNITransaction_ProcessingCode
	, sid_trantype_trancode
)

SELECT 
'241'
 ,vw241.sid_rnirawimport_id	
 ,MemberNumber	
 ,ts.TIPNUMBER 
 ,PrimaryAccountNumber 
 , convert( date, left(LocalTransactionDateTime,6),112 )
 ,MessageType	
 , convert ( decimal (12,2) , ReconciliationAmount  ) /100 --- rnitransaction is a decimal
 ,CardAcceptorLocation	
 ,CardAcceptorIDCode	
 , dim_rnirawimport_processingenddate
 , 1  as processingcode 
 ,ts.trancode
FROM
	Rewardsnow.dbo.vw_241_TRAN_SOURCE_1 vw241 with (nolock) 
 	join [241].dbo.TransStandard_UnRolled ts with (nolock) on ts.sid_rnirawimport_id = vw241.sid_rnirawimport_id 
	left outer join Rewardsnow.dbo.RNITransaction rnit with (nolock) on rnit.sid_rnirawimport_id = ts.sid_rnirawimport_id
where sid_rnirawimportstatus_id	= 1
	  and ts.TranDate = @ProcessDate 
	  and rnit.sid_rnirawimport_id is null 

/* ========================================= */
/* Load auxiliary table */ 
/* ========================================= */
Insert into Rewardsnow.dbo.RNITransactionAux
(sid_rnitransaction_id, sid_rnitransactionauxkey_id, dim_rnitransactionaux_value )
Select 
rnit.sid_RNITransaction_ID , 1, CardAcceptorBusinessCode 
From Rewardsnow.dbo.vw_241_TRAN_SOURCE_1 vw with (nolock)
	Join Rewardsnow.dbo.RNITransaction rnit with (nolock)
		on rnit.sid_rnirawimport_id = vw.sid_rnirawimport_id 
where sid_RNITransaction_ID not in
	( select sid_RNITransaction_ID from Rewardsnow.dbo.RNITransactionAux where sid_rnitransactionauxkey_id = 1 ) 
	

/* Credit Load 2 Merchant Name */		
Insert into Rewardsnow.dbo.RNITransactionAux
(sid_rnitransaction_id, sid_rnitransactionauxkey_id, dim_rnitransactionaux_value )
Select 
rnit.sid_RNITransaction_ID , 2, CardAcceptorName 
From Rewardsnow.dbo.vw_241_TRAN_SOURCE_1 vw with (nolock)
	Join Rewardsnow.dbo.RNITransaction rnit with (nolock)
		on rnit.sid_rnirawimport_id = vw.sid_rnirawimport_id 
where sid_RNITransaction_ID not in
	( select sid_RNITransaction_ID from Rewardsnow.dbo.RNITransactionAux where sid_rnitransactionauxkey_id = 2 ) 


/* Credit Load 3 Merchant Location */
Insert into Rewardsnow.dbo.RNITransactionAux
(sid_rnitransaction_id, sid_rnitransactionauxkey_id, dim_rnitransactionaux_value )
Select 
rnit.sid_RNITransaction_ID , 3, CardAcceptorLocation
From Rewardsnow.dbo.vw_241_TRAN_SOURCE_1 vw with (nolock)
	Join Rewardsnow.dbo.RNITransaction rnit with (nolock)
		on rnit.sid_rnirawimport_id = vw.sid_rnirawimport_id 
where sid_RNITransaction_ID not in
	( select sid_RNITransaction_ID from Rewardsnow.dbo.RNITransactionAux where sid_rnitransactionauxkey_id = 3 ) 


/* Credit Load 4 MCC */
Insert into Rewardsnow.dbo.RNITransactionAux
(sid_rnitransaction_id, sid_rnitransactionauxkey_id, dim_rnitransactionaux_value )
Select 
rnit.sid_RNITransaction_ID , 4, MerchantType
From Rewardsnow.dbo.vw_241_TRAN_SOURCE_1 vw with (nolock)
	Join Rewardsnow.dbo.RNITransaction rnit with (nolock)
		on rnit.sid_rnirawimport_id = vw.sid_rnirawimport_id 
where sid_RNITransaction_ID not in
	( select sid_RNITransaction_ID from Rewardsnow.dbo.RNITransactionAux where sid_rnitransactionauxkey_id = 4 ) 

