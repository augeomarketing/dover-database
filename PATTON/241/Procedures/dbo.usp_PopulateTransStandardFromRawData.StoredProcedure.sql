USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_PopulateTransStandardFromRawData]    Script Date: 04/24/2015 10:17:24 ******/
DROP PROCEDURE [dbo].[usp_PopulateTransStandardFromRawData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
RDT JIRA 
*/
CREATE PROCEDURE [dbo].[usp_PopulateTransStandardFromRawData]
	@processingEndDate DATE = NULL
	, @reset BIT = 1
	, @flagMessageType BIT = 1
	, @flagResponseCode BIT = 1
	, @flagProcessingCode BIT = 1	
	, @flagActionCode BIT = 1	
	, @flagPOSCode BIT = 1	
	, @flagOrphans BIT = 1
AS
SET NOCOUNT ON

--select * from RewardsNow.dbo.vw_241_TRAN_SOURCE_1
CREATE TABLE #vw_241_TRAN_SOURCE_1
(
	sid_rnirawimport_id bigint primary key
	, sid_rnirawimportstatus_id INT
	, dim_rnirawimport_processingenddate DATE
	, MessageType INT
	, TransactionResponseCode VARCHAR(3)
	, ActionCode INT
	, TransactionProcessingCode VARCHAR(6)
	, PointOfServiceCode INT
	, TransactionProcessingCodeANSI_Left2 INT
	, ReconciliationAmount VARCHAR(12)
	, DebitCreditFlag VARCHAR(2)
	, PointOfServiceDataCode_Pos8 INT
	, PrimaryAccountNumber VARCHAR(20)
)

CREATE INDEX ix_vw241TranSouce1 ON #vw_241_TRAN_SOURCE_1 
(
	MessageType
	, TransactionResponseCode
	, ActionCode
	, TransactionProcessingCode
	, PointOfServiceCode
	, TransactionProcessingCodeANSI_Left2
	, PointOfServiceDataCode_Pos8
) INCLUDE (PrimaryAccountNumber, dim_rnirawimport_processingenddate)

INSERT INTO #vw_241_TRAN_SOURCE_1
(
	sid_rnirawimport_id
	, sid_rnirawimportstatus_id
	, dim_rnirawimport_processingenddate
	, MessageType
	, TransactionResponseCode
	, ActionCode
	, TransactionProcessingCode
	, PointOfServiceCode
	, TransactionProcessingCodeANSI_Left2
	, ReconciliationAmount
	, DebitCreditFlag
	, PointOfServiceDataCode_Pos8
	, PrimaryAccountNumber
)
SELECT
	sid_rnirawimport_id
	, sid_rnirawimportstatus_id
	, dim_rnirawimport_processingenddate
	, CONVERT(INT, MessageType)
	, TransactionResponseCode
	, CONVERT(INT, ActionCode)
	, TransactionProcessingCode
	, CONVERT(INT, PointOfServiceCode)
	, CONVERT(INT, TransactionProcessingCodeANSI_Left2)
	, ReconciliationAmount
	, DebitCreditFlag
	, CONVERT(INT, PointOfServiceDataCode_Pos8)
	, PrimaryAccountNumber
FROM
	Rewardsnow.dbo.vw_241_TRAN_SOURCE_1
	

CREATE TABLE #RNIProcessingParameter 
(
	sid_dbprocessinfo_dbnumber VARCHAR(3)
	, dim_rniprocessingparameter_key VARCHAR(50) PRIMARY KEY
	, dim_rniprocessingparameter_value VARCHAR(MAX)
	, dim_rniprocessingparameter_active INT
)

CREATE INDEX ix_RNIProcessingParameter241 ON #RNIProcessingParameter
(
	sid_dbprocessinfo_dbnumber
	, dim_rniprocessingparameter_key
) 

INSERT INTO #RNIProcessingParameter
(	
	sid_dbprocessinfo_dbnumber
	, dim_rniprocessingparameter_key
	, dim_rniprocessingparameter_value
	, dim_rniprocessingparameter_active
)
SELECT
	sid_dbprocessinfo_dbnumber
	, dim_rniprocessingparameter_key
	, dim_rniprocessingparameter_value
	, dim_rniprocessingparameter_active
FROM
	RewardsNow.dbo.RNIProcessingParameter
WHERE
	sid_dbprocessinfo_dbnumber = '241'
	and dim_rniprocessingparameter_active = 1
	
DECLARE @startingTranCount VARCHAR(20)
	, @trancountAfterMessageType VARCHAR(20)
	, @trancountAfterProcessingCode VARCHAR(20)
	, @trancountAfterResponseCode VARCHAR(20)
	, @trancountAfterOrphans VARCHAR(20)
	, @transactionsLoaded VARCHAR(20)
	, @transactionsNotLoaded_BusinessRules VARCHAR(20)
	, @trancountAfterActionCode  VARCHAR(20)
	, @trancountAfterPOSCode  VARCHAR(20)
	, @transactionsNotLoaded_Orphan VARCHAR(20)
	, @transactionsNotLoaded_Error VARCHAR(20)
	, @batchdebugprocessname VARCHAR(255) = 'usp_PopulateTransStandardFromRawData 241'

-- SET END DATE TO THE LAST OF THE PREVIOUS MONTH IF IT IS NULL
SET @processingEndDate = ISNULL(@processingEndDate, Rewardsnow.dbo.ufn_GetLastOfPrevMonth(getdate()))

DELETE FROM RewardsNow.dbo.BatchDebugLog WHERE dim_batchdebuglog_process = @batchdebugprocessname

--RESET DATA IF RESET BIT IS SET

INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @batchdebugprocessname, 'Begin Process'

IF @reset = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Resetting Transaction Status'

	UPDATE #vw_241_TRAN_SOURCE_1 
	SET sid_rnirawimportstatus_id = 0 
	WHERE sid_rnirawimportstatus_id <> 0
		AND dim_rnirawimport_processingenddate = @processingEndDate;

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Clearing TransStandard in FI DB'
		
	DELETE FROM [241].dbo.TransStandard

END

SET @startingTranCount = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))
SET @trancountAfterMessageType = @startingTranCount
SET @trancountAfterProcessingCode = @startingTranCount
SET @trancountAfterResponseCode = @startingTranCount
SET @trancountAfterOrphans = @trancountAfterOrphans 
SET @trancountAfterPOSCode = @trancountAfterOrphans

--SELECT * FROM REWARDSNOW.DBO.RNIPROCESSINGPARAMETER WHERE dim_rniprocessingparameter_key like 'VALIDMESSAGETYPE%'

IF @flagMessageType = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging For MessageType';

	WITH validtype(msgtype)
	AS
	(
		SELECT CONVERT(INT, dim_rniprocessingparameter_value) as msgtype
		FROM #RNIProcessingParameter
		WHERE 
			dim_rniprocessingparameter_key LIKE 'VALIDMESSAGETYPE%'
			AND sid_dbprocessinfo_dbnumber = '241'
			--AND dim_rniprocessingparameter_active = 1  -- This is commented out because only active rows are inserted into the temp table #RNIProcessingParameter
	)
	UPDATE t
	SET sid_rnirawimportstatus_id = 2
	--SELECT t.MessageType
	FROM #vw_241_TRAN_SOURCE_1 t
	LEFT OUTER JOIN validtype m
		ON t.MessageType = m.msgtype
	WHERE
		m.msgtype is null
		and t.sid_rnirawimportstatus_id = 0
		and t.dim_rnirawimport_processingenddate = @processingEndDate;

	SET @trancountAfterMessageType = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))
	SET @trancountAfterProcessingCode = @trancountAfterMessageType
	SET @trancountAfterResponseCode = @trancountAfterMessageType
	SET @trancountAfterOrphans = @trancountAfterMessageType
	SET @trancountAfterPOSCode = @trancountAfterMessageType

END

--SELECT * FROM #RNIProcessingParameter WHERE dim_rniprocessingparameter_key like 'TRANSACTIONRESPONSECODE%'

IF @flagResponseCode = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging For Transaction Response Code';

	WITH responsecode(code)
	AS
	(
		SELECT LEFT(dim_rniprocessingparameter_value, 3) as code
		FROM #RNIProcessingParameter
		WHERE 
			dim_rniprocessingparameter_key LIKE 'TRANSACTIONRESPONSECODE%'
			AND sid_dbprocessinfo_dbnumber = '241'
			--AND dim_rniprocessingparameter_active = 1  -- This is commented out because only active rows are inserted into the temp table #RNIProcessingParameter
	)
	UPDATE t
	SET sid_rnirawimportstatus_id = 2
	--SELECT t.MessageType
	FROM #vw_241_TRAN_SOURCE_1 t
	LEFT OUTER JOIN  responsecode r
		ON t.TransactionResponseCode = r.code
	WHERE
		r.code is null
		and t.sid_rnirawimportstatus_id = 0
		and dim_rnirawimport_processingenddate = @processingEndDate;

	SET @trancountAfterResponseCode = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))
	SET @trancountAfterProcessingCode = @trancountAfterResponseCode
	SET @trancountAfterOrphans = @trancountAfterResponseCode
	SET @trancountAfterPOSCode = @trancountAfterResponseCode

END
																/****** ACTION CODE *****/
--SELECT * FROM #RNIProcessingParameter WHERE dim_rniprocessingparameter_key like 'ACTIONCODE%'
IF @flagActionCode = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging For Transaction Action Code';

	WITH processingcode(code)
	AS
	(
		SELECT CONVERT(INT, dim_rniprocessingparameter_value) as code
		FROM #RNIProcessingParameter
		WHERE 
			dim_rniprocessingparameter_key LIKE 'ACTIONCODE%'
			AND sid_dbprocessinfo_dbnumber = '241'
			--AND dim_rniprocessingparameter_active = 1  -- This is commented out because only active rows are inserted into the temp table #RNIProcessingParameter
	)
	UPDATE t
	SET sid_rnirawimportstatus_id = 2
	--SELECT t.MessageType
	FROM #vw_241_TRAN_SOURCE_1 t
	LEFT OUTER JOIN  processingcode p
		ON t.ActionCode = p.code
	WHERE
		p.code is null
		and t.sid_rnirawimportstatus_id = 0
		and dim_rnirawimport_processingenddate = @processingEndDate;

	SET @trancountAfterActionCode = CONVERT(VARCHAR, (SELECT COUNT(*) 
				FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))
	SET @trancountAfterProcessingCode = @trancountAfterActionCode 
	SET @trancountAfterOrphans = @trancountAfterActionCode 
	SET @trancountAfterPOSCode = @trancountAfterActionCode 

END

																/****** POINT OF SERVICE CODE	*****/
--SELECT * FROM #RNIProcessingParameter WHERE dim_rniprocessingparameter_key like 'POINTOFSERVICECODE_OMIT%'
IF @flagPOSCode = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging For Point of Service Code';

	WITH processingcode(code)
	AS
	(
		SELECT CONVERT(INT, dim_rniprocessingparameter_value) as code
		FROM #RNIProcessingParameter
		WHERE 
			dim_rniprocessingparameter_key LIKE 'POINTOFSERVICECODE_OMIT%'
			AND sid_dbprocessinfo_dbnumber = '241'
			--AND dim_rniprocessingparameter_active = 1  -- This is commented out because only active rows are inserted into the temp table #RNIProcessingParameter
	)
	UPDATE t
	SET sid_rnirawimportstatus_id = 2
	--SELECT t.MessageType
	FROM #vw_241_TRAN_SOURCE_1 t
	JOIN  processingcode p					--- Note JOIN 
		ON t.PointOfServiceCode = p.code
	WHERE
		t.sid_rnirawimportstatus_id = 0
		and dim_rnirawimport_processingenddate = @processingEndDate;

	SET @trancountAfterPOSCode   = CONVERT(VARCHAR, (SELECT COUNT(*) 
				FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))
	SET @trancountAfterProcessingCode = @trancountAfterPOSCode   
	SET @trancountAfterOrphans = @trancountAfterPOSCode   

END

												/****** TRANSACTION PROCESSING CODE *****/ 
--SELECT * FROM #RNIProcessingParameter WHERE dim_rniprocessingparameter_key like 'TRANSACTIONPROCESSINGCODE%'
IF @flagProcessingCode = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging For Transaction Processing Code ANSI';

	WITH processingcode(code)
	AS
	(
		SELECT CONVERT(INT, dim_rniprocessingparameter_value) as code
		FROM #RNIProcessingParameter
		WHERE 
			dim_rniprocessingparameter_key LIKE 'TRANSACTIONPROCESSINGCODE%'
			AND sid_dbprocessinfo_dbnumber = '241'
			--AND dim_rniprocessingparameter_active = 1  -- This is commented out because only active rows are inserted into the temp table #RNIProcessingParameter
	)
	UPDATE t
	SET sid_rnirawimportstatus_id = 2
	FROM #vw_241_TRAN_SOURCE_1 t
	LEFT OUTER JOIN  processingcode p
		ON t.TransactionProcessingCodeANSI_Left2 = p.code
	WHERE
		p.code is null
		and t.sid_rnirawimportstatus_id = 0
		and dim_rnirawimport_processingenddate = @processingEndDate;

	SET @trancountAfterProcessingCode = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))
	SET @trancountAfterOrphans = @trancountAfterProcessingCode

END


IF @flagOrphans = 1
BEGIN

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging Orphan Transactions'
		
	UPDATE t 
	SET sid_rnirawimportstatus_id = 4 --ORPHTRAN
	FROM #vw_241_TRAN_SOURCE_1 t
	LEFT OUTER JOIN [241].dbo.vw_AllAffiliat af
		ON t.PrimaryAccountNumber = af.ACCTID
	WHERE
		af.ACCTID IS NULL
		AND t.sid_rnirawimportstatus_id = 0
		AND t.dim_rnirawimport_processingenddate = @processingEndDate;

	SET @trancountAfterOrphans = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))


END

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Pulling Data From rniTranTypeXRef table'

CREATE TABLE #xref  
(
	TransactionProcessingCodeANSI_Left2 varchar(10)
	, MessageType INT
	, DebitCreditFlag varchar(2)
	, PointOfServiceDataCode_Pos8 INT
	, CustomerType INT
	, dim_rnitrantypexref_factor decimal(24,5)
	, sid_trantype_trancode varchar(2)
)	

CREATE INDEX ix_xref241 ON #xref
(
	TransactionProcessingCodeANSI_Left2
	, MessageType
	, PointOfServiceDataCode_Pos8
	, CustomerType
	, sid_trantype_trancode
) 

INSERT INTO #xref 
(
	TransactionProcessingCodeANSI_Left2
	, MessageType
	, DebitCreditFlag 
	, PointOfServiceDataCode_Pos8
	, CustomerType
	, dim_rnitrantypexref_factor
	, sid_trantype_trancode
)
SELECT 
	dim_rnitrantypexref_code02 as TransactionProcessingCodeANSI 
	, CONVERT(INT, dim_rnitrantypexref_code01) as MessageType
	, RTRIM(dim_rnitrantypexref_code03) as DebitCreditFlag 
	, CONVERT(INT, dim_rnitrantypexref_code04) as PointOfServiceDataCodePos8
	, CONVERT(INT, RTRIM(dim_rnitrantypexref_code05)) as CustomerType
	, dim_rnitrantypexref_factor
	, sid_trantype_trancode
FROM
	Rewardsnow.dbo.rniTrantypeXref
WHERE
	sid_dbprocessinfo_dbnumber = '241'
	and dim_rnitrantypexref_active = 1
	and dim_rnitrantypexref_code04 is not null


	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Inserting Data Into TransStandard'


CREATE TABLE #tmpTransStandard 
(
	sid_rnirawimport_id BIGINT
	, TIPNUMBER VARCHAR(15)
	, TranDate DATETIME
	, AcctID VARCHAR(25)
	, Trancode VARCHAR(2)
	, TranNum INT
	, TranAmt INT
	, TranType VARCHAR(40)
	, Ratio INT
	, CrdActvlDt DATE
)


INSERT INTO #tmpTransStandard
(
	TIPNumber
	, TranDate
	, AcctID
	, TranCode
	, TranNum
	, TranAmt
	, TranType
	, Ratio
	, sid_rnirawimport_id
)
SELECT 
	rnic.dim_RNICustomer_RNIId
	, @processingEndDate
	, t.PrimaryAccountNumber
	, x.sid_trantype_trancode
	, 1
	, RewardsNow.dbo.ufn_CalculatePoints(t.reconciliationamount, x.dim_rnitrantypexref_factor, 2) 	-- JIRA ENTERPRISE-98
	, x.sid_trantype_trancode
	, tt.Ratio
	, t.sid_rnirawimport_id
FROM #vw_241_TRAN_SOURCE_1 t
--INNER JOIN RewardsNow.dbo.RNICustomer rnic with (Nolock)
INNER JOIN (select distinct dim_RNICustomer_CardNumber, dim_RNICustomer_RNIId, sid_dbprocessinfo_dbnumber from RewardsNow.dbo.RNICustomer with (Nolock) ) rnic
ON rnic.dim_RNICustomer_CardNumber = t.PrimaryAccountNumber
INNER JOIN [241].dbo.ufn_241_MaxTypeByCard() ct
ON rnic.dim_RNICustomer_CardNumber = ct.dim_rnicustomer_cardnumber

inner join #xref x

	on t.TransactionProcessingCodeANSI_Left2  = x.TransactionProcessingCodeANSI_Left2 -- RDT CWH 
		and t.MessageType = x.MessageType
		and t.DebitCreditFlag = x.DebitCreditFlag
		and t.PointOfServiceDataCode_Pos8 = x.PointOfServiceDataCode_Pos8 --CWH
		and ct.dim_RNICustomer_CustomerType = x.CustomerType
INNER JOIN Rewardsnow.dbo.TranType tt
	on x.sid_trantype_trancode = tt.TranCode
where
	t.sid_rnirawimportstatus_id = 0
	and rnic.sid_dbprocessinfo_dbnumber='241'
	and t.dim_rnirawimport_processingenddate = @processingEndDate
order by rnic.dim_RNICustomer_RNIId, ReconciliationAmount

INSERT INTO [241].dbo.TransStandard
(
	TIPNUMBER, TranDate, AcctID, Trancode
	, TranNum, TranAmt, TranType, Ratio, CrdActvlDt, sid_rnirawimport_id 
)
SELECT
	TIPNUMBER, TranDate, AcctID, Trancode
	, TranNum, TranAmt, TranType, Ratio, CrdActvlDt,sid_rnirawimport_id 
FROM #tmpTransStandard


	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Flagging Loaded Transactions'

UPDATE t 
SET sid_rnirawimportstatus_id = 1
FROM #vw_241_TRAN_SOURCE_1 t
INNER JOIN #tmpTransStandard ts
ON t.sid_rnirawimport_id = ts.sid_rnirawimport_id
	
SET @transactionsLoaded = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 1 and dim_rnirawimport_processingenddate = @processingEndDate))
SET @transactionsNotLoaded_BusinessRules = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 2 and dim_rnirawimport_processingenddate = @processingEndDate))
SET @transactionsNotLoaded_Orphan = CONVERT(VARCHAR, (SELECT COUNT(*) 
FROM #vw_241_TRAN_SOURCE_1 
where sid_rnirawimportstatus_id = 4 and dim_rnirawimport_processingenddate = @processingEndDate))


SET @transactionsNotLoaded_Error = CONVERT(VARCHAR, (SELECT COUNT(*) FROM #vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0 and dim_rnirawimport_processingenddate = @processingEndDate))

	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, '--->Writing Flags Back To Raw Data Table'


UPDATE rri
	SET sid_rnirawimportstatus_id = lts.sid_rnirawimportstatus_id
FROM RewardsNow.dbo.RNIRawImport rri
INNER JOIN #vw_241_TRAN_SOURCE_1 lts
	ON rri.sid_rnirawimport_id = lts.sid_rnirawimport_id

BEGIN
	DECLARE @msg VARCHAR(MAX)


	SET @msg = '--->Statistics: '
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Starting Transaction Count: ' + @startingTranCount
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Valid Transactions after MessageType: ' + @trancountAfterMessageType
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Valid Transactions after ResponseCode: ' + @trancountAfterResponseCode
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Valid Transactions after ProcessingCodeANSI: ' + @trancountAfterProcessingCode
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Valid Transactions after PointOfServiceCode: ' + @trancountAfterPOSCode
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Transactions Loaded: ' + @transactionsLoaded
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Transactions Not Loaded Due to Business Rules: ' + @transactionsNotLoaded_BusinessRules
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Transactions Not Loaded Due to Orphans: ' + @transactionsNotLoaded_Orphan
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = '--->--->Transactions Not Loaded (Reason Unknown/Possible Cross Reference Mismatch): ' + @transactionsNotLoaded_Error
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg

	SET @msg = 'Process Complete For ' + CONVERT(varchar(10), @processingEndDate, 101)
	INSERT INTO RewardsNow.dbo.BatchDebugLog(dim_batchdebuglog_process, dim_batchdebuglog_logentry)
	SELECT @batchdebugprocessname, @msg
END
GO
