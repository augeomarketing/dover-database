USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyAuditUpdateColumns]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_MonthlyAuditUpdateColumns]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================
-- Author:		Rich T
-- Description: Update additional columns in Monthly_Statement_File
-- =================================================
CREATE PROCEDURE [dbo].[usp_MonthlyAuditUpdateColumns] 
AS

Update Monthly_Statement_File 
Set 
	City		= RTRIM( cs.City) ,
	State	= RTRIM( cs.State) , 
	Zip		= RTRIM(cs.ZipCode ) 
	From CUSTOMER_Stage cs 
		join Monthly_Statement_File ms 
			on cs.TIPNUMBER = ms.Tipnumber
GO
