USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadRNICustomer]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_LoadRNICustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/*  Tables:  
	Input_Customer  - select 
	
	Rewardsnow.dbo.RNICustomer	 - Delete , Insert.
*/

CREATE PROCEDURE [dbo].[usp_LoadRNICustomer]    AS 

Delete from RewardsNow.dbo.RNICustomer 
	where dim_RNICustomer_TipPrefix = '241'

Insert 
into RewardsNow.dbo.RNICustomer 
		( dim_RNICustomer_TipPrefix, 
		dim_RNICustomer_Portfolio, 
		dim_RNICustomer_Member, 
		dim_RNICustomer_PrimaryId, 
		dim_RNICustomer_RNIId, 
		dim_RNICustomer_PrimaryIndicator,
		 dim_RNICustomer_Name1, 
		 dim_RNICustomer_Name2, 
		 dim_RNICustomer_Name3, 
		 dim_RNICustomer_Name4, 
		 dim_RNICustomer_Address1, 
		 dim_RNICustomer_Address2, 
		 dim_RNICustomer_Address3, 
		 dim_RNICustomer_City, 
		 dim_RNICustomer_StateRegion, 
		 dim_RNICustomer_CountryCode, 
		 dim_RNICustomer_PostalCode, 
		 dim_RNICustomer_PriPhone, 
		 dim_RNICustomer_PriMobilPhone, 
		 dim_RNICustomer_CustomerCode, 
		 dim_RNICustomer_BusinessFlag, 
		 dim_RNICustomer_EmployeeFlag, 
		 dim_RNICustomer_InstitutionID, 
		 dim_RNICustomer_CardNumber, 
		 dim_RNICustomer_EmailAddress, 
		 dim_RNICustomer_CustomerType) 
select 
		LEFT(Tipnumber, 3) , 
		PortfolioId, 
		DDA, 
		PrimaryId, 
		Tipnumber, 
		PrimaryFlag, 
		NAME1, 
		NAME2, 
		NAME3, 
		NAME4, 
		ADDRESS1, 
		ADDRESS2, 
		ADDRESS3, 
		City, 
		State, 
		Country, 
		PostalCode, 
		PrimaryPhone, 
		MobilePhone, 
		--Case CustomerCode 
		--	When 'A' then 1
		--	When 'C' then 2
		--	else CustomerCode 
		--End as 
		CustomerCode, 
		BusinessFlag, 
		EmployeeFlag, 
		InstitionID, 
		CardNumber, 
		Email, 
		CustomerType
from Input_Customer
GO
