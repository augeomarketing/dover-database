USE [241]
GO
/****** Object:  StoredProcedure [dbo].[usp_StageCurrentMonthActivity]    Script Date: 01/25/2012 14:33:26 ******/
DROP PROCEDURE [dbo].[usp_StageCurrentMonthActivity]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_StageCurrentMonthActivity] @EndDateParm DateTime 
AS

Declare @EndDate DateTime 						--RDT 10/09/2006 
Set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 

Truncate Table Current_Month_Activity

Insert into Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
Select tipnumber, IsNull(RunAvailable,0) ,0 ,0 ,0 from Customer_Stage


/* Load the current activity table with increases for the current month         */
Update Current_Month_Activity
Set increases=(select sum(points) 	from history_Stage where histdate>@enddate and ratio='1' 
							and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
Where exists(select * from history_Stage where histdate>@enddate and ratio='1'
							and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
Update Current_Month_Activity
Set decreases=(select sum(points) from history_Stage where histdate>@enddate and ratio='-1'
							and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
Where exists(select * from history_Stage where histdate>@enddate and ratio='-1'
						and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
	Set adjustedendingpoints	=	endingpoints - increases + decreases
GO
