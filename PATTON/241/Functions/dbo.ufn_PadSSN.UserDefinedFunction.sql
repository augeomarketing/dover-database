USE [241]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_PadSSN]    Script Date: 01/25/2012 14:33:26 ******/
DROP FUNCTION [dbo].[ufn_PadSSN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rich 
-- Create date: 2011-10-30
-- Description:	pad SSN with leading zeros
-- =============================================
CREATE FUNCTION [dbo].[ufn_PadSSN] 
(
	@inputstring VARCHAR(MAX)
) RETURNS VARCHAR(MAX)
AS
BEGIN
	-- Declare the return variable here
		DECLARE @output VARCHAR(MAX)

	-- Add the T-SQL statements to compute the return value here
	set @output =  REPLICATE ( '0',9-Len(rewardsnow.dbo.ufn_Trim(@inputstring)))+@inputstring

	-- Return the result of the function
	RETURN @output 

END
GO
