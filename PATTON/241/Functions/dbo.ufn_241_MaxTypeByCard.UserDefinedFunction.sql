USE [241]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_241_MaxTypeByCard]    Script Date: 01/25/2012 14:33:26 ******/
DROP FUNCTION [dbo].[ufn_241_MaxTypeByCard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_241_MaxTypeByCard] ()
RETURNS @maxtypebycard TABLE
	(
		dim_rnicustomer_cardnumber VARCHAR(20)
		, dim_rnicustomer_customertype INT
	)
AS
BEGIN
	
	INSERT INTO @maxtypebycard (dim_rnicustomer_cardnumber, dim_rnicustomer_customertype)
	SELECT MAXLVL.dim_RNICustomer_CardNumber, xref.dim_typelevelxref_type as dim_rnicustomer_customertype
	FROM
	(	
		SELECT LVL.dim_RNICustomer_CardNumber, MAX(LVL.dim_typelevelxref_level) dim_typelevelxref_level
		FROM
			(
				select rnic.dim_RNICustomer_CardNumber, rnic.dim_RNICustomer_CustomerType, xref.dim_typelevelxref_level
				from rewardsnow.dbo.RNICustomer rnic
				INNER JOIN [241].dbo.TypeLevelXref xref
					ON rnic.dim_RNICustomer_CustomerType = xref.dim_typelevelxref_type
				WHERE sid_dbprocessinfo_dbnumber = '241'
				GROUP BY rnic.dim_RNICustomer_CardNumber, rnic.dim_RNICustomer_CustomerType, xref.dim_typelevelxref_level
			) LVL
		GROUP BY LVL.dim_RNICustomer_CardNumber
	) MAXLVL
	INNER JOIN [241].dbo.TypeLevelXref xref
		ON MAXLVL.dim_typelevelxref_level = xref.dim_typelevelxref_level	

	RETURN
END
GO
