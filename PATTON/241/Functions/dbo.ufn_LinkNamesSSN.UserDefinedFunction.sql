USE [241]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_LinkNamesSSN]    Script Date: 01/25/2012 14:33:26 ******/
DROP FUNCTION [dbo].[ufn_LinkNamesSSN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_LinkNamesSSN] ( @SSN nvarchar(9))

RETURNS @LinkedNames table(SSN nvarchar(9) primary key, Name1 nvarchar(40), Name2 nvarchar(40), Name3 nvarchar(40), Name4 nvarchar(40) )

as
-- RDT Stolen from MetevanteWork

BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(40), SSN nvarchar(9))

	-- Get distinct names from NAME1 column for rows that match the @DDA parm
	Insert into @names
	Select distinct Name1, PrimaryID
		From input_Customer
		Where PrimaryID = @SSN

	-- Get distinct names from NAME2 column for rows that match the @DDA parm
	Insert into @names
		Select distinct Name2, PrimaryID
			From input_Customer
			Where PrimaryID = @SSN and
				Name2 not in (select AcctName from @Names)

	-- Get distinct names from NAME3 column for rows that match the @DDA parm
	Insert into @names
		Select distinct Name3, PrimaryID
			From input_Customer
			Where PrimaryID = @SSN and
				Name3 not in (select AcctName from @Names)

	-- Get distinct names from NAME4 column for rows that match the @DDA parm
	Insert into @names
		Select distinct Name4, PrimaryID
			From input_Customer
				Where PrimaryID = @SSN and
				Name4 not in (select AcctName from @Names)


	-- Flatten out the @names table by returning a single row of up to 4 names 
	Insert into @LinkedNames
	Select	@SSN as SSN, 
			(select AcctName from @names where id=1) as Name1, 
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4

	Return
END
GO
