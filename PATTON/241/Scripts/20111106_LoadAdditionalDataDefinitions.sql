USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO


BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIRawImportDataDefinition]([sid_rnirawimportdatadefinitiontype_id], [sid_rniimportfiletype_id], [sid_dbprocessinfo_dbnumber], [dim_rnirawimportdatadefinition_outputfield], [dim_rnirawimportdatadefinition_outputdatatype], [dim_rnirawimportdatadefinition_sourcefield], [dim_rnirawimportdatadefinition_startindex], [dim_rnirawimportdatadefinition_length], [dim_rnirawimportdatadefinition_multipart], [dim_rnirawimportdatadefinition_multipartpart], [dim_rnirawimportdatadefinition_multipartlength], [dim_rnirawimportdatadefinition_version], [dim_rnirawimportdatadefinition_applyfunction])
SELECT 1, 2, N'241', N'TransactionProcessingCodeANSI_Left2', N'VARCHAR', N'FIELD13', 1, 2, 0, 1, 1, 1, NULL UNION ALL
SELECT 1, 2, N'241', N'PointOfServiceDataCode_Pos8', N'VARCHAR', N'FIELD36', 8, 1, 0, 1, 1, 1, NULL
COMMIT;
RAISERROR (N'[dbo].[RNIRawImportDataDefinition]: Insert Batch: 1 of 1 ....Done!', 10, 1) WITH NOWAIT;
GO

EXEC usp_GenerateRawDataViews '241'
