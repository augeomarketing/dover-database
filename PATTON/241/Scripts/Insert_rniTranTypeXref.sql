



delete from RewardsNow.dbo.rniTrantypeXref
where sid_dbprocessinfo_dbnumber = '241'


insert into RewardsNow.dbo.rniTrantypeXref
(
sid_dbprocessinfo_dbnumber, 
sid_trantype_trancode, 
dim_rnitrantypexref_code01, -- Silver, Gold, Emerald FI Code ( 30109,30110,30115,30119)
dim_rnitrantypexref_code02, -- Signature, Pin  <--???????
dim_rnitrantypexref_code03, -- Purchase Return  <--???????
dim_rnitrantypexref_factor, 
dim_rnitrantypexref_active,
dim_rnitrantypexref_code10 -- Description
) 

( 
select '241' as sid_dbprocessinfo_dbnumber , '3A' as sid_trantype_trancode , 
	  '30109' as dim_rnitrantypexref_code01, 'SIG' as dim_rnitrantypexref_code02,
	  'Return'as dim_rnitrantypexref_code03, .33 as dim_rnitrantypexref_factor,
	  1 as dim_rnitrantypexref_active, 'Silver Sig Ret' as dim_rnitrantypexref_code10
 UNION  select '241', '6A' , '30109', 'SIG','Purch', .33 ,1,'Silver Sig Purch' 
 UNION  select '241', '3D' , '30109', 'PIN','Return', .25 ,1 ,'Silver Pin Ret'
 UNION  select '241', '6D' , '30109', 'PIN','Purch', .25 ,1 ,'Silver Pin Purch' 
 
 UNION  select '241', '3G' , '30110', 'SIG','Return', .5 ,1, 'Gold Sig Ret'  
 UNION  select '241', '6G' , '30110', 'SIG','Purch', .5 ,1,'Gold Sig Purch'  
 UNION  select '241', '3E' , '30110', 'PIN','Return', .25 ,1 ,'Gold Pin Ret' 
 UNION  select '241', '6E' , '30110', 'PIN','Purch', .25 ,1 ,'Gold Pin Purch'

  UNION  select'241', '3C' , '30115', 'SIG','Return', 1 ,1, 'Emerald Sig Ret emp' 
 UNION  select'241', '6C' , '30115', 'SIG','Purch', 1 ,1,'Emerald Sig Purch emp'  
 UNION  select'241', '3F' , '30115', 'PIN','Return', .25 ,1 ,'Emerald Pin Ret emp' 
 UNION  select'241', '6F' , '30115', 'PIN','Purch', .25 ,1 ,'Gold Pin Purch emp' 

  UNION  select'241', '3C' , '30119', 'SIG','Return', 1 ,1, 'Emerald Sig Ret'  
 UNION  select'241', '6C' , '30119', 'SIG','Purch', 1 ,1,'Emerald Sig Purch'  
 UNION  select'241', '3F' , '30119', 'PIN','Return', .25 ,1 ,'Emerald Pin Ret' 
 UNION  select'241', '6F' , '30119', 'PIN','Purch', .25 ,1 ,'Gold Pin Purch' 
)


select 
sid_dbprocessinfo_dbnumber, 
sid_trantype_trancode, 
dim_rnitrantypexref_code01, -- Silver, Gold, Emerald FI Code ( 30109,30110,30115,30119)
dim_rnitrantypexref_code02, -- Signature, Pin  <--???????
dim_rnitrantypexref_code03, -- Purchase Return  <--???????
dim_rnitrantypexref_factor, 
dim_rnitrantypexref_active,
dim_rnitrantypexref_code10 -- Description
from RewardsNow.dbo.rniTrantypeXref where sid_dbprocessinfo_dbnumber = '241'