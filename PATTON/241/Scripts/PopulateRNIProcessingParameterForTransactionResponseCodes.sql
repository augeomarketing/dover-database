USE [241]
GO

DELETE FROM RewardsNow.dbo.RNIProcessingParameter 
WHERE sid_dbprocessinfo_dbnumber = '241' AND dim_rniprocessingparameter_key like 'TRANSACTIONRESPONSECODE%'


INSERT INTO RewardsNow.dbo.RNIProcessingParameter 
(
	sid_dbprocessinfo_dbnumber
	, dim_rniprocessingparameter_key
	, dim_rniprocessingparameter_value
	, dim_rniprocessingparameter_active
)
SELECT '241' AS sid_dbprocessinfo_dbnumber
	, 'TRANSACTIONRESPONSECODE_' + trc_s.TransactionReponseCode_S + trc_rr.TransactionReponseCode_RR AS dim_rniprocessingparameter_key
	, trc_s.TransactionReponseCode_S + trc_rr.TransactionReponseCode_RR as dim_rniprocessingparameter_value
	, 1 as dim_rniprocessingparameter_active
FROM
	(
		SELECT '0' AS TransactionReponseCode_S
		UNION SELECT '5'	
	) trc_s
	CROSS JOIN
	(
		SELECT '0A' AS TransactionReponseCode_RR
		UNION SELECT '00'	
		UNION SELECT '01'	
		UNION SELECT '02'	
		UNION SELECT '03'	
		UNION SELECT '04'	
		UNION SELECT '05'	
		UNION SELECT '06'	
	) trc_rr


DELETE FROM RewardsNow.dbo.RNIProcessingParameter 
WHERE sid_dbprocessinfo_dbnumber = '241' AND dim_rniprocessingparameter_key like 'VALIDMESSAGETYPE%'


INSERT INTO RewardsNow.dbo.RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
SELECT '241', 'VALIDMESSAGETYPE_1110', '1110', 1
UNION SELECT '241', 'VALIDMESSAGETYPE_1210', '1210', 1
UNION SELECT '241', 'VALIDMESSAGETYPE_1420', '1420', 1


DELETE FROM RewardsNow.dbo.RNIProcessingParameter
WHERE sid_dbprocessinfo_dbnumber = '241' and dim_rniprocessingparameter_key LIKE 'TRANSACTIONPROCESSINGCODE%'

INSERT INTO RewardsNow.dbo.RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
SELECT '241', 'TRANSACTIONPROCESSINGCODE_010020', '010020', 1
UNION SELECT '241', 'TRANSACTIONPROCESSINGCODE_440020', '440020', 1
UNION SELECT '241', 'TRANSACTIONPROCESSINGCODE_450020', '450020', 1

