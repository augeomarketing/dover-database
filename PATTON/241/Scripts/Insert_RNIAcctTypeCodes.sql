use RewardsNow 
truncate table  RNIAcctType 
--insert into RNIAcctType 
--(  dim_RNIAcctType_ShortDescription,  dim_RNIAcctType_LongDescription )
--values 
--( 'DDA' , 'DDA')


insert into RNIAcctType 
  (dim_RNIAcctType_Active,  dim_RNIAcctType_ShortDescription,  dim_RNIAcctType_LongDescription )

select 1 as dim_RNIAcctType_Active,
t1.dim_RNIAcctType_ShortDescription as dim_RNIAcctType_ShortDescription,
t1.dim_RNIAcctType_LongDescription as dim_RNIAcctType_LongDescription
from 
(
select 'Credit' as dim_RNIAcctType_ShortDescription,  'Credit Card' as dim_RNIAcctType_LongDescription 
Union select 'Debit', 'Debit Card'
Union select 'TaxID','TIN or SSN Number '
Union select 'DDA','DDA Number'

) t1

--  select * from RNIAcctType 
  