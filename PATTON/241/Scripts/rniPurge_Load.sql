
truncate table rniPurge

insert into rniPurge
(dim_rniPurge_TipFirst, dim_rniPurge_AccountType , dim_rniPurge_Account , dim_rniPurge_DateAdded, dim_rniPurge_DateRecent)
select 
	left(af.TIPNUMBER ,3) , af.AcctType, af.ACCTID , min (histdate) , max(histdate) 
from AFFILIAT af 
		join HISTORY hi on hi.ACCTID = af.ACCTID 
	where hi.ACCTID  is not null 
	and hi.TRANCODE like '[36]%'
	group by left(af.TIPNUMBER,3) , af.AcctType, af.ACCTID 

select 
dim_rniPurge_DateRecent - dim_rniPurge_DateAdded  
from rniPurge


-------- Update the customer with the status of the active debit card. 
	Update  cst 
	Set status =  af.MinStat 
	From Customer cst 
		Join 	( select tipnumber , AcctType , MIN (acctstatus) as MinStat from AFFILIAT group by TIPNUMBER , AcctType ) as af 
			on cst.TIPNUMBER = af.TIPNUMBER
	Where af.AcctType = 'debit' 
	
	