--  Bonus data definition  - Relationship

delete from 
RNIRawImportDataDefinition 
where sid_dbprocessinfo_dbnumber = '241' and 
dim_rnirawimportdatadefinition_version = 1 and 
sid_rniimportfiletype_id = 5


INSERT INTO RNIRawImportDataDefinition 
(
      sid_rnirawimportdatadefinitiontype_id
      , sid_rniimportfiletype_id
      , sid_dbprocessinfo_dbnumber
      , dim_rnirawimportdatadefinition_outputfield
      , dim_rnirawimportdatadefinition_outputdatatype
      , dim_rnirawimportdatadefinition_sourcefield
      , dim_rnirawimportdatadefinition_startindex
      , dim_rnirawimportdatadefinition_length
      , dim_rnirawimportdatadefinition_multipart
      , dim_rnirawimportdatadefinition_multipartpart
      , dim_rnirawimportdatadefinition_multipartlength
      , dim_rnirawimportdatadefinition_version
)
SELECT 
      2 as sid_rnirawimportdatadefinitiontype_id
      , 5 as sid_rniimportfiletype_id
      , t2.dbnumber as sid_dbprocessinfo_dbnumber
      , t1.OutputField as dim_rnirawimportdatadefinition_outputfield
      , 'VARCHAR' as dim_rnirawimportdatadefinition_outputdatatype
      , t1.SourceField as dim_rnirawimportdatadefinition_sourcefield
      , 1 as dim_rnirawimportdatadefinition_startindex
      , 255 as dim_rnirawimportdatadefinition_length
      , 0 as dim_rnirawimportdatadefinition_multipart
      , 1 as dim_rnirawimportdatadefinition_multipartpart
      , 1 as dim_rnirawimportdatadefinition_multipartlength
      , 1 as dim_rnirawimportdatadefinition_version
FROM
(
      SELECT 'RecordType' as OutputField, 'Field01' as SourceField UNION
      SELECT 'ClientID'						, 'Field02' UNION
      SELECT 'AccountID'					, 'Field03' UNION
      SELECT 'Cardnumber'					, 'Field04' UNION
      SELECT 'AdjustDescription'		, 'Field05' UNION
      SELECT 'AdjustIndicator'			, 'Field06' UNION
      SELECT 'AdjustDate'					,'Field07' UNION
     SELECT 'AdjustAmount'				, 'Field08'  UNION
     SELECT 'BonusFileType'				, 'Field59'  
     
) t1
CROSS JOIN 
(
      SELECT '241' as dbnumber 
) t2
ORDER BY t2.dbnumber, t1.SourceField
