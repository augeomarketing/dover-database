USE [rewardsnow]
GO

set nocount on;

--RESET
--UPDATE RewardsNow.dbo.vw_241_TRAN_SOURCE_1 SET sid_rnirawimportstatus_id = 0 WHERE sid_rnirawimportstatus_id = 2;

-- SELECT * FROM REWARDSNOW.DBO.RNIPROCESSINGPARAMETER WHERE dim_rniprocessingparameter_key like 'VALIDMESSAGETYPE%'

WITH validtype(msgtype)
AS
(
	SELECT dim_rniprocessingparameter_value as msgtype
	FROM RewardsNow.dbo.RNIProcessingParameter
	WHERE 
		dim_rniprocessingparameter_key LIKE 'VALIDMESSAGETYPE%'
		AND sid_dbprocessinfo_dbnumber = '241'
		AND dim_rniprocessingparameter_active = 1
)
UPDATE t
SET sid_rnirawimportstatus_id = 2
--SELECT t.MessageType
FROM rewardsnow.dbo.vw_241_TRAN_SOURCE_1 t
LEFT OUTER JOIN validtype m
	ON t.MessageType = m.msgtype
WHERE
	m.msgtype is null
	and t.sid_rnirawimportstatus_id = 0;

--SELECT * FROM REWARDSNOW.DBO.RNIPROCESSINGPARAMETER WHERE dim_rniprocessingparameter_key like 'TRANSACTIONRESPONSECODE%'
	
WITH responsecode(code)
AS
(
	SELECT dim_rniprocessingparameter_value as msgtype
	FROM RewardsNow.dbo.RNIProcessingParameter
	WHERE 
		dim_rniprocessingparameter_key LIKE 'TRANSACTIONRESPONSECODE%'
		AND sid_dbprocessinfo_dbnumber = '241'
		AND dim_rniprocessingparameter_active = 1
)
UPDATE t
SET sid_rnirawimportstatus_id = 2
--SELECT t.MessageType
FROM rewardsnow.dbo.vw_241_TRAN_SOURCE_1 t
LEFT OUTER JOIN  responsecode r
	ON t.TransactionResponseCode = r.code
WHERE
	r.code is null
	and t.sid_rnirawimportstatus_id = 0;

--SELECT * FROM REWARDSNOW.DBO.RNIPROCESSINGPARAMETER WHERE dim_rniprocessingparameter_key like 'TRANSACTIONPROCESSINGCODE%'

WITH processingcode(code)
AS
(
	SELECT dim_rniprocessingparameter_value as msgtype
	FROM RewardsNow.dbo.RNIProcessingParameter
	WHERE 
		dim_rniprocessingparameter_key LIKE 'TRANSACTIONPROCESSINGCODE%'
		AND sid_dbprocessinfo_dbnumber = '241'
		AND dim_rniprocessingparameter_active = 1
)
UPDATE t
SET sid_rnirawimportstatus_id = 2
--SELECT t.MessageType
FROM rewardsnow.dbo.vw_241_TRAN_SOURCE_1 t
LEFT OUTER JOIN  processingcode p
	ON t.TransactionProcessingCodeANSI = p.code
WHERE
	p.code is null
	and t.sid_rnirawimportstatus_id = 0;


--select * from vw_241_TRAN_SOURCE_1 where sid_rnirawimportstatus_id = 0;

DECLARE @xref TABLE
(
	TransactionProcessingCodeANSI varchar(10)
	, MessageType varchar(4)
	, DebitCreditFlag varchar(2)
	, PointOfServiceDataCodePos8 varchar(1)
	, CustomerType varchar(10)
	, dim_rnitrantypexref_factor decimal(24,5)
	, sid_trantype_trancode varchar(2)
)	

INSERT INTO @xref 
(
	TransactionProcessingCodeANSI, MessageType, DebitCreditFlag 
	, PointOfServiceDataCodePos8, CustomerType, dim_rnitrantypexref_factor, sid_trantype_trancode
)
SELECT 
	dim_rnitrantypexref_code02 as TransactionProcessingCodeANSI 
	, dim_rnitrantypexref_code01 as MessageType
	, dim_rnitrantypexref_code03 as DebitCreditFlag 
	, dim_rnitrantypexref_code04 as PointOfServiceDataCodePos8
	, dim_rnitrantypexref_code05 as CustomerType
	, dim_rnitrantypexref_factor
	, sid_trantype_trancode
FROM
	Rewardsnow.dbo.rniTrantypeXref
WHERE
	sid_dbprocessinfo_dbnumber = '241'
	and dim_rnitrantypexref_active = 1
	and dim_rnitrantypexref_code04 is not null

SELECT 
	a.CardNumber
	, t.IssuerFundsTransactionAmount
	, x.dim_rnitrantypexref_factor
	, RewardsNow.dbo.ufn_CalculatePoints(t.IssuerFundsTransactionAmount, x.dim_rnitrantypexref_factor, 2) as points 
	, x.sid_trantype_trancode
FROM vw_241_TRAN_SOURCE_1 t
inner join rewardsnow.dbo.vw_241_ACCT_TARGET_226 a
	on t.PrimaryAccountNumber = a.CardNumber
inner join @xref x
	on t.TransactionProcessingCodeANSI = x.TransactionProcessingCodeANSI
		and t.MessageType = x.MessageType
		and t.DebitCreditFlag = x.DebitCreditFlag
		and substring(t.PointOfServiceDataCode, 8, 1) = x.PointOfServiceDataCodePos8
		and a.CustomerType = x.CustomerType
where
	t.sid_rnirawimportstatus_id = 0;

	
