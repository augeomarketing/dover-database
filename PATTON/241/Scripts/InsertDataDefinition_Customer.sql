use RewardsNow
/* 
delete from RNIRawImportDataDefinition 
	where sid_dbprocessinfo_dbnumber = '241' 
		and dim_rnirawimportdatadefinition_version = '226'
		and sid_rniimportfiletype_id = 1
*/
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'PortfolioId', 'VARCHAR','Field01',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'MemberId', 'VARCHAR','Field02',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'PrimaryId', 'VARCHAR','Field03',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'DDA', 'VARCHAR','Field04',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'PrimaryFlag', 'VARCHAR','Field05',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'NAME1', 'VARCHAR','Field06',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'NAME2', 'VARCHAR','Field07',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'NAME3', 'VARCHAR','Field08',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'NAME4', 'VARCHAR','Field09',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'ADDRESS1', 'VARCHAR','Field10',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'ADDRESS2', 'VARCHAR','Field11',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'ADDRESS3', 'VARCHAR','Field12',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'City', 'VARCHAR','Field13',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'State', 'VARCHAR','Field14',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'Country', 'VARCHAR','Field15',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'PostalCode', 'VARCHAR','Field16',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'PrimaryPhone', 'VARCHAR','Field17',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'MobilePhone', 'VARCHAR','Field18',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'CustomerCode', 'VARCHAR','Field19',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'BusinessFlag', 'VARCHAR','Field20',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'EmployeeFlag', 'VARCHAR','Field21',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'InstitionID', 'VARCHAR','Field22',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'CardNumber', 'VARCHAR','Field23',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'Email', 'VARCHAR','Field24',1,300,0,1,1, 226 ) 
insert into RNIRawImportDataDefinition
( sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, sid_dbprocessinfo_dbnumber, 
dim_rnirawimportdatadefinition_outputfield, dim_rnirawimportdatadefinition_outputdatatype,dim_rnirawimportdatadefinition_sourcefield,
dim_rnirawimportdatadefinition_startindex, dim_rnirawimportdatadefinition_length, dim_rnirawimportdatadefinition_multipart, 
dim_rnirawimportdatadefinition_multipartpart,dim_rnirawimportdatadefinition_multipartlength, 
dim_rnirawimportdatadefinition_version )
values 
( 2, 1, '241', 'CustomerType', 'VARCHAR','Field25',1,300,0,1,1, 226 ) 



-- SELECT  * FROM RNIRawImportDataDefinition where sid_dbprocessinfo_dbnumber = '241' and dim_rnirawimportdatadefinition_version = '226'
-- and sid_rniimportfiletype_id = 1




-- 
-- execute dbo.usp_GenerateRawDataViews '241',
execute dbo.usp_GenerateRawDataViews '241','',1

select * from  dbo.vw_241_ACCT_TARGET_226
