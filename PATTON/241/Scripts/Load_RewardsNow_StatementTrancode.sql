
delete from [RewardsNow].dbo.StatementTranCode where dim_StatementTranCode_Tip = '241'

insert into [RewardsNow].dbo.StatementTranCode
(dim_StatementTranCode_Tip,dim_StatementTranCode_EffectiveStart, dim_StatementTranCode_ColumnName,dim_StatementTranCode_TranCode)

select 
'241' as dim_StatementTranCode_Tip,
'08/31/2011' as  dim_StatementTranCode_EffectiveStart,
t1.ColumnName as dim_StatementTranCode_ColumnName, 
t1.TranCode  as dim_StatementTranCode_TranCode
From 
(

-- 6A  - Silver Debit Sig Purchase 
-- 3A - Silver Debit Sig Return
-- 6D  - Silver Debit PIN Purchase 
-- 3D - Silver Debit PIN Return

-- 6G  - Gold Debit Sig Purchase 
-- 3G - Gold Debit Sig Return
-- 6E  - Gold Debit PIN Purchase 
-- 3E - Gold Debit PIN Return

-- 6C  - Emerald Debit Sig Purchase 
-- 3C - Emerald Debit Sig Return
-- 6F  - Emerald Debit PIN Purchase 
-- 3F - Emerald Debit PIN Return


	select 'PointsPurchasedDB' as ColumnName , '6A' as TranCode UNION
	select 'PointsReturnedDB'		,'3A' UNION
	select 'PointsPurchasedDB'		,'6D' UNION
	select 'pointsReturnedDB'		,'3D' UNION
	
	select 'PointsPurchasedDB'		,'6G' UNION
	select 'pointsReturnedDB'		,'3G' UNION
	select 'PointsPurchasedDB'		,'6E' UNION
	select 'pointsReturnedDB'		,'3E' UNION

	select 'PointsPurchasedDB'		,'6C' UNION
	select 'pointsReturnedDb'		,'3C' UNION
	select 'PointsPurchasedDB'		,'6F' UNION
	select 'pointsReturnedDB'		,'3F' UNION

	select 'PointsIncreased','6A' UNION
	select 'pointsIncreased','6D' UNION
	select 'PointsIncreased','6G' UNION
	select 'pointsIncreased','6E' UNION
	select 'PointsIncreased','6C' UNION
	select 'pointsIncreased','6F' UNION

	select 'pointsdecreased','3A' UNION
	select 'pointsdecreased','3D' UNION
	select 'pointsdecreased','3G' UNION
	select 'pointsdecreased','3E' UNION
	select 'pointsdecreased','3C' UNION
	select 'pointsdecreased','3F' UNION

	select	'PointsBonus',			'BA'	 UNION
	select	'PointsBonus',			'BI' UNION
	select	'PointsBonus',			'BE' UNION
	select	'PointsBonus',			'BN' UNION
	select	'PointsBonusMN',	'F0' UNION
	select 'PointsBonusMN',	'F9' UNION
	select 'PointsBonusMN',	'G0' UNION
	select 'PointsBonusMN',	'G9' UNION
	select 'PointsBonus',			'BA' UNION
	select 'PointsBonus',			'BE' UNION
	select 'PointsBonus',			'BN' UNION
	select 'PointsBonus',			'BI' UNION
	select 'PointsBonus',			'F0' UNION
	select 'PointsBonus',			'F9' UNION
	select 'PointsBonus',			'G0' UNION
	select 'PointsBonus',			'G9' UNION
	select 'pointsadded',			'IE' UNION
	select 'pointsadded',			'DR' UNION
	select 'pointsadded',			'XF' UNION
	select 'pointsredeemed','RC' UNION
	select 'pointsredeemed','RD' UNION
	select 'pointsredeemed','RM' UNION
	select 'pointsredeemed','RQ' UNION
	select 'pointsredeemed','RT' UNION
	select 'pointsredeemed','RU' UNION
	select 'pointsredeemed','RV' UNION
	select 'pointssubtracted','IR' UNION
	select 'pointssubtracted','DE' UNION
	select 'pointssubtracted','XP' UNION
	select 'PointsIncreased','BA' UNION
	select 'PointsIncreased','BE' UNION
	select 'PointsIncreased','BN' UNION
	select 'PointsIncreased','BI' UNION
	select 'PointsIncreased','F0' UNION
	select 'PointsIncreased','F9' UNION
	select 'PointsIncreased','G0' UNION
	select 'PointsIncreased','G9' UNION
	select 'PointsIncreased','IE' UNION
	select 'PointsIncreased','DR' UNION 
	select 'PointsIncreased','XF' UNION
	select 'pointsdecreased','RC' UNION
	select 'pointsdecreased','RD' UNION
	select 'pointsdecreased','RM' UNION
	select 'pointsdecreased','RQ' UNION
	select 'pointsdecreased','RT' UNION
	select 'pointsdecreased','RU' UNION
	select 'pointsdecreased','RV' UNION
	select 'pointsdecreased','IR' UNION
	select 'pointsdecreased','XP' UNION
	select 'pointsdecreased','DE' 	

) t1
CROSS JOIN 
( Select '241' as dbnumber ) t2 order by Trancode 

