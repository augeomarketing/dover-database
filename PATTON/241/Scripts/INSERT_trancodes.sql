--select * from 
--RewardsNow.dbo.TranType 
--where TranCode in 
--(
--'3A','6A','3G','6G','3C','6C','3D','6D','3E','6E','3F','6F'
--)
--order by Description 


insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('3A','Silver Debit Sig Return','D','A',1,-1,7)

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('3D','Silver Debit PIN Return','D','A',1,-1,7)

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('3G','Gold Debit Sig Return','D','A',1,-1,7)

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('3E','Gold Debit PIN Return','D','A',1,-1,7)

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('3F','Emerald Debit PIN Return','D','A',1,-1,7)

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('3C','Emerald Debit Sig Return','D','A',1,-1,7)


insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('6A', 'Silver Debit Sig Purchase','I','A',1,1,7) 

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('6C', 'Emerald Debit Sig Purchase','I','A',1,1,7) 

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('6D', 'Silver Debit PIN Purchase','I','A',1,1,7) 

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('6G', 'Gold Debit Sig Purchase','I','A',1,1,7) 

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('6E', 'Gold Debit PIN Purchase','I','A',1,1,7) 

insert into RewardsNow.dbo.TranType 
(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
values 
('6F', 'Emerald Debit PIN Purchase','I','A',1,1,7) 


-- 3A- Silver Debit Sig Return
-- 6A  - Silver Debit Sig Purchase 
-- 3G - Gold Debit Sig Return
-- 6G  - Gold Debit Sig Purchase 
-- 3C - Emerald Debit Sig Return
-- 6C  - Emerald Debit Sig Purchase 
-- 3D - Silver Debit PIN Return
-- 6D  - Silver Debit PIN Purchase 
-- 3E - Gold Debit PIN Return
-- 6E  - Gold Debit PIN Purchase 
-- 3F - Emerald Debit PIN Return
-- 6F  - Emerald Debit PIN Purchase 