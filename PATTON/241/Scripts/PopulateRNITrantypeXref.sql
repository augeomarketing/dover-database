USE [241]
GO

INSERT INTO Rewardsnow.dbo.RNITranTypeXref (sid_dbprocessinfo_dbnumber, dim_rnitrantypexref_code01, dim_rnitrantypexref_code02, dim_rnitrantypexref_code03, dim_rnitrantypexref_code04, dim_rnitrantypexref_code05, dim_rnitrantypexref_code10, sid_trantype_trancode, dim_rnitrantypexref_factor)
SELECT '241', '1110', '010020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1110', '010020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1110', '010020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1110', '010020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1110', '010020','CR','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '010020','CR','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '010020','CR','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '010020','CR','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '010020','CR','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '010020','CR','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '010020','CR','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '010020','CR','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '010020','CR','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '010020','CR','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '010020','CR','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '010020','CR','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '010020','DB','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1110', '010020','DB','1','30110','Gold PIN','6F',.250
UNION SELECT '241', '1110', '010020','DB','1','30115','Emerald PIN','6F',.250
UNION SELECT '241', '1110', '010020','DB','1','30119','Employee Emerald PIN','6F',.250
UNION SELECT '241', '1110', '010020','DB','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '010020','DB','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '010020','DB','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '010020','DB','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '010020','DB','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '010020','DB','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '010020','DB','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '010020','DB','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '010020','DB','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '010020','DB','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '010020','DB','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '010020','DB','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '440020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1110', '440020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1110', '440020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1110', '440020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1110', '440020','CR','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '440020','CR','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '440020','CR','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '440020','CR','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '440020','CR','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '440020','CR','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '440020','CR','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '440020','CR','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '440020','CR','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '440020','CR','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '440020','CR','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '440020','CR','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '440020','DB','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1110', '440020','DB','1','30110','Gold PIN','6F',.250
UNION SELECT '241', '1110', '440020','DB','1','30115','Emerald PIN','6F',.250
UNION SELECT '241', '1110', '440020','DB','1','30119','Employee Emerald PIN','6F',.250
UNION SELECT '241', '1110', '440020','DB','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '440020','DB','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '440020','DB','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '440020','DB','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '440020','DB','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '440020','DB','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '440020','DB','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '440020','DB','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '440020','DB','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '440020','DB','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '440020','DB','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '440020','DB','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '450020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1110', '450020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1110', '450020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1110', '450020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1110', '450020','CR','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '450020','CR','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '450020','CR','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '450020','CR','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '450020','CR','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '450020','CR','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '450020','CR','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '450020','CR','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '450020','CR','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1110', '450020','CR','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1110', '450020','CR','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1110', '450020','CR','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1110', '450020','DB','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1110', '450020','DB','1','30110','Gold PIN','6E',.250
UNION SELECT '241', '1110', '450020','DB','1','30115','Emerald PIN','6C',.250
UNION SELECT '241', '1110', '450020','DB','1','30119','Employee Emerald PIN','6C',.250
UNION SELECT '241', '1110', '450020','DB','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '450020','DB','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '450020','DB','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '450020','DB','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '450020','DB','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '450020','DB','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '450020','DB','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '450020','DB','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1110', '450020','DB','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1110', '450020','DB','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1110', '450020','DB','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1110', '450020','DB','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '010020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1210', '010020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1210', '010020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1210', '010020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1210', '010020','CR','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '010020','CR','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '010020','CR','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '010020','CR','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '010020','CR','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '010020','CR','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '010020','CR','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '010020','CR','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '010020','CR','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '010020','CR','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '010020','CR','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '010020','CR','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '010020','DB','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1210', '010020','DB','1','30110','Gold PIN','6F',.250
UNION SELECT '241', '1210', '010020','DB','1','30115','Emerald PIN','6F',.250
UNION SELECT '241', '1210', '010020','DB','1','30119','Employee Emerald PIN','6F',.250
UNION SELECT '241', '1210', '010020','DB','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '010020','DB','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '010020','DB','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '010020','DB','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '010020','DB','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '010020','DB','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '010020','DB','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '010020','DB','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '010020','DB','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '010020','DB','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '010020','DB','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '010020','DB','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '440020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1210', '440020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1210', '440020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1210', '440020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1210', '440020','CR','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '440020','CR','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '440020','CR','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '440020','CR','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '440020','CR','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '440020','CR','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '440020','CR','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '440020','CR','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '440020','CR','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '440020','CR','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '440020','CR','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '440020','CR','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '440020','DB','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1210', '440020','DB','1','30110','Gold PIN','6F',.250
UNION SELECT '241', '1210', '440020','DB','1','30115','Emerald PIN','6F',.250
UNION SELECT '241', '1210', '440020','DB','1','30119','Employee Emerald PIN','6F',.250
UNION SELECT '241', '1210', '440020','DB','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '440020','DB','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '440020','DB','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '440020','DB','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '440020','DB','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '440020','DB','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '440020','DB','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '440020','DB','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '440020','DB','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '440020','DB','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '440020','DB','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '440020','DB','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '450020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1210', '450020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1210', '450020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1210', '450020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1210', '450020','CR','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '450020','CR','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '450020','CR','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '450020','CR','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '450020','CR','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '450020','CR','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '450020','CR','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '450020','CR','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '450020','CR','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1210', '450020','CR','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1210', '450020','CR','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1210', '450020','CR','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1210', '450020','DB','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1210', '450020','DB','1','30110','Gold PIN','6E',.250
UNION SELECT '241', '1210', '450020','DB','1','30115','Emerald PIN','6C',.250
UNION SELECT '241', '1210', '450020','DB','1','30119','Employee Emerald PIN','6C',.250
UNION SELECT '241', '1210', '450020','DB','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '450020','DB','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '450020','DB','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '450020','DB','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '450020','DB','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '450020','DB','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '450020','DB','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '450020','DB','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1210', '450020','DB','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1210', '450020','DB','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1210', '450020','DB','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1210', '450020','DB','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','CR','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1420', '010020','CR','1','30110','Gold PIN','6E',.250
UNION SELECT '241', '1420', '010020','CR','1','30115','Emerald PIN','6F',.250
UNION SELECT '241', '1420', '010020','CR','1','30119','Employee Emerald PIN','6F',.250
UNION SELECT '241', '1420', '010020','CR','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '010020','CR','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '010020','CR','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','CR','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','CR','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '010020','CR','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '010020','CR','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','CR','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','CR','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '010020','CR','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '010020','CR','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','CR','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '010020','DB','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1420', '010020','DB','1','30110','Gold PIN','3F',.250
UNION SELECT '241', '1420', '010020','DB','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1420', '010020','DB','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1420', '010020','DB','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '010020','DB','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '010020','DB','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '010020','DB','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '010020','DB','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '010020','DB','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '010020','DB','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '010020','DB','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '010020','DB','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '010020','DB','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '010020','DB','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '010020','DB','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '440020','CR','1','30109','Silver PIN','6D',.250
UNION SELECT '241', '1420', '440020','CR','1','30110','Gold PIN','6F',.250
UNION SELECT '241', '1420', '440020','CR','1','30115','Emerald PIN','6F',.250
UNION SELECT '241', '1420', '440020','CR','1','30119','Employee Emerald PIN','6F',.250
UNION SELECT '241', '1420', '440020','CR','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '440020','CR','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '440020','CR','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '440020','CR','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '440020','CR','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '440020','CR','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '440020','CR','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '440020','CR','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '440020','CR','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '440020','CR','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '440020','CR','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '440020','CR','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '440020','DB','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1420', '440020','DB','1','30110','Gold PIN','3F',.250
UNION SELECT '241', '1420', '440020','DB','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1420', '440020','DB','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1420', '440020','DB','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '440020','DB','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '440020','DB','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '440020','DB','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '440020','DB','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '440020','DB','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '440020','DB','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '440020','DB','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '440020','DB','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '440020','DB','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '440020','DB','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '440020','DB','6','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '450020','CR','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1420', '450020','CR','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1420', '450020','CR','1','30115','Emerald PIN','3F',.250
UNION SELECT '241', '1420', '450020','CR','1','30119','Employee Emerald PIN','3F',.250
UNION SELECT '241', '1420', '450020','CR','2','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '450020','CR','2','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '450020','CR','2','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '450020','CR','2','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '450020','CR','5','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '450020','CR','5','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '450020','CR','5','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '450020','CR','5','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '450020','CR','6','30109','Silver Signature','6A',.330
UNION SELECT '241', '1420', '450020','CR','6','30110','Gold Signature','6G',.500
UNION SELECT '241', '1420', '450020','CR','6','30115','Emerald Signature','6C',1
UNION SELECT '241', '1420', '450020','CR','6','30119','Employee Emerald Signature','6C',1
UNION SELECT '241', '1420', '450020','DB','1','30109','Silver PIN','3D',.250
UNION SELECT '241', '1420', '450020','DB','1','30110','Gold PIN','3E',.250
UNION SELECT '241', '1420', '450020','DB','1','30115','Emerald PIN','3C',.250
UNION SELECT '241', '1420', '450020','DB','1','30119','Employee Emerald PIN','3C',.250
UNION SELECT '241', '1420', '450020','DB','2','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '450020','DB','2','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '450020','DB','2','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '450020','DB','2','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '450020','DB','5','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '450020','DB','5','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '450020','DB','5','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '450020','DB','5','30119','Employee Emerald Signature','3C',1
UNION SELECT '241', '1420', '450020','DB','6','30109','Silver Signature','3A',.330
UNION SELECT '241', '1420', '450020','DB','6','30110','Gold Signature','3G',.500
UNION SELECT '241', '1420', '450020','DB','6','30115','Emerald Signature','3C',1
UNION SELECT '241', '1420', '450020','DB','6','30119','Employee Emerald Signature','3C',1
