declare @NewStartDate date
declare @NewEndDate      date
declare @TipFirst      varchar(3)

set @NewStartDate = '09/01/2011'
set @TipFirst = '241'

-- set @NewStartDate = dateadd(mm, 1, @NewStartDate)
set @NewEndDate = dateadd(dd, -1, dateadd(mm, 1, @NewStartDate))


exec Rewardsnow.dbo.spUpdateSSISConfigurations @TipFirst, @NewStartDate, '\Package.Variables[User::DateStart].Properties[Value]'
exec Rewardsnow.dbo.spUpdateSSISConfigurations @TipFirst, @NewEndDate, '\Package.Variables[User::DateEnd].Properties[Value]'

select * from RewardsNow.dbo.[SSIS Configurations]  where ConfigurationFilter like '241%'

