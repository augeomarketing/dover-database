USE [241]
GO
/****** Object:  View [dbo].[vw_AllAffiliat]    Script Date: 01/25/2012 14:33:26 ******/
DROP VIEW [dbo].[vw_AllAffiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_AllAffiliat]
AS

SELECT
	ACCTID
	, TIPNUMBER
	, AcctType
	, DATEADDED
	, SECID
	, AcctStatus
	, AcctTypeDesc
	, LastName
	, YTDEarned
	, CustID
FROM AFFILIAT_Stage

UNION

SELECT
	af.ACCTID
	, af.TIPNUMBER
	, af.AcctType
	, af.DATEADDED
	, af.SECID
	, af.AcctStatus
	, af.AcctTypeDesc
	, af.LastName
	, af.YTDEarned
	, af.CustID
FROM AFFILIAT af
LEFT OUTER JOIN AFFILIAT_Stage afs
ON af.ACCTID = afs.ACCTID
	AND af.TIPNUMBER = afs.TIPNUMBER
WHERE afs.TIPNUMBER is null
GO
