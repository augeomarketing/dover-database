USE [241]
GO
/****** Object:  View [dbo].[vw_AllHistory]    Script Date: 01/25/2012 14:33:26 ******/
DROP VIEW [dbo].[vw_AllHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_AllHistory]
AS

SELECT
	TIPNUMBER
	, ACCTID
	, HISTDATE
	, TRANCODE
	, TranCount
	, POINTS
	, Description
	, SECID
	, Ratio
	, Overage
FROM
	HISTORY

UNION ALL

SELECT
	TIPNUMBER
	, ACCTID
	, HISTDATE
	, TRANCODE
	, TranCount
	, POINTS
	, Description
	, SECID
	, Ratio
	, Overage
FROM
	HISTORY_Stage
WHERE
	ISNULL(SECID, 'NEW') = 'NEW'
GO
