USE [241]
GO
/****** Object:  View [dbo].[vw_histpoints]    Script Date: 01/25/2012 14:33:26 ******/
DROP VIEW [dbo].[vw_histpoints]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vw_histpoints] as select tipnumber, sum(points*ratio) as points from history_stage where secid = 'NEW'group by tipnumber
GO
