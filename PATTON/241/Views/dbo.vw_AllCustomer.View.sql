USE [241]
GO
/****** Object:  View [dbo].[vw_AllCustomer]    Script Date: 01/25/2012 14:33:26 ******/
DROP VIEW [dbo].[vw_AllCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  View [dbo].[vw_AllCustomer]    Script Date: 10/25/2011 12:34:42 ******/


CREATE VIEW [dbo].[vw_AllCustomer]
as

SELECT
	TIPNUMBER
	, RunAvailable
	, RUNBALANCE
	, RunRedeemed
	, LastStmtDate
	, NextStmtDate
	, STATUS
	, DATEADDED
	, LASTNAME
	, TIPFIRST
	, TIPLAST
	, ACCTNAME1
	, ACCTNAME2
	, ACCTNAME3
	, ACCTNAME4
	, ACCTNAME5
	, ACCTNAME6
	, ADDRESS1
	, ADDRESS2
	, ADDRESS3
	, ADDRESS4
	, City
	, State
	, ZipCode
	, StatusDescription
	, HOMEPHONE
	, WORKPHONE
	, BusinessFlag
	, EmployeeFlag
	, SegmentCode
	, ComboStmt
	, RewardsOnline
	, NOTES
	, BonusFlag
	, Misc1
	, Misc2
	, Misc3
	, Misc4
	, Misc5
	, RunBalanceNew
	, RunAvaliableNew
	, 1 as source
FROM CUSTOMER_Stage
UNION 
SELECT
	c.TIPNUMBER
	, c.RunAvailable
	, c.RUNBALANCE
	, c.RunRedeemed
	, c.LastStmtDate
	, c.NextStmtDate
	, c.STATUS
	, c.DATEADDED
	, c.LASTNAME
	, c.TIPFIRST
	, c.TIPLAST
	, c.ACCTNAME1
	, c.ACCTNAME2
	, c.ACCTNAME3
	, c.ACCTNAME4
	, c.ACCTNAME5
	, c.ACCTNAME6
	, c.ADDRESS1
	, c.ADDRESS2
	, c.ADDRESS3
	, c.ADDRESS4
	, c.City
	, c.State
	, c.ZipCode
	, c.StatusDescription
	, c.HOMEPHONE
	, c.WORKPHONE
	, c.BusinessFlag
	, c.EmployeeFlag
	, c.SegmentCode
	, c.ComboStmt
	, c.RewardsOnline
	, c.NOTES
	, c.BonusFlag
	, c.Misc1
	, c.Misc2
	, c.Misc3
	, c.Misc4
	, c.Misc5
	, c.RunBalanceNew
	, c.RunAvaliableNew
	, 0 as source
FROM CUSTOMER c
LEFT OUTER JOIN CUSTOMER_Stage s
ON c.TIPNUMBER = s.TIPNUMBER
WHERE s.TIPNUMBER is null
GO
