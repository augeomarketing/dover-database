USE [241]
GO

/****** Object:  Table [dbo].[rniPurge]    Script Date: 07/19/2012 16:02:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rniPurge]') AND type in (N'U'))
DROP TABLE [dbo].[rniPurge]
GO

USE [241]
GO

/****** Object:  Table [dbo].[rniPurge]    Script Date: 07/19/2012 16:02:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[rniPurge](
	[sid_rniPurge_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rniPurge_TipFirst] [char](3) NOT NULL,
	[dim_rniPurge_AccountType] [varchar](20) NOT NULL,
	[dim_rniPurge_Account] [varchar](max) NOT NULL,
	[dim_rniPurge_DateAdded] [date] NOT NULL,
	[dim_rniPurge_DateRecent] [date] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


