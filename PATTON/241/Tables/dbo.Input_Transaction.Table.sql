USE [241]
GO
/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 01/25/2012 14:33:26 ******/
ALTER TABLE [dbo].[Input_Transaction] DROP CONSTRAINT [DF__Input_Tra__TranA__32E0915F]
GO
ALTER TABLE [dbo].[Input_Transaction] DROP CONSTRAINT [DF__Input_Tra__TranC__33D4B598]
GO
ALTER TABLE [dbo].[Input_Transaction] DROP CONSTRAINT [DF__Input_Tra__Point__34C8D9D1]
GO
DROP TABLE [dbo].[Input_Transaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[PortfolioId] [varchar](20) NULL,
	[MemberId] [varchar](20) NULL,
	[PrimaryId] [varchar](20) NULL,
	[RNIId] [varchar](20) NULL,
	[TranAmount] [decimal](8, 2) NULL,
	[TranCount] [int] NULL,
	[TranDate] [date] NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Input_Transaction] ADD  DEFAULT ((0)) FOR [TranAmount]
GO
ALTER TABLE [dbo].[Input_Transaction] ADD  DEFAULT ((0)) FOR [TranCount]
GO
ALTER TABLE [dbo].[Input_Transaction] ADD  DEFAULT ((0)) FOR [Points]
GO
