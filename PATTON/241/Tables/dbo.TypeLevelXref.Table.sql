USE [241]
GO
/****** Object:  Table [dbo].[TypeLevelXref]    Script Date: 01/25/2012 14:33:26 ******/
DROP TABLE [dbo].[TypeLevelXref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TypeLevelXref](
	[sid_typelevelxref_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_typelevelxref_type] [int] NULL,
	[dim_typelevelxref_level] [int] NULL,
	[dim_typelevelxref_typedescription] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_typelevelxref_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
