USE [241]
GO
/****** Object:  Table [dbo].[Input_CUSTOMER_Error]    Script Date: 01/25/2012 14:33:26 ******/
DROP TABLE [dbo].[Input_CUSTOMER_Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_CUSTOMER_Error](
	[PortfolioId] [varchar](20) NULL,
	[MemberId] [varchar](20) NULL,
	[PrimaryId] [varchar](20) NULL,
	[DDA] [varchar](20) NULL,
	[PrimaryFlag] [int] NULL,
	[NAME1] [varchar](40) NOT NULL,
	[NAME2] [varchar](40) NULL,
	[NAME3] [varchar](40) NULL,
	[NAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NOT NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NOT NULL,
	[State] [varchar](3) NOT NULL,
	[Country] [varchar](3) NOT NULL,
	[PostalCode] [varchar](20) NOT NULL,
	[PrimaryPhone] [varchar](20) NULL,
	[MobilePhone] [varchar](20) NULL,
	[CustomerCode] [int] NOT NULL,
	[BusinessFlag] [int] NULL,
	[EmployeeFlag] [int] NULL,
	[InstitionID] [varchar](20) NULL,
	[CardNumber] [varchar](16) NULL,
	[Email] [varchar](254) NULL,
	[CustomerType] [int] NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[ErrorMessage] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
