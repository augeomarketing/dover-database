USE [241]
GO
/****** Object:  Table [dbo].[wrkHDRTransaction]    Script Date: 01/25/2012 14:33:26 ******/
DROP TABLE [dbo].[wrkHDRTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkHDRTransaction](
	[HeaderID] [varchar](8) NULL,
	[PDFBusinessDate] [varchar](6) NULL,
	[FileCreationDate] [varchar](6) NULL,
	[FileCreationTime] [varchar](6) NULL,
	[Reserved] [varchar](54) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
