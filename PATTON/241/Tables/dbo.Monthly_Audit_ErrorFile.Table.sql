USE [241]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 01/25/2012 14:33:26 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__37A5467C]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__38996AB5]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__398D8EEE]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__3A81B327]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__3B75D760]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__3C69FB99]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__3D5E1FD2]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__3E52440B]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__3F466844]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__403A8C7D]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__412EB0B6]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4222D4EF]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4316F928]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Expir__440B1D61]
GO
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [varchar](15) NOT NULL,
	[PointsBegin] [bigint] NULL,
	[PointsEnd] [bigint] NULL,
	[PointsPurchasedCR] [bigint] NULL,
	[PointsPurchasedDB] [bigint] NULL,
	[PointsBonus] [bigint] NULL,
	[PointsBonusMN] [bigint] NULL,
	[PointsAdded] [bigint] NULL,
	[PointsIncreased] [bigint] NULL,
	[PointsRedeemed] [bigint] NULL,
	[PointsReturnedCR] [bigint] NULL,
	[PointsReturnedDB] [bigint] NULL,
	[PointsSubtracted] [bigint] NULL,
	[PointsDecreased] [bigint] NULL,
	[ExpiringPts] [bigint] NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [bigint] NULL,
 CONSTRAINT [PK_Monthly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsBonusMN]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT ((0)) FOR [ExpiringPts]
GO
