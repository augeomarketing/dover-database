USE [241]
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 01/25/2012 14:33:26 ******/
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__RunAva__43D61337]
GO
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__RUNBAL__44CA3770]
GO
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__RunRed__45BE5BA9]
GO
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__RunBal__46B27FE2]
GO
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__RunAva__47A6A41B]
GO
DROP TABLE [dbo].[CUSTOMER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NOT NULL,
	[RUNBALANCE] [int] NOT NULL,
	[RunRedeemed] [int] NOT NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [varchar](max) NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NOT NULL,
	[RunAvaliableNew] [int] NOT NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ((0)) FOR [RunAvailable]
GO
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ((0)) FOR [RUNBALANCE]
GO
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ((0)) FOR [RunRedeemed]
GO
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ((0)) FOR [RunBalanceNew]
GO
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ((0)) FOR [RunAvaliableNew]
GO
