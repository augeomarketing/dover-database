USE [241]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 09/30/2011 15:11:41 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBillPay]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBillPay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBillPay]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsTransferred]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsTransferred]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsTransferred]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBillPay] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[acctid] [char](16) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
	[PointsTransferred] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBillPay]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBillPay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBillPay]  DEFAULT ((0)) FOR [PointsBillPay]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsTransferred]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsTransferred]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsTransferred]  DEFAULT ((0)) FOR [PointsTransferred]
END


End
GO
