USE [241]
GO
/****** Object:  Table [dbo].[EStmtTips]    Script Date: 09/30/2011 15:11:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmtTips]') AND type in (N'U'))
DROP TABLE [dbo].[EStmtTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmtTips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EStmtTips](
	[sid_estmttips_tipnumber] [varchar](15) NOT NULL,
 CONSTRAINT [PK_EStmtTips] PRIMARY KEY CLUSTERED 
(
	[sid_estmttips_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
