USE [406]
GO
/****** Object:  StoredProcedure [dbo].[sp406GenTIPNumbersStage]    Script Date: 12/16/2011 14:27:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp406GenTIPNumbersStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp406GenTIPNumbersStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp406GenTIPNumbersStage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp406GenTIPNumbersStage]
AS 


update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.acctid = b.acctid 

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = '' '')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 406, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 406000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = '' ''

exec RewardsNOW.dbo.spPutLastTipNumberUsed 406, @newnum

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = '' '')
' 
END
GO
