USE [406]
GO
/****** Object:  Table [dbo].[CustomerWork]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerWork]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerWork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerWork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerWork](
	[Acctid] [varchar](25) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [varchar](40) NULL,
	[Acctname4] [varchar](40) NULL,
	[Acctname5] [varchar](40) NULL,
	[acctname6] [varchar](40) NULL,
	[status] [varchar](1) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Addr1] [varchar](40) NULL,
	[Addr2] [varchar](40) NULL,
	[Addr3] [varchar](40) NULL,
	[Addr4] [varchar](40) NULL,
	[Zip] [varchar](15) NULL,
	[Lastname] [varchar](40) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[Trandate] [datetime] NULL,
	[BusinessFlag] [varchar](1) NULL,
	[SegmentCode] [varchar](2) NULL,
	[Filler] [varchar](93) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[City] [varchar](30) NULL,
	[State] [varchar](5) NULL,
	[Custid] [varchar](25) NULL,
	[last6] [char](6) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
