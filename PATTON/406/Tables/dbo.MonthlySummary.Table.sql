USE [406]
GO
/****** Object:  Table [dbo].[MonthlySummary]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlySummary]') AND type in (N'U'))
DROP TABLE [dbo].[MonthlySummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlySummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MonthlySummary](
	[rowid] [int] NOT NULL,
	[Dateadded] [datetime] NULL,
	[Purchases] [decimal](18, 2) NULL,
	[Returned] [decimal](18, 2) NULL,
	[Bonuses] [decimal](18, 2) NULL
) ON [PRIMARY]
END
GO
