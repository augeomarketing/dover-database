USE [406]
GO
/****** Object:  Table [dbo].[QCReferenceTable]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QCReferenceTable]') AND type in (N'U'))
DROP TABLE [dbo].[QCReferenceTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[QCReferenceTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[QCReferenceTable](
	[QCFlag] [varchar](2) NULL,
	[ROWCNT] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
