USE [406]
GO
/****** Object:  Table [dbo].[Reg]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reg]') AND type in (N'U'))
DROP TABLE [dbo].[Reg]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reg]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
