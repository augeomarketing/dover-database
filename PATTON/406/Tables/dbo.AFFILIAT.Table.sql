USE [406]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIAT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
