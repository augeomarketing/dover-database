USE [406]
GO
/****** Object:  Table [dbo].[MonthlyExpPoints]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlyExpPoints]') AND type in (N'U'))
DROP TABLE [dbo].[MonthlyExpPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlyExpPoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MonthlyExpPoints](
	[Tipnumber] [varchar](15) NOT NULL,
	[EligiblePoints] [decimal](18, 0) NULL,
	[UsedPoints] [decimal](18, 0) NULL,
	[PointstoLose] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
