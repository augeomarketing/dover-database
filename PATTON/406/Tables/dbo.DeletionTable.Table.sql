USE [406]
GO
/****** Object:  Table [dbo].[DeletionTable]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeletionTable]') AND type in (N'U'))
DROP TABLE [dbo].[DeletionTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeletionTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeletionTable](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RunBalance] [int] NULL,
	[RunRedeemed] [int] NULL,
	[CustID] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
