USE [406]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 12/16/2011 14:26:11 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturned]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_ExpPnts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_ExpPnts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_ExpPnts]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [char](40) NULL,
	[Acctname4] [char](40) NULL,
	[Acctname5] [char](40) NULL,
	[Acctname6] [char](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[ExpPnts] [decimal](18, 0) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsPurchased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchased]  DEFAULT ((0)) FOR [PointsPurchased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturned]  DEFAULT ((0)) FOR [PointsReturned]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_ExpPnts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_ExpPnts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_ExpPnts]  DEFAULT ((0)) FOR [ExpPnts]
END


End
GO
