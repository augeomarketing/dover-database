USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[Input_Transactions_OAA_orphans]    Script Date: 11/10/2010 13:37:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_OAA_orphans]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transactions_OAA_orphans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_OAA_orphans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Input_Transactions_OAA_orphans](
	[Tipnumber] [varchar](15) NULL,
	[FName] [varchar](40) NULL,
	[LName] [varchar](40) NULL,
	[SSN] [varchar](11) NULL,
	[TransAmt] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
