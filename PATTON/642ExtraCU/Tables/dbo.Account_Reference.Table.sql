USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[Account_Reference]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_Reference_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account_Reference]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_Reference_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account_Reference] DROP CONSTRAINT [DF_Account_Reference_DateAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_Account_Reference] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC,
	[acctnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference]') AND name = N'IX_Account_Reference_acctNumber')
CREATE NONCLUSTERED INDEX [IX_Account_Reference_acctNumber] ON [dbo].[Account_Reference] 
(
	[acctnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference]') AND name = N'IX_Account_Reference_TipNumber')
CREATE NONCLUSTERED INDEX [IX_Account_Reference_TipNumber] ON [dbo].[Account_Reference] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_Reference_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account_Reference]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_Reference_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account_Reference] ADD  CONSTRAINT [DF_Account_Reference_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
END


End
GO
