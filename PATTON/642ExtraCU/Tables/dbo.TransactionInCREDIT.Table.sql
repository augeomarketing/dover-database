USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[TransactionInCREDIT]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT]') AND type in (N'U'))
DROP TABLE [dbo].[TransactionInCREDIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransactionInCREDIT](
	[RecType] [varchar](1) NULL,
	[CardNumber] [varchar](16) NULL,
	[TransSign] [varchar](1) NULL,
	[TransAmt] [varchar](13) NULL,
	[PurchaseDate] [varchar](10) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT]') AND name = N'IX_TransactionInCREDIT_tipnumber')
CREATE NONCLUSTERED INDEX [IX_TransactionInCREDIT_tipnumber] ON [dbo].[TransactionInCREDIT] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
