USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[wrkBonusPoints]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkBonusPoints]') AND type in (N'U'))
DROP TABLE [dbo].[wrkBonusPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkBonusPoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkBonusPoints](
	[CardNum] [varchar](16) NULL,
	[Points] [varchar](5) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
