USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[zDELETEDemographicIN_credit]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zDELETEDemographicIN_credit]') AND type in (N'U'))
DROP TABLE [dbo].[zDELETEDemographicIN_credit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zDELETEDemographicIN_credit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zDELETEDemographicIN_credit](
	[TipNumber] [nvarchar](15) NULL,
	[PAN] [varchar](16) NULL,
	[AcctName] [varchar](40) NULL,
	[RNAcctName] [varchar](40) NULL,
	[SSN] [varchar](9) NULL,
	[Address1] [varchar](25) NULL,
	[Address2] [varchar](25) NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](3) NULL,
	[Zip] [varchar](5) NULL,
	[HomePhone] [varchar](10) NULL,
	[FirstName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
