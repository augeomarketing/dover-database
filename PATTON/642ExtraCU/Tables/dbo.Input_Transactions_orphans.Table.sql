USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[Input_Transactions_orphans]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_orphans]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transactions_orphans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_orphans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Input_Transactions_orphans](
	[Tipnumber] [varchar](15) NULL,
	[Cardnumber] [varchar](50) NULL,
	[FName] [varchar](40) NULL,
	[LName] [varchar](40) NULL,
	[SavingsAcctNum] [varchar](20) NULL,
	[SSN] [varchar](11) NULL,
	[DDA] [varchar](20) NULL,
	[TransAmt] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
