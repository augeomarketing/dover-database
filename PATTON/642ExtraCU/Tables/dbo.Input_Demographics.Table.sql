USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[Input_Demographics]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Demographics]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Demographics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Demographics]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Input_Demographics](
	[Tipnumber] [varchar](15) NULL,
	[CardNumber] [varchar](16) NULL,
	[FName] [varchar](40) NULL,
	[LName] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](9) NULL,
	[HomePhone] [varchar](50) NULL,
	[SSN] [varchar](16) NULL,
	[DDA] [varchar](16) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
