USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[Input_ConversionPoints]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_ConversionPoints]') AND type in (N'U'))
DROP TABLE [dbo].[Input_ConversionPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_ConversionPoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Input_ConversionPoints](
	[Tipnumber] [varchar](15) NULL,
	[CardNumber] [varchar](20) NULL,
	[FName] [varchar](50) NULL,
	[LName] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
	[HomePhone] [varchar](50) NULL,
	[SSN] [varchar](50) NULL,
	[DDA] [varchar](50) NULL,
	[ConvertedPoints] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
