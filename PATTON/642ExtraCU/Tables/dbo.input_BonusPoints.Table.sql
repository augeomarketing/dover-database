USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[input_BonusPoints]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[input_BonusPoints]') AND type in (N'U'))
DROP TABLE [dbo].[input_BonusPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[input_BonusPoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[input_BonusPoints](
	[Fname] [varchar](50) NULL,
	[Lname] [varchar](50) NULL,
	[SSN] [varchar](50) NULL,
	[BonusPoints] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
