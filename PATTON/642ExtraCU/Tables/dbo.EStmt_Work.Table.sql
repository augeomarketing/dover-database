USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[EStmt_Work]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Work]') AND type in (N'U'))
DROP TABLE [dbo].[EStmt_Work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Work]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EStmt_Work](
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
