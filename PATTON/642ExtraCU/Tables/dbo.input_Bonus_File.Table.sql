USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[input_Bonus_File]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[input_Bonus_File]') AND type in (N'U'))
DROP TABLE [dbo].[input_Bonus_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[input_Bonus_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[input_Bonus_File](
	[Fname] [nvarchar](20) NULL,
	[Lname] [nvarchar](40) NULL,
	[SSN] [nvarchar](9) NULL,
	[Points] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
