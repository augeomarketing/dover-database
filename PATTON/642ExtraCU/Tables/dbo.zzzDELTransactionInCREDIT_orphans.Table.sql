USE [642ExtraCU]
GO
/****** Object:  Table [dbo].[zzzDELTransactionInCREDIT_orphans]    Script Date: 07/30/2010 13:37:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zzzDELTransactionInCREDIT_orphans]') AND type in (N'U'))
DROP TABLE [dbo].[zzzDELTransactionInCREDIT_orphans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zzzDELTransactionInCREDIT_orphans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[zzzDELTransactionInCREDIT_orphans](
	[RecType] [varchar](1) NULL,
	[CardNumber] [varchar](16) NULL,
	[TransSign] [varchar](1) NULL,
	[TransAmt] [varchar](13) NULL,
	[PurchaseDate] [varchar](10) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
