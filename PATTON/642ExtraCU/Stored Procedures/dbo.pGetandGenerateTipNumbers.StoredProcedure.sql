USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbers]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetandGenerateTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbers] @TipFirstParm varchar(3)
AS 
declare @LastTipUsed char(15), @ctr int
declare  @PAN nchar(16), @SSN varchar(16), @DDA varchar(16), @1stDDA varchar(16), @2ndDDA varchar(16), @3rdDDA varchar(16), @4thDDA varchar(16), @5thDDA varchar(16), @tipfirst varchar(3), @newtipnumber bigint, @tipnumber varchar(15), @UpdateTip varchar(15)

set @ctr=0
update DemographicIn set [Prim DDA]=rtrim(ltrim([Prim DDA]))
update DemographicIn set [SSN]=null where len(rtrim(ltrim([SSN])))=0

-------------BEGIN update DemographicIn to Assign Tips to recs via DDA first, then SSN
--pass 1 (update by DDA)
update id
      set TipNumber = ar.tipnumber
from dbo.DemographicIn id join dbo.account_reference ar
      on id.[Prim DDA] = ar.acctnumber      
      where id.Tipnumber is null
      
     
--pass 2 (update by SSN)      
      
update id
      set TipNumber = ar.tipnumber
	from dbo.DemographicIn id join dbo.account_reference ar
      on id.[SSN] = ar.acctnumber
      where id.Tipnumber is null

-----------END update DemographicIn to Assign Tips to recs via SSN first, then DDA      



---------OPEN THE ONES THAT STILL DON''T HAVE A TIP==============================================
--create a temp table based on Distinct SSNs and DDAs for recs still w/o tipnumber
select distinct [SSN],[Prim DDA]
into #tmpSSN_DDA
from DemographicIn 
where [Tipnumber] is null
--where [Prim DDA] not in (select acctnumber from Account_Reference)


declare csrSSN_DDA cursor FAST_FORWARD for
select SSN, [Prim DDA] from #tmpSSN_DDA
open csrSSN_DDA
------------------
      exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstParm, @LastTipUsed output
      select @LastTipUsed as LastTipUsed
	  set @newtipnumber=@LastTipUsed
	  set @newtipnumber = cast(@newtipnumber as bigint) + 1  
	  --set the value of the first new tipnumber
---------------     
--Print ''@CTR"'' + convert(varchar(10),@ctr)	

fetch next from csrSSN_DDA
      into  @SSN, @DDA
      


while @@FETCH_STATUS = 0
BEGIN
      -- Get new tip#
   
	set @ctr=@ctr+1
	--Print ''@CTR='' + convert(varchar(10),@ctr)
	
      -- if Neither SSN OR DDA are in account_reference, get a new tip and add both
if @DDA is not null
	begin
		if NOT exists(select acctnumber from account_reference where acctnumber=@SSN) AND NOT exists(select acctnumber from account_reference where acctnumber=@DDA)
			begin
				set @newtipnumber = cast(@newtipnumber as bigint) + 1  
				set @UpdateTip=@newtipnumber
				--
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @DDA, @TipFirstParm)	
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 

			end
				/*
				begin
					set @newtipnumber = cast(@newtipnumber as bigint) + 1  
					set @UpdateTip=@newtipnumber
				end
				*/
		--if the SSN isn''t in AcctRef but DDA IS, Lookup tip associated with DDA  and add this in with the SSN
		if not exists(select acctnumber from account_reference where acctnumber=@SSN) and EXISTS(select acctnumber from account_reference where acctnumber=@DDA)	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@DDA
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 		
			End

		--if the DDA isn''t in AcctRef but SSN IS, Lookup tip associated with SSN  and add this in with the DDA
		if not exists(select acctnumber from account_reference where acctnumber=@DDA) and EXISTS(select acctnumber from account_reference where acctnumber=@SSN)	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@SSN
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @DDA, @TipFirstParm) 		
			End			
	End	
else    --if the DDA IS null,
	begin

			--if the SSN IS in AcctRef , Lookup tip associated with SSN
		if exists(select acctnumber from account_reference where acctnumber=@SSN) 	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@SSN		
			End
		
		--if the SSN isn''t in AcctRef
		if not EXISTS(select acctnumber from account_reference where acctnumber=@SSN)	
			Begin

				set @newtipnumber = cast(@newtipnumber as bigint) + 1  
				set @UpdateTip=@newtipnumber

				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 		
			End		
	end				

--print ''CURSOR@newtipnumber:'' + convert(varchar(50),@newtipnumber)  
--print ''CURSOR@Updatetipnumber:'' + convert(varchar(50),isnull(@UpdateTip,0))
                           
      fetch next from csrSSN_DDA
      into  @SSN, @DDA
      
      
END



close csrSSN_DDA
deallocate csrSSN_DDA



PRINT ''HERE''
print ''@TipFirstParm:'' + @TipFirstParm
print ''@UpdateTip:'' + convert(varchar(50),@UpdateTip)
--select LastTipNumberUsed ,* from client
--exec pGetandGenerateTipNumbers ''642''


-- Update last Tip Number Used
exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstParm, @newtipnumber 
---===============================================
--PRINT ''TTTTHHERE''
--re-update Demographic in to give tips to the NEW card numbers
update id
      set TipNumber = ar.tipnumber
from dbo.DemographicIn id join dbo.account_reference ar
      on id.[Prim DDA] = ar.acctnumber
      where id.Tipnumber is null
-----------================================================== 


---------------' 
END
GO
