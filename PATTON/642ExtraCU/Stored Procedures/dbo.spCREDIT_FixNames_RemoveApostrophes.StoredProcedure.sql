USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[spCREDIT_FixNames_RemoveApostrophes]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCREDIT_FixNames_RemoveApostrophes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCREDIT_FixNames_RemoveApostrophes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCREDIT_FixNames_RemoveApostrophes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spCREDIT_FixNames_RemoveApostrophes] 
AS

--parse first and last names out into their own fields
update DemographicIn_CREDIT
set 
FirstName= rtrim(ltrim(substring(rtrim(ltrim(substring(AcctName,charindex('','',AcctName)+1,len(AcctName) ))),1,charindex('' '',rtrim(ltrim(substring(AcctName,charindex('','',AcctName)+1,len(AcctName) ))))))),
LastName=rtrim(ltrim(substring(AcctName,1,charindex('','',AcctName)-1)))

--replace apostrophes with nothing
Update DemographicIn_CREDIT set 
[Address1]=replace([Address1],char(39), ''''), 
[Address2]=replace([Address2],char(39), ''''), 
CITY=replace(City,char(39), ''''),
Firstname=replace(FirstName,char(39), ''''), 
LastName=replace(LastName,char(39), '''') 

--Update the value of RNAcctName
update DemographicIn_CREDIT
set RNAcctName=Firstname + '' '' + Lastname
' 
END
GO
