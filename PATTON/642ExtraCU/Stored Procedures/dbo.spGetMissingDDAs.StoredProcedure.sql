USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[spGetMissingDDAs]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetMissingDDAs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetMissingDDAs]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetMissingDDAs]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create PROCEDURE [dbo].[spGetMissingDDAs] @Bin varchar(9)
AS

truncate table DemographicIn_MissingDDA

insert into DemographicIn_MissingDDA
SELECT     Pan, [Inst ID], CS, [Prim DDA], [1st DDA], [2nd DDA], [3rd DDA], [4th DDA], [5th DDA], [Address #1], [Address #2], [City ], ST, Zip, [First], [Last], MI, [First2], [Last2], MI2, [First3], [Last3], MI3, [First4], [Last4], MI4, SSN, [Home Phone], [Work Phone], TipFirst, TipNumber, NA1, NA2, NA3, NA4, NA5, NA6, LastName
FROM         DemographicIn
--WHERE     (Pan LIKE N''544330826%'') 
where pan like (rtrim(@Bin) + ''%'')
AND 
(
(
LEN(RTRIM(LTRIM([Prim DDA]))) = 0
AND
LEN(RTRIM(LTRIM([1st DDA]))) = 0 
AND
LEN(RTRIM(LTRIM([2nd DDA]))) = 0 
AND
LEN(RTRIM(LTRIM([3rd DDA]))) = 0 
AND
LEN(RTRIM(LTRIM([4th DDA]))) = 0 
AND
LEN(RTRIM(LTRIM([5th DDA]))) = 0 
) 
   OR (LEN(RTRIM(LTRIM([Prim DDA]))) = 0  )  
)
ORDER BY LEN(RTRIM(LTRIM([Last])))

--------delete the bad recs from demographicIn
Delete DemographicIn where pan in (Select pan from DemographicIn_missingDDA)
' 
END
GO
