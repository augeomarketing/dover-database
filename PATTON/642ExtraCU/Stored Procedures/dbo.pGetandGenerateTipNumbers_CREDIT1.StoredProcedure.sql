USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbers_CREDIT1]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_CREDIT1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetandGenerateTipNumbers_CREDIT1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_CREDIT1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create PROCEDURE [dbo].[pGetandGenerateTipNumbers_CREDIT1]
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS  for the CREDIT cards from the DELTA file that DON''T are NEW accounts (don''t have a value in the replacement card field and are not being closed (''c'' in the CloseFlag field)                   */
/*                                                                            */
/******************************************************************************/
-- Changed logic to employ last tip used instead of max tip
declare  @PAN nchar(16),  @SSN nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15)



/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN_CREDIT                               */
                                                                      
declare DEMO_crsr cursor
for select Tipnumber, Pan, SSN
from DemographicIn_CREDIT /* SEB001 */
for update
/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber=''0''
		
fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
set @worktip=''0''
		
	-- Check if PAN already assigned to a tip number.

	if exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			set @Worktip=(select tipnumber from account_reference where acctnumber=@PAN)
		END
	else -- Check if SSN is already assigned to a tip number
		begin
			set @Worktip=(select tipnumber from account_reference where acctnumber=@SSN)
		end
	
	
	
	If @workTip=''0'' or @worktip is null
	Begin

		declare @LastTipUsed char(15)
		
		exec rewardsnow.dbo.spGetLastTipNumberUsed ''638'', @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @newtipnumber = cast(@LastTipUsed as bigint) + 1  
	/*********  End SEB002  *************************/
		set @UpdateTip=@newtipnumber
		
		--if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			insert into Account_Reference (Tipnumber, acctnumber, TipFirst)
			values(@UpdateTip, @PAN, left(@UpdateTip,3))

			insert into Account_Reference (Tipnumber, acctnumber, TipFirst)
			values(@UpdateTip, @SSN, left(@UpdateTip,3))			
		
		End

		
		exec RewardsNOW.dbo.spPutLastTipNumberUsed ''638'', @Newtipnumber  /*SEB002 */
	End
	Else
	If @workTip<>''0'' and @worktip is not null
		Begin
			set @updateTip=@worktip
		End	
	update DemographicIn_CREDIT	/* SEB001 */
	set tipnumber = @UpdateTip 
	where current of demo_crsr 
	goto Next_Record
Next_Record:
		fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN
end
Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr

' 
END
GO
