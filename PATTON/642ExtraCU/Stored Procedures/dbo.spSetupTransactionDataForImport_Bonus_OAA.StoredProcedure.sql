USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport_Bonus_OAA]    Script Date: 11/10/2010 13:36:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_Bonus_OAA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport_Bonus_OAA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_Bonus_OAA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport_Bonus_OAA] 
	-- Add the parameters for the stored procedure here
	@StartDate char(10), 
	@EndDate char(10), 
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @DollarsPerPoint int
set @DollarsPerPoint=1

    -- Insert statements for procedure here
truncate table Input_Transactions_OAA_orphans
truncate table transstandard_bonuses

update dbo.Input_Transactions
set SSN=''0'' + SSN
where LEN(SSN)=''8''

update dbo.Input_Transactions
set tipnumber=Account_Reference.tipnumber
from dbo.Input_Transactions, Account_Reference
where dbo.Input_Transactions.ssn=Account_Reference.acctnumber and dbo.Input_Transactions.tipnumber is null

Insert into Input_Transactions_OAA_orphans (Tipnumber, FName, LName, SSN, TransAmt)
select Tipnumber, FName, LName, SSN, TransAmt
	from Input_Transactions where Tipnumber is null

INSERT INTO transstandard_bonuses(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
					select tipnumber, @enddate, Cardnumber, ''BI'',   1,     TransAmt / @DollarsPerPoint, ''BONUS INCREASE'', ''1'', '' '' from Input_Transactions 
WHERE TransAmt / @DollarsPerPoint > 0 and Tipnumber is not null
	
END
' 
END
GO
