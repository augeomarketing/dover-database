USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[sp642Input_Scrub]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp642Input_Scrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp642Input_Scrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp642Input_Scrub]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[sp642Input_Scrub] 
AS 

	--scrub the known demographic file issues 
	--set DDA=NULL if it''s an empty string OR if it''s a carriage return (FI unable to line up all data)
	
	update Input_Demographics Set DDA=Null
	where len(rtrim(ltrim(DDA)))=0
	or ascii(DDA )=13




	--scrub the known transaction file issues 
 	update Input_transactions Set DDA=Null
	where len(rtrim(ltrim(DDA)))=0 
	OR
	 DDA=''(blank)''
' 
END
GO
