USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport_CREDIT]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_CREDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport_CREDIT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_CREDIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport_CREDIT] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CREDIT DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/***************************************************************/
/*                                                             */
/* S Blanchette                                                */
/* 8/2010                                                      */
/* get rid of comma in transamt field                          */
/* SEB001                                                      */
/*		                    							       */
/***************************************************************/

declare @DollarsPerPoint int
set @DollarsPerPoint=2

truncate table TransStandardCREDIT
truncate table Input_Transactions_orphans

--delete the row that had the word TOTALS in the TransAmt field
delete Input_Transactions where ISNUMERIC(transamt)=0 or Cardnumber =''Grand Total''


update Input_Transactions
set Input_Transactions.tipnumber=affiliat_stage.tipnumber
from Input_Transactions, affiliat_stage
where Input_Transactions.Cardnumber=affiliat_stage.acctid and Input_Transactions.tipnumber is null

update Input_Transactions
set transamt = replace(transamt,char(44), '''')


/*Get the orphans*/


Insert into Input_Transactions_orphans (Tipnumber, Cardnumber, FName, LName, SavingsAcctNum, SSN, DDA, TransAmt)
select Tipnumber, Cardnumber, FName, LName, SavingsAcctNum, SSN, DDA, TransAmt
	from Input_Transactions where Tipnumber is null

	
/* end get orphans*/



--Move and calc from TransactionInCREDIT --> TransStandardCREDIT
-- one point for each dollar spent

INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
					select tipnumber, @enddate, Cardnumber, ''63'',   1,     TransAmt / @DollarsPerPoint, ''CREDIT'', ''1'', '' '' from Input_Transactions 
WHERE TransAmt / @DollarsPerPoint > 0 and Tipnumber is not null


	
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, ''33'', 1,  TransAmt / @DollarsPerPoint, ''CREDIT'', ''-1'', '' '' from Input_Transactions
WHERE  Tipnumber is not null
and TransAmt / @DollarsPerPoint<0 and Tipnumber is not null

--returns for credit come in as negative values so these need to be made postive once the ratio has been set to -1
update transstandardCREDIT set Tranamt=ABS(tranamt) where tranamt<0



' 
END
GO
