USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard_Bonuses]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard_Bonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadTransStandard_Bonuses] @TipFirst varchar(15),@EndDate char(10)
AS



declare @StartDate char(10), @TranCode varchar(2), @Description nvarchar(40), @TranAmt int
set @StartDate=substring(@EndDate,1,2) + ''/01/'' + substring(@EndDate,7,4)

--clear the table
Truncate table TransStandard_Bonuses

---------------------------------------------------------------------------------------
/* Generic Bonuses BI   Limestone gives us a file with ALL of the different bonuses that they want to award and wanted them all awarded under the same trancode*/
/* taken out for April processing because FI says they want to do point adjustments through class
delete wrkBonusPoints where Cardnum is null
Set @TranCode = ''BI''
Select @Description= [Description] from tranType where TranCode=@TranCode
------------------update the tipnumber
UPDATE w set w.TIPNUMBER =a.tipnumber 
FROM  aFFILIAT_stage a
join wrkBonusPoints w on
w.CardNum = a.acctID
-------put recs that were not matched to a tipnumber in the error table and remove from Points table
truncate table wrkBonusPoints_err
insert into wrkBonusPoints_err
	Select * From wrkBonusPoints where tipnumber is null
delete wrkBonusPoints where Tipnumber is null
---------------------------------------------------

Insert into TransStandard_Bonuses (tfno, TranDate , Acct_Num, Trancode, TranNum, TranAmt, TranType, Ratio)
	select tipnumber, @EndDate, CardNum , @TranCode, 1, Points, @Description, 1 from wrkBonusPoints
--------
--load _stage
Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select Tipnumber, @TranCode, CardNum ,@EndDate from wrkBonusPoints

*/


-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

/* Online Registration Bonus */
/*
Set @TranCode = ''BR''
Set @TranAmt=250
Select @Description= [Description] from tranType where TranCode=@TranCode
------------------
Insert into TransStandard_Bonuses (tfno) 
	select tipnumber from RN1.Coop.dbo.[1security]
			where password is not null and regdate>=@StartDate and regdate<=@EndDate and left(tipnumber,3) = @TipFirst
--update the newly added records
update TransStandard_Bonuses
set
TranDate=@EndDate,
Acct_Num=NULL,
Trancode=@TranCode,
TranNum=1,
TranAmt=@TranAmt,
TranType=@Description,
Ratio=1
where Trancode is Null and tfno is not Null
--------
--load _stage
Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tfno, @TranCode, null ,@EndDate from TransStandard_Bonuses where tfno not in (select tipnumber from OnetimeBonuses_stage where trancode=@TranCode)

*/

-----------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--E-Statement Bonuses
/*
Set @TranCode = ''BE''
Set @TranAmt=250
Select @Description= [Description] from tranType where TranCode=@TranCode
------------------
Insert into TransStandard_Bonuses (tfno) 
	select tipnumber from RN1.Coop.dbo.[1security]
		where emailstatement = ''Y'' and left(tipnumber,3)=@TipFirst
--update the newly added records
update TransStandard_Bonuses
set
TranDate=@EndDate,
Acct_Num=NULL,
Trancode=@TranCode,
TranNum=1,
TranAmt=@TranAmt,
TranType=@Description,
Ratio=1
where Trancode is Null and tfno is not Null
--------
--load _stage
Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tfno, @TranCode, null ,@EndDate from TransStandard_Bonuses where tfno not in (select tipnumber from OnetimeBonuses_stage where trancode=@TranCode)
*/
--============================================-


/* Copy records to TransStandard*/

insert into transstandard 
	Select * from TransStandard_Bonuses' 
END
GO
