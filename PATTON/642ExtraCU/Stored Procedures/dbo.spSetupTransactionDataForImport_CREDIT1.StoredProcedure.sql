USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport_CREDIT1]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_CREDIT1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport_CREDIT1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport_CREDIT1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create PROCEDURE [dbo].[spSetupTransactionDataForImport_CREDIT1] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CREDIT DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */

truncate table TransStandardCREDIT

update TransactionInCREDIT
set TransactionInCREDIT.tipnumber=affiliat_stage.tipnumber
from TransactionInCREDIT, affiliat_stage
where TransactionInCREDIT.Cardnumber=affiliat_stage.acctid and TransactionInCREDIT.tipnumber is null

/*Get the orphans*/
truncate table TransactionInCREDIT_orphans

Insert into TransactionInCREDIT_orphans (RecType, CardNumber, TransSign, TransAmt, PurchaseDate, Tipnumber)
select RecType, CardNumber, TransSign, TransAmt, PurchaseDate, Tipnumber
	from TransactionInCREDIT where Tipnumber is null

delete TransactionInCREDIT  where Tipnumber is null	
/* end get orphans*/



--Move and calc from TransactionInCREDIT --> TransStandardCREDIT
-- one point for each dollar spent
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, ''63'', Count(Cardnumber), cast(round( sum(cast(TransAmt as decimal)) / 100, 0) as int) , ''CREDIT'', ''1'', '' '' from transactionInCREDIT
WHERE TransSign<>''-''
group by tipnumber, CardNumber
	
INSERT INTO TransStandardCREDIT(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Cardnumber, ''33'', Count(Cardnumber),  cast(round( sum(cast(TransAmt as decimal)) / 100, 0) as int), ''CREDIT'', ''-1'', '' '' from transactionInCREDIT
WHERE  TransSign = ''-''
group by tipnumber, CardNumber


' 
END
GO
