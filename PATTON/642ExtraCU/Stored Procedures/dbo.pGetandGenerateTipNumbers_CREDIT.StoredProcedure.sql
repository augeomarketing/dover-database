USE [642ExtraCU]
GO
/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbers_CREDIT]    Script Date: 07/30/2010 13:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_CREDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetandGenerateTipNumbers_CREDIT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_CREDIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbers_CREDIT] @TipFirstParm varchar(3)
AS 
declare @LastTipUsed char(15)
declare  @PAN nchar(16), @SSN varchar(16), @DDA nchar(16), @1stDDA nchar(16), @2ndDDA nchar(16), @3rdDDA nchar(16), @4thDDA nchar(16), @5thDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)

update Input_demographics set DDA=rtrim(ltrim(DDA))
update Input_Demographics set [SSN]=null where len(rtrim(ltrim([SSN])))=0

-------------BEGIN update Input_Demographics to Assign Tips to recs via DDA first, then SSN
--pass 1 (update by DDA)
update id
      set TipNumber = ar.tipnumber
from dbo.Input_Demographics id join dbo.account_reference ar
      on id.[DDA] = ar.acctnumber      
      where id.Tipnumber is null
      
     
--pass 2 (update by SSN)      
      
update id
      set TipNumber = ar.tipnumber
	from dbo.Input_Demographics id join dbo.account_reference ar
      on id.[SSN] = ar.acctnumber
      where id.Tipnumber is null

-----------END update Input_Demographics to Assign Tips to recs via SSN first, then DDA      



---------==============================================
--create a temp table based on Distinct SSNs and DDAs for recs still w/o tipnumber
select distinct [SSN],[DDA]
into #tmpSSN_DDA
from Input_Demographics 
where [Tipnumber] is null
--where [Prim DDA] not in (select acctnumber from Account_Reference)


declare csrSSN_DDA cursor FAST_FORWARD for
select SSN, DDA from #tmpSSN_DDA
open csrSSN_DDA
------------------
      exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstParm, @LastTipUsed output
      select @LastTipUsed as LastTipUsed
	  set @newtipnumber=@LastTipUsed
	  set @newtipnumber = cast(@newtipnumber as bigint) + 1  
	  --set the value of the first new tipnumber
---------------     



fetch next from csrSSN_DDA
      into  @SSN, @DDA

while @@FETCH_STATUS = 0
BEGIN
      -- Get new tip#

		
      -- if Neither SSN OR DDA are in account_reference, get a new tip and add both
if @DDA is not null
	begin
		if NOT exists(select acctnumber from account_reference where acctnumber=@SSN) AND NOT exists(select acctnumber from account_reference where acctnumber=@DDA)
			begin
				set @newtipnumber = cast(@newtipnumber as bigint) + 1  
				set @UpdateTip=@newtipnumber
				--
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @DDA, @TipFirstParm)	
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 

			end
				begin
					set @newtipnumber = cast(@newtipnumber as bigint) + 1  
					set @UpdateTip=@newtipnumber
				end
		--if the SSN isn''t in AcctRef but DDA IS, Lookup tip associated with DDA  and add this in with the SSN
		if not exists(select acctnumber from account_reference where acctnumber=@SSN) and EXISTS(select acctnumber from account_reference where acctnumber=@DDA)	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@DDA
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 		
			End

		--if the DDA isn''t in AcctRef but SSN IS, Lookup tip associated with SSN  and add this in with the DDA
		if not exists(select acctnumber from account_reference where acctnumber=@DDA) and EXISTS(select acctnumber from account_reference where acctnumber=@SSN)	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@SSN
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @DDA, @TipFirstParm) 		
			End			
	End	
else    --if the DDA IS null,
	begin

			--if the SSN IS in AcctRef , Lookup tip associated with SSN
		if exists(select acctnumber from account_reference where acctnumber=@SSN) 	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@SSN		
			End
		
		--if the SSN isn''t in AcctRef
		if not EXISTS(select acctnumber from account_reference where acctnumber=@SSN)	
			Begin

				set @newtipnumber = cast(@newtipnumber as bigint) + 1  
				set @UpdateTip=@newtipnumber

				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 		
			End		
	end				


                     
                     
      fetch next from csrSSN_DDA
      into  @SSN, @DDA
END

close csrSSN_DDA
deallocate csrSSN_DDA




-- Update last Tip Number Used
exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstParm, @newtipnumber 
---===============================================

--re-update input_Demographic in to give tips to the NEW card numbers by DDA
update id
      set TipNumber = ar.tipnumber
from dbo.Input_Demographics id join dbo.account_reference ar
      on id.[DDA] = ar.acctnumber
      where id.Tipnumber is null
-----------==================================================    
--re-update input_Demographic in to give tips to the NEW card numbers by SSN
update id
      set TipNumber = ar.tipnumber
from dbo.Input_Demographics id join dbo.account_reference ar
      on id.[SSN] = ar.acctnumber
      where id.Tipnumber is null
' 
END
GO
