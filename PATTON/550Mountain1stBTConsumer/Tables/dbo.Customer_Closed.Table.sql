USE [550Mountain1stBTConsumer]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 09/25/2009 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
GO
