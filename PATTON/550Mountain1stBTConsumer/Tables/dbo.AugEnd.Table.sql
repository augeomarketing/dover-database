USE [550Mountain1stBTConsumer]
GO
/****** Object:  Table [dbo].[AugEnd]    Script Date: 09/25/2009 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AugEnd](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AugEnd] ADD  CONSTRAINT [DF__AugEnd__PNTEND__7A672E12]  DEFAULT (0) FOR [PNTEND]
GO
