USE [550Mountain1stBTConsumer]
GO
/****** Object:  Table [dbo].[expiredpoints]    Script Date: 09/25/2009 11:39:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[expiredpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateOfExpire] [nvarchar](12) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
