USE [V2]
GO
/****** Object:  Table [dbo].[figroupcontactcontactrole]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroupcontactcontactrole](
	[sid_figroup_id] [dbo].[sid] NOT NULL,
	[sid_contact_id] [dbo].[sid] NOT NULL,
	[sid_contactrole_id] [dbo].[sid] NOT NULL,
	[dim_figroupcontactcontactrole_effectivedate] [datetime] NOT NULL,
	[dim_figroupcontactcontactrole_expirationdate] [datetime] NOT NULL,
	[dim_figroupcontactcontactrole_created] [datetime] NOT NULL,
	[dim_figroupcontactcontactrole_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupcontactcontactrole] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC,
	[sid_contact_id] ASC,
	[sid_contactrole_id] ASC,
	[dim_figroupcontactcontactrole_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroupcontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_figroupcontactcontactrole_contact] FOREIGN KEY([sid_contact_id])
REFERENCES [dbo].[contact] ([sid_contact_id])
GO
ALTER TABLE [dbo].[figroupcontactcontactrole] CHECK CONSTRAINT [FK_figroupcontactcontactrole_contact]
GO
ALTER TABLE [dbo].[figroupcontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_figroupcontactcontactrole_contactrole] FOREIGN KEY([sid_contactrole_id])
REFERENCES [dbo].[contactrole] ([sid_contactrole_id])
GO
ALTER TABLE [dbo].[figroupcontactcontactrole] CHECK CONSTRAINT [FK_figroupcontactcontactrole_contactrole]
GO
ALTER TABLE [dbo].[figroupcontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_figroupcontactcontactrole_figroup] FOREIGN KEY([sid_figroup_id])
REFERENCES [dbo].[figroup] ([sid_figroup_id])
GO
ALTER TABLE [dbo].[figroupcontactcontactrole] CHECK CONSTRAINT [FK_figroupcontactcontactrole_figroup]
GO
ALTER TABLE [dbo].[figroupcontactcontactrole] ADD  CONSTRAINT [DF_figroupcontactcontactrole_dim_figroupcontactcontactrole_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_figroupcontactcontactrole_expirationdate]
GO
ALTER TABLE [dbo].[figroupcontactcontactrole] ADD  CONSTRAINT [DF_figroupcontactcontactrole_dim_figroupcontactcontactrole_created]  DEFAULT (getdate()) FOR [dim_figroupcontactcontactrole_created]
GO
ALTER TABLE [dbo].[figroupcontactcontactrole] ADD  CONSTRAINT [DF_figroupcontactcontactrole_dim_figroupcontactcontactrole_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupcontactcontactrole_lastmodified]
GO
