USE [V2]
GO
/****** Object:  Table [dbo].[customergroupcustomer]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customergroupcustomer](
	[sid_customergroup_id] [dbo].[sid] NOT NULL,
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[dim_customergroupcustomer_effectivedate] [datetime] NOT NULL,
	[dim_customergroupcustomer_expirationdate] [datetime] NOT NULL,
	[dim_customergroupcustomer_created] [datetime] NOT NULL,
	[dim_customergroupcustomer_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customergroupcustomer] PRIMARY KEY CLUSTERED 
(
	[sid_customergroup_id] ASC,
	[sid_customer_id] ASC,
	[dim_customergroupcustomer_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customergroupcustomer]  WITH CHECK ADD  CONSTRAINT [FK_customergroupcustomer_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[customergroupcustomer] CHECK CONSTRAINT [FK_customergroupcustomer_customer]
GO
ALTER TABLE [dbo].[customergroupcustomer]  WITH CHECK ADD  CONSTRAINT [FK_customergroupcustomer_customergroup] FOREIGN KEY([sid_customergroup_id])
REFERENCES [dbo].[customergroup] ([sid_customergroup_id])
GO
ALTER TABLE [dbo].[customergroupcustomer] CHECK CONSTRAINT [FK_customergroupcustomer_customergroup]
GO
ALTER TABLE [dbo].[customergroupcustomer] ADD  CONSTRAINT [DF_customergroupcustomer_dim_customergroupcustomer_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_customergroupcustomer_expirationdate]
GO
ALTER TABLE [dbo].[customergroupcustomer] ADD  CONSTRAINT [DF_customergroupcustomer_dim_customergroupcustomer_created]  DEFAULT (getdate()) FOR [dim_customergroupcustomer_created]
GO
ALTER TABLE [dbo].[customergroupcustomer] ADD  CONSTRAINT [DF_customergroupcustomer_dim_customergroupcustomer_lastmodified]  DEFAULT (getdate()) FOR [dim_customergroupcustomer_lastmodified]
GO
