USE [V2]
GO
/****** Object:  Table [dbo].[customercrossreferencetype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customercrossreferencetype](
	[sid_customercrossreferencetype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_customercrossreferencetype_description] [dbo].[description] NOT NULL,
	[dim_customercrossreferencetype_created] [datetime] NOT NULL,
	[dim_customercrossreferencetype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customercrossreferencetype] PRIMARY KEY CLUSTERED 
(
	[sid_customercrossreferencetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customercrossreferencetype] ADD  CONSTRAINT [DF_customercrossreferencetype_dim_customercrossreferencetype_created]  DEFAULT (getdate()) FOR [dim_customercrossreferencetype_created]
GO
ALTER TABLE [dbo].[customercrossreferencetype] ADD  CONSTRAINT [DF_customercrossreferencetype_dim_customercrossreferencetype_lastmodified]  DEFAULT (getdate()) FOR [dim_customercrossreferencetype_lastmodified]
GO
