USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutioncrossreferencestatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutioncrossreferencestatus](
	[sid_financialinstitutioncrossreferencestatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_financialinstitutioncrossreferencestatus_description] [dbo].[description] NOT NULL,
	[dim_financialinstitutioncrossreferencestatus_created] [datetime] NOT NULL,
	[dim_financialinstitutioncrossreferencestatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutioncrossreferencestatus] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitutioncrossreferencestatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreferencestatus] ADD  CONSTRAINT [DF_financialinstitutioncrossreferencestatus_dim_financialinstitutioncrossreferencestatus_created]  DEFAULT (getdate()) FOR [dim_financialinstitutioncrossreferencestatus_created]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreferencestatus] ADD  CONSTRAINT [DF_financialinstitutioncrossreferencestatus_dim_financialinstitutioncrossreferencestatus_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutioncrossreferencestatus_lastmodified]
GO
