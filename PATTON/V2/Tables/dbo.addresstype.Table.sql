USE [V2]
GO
/****** Object:  Table [dbo].[addresstype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[addresstype](
	[sid_addresstype_id] [dbo].[sid] NOT NULL,
	[dim_addresstype_description] [dbo].[description] NOT NULL,
	[dim_addresstype_created] [datetime] NOT NULL,
	[dim_addresstype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_addresstype] PRIMARY KEY CLUSTERED 
(
	[sid_addresstype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[addresstype] ADD  CONSTRAINT [DF_addresstype_dim_addresstype_created]  DEFAULT (getdate()) FOR [dim_addresstype_created]
GO
ALTER TABLE [dbo].[addresstype] ADD  CONSTRAINT [DF_addresstype_dim_addresstype_lastmodified]  DEFAULT (getdate()) FOR [dim_addresstype_lastmodified]
GO
