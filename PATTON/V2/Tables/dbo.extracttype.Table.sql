USE [V2]
GO
/****** Object:  Table [dbo].[extracttype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[extracttype](
	[sid_extracttype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_extracttype_description] [dbo].[description] NOT NULL,
	[dim_extracttype_created] [datetime] NOT NULL,
	[dim_extracttype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_extracttype] PRIMARY KEY CLUSTERED 
(
	[sid_extracttype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[extracttype] ADD  CONSTRAINT [DF_extracttype_dim_extracttype_created]  DEFAULT (getdate()) FOR [dim_extracttype_created]
GO
ALTER TABLE [dbo].[extracttype] ADD  CONSTRAINT [DF_extracttype_dim_extracttype_lastmodified]  DEFAULT (getdate()) FOR [dim_extracttype_lastmodified]
GO
