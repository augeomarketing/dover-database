USE [V2]
GO
/****** Object:  Table [dbo].[country]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[country](
	[sid_country_code] [varchar](3) NOT NULL,
	[dim_country_name] [varchar](255) NOT NULL,
	[dim_country_created] [datetime] NOT NULL,
	[dim_country_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[sid_country_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[country] ADD  CONSTRAINT [DF_country_dim_country_created]  DEFAULT (getdate()) FOR [dim_country_created]
GO
ALTER TABLE [dbo].[country] ADD  CONSTRAINT [DF_country_dim_country_lastmodified]  DEFAULT (getdate()) FOR [dim_country_lastmodified]
GO
