USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutioncommunications]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutioncommunications](
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[sid_communications_id] [bigint] NOT NULL,
	[dim_financialinstitutioncommunications_effectivedate] [datetime] NOT NULL,
	[dim_financialinstitutioncommunications_expirationdate] [datetime] NOT NULL,
	[dim_financialinstitutioncommunications_created] [datetime] NOT NULL,
	[dim_financialinstitutioncommunications_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutioncommunications] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_communications_id] ASC,
	[dim_financialinstitutioncommunications_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutioncommunications]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncommunications_communications] FOREIGN KEY([sid_communications_id])
REFERENCES [dbo].[communications] ([sid_communications_id])
GO
ALTER TABLE [dbo].[financialinstitutioncommunications] CHECK CONSTRAINT [FK_financialinstitutioncommunications_communications]
GO
ALTER TABLE [dbo].[financialinstitutioncommunications]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncommunications_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutioncommunications] CHECK CONSTRAINT [FK_financialinstitutioncommunications_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutioncommunications] ADD  CONSTRAINT [DF_financialinstitutioncommunications_dim_financialinstitutioncommunications_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_financialinstitutioncommunications_expirationdate]
GO
ALTER TABLE [dbo].[financialinstitutioncommunications] ADD  CONSTRAINT [DF_financialinstitutioncommunications_dim_financialinstitutioncommunications_created]  DEFAULT (getdate()) FOR [dim_financialinstitutioncommunications_created]
GO
ALTER TABLE [dbo].[financialinstitutioncommunications] ADD  CONSTRAINT [DF_financialinstitutioncommunications_dim_financialinstitutioncommunications_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutioncommunications_lastmodified]
GO
