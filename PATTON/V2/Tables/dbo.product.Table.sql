USE [V2]
GO
/****** Object:  Table [dbo].[product]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[sid_product_partnumber] [varchar](20) NOT NULL,
	[dim_product_description] [dbo].[description] NOT NULL,
	[sid_producttype_id] [dbo].[sid] NOT NULL,
	[sid_productcategory_id] [dbo].[sid] NOT NULL,
	[sid_unitofmeasure_id] [dbo].[sid] NOT NULL,
	[dim_product_weight] [numeric](18, 4) NOT NULL,
	[dim_product_leadtimedays] [int] NOT NULL,
	[dim_product_minimumquantityonhand] [int] NOT NULL,
	[dim_product_maximumquantityonhand] [int] NOT NULL,
	[dim_product_actualcost] [numeric](18, 2) NOT NULL,
	[dim_product_lastcost] [numeric](18, 2) NOT NULL,
	[dim_product_onhandquantity] [int] NOT NULL,
	[dim_product_created] [datetime] NOT NULL,
	[dim_product_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_product] PRIMARY KEY CLUSTERED 
(
	[sid_product_partnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_productcategory] FOREIGN KEY([sid_productcategory_id])
REFERENCES [dbo].[productcategory] ([sid_productcategory_id])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_productcategory]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_producttype] FOREIGN KEY([sid_producttype_id])
REFERENCES [dbo].[producttype] ([sid_producttype_id])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_producttype]
GO
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_unitofmeasure] FOREIGN KEY([sid_unitofmeasure_id])
REFERENCES [dbo].[unitofmeasure] ([sid_unitofmeasure_id])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_unitofmeasure]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_weight]  DEFAULT ((0.00)) FOR [dim_product_weight]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_leadtimedays]  DEFAULT ((0)) FOR [dim_product_leadtimedays]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_minimumquantityonhand]  DEFAULT ((0)) FOR [dim_product_minimumquantityonhand]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_maximumquantityonhand]  DEFAULT ((0)) FOR [dim_product_maximumquantityonhand]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_actualcost]  DEFAULT ((0.00)) FOR [dim_product_actualcost]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_lastcost]  DEFAULT ((0.00)) FOR [dim_product_lastcost]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_onhandquantity]  DEFAULT ((0)) FOR [dim_product_onhandquantity]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_created]  DEFAULT (getdate()) FOR [dim_product_created]
GO
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF_product_dim_product_lastmodified]  DEFAULT (getdate()) FOR [dim_product_lastmodified]
GO
