USE [V2]
GO
/****** Object:  Table [dbo].[rewardsprogram]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rewardsprogram](
	[sid_rewardsprogram_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_rewardsprogram_name] [dbo].[description] NOT NULL,
	[dim_rewardsprogram_created] [datetime] NOT NULL,
	[dim_rewardsprogram_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_rewardsprogram] PRIMARY KEY CLUSTERED 
(
	[sid_rewardsprogram_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rewardsprogram] ADD  CONSTRAINT [DF_rewardsprogram_dim_rewardsprogram_created]  DEFAULT (getdate()) FOR [dim_rewardsprogram_created]
GO
ALTER TABLE [dbo].[rewardsprogram] ADD  CONSTRAINT [DF_rewardsprogram_dim_rewardsprogram_lastmodified]  DEFAULT (getdate()) FOR [dim_rewardsprogram_lastmodified]
GO
