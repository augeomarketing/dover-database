USE [V2]
GO
/****** Object:  Table [dbo].[customertype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customertype](
	[sid_customertype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_customertype_description] [dbo].[description] NOT NULL,
	[dim_customertype_created] [datetime] NOT NULL,
	[dim_customertype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customertype] PRIMARY KEY CLUSTERED 
(
	[sid_customertype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customertype] ADD  CONSTRAINT [DF_customertype_dim_customertype_created]  DEFAULT (getdate()) FOR [dim_customertype_created]
GO
ALTER TABLE [dbo].[customertype] ADD  CONSTRAINT [DF_customertype_dim_customertype_lastmodified]  DEFAULT (getdate()) FOR [dim_customertype_lastmodified]
GO
