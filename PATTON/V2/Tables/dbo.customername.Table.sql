USE [V2]
GO
/****** Object:  Table [dbo].[customername]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customername](
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[dim_customername_sequence] [int] NOT NULL,
	[dim_customername_name] [dbo].[description] NOT NULL,
	[dim_customername_isprimary] [varchar](1) NOT NULL,
	[dim_customername_created] [datetime] NOT NULL,
	[dim_customername_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customername] PRIMARY KEY CLUSTERED 
(
	[sid_customer_id] ASC,
	[dim_customername_sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customername]  WITH CHECK ADD  CONSTRAINT [FK_customername_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[customername] CHECK CONSTRAINT [FK_customername_customer]
GO
ALTER TABLE [dbo].[customername]  WITH CHECK ADD  CONSTRAINT [CK_customername_isprimary] CHECK  (([dim_customername_isprimary]='N' OR [dim_customername_isprimary]='Y'))
GO
ALTER TABLE [dbo].[customername] CHECK CONSTRAINT [CK_customername_isprimary]
GO
ALTER TABLE [dbo].[customername] ADD  CONSTRAINT [DF_customername_dim_customername_isprimary]  DEFAULT ('N') FOR [dim_customername_isprimary]
GO
ALTER TABLE [dbo].[customername] ADD  CONSTRAINT [DF_customername_dim_customername_created]  DEFAULT (getdate()) FOR [dim_customername_created]
GO
ALTER TABLE [dbo].[customername] ADD  CONSTRAINT [DF_customername_dim_customername_lastmodified]  DEFAULT (getdate()) FOR [dim_customername_lastmodified]
GO
