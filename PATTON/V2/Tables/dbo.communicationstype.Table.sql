USE [V2]
GO
/****** Object:  Table [dbo].[communicationstype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[communicationstype](
	[sid_communicationstype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_communicationstype_description] [dbo].[description] NOT NULL,
	[dim_communicationstype_created] [datetime] NOT NULL,
	[dim_communicationstype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_communicationstype] PRIMARY KEY CLUSTERED 
(
	[sid_communicationstype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[communicationstype] ADD  CONSTRAINT [DF_communicationstype_dim_communicationstype_created]  DEFAULT (getdate()) FOR [dim_communicationstype_created]
GO
ALTER TABLE [dbo].[communicationstype] ADD  CONSTRAINT [DF_communicationstype_dim_communicationstype_lastmodified]  DEFAULT (getdate()) FOR [dim_communicationstype_lastmodified]
GO
