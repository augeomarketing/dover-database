USE [V2]
GO
/****** Object:  Table [dbo].[figroupaddress]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroupaddress](
	[sid_figroup_id] [dbo].[sid] NOT NULL,
	[sid_addess_id] [dbo].[sid] NOT NULL,
	[dim_figroupaddress_effectivedate] [datetime] NOT NULL,
	[dim_figroupaddress_expirationdate] [datetime] NOT NULL,
	[dim_figroupaddress_created] [datetime] NOT NULL,
	[dim_figroupaddress_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupaddress] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC,
	[sid_addess_id] ASC,
	[dim_figroupaddress_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroupaddress]  WITH CHECK ADD  CONSTRAINT [FK_figroupaddress_address] FOREIGN KEY([sid_addess_id])
REFERENCES [dbo].[address] ([sid_address_id])
GO
ALTER TABLE [dbo].[figroupaddress] CHECK CONSTRAINT [FK_figroupaddress_address]
GO
ALTER TABLE [dbo].[figroupaddress]  WITH CHECK ADD  CONSTRAINT [FK_figroupaddress_figroup] FOREIGN KEY([sid_figroup_id])
REFERENCES [dbo].[figroup] ([sid_figroup_id])
GO
ALTER TABLE [dbo].[figroupaddress] CHECK CONSTRAINT [FK_figroupaddress_figroup]
GO
ALTER TABLE [dbo].[figroupaddress] ADD  CONSTRAINT [DF_figroupaddress_dim_figroupaddress_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_figroupaddress_expirationdate]
GO
ALTER TABLE [dbo].[figroupaddress] ADD  CONSTRAINT [DF_figroupaddress_dim_figroupaddress_created]  DEFAULT (getdate()) FOR [dim_figroupaddress_created]
GO
ALTER TABLE [dbo].[figroupaddress] ADD  CONSTRAINT [DF_figroupaddress_dim_figroupaddress_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupaddress_lastmodified]
GO
