USE [V2]
GO
/****** Object:  Table [dbo].[redemptiondetail]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[redemptiondetail](
	[sid_redemptionheader_id] [dbo].[sid] NOT NULL,
	[sid_redemptiondetail_sequencenumber] [int] NOT NULL,
	[sid_product_partnumber] [varchar](20) NOT NULL,
	[dim_redemptiondetail_quantityordered] [int] NOT NULL,
	[dim_redemptiondetail_quantityshipped] [int] NOT NULL,
	[sid_transactioncode_id] [dbo].[sid] NOT NULL,
	[sid_redemptiondetailstatus_id] [dbo].[sid] NOT NULL,
	[dim_redemptiondetail_created] [datetime] NOT NULL,
	[dim_redemptiondetail_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_redemptiondetail] PRIMARY KEY CLUSTERED 
(
	[sid_redemptionheader_id] ASC,
	[sid_redemptiondetail_sequencenumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[redemptiondetail]  WITH CHECK ADD  CONSTRAINT [FK_redemptiondetail_product] FOREIGN KEY([sid_product_partnumber])
REFERENCES [dbo].[product] ([sid_product_partnumber])
GO
ALTER TABLE [dbo].[redemptiondetail] CHECK CONSTRAINT [FK_redemptiondetail_product]
GO
ALTER TABLE [dbo].[redemptiondetail]  WITH CHECK ADD  CONSTRAINT [FK_redemptiondetail_redemptiondetailstatus] FOREIGN KEY([sid_redemptiondetailstatus_id])
REFERENCES [dbo].[redemptiondetailstatus] ([sid_redemptiondetailstatus_id])
GO
ALTER TABLE [dbo].[redemptiondetail] CHECK CONSTRAINT [FK_redemptiondetail_redemptiondetailstatus]
GO
ALTER TABLE [dbo].[redemptiondetail]  WITH CHECK ADD  CONSTRAINT [FK_redemptiondetail_redemptionheader] FOREIGN KEY([sid_redemptionheader_id])
REFERENCES [dbo].[redemptionheader] ([sid_redemptionheader_id])
GO
ALTER TABLE [dbo].[redemptiondetail] CHECK CONSTRAINT [FK_redemptiondetail_redemptionheader]
GO
ALTER TABLE [dbo].[redemptiondetail]  WITH CHECK ADD  CONSTRAINT [FK_redemptiondetail_transactioncode] FOREIGN KEY([sid_transactioncode_id])
REFERENCES [dbo].[transactioncode] ([sid_transactioncode_id])
GO
ALTER TABLE [dbo].[redemptiondetail] CHECK CONSTRAINT [FK_redemptiondetail_transactioncode]
GO
ALTER TABLE [dbo].[redemptiondetail] ADD  CONSTRAINT [DF_redemptiondetail_dim_redemptiondetail_quantityordered]  DEFAULT ((0)) FOR [dim_redemptiondetail_quantityordered]
GO
ALTER TABLE [dbo].[redemptiondetail] ADD  CONSTRAINT [DF_redemptiondetail_dim_redemptiondetail_quantityshipped]  DEFAULT ((0)) FOR [dim_redemptiondetail_quantityshipped]
GO
ALTER TABLE [dbo].[redemptiondetail] ADD  CONSTRAINT [DF_redemptiondetail_dim_redemptiondetail_created]  DEFAULT (getdate()) FOR [dim_redemptiondetail_created]
GO
ALTER TABLE [dbo].[redemptiondetail] ADD  CONSTRAINT [DF_redemptiondetail_dim_redemptiondetail_lastmodified]  DEFAULT (getdate()) FOR [dim_redemptiondetail_lastmodified]
GO
