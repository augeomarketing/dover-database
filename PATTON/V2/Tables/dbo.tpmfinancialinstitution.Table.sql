USE [V2]
GO
/****** Object:  Table [dbo].[tpmfinancialinstitution]    Script Date: 05/07/2010 17:43:51 ******/
ALTER TABLE [dbo].[tpmfinancialinstitution] DROP CONSTRAINT [FK_tpmfinancialinstitution_financialinstitution]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] DROP CONSTRAINT [FK_tpmfinancialinstitution_tpm]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] DROP CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_effectivedate]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] DROP CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_expirationdate]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] DROP CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_created]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] DROP CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_lastmodified]
GO
DROP TABLE [dbo].[tpmfinancialinstitution]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tpmfinancialinstitution](
	[sid_tpm_id] [bigint] NOT NULL,
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[dim_tpmfinancialinstitution_effectivedate] [datetime] NOT NULL,
	[dim_tpmfinancialinstitution_expirationdate] [datetime] NOT NULL,
	[dim_tpmfinancialinstitution_created] [datetime] NOT NULL,
	[dim_tpmfinancialinstitution_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_tpmfinancialinstitution] PRIMARY KEY CLUSTERED 
(
	[sid_tpm_id] ASC,
	[sid_financialinstitution_id] ASC,
	[dim_tpmfinancialinstitution_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution]  WITH CHECK ADD  CONSTRAINT [FK_tpmfinancialinstitution_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] CHECK CONSTRAINT [FK_tpmfinancialinstitution_financialinstitution]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution]  WITH CHECK ADD  CONSTRAINT [FK_tpmfinancialinstitution_tpm] FOREIGN KEY([sid_tpm_id])
REFERENCES [dbo].[tpm] ([sid_tpm_id])
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] CHECK CONSTRAINT [FK_tpmfinancialinstitution_tpm]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] ADD  CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_effectivedate]  DEFAULT (getdate()) FOR [dim_tpmfinancialinstitution_effectivedate]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] ADD  CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_tpmfinancialinstitution_expirationdate]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] ADD  CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_created]  DEFAULT (getdate()) FOR [dim_tpmfinancialinstitution_created]
GO
ALTER TABLE [dbo].[tpmfinancialinstitution] ADD  CONSTRAINT [DF_tpmfinancialinstitution_dim_tpmfinancialinstitution_lastmodified]  DEFAULT (getdate()) FOR [dim_tpmfinancialinstitution_lastmodified]
GO
