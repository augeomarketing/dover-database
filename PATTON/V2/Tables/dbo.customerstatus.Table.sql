USE [V2]
GO
/****** Object:  Table [dbo].[customerstatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customerstatus](
	[sid_customerstatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_customerstatus_description] [dbo].[description] NOT NULL,
	[dim_customerstatus_created] [datetime] NOT NULL,
	[dim_customerstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customerstatus] PRIMARY KEY CLUSTERED 
(
	[sid_customerstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customerstatus] ADD  CONSTRAINT [DF_customerstatus_dim_status_created]  DEFAULT (getdate()) FOR [dim_customerstatus_created]
GO
ALTER TABLE [dbo].[customerstatus] ADD  CONSTRAINT [DF_customerstatus_dim_status_lastmodified]  DEFAULT (getdate()) FOR [dim_customerstatus_lastmodified]
GO
