USE [V2]
GO
/****** Object:  Table [dbo].[figroupfi]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroupfi](
	[sid_figroup_id] [dbo].[sid] NOT NULL,
	[sid_finantialinstitution_id] [dbo].[sid] NOT NULL,
	[dim_figroupfi_effectivedate] [datetime] NOT NULL,
	[dim_figroupfi_expirationdate] [datetime] NOT NULL,
	[dim_figroupfi_created] [datetime] NOT NULL,
	[dim_figroupfi_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupfi] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC,
	[sid_finantialinstitution_id] ASC,
	[dim_figroupfi_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroupfi]  WITH CHECK ADD  CONSTRAINT [FK_figroupfi_figroupfi] FOREIGN KEY([sid_figroup_id])
REFERENCES [dbo].[figroup] ([sid_figroup_id])
GO
ALTER TABLE [dbo].[figroupfi] CHECK CONSTRAINT [FK_figroupfi_figroupfi]
GO
ALTER TABLE [dbo].[figroupfi]  WITH CHECK ADD  CONSTRAINT [FK_figroupfi_financialinstitution] FOREIGN KEY([sid_finantialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[figroupfi] CHECK CONSTRAINT [FK_figroupfi_financialinstitution]
GO
ALTER TABLE [dbo].[figroupfi] ADD  CONSTRAINT [DF_figroupfi_dim_figroupfi_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_figroupfi_expirationdate]
GO
ALTER TABLE [dbo].[figroupfi] ADD  CONSTRAINT [DF_figroupfi_dim_figroupfi_created]  DEFAULT (getdate()) FOR [dim_figroupfi_created]
GO
ALTER TABLE [dbo].[figroupfi] ADD  CONSTRAINT [DF_figroupfi_dim_figroupfi_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupfi_lastmodified]
GO
