USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutionrewardsprogram]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutionrewardsprogram](
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[sid_rewardsprogram_id] [bigint] NOT NULL,
	[dim_financialinstitutionrewardsprogram_effectivedate] [datetime] NOT NULL,
	[dim_financialinstitutionrewardsprogram_expirationdate] [datetime] NOT NULL,
	[dim_financialinstitutionrewardsprogram_created] [datetime] NOT NULL,
	[dim_financialinstitutionrewardsprogram_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutionrewardsprogram] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_rewardsprogram_id] ASC,
	[dim_financialinstitutionrewardsprogram_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutionrewardsprogram_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram] CHECK CONSTRAINT [FK_financialinstitutionrewardsprogram_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutionrewardsprogram_rewardsprogram] FOREIGN KEY([sid_rewardsprogram_id])
REFERENCES [dbo].[rewardsprogram] ([sid_rewardsprogram_id])
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram] CHECK CONSTRAINT [FK_financialinstitutionrewardsprogram_rewardsprogram]
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram] ADD  CONSTRAINT [DF_financialinstitutionrewardsprogram_dim_financialinstitutionrewardsprogram_effectivedate]  DEFAULT (getdate()) FOR [dim_financialinstitutionrewardsprogram_effectivedate]
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram] ADD  CONSTRAINT [DF_financialinstitutionrewardsprogram_dim_financialinstitutionrewardsprogram_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_financialinstitutionrewardsprogram_expirationdate]
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram] ADD  CONSTRAINT [DF_financialinstitutionrewardsprogram_dim_financialinstitutionrewardsprogram_created]  DEFAULT (getdate()) FOR [dim_financialinstitutionrewardsprogram_created]
GO
ALTER TABLE [dbo].[financialinstitutionrewardsprogram] ADD  CONSTRAINT [DF_financialinstitutionrewardsprogram_dim_financialinstitutionrewardsprogram_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutionrewardsprogram_lastmodified]
GO
