USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutioncrossreference]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutioncrossreference](
	[sid_financialinstitution_id] [dbo].[sid] NOT NULL,
	[sid_financialinstitutioncrossreference_number] [varchar](50) NOT NULL,
	[sid_financialinstitutioncrossreferencetype_id] [dbo].[sid] NOT NULL,
	[sid_financialinstitutioncrossreferencestatus_id] [dbo].[sid] NOT NULL,
	[dim_financialinstitutioncrossreference_created] [datetime] NOT NULL,
	[dim_financialinstitutioncrossreference_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutioncrossreference] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_financialinstitutioncrossreference_number] ASC,
	[sid_financialinstitutioncrossreferencetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncrossreference_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference] CHECK CONSTRAINT [FK_financialinstitutioncrossreference_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncrossreference_financialinstitutioncrossreferencestatus] FOREIGN KEY([sid_financialinstitutioncrossreferencestatus_id])
REFERENCES [dbo].[financialinstitutioncrossreferencestatus] ([sid_financialinstitutioncrossreferencestatus_id])
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference] CHECK CONSTRAINT [FK_financialinstitutioncrossreference_financialinstitutioncrossreferencestatus]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncrossreference_financialinstitutioncrossreferencetype] FOREIGN KEY([sid_financialinstitutioncrossreferencetype_id])
REFERENCES [dbo].[financialinstitutioncrossreferencetype] ([sid_financialinstitutioncrossreferencetype_id])
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference] CHECK CONSTRAINT [FK_financialinstitutioncrossreference_financialinstitutioncrossreferencetype]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference] ADD  CONSTRAINT [DF_financialinstitutioncrossreference_dim_financialinstitutioncrossreference_created]  DEFAULT (getdate()) FOR [dim_financialinstitutioncrossreference_created]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreference] ADD  CONSTRAINT [DF_financialinstitutioncrossreference_dim_financialinstitutioncrossreference_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutioncrossreference_lastmodified]
GO
