USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutionaddress]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutionaddress](
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[sid_address_id] [bigint] NOT NULL,
	[dim_financialinstitutionaddress_effectivedate] [datetime] NOT NULL,
	[dim_financialinstitutionaddress_expirationdate] [datetime] NOT NULL,
	[dim_financialinstitutionaddress_created] [datetime] NOT NULL,
	[dim_financialinstitutionaddress_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutionaddress] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_address_id] ASC,
	[dim_financialinstitutionaddress_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutionaddress]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutionaddress_address] FOREIGN KEY([sid_address_id])
REFERENCES [dbo].[address] ([sid_address_id])
GO
ALTER TABLE [dbo].[financialinstitutionaddress] CHECK CONSTRAINT [FK_financialinstitutionaddress_address]
GO
ALTER TABLE [dbo].[financialinstitutionaddress]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutionaddress_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutionaddress] CHECK CONSTRAINT [FK_financialinstitutionaddress_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutionaddress] ADD  CONSTRAINT [DF_financialinstitutionaddress_dim_financialinstitutionaddress_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_financialinstitutionaddress_expirationdate]
GO
ALTER TABLE [dbo].[financialinstitutionaddress] ADD  CONSTRAINT [DF_financialinstitutionaddress_dim_financialinstitutionaddress_created]  DEFAULT (getdate()) FOR [dim_financialinstitutionaddress_created]
GO
ALTER TABLE [dbo].[financialinstitutionaddress] ADD  CONSTRAINT [DF_financialinstitutionaddress_dim_financialinstitutionaddress_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutionaddress_lastmodified]
GO
