USE [V2]
GO
/****** Object:  Table [dbo].[customercommunications]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customercommunications](
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[sid_communications_id] [dbo].[sid] NOT NULL,
	[dim_customercommunications_effectivedate] [datetime] NOT NULL,
	[dim_customercommunications_expirationdate] [datetime] NOT NULL,
	[dim_customercommunications_created] [datetime] NOT NULL,
	[dim_customercommunications_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customercommunications] PRIMARY KEY CLUSTERED 
(
	[sid_customer_id] ASC,
	[sid_communications_id] ASC,
	[dim_customercommunications_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customercommunications]  WITH CHECK ADD  CONSTRAINT [FK_customercommunications_communications] FOREIGN KEY([sid_communications_id])
REFERENCES [dbo].[communications] ([sid_communications_id])
GO
ALTER TABLE [dbo].[customercommunications] CHECK CONSTRAINT [FK_customercommunications_communications]
GO
ALTER TABLE [dbo].[customercommunications]  WITH CHECK ADD  CONSTRAINT [FK_customercommunications_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[customercommunications] CHECK CONSTRAINT [FK_customercommunications_customer]
GO
ALTER TABLE [dbo].[customercommunications] ADD  CONSTRAINT [DF_customercommunications_dim_customercommunications_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_customercommunications_expirationdate]
GO
ALTER TABLE [dbo].[customercommunications] ADD  CONSTRAINT [DF_customercommunications_dim_customercommunications_created]  DEFAULT (getdate()) FOR [dim_customercommunications_created]
GO
ALTER TABLE [dbo].[customercommunications] ADD  CONSTRAINT [DF_customercommunications_dim_customercommunications_lastmodified]  DEFAULT (getdate()) FOR [dim_customercommunications_lastmodified]
GO
