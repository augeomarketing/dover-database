USE [V2]
GO
/****** Object:  Table [dbo].[redemptionsource]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[redemptionsource](
	[sid_redemptionsource_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_redemptionsource_description] [dbo].[description] NOT NULL,
	[dim_redemptionsource_created] [datetime] NOT NULL,
	[dim_redemptionsource_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_redemptionsource] PRIMARY KEY CLUSTERED 
(
	[sid_redemptionsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[redemptionsource] ADD  CONSTRAINT [DF_redemptionsource_dim_redemptionsource_created]  DEFAULT (getdate()) FOR [dim_redemptionsource_created]
GO
ALTER TABLE [dbo].[redemptionsource] ADD  CONSTRAINT [DF_redemptionsource_dim_redemptionsource_lastmodified]  DEFAULT (getdate()) FOR [dim_redemptionsource_lastmodified]
GO
