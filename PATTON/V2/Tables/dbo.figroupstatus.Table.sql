USE [V2]
GO
/****** Object:  Table [dbo].[figroupstatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroupstatus](
	[sid_figroupstatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_figroupstatus_description] [dbo].[description] NOT NULL,
	[dim_figroupstatus_created] [datetime] NOT NULL,
	[dim_figroupstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupstatus] PRIMARY KEY CLUSTERED 
(
	[sid_figroupstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroupstatus] ADD  CONSTRAINT [DF_figroupstatus_dim_figroupstatus_created]  DEFAULT (getdate()) FOR [dim_figroupstatus_created]
GO
ALTER TABLE [dbo].[figroupstatus] ADD  CONSTRAINT [DF_figroupstatus_dim_figroupstatus_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupstatus_lastmodified]
GO
