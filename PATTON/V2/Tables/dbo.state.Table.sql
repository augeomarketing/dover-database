USE [V2]
GO
/****** Object:  Table [dbo].[state]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[state](
	[sid_state_code] [varchar](3) NOT NULL,
	[dim_state_name] [dbo].[description] NOT NULL,
	[dim_state_created] [datetime] NOT NULL,
	[dim_state_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_state] PRIMARY KEY CLUSTERED 
(
	[sid_state_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[state] ADD  CONSTRAINT [DF_state_dim_state_created]  DEFAULT (getdate()) FOR [dim_state_created]
GO
ALTER TABLE [dbo].[state] ADD  CONSTRAINT [DF_state_dim_state_lastmodified]  DEFAULT (getdate()) FOR [dim_state_lastmodified]
GO
