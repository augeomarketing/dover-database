USE [V2]
GO
/****** Object:  Table [dbo].[figrouptype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figrouptype](
	[sid_figrouptype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_figrouptype_description] [dbo].[description] NOT NULL,
	[dim_figrouptype_created] [datetime] NOT NULL,
	[dim_figrouptype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figrouptype] PRIMARY KEY CLUSTERED 
(
	[sid_figrouptype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figrouptype] ADD  CONSTRAINT [DF_figrouptype_dim_figrouptype_created]  DEFAULT (getdate()) FOR [dim_figrouptype_created]
GO
ALTER TABLE [dbo].[figrouptype] ADD  CONSTRAINT [DF_figrouptype_dim_figrouptype_lastmodified]  DEFAULT (getdate()) FOR [dim_figrouptype_lastmodified]
GO
