USE [V2]
GO
/****** Object:  Table [dbo].[status]    Script Date: 05/07/2010 17:43:51 ******/
ALTER TABLE [dbo].[status] DROP CONSTRAINT [DF_status_dim_status_created]
GO
ALTER TABLE [dbo].[status] DROP CONSTRAINT [DF_status_dim_status_lastmodified]
GO
DROP TABLE [dbo].[status]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[status](
	[sid_status_code] [varchar](2) NOT NULL,
	[dim_status_description] [varchar](255) NOT NULL,
	[dim_status_created] [datetime] NOT NULL,
	[dim_status_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_status] PRIMARY KEY CLUSTERED 
(
	[sid_status_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[status] ADD  CONSTRAINT [DF_status_dim_status_created]  DEFAULT (getdate()) FOR [dim_status_created]
GO
ALTER TABLE [dbo].[status] ADD  CONSTRAINT [DF_status_dim_status_lastmodified]  DEFAULT (getdate()) FOR [dim_status_lastmodified]
GO
