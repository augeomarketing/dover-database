USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutionstatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutionstatus](
	[sid_financialinstitutionstatus_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_financialinstitutionstatus_description] [varchar](255) NOT NULL,
	[dim_financialinstitutionstatus_created] [datetime] NOT NULL,
	[dim_financialinstitutionstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutionstatus] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitutionstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutionstatus] ADD  CONSTRAINT [DF_financialinstitutionstatus_dim_status_created]  DEFAULT (getdate()) FOR [dim_financialinstitutionstatus_created]
GO
ALTER TABLE [dbo].[financialinstitutionstatus] ADD  CONSTRAINT [DF_financialinstitutionstatus_dim_status_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutionstatus_lastmodified]
GO
