USE [V2]
GO
/****** Object:  Table [dbo].[producttype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[producttype](
	[sid_producttype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_producttype_description] [dbo].[description] NOT NULL,
	[dim_producttype_created] [datetime] NOT NULL,
	[dim_producttype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_producttype] PRIMARY KEY CLUSTERED 
(
	[sid_producttype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[producttype] ADD  CONSTRAINT [DF_producttype_dim_producttype_created]  DEFAULT (getdate()) FOR [dim_producttype_created]
GO
ALTER TABLE [dbo].[producttype] ADD  CONSTRAINT [DF_producttype_dim_producttype_lastmodified]  DEFAULT (getdate()) FOR [dim_producttype_lastmodified]
GO
