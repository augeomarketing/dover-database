USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutioncontactcontactrole]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutioncontactcontactrole](
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[sid_contact_id] [bigint] NOT NULL,
	[sid_contactrole_id] [bigint] NOT NULL,
	[dim_financialinstitutioncontactcontactrole_effectivedate] [datetime] NOT NULL,
	[dim_financialinstitutioncontactcontactrole_expirationdate] [datetime] NOT NULL,
	[dim_financialinstitutioncontactcontactrole_created] [datetime] NOT NULL,
	[dim_financialinstitutioncontactcontactrole_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutioncontactcontactrole] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_contact_id] ASC,
	[sid_contactrole_id] ASC,
	[dim_financialinstitutioncontactcontactrole_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncontactcontactrole_contact] FOREIGN KEY([sid_contact_id])
REFERENCES [dbo].[contact] ([sid_contact_id])
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] CHECK CONSTRAINT [FK_financialinstitutioncontactcontactrole_contact]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncontactcontactrole_contactrole] FOREIGN KEY([sid_contactrole_id])
REFERENCES [dbo].[contactrole] ([sid_contactrole_id])
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] CHECK CONSTRAINT [FK_financialinstitutioncontactcontactrole_contactrole]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutioncontactcontactrole_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] CHECK CONSTRAINT [FK_financialinstitutioncontactcontactrole_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] ADD  CONSTRAINT [DF_financialinstitutioncontactcontactrole_dim_financialinstitutioncontactcontactrole_effectivedate]  DEFAULT (getdate()) FOR [dim_financialinstitutioncontactcontactrole_effectivedate]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] ADD  CONSTRAINT [DF_financialinstitutioncontactcontactrole_dim_financialinstitutioncontactcontactrole_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_financialinstitutioncontactcontactrole_expirationdate]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] ADD  CONSTRAINT [DF_financialinstitutioncontactcontactrole_dim_financialinstitutioncontactcontactrole_created]  DEFAULT (getdate()) FOR [dim_financialinstitutioncontactcontactrole_created]
GO
ALTER TABLE [dbo].[financialinstitutioncontactcontactrole] ADD  CONSTRAINT [DF_financialinstitutioncontactcontactrole_dim_financialinstitutioncontactcontactrole_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutioncontactcontactrole_lastmodified]
GO
