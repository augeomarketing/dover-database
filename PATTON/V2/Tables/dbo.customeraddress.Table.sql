USE [V2]
GO
/****** Object:  Table [dbo].[customeraddress]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customeraddress](
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[sid_address_id] [dbo].[sid] NOT NULL,
	[dim_customeraddress_effectivedate] [datetime] NOT NULL,
	[dim_customeraddress_expirationdate] [datetime] NOT NULL,
	[dim_customeraddress_created] [datetime] NOT NULL,
	[dim_customeraddress_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customeraddress] PRIMARY KEY CLUSTERED 
(
	[sid_customer_id] ASC,
	[sid_address_id] ASC,
	[dim_customeraddress_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customeraddress]  WITH CHECK ADD  CONSTRAINT [FK_customeraddress_address] FOREIGN KEY([sid_address_id])
REFERENCES [dbo].[address] ([sid_address_id])
GO
ALTER TABLE [dbo].[customeraddress] CHECK CONSTRAINT [FK_customeraddress_address]
GO
ALTER TABLE [dbo].[customeraddress]  WITH CHECK ADD  CONSTRAINT [FK_customeraddress_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[customeraddress] CHECK CONSTRAINT [FK_customeraddress_customer]
GO
ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_effectivedate]  DEFAULT (getdate()) FOR [dim_customeraddress_effectivedate]
GO
ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_customeraddress_expirationdate]
GO
ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_created]  DEFAULT (getdate()) FOR [dim_customeraddress_created]
GO
ALTER TABLE [dbo].[customeraddress] ADD  CONSTRAINT [DF_customeraddress_dim_customeraddress_lastmodified]  DEFAULT (getdate()) FOR [dim_customeraddress_lastmodified]
GO
