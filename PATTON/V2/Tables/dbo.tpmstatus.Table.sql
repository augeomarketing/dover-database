USE [V2]
GO
/****** Object:  Table [dbo].[tpmstatus]    Script Date: 05/07/2010 17:43:51 ******/
ALTER TABLE [dbo].[tpmstatus] DROP CONSTRAINT [DF_tpmstatus_dim_tpmstatus_created]
GO
ALTER TABLE [dbo].[tpmstatus] DROP CONSTRAINT [DF_tpmstatus_dim_tpmstatus_lastmodified]
GO
DROP TABLE [dbo].[tpmstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tpmstatus](
	[sid_tpmstatus_code] [varchar](2) NOT NULL,
	[dim_tpmstatus_description] [varchar](255) NOT NULL,
	[dim_tpmstatus_created] [datetime] NOT NULL,
	[dim_tpmstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_tpmstatus] PRIMARY KEY CLUSTERED 
(
	[sid_tpmstatus_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tpmstatus] ADD  CONSTRAINT [DF_tpmstatus_dim_tpmstatus_created]  DEFAULT (getdate()) FOR [dim_tpmstatus_created]
GO
ALTER TABLE [dbo].[tpmstatus] ADD  CONSTRAINT [DF_tpmstatus_dim_tpmstatus_lastmodified]  DEFAULT (getdate()) FOR [dim_tpmstatus_lastmodified]
GO
