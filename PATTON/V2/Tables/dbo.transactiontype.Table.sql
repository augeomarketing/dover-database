USE [V2]
GO
/****** Object:  Table [dbo].[transactiontype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transactiontype](
	[sid_transactiontype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_transactiontype_description] [dbo].[description] NOT NULL,
	[dim_transactiontype_created] [datetime] NOT NULL,
	[dim_transactiontype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_transactiontype] PRIMARY KEY CLUSTERED 
(
	[sid_transactiontype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
