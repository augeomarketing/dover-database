USE [V2]
GO
/****** Object:  Table [dbo].[rewardsprogramattributetype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rewardsprogramattributetype](
	[sid_rewardsprogramattributetype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_rewardsprogramattributetype_description] [dbo].[description] NOT NULL,
	[dim_rewardsprogramattributetype_ismandatory] [varchar](1) NOT NULL,
	[dim_rewardsprogramattributetype_created] [datetime] NOT NULL,
	[dim_rewardsprogramattributetype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_rewardsprogramattributetype] PRIMARY KEY CLUSTERED 
(
	[sid_rewardsprogramattributetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rewardsprogramattributetype]  WITH CHECK ADD  CONSTRAINT [CK_rewardsprogramattributetype_ismandatory] CHECK  (([dim_rewardsprogramattributetype_ismandatory]='N' OR [dim_rewardsprogramattributetype_ismandatory]='Y'))
GO
ALTER TABLE [dbo].[rewardsprogramattributetype] CHECK CONSTRAINT [CK_rewardsprogramattributetype_ismandatory]
GO
ALTER TABLE [dbo].[rewardsprogramattributetype] ADD  CONSTRAINT [DF_rewardsprogramattributetype_dim_rewardsprogramattributetype_ismandatory]  DEFAULT ('N') FOR [dim_rewardsprogramattributetype_ismandatory]
GO
ALTER TABLE [dbo].[rewardsprogramattributetype] ADD  CONSTRAINT [DF_rewardsprogramattributetype_dim_rewardsprogramattributetype_created]  DEFAULT (getdate()) FOR [dim_rewardsprogramattributetype_created]
GO
ALTER TABLE [dbo].[rewardsprogramattributetype] ADD  CONSTRAINT [DF_rewardsprogramattributetype_dim_rewardsprogramattributetype_lastmodified]  DEFAULT (getdate()) FOR [dim_rewardsprogramattributetype_lastmodified]
GO
