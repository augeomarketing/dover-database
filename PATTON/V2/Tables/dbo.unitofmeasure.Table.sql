USE [V2]
GO
/****** Object:  Table [dbo].[unitofmeasure]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[unitofmeasure](
	[sid_unitofmeasure_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_unitofmeasure_abbreviation] [varchar](2) NOT NULL,
	[dim_unitofmeasure_description] [dbo].[description] NOT NULL,
	[dim_unitofmeasure_created] [datetime] NOT NULL,
	[dim_unitofmeasure_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_unitofmeasure] PRIMARY KEY CLUSTERED 
(
	[sid_unitofmeasure_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[unitofmeasure] ADD  CONSTRAINT [DF_unitofmeasure_dim_unitofmeasure_created]  DEFAULT (getdate()) FOR [dim_unitofmeasure_created]
GO
ALTER TABLE [dbo].[unitofmeasure] ADD  CONSTRAINT [DF_unitofmeasure_dim_unitofmeasure_lastmodified]  DEFAULT (getdate()) FOR [dim_unitofmeasure_lastmodified]
GO
