USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutionextracttype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutionextracttype](
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[sid_extracttype_id] [bigint] NOT NULL,
	[dim_financialinstitutionextracttype_effectivedate] [datetime] NOT NULL,
	[dim_financialinstitutionextracttype_expirationdate] [datetime] NOT NULL,
	[dim_financialinstitutionextracttype_created] [datetime] NOT NULL,
	[dim_financialinstitutionextracttype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutionextracttype] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_extracttype_id] ASC,
	[dim_financialinstitutionextracttype_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutionextracttype]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutionextracttype_extracttype] FOREIGN KEY([sid_extracttype_id])
REFERENCES [dbo].[extracttype] ([sid_extracttype_id])
GO
ALTER TABLE [dbo].[financialinstitutionextracttype] CHECK CONSTRAINT [FK_financialinstitutionextracttype_extracttype]
GO
ALTER TABLE [dbo].[financialinstitutionextracttype]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutionextracttype_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutionextracttype] CHECK CONSTRAINT [FK_financialinstitutionextracttype_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutionextracttype] ADD  CONSTRAINT [DF_financialinstitutionextracttype_dim_financialinstitutionextracttype_effectivedate]  DEFAULT (getdate()) FOR [dim_financialinstitutionextracttype_effectivedate]
GO
ALTER TABLE [dbo].[financialinstitutionextracttype] ADD  CONSTRAINT [DF_financialinstitutionextracttype_dim_financialinstitutionextracttype_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_financialinstitutionextracttype_expirationdate]
GO
ALTER TABLE [dbo].[financialinstitutionextracttype] ADD  CONSTRAINT [DF_financialinstitutionextracttype_dim_financialinstitutionextracttype_created]  DEFAULT (getdate()) FOR [dim_financialinstitutionextracttype_created]
GO
ALTER TABLE [dbo].[financialinstitutionextracttype] ADD  CONSTRAINT [DF_financialinstitutionextracttype_dim_financialinstitutionextracttype_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutionextracttype_lastmodified]
GO
