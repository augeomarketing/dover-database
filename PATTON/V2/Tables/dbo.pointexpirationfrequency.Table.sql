USE [V2]
GO
/****** Object:  Table [dbo].[pointexpirationfrequency]    Script Date: 05/07/2010 17:43:50 ******/
DROP TABLE [dbo].[pointexpirationfrequency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pointexpirationfrequency](
	[sid_pointexpirationfrequency_code] [varchar](2) NOT NULL,
	[dim_pointexpirationfrequency_description] [varchar](255) NOT NULL,
	[dim_pointexpirationfrequency_created] [datetime] NOT NULL,
	[dim_pointexpirationfrequency_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_pointexpirationfrequency] PRIMARY KEY CLUSTERED 
(
	[sid_pointexpirationfrequency_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
