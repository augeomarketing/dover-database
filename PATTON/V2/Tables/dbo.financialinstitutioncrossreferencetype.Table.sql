USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutioncrossreferencetype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutioncrossreferencetype](
	[sid_financialinstitutioncrossreferencetype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_financialinstitutioncrossreferencetype_description] [dbo].[description] NOT NULL,
	[dim_financialinstitutioncrossreferencetype_created] [datetime] NOT NULL,
	[dim_financialinstitutioncrossreferencetype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutioncrossreferencetype] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitutioncrossreferencetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreferencetype] ADD  CONSTRAINT [DF_financialinstitutioncrossreferencetype_dim_financialinstitutioncrossreferencetype_created]  DEFAULT (getdate()) FOR [dim_financialinstitutioncrossreferencetype_created]
GO
ALTER TABLE [dbo].[financialinstitutioncrossreferencetype] ADD  CONSTRAINT [DF_financialinstitutioncrossreferencetype_dim_financialinstitutioncrossreferencetype_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutioncrossreferencetype_lastmodified]
GO
