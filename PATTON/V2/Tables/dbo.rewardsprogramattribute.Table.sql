USE [V2]
GO
/****** Object:  Table [dbo].[rewardsprogramattribute]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rewardsprogramattribute](
	[sid_rewardsprogram_id] [dbo].[sid] NOT NULL,
	[sid_rewardsprogramattributetype_id] [dbo].[sid] NOT NULL,
	[dim_rewardsprogramattribute_effectivedate] [datetime] NOT NULL,
	[dim_rewardsprogramattribute_expirationdate] [datetime] NOT NULL,
	[dim_rewardsprogramattribute_value] [varchar](max) NOT NULL,
	[dim_rewardsprogramattribute_datatype] [varchar](25) NOT NULL,
	[dim_rewardsprogramattribute_created] [datetime] NOT NULL,
	[dim_rewardsprogramattribute_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_rewardsprogramattribute] PRIMARY KEY CLUSTERED 
(
	[sid_rewardsprogram_id] ASC,
	[sid_rewardsprogramattributetype_id] ASC,
	[dim_rewardsprogramattribute_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rewardsprogramattribute]  WITH CHECK ADD  CONSTRAINT [FK_rewardsprogramattribute_rewardsprogram] FOREIGN KEY([sid_rewardsprogram_id])
REFERENCES [dbo].[rewardsprogram] ([sid_rewardsprogram_id])
GO
ALTER TABLE [dbo].[rewardsprogramattribute] CHECK CONSTRAINT [FK_rewardsprogramattribute_rewardsprogram]
GO
ALTER TABLE [dbo].[rewardsprogramattribute]  WITH CHECK ADD  CONSTRAINT [FK_rewardsprogramattribute_rewardsprogramattribute] FOREIGN KEY([sid_rewardsprogramattributetype_id])
REFERENCES [dbo].[rewardsprogramattributetype] ([sid_rewardsprogramattributetype_id])
GO
ALTER TABLE [dbo].[rewardsprogramattribute] CHECK CONSTRAINT [FK_rewardsprogramattribute_rewardsprogramattribute]
GO
ALTER TABLE [dbo].[rewardsprogramattribute] ADD  CONSTRAINT [DF_rewardsprogramattribute_dim_rewardsprogramattribute_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_rewardsprogramattribute_expirationdate]
GO
ALTER TABLE [dbo].[rewardsprogramattribute] ADD  CONSTRAINT [DF_rewardsprogramattribute_dim_rewardsprogramattribute_created]  DEFAULT (getdate()) FOR [dim_rewardsprogramattribute_created]
GO
ALTER TABLE [dbo].[rewardsprogramattribute] ADD  CONSTRAINT [DF_rewardsprogramattribute_dim_rewardsprogramattribute_lastmodified]  DEFAULT (getdate()) FOR [dim_rewardsprogramattribute_lastmodified]
GO
