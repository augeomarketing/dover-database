USE [V2]
GO
/****** Object:  Table [dbo].[communications]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[communications](
	[sid_communications_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_communications_method] [dbo].[description] NOT NULL,
	[sid_communicationstype_id] [dbo].[sid] NOT NULL,
	[dim_communincations_created] [datetime] NOT NULL,
	[dim_communications_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_communications] PRIMARY KEY CLUSTERED 
(
	[sid_communications_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[communications]  WITH CHECK ADD  CONSTRAINT [FK_communications_communicationstype] FOREIGN KEY([sid_communicationstype_id])
REFERENCES [dbo].[communicationstype] ([sid_communicationstype_id])
GO
ALTER TABLE [dbo].[communications] CHECK CONSTRAINT [FK_communications_communicationstype]
GO
ALTER TABLE [dbo].[communications] ADD  CONSTRAINT [DF_communications_dim_communincations_created]  DEFAULT (getdate()) FOR [dim_communincations_created]
GO
ALTER TABLE [dbo].[communications] ADD  CONSTRAINT [DF_communications_dim_communications_lastmodified]  DEFAULT (getdate()) FOR [dim_communications_lastmodified]
GO
