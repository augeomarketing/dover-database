USE [V2]
GO
/****** Object:  Table [dbo].[tpm]    Script Date: 05/07/2010 17:43:51 ******/
ALTER TABLE [dbo].[tpm] DROP CONSTRAINT [FK_tpm_tpmstatus]
GO
ALTER TABLE [dbo].[tpm] DROP CONSTRAINT [DF_tpm_dim_tpm_created]
GO
ALTER TABLE [dbo].[tpm] DROP CONSTRAINT [DF_tpm_dim_tpm_lastmodified]
GO
DROP TABLE [dbo].[tpm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tpm](
	[sid_tpm_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_tpm_name] [varchar](255) NOT NULL,
	[sid_tpmstatus_code] [varchar](2) NOT NULL,
	[dim_tpm_created] [datetime] NOT NULL,
	[dim_tpm_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_tpm] PRIMARY KEY CLUSTERED 
(
	[sid_tpm_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tpm]  WITH CHECK ADD  CONSTRAINT [FK_tpm_tpmstatus] FOREIGN KEY([sid_tpmstatus_code])
REFERENCES [dbo].[tpmstatus] ([sid_tpmstatus_code])
GO
ALTER TABLE [dbo].[tpm] CHECK CONSTRAINT [FK_tpm_tpmstatus]
GO
ALTER TABLE [dbo].[tpm] ADD  CONSTRAINT [DF_tpm_dim_tpm_created]  DEFAULT (getdate()) FOR [dim_tpm_created]
GO
ALTER TABLE [dbo].[tpm] ADD  CONSTRAINT [DF_tpm_dim_tpm_lastmodified]  DEFAULT (getdate()) FOR [dim_tpm_lastmodified]
GO
