USE [V2]
GO
/****** Object:  Table [dbo].[figroup]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroup](
	[sid_figroup_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_figroup_name] [varchar](50) NOT NULL,
	[dim_figroup_description] [dbo].[description] NOT NULL,
	[sid_figrouptype_id] [dbo].[sid] NOT NULL,
	[sid_figroupstatus_id] [dbo].[sid] NOT NULL,
	[dim_figroup_created] [datetime] NOT NULL,
	[dim_figroup_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroup] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroup]  WITH CHECK ADD  CONSTRAINT [FK_figroup_figroupstatus] FOREIGN KEY([sid_figroupstatus_id])
REFERENCES [dbo].[figroupstatus] ([sid_figroupstatus_id])
GO
ALTER TABLE [dbo].[figroup] CHECK CONSTRAINT [FK_figroup_figroupstatus]
GO
ALTER TABLE [dbo].[figroup]  WITH CHECK ADD  CONSTRAINT [FK_figroup_figrouptype] FOREIGN KEY([sid_figrouptype_id])
REFERENCES [dbo].[figrouptype] ([sid_figrouptype_id])
GO
ALTER TABLE [dbo].[figroup] CHECK CONSTRAINT [FK_figroup_figrouptype]
GO
ALTER TABLE [dbo].[figroup] ADD  CONSTRAINT [DF_figroup_dim_figroup_created]  DEFAULT (getdate()) FOR [dim_figroup_created]
GO
ALTER TABLE [dbo].[figroup] ADD  CONSTRAINT [DF_figroup_dim_figroup_lastmodified]  DEFAULT (getdate()) FOR [dim_figroup_lastmodified]
GO
