USE [V2]
GO
/****** Object:  Table [dbo].[address]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[address](
	[sid_address_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[sid_addresstype_id] [dbo].[sid] NOT NULL,
	[dim_address_lineone] [varchar](70) NOT NULL,
	[dim_address_linetwo] [varchar](70) NULL,
	[dim_address_city] [varchar](70) NOT NULL,
	[sid_country_code] [varchar](3) NOT NULL,
	[sid_state_code] [varchar](3) NOT NULL,
	[dim_address_postalcode] [varchar](15) NOT NULL,
	[dim_address_created] [datetime] NOT NULL,
	[dim_address_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_address] PRIMARY KEY CLUSTERED 
(
	[sid_address_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[address]  WITH CHECK ADD  CONSTRAINT [FK_address_addresstype] FOREIGN KEY([sid_addresstype_id])
REFERENCES [dbo].[addresstype] ([sid_addresstype_id])
GO
ALTER TABLE [dbo].[address] CHECK CONSTRAINT [FK_address_addresstype]
GO
ALTER TABLE [dbo].[address]  WITH CHECK ADD  CONSTRAINT [FK_address_country] FOREIGN KEY([sid_country_code])
REFERENCES [dbo].[country] ([sid_country_code])
GO
ALTER TABLE [dbo].[address] CHECK CONSTRAINT [FK_address_country]
GO
ALTER TABLE [dbo].[address]  WITH CHECK ADD  CONSTRAINT [FK_address_state] FOREIGN KEY([sid_state_code])
REFERENCES [dbo].[state] ([sid_state_code])
GO
ALTER TABLE [dbo].[address] CHECK CONSTRAINT [FK_address_state]
GO
ALTER TABLE [dbo].[address] ADD  CONSTRAINT [DF_address_dim_address_created]  DEFAULT (getdate()) FOR [dim_address_created]
GO
ALTER TABLE [dbo].[address] ADD  CONSTRAINT [DF_address_dim_address_lastmodified]  DEFAULT (getdate()) FOR [dim_address_lastmodified]
GO
