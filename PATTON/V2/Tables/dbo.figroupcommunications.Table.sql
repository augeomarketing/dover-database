USE [V2]
GO
/****** Object:  Table [dbo].[figroupcommunications]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[figroupcommunications](
	[sid_figroup_id] [dbo].[sid] NOT NULL,
	[sid_communications_id] [dbo].[sid] NOT NULL,
	[dim_figroupcommunications_effectivedate] [datetime] NOT NULL,
	[dim_figroupcommunications_expirationdate] [datetime] NOT NULL,
	[dim_figroupcommunications_created] [datetime] NOT NULL,
	[dim_figroupcommunications_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_figroupcommunications] PRIMARY KEY CLUSTERED 
(
	[sid_figroup_id] ASC,
	[sid_communications_id] ASC,
	[dim_figroupcommunications_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[figroupcommunications]  WITH CHECK ADD  CONSTRAINT [FK_figroupcommunications_communications] FOREIGN KEY([sid_communications_id])
REFERENCES [dbo].[communications] ([sid_communications_id])
GO
ALTER TABLE [dbo].[figroupcommunications] CHECK CONSTRAINT [FK_figroupcommunications_communications]
GO
ALTER TABLE [dbo].[figroupcommunications]  WITH CHECK ADD  CONSTRAINT [FK_figroupcommunications_figroup] FOREIGN KEY([sid_figroup_id])
REFERENCES [dbo].[figroup] ([sid_figroup_id])
GO
ALTER TABLE [dbo].[figroupcommunications] CHECK CONSTRAINT [FK_figroupcommunications_figroup]
GO
ALTER TABLE [dbo].[figroupcommunications] ADD  CONSTRAINT [DF_figroupcommunications_dim_figroupcommunications_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_figroupcommunications_expirationdate]
GO
ALTER TABLE [dbo].[figroupcommunications] ADD  CONSTRAINT [DF_figroupcommunications_dim_figroupcommunications_created]  DEFAULT (getdate()) FOR [dim_figroupcommunications_created]
GO
ALTER TABLE [dbo].[figroupcommunications] ADD  CONSTRAINT [DF_figroupcommunications_dim_figroupcommunications_lastmodified]  DEFAULT (getdate()) FOR [dim_figroupcommunications_lastmodified]
GO
