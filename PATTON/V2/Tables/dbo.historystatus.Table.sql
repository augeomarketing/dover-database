USE [V2]
GO
/****** Object:  Table [dbo].[historystatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[historystatus](
	[sid_historystatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_historystatus_description] [dbo].[description] NOT NULL,
	[dim_historystatus_created] [datetime] NOT NULL,
	[dim_historystatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_historystatus] PRIMARY KEY CLUSTERED 
(
	[sid_historystatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[historystatus] ADD  CONSTRAINT [DF_historystatus_dim_historystatus_created]  DEFAULT (getdate()) FOR [dim_historystatus_created]
GO
ALTER TABLE [dbo].[historystatus] ADD  CONSTRAINT [DF_historystatus_dim_historystatus_lastmodified]  DEFAULT (getdate()) FOR [dim_historystatus_lastmodified]
GO
