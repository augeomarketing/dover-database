USE [V2]
GO
/****** Object:  Table [dbo].[contact]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[sid_contact_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_contact_name] [varchar](255) NOT NULL,
	[dim_contact_attentionline] [varchar](255) NOT NULL,
	[dim_contact_addresslineone] [varchar](70) NOT NULL,
	[dim_contact_addresslinetwo] [varchar](70) NULL,
	[dim_contact_city] [varchar](70) NOT NULL,
	[sid_country_code] [varchar](3) NOT NULL,
	[sid_state_code] [varchar](3) NOT NULL,
	[dim_contact_postalcode] [varchar](15) NOT NULL,
	[dim_contact_workphone] [varchar](25) NULL,
	[dim_contact_cellphone] [varchar](25) NULL,
	[dim_contact_fax] [varchar](25) NULL,
	[dim_contact_emailaddress] [varchar](255) NULL,
	[dim_contact_websiteurl] [varchar](255) NULL,
	[dim_contact_guid] [uniqueidentifier] NOT NULL,
	[dim_contact_created] [datetime] NOT NULL,
	[dim_contact_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[sid_contact_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contact]  WITH CHECK ADD  CONSTRAINT [FK_contact_country] FOREIGN KEY([sid_country_code])
REFERENCES [dbo].[country] ([sid_country_code])
GO
ALTER TABLE [dbo].[contact] CHECK CONSTRAINT [FK_contact_country]
GO
ALTER TABLE [dbo].[contact]  WITH CHECK ADD  CONSTRAINT [FK_contact_state] FOREIGN KEY([sid_state_code])
REFERENCES [dbo].[state] ([sid_state_code])
GO
ALTER TABLE [dbo].[contact] CHECK CONSTRAINT [FK_contact_state]
GO
ALTER TABLE [dbo].[contact] ADD  CONSTRAINT [DF_contact_dim_contact_guid]  DEFAULT (newid()) FOR [dim_contact_guid]
GO
ALTER TABLE [dbo].[contact] ADD  CONSTRAINT [DF_contact_dim_contact_created]  DEFAULT (getdate()) FOR [dim_contact_created]
GO
ALTER TABLE [dbo].[contact] ADD  CONSTRAINT [DF_contact_dim_contact_lastmodified]  DEFAULT (getdate()) FOR [dim_contact_lastmodified]
GO
