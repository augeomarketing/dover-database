USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitutiontransactioncodemultiplier]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitutiontransactioncodemultiplier](
	[sid_financialinstitution_id] [dbo].[sid] NOT NULL,
	[sid_transactioncode_id] [dbo].[sid] NOT NULL,
	[dim_financialinstitutiontransactioncodemultiplier_effectivedate] [datetime] NOT NULL,
	[dim_financialinstitutiontransactioncodemultiplier_expirationdate] [datetime] NOT NULL,
	[dim_financialinstitutiontransactioncodemultiplier_multiplier] [decimal](5, 2) NOT NULL,
	[dim_financialinstitutiontransactioncodemultiplier_created] [datetime] NOT NULL,
	[dim_financialinstitutiontransactioncodemultiplier_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitutiontransactioncodemultiplier] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC,
	[sid_transactioncode_id] ASC,
	[dim_financialinstitutiontransactioncodemultiplier_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutiontransactioncodemultiplier_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier] CHECK CONSTRAINT [FK_financialinstitutiontransactioncodemultiplier_financialinstitution]
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitutiontransactioncodemultiplier_transactioncode] FOREIGN KEY([sid_transactioncode_id])
REFERENCES [dbo].[transactioncode] ([sid_transactioncode_id])
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier] CHECK CONSTRAINT [FK_financialinstitutiontransactioncodemultiplier_transactioncode]
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier] ADD  CONSTRAINT [DF_financialinstitutiontransactioncodemultiplier_dim_financialinstitutiontransactioncodemultiplier_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_financialinstitutiontransactioncodemultiplier_expirationdate]
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier] ADD  CONSTRAINT [DF_financialinstitutiontransactioncodemultiplier_dim_financialinstitutiontransactioncodemultiplier_multiplier]  DEFAULT ((1.0)) FOR [dim_financialinstitutiontransactioncodemultiplier_multiplier]
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier] ADD  CONSTRAINT [DF_financialinstitutiontransactioncodemultiplier_dim_financialinstitutiontransactioncodemultiplier_created]  DEFAULT (getdate()) FOR [dim_financialinstitutiontransactioncodemultiplier_created]
GO
ALTER TABLE [dbo].[financialinstitutiontransactioncodemultiplier] ADD  CONSTRAINT [DF_financialinstitutiontransactioncodemultiplier_dim_financialinstitutiontransactioncodemultiplier_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitutiontransactioncodemultiplier_lastmodified]
GO
