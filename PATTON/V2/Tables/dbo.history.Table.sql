USE [V2]
GO
/****** Object:  Table [dbo].[history]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[history](
	[sid_history_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_history_transactionid] [uniqueidentifier] NOT NULL,
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[sid_customercrossreference_accountnumber] [varchar](25) NULL,
	[sid_transactioncode_id] [dbo].[sid] NOT NULL,
	[sid_historystatus_id] [dbo].[sid] NOT NULL,
	[dim_history_points] [int] NOT NULL,
	[dim_history_transactiondate] [datetime] NOT NULL,
	[dim_history_transactioncount] [int] NOT NULL,
	[dim_history_description] [dbo].[description] NOT NULL,
	[dim_history_overage] [int] NOT NULL,
	[dim_history_created] [datetime] NOT NULL,
	[dim_history_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_history] PRIMARY KEY CLUSTERED 
(
	[sid_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[history]  WITH CHECK ADD  CONSTRAINT [FK_history_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[history] CHECK CONSTRAINT [FK_history_customer]
GO
ALTER TABLE [dbo].[history]  WITH CHECK ADD  CONSTRAINT [FK_history_historystatus] FOREIGN KEY([sid_historystatus_id])
REFERENCES [dbo].[historystatus] ([sid_historystatus_id])
GO
ALTER TABLE [dbo].[history] CHECK CONSTRAINT [FK_history_historystatus]
GO
ALTER TABLE [dbo].[history]  WITH CHECK ADD  CONSTRAINT [FK_history_transactioncode] FOREIGN KEY([sid_transactioncode_id])
REFERENCES [dbo].[transactioncode] ([sid_transactioncode_id])
GO
ALTER TABLE [dbo].[history] CHECK CONSTRAINT [FK_history_transactioncode]
GO
ALTER TABLE [dbo].[history] ADD  CONSTRAINT [DF_history_dim_history_points]  DEFAULT ((0)) FOR [dim_history_points]
GO
ALTER TABLE [dbo].[history] ADD  CONSTRAINT [DF_history_dim_history_transactioncount]  DEFAULT ((1)) FOR [dim_history_transactioncount]
GO
ALTER TABLE [dbo].[history] ADD  CONSTRAINT [DF_history_dim_history_overage]  DEFAULT ((0)) FOR [dim_history_overage]
GO
ALTER TABLE [dbo].[history] ADD  CONSTRAINT [DF_history_dim_history_created]  DEFAULT (getdate()) FOR [dim_history_created]
GO
ALTER TABLE [dbo].[history] ADD  CONSTRAINT [DF_history_dim_history_lastmodified]  DEFAULT (getdate()) FOR [dim_history_lastmodified]
GO
