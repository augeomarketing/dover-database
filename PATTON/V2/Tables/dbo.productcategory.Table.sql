USE [V2]
GO
/****** Object:  Table [dbo].[productcategory]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productcategory](
	[sid_productcategory_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_productcategory_description] [dbo].[description] NOT NULL,
	[dim_productcategory_created] [datetime] NOT NULL,
	[dim_productcategory_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_productcategory] PRIMARY KEY CLUSTERED 
(
	[sid_productcategory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[productcategory] ADD  CONSTRAINT [DF_productcategory_dim_productcategory_created]  DEFAULT (getdate()) FOR [dim_productcategory_created]
GO
ALTER TABLE [dbo].[productcategory] ADD  CONSTRAINT [DF_productcategory_dim_productcategory_lastmodified]  DEFAULT (getdate()) FOR [dim_productcategory_lastmodified]
GO
