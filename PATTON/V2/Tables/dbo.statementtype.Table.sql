USE [V2]
GO
/****** Object:  Table [dbo].[statementtype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statementtype](
	[sid_statementtype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_statementtype_description] [dbo].[description] NOT NULL,
	[dim_statementtype_created] [datetime] NOT NULL,
	[dim_statementtype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_statementtype] PRIMARY KEY CLUSTERED 
(
	[sid_statementtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[statementtype] ADD  CONSTRAINT [DF_statementtype_dim_statementtype_created]  DEFAULT (getdate()) FOR [dim_statementtype_created]
GO
ALTER TABLE [dbo].[statementtype] ADD  CONSTRAINT [DF_statementtype_dim_statementtype_lastmodified]  DEFAULT (getdate()) FOR [dim_statementtype_lastmodified]
GO
