USE [V2]
GO
/****** Object:  Table [dbo].[customer]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer](
	[sid_customer_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[sid_financialinstitution_id] [dbo].[sid] NOT NULL,
	[sid_customertype_id] [dbo].[sid] NOT NULL,
	[sid_customerstatus_id] [dbo].[sid] NOT NULL,
	[dim_customer_guid] [uniqueidentifier] NOT NULL,
	[dim_customer_dateadded] [date] NOT NULL,
	[dim_customer_created] [datetime] NOT NULL,
	[dim_customer_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customer] PRIMARY KEY CLUSTERED 
(
	[sid_customer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [FK_customer_customerstatus] FOREIGN KEY([sid_customerstatus_id])
REFERENCES [dbo].[customerstatus] ([sid_customerstatus_id])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [FK_customer_customerstatus]
GO
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [FK_customer_customertype] FOREIGN KEY([sid_customertype_id])
REFERENCES [dbo].[customertype] ([sid_customertype_id])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [FK_customer_customertype]
GO
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [FK_customer_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [FK_customer_financialinstitution]
GO
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF_customer_dim_customer_guid]  DEFAULT (newid()) FOR [dim_customer_guid]
GO
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF_customer_dim_customer_created]  DEFAULT (getdate()) FOR [dim_customer_created]
GO
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF_customer_dim_customer_lastmodified]  DEFAULT (getdate()) FOR [dim_customer_lastmodified]
GO
