USE [V2]
GO
/****** Object:  Table [dbo].[customercrossreference]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customercrossreference](
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[sid_customercrossreference_accountnumber] [varchar](25) NOT NULL,
	[sid_customercrossreferencetype_id] [dbo].[sid] NOT NULL,
	[sid_customercrossreferencestatus_id] [dbo].[sid] NOT NULL,
	[dim_customercrossreference_created] [datetime] NOT NULL,
	[dim_customercrossreference_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customercrossreference] PRIMARY KEY CLUSTERED 
(
	[sid_customer_id] ASC,
	[sid_customercrossreference_accountnumber] ASC,
	[sid_customercrossreferencetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_customercrossreference_accountnumber_unique] ON [dbo].[customercrossreference] 
(
	[sid_customercrossreference_accountnumber] ASC,
	[sid_customercrossreferencetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customercrossreference]  WITH CHECK ADD  CONSTRAINT [FK_customercrossreference_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[customercrossreference] CHECK CONSTRAINT [FK_customercrossreference_customer]
GO
ALTER TABLE [dbo].[customercrossreference]  WITH CHECK ADD  CONSTRAINT [FK_customercrossreference_customercrossreferencestatus] FOREIGN KEY([sid_customercrossreferencestatus_id])
REFERENCES [dbo].[customercrossreferencestatus] ([sid_customercrossreferencestatus_id])
GO
ALTER TABLE [dbo].[customercrossreference] CHECK CONSTRAINT [FK_customercrossreference_customercrossreferencestatus]
GO
ALTER TABLE [dbo].[customercrossreference]  WITH CHECK ADD  CONSTRAINT [FK_customercrossreference_customercrossreferencetype] FOREIGN KEY([sid_customercrossreferencetype_id])
REFERENCES [dbo].[customercrossreferencetype] ([sid_customercrossreferencetype_id])
GO
ALTER TABLE [dbo].[customercrossreference] CHECK CONSTRAINT [FK_customercrossreference_customercrossreferencetype]
GO
ALTER TABLE [dbo].[customercrossreference] ADD  CONSTRAINT [DF_customercrossreference_dim_customercrossreference_created]  DEFAULT (getdate()) FOR [dim_customercrossreference_created]
GO
ALTER TABLE [dbo].[customercrossreference] ADD  CONSTRAINT [DF_customercrossreference_dim_customercrossreference_lastmodified]  DEFAULT (getdate()) FOR [dim_customercrossreference_lastmodified]
GO
