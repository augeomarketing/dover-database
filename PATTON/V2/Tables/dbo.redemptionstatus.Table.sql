USE [V2]
GO
/****** Object:  Table [dbo].[redemptionstatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[redemptionstatus](
	[sid_redemptionstatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_redemptionstatus_description] [dbo].[description] NOT NULL,
	[dim_redemptionstatus_created] [datetime] NOT NULL,
	[dim_redemptionstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_redemptionstatus] PRIMARY KEY CLUSTERED 
(
	[sid_redemptionstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[redemptionstatus] ADD  CONSTRAINT [DF_redemptionstatus_dim_redemptionstatus_created]  DEFAULT (getdate()) FOR [dim_redemptionstatus_created]
GO
ALTER TABLE [dbo].[redemptionstatus] ADD  CONSTRAINT [DF_redemptionstatus_dim_redemptionstatus_lastmodified]  DEFAULT (getdate()) FOR [dim_redemptionstatus_lastmodified]
GO
