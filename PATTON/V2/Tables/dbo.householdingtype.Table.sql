USE [V2]
GO
/****** Object:  Table [dbo].[householdingtype]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[householdingtype](
	[sid_householdingtype_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_householdingtype_description] [dbo].[description] NOT NULL,
	[dim_householdingtype_created] [datetime] NOT NULL,
	[dim_householdingtype_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_householdingtype] PRIMARY KEY CLUSTERED 
(
	[sid_householdingtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[householdingtype] ADD  CONSTRAINT [DF_householdingtype_dim_householdingtype_created]  DEFAULT (getdate()) FOR [dim_householdingtype_created]
GO
ALTER TABLE [dbo].[householdingtype] ADD  CONSTRAINT [DF_householdingtype_dim_householdingtype_lastmodified]  DEFAULT (getdate()) FOR [dim_householdingtype_lastmodified]
GO
