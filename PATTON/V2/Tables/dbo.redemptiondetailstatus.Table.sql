USE [V2]
GO
/****** Object:  Table [dbo].[redemptiondetailstatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[redemptiondetailstatus](
	[sid_redemptiondetailstatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_redemptiondetailstatus_description] [dbo].[description] NOT NULL,
	[dim_redemptiondetailstatus_created] [datetime] NOT NULL,
	[dim_redemptiondetailstatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_redemptiondetailstatus] PRIMARY KEY CLUSTERED 
(
	[sid_redemptiondetailstatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[redemptiondetailstatus] ADD  CONSTRAINT [DF_redemptiondetailstatus_dim_redemptiondetailstatus_created]  DEFAULT (getdate()) FOR [dim_redemptiondetailstatus_created]
GO
ALTER TABLE [dbo].[redemptiondetailstatus] ADD  CONSTRAINT [DF_redemptiondetailstatus_dim_redemptiondetailstatus_lastmodified]  DEFAULT (getdate()) FOR [dim_redemptiondetailstatus_lastmodified]
GO
