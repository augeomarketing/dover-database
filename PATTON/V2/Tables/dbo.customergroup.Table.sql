USE [V2]
GO
/****** Object:  Table [dbo].[customergroup]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customergroup](
	[sid_customergroup_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_customergroup_description] [dbo].[description] NOT NULL,
	[sid_financialinstitution_id] [dbo].[sid] NOT NULL,
	[dim_customergroup_portalcombinestransid] [uniqueidentifier] NOT NULL,
	[dim_customergroup_created] [datetime] NOT NULL,
	[dim_customergroup_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customergroup] PRIMARY KEY CLUSTERED 
(
	[sid_customergroup_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customergroup]  WITH CHECK ADD  CONSTRAINT [FK_customergroup_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[customergroup] CHECK CONSTRAINT [FK_customergroup_financialinstitution]
GO
ALTER TABLE [dbo].[customergroup] ADD  CONSTRAINT [DF_customergroup_dim_customergroup_created]  DEFAULT (getdate()) FOR [dim_customergroup_created]
GO
ALTER TABLE [dbo].[customergroup] ADD  CONSTRAINT [DF_customergroup_dim_customergroup_lastmodified]  DEFAULT (getdate()) FOR [dim_customergroup_lastmodified]
GO
