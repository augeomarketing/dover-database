USE [V2]
GO
/****** Object:  Table [dbo].[customercrossreferencestatus]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customercrossreferencestatus](
	[sid_customercrossreferencestatus_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_customercrossreferencestatus_description] [dbo].[description] NOT NULL,
	[dim_customercrossreferencestatus_created] [datetime] NOT NULL,
	[dim_customercrossreferencestatus_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_customercrossreferencestatus] PRIMARY KEY CLUSTERED 
(
	[sid_customercrossreferencestatus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[customercrossreferencestatus] ADD  CONSTRAINT [DF_customercrossreferencestatus_dim_status_created]  DEFAULT (getdate()) FOR [dim_customercrossreferencestatus_created]
GO
ALTER TABLE [dbo].[customercrossreferencestatus] ADD  CONSTRAINT [DF_customercrossreferencestatus_dim_status_lastmodified]  DEFAULT (getdate()) FOR [dim_customercrossreferencestatus_lastmodified]
GO
