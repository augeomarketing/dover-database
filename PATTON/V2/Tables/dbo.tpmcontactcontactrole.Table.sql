USE [V2]
GO
/****** Object:  Table [dbo].[tpmcontactcontactrole]    Script Date: 05/07/2010 17:43:51 ******/
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [FK_tpmcontactcontactrole_contact]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [FK_tpmcontactcontactrole_contactrole]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [FK_tpmcontactcontactrole_tpm]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_effectivedate]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_expirationdate]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_created]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] DROP CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_lastmodified]
GO
DROP TABLE [dbo].[tpmcontactcontactrole]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tpmcontactcontactrole](
	[sid_tpm_id] [bigint] NOT NULL,
	[sid_contact_id] [bigint] NOT NULL,
	[sid_contactrole_id] [bigint] NOT NULL,
	[dim_tpmcontactcontactrole_effectivedate] [datetime] NOT NULL,
	[dim_tpmcontactcontactrole_expirationdate] [datetime] NOT NULL,
	[dim_tpmcontactcontactrole_created] [datetime] NOT NULL,
	[dim_tpmcontactcontactrole_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_tpmcontactcontactrole] PRIMARY KEY CLUSTERED 
(
	[sid_tpm_id] ASC,
	[sid_contact_id] ASC,
	[sid_contactrole_id] ASC,
	[dim_tpmcontactcontactrole_effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_tpmcontactcontactrole_contact] FOREIGN KEY([sid_contact_id])
REFERENCES [dbo].[contact] ([sid_contact_id])
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] CHECK CONSTRAINT [FK_tpmcontactcontactrole_contact]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_tpmcontactcontactrole_contactrole] FOREIGN KEY([sid_contactrole_id])
REFERENCES [dbo].[contactrole] ([sid_contactrole_id])
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] CHECK CONSTRAINT [FK_tpmcontactcontactrole_contactrole]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole]  WITH CHECK ADD  CONSTRAINT [FK_tpmcontactcontactrole_tpm] FOREIGN KEY([sid_tpm_id])
REFERENCES [dbo].[tpm] ([sid_tpm_id])
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] CHECK CONSTRAINT [FK_tpmcontactcontactrole_tpm]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] ADD  CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_effectivedate]  DEFAULT (getdate()) FOR [dim_tpmcontactcontactrole_effectivedate]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] ADD  CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_expirationdate]  DEFAULT ('12/31/9999') FOR [dim_tpmcontactcontactrole_expirationdate]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] ADD  CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_created]  DEFAULT (getdate()) FOR [dim_tpmcontactcontactrole_created]
GO
ALTER TABLE [dbo].[tpmcontactcontactrole] ADD  CONSTRAINT [DF_tpmcontactcontactrole_dim_tpmcontactcontactrole_lastmodified]  DEFAULT (getdate()) FOR [dim_tpmcontactcontactrole_lastmodified]
GO
