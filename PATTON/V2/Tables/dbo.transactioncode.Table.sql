USE [V2]
GO
/****** Object:  Table [dbo].[transactioncode]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transactioncode](
	[sid_transactioncode_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_transactioncode_description] [dbo].[description] NOT NULL,
	[sid_transactiontype_id] [dbo].[sid] NOT NULL,
	[dim_transactioncode_ratio] [int] NOT NULL,
	[dim_transactioncode_created] [datetime] NOT NULL,
	[dim_transactioncode_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_transactioncode] PRIMARY KEY CLUSTERED 
(
	[sid_transactioncode_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[transactioncode]  WITH CHECK ADD  CONSTRAINT [FK_transactioncode_transactiontype] FOREIGN KEY([sid_transactiontype_id])
REFERENCES [dbo].[transactiontype] ([sid_transactiontype_id])
GO
ALTER TABLE [dbo].[transactioncode] CHECK CONSTRAINT [FK_transactioncode_transactiontype]
GO
ALTER TABLE [dbo].[transactioncode] ADD  CONSTRAINT [DF_transactioncode_dim_transactioncode_ratio]  DEFAULT ((1)) FOR [dim_transactioncode_ratio]
GO
ALTER TABLE [dbo].[transactioncode] ADD  CONSTRAINT [DF_transactioncode_dim_transactioncode_created]  DEFAULT (getdate()) FOR [dim_transactioncode_created]
GO
ALTER TABLE [dbo].[transactioncode] ADD  CONSTRAINT [DF_transactioncode_dim_transactioncode_lastmodified]  DEFAULT (getdate()) FOR [dim_transactioncode_lastmodified]
GO
