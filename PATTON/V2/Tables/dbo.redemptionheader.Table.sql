USE [V2]
GO
/****** Object:  Table [dbo].[redemptionheader]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[redemptionheader](
	[sid_redemptionheader_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[sid_customer_id] [dbo].[sid] NOT NULL,
	[sid_address_id] [dbo].[sid] NOT NULL,
	[sid_redemptionsource_id] [dbo].[sid] NOT NULL,
	[sid_redemptionstatus_id] [dbo].[sid] NOT NULL,
	[dim_redemptionheader_redemptiondate] [datetime] NOT NULL,
	[dim_redemptionheader_invoicedate] [datetime] NOT NULL,
	[dim_history_transactionid] [uniqueidentifier] NOT NULL,
	[dim_redemptionheader_created] [datetime] NOT NULL,
	[dim_redemptionheader_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_redemptionheader] PRIMARY KEY CLUSTERED 
(
	[sid_redemptionheader_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[redemptionheader]  WITH CHECK ADD  CONSTRAINT [FK_redemptionheader_address] FOREIGN KEY([sid_address_id])
REFERENCES [dbo].[address] ([sid_address_id])
GO
ALTER TABLE [dbo].[redemptionheader] CHECK CONSTRAINT [FK_redemptionheader_address]
GO
ALTER TABLE [dbo].[redemptionheader]  WITH CHECK ADD  CONSTRAINT [FK_redemptionheader_customer] FOREIGN KEY([sid_customer_id])
REFERENCES [dbo].[customer] ([sid_customer_id])
GO
ALTER TABLE [dbo].[redemptionheader] CHECK CONSTRAINT [FK_redemptionheader_customer]
GO
ALTER TABLE [dbo].[redemptionheader]  WITH CHECK ADD  CONSTRAINT [FK_redemptionheader_redemptionsource] FOREIGN KEY([sid_redemptionsource_id])
REFERENCES [dbo].[redemptionsource] ([sid_redemptionsource_id])
GO
ALTER TABLE [dbo].[redemptionheader] CHECK CONSTRAINT [FK_redemptionheader_redemptionsource]
GO
ALTER TABLE [dbo].[redemptionheader]  WITH CHECK ADD  CONSTRAINT [FK_redemptionheader_redemptionstatus] FOREIGN KEY([sid_redemptionstatus_id])
REFERENCES [dbo].[redemptionstatus] ([sid_redemptionstatus_id])
GO
ALTER TABLE [dbo].[redemptionheader] CHECK CONSTRAINT [FK_redemptionheader_redemptionstatus]
GO
ALTER TABLE [dbo].[redemptionheader] ADD  CONSTRAINT [DF_redemptionheader_dim_redemptionheader_created]  DEFAULT (getdate()) FOR [dim_redemptionheader_created]
GO
ALTER TABLE [dbo].[redemptionheader] ADD  CONSTRAINT [DF_redemptionheader_dim_redemptionheader_lastmodified]  DEFAULT (getdate()) FOR [dim_redemptionheader_lastmodified]
GO
