USE [V2]
GO
/****** Object:  Table [dbo].[contactrole]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contactrole](
	[sid_contactrole_id] [dbo].[sid] IDENTITY(1,1) NOT NULL,
	[dim_contactrole_description] [dbo].[description] NOT NULL,
	[dim_contactrole_created] [datetime] NOT NULL,
	[dim_contactrole_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_contactrole] PRIMARY KEY CLUSTERED 
(
	[sid_contactrole_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[contactrole] ADD  CONSTRAINT [DF_contactrole_dim_contractrole_created]  DEFAULT (getdate()) FOR [dim_contactrole_created]
GO
ALTER TABLE [dbo].[contactrole] ADD  CONSTRAINT [DF_contactrole_dim_contractrole_lastmodified]  DEFAULT (getdate()) FOR [dim_contactrole_lastmodified]
GO
