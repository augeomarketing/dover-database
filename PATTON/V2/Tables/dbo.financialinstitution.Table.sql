USE [V2]
GO
/****** Object:  Table [dbo].[financialinstitution]    Script Date: 07/28/2010 11:29:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[financialinstitution](
	[sid_financialinstitution_id] [dbo].[sid] NOT NULL,
	[dim_financialinstitution_tipfirst] [varchar](5) NOT NULL,
	[dim_financialinstitution_name] [varchar](255) NOT NULL,
	[dim_financialinstitution_datejoinedprogram] [date] NOT NULL,
	[sid_financialinstitutionstatus_id] [dbo].[sid] NOT NULL,
	[dim_financialinstitution_webdbserver] [varchar](50) NOT NULL,
	[dim_financialinstitution_webdbname] [varchar](50) NOT NULL,
	[sid_statementtype_id] [dbo].[sid] NOT NULL,
	[dim_financialinstitution_guid] [uniqueidentifier] NOT NULL,
	[dim_financialinstitution_excludeestatement] [varchar](1) NOT NULL,
	[dim_financialinstitution_purgeinactivepoints] [varchar](1) NOT NULL,
	[dim_financialinstitution_inactivepurgeperiodmonths] [int] NOT NULL,
	[sid_householdingtype_id] [dbo].[sid] NOT NULL,
	[dim_financialinstitution_created] [datetime] NOT NULL,
	[dim_financialinstitution_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitution] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitution]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitution_financialinstitutionstatus] FOREIGN KEY([sid_financialinstitutionstatus_id])
REFERENCES [dbo].[financialinstitutionstatus] ([sid_financialinstitutionstatus_id])
GO
ALTER TABLE [dbo].[financialinstitution] CHECK CONSTRAINT [FK_financialinstitution_financialinstitutionstatus]
GO
ALTER TABLE [dbo].[financialinstitution]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitution_householdingtype] FOREIGN KEY([sid_householdingtype_id])
REFERENCES [dbo].[householdingtype] ([sid_householdingtype_id])
GO
ALTER TABLE [dbo].[financialinstitution] CHECK CONSTRAINT [FK_financialinstitution_householdingtype]
GO
ALTER TABLE [dbo].[financialinstitution]  WITH CHECK ADD  CONSTRAINT [FK_financialinstitution_statementtype] FOREIGN KEY([sid_statementtype_id])
REFERENCES [dbo].[statementtype] ([sid_statementtype_id])
GO
ALTER TABLE [dbo].[financialinstitution] CHECK CONSTRAINT [FK_financialinstitution_statementtype]
GO
ALTER TABLE [dbo].[financialinstitution]  WITH CHECK ADD  CONSTRAINT [CK_financialinstitution_excludeestatement] CHECK  (([dim_financialinstitution_excludeestatement]='N' OR [dim_financialinstitution_excludeestatement]='Y'))
GO
ALTER TABLE [dbo].[financialinstitution] CHECK CONSTRAINT [CK_financialinstitution_excludeestatement]
GO
ALTER TABLE [dbo].[financialinstitution]  WITH CHECK ADD  CONSTRAINT [CK_financialinstitution_purgeinactivepoints] CHECK  (([dim_financialinstitution_purgeinactivepoints]='N' OR [dim_financialinstitution_purgeinactivepoints]='Y'))
GO
ALTER TABLE [dbo].[financialinstitution] CHECK CONSTRAINT [CK_financialinstitution_purgeinactivepoints]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_guid]  DEFAULT (newid()) FOR [dim_financialinstitution_guid]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_excludeestatement]  DEFAULT ('N') FOR [dim_financialinstitution_excludeestatement]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_purgeinactivepoints]  DEFAULT ('N') FOR [dim_financialinstitution_purgeinactivepoints]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_inactivepurgeperiodmonths]  DEFAULT ((0)) FOR [dim_financialinstitution_inactivepurgeperiodmonths]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_created]  DEFAULT (getdate()) FOR [dim_financialinstitution_created]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitution_lastmodified]
GO
