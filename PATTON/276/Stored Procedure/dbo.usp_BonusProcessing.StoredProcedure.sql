USE [276]
GO
/****** Object:  StoredProcedure [dbo].[usp_BonusProcessing]    Script Date: 01/29/2015 14:25:28 ******/
DROP PROCEDURE [dbo].[usp_BonusProcessing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_BonusProcessing]
	@tipfirst VARCHAR(3)
	,@MonthEndDate date
	, @debug INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @dbname VARCHAR(255)
		, @sqlUpdate NVARCHAR(MAX)
		, @sqlInsert NVARCHAR(MAX)
		, @sqlDelete NVARCHAR(MAX)
		
	SELECT @dbname = dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst

--
-- Get most current tipnumber in case of combines
	SET @sqlUpdate = REPLACE(
		'
		Update [<DBNAME>].dbo.BonusTracking	
		set tipnumber = rewardsnow.dbo.ufn_getcurrenttip( tipnumber) 
		'
	, '<DBNAME>', @dbname)
	
	if @debug = 0
		EXEC sp_executesql @sqlUpdate
	else 
		print @sqlUpdate

--
-- Get most current tipnumber in case of combines
	SET @sqlUpdate = REPLACE(
		'
		Update [<DBNAME>].dbo.OneTimeBonuses	
		set tipnumber = rewardsnow.dbo.ufn_getcurrenttip( tipnumber) 
		'
	, '<DBNAME>', @dbname)
	
	if @debug = 0
		EXEC sp_executesql @sqlUpdate
	else 
		print @sqlUpdate

--
-- delete records from bonustracking where tip is not in customer stage
	SET @sqlDelete = REPLACE(
		'
		delete bnst 
		from [<DBNAME>].dbo.BonusTracking bnst
		left outer join [<DBNAME>].dbo.CUSTOMER_Stage css
		on bnst.tipnumber = css.TIPNUMBER
		where css.tipnumber is null
		'
	, '<DBNAME>', @dbname)
	
	if @debug = 0
		EXEC sp_executesql @sqlDelete
	else 
		print @sqlDelete

--
-- load records into bonus tracking file
	SET @sqlInsert = REPLACE(REPLACE(REPLACE(
		'
		Insert into [<DBNAME>].dbo.BonusTracking (Tipnumber, ProcessingDate, TransAmt,sid_RNITransaction_ID)
		select rnit.dim_RNITransaction_RNIId, rnit.dim_RNITransaction_EffectiveDate, rnit.dim_RNITransaction_TransactionAmount,rnit.sid_RNITransaction_ID
		from RewardsNow.dbo.RNITransaction rnit with (nolock)
		join [<DBNAME>].dbo.CUSTOMER_Stage css
		on rnit.dim_RNITransaction_RNIId = css.TIPNUMBER
		left outer join [<DBNAME>].dbo.OneTimeBonuses otb
			on otb.TipNumber = rnit.dim_RNITransaction_RNIId
		left outer join [<DBNAME>].dbo.BonusTracking bt
			on rnit.sid_RNITransaction_ID = bt.sid_RNITransaction_ID
		where rnit.sid_dbprocessinfo_dbnumber=''<DBNUMBER>''
		and rnit.dim_RNITransaction_EffectiveDate = ''<MONTHENDDATE>''
		and css.BusinessFlag in (''1'',''2'',''4'')
		and rnit.sid_trantype_trancode = ''63''
		and rnit.dim_RNITransaction_TransactionAmount >=50
		and DATEDIFF(month, css.dateadded, ''<MONTHENDDATE>'')<3
		and otb.TipNumber is null
		and bt.sid_RNITransaction_ID is null
		'
	, '<DBNAME>', @dbname)
	, '<DBNUMBER>', @tipfirst)
	, '<MONTHENDDATE>', @monthenddate)
	
	if @debug = 0
		EXEC sp_executesql @sqlInsert
	else 
		print @sqlInsert
		
--
-- delete records from bonustracking where tip is beyond date range
	SET @sqlDelete = REPLACE(REPLACE(
		'
		delete bnst 
		from [<DBNAME>].dbo.BonusTracking bnst
		join [<DBNAME>].dbo.CUSTOMER_Stage css
		on bnst.tipnumber = css.TIPNUMBER
		where DATEDIFF(month, css.dateadded, ''<MONTHENDDATE>'')>2
		'
	, '<DBNAME>', @dbname)
	, '<MONTHENDDATE>', @monthenddate)
	
	if @debug = 0
		EXEC sp_executesql @sqlDelete
	else 
		print @sqlDelete

--
-- Delete from bonustracking table where bonus already awarded for tip
	SET @sqlDelete = REPLACE(
		'
		delete [<DBNAME>].dbo.BonusTracking
		from [<DBNAME>].dbo.BonusTracking bnst
		join [<DBNAME>].dbo.OneTimeBonuses otb
		on bnst.tipnumber = otb.TIPNUMBER
		'
	, '<DBNAME>', @dbname)
	
	if @debug = 0
		EXEC sp_executesql @sqlDelete
	else 
		print @sqlDelete

--
-- Create Table variable
	Create table #tmp
		(
		Tipnumber varchar(15)
		)
--
-- Determine tips that are in the 60 period
	SET @sqlInsert = REPLACE(REPLACE(
		'
		insert into #tmp
		select Tipnumber
		from [<DBNAME>].dbo.CUSTOMER_Stage css
		where DATEDIFF(month, css.dateadded, ''<MONTHENDDATE>'')=2
	'
	, '<DBNAME>', @dbname)
	, '<MONTHENDDATE>', @monthenddate)
	
	if @debug = 0
		EXEC sp_executesql @sqlInsert
	else 
		print @sqlInsert	

--
-- Generate to bonus
	SET @sqlInsert = REPLACE(REPLACE(
		'
		insert into [<DBNAME>].dbo.HISTORY_Stage
			(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,Overage)
		select distinct bns.tipnumber, null, ''<MONTHENDDATE>'', ''BI'', ''1'', ''2500'', ''Bonus Generic'', ''NEW'', ''1'', 0
		from (select tipnumber from [<DBNAME>].dbo.BonusTracking
						group by Tipnumber having COUNT(*)>=3) bns
		join #tmp tmp
			on bns.tipnumber = tmp.tipnumber
	'
	, '<DBNAME>', @dbname)
	, '<MONTHENDDATE>', @monthenddate)
	
	if @debug = 0
		EXEC sp_executesql @sqlInsert
	else 
		print @sqlInsert

--
-- Insert into onetimebonuses
	SET @sqlInsert = REPLACE(REPLACE(
		'
		insert into [<DBNAME>].dbo.onetimebonuses (tipnumber, trancode, acctid, dateawarded)
		select distinct bns.tipnumber, ''BI'', null, ''<MONTHENDDATE>''
		from (select tipnumber from [<DBNAME>].dbo.BonusTracking
					group by Tipnumber having COUNT(*)>=3) bns
		join #tmp tmp
			on bns.tipnumber = tmp.tipnumber
	'
	, '<DBNAME>', @dbname)
	, '<MONTHENDDATE>', @monthenddate)
	
	if @debug = 0
		EXEC sp_executesql @sqlInsert
	else 
		print @sqlInsert	
END
GO
