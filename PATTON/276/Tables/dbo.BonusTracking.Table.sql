USE [276]
GO
/****** Object:  Table [dbo].[BonusTracking]    Script Date: 01/26/2015 14:07:08 ******/
ALTER TABLE [dbo].[BonusTracking] DROP CONSTRAINT [DF_BonusTracking_TransAmt]
GO
DROP TABLE [dbo].[BonusTracking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusTracking](
	[Tipnumber] [varchar](15) NOT NULL,
	[ProcessingDate] [date] NULL,
	[TransAmt] [decimal](18, 2) NOT NULL,
	[sid_RNITransaction_ID] Bigint
) ON [PRIMARY]
GO
SET ANSI_PADDING On
GO
ALTER TABLE [dbo].[BonusTracking] ADD  CONSTRAINT [DF_BonusTracking_TransAmt]  DEFAULT ((0)) FOR [TransAmt]
GO
