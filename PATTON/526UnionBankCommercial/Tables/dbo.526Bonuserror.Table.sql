USE [526UnionBankCommercial]
GO
/****** Object:  Table [dbo].[526Bonuserror]    Script Date: 09/25/2009 10:35:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[526Bonuserror](
	[Name] [nvarchar](255) NULL,
	[Card Number] [nvarchar](255) NULL,
	[DDA] [nvarchar](255) NULL,
	[ssn] [float] NULL,
	[address1] [nvarchar](255) NULL,
	[address2] [nvarchar](255) NULL,
	[address3] [nvarchar](255) NULL,
	[City State Zip] [nvarchar](255) NULL,
	[10000 Pts] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
