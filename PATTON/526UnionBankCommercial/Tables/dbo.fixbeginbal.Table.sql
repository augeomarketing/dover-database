USE [526UnionBankCommercial]
GO
/****** Object:  Table [dbo].[fixbeginbal]    Script Date: 09/25/2009 10:35:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixbeginbal](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
