USE [51VCommercialBank]
GO
/****** Object:  Table [dbo].[AugEnd]    Script Date: 09/24/2009 14:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AugEnd](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
GO
