USE [111Texans]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 01/11/2010 16:40:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vwCustomer]') AND OBJECTPROPERTY(id, N'IsView') = 1)
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwCustomer]
			 as
			 select *
			 from [111Texans].dbo.customer

'
GO
