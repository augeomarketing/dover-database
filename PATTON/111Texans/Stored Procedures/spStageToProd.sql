USE [111texans]
GO

/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 12/15/2009 11:13:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*  ******************************************/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* Modified							*/
/* 2008-04-23 PHB						*/
/* Mod: Changed to work on 218 LOC FCU		*/
/*  ******************************************/
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spImportStageToProd] @TipFirst char(3)
AS 

Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
Update a
	set YTDEarned = S.YTDEarned 
from dbo.Affiliat_Stage S join dbo.Affiliat A 
	on S.Tipnumber = A.Tipnumber

--	Insert New Affiliat accounts
Insert into dbo.Affiliat 
	select stg.* 
	from dbo.Affiliat_Stage stg left outer join dbo.Affiliat aff
		on stg.tipnumber = aff.tipnumber
	where aff.Tipnumber is null


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
Update c
	Set 
	Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
	Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
	Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
	Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
	City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
	Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
	Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
	Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
From dbo.Customer_Stage S Join dbo.Customer C 
On S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update dbo.Customer_Stage 	
	Set RunAvailable = 0 

--	Insert New Customers from Customers_Stage
Insert into Customer 
	select stg.*
	from Customer_Stage stg left outer join dbo.Customer cus
	on stg.tipnumber = cus.tipnumber
	where cus.Tipnumber is null

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
Update C 
	set RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
From Customer_Stage S Join Customer C 
	On S.Tipnumber = C.Tipnumber

----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
Insert Into dbo.History 
select * from dbo.History_Stage where SecID = 'NEW'

-- Set SecID in History_Stage so it doesn't get double posted. 
Update dbo.History_Stage  
	set SECID = 'Imported to Production '+ convert(char(20), GetDate(), 120)  
where SecID = 'NEW'

----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
insert into dbo.OneTimeBonuses 
select stg.* 
from dbo.OneTimeBonuses_stage stg left outer join dbo.OneTimeBonuses otb
	on stg.TipNumber = otb.tipnumber
	and stg.trancode = otb.trancode
where otb.tipnumber is null

-- Truncate Stage Tables so's we don't double post.
truncate table dbo.Customer_Stage
truncate table dbo.Affiliat_Stage
truncate table dbo.History_Stage
truncate table dbo.OneTimeBonuses_stage

--
GO


