USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 08/10/2010 17:20:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO

USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 08/10/2010 17:20:41 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded datetime AS

-- Clear TransStandard 
Truncate table dbo.TransStandard 

declare @strDate nvarchar(10)

set @strdate = cast( year(@DateAdded) as nvarchar(4)) + '/' +
		     cast( right('00' + cast( month(@DateAdded) as nvarchar(2)), 2) as nvarchar(2))+ '/' +
			cast( right('00' + cast( day(@DateAdded) as nvarchar(2)), 2) as nvarchar(2))

-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	( [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  @strDate, txn.acctid, Trancode, count(txn.acctid), sum(points), @strDate
from dbo.impTransaction txn
group by txn.acctid , trancode


-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update dbo.TransStandard 
	set	TranType = R.Description,
		Ratio = R.Ratio 
from RewardsNow.dbo.Trantype R join dbo.TransStandard T 
on R.TranCode = T.Trancode

update TransStandard
set tip = (Select top 1 tipnumber from affiliat_stage where AcctNum = ACCTID)
where TIP = '' or TIP is null

update TransStandard
set tip = (Select top 1 tipnumber from imptransaction where acctid = transstandard.acctnum)
where TIP = '' or TIP is null

update TransStandard
set TIP = afs.tipnumber
from imptransaction it join TransStandard t
	on it.AcctID = t.AcctNum
join affiliat_stage afs
	on afs.custid = it.ssn

GO


--exec spLoadTransStandard '07/31/2010'