USE [111texans]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 12/14/2009 16:01:18 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded datetime AS

-- Clear TransStandard 
Truncate table dbo.TransStandard 

declare @strDate nvarchar(10)

set @strdate = cast( year(@DateAdded) as nvarchar(4)) + '/' +
		     cast( right('00' + cast( month(@DateAdded) as nvarchar(2)), 2) as nvarchar(2))+ '/' +
			cast( right('00' + cast( day(@DateAdded) as nvarchar(2)), 2) as nvarchar(2))

-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  cus.[TipNumber], @strDate, txn.acctid, '67', count(txn.acctid), sum(points), @strDate
from dbo.impTransaction txn join dbo.customer_stage cus
	on txn.tipnumber = cus.tipnumber
group by cus.[TipNumber], txn.acctid 


-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update dbo.TransStandard 
	set	TranType = R.Description,
		Ratio = R.Ratio 
from RewardsNow.dbo.Trantype R join dbo.TransStandard T 
on R.TranCode = T.Trancode

GO


