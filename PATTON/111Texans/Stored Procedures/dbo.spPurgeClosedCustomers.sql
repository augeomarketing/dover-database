USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 03/22/2011 09:19:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO

USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 03/22/2011 09:19:37 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  Shawn Smith  */
/* DATE: 9/2010   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 

-- read input_purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.

/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted char(10) AS

DECLARE @TipFirst CHAR(3)
SELECT @TipFirst = tipfirst FROM Client

INSERT INTO input_purge (purgedate, tipnumber)
SELECT @DateDeleted, tipnumber FROM rewardsnow.dbo.optouttracking
WHERE tipPrefix = @tipFirst

INSERT INTO input_purge (purgedate, custid)
SELECT DISTINCT @DateDeleted, SSN FROM input_purgessn

-- find tipnumbers for purge accounts
UPDATE ip
  SET ip.tipnumber = a.tipnumber
    FROM affiliat a 
    INNER JOIN input_purge ip 
      ON a.custid = ip.custid
    WHERE ip.tipnumber IS NULL

TRUNCATE TABLE input_purge_error

--exception report
INSERT INTO input_purge_error (acctnbr, reason)
SELECT custid, 'No Record Found' FROM input_purge WHERE tipnumber is null

DELETE FROM input_purge WHERE tipnumber is null

--INSERT INTO input_purge_error (acctnbr, reason)
--SELECT [Primary Acct Nbr], 'Has Current Month Transactions' FROM input_purge ip
--  INNER JOIN input_transaction it
--    ON ip.custid = it.[Primary Acct Nbr]

--DELETE FROM input_purge WHERE custid IN (SELECT [Primary Acct Nbr] FROM input_transaction)


-- load pending purges into purge file
INSERT INTO Input_Purge (tipnumber, purgedate)
SELECT tipnumber, purgedate FROM Purge_Pending 

TRUNCATE TABLE Purge_Pending

-- any tip that has history after purge date is pending
INSERT INTO purge_pending (tipnumber, purgedate)
SELECT ip.tipnumber, @DateDeleted 
  FROM input_purge ip
    INNER JOIN history h
      ON h.tipnumber = ip.tipnumber
  WHERE histdate > @DateDeleted AND TranCode <> 'RQ' 

DELETE FROM input_purge
WHERE tipnumber IN (SELECT tipnumber FROM history WHERE histdate > @DateDeleted AND TranCode <> 'RQ' )


----------- Stage Table Processing ----------
IF @Production_Flag = 'S'
----------- Stage Table Processing ----------
BEGIN
	TRUNCATE TABLE CustomerDeleted_stage 	
	TRUNCATE TABLE AffiliatDeleted_Stage 	
	TRUNCATE TABLE HistoryDeleted_stage 	
	
	INSERT INTO CustomerDeleted_stage
	SELECT * FROM Customer_stage WHERE TipNumber IN (SELECT TipNumber FROM Input_Purge)

	INSERT INTO AffiliatDeleted_Stage
	SELECT * FROM Affiliat_Stage WHERE TipNumber IN (SELECT TipNumber FROM Input_Purge)

	INSERT INTO HistoryDeleted_stage
	SELECT * FROM History_stage WHERE TipNumber IN (SELECT TipNumber FROM Input_Purge)
	
	DELETE FROM Customer_stage WHERE TipNumber IN (SELECT TipNumber FROM Input_Purge)
	DELETE FROM Affiliat_Stage WHERE TipNumber NOT IN (SELECT TipNumber FROM Customer_Stage)
	DELETE FROM History_stage 	WHERE TipNumber NOT IN (SELECT TipNumber FROM Customer_Stage)
END

----------- Production Table Processing ----------
IF @Production_Flag = 'P'
----------- Production Table Processing ----------
BEGIN
	-------------- purge remainging input_purge records. 
	-- Insert customer to customerdeleted 
	INSERT INTO CustomerDeleted (TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
	SELECT c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	FROM Customer c
	  INNER JOIN Input_Purge ip
	    ON c.tipnumber = ip.tipnumber

	-- Insert affiliat to affiliatdeleted 
	INSERT INTO AffiliatDeleted (TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
	SELECT a.TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, a.CustID, @DateDeleted
	FROM Affiliat a
	  INNER JOIN Input_Purge ip
	    ON a.tipnumber = ip.tipnumber

	-- copy history to historyDeleted 
	INSERT INTO HistoryDeleted (tipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	SELECT h.TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, @DateDeleted
	FROM History h
	WHERE TIPNUMBER in ( SELECT TipNumber FROM Input_Purge)

	-- Delete from customer 
	DELETE FROM Customer
	WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- Delete records from affiliat 
	DELETE FROM Affiliat   
	WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- Delete records from History 
	DELETE FROM History 
	WHERE TipNumber IN ( SELECT TipNumber FROM Input_Purge) 

	-- flag all Undeleted Customers "C" that have an input_purge_pending record 
	UPDATE customer SET status = 'C' 
	WHERE tipnumber IN (SELECT Tipnumber FROM Purge_Pending)
END



GO


