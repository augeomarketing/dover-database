USE [111Texans]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateSysConfig]    Script Date: 01/11/2010 16:40:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spUpdateSysConfig]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
create procedure [dbo].[spUpdateSysConfig]
	(@SysConfigParm		nvarchar(50),
	 @SysConfigValue		nvarchar(50))

as

Update dbo.SysConfig 
	set SysConfigValue = @SysConfigValue
where SysConfigParameter = @SysConfigParm

if @@rowcount = 0 -- If 0 rows affected, invalid parameter specified
BEGIN
	raiserror(''Specified parameter not found in SysConfig table.'', 16, 1)
END

' 
END
GO
