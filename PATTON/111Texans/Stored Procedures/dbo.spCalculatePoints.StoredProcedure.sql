USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 08/10/2010 17:21:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCalculatePoints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCalculatePoints]
GO

USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 08/10/2010 17:21:48 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/*  **************************************	*/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* 4/15/08 PHB - Modified to work with		*/
/*				LOC FCU				*/
/*  **************************************	*/
/*  Calculates points in input_transaction	*/
/*  **************************************	*/
CREATE PROCEDURE [dbo].[spCalculatePoints]  AS   

	Update imptransaction
	set points = round(cast(replace(tranamt,CHAR(13),'') as numeric),0) / 2
	
	update imptransaction
	set Trancode = '37', Points = abs(Points)
	where points < 0

GO

/*
exec spcalculatepoints

select top 1000 * from imptransaction 
*/