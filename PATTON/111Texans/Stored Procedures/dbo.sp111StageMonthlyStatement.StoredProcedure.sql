USE [111Texans]
GO
/****** Object:  StoredProcedure [dbo].[sp111StageMonthlyStatement]    Script Date: 01/11/2010 16:40:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sp111StageMonthlyStatement]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'


/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 4/17/2008 Chged to work with LOCFCU
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[sp111StageMonthlyStatement]  @StartDate datetime, @EndDate datetime

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

--Declare @StartDate DateTime 	--RDT 10/09/2006 
--Declare @EndDate DateTime 	--RDT 10/09/2006 

--set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')	--RDT 10/09/2006 
--set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )	--RDT 10/09/2006 

--print @Startdate 
--print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from dbo.Monthly_Statement_File

insert into dbo.Monthly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
from dbo.customer_Stage


/* Load the statmement file with DEBIT purchases          */
update dbo.Monthly_Statement_File 
	set pointspurchased = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''67''), 0)


/* Load the statmement file with DEBIT  returns            */
update dbo.Monthly_Statement_File 
	set pointsreturned = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''37''), 0)


/* Load the statmement file with bonuses            */

update Monthly_Statement_File
	set pointsbonus=(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''B%'')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like  ''B%'')

/* Add  VESDIA to adjustments     */
update dbo.Monthly_Statement_File
	set pointsbonus = pointsbonus + isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''F0''), 0)

/* Add  Access Development to adjustments     */
update dbo.Monthly_Statement_File
	set pointsbonus = pointsbonus + isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''G0''), 0)

/* Load the statmement file with plus adjustments    */
update dbo.Monthly_Statement_File
	set pointsadded = isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''IE''), 0)

/* Add  DECREASED REDEEMED to adjustments     */
update dbo.Monthly_Statement_File
	set pointsadded = pointsadded + isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''DR''), 0)

/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchased + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points) from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')


/* Load the statmement file with minus adjustments    */
update dbo.Monthly_Statement_File
	set pointssubtracted = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''DE''), 0)
	
update dbo.Monthly_Statement_File
	set pointssubtracted = pointssubtracted + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''IR''), 0)
	
update dbo.Monthly_Statement_File
	set pointssubtracted = pointssubtracted + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''F9''), 0)

update dbo.Monthly_Statement_File
	set pointssubtracted = pointssubtracted + isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''G9''), 0)

-- RDT 8/28/2007  Added expired Points 
/* Add expired Points 
Update dbo.Monthly_Statement_File
	set PointsExpire = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''XP''), 0)*/


/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted -- + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update dbo.Monthly_Statement_File
set pointsbegin=(select monthbeg''+ @MonthBegin + N'' from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)''

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased

' 
END
GO
