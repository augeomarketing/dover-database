USE [111texans]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 12/14/2009 15:32:24 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/********************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table		*/
/*    it only updates the customer demographic data						*/
/*																*/
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE						*/
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 1/2007													*/
/* REVISION: 1														*/
/* 6/22/07 South Florida now sending the FULL name in the Name field.			*/
/*																*/
/* 4/15/08 Paul H. Butler:  Copied & modified for use with LOC FCU			*/
/********************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS
/* Update Existing Customers                                         */






Update cstg
	set	lastname		= ltrim(rtrim(imp.lastname)),
		acctname1		= ltrim(rtrim(imp.name1)),
		acctname2		= ltrim(rtrim(imp.name2)),
		address1		= ltrim(rtrim(imp.address1)),	
		address2		= ltrim(rtrim(imp.address2)),	
		address4		= left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.State)) + ' ' + ltrim(rtrim(imp.Zipcode)), 40),
		city			= ltrim(rtrim(imp.City)),
		state		= ltrim(rtrim(imp.State)),
		zipcode		= left(ltrim(rtrim(imp.Zipcode)), 5) + case
														  when right(cstg.zipcode,4) = '0000' then ''
														  else right(cstg.zipcode,4)
												        end,
		misc1		= ltrim(rtrim(imp.ssn))
from dbo.Customer_Stage cstg join dbo.impCustomer imp
on	cstg.tipnumber = imp.tipnumber



/* Add New Customers                                                      */
Insert into Customer_Stage
	(tipnumber, tipfirst, tiplast, lastname,
	 acctname1, acctname2, address1, address2, address4,
	 city, state, zipcode, 
	 misc1, status, dateadded, 
	 runavailable, runbalance, runredeemed, runavaliableNew)
select distinct imp.TIPNUMBER, left(imp.TIPNUMBER,3), right(ltrim(rtrim(imp.TIPNUMBER)),12), left(ltrim(rtrim(imp.Lastname)),40),
		ltrim(rtrim(imp.name1)), ltrim(rtrim(imp.name2)), ltrim(rtrim(imp.address1)), ltrim(rtrim(imp.address2)), 
		left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.State)) + ' ' + ltrim(rtrim(imp.zipcode)) , 40),
		ltrim(rtrim(imp.city)), ltrim(rtrim(imp.state)), 
		left(ltrim(rtrim(imp.zipcode)), 5)  + case
											  when right(cstg.zipcode,4) = '0000' then ''
											  else right(cstg.zipcode,4)
									        end,
		ltrim(rtrim(SSN)),
		'A', @EndDate, 0, 0, 0, 0
from dbo.impCustomer imp left outer join dbo.Customer_Stage cstg
	on imp.tipnumber = cstg.tipnumber
where cstg.tipnumber is null


/* set Default status to A */
Update dbo.Customer_Stage
	Set STATUS = 'A' 
Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status
