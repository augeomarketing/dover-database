USE [111Texans]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 01/11/2010 16:40:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spLoadTipNumbers]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[spLoadTipNumbers] 
		@TipPrefix char(3),
		@MonthEndDate datetime


AS

declare @NewTip			varchar(15)
declare @zeroes			varchar(12)

declare @TipCtr			bigint
declare @lastTipUsed		  varchar(15)


-----------------------------------------------------------------------
--
-- Update tips for existing customers
--
-----------------------------------------------------------------------
update imp
	set tipnumber = aff.tipnumber
from dbo.impCustomer imp join (select distinct ssn, acctid from dbo.imptransaction) txn
	on imp.ssn = txn.ssn
join dbo.affiliat_stage aff
	on txn.acctid = aff.acctid
	
update imp
	set tipnumber = aff.tipnumber
from dbo.impCustomer imp join (select distinct dda, acctid from dbo.imptransaction) txn
	on imp.dda = txn.dda
join dbo.affiliat_stage aff
	on txn.acctid = aff.acctid
where imp.tipnumber is null

-----------------------------------------------------------------------
--
-- Generate TIPS for new customers
--
-----------------------------------------------------------------------

-- Build string of 12 "0"
set @zeroes = replicate(''0'', 12)

-- Get last Tip # Used
--set @NewTip = ( select LastTipNumberUsed  from dbo.Client )
exec rewardsnow.dbo.spGetLastTipNumberUsed @TipPrefix,  @NewTip	   OUTPUT

-- If tip is null, set it to equal to TipFirst + all zeroes
If @NewTip is NULL  Set @NewTip = @TipPrefix+''000000000000''

-- Take the right 12 digits of tip, convert to BIGINT
set @TipCtr = cast(  right(@NewTip,12) as bigint)


-- Build temp table for new tip # generation
create table #NewTipAssignments
	(SSN		varchar(25) primary key,
	 TipNumber			varchar(15) null)

-- Build distinct list of customers needing a tip generated
insert into #NewTipAssignments (SSN)
select distinct SSN
from dbo.impcustomer
where tipnumber is null



/* Assign Get New Tips to new customers  in the temp table*/
update #NewTipAssignments
	set  @Tipctr = ( @Tipctr + 1 ),
	      TIPNUMBER =  @TipPrefix + right(@Zeroes + cast(@TipCtr as varchar(12)), 12)
	where TipNumber is null 


-- Update LastTip in the Client table
--Update dbo.Client 

set @lastTipUsed = @TipPrefix + right(@Zeroes + cast(@TipCtr as varchar(12)), 12)

exec rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @LastTipUsed 

-- Now update the impCustomer table with the new tips
update cus
	set tipnumber = tmp.tipnumber
from dbo.impcustomer cus join #NewTipAssignments tmp
	on cus.SSN = tmp.SSN


-- Update imptransaction table with TIPNumbers
update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impCustomer cus
	on txn.SSN = cus.SSN
	
update txn
	set tipnumber = aff.tipnumber
from dbo.imptransaction txn join dbo.affiliat aff
	on txn.acctid = aff.acctid
where txn.tipnumber is null

' 
END
GO
