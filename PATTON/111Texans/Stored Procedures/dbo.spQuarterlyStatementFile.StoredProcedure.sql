USE [111Texans]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 01/11/2010 16:40:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFile]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spQuarterlyStatementFile] @StartDate varchar(20), @EndDate varchar(20)
AS 
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from quarterly_Statement_File

if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[view_history_TranCode]'') and OBJECTPROPERTY(id, N''IsView'') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = ''create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between ''''''+@StartDate+'''''' and ''''''+ @EndDate +'''''' group by tipnumber, trancode''
exec sp_executesql @SQLDynamic

insert into quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, address4
from customer

/* Load the statmement file with CREDIT purchases          */
update quarterly_Statement_File 
set pointspurchased
= view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''63'' 

update quarterly_Statement_File 
set pointspurchased = pointspurchased +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''67'' 

/* Load the statmement file CREDIT with returns            */
update quarterly_Statement_File 
set pointsreturned
= view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode =''33''

update quarterly_Statement_File 
set pointsreturned = pointsreturned +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode =''37''

/* Load the statmement file with bonuses            */
update quarterly_Statement_File 
set pointsbonus
= view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode like ''B%''

update quarterly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode like ''F0''

update quarterly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode like ''G0''


/* Load the statmement file with plus adjustments */
update quarterly_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
( view_history_TranCode.trancode =''IE''  )

update quarterly_Statement_File 
set pointsadded = pointsadded +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
( view_history_TranCode.trancode =''TR''  )

update quarterly_Statement_File 
set pointsadded = pointsadded +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
( view_history_TranCode.trancode =''TX''  )


/* Add  DECREASED REDEEMED to PointsAdded */
update quarterly_Statement_File 
set  pointsadded=pointsadded + 
 view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''DR''


/* Load the statmement file with total point increases */
update quarterly_Statement_File
set pointsincreased= pointspurchased + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update quarterly_Statement_File 
set pointsredeemed =
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RM''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RT''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RU''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RV''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RC''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RS''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RP''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RG''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RB''

update quarterly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''RD''

/* Load the statmement file with Increases to Subtractions          */
update quarterly_Statement_File
set pointssubtracted = 
view_history_TranCode.TranCodePoints  from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''IR'' 

update quarterly_Statement_File
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''DE''

update quarterly_Statement_File
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''F9''

update quarterly_Statement_File
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''G9''

update quarterly_Statement_File
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from quarterly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = quarterly_Statement_File.tipnumber and 
view_history_TranCode.trancode = ''XP''

/* Load the statmement file with total point decreases */
update quarterly_Statement_File
set pointsdecreased = pointsredeemed+ pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update quarterly_Statement_File
set pointsbegin = (select monthbeg''+ @MonthBegin + N'' from Beginning_Balance_Table where tipnumber=quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=quarterly_Statement_File.tipnumber)''

exec sp_executesql @SQLUpdate

/* Load the statmement file with beginning points */
update quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Drop the view */
drop view view_history_TranCode' 
END
GO
