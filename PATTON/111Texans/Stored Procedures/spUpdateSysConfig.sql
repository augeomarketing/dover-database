USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateSysConfig]    Script Date: 12/14/2009 13:58:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spUpdateSysConfig]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spUpdateSysConfig]
GO

USE [111Texans]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateSysConfig]    Script Date: 12/14/2009 13:58:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[spUpdateSysConfig]
	(@SysConfigParm		nvarchar(50),
	 @SysConfigValue		nvarchar(50))

as

Update dbo.SysConfig 
	set SysConfigValue = @SysConfigValue
where SysConfigParameter = @SysConfigParm

if @@rowcount = 0 -- If 0 rows affected, invalid parameter specified
BEGIN
	raiserror('Specified parameter not found in SysConfig table.', 16, 1)
END

GO


