USE [111Texans]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 01/11/2010 16:40:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*  ******************************************/
/* Date:  4/1/07						*/
/* Author:  Rich T						*/
/*  ******************************************/

/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*
	4/15/2008 PHB:  Copied & modified for use w/ 218LOCFCU
*/
/*********************************************/


CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd datetime  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select distinct t.acctid, t.TipNumber, ''DEBIT'',  @MonthEnd, ''A'', ''DEBIT CARD'', c.lastname, 0
	from dbo.impCustomer c join dbo.impTransaction t 
		on c.Tipnumber = t.Tipnumber
	left outer join dbo.Affiliat_Stage aff
		on t.acctid = aff.AcctId
	where aff.AcctId is null
	
' 
END
GO
