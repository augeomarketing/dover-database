USE [111Texans]
GO
/****** Object:  UserDefinedFunction [dbo].[fnTotalPointsFromHistoryForTranCode]    Script Date: 01/11/2010 16:40:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fnTotalPointsFromHistoryForTranCode]') AND xtype in (N'FN', N'IF', N'TF'))
BEGIN
execute dbo.sp_executesql @statement = N'

create function [dbo].[fnTotalPointsFromHistoryForTranCode] (@TipNumber nvarchar(16), @StartDate datetime, @EndDate datetime, @TranCode nvarchar(2))

returns int

AS

BEGIN
declare @points	int

	set @points = 0

	set @points = (select sum(points) 
					from dbo.History_Stage
					where tipnumber=@TipNumber
					and histdate between @StartDate and @EndDate
					and trancode=@TranCode)

	return @points

END
' 
END
GO
