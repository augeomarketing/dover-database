USE [111Texans]
GO
/****** Object:  User [RNIColdFusion]    Script Date: 01/11/2010 16:40:21 ******/
IF NOT EXISTS (SELECT * FROM dbo.sysusers WHERE name = N'RNIColdFusion')
EXEC dbo.sp_grantdbaccess @loginame = N'RNIColdFusion', @name_in_db = N'RNIColdFusion'
GO
