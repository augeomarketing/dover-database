USE [111Texans]
GO
/****** Object:  Table [dbo].[NewTips]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[NewTips]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[NewTips](
	[acctid] [varchar](16) NULL,
	[tipnumber] [varchar](15) NULL,
	[currentlastname] [nvarchar](255) NULL,
	[otherlastname] [nvarchar](255) NULL,
	[dateadded] [smalldatetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
