USE [111Texans]
GO
/****** Object:  Table [dbo].[SSNFIX]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SSNFIX]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[SSNFIX](
	[SSNUM] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[OLDCCNUM] [nvarchar](16) NULL
) ON [PRIMARY]
END
GO
