USE [111Texans]
GO
/****** Object:  Table [dbo].[Retro_file]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Retro_file]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Retro_file](
	[DDA] [varchar](15) NOT NULL,
	[SSN] [varchar](12) NOT NULL,
	[AcctID] [char](16) NOT NULL,
	[Tranamt] [numeric](18, 0) NOT NULL,
	[Trandate] [smalldatetime] NOT NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
