USE [111Texans]
GO
/****** Object:  Table [dbo].[impcustomer_error]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impcustomer_error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[impcustomer_error](
	[DDA] [varchar](20) NULL,
	[SSN] [varchar](12) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zipcode] [varchar](15) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Lastname] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
