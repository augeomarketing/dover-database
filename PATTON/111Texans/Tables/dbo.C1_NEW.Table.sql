USE [111Texans]
GO
/****** Object:  Table [dbo].[C1_NEW]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[C1_NEW]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[C1_NEW](
	[ACCTNUM] [nvarchar](25) NULL,
	[NAME1] [nvarchar](40) NULL,
	[NAME2] [nvarchar](40) NULL,
	[NAME3] [nvarchar](40) NULL,
	[NAME4] [nvarchar](40) NULL,
	[NAME5] [nvarchar](40) NULL,
	[NAME6] [nvarchar](40) NULL,
	[STATUS] [nvarchar](1) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[ADDRESS1] [nvarchar](40) NULL,
	[ADDRESS2] [nvarchar](40) NULL,
	[ADDRESS3] [nvarchar](40) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[ZIP] [nvarchar](15) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[HOMEPHONE] [nvarchar](10) NULL,
	[WKPHONE] [nvarchar](10) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[BUSFLAG] [nvarchar](1) NULL,
	[BEHSEG] [nvarchar](2) NULL,
	[EMAIL] [nvarchar](40) NULL,
	[CURRBAL] [float] NULL,
	[PAYMENT] [float] NULL,
	[FEE] [float] NULL,
	[ISSUE_DT] [nvarchar](10) NULL,
	[ACT_DATE] [nvarchar](10) NULL,
	[MISC1] [nvarchar](20) NULL,
	[MISC2] [nvarchar](20) NULL,
	[MISC3] [nvarchar](20) NULL
) ON [PRIMARY]
END
GO
