USE [111Texans]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchased] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturned] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Monthly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 99 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__787EE5A0]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__787EE5A0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__787EE5A0]  DEFAULT (0) FOR [PointsBegin]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__797309D9]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__797309D9]  DEFAULT (0) FOR [PointsEnd]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7A672E12]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7A672E12]  DEFAULT (0) FOR [PointsPurchased]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7B5B524B]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7B5B524B]  DEFAULT (0) FOR [PointsBonus]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7C4F7684]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7C4F7684]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7C4F7684]  DEFAULT (0) FOR [PointsAdded]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__7F2BE32F]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__7F2BE32F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__7F2BE32F]  DEFAULT (0) FOR [PointsIncreased]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__00200768]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__00200768]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__00200768]  DEFAULT (0) FOR [PointsRedeemed]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__01142BA1]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__01142BA1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__01142BA1]  DEFAULT (0) FOR [PointsReturned]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__02084FDA]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__02084FDA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__02084FDA]  DEFAULT (0) FOR [PointsSubtracted]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__03F0984C]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__03F0984C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__03F0984C]  DEFAULT (0) FOR [PointsDecreased]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Curre__04E4BC85]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Curre__04E4BC85]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Curre__04E4BC85]  DEFAULT (0) FOR [Currentend]
END


END
GO
