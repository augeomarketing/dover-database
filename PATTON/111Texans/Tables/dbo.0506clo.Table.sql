USE [111Texans]
GO
/****** Object:  Table [dbo].[0506clo]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[0506clo]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[0506clo](
	[TIPNUMBER] [nvarchar](15) NULL,
	[RUNAVAILAB] [float] NULL,
	[RUNBALANCE] [float] NULL,
	[RUNREDEEME] [float] NULL,
	[LASTSTMTDA] [smalldatetime] NULL,
	[DATEADDED] [smalldatetime] NULL,
	[TIPFIRST] [nvarchar](3) NULL,
	[TIPLAST] [nvarchar](12) NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[ACCTNAME2] [nvarchar](40) NULL,
	[ACCTNAME3] [nvarchar](40) NULL,
	[ACCTNAME4] [nvarchar](40) NULL,
	[ACCTNAME5] [nvarchar](40) NULL,
	[ACCTNAME6] [nvarchar](40) NULL,
	[ADDRESS1] [nvarchar](40) NULL,
	[ADDRESS2] [nvarchar](40) NULL,
	[ADDRESS3] [nvarchar](40) NULL,
	[ADDRESS4] [nvarchar](40) NULL,
	[ZIPCODE] [nvarchar](15) NULL,
	[HOMEPHONE] [nvarchar](20) NULL,
	[WORKPHONE] [nvarchar](20) NULL
) ON [PRIMARY]
END
GO
