USE [111Texans]
GO

/****** Object:  Table [dbo].[input_purge_error]    Script Date: 09/30/2010 17:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[input_purge_error]') AND type in (N'U'))
DROP TABLE [dbo].[input_purge_error]
GO

USE [111Texans]
GO

/****** Object:  Table [dbo].[input_purge_error]    Script Date: 09/30/2010 17:00:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[input_purge_error](
	[input_purge_error_id] [int] IDENTITY(1,1) NOT NULL,
	[acctnbr] [varchar](20) NULL,
	[reason] [varchar](1024) NULL,
 CONSTRAINT [PK_input_purge_error] PRIMARY KEY CLUSTERED 
(
	[input_purge_error_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


