USE [111Texans]
GO
/****** Object:  Table [dbo].[TEXIN2]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TEXIN2]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TEXIN2](
	[DDA] [nvarchar](20) NULL,
	[NAME1] [nvarchar](35) NULL,
	[NAME2] [nvarchar](35) NULL,
	[ADDRESS1] [nvarchar](32) NULL,
	[ADDRESS2] [nvarchar](10) NULL,
	[CITY] [nvarchar](32) NULL,
	[STATE] [nvarchar](32) NULL,
	[ZIP] [nvarchar](5) NULL,
	[PHONE1] [nvarchar](12) NULL,
	[SSNUM] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[ISTATUS] [nvarchar](25) NULL,
	[USED] [nvarchar](10) NULL,
	[DDA2] [nvarchar](20) NULL,
	[SHARE] [nvarchar](2) NULL,
	[MISC] [nvarchar](8) NULL,
	[OLDACCTID] [nvarchar](16) NULL
) ON [PRIMARY]
END
GO
