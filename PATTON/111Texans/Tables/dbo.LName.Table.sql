USE [111Texans]
GO
/****** Object:  Table [dbo].[LName]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[LName]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[LName](
	[LASTNAME] [nvarchar](50) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[RECNUM] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
END
GO
