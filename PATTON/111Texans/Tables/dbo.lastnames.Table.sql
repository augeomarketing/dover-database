USE [111Texans]
GO
/****** Object:  Table [dbo].[lastnames]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[lastnames]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[lastnames](
	[TIPNUMBER] [nvarchar](15) NULL,
	[LASTNAME] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
