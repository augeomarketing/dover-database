USE [111Texans]
GO
/****** Object:  Table [dbo].[retro_nocard]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[retro_nocard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[retro_nocard](
	[DDA] [varchar](20) NOT NULL,
	[SSN] [varchar](9) NOT NULL,
	[Amount] [numeric](18, 0) NOT NULL,
	[Acctid] [varchar](16) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
