USE [111Texans]
GO
/****** Object:  Table [dbo].[Tran_CSV_In]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Tran_CSV_In]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Tran_CSV_In](
	[DDA] [varchar](15) NOT NULL,
	[AcctID] [char](16) NOT NULL,
	[Trandate] [smalldatetime] NOT NULL,
	[Tranamt] [money] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
