USE [111Texans]
GO
/****** Object:  Table [dbo].[TEXRED]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TEXRED]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TEXRED](
	[tipnumber] [varchar](15) NOT NULL,
	[red] [decimal](38, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
