USE [111Texans]
GO

/****** Object:  Table [dbo].[input_purgessn]    Script Date: 09/30/2010 17:00:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[input_purgessn]') AND type in (N'U'))
DROP TABLE [dbo].[input_purgessn]
GO

USE [111Texans]
GO

/****** Object:  Table [dbo].[input_purgessn]    Script Date: 09/30/2010 17:00:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[input_purgessn](
	[input_purgessn_id] [int] IDENTITY(1,1) NOT NULL,
	[SSN] [varchar](25) NOT NULL,
 CONSTRAINT [PK_input_purgessn] PRIMARY KEY CLUSTERED 
(
	[input_purgessn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


