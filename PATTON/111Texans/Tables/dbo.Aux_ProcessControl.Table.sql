USE [111Texans]
GO
/****** Object:  Table [dbo].[Aux_ProcessControl]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Aux_ProcessControl]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Aux_ProcessControl](
	[RecordNumber] [int] IDENTITY(1,1) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[MonthBegin] [datetime] NOT NULL,
	[MonthEnd] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
