USE [111Texans]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 01/11/2010 16:40:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TransStandard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TransStandard](
	[TIP] [nvarchar](15) NOT NULL,
	[TranDate] [nvarchar](10) NOT NULL,
	[AcctNum] [nvarchar](25) NOT NULL,
	[TranCode] [nvarchar](2) NOT NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](20) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL,
 CONSTRAINT [PK_TransStandard] PRIMARY KEY CLUSTERED 
(
	[TIP] ASC,
	[TranDate] ASC,
	[AcctNum] ASC,
	[TranCode] ASC
)WITH FILLFACTOR = 99 ON [PRIMARY]
) ON [PRIMARY]
END
GO
