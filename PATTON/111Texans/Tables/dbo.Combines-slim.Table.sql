USE [111Texans]
GO
/****** Object:  Table [dbo].[Combines-slim]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Combines-slim]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Combines-slim](
	[tipnumber] [varchar](15) NULL,
	[Expr1] [varchar](40) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
