USE [111Texans]
GO
/****** Object:  Table [dbo].[TNBSRC1]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TNBSRC1]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TNBSRC1](
	[ACCTNUM] [nvarchar](16) NULL,
	[NAME1] [nvarchar](35) NULL,
	[NAME1F] [nvarchar](30) NULL,
	[NAME1M] [nvarchar](30) NULL,
	[NAME1L] [nvarchar](30) NULL,
	[NAME2] [nvarchar](35) NULL,
	[ESTATUS] [nvarchar](1) NULL,
	[ISTATUS] [nvarchar](1) NULL,
	[OLDCCNUM] [nvarchar](16) NULL,
	[ADDRESS1] [nvarchar](32) NULL,
	[ADDRESS2] [nvarchar](32) NULL,
	[CITY] [nvarchar](32) NULL,
	[STATE] [nvarchar](32) NULL,
	[ZIP] [nvarchar](5) NULL,
	[PHONE1] [nvarchar](12) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[SSNUM] [nvarchar](11) NULL,
	[ASSNUM] [nvarchar](3) NULL,
	[MISCFLD3] [nvarchar](10) NULL,
	[SSNUM2] [nvarchar](11) NULL,
	[DDA] [nvarchar](20) NULL,
	[SAV] [nvarchar](20) NULL,
	[REFERENCE] [nvarchar](16) NULL
) ON [PRIMARY]
END
GO
