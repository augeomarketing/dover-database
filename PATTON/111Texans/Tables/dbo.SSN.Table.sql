USE [111Texans]
GO
/****** Object:  Table [dbo].[SSN]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SSN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[SSN](
	[ACCTNUM] [nvarchar](16) NULL,
	[SSNUM] [nvarchar](11) NULL,
	[TIPNumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
