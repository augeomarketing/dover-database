USE [111Texans]
GO
/****** Object:  Table [dbo].[dupes]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[dupes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[dupes](
	[acctid] [nvarchar](255) NOT NULL,
	[OldTip] [float] NULL,
	[rightlastname] [nvarchar](255) NULL,
	[lastname] [nvarchar](255) NULL,
	[dateadded] [smalldatetime] NULL,
	[NewTip] [float] NULL,
 CONSTRAINT [PK_dupes] PRIMARY KEY CLUSTERED 
(
	[acctid] ASC
)WITH FILLFACTOR = 99 ON [PRIMARY]
) ON [PRIMARY]
END
GO
