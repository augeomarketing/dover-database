USE [111Texans]
GO
/****** Object:  Table [dbo].[imptransaction_error]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[imptransaction_error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[imptransaction_error](
	[DDA] [varchar](15) NOT NULL,
	[SSN] [varchar](10) NOT NULL,
	[AcctID] [char](16) NOT NULL,
	[Trandate] [smalldatetime] NOT NULL,
	[Tranamt] [numeric](9, 2) NOT NULL,
	[Tipnumber] [varchar](15) NULL,
	[Points] [bigint] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
