USE [111Texans]
GO
/****** Object:  Table [dbo].[M1110207]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[M1110207]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[M1110207](
	[TRAVNUM] [nvarchar](15) NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[ACCTNAME2] [nvarchar](40) NULL,
	[ADDRESS1] [nvarchar](40) NULL,
	[ADDRESS2] [nvarchar](40) NULL,
	[ADDRESS3] [nvarchar](40) NULL,
	[LASTLINE] [nvarchar](40) NULL,
	[STDATE] [nvarchar](30) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHS] [float] NULL,
	[PNTBONUS] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRN] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTDECRS] [float] NULL,
	[ZIP] [nvarchar](5) NULL,
	[ACCT_NUM] [nvarchar](16) NULL,
	[LASTFOUR] [nvarchar](4) NULL
) ON [PRIMARY]
END
GO
