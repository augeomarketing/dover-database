USE [111Texans]
GO
/****** Object:  Table [dbo].[Combines]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Combines]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Combines](
	[TIP_PRI] [varchar](15) NOT NULL,
	[TIP_SEC] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[Expr2] [varchar](40) NOT NULL,
	[Expr3] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[status1] [char](1) NULL,
	[Expr4] [varchar](40) NULL,
	[Expr5] [varchar](40) NULL,
	[status2] [char](1) NULL,
	[recnum] [int] IDENTITY(1,1) NOT NULL,
	[errmsg] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
