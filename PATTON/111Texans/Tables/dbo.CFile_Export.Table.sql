USE [111Texans]
GO
/****** Object:  Table [dbo].[CFile_Export]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CFile_Export]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[CFile_Export](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[DATEADDED] [datetime] NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[CardNo] [char](16) NULL,
 CONSTRAINT [PK_CFile_Export] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 99 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
