USE [111Texans]
GO
/****** Object:  Table [dbo].[sysconfig]    Script Date: 01/11/2010 16:40:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[sysconfig]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[sysconfig](
	[SysConfigId] [int] IDENTITY(1,1) NOT NULL,
	[SysConfigParameter] [varchar](50) NOT NULL,
	[SysConfigValue] [varchar](500) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
