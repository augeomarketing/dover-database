USE [513LaytonCommercial]
GO
/****** Object:  Table [dbo].[DailyPoints]    Script Date: 09/24/2009 09:16:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyPoints](
	[tipnumber] [char](20) NOT NULL,
	[availablebal] [int] NULL,
	[AccountNumber] [char](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
