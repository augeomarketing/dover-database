USE [513LaytonCommercial]
GO
/****** Object:  Table [dbo].[WorkAccountNumber]    Script Date: 09/24/2009 09:16:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkAccountNumber](
	[AccountNumber] [char](16) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
