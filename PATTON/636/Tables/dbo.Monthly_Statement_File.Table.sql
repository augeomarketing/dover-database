USE [636SuperiorIron]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 04/22/2011 15:25:13 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_pointfloor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_pointfloor]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0),
	[PointsPurchasedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0),
	[PointsBonusCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]  DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0),
	[PointsPurchasedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0),
	[PointsBonusDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0),
	[PointsReturnedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0),
	[PointsReturnedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0),
	[acctid] [char](16) NULL,
	[lastfour] [char](4) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[pointfloor] [char](11) NULL CONSTRAINT [DF_Monthly_Statement_File_pointfloor]  DEFAULT (0),
	[PointsExpire] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT (0),
	[PointsBonusMN] [numeric](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
