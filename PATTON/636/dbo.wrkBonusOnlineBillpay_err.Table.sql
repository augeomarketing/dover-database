SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkBonusOnlineBillpay_err](
	[Cardnumber] [varchar](20) NULL,
	[Customer] [varchar](50) NULL,
	[RewardsPoints] [int] NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
