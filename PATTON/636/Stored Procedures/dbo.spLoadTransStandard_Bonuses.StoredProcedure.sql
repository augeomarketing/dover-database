USE [636SuperiorIron]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard_Bonuses]    Script Date: 08/09/2011 10:39:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard_Bonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard_Bonuses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadTransStandard_Bonuses] @TipFirst varchar(15),@EndDate char(10)
AS
/*********************************************/
/* S Blanchette                              */
/* 8/2011                                    */
/* SEB001                                    */
/* Add BE bonus must remove after Nov process*/
/* of October data                           */
/*                                           */
/*********************************************/
/*********************************************/
/* S Blanchette                              */
/* 12/2011                                    */
/* SEB002                                    */
/* Remove BE bonus after Dec process         */
/*                                           */
/*********************************************/


declare @StartDate char(10), @TranCode varchar(2), @Description nvarchar(40), @TranAmt int
set @StartDate=substring(@EndDate,1,2) + ''/01/'' + substring(@EndDate,7,4)

--clear the table
Truncate table TransStandard_Bonuses

--------------------------------------------------------------------------
--Activation bonus - For those who get tips AFTER the first initial load of the first month
Set @TranCode = ''BA''
Set @TranAmt=2500
Select @Description= [Description] from tranType where TranCode=@TranCode
------------------
--MODIFY THIS ON 2nd MONTH OF PROCESSING
Insert into TransStandard_Bonuses (tfno) 
	select tipnumber from Customer_stage where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=@TranCode)
	AND DateAdded=@EndDate
	--AND DateAdded >''03/31/2009''
	--Un-rem and change this to the EndDate of whatever the 2nd month of processing is
	--AND DateAdded >''03/31/2009''

--update the newly added records
update TransStandard_Bonuses
set
TranDate=@EndDate,
Acct_Num=NULL,
Trancode=@TranCode,
TranNum=1,
TranAmt=@TranAmt,
TranType=@Description,
Ratio=1
where Trancode is Null and tfno is not Null
--------
--load _stage
Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tfno, @TranCode, null ,@EndDate from TransStandard_Bonuses where tfno not in (select tipnumber from OnetimeBonuses_stage where trancode=@TranCode)
-----------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
/* Generic Bonuses BI---  Superior provides a file of 500 point bonuses for OnlineBillPay*/

truncate table wrkBonusOnlineBillpay_err
declare @BillPayBonus varchar(5)
set @BillPayBonus=''500''
Set @TranCode = ''BI''
Select @Description= [Description] from tranType where TranCode=@TranCode
------------------update the tipnumber
UPDATE w set w.TIPNUMBER =a.tipnumber 
FROM  aFFILIAT_stage a
join wrkBonusOnlineBillpay w on
w.CardNumber = a.acctID
-------put recs that were not matched to a tipnumber in the error table and remove from Points table

insert into wrkBonusOnlineBillpay_err
	Select * From wrkBonusOnlineBillpay where tipnumber is null
delete wrkBonusOnlineBillpay where Tipnumber is null
-------------------

Insert into TransStandard_Bonuses (tfno, TranDate , Acct_Num, Trancode, TranNum, TranAmt, TranType, Ratio)
	select tipnumber, @EndDate, CardNumber , @TranCode, 1, @BillPayBonus, @Description, 1 from wrkBonusOnlineBillpay  WHERE cardnumber not in (select acctid from OnetimeBonuses_stage where trancode=@TranCode)

----------------------
--load _stage
Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select Tipnumber, @TranCode, CardNumber ,@EndDate from wrkBonusOnlineBillpay where Cardnumber not in (select acctid from OnetimeBonuses_stage where trancode=@TranCode)

-----------------------------------------------------------------------------------------------------------------------
/*****************/
/* START SEB002  */
/*****************/

/*****************/
/* START SEB001  */
/*****************/

----E-Statement Bonuses
--Set @TranCode = ''BE''
--Set @TranAmt=500
--Select @Description= [Description] from [RewardsNow].[dbo].[TranType] where TranCode=@TranCode
--------------------
--Insert into TransStandard_Bonuses (tfno) 
--	select tipnumber from EStmt_Work 
--		where tipnumber not in (select tipnumber from OnetimeBonuses_stage where trancode=''BE'')
----update the newly added records
--update TransStandard_Bonuses
--set
--TranDate=@EndDate,
--Acct_Num=NULL,
--Trancode=@TranCode,
--TranNum=1,
--TranAmt=@TranAmt,
--TranType=@Description,
--Ratio=1
--where Trancode is Null and tfno is not Null
----------
----load _stage
--Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
--select tfno, @TranCode, null ,@EndDate from TransStandard_Bonuses where tfno not in (select tipnumber from OnetimeBonuses_stage where trancode=@TranCode)

/*****************/
/* END SEB001    */
/*****************/
/*****************/
/* END SEB002    */
/*****************/
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
/*
--Online Registration Bonus 

Set @TranCode = ''BR''
Set @TranAmt=250
Select @Description= [Description] from tranType where TranCode=@TranCode
------------------
Insert into TransStandard_Bonuses (tfno) 
	select tipnumber from RN1.Coop.dbo.[1security]
			where password is not null and regdate>=@StartDate and regdate<=@EndDate and left(tipnumber,3) = @TipFirst
--update the newly added records
update TransStandard_Bonuses
set
TranDate=@EndDate,
Acct_Num=NULL,
Trancode=@TranCode,
TranNum=1,
TranAmt=@TranAmt,
TranType=@Description
where Trancode is Null and tfno is not Null
--------
--load _stage
Insert into OnetimeBonuses_stage (Tipnumber, trancode, Acctid, DateAwarded)
select tfno, @TranCode, null ,@EndDate from TransStandard_Bonuses where tfno not in (select tipnumber from OnetimeBonuses_stage where trancode=@TranCode)
*/

------------------------------------------------------------------------------------------------------------------------
/* Copy records to TransStandard*/

insert into transstandard 
	Select * from TransStandard_Bonuses


---------------------------------------------------------------------------------------
' 
END
GO
