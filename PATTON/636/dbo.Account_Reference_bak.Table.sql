SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Reference_bak](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Account_Reference_bak] ADD  CONSTRAINT [DF_Account_Reference_bak_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
