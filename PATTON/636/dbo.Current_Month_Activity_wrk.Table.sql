SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity_wrk](
	[Tipnumber] [nvarchar](15) NULL,
	[Points] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Current_Month_Activity_wrk] ADD  CONSTRAINT [DF_Current_Month_Activity_wrk_Points]  DEFAULT (0) FOR [Points]
GO
