USE [ASB]
GO
/****** Object:  Table [dbo].[fixbonus]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixbonus]') AND type in (N'U'))
DROP TABLE [dbo].[fixbonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixbonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixbonus](
	[tipnumber] [nvarchar](255) NULL,
	[crdhlr_nbr] [nvarchar](255) NULL,
	[points] [float] NULL
) ON [PRIMARY]
END
GO
