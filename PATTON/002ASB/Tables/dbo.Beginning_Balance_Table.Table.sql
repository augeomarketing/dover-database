USE [ASB]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1CBC4616]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1CBC4616]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__1CBC4616]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1DB06A4F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__1DB06A4F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1EA48E88]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__1EA48E88]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1F98B2C1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__1F98B2C1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__208CD6FA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__208CD6FA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__2180FB33]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__2180FB33]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__22751F6C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__22751F6C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__236943A5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__236943A5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__245D67DE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__245D67DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__245D67DE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__25518C17]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__25518C17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__25518C17]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__2645B050]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__2645B050]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__2739D489]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__2739D489]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1CBC4616]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1CBC4616]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__1CBC4616]  DEFAULT (0) FOR [MonthBeg1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1DB06A4F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__1DB06A4F]  DEFAULT (0) FOR [MonthBeg2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1EA48E88]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__1EA48E88]  DEFAULT (0) FOR [MonthBeg3]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__1F98B2C1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__1F98B2C1]  DEFAULT (0) FOR [MonthBeg4]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__208CD6FA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__208CD6FA]  DEFAULT (0) FOR [MonthBeg5]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__2180FB33]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__2180FB33]  DEFAULT (0) FOR [MonthBeg6]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__22751F6C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__22751F6C]  DEFAULT (0) FOR [MonthBeg7]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__236943A5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__236943A5]  DEFAULT (0) FOR [MonthBeg8]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__245D67DE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__245D67DE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__245D67DE]  DEFAULT (0) FOR [MonthBeg9]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__25518C17]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__25518C17]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__25518C17]  DEFAULT (0) FOR [MonthBeg10]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__2645B050]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__2645B050]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__2645B050]  DEFAULT (0) FOR [MonthBeg11]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__2739D489]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__2739D489]  DEFAULT (0) FOR [MonthBeg12]
END


End
GO
