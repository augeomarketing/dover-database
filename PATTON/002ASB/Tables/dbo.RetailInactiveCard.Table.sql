USE [ASB]
GO
/****** Object:  Table [dbo].[RetailInactiveCard]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailInactiveCard]') AND type in (N'U'))
DROP TABLE [dbo].[RetailInactiveCard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailInactiveCard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RetailInactiveCard](
	[tipnumber] [varchar](15) NOT NULL,
	[name] [char](40) NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
