USE [ASB]
GO
/****** Object:  Table [dbo].[srchdiff]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[srchdiff]') AND type in (N'U'))
DROP TABLE [dbo].[srchdiff]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[srchdiff]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[srchdiff](
	[newtip] [char](15) NOT NULL,
	[seg1] [varchar](1) NOT NULL,
	[oldtip] [char](15) NOT NULL,
	[seg2] [varchar](1) NOT NULL,
	[points] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
