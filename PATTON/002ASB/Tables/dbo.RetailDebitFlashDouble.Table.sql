USE [ASB]
GO
/****** Object:  Table [dbo].[RetailDebitFlashDouble]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailDebitFlashDouble]') AND type in (N'U'))
DROP TABLE [dbo].[RetailDebitFlashDouble]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailDebitFlashDouble]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RetailDebitFlashDouble](
	[ACCT_NBR] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[CRDHLDR_NME] [nvarchar](255) NULL,
	[REW_PLN] [nvarchar](255) NULL,
	[tipnumber] [nchar](15) NULL,
	[purch] [float] NULL
) ON [PRIMARY]
END
GO
