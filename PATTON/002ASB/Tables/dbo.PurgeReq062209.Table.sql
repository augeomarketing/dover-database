USE [ASB]
GO
/****** Object:  Table [dbo].[PurgeReq062209]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeReq062209]') AND type in (N'U'))
DROP TABLE [dbo].[PurgeReq062209]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeReq062209]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurgeReq062209](
	[Tipnumber] [nvarchar](255) NULL,
	[acctname1] [nvarchar](255) NULL,
	[acctname2] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
