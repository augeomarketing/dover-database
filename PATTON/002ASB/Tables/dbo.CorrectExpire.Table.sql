USE [ASB]
GO
/****** Object:  Table [dbo].[CorrectExpire]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorrectExpire]') AND type in (N'U'))
DROP TABLE [dbo].[CorrectExpire]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CorrectExpire]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CorrectExpire](
	[tipnumber] [nchar](15) NULL,
	[correctpointstoexpire] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
