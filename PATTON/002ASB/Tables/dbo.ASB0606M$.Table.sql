USE [ASB]
GO
/****** Object:  Table [dbo].[ASB0606M$]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB0606M$]') AND type in (N'U'))
DROP TABLE [dbo].[ASB0606M$]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB0606M$]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASB0606M$](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
END
GO
