USE [ASB]
GO
/****** Object:  Table [dbo].[closedredemptions]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[closedredemptions]') AND type in (N'U'))
DROP TABLE [dbo].[closedredemptions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[closedredemptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[closedredemptions](
	[tipnumber] [varchar](15) NOT NULL,
	[Datelastpurchase] [datetime] NULL,
	[Datelastredemption] [datetime] NULL,
	[flag] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
