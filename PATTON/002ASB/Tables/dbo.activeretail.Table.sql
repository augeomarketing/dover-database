USE [ASB]
GO
/****** Object:  Table [dbo].[activeretail]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[activeretail]') AND type in (N'U'))
DROP TABLE [dbo].[activeretail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[activeretail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[activeretail](
	[tipnumber] [varchar](15) NOT NULL,
	[segmentcode] [char](20) NULL,
	[histdate] [datetime] NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[runavailable] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
