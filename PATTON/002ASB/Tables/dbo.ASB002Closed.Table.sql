USE [ASB]
GO
/****** Object:  Table [dbo].[ASB002Closed]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB002Closed]') AND type in (N'U'))
DROP TABLE [dbo].[ASB002Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASB002Closed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASB002Closed](
	[TIPNUMBER] [nvarchar](255) NULL,
	[ACCTNUM] [nvarchar](255) NULL,
	[OLDCC] [nvarchar](255) NULL,
	[NA1] [nvarchar](255) NULL,
	[NA2] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
