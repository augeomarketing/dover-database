USE [ASB]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Endin__29221CFB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Endin__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Endin__29221CFB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Incre__2A164134]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Incre__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Incre__2A164134]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Decre__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Decre__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Decre__2B0A656D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Adjus__2BFE89A6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Adjus__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Adjus__2BFE89A6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Endin__29221CFB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Endin__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [EndingPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Incre__2A164134]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Incre__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [Increases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Decre__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Decre__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [Decreases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Adjus__2BFE89A6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Adjus__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [AdjustedEndingPoints]
END


End
GO
