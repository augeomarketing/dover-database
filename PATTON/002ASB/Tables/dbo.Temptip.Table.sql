USE [ASB]
GO
/****** Object:  Table [dbo].[Temptip]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Temptip]') AND type in (N'U'))
DROP TABLE [dbo].[Temptip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Temptip]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Temptip](
	[tipnumber] [varchar](15) NOT NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
