USE [ASB]
GO
/****** Object:  Table [dbo].[wrkhist]    Script Date: 01/12/2010 08:13:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist]') AND type in (N'U'))
DROP TABLE [dbo].[wrkhist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkhist](
	[tipnumber] [varchar](15) NOT NULL,
	[trancode] [varchar](2) NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[wrkhist]') AND name = N'IX_wrkhist')
CREATE NONCLUSTERED INDEX [IX_wrkhist] ON [dbo].[wrkhist] 
(
	[tipnumber] ASC,
	[trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
