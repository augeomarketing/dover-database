USE [ASB]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AffiliatDeleted_TipNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AffiliatDeleted_TipNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AffiliatDeleted] DROP CONSTRAINT [DF_AffiliatDeleted_TipNumber]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AffiliatDeleted_YTDEarned]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AffiliatDeleted_YTDEarned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AffiliatDeleted] DROP CONSTRAINT [DF_AffiliatDeleted_YTDEarned]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[AffiliatDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NULL,
	[CustID] [char](13) NULL,
	[datedeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AffiliatDeleted_TipNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AffiliatDeleted_TipNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AffiliatDeleted] ADD  CONSTRAINT [DF_AffiliatDeleted_TipNumber]  DEFAULT (0) FOR [TipNumber]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AffiliatDeleted_YTDEarned]') AND parent_object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AffiliatDeleted_YTDEarned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AffiliatDeleted] ADD  CONSTRAINT [DF_AffiliatDeleted_YTDEarned]  DEFAULT (0) FOR [YTDEarned]
END


End
GO
