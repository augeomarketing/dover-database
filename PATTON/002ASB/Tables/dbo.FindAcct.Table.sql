USE [ASB]
GO
/****** Object:  Table [dbo].[FindAcct]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAcct]') AND type in (N'U'))
DROP TABLE [dbo].[FindAcct]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAcct]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FindAcct](
	[002000000000750] [nvarchar](255) NULL,
	[RENEE K NAMAHOE] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
