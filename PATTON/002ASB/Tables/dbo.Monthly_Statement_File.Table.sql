USE [ASB]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6CD828CA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6CD828CA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__6CD828CA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6DCC4D03]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6DCC4D03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__6DCC4D03]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6EC0713C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6EC0713C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__6EC0713C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6FB49575]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6FB49575]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__6FB49575]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__70A8B9AE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__70A8B9AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__70A8B9AE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__719CDDE7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__719CDDE7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__719CDDE7]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__72910220]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__72910220]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__72910220]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__73852659]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__73852659]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__73852659]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__74794A92]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__74794A92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__74794A92]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__756D6ECB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__756D6ECB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__756D6ECB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__76619304]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__76619304]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__76619304]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__7755B73D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__7755B73D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__7755B73D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__7849DB76]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__7849DB76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__7849DB76]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[PointsToExpire] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND name = N'IX_Monthly_Statement_File')
CREATE NONCLUSTERED INDEX [IX_Monthly_Statement_File] ON [dbo].[Monthly_Statement_File] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6CD828CA]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6CD828CA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__6CD828CA]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6DCC4D03]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6DCC4D03]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__6DCC4D03]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6EC0713C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6EC0713C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__6EC0713C]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__6FB49575]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__6FB49575]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__6FB49575]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__70A8B9AE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__70A8B9AE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__70A8B9AE]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__719CDDE7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__719CDDE7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__719CDDE7]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__72910220]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__72910220]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__72910220]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsBonusMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT (0) FOR [PointsBonusMN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__73852659]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__73852659]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__73852659]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__74794A92]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__74794A92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__74794A92]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__756D6ECB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__756D6ECB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__756D6ECB]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__76619304]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__76619304]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__76619304]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__7755B73D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__7755B73D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__7755B73D]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_S__Point__7849DB76]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__7849DB76]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__7849DB76]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Monthly_Statement_File_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsToExpire]  DEFAULT ('0') FOR [PointsToExpire]
END


End
GO
