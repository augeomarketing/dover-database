USE [ASB]
GO
/****** Object:  Table [dbo].[wrkexpireamt]    Script Date: 01/12/2010 08:13:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkexpireamt]') AND type in (N'U'))
DROP TABLE [dbo].[wrkexpireamt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkexpireamt]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkexpireamt](
	[tipnumber] [varchar](15) NOT NULL,
	[expire] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
