USE [ASB]
GO
/****** Object:  Table [dbo].[CustomerPurge]    Script Date: 06/14/2010 06:32:18 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CustomerPurge_RunAvailable]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPurge]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CustomerPurge_RunAvailable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerPurge] DROP CONSTRAINT [DF_CustomerPurge_RunAvailable]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPurge]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPurge]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerPurge](
	[Tipnumber] [char](15) NOT NULL,
	[DateDeleted] [datetime] NOT NULL,
	[RunAvailable] [int] NOT NULL,
	[SegmentCode] [nchar](1) NULL,
 CONSTRAINT [PK_CustomerPurge] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CustomerPurge_RunAvailable]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPurge]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CustomerPurge_RunAvailable]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerPurge] ADD  CONSTRAINT [DF_CustomerPurge_RunAvailable]  DEFAULT ((0)) FOR [RunAvailable]
END


End
GO
