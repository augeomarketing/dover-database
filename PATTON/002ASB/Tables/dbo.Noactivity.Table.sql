USE [ASB]
GO
/****** Object:  Table [dbo].[Noactivity]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Noactivity]') AND type in (N'U'))
DROP TABLE [dbo].[Noactivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Noactivity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Noactivity](
	[tipnumber] [varchar](15) NOT NULL,
	[Typecode] [char](10) NULL,
	[histdate] [datetime] NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Runavailable] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
