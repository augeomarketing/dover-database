USE [ASB]
GO
/****** Object:  Table [dbo].[orphanwork]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[orphanwork]') AND type in (N'U'))
DROP TABLE [dbo].[orphanwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[orphanwork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[orphanwork](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[trancount] [int] NULL,
	[points] [numeric](18, 0) NULL,
	[description] [varchar](50) NULL,
	[secid] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [numeric](9, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
