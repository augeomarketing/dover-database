USE [ASB]
GO
/****** Object:  Table [dbo].[DDANAME]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DDANAME]') AND type in (N'U'))
DROP TABLE [dbo].[DDANAME]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DDANAME]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DDANAME](
	[DDANUM] [nvarchar](11) NULL,
	[NA1] [nvarchar](40) NULL,
	[NA2] [nvarchar](40) NULL,
	[NA3] [nvarchar](40) NULL,
	[NA4] [nvarchar](40) NULL,
	[NA5] [nvarchar](40) NULL,
	[NA6] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
