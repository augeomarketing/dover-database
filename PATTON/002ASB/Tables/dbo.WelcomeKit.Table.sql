USE [ASB]
GO
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 01/12/2010 08:13:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WelcomeKit]') AND type in (N'U'))
DROP TABLE [dbo].[WelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WelcomeKit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WelcomeKit](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[Type] [char](1) NULL,
	[DDA] [char](13) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
