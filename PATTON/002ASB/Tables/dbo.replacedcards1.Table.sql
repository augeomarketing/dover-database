USE [ASB]
GO
/****** Object:  Table [dbo].[replacedcards1]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[replacedcards1]') AND type in (N'U'))
DROP TABLE [dbo].[replacedcards1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[replacedcards1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[replacedcards1](
	[ACCTID] [nvarchar](255) NULL,
	[TIPNUMBER] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
