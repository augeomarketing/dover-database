USE [ASB]
GO
/****** Object:  Table [dbo].[ssnwrk1]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ssnwrk1]') AND type in (N'U'))
DROP TABLE [dbo].[ssnwrk1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ssnwrk1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ssnwrk1](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
