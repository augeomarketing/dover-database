USE [ASB]
GO
/****** Object:  Table [dbo].[CardPurgeReq062209]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CardPurgeReq062209]') AND type in (N'U'))
DROP TABLE [dbo].[CardPurgeReq062209]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CardPurgeReq062209]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CardPurgeReq062209](
	[tipnumber] [nvarchar](255) NULL,
	[acctname1] [nvarchar](255) NULL,
	[acctname2] [nvarchar](255) NULL,
	[acctid] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
