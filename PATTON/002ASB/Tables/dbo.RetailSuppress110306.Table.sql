USE [ASB]
GO
/****** Object:  Table [dbo].[RetailSuppress110306]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailSuppress110306]') AND type in (N'U'))
DROP TABLE [dbo].[RetailSuppress110306]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailSuppress110306]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RetailSuppress110306](
	[tipnumber] [nvarchar](255) NULL,
	[acctname1] [nvarchar](255) NULL,
	[acctname2] [nvarchar](255) NULL,
	[address1] [nvarchar](255) NULL,
	[city] [nvarchar](255) NULL,
	[zipcode] [nvarchar](255) NULL,
	[acctid] [nvarchar](255) NULL,
	[Agnt 9/30/06] [nvarchar](255) NULL,
	[CR 9/30/06] [nvarchar](255) NULL,
	[Card Not Found on TSYS] [nvarchar](255) NULL,
	[Non-TAP Agent] [nvarchar](255) NULL,
	[Closed CR Code] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
