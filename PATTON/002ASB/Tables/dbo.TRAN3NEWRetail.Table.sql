USE [ASB]
GO
/****** Object:  Table [dbo].[TRAN3NEWRetail]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRAN3NEWRetail]') AND type in (N'U'))
DROP TABLE [dbo].[TRAN3NEWRetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRAN3NEWRetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TRAN3NEWRetail](
	[TFNO] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANNUM] [nchar](4) NULL,
	[TRANAMT] [nchar](15) NULL,
	[TRANTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[CRDACTVLDT] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
