USE [ASB]
GO
/****** Object:  Table [dbo].[ASBIMP5Retail]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASBIMP5Retail]') AND type in (N'U'))
DROP TABLE [dbo].[ASBIMP5Retail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASBIMP5Retail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ASBIMP5Retail](
	[TFNO] [nvarchar](19) NULL,
	[BANK] [nvarchar](4) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[OLDCCNUM] [nvarchar](16) NULL,
	[NA1] [nvarchar](25) NULL,
	[NA2] [nvarchar](25) NULL,
	[STATUS] [nvarchar](14) NULL,
	[NA3] [nvarchar](32) NULL,
	[NA4] [nvarchar](32) NULL,
	[NA5] [nvarchar](32) NULL,
	[NA6] [nvarchar](32) NULL,
	[CITYSTATE] [nvarchar](29) NULL,
	[ZIP] [nvarchar](9) NULL,
	[N12] [nvarchar](32) NULL,
	[HOMEPHONE] [nvarchar](10) NULL,
	[WORKPHONE] [nvarchar](10) NULL,
	[NUMPURCH] [nvarchar](9) NULL,
	[AMTPURCH] [float] NULL,
	[NUMCR] [nvarchar](9) NULL,
	[AMTCR] [float] NULL,
	[STMTDATE] [nvarchar](14) NULL,
	[LASTNAME] [nvarchar](30) NULL,
	[oldpre] [char](8) NULL,
	[oldpost] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
