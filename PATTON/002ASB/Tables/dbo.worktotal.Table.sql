USE [ASB]
GO
/****** Object:  Table [dbo].[worktotal]    Script Date: 01/12/2010 08:13:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[worktotal]') AND type in (N'U'))
DROP TABLE [dbo].[worktotal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[worktotal]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[worktotal](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
