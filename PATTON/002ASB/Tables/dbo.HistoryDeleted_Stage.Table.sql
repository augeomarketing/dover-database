USE [ASB]
GO
/****** Object:  Table [dbo].[HistoryDeleted_Stage]    Script Date: 06/13/2010 11:41:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HistoryDeleted_Stage](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL,
	[datedeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted_Stage]') AND name = N'ix_HistoryDeleted_Stage_AcctID')
CREATE NONCLUSTERED INDEX [ix_HistoryDeleted_Stage_AcctID] ON [dbo].[HistoryDeleted_Stage] 
(
	[AcctID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
