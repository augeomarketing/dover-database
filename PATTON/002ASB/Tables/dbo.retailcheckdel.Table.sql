USE [ASB]
GO
/****** Object:  Table [dbo].[retailcheckdel]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[retailcheckdel]') AND type in (N'U'))
DROP TABLE [dbo].[retailcheckdel]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[retailcheckdel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[retailcheckdel](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
