USE [ASB]
GO
/****** Object:  Table [dbo].[DCIN2]    Script Date: 01/12/2010 08:14:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DCIN2]') AND type in (N'U'))
DROP TABLE [dbo].[DCIN2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DCIN2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DCIN2](
	[DEBIT_CARD_NUMBER] [nvarchar](25) NULL,
	[DDA_NUMBER] [nvarchar](15) NULL,
	[SSN_NUMBER] [nvarchar](9) NULL,
	[NAME_1] [nvarchar](40) NULL,
	[NAME_2] [nvarchar](40) NULL,
	[ADDRESS 1] [nvarchar](40) NULL,
	[ADDRESS 2] [nvarchar](40) NULL,
	[ADDRESS 3] [nvarchar](40) NULL,
	[ADDRESS 4] [nvarchar](40) NULL,
	[CITY] [nvarchar](40) NULL,
	[STATE] [nvarchar](2) NULL,
	[ZIP] [nvarchar](10) NULL,
	[HPHONE] [nvarchar](10) NULL,
	[BPHONE] [nvarchar](10) NULL,
	[TipFirst] [char](3) NULL,
	[Tipnumber] [char](15) NULL,
	[Na3] [char](40) NULL,
	[Na4] [char](40) NULL,
	[Na5] [char](40) NULL,
	[Na6] [char](10) NULL,
	[joint] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
