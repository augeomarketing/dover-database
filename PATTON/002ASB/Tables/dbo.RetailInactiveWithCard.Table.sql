USE [ASB]
GO
/****** Object:  Table [dbo].[RetailInactiveWithCard]    Script Date: 01/12/2010 08:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailInactiveWithCard]') AND type in (N'U'))
DROP TABLE [dbo].[RetailInactiveWithCard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RetailInactiveWithCard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RetailInactiveWithCard](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctid] [char](16) NOT NULL,
	[Segmentcode] [char](2) NULL,
	[Dateadded] [char](10) NULL,
	[histdate] [datetime] NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[Purchases] [int] NULL,
	[Bonuses] [int] NULL,
	[Runavailable] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
