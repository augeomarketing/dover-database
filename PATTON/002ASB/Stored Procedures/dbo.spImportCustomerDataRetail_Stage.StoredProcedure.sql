USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spImportCustomerDataRetail_Stage]    Script Date: 05/14/2010 14:03:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerDataRetail_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportCustomerDataRetail_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerDataRetail_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2010
-- Description:	THIS IS TO IMPORT CUSTOMER AND DATA TO STAGE
-- =============================================


/***********************************************************************/
/*  BY: S Blanchette                                                   */
/*  DATE: 6/10                                                         */
/*  REASON:  Added SSN to the update statement for the affiliat table  */
/*                                                                     */
/***********************************************************************/
/***********************************************************************/
/*  BY: S Blanchette                                                   */
/*  DATE: 6/10                                                         */
/*  REASON:  Added DDA to the update statement for the affiliat table  */
/*                                                                     */
/***********************************************************************/
/***********************************************************************/
/*  BY: S Blanchette                                                   */
/*  DATE: 8/10                                                         */
/*  REASON:  Condition INSERS WITH status of A                         */
/*  SCAN:    SEB003                                                    */
/***********************************************************************/
/***********************************************************************/
/*  BY: S Blanchette                                                   */
/*  DATE: 11/10                                                         */
/*  REASON:  deal with city state less then 4 positions                */
/*  SCAN:    SEB004                                                    */
/***********************************************************************/

CREATE PROCEDURE [dbo].[spImportCustomerDataRetail_Stage]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @NA1 char(40), @NA2 char(40), @ADDR1 char(40), @ADDR2 char(40), @ADDR3 char(40), @CityState char(40),
 @CityLength nchar(2), @CSSTAT char(2), @ZIP nchar(9), @CSHFON nchar(10), @CSBFON nchar(10), 
 @DMSTATUS char(1), @ACCTNO nchar(16), @PRODCT char(20), @CARDNO char(16), @NUMCREDIT nchar(6), @AMTCREDIT float(8), @NUMDEBIT nchar(6), @AMTDEBIT float(8), @Tipnumber varchar(15), @dateadded nchar(10), @lastname varchar(40),
 @NA3 varchar(40), @NA4 varchar(40), @NA5 varchar(40), @NA6 varchar(40), @proddesc varchar(50), @ssn char(9), @DDANUM char(11), @Ctype char(1), @StatusDesc varchar(50)

delete from asbwork.dbo.cardsin where tipnumber is null

update asbwork.dbo.cardsin
set citystate=replace(citystate,char(44), '''')

/******************************************/
/* start seb004                           */
/******************************************/
update ASBWork.dbo.cardsin
set CITYSTATE = CITYSTATE + ''....''
where LEN(rtrim(citystate))<''4''
/******************************************/
/* end seb004                           */
/******************************************/


/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING RNFILE TABLE                         */
/*                                                                            */
declare RNFILE_crsr cursor
for select TIPNUMBER, SSN, DDANUM, ACCTNUM, NA1, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, PERIOD, LASTNAME, Cardtype
from asbwork.dbo.cardsin
order by tipnumber, status desc
/*                                                                            */
open RNFILE_crsr
/*                                                                            */
fetch RNFILE_crsr into @Tipnumber, @ssn, @ddanum, @cardno, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CityState, @zip, @DMstatus, @CSHFON, @CSBFON, @NUMDEBIT, @AMTDEBIT, @NUMCREDIT, @AMTCREDIT, @dateadded, @lastname, @Ctype
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
	if @ctype=''C''
	Begin
		Set @prodct=''Credit''
	end
	else
		Begin
			set @prodct=''Debit''
		end
	set @proddesc=(select Accttypedesc from accttype where accttype=@prodct)
	set @StatusDesc=(select StatusDescription from Status where Status=@DMStatus)
 		if not exists (select tipnumber from customer_Stage where tipnumber=@tipnumber)
			begin
				if @DMstatus = ''A'' /* SEB003 */
				Begin /* SEB003 */
					INSERT INTO Customer_Stage (TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, Status, StatusDescription, HomePhone, WorkPhone, SegmentCode, DateAdded, Lastname, RunBalanceNew, RunAvaliableNew)
       				values(@tipnumber, ''0'', ''0'', ''0'', left(@tipnumber,3), right(@tipnumber,12), @NA1, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, (rtrim(@CityState) + '' '' + rtrim(@zip)), left(@citystate,(len(@citystate)-3)), right(rtrim(@citystate),2), @ZIP, ''A'', ''Active[A]'', @CSHFON, @CSBFON, @Ctype, @DateAdded, @lastname, ''0'', ''0'') 
                end /* SEB003 */
			end
 		else 
			begin
			update CUSTOMER_STAGE
			set AcctName1=@NA1, AcctName2=@NA2, AcctName3=@NA3, AcctName4=@NA4, AcctName5=@NA5, AcctName6=@NA6, Address1=@ADDR1, Address2=@ADDR2, Address4=(rtrim(@CityState) + '' '' + rtrim(@zip)), City=left(@citystate,(len(@citystate)-3)), State=right(rtrim(@citystate),2), Zipcode=@ZIP, HomePhone=@CSHFON, WorkPhone=@CSBFON, lastname= @lastname, status=@DMstatus, StatusDescription=@StatusDesc 
			where tipnumber=@tipnumber
			end	
/*   AFFILIAT TABLE                      */
/*                                       */
/* Will populate the following           */
/*  DDA#(Account#)  goes into CUSTID               */
/*  SSN goes into  SECID      */
/*  Card# goes into ACCTID    */
/*                                       */
		if not exists (select tipnumber from affiliat_Stage where acctid= @cardno)
			begin
				if @DMstatus = ''A'' /* SEB003 */
				Begin /* SEB003 */
					INSERT INTO affiliat_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, CustID)
       				values(@cardno, @tipnumber, @prodct, @dateadded, @ssn, ''A'', @proddesc, @lastname, @ddanum) 
				end /* SEB003 */
			end
 		else 
			begin
			update AFFILIAT_STAGE
			set lastname=@lastname, AcctStatus=@DMstatus, SECID = @ssn, CustID = @ddanum
			where tipnumber=@tipnumber and acctid=@cardno
			end	
				
goto Next_Record
Next_Record:
		fetch RNFILE_crsr into @Tipnumber, @ssn, @ddanum, @cardno, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6, @ADDR1, @ADDR2, @ADDR3, @CityState, @zip, @DMstatus, @CSHFON, @CSBFON, @NUMDEBIT, @AMTDEBIT, @NUMCREDIT, @AMTCREDIT, @dateadded, @lastname, @Ctype
end
Fetch_Error:
close  rnfile_crsr
deallocate  rnfile_crsr
END
' 
END
GO
