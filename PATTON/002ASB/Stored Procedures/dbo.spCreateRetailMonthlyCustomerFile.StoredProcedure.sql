USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spCreateRetailMonthlyCustomerFile]    Script Date: 01/12/2010 08:14:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateRetailMonthlyCustomerFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateRetailMonthlyCustomerFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateRetailMonthlyCustomerFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spCreateRetailMonthlyCustomerFile] @DateIn varchar(10)
AS

DELETE FROM ASBimp5Retail
where oldpre>=''39000000''

DELETE FROM ASBimp5Retail
/* WHERE    BANK in ( ''    '',''0302'',''0311'',''0611'',''0620'',''0621'',''0622'',''0623'') removed 2/20/06 */
where not exists(select * from ValidAgent where agent=asbimp5retail.bank)

DELETE FROM ASBIMP5Retail
WHERE    acct_num in ( ''4168640610938105'',''4168640610800537'',''4168640610831698'',''4168640610304894'',''4168640610912944'',''4168640610822531'',''4625270310010893'',''4168640610931340'',''4168640610514336'',''4168640610851696'',''4168640610880299'',''4168640610234653'',''4168646240006619'',''4168640610899455'')

update ASBIMP5Retail
set status = '' '' 
where status in ( ''AV'',''LG'',''NR'',''CC'',''PC'',''S2'',''T2'',''T3'',''V4'',''V5'',''V7'',''S '',''B5'')

update ASBIMP5Retail
set status = ''C'' 
where status in ( ''M9'',''V9'',''B9'',''FA'',''P9'',''Q9'',''R9'',''Y9'')

update ASBIMP5Retail
set status = ''L'' 
where status in ( ''F1'')

/* update ASBIMP5Retail
set status = ''C'' 
where BANK in ( ''    '',''0302'',''0311'',''0611'',''0620'',''0621'',''0622'',''0623'') REMOVED 2/20/06 */

DELETE FROM ASBIMP5Retail
WHERE    status=''L''
update ASBIMP5Retail
set oldpre=''  462527'' where oldpre=''38363836''
  
update ASBIMP5Retail
set oldpre=''  416864'' where oldpre=''38353835''
 
update ASBIMP5Retail
set oldpre=''       '' where oldpost=''0000000000''
  
update ASBIMP5Retail
set oldpost=''         '' where oldpost=''0000000000''

update ASBIMP5Retail
set oldccnum = substring(oldpre,3,6) + substring(oldpost, 1,10)

declare @DTE varchar(10)
set @DTE = @DateIn

update ASBIMP5Retail
set stmtdate= @DateIn
update ASBIMP5Retail
set status = ''A'' 
where status in (''  '')' 
END
GO
