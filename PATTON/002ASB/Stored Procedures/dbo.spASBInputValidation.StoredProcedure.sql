USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spASBInputValidation]    Script Date: 01/12/2010 08:14:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBInputValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spASBInputValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBInputValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spASBInputValidation]
AS

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO validate IMPORT data                                         */
/*                                                                            */
/******************************************************************************/

declare @TFNO nvarchar (19), @BANK nvarchar (4), @ACCT_NUM nvarchar (25), @OLDCCNUM nvarchar (16), @NA1 nvarchar (25), @NA2 nvarchar (25), @STATUS nvarchar (14), @NA3 nvarchar (32), @NA4 nvarchar (32), @NA5 nvarchar (32), @NA6 nvarchar (32), @CITYSTATE nvarchar (29), @ZIP nvarchar (9), @N12 nvarchar (32), @HOMEPHONE nvarchar (10), @WORKPHONE nvarchar (10), @NUMPURCH nvarchar (9), @AMTPURCH float, @NUMCR nvarchar (9), @AMTCR float, @STMTDATE nvarchar (14), @LASTNAME nvarchar (30), @oldpre char (8), @oldpost char (10) 

declare @errmsg varchar(50)

delete from Errorfile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING RNFILE TABLE                         */
/*                                                                            */
declare ASBIMP5Retail_crsr cursor
for select *
from ASBIMP5Retail
/*                                                                            */
open ASBIMP5Retail_crsr
/*                                                                            */
fetch ASBIMP5Retail_crsr into @TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @oldpre, @oldpost 

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL

		if @bank is null or len(rtrim(@bank)) = 0 or @bank = ''        ''
		begin
			set @errmsg=''Missing Bank Number''
			INSERT INTO ErrorFile
       			values(@TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @oldpre, @oldpost, @errmsg)
		goto Next_Record
		end

		if @ACCT_NUM is null or len(rtrim(@ACCT_NUM)) = 0 or @ACCT_NUM = ''                ''
		begin
			set @errmsg=''Missing Account Number''
			INSERT INTO ErrorFile
       			values(@TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @oldpre, @oldpost, @errmsg)
		goto Next_Record
		end

		if @NA1 is null or len(rtrim(@NA1)) = 0 
		begin
			set @errmsg=''Missing Name''
			INSERT INTO ErrorFile
       			values(@TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @oldpre, @oldpost, @errmsg)
		goto Next_Record
		end

		if @CityState is null or len(rtrim(@CityState)) = 0 
		begin
			set @errmsg=''Missing City State''
			INSERT INTO ErrorFile
       			values(@TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @oldpre, @oldpost, @errmsg)
		goto Next_Record
		end

goto Next_Record

Next_Record:
		fetch ASBIMP5Retail_crsr into @TFNO, @BANK, @ACCT_NUM, @OLDCCNUM, @NA1, @NA2, @STATUS, @NA3, @NA4, @NA5, @NA6, @CITYSTATE, @ZIP, @N12, @HOMEPHONE, @WORKPHONE, @NUMPURCH, @AMTPURCH, @NUMCR, @AMTCR, @STMTDATE, @LASTNAME, @oldpre, @oldpost 


	end

Fetch_Error:
close  ASBIMP5Retail_crsr
deallocate  ASBIMP5Retail_crsr' 
END
GO
