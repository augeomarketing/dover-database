USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[pQuarterlyAuditValidation]    Script Date: 01/12/2010 08:14:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pQuarterlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pQuarterlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @errmsg varchar(50) 

set @errmsg=''Ending Balances do not match''

delete from Quarterly_Audit_ErrorFile

INSERT INTO Quarterly_Audit_ErrorFile
select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsbonusMN, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
from Quarterly_Statement_File a, 	Current_Month_Activity b
where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints' 
END
GO
