USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spProcessExpiredPoints]    Script Date: 01/12/2010 08:14:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spProcessExpiredPoints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spProcessExpiredPoints]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spProcessExpiredPoints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/******************************************************************************/
/*    This will Process the Expired Points for ASB                    */
/* */
/*   - Read ExpiredPoints  */
/*  - Update CUSTOMER      */
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 3/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spProcessExpiredPoints] @MonthEndDate nvarchar(12)AS      

/* input */
--declare @MonthEndDate nvarchar(12)
--set @MonthEndDate = ''12/31/2007''
Declare @TipNumber varchar(15)
Declare @CustomerFound char(1)
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(10)
Declare @OVERAGE numeric(9)
declare @DateOfExpire NVARCHAR(10)
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
Declare @PREVEXPIRED int
Declare @TRANDESC nvarchar(50)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare XP_crsr cursor
for Select *
From EXPIREDPOINTS

Open XP_crsr
/*                  */



Fetch XP_crsr  
into  	@TipNumber, @PointsEarned, @Pointsredeemed, @PointsToExpire, @PREVEXPIRED,  @DateOfExpire 


IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @CustomerFound = '' ''
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		




/*                                                                            */

while @@FETCH_STATUS = 0
BEGIN

	if @PointsToExpire < ''1''
	   goto Fetch_Next

		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		  ,@CustomerFound = ''Y''
		From
		   CUSTOMER
		Where
		   TIPNUMBER = @TipNumber


	IF @CustomerFound = ''Y''
	BEGIN
/*  UPDATE THE CUSTOMER RECORD WITH THE EXPIRING POINTS TRANSACTION DATA          */
		IF @PointsToExpire > ''0''
		Begin	
		Update Customer
		Set 
		   RunAvailable = @RunAvailable - @PointsToExpire
		   ,RUNBALANCE = @RUNBALANCE - @PointsToExpire	  	           
		Where @TipNumber = Customer.TIPNUMBER
		END

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From rewardsnow.dbo.TranType
		where
		   TranCode = ''XP''

/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR EXPIRED POINTS     			   */

		set @POINTS = ''0''

		
		IF @PointsToExpire > ''0''	        
		Begin	
	           set @POINTS = @PointsToExpire
		   Insert into HISTORY
		   (
		      TIPNUMBER	
		      ,HISTDATE
		      ,TRANCODE
		      ,TRANCOUNT	      
		      ,POINTS
		      ,Ratio
		      ,Description		      
		   )
		   Values
		   (
		    @TipNumber
	            ,@MonthEndDate 
	            ,''XP''
	           ,''1''
	            ,@POINTS
		    ,''-1''
		    ,@TRANDESC          
	            )
	         END
	END	        


	set @CustomerFound = '' ''
	        

FETCH_NEXT:
	
	Fetch XP_crsr  
	into  	@TipNumber, @PointsEarned, @Pointsredeemed, @PointsToExpire, @PREVEXPIRED, @DateOfExpire 
END /*while */


	 
GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:
close  XP_crsr
deallocate  XP_crsr' 
END
GO
