USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spASBImportTransactionData]    Script Date: 01/12/2010 08:14:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBImportTransactionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spASBImportTransactionData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spASBImportTransactionData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spASBImportTransactionData] @DateIn varchar(10)
AS 

Delete from transum

insert into transum (tipnumber, acctno, NUMPURCH, AMTPURCH, NUMCR, AMTCR)
select tfno, acct_num, NUMPURCH, AMTPURCH, NUMCR, AMTCR
from ASBIMP5RETAIL

/*********************************************************************/
/*  purchases                                                        */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''63'', description=''Credit Card Purchase'', histdate=@datein
where amtpurch>0

insert into history
select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
from transum
where trancode=''63''

/*********************************************************************/
/*  returns                                                          */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''33'', description=''Credit Card Purchase Returns'', histdate=@datein
where amtcr>0

insert into history
select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
from transum
where trancode=''33''

update customer
set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber=customer.tipnumber), runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber=customer.tipnumber)' 
END
GO
