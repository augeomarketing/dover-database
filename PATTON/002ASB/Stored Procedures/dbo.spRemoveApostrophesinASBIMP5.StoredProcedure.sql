USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveApostrophesinASBIMP5]    Script Date: 01/12/2010 08:14:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveApostrophesinASBIMP5]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveApostrophesinASBIMP5]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveApostrophesinASBIMP5]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRemoveApostrophesinASBIMP5] AS
update customer
set acctname1=replace(acctname1,char(39), '' ''),acctname2=replace(acctname2,char(39), '' ''), lastname=replace(lastname,char(39), '' ''), acctname3=replace(acctname3,char(39), '' ''), acctname4=replace(acctname4,char(39), '' ''), acctname5=replace(acctname5,char(39), '' ''), acctname6=replace(acctname6,char(39), '' ''),city=replace(city,char(39), '' '')
' 
END
GO
