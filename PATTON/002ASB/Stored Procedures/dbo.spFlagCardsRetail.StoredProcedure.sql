USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[spFlagCardsRetail]    Script Date: 12/17/2010 14:27:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlagCardsRetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFlagCardsRetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFlagCardsRetail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 12/2010
-- Description:	Flag cards that are not found in the Cards in file
-- =============================================
CREATE PROCEDURE [dbo].[spFlagCardsRetail] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	drop table dbo.UnmatchedCards
	
	select a.tipnumber, a.acctid, a.custid, B.acctname1
	into dbo.UnmatchedCards
	from AFFILIAT_STAGE a join CUSTOMER_STAGE b on a.TIPNUMBER = b.TIPNUMBER
	where a.ACCTID not in (select acctnum from ASBWork.dbo.CARDSIN)
	
	update dbo.AFFILIAT_STAGE
	set AcctStatus = ''C''
	where ACCTID in (select ACCTID from dbo.UnmatchedCards)
	
END
' 
END
GO
