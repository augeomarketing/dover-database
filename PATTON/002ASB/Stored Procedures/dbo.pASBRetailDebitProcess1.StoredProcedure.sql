USE [ASB]
GO
/****** Object:  StoredProcedure [dbo].[pASBRetailDebitProcess1]    Script Date: 01/12/2010 08:14:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBRetailDebitProcess1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pASBRetailDebitProcess1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pASBRetailDebitProcess1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[pASBRetailDebitProcess1]
as  

/*           Set Tip First                                                       */
update DCIn
set tipfirst=''002''
where left(DEBIT_CARD_NUMBER,6) in (''414579'', ''414580'') 

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/


delete from DCIN
where DDA_NUMBER is Null or DDA_NUMBER=''00000000000''

/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
declare @DDANUM nchar(11), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @ADDR1 nvarchar(40)

delete from ddaname

declare debitcardin_crsr cursor
for select DDA_NUMBER, [NAME_1], [NAME_2]
from DCIN
/* where ddanum is not null */
/* for update */ 
/*                                                                            */
open debitcardin_crsr
/*                                                                            */
fetch debitcardin_crsr into @DDANUM, @NA1, @NA2
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from ddaname where DDANUM=@ddanum)
	begin
		insert ddaname(ddanum, na1, na2) values(@ddanum, @na1, @na2)
		goto Next_Record1
	end
	
	else
		if exists(select * from ddaname where ddanum=@ddanum and substring(na2,1,1) like '' '')
		begin
			update ddaname
			set na2=@na1, na3=@na2
			where ddanum=@ddanum
			goto Next_Record1
		end
		else
			if exists(select * from ddaname where ddanum=@ddanum and substring(na3,1,1) like '' '')
			begin
				update ddaname
				set na3=@na1, na4=@na2
				where ddanum=@ddanum
				goto Next_Record1
			end
			else
				if exists(select * from ddaname where ddanum=@ddanum and substring(na4,1,1) like '' '')
				begin
					update ddaname
					set na4=@na1, na5=@na2
					where ddanum=@ddanum
					goto Next_Record1
				end
				else
					if exists(select * from ddaname where ddanum=@ddanum and substring(na5,1,1) like '' '')
					begin
						update ddaname
						set na5=@na1, na6=@na2
						where ddanum=@ddanum
						goto Next_Record1
					end
					else
						if exists(select * from ddaname where ddanum=@ddanum and substring(na6,1,1) like '' '')
						begin
							update ddaname
							set na6=@na1
							where ddanum=@ddanum
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch debitcardin_crsr into @DDANUM, @NA1, @NA2
end

Fetch_Error1:
close debitcardin_crsr
deallocate debitcardin_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update ddaname
set na2=null
where na2=na1

update ddaname
set na3=null
where na3=na1 or na3=na2

update ddaname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update ddaname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update ddaname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update ddaname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or substring(na1,1,1) like '' ''

	update ddaname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or substring(na2,1,1) like '' ''

	update ddaname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or substring(na3,1,1) like '' ''

	update ddaname
	set na4=na5, na5=na6, na6=null
	where na4 is null or substring(na4,1,1) like '' ''

	update ddaname
	set na5=na6, na6=null
	where na5 is null or substring(na5,1,1) like '' ''

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names into debitcardin table                           */
/******************************************************************************/

update DCIN
set DCIN.[NAME_1]=ddaname.na1, DCIN.[NAME_2]=ddaname.na2, DCIN.na3=ddaname.na3, DCIN.na4=ddaname.na4, DCIN.na5=ddaname.na5, DCIN.na6=ddaname.na6 
from DCIN, ddaname
where DCIN.dda_NUMBER=ddaname.ddanum


/*                                                                                 */
/* End of New code to replace cursor processing for rolling acct demographic data  */
/*                                                                                 */


/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update DCIN
set joint=''J''
where [NAME_2] is not null and substring([NAME_2],1,1) not like '' ''

update DCIN
set joint=''S''
where [NAME_2] is null or substring([NAME_2],1,1) like '' ''' 
END
GO
