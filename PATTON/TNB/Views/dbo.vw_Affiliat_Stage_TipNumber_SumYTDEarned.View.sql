USE [NewTNB]
GO
/****** Object:  View [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]    Script Date: 02/09/2010 08:19:18 ******/
DROP VIEW [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]

as


SELECT     TIPNUMBER, ACCTID, SUM(YTDEarned) AS YTDEarned
FROM         dbo.Affiliat_Stage
GROUP BY TIPNUMBER, ACCTID
GO
