USE [NewTNB]
GO
/****** Object:  View [dbo].[vw_histpoints]    Script Date: 02/09/2010 08:19:18 ******/
DROP VIEW [dbo].[vw_histpoints]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create view [dbo].[vw_histpoints] as 
		select tipnumber, sum(points * ratio) as points 
	from dbo.history_stage 
	where secid = 'NEW' group by tipnumber
GO
