declare @DBName		nvarchar(100)
declare @SQL			nvarchar(4000)
Declare @filenumber		int
declare @GUID			uniqueidentifier

set @GUID = newid()

declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbpi.dbnamenexl = 'newtnb' 
	order by dbnamepatton

open csrDB

fetch next from csrDB into @DBName

while @@FETCH_STATUS = 0
BEGIN

	set @SQL = '
	exec master.dbo.xp_restore_database @database = ' + @DBName + ',
	@filename =  ' + char(39) + 'C:\PHB TNB Backups\' + replace(replace(@DBName, '[', ''), ']', '') + '_BeforePostToProd.bak' + char(39) + ', 
	@filenumber = 1,
	@with = N''REPLACE'',
	@with = N''STATS = 10'',
	@affinity = 0,
	@logging = 0'


	print 'Begin RESTORE up database: ' + @dbname
	exec sp_executesql @sql
	print 'Done RESTORING up database: ' + @dbname
	print ' '
	print ' '


	fetch next from csrDB into @DBName
END

close csrDB
deallocate csrDB