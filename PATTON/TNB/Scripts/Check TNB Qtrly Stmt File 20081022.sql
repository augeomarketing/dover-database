
declare @SQL nvarchar(4000)
declare @dbname varchar(50)
declare @tipfirst varchar(3)

declare @begbal		int

/*

-- Get exclusion list of tips with e-statements
drop table #estatements
create table #EStatements
	(tipnumber		varchar(15) primary key)

insert into #EStatements
select tipnumber
from [167395-DB1].newtnb.dbo.[1Security]
where isnull(emailstatement, '') != 'Y'

*/

-- Build temp tables
drop table r2uwork.dbo.phb_qsf
drop table r2uwork.dbo.phb_hist

create table r2uwork.dbo.phb_qsf
(	[Tipnumber] [varchar](15) NOT NULL primary key,
	[PointsBegin] [numeric](18, 0),
	[PointsEnd] [numeric](18, 0),
	[PointsPurchased] [numeric](18, 0) ,
	[PointsBonus] [numeric](18, 0) ,
	[PointsAdded] [numeric](18, 0) ,
	[PointsIncreased] [numeric](18, 0),
	[PointsRedeemed] [numeric](18, 0) ,
	[PointsReturned] [numeric](18, 0) ,
	[PointsSubtracted] [numeric](18, 0),
	[PointsDecreased] [numeric](18, 0) ,
	expire [numeric](18, 0)
)


create table r2uwork.dbo.phb_hist
(tipnumber			varchar(15) ,
 BeginningBalance	numeric(18,0),
 TotalPoints		numeric(18,0)
)




-- Loop through quarterly statement file
declare csrdb cursor FAST_FORWARD for
	select distinct left(qsf.tipnumber,3) tipfirst, quotename(dbnamepatton) as dbnamepatton
	from r2uwork.dbo.quarterly_statement_file qsf join rewardsnow.dbo.dbprocessinfo dbp
		on left(qsf.tipnumber,3) = dbp.dbnumber
	join r2uwork.dbo.R2U_Expire_PHB new
		on qsf.tipnumber = new.tipnumber
--	where left(qsf.tipnumber,3) = '158'

open csrdb

fetch next from csrdb into @TipFirst, @dbname

while @@FETCH_STATUS = 0
BEGIN

	print 'Processing Database: ' + @tipfirst + ':  ' + @dbname

	truncate table r2uwork.dbo.phb_qsf
	truncate table r2uwork.dbo.phb_hist

	-- total of statement file by tipfirst
	insert into r2uwork.dbo.phb_qsf
	select qsf.tipnumber, --left(qsf.tipnumber,3) tipfirst, 
			sum(pointsbegin) pointsbegin, sum(pointsend) pointsend, sum(pointspurchased) pointspurchased,
			sum(pointsbonus) pointsbonus, sum(pointsadded) pointsadded, sum(pointsincreased) pointsincreased,	
			sum(pointsredeemed) pointsredeemed, sum(pointsreturned) pointsreturned, sum(pointssubtracted) pointssubtracted,
			sum(pointsdecreased) pointsdecreased, sum(new.pointstoexpire) expire
	from r2uwork.dbo.quarterly_statement_file qsf join r2uwork.dbo.R2U_Expire_PHB new
	on qsf.tipnumber = new.tipnumber

	where left(qsf.tipnumber,3) = @TipFirst
	group by qsf.tipnumber
			--left(qsf.tipnumber,3)

	-- total beginning balance table, with history
	set @sql = 
	'insert into r2uwork.dbo.phb_hist
	select bbt.tipnumber, --left(bbt.tipnumber,3) tipfirst, 
			monthbeg2 monthbeg2 , sum(points * ratio) as TotalPoints	
	from r2uwork.dbo.beginning_balance_table bbt  join ' + @dbname + '.dbo.history his
		on bbt.tipnumber = his.tipnumber

	join r2uwork.dbo.R2U_Expire_PHB new
		on bbt.tipnumber = new.tipnumber

	where left(bbt.tipnumber,3) = ' + char(39) + @tipfirst + char(39) + ' and histdate between ''2008-02-01 00:00:00'' and ''2008-09-30 23:59:59'' 
	group by bbt.tipnumber --left(bbt.tipnumber,3)
			,monthbeg2'

	exec sp_executesql @sql


	--
	-- Now diff the tables
	--
	if exists(select 1
			from r2uwork.dbo.phb_qsf qsf join r2uwork.dbo.phb_hist hst
				on qsf.tipnumber = hst.tipnumber
			where (qsf.pointsbegin != hst.beginningbalance)  OR
				 (qsf.pointsend != (hst.beginningbalance + totalpoints) ) )
		select *
			from r2uwork.dbo.phb_qsf qsf join r2uwork.dbo.phb_hist hst
				on qsf.tipnumber = hst.tipnumber
			where (qsf.pointsbegin != hst.beginningbalance)  OR
				 (qsf.pointsend != (hst.beginningbalance + totalpoints) )
	else
		print '	---  Tip: ' + @tipfirst + ':  ' + @dbname + ' is OK.'

	print 'Completed Database: ' + @tipfirst + ':  ' + @dbname
	print ' '
	print ' '
	print ' '
	fetch next from csrdb into @TipFirst, @dbname
END

close csrdb

deallocate csrdb

/*
Processing Database: 158:  [158ValleyStone]

(529 row(s) affected)

(490 row(s) affected)
	---  Tip: 158:  [158ValleyStone] is OK.
Completed Database: 158:  [158ValleyStone]
 
 
 
Processing Database: 154:  [154Engraving]

(17 row(s) affected)

(16 row(s) affected)
	---  Tip: 154:  [154Engraving] is OK.
Completed Database: 154:  [154Engraving]
 
 
 
Processing Database: 140:  [140VisionOne]

(98 row(s) affected)

(79 row(s) affected)
	---  Tip: 140:  [140VisionOne] is OK.
Completed Database: 140:  [140VisionOne]
 
 
 
Processing Database: 159:  [159Adirondack]

(681 row(s) affected)

(681 row(s) affected)
	---  Tip: 159:  [159Adirondack] is OK.
Completed Database: 159:  [159Adirondack]
 
 
 
Processing Database: 155:  [155Laramie]

(204 row(s) affected)

(147 row(s) affected)
	---  Tip: 155:  [155Laramie] is OK.
Completed Database: 155:  [155Laramie]
 
 
 
Processing Database: 128:  [128Banner]

(476 row(s) affected)

(409 row(s) affected)
	---  Tip: 128:  [128Banner] is OK.
Completed Database: 128:  [128Banner]
 
 
 
Processing Database: 157:  [157Grafton]

(726 row(s) affected)

(607 row(s) affected)
	---  Tip: 157:  [157Grafton] is OK.
Completed Database: 157:  [157Grafton]
 
 
 
Processing Database: 121:  [121Numark]

(2759 row(s) affected)

(2267 row(s) affected)
	---  Tip: 121:  [121Numark] is OK.
Completed Database: 121:  [121Numark]
 
 
 
Processing Database: 185:  [185Parda]

(2355 row(s) affected)

(2355 row(s) affected)
	---  Tip: 185:  [185Parda] is OK.
Completed Database: 185:  [185Parda]
 
 
 
Processing Database: 149:  [149Breco]

(442 row(s) affected)

(365 row(s) affected)
	---  Tip: 149:  [149Breco] is OK.
Completed Database: 149:  [149Breco]
 
 
 
Processing Database: 182:  [182Independant]

(33 row(s) affected)

(33 row(s) affected)
	---  Tip: 182:  [182Independant] is OK.
Completed Database: 182:  [182Independant]
 
 
 
Processing Database: 152:  [152GoodShepherd]

(237 row(s) affected)

(208 row(s) affected)
	---  Tip: 152:  [152GoodShepherd] is OK.
Completed Database: 152:  [152GoodShepherd]
 
 
 
Processing Database: 184:  [184TowerCU]

(396 row(s) affected)

(396 row(s) affected)
	---  Tip: 184:  [184TowerCU] is OK.
Completed Database: 184:  [184TowerCU]
 
 
 
Processing Database: 124:  [124BestOfIowa]

(164 row(s) affected)

(141 row(s) affected)
	---  Tip: 124:  [124BestOfIowa] is OK.
Completed Database: 124:  [124BestOfIowa]
 
 
 
Processing Database: 183:  [183HawaiiPacific]

(370 row(s) affected)

(370 row(s) affected)
	---  Tip: 183:  [183HawaiiPacific] is OK.
Completed Database: 183:  [183HawaiiPacific]
 
 
 
Processing Database: 153:  [153Island]

(2784 row(s) affected)

(2370 row(s) affected)
	---  Tip: 153:  [153Island] is OK.
Completed Database: 153:  [153Island]
 
 
 
Processing Database: 141:  [141MissouriCU]

(1050 row(s) affected)

(806 row(s) affected)
	---  Tip: 141:  [141MissouriCU] is OK.
Completed Database: 141:  [141MissouriCU]
 
 
 
Processing Database: 143:  [143FAAERFCU]

(1964 row(s) affected)

(1344 row(s) affected)
	---  Tip: 143:  [143FAAERFCU] is OK.
Completed Database: 143:  [143FAAERFCU]
 
 
 
Processing Database: 127:  [127WindsorLocks]

(146 row(s) affected)

(125 row(s) affected)
	---  Tip: 127:  [127WindsorLocks] is OK.
Completed Database: 127:  [127WindsorLocks]
 
 
 
Processing Database: 147:  [147Westminster]

(183 row(s) affected)

(163 row(s) affected)
	---  Tip: 147:  [147Westminster] is OK.
Completed Database: 147:  [147Westminster]
 
 
 
Processing Database: 148:  [148Citizens]

(1340 row(s) affected)

(1112 row(s) affected)
	---  Tip: 148:  [148Citizens] is OK.
Completed Database: 148:  [148Citizens]
 
 
 
*/