declare @histdate		datetime
set @histdate = getdate()

drop table #reversexp

create table #reverseXP
	(tipnumber		varchar(15) primary key,
	 XPPoints		int)

-- add XP transaction tips and amounts to temp table
insert into #reversexp
select tipnumber, sum(points) points
from [114Centex].dbo.history
where trancode = 'XP' and histdate >= '10/01/2009'
group by tipnumber


select * from #reversexp


/*  ADD ROWS TO HISTORY */
insert into [114Centex].dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, Ratio)
select tipnumber, null, @histdate, 'XF', 1, XPPoints, tt.description, tt.ratio
from #reversexp tmp join rewardsnow.dbo.trantype tt
	on 'XF' = tt.trancode


/* UPDATE Customer balances */
update cus
	set runavailable = runavailable + xppoints,
		runbalance = runbalance + xppoints
from #reversexp tmp join [114Centex].dbo.customer cus
	on tmp.tipnumber = cus.tipnumber



--/** Update web customer */
update web
	set earnedbalance = earnedbalance + xppoints,
		availablebal = availablebal + xppoints
from #reversexp tmp join rn1.newtnb.dbo.customer web
	on tmp.tipnumber = web.tipnumber




----------

select *
from #reversexp


declare @tip varchar(15)

set @tip = '114000000001275 '

select *
from history
where tipnumber= @tip

select sum(points * ratio) from history where tipnumber = @tip

select tipnumber, runavailable, runredeemed, runbalance, * from customer where tipnumber = @tip








select *
from rewardsnow.dbo.dbprocessinfo where dbnumber = '148'



