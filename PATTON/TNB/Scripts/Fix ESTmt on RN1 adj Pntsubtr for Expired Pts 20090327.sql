select PNTSUBTR + ( pntdecrs -  (  pntredem + pntretrn + pntsubtr)), *
from statement
where travnum = '120000000002725'



--update statement
--	set PNTSUBTR = PNTSUBTR + ( pntdecrs +  (  pntredem + pntretrn + pntsubtr))

drop table #check

select travnum, PNTSUBTR + ( pntdecrs -  (  pntredem + pntretrn + pntsubtr)) pntsubtr, pntredem, pntretrn, pntdecrs
into #check
from statement


select *
from #check chk left outer join dbo.tipstoexclude tte
	on left(travnum,3) = tte.tipfirst
where tte.tipfirst is null
and pntsubtr + pntredem + pntretrn != pntdecrs


select * from #check
where pntsubtr != 0



begin tran

update stm
	set pntsubtr = PNTSUBTR + ( pntdecrs -  (  pntredem + pntretrn + pntsubtr))
from dbo.statement stm left outer join dbo.tipstoexclude tte
	on left(stm.travnum,3) = tte.tipfirst
where tte.tipfirst is null

rollback tran