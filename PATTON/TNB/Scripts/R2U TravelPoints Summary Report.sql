--- R2UTP ---

SELECT 
LEFT(Tipnumber, 3) AS inst,
clientname as 'FI Name',
datename(mm, LEFT(GETDATE() - DAY(GETDATE()), 12))+' '+ datename(yy, LEFT(GETDATE() - DAY(GETDATE()), 12)) AS 'Report Date', 
COUNT(LEFT(Tipnumber, 3)) AS 'Participating Customers', 
(select count(Tipnumber) from newtnb.dbo.monthly_statement_file a where PointsEnd >= 750 and left(newtnb.dbo.monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as 'Eligible Customer Count',
(select count(Tipnumber) from newtnb.dbo.monthly_statement_file a where PointsEnd < 750 and left(newtnb.dbo.monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as 'Ineligible Customer Count',
cast(SUM(PointsEnd) as int) AS 'Points Outstanding',
cast((select sum((floor(PointsEnd/750))*750) from newtnb.dbo.monthly_statement_file a where PointsEnd >= 750 and left(newtnb.dbo.monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as int) as 'Points Available to Redeem',
cast((select avg((floor(PointsEnd/750))*750) from newtnb.dbo.monthly_statement_file a where PointsEnd >= 750 and left(newtnb.dbo.monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as int) as 'Average Points per Eligible Customer',
cast(avg(PointsEnd) as int) as 'Average Points per Part. Customer',
cast(SUM(PointsPurchasedCR+PointsPurchasedDB) as int) AS 'Points Added for Purchases', 
cast(SUM(PointsBonus) as int) AS 'Points Added for Bonus',
cast(sum(PointsAdded) as int) as 'Points Added/ Misc', 
cast(SUM(PointsIncreased) as int) AS 'Total Points Added', 
cast(SUM(PointsRedeemed) as int) AS 'Points Subtracted for Redeemed', 
cast(SUM(PointsReturnedCR+PointsReturnedDB) as int) AS 'Points Subtracted for Returns', 
cast(sum(PointsSubtracted) as int) as 'Points Subtracted/ Misc', 
cast(SUM(PointsDecreased) as int) AS 'Total Points Subtracted'
FROM newtnb.dbo.monthly_statement_file, login.dbo.client
where (left(Tipnumber,3) in ('115', '112') or left(Tipnumber,3) like '12%' or left(Tipnumber,3) like '14%' or left(Tipnumber,3) like '18%' or left(Tipnumber,3) like '15%') and left(Tipnumber,3) not in ('120', '150') and login.dbo.client.tipfirst = left(Tipnumber,3)
GROUP BY LEFT(Tipnumber, 3), clientname
ORDER BY LEFT(Tipnumber, 3)

