select month(histdate), sum(points * ratio)
from history
where histdate >= '03/01/2009' and (trancode like '6%' or trancode like '3%' or trancode = 'DE')
group by month(histdate)
order by month(histdate)

228445
228445

-------


select *
from history
where histdate = '03/31/2009'

select *
from history
where trancode like 'r%'
and histdate >= '05/22/2009'


select sum(points * ratio)
from history
where histdate = '04/30/2009'
and trancode != 'xp'




select sum(points * ratio)
from history
where histdate = '05/31/2009'
and trancode != 'xp'



select *
from history
where histdate = '04/30/2009'


-- 102bcm appears to have history doubled up in april


if exists(
    select tipnumber, histdate, acctid, trancode, points
    from dbo.history
    where histdate = '04/30/2009'
    group by tipnumber, histdate, trancode, points
    having count(*) > 1)

    select 'FOUND'


--
-- Need to find out when/how to date this
--    Do it in June, then the statements will be overstated but Liabilitys OK.  
--    date in May, statements OK, will need to re-run the liability report

-- WHY/How did this happen?
-- Error in processing during posting to production files is the cause.  but i did do a restore.  why didn't
-- 102bcm not restore?

drop table #temp

select tipnumber, acctid, histdate, trancode, points, ratio
into #temp
    from dbo.history
    where histdate = '04/30/2009'
    group by tipnumber, acctid, histdate, trancode, points, ratio
    having count(*) > 1

select tipnumber, sum(points * ratio) totpts
into #totpoints
from #temp
group by tipnumber

select sum(totpts)
from #totpoints



--
-- Add rows into History to fix double posting
insert into [102bcm].dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio)
select distinct t.tipnumber, acctid, '05/31/2009', 'DE', 1, totpts, 'Correct April Double Posting', 'OLD', -1
from #temp t join #totpoints tp
    on t.tipnumber = tp.tipnumber
order by t.tipnumber

--
-- Need to back out the #s in the affiliat table ytdearned
--begin tran

select aff.*
from #temp t join dbo.affiliat aff
    on t.tipnumber = aff.tipnumber
    and t.acctid = aff.acctid
order by aff.tipnumber

select sum(ytdearned) from affiliat

update aff
    set ytdearned = ytdearned - (points * ratio)
from #temp t join dbo.affiliat aff
    on t.tipnumber = aff.tipnumber
    and t.acctid = aff.acctid


select sum(ytdearned) from affiliat

select aff.*
from #temp t join dbo.affiliat aff
    on t.tipnumber = aff.tipnumber
    and t.acctid = aff.acctid
order by aff.tipnumber

rollback tran

--
-- Need to fix the customer table runbalance, runavailable

begin tran

select sum(runavailable) runavailable, sum(runbalance) runbalance from customer

update cus
    set runavailable = runavailable - totpts,
	   runbalance = runbalance - totpts
from #totpoints t join customer cus
    on t.tipnumber = cus.tipnumber


select sum(runavailable) runavailable, sum(runbalance) runbalance from customer

rollback tran


select *
from #temp



select tipnumber, sum(points*ratio) pts
into #tipcheck 
from history
group by tipnumber


select *
from customer c join #tipcheck tc
    on c.tipnumber = tc.tipnumber
where runavailable != pts


select tipnumber, acctid, sum(points*ratio) pts
into #ytd
from history
where histdate >= '01/01/2009'
and (trancode like '3%' or trancode like '6%')
group by tipnumber, acctid

update aff
    set ytdearned = pts
from #ytd ytd join affiliat aff
on ytd.tipnumber = aff.tipnumber
and ytd.acctid = aff.acctid



select *
from affiliat
order by tipnumber

select *
from #ytd
order by tipnumber




select sum(points*ratio)
from history
where histdate < '06/01/2009' and histdate >= '03/01/2009'
and trancode = 'xp'






select sum(totpts)
from #totpoints



select * from #totpoints


select 'BEFORE', *
from rn1.newtnb.dbo.statement


update stmt
    set pntend = pntend - totpts,
	   pntsubtr = pntsubtr + totpts,
	   pntdecrs = pntdecrs + totpts
from #totpoints tp join rn1.newtnb.dbo.statement stmt
    on tp.tipnumber = stmt.travnum

select 'AFTER', *
from rn1.newtnb.dbo.statement
where left(travnum,3) = '102'


select * from #totpoints





select *
from history
where histdate >= '03/01/2009'
and trancode in ('de', 'xp')
order by tipnumber, histdate