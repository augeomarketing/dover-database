
select * from impcustomer2

declare @last_sid			int
declare @current_sid		int

declare @tipfirst			varchar(3)
declare @tipnumber			varchar(15)

set @last_sid = 0


select top 1 @current_sid = sid_impcustomer2_id, @tipfirst = tipprefix
	from dbo.impcustomer2
	where tipnumber is null and sid_impcustomer2_id > @last_sid

while @last_sid < @current_sid
BEGIN

	set @tipnumber = (select tipfirst + right('000000000000' + cast(cast( right(LastTipnumberUsed,12) as bigint) + 1 as varchar(12)), 12) 
					from dbo.client where tipfirst = @tipfirst) 

	update dbo.client set lasttipnumberused = @tipnumber where tipfirst = @tipfirst

	update dbo.impcustomer2 set tipnumber = @tipnumber where sid_impcustomer2_id = @current_sid

--print cast(@current_sid as varchar(10)) + '  |  ' + isnull(cast(@last_sid as varchar(10)), 'Missing LastSid') + '  |  ' + isnull(@tipfirst, 'Missing TipFirst')

	set @last_sid = @current_sid

	select top 1 @current_sid = sid_impcustomer2_id, @tipfirst = tipprefix
		from dbo.impcustomer2
		where tipnumber is null and sid_impcustomer2_id > @last_sid
END



select *
from impcustomer2
where tipnumber like '179%'

------------------------------------------------

declare @enddate datetime
declare @Tip		varchar(15)

set @enddate = '10/31/2008'

declare csrInsert cursor FAST_FORWARD for	
	select distinct imp.tipnumber
	from dbo.impcustomer2 imp left outer join dbo.customer_stage stg
		on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null

open csrInsert

fetch next from csrInsert into @Tip
while @@FETCH_STATUS = 0
BEGIN

	insert into dbo.Customer_Stage
	(tipnumber, tipfirst, tiplast, lastname, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6,
	 address1, address2, address4, city, state, zipcode, homephone, workphone, misc2, misc1, status, dateadded, 
	 runavailable, runbalance, runredeemed, runavaliableNew)
	select top 1 tipnumber, tipprefix, right(tipnumber,12), lastname, primarynm, secondarynm, acctname3, acctname4,
			   acctname5, acctname6, addressline1, addressline2,
			   left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.StateCd)) + ' ' + ltrim(rtrim(imp.Zip)), 40) as AddressLine4,
			   city, statecd, zip, phone1, phone2, right(ltrim(rtrim(imp.CCAcctNbr)), 6) as misc2, right(ltrim(rtrim(PrimarySSN)), 4) as misc1, 
			   rewardsnowsts, @EndDate, 0, 0, 0, 0
	from dbo.impcustomer2 imp
	where tipnumber = @Tip
	


	fetch next from csrInsert into @Tip
END

close csrInsert
deallocate csrInsert




/* set Default status to A */
Update dbo.Customer_Stage
	Set STATUS = 'A' 
Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status



------------------------

begin tran
/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, AffiliatFlag)
select distinct imp.ccacctnbr, imp.tipnumber, 'CREDIT', '10/31/2008', null, 'CREDIT CARD', stg.lastname, 0, null
from customer_stage stg join dbo.impcustomer2 imp
	on stg.tipnumber = imp.tipnumber
where tipprefix = '179'

rollback tran  --  commit tran



select *
from affiliat_stage


