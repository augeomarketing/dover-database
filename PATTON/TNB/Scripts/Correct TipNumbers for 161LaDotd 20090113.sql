
/*

162000000001639x
162000000001640x
162000000001641x
162000000001642

select *
from customer where tipnumber = '162000000001640'


*/


declare @LastTipUsed		varchar(15)

declare @OLDTip			varchar(15)
declare @NEWTip			varchar(15)

set @LastTipUsed = (Select lasttipnumberused from [161ladotd].dbo.client)

set @LastTipUsed =  left(@lasttipused,3) + right('000000000000' + cast(cast( right(@LastTipUsed,12) as bigint) + 1 as varchar(12)), 12)

update [161ladotd].dbo.client
	set lasttipnumberused = @LastTipUsed

set @NEWTip = @LastTipUsed

set @OldTip = '162000000001642'

select @oldtip OLDTIP, @NewTip NEWTIP
---
--customer
---
update [161Ladotd].dbo.customer
	set tipnumber = @NewTip
where tipnumber = @OldTip

---
--affiliat
---
update [161Ladotd].dbo.affiliat
	set tipnumber = @NewTip
where tipnumber = @OldTip

---
--history
---
update [161ladotd].dbo.history
	set tipnumber = @NewTip
where tipnumber =@OLDTip

---
--beginning_balance
---
update [161ladotd].dbo.beginning_balance_table	
	set tipnumber = @NEWTip
where tipnumber = @OLDTip

---
-- New_TipTracking table in onlinehistorywork - for combines
---
update onlinehistorywork.dbo.new_TipTracking
	set newtip = @NewTip
where newtip = @OLDTIP


----------------------------
-- CONNECT TO 167395-DB1 to run these
----------------------------


declare @OLDTip			varchar(15)
declare @NEWTip			varchar(15)


set @NEWTip = '161000000026014'

set @OldTip = '162000000001642'

---
--web
---
update newtnb.dbo.[1Security]
	set tipnumber = @NEWTip
where tipnumber = @OLDTip


update newtnb.dbo.Account
	set tipnumber = @NEWTip
where tipnumber = @OLDTip


update newtnb.dbo.Customer
	set tipnumber = @NEWTip
where tipnumber = @OLDTip


update newtnb.dbo.Statement
	set travnum = @NEWTip
where travnum = @OLDTip

