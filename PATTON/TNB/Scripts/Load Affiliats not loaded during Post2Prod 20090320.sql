
declare @TipFirst		varchar(3)
declare @db				nvarchar(50)
declare @sql			nvarchar(4000)
declare @RC				int

drop table newtnb.dbo.Affs2Add
create table newtnb.dbo.Affs2Add
	(ccacctnbr		varchar(25) primary key,
	 tipnumber		varchar(15) not null,
	 AcctType		varchar(20) null,
	 AcctTypeDesc	varchar(50) null,
	 LastName		varchar(40) null)
	 
	 
declare csrdb cursor FAST_FORWARD for
	select dbnumber, quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null and dbnamenexl = 'newtnb'
	
open csrdb

fetch next from csrdb into @TipFirst, @DB
while @@FETCH_STATUS = 0
BEGIN

	set @sql = '
	insert into newtnb.dbo.Affs2Add
	(tipnumber, ccacctnbr)
	select imp.tipnumber, imp.ccacctnbr
	from newtnb.dbo.impcustomer imp left outer join ' + @db + '.dbo.Affiliat aff
		on imp.tipnumber = aff.tipnumber
		and imp.ccacctnbr = aff.acctid
	where aff.tipnumber is null
	and left(imp.tipnumber,3) = ' + char(39) + @TipFirst + char(39)
	
	print @sql
	exec sp_executesql @SQL


	set @SQL = '
	update tmp
		set AcctType = w.accttypedesc
	from newtnb.dbo.Affs2Add tmp join (select distinct a.tipnumber, a.ccacctnbr, act.accttypedesc
							 from newtnb.dbo.Affs2Add a join ' + @db + '.dbo.history h
								on a.tipnumber = h.tipnumber
								and a.ccacctnbr = h.acctid
							 join newtnb.dbo.AcctType act
								on act.AcctType = h.trancode) w
	on tmp.tipnumber = w.tipnumber
	and tmp.ccacctnbr = w.ccacctnbr'
	
	print @sql
	exec sp_executesql @SQL


	delete from newtnb.dbo.Affs2Add where accttype is null


	set @rc = 0
	begin tran
	
	set @SQL = '
	insert into ' + @db + '.dbo.affiliat
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select tmp.ccacctnbr, tmp.tipnumber, tmp.accttype, ''02/28/2009'' DateAdded, ''OLD'' SECID, ''A'' AcctStatus,
				case
					when tmp.accttype = ''CREDIT'' then ''CREDIT CARD''
					when tmp.accttype = ''DEBIT'' then ''DEBIT CARD''
				end as AcctTypeDesc, c.lastname, 0 as YTDEarned
	from newtnb.dbo.Affs2Add tmp join ' + @DB + '.dbo.customer c
		on tmp.tipnumber = c.tipnumber'
		
	print @SQL
	exec @RC = sp_executesql @SQL
	
	if @RC = 0 -- Insert OK
		commit tran
	else
	BEGIN
		print 'ERROR on TIP First: ' + @TipFirst
		
		rollback tran
	END


	truncate table newtnb.dbo.affs2add


	fetch next from csrdb into @TipFirst, @DB
END

close csrdb

deallocate csrdb