DECLARE @SQL		NVARCHAR(4000)
DECLARE @db		NVARCHAR(50)
DECLARE @TipFirst	NVARCHAR(3)

DECLARE @CurMonth	INT
DECLARE @LastMonth	INT
DECLARE @YEAR		INT
DECLARE @DATE		DATETIME
DECLARE @strdate	NVARCHAR(10)
DECLARE @ctr		INT


IF EXISTS(SELECT 1 FROM newtnb.dbo.sysobjects WHERE name = 'wrkbbt' AND xtype = 'U')
	TRUNCATE TABLE newtnb.dbo.wrkbbt
ELSE
	CREATE TABLE newtnb.dbo.wrkbbt
	(Tipnumber VARCHAR(15) PRIMARY KEY, 
	 MonthBeg1 INT, MonthBeg2 INT, MonthBeg3 INT, MonthBeg4 INT, MonthBeg5 INT, MonthBeg6 INT, 
	 MonthBeg7 INT, MonthBeg8 INT, MonthBeg9 INT, MonthBeg10 INT, MonthBeg11 INT, MonthBeg12 INT)


DECLARE csrDb CURSOR fast_forward FOR
	SELECT dbnumber AS TipFirst, QUOTENAME(dbnamepatton) AS dbnamepatton
	FROM rewardsnow.dbo.dbprocessinfo dbpi LEFT OUTER JOIN newtnb.dbo.tipstoexclude tte
		ON dbpi.dbnumber = tte.tipfirst
	WHERE dbpi.dbnamenexl = 'newtnb' AND tte.tipfirst IS NULL

OPEN csrDb

FETCH NEXT FROM csrDb INTO @TipFirst, @db
WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT 'Begin processing database: ' + @db
	--
	-- truncate the work table
	TRUNCATE TABLE newtnb.dbo.wrkbbt

	--
	-- truncate the beginning balance table
	SET @SQL = 'truncate table ' + @db + '.dbo.beginning_balance_table'
	EXEC sp_executesql @SQL

	--
	-- Add customers & 0 balances to the work beginning balance table
	SET @SQL = '
	insert into newtnb.dbo.wrkbbt
	select cus.tipnumber, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	from ' + @db + '.dbo.customer cus left outer join ' + @db + '.dbo.beginning_balance_table bbt
		on cus.tipnumber = bbt.tipnumber
	where bbt.tipnumber is null'
	EXEC sp_executesql @SQL

	--
	--
	SET @DATE = '02/01/2008'
	SET @Ctr = 1

	WHILE @Ctr <= 12
	BEGIN

		SET @strDate = CAST( YEAR(@DATE) AS NVARCHAR(4)) + '/' +
					RIGHT( '00' + CAST( MONTH(@DATE) AS NVARCHAR(2)), 2) + '/' +
					RIGHT( '00' + CAST( DAY(@DATE) AS NVARCHAR(2)), 2)

		SET @CurMonth = MONTH(@strDate)

		SET @SQL = 'update bbt set MonthBeg' + CAST(@CurMonth AS NVARCHAR(2)) + 
					' = isnull((select sum(points * ratio) from ' + @db + '.dbo.history his where his.tipnumber = bbt.tipnumber and
						histdate < ' + CHAR(39) + @strDate + CHAR(39) + '), 0) from newtnb.dbo.wrkbbt bbt'
		PRINT @SQL
		EXEC sp_executesql  @SQL

		SET @DATE = DATEADD(mm, 1, @DATE)
		
		SET @Ctr = @Ctr + 1
	END

	SET @SQL = '
	INSERT INTO ' + @db + '.dbo.beginning_balance_table SELECT * FROM newtnb.dbo.wrkbbt'
	EXEC sp_executesql @SQL

	PRINT 'COMPLETE processing database: ' + @db
	PRINT ' '
	PRINT ' '

	FETCH NEXT FROM csrDb INTO @TipFirst, @db
END

CLOSE csrDb
DEALLOCATE csrDb