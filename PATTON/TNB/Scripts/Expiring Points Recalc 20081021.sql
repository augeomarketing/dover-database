declare @tipfirst		varchar(3)
declare @dbname		varchar(50)
declare @sql			nvarchar(4000)
declare @TfrPointsDate	datetime

declare csrR2U cursor FAST_FORWARD for
	select tipfirst
	from tnb_phb.dbo.assoc
	where reporttypecd = 'R'

open csrR2U

fetch next from csrR2U into @TipFirst

while @@FETCH_STATUS = 0
BEGIN

	-- Get DB Name
	set @dbname = quotename( (select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst))

	-- Check if they have transferred points coming in

	set @sql = 'set @TfrPointsDate = (select cast(min(histdate) as datetime) from ' + @dbname + '.dbo.history where trancode like ''t%'')'
	print @sql
	exec sp_executesql @sql, N'@TfrPointsDate datetime OUTPUT', @TfrPointsDate = @TfrPointsDate OUTPUT	

--	select @tfrpointsdate

	if (@TfrPointsDate is not null) and (@TfrPointsDate >= '2004-01-01')
	BEGIN

		print '========================================================================='
		print '==  Processing Database: ' + @DBName
		begin tran
			exec tnb.dbo.spExpirePointsExtractPHB @dbname, @TfrPointsDate
		commit tran  -- rollback
		print ' '
		print '========================================================================='

	END


	fetch next from csrR2U into @TipFirst
END

close csrR2U

deallocate csrR2U


/*

set @TfrPointsDate = (select cast(min(histdate) as datetime) from [112Velocity].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [115HeartoTexas].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [121Numark].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [121Numark]
4
Ratio:
1
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [121Numark].dbo.expiringpoints
INSERT   into [121Numark].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [121Numark].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(2759 row(s) affected)

      UPDATE [121Numark].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [121Numark].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [121Numark].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [121Numark].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [121Numark].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(906 row(s) affected)

      UPDATE [121Numark].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [121Numark].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [121Numark].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [121Numark].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [121Numark].dbo.expiringpoints.TIPNUMBER)

(2466 row(s) affected)
 
      UPDATE [121Numark].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [121Numark].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [121Numark].dbo.expiringpoints.TIPNUMBER)

(2759 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(2759 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(2759 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(2759 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(779 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(2759 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(2759 row(s) affected)
UPDATE [121Numark].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(667 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [1221STCommunity].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [123BlueBonnet].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [124BestOfIowa].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [124BestOfIowa]
4
Ratio:
1
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [124BestOfIowa].dbo.expiringpoints
INSERT   into [124BestOfIowa].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [124BestOfIowa].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(164 row(s) affected)

      UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [124BestOfIowa].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [124BestOfIowa].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [124BestOfIowa].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [124BestOfIowa].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(42 row(s) affected)

      UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [124BestOfIowa].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [124BestOfIowa].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [124BestOfIowa].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [124BestOfIowa].dbo.expiringpoints.TIPNUMBER)

(159 row(s) affected)
 
      UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [124BestOfIowa].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [124BestOfIowa].dbo.expiringpoints.TIPNUMBER)

(164 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(164 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(164 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(164 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(36 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(164 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(162 row(s) affected)
UPDATE [124BestOfIowa].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(24 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [125Cosden].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [126Sterlent].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [127WindsorLocks].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [127WindsorLocks]
3
Ratio:
0.75
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [127WindsorLocks].dbo.expiringpoints
INSERT   into [127WindsorLocks].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [127WindsorLocks].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(146 row(s) affected)

      UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [127WindsorLocks].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [127WindsorLocks].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [127WindsorLocks].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [127WindsorLocks].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(14 row(s) affected)

      UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [127WindsorLocks].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [127WindsorLocks].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [127WindsorLocks].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [127WindsorLocks].dbo.expiringpoints.TIPNUMBER)

(145 row(s) affected)
 
      UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [127WindsorLocks].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [127WindsorLocks].dbo.expiringpoints.TIPNUMBER)

(146 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(146 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(146 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(146 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(11 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(146 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(146 row(s) affected)
UPDATE [127WindsorLocks].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(11 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [128Banner].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [128Banner]
3
Ratio:
0.75
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [128Banner].dbo.expiringpoints
INSERT   into [128Banner].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [128Banner].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(476 row(s) affected)

      UPDATE [128Banner].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [128Banner].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [128Banner].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [128Banner].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [128Banner].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(85 row(s) affected)

      UPDATE [128Banner].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [128Banner].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [128Banner].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [128Banner].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [128Banner].dbo.expiringpoints.TIPNUMBER)

(450 row(s) affected)
 
      UPDATE [128Banner].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [128Banner].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [128Banner].dbo.expiringpoints.TIPNUMBER)

(476 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(476 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(476 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(476 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(78 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(476 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(476 row(s) affected)
UPDATE [128Banner].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(76 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [129R2UGeneric].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [140VisionOne].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [140VisionOne]
2
Ratio:
0.5
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [140VisionOne].dbo.expiringpoints
INSERT   into [140VisionOne].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [140VisionOne].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(98 row(s) affected)

      UPDATE [140VisionOne].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [140VisionOne].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [140VisionOne].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [140VisionOne].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [140VisionOne].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(53 row(s) affected)

      UPDATE [140VisionOne].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [140VisionOne].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [140VisionOne].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [140VisionOne].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [140VisionOne].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [140VisionOne].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [140VisionOne].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [140VisionOne].dbo.expiringpoints.TIPNUMBER)

(98 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(98 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(98 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(98 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(45 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(98 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(98 row(s) affected)
UPDATE [140VisionOne].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(45 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [141MissouriCU].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [141MissouriCU]
2
Ratio:
0.5
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [141MissouriCU].dbo.expiringpoints
INSERT   into [141MissouriCU].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [141MissouriCU].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(1050 row(s) affected)

      UPDATE [141MissouriCU].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [141MissouriCU].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [141MissouriCU].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [141MissouriCU].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [141MissouriCU].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(241 row(s) affected)

      UPDATE [141MissouriCU].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [141MissouriCU].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [141MissouriCU].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [141MissouriCU].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [141MissouriCU].dbo.expiringpoints.TIPNUMBER)

(863 row(s) affected)
 
      UPDATE [141MissouriCU].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [141MissouriCU].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [141MissouriCU].dbo.expiringpoints.TIPNUMBER)

(1050 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(1050 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(1050 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(1050 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(192 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(1050 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(1050 row(s) affected)
UPDATE [141MissouriCU].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(189 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [142Southpointe].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [143FAAERFCU].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [143FAAERFCU]
2
Ratio:
0.5
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [143FAAERFCU].dbo.expiringpoints
INSERT   into [143FAAERFCU].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [143FAAERFCU].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(1964 row(s) affected)

      UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [143FAAERFCU].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [143FAAERFCU].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [143FAAERFCU].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [143FAAERFCU].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(211 row(s) affected)

      UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [143FAAERFCU].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [143FAAERFCU].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [143FAAERFCU].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [143FAAERFCU].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [143FAAERFCU].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [143FAAERFCU].dbo.expiringpoints.TIPNUMBER)

(1964 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(1964 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(1964 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(1964 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(180 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(1964 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(1680 row(s) affected)
UPDATE [143FAAERFCU].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(180 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [144SouthernLakesCU].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [145CMECU].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [146APlus].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [147Westminster].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [147Westminster]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [147Westminster].dbo.expiringpoints
INSERT   into [147Westminster].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [147Westminster].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(183 row(s) affected)

      UPDATE [147Westminster].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [147Westminster].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [147Westminster].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [147Westminster].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [147Westminster].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(15 row(s) affected)

      UPDATE [147Westminster].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [147Westminster].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [147Westminster].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [147Westminster].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [147Westminster].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [147Westminster].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [147Westminster].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [147Westminster].dbo.expiringpoints.TIPNUMBER)

(183 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(183 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(183 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(183 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(14 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(183 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(176 row(s) affected)
UPDATE [147Westminster].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(14 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [148Citizens].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [148Citizens]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [148Citizens].dbo.expiringpoints
INSERT   into [148Citizens].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [148Citizens].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(1340 row(s) affected)

      UPDATE [148Citizens].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [148Citizens].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [148Citizens].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [148Citizens].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [148Citizens].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(140 row(s) affected)

      UPDATE [148Citizens].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [148Citizens].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [148Citizens].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [148Citizens].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [148Citizens].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [148Citizens].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [148Citizens].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [148Citizens].dbo.expiringpoints.TIPNUMBER)

(1340 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(1340 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(1340 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(1340 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(126 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(1340 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(1313 row(s) affected)
UPDATE [148Citizens].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(126 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [149Breco].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [149Breco]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [149Breco].dbo.expiringpoints
INSERT   into [149Breco].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [149Breco].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(442 row(s) affected)

      UPDATE [149Breco].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [149Breco].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [149Breco].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [149Breco].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [149Breco].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(34 row(s) affected)

      UPDATE [149Breco].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [149Breco].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [149Breco].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [149Breco].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [149Breco].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [149Breco].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [149Breco].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [149Breco].dbo.expiringpoints.TIPNUMBER)

(442 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(442 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(442 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(442 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(28 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(442 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(435 row(s) affected)
UPDATE [149Breco].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(28 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [151CommunityFirst].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [151CommunityFirst].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [151CommunityFirst].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [152GoodShepherd].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [152GoodShepherd]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [152GoodShepherd].dbo.expiringpoints
INSERT   into [152GoodShepherd].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [152GoodShepherd].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(237 row(s) affected)

      UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [152GoodShepherd].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [152GoodShepherd].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [152GoodShepherd].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [152GoodShepherd].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(21 row(s) affected)

      UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [152GoodShepherd].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [152GoodShepherd].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [152GoodShepherd].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [152GoodShepherd].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [152GoodShepherd].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [152GoodShepherd].dbo.expiringpoints.TIPNUMBER)

(237 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(237 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(237 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(237 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(19 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(237 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(232 row(s) affected)
UPDATE [152GoodShepherd].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(19 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [153Island].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [153Island]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [153Island].dbo.expiringpoints
INSERT   into [153Island].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [153Island].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(2784 row(s) affected)

      UPDATE [153Island].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [153Island].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [153Island].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [153Island].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [153Island].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(263 row(s) affected)

      UPDATE [153Island].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [153Island].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [153Island].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [153Island].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [153Island].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [153Island].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [153Island].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [153Island].dbo.expiringpoints.TIPNUMBER)

(2784 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(2784 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(2784 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(2784 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(217 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(2784 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(2782 row(s) affected)
UPDATE [153Island].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(217 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [154Engraving].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [154Engraving]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [154Engraving].dbo.expiringpoints
INSERT   into [154Engraving].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [154Engraving].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(17 row(s) affected)

      UPDATE [154Engraving].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [154Engraving].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [154Engraving].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [154Engraving].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [154Engraving].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(1 row(s) affected)

      UPDATE [154Engraving].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [154Engraving].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [154Engraving].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [154Engraving].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [154Engraving].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [154Engraving].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [154Engraving].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [154Engraving].dbo.expiringpoints.TIPNUMBER)

(17 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(17 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(17 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(17 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(1 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(17 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(16 row(s) affected)
UPDATE [154Engraving].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(1 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [155Laramie].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [155Laramie]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [155Laramie].dbo.expiringpoints
INSERT   into [155Laramie].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [155Laramie].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(204 row(s) affected)

      UPDATE [155Laramie].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [155Laramie].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [155Laramie].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [155Laramie].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [155Laramie].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(5 row(s) affected)

      UPDATE [155Laramie].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [155Laramie].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [155Laramie].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [155Laramie].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [155Laramie].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [155Laramie].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [155Laramie].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [155Laramie].dbo.expiringpoints.TIPNUMBER)

(204 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(204 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(204 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(204 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(5 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(204 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(192 row(s) affected)
UPDATE [155Laramie].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(5 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [156Baker].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [157Grafton].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [157Grafton]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [157Grafton].dbo.expiringpoints
INSERT   into [157Grafton].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [157Grafton].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(726 row(s) affected)

      UPDATE [157Grafton].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [157Grafton].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [157Grafton].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [157Grafton].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [157Grafton].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(17 row(s) affected)

      UPDATE [157Grafton].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [157Grafton].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [157Grafton].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [157Grafton].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [157Grafton].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [157Grafton].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [157Grafton].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [157Grafton].dbo.expiringpoints.TIPNUMBER)

(726 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(726 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(726 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(726 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(13 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(726 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(690 row(s) affected)
UPDATE [157Grafton].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(13 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [158ValleyStone].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [158ValleyStone]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [158ValleyStone].dbo.expiringpoints
INSERT   into [158ValleyStone].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [158ValleyStone].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(529 row(s) affected)

      UPDATE [158ValleyStone].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [158ValleyStone].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [158ValleyStone].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [158ValleyStone].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [158ValleyStone].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(24 row(s) affected)

      UPDATE [158ValleyStone].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [158ValleyStone].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [158ValleyStone].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [158ValleyStone].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [158ValleyStone].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [158ValleyStone].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [158ValleyStone].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [158ValleyStone].dbo.expiringpoints.TIPNUMBER)

(529 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(529 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(529 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(529 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(19 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(529 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(529 row(s) affected)
UPDATE [158ValleyStone].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(19 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [159Adirondack].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [159Adirondack]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [159Adirondack].dbo.expiringpoints
INSERT   into [159Adirondack].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [159Adirondack].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(681 row(s) affected)

      UPDATE [159Adirondack].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [159Adirondack].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [159Adirondack].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [159Adirondack].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [159Adirondack].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(7 row(s) affected)

      UPDATE [159Adirondack].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [159Adirondack].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [159Adirondack].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [159Adirondack].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [159Adirondack].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [159Adirondack].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [159Adirondack].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [159Adirondack].dbo.expiringpoints.TIPNUMBER)

(681 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(681 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(681 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(681 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(6 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(681 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(680 row(s) affected)
UPDATE [159Adirondack].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(6 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [180Kauai].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [181GulfCoast].dbo.history where trancode like 't%')
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [182Independant].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [182Independant]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [182Independant].dbo.expiringpoints
INSERT   into [182Independant].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [182Independant].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(33 row(s) affected)

      UPDATE [182Independant].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [182Independant].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [182Independant].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [182Independant].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [182Independant].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(3 row(s) affected)

      UPDATE [182Independant].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [182Independant].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [182Independant].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [182Independant].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [182Independant].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [182Independant].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [182Independant].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [182Independant].dbo.expiringpoints.TIPNUMBER)

(33 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(33 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(33 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(33 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(1 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(33 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(33 row(s) affected)
UPDATE [182Independant].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(1 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [183HawaiiPacific].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [183HawaiiPacific]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [183HawaiiPacific].dbo.expiringpoints
INSERT   into [183HawaiiPacific].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [183HawaiiPacific].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(370 row(s) affected)

      UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [183HawaiiPacific].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [183HawaiiPacific].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [183HawaiiPacific].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [183HawaiiPacific].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(4 row(s) affected)

      UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [183HawaiiPacific].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [183HawaiiPacific].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [183HawaiiPacific].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [183HawaiiPacific].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [183HawaiiPacific].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [183HawaiiPacific].dbo.expiringpoints.TIPNUMBER)

(370 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(370 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(370 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(370 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(3 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(370 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(370 row(s) affected)
UPDATE [183HawaiiPacific].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(3 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [184TowerCU].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [184TowerCU]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [184TowerCU].dbo.expiringpoints
INSERT   into [184TowerCU].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [184TowerCU].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(396 row(s) affected)

      UPDATE [184TowerCU].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [184TowerCU].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [184TowerCU].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [184TowerCU].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [184TowerCU].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(3 row(s) affected)

      UPDATE [184TowerCU].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [184TowerCU].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [184TowerCU].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [184TowerCU].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [184TowerCU].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [184TowerCU].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [184TowerCU].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [184TowerCU].dbo.expiringpoints.TIPNUMBER)

(396 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(396 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(396 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(396 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(1 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(396 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(396 row(s) affected)
UPDATE [184TowerCU].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(1 row(s) affected)
 
=========================================================================
set @TfrPointsDate = (select cast(min(histdate) as datetime) from [185Parda].dbo.history where trancode like 't%')
=========================================================================
==  Processing Database: [185Parda]
1
Ratio:
0.25
month end date
Dec 31 2008 11:59PM
month end date next
Feb  1 2009 12:00AM
Expiration Date: 2005-12-31 23:59:00.000
TRUNCATE TABLE [185Parda].dbo.expiringpoints
INSERT   into [185Parda].dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from [185Parda].dbo.history
      where 
      (trancode like('t%') and
           trancode <> 'IR')
      group by tipnumber

(2355 row(s) affected)

      UPDATE [185Parda].dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM [185Parda].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [185Parda].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00') 
      WHERE EXISTS  (SELECT *
             FROM [185Parda].dbo.HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = [185Parda].dbo.expiringpoints.TIPNUMBER
             and histdate < '10/1/2008 00:00:00')

(55 row(s) affected)

      UPDATE [185Parda].dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM [185Parda].dbo.HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = [185Parda].dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM [185Parda].dbo.HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = [185Parda].dbo.expiringpoints.TIPNUMBER)

(0 row(s) affected)
 
      UPDATE [185Parda].dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM [185Parda].dbo.HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = [185Parda].dbo.expiringpoints.TIPNUMBER)

(2355 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

(2355 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)

(2355 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

(2355 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

(48 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))

(2355 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 

(2201 row(s) affected)
UPDATE [185Parda].dbo.expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

(48 row(s) affected)
 
=========================================================================



*/