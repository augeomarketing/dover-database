/******************************************************************************/
/*    This will Create the Expering Points Table                              */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
--CREATE PROCEDURE spExpirePointsExtract @MonthBeg VARCHAR(10), @NumberOfYears int  AS   

Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
Declare @PointsToExpireNext int
Declare @AddPointsNext int
Declare @PointsprevExpired int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intmonthnext int
declare @intyear int
declare @ExpireDate DATETIME
declare @MonthEndDate DATETIME
declare @MonthEndDatenext DATETIME

declare @MonthBeg VARCHAR(10)
declare @NumberOfYears int
declare @trandate datetime
declare @yeardiff int
declare @ratio float


SET @MonthBeg = '06/01/2009'

set @NumberOfYears = '3' 

set @trandate = (select cast(min(histdate) as datetime) from history where trancode like 't%')

set @yeardiff = year(@monthbeg) - year(@trandate)
if month(@trandate) < 7
      set @yeardiff = @yeardiff + 1

--set @ratio = cast(@yeardiff as float)/ 4
set @ratio = 1
print 'Ratio:'
print @ratio




set @MonthEndDate = cast(@MonthBeg as datetime)
set @MonthEndDate = Dateadd(month, 1, @MonthBeg)
set @MonthEndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDate)),121)
set @MonthEndDatenext = cast(@MonthBeg as datetime)
set @MonthEndDatenext = Dateadd(month, 2, @MonthBeg)
--set @MonthEndDatenext = Dateadd(month, 1, @MonthBeg)
--set @MonthEndDatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDatenext)),121)
print 'month end date'
print @MonthEndDate
print 'month end date next'
print @MonthEndDatenext

set @expirationdate = cast(@MonthEndDate as datetime)
set @expirationdatenext = cast(@MonthEndDatenext as datetime)
set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)
--set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -@NumberOfYears, @expirationdate)),121)
print 'Expiration Date: ' + @expirationdate
set @expirationdatenext = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdatenext)),121)
set @expirationdatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdatenext)),121)
 

TRUNCATE TABLE expiringpoints


--
-- Get transfer points per tip
      INSERT   into expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      from history
      where 
      --histdate < @expirationdate and 
      trancode like('t%')
      group by tipnumber



--
-- Get total redemptions/increase redeemed by tip
      UPDATE expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = expiringpoints.TIPNUMBER) 
      WHERE EXISTS  (SELECT *
             FROM HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = expiringpoints.TIPNUMBER) 


--
-- get any prior expired points
      UPDATE expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = expiringpoints.TIPNUMBER)
 

--
-- 
      UPDATE expiringpoints  
      SET addpointsnext = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
             histdate < @expirationdatenext and trancode like ('t%')
             AND TIPNUMBER = expiringpoints.TIPNUMBER)
 
--
-- Add back expired point refunds
      UPDATE expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = expiringpoints.TIPNUMBER)


--
-- 
      UPDATE expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)


--
-- Calculate Points to Expire
      UPDATE expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)


--
-- Calc points expired to date
      UPDATE expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)


--
-- If pointstoexpire is null or <0, set to 0
      UPDATE expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 



      UPDATE expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))



      UPDATE expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 


      UPDATE expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 

GO


select *
from expiringpoints

