/*
MB	Manual Bonus
NW	New Account Bonus
RS	Redeem for Tunes
RU	Desc. Is QTR - 900454, Cert no. 76721                       - so I assume Redeem Travel?
TR	Transfer?
tx	Transfer Points
*/
---------------------------------
DROP TABLE dbo.wrkTranCodeXRef

CREATE TABLE dbo.wrkTranCodeXRef
	(oldtrancode		VARCHAR(2) PRIMARY KEY,
	 newtrancode		VARCHAR(2))

CREATE INDEX ix_tmptrancodexref2 ON wrkTranCodeXRef(newtrancode, oldtrancode)

INSERT INTO wrkTranCodeXRef
SELECT 'MB', 'BI'
UNION
SELECT 'NW', 'BN'
UNION
SELECT 'TR', 'TP'
UNION
SELECT 'tx', 'TP'
UNION
SELECT 'RU', 'RT'
UNION
SELECT 'RS', 'RD'

IF EXISTS(SELECT 1 FROM newtnb.dbo.sysobjects WHERE name = 'HISTORY_Rows_With_Corrected_TranCodes' AND xtype = 'U')
	TRUNCATE TABLE dbo.HISTORY_Rows_With_Corrected_TranCodes
ELSE
	CREATE TABLE [newtnb].[dbo].[HISTORY_Rows_With_Corrected_TranCodes](
		[TIPNUMBER] [varchar](15) NOT NULL,
		[ACCTID] [varchar](25) NULL,
		[HISTDATE] [datetime] NULL,
		[OLDTRANCODE] [varchar](2) NULL,
		[NEWTRANCODE] [varchar](2) NULL,
		[TranCount] [int] NULL,
		[POINTS] [int] NULL,
		[Description] [varchar](50) NULL,
		[SECID] [varchar](50) NULL,
		[Ratio] [numeric](18, 2) NULL,
		[Overage] [int] NULL
	) ON [PRIMARY]


DECLARE @db			VARCHAR(50)
DECLARE @SQL			NVARCHAR(4000)

DECLARE csrDB CURSOR FAST_FORWARD FOR
	SELECT QUOTENAME(dbnamepatton) AS dbnamepatton
	FROM rewardsnow.dbo.dbprocessinfo
	WHERE dbnamenexl = 'newtnb'

OPEN csrDB

FETCH NEXT FROM csrDB INTO @DB
WHILE @@FETCH_STATUS = 0
BEGIN

	SET @SQL = 'create index ix_History_temp__trancode_Tipnumber on ' + @DB + '.dbo.history(trancode, tipnumber)'
	EXEC sp_executesql @SQL

	SET @SQL = 
	'insert into newtnb.dbo.HISTORY_Rows_With_Corrected_TranCodes
	 (TIPNUMBER, ACCTID, HISTDATE, OLDTRANCODE, NEWTRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	 select tipnumber, acctid, histdate, trancode, newtrancode, trancount, points, description, secid, ratio, overage
	 from ' + @DB + '.dbo.history his join wrkTranCodeXRef xref
		on his.trancode = xref.oldtrancode'
	EXEC sp_executesql @SQL


	SET @SQL = 
	'update his
		set trancode = xref.newtrancode
	 from ' + @db + '.dbo.history his join wrkTranCodeXRef xref
		on his.trancode = xref.oldtrancode'

	PRINT @SQL

	--begin tran
	EXEC sp_executesql @SQL
	--rollback tran

	SET @SQL = 'use ' + @DB + '; DROP INDEX history.[ix_History_temp__trancode_TipNumber]'
	EXEC sp_executesql @SQL


	FETCH NEXT FROM csrDB INTO @DB
END

CLOSE csrDB
DEALLOCATE csrDB


