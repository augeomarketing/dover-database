


--
-- problems with 119 complex combines
-- problems with 161 Ladotd combines

declare @tipfirst nvarchar(3)

set @tipfirst =  '161' --(select top 1 tipfirst from assoc where cunum = '1982')

select sum(qsf.pointsend), sum(msf.pointsend)
from quarterly_statement_file_AUDIT qsf with (nolock) join monthly_statement_file msf with (nolock)
on qsf.tipnumber = msf.tipnumber
where left(qsf.tipnumber,3) = @tipfirst

select sum(pointsend), count(*)
from quarterly_statement_file_audit
where left(tipnumber,3) = @tipfirst


select sum(pointsend), count(*)
from monthly_statement_file
where left(tipnumber,3) = @tipfirst


select qsf.tipnumber, qsf.pointsend, msf.pointsend
from quarterly_statement_file_audit qsf join monthly_statement_file msf
	on qsf.tipnumber = msf.tipnumber
where left(qsf.tipnumber,3) = '161'
and qsf.pointsend != msf.pointsend




select msf.tipnumber , right( ltrim(rtrim(statusdescription)), 15)
from monthly_statement_file msf left outer join dbo.quarterly_statement_file_audit qsf
	on msf.tipnumber = qsf.tipnumber
join [161ladotd].dbo.customerdeleted cd
	on msf.tipnumber = cd.tipnumber
where left(msf.tipnumber,3) = '161'
 and qsf.tipnumber is null


select right(ltrim(rtrim(statusdescription)),15), * from [119complex].dbo.customerdeleted where tipnumber = '119000000001917'

-----
drop table #combined


create table #combined (oldtip varchar(15) primary key, newtip varchar(15))

insert into #combined
select msf.tipnumber , right( ltrim(rtrim(statusdescription)), 15)
from monthly_statement_file msf left outer join dbo.quarterly_statement_file_audit qsf
	on msf.tipnumber = qsf.tipnumber
join [161ladotd].dbo.customerdeleted cd
	on msf.tipnumber = cd.tipnumber
where left(msf.tipnumber,3) = '161' and left(right( ltrim(rtrim(statusdescription)), 15),3) = '161'
 and qsf.tipnumber is null

select newtip, count(*)
from #combined
group by newtip

select *
into bkup_monthly_statement_file
from monthly_statement_file


declare @oldtip varchar(15), @newtip varchar(15)

drop table #msf
select *
into #msf
from monthly_statement_file
where 1 = 0

declare csr cursor fast_forward for
	select distinct newtip from #combined
	
open csr

fetch next from csr into  @newtip
while @@FETCH_STATUS = 0
BEGIN
	truncate table #msf

	begin tran
	
	insert into #msf
	select msf.* from monthly_statement_file msf join #combined c
		on msf.tipnumber = c.oldtip
	where c.newtip = @newtip
	
	delete msf
	from monthly_statement_file msf join #combined c
		on msf.tipnumber = c.oldtip
	where c.newtip = @newtip
	
	insert into monthly_statement_file
	(Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, PointsBegin, PointsEnd, 
		PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturnedCR, 
		PointsReturnedDB, PointsSubtracted, PointsDecreased, acctid, cardseg, status, lastfour, pointfloor, PointsExpire)
	select cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
			ltrim(rtrim(cus.city)) + ',  ' + ltrim(rtrim(cus.state)) + '     ' + ltrim(rtrim(cus.zipcode)),
			sum(pointsbegin), sum(pointsend), sum(pointspurchasedcr), sum(pointspurchaseddb), sum(pointsbonus), sum(pointsadded),
			sum(pointsincreased), sum(pointsredeemed), sum(pointsreturnedcr), sum(pointsreturneddb), sum(pointssubtracted),
			sum(pointsdecreased), (select top 1 acctid from affiliat aff where aff.tipnumber = @newtip), '' cardseg,
			cus.status, null, null as pointfloor, sum(pointsexpire)
	from customer cus join #combined c
		on cus.tipnumber = c.newtip
	join #msf msf
		on c.oldtip = msf.tipnumber
	where cus.tipnumber = @newtip
	group by cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
			ltrim(rtrim(cus.city)) + ',  ' + ltrim(rtrim(cus.state)) + '     ' + ltrim(rtrim(cus.zipcode)), cus.status

	
	commit tran


	fetch next from csr into  @newtip
END

close csr
deallocate csr




----------------------


declare @newtip varchar(15)
set @newtip = '119100000031689'
truncate table #msf
--select *
--into #msf
--from monthly_statement_file
--where 1 = 0

	insert into #msf
	select msf.* from monthly_statement_file msf join #combined c
		on msf.tipnumber = c.oldtip
	where c.newtip = @newtip
	
	--delete msf
	--from monthly_statement_file msf join #combined c
	--	on msf.tipnumber = c.oldtip
	--where c.newtip = @newtip
	
	--insert into monthly_statement_file
	--(Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, PointsBegin, PointsEnd, 
	--	PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturnedCR, 
	--	PointsReturnedDB, PointsSubtracted, PointsDecreased, acctid, cardseg, status, lastfour, pointfloor, PointsExpire)
	select cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
			ltrim(rtrim(cus.city)) + ',  ' + ltrim(rtrim(cus.state)) + '     ' + ltrim(rtrim(cus.zipcode)),
			sum(pointsbegin), sum(pointsend), sum(pointspurchasedcr), sum(pointspurchaseddb), sum(pointsbonus), sum(pointsadded),
			sum(pointsincreased), sum(pointsredeemed), sum(pointsreturnedcr), sum(pointsreturneddb), sum(pointssubtracted),
			sum(pointsdecreased), (select top 1 acctid from affiliat aff where aff.tipnumber = @newtip), '' cardseg,
			cus.status, null, null as pointfloor, sum(pointsexpire)
	from customer cus join #combined c
		on cus.tipnumber = c.newtip
	join #msf msf
		on c.oldtip = msf.tipnumber
	where cus.tipnumber = @newtip
	group by cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
			ltrim(rtrim(cus.city)) + ',  ' + ltrim(rtrim(cus.state)) + '     ' + ltrim(rtrim(cus.zipcode)), cus.status
	