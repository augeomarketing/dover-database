select sum(runavailable)
from customer

select sum(points*ratio)
from history

--------

drop table #histpoints

select tipnumber, sum(points * ratio) pts
into #histpoints
from history where left(tipnumber,3) = '161'
group by tipnumber


select h.pts - runavailable, *
from #histpoints h join [161ladotd].dbo.customer c
on h.tipnumber = c.tipnumber
where pts != runavailable
order by h.tipnumber




-----------------
--
-----------------
/*
drop table #hist

create table #hist
	(tipnumber		varchar(15) primary key,
	 histpts		bigint,
	 transpts		bigint)

insert into #hist
(tipnumber, histpts)
select tipnumber, sum(points * ratio)
from history
where left(tipnumber,3) = '161'
group by tipnumber


update tmp
	set transpts = (select sum(cast(tranamt as bigint) * ratio) from newtnb.dbo.transstandard ts where ts.tip = tmp.tipnumber)
from #hist tmp

select tmp.*, histpts + transpts TotTransStd, c.runavailable, histpts + transpts - c.runavailable Variance
--into #missinghist
from #hist tmp join customer c
	on tmp.tipnumber = c.tipnumber
where 	histpts + transpts - c.runavailable = 0


insert into dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select tip, acctnum, '03/31/2009', ts.trancode, cast(trannum as int), cast(tranamt as decimal(18,0)), tt.description, 'OLD', ts.ratio, 0
from newtnb.dbo.transstandard ts join #missinghist tmp
	on ts.tip = tmp.tipnumber
join dbo.trantype tt
	on ts.trancode = tt.trancode
	

*/


insert into dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select '161000000029974', acctnum, '03/31/2009', ts.trancode, cast(trannum as int), cast(tranamt as decimal(18,0)), tt.description, 'OLD', ts.ratio, 0
from newtnb.dbo.transstandard ts join dbo.trantype tt
	on ts.trancode = tt.trancode
where tip = '161000000002510'





--  161000000029974
--  161000000029975

select *
from newtnb.dbo.transstandard
where tip in 
(
'161000000024345',
'161000000002510'
)




select *
from history
where tipnumber = '161000000029974'
order by histdate, trancode, points



select *
from customerdeleted
order by datedeleted desc

161000000028516
161000000028762

update history
set tipnumber = '161000000029975'
where tipnumber in
(
'161000000028516',
'161000000028762'
)

select *
from customerdeleted
where tipnumber in
(
'161000000002510',
'161000000024345'
)




121 has 3
135 has 6
143 has 4
161 is fubar
165 efcu has 2

---------------------------




select tipnumber, histdate, trancode, points, ratio
into #dupes
from history
group by tipnumber, histdate, trancode, points, ratio
having count(*) > 1
order by tipnumber, histdate, trancode, points

select *
from #dupes


select sum(points * ratio)
from #dupes
where trancode not like 'r%'
and histdate > '01/31/2009'


select sum(points * ratio) 
from history
where histdate < '04/01/2009'


select c.tipnumber    --sum(c.runavailable)
into #live
from customer c join customerdeleted cd
	on c.tipnumber = cd.tipnumber
	
	

select c.tipnumber    --sum(c.runavailable)
into #old
from zz161ladotdb4post.dbo.customer c join zz161ladotdb4post.dbo.customerdeleted cd
	on c.tipnumber = cd.tipnumber
	
select *
from #live l left outer join #old o
	on l.tipnumber = o.tipnumber
where o.tipnumber is null



select *
from #old o left outer join #live l
	on o.tipnumber = l.tipnumber
where l.tipnumber is null



--insert into customerdeleted
select *
from zz161ladotdb4post.dbo.customerdeleted
where tipnumber in
(
'161000000001849',
--'161000000008375',
'161000000005349',
'161000000017843'
)

--insert into affiliatdeleted
select *
from zz161ladotdb4post.dbo.affiliatdeleted
where tipnumber in
(
'161000000001849',
--'161000000008375',
'161000000005349',
'161000000017843'
)

insert into historydeleted
select *
from zz161ladotdb4post.dbo.historydeleted
where tipnumber in
(
'161000000001849',
--'161000000008375',
'161000000005349',
'161000000017843'
)






select *
from dbo.customerdeleted
where tipnumber in
(
'161000000001849',
--'161000000008375',
'161000000005349',
'161000000017843'
)







select *
from dbo.customer
where tipnumber in
(
'161000000001849',
'161000000008375',
'161000000005349',
'161000000017843'
)
/* these are active
161000000001849
161000000005349
161000000017843
*/

delete
from dbo.historydeleted
where tipnumber in
(
'161000000001849',
'161000000005349',
'161000000017843'
)
--and histdate > '03/31/2009'

insert into dbo.historydeleted
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
from zz161ladotdb4post.dbo.historydeleted
where tipnumber in
(
'161000000001849',
'161000000005349',
'161000000017843'
)




select *
from affiliat
where tipnumber in
(
'161000000001849',
'161000000005349',
'161000000017843'
)



select *
from affiliatdeleted
where tipnumber in
(
'161000000001849',
'161000000005349',
'161000000017843'
)


select *
from zz161ladotdb4post.dbo.affiliat
where tipnumber in
(
'161000000001849',
'161000000005349',
'161000000017843'
)



select *
from zz161ladotdb4post.dbo.affiliatdeleted
where tipnumber in
(
'161000000001849',
'161000000005349',
'161000000017843'
)


---------------------------------------------------------


select sum(points * ratio)
from history
where histdate < '03/01/2009'   --   95,644,775   s/b 95,837,531



drop table #year1, #year2

select tipnumber, year(histdate) yr, trancode, sum(points) pts
into #year1
from history
where histdate < '03/01/2009'
group by tipnumber, year(histdate), trancode
order by tipnumber, year(histdate), trancode



select tipnumber, year(histdate) yr, trancode, sum(points) pts
into #year2
from zz161ladotdb4post.dbo.history
where histdate < '03/01/2009'
group by tipnumber, year(histdate), trancode
order by tipnumber, year(histdate), trancode


select y2.tipnumber, y2.yr, sum(y2.pts)
from #year2 y2 left outer join #year1 y1
	on y2.tipnumber = y1.tipnumber
	and y2.yr = y1.yr
	and y2.trancode = y1.trancode
where y2.pts != y1.pts
--where y1.tipnumber is null 
and y2.tipnumber not in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)
group by y2.tipnumber, y2.yr
order by y2.tipnumber, y2.yr


select y1.*
from #year1 y1 left outer join #year2 y2
	on y1.tipnumber = y2.tipnumber
	and y1.yr = y2.yr
	and y2.trancode = y2.trancode
where y2.tipnumber is null
order by y1.tipnumber, y1.yr

select *
from history
where tipnumber like '16100000002997%'


--- these should NOT be in customer, but in deleted
select * from customer
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

select * from affiliat
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

delete from history
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)




--- these should NOT be in customer, but in deleted
select * from customerdeleted
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

select * from affiliatdeleted
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

select * from historydeleted
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)



select
where tipnumber in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

---------------
insert into customerdeleted
(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
select TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, '03/31/2009'
from customer
where tipnumber in
(
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

delete from customer
where tipnumber in
(
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)

--------------------------------------------------------------------
----
----
--------------------------------------------------------------------

select count(*), sum(monthbeg3)
from newtnb.dbo.beginning_balance_table
where left(tipnumber,3) = '161'  ---   Count: 8554	  points: 95837531
and monthbeg3 != 0

select tipnumber, sum(points * ratio) pts
into #checkbal
from dbo.history
where histdate < '03/01/2009'
group by tipnumber

select count(*) from #checkbal  -- count: 8536

select tipnumber
into #mt
from newtnb.dbo.beginning_balance_table
where left(tipnumber,3) = '161'  ---   Count: 8554	  points: 95837531
and monthbeg3 != 0



select mt.tipnumber
from #mt mt left outer join customer c
	on mt.tipnumber = c.tipnumber
where c.tipnumber is null
and mt.tipnumber not in
(
'161000000002510',
'161000000024345',
'161000000028516',
'161000000028762',
'161000000002297',
'161000000005655',
'161000000006060',
'161000000012060'
)


/**############################################**/
/** WOO HOO THESE ARE THE CUSTOMER MISSING!!! 24 tips **/
/**############################################**/

select sum(monthbeg3)
from newtnb.dbo.beginning_balance_table
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)


select distinct tipnumber
from customerdeleted
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)


select sum(points * ratio)
from zz161ladotdb4post.dbo.history
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)


insert into customer
(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded,  BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
select distinct  TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded,  BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
from customerdeleted
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)
and status = 'A'

delete
from customerdeleted
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)
and status = 'A'


insert into history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
from zz161ladotdb4post.dbo.history
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)


insert into dbo.affiliat
select *
from zz161ladotdb4post.dbo.affiliat
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)

delete
from historydeleted
where tipnumber
in
(
'161000000014589',
'161000000020844',
'161000000013215',
'161000000008375',
'161000000028372',
'161000000007774',
'161000000010356',
'161000000028373',
'161000000022819',
'161000000006782',
'161000000009146',
'161000000006768',
'161000000008493',
'161000000005893',
'161000000006170',
'161000000015955',
'161000000009289',
'161000000011913',
'161000000009041',
'161000000012435',
'161000000017779',
'161000000008785',
'161000000014643',
'161000000014642'
)


select * from zz161ladotdb4post.dbo.customerdeleted where tipnumber ='161000000008375'
select * from zz161ladotdb4post.dbo.historydeleted where tipnumber = '161000000008375'
select * from zz161ladotdb4post.dbo.affiliatdeleted where tipnumber = '161000000008375'

delete
from affiliatdeleted
where tipnumber = '161000000008375'

-------------------------------
--
-------------------------------

select *
from customer c join customerdeleted cd
on c.tipnumber = cd.tipnumber


select distinct h.tipnumber
from history h left outer join customer c
on h.tipnumber = c.tipnumber
where c.tipnumber is null


select distinct h.tipnumber
from historydeleted h left outer join customerdeleted c
on h.tipnumber = c.tipnumber
where c.tipnumber is null

