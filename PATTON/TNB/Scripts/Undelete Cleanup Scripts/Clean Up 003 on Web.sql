
begin tran

update sec
	set username = bkup.username,
		[password] = bkup.[password],
		secretq = bkup.secretq,
		secreta = bkup.secreta,
		emailstatement = bkup.emailstatement,
		email = bkup.email,
		email2 = bkup.email2,
		emailother = bkup.emailother,
		regdate = bkup.regdate,
		nag = bkup.nag
from newtnb.dbo.bkup_1security bkup join newtnb.dbo.[1security] sec
	on bkup.tipnumber = sec.tipnumber
where bkup.username is not null
and sec.username is null

commit tran



select *
from bkup_1security
where tipnumber = '138000000001118'

insert into [1security] (tipnumber) values('138000000001118')

select *
from statement
where travnum = '138000000001118'



insert into [1security]
(TipNumber, username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, RegDate, Nag)
select bk.TipNumber, bk.username, bk.Password, bk.SecretQ, bk.SecretA, bk.EmailStatement, bk.Email, bk.Email2, bk.EMailOther, bk.RegDate, bk.Nag
from bkup_1security bk left outer join dbo.[1security] sec
on bk.tipnumber = sec.tipnumber
where sec.tipnumber is null


select sec.tipnumber
into wrk_TipsMissing200903_deletes
from [1security] sec left outer join customer  cus
on sec.tipnumber = cus.tipnumber
where cus.tipnumber is null


begin tran

delete sec
from [1security] sec left outer join customer  cus
on sec.tipnumber = cus.tipnumber
where cus.tipnumber is null

rollback tran  --   commit tran



