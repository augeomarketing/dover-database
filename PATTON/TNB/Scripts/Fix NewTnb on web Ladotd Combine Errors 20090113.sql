-- Check newtiptracking in onlinehistorywork
delete from onlinehistorywork.dbo.New_TipTracking where newtip = '161000000002095'

--Remove Customer
delete from dbo.customer
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

--Remove History
delete from dbo.history
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

--Remove Affiliat
delete from dbo.affiliat
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

-- Remove beginning_balance_table
delete from dbo.beginning_balance_table
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)


-- Copy customer from zz161ladotd
insert into [161ladotd].dbo.customer
select *
from zz161ladotd.dbo.customer
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

-- Copy Affiliat from zz161ladotd
insert into [161ladotd].dbo.affiliat
select *
from zz161ladotd.dbo.affiliat
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

-- Copy History from zz161ladotd
insert into [161ladotd].dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
from zz161ladotd.dbo.history
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

-- Copy Beginning_balance_table from zz161ladotd
insert into [161ladotd].dbo.beginning_balance_table
select *
from zz161ladotd.dbo.beginning_balance_table
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)


--Remove CustomerDeleted
delete from customerdeleted 
where tipnumber in
(
'161000000009331',
'161000000003845',
'161000000007231'
)


--Remove HistoryDeleted
delete from dbo.historydeleted
where tipnumber in
(
'161000000009331',
'161000000003845',
'161000000007231'
)



--Remove AffiliatDeleted
delete from dbo.affiliatdeleted
where tipnumber in
(
'161000000009331',
'161000000003845',
'161000000007231'
)


/*

select *
from zz161ladotd.dbo.customerdeleted
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

*/



--======================
-- CHANGE DB CONNECTION TO 167395-db1
--======================
-- Check newtiptracking in onlinehistorywork
select * from onlinehistorywork.dbo.New_TipTracking where newtip = '161000000002095'

--
--  now adjust the web
--

--  Check NewTipTracking table in onlinehistorywork
delete from onlinehistorywork.dbo.New_TipTracking where newtip = '161000000002095'


delete from newtnb.dbo.[1Security]
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

delete from newtnb.dbo.account
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

delete from newtnb.dbo.customer
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

delete from newtnb.dbo.statement
where travnum in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

insert into newtnb.dbo.[1Security]
(TipNumber, username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, RegDate, Nag)
select TipNumber, username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, RegDate, Nag
from zzNewTNB.dbo.[1Security]
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)

insert into newtnb.dbo.account
(TipNumber, LastName, LastSix, SSNLast4)
select TipNumber, LastName, LastSix, SSNLast4
from zznewtnb.dbo.account
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)


insert into newtnb.dbo.customer
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, [Status], Segment, city, [state])
select TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, [Status], Segment, city, [state]
from zznewtnb.dbo.customer
where tipnumber in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)



insert into newtnb.dbo.statement
select *
from zznewtnb.dbo.statement
where travnum in
(
'161000000002095',
'161000000009331',
'161000000003845',
'161000000007231'
)





