
declare @sql		    nvarchar(4000)
declare @db		    nvarchar(50)

declare @OLDtip	    nvarchar(15)
declare @NEWtip	    nvarchar(15)
declare @tipint	    int

declare @OLDtipfirst    nvarchar(3)

declare @CDMsg		    nvarchar(50)

declare @RC		    int


set @RC = 0
set @CDMsg = 'Replaced with TIP#: '



declare csrR2UTP cursor FAST_FORWARD
    for select tipnumber	   
	   from newtnb.dbo.wrkR2UTPXfer
	   where processed = 'N'
	   order by tipnumber

open csrR2UTP

fetch next from csrR2UTP into @OLDtip

while @@FETCH_STATUS = 0
BEGIN

    set @OLDtipfirst = left(@OLDtip,3)

    -- Get dbname from oldtip
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @OLDtipfirst)


    BEGIN TRAN


    -- get next tip # for 199
    exec @RC = rewardsnow.dbo.spGetLastTipNumberUsed '199', @NEWtip output
    if @RC != 0
	   goto badend

    set @tipint = cast(right(@NEWtip, 12) as int) + 1
    set @NEWtip = left(@NEWtip,3) + right('000000000000' + cast(@tipint as varchar(12)), 12)

    exec @RC = rewardsnow.dbo.spPutLastTipNumberUsed '199', @NEWtip
    if @RC != 0
	   goto badend

    print @OLDtip + '          |          ' + @NEWtip

    -- Update wrkR2UTPxfer with new tip# assignment
    update newtnb.dbo.wrkR2UTPXfer
	   set NewTipNumber = @NEWtip
    where tipnumber = @OLDtip
    
    -- Add to 199 Customer
    set @SQL = '
    insert into [199TownNorthbank].dbo.customer
    (TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, 
     TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, 
     ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, 
     ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
    select ' + char(39) + @NEWtip + char(39) + ', RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, ' +
    char(39) + left(@NEWtip,3) + char(39) + ', ' + char(39) + right(@NEWtip, 12) + char(39) + ', 
    ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, 
     ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, 
     ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
    from ' + @db + '.dbo.customer where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend

    -- Add to 199 Affiliat
    set @SQL = '
    insert into [199TownNorthBank].dbo.affiliat
    (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
    select acctid, ' + char(39) + @NEWtip + char(39) + ', AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned
    from ' + @db + '.dbo.affiliat where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    -- Add to 199 History
    set @SQL = '
    insert into [199TownNorthBank].dbo.history
    (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
    select ' + char(39) + @NEWtip + char(39) + ', ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
    from ' + @db + '.dbo.history where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend



    -- Now move OLD Tip to the ..deleted tables, and delete from prod tables
    set @SQL = '
    insert into ' + @db + '.dbo.customerdeleted    
    (TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, 
     Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, 
     RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, 
     EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
    select TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, 
     Address3, Address4, City, State, Zipcode, LastName, Status, ' + char(39) + @CDMsg + @NEWtip + char(39) + ',  HomePhone, WorkPhone, RunBalance, 
     RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, 
     EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, getdate()
    from ' + @db + '.dbo.customer where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    set @SQL = '
    insert into ' + @db + '.dbo.affiliatdeleted
    (TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
    select TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, getdate()
    from ' + @DB + '.dbo.affiliat
    where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    set @SQL = '
    insert into ' + @db + '.dbo.historydeleted
    (TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
    select TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, getdate()
    from ' + @db + '.dbo.history
    where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    set @SQL = 'delete from ' + @db + '.dbo.customer where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    set @SQL = 'delete from ' + @db + '.dbo.affiliat where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    set @SQL = 'delete from ' + @db + '.dbo.history where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend


    set @SQL = 'delete from ' + @db + '.dbo.beginning_balance_table where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   goto badend



    --rollback tran 
    COMMIT TRAN

    goto UpdateWeb

badend:
    rollback tran
    print '***********ERROR***********'
    goto the_end


UpdateWeb:


    -- Update web database with new tip
    set @SQL = 'update rn1.newtnb.dbo.statement set travnum = ' + char(39) + @NEWtip + char(39) + ' where travnum = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   print 'ERROR UPDATING STATEMENT'


    set @SQL = 'update rn1.newtnb.dbo.account set tipnumber = ' + char(39) + @NEWtip + char(39) + ' where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   print 'ERROR UPDATING ACCOUNT'


    set @SQL = 'update rn1.newtnb.dbo.customer  set tipnumber = ' + char(39) + @NEWtip + char(39) + ' where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   print 'ERROR UPDATING CUSTOMER'


    set @SQL = 'update rn1.newtnb.dbo.[1security]  set tipnumber = ' + char(39) + @NEWtip + char(39) + ' where tipnumber = ' + char(39) + @OLDtip + char(39)

    print @sql
    exec @RC = sp_executesql @SQL
    if @RC != 0
	   print 'ERROR UPDATING 1SECURITY'

   
the_end:

    fetch next from csrR2UTP into @OLDtip
END

close csrR2UTP
deallocate csrR2UTP


