declare @DBName		nvarchar(100)
declare @SQL			nvarchar(4000)
Declare @filenumber		int
declare @FileName		nvarchar(255)
declare @GUID			uniqueidentifier

set @GUID = newid()

declare csrDB cursor FAST_FORWARD for
	select dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbpi.dbnamenexl = 'newtnb'  
	order by dbnamepatton

open csrDB

fetch next from csrDB into @DBName

while @@FETCH_STATUS = 0
BEGIN


	-- Get filename of last database backup done on the database
	set @FileName = (select physical_device_name
			  from msdb.dbo.backupmediafamily
			  where media_set_id = 
					(select top 1 media_set_id 
					 from msdb.dbo.backupset
					 where database_name = @DBName and
					 backup_finish_date = (select MAX(backup_finish_date) 
											from msdb.dbo.backupset 
											where database_name = @DBName)))

	-- build SQL statement to do the SQL LiteSpeed database restore
	set @SQL = 
	'exec master.dbo.xp_restore_database @database = ' + quotename(@DBName) + ', 
	@filename = ' + char(39) + @FileName + char(39) + ',
	@filenumber = 1,
	@with = N''REPLACE'',
	@with = N''STATS = 10'',
	@affinity = 0,
	@logging = 0'

	print 'Begin RESTORE up database: ' + @dbname

	exec sp_executesql @sql

	print @sql

	print 'Done RESTORING up database: ' + @dbname
	print ' '
	print ' '


	fetch next from csrDB into @DBName
END

close csrDB
deallocate csrDB




GO
