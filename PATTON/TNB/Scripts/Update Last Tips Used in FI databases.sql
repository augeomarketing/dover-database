use newtnb
GO

if exists(select 1 from dbo.sysobjects where name = 'spUpdateLastTipsUsed' and xtype = 'P')
	drop procedure dbo.spUpdateLasttipsUsed
GO

create procedure dbo.spUpdateLastTipsUsed

as

if exists(select 1 from dbo.sysobjects where name = 'wrk_lasttips' and xtype = 'U')
	drop table dbo.wrk_lasttips

create table dbo.wrk_lasttips (dbname nvarchar(100) primary key, lasttipused varchar(15))
create index ix_wrk_lasttips_dbname_lasttipused on dbo.wrk_lasttips (dbname, lasttipused)


declare @sql		nvarchar(4000)
declare @db		nvarchar(100)
declare @tipfirst	nvarchar(3)

declare csrDB cursor FAST_FORWARD for
	select dbnumber, quotename(dbnamepatton) 
	from rewardsnow.dbo.dbprocessinfo db left outer join newtnb.dbo.tipstoexclude tte
		on db.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbnamenexl = 'newtnb'

open csrdb

fetch next from csrDB into @tipfirst, @db

while @@FETCH_STATUS = 0
BEGIn

	set @sql = 
	'insert into newtnb.dbo.wrk_lasttips
	 select ' + char(39) + @db + char(39) + ', isnull((select max(tipnumber) from ' + @db + '.dbo.customer
										 where tipnumber not like ''___9%'' ), ' + char(39) + @tipfirst + '000000000000' + char(39) + ')'

--	print @sql
	exec sp_executesql @sql

	set @sql = 
	'update dbo.wrk_lasttips
		set lasttipused = (select max(tipnumber) 
						from ' + @db + '.dbo.customerdeleted where tipnumber not like ''___9%'')
	where dbname = ' + char(39) + @db + char(39) + ' and
		lasttipused <  (select max(tipnumber)
			  		 from ' + @db + '.dbo.customerdeleted
					 where tipnumber not like ''___9%'' ) '

--	print @sql
	exec sp_executesql @sql
					

	fetch next from csrdb into @tipfirst, @db
ENd

close csrdb

deallocate csrdb


Update C
	set LastTipnumberUsed = lasttipused
 from dbo.client c join newtnb.dbo.wrk_lasttips phb
	on c.tipfirst = left(phb.lasttipused,3)





