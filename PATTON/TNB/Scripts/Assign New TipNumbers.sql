

declare @last_sid			int
declare @current_sid		int

declare @tipfirst			varchar(3)
declare @tipnumber			varchar(15)

set @last_sid = 0


select top 1 @current_sid = sid_impcustomer_id, @tipfirst = tipprefix
	from dbo.impcustomer
	where tipnumber is null and sid_impcustomer_id > @last_sid

while @last_sid < @current_sid
BEGIN

	set @tipnumber = (select tipfirst + right('000000000000' + cast(cast( right(LastTipnumberUsed,12) as bigint) + 1 as varchar(12)), 12) 
					from dbo.client where tipfirst = @tipfirst) 

	update dbo.client set lasttipnumberused = @tipnumber where tipfirst = @tipfirst

	update dbo.impcustomer set tipnumber = @tipnumber where sid_impcustomer_id = @current_sid

--print cast(@current_sid as varchar(10)) + '  |  ' + isnull(cast(@last_sid as varchar(10)), 'Missing LastSid') + '  |  ' + isnull(@tipfirst, 'Missing TipFirst')

	set @last_sid = @current_sid

	select top 1 @current_sid = sid_impcustomer_id, @tipfirst = tipprefix
		from dbo.impcustomer
		where tipnumber is null and sid_impcustomer_id > @last_sid
END


---
--
--select tipfirst + right('000000000000' + cast(cast( right(LastTipnumberUsed,12) as bigint) + 1 as varchar(12)), 12)
--from dbo.client 
--where tipfirst = '102'
--
