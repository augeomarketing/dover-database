--exec newtnb.dbo.spQuarterlyStatement '11/01/2008', '11/30/2008', '113'
--
--select *
--from dbo.quarterly_statement_file
--
--
--select city, replace(city, ' ' + zipcode, '') cityfixed,
--		case
--			when right(replace(city, ' ' + zipcode, ''),3) = ' ' + ltrim(rtrim(state)) then left(replace(city, ' ' + zipcode, ''), len(replace(city, ' ' + zipcode, ''))-3)
--			else city
--		end as cityfixed2
--from [161ladotd].dbo.customer
----where tipnumber = '113000000001000'
--[130EECU ]
--
--select *
--from rewardsnow.dbo.dbprocessinfo
--------------------------------------------------------------------

declare @db			varchar(50)
declare @SQL			nvarchar(4000)

declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) as dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'newtnb' and tte.tipfirst is null

open csrDB

fetch next from csrDB into @DB
while @@FETCH_STATUS = 0
BEGIN

	set @SQL = '
		update ' + @db + '.dbo.customer	
			set city = ltrim(rtrim(replace(city, zipcode, '''')))'
	print @SQL
	exec sp_executesql @SQL


	set @SQL = '
			update ' + @db + '.dbo.customer	
			set city = 
					case
						when right(city,3) = '' '' + ltrim(rtrim(state)) then left(city, len(city) - 3)
						else city
					end'
					
	print @SQL
	exec sp_executesql @SQL

	fetch next from csrDB into @DB
END

close csrDB
deallocate csrDB



