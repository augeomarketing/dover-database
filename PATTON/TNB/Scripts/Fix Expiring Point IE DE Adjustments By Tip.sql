--
declare @db		nvarchar(50)
declare @sql		nvarchar(4000)
declare @TipFirst	nvarchar(3)

declare @TransferPointsDate		datetime
declare @TransferAdjustmentsDate	datetime

--
-- SET THE TIPFIRST HERE!!
set @TipFirst = '187'


--
-- Get the FI database name
set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

--
-- update history table, properly setting the trancode for transfer points
set @sql = 'update ' + @db + '.dbo.history set trancode = ''TP'' where trancode = ''tx'' '
exec sp_executesql @sql


-- Get date transfered points entered into history
set @sql = 'select top 1 @TransferPointsDate = histdate from ' + @db + '.dbo.history where trancode = ''tp'' '
exec sp_executesql @sql, N'@TransferPointsDate datetime OUTPUT', @TransferPointsDate = @TransferPointsDate OUTPUT

-- Go 2 months out.  any ie/de done in this window will be converted to TI/TD
set @TransferAdjustmentsDate = dateadd(mm, 2, @TransferPointsDate)

select @transferpointsdate, @transferadjustmentsdate



set @SQL = '
	update his
		set trancode = case
						when trancode = ''DE'' then ''TD''
						else ''TI''
					end
	from ' + @db + '.dbo.history his join (select distinct tipnumber from ' + @db + '.dbo.history where trancode = ''tp'') tfr
			on his.tipnumber = tfr.tipnumber
	where trancode in (''ie'', ''de'') and histdate between @TransferPointsDate and @TransferAdjustmentsDate'
print @sql

exec sp_executesql @SQL, N'@TransferPointsDate datetime, @TransferAdjustmentsDate datetime', 
	@TransferPointsdate = @TransferPointsDate, @TransferAdjustmentsDate = @TransferAdjustmentsDate
