/*
   Friday, April 22, 20112:37:59 PM
   User: 
   Server: doolittle\rn
   Database: NewTNB
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsBegin
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsEnd
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsBonus
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsPurchasedDB
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsBonusDB
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsPurchasedCR
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsBonusCR
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsAdded
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsIncreased
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsRedeemed
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsReturnedCR
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsReturnedDB
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsSubtracted
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsDecreased
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit
	DROP CONSTRAINT DF_Quarterly_Statement_File_Audit_PointsExpire
GO
CREATE TABLE dbo.Tmp_Quarterly_Statement_File_Audit
	(
	Tipnumber nchar(15) NOT NULL,
	Acctname1 varchar(40) NULL,
	Acctname2 varchar(40) NULL,
	Address1 varchar(40) NULL,
	Address2 varchar(40) NULL,
	Address3 varchar(40) NULL,
	CityStateZip varchar(50) NULL,
	City varchar(40) NULL,
	State varchar(2) NULL,
	ZipCode varchar(20) NULL,
	STDATE date NULL,
	PointsBegin int NOT NULL,
	PointsEnd int NOT NULL,
	PointsBonus int NOT NULL,
	PointsPurchasedDB int NOT NULL,
	PointsBonusDB int NOT NULL,
	PointsPurchasedCR int NOT NULL,
	PointsBonusCR int NOT NULL,
	POINTSBONUSMN int NOT NULL,
	PointsAdded int NOT NULL,
	PointsIncreased int NOT NULL,
	PointsRedeemed int NOT NULL,
	PointsReturnedCR int NOT NULL,
	PointsReturnedDB int NOT NULL,
	PointsSubtracted int NOT NULL,
	PointsDecreased int NOT NULL,
	PointsExpire int NOT NULL,
	AcctID char(16) NULL,
	BankNum varchar(100) NULL,
	HomePhone varchar(10) NULL,
	WorkPhone varchar(10) NULL,
	status char(1) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsBegin DEFAULT (0) FOR PointsBegin
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsEnd DEFAULT (0) FOR PointsEnd
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsBonus DEFAULT (0) FOR PointsBonus
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsPurchasedDB DEFAULT (0) FOR PointsPurchasedDB
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsBonusDB DEFAULT (0) FOR PointsBonusDB
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsPurchasedCR DEFAULT (0) FOR PointsPurchasedCR
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsBonusCR DEFAULT (0) FOR PointsBonusCR
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_POINTSBONUSMN DEFAULT 0 FOR POINTSBONUSMN
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsAdded DEFAULT (0) FOR PointsAdded
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsIncreased DEFAULT (0) FOR PointsIncreased
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsRedeemed DEFAULT (0) FOR PointsRedeemed
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsReturnedCR DEFAULT (0) FOR PointsReturnedCR
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsReturnedDB DEFAULT (0) FOR PointsReturnedDB
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsSubtracted DEFAULT (0) FOR PointsSubtracted
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsDecreased DEFAULT (0) FOR PointsDecreased
GO
ALTER TABLE dbo.Tmp_Quarterly_Statement_File_Audit ADD CONSTRAINT
	DF_Quarterly_Statement_File_Audit_PointsExpire DEFAULT (0) FOR PointsExpire
GO
IF EXISTS(SELECT * FROM dbo.Quarterly_Statement_File_Audit)
	 EXEC('INSERT INTO dbo.Tmp_Quarterly_Statement_File_Audit (Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, City, State, ZipCode, STDATE, PointsBegin, PointsEnd, PointsBonus, PointsPurchasedDB, PointsBonusDB, PointsPurchasedCR, PointsBonusCR, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, PointsExpire, AcctID, BankNum, HomePhone, WorkPhone, status)
		SELECT Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, City, State, ZipCode, CONVERT(date, STDATE), CONVERT(int, PointsBegin), CONVERT(int, PointsEnd), CONVERT(int, PointsBonus), CONVERT(int, PointsPurchasedDB), CONVERT(int, PointsBonusDB), CONVERT(int, PointsPurchasedCR), CONVERT(int, PointsBonusCR), CONVERT(int, PointsAdded), CONVERT(int, PointsIncreased), CONVERT(int, PointsRedeemed), CONVERT(int, PointsReturnedCR), CONVERT(int, PointsReturnedDB), CONVERT(int, PointsSubtracted), CONVERT(int, PointsDecreased), CONVERT(int, PointsExpire), AcctID, BankNum, HomePhone, WorkPhone, status FROM dbo.Quarterly_Statement_File_Audit WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Quarterly_Statement_File_Audit
GO
EXECUTE sp_rename N'dbo.Tmp_Quarterly_Statement_File_Audit', N'Quarterly_Statement_File_Audit', 'OBJECT' 
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit ADD CONSTRAINT
	PK_Quarterly_Statement_File_Audit PRIMARY KEY CLUSTERED 
	(
	Tipnumber
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 99, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
