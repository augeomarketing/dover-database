
---------------
begin tran

declare @oldtip	varchar(15)
declare @newtip	varchar(15)


set @oldtip = '161000000003829'
set @newtip = '161000000025856'

update [1Security]
	set tipnumber = @newtip
where tipnumber = @oldtip


update dbo.account
	set tipnumber = @newtip
where tipnumber = @oldtip



update dbo.onlhistory
	set tipnumber = @newtip
where tipnumber = @oldtip


update statement
	set travnum = @newtip
where travnum = @oldtip


update onlinehistorywork.dbo.New_TipTracking
	set newtip = @newtip
where newtip = @oldtip

rollback tran  --       commit tran