
declare @sql		nvarchar(4000)
declare @db			nvarchar(50)
declare @tip		nvarchar(15)
declare @rc			int

declare csrDB cursor FAST_FORWARD for
	select tipnumber
	from newtnb.dbo.wrkUndeletes
 
open csrDB


fetch next from csrDB into @tip
while @@FETCH_STATUS = 0
BEGIN
 
print '/*********************************************************************************************************/'
print '/* BEGIN PROCESSING TIP: ' + @tip

	set @rc = 0
	
	exec @rc = newtnb.dbo.spUndeleteTip @tip
	
	if @rc != 0
		print '!!!!! ERROR PROCESSING TIP: ' + @tip + '  !!!!!'
 
print ' '
print '/*********************************************************************************************************/' 
 
	fetch next from csrDB into @tip
END

close csrDB
deallocate csrDB
