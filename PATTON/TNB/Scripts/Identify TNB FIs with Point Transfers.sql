declare @db		nvarchar(100)
declare @sql		nvarchar(4000)


declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbnamenexl = 'newtnb'

open csrDB

fetch next from csrDB into @DB
while @@FETCH_STATUS = 0
BEGIN

	set @sql = '
	if exists(select 1 from ' + @db + '.dbo.history where trancode in (''tx'', ''tp''))
		print ''Database: '' + ' + char(39) + @db + char(39)

--print @sql
	exec sp_executesql @sql


	fetch next from csrDB into @DB
END

close csrDB
deallocate csrDB

-- databases with transferred points
Database: [116GrandCanyon]
Database: [120Jersey]
Database: [121Numark]
Database: [124BestOfIowa]
Database: [127WindsorLocks]
Database: [128Banner]
Database: [140VisionOne]
Database: [141MissouriCU]
Database: [143FAAERFCU]
Database: [147Westminster]
Database: [148Citizens]
Database: [149Breco]
Database: [150APFCU]
Database: [152GoodShepherd]
Database: [153Island]
Database: [154Engraving]
Database: [157Grafton]
Database: [158ValleyStone]
Database: [159Adirondack]
Database: [161Ladotd]
Database: [162Superior]
Database: [171Generations]
Database: [174MembersAdvantage]
Database: [183HawaiiPacific]
Database: [184TowerCU]
Database: [185Parda]
Database: [186WinstonSalem]
Database: [187OneComm]


-- 116 GrandCanyon is OK
select *
from [116GrandCanyon].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


-- 120 Jersey is ok
select *
from [120Jersey].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--  Has DEs that are 2 months older than transfers
select *
from [141MissouriCU].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--  has de/ie tied to TP
select *
from [150APFCU].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--  has de/ie tied to TP
select *
from [153Island].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--   has de/ie tied to TP
select *
from [157Grafton].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--   has de/ie tied to TP
select *
from [158ValleyStone].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--   has a few de/ie tied to TP
select *
from [162Superior].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--   has a few de/ie tied to TP
select *
from [184TowerCU].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate



--   has a few de/ie tied to TP
select *
from [186WinstonSalem].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--   has a few de/ie tied to TP
select *
from [187OneComm].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate


--   has a few de/ie tied to TP
select *
from [135texasbayarea].dbo.history
where trancode in ('tx', 'tp','ie', 'de')
order by tipnumber, histdate

select distinct trancode
from [135texasbayarea].dbo.history