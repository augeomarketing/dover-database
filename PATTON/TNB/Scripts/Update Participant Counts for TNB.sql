
declare @monthenddate		nvarchar(10)

declare @sql				nvarchar(4000)
declare @SQLParms			nvarchar(4000)
declare @SQLParmsOutput		nvarchar(4000)
declare @TipFirst			varchar(3)
declare @DBName			nvarchar(100)

declare @ParticipantCount	int

declare @rc				int

set @MonthEndDate = '11/30/2008'


set @SQLParms = '@Count int OUTPUT'
set @SQLParmsOutput = '@Count = @ParticipantCount OUTPUT'


declare csrTips cursor FAST_FORWARD for
	select dbnumber, dbnamepatton 
	from rewardsnow.dbo.dbprocessinfo  dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'NewTNB' and tte.tipfirst is null
	order by dbnumber


open csrTips

fetch next from csrTips into @TipFirst, @DBName

While @@FETCH_STATUS = 0
BEGIN

	set @SQL = 'select @Count = count(distinct tipnumber) from [' + @DBName + '].dbo.customer'

	exec @RC = sp_executesql @SQL, @SQLParms, @Count = @ParticipantCount OUTPUT

--	print @DBName + ': ' + Cast(@Participantcount as nvarchar(10)) + '|| ' + @sql

	exec @rc = rewardsnow.dbo.spmonthlyparticipantcounts @TipFirst, @MonthEndDate

	fetch next from csrTips into @TipFirst, @DBName
END

close csrTips

deallocate csrTips

