SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO




CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [numeric](10, 0) NULL,
	[ENDBAL] [numeric](10, 0) NULL,
	[CCPURCHASE] [numeric](10, 0) NULL,
	[BONUS] [numeric](10, 0) NULL,
	[PNTADD] [numeric](10, 0) NULL,
	[PNTINCRS] [numeric](10, 0) NULL,
	[REDEEMED] [numeric](10, 0) NULL,
	[PNTRETRN] [numeric](10, 0) NULL,
	[PNTSUBTR] [numeric](10, 0) NULL,
	[PNTDECRS] [numeric](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[expiringpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateofExpire] [nvarchar](25) NULL,
	[PointsToExpireNext] [float] NULL,
	[ADDPOINTSNEXT] [float] NULL,
	[EXPTODATE] [float] NULL,
	[ExpiredRefunded] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL DEFAULT (0),
	[CustID] [char](13) NULL DEFAULT (0),
	[Affiliatflag] [varchar](2) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY NONCLUSTERED 
(
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [AFFTIP] ON [dbo].[AFFILIAT] 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL DEFAULT (0),
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [int] NOT NULL,
	[CatalogCode] [varchar](50) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NOT NULL,
	[Special] [char](1) NOT NULL,
	[Business] [char](1) NOT NULL,
	[Television] [char](1) NOT NULL,
	[ClientAwardPoints] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Certificate](
	[CertificateID] [int] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [int] NULL,
	[RedeemedNow] [int] NULL,
	[RunAvailable] [int] NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [numeric](18, 0) NULL,
	[RedeemedNow] [numeric](18, 0) NOT NULL,
	[RunAvailable] [numeric](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY NONCLUSTERED 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [CUSTTIP] ON [dbo].[CUSTOMER] 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistTemp](
	[SlNo] [decimal](18, 0) NULL,
	[TipNumber] [varchar](15) NULL,
	[Histdate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL,
	[TranCode] [varchar](2) NULL,
	[IncDec] [nvarchar](1) NULL,
	[TypeCode] [nvarchar](1) NULL,
	[ExpiryFlag] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerDeleted](
	[TIPNumber] [varchar](15) NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [numeric](10, 0) NULL,
	[RunRedeemed] [numeric](10, 0) NULL,
	[RunAvailable] [numeric](10, 0) NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[Notes] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BusinessFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistExp](
	[TIPNumber] [varchar](15) NOT NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Zipcode] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[TotEarned] [int] NULL,
	[RunRedemed] [int] NULL,
	[Available] [int] NULL,
	[LastStmtDT] [smalldatetime] NULL,
	[AcctID] [varchar](25) NULL,
	[DateAdded] [smalldatetime] NULL,
	[CardType] [varchar](20) NULL,
	[Descriptio] [nvarchar](40) NULL,
	[TranCode] [nvarchar](2) NULL,
	[Points] [numeric](18, 0) NULL,
	[HistDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [HISTTIP] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL DEFAULT (0),
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Results](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [smalldatetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [numeric](18, 0) NULL,
	[MerchandiseBonusMinPoints] [numeric](18, 0) NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Pass] [varchar](30) NOT NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipnumberUsed] [char](15) NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[ClosedMonths] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]
GO






-------------------------------------------------------------------------------
-- Load up base tables
-------------------------------------------------------------------------------

SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

declare @TipFirst		varchar(3)
declare @AssocNbr		varchar(3)
declare @CUNum			varchar(4)

declare @CombineNames	varchar(1)
declare @DDAMatch		varchar(1)
declare @ReportTypeCd	varchar(1)

declare @ClientID		varchar(3)
declare @ClientCode		varchar(15)
declare @FIName		varchar(50)
declare @DBNamePatton	varchar(50)
declare @DBNameRN1		varchar(50)
declare @PointExpireYrs	varchar(1)
declare @DateJoined		datetime
declare @ExpFreq		varchar(2)
declare @RNProgramName	varchar(30)

set @TipFirst = '132'
set @AssocNbr = '466'
set @CUNum = null

set @CombineNames = 'N'
set @DDAMatch = 'N'
set @ReportTypeCd = 'L'
--  'R' = Rewards2U Travel Points
--  'L' = Liability Report
--  'S' = Summary Report

set @ClientID = 'ECU'
set @ClientCode = 'EmeryFCU'
set @FIName = 'Emery Federal CU'
set @DBNamePatton = '132EmeryFCU'
set @DBNameRN1 = 'NewTNB'
set @PointExpireYrs = '4'
set @ExpFreq = 'ME'
set @RNProgramName = 'Rewards2U'


BEGIN TRANSACTION;
INSERT INTO [rewardsnow].[dbo].[ClientData]
([tipfirst], [clientname], [DBNAMEONPATTON], [DBNAMEONNEXL], [PointExpirationYears], [datejoined], [EXPFREQ])
SELECT @TipFirst, @FIName, @DBNamePatton, @DBNameRN1, @PointExpireYrs, getdate(), @ExpFreq
COMMIT TRAN


BEGIN TRANSACTION
insert into rewardsnow.dbo.DBProcessInfo
(DBNumber, DBNamePatton, DBNameNEXL, DBAvailable)
select @tipfirst, @dbnamepatton, @dbnamern1, 'Y'
COMMIT TRAN



BEGIN TRANSACTION;
INSERT INTO [dbo].[Client]
([ClientCode], [ClientName], [Description], [TipFirst], [Address1], [Address2], [Address3], [Address4], [City], [State], 
[Zipcode], [Phone1], [Phone2], [ContactPerson1], [ContactPerson2], [ContactEmail1], [ContactEmail2], [DateJoined], 
[RNProgramName], [TermsConditions], [PointsUpdatedDT], [MinRedeemNeeded], [TravelFlag], [MerchandiseFlag], [TravelIncMinPoints], 
[MerchandiseBonusMinPoints], [MaxPointsPerYear], [PointExpirationYears], [ClientID], [Pass], [ServerName], [DbName], [UserName], 
[Password], [PointsExpire], [LastTipnumberUsed], [PointsExpireFrequencyCd], [ClosedMonths])

SELECT @ClientCode, @FIName, @FIName, @TipFirst, '', '', NULL, NULL, '', '', 
'', '', '', '', '', '', '', getdate(), 
@RNProgramName, NULL, '01-01-1900', 750, '0', '0', NULL, 
'0', '100000', @PointExpireYrs, @ClientID, @ClientID, 'patton\rn', @DBNamePatton, '', '', @PointExpireYrs, 
@TIPFirst + '000000000000', NULL, NULL

COMMIT;
RAISERROR (N'[dbo].[Client]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;



BEGIN TRANSACTION;
INSERT INTO [dbo].[PointsExpireFrequency]([PointsExpireFrequencyCd], [PointsExpireFrequencyNm])
SELECT 'ME', 'Month End' UNION ALL
SELECT 'MM', 'Mid Month' UNION ALL
SELECT 'YE', 'Year End'
COMMIT;
RAISERROR (N'[dbo].[PointsExpireFrequency]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;



BEGIN TRANSACTION;
INSERT INTO [dbo].[AcctType]([AcctType], [AcctTypeDesc])
SELECT 'CAR', 'Car Loan' UNION ALL
SELECT 'CD', 'CD' UNION ALL
SELECT 'CHECK', 'Checking' UNION ALL
SELECT 'Credit', 'Credit Card' UNION ALL
SELECT 'CREDIT/DEBIT', 'Credit/Debit Card' UNION ALL
SELECT 'Debit', 'Debit Card' UNION ALL
SELECT 'OTHER', 'Other Card' UNION ALL
SELECT 'Saving', 'Saving Card'
COMMIT;
RAISERROR (N'[dbo].[AcctType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;



BEGIN TRANSACTION;
INSERT INTO [dbo].[Status]([Status], [StatusDescription])
SELECT 'A', 'Active[A]' UNION ALL
SELECT 'C', 'Closed[C]' UNION ALL
SELECT 'K', 'Bankrupt[K]' UNION ALL
SELECT 'L', 'Lost/Stolen[L]' UNION ALL
SELECT 'O', 'OverLimit[O]' UNION ALL
SELECT 'U', 'Fraud[U]' UNION ALL
SELECT 'Y', 'FinanceChargeFrozen[Y]' UNION ALL
SELECT 'I', 'Inactive[I]' UNION ALL
SELECT 'X', 'PastDue[X]' UNION ALL
SELECT 'D', 'Deleted[D]' UNION ALL
SELECT 'S', 'Suspended[S]' UNION ALL
SELECT 'P', 'Pending[P]'
COMMIT;
RAISERROR (N'[dbo].[Status]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;



begin transaction
	insert into dbo.trantype
	select * from rewardsnow.dbo.trantype
COMMIT TRAN
Raiserror (N'[dbo].[TranType]: Loaded from RewardsNWO: .....Done!', 10, 1) with nowait



insert into newtnb.dbo.assoc
(TIPFirst, AssocNum, CUNum, CombineNames, DDAMatch, ReporttypeCd)
select @TipFirst, @AssocNbr, @CUNum, @CombineNames, @DDAMatch, @ReportTypeCd
