declare @prmTipFirst varchar(3)
set @prmTipFirst = '119'

--- Individual ---
SELECT 
LEFT(tipnumber, 3) AS inst,
clientname as 'FI Name',
datename(mm, LEFT((GETDATE() - 5 - DAY(GETDATE() - 5)), 12))+' '+ datename(yy, LEFT(GETDATE() - DAY(GETDATE()), 12)) AS 'Report Date', 

COUNT(LEFT(tipnumber, 3)) AS 'Participating Customers', 
(select count(tipnumber) from newtnb.dbo.monthly_statement_file a where PointsEnd >= 750 and left(aud.tipnumber,3) = left(a.tipnumber,3) Group by left(tipnumber,3)) as 'Eligible Customer Count',
(select count(tipnumber) from newtnb.dbo.monthly_statement_file a where PointsEnd < 750 and left(aud.tipnumber,3) = left(a.tipnumber,3) Group by left(tipnumber,3)) as 'Ineligible Customer Count',
cast(SUM(PointsEnd) as int) AS 'Points Outstanding',
cast((select sum(floor(PointsEnd/750)*750) from newtnb.dbo.monthly_statement_file a where PointsEnd >= 750 and left(aud.tipnumber,3) = left(a.tipnumber,3) Group by left(tipnumber,3)) as int) as 'Points Available to Redeem',
cast((select avg((floor(PointsEnd/750))*750) from newtnb.dbo.monthly_statement_file a where PointsEnd >= 750 and left(aud.tipnumber,3) = left(a.tipnumber,3) Group by left(tipnumber,3)) as int) as 'Average Points per Eligible Customer',
cast(avg(PointsEnd) as int) as 'Average Points per Part. Customer',
cast(SUM(PointsPurchasedCR+PointsPurchasedDB) as int) AS 'Points Added for Purchases', 
cast(SUM(PointsBonus) as int) AS 'Points Added for Bonus',
cast(sum(PointsAdded) as int) as 'Points Added/ Misc', 
cast(SUM(PointsIncreased) as int) AS 'Total Points Added', 
cast(SUM(PointsRedeemed) as int) AS 'Points Subtracted for Redeemed', 
cast(SUM(PointsReturnedCR+PointsReturnedDB) as int) AS 'Points Subtracted for Returns', 
cast(sum(PointsSubtracted) as int) as 'Points Subtracted/ Misc', 
cast(SUM(PointsDecreased) as int) AS 'Total Points Subtracted'
FROM newtnb.dbo.monthly_statement_file aud join rewardsnow.dbo.dbprocessinfo cli
                on left(aud.tipnumber,3) = cli.dbnumber
and cli.dbnumber = @prmTipFirst

GROUP BY LEFT(tipnumber, 3), clientname
ORDER BY LEFT(tipnumber, 3)

