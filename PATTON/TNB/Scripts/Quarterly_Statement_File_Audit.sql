/*
   Thursday, September 05, 20139:21:49 AM
   User: 
   Server: doolittle\rn
   Database: NewTNB
   Application: 
*/
use NewTNB
go

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit ADD
	PurchasedPoints int NOT NULL CONSTRAINT DF_Quarterly_Statement_File_Audit_PurchasedPoints DEFAULT (0)
GO
ALTER TABLE dbo.Quarterly_Statement_File_Audit SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
