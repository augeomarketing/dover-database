declare @tipfirst varchar(3)
set @tipfirst = '145'


select count(*) Customer_Count
from newtnb.dbo.monthly_statement_file
where left(tipnumber,3) = @tipfirst

select count(*) Customer_count_from_customer
from dbo.customer


select count(*) Customer_Count_Eligible
from newtnb.dbo.monthly_statement_file
where left(tipnumber,3) = @tipfirst
and pointsend >= 750



--select * 
--from newtnb.dbo.monthly_statement_file msf left outer join dbo.customer cus
--    on msf.tipnumber = cus.tipnumber
--where left(msf.tipnumber,3) = '119' and cus.tipnumber is null
--order by msf.tipnumber



select sum(points * ratio) Points_Eligible_To_Redeem
from dbo.history where histdate < '06/01/2009'


select sum(points*ratio) Purchases
from dbo.history
where histdate >= '05/01/2009' --and histdate <= '05/31/2009'
and trancode like '6%'


select sum(points * ratio) Misc_Points_Added
from dbo.history
where histdate >= '05/01/2009' and histdate < '06/01/2009'
and trancode in ('ie', 'dr')	  


select sum(points * ratio) Redeemed
from dbo.history
where histdate >= '05/01/2009' and histdate <= '05/31/2009 23:59:59'
and trancode like 'r%'


select sum(points * ratio) Misc_Points_Decr
from dbo.history
where histdate >= '05/01/2009' and histdate < '06/01/2009'
and trancode in ('de', 'ir', 'xp')


select sum(points*ratio) Credits
from dbo.history
where histdate >= '05/01/2009' --and histdate <= '05/31/2009'
and trancode like '3%'



