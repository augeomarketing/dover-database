declare @db			nvarchar(50)
declare @sql		nvarchar(4000)
declare @txndate	datetime


set @txndate = '03/31/2009'

declare csrDB cursor fast_forward for
	select quotename(dbnamepatton)
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbnamenexl = 'newtnb'
	
open csrdb

fetch next from csrdb into @db
while @@FETCH_STATUS = 0
BEGIN

begin tran
	set @sql = '
		update ' + @db + '.dbo.history
			set histdate = @txndate
		where trancode = ''XP'' and histdate > ''03/31/2009'' and histdate < ''04/01/2009'' '
		
	exec sp_executesql @sql, N'@txndate datetime', @txndate = @txndate
	print @sql

commit tran

	fetch next from csrdb into @db
END

close csrdb
deallocate csrdb
