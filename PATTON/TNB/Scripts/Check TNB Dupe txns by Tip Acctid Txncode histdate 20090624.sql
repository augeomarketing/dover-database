declare @sql		    nvarchar(4000)
declare @recctr	    int
declare @db		    nvarchar(50)

declare csrdb cursor FAST_FORWARD for
    select quotename(dbnamepatton)
    from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
	   on dbpi.dbnumber = tte.tipfirst
    where dbpi.dbnamenexl = 'newtnb' and tte.tipfirst is null
    order by dbnamepatton

open csrdb

fetch next from csrdb into @db

while @@FETCH_STATUS = 0
BEGIN

    set @sql = '
			 if exists(
				        select tipnumber, acctid, histdate, trancode, points
				        from ' + @db + '.dbo.history
				        where histdate = ''04/30/2009''
				        group by tipnumber, acctid, histdate, trancode, points
				        having count(*) > 1)

			     select ' + char(39) + @db + char(39) 

    exec sp_executesql @sql
    
    fetch next from csrDB into @db
END

close csrdb
deallocate csrdb
