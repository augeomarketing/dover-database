/****** Object:  Table [dbo].[Certificate]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Certificate](
	[CertificateID] [int] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [int] NULL,
	[RedeemedNow] [int] NULL,
	[RunAvailable] [int] NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CertificateDeleted]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [numeric](18, 0) NULL,
	[RedeemedNow] [numeric](18, 0) NOT NULL,
	[RunAvailable] [numeric](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY NONCLUSTERED 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [CUSTTIP] ON [dbo].[CUSTOMER] 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerDeleted]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerDeleted](
	[TIPNumber] [varchar](15) NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [numeric](10, 0) NULL,
	[RunRedeemed] [numeric](10, 0) NULL,
	[RunAvailable] [numeric](10, 0) NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[Notes] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BusinessFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HistExp]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistExp](
	[TIPNumber] [varchar](15) NOT NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Zipcode] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[TotEarned] [int] NULL,
	[RunRedemed] [int] NULL,
	[Available] [int] NULL,
	[LastStmtDT] [smalldatetime] NULL,
	[AcctID] [varchar](25) NULL,
	[DateAdded] [smalldatetime] NULL,
	[CardType] [varchar](20) NULL,
	[Descriptio] [nvarchar](40) NULL,
	[TranCode] [nvarchar](2) NULL,
	[Points] [numeric](18, 0) NULL,
	[HistDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Location]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Reg]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Results]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Results](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [smalldatetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [HISTTIP] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StmtExp]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [numeric](10, 0) NULL,
	[ENDBAL] [numeric](10, 0) NULL,
	[CCPURCHASE] [numeric](10, 0) NULL,
	[BONUS] [numeric](10, 0) NULL,
	[PNTADD] [numeric](10, 0) NULL,
	[PNTINCRS] [numeric](10, 0) NULL,
	[REDEEMED] [numeric](10, 0) NULL,
	[PNTRETRN] [numeric](10, 0) NULL,
	[PNTSUBTR] [numeric](10, 0) NULL,
	[PNTDECRS] [numeric](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[expiringpoints]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[expiringpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateofExpire] [nvarchar](25) NULL,
	[PointsToExpireNext] [float] NULL,
	[ADDPOINTSNEXT] [float] NULL,
	[EXPTODATE] [float] NULL,
	[ExpiredRefunded] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[Affiliatflag] [varchar](2) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY NONCLUSTERED 
(
	[ACCTID] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [AFFTIP] ON [dbo].[AFFILIAT] 
(
	[TIPNUMBER] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Award]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [int] NOT NULL,
	[CatalogCode] [varchar](50) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NOT NULL,
	[Special] [char](1) NOT NULL,
	[Business] [char](1) NOT NULL,
	[Television] [char](1) NOT NULL,
	[ClientAwardPoints] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Client]    Script Date: 09/01/2009 15:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [numeric](18, 0) NULL,
	[MerchandiseBonusMinPoints] [numeric](18, 0) NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Pass] [varchar](30) NOT NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipnumberUsed] [char](15) NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[PointsUpdated] [datetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [bit] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[logo] [varchar](50) NULL,
	[landing] [varchar](255) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StmtNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__AFFILIAT__YTDEar__0EA330E9]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [YTDEarned]
GO
/****** Object:  Default [DF__AFFILIAT__CustID__0F975522]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [CustID]
GO
/****** Object:  Default [DF__AffiliatD__YTDEa__108B795B]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[AffiliatDeleted] ADD  DEFAULT (0) FOR [YTDEarned]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg1]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT (0) FOR [MonthBeg1]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg2]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT (0) FOR [MonthBeg2]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg3]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT (0) FOR [MonthBeg3]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg4]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT (0) FOR [MonthBeg4]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg5]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT (0) FOR [MonthBeg5]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg6]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT (0) FOR [MonthBeg6]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg7]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT (0) FOR [MonthBeg7]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg8]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT (0) FOR [MonthBeg8]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg9]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT (0) FOR [MonthBeg9]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg10]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT (0) FOR [MonthBeg10]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg11]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT (0) FOR [MonthBeg11]
GO
/****** Object:  Default [DF_Beginning_Balance_Table_MonthBeg12]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT (0) FOR [MonthBeg12]
GO
/****** Object:  Default [DF__HISTORY__Overage__1CF15040]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[HISTORY] ADD  CONSTRAINT [DF__HISTORY__Overage__1CF15040]  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__HistoryDe__Overa__1DE57479]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[HistoryDeleted] ADD  CONSTRAINT [DF__HistoryDe__Overa__1DE57479]  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__HISTORYTI__Overa__1ED998B2]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[HISTORYTIP] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  ForeignKey [FK_Client_PointsExpireFrequency]    Script Date: 09/01/2009 15:38:07 ******/
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]
GO
