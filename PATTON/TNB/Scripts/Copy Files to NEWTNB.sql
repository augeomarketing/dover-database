use newtnb
GO

declare @dbname	nvarchar(128)
declare @sqlcmnd	nvarchar(4000)

declare csrDBName cursor FAST_FORWARD for
	select dbnamepatton 
	from rewardsnow.dbo.DBProcessInfo dbpi left outer join dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'NewTNB' and dbavailable = 'Y' and tte.tipfirst is null
	order by dbnamepatton

open csrDBName

fetch next from csrDBName into @DbName

while @@FETCH_STATUS = 0
BEGIN
print 'Begin Processing database: ' + @DbName
	-- Load the stage tables
	set @SQLCmnd = 'Insert into dbo.Customer Select * from [' +  @dbName + '].dbo.Customer'
	Exec sp_executeSql @SQLCmnd 

--	set @SQLCmnd = 'Update dbo.Customer Set  RunAvaliableNew = 0 '
--	Exec sp_executeSql @SQLCmnd 

	set @SQLCmnd = 'Insert into dbo.Affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag)
				 Select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag from [' + @dbName + '].dbo.Affiliat'
	Exec sp_executeSql @SQLCmnd 

	set @SQLCmnd = 'Insert into dbo.History (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
				 Select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage from [' + @dbName  + '].dbo.History '
	Exec sp_executeSql @SQLCmnd 

--	set @SQLCmnd = 'Update dbo.History set SecId = ' + char(39) +  'OLD' + char(39) 
--	Exec sp_executeSql @SQLCmnd 

--	set @SQLCmnd = 'Insert into dbo.OnetimeBonuses_Stage Select * from [' + @dbName + '].dbo.OnetimeBonuses'
--	Exec sp_executeSql @SQLCmnd

print 'Complete Processing database: ' + @DbName
print ' '
print ' '

	fetch next from csrDBName into @DbName
END

close csrDBName

deallocate csrDBName

Update dbo.Customer Set  RunAvaliableNew = 0 
Update dbo.History set SecId = 'OLD'



--truncate table  newtnb.dbo.beginning_balance_table

--drop table #bbt


--create table #bbt
--	(Tipnumber varchar(15) primary key, 
--	 MonthBeg1 int, MonthBeg2 int, MonthBeg3 int, MonthBeg4 int, MonthBeg5 int, MonthBeg6 int, 
--	 MonthBeg7 int, MonthBeg8 int, MonthBeg9 int, MonthBeg10 int, MonthBeg11 int, MonthBeg12 int)

--insert into #bbt
--select cus.tipnumber, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
--from dbo.customer cus left outer join dbo.beginning_balance_table bbt
--	on cus.tipnumber = bbt.tipnumber
--where bbt.tipnumber is null



--declare @CurMonth	int
--declare @LastMonth	int
--declare @Year		int
--declare @date		datetime
--declare @strdate	nvarchar(10)
--declare @ctr		int
--declare @sql		nvarchar(4000)

--set @Date = '04/01/2008'

--set @CurMonth = month(@Date)

--set @ctr = 1

--while @ctr <= 12
--BEGIN

--	set @strDate = cast( year(@date) as nvarchar(4)) + '/' +
--				right( '00' + cast( month(@date) as nvarchar(2)), 2) + '/' +
--				right( '00' + cast( day(@date) as nvarchar(2)), 2)

--	set @SQL = 'update #bbt set MonthBeg' + cast(@CurMonth as nvarchar(2)) + 
--				' = isnull((select sum(points * ratio) from dbo.history his where his.tipnumber = #bbt.tipnumber and
--					histdate < ' + char(39) + @strDate + char(39) + '), 0)'
--	print @sql
--	exec sp_executesql  @sql

--	set @date = dateadd(mm, 1, @date)
--	set @CurMonth = month(@Date)

--	set @ctr = @ctr + 1
--END




--insert into dbo.beginning_balance_table
--select * from #BBT







--/* now get the beginning_balance_table for all the tips.  These are all over the place. :(   */
--
--delete from newtnb.dbo.beginning_balance_table
--
--insert into newtnb.dbo.beginning_balance_table
--select *
--from tnb.dbo.beginning_balance_table
--where left(tipnumber, 3) not in ('000', '111', '115', '112', '129', '151')
--UNION
--select *
--from r2uwork.dbo.beginning_balance_table where left(tipnumber,3) not in ('000', '111')
--
--
--drop table #bbt
--
--
--create table #bbt
--	(Tipnumber varchar(15) primary key, 
--	 MonthBeg1 int, MonthBeg2 int, MonthBeg3 int, MonthBeg4 int, MonthBeg5 int, MonthBeg6 int, 
--	 MonthBeg7 int, MonthBeg8 int, MonthBeg9 int, MonthBeg10 int, MonthBeg11 int, MonthBeg12 int)
--
--insert into #bbt
--select cus.tipnumber, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
--from dbo.customer cus left outer join dbo.beginning_balance_table bbt
--	on cus.tipnumber = bbt.tipnumber
--where bbt.tipnumber is null
--
--
--
--declare @CurMonth	int
--declare @LastMonth	int
--declare @Year		int
--declare @date		datetime
--declare @strdate	nvarchar(10)
--
--declare @sql		nvarchar(4000)
--
--set @Date = '01/01/2008'
--
--set @CurMonth = 1
--
--while @CurMonth <= 12
--BEGIN
--
--	set @strDate = cast( year(@date) as nvarchar(4)) + '/' +
--				right( '00' + cast( month(@date) as nvarchar(2)), 2) + '/' +
--				right( '00' + cast( day(@date) as nvarchar(2)), 2)
--
--	set @SQL = 'update #bbt set MonthBeg' + cast(@CurMonth as nvarchar(2)) + 
--				' = isnull((select sum(points * ratio) from dbo.history his where his.tipnumber = #bbt.tipnumber and
--					histdate < ' + char(39) + @strDate + char(39) + '), 0)'
--	print @sql
--	exec sp_executesql  @sql
--
--	set @date = dateadd(mm, 1, @date)
--	
--	set @CurMonth = @CurMonth + 1
--END
--
----
----select * from #BBT
----order by tipnumber
--
--
--insert into dbo.beginning_balance_table
--select * from #BBT
--
--
--select *
--from #bbt where tipnumber = '105999999999802'
--

--
--select tipnumber, sum(points * ratio) as points
--into #points
--from history
--where histdate = '09/30/2008' and (trancode in ('63', '33', '67', '37') OR trancode  like 'b%')
--group by tipnumber
--
--create index ix_tmppoints_tipnumber_points on #points (tipnumber, points)
--
---- now back out these points from customeravailable
--update cus
--	set RunAvailable = runavailable - points
--from #points pts join dbo.customer cus
--	on pts.tipnumber = cus.tipnumber
--
---- now back out these points from customeravailable
--update cus
--	set RunAvailable = runavailable - points
--from #points pts join dbo.customer_stage cus
--	on pts.tipnumber = cus.tipnumber
--
--
---- lastly, delete these transactions from history
--
--delete from history
--where histdate = '09/30/2008' and (trancode in ('63', '33', '67', '37') OR trancode  like 'b%')
--
