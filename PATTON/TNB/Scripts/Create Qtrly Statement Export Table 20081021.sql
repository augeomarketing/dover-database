
truncate table r2uwork.dbo.Quarterly_Statement_File_20081021


-- Get exclusion list of tips with e-statements
drop table #estatements
create table #EStatements
	(tipnumber		varchar(15) primary key)

insert into #EStatements
select tipnumber
from [167395-DB1].newtnb.dbo.[1Security]
where isnull(emailstatement, '') != 'Y'


-- Insert statements into qsf_20081021 table for only those tips where statements
-- need to be resent
insert into r2uwork.dbo.Quarterly_Statement_File_20081021
(Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, STDATE, PointsBegin, PointsEnd, 
	PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturned, PointsSubtracted, 
	PointsDecreased, AcctID, BankNum, LastLine2, ExpiringPoints)
select qsf.Tipnumber, qsf.Acctname1, qsf.Acctname2, qsf.Address1, qsf.Address2, qsf.Address3, CityStateZip, STDATE, PointsBegin, PointsEnd, 
		PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturned, PointsSubtracted, 
		PointsDecreased, AcctID, qsf.BankNum, citystatezip + ' ' + zipcode as lastline2, new.pointstoexpire as expire
from r2uwork.dbo.quarterly_statement_file qsf join r2uwork.dbo.R2U_Expire_PHB new
	on qsf.tipnumber = new.tipnumber

join r2uwork.dbo.customer c
	on qsf.tipnumber = c.tipnumber

left join r2uwork.dbo.R2U_Expire old
	on qsf.tipnumber = old.tipnumber

join #EStatements tmp
	on tmp.tipnumber = qsf.tipnumber

where
	abs(new.pointstoexpire - isnull(old.pointstoexpire, 0)) > 3  and
	left(qsf.tipnumber,3) not in ('148', '112', '115', '122', '123', '125', '126') 

select @@rowcount as RowsInserted_ExpiringPointRecalc

--
-- Now add in all of 148Citizens
insert into r2uwork.dbo.Quarterly_Statement_File_20081021
(Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, STDATE, PointsBegin, PointsEnd, 
	PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturned, PointsSubtracted, 
	PointsDecreased, AcctID, BankNum, LastLine2, ExpiringPoints )
select qsf.Tipnumber, qsf.Acctname1, qsf.Acctname2, qsf.Address1, qsf.Address2, qsf.Address3, CityStateZip, STDATE, PointsBegin, PointsEnd, 
	PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturned, PointsSubtracted, 
	PointsDecreased, AcctID, qsf.BankNum, citystatezip + ' ' + zipcode as lastline2, ep.pointstoexpire as expire
from r2uwork.dbo.quarterly_statement_file qsf join #EStatements tmp
	on qsf.tipnumber = tmp.tipnumber

join [148Citizens].dbo.Customer c
	on c.tipnumber = qsf.tipnumber
join [148Citizens].dbo.expiringpoints ep
	on ep.tipnumber = qsf.tipnumber
where left(qsf.tipnumber,3) = '148'

select @@rowcount as RowsInserted_Citizens


/*

Rows inserted for Expiring Point recalc & statement reissue:  10,185

Rows inserted for Citizens:  1,103

*/

-- Make sure points in quarterly statement export file match the expiring points table
select qsf.tipnumber, qsf.expiringpoints, new.pointstoexpire
from r2uwork.dbo.Quarterly_Statement_File_20081021 qsf join r2uwork.dbo.R2U_Expire_PHB new
	on qsf.tipnumber = new.tipnumber
where 
--(qsf.expiringpoints - new.pointstoexpire) != 0 and 
left(new.tipnumber,3) != '148'


-- Make sure that the expiring points for 148Citizens is 0
select *
from r2uwork.dbo.Quarterly_Statement_File_20081021 
where expiringpoints != 0
and tipnumber = '185000000002885'


select *
from [185Parda].dbo.history
where tipnumber = '185000000002885'


select Count(*)
from r2uwork.dbo.Quarterly_Statement_File_20081021 