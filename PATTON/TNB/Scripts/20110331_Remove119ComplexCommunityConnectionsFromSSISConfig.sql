
delete
from [ssis configurations]
where packagepath in
(
'\Package.Connections[ComplexDeletedCards].Properties[ConnectionString]',
'\Package.Connections[Complex Transactions].Properties[ConnectionString]',
'\Package.Connections[Complex Demographics].Properties[ConnectionString]',
'\Package.Connections[DowTran.txt].Properties[ConnectionString]',
'\Package.Connections[DowCust.txt].Properties[ConnectionString]',
'\Package.Connections[Dow Summary].Properties[ConnectionString]'
)


