declare @SQL			nvarchar(4000)
declare @DB			nvarchar(50)

declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) dbnamepatton 
	from rewardsnow.dbo.dbprocessinfo
	where dbnamenexl = 'newtnb'
	order by dbnumber

open csrDB

fetch next from csrDB into @DB
while @@FETCH_STATUS = 0
BEGIN

	set @SQL = '
	if not exists(select 1 from ' + @DB + '.dbo.sysobjects where name = ''beginning_balance_table'' and xtype = ''U'')
		BEGIN
		CREATE TABLE ' + @DB + '.[dbo].[Beginning_Balance_Table](
			[Tipnumber] [nchar](15) NOT NULL,
			[MonthBeg1] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT (0),
			[MonthBeg2] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT (0),
			[MonthBeg3] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT (0),
			[MonthBeg4] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT (0),
			[MonthBeg5] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT (0),
			[MonthBeg6] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT (0),
			[MonthBeg7] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT (0),
			[MonthBeg8] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT (0),
			[MonthBeg9] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT (0),
			[MonthBeg10] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT (0),
			[MonthBeg11] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT (0),
			[MonthBeg12] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT (0),
		 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
		(
			[Tipnumber] ASC
		) );
		print ''BEGINNING BALANCE TABLE CREATED IN DB: '' + ' + char(39) + @DB + char(39) + '; 
		end'

	--print @sql
	exec sp_executesql @SQL

	fetch next from csrDB into @DB
END

close csrDB

deallocate csrDB