drop table #hist


create table #hist
(tipnumber	varchar(15) primary key,
 pts bigint)
 
 insert into #hist
 select tipnumber, sum(points * ratio)
 from history
 group by tipnumber
 
 
 select *
 from #hist h join customer c
 on h.tipnumber = c.tipnumber
 where h.pts != c.runavailable
 
 select *
 from #hist h left outer join customer c
 on h.tipnumber = c.tipnumber
 where c.tipnumber is null

select h.*, c.*
from customer c left outer join #hist h
on c.tipnumber = h.tipnumber
where h.tipnumber is null
and runavailable != 0



select *
from history
where tipnumber = '135000000013274' and trancode like 'r%'
order by histdate, trancode, points

select *
from newtnb.dbo.history
where tipnumber = '135000000013274' and trancode like 'r%'
order by histdate, trancode, points


-----

--insert into history
--select top 1 *
--from history
--where histdate = '2008-11-25 19:05:00.000'
--and tipnumber = '135000000013274'
--and trancode = 'RC'
--and points = 14000


--select *
--from history
--where left(tipnumber,3) = '135'