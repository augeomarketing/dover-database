declare @db			nvarchar(50)
declare @sql		nvarchar(4000)

declare @custpoints	bigint
declare @histpoints bigint

declare csrdb  cursor fast_forward for
	select quotename(dbnamepatton)
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'newtnb' and tte.tipfirst is null
	order by dbnamenexl

open csrdb

fetch next from csrdb into @db
while @@fetch_status = 0
BEGIN

	set @custpoints = 0
	set @histpoints = 0
	
	set @sql = 'set @custpoints = (select sum(isnull(runavailable, 0)) from ' + @db + '.dbo.customer)'
	
	exec sp_executesql @sql, N'@CustPoints bigint OUTPUT', @custpoints = @custpoints OUTPUT
	
	set @sql = 'set @histpoints = (select sum(points * ratio) from ' + @db + '.dbo.history)'
	exec sp_executesql @sql, N'@HistPoints bigint OUTPUT', @histpoints = @histpoints OUTPUT
	
	if @custpoints != @histpoints
		print '#### Points do not match in database: ' + @db+ '|	Cust Points ' + cast(@custpoints as varchar(10)) + ' |  Hist Points ' + cast(@histpoints as varchar(10))
--	else
--		print 'Database: ' + @db + '|	Cust Points ' + cast(@custpoints as varchar(10)) + ' |  Hist Points ' + cast(@histpoints as varchar(10))


	fetch next from csrdb into @db
END

close csrdb
deallocate csrdb

(Broken Redemption) #### Points do not match in database: [143FAAERFCU]|	Cust Points 23960757 |  Hist Points 23988757
