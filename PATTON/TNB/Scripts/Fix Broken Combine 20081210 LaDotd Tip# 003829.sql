-- This was combined into 3829
Select * from customerdeleted where tipnumber in
('161000000001822','161000000009907')


Select * from affiliat where tipnumber = '161000000003829'

Select * from customer where tipnumber = '161000000003829'

Select * from history where tipnumber = '161000000003829' order by histdate desc


select *
from customerdeleted where tipnumber  = '161000000003829'

-------------
begin tran
declare @oldtip		varchar(15)
declare @lasttipused	varchar(15)

set @oldtip = '161000000003829'

set @lasttipused = (select lasttipnumberused from dbo.client where tipfirst = '161')
print @lasttipused
set @lasttipused = cast(@lasttipused as bigint) + 1
print @lasttipused

update dbo.client set lasttipnumberused = @lasttipused where tipfirst = '161'


print 'Begin changing from tip: ' + @oldtip + ' to new tip#: ' + @lasttipused

print '	:Begin Update History'
update dbo.history
	set tipnumber = @lasttipused
where tipnumber = @oldtip
print '	:Done History Update'
print ' '
print ' '


print '	:Begin Update Affiliat'
update dbo.affiliat
	set tipnumber = @lasttipused
where tipnumber = @oldtip
print '	:Done Affiliat Update'
print ' '
print ' '


print '	:Begin Update Customer'
update dbo.customer
	set tipnumber = @lasttipused
where tipnumber = @oldtip
print '	:Done Customer Update'
print ' '
print ' '

print '	:Insert row into Beginning Balance Table'
insert into dbo.beginning_balance_table
(Tipnumber, MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12)
values(@LastTipUsed, 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0)
print '	:Done Beginning Balance Insert'
print ' '
print ' '
print 'Conversion complete.'

commit

select *
from customer where tipnumber = '161000000025856'