
declare @sql		nvarchar(4000)
declare @db		nvarchar(50)


declare csrDB cursor fast_forward for
	select quotename(dbnamepatton)
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbnamenexl = 'newtnb'


open csrdb

fetch next from csrdb into @db
while @@FETCH_STATUS = 0
BEGIN

	set @sql = 'Update ' + @db + '.dbo.affiliat set ytdearned = 0'
	exec sp_executesql @sql

	set @sql = 'Update ' + @db + '.dbo.affiliatdeleted set ytdearned = 0'
	exec sp_executesql @sql


	fetch next from csrdb into @db
END

close csrdb
deallocate csrdb
