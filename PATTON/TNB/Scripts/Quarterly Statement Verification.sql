
declare @tipnumber	varchar(15)
declare @startdate	datetime
declare @enddate	datetime

-- set @tipnumber = '161000000025514'    -- the big kahuna

set @tipnumber = '150000000002111'

set @startdate = '07/01/2009'
set @enddate = '09/30/2009'


select sum(points * ratio) BeginningBalance
from dbo.history
where tipnumber = @tipnumber
and histdate < @startdate


select sum(points * ratio) StatementPointActivity
from dbo.history
where tipnumber = @tipnumber
and histdate between @startdate and @enddate


select sum(points * ratio) AllHistPoints
from dbo.history
where tipnumber = @tipnumber
and histdate <= @enddate


select sum(points * ratio) purchases
from dbo.history
where tipnumber = @tipnumber
and histdate between @startdate and @enddate and trancode like '6%'

select sum(points * ratio) Returns
from dbo.history
where tipnumber = @tipnumber
and histdate between @startdate and @enddate and trancode like '3%'

select sum(points * ratio) Redeems
from dbo.history
where tipnumber = @tipnumber
and histdate between @startdate and @enddate and trancode like 'r%'



select *
from dbo.history
where tipnumber = @tipnumber
and histdate between @startdate and @enddate


--select *
--from dbo.history
--where tipnumber = @tipnumber

--
--select *
--from history where trancode = 'xp' and histdate > '12/31/2008'