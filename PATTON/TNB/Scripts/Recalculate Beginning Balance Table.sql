use newtnb
GO

truncate table  newtnb.dbo.beginning_balance_table

IF object_id('tempdb..#bbt') IS NOT NULL
BEGIN
   DROP TABLE #bbt
END


create table #bbt
	(Tipnumber varchar(15) primary key, 
	 MonthBeg1 int, MonthBeg2 int, MonthBeg3 int, MonthBeg4 int, MonthBeg5 int, MonthBeg6 int, 
	 MonthBeg7 int, MonthBeg8 int, MonthBeg9 int, MonthBeg10 int, MonthBeg11 int, MonthBeg12 int)

insert into #bbt
select cus.tipnumber, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
from dbo.customer cus left outer join dbo.beginning_balance_table bbt
	on cus.tipnumber = bbt.tipnumber
where bbt.tipnumber is null



declare @CurMonth	int
declare @LastMonth	int
declare @Year		int
declare @date		datetime
declare @strdate	nvarchar(10)

declare @sql		nvarchar(4000)

set @Date = '07/01/2008'  -- use processing month + 1, processing year - 1

set @CurMonth = 1

while @CurMonth <= 12
BEGIN

	set @strDate = cast( year(@date) as nvarchar(4)) + '/' +
				right( '00' + cast( month(@date) as nvarchar(2)), 2) + '/' +
				right( '00' + cast( day(@date) as nvarchar(2)), 2)

	set @SQL = 'update #bbt set MonthBeg' + cast(month(@date) as nvarchar(2)) + 
				' = isnull((select sum(points * ratio) from dbo.history his where his.tipnumber = #bbt.tipnumber and
					histdate < ' + char(39) + @strDate + char(39) + '), 0)'
	print @sql
	exec sp_executesql  @sql

	set @date = dateadd(mm, 1, @date)
	
	set @CurMonth = @CurMonth + 1
END




insert into dbo.beginning_balance_table
select * from #BBT





