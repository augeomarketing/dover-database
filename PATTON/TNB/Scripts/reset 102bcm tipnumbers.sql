declare @sqlcmd nvarchar(4000), @dbname nvarchar(50)



--
-- Now update each FI's Client table with the last Tip# used
--
Declare curDBName cursor FAST_FORWARD for
	select quotename(ltrim(rtrim(dbnamepatton))) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join dbo.TipsToExclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'newtnb'
	and tte.tipfirst is null


open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
Begin
	set @sqlcmd = 'UPDATE fi
					set LastTipnumberUsed = tnb.LastTipnumberUsed
				FROM ' + @DBName + '.dbo.client fi join dbo.client tnb
					on fi.tipfirst = tnb.tipfirst'

	--exec sp_executesql @sqlcmd
	print @sqlcmd

	Fetch next from curDBName into @DBName
END

close curDBName

deallocate curDBName





begin tran

UPDATE fi
					set LastTipnumberUsed = tnb.LastTipnumberUsed
				FROM [102BCM].dbo.client fi join dbo.client tnb
					on fi.tipfirst = tnb.tipfirst

rollback tran


select lasttipnumberused
from [102bcm].dbo.client

select lasttipnumberused
from dbo.client where tipfirst = '102'

select max(tipnumber)
from impcustomer
where left(tipnumber,3) = '102'

select top 300 tipnumber
from impcustomer
where left(tipnumber,4) = '1022'



select max(tipnumber)
from [102bcm].dbo.customer
where tipnumber not like '1029%'

select max(tipnumber)
from [102bcm].dbo.customerdeleted
where tipnumber not like '1029%'


update client
set lasttipnumberused = '102000000002400'
where tipfirst = '102'




---------------------------------------------------------------------------------------------------


update client
set lasttipnumberused = '102000000002400'
where tipfirst = '102'


drop table #check
create table #check (tipnumber varchar(15) primary key, transactionamt money, newtip varchar(15))


insert into #check
(tipnumber, transactionamt)
select cus.tipnumber, sum(transactionamt)
from dbo.impcustomer cus join dbo.imptransaction txn
	on cus.tipnumber = txn.tipnumber
where cus.tipnumber like '1022%'
group by cus.tipnumber
order by cus.tipnumber


declare @tip varchar(15)
declare @newtip varchar(15)

declare csrtip cursor for
	select tipnumber
	from dbo.impcustomer
	where left(tipnumber,4) = '1022'
	order by tipnumber

open csrtip

fetch next from csrtip into @tip
while @@FETCH_STATUS = 0
BEGIN
	set @newtip = (select lasttipnumberused from dbo.client where tipfirst = '102')
	set @newtip = cast( cast(@newtip as bigint) + 1 as varchar(15))

	update #check
		set newtip = @newtip
	where tipnumber = @tip


	update dbo.imptransaction
		set tipnumber = @newtip
	where tipnumber = @tip


	update dbo.impcustomer
		set tipnumber = @newtip
	where tipnumber = @tip

	update dbo.client
		set lasttipnumberused = @newtip
	where tipfirst = '102'


	fetch next from csrtip into @tip
END

close csrtip
deallocate csrtip


select tmp.tipnumber, tmp.transactionamt, tmp.newtip, sum(txn.transactionamt) as newamt
from #check tmp join dbo.imptransaction txn
	on tmp.newtip = txn.tipnumber
group by tmp.tipnumber, tmp.transactionamt, tmp.newtip
--having tmp.transactionamt <> sum(txn.transactionamt)
order by tmp.tipnumber




