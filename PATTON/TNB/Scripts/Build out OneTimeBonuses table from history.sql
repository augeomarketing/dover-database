--select *
--from fi_onetimebonus


--select *
--from onetimebonuses


--select distinct trancode, points
--from history
--where trancode like 'b%'
--and left(tipnumber,3) = '135'
--and points = 2500



use newtnb
GO



truncate table dbo.onetimebonuses
truncate table dbo.onetimebonuses_stage
GO


insert into dbo.onetimebonuses
(TipNumber, Trancode, AcctID, DateAwarded)
select tipnumber, trancode, acctid, histdate
from dbo.history his join dbo.bonusprogramfi bpfi
    on left(his.tipnumber,3) = bpfi.sid_BonusProgramFI_TipFirst
join dbo.bonusprogram bp
    on bp.sid_BonusProgram_ID = bpfi.sid_BonusProgram_ID
where his.trancode = bp.sid_TranType_TranCode



