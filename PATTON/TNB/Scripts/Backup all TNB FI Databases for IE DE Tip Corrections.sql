declare @DBName		nvarchar(100)
declare @SQL			nvarchar(4000)
Declare @filenumber		int
declare @GUID			uniqueidentifier

set @GUID = newid()

exec master.dbo.xp_backup_database
	@database = 'NewTNB',
	@BackupName = 'NEWTNB - BEFORE IE DE TIP CORRECTIONS',
	@FileName = 'E:\PHB TNB Backups\NEWTNB_BEFORE_IE_DE_TIP_CORRECTIONS.bak',
	@init = 1,
	@with = N'SKIP',
	@with = N'STATS = 10'


print 'Begin backing up database: NEWTNB'
declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbpi.dbnamenexl = 'newtnb'
	order by dbnamepatton
print 'Done backing up database: NEWTNB'
print ' '
print ' '

open csrDB

fetch next from csrDB into @DBName

while @@FETCH_STATUS = 0
BEGIN

	set @SQL = '
	exec master.dbo.xp_backup_database 
		@database = ' + @DBName + ',
		@GUID = ' + char(39) + cast(@GUID as nvarchar(50)) + char(39) + ',
		@backupname = ' + char(39) + @DBName + ' - Full Database Backup' + char(39) + ',
		@compressionlevel = 11,
		@filename = ' + char(39) + 'E:\PHB TNB Backups\' + replace(replace(@DBName, '[', ''), ']', '') + '_Before_IE_DE_Corrections.bak' + char(39) + ', 
		@init = 1 ,
		@with = N''SKIP'', 
		@with = N''STATS = 10''  '

	print 'Begin backing up database: ' + @dbname
	exec sp_executesql @sql
	print 'Done backing up database: ' + @dbname
	print ' '
	print ' '

--	DECLARE @i int
--	SELECT @i = position
--	FROM msdb.dbo.backupset 
--	WHERE database_name = '102BCM' AND type != 'F' AND backup_set_id = (
--	    SELECT MAX(backup_set_id) 
--	    FROM msdb.dbo.backupset 
--	    WHERE database_name = '102BCM')
--
--	exec master.dbo.xp_restore_verifyonly
--		@filename = 'E:\PHB TNB Backups\DBName_BeforePostToProd_Date.bak', 
--		@filenumber = @i
--	GO

	fetch next from csrDB into @DBName
END

close csrDB
deallocate csrDB