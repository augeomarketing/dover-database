

drop table #jerseyexpires, #jerseyfinal

create table #JerseyExpires
(tipnumber		varchar(15) primary key,
 Yr4PointsExpire	int,
 yr3PointsExpire    int)

insert into #jerseyexpires
select tipnumber, pointstoexpire, 0
from dbo.expiringpoints_fouryears



insert into #jerseyexpires
(tipnumber, yr4pointsexpire, yr3pointsexpire)
select yr3.tipnumber, 0, pointstoexpire
from dbo.expiringpoints_threeyears yr3 left outer join #jerseyexpires tmp
    on yr3.tipnumber = tmp.tipnumber
where tmp.tipnumber is null


update tmp
    set yr3pointsexpire = pointstoexpire
from #jerseyexpires tmp join dbo.expiringpoints_threeyears yr3
    on tmp.tipnumber = yr3.tipnumber




--select sum(yr4pointsexpire) yr4, sum(yr3pointsexpire) yr3
--from #jerseyexpires


--select sum(pointstoexpire) from expiringpoints_fouryears

--select sum(pointstoexpire) from expiringpoints_threeyears


create table #jerseyfinal
    (tipnumber		    varchar(15) primary key,
     name1		    varchar(40),
     name2		    varchar(40),
     address1		    varchar(40),
     city			    varchar(40),
     stateCd		    varchar(3),
     zipCode		    varchar(10),
     ccAcctNbr		    varchar(16),
     expiringPointsThreeYears	  int,
     expiringPointsFourYears	  int)

insert into #jerseyFinal
(tipnumber, name1, name2, address1, city, statecd, zipcode, expiringpointsthreeyears, expiringpointsfouryears)
select tmp.tipnumber, acctname1, acctname2, address1, city, state, zipcode, yr3pointsexpire, yr4pointsexpire
from #jerseyexpires tmp join dbo.customer cus
    on tmp.tipnumber = cus.tipnumber


update #jerseyFinal
    set ccacctnbr = (select top 1 acctid from dbo.affiliat aff where aff.tipnumber = #jerseyfinal.tipnumber order by dateadded desc)


select *
from #jerseyfinal


select *
from affiliat
where tipnumber = '120000000002572'

select *
from history
where tipnumber = '120000000002572'