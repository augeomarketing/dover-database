select tipnumber, pointsend, currentend, pointsend-currentend as variance
into #bonusfix
from monthly_audit_errorfile


select * from #bonusfix

begin tran

update cus
	set RunAvailable = runavailable + bf.variance,
		RUNBALANCE = runbalance + bf.variance
from #bonusfix bf join [135TexasBayArea].dbo.customer cus
	on bf.tipnumber = cus.tipnumber

rollback tran  --  commit tran

