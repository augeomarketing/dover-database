
drop table #points

create table #points
    (tipnumber		varchar(15) primary key,
     points		int)

insert into #points
select tipnumber, sum(points * ratio)
from dbo.history
where histdate = '09/30/2009'
and 
    (trancode like '6%' or trancode like '3%')
group by tipnumber



/*  ADD ROWS TO HISTORY */
insert into [132EmeryFCU].dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, Ratio)
select tipnumber, null, '11/05/2009', 'IE', 1, tmp.Points, tt.description, tt.ratio
from #points tmp join rewardsnow.dbo.trantype tt
	on 'IE' = tt.trancode


/* UPDATE Customer balances */
update cus
	set runavailable = runavailable + tmp.points,
		runbalance = runbalance + tmp.points
from #points tmp join [132EmeryFCU].dbo.customer cus
	on tmp.tipnumber = cus.tipnumber



--/** Update web customer */
update web
	set earnedbalance = earnedbalance + tmp.points,
		availablebal = availablebal + tmp.points
from #points tmp join rn1.newtnb.dbo.customer web
	on tmp.tipnumber = web.tipnumber


