
use [148citizens]

/******************************************************************************/
/*    This will Create the Expering Points Table                              */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
--CREATE PROCEDURE spExpirePointsExtract @MonthBeg VARCHAR(10), @NumberOfYears int  AS   

declare @MonthBeg VARCHAR(10)
SET @MonthBeg = '12/01/2008'
declare @NumberOfYears int
set @NumberOfYears = '3' 
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
Declare @PointsToExpireNext int
Declare @AddPointsNext int
Declare @PointsprevExpired int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intmonthnext int
declare @intyear int
declare @ExpireDate DATETIME
declare @MonthEndDate DATETIME
declare @MonthEndDatenext DATETIME


set @MonthEndDate = cast(@MonthBeg as datetime)
set @MonthEndDate = Dateadd(month, 1, @MonthBeg)
set @MonthEndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDate)),121)
set @MonthEndDatenext = cast(@MonthBeg as datetime)
set @MonthEndDatenext = Dateadd(month, 2, @MonthBeg)
--set @MonthEndDatenext = Dateadd(month, 1, @MonthBeg)
--set @MonthEndDatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDatenext)),121)
print 'month end date'
print @MonthEndDate
print 'month end date next'
print @MonthEndDatenext
set @expirationdate = cast(@MonthEndDate as datetime)
set @expirationdatenext = cast(@MonthEndDatenext as datetime)
set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)
--set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -@NumberOfYears, @expirationdate)),121)
print 'Expiration Date: ' + @expirationdate
set @expirationdatenext = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdatenext)),121)
set @expirationdatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdatenext)),121)
 
/*set @ExpireDate = convert(datetime, @DateOfExpire)
set @ExpireDate = @ExpireDate +   ' 23:59:59.997'
SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intmonthnext = @intmonth + 1
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 3
set @expirationdate = (rtrim(@intYear) + '-' + rtrim(@intmonth) + '-' + rtrim(@intday) +   ' 23:59:59.997')
set @expirationdatenext = (rtrim(@intYear) + '-' + rtrim(@intmonthnext) + '-' + rtrim(@intday) +   ' 23:59:59.997')
*/


/*print 'NumberOfYears'
print @NumberOfYears

print '@expirationdate'
print @expirationdate 
print '@expirationdatenext'
print @expirationdatenext 
*/

      TRUNCATE TABLE expiringpoints

      INSERT   into expiringpoints
       SELECT tipnumber, '0' as addpoints,
      '0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
      '0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
      --from history
      from customer
      --where histdate < @expirationdate and trancode like('t%')
      group by tipnumber

      UPDATE expiringpoints  
      SET addpoints = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE trancode  like 't%'
             AND TIPNUMBER = expiringpoints.TIPNUMBER
             and histdate =< @expiredate) 
      WHERE EXISTS  (SELECT *
             FROM HISTORY 
             WHERE trancode  like 't%'
             AND TIPNUMBER = expiringpoints.TIPNUMBER
             and histdate =< @expiredate) 


      UPDATE expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = expiringpoints.TIPNUMBER) 
      WHERE EXISTS  (SELECT *
             FROM HISTORY 
             WHERE
             (trancode  like('R%') or
              trancode = 'IR')  
             AND TIPNUMBER = expiringpoints.TIPNUMBER) 


      UPDATE expiringpoints  
      SET prevExpired = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
              trancode = 'XP'   
             AND TIPNUMBER = expiringpoints.TIPNUMBER) 
      WHERE EXISTS  (SELECT *
             FROM HISTORY 
             WHERE
              trancode = 'XP'  
             AND TIPNUMBER = expiringpoints.TIPNUMBER       and   histdate > @expirationdate)
 
      UPDATE expiringpoints  
      SET addpointsnext = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
             histdate < @expirationdatenext and (trancode not like('R%') and
               trancode <> 'IR') 
             AND TIPNUMBER = expiringpoints.TIPNUMBER)
 
      UPDATE expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM HISTORY 
             WHERE
             trancode = 'XF'
             AND TIPNUMBER = expiringpoints.TIPNUMBER)

      UPDATE expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)

      UPDATE expiringpoints  
      SET POINTSTOEXPIRE = ((ADDPOINTS + REDPOINTS)  - PREVEXPIRED)

      UPDATE expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

      UPDATE expiringpoints  
      SET POINTSTOEXPIRE = '0'
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < '0' 

      UPDATE expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))



      UPDATE expiringpoints  
      SET POINTSTOEXPIREnext = '0'
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < '0' 


      UPDATE expiringpoints  
      SET EXPTODATE = '0'
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < '0' 


/*    update Monthly_Statement_File             
      set
          POINTSTOEXPIRE = XP.POINTSTOEXPIRE,
          DATEOFEXPIRATION = @ExpireDate,
          PointsprevExpired = xp.PointsprevExpired,
          PointstoExpireNext = xp.PointstoExpireNext
      from dbo.expiringpoints as XP
      inner JOIN dbo.Monthly_Statement_File as BAS
      on XP.TIPNUMBER = BAS.TIPNUMBER  
      where XP.TIPNUMBER in (select BAS.TIPNUMBER from Monthly_Statement_File)
*/
GO

