

--select *
--from [135texasbayarea].dbo.customerdeleted
--where tipnumber = '135000000003745'


--select sum(points * ratio)
--from [135texasbayarea].dbo.historydeleted
--where tipnumber = '135000000003745'


declare @tip		varchar(15)

set @tip = '167000000001995'


begin tran

	insert into dbo.customer
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select distinct TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	from dbo.customerdeleted
	where tipnumber = @tip

	delete from dbo.customerdeleted where tipnumber = @tip


	insert into dbo.affiliat
	(TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select distinct TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned
	from dbo.affiliatdeleted
	where tipnumber = @tip

	delete from dbo.affiliatdeleted where tipnumber = @tip


	insert into dbo.history
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage)
	select distinct TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage
	from dbo.historydeleted
	where tipnumber = @tip


	--insert into dbo.bkup_histdeleted
	--select *
	--from dbo.historydeleted 
	--where tipnumber = @tip

	delete from dbo.historydeleted where tipnumber = @tip


commit tran  -- rollback

--
-- Now add them into rn1


declare @tip		varchar(15)

set @tip = '167000000001995'

insert into rn1.newtnb.dbo.customer
(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status, Segment, city, state)
select TipNumber, TipFirst, TipLast, acctName1, acctName2, acctName3, acctName4, acctName5, Address1, Address2, Address3, city + ', ' + state + '  ' + zipcode, ZipCode, runbalance, runRedeemed, runavailable, Status, null, city, state
from dbo.customer
where tipnumber = @tip

insert into rn1.newtnb.dbo.account
(TipNumber, LastName, LastSix)
select tipnumber, lastname, right(acctid,6)
from dbo.affiliat
where tipnumber = @tip

insert into rn1.newtnb.dbo.[1security]
(TipNumber, username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, RegDate, Nag)
select TipNumber, username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, RegDate, Nag
from rn1.zznewtnb.dbo.[1security]
where tipnumber = @tip

if @@rowcount = 0 -- if not found in backup of 1security, add tip so they can register
	insert into rn1.newtnb.dbo.[1security] (tipnumber) values(@tip)

	insert into rn1.newtnb.dbo.[1security] (tipnumber) values( '167000000001995' )

