declare @db		nvarchar(100)
declare @sql		nvarchar(4000)

if exists(select 1 from newtnb.dbo.sysobjects where [name] = 'wrkTipstofix' and xtype = 'U')
	drop table newtnb.dbo.wrktipstofix


create table newtnb.dbo.wrkTipsToFix
	(tipnumber			nvarchar(15) primary key)
			

declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null
	and dbnamenexl = 'newtnb'

open csrDB

fetch next from csrDB into @DB
while @@FETCH_STATUS = 0
BEGIN
	
	set @sql = '
	insert into newtnb.dbo.wrktipstofix
	select distinct cus.tipnumber
	--cus.status, cus.statusdescription, sts.statusdescription, imp.externalsts, imp.internalsts, imp.rewardsnowsts
	from ' + @db + '.dbo.customer cus join dbo.status sts
		on cus.status = sts.status
	join newtnb.dbo.impcustomer imp
		on cus.tipnumber = imp.tipnumber
	where cus.statusdescription != sts.statusdescription
		and cus.status != imp.rewardsnowsts
		and imp.rewardsnowsts = ''A''  '
	print @sql
	exec sp_executesql @sql


	set @sql = '
	insert into [167395-db1].newtnb.dbo.account
	(TipNumber, LastName, LastSix, SSNLast4)
	select aff.tipnumber, aff.lastname, right(acctid,6) lastsix, null
	from ' + @DB + '.dbo.affiliat aff join newtnb.dbo.wrktipstofix wrk
		on aff.tipnumber = wrk.tipnumber
	left outer join [167395-db1].newtnb.dbo.account acct
		on aff.tipnumber = acct.tipnumber
		and aff.lastname = acct.lastname
		and right(aff.acctid,6) = acct.lastsix
	where acct.tipnumber is null'

	print @sql
	exec sp_executesql @sql


	set @sql = '
	insert into [167395-db1].newtnb.dbo.customer
	(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, 
	 Address1, Address2, Address3, CityStateZip, ZipCode, EarnedBalance, Redeemed, AvailableBal, Status)
	select cus.tipnumber, left(cus.tipnumber,3), right(cus.tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
		cus.address1, cus.address2, cus.address3, cus.address4, cus.zipcode, RUNBALANCE, RunRedeemed, RunAvailable, ''A''
	from ' + @db + '.dbo.customer cus join newtnb.dbo.wrktipstofix wrk
		on cus.tipnumber = wrk.tipnumber
	left outer join [167395-db1].newtnb.dbo.customer web
		on web.tipnumber = cus.tipnumber
	where web.tipnumber is null'
	print @sql
	exec sp_executesql @sql


	set @sql = '
		update cus
			set status = ''A''
		from ' + @db + '.dbo.customer cus join newtnb.dbo.wrktipstofix wrk
			on cus.tipnumber = wrk.tipnumber'
	print @sql
	exec sp_executesql @sql

print ' '
print 'Done processing database: ' + @db
print ' '
print '------------------------------------------------------------------------------------------------------------------'
print ' '

	fetch next from csrDB into @DB
END

close csrDB
deallocate csrDB



	insert into [167395-db1].newtnb.dbo.[1security]
	(TipNumber, username, Password, SecretQ, SecretA, EmailStatement, Email, Email2, EMailOther, RegDate, Nag)
	select bkup.TipNumber, bkup.username, bkup.Password, bkup.SecretQ, bkup.SecretA, bkup.EmailStatement, bkup.Email, bkup.Email2, bkup.EMailOther, bkup.RegDate, bkup.Nag
	from [167395-db1].tnb_b4_posttoweb.dbo.[1security] bkup join newtnb.dbo.wrktipstofix wrk
		on bkup.tipnumber = wrk.tipnumber
	left outer join [167395-db1].newtnb.dbo.[1security] sec
		on sec.tipnumber = bkup.tipnumber
	where sec.tipnumber is null

