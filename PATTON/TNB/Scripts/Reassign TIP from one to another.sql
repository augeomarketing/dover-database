declare @newtip			varchar(15)
declare @OldTip			varchar(15)

set @oldtip = '105999999999802'

set @newtip = (select lasttipnumberused from dbo.client)

print 'OLD Tip:  ' + @oldtip
set @newtip = left(@newtip, 12) + cast(  cast(right(@newtip,3) as int) + 1 as varchar(7))
print 'NEW TIP:  ' + @newtip


begin tran

-- Update last tip used in client table
update dbo.client
	set lasttipnumberused = @newtip

-- Upate history
update dbo.history
	set tipnumber = @newtip
where tipnumber = @oldtip


-- Update Affiliat
update dbo.affiliat
	set tipnumber = @newtip
where tipnumber = @oldtip


-- update customer
update dbo.customer
	set tipnumber = @newtip
where tipnumber = @oldtip


-- beginning balance
update dbo.beginning_balance_table
	set tipnumber = @newtip
where tipnumber = @oldtip


-- Update fullfillment db
update fullfillment.dbo.main
	set tipnumber = @newtip
where tipnumber = @oldtip


-- OnlineHistoryWork
update OnlineHistoryWork.dbo.Portal_Adjustments
	set tipnumber = @newtip
where tipnumber = @oldtip


update OnlineHistoryWork.dbo.OnlHistory
	set tipnumber = @newtip
where tipnumber = @oldtip


--
--
--
commit tran

--
-- NOW UPDATE RN1
-- IMPORTANT:  CHANGE THE QUERY CONNECTION!!!
--
declare @newtip			varchar(15)
declare @OldTip			varchar(15)

set @oldtip = '105999999999802'
set @NewTip = '105000000031317'

begin tran

update newtnb.dbo.[1Security]
	set tipnumber = @newtip
where tipnumber = @oldtip


update newtnb.dbo.account
	set tipnumber = @newtip
where tipnumber = @oldtip


update newtnb.dbo.customer
	set tipnumber = @newtip
where tipnumber = @oldtip


update newtnb.dbo.OnlHistory
	set tipnumber = @newtip
where tipnumber = @oldtip


update newtnb.dbo.statement
	set TravNum = @newtip
where travnum = @oldtip



update onlinehistorywork.dbo.OnlHistory
	set tipnumber = @newtip
where tipnumber = @oldtip


update onlinehistorywork.dbo.portal_adjustments
	set tipnumber = @newtip
where tipnumber = @oldtip


commit tran


select *
from [1security]
where tipnumber = '105000000031317'


update customer
set tiplast = right(tipnumber,12)
where tipnumber = '105000000031317'
