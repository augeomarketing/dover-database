

begin tran

update customer 
set runredeemed = (select sum(points) from history where customer.tipnumber = history.tipnumber and trancode like 'r%') 
where tipnumber in (select tipnumber from history where trancode like 'r%') 
and tipnumber in (select tipnumber from tnb.dbo.phb_undeletes where left(tipnumber,3) = '119')
 
update customer 
set runavailable = (select sum(points*ratio) from history where customer.tipnumber = history.tipnumber) 
where tipnumber in (select tipnumber from history) 
and tipnumber in (select tipnumber from tnb.dbo.phb_undeletes where left(tipnumber,3) = '119')

 
update customer 
set runbalance = runavailable + runredeemed 
where tipnumber in (select tipnumber from tnb.dbo.phb_undeletes where left(tipnumber,3) = '119')

rollback tran  --  commit tran



select aff.*
into b4_aff
from dbo.affiliat aff join tnb.dbo.phb_undeletes phb on aff.tipnumber = phb.tipnumber


select b4.tipnumber, b4.acctid, b4.ytdearned YTDEarned_B4, aff.ytdearned YTDEarned_Aft
from b4_aff b4 join dbo.affiliat aff
	on b4.acctid = aff.acctid
join tnb.dbo.phb_undeletes phb
	on phb.tipnumber = b4.tipnumber





begin tran

update aff
	set ytdearned = isnull((select sum(points * ratio) 
					from dbo.history his 
					where aff.acctid = his.acctid and (trancode like '3%' or trancode like '6%')
					and year(histdate) = 2008), 0)

from dbo.affiliat aff join tnb.dbo.phb_undeletes phb
	on aff.tipnumber = phb.tipnumber
where left(phb.tipnumber,3) = '119'
and aff.acctid in (select acctid from dbo.history where year(histdate) = 2008 and (trancode like '3%' or trancode like '6%'))


rollback tran  --  commit tran



