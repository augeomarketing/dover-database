/*
MB	Manual Bonus
NW	New Account Bonus
RS	Redeem for Tunes
RU	Desc. Is QTR - 900454, Cert no. 76721                       - so I assume Redeem Travel?
TR	Transfer?
tx	Transfer Points
*/
---------------------------------
drop table #trancodexref

create table #trancodexref
	(oldtrancode		varchar(2) primary key,
	 newtrancode		varchar(2))

create index ix_tmptrancodexref2 on #trancodexref(newtrancode, oldtrancode)

insert into #trancodexref
select 'MB', 'BI'
union
select 'NW', 'BN'
union
select 'TR', 'TP'
union
select 'tx', 'TP'
union
select 'RU', 'RT'
union
select 'RS', 'RD'

if exists(select 1 from newtnb.dbo.sysobjects where name = 'HISTORY_Rows_With_Corrected_TranCodes' and xtype = 'U')
	truncate table dbo.HISTORY_Rows_With_Corrected_TranCodes
else
	CREATE TABLE [newtnb].[dbo].[HISTORY_Rows_With_Corrected_TranCodes](
		[TIPNUMBER] [varchar](15) NOT NULL,
		[ACCTID] [varchar](25) NULL,
		[HISTDATE] [datetime] NULL,
		[OLDTRANCODE] [varchar](2) NULL,
		[NEWTRANCODE] [varchar](2) NULL,
		[TranCount] [int] NULL,
		[POINTS] [int] NULL,
		[Description] [varchar](50) NULL,
		[SECID] [varchar](50) NULL,
		[Ratio] [numeric](18, 2) NULL,
		[Overage] [int] NULL
	) ON [PRIMARY]






declare @db			varchar(50)
declare @SQL			nvarchar(4000)

declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton) as dbnamepatton
	from rewardsnow.dbo.dbprocessinfo
	where dbnamenexl = 'newtnb'

open csrDB

fetch next from csrDB into @DB
while @@FETCH_STATUS = 0
BEGIN

	set @SQL = 'create index ix_History_temp__trancode_Tipnumber on ' + @DB + '.dbo.history(trancode, tipnumber)'
	exec sp_executesql @SQL

	set @SQL = 
	'insert into newtnb.dbo.HISTORY_Rows_With_Corrected_TranCodes
	 (TIPNUMBER, ACCTID, HISTDATE, OLDTRANCODE, NEWTRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	 select tipnumber, acctid, histdate, trancode, newtrancode, trancount, points, description, secid, ratio, overage
	 from ' + @DB + '.dbo.history his join #trancodexref xref
		on his.trancode = xref.oldtrancode'
	exec sp_executesql @SQL


	set @SQL = 
	'update his
		set trancode = xref.newtrancode
	 from ' + @db + '.dbo.history his join #trancodexref xref
		on his.trancode = xref.oldtrancode'

	print @SQL

	--begin tran
	exec sp_executesql @SQL
	--rollback tran

	set @SQL = 'use ' + @DB + '; DROP INDEX history.[ix_History_temp__trancode_TipNumber]'
	exec sp_executesql @SQL


	fetch next from csrDB into @DB
END

close csrDB
deallocate csrDB


