use newtnb
go


drop table #hist
drop table #TNBSum

create table #hist
	(tipfirst			varchar(3) primary key,
	 Points			numeric(18,4) not null)

create table #TNBSum
	(TipFirst			varchar(3) primary key,
	 AssocNbrs		varchar(100) not null,
	 TNBSumPoints		numeric(18,4) not null)


insert into #hist
select left(tipnumber,3) tipfirst, sum(points * ratio) points
from dbo.history_stage
where secid = 'new'
group by left(tipnumber,3)


insert into #TNBSum
select isnull(ass.tipfirst, '129') TipFirst, dbo.fnPivotAssocNbrstoString (isnull(ass.tipfirst, '129')) AssocNbrs, sum(totaltxnamount) totaltxnamount
from dbo.imp_tnbsummary tnb left outer join dbo.assoc ass
	on cast(tnb.assocnbr as varchar(3)) = cast(ass.assocnum as varchar(3))
group by isnull(ass.tipfirst, '129'), dbo.fnPivotAssocNbrstoString (isnull(ass.tipfirst, '129'))
order by isnull(ass.tipfirst, '129')


select tnb.assocnbrs AssocNbrs, tnb.tipfirst TipFirst, his.points RNI_HistPoints, tnb.tnbsumpoints TNB_Summary_Points,
		(his.points - tnb.tnbsumpoints) Variance, cast(((his.points - tnb.tnbsumpoints) / his.points) * 100 as numeric(18,2)) as PctVariance
from #TNBSum tnb join #hist his
	on tnb.tipfirst = his.tipfirst
order by tnb.tipfirst


--
--select ass.assocnum, isnull(ass.tipfirst, '129') tipfirst, his.points as historyPoints, sum(totaltxnamount) totaltxnamount
--from imp_tnbsummary tnb left outer join dbo.assoc ass
--	on cast(tnb.assocnbr as varchar(3)) = cast(ass.assocnum as varchar(3))
--join #hist his
--	on his.tipfirst = isnull(ass.tipfirst, '129')
--group by ass.assocnum, isnull(ass.tipfirst, '129'), his.points
--order by 
--isnull(ass.tipfirst, '129')
