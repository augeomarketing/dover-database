
--  74,957,371  looking for 66,394 points
--  Beg Pts Outstanding:      74,890,977
--  beg pts from rptliability 74,957,371

-- 74,837,371

select tipnumber, monthbeg3
into #bbt
from newtnb.dbo.beginning_balance_table 
where left(tipnumber,3) = '165' and monthbeg3 != 0

select sum(monthbeg3)
from #bbt

select tipnumber, year(histdate) yr, trancode, sum(points * ratio) pts
into #current
from history
where histdate < '03/01/2009'
group by tipnumber, year(histdate), trancode
order by tipnumber, year(histdate), trancode


select tipnumber, year(histdate) yr, trancode, sum(points * ratio) pts
into #newtnb
from newtnb.dbo.history
where left(tipnumber,3) = '165' and histdate < '03/01/2009'
group by tipnumber, year(histdate), trancode
order by tipnumber, year(histdate), trancode

select *
from #newtnb tnb left outer join #current cur
	on tnb.tipnumber = cur.tipnumber
	and tnb.yr = cur.yr
	and tnb.trancode = cur.trancode
where cur.tipnumber is null



select *
from historydeleted
where histdate > '03/01/2009'

select * from customerdeleted where tipnumber = '165000000004112'


insert into customer
(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
select TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, 'A', StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
from customerdeleted
where tipnumber = '165000000002812'

delete 
from customerdeleted
where tipnumber = '165000000002812'

insert into affiliat
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
select distinct ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
from affiliatdeleted
where tipnumber = '165000000002812'

delete from affiliatdeleted where tipnumber = '165000000002812'


insert into history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select distinct TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
from historydeleted
where tipnumber = '165000000002812'

delete from historydeleted
where tipnumber = '165000000002812'


select * from customer
where tipnumber = '165000000004112'

drop table #h1, #h2

select tipnumber, histdate, trancode, sum(points * ratio) pts
into #h1
from history
where trancode like 'r%' and trancode != 'rq' and histdate < '03/01/2009'
group by tipnumber, histdate, trancode
order by tipnumber, histdate, trancode


select tipnumber, histdate, trancode, sum(points * ratio) pts
into #h2
from newtnb.dbo.history
where left(tipnumber,3) = '165' and trancode like 'r%'  and trancode != 'rq' and histdate < '03/01/2009'
group by tipnumber, histdate, trancode
order by tipnumber, histdate, trancode


select *
from #h2 h2 left outer join #h1 h1
	on h2.tipnumber = h1.tipnumber
	and h2.histdate = h1.histdate
	and h2.trancode = h1.trancode
where h1.tipnumber is null


select * from customer where tipnumber = '165000000002812'
select * from newtnb.dbo.customer where tipnumber = '165000000002812'

select * from customerdeleted where tipnumber = '165000000002812'



select *
from historydeleted
where trancode like 'r%'


select *
from history
where tipnumber = '165000000003845'


select sum(points * ratio) from history

select sum(runavailable) from customer

-- H:  75,316,061
-- C:  75,260,061



select tipnumber, histdate, trancode, sum(points * ratio) pts , count(*) ctr
from history
where histdate > '03/01/2009' and trancode like 'r%'
group by tipnumber, histdate, trancode
having count(*) > 1
order by tipnumber, histdate, trancode


select tipnumber, histdate, trancode, sum(points * ratio) pts, count(*) ctr
from newtnb.dbo.history
where left(tipnumber,3) = '165' and histdate > '03/01/2009'
group by tipnumber, histdate, trancode
having count(*) > 1
order by tipnumber, histdate, trancode


select *
from history
where tipnumber = '165000000003897'
select *
from zz165.dbo.historydeleted
where tipnumber = '165000000003897'

insert into history
select top 1 *
from history
where tipnumber = '165000000003897'
and histdate = '2009-03-29 15:30:00.000'




select tipnumber, sum(points * ratio) pts
into #hist
from history
group by tipnumber

select *
from customer c left outer join #hist h
	on c.tipnumber = h.tipnumber
--where c.runavailable != h.pts
where h.tipnumber is null

select *
from #hist h left outer join customer c
on h.tipnumber = c.tipnumber
where c.tipnumber is null

select count(*) from customer where runavailable != 0
select count(*) from #hist where pts != 0

