
declare @SQL			nvarchar(4000)
declare @RC			int

declare @OLDDB			nvarchar(100)
declare @NEWDB			nvarchar(100)

declare @OLDTip		nvarchar(15)
declare @NewTip		nvarchar(15)
declare @LastUsedNewTip	nvarchar(15)
declare @TipCounter		bigint

declare @OLDTipFirst	nvarchar(3)
declare @NEWTipFirst	nvarchar(3)

set @OLDTipFirst = '126' -- Sterlent
set @NEWTipFirst = '129' -- Various/Generic

set @OLDDB = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @OLDTipFirst)
set @NEWDB = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @NEWTipFirst)

print 'Old FI Database: ' + @OLDDB
print 'New FI Database: ' + @NEWDB


-------
-- On Patton, need to migrate:
--		1) Customer
--		2) Affiliat
--		3) History
--		4) OneTimebonuses (if applicable) **NEED TO IMPLEMENT
--		5) OnlHistory and Portal_Adjustments in OnlineHistoryWork database
--		6) FulFillmentDB
--			a) Main
--
	
--
-- On 167395-DB1 (RN1), need to migrate:
--		1) [1Security]
--		2) Account
--		3) Customer
--		4) OnlHistory
--		5) Statement
--		


if exists(select 1 from rewardsnow.dbo.sysobjects where name = 'wrkConvertFITipXref' and xtype = 'U')
	truncate table rewardsnow.dbo.wrkConvertFITipXref
else
create table rewardsnow.dbo.wrkConvertFITipXref
	(OldTipNumber			varchar(15) primary key,
	 NewTipNumber			varchar(15))

--
-- Get last Tip Number used from the Destination FI's database
set @SQL = 'Set @LastUsedNewTip = (select top 1 LastTipNumberUsed from ' + @NEWDB + '.dbo.client)'
exec @RC = sp_executesql @SQL, N'@LastUsedNewTip nvarchar(15) OutPut', @LastUsedNewTip = @LastUsedNewTip OutPut

set @SQL = 'declare csrOLDTips cursor fast_forward for
			select tipnumber 
			from ' + @OLDDB + '.dbo.customer order by tipnumber'

exec sp_executesql @SQL

open csrOLDTips

fetch next from csrOLDTips into @OldTip
while @@FETCH_STATUS = 0
BEGIN

	set @TipCounter = cast( right(@LastUsedNewTip, 12) as bigint) + 1
	set @NewTip = @NewTipFirst + right('000000000000' + cast(@TipCounter as nvarchar(12)), 12)
	set @LastUsedNewTip = @NewTip

	--
	-- Build xref table of new tip vs. old tip.  This will be needed for converting tables on the web database
	insert into rewardsnow.dbo.wrkConvertFITipXref
	values(@OldTip, @NewTip)
	if @@ERROR != 0
		goto ERROR01

	goto OKAY01

ERROR01:
	print 'ERROR '
	goto nextrec01

OKAY01:


nextrec01:

	fetch next from csrOLDTips into @OldTip
END

close csrOLDTips
deallocate csrOLDTips

set @SQL = 'Update ' + @NEWDB + '.dbo.client set lasttipnumberused = ' + char(39) + @LastUsedNewTip + char(39)
exec @RC = sp_executesql @SQL

select *
from rewardsnow.dbo.wrkConvertFITipXref

--
-- Conversion table is now built.  Now begin converting tips in the databases
--

set @RC = 0 -- Initialize the Return Code variable


--
-- Convert Customer
set @SQL = '
insert into ' + @NEWDB + '.dbo.Customer
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, 
 TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
 NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)

select newtipnumber, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, 
 left(newtipnumber,3), right(newtipnumber,12), ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
 NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
from ' + @OLDDB + '.dbo.customer cus join rewardsnow.dbo.wrkConvertFITipXref xref
	on cus.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR


--
-- Convert History
set @SQL = '
insert into ' + @NEWDB + '.dbo.history
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select newtipnumber, AcctId, HistDate, TranCode, TranCount, POINTS, Description, SECID, Ratio, Overage
from ' + @OLDDB + '.dbo.history his join rewardsnow.dbo.wrkConvertFITipXref xref
	on his.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR


--
-- Convert Affiliat
set @SQL = '
insert into ' + @NEWDB + '.dbo.affiliat
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, affiliatflag)
select acctid, newtipnumber, accttype, dateadded, secid, acctstatus, accttypedesc, lastname, YTDEarned,
		CustId, affiliatflag
from ' + @OLDDB + '.dbo.affiliat aff join rewardsnow.dbo.wrkConvertFITipXref xref
	on aff.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR


--
-- Convert Beginning_Balance_Table
set @SQL = '
insert into ' + @NEWDB + '.dbo.Beginning_Balance_Table
(Tipnumber, MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12)

select newtipnumber, MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12
from ' + @OLDDB + '.dbo.Beginning_Balance_Table bbt  join rewardsnow.dbo.wrkConvertFITipXref xref
	on bbt.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR


--
-- One Time bonuses - need to check for existence!!



--
-- convert online history work - OnlHistory
update oh
	set tipnumber = newtipnumber,
		DBNum = left(newtipnumber,3)
from onlinehistorywork.dbo.onlhistory oh join rewardsnow.dbo.wrkConvertFITipXref xref
	on oh.tipnumber = xref.OldTipNumber


--
-- Convert Portal Adjustments
update pa
	set tipfirst = left(NewTipNumber,3),
		TipNumber = NewTipNumber
from onlinehistorywork.dbo.Portal_Adjustments pa join  rewardsnow.dbo.wrkConvertFITipXref xref
	on pa.tipnumber = xref.OldTipNumber


--
-- Convert Portal Combines



--
-- Convert the Fullfillment MAIN table
update m
	set tipnumber = NewTipNumber,
		TipFirst = left(NewTipNumber,3)
from fullfillment.dbo.main m  join  rewardsnow.dbo.wrkConvertFITipXref xref
	on m.tipnumber = xref.OldTipNumber


goto OKAY



ERROR:
	print 'ERROR!!!'
	goto nextrec

OKAY:


nextrec:



----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--
-- Now convert the deleted accounts
--
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------


if exists(select 1 from rewardsnow.dbo.sysobjects where name = 'wrkConvertFITipXrefDeleted' and xtype = 'U')
	truncate table rewardsnow.dbo.wrkConvertFITipXrefDeleted
else
create table rewardsnow.dbo.wrkConvertFITipXrefDeleted
	(OldTipNumber			varchar(15) primary key,
	 NewTipNumber			varchar(15))

--
-- Get last Tip Number used from the Destination FI's database
set @SQL = 'Set @LastUsedNewTip = (select top 1 LastTipNumberUsed from ' + @NEWDB + '.dbo.client)'
exec @RC = sp_executesql @SQL, N'@LastUsedNewTip nvarchar(15) OutPut', @LastUsedNewTip = @LastUsedNewTip OutPut

set @SQL = 'declare csrOLDTips cursor fast_forward for
			select distinct tipnumber 
			from ' + @OLDDB + '.dbo.customerdeleted order by tipnumber'

exec sp_executesql @SQL

open csrOLDTips

fetch next from csrOLDTips into @OldTip
while @@FETCH_STATUS = 0
BEGIN

	set @TipCounter = cast( right(@LastUsedNewTip, 12) as bigint) + 1
	set @NewTip = @NewTipFirst + right('000000000000' + cast(@TipCounter as nvarchar(12)), 12)
	set @LastUsedNewTip = @NewTip

	--
	-- Build xref table of new tip vs. old tip.  This will be needed for converting tables on the web database
	insert into rewardsnow.dbo.wrkConvertFITipXrefDeleted
	values(@OldTip, @NewTip)
	if @@ERROR != 0
		goto ERROR02

	goto OKAY02

ERROR02:
	print 'ERROR '
	goto nextrec02

OKAY02:


nextrec02:

	fetch next from csrOLDTips into @OldTip
END

close csrOLDTips
deallocate csrOLDTips

set @SQL = 'Update ' + @NEWDB + '.dbo.client set lasttipnumberused = ' + char(39) + @LastUsedNewTip + char(39)
exec @RC = sp_executesql @SQL


--
-- Conversion table is now built.  Now begin converting tips in the databases
--



set @RC = 0 -- Initialize the Return Code variable


--
-- Convert CustomerDeleted
set @SQL = '
insert into ' + @NEWDB + '.dbo.CustomerDeleted
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, 
 TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
 NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)

select newtipnumber, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, 
 left(newtipnumber,3), right(newtipnumber,12), ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, 
 NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
from ' + @OLDDB + '.dbo.customerdeleted cus join rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on cus.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR03


--
-- Convert History
set @SQL = '
insert into ' + @NEWDB + '.dbo.historydeleted
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select newtipnumber, AcctId, HistDate, TranCode, TranCount, POINTS, Description, SECID, Ratio, Overage
from ' + @OLDDB + '.dbo.historydeleted his join rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on his.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR03


--
-- Convert Affiliat
set @SQL = '
insert into ' + @NEWDB + '.dbo.affiliatdeleted
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, datedeleted)
select acctid, newtipnumber, accttype, dateadded, secid, acctstatus, accttypedesc, lastname, YTDEarned,
		CustId, datedeleted
from ' + @OLDDB + '.dbo.affiliatdeleted aff join rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on aff.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR03


--insert into [129R2UGeneric].dbo.affiliatdeleted
--(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, datedeleted)
--select acctid, newtipnumber, accttype, dateadded, secid, acctstatus, accttypedesc, lastname, YTDEarned,
--		CustId, datedeleted
--from [126Sterlent].dbo.affiliatdeleted aff join rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
--	on aff.tipnumber = xref.OldTipNumber




--
-- Convert Beginning_Balance_Table
set @SQL = '
insert into ' + @NEWDB + '.dbo.Beginning_Balance_Table
(Tipnumber, MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12)

select newtipnumber, MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12
from ' + @OLDDB + '.dbo.Beginning_Balance_Table bbt  join rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on bbt.tipnumber = xref.OldTipNumber'
exec @RC = sp_executesql @SQL
if @RC != 0
	goto ERROR03


--
-- One Time bonuses - need to check for existence!!



--
-- convert online history work - OnlHistory
update oh
	set tipnumber = newtipnumber,
		DBNum = left(newtipnumber,3)
from onlinehistorywork.dbo.onlhistory oh join rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on oh.tipnumber = xref.OldTipNumber


--
-- Convert Portal Adjustments
update pa
	set tipfirst = left(NewTipNumber,3),
		TipNumber = NewTipNumber
from onlinehistorywork.dbo.Portal_Adjustments pa join  rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on pa.tipnumber = xref.OldTipNumber

--
-- Portal Combines


--
-- Convert the Fullfillment MAIN table
update m
	set tipnumber = NewTipNumber,
		TipFirst = left(NewTipNumber,3)
from fullfillment.dbo.main m  join  rewardsnow.dbo.wrkConvertFITipXrefDeleted xref
	on m.tipnumber = xref.OldTipNumber


goto OKAY03


ERROR03:
	print 'ERROR!!!'
	goto nextrec03

OKAY03:


nextrec03:

--
-- Remove from dbprocessinfo
delete from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @OLDTipFirst

--
-- Remove from rptctlclients
delete from rewardsnow.dbo.rptctlclients
	where clientnum = @OLDTipFirst


--
-- Remove from Login.dbo.client
delete from login.dbo.client
	where tipfirst = @OLDTipFirst

--
-- Remove from rewardsnow.dbo.clients
delete from rewardsnow.dbo.clients
	where tipfirst = @OLDTipFirst


--
-- Remove from rewardsnow.dbo.clientdata
delete from rewardsnow.dbo.clientdata
	where tipfirst = @OLDTipFirst







----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--
-- Now convert the tables on the web - 167395-db1
--
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

--
-- IMPORTANT - Copy the rewardsnow.dbo.wrkConvertFITipXref table up to the 167395-DB1 server BEFORE RUNNING TIHS!!!

-- On 167395-DB1 (RN1), need to migrate:
--		1) [1Security]
--		2) Account
--		3) Customer
--		4) OnlHistory
--		5) Statement
--		

--
-- NOTE:  IMPORT FILES FROM PATTON TO 167395-DB1 BEFORE CONTINUING!!!!!!!!!!!!!!
--

declare @oldtipfirst nvarchar(3)
declare @newtipfirst nvarchar(3)

declare @olddb nvarchar(50)
declare @newdb nvarchar(50)

set @OLDTipFirst = '126' -- Sterlent
set @NEWTipFirst = '129' -- Various/Generic

set @OLDDB = (select quotename(dbnamenexl) from rewardsnow.dbo.dbprocessinfo where dbnumber = @OLDTipFirst)
set @NEWDB = (select quotename(dbnamenexl) from rewardsnow.dbo.dbprocessinfo where dbnumber = @NEWTipFirst)

print 'Old FI Database: ' + @OLDDB
print 'New FI Database: ' + @NEWDB


--
-- 1Security
update sec
	set tipnumber = xref.newtipnumber
from newtnb.dbo.[1security] sec join rewardsnow.dbo.wrkConvertFITipXref xref
	on sec.tipnumber = xref.oldtipnumber


--
-- Account
update acc
	set tipnumber = xref.newtipnumber
from newtnb.dbo.account acc join rewardsnow.dbo.wrkConvertFITipXref xref
	on acc.tipnumber = xref.oldtipnumber


--
-- Customer
update cus
	set tipnumber = xref.newtipnumber
from newtnb.dbo.customer cus join rewardsnow.dbo.wrkConvertFITipXref xref
	on cus.tipnumber = xref.oldtipnumber
	

--
-- Online History within FI db
update onl
	set tipnumber = xref.newtipnumber
from newtnb.dbo.onlHistory onl join rewardsnow.dbo.wrkConvertFITipXref xref
	on onl.tipnumber = xref.oldtipnumber


--
-- E-Statement file
update st
	set travnum =  xref.newtipnumber
from newtnb.dbo.statement st join rewardsnow.dbo.wrkConvertFITipXref xref
	on st.travnum = xref.oldtipnumber


--
-- Online History
update oh
	set tipnumber = xref.newtipnumber
from onlinehistorywork.dbo.onlhistory oh join  rewardsnow.dbo.wrkConvertFITipXref xref
	on oh.tipnumber = xref.oldtipnumber


--
-- Portal Adjustments
update pa
	set tipnumber = xref.newtipnumber,
		tipfirst = left(xref.newtipnumber,3)
from onlinehistorywork.dbo.portal_adjustments pa  join  rewardsnow.dbo.wrkConvertFITipXref xref
	on pa.tipnumber = xref.oldtipnumber

--
-- Portal Combines.



--
-- Remove from SearchDB
delete from rewardsnow.dbo.searchdb
	where tipfirst = @OLDTipFirst


--
-- Remove from dbprocessinfo
delete from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @OLDTipFirst


