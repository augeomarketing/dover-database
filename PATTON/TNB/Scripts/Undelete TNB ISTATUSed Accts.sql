


declare @sql		nvarchar(4000)
declare @dbname	nvarchar(50)
--
--if exists(select 1 from tnb.dbo.sysobjects where name = 'phb_undeletes' and xtype = 'U')
--	drop table tnb.dbo.phb_undeletes
--
--create table tnb.dbo.phb_undeletes (tipnumber	varchar(15) primary key)
--
--declare csrDB cursor fast_forward for
--	select quotename(dbnamepatton) dbnamepatton
--	from rewardsnow.dbo.dbprocessinfo
--	where dbnamenexl = 'newtnb'
--
--open csrdb
--
--fetch next from csrdb into @dbname
--while @@FETCH_STATUS = 0
--BEGIN
--
--	print 'START:  ' + @dbname
--
--	set @sql = 'insert into tnb.dbo.phb_undeletes select distinct tipnumber
--				from ' + @dbname + '.dbo.affiliatdeleted afd join tnb.dbo.tnbsrc2 src2
--				on afd.tipnumber = src2.fulltip
--				where datedeleted = ''2008-12-03 00:00:00.000'' and isnull(istatus, '''') != ''Z'' '
--
--	print '	' + @sql
--
--	exec sp_executesql @sql
--
--	print 'END:   ' + @dbname
--
--	print ' '
--	print ' '
--
--	fetch next from csrdb into @dbname
--END
--
--close csrdb
--
--deallocate csrdb

---------------------------------------------------------------
---
---------------------------------------------------------------


BEGIN TRAN


declare @sql			nvarchar(4000)
declare @dbname		nvarchar(50)
declare @tip			nvarchar(15)
declare @datedeleted	nvarchar(25)

set @datedeleted = '2008-12-03 00:00:00.000'


declare csrUnDeletes cursor fast_forward for
	select tipnumber from tnb.dbo.phb_undeletes where left(tipnumber, 3) != '114'

open csrUnDeletes

fetch next from csrUnDeletes into @tip
while @@FETCH_STATUS = 0
BEGIN

	set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = left(@tip,3))

	set @sql = '
	insert into ' + @dbname + '.dbo.customer
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select cd.TIPNUMBER, cd.RunAvailable, cd.RUNBALANCE, cd.RunRedeemed, cd.LastStmtDate, cd.NextStmtDate, ''S'' as STATUS, 
		cd.DATEADDED, cd.LASTNAME, cd.TIPFIRST, cd.TIPLAST, cd.ACCTNAME1, cd.ACCTNAME2, cd.ACCTNAME3, cd.ACCTNAME4, cd.ACCTNAME5, 
		cd.ACCTNAME6, cd.ADDRESS1, cd.ADDRESS2, cd.ADDRESS3, cd.ADDRESS4, cd.City, cd.State, cd.ZipCode, cd.StatusDescription, cd.HOMEPHONE, 
		cd.WORKPHONE, cd.BusinessFlag, cd.EmployeeFlag, cd.SegmentCode, cd.ComboStmt, cd.RewardsOnline, cd.NOTES, cd.BonusFlag, cd.Misc1, 
		cd.Misc2, cd.Misc3, cd.Misc4, cd.Misc5, cd.RunBalanceNew, cd.RunAvaliableNew
	from ' + @dbname + '.dbo.customerdeleted cd left outer join ' + @dbname + '.dbo.customer c on cd.tipnumber = c.tipnumber
	where cd.tipnumber = ' + char(39) + @tip + char(39) + '  and c.tipnumber is null and cd.datedeleted = ' + char(39) + @datedeleted + char(39)

	print @sql
	exec sp_executesql @sql

	set @sql = 'delete from ' + @dbname + '.dbo.customerdeleted where tipnumber = ' + char(39) + @tip + char(39) + '  and datedeleted = ' + char(39) + @datedeleted + char(39)
	print @sql
	exec sp_executesql @sql


	set @sql = '
	insert into ' + @dbname + '.dbo.affiliat
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select ad.ACCTID, ad.TIPNUMBER, ad.AcctType, ad.DATEADDED, ad.SECID, ''S'', ad.AcctTypeDesc, ad.LastName, ad.YTDEarned, ad.CustID
	from ' + @dbname + '.dbo.affiliatdeleted ad left outer join ' + @dbname + '.dbo.affiliat aff on ad.acctid = aff.acctid
	where ad.tipnumber = ' + char(39) + @tip + char(39) + '  and aff.acctid is null and ad.datedeleted = ' + char(39) + @datedeleted + char(39)

	print @sql
	exec sp_executesql @sql

	set @sql = 'delete from ' + @dbname + '.dbo.affiliatdeleted where tipnumber = ' + char(39) + @tip + char(39) + '  and datedeleted = ' + char(39) + @datedeleted + char(39)
	print @sql
	exec sp_executesql @sql


	set @sql = '
	insert into ' + @dbname + '.dbo.history
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	from ' + @dbname + '.dbo.historydeleted where tipnumber = ' + char(39) + @tip + char(39) + '  and datedeleted = ' + char(39) + @datedeleted + char(39)

	print @sql
	exec sp_executesql @sql

	set @sql = 'delete from ' + @dbname + '.dbo.historydeleted where tipnumber = ' + char(39) + @tip + char(39) + '  and datedeleted = ' + char(39) + @datedeleted + char(39)
	print @sql
	exec sp_executesql @sql


	fetch next from csrUnDeletes into @tip
END

close csrUnDeletes
deallocate csrUnDeletes


ROLLBACK TRAN  --    commit tran




