USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateTNBParticipantCounts]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spUpdateTNBParticipantCounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpdateTNBParticipantCounts]
		@MonthEndDate		datetime

AS

declare @TipFirst			varchar(3)
declare @ParticipantCount	int
declare @rc				int


declare csrTips cursor FAST_FORWARD for
	select dbnumber 
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'NewTNB' and tte.tipfirst is null
	order by dbnumber


open csrTips

fetch next from csrTips into @TipFirst

While @@FETCH_STATUS = 0
BEGIN

	set @ParticipantCount = (select count(distinct tipnumber) from dbo.customer_stage where left(tipnumber,3) = @tipfirst)

	exec @rc = rewardsnow.dbo.spmonthlyparticipantcounts @TipFirst, @MonthEndDate

	fetch next from csrTips into @TipFirst
END

close csrTips

deallocate csrTips
GO
