USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spTruncateNewTNBTables]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spTruncateNewTNBTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spTruncateNewTNBTables]

AS


truncate table dbo.AccountTips
truncate table dbo.AFFILIAT
truncate table dbo.Affiliat_Stage
truncate table dbo.affiliatdeleted
truncate table dbo.beginning_balance_table
truncate table dbo.current_month_activity
truncate table dbo.customer
truncate table dbo.customer_stage
truncate table dbo.customerdeleted
truncate table dbo.history
truncate table dbo.history_stage
truncate table dbo.imp_tnbsummary
truncate table dbo.impBonus
truncate table dbo.impCustomer
truncate table dbo.impCustomer_Agent
truncate table dbo.impCustomer_Complex
truncate table dbo.impCustomer_Cust
truncate table dbo.impCustomer_DebitCust
truncate table dbo.impCustomer_DowCust
truncate table dbo.impCustomer_Error
truncate table dbo.impCustomer_Purge
truncate table dbo.impCustomer_Purge_Pending
truncate table dbo.impCustomer_R2UCust
truncate table dbo.impTransaction
truncate table dbo.impTransaction_Agent
truncate table dbo.impTransaction_Complex
truncate table dbo.impTransaction_DebitTran
truncate table dbo.impTransaction_DowTran
truncate table dbo.impTransaction_Error
truncate table dbo.impTransaction_R2UTran
truncate table dbo.impTransaction_tran
truncate table dbo.impWelcome_R2UWelcome
truncate table dbo.Monthly_Audit_ErrorFile
truncate table dbo.Monthly_Statement_File
truncate table dbo.OneTimeBonuses
truncate table dbo.OneTimeBonuses_Stage
truncate table dbo.Quarterly_Audit_ErrorFile
truncate table dbo.Quarterly_Statement_File_Bak
truncate table dbo.TransStandard
truncate table dbo.WelcomeKits
GO
