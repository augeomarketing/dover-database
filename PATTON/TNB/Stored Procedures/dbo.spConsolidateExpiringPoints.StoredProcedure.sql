USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spConsolidateExpiringPOints]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spConsolidateExpiringPOints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spConsolidateExpiringPOints]

AS

declare @SQL		nvarchar(4000)
declare @DB		nvarchar(50)

truncate table newtnb.dbo.expiringpoints

declare csrDB cursor FAST_FORWARD for	
    select quotename(dbnamepatton) dbnamepatton
    from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
	   on dbpi.dbnumber = tte.tipfirst
    where dbpi.dbnamenexl = 'NEWTNB' and tte.tipfirst is null and dbpi.PointsExpireFrequencyCd = 'ME'


open csrDB

fetch next from csrdb into @db
while @@FETCH_STATUS = 0
BEGIN

    set @sql = '
	   if exists(select 1 from ' + @db + '.dbo.sysobjects where name = ''expiringpoints'' and xtype = ''u'')
		  insert into newtnb.dbo.expiringpoints
		  (tipnumber, addpoints, REDPOINTS, POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, PointsToExpireNext, ADDPOINTSNEXT, EXPTODATE, ExpiredRefunded)
		  select tipnumber, addpoints, REDPOINTS, POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, PointsToExpireNext, ADDPOINTSNEXT, EXPTODATE, ExpiredRefunded
		  from ' + @db + '.dbo.expiringpoints'

    exec sp_executesql @sql




    fetch next from csrdb into @db
END

close csrdb
deallocate csrdb


/*

exec dbo.spConsolidateExpiringPoints


*/
GO
