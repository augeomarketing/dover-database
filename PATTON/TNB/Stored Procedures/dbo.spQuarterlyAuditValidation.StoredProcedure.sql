USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spQuarterlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] 


AS

delete from dbo.Quarterly_Audit_ErrorFile

insert into dbo.Quarterly_Audit_ErrorFile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
 PointsReturned, PointsSubtracted, PointsDecreased, Errormsg, Currentend)
select qsf.Tipnumber, PointsBegin, PointsEnd, (pointspurchasedcr + pointspurchaseddb), PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
 (pointsreturnedcr + pointsreturneddb), PointsSubtracted, PointsDecreased, 'Ending Balances do not match', AdjustedEndingPoints
from dbo.quarterly_statement_file qsf join dbo.current_month_activity cma
	on qsf.tipnumber = cma.tipnumber
where pointsend != cma.adjustedendingpoints
GO
