USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateCreditUnionAuditFiles]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spGenerateCreditUnionAuditFiles]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spGenerateCreditUnionAuditFiles]
	@CUNum	varchar(4)
AS


declare @assoc		varchar(3)

set @assoc = (select top 1 assocnum from dbo.assoc where cunum = @CUNum)

select distinct @Assoc AssocNum, tipnumber,
					isnull( (select top 1 acctid from dbo.affiliat_stage cus where cus.tipnumber = stg.tipnumber and acctstatus = 'A' order by dateadded desc),
					        (select top 1 acctid from dbo.affiliat_stage cus where cus.tipnumber = stg.tipnumber order by dateadded desc)) as PriAcctNum, 
	acctname1, acctname2, address1, address2, address3, city + ',  ' + state + '   ' + zipcode as CityStateZip, 
	zipcode, homephone, workphone, dateadded, (runbalance + RunAvaliableNew) runbalance,	runredeemed, runavailable, status
from dbo.assoc ass join dbo.customer_stage stg
	on ass.tipfirst = stg.tipfirst
where ass.cunum = @CUNum
GO
