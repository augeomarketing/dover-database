USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spLoadTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spLoadTipNumbers]

as


declare @lastTipPrefix		varchar(3)
declare @TipFirst		   	varchar(3)
declare @LastTipUsed		varchar(15)

declare @last_sid			int
declare @current_sid		int

declare @tipnumber			varchar(15)


Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(4000)

-------------------------------------------------------------
-- Copy Tip#s from 
-------------------------------------------------------------

create table #TNBFI
    (TipFirst			   varchar(3) primary key,
     LastTipNumberused	   varchar(15) null)


--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor FAST_FORWARD for
	select ltrim(rtrim(dbnumber)) TipFirst
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.TipsToExclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'newtnb' 
	and tte.tipfirst is null


open curDBName
Fetch next from curDBName into @TipFirst
while @@Fetch_Status = 0
Begin

	  exec rewardsnow.dbo.spGetLastTipNumberUsed
		  @TipFirst,
		  @LastTipUsed	   OUTPUT

	  insert into #TNBFI (TipFirst, LastTipNumberUsed)
	  Values( @TipFirst, @LastTipUsed )

	  fetch Next from curDBName into @TipFirst
end

Close curDBName
deallocate curdbname

-- Now update last tipnumber used into the newtnb db - in case the FI version is out of synch
exec dbo.spUpdateLastTipsUsed


update tmp
    set LastTipNumberUsed = lasttipused
from #TNBFI tmp join newtnb.dbo.wrk_lasttips wrk
    on tmp.tipfirst = wrk.tipfirst


-------------------------------------------------------------
-- Assign tips to known accounts
-------------------------------------------------------------

--
-- Lookup tipnumber using credit card# in affiliat_stage
--
update cus
	set tipnumber = stg.tipnumber
from dbo.impcustomer cus join dbo.affiliat_stage stg
	on cus.ccacctnbr = stg.acctid
	and cus.tipprefix = left(stg.tipnumber,3)


--
-- Lookup tipnumber using old credit card# in affiliat_stage
--
update cus
	set tipnumber = stg.tipnumber
from dbo.impcustomer cus join dbo.affiliat_stage stg
	on cus.oldccacctnbr = stg.acctid
	and cus.tipprefix = left(stg.tipnumber,3)
where isnull(cus.tipnumber, '')  = ''
and cus.oldccacctnbr != ''



-- Update new cards where old card # has had a tip# assigned	

update imp1
	set tipnumber = imp2.tipnumber
from dbo.impcustomer imp1 join impcustomer imp2
	on imp1.oldccacctnbr = imp2.ccacctnbr
where isnull(imp2.tipnumber, '') != ''
and imp1.oldccacctnbr != ''
and imp1.assocnbr = imp2.assocnbr


--
-- create table of checking numbers with assigned tips.
create table #dda (tipfirst varchar(3), tipnumber varchar(15), checkingnbr varchar(25))
insert into #dda
select tipprefix, tipnumber, checkingnbr
from dbo.impcustomer imp join dbo.assoc ass
	on imp.tipprefix = ass.tipfirst
where checkingnbr != ''
and isnull(tipnumber, '') != ''
and ass.ddamatch = 'Y'


--
-- now link in customers that don't have tip number or cc# but have checking#.
update imp
	set tipnumber = dda.tipnumber
from #dda dda join dbo.impcustomer imp
	on dda.checkingnbr = imp.checkingnbr
	and dda.tipfirst = imp.tipprefix
where isnull(imp.tipnumber, '') = ''




--
-- For complex, check the card# in the transaction
--
select checkingnbr
into #complexcustomers
from dbo.impcustomer
where isnull(tipnumber, '') = ''
and tipprefix = '119'

update imp
	set tipnumber = aff.tipnumber
from #complexcustomers ccus join dbo.imptransaction txn
	on ccus.checkingnbr = ltrim(rtrim(txn.acctnbr))
join dbo.affiliat_stage aff
	on txn.ccacctnbr = aff.acctid
join dbo.impcustomer imp
	on imp.checkingnbr = ccus.checkingnbr
where isnull(imp.tipnumber, '') = ''



-------------------------------------------------------------
-- Now assign new tips
-------------------------------------------------------------
--
--Declare @DBName varchar(100)
--Declare @sqlcmd nvarchar(4000)


create table #DDAMatchTips (TipFirst varchar(3) primary key)
insert into #DDAMatchTips
select tipfirst
from dbo.assoc 
where ddamatch = 'Y'


set @last_sid = 0


select top 1 @current_sid = sid_impcustomer_id, @tipfirst = tipprefix
	from dbo.impcustomer
	where tipnumber is null and sid_impcustomer_id > @last_sid and isnull(externalsts, '') not in ('L', 'U')
	order by sid_impcustomer_id

while @last_sid < @current_sid
BEGIN

	set @tipnumber = (select tipfirst + right('000000000000' + cast(cast( right(LastTipnumberUsed,12) as bigint) + 1 as varchar(12)), 12) 
					from #TNBFI where tipfirst = @tipfirst) 

	update #TNBFI set lasttipnumberused = @tipnumber where tipfirst = @tipfirst

	update dbo.impcustomer set tipnumber = @tipnumber where sid_impcustomer_id = @current_sid

--
-- Check if there any other impcustomer rows with same DDA & TipFirst.  If so, update these as well.
-- Only do if DDAMatch flag is set in the dbo.Assoc table!
--
	update imp1	
		set tipnumber = @TipNumber
	from dbo.impcustomer imp1 join dbo.impcustomer imp2
		on imp1.CheckingNbr = imp2.CheckingNbr
		and imp1.tipprefix = imp2.tipprefix
	join #DDAMatchTips dda
		on dda.tipfirst = imp2.tipprefix
	where imp2.sid_impcustomer_id = @current_sid
	and imp2.checkingnbr != ''

--print cast(@current_sid as varchar(10)) + '  |  ' + isnull(cast(@last_sid as varchar(10)), 'Missing LastSid') + '  |  ' + isnull(@tipfirst, 'Missing TipFirst')

	set @last_sid = @current_sid

	select top 1 @current_sid = sid_impcustomer_id, @tipfirst = tipprefix
		from dbo.impcustomer
		where tipnumber is null and sid_impcustomer_id > @last_sid
		order by sid_impcustomer_id
END


--------------------------------------------------------------------------
-- Check the TipSwitch table, reassign tip numbers 
--------------------------------------------------------------------------

update cus
	set tipnumber = ts.newtip,
		tipprefix = left(ts.newtip,3)
from dbo.impCustomer cus join dbo.TipSwitch ts
	on cus.tipnumber = ts.tfno

/*  PHB 2009/04/23  Lost/Stolen cards should not be closed 
---------------------------------------------------------------------------
-- Now that Lost/Stolen/Fraud accounts have been used to help assign tips
-- Mark these records as CLOSED.
---------------------------------------------------------------------------

update dbo.impCustomer
	set rewardsnowsts = 'C'
where externalsts in ('L', 'U')

*/

---------------------------------------------------------------------------
-- Add tip #s to transaction table
---------------------------------------------------------------------------

update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impcustomer cus
	on ltrim(rtrim(txn.acctnbr)) = ltrim(rtrim(cus.checkingnbr))
where prin = 'cplx' --and txn.ccacctnbr = ''
and isnull(txn.tipnumber, '') = ''


update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impcustomer cus
	on txn.ccacctnbr = cus.ccacctnbr
where txn.ccacctnbr != '' and isnull(txn.tipnumber, '') = ''


update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impcustomer cus
	on txn.ccacctnbr = cus.oldccacctnbr
where isnull(txn.tipnumber, '') = '' and txn.ccacctnbr != ''


update txn
	set tipnumber = aff.tipnumber
from dbo.imptransaction txn join dbo.affiliat_stage aff
	on txn.ccacctnbr = aff.acctid
where isnull(txn.tipnumber, '') = ''  and txn.ccacctnbr != ''

---
-- Drop rows in imptransaction w/o tips
---
insert into dbo.imptransaction_notips
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, CreditUnionNbr, TipNumber, Points, RNTranCode, AcctNbr, Misc3)
select AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, CreditUnionNbr, TipNumber, Points, RNTranCode, AcctNbr, Misc3
from dbo.imptransaction
where tipnumber is null

delete from dbo.imptransaction where tipnumber is null



--
-- Now update each FI's Client table with the last Tip# used
--
Declare csrLTU cursor FAST_FORWARD for
	select tipfirst, lasttipnumberused
	from #TNBFI


open csrLTU
Fetch next from csrLTU into @Tipfirst, @LastTipUsed

while @@Fetch_Status = 0
Begin

    exec rewardsnow.dbo.spPutLastTipNumberUsed
		  @TipFirst, @LastTipUsed

	Fetch next from csrLTU into @Tipfirst, @LastTipUsed
END

close csrLTU

deallocate csrLTU
GO
