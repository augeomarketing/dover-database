USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[usp_164_Holiday_Bonus]    Script Date: 09/30/2016 13:25:52 ******/
DROP PROCEDURE [dbo].[usp_164_Holiday_Bonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_164_Holiday_Bonus]
	-- Add the parameters for the stored procedure here
		@DateAdded datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select  cus.[TipNumber],txn.[CCAcctNbr] Card, sum(txn.Points*2) Points, '2' Mult
	--into #tmp
	--from dbo.impTransaction txn join dbo.Customer_Stage cus
	--	on txn.tipnumber = cus.tipnumber
	--where Status in ('A', 'S') 
	--and txn.TipNumber like '164%'
	--and RNTranCode='63'
	--and cast(TransactionDt as date) between '2015-11-27' and '2015-11-30'
	--group by cus.[TipNumber], txn.[CCAcctNbr], [RNTranCode]
	--order by cus.[TipNumber]

--	insert into #tmp
--	(TipNumber, Card, Points, Mult)
	select  cus.[TipNumber],txn.[CCAcctNbr] Card, sum(txn.Points) Points, '1' Mult
	into #tmp
	from dbo.impTransaction txn join dbo.Customer_Stage cus
		on txn.tipnumber = cus.tipnumber
	where Status in ('A', 'S') 
	and txn.TipNumber like '164%'
	and RNTranCode='63'
	--and cast(TransactionDt as date) not between '2015-11-27' and '2015-11-30'
	group by cus.[TipNumber], txn.[CCAcctNbr], [RNTranCode]
	order by cus.[TipNumber]

	insert into dbo.history_Stage 	
		(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select  TipNumber,Card, @DateAdded, 'BI', '1', Points ,'Holiday Bonus 2015 2 Point','1','NEW','0'
	from #tmp
	where Mult='1'

	--insert into dbo.history_Stage 	
	--	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	--select  TipNumber,Card, @DateAdded, 'BI', '1', Points ,'Holiday Bonus 2015 3 Point','1','NEW','0'
	--from #tmp
	--where Mult='2'

	select TIPNUMBER, SUM(points) Points
	into #tmp2
	from #tmp 
	group by tipnumber

	Update cs
	Set	RunAvailable  = RunAvailable + isnull(tmp.Points, 0),
			RunAvaliableNew = isnull(tmp.Points, 0)
	from Customer_Stage cs
	join #tmp2 tmp
	on cs.TIPNUMBER = tmp.tipnumber
	where cs.TIPNUMBER like '164%'
END
GO
