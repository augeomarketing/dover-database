USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spCombineImpCustomerTables]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spCombineImpCustomerTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCombineImpCustomerTables]

as

-- Combines the following import customer files:
--         impCustomer_Agent
--         impCustomer_Complex
--         impCustomer_Cust
--         impCustomer_DebitCust
--         impCustomer_DowCust
--         impCustomer_R2UCust

truncate table dbo.impCustomer

insert into dbo.impCustomer
(CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, AddressLine2, 
	City, StateCd, Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr, CreditUnionNbr, TipPrefix, TIPNumber, MISC3,
      DelinquentCycles, DelinquentDays, GroupID, RelationshipControl, DependentStrategy  )


select	CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, 
		AddressLine2, City, ltrim(rtrim(StateCd)), Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr, null as CreditUnionNbr,
		null as TipPrefix, null as TIPNumber, null as MISC3, null as delinquentcycles, null as delinquentdays, null as groupid,
		null as relationshipcontrol, null as dependentstrategy
from dbo.impCustomer_Agent


UNION ALL


select	distinct CCAcctNbr, AcctName as PrimaryNm, null as SecondaryNm, null as ExternalSts, null as InternalSts, null as OldCCAcctNbr,
		AddressLine1, AddressLine2, City, ltrim(rtrim(StateCd)), ZipCd, null as Phone1, null as Phone2, null as PrimarySSN,
		null as AssocNbr, left(ltrim(rtrim(AcctNbr)), 16) as CheckingNbr, null as CreditUnionNbr, TipFirst as TIPPrefix, null as TIPNumber, null as MISC3,
		null as delinquentcycles, null as delinquentdays, null as groupid, null as relationshipcontrol, null as dependentstrategy
from dbo.impCustomer_Complex


UNION ALL


select	CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, 
		AddressLine2, City,  ltrim(rtrim(StateCd)), Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr, CreditUnionNbr,
		null as TipPrefix, null as TIPNumber, misc3, null as delinquentcycles, null as delinquentdays, null as groupid,
		null as relationshipcontrol, null as dependentstrategy
from dbo.impcustomer_Cust



UNION ALL


select	CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, 
		AddressLine2, City,  ltrim(rtrim(StateCd)), Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr, CreditUnionNbr,
		null as TipPrefix, null as TIPNumber, null as misc3, null as delinquentcycles, null as delinquentdays, null as groupid,
		null as relationshipcontrol, null as dependentstrategy
from dbo.impCustomer_DebitCust


UNION ALL


select	CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, 
		AddressLine2, City,  ltrim(rtrim(StateCd)), Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr, CreditUnionNbr,
		null as TipPrefix, null as TIPNumber, null as misc3, delinquentcycles, delinquentdays, groupid,relationshipcontrol, dependentstrategy
from dbo.impCustomer_R2UCust


--UNION ALL
--
--
--select	CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, 
--		AddressLine2, City, StateCd, Zip, Phone1, Phone2, null as PrimarySSN, null as AssocNbr, null as CheckingNbr,
--		null as CreditUnionNbr, '000' as TipFirst, '000' + right('000000000000' + ltrim(rtrim(TravelFreeNbr)), 12) as TIPNumber
--from dbo.impCustomer_DowCust
GO
