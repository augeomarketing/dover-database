USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spCopyFITablesToNewTNB]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spCopyFITablesToNewTNB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spCopyFITablesToNewTNB]
	@MonthBeginDate		datetime
As

	declare @dbname	nvarchar(128)
	declare @sqlcmnd	nvarchar(4000)

	declare csrDBName cursor FAST_FORWARD for
		select dbnamepatton 
		from rewardsnow.dbo.DBProcessInfo dbpi left outer join dbo.tipstoexclude tte
			on dbpi.dbnumber = tte.tipfirst
		where dbnamenexl = 'NewTNB' and tte.tipfirst is null and isnull(sid_fiprodstatus_statuscode, '')  not in ('X', 'I')
		order by dbnamepatton

	open csrDBName

	fetch next from csrDBName into @DbName

	while @@FETCH_STATUS = 0
	BEGIN
	print 'Begin Processing database: ' + @DbName
		-- Load the stage tables
		set @SQLCmnd = 'Insert into dbo.Customer Select * from [' +  @dbName + '].dbo.Customer'
		Exec sp_executeSql @SQLCmnd 

	--	set @SQLCmnd = 'Update dbo.Customer Set  RunAvaliableNew = 0 '
	--	Exec sp_executeSql @SQLCmnd 

		set @SQLCmnd = 'Insert into dbo.Affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag)
					 Select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag from [' + @dbName + '].dbo.Affiliat'
		Exec sp_executeSql @SQLCmnd 

		set @SQLCmnd = 'Insert into dbo.History (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
					 Select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage from [' + @dbName  + '].dbo.History '
		Exec sp_executeSql @SQLCmnd 

	--	set @SQLCmnd = 'Update dbo.History set SecId = ' + char(39) +  'OLD' + char(39) 
	--	Exec sp_executeSql @SQLCmnd 

	--	set @SQLCmnd = 'Insert into dbo.OnetimeBonuses_Stage Select * from [' + @dbName + '].dbo.OnetimeBonuses'
	--	Exec sp_executeSql @SQLCmnd

	print 'Complete Processing database: ' + @DbName
	print ' '
	print ' '

		fetch next from csrDBName into @DbName
	END

	close csrDBName

	deallocate csrDBName

	Update dbo.Customer Set  RunAvaliableNew = 0 
	Update dbo.History set SecId = 'OLD'


----------------------------------------------------------------------------------------------
--
-- Recalc dbo.onetimebonuses table
----------------------------------------------------------------------------------------------


truncate table dbo.onetimebonuses
truncate table dbo.onetimebonuses_stage


insert into dbo.onetimebonuses
(TipNumber, Trancode, AcctID, DateAwarded)
select tipnumber, trancode, acctid, histdate
from dbo.history his join dbo.bonusprogramfi bpfi
    on left(his.tipnumber,3) = bpfi.sid_BonusProgramFI_TipFirst
join dbo.bonusprogram bp
    on bp.sid_BonusProgram_ID = bpfi.sid_BonusProgram_ID
where his.trancode = bp.sid_TranType_TranCode



----------------------------------------------------------------------------------------------
--
-- Recalc dbo.beginning_balance_table table
----------------------------------------------------------------------------------------------

	truncate table  newtnb.dbo.beginning_balance_table

	create table #bbt
		(Tipnumber varchar(15) primary key, 
		 MonthBeg1 int, MonthBeg2 int, MonthBeg3 int, MonthBeg4 int, MonthBeg5 int, MonthBeg6 int, 
		 MonthBeg7 int, MonthBeg8 int, MonthBeg9 int, MonthBeg10 int, MonthBeg11 int, MonthBeg12 int)

	insert into #bbt
	select cus.tipnumber, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	from dbo.customer cus left outer join dbo.beginning_balance_table bbt
		on cus.tipnumber = bbt.tipnumber
	where bbt.tipnumber is null



	declare @CurMonth	int
	declare @LastMonth	int
	declare @Year		int
	declare @date		datetime
	declare @strdate	nvarchar(10)
	declare @ctr		int
	declare @sql		nvarchar(4000)

	set @Date = @MonthBeginDate
	set @date = dateadd(mm, 1, @date)
	set @date = dateadd(yy, -1, @date)

	set @CurMonth = month(@Date)

	set @ctr = 1

	while @ctr <= 12
	BEGIN

		set @strDate = cast( year(@date) as nvarchar(4)) + '/' +
					right( '00' + cast( month(@date) as nvarchar(2)), 2) + '/' +
					right( '00' + cast( day(@date) as nvarchar(2)), 2)

		set @SQL = 'update #bbt set MonthBeg' + cast(@CurMonth as nvarchar(2)) + 
					' = isnull((select sum(points * ratio) from dbo.history his where his.tipnumber = #bbt.tipnumber and
						histdate < ' + char(39) + @strDate + char(39) + '), 0)'
		print @sql
		exec sp_executesql  @sql

		set @date = dateadd(mm, 1, @date)
		set @CurMonth = month(@Date)

		set @ctr = @ctr + 1
	END


	insert into dbo.beginning_balance_table
	select * from #BBT
GO
