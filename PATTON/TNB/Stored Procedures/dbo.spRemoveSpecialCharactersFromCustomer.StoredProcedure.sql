USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharactersFromCustomer]    Script Date: 02/09/2010 08:17:05 ******/
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2010                                                              */
/* REVISION: 1                                                               */
/* SCAN: SEB001                                                              */ 
/* REASON: Add code to remove #, `, forward slash, carriage return, extra spaces */
/******************************************************************************/
DROP PROCEDURE [dbo].[spRemoveSpecialCharactersFromCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spRemoveSpecialCharactersFromCustomer]
AS

Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(34), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(34),'')
	, ACCTNAME3=replace(ACCTNAME3,char(34), '')
	, ACCTNAME4=replace(ACCTNAME4,char(34), '')
	, ACCTNAME5=replace(ACCTNAME5,char(34), '')
	, ACCTNAME6=replace(ACCTNAME6,char(34), '')
	, lastname=replace(lastname,char(34), '')
	, ADDRESS1=replace(ADDRESS1,char(34), '')
	, ADDRESS2=replace(ADDRESS2,char(34), '')
	, ADDRESS3=replace(ADDRESS3,char(34), '')
	, ADDRESS4=replace(ADDRESS4,char(34), '')
	, city=replace(city,char(34), '')
	, state=replace(state,char(34), '')


Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(39), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(39), '')
	, ACCTNAME3=replace(ACCTNAME3,char(39), '')
	, ACCTNAME4=replace(ACCTNAME4,char(39), '')
	, ACCTNAME5=replace(ACCTNAME5,char(39), '')
	, ACCTNAME6=replace(ACCTNAME6,char(39), '')
	, lastname=replace(lastname,char(39), '')
	, ADDRESS1=replace(ADDRESS1,char(39), '')
	, ADDRESS2=replace(ADDRESS2,char(39), '')
	, ADDRESS3=replace(ADDRESS3,char(39), '')
	, ADDRESS4=replace(ADDRESS4,char(39), '')
	, city=replace(city,char(39), '')
	, state=replace(state,char(39), '') 


Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(44), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(44), '')
	, ACCTNAME3=replace(ACCTNAME3,char(44), '')
	, ACCTNAME4=replace(ACCTNAME4,char(44), '')
	, ACCTNAME5=replace(ACCTNAME5,char(44), '')
	, ACCTNAME6=replace(ACCTNAME6,char(44), '')
	, lastname=replace(lastname,char(44), '')
	, ADDRESS1=replace(ADDRESS1,char(44), '')
	, ADDRESS2=replace(ADDRESS2,char(44), '')
	, ADDRESS3=replace(ADDRESS3,char(44), '')
	, ADDRESS4=replace(ADDRESS4,char(44), '')
	, city=replace(city,char(44), '')
	, state=replace(state,char(44), '')



Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(46), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(46), '')
	, ACCTNAME3=replace(ACCTNAME3,char(46), '')
	, ACCTNAME4=replace(ACCTNAME4,char(46), '')
	, ACCTNAME5=replace(ACCTNAME5,char(46), '')
	, ACCTNAME6=replace(ACCTNAME6,char(46), '')
	, lastname=replace(lastname,char(46), '')
	, ADDRESS1=replace(ADDRESS1,char(46), '')
	, ADDRESS2=replace(ADDRESS2,char(46), '')
	, ADDRESS3=replace(ADDRESS3,char(46), '')
	, ADDRESS4=replace(ADDRESS4,char(46), '')
	, city=replace(city,char(46), '')
	, state=replace(state,char(46), '')


Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(140), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(140), '')
	, ACCTNAME3=replace(ACCTNAME3,char(140), '')
	, ACCTNAME4=replace(ACCTNAME4,char(140), '')
	, ACCTNAME5=replace(ACCTNAME5,char(140), '')
	, ACCTNAME6=replace(ACCTNAME6,char(140), '')
	, lastname=replace(lastname,char(140), '')
	, ADDRESS1=replace(ADDRESS1,char(140), '')
	, ADDRESS2=replace(ADDRESS2,char(140), '')
	, ADDRESS3=replace(ADDRESS3,char(140), '')
	, ADDRESS4=replace(ADDRESS4,char(140), '')
	, city=replace(city,char(140), '')
	, state=replace(state,char(140), '')

/*************************************/
/* Start SEB001                      */
/*************************************/
/*  35 is # */
Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(35), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(35),'')
	, ACCTNAME3=replace(ACCTNAME3,char(35), '')
	, ACCTNAME4=replace(ACCTNAME4,char(35), '')
	, ACCTNAME5=replace(ACCTNAME5,char(35), '')
	, ACCTNAME6=replace(ACCTNAME6,char(35), '')
	, lastname=replace(lastname,char(35), '')
	, ADDRESS1=replace(ADDRESS1,char(35), '')
	, ADDRESS2=replace(ADDRESS2,char(35), '')
	, ADDRESS3=replace(ADDRESS3,char(35), '')
	, ADDRESS4=replace(ADDRESS4,char(35), '')
	, city=replace(city,char(35), '')
	, state=replace(state,char(35), '')

/*  96 is ` */
Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(96), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(96),'')
	, ACCTNAME3=replace(ACCTNAME3,char(96), '')
	, ACCTNAME4=replace(ACCTNAME4,char(96), '')
	, ACCTNAME5=replace(ACCTNAME5,char(96), '')
	, ACCTNAME6=replace(ACCTNAME6,char(96), '')
	, lastname=replace(lastname,char(96), '')
	, ADDRESS1=replace(ADDRESS1,char(96), '')
	, ADDRESS2=replace(ADDRESS2,char(96), '')
	, ADDRESS3=replace(ADDRESS3,char(96), '')
	, ADDRESS4=replace(ADDRESS4,char(96), '')
	, city=replace(city,char(96), '')
	, state=replace(state,char(96), '')

/*  47 is forward slash */
Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(47), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(47),'')
	, ACCTNAME3=replace(ACCTNAME3,char(47), '')
	, ACCTNAME4=replace(ACCTNAME4,char(47), '')
	, ACCTNAME5=replace(ACCTNAME5,char(47), '')
	, ACCTNAME6=replace(ACCTNAME6,char(47), '')
	, lastname=replace(lastname,char(47), '')
	, ADDRESS1=replace(ADDRESS1,char(47), '')
	, ADDRESS2=replace(ADDRESS2,char(47), '')
	, ADDRESS3=replace(ADDRESS3,char(47), '')
	, ADDRESS4=replace(ADDRESS4,char(47), '')
	, city=replace(city,char(47), '')
	, state=replace(state,char(47), '')

/*  13 is Carriage Return */
Update dbo.Customer_stage set	   	
	ACCTNAME1=replace(ACCTNAME1,char(13), '') 
	, ACCTNAME2=replace(ACCTNAME2,char(13),'')
	, ACCTNAME3=replace(ACCTNAME3,char(13), '')
	, ACCTNAME4=replace(ACCTNAME4,char(13), '')
	, ACCTNAME5=replace(ACCTNAME5,char(13), '')
	, ACCTNAME6=replace(ACCTNAME6,char(13), '')
	, lastname=replace(lastname,char(13), '')
	, ADDRESS1=replace(ADDRESS1,char(13), '')
	, ADDRESS2=replace(ADDRESS2,char(13), '')
	, ADDRESS3=replace(ADDRESS3,char(13), '')
	, ADDRESS4=replace(ADDRESS4,char(13), '')
	, city=replace(city,char(13), '')
	, state=replace(state,char(13), '')

/*  Extra Blanks */
Update dbo.Customer_stage set	   	
	ACCTNAME1=rtrim(ltrim(replace(replace(replace(ACCTNAME1,' ','<>'),'><',''),'<>',' ')))
	, ACCTNAME2=rtrim(ltrim(replace(replace(replace(ACCTNAME2,' ','<>'),'><',''),'<>',' ')))
	, ACCTNAME3=rtrim(ltrim(replace(replace(replace(ACCTNAME3,' ','<>'),'><',''),'<>',' ')))
	, ACCTNAME4=rtrim(ltrim(replace(replace(replace(ACCTNAME4,' ','<>'),'><',''),'<>',' ')))
	, ACCTNAME5=rtrim(ltrim(replace(replace(replace(ACCTNAME5,' ','<>'),'><',''),'<>',' ')))
	, ACCTNAME6=rtrim(ltrim(replace(replace(replace(ACCTNAME6,' ','<>'),'><',''),'<>',' ')))
	, lastname=rtrim(ltrim(replace(replace(replace(lastname,' ','<>'),'><',''),'<>',' ')))
	, ADDRESS1=rtrim(ltrim(replace(replace(replace(ADDRESS1,' ','<>'),'><',''),'<>',' ')))
	, ADDRESS2=rtrim(ltrim(replace(replace(replace(ADDRESS2,' ','<>'),'><',''),'<>',' ')))
	, ADDRESS3=rtrim(ltrim(replace(replace(replace(ADDRESS3,' ','<>'),'><',''),'<>',' ')))
	, ADDRESS4=rtrim(ltrim(replace(replace(replace(ADDRESS4,' ','<>'),'><',''),'<>',' ')))
	, city=rtrim(ltrim(replace(replace(replace(city,' ','<>'),'><',''),'<>',' ')))
	, state=rtrim(ltrim(replace(replace(replace(state,' ','<>'),'><',''),'<>',' '))) 

/*************************************/
/* END SEB001                      */
/*************************************/

GO
