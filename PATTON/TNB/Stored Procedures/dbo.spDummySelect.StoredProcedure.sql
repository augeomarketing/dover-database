USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spDummySelect]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spDummySelect]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spDummySelect]

as

select TIPFirst, AssocNum, CUNum, CombineNames, DDAMatch, ReporttypeCd, sid_QuarterlyStatementFormat_id, sid_StatementOption_Code, ExpireTransferPoints
from dbo.assoc
order by tipfirst
GO
