USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[CreateFIExportFileNames]    Script Date: 02/09/2010 08:17:03 ******/
DROP PROCEDURE [dbo].[CreateFIExportFileNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CreateFIExportFileNames] (@FilePath  varchar(512))

as

truncate table dbo.FIexportfilenames

insert into dbo.FIexportfilenames
select distinct a.tipfirst, ltrim(rtrim(@FilePath)) + a.tipfirst + '_' + replace(ClientName, ' ', '') + '_' + 
		cast(datepart(yyyy, getdate()) as nvarchar(4)) + 
		cast(datepart(mm, getdate()) as nvarchar(2)) +
		cast(datepart(dd, getdate()) as nvarchar(2)) + '.TXT' as filename

from dbo.assoc a join rewardsnow.dbo.clients c
	on a.tipfirst = c.tipfirst

order by a.tipfirst
GO
