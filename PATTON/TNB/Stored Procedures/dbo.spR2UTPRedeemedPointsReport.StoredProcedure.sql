USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spR2UTPRedeemedPointsReport]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spR2UTPRedeemedPointsReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spR2UTPRedeemedPointsReport]

as

set nocount on

declare @RedeemedPoints table
	(TipFirst			varchar(3) primary key,
	 FIName				varchar(50),
	 PointsOutstanding	numeric(18,3),
	 PointsRedeemed		numeric(18,3),
	 PointsEarned		numeric(18,3))
	 
declare @db			nvarchar(50)
declare @sql		nvarchar(4000)
declare @TipFirst	varchar(3)
declare @FIName		varchar(50)
declare @PO			numeric(18,3)
declare @PR			numeric(18,3)
declare @PE			numeric(18,3)

declare csrDB cursor FAST_FORWARD for
	select dbnumber, quotename(dbnamepatton), clientname
	from rewardsnow.dbo.dbprocessinfo dbpi
	where dbpi.welcomekitgroupname = 'R2UTP'
	
open csrDB

fetch next from csrDB into @TipFirst, @db, @FIName
while @@FETCH_STATUS = 0
BEGIN

	set @SQL = 'select @PO = SUM(runavailable), @PR = SUM(runredeemed), @PE = sum(runbalance)
				from ' + @db + '.dbo.customer'
	exec sp_executesql @SQL, 
		N'@PO numeric(18,3) OUTPUT, @PR numeric(18,3) OUTPUT, @PE numeric(18,3) OUTPUT',
		@PO = @PO OUTPUT, @PR = @PR OUTPUT, @PE = @PE OUTPUT


	insert into @RedeemedPoints
	values( @TipFirst, @FIName, @PO, @PR, @PE )

	fetch next from csrDB into @TipFirst, @db, @FIName
END

close csrdb
deallocate csrdb

select tipfirst Inst, finame as [FI Name], getdate() as RunDate, cast(PointsOutstanding as bigint) PointsOutstanding, cast(PointsRedeemed as bigint) PointsRedeemed, 
		cast(PointsEarned as bigint) PointsEarned, cast( (PointsRedeemed / PointsEarned) * 100 as numeric(18,3)) RedeemedPercent
from @RedeemedPoints


/*


exec dbo.spR2UTPRedeemedPointsReport


*/
GO
