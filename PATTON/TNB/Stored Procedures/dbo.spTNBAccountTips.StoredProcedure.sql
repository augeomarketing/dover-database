USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spTNBAccountTips]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spTNBAccountTips]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTNBAccountTips] AS

delete from dbo.accounttips

--Cursor through DBProcessinfo and get number of customer records
Declare curDBName cursor fast_forward for
	Select dbnamepatton 
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'newtnb' and tte.tipfirst is null

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		'Insert into dbo.accounttips
		 Select acctid, tipnumber from ['+@DBName+'].dbo.affiliat'
  	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName
GO
