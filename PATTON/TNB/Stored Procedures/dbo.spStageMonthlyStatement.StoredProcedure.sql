USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 10/21/2013 10:13:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyStatement]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 4/17/2008 Chged to work with LOCFCU
-- PHB 8/03/2008 Chged to work for TNB
-- SEB 10/21/2013 Changed to ad H0 and H9 codes and PP Purchased Points
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[spStageMonthlyStatement]  @StartDate datetime, @EndDate datetime

AS 

Declare  @MonthBegin char(2)
Declare  @SQLUpdate nvarchar(1000)

set @MonthBegin = month(Convert(datetime, @StartDate) )

-- Change enddate so it reflects startdate + 1 month
set @enddate = dateadd(dd, 1, @enddate)


/* Load the statement file from the customer table  */
truncate table dbo.Monthly_Statement_File

insert into dbo.Monthly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(isnull(city, '''')) + '' '' + rtrim(isnull(state, '''')) + '' '' + isnull(zipcode, '''')) as CityStateZip
from dbo.customer_Stage


--drop table #HistSum
create table #HistSum
(TipNumber		varchar(15),
 TranCode			varchar(2),
 SumPoints		bigint

 CONSTRAINT [PK_Tmp_HistSum] PRIMARY KEY CLUSTERED 
(TipNumber ASC, TranCode Asc)
)

insert into #HistSum
select tipnumber, trancode, sum(points)  -- sum(points * ratio)
from history_stage
where histdate >=  @startdate and histdate < @enddate
group by tipnumber, trancode



/* Load the statmement file with CREDIT purchases          */
update msf
	set pointspurchasedCR = tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''63''


/* Load the statmement file  with CREDIT returns            */
update msf
	set pointsreturnedCR = tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''33''


/* Load the statmement file with DEBIT purchases          */
update msf
	set pointspurchasedDB = tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''67''


/* Load the statmement file with DEBIT  returns            */
update msf 
	set pointsreturnedDB = tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''37''


/* Load the statmement file with plus adjustments    */
update msf
	set pointsadded  = tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''IE''


/* Load the statmement file with Bonuses    */
-- SEB 10/21/2013 Added the H0 and H9
update msf
	set pointsBonus  = tmp.SumPoints
from dbo.Monthly_Statement_File msf join (select tipnumber, sum(sumpoints * tt.ratio ) sumpoints
                                          from #HistSum hs join newtnb.dbo.trantype tt
												on hs.trancode = tt.trancode
                                          where hs.trancode like ''b%'' or hs.trancode like ''f%'' or hs.trancode like ''g%'' or hs.trancode like ''h%'' or hs.trancode like ''0%''
                                          group by tipnumber) tmp
                                          
	on msf.tipnumber = tmp.tipnumber



/* Add  DECREASED REDEEMED to adjustments     */
update msf
	set pointsadded = isnull(pointsadded, 0) + tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''DR''



-- PHB 10/13/2008  Added transferred Points 
/* Add expired Points */
Update msf
	set Pointsadded = isnull(pointsadded, 0) + tmp.sumpoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''XF''




-- PHB 10/13/2008  Added transferred Points 
/* Add expired Points */
Update msf
	set Pointsadded = isnull(pointsadded, 0) + tmp.sumpoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''TP''

-- SEB 10/21/2013  Added Purchased Points 
/* Add expired Points */
Update msf
	set Pointsadded = isnull(pointsadded, 0) + tmp.sumpoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''PP''


/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded


/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) 
					from dbo.History_Stage 
					where tipnumber=Monthly_Statement_File.tipnumber and 
						histdate>=@startdate 
						and histdate < @enddate 
						and trancode like ''R%'')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<@enddate and trancode like ''R%'')



/* Load the statmement file with minus adjustments    */
update msf
	set pointssubtracted = tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''DE''

update msf
	set pointssubtracted = isnull(PointsSubtracted,0) + tmp.SumPoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''IR''





/* Add EP to  minus adjustments    */
update msf
	set pointssubtracted = isnull(pointssubtracted, 0) + isnull(tmp.SumPoints, 0)
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''EP''




-- RDT 8/28/2007  Added expired Points 
/* Add expired Points */
Update msf
	set PointsExpire = tmp.SumPoints,
		PointsSubtracted = PointsSubtracted + tmp.sumpoints
from dbo.Monthly_Statement_File msf join #HistSum tmp
	on msf.tipnumber = tmp.tipnumber
where tmp.trancode = ''XP''



/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update dbo.Monthly_Statement_File
set pointsbegin=isnull((select monthbeg''+ @MonthBegin + N'' from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber), 0)
where exists(select * from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)''

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased


' 
END
GO
