USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spCombineImpTransactionTables]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spCombineImpTransactionTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spCombineImpTransactionTables]

as

truncate table dbo.impTransaction

insert into dbo.imptransaction
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, 
	CreditUnionNbr, TipNumber, MISC3, MerchantSICCode)
select AssocNbr, System, Prin, TransactionDt, ltrim(rtrim(CCAcctNbr)), TransactionCd, TransactionDesc, 
			case
				when transactionamt like '%.%' then cast(left(transactionamt, len(transactionamt) - 3) as money)
				when transactionamt like '%-%' then cast(right(transactionamt, len(transactionamt) - charindex('-', transactionamt)) as money)
				else cast(transactionamt as money)
			end as TransactionAmt, AccountSts, CreditUnionNbr, null as TipNumber, null as MISC3, null as MerchantSICCode
from dbo.impTransaction_DebitTran



insert into dbo.imptransaction
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, 
 CreditUnionNbr, TipNumber, misc3, MerchantSICCode)
select AssocNbr, System, Prin, TransactionDt, ltrim(rtrim(CCAcctNbr)), TransactionCd, TransactionDesc, 			case
				when transactionamt like '%.%' then cast(left(transactionamt, len(transactionamt) - 3) as money)
				when transactionamt like '%-%' then cast(right(transactionamt, len(transactionamt) - charindex('-', transactionamt)) as money)
				else cast(transactionamt as money)
			end as TransactionAmt, AccountSts, CreditUnionNbr, null as TipNumber, null as MISC3, MerchantSICCode
from dbo.impTransaction_R2UTran



insert into dbo.imptransaction
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, 
 CreditUnionNbr, TipNumber, misc3, MerchantSICCode)
select AssocNbr, System, Prin, TransactionDt, ltrim(rtrim(CCAcctNbr)), TransactionCd, TransactionDesc, 			case
				when transactionamt like '%.%' then cast(left(transactionamt, len(transactionamt) - 3) as money)
				when transactionamt like '%-%' then cast(right(transactionamt, len(transactionamt) - charindex('-', transactionamt)) as money)
				else cast(transactionamt as money)
			end as TransactionAmt, AccountSts, CreditUnionNbr, null as TipNumber, misc3, null as MerchantSICCode
from dbo.impTransaction_tran



insert into dbo.imptransaction
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, 
 CreditUnionNbr, TipNumber, misc3, MerchantSICCode)
select AssocNbr, System, Prin, TransactionDt, ltrim(rtrim(CCAcctNbr)), TransactionCd, TransactionDesc, 			case
				when transactionamt like '%.%' then cast(left(transactionamt, len(transactionamt) - 3) as money)
				when transactionamt like '%-%' then cast(right(transactionamt, len(transactionamt) - charindex('-', transactionamt)) as money)
				else cast(transactionamt as money)
			end as TransactionAmt, AccountSts, '' as CreditUnionNbr, null as TipNumber, null as misc3, null as MerchantSICCode
from dbo.impTransaction_Agent



insert into dbo.imptransaction
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, 
 CreditUnionNbr, TipNumber, acctnbr, MerchantSICCode)

select null as assocnbr, null as system, 'CPLX' as prin, dim_impTransaction_Complex_histdate as TransactionDt, 
		 dim_impTransaction_Complex_acctid as ccacctnbr,
		dim_impTransaction_Complex_transcode, dim_impTransaction_Complex_transdescr as TransactionDesc,
		round( abs( cast(dim_impTransaction_Complex_amount as numeric(18,2)) / 100) , 0) as TransactionAmt, ' ' as AccountSts,
		' ' as CreditUnionNbr, null as TipNumber, null, null as MerchantSICCode
from dbo.impTransaction_Complex txn 
