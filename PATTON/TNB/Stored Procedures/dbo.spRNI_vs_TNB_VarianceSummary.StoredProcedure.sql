USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spRNI_vs_TNB_VarianceSummary]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spRNI_vs_TNB_VarianceSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spRNI_vs_TNB_VarianceSummary]

as

SET NOCOUNT ON

--drop table #hist
--drop table #TNBSum

declare @Hist table
	(tipfirst			varchar(3) primary key,
      CreditPoints	     numeric(18,4) not null default (0),
	 DebitPoints		numeric(18,4) not null default (0))

declare @TNBSum table
	(TipFirst			varchar(3) primary key,
	 AssocNbrs		varchar(100) not null,
	 TNBSumPointsCredit	numeric(18,4) not null default(0),
      TNBSumPointsDebit  numeric(18,4) not null default(0))


insert into @hist
select a.tipfirst, isnull(c.points, 0), isnull(d.points, 0)
from (select distinct tipfirst from newtnb.dbo.assoc) a 
left outer join (select left(tipnumber,3) tipfirst, sum(points * ratio) points from newtnb.dbo.history_stage hs where hs.secid = 'NEW' and hs.trancode in ('63', '33') group by left(tipnumber,3) ) c
    on a.tipfirst = c.tipfirst
left outer join (select left(tipnumber,3) tipfirst, sum(points * ratio) points from newtnb.dbo.history_stage hs where hs.secid = 'NEW' and hs.trancode in ('67', '37') group by left(tipnumber,3) ) D
    on a.tipfirst = D.tipfirst


insert into @TNBSum
(tipfirst, assocnbrs, tnbsumpointscredit, tnbsumpointsdebit)
select isnull(ass.tipfirst, 'YYY') TipFirst, dbo.fnPivotAssocNbrstoString (ass.tipfirst) AssocNbrs, sum(totaltxnamountCredit) totaltxnamountCredit, sum(totalTxnAmountDebit) totaltxnamountDebit
from dbo.imp_tnbsummary tnb left outer join dbo.assoc ass
	on cast(tnb.assocnbr as varchar(3)) = cast(ass.assocnum as varchar(3))
group by ass.tipfirst, dbo.fnPivotAssocNbrstoString (ass.tipfirst)
order by ass.tipfirst




select tnb.assocnbrs AssocNbrs, tnb.tipfirst TipFirst, 
	   creditpoints, tnbsumpointscredit, (creditpoints - tnbsumpointscredit) creditvariance, 
	   case
		  when creditpoints <> 0 then cast(((creditpoints - tnbsumpointscredit) / creditpoints) *100 as numeric(18,2))
		  else 100
	   end as PctVarianceCredit,
	   debitpoints, TNBSumPointsDebit, (debitpoints - TNBSumPointsDebit) debitvariance, 
	   case
		  when debitpoints <> 0 then cast(((debitpoints - TNBSumPointsDebit) / debitpoints) *100 as numeric(18,2)) 
		  else 100
	   end as PctVarianceDebit

from @TNBSum tnb join @hist his
	on tnb.tipfirst = his.tipfirst
order by tnb.tipfirst

GO

/* Test Harness


exec  [dbo].[spRNI_vs_TNB_VarianceSummary]

select *
from dbo.imp_tnbsummary


*/


