USE [TNB]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile]    Script Date: 08/18/2014 15:36:36 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[spMonthlyStatementFile] @StartDate varchar(20), @EndDate varchar(20)
AS 
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from Monthly_Statement_File


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, 
N'IsView') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = 'create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between '''+@StartDate+''' and '''+ @EndDate +''' group by tipnumber, trancode'
exec sp_executesql @SQLDynamic

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, address4
from customer

/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File 
set pointspurchased
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '63' 

update Monthly_Statement_File 
set pointspurchased = pointspurchased +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '67' 

/* Load the statmement file CREDIT with returns            */
update Monthly_Statement_File 
set pointsreturned
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode ='33'

update Monthly_Statement_File 
set pointsreturned = pointsreturned +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode ='37'

/* Load the statmement file with bonuses            */
update Monthly_Statement_File 
set pointsbonus
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BE'

update Monthly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BI'

update Monthly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BT'

update Monthly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BN'


update Monthly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'F0'

update Monthly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'G0'

update Monthly_Statement_File 
set pointsbonus = pointsbonus +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'H0'


update Monthly_Statement_File 
set pointsbonus = pointsbonus -
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'F9'

update Monthly_Statement_File 
set pointsbonus = pointsbonus -
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'G9'

update Monthly_Statement_File 
set pointsbonus = pointsbonus -
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'H9'



/* Load the statmement file with plus adjustments */
update Monthly_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='IE'  )

update Monthly_Statement_File 
set pointsadded = pointsadded +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='TR'  )

update Monthly_Statement_File 
set pointsadded = pointsadded +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='TX'  )

update Monthly_Statement_File 
set pointsadded = pointsadded +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='TP'  )

/* Add  DECREASED REDEEMED to PointsAdded */
update Monthly_Statement_File 
set  pointsadded=pointsadded + 
 view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'


/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchased + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed =
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RM'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RT'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RU'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RV'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RC'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RS'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RP'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RG'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RB'

update Monthly_Statement_File 
set pointsredeemed = pointsredeemed + 
view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'RD'

/* Load the statmement file with Increases to Subtractions          */
update Monthly_Statement_File 
set pointssubtracted
=  view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'IR' 

update Monthly_Statement_File 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DE'

update Monthly_Statement_File 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'XP'

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased = pointsredeemed+ pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin = (select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where 
tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate

/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Drop the view */
drop view view_history_TranCode
GO


