USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTipPrefixes]    Script Date: 02/09/2010 08:17:03 ******/
DROP PROCEDURE [dbo].[spAssignTipPrefixes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spAssignTipPrefixes]

as

--=====================================================================
-- Author:  Paul H. Butler
-- Date:    16 June 2008
--
-- Descr:   Assign tip prefix to customer rows based on Association
--          number or Credit Union Number
--
-- Modificatons:
--
--
--
--=====================================================================


-----
-- Assign tip prefix by checking xref table using Association Number
-----
update imp
	set TipPrefix = xref.Tipfirst
from dbo.impCustomer imp join dbo.assoc xref
    on right('000' + imp.assocnbr,3) = right('000' + xref.assocnum,3)
where imp.tipprefix is null 



-----
-- Assign tip prefix by checking xref table using Credit Union Number
-----
update imp
	set TipPrefix = xref.Tipfirst
from dbo.impCustomer imp join dbo.assoc xref
	on imp.CreditUnionNbr = xref.CuNum
where imp.TipPrefix is null


-----
-- Assign 129 tip prefix for all others that fall out
-----
update imp
	set TipPrefix = '129'
from dbo.impCustomer imp 
where imp.TipPrefix is null
GO
