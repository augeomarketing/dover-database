USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spR2UTPWindDownExtracts]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spR2UTPWindDownExtracts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spR2UTPWindDownExtracts]

As


declare @db		nvarchar(50)
declare @sql		nvarchar(4000)



if exists(select 1 from newtnb.dbo.sysobjects where name = 'R2UTPWindDown_Cust' and xtype = 'U')
    drop table newtnb.dbo.R2UTPWindDown_Cust


create table newtnb.dbo.R2UTPWindDown_Cust
    (tipnumber		    varchar(15) primary key,
     AcctName1		    varchar(40),
     AcctName2		    varchar(40),
     AcctName3		    varchar(40),
	AcctName4		    varchar(40),
     Address1		    varchar(40),
     Address2		    varchar(40),
     City			    varchar(40),
     StateCd		    varchar(2),
     ZipCode		    varchar(15),
     HomePhone		    varchar(10),
     WorkPhone		    varchar(10),
     PointsEarned	    int,
     PointsRedeemed	    int,
     PointsBalance	    int,
     StatusCd		    varchar(1))

if exists(select 1 from newtnb.dbo.sysobjects where name = 'R2UTPWindDown_CardXref' and xtype = 'U')
    drop table newtnb.dbo.R2UTPWindDown_CardXref


create table newtnb.dbo.R2UTPWindDown_CardXref
    (TipNumber		    varchar(15),
     CCAcctNbr		    varchar(16)
    )



declare csrDB cursor fast_forward for
    select distinct quotename(dbnamepatton)
    from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
	   on dbpi.dbnumber = tte.tipfirst
    join newtnb.dbo.assoc ass
	   on dbpi.dbnumber = ass.tipfirst
    where tte.tipfirst is null
    and ass.reporttypecd = 'R'

open csrdb

fetch next from csrdb into @db

while @@FETCH_STATUS = 0
BEGIN

    set @sql = '
    insert into newtnb.dbo.R2UTPWindDown_Cust
    (tipnumber, AcctName1, AcctName2, AcctName3, AcctName4, Address1, Address2, City, StateCd, ZipCode,
     HomePhone, WorkPhone, PointsEarned, PointsRedeemed, PointsBalance, statuscd)
    select tipnumber, AcctName1, AcctName2, AcctName3, AcctName4, Address1, Address2, City, State, ZipCode,
     HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, status
    from ' + @db + '.dbo.customer 
    where status != ''C'''

    exec sp_executesql @sql


    set @sql = '
    insert into newtnb.dbo.R2UTPWindDown_CardXref
    (TipNumber, CCAcctNbr)  
    select tipnumber, acctid
    from ' + @db + '.dbo.affiliat'
--    where isnull(AcctStatus, ''A'') = ''A'' '

    exec sp_executesql @sql


    fetch next from csrdb into @db
END

close csrdb
deallocate csrdb


/*

exec dbo.spR2UTPWindDownExtracts

*/
GO
