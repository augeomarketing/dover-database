USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spQSFCurrentMonthlyActivity]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spQSFCurrentMonthlyActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spQSFCurrentMonthlyActivity]
	@EndDate			datetime
	
as

declare @db			nvarchar(50)
declare @sql		nvarchar(4000)

-- clear work tables
truncate table newtnb.dbo.Current_Month_Activity
truncate table newtnb.dbo.wrkQuarterlyStatementTipPoints

--
set @enddate = dateadd(hh, 23, @enddate)
set @enddate = dateadd(mi, 59, @enddate)
set @enddate = dateadd(ss, 59, @enddate)

select @enddate

declare csrDB cursor FAST_FORWARD for
	select quotename(dbnamepatton)
	from rewardsnow.dbo.dbprocessinfo dbpi join newtnb.dbo.statementrun sr
		on dbpi.dbnumber = sr.tipfirst
	where statementruncomplete = 'N'
	
open csrDB

fetch next from csrDB into @db
while @@FETCH_STATUS = 0
BEGIN

	set @sql = '
				insert into newtnb.dbo.Current_Month_Activity 
					(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
				select tipnumber, RunAvailable,0 ,0 ,0 
				from ' + @db + '.dbo.Customer c'

	exec sp_executesql @sql


	set @sql = '
				insert into newtnb.dbo.wrkQuarterlyStatementtipPoints
				select h.tipnumber, ratio, sum(points)
				from ' + @db + '.dbo.history h join newtnb.dbo.Current_Month_Activity cma
						on h.tipnumber = cma.tipnumber
				where ratio in (1, -1) and h.histdate > @enddate
				group by h.tipnumber, ratio'
	exec sp_executesql @sql, N'@EndDate datetime', @EndDate = @EndDate

	fetch next from csrDB into @db
END

close csrDB
deallocate csrDB

update cma
	set increases = isnull(points, 0)
from newtnb.dbo.current_month_activity cma left outer join newtnb.dbo.wrkQuarterlyStatementTipPoints tmp
	on cma.tipnumber = tmp.tipnumber
where ratio = '1'

update cma
	set decreases = isnull(points, 0)
from newtnb.dbo.current_month_activity cma left outer join newtnb.dbo.wrkQuarterlyStatementtipPoints tmp
	on cma.tipnumber = tmp.tipnumber
where ratio = '-1'


/* Load the calculate the adjusted ending balance        */
update newtnb.dbo.Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
