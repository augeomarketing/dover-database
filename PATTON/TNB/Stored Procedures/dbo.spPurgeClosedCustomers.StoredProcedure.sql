USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 02/09/2010 08:17:05 ******/
if object_id('spPurgeClosedCustomers') is not null
    DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read impCustomer_Purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.

--RDT 3/21/08 and tipnumber Not In ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

--PHB 4/15/08 refactored sql inserts/updates.  Changed to match the tables used for 218LOCFCU

/******************************************************************************/
create PROCEDURE [dbo].[spPurgeClosedCustomers]  
	@Production_Flag	char(1), 
	@DateDeleted		datetime,
	@TipFirst			varchar(3) = null

AS

Declare @SQLDynamic nvarchar(4000)
declare @SQL		nvarchar(4000)
Declare @DBName	varchar(50)

Declare @Tipnumber 	char(15)


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin

	truncate table dbo.impcustomer_Purge


	insert into dbo.impCustomer_Purge
	(TipNumber, CCAcctNbr, PrimaryNm)
	select TipNumber, CCAcctNbr, PrimaryNm
	from dbo.impCustomer_Purge_Pending ipp 
	

	truncate table dbo.impCustomer_Purge_Pending
	
	
	--
	-- Add in replacement cards for tips that are in the OptOut table.
	-- These are cases where TNB is still sending us the customers even
	-- though they opted-out.  If the cards are not added into the optout table,
	-- the account will be deleted, and then re-added back in - causing monthly audit errors.
	--
	
	-- create temp table of tip# and lastname
	create table #oot
		(tipnumber		varchar(15) primary key,
		 lastname			varchar(50))

	insert into #oot
	select distinct tipnumber, oot.lastname
	from rewardsnow.dbo.optouttracking oot join rewardsnow.dbo.dbprocessinfo dbpi
		on left(oot.tipnumber,3) = dbpi.dbnumber
		and 'newtnb' = dbpi.dbnamenexl
	left outer join newtnb.dbo.tipstoexclude tte
		on dbnumber = tte.tipfirst
	where tte.tipfirst is null


	-- create temp table of card numbers in opt out table
	create table #optoutcards
		(acctid		varchar(25))
		
	insert into #optoutcards
	select distinct acctid
	from rewardsnow.dbo.optouttracking oot join rewardsnow.dbo.dbprocessinfo dbpi
		on left(oot.tipnumber,3) = dbpi.dbnumber
		and 'newtnb' = dbpi.dbnamenexl
	left outer join newtnb.dbo.tipstoexclude tte
		on dbnumber = tte.tipfirst
	where tte.tipfirst is null


	-- Add in the replacement cards
	insert into rewardsnow.dbo.optouttracking
	(TipPrefix, TIPNUMBER, ACCTID, FIRSTNAME, LASTNAME, OPTOUTDATE, OPTOUTSOURCE, OPTOUTPOSTED)

	select left(imp.tipnumber,3) tipprefix, imp.tipnumber, ccacctnbr, primarynm, oot.lastname, '04/01/2009' optoutdate,
			'PROCESSING' optoutsource, '04/01/2009'
	from dbo.impcustomer imp join #oot oot
		on imp.tipnumber = oot.tipnumber

	left outer join #optoutcards ooc
		on imp.ccacctnbr = ooc.acctid
	where ooc.acctid is null

	
	-- Get all the opt outs
	Insert Into dbo.impCustomer_Purge
	(TipNumber, CCAcctNbr, PrimaryNm)
	Select TipNumber, AcctId, FirstName
	from rewardsnow.dbo.OptOutTracking oot join rewardsnow.dbo.dbprocessinfo dbpi
		on oot.tipprefix = dbpi.dbnumber
	left outer join newtnb.dbo.tipstoexclude tte
		on oot.tipprefix = tte.tipfirst
		
	where tte.tipfirst is null and dbnamenexl = 'newtnb'
	and tipnumber not in (select distinct tipnumber 
							from dbo.history 
							where histdate > @DateDeleted and trancode not in ('RQ'))


     -- 8 Sept. 2010 PHB
     -- Update affiliat_stage card status from TNB's deleted card file.
     --
	update aff
	   set acctstatus = 'C'
     from dbo.affiliat_stage aff join dbo.impDeletedCards idc
	   on aff.acctid = idc.ccacctnbr


     -- 29 Oct. 2010 PHB
     -- Update affiliat_stage card status from Complex's deleted card file.
     --
	update aff
	   set acctstatus = 'C'
     from dbo.affiliat_stage aff join dbo.impDeletedCards_Complex idc
	   on aff.acctid = idc.acctid
	
	
	--
	-- Now check closed/Charged off/bankrupt/revoked/fraud/Interest Prohibited accounts
	-- IMPORTANT.  these types of accounts can only be closed if ALL the cards associated to the tip are closed
	--
	
	-- Get count of all card#s associated to a tip using AFFILIAT_STAGE
	select tipnumber, count(*) cardcount
	into #all
	from dbo.affiliat_stage
	group by tipnumber


	-- Get count of all cards associated to a tip, that have a status C in at least one of their rows
	select tipnumber, count(*) cardcount
	into #closed
	from dbo.affiliat_stage
	where acctstatus in ('C')
	group by tipnumber

	select distinct a.tipnumber
	into #purge_c_z
	from #all a join #closed c
		on a.tipnumber = c.tipnumber
	where a.cardcount = c.cardcount

	-- For tips that have ALL the cards marked as closed, add them to the purge.
	insert into dbo.impcustomer_purge
	(tipnumber, ccacctnbr, primarynm)
	select aff.tipnumber, aff.acctid, aff.lastname
	from #purge_c_z cz join dbo.affiliat_stage aff
		on cz.tipnumber = aff.tipnumber
	left outer join dbo.impcustomer_purge pg
		on aff.acctid = pg.ccacctnbr
	where pg.ccacctnbr is null

--
-- purge tips without any activity for a specific period.  this is configured in the newtnb.dbo.assoc table.  Period of inactivity
-- is configurable by FI.  Period is in months
--
    if object_id('tempdb..#tipfirstrollingpurge') is not null  drop table #tipfirstrollingpurge
    if object_id('tempdb..#tipsnoactivity') is not null   drop table #tipsnoactivity


    create table #TipFirstRollingPurge
	   (tipfirst	    varchar(3) primary key,
	    InactivePurgePeriodMonths int not null)

    create table #TipsNoActivity
	   (tipnumber		varchar(15) primary key)

    insert into #TipFirstRollingPurge
    select distinct tipfirst, InactivePurgePeriodMonths
    from newtnb.dbo.assoc
    where InactivePurgeFlag = 'Y'


    --
    -- By FI, get the max hist date for each tip.  Compare that to the inactive period configured.
    -- The InactivePurgePeriodMonths is subtracted from the @DateDeleted.  NOte that the month is stored in the config
    -- table as a positive value.  In the below having clause, it is then multiplied by -1 so it will be subtracted
    -- The resultant date is then incremented by 1 day.
    -- Ex.
    -- @DateDeleted = '05/31/2010'
    -- InactivePurgePeriodMonths = 12
    -- Subtracting 12 months from @Datedeleted gets us to 5/31/2009
    -- Add one day to that so its now  6/1/2009.
    -- Any tips that have a max hist date less than 6/1/2009 would now be purged
    -- Remember that this is FI specific
    -- And that transactions such as "RQ" (Brochure request), and "XP"/"XF" (Expiring Points/ExpiringPointFix) are not included
    insert into #tipsnoactivity
    select h.tipnumber
    from dbo.history h join #TipFirstRollingPurge tmp
	   on left(h.tipnumber,3) = tmp.tipfirst
    where h.trancode not in ('RQ', 'XP', 'XF')
    group by tipnumber
    having max(histdate) < max( dateadd(dd, 1, dateadd(mm, inactivepurgeperiodmonths * -1, @DateDeleted) ) )


    --
    -- Based on the temp table #tipsnoactivity, insert the tips and affiliated cards into the impcustomer_purge
    -- table.  A left outer join is done against the impcustomer_purge table to prevent duplicates
    insert into dbo.impcustomer_purge
    (tipnumber, ccacctnbr, primarynm)
    select c.tipnumber, aff.acctid, c.acctname1
    from #tipsnoactivity tmp join dbo.customer_stage c
	   on tmp.tipnumber = c.tipnumber
    join dbo.affiliat_stage aff
	   on c.tipnumber = aff.tipnumber
    left outer join newtnb.dbo.impcustomer_purge prg
	   on c.tipnumber = prg.tipnumber
	   and aff.acctid = prg.ccacctnbr
    where prg.tipnumber is null


	--
	-- For accounts with activity after the monthend date, put them into purge_pending
	-- then remove them from purge
	--
	insert into dbo.impcustomer_purge_pending
	(tipnumber, ccacctnbr, primarynm)
	select distinct pg.tipnumber, pg.ccacctnbr, pg.primarynm
	from dbo.impcustomer_purge pg
	where pg.tipnumber in (select tipnumber from dbo.history_stage his 
					   where histdate > @DateDeleted
					   and trancode != 'RQ')

	delete pg
	from dbo.impcustomer_purge pg join dbo.impcustomer_purge_pending pp
		on pg.tipnumber = pp.tipnumber

/****  Add in customer_stage, Affiliat_stage and History_Stage rows to their corresponding _deleted tables ****/
	create table #Stage_Deleted (tipnumber	varchar(15) primary key)

	insert into #Stage_Deleted
	select distinct tipnumber
	from dbo.impcustomer_purge

	-- Customer_Stage	
	insert into dbo.customer_stage_deleted
	(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, 
	 Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, 
	 RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, 
	 BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, BonusFlag, DateDeleted)
	select stg.TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, 
		  Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, 
		  RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, 
		  BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, BonusFlag, @DateDeleted
	from #stage_deleted sd join dbo.customer_stage stg
		on sd.tipnumber = stg.tipnumber


	-- Affiliat_Stage
	insert into dbo.affiliat_stage_deleted
	(TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
	select aff.TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, @DateDeleted
	from #stage_deleted sd join dbo.affiliat_stage aff
		on sd.tipnumber = aff.tipnumber


	-- History_Stage
	insert into dbo.history_stage_deleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	select h.TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, @DateDeleted
	from #stage_deleted sd join history_stage h
		on sd.tipnumber = h.tipnumber



	--
	-- Begin removing purged tips from import tables and staging tables
	--

	delete cus
	from dbo.impCustomer cus join dbo.impCustomer_Purge prg
		on cus.tipnumber = prg.tipnumber
		

	delete txn
	from dbo.impTransaction txn join dbo.impCustomer_Purge prg
		on txn.tipnumber = prg.tipnumber


	delete aff
	from dbo.affiliat_stage aff join dbo.impCustomer_Purge dlt
		on aff.tipnumber = dlt.tipnumber


	delete cus
	from dbo.customer_stage cus join dbo.impCustomer_Purge dlt
		on cus.tipnumber = dlt.tipnumber


	delete his
	from dbo.history_stage his join dbo.impCustomer_Purge dlt
		on his.tipnumber = dlt.tipnumber


End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin



	set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

	-- copy any impCustomer_Purge_pending into impCustomer_Purge
	 
	Insert into dbo.impCustomer_Purge
	(TipNumber, CCAcctNbr, PrimaryNm)
	select TipNumber, CCAcctNbr, PrimaryNm
	from dbo.impCustomer_Purge_Pending
	where left(tipnumber,3) = @TipFirst

	-- Clear impCustomer_Purge_Pending 
	delete from dbo.impCustomer_Purge_Pending where left(tipnumber,3) = @TipFirst

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(TipNumber, CCAcctNbr, PrimaryNm)
	select distinct imp.TipNumber, CCAcctNbr, PrimaryNm
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	'RQ' != his.trancode
		where left(imp.tipnumber,3) = @TipFirst


	if exists(select 1 from dbo.sysobjects where name = 'wrkPurgeTips' and xtype = 'U')
		drop table dbo.wrkPurgeTips
	
	create table dbo.wrkPurgeTips (TipNumber varchar(15) primary key)

	insert into dbo.wrkPurgeTips
	select distinct tipnumber from dbo.impcustomer_purge


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	set @sql = '
	Delete imp
		from dbo.wrkPurgeTips imp join ' + @dbname + '.dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @DateDeleted
			and	his.trancode != ''RQ''
		where left(imp.tipnumber,3) = @TipFirst'
	exec sp_executesql @SQL, N'@datedeleted datetime, @TipFirst varchar(3)', @datedeleted = @datedeleted, @TipFirst = @tipfirst



	-- Insert customer to customerdeleted
	set @SQL = '
	Insert Into ' + @dbname + '.dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from ' + @dbname + '.dbo.Customer c join newtnb.dbo.wrkPurgeTips prg on c.tipnumber = prg.tipnumber
	where left(c.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

	exec sp_executesql @sql, N'@DateDeleted datetime', @DateDeleted = @DateDeleted




	-- Insert affiliat to affiliatdeleted 
	set @SQL = '
	Insert Into ' + @dbname + '.dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
	from ' + @dbname + '.dbo.Affiliat aff join dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber
	where left(aff.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

	exec sp_executesql @sql, N'@DateDeleted datetime', @DateDeleted = @DateDeleted

	-- copy history to historyDeleted 
	set @SQL = '
	Insert Into ' + @dbname + '.dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from ' + @dbname + '.dbo.History H join newtnb.dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber
	where left(h.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

	exec sp_executesql @sql, N'@DateDeleted datetime', @DateDeleted = @DateDeleted


	-- Delete from customer 
	set @sql = '
	Delete c
	from ' + @dbname + '.dbo.Customer c join newtnb.dbo.wrkPurgeTips prg
		on c.tipnumber = prg.tipnumber'

	exec sp_executesql @sql


	-- Delete records from affiliat 
	set @SQL = '
	Delete aff
	from ' + @dbname + '.dbo.Affiliat aff join newtnb.dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber'

	exec sp_executesql @SQL


	-- Delete records from History 
	set @SQL = '
	Delete h
	from ' + @dbname + '.dbo.History h join newtnb.dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber'
	
	exec sp_executesql @SQL


	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	set @SQL = '
	Update c
		set status = ''C''
	from ' + @dbname + '.dbo.customer c join dbo.impCustomer_Purge_Pending prg
		on c.tipnumber = prg.tipnumber'

	exec sp_executesql @SQL


End
GO

