USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spAddStatementFileToStatementFileAudit]    Script Date: 02/09/2010 08:17:03 ******/
DROP PROCEDURE [dbo].[spAddStatementFileToStatementFileAudit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spAddStatementFileToStatementFileAudit]
as
-- SEB 9/2013 Added PurchasedPoints


insert into dbo.Quarterly_Statement_File_AUDIT
(Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, City, State, ZipCode, STDATE, 
 PointsBegin, PointsEnd, PointsBonus, PointsPurchasedDB, PointsBonusDB, PointsPurchasedCR, PointsBonusCR, POINTSBONUSMN , PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, PointsExpire, 
 AcctID, BankNum, HomePhone, WorkPhone, status, PurchasedPoints, Points2Expire)
select Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, City, State, ZipCode, STDATE, 
 PointsBegin, PointsEnd, PointsBonus, PointsPurchasedDB, PointsBonusDB, PointsPurchasedCR, PointsBonusCR, POINTSBONUSMN , PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, PointsExpire, 
 AcctID, BankNum, HomePhone, WorkPhone, status, PurchasedPoints, Points2Expire
from dbo.Quarterly_Statement_File
GO
