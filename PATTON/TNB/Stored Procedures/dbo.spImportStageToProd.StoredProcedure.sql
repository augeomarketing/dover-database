USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spImportStageToProd]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spImportStageToProd]
	(@TipFirst		varchar(3))
AS 


declare @dbname		varchar(50)
declare @SQL			nvarchar(4000)

set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage

set @SQL = '
	Update a
		set YTDEarned = S.YTDEarned,
		    AcctStatus = s.AcctStatus
	from dbo.Affiliat_Stage S join ' + @dbname + '.dbo.Affiliat A 
		on S.Tipnumber = A.Tipnumber
		and s.acctid = a.acctid'

exec sp_executesql @SQL

--	Insert New Affiliat accounts
set @SQL = '
	Insert into ' +  @dbname + '.dbo.Affiliat 
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag)
		select stg.ACCTID, stg.TIPNUMBER, stg.AcctType, stg.DATEADDED, stg.SECID, stg.AcctStatus, stg.AcctTypeDesc, 
			stg.LastName, stg.YTDEarned, stg.CustID, stg.AffiliatFlag
		from dbo.Affiliat_Stage stg left outer join ' + @dbname + '.dbo.Affiliat aff
			on stg.tipnumber = aff.tipnumber
			and stg.acctid = aff.acctid
		where aff.Tipnumber is null and left(stg.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

exec sp_executesql @SQL


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 

set @SQL = '
	Update c
		Set 
		Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
		Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
		Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
		Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
		City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
		Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
		Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
		Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
	From dbo.Customer_Stage S Join ' + @dbname + '.dbo.Customer C 
		On S.Tipnumber = C.Tipnumber'

exec sp_executesql @SQL


-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update dbo.Customer_Stage 	
	Set RunAvailable = 0 


--	Insert New Customers from Customers_Stage

set @SQL = '
	Insert into ' + @dbname + '.dbo.Customer
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
		 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, 
		 ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, 
		 EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, 
		 RunBalanceNew, RunAvaliableNew)
		select stg.TIPNUMBER, stg.RunAvailable, stg.RUNBALANCE, stg.RunRedeemed, stg.LastStmtDate, stg.NextStmtDate, stg.STATUS, stg.DATEADDED, 
			  stg.LASTNAME, stg.TIPFIRST, stg.TIPLAST, stg.ACCTNAME1, stg.ACCTNAME2, stg.ACCTNAME3, stg.ACCTNAME4, stg.ACCTNAME5, stg.ACCTNAME6, stg.ADDRESS1, 
			  stg.ADDRESS2, stg.ADDRESS3, stg.ADDRESS4, stg.City, stg.State, stg.ZipCode, stg.StatusDescription, stg.HOMEPHONE, stg.WORKPHONE, stg.BusinessFlag, 
			  stg.EmployeeFlag, stg.SegmentCode, stg.ComboStmt, stg.RewardsOnline, stg.NOTES, stg.BonusFlag, stg.Misc1, stg.Misc2, stg.Misc3, stg.Misc4, stg.Misc5, 
			  stg.RunBalanceNew, stg.RunAvaliableNew
		from dbo.Customer_Stage stg left outer join ' + @dbname + '.dbo.Customer cus
		on stg.tipnumber = cus.tipnumber
		where cus.Tipnumber is null and left(stg.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

exec sp_executesql @SQL


--	Add RunBalanceNew (net) to RunBalance and RunAvailable 

set @SQL = '
	Update C 
		set RunBalance = C.RunBalance + S.RunAvaliableNew,
			 RunAvailable = C.RunAvailable + S.RunAvaliableNew
	From dbo.Customer_Stage S Join ' + @dbname + '.dbo.Customer C 
		On S.Tipnumber = C.Tipnumber'

exec sp_executesql @SQL


----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .

set @SQL = '
	Insert Into ' + @dbname + '.dbo.History 
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, ''OLD'', Ratio, Overage 
	from dbo.History_Stage where SecID = ''NEW'' and left(tipnumber,3) = ' + char(39) + @Tipfirst + char(39)

exec sp_executesql @SQL

-- Set SecID in History_Stage so it doesn't get double posted. 
Update dbo.History_Stage  
	set SECID = 'Imported to Production '+ convert(char(20), GetDate(), 120)  
where SecID = 'NEW'
and left(tipnumber,3) = @tipfirst

----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.

insert into dbo.OneTimeBonuses
( TipNumber, Trancode, AcctID, DateAwarded)
select stg.TipNumber, stg.Trancode, stg.AcctID, stg.DateAwarded
from dbo.OneTimeBonuses_stage stg left outer join dbo.OneTimeBonuses otb
	on stg.TipNumber = otb.tipnumber
	and stg.trancode = otb.trancode
where otb.tipnumber is null
and left(stg.tipnumber,3) = @tipfirst


delete from dbo.Customer_Stage where left(tipnumber,3) = @tipfirst
delete from dbo.affiliat_stage where left(tipnumber,3) = @tipfirst
delete from dbo.history_stage  where left(tipnumber,3) = @tipfirst
delete from dbo.OneTimebonuses_stage  where left(tipnumber,3) = @tipfirst

--
GO
