USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spTNBExpirePoints]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spTNBExpirePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spTNBExpirePoints]
	(@MonthEndDate			datetime)

AS

declare @TipFirst		varchar(3)
declare @ExpireFreqCd	varchar(2)
declare @ExpireYears	int

declare @StoredProc		varchar(255)
declare @SQL			nvarchar(4000)

declare @errmsg		nvarchar(4000)
declare @rc			int

declare @strMonthEndDate	varchar(10)

set @strMonthEndDate = 
		cast( year(@MonthEndDate) as varchar(4)) + '/' +
		right( '00' + cast(month(@MonthEndDate) as varchar(2)), 2) + '/' +
		right( '00' + cast(day(@MonthEndDate) as varchar(2)), 2) 



declare csrTips cursor FAST_FORWARD for
	select tipfirst, PointsExpireFrequencyCd, PointExpirationYears
	from dbo.client
	where PointsExpireFrequencyCd is not null

open csrTips

fetch next from csrTips into @TipFirst, @ExpireFreqCd, @ExpireYears

while @@FETCH_STATUS = 0
BEGIN

	print 'Processing TIP: ' + @TipFirst

	if @ExpireFreqCd = 'ME'  OR  -- If Points Expire Monthly  OR
	   (@ExpireFreqCd = 'YE' AND month(@MonthEndDate) = 12) -- Points expire yearly, and it is year end (December)
	BEGIN
		-- Get point expiration stored procedure
		set @StoredProc = (select PointExpireStoredProc
					from dbo.TipFirstPointExpireControl TFPEC join dbo.PointExpireControl PEC
						on TFPEC.sid_PointExpireControl_Id = PEC.sid_PointExpireControl_Id
					where TFPEC.tipfirst = @TipFirst
					and	 getdate() between TipFirstPointExpireControlEffectiveDate and TipFirstPointExpireControlExpirationDate)

		--
		-- call Stored Proc.  NOTE: sproc MUST accept TipFirst and MonthEndDate as parms
		--

		set @SQL = 'exec ' + @StoredProc + '  ' + char(39) + @TipFirst + char(39) + ',  ' + char(39) + @strMonthEndDate + char(39) + ', ' + @ExpireYears

		print '+-----  ' + @sql
		exec @rc = sp_executesql @sql

		if @rc != 0  -- If Error Occurred
		BEGIN
			set @errmsg = 'Failure: ' + @sql
			raiserror(@errmsg, 11, 1)	
		END

		print 'Completed Processing TIP: ' + @TipFirst
		print ' '
		print ' '
		print ' '

	END  -- End if @ExpireFreqCd......

	fetch next from csrTips into @TipFirst, @ExpireFreqCd, @ExpireYears
END

close csrTips

deallocate csrTips
GO
