USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePoints_148Citizens]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spExpirePoints_148Citizens]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spExpirePoints_148Citizens]
	(@TipFirst		varchar(3),
	 @MonthEndDate		datetime,
	 @NumberOfYears	int)

AS


/*

--
-- Expire points for 148 Citizens only
--

declare @MonthBeg VARCHAR(10)

Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
Declare @PointsToExpireNext int
Declare @AddPointsNext int
Declare @PointsprevExpired int

DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)

declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intmonthnext int
declare @intyear int
declare @ExpireDate DATETIME

declare @MonthEndDatenext DATETIME

SET @MonthBeg = right( '00' + cast( month(@MonthEndDate) as varchar(2)), 2) + '/01/' +
			 cast( year(@MonthEndDate) as varchar(4))


set @MonthEndDatenext = cast(@MonthBeg as datetime)
set @MonthEndDatenext = Dateadd(month, 2, @MonthBeg)

--set @MonthEndDatenext = Dateadd(month, 1, @MonthBeg)
--set @MonthEndDatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDatenext)),121)

print 'month end date'
print @MonthEndDate
print 'month end date next'
print @MonthEndDatenext

set @expirationdate = cast(@MonthEndDate as datetime)
set @expirationdatenext = cast(@MonthEndDatenext as datetime)

set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)

--set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -@NumberOfYears, @expirationdate)),121)

print 'Expiration Date: ' + @expirationdate
set @expirationdatenext = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdatenext)),121)
set @expirationdatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdatenext)),121)
 

	TRUNCATE TABLE expiringpoints

	INSERT   into expiringpoints
	 SELECT tipnumber, '0' as addpoints,
	'0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as prevExpired, DateOfExpire = @expirationdate,
	'0' as PointstoExpireNext,'0' as addpointsnext, '0' as EXPTODATE, '0' as ExpiredRefunded
	--from history
	from customer
	--where histdate < @expirationdate and trancode like('t%')
	group by tipnumber

	UPDATE expiringpoints  
	SET addpoints = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE trancode  like 't%'
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER
		 and histdate =< @expiredate) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE trancode  like 't%'
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER
		 and histdate =< @expiredate) 


	UPDATE expiringpoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 


	UPDATE expiringpoints  
	SET prevExpired = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
   		  trancode = 'XP'   
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
   		  trancode = 'XP'  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER 	and	 histdate > @expirationdate)
 
	UPDATE expiringpoints  
	SET addpointsnext = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
 		 histdate < @expirationdatenext and (trancode not like('R%') and
	         trancode <> 'IR') 
 	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER)
 
	UPDATE expiringpoints  
	SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
 		 trancode = 'XF'
 	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER)

 	UPDATE expiringpoints  
	SET PREVEXPIRED = (PREVEXPIRED * -1)

	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = ((ADDPOINTS + REDPOINTS)  - PREVEXPIRED)

	UPDATE expiringpoints  
	SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)

	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE IS NULL 
	or 
	POINTSTOEXPIRE < '0' 

	UPDATE expiringpoints  
	SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))



	UPDATE expiringpoints  
	SET POINTSTOEXPIREnext = '0'
	WHERE
	POINTSTOEXPIREnext IS NULL 
	or 
	POINTSTOEXPIREnext < '0' 


	UPDATE expiringpoints  
	SET EXPTODATE = '0'
	WHERE
	EXPTODATE IS NULL 
	or 
	EXPTODATE < '0' 


*/
GO
