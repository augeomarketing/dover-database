use newtnb
go

if exists(select 1 from dbo.sysobjects where name = 'spUndeleteTip2' and xtype = 'P')
	drop procedure dbo.spUndeleteTip2
go

create procedure dbo.spUndeleteTip2
	@TipNumber			varchar(15),
	@DateDeleted		datetime
	
AS

declare @SQL					nvarchar(4000)
declare @db						nvarchar(50)
declare @dbWeb					nvarchar(50)
declare @dbServerWeb			nvarchar(50)
declare @TipFirst				nvarchar(3)
declare @TipExists				int
declare @rc						int
declare @ErrMsg					nvarchar(1000)

set @TipFirst = left(@TipNumber,3)

set @db =	 (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)
set @dbWeb = (select quotename(dbnameNexl) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)
set @DBServerWeb = 'RN1'

--
-- Make sure we have a valid database
--
if @db is null or @dbweb is null
begin
	set @ErrMsg = 'Database for TIP: ' + @tipnumber + ' not found.'
	raiserror(@ErrMsg, 16, 1)
	goto badend
end

--
-- Check to make sure tip does NOT exist in the active customer table for the FI
--
set @sql = 'select @TipExists = count(*) from ' + @db + '.dbo.customer where tipnumber = ' + char(39) + @TipNumber + char(39)
exec sp_executesql @SQL, N'@TipExists int OUTPUT', @TipExists = @TipExists OUTPUT

if @TipExists <> 0
BEGIN
	set @ErrMsg = 'Tip: ' + @tipnumber + ' already exists in the active customer table in database: ' + @db + '.'
	raiserror (@ErrMsg, 16, 1)
	goto badend
END


--
-- At this point, the undelete can proceed.
--

BEGIN TRAN PATTON_UnDelete

-- Undelete Customer
set @SQL = 
	'insert into ' + @db + '.dbo.customer
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, 
		 TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
		 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, 
		 RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	 select top 1 TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, 
		 TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, 
		 City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, 
		 RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	 from ' + @db + '.dbo.customerdeleted
	 where tipnumber = ' + char(39) + @TipNumber + char(39) + 
	 ' order by datedeleted desc'
	 
	exec @rc = sp_executesql @SQL
	
	if @rc <> 0
	BEGIN
		set @ErrMsg = 'Error inserting into ' + @db + '.dbo.customer.'
		raiserror (@ErrMsg, 16, 1)
		goto badend
	END
	
-- Now remove rows from customerdeleted
	set @sql = 'delete from ' + @db + '.dbo.customerdeleted where tipnumber = ' + char(39) + @tipnumber + char(39)
	exec @rc = sp_executesql @SQL
	if @rc = 0
		print '		CustomerDeleted rows removed successfully'
	else
		print '*****ERROR:  Rows not removed from Customerdeleted'


-- Undelete Affiliat
set @SQL = 
	'insert into ' + @db + '.dbo.affiliat
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	 select distinct ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
	 from ' + @db + '.dbo.affiliatdeleted
	 where tipnumber = ' + char(39) + @TipNumber + char(39)
	 
	 exec @rc = sp_executesql @SQL
	 
	if @rc <> 0
	BEGIN
		set @ErrMsg = 'Error inserting into ' + @db + '.dbo.affiliat.'
		raiserror (@ErrMsg, 16, 1)
		goto badend
	END

-- Now remove rows from affiliatdeleted
	set @sql = 'delete from ' + @db + '.dbo.affiliatdeleted where tipnumber = ' + char(39) + @tipnumber + char(39)
	exec @rc = sp_executesql @SQL
	if @rc = 0
		print '		AffiliatDeleted rows removed successfully'
	else
		print '*****ERROR:  Rows not removed from Affiliatdeleted'



-- Undelete History
set @SQL = 
	'insert into ' + @db + '.dbo.History
	 (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	 select distinct TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	 from ' + @db + '.dbo.historydeleted
	 where tipnumber = ' + char(39) + @TipNumber + char(39)
	 
	 exec @rc = sp_executesql @SQL
	 
	if @rc <> 0
	BEGIN
		set @ErrMsg = 'Error inserting into ' + @db + '.dbo.history.'
		raiserror (@ErrMsg, 16, 1)
		goto badend
	END
	
-- Now remove rows from affiliatdeleted
	set @sql = 'delete from ' + @db + '.dbo.historydeleted where tipnumber = ' + char(39) + @tipnumber + char(39)
	exec @rc = sp_executesql @SQL
	if @rc = 0
		print '		HistoryDeleted rows removed successfully'
	else
		print '*****ERROR:  Rows not removed from Historydeleted'

	 
--
-- Commit changes to PATTON here.  Can't keep an open transaction across multiple servers
--

COMMIT TRAN PATTON_UnDelete



--
-- Now add them into the web
--

-- Customer
print 'Adding tip to Web Customer.....'

	set @sql = '
		insert into ' + @DBServerWeb + '.' + @dbweb + '.dbo.customer
			(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
			 EarnedBalance, Redeemed, AvailableBal, Status, city, state)
		select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
				address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
		from ' + @db + '.dbo.customer
		where tipnumber = ' + char(39) + @TipNumber + char(39)
print @sql		
	exec @rc = sp_executesql @SQL
	if @rc <> 0
	BEGIN
		set @ErrMsg = 'Error inserting into ' + @db + '.dbo.CUSTOMER on the web database server.'
		raiserror (@ErrMsg, 16, 1)
		goto badendWeb
	END
print 'Done adding tip to Web Customer.'
print ' '
print ' '


-- Account
print 'Adding tip to Web Account.....'
	set @sql = '
		insert into ' + @DBServerWeb + '.' + @dbWeb + '.dbo.account (TipNumber, LastName, LastSix, SSNLast4)
		select aff.tipnumber, isnull(aff.lastname, ''NotAssigned'') , right(acctid,6), null
		from ' + @db + '.dbo.affiliat aff join ' + @db + '.dbo.customer cus
			on aff.tipnumber = cus.tipnumber
		where cus.tipnumber = ' + char(39) + @TipNumber + char(39)
print @sql		
	exec @rc = sp_executesql @sql
	if @rc <> 0
	BEGIN
		set @ErrMsg = 'Error inserting into ' + @db + '.dbo.Account on the web database server.'
		raiserror (@ErrMsg, 16, 1)
		goto badendWeb
	END
print 'Done adding tip to Web Account.'
print ' '
print ' '


-- 1Security
print 'Adding tip to 1Security.....'
	set @sql = 'insert into ' + @DBServerWeb + '.' + @dbWeb + '.dbo.[1Security] (TipNumber)
		values(' + char(39) + @TipNumber + char(39) + ')'
print @sql				
	exec @RC = sp_executesql @Sql
	if @rc <> 0
	BEGIN
		set @ErrMsg = 'Error inserting into ' + @db + '.dbo.1Security on the web database server.'
		raiserror (@ErrMsg, 16, 1)
		goto badendWeb
	END
print 'Done adding tip to Web 1Security.'




-- Statement

-- First check if row exists in statement file
set @sql = 'select @TipExists = count(*) from ' + @DBServerWeb + '.' + @dbWeb + '.dbo.statement where travnum = ' + char(39) + @TipNumber + char(39)
exec sp_executesql @SQL, N'@TipExists int OUTPUT', @TipExists = @TipExists OUTPUT

if @TipExists = 0
BEGIN
	print 'Adding tip to Statement table.....'
		set @SQL = '
			insert into ' + @DBServerWeb + '.' + @dbWeb + '.dbo.Statement
			(TRAVNUM, ACCTNAME1, PNTBEG, PNTEND, PNTPRCHS, PNTBONUS, PNTADD, PNTINCRS, PNTREDEM, PNTRETRN, PNTSUBTR, PNTDECRS)
			select Tipnumber as Travnum, Acctname1, PointsBegin as PntBeg, PointsEnd as PntEnd, PointsPurchasedCR + PointsPurchasedDB as PntPrchs, 
				PointsBonus as PntBonus, PointsAdded as PntAdd, PointsIncreased as PntIncrs, PointsRedeemed as PntRedem, PointsReturnedCR + PointsReturnedDB as PntRetrn,
				PointsSubtracted as PntSubtr, PointsDecreased as PntDecrs
			from newtnb.dbo.Monthly_Statement_File stmt
			where tipnumber = ' + char(39) + @TipNumber + char(39)

	print @sql		
		exec @rc = sp_executesql @sql
		if @rc <> 0
		BEGIN
			set @ErrMsg = 'Error inserting into ' + @db + '.dbo.Statement on the web database server.'
			raiserror (@ErrMsg, 16, 1)
			goto badendWeb
		END
	print 'Done adding tip to Web Statement.'
	print ' '
	print ' '	
END

		
GOTO VERYEND


badend:
rollback tran PATTON_UnDelete

badendWeb:

VERYEND:

--
-- All done
--


/*


exec spUndeleteTip '135000000002370'

exec spUndeleteTip '163000000001010'

exec spUndeleteTip '165000000002140'

exec dbo.spUndeleteTip '121000000003493'

exec dbo.spUndeleteTip '185000000002913'

exec dbo.spUndeleteTip '160000000001421'

exec dbo.spUndeleteTip '160000000004357'

exec dbo.spUndeleteTip '135000000003120'  *******  ERROR Dupe in Affiliat ************

exec dbo.spUndeletetip '129000000003722'

exec dbo.spUndeleteTip '112000000001674'


*/

