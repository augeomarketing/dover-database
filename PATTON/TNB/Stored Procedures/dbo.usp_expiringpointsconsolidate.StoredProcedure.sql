use newtnb
GO

if object_id('newtnb.dbo.usp_expiringpointsconsolidate') is not null
    drop procedure dbo.usp_expiringpointsconsolidate
GO


create procedure dbo.usp_expiringpointsconsolidate
    @MonthEndDate		   datetime

as


declare @db nvarchar(50)
declare @sql nvarchar(4000)

truncate table dbo.expiringpoints

declare csrdb cursor fast_forward for
	select  quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbProcessInfo 
	where PointsExpireFrequencyCd not in ('MM','YE','NS')
		and  PointExpirationYears <> 0 and datejoined is not null 
		and datejoined <= Dateadd(year, -PointExpirationYears, @monthenddate)
		and sid_fiprodstatus_statuscode = 'P'  
		and dbnamenexl = 'newtnb' and dbnumber not in ('000', '111')

open csrdb

fetch next from csrdb into @db
while @@FETCH_STATUS = 0
BEGIN

	set @sql = 'insert into dbo.EXPIRINGPOINTS
			  (tipnumber, CredAddPoints, DebAddpoints, CredRetPoints, DebRetPoints, CredAddPointsNext, 
			   DebAddPointsNext, CredRetPointsNext, DebRetPointsNext, REDPOINTS, POINTSTOEXPIRE, PREVEXPIRED, 
			   DateofExpire, PointsToExpireNext, AddPoints, ADDPOINTSNEXT, PointsOther, PointsOthernext, 
			   EXPTODATE, ExpiredRefunded, dbnameonnexl, CredaddpointsPLUS3MO, DEbaddpointsPLUS3MO, 
			   CredRetpointsPLUS3MO, DEbRetpointsPLUS3MO, POINTSTOEXPIREPLUS3MO, ADDPOINTSPLUS3MO, 
			   PointsOtherPLUS3MO)

				select  tipnumber, CredAddPoints, DebAddpoints, CredRetPoints, DebRetPoints, CredAddPointsNext,
				DebAddPointsNext, CredRetPointsNext, DebRetPointsNext, REDPOINTS, POINTSTOEXPIRE, PREVEXPIRED, 
				DateofExpire, PointsToExpireNext, AddPoints, ADDPOINTSNEXT, PointsOther, PointsOthernext, 
				EXPTODATE, ExpiredRefunded, dbnameonnexl, CredaddpointsPLUS3MO, DEbaddpointsPLUS3MO, 
    				CredRetpointsPLUS3MO, DEbRetpointsPLUS3MO, POINTSTOEXPIREPLUS3MO, ADDPOINTSPLUS3MO, 
				PointsOtherPLUS3MO

				from ' + @db + '.dbo.expiringpoints'

	exec sp_executesql @sql

	fetch next from csrdb into @db
END

close csrdb
deallocate csrdb


/*

exec dbo.usp_expiringpointsconsolidate '08/31/2010'

select * from dbo.expiringpoints

*/