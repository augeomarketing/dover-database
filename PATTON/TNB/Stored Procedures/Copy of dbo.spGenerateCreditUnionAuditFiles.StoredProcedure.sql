use newtnb
GO

if exists(select 1 from dbo.sysobjects where name = 'spGenerateCreditUnionAuditFiles' and xtype = 'P')
	drop procedure dbo.spGenerateCreditUnionAuditFiles
GO


create procedure dbo.spGenerateCreditUnionAuditFiles
	@CUNum	varchar(4),
     @MonthEndDate datetime
AS

set nocount on

declare @assoc		varchar(3)
declare @TipFirst	varchar(3)

set @MonthEndDate = dateadd(dd, 1, @MonthEndDate)
set @MonthEndDate = dateadd(ms, -3, @MonthEndDate)

select top 1 @assoc = assocnum, @TipFirst = tipfirst  from dbo.assoc where cunum = @CUNum

create table #Custbalances
    (TipNumber		varchar(15) primary key,
     RunBalance	int,
     RunRedeemed	int,
     RunAvailable	int)

insert into #CustBalances
select tipnumber, runbalance, runredeemed, runavailable
from dbo.customer_stage
where left(tipnumber,3) = @TipFirst

Update cb
    set runavailable = runavailable + (points * ratio * -1),
	   runredeemed = runredeemed - (points * ratio * -1)
from #CustBalances cb join dbo.history_stage his
    on cb.tipnumber = his.tipnumber
where (his.trancode like 'r%' ) and his.secid = 'OLD'  and histdate >= @MonthEndDate


Update cb
    set runavailable = runavailable - (points * ratio * -1),
	   runredeemed = runredeemed + (points * ratio * -1)
from #CustBalances cb join dbo.history_stage his
    on cb.tipnumber = his.tipnumber
where his.trancode in ('IR')  and his.secid = 'OLD'    and histdate >= @MonthEndDate



Update cb
    set runavailable = runavailable + (points * ratio),
	   runredeemed = runredeemed - (points * ratio)
from #CustBalances cb join dbo.history_stage his
    on cb.tipnumber = his.tipnumber
where his.trancode in ('DR')  and his.secid = 'OLD'    and histdate >= @MonthEndDate



Update cb
    set runbalance = runbalance + (points * ratio * -1),
	   RunAvailable = runAvailable + (points * ratio * -1)
from #CustBalances cb join dbo.history_stage his
    on cb.tipnumber = his.tipnumber
where his.trancode not like 'r%' and his.trancode not in ('IR', 'DR') and his.secid = 'OLD'  and histdate >= @MonthEndDate




select distinct @Assoc AssocNum, stg.tipnumber,
					isnull( (select top 1 acctid from dbo.affiliat_stage cus where cus.tipnumber = stg.tipnumber and acctstatus = 'A' order by dateadded desc),
					        (select top 1 acctid from dbo.affiliat_stage cus where cus.tipnumber = stg.tipnumber order by dateadded desc)) as PriAcctNum, 
	acctname1, acctname2, address1, address2, address3, city + ',  ' + state + '   ' + zipcode as CityStateZip, 
	zipcode, homephone, workphone, dateadded, cb.runbalance, cb.runredeemed, cb.runavailable, status
from dbo.assoc ass join dbo.customer_stage stg
	on ass.tipfirst = stg.tipfirst

join #CustBalances cb
    on stg.tipnumber = cb.tipnumber

where ass.cunum = @CUNum


