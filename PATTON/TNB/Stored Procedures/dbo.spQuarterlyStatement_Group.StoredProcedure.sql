USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement_Group]    Script Date: 10/21/2013 10:13:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatement_Group]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatement_Group]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatement_Group]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 7/02/2008 Revised to work for 218 LOC FCU
-- SEB 9/2013 Added PurchasedPoints
-- SEB 10/21/2013 Added Bonus codes H0 and H9
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[spQuarterlyStatement_Group]  @StartDate datetime, @EndDate datetime, @StatementGroup nvarchar(50)

AS 
declare @SQL				nvarchar(4000)
declare @DB					nvarchar(100)
declare @TipFirst			nvarchar(3)

Declare  @MonthBegin varchar(2)
Declare  @SQLUpdate nvarchar(1000)
declare  @AssocNbrs nvarchar(100)

set @MonthBegin = month(Convert(datetime, @StartDate) )

-- 
-- clear the quarterly statement file and the statement summary file
truncate table dbo.Quarterly_Statement_File
truncate table dbo.qsfHistorySummary

--
set @enddate = dateadd(hh, 23, @enddate)
set @enddate = dateadd(mi, 59, @enddate)
set @enddate = dateadd(ss, 59, @enddate)

--
-- Loop through databases that match the group name
declare csrDB cursor fast_forward for
	select dbnumber as TipFirst, quotename(dbnamepatton) dbnamepatton
	from rewardsnow.dbo.dbprocessinfo dbpi join newtnb.dbo.statementrun sr
		on dbpi.dbnumber = sr.tipfirst
	where WelcomeKitGroupName = @StatementGroup 

open csrDB

fetch next from csrDB into @TipFirst, @DB

while @@FETCH_STATUS = 0
BEGIN

	set @AssocNbrs = (SELECT [NewTNB].[dbo].[fnPivotAssocNbrstoString] (@TipFirst)  )

	-- Build the history summary 
	set @SQL = ''
			insert into dbo.qsfHistorySummary
			(TipNumber, TranCode, Points)
			select tipnumber, trancode, sum(points) points
			from '' + @DB + ''.dbo.history
			where histdate between @startdate and @endDate
			group by tipnumber, trancode''
	print @SQL
	exec sp_executesql @SQL, N''@startdate datetime, @enddate datetime'', @startdate = @startdate, @enddate = @enddate


	--
	-- Add in customer data
	set @SQL = ''
		insert into dbo.Quarterly_Statement_File 
		(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, city, state, zipcode, stdate, 
		 banknum, AcctId, HomePhone, WorkPhone, Status)
		select cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
				(rtrim(cus.city) + '''' '''' + rtrim(cus.state) + '''' '''' + cus.zipcode), cus.city, cus.state, cus.zipcode, @enddate, '' + char(39) +@AssocNbrs + char(39) + '',
			(Select top 1 AcctId from '' + @DB + ''.dbo.affiliat aff where aff.tipnumber = cus.tipnumber) AcctId,
			cus.homephone, cus.workphone, cus.status
		from '' + @DB + ''.dbo.customer cus
		where left(cus.tipnumber,3) = '' + char(39) + @TipFirst + char(39) 
	print @SQL
	exec sp_executesql @SQL, N''@EndDate datetime'', @EndDate = @EndDate


	/* Load the statmement file with CREDIT purchases          */
	update qsf
		set pointspurchasedCR = pointspurchasedCR + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in (''63'')), 0)
	from dbo.quarterly_statement_file qsf
	where left(qsf.tipnumber,3) = @TipFirst
	
	
	update qsf
		set pointspurchasedDB = pointspurchasedDB + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in (''67'')), 0)
	from dbo.quarterly_statement_file qsf
	where left(qsf.tipnumber,3) = @TipFirst


	/* Load the statmement file  with CREDIT returns            */
	update qsf
		set pointsreturnedCR = pointsreturnedCR + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in (''33'')), 0)
	from dbo.quarterly_statement_file qsf
	where left(qsf.tipnumber,3) = @TipFirst

	update qsf
		set pointsreturnedDB = pointsreturnedDB + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in (''37'')), 0)
	from dbo.quarterly_statement_file qsf
	where left(qsf.tipnumber,3) = @TipFirst
	

	/* Load the statmement file with bonuses            */
	update qsf
		set pointsbonus = pointsbonus + hs.points
	from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
		on qsf.tipnumber = hs.tipnumber
	where hs.trancode like ''b%''
	and left(qsf.tipnumber,3) = @TipFirst

	/* Load the statmement file with Merchant Bonuses   */
	update qsf
		set POINTSBONUSMN = POINTSBONUSMN + hs.points
	from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
		on qsf.tipnumber = hs.tipnumber
	where hs.trancode in (''F0'',''G0'',''H0'')
	and left(qsf.tipnumber,3) = @TipFirst

	update qsf
		set POINTSBONUSMN = POINTSBONUSMN - hs.points
	from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
		on qsf.tipnumber = hs.tipnumber
	where hs.trancode in (''F9'',''G9'',''H9'')
	and left(qsf.tipnumber,3) = @TipFirst
		
	/* Load the statmement file with plus adjustments    */
	update qsf
		set pointsadded = pointsadded + hs.points
	from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
		on qsf.tipnumber = hs.tipnumber
	where hs.trancode in (''IE'', ''TR'', ''TX'', ''TP'', ''DR'', ''XF'')
	and left(qsf.tipnumber,3) = @TipFirst

/* Load the statmement file with Purchased Points    */

	update qsf
		set PurchasedPoints = PurchasedPoints + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in (''PP'')), 0)
	from dbo.quarterly_statement_file qsf
	where left(qsf.tipnumber,3) = @TipFirst
	
	/* Load the statmement file with total point increases */
update dbo.Quarterly_Statement_File
	set pointsincreased= pointspurchaseddb + pointspurchasedcr + pointsbonus + pointsadded + PurchasedPoints + POINTSBONUSMN
where left(tipnumber,3) = @TipFirst

	/* Load the statmement file with redemptions          */
	--update qsf
	--	set pointsredeemed = pointsredeemed + hs.points
	--from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
	--	on qsf.tipnumber = hs.tipnumber
	--where hs.trancode like ''R%''

	update qsf
		set pointsredeemed = isnull((select sum(points) 
								from dbo.qsfHistorySummary hs 
								where hs.tipnumber = qsf.tipnumber 
								and hs.trancode like ''r%''), 0)
	from dbo.Quarterly_Statement_File qsf
	where left(qsf.tipnumber,3) = @TipFirst


	/* Load the statmement file with minus adjustments    */
	update qsf
		set pointssubtracted = pointssubtracted + hs.points
	from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
		on qsf.tipnumber = hs.tipnumber
	where hs.trancode in (''DE'', ''IR'', ''EP'')
	and left(qsf.tipnumber,3) = @TipFirst


	/* Add expired Points */
	update qsf
		set pointsexpire = pointsexpire + hs.points
	from dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
		on qsf.tipnumber = hs.tipnumber
	where hs.trancode in(''XP'')
	and left(qsf.tipnumber,3) = @TipFirst


	/* Load the statmement file with total point decreases */
	update dbo.Quarterly_Statement_File
		set pointsdecreased=pointsredeemed + pointsreturnedCR + PointsReturnedDB + pointssubtracted + PointsExpire
	where left(tipnumber,3) = @TipFirst

	/* Load the statmement file with the Beginning balance for the Month */
	set @SQL =	''update qsf
					set pointsbegin = monthbeg'' + @MonthBegin + N''
				from dbo.Quarterly_Statement_File qsf join dbo.beginning_balance_table bbt
					on qsf.tipnumber = bbt.tipnumber
				where left(qsf.tipnumber,3) = '' + char(39) + @TipFirst + char(39)
	print @SQL
	exec sp_executesql @SQL


	/* Load the statmement file with beginning points */
	update dbo.Quarterly_Statement_File
		set pointsend=pointsbegin + pointsincreased - pointsdecreased
	where left(tipnumber,3) = @TipFirst



	fetch next from csrDB into @TipFirst, @DB
END

close csrDB
deallocate csrDB
' 
END
GO
