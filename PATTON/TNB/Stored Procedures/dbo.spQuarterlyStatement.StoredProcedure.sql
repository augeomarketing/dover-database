USE [NewTNB]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement]    Script Date: 03/07/2014 14:56:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatement]
GO

USE [NewTNB]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement]    Script Date: 03/07/2014 14:56:09 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spQuarterlyStatement]  @StartDate datetime, @EndDate datetime, @TipFirst varchar(3)

AS 

DECLARE	@SQL				nvarchar(max)
	,	@DB					nvarchar(100)	=	(SELECT QUOTENAME(dbnamepatton) FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @TipFirst)
	,	@AssocNbrs			nvarchar(100)	=	(SELECT dbo.fnPivotAssocNbrstoString(@TipFirst))
	,	@MonthBegin			varchar(2)		=	MONTH(CONVERT(DATETIME, @StartDate))
	,	@StatementFormat	int				=	(SELECT TOP 1 sid_quarterlystatementformat_id FROM dbo.assoc WHERE tipfirst = @tipfirst)


-- clear the quarterly statement file and the statement summary file
TRUNCATE TABLE dbo.Quarterly_Statement_File
TRUNCATE TABLE dbo.qsfHistorySummary

-- Build the history summary 
SET	@SQL	=	'
				INSERT INTO	dbo.qsfHistorySummary
					(TipNumber, TranCode, Points)
				SELECT		tipnumber, trancode, sum(points) points
				FROM		<<DBNAME>>.dbo.history
				WHERE		CAST(histdate as DATE) >= CAST(@startdate as DATE) and CAST(histdate as DATE) <= CAST(@endDate as DATE)
				GROUP BY	tipnumber, trancode
				'
SET	@SQL	=	REPLACE(@sql,'<<DBNAME>>', @DB)
--PRINT	@SQL
EXEC	sp_executesql @SQL, N'@startdate datetime, @enddate datetime', @startdate = @startdate, @enddate = @enddate


-- Add in customer data
SET	@SQL	=	'
				INSERT INTO	dbo.Quarterly_Statement_File 
					(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, city, state, 
					 zipcode, stdate, banknum, AcctId, homephone, workphone, status)
				SELECT		cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
							(rtrim(cus.city) + '' '' + rtrim(cus.state) + '' '' + cus.zipcode), cus.city, cus.state, 
							cus.zipcode, @enddate, ''<<ASSOC>>'',
							(Select top 1 AcctId from <<DBNAME>>.dbo.affiliat aff where aff.tipnumber = cus.tipnumber) AcctId,
							cus.homephone, cus.workphone, cus.status
				FROM		<<DBNAME>>.dbo.customer cus
				WHERE		left(cus.tipnumber,3) = ''<<TIPFIRST>>''
				'
SET	@SQL	=	REPLACE(@sql,'<<DBNAME>>', @DB)
SET	@SQL	=	REPLACE(@sql,'<<ASSOC>>', @AssocNbrs)
SET	@SQL	=	REPLACE(@sql,'<<TIPFIRST>>', @TipFirst)
--PRINT	@SQL
EXEC	sp_executesql @SQL, N'@EndDate datetime', @EndDate = @EndDate

--------------------------SPEND--------------------------------------------------------------
---CREDIT SPEND
UPDATE	qsf
SET		pointspurchasedCR = pointspurchasedCR + isnull((select	sum(points)
														from	dbo.qsfHistorySummary hs
														where	hs.tipnumber = qsf.tipnumber
															and	hs.trancode in ('63')), 0)
FROM	dbo.quarterly_statement_file qsf
--DEBIT SPEND
UPDATE	qsf
SET		pointspurchasedDB = pointspurchasedDB + isnull((select	sum(points)
														from	dbo.qsfHistorySummary hs
														where	hs.tipnumber = qsf.tipnumber
															and	hs.trancode in ('67')), 0)
FROM	 dbo.quarterly_statement_file qsf
--CREDIT RETURN
UPDATE	qsf
SET		pointsreturnedCR = pointsreturnedCR + isnull((	select	sum(points)
														from	dbo.qsfHistorySummary hs
														where	hs.tipnumber = qsf.tipnumber
															and	hs.trancode in ('33')), 0)
FROM	dbo.quarterly_statement_file qsf
--DEBIT RETURN
UPDATE	qsf
SET		pointsreturnedDB = pointsreturnedDB + isnull((	select	sum(points)
														from	dbo.qsfHistorySummary hs
														where	hs.tipnumber = qsf.tipnumber
															and	hs.trancode in ('37')), 0)
FROM	dbo.quarterly_statement_file qsf

--------------------------BONUS--------------------------------------------------------------
--STANDARD BONUSES
UPDATE	qsf
SET		pointsbonus = isnull((	select	sum(points) 
								from	dbo.qsfHistorySummary hs 
								where	hs.tipnumber = qsf.tipnumber 
									and	(hs.trancode like 'B%' or hs.trancode like 'F%' or hs.trancode like 'G%' or hs.trancode like '0%')
									and	hs.trancode not in ('F0', 'G0', 'F9', 'G9')), 0)
FROM	dbo.Quarterly_Statement_File qsf
--MERCHANT SPEND BONUS
UPDATE	qsf
SET		POINTSBONUSMN = isnull((select	sum(points) 
								from	dbo.qsfHistorySummary hs 
								where	hs.tipnumber = qsf.tipnumber 
									and	hs.trancode in ('F0', 'G0', 'H0')), 0)
FROM	dbo.Quarterly_Statement_File qsf
--MERCHANT RETURN BONUS
UPDATE	qsf
SET		POINTSBONUSMN = pointsbonusmn - isnull((select	sum(points) 
												from	dbo.qsfHistorySummary hs 
												where	hs.tipnumber = qsf.tipnumber 
													and	hs.trancode in ('F9', 'G9', 'H9')), 0)
FROM	dbo.Quarterly_Statement_File qsf


--------------------------POSITIVE ADJUSTMENTS-------------------------------------------------
--POINT ADDS
UPDATE	qsf
SET		pointsadded = pointsadded + isnull((select	sum(points)
											from	dbo.qsfHistorySummary hs
											where	hs.tipnumber = qsf.tipnumber
												and	hs.trancode in ('IE', 'TR', 'TX', 'TP', 'DR', 'XF')), 0)
FROM	dbo.quarterly_statement_file qsf
--PURHCASED POINTS
UPDATE	qsf
SET		PurchasedPoints = isnull((	select	sum(points)
									from	dbo.qsfHistorySummary hs
									where	hs.tipnumber = qsf.tipnumber
										and	hs.trancode in ('PP')), 0)
FROM	dbo.quarterly_statement_file qsf

--------------------------POINT INCREASES------------------------------------------------------
UPDATE	dbo.Quarterly_Statement_File
SET		pointsincreased = pointspurchaseddb + pointspurchasedcr + pointsbonus + pointsadded + POINTSBONUSMN + PurchasedPoints
-----------------------------------------------------------------------------------------------

--------------------------REDEMPTIONS----------------------------------------------------------
UPDATE	qsf
SET		pointsredeemed = isnull((	select	sum(points) 
									from	dbo.qsfHistorySummary hs 
									where	hs.tipnumber = qsf.tipnumber 
										and	hs.trancode like 'R%'), 0)
FROM	dbo.Quarterly_Statement_File qsf

--------------------------POSITIVE ADJUSTMENTS-------------------------------------------------
--POINT SUBS
UPDATE	qsf
SET		pointssubtracted = isnull((	select	sum(points)
									from	dbo.qsfHistorySummary hs
									where	hs.tipnumber = qsf.tipnumber
										and	hs.trancode in  ('DE', 'IR', 'EP')), 0)
FROM	 dbo.quarterly_statement_file qsf
--EXPIRED POINTS
UPDATE	qsf
SET		pointsexpire = pointsexpire + hs.points
FROM	dbo.Quarterly_Statement_File qsf join dbo.qsfHistorySummary hs
	ON	qsf.tipnumber = hs.tipnumber
WHERE	hs.trancode = 'XP'

--------------------------POINT DECREASES------------------------------------------------------
UPDATE	dbo.Quarterly_Statement_File
SET		pointsdecreased = pointsredeemed +  pointsreturneddb + pointsreturnedcr + pointssubtracted + PointsExpire
-----------------------------------------------------------------------------------------------

--POINTS EXPIRED PROJECTION
EXEC RewardsNow.dbo.usp_ExpirePoints @tipfirst, 1
UPDATE	qsf
SET		Points2Expire = isnull(e.points_expiring, 0)
FROM	dbo.quarterly_statement_file qsf join RewardsNow.dbo.RNIExpirationProjection e on qsf.Tipnumber = e.tipnumber

/* Load the statmement file with the Beginning balance for the Month */
SET		@SQL =	'
				UPDATE	qsf
				SET		pointsbegin = monthbeg<<MTHBEGIN>>
				FROM	dbo.Quarterly_Statement_File qsf join dbo.beginning_balance_table bbt
					ON	qsf.tipnumber = bbt.tipnumber
				'
SET	@SQL	=	REPLACE(@sql,'<<MTHBEGIN>>', @MonthBegin)
--PRINT	@SQL
EXEC	sp_executesql @SQL

/* Load the statmement file with beginning points */
UPDATE	dbo.Quarterly_Statement_File
SET		pointsend = pointsbegin + pointsincreased - pointsdecreased


GO