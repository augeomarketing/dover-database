USE [NewTNB]
GO

/****** Object:  StoredProcedure [dbo].[spTNBMonthlySummaryExtract]    Script Date: 05/10/2010 14:22:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTNBMonthlySummaryExtract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spTNBMonthlySummaryExtract]
GO

USE [NewTNB]
GO

/****** Object:  StoredProcedure [dbo].[spTNBMonthlySummaryExtract]    Script Date: 05/10/2010 14:22:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[spTNBMonthlySummaryExtract]
				@RuNDate datetime

As



declare @db		nvarchar(50)
declare @sql		nvarchar(4000)

declare @strYTDDate varchar(10)
declare @YTDdate datetime

declare @year int
declare @month int

set @year = case
			 when month(@RunDate) = 1 then year(@RunDate) - 1
			 else year(@RunDate)
		  end

set @month = case
			 when month(@RunDate) = 1 then 12
			 else month(@RunDate) - 1
		  end

set @strYTDDate = cast(@year as varchar(4)) + '/' +
				right('00' + cast(@month as varchar(2)), 2) + '/01 00:00:00'

set @YTDDate = cast(@strYTDDate as datetime)



if exists(select 1 from newtnb.dbo.sysobjects where name = 'TNBMonthlySummaryExtract_Cust' and xtype = 'U')
    drop table newtnb.dbo.TNBMonthlySummaryExtract_Cust


create table newtnb.dbo.TNBMonthlySummaryExtract_Cust
    (tipnumber				  varchar(15) primary key,
     AcctName1				  varchar(40),
     AcctName2				  varchar(40),
     AcctName3				  varchar(40),
	AcctName4				  varchar(40),
     Address1				  varchar(40),
     Address2				  varchar(40),
     City					  varchar(40),
     StateCd				  varchar(2),
     ZipCode				  varchar(15),
     HomePhone				  varchar(10),
     WorkPhone				  varchar(10),
     PointsEarned			  int default(0),
     PointsRedeemed			  int default(0),
     PointsBalance			  int default(0),
     LastMonthPointsRedeemed	  int default(0),
     LastMonthPointsAdjusted	  int default(0),
     LastMonthPointsEarned	  int default(0),
     YTDPointsRedeemed		  int default(0),
     StatusCd				  varchar(1))

if exists(select 1 from newtnb.dbo.sysobjects where name = 'TNBMonthlySummaryExtract_CardXref' and xtype = 'U')
    drop table newtnb.dbo.TNBMonthlySummaryExtract_CardXref


create table newtnb.dbo.TNBMonthlySummaryExtract_CardXref
    (TipNumber		    varchar(15),
     CCAcctNbr		    varchar(16) primary key
    )



declare csrDB cursor fast_forward for
    select distinct quotename(dbnamepatton)
    from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
	   on dbpi.dbnumber = tte.tipfirst
    join newtnb.dbo.assoc ass
	   on dbpi.dbnumber = ass.tipfirst
    where tte.tipfirst is null
    and sid_fiprodstatus_statuscode in ('P', 'V')


open csrdb

fetch next from csrdb into @db

while @@FETCH_STATUS = 0
BEGIN

    set @sql = '
    insert into newtnb.dbo.TNBMonthlySummaryExtract_Cust
    (tipnumber, AcctName1, AcctName2, AcctName3, AcctName4, Address1, Address2, City, StateCd, ZipCode,
     HomePhone, WorkPhone, PointsEarned, PointsRedeemed, PointsBalance, LastMonthPointsRedeemed, YTDPointsRedeemed, statuscd)

    select c.tipnumber, c.acctname1, c.acctname2, c.acctname3, c.acctname4, c.address1, c.address2, c.city, c.state, c.zipcode,
		  c.homephone, c.workphone, c.runbalance, c.runredeemed, c.runavailable, 
	   	   isnull(mtd.mtdredemptions, 0) LastMonthRedemptions, isnull(ytd.ytdredemptions, 0) YTDRedemptions, c.status

    from ' + @db + '.dbo.customer c left outer join (select tipnumber, sum(points) mtdredemptions
	   							 from ' + @db + '.dbo.history
								 where trancode like ''R%'' and
								    year(histdate) = @year and
								    month(histdate) = @month
								 group by tipnumber) mtd
        on c.tipnumber = mtd.tipnumber

    left outer join (select tipnumber, sum(points) ytdredemptions
				 from ' + @db + '.dbo.history
				 where trancode like ''R%'' and
				    year(histdate) = @year
				    and histdate < @YTDDate
				 group by tipnumber) ytd
        on c.tipnumber = ytd.tipnumber

    where status != ''C''


    





'

    exec sp_executesql @sql, N'@year int, @month int, @YTDDate datetime', @year, @month, @YTDDate


    set @sql = '
    insert into newtnb.dbo.TNBMonthlySummaryExtract_CardXref
    (TipNumber, CCAcctNbr)  
    select tipnumber, acctid
    from ' + @db + '.dbo.affiliat'
--    where isnull(AcctStatus, ''A'') = ''A'' '

    exec sp_executesql @sql


    fetch next from csrdb into @db
END

close csrdb
deallocate csrdb


/*

exec dbo.spTNBMonthlySummaryExtract '03/02/2010'



select *
from newtnb.dbo.TNBMonthlySummaryExtract_Cust


*/
