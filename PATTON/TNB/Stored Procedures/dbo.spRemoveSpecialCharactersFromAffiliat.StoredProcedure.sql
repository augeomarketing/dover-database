USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharactersFromAffiliat]    Script Date: 02/09/2010 08:17:05 ******/
/******************************************************************************/
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 1/2011                                                              */
/* REVISION: 1                                                               */
/*                                                                           */
/* REASON: Add code to remove #, `, forward slash, carriage return, extra spaces */
/******************************************************************************/
DROP PROCEDURE [dbo].[spRemoveSpecialCharactersFromAffiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spRemoveSpecialCharactersFromAffiliat]
AS

/*  34 is double quote */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(34), '')

/*  39 is apostrophe */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(39), '')

/*  44 is commas */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(44), '')

/*  46 is period */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(46), '')
 
/*  140 is ` backwards apostrophe  */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(140), '')

/*************************************/
/* Start SEB001                      */
/*************************************/
/*  35 is # */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(35), '')

/*  96 is ` */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(96), '')

/*  47 is forward slash */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(47), '')

/*  13 is Carriage Return */
Update dbo.Affiliat_stage set lastname=replace(lastname,char(13), '')

/*  Extra Blanks */
Update dbo.Affiliat_stage set lastname=rtrim(ltrim(replace(replace(replace(lastname,' ','<>'),'><',''),'<>',' '))) 

/*************************************/
/* END SEB001                      */
/*************************************/

GO
