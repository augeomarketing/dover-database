USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spInputScrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables  */
/*																*/
/********************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS

/* clear error tables */
Truncate Table  ImpCustomer_Error
Truncate Table ImpTransaction_Error


--------------- Input Customer table
/* Remove input_customer records with Name = null */
Insert into dbo.ImpCustomer_error
(CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, AddressLine2, City, StateCd, Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr)
	select  CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, AddressLine2, City, StateCd, Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr
	from dbo.ImpCustomer 
	where (PrimaryNm is null) OR (CCAcctNbr is null) OR (CCAcctNbr = '')  or (ccacctnbr = 'null')

delete from dbo.ImpCustomer 
	where (PrimaryNm is null) OR (CCAcctNbr is null) OR (CCAcctNbr = '') or (ccacctnbr = 'null')


/* remove leading and trailing spaces in names and addresses */ 
Update dbo.ImpCustomer 
	set	CCAcctNbr =		ltrim(rtrim(CCAcctNbr)), 
		PrimaryNm =		ltrim(rtrim(PrimaryNm)), 
		SecondaryNm =		ltrim(rtrim(SecondaryNm)), 
		OldCCAcctNbr =		ltrim(rtrim(OldCCAcctNbr)),	
		AddressLine1 =		ltrim(rtrim(AddressLine1)), 
		AddressLine2 =		ltrim(rtrim(AddressLine2)), 
		City =			ltrim(rtrim(City)), 
		StateCd =			ltrim(rtrim(StateCd)), 
		Zip =			ltrim(rtrim(Zip)),		
		Phone1 =			ltrim(rtrim(Phone1)), 
		Phone2 =			ltrim(rtrim(Phone2)), 
		PrimarySSN =		ltrim(rtrim(PrimarySSN)), 
		AssocNbr =		ltrim(rtrim(AssocNbr)), 
		CheckingNbr =		ltrim(rtrim(CheckingNbr)), 
		CreditUnionNbr =	ltrim(rtrim(CreditUnionNbr))

/* Check if there are transactions which there are no matching customers */
Insert into dbo.ImpTransaction_error 
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts)
select txn.AssocNbr, txn.System, txn.Prin, txn.TransactionDt, txn.CCAcctNbr, txn.TransactionCd, txn.TransactionDesc, 
		cast(txn.TransactionAmt as varchar(10)) as TxnAmt, txn.AccountSts 
from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
	on txn.CCAcctNbr = cus.CCAcctNbr

left outer join dbo.impcustomer cus2
	on txn.ccacctnbr = cus2.oldccacctnbr 

left outer join dbo.affiliat_stage aff
	on txn.ccacctnbr = aff.acctid
where ( (cus.CCAcctNbr is null and cus2.oldccacctnbr is null and aff.tipnumber is null) or txn.ccacctnbr = '') 




/* Check if there are transactions without valid tran codes */
Insert into dbo.ImpTransaction_error 
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, AcctNbr)

select txn.AssocNbr, txn.System, txn.Prin, txn.TransactionDt, txn.CCAcctNbr, txn.TransactionCd, txn.TransactionDesc, 
		cast(txn.TransactionAmt as varchar(10)) as TxnAmt, txn.AccountSts, txn.AcctNbr
from dbo.ImpTransaction txn
where transactioncd is null


Delete txn
from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
	on txn.CCAcctNbr = cus.CCAcctNbr

left outer join dbo.impcustomer cus2
	on txn.ccacctnbr = cus2.oldccacctnbr 

left outer join dbo.affiliat_stage aff
	on txn.ccacctnbr = aff.acctid
where ( (cus.CCAcctNbr is null and cus2.oldccacctnbr is null and aff.tipnumber is null) or txn.ccacctnbr = '') 


delete from dbo.impTransaction
	where transactioncd is null


------------------------------------------------------------
-- Now, purge transactions with account numbers in the 
-- AccountNumberstoDelete table
------------------------------------------------------------

delete txn
from dbo.accountnumberstodelete antd join dbo.imptransaction txn
	on antd.acctnum = txn.ccacctnbr

delete ic
from dbo.accountnumberstodelete antd join dbo.impcustomer ic
	on antd.acctnum = ic.ccacctnbr



------------------------------------------------------------
-- Purge rows with MISC3 = "AUTO" - these have been
-- copied to 222FirstFinancialAuto
------------------------------------------------------------

delete txn
from dbo.impTransaction txn join dbo.impCustomer cus
	on txn.ccacctnbr = cus.ccacctnbr
where cus.MISC3 = 'AUTO'


delete from dbo.impCustomer
where MISC3 = 'AUTO'



------------------------------------------------------------
-- Change checking DDA to empty string where it is all zeroes
------------------------------------------------------------
update dbo.impcustomer
	set CheckingNbr = ''
where checkingNbr is null or checkingNbr =  '0000000000000000' 


------------------------------------------------------------
-- Change OLD CC Acct# from null to empty string
------------------------------------------------------------
update dbo.impcustomer
	set oldccacctnbr = ''
where oldccacctnbr is null or oldccacctnbr = '0000000000000000'


------------------------------------------------------------
-- default association# with 883 if association# = " "
------------------------------------------------------------
update dbo.impCustomer
	set AssocNbr = '883'
where AssocNbr = ' '


------------------------------------------------------------
-- Next, insert into dbo.LostCards all cards with a 
-- IStatus or Estatus of "L" - Lost.
------------------------------------------------------------

insert into dbo.LostCards
(ASSNUM, TFPRE, ACCTNUM, NAME1, NAME2, ESTATUS, ISTATUS, OLDCCNUM, ADDRESS1, ADDRESS2,
 CITY, STATE, ZIP, PHONE1, PHONE2, SSNUM, CHECKING, CUNUM)
select AssocNbr, TipPrefix, CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, AddressLine2, 
 City, StateCd, Zip, Phone1, Phone2, PrimarySSN, CheckingNbr, CreditUnionNbr
from dbo.impCustomer
where externalsts in ('L', 'U')

------------------------------------------------------------
-- Update status codes based on StatusCode_CrossReference
------------------------------------------------------------

Update cus
	set RewardsNowSts = xref.RNStatusCd
from dbo.StatusCode_CrossREference xref join dbo.impCustomer cus
on cus.ExternalSts = xref.TNBStatusCd
and 'E' = xref.statuscodetype


Update cus
	set RewardsNowSts = xref.RNStatusCd
from dbo.StatusCode_CrossREference xref join dbo.impCustomer cus
on cus.InternalSts = xref.TNBStatusCd
and 'I' = xref.statuscodetype
where isnull(rewardsnowsts, '') = ''  -- only update rewardsnowsts if there isn't an external status


update dbo.impcustomer
	set rewardsnowsts = 'A'
where rewardsnowsts is null


/*
------------------------------------------------------------
-- Update AddressLine6
------------------------------------------------------------

Update dbo.impCustomer
	set AddressLine6 = ltrim(rtrim(city)) + ' ' + ltrim(rtrim(statecd)) + ' ' + ltrim(rtrim(zip))
*/


------------------------------------------------------------
-- Translate TNB transaction codes to RewardsNow tran codes
------------------------------------------------------------
update dbo.impTransaction
	set RNTranCode = case
					when TransactionCd = '253' then '67'
					when TransactionCd = '255' then '37'
					when TransactionCd = '256' then '37'
				  end
where system in('3528', '4882', '8969')

update dbo.impTransaction
	set RNTranCode = case
					when TransactionCd = '253' then '63'
					when TransactionCd = '255' then '33'
					when TransactionCd = '256' then '33'
				  end
where system not in('3528', '4882', '8969')

update dbo.impTransaction
	set RNTranCode = case
					when TransactionCd in ('PCV', 'PCP') then '67'
					when TransactioNCd in ('RET') then '37'
				  end
where transactioncd in('PCV', 'PCP', 'RET')

-- Update trancodes from Complex
-- Updated stmt to use trancodes coming from complex - they are using ours
update dbo.imptransaction
	set RNTranCode = case
					   when TransactionCd = '063' then '67'
					   when transactioncd = '033' then '37'
					   else '67'
				  end 
where prin = 'CPLX'

--
-- Remove tranasctions that have null trancodes
-- Right now, this is just TNB TransactionCode 280 which is adjustments
delete from dbo.impTransaction
where RNTranCode is null


------------------------------------------------------------
-- Update Transaction Amounts
------------------------------------------------------------

-- Convert to dollars 
update dbo.impTransaction
	set TransactionAmt = abs( cast(TransactionAmt as money) / cast(100 as money))
where	prin != 'CPLX'	-- Dont do for Complex.  these already come in as dollars and cents explicitly

-- Halve transaction amounts 
update dbo.impTransaction
	set TransactionAmt = TransactionAmt / 2
where system in('3528', '4882')


--
-- Some accounts (TBA, LADOTD for example) halve their debit card
-- Transactions.  Use this lookup table to get the override multiplier
-- and update the transaction amounts.  NOTE: this is assoc#/TranCode
-- specific.
--
update txn
	set TransactionAmt = TransactionAmt * TransactionMultiplier
from dbo.imptransaction txn join dbo.AssocTranCodeMultiplierOverride ovr
	on txn.assocnbr = ovr.assocnbr
	and txn.RNTranCode = ovr.trancode
GO
