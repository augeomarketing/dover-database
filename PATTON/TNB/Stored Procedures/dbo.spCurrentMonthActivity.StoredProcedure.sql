USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivity]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spCurrentMonthActivity]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
-- PHB 7/2/2008 modified for LOC FCU
-- PHB 8/4/2008 chged for TNB
*/


CREATE PROCEDURE [dbo].[spCurrentMonthActivity] @EndDate datetime
AS

declare @SQL			nvarchar(4000)
declare @db			nvarchar(50)
declare @tipfirst		nvarchar(3)

truncate table Current_Month_Activity

declare csrdb cursor FAST_FORWARD for
	select quotename(dbnamepatton), dbnumber
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst	
	where tte.tipfirst is null
	and dbnamenexl = 'newtnb'

open csrdb

fetch next from csrdb into @db, @tipfirst
while @@FETCH_STATUS = 0
BEGIN
	set @SQL = 'insert into Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
				select tipnumber, RunAvailable,0 ,0 ,0 from ' + @db + '.dbo.Customer'
	exec sp_executesql @SQL

--

/* Load the current activity table with increases for the current month         */
	set @SQL = 'Update newtnb.dbo.Current_Month_Activity
					set increases=(select sum(points) from ' + @db + '.dbo.history h where histdate>@enddate and ratio=''1''
								and H.Tipnumber = Current_Month_Activity.Tipnumber )
			  where exists(select * from ' + @db + '.dbo.history h2 where histdate>@enddate and ratio=''1''
							and H2.Tipnumber = Current_Month_Activity.Tipnumber )'
	exec sp_executesql @SQL, N'@enddate datetime', @enddate = @enddate


/* Load the current activity table with decreases for the current month         */
	set @SQL = 'update newtnb.dbo.Current_Month_Activity
				set decreases=(select sum(points) from ' + @db + '.dbo.history h where histdate>@enddate and ratio= ''-1''
								and H.Tipnumber = Current_Month_Activity.Tipnumber )
			where exists(select * from ' + @db + '.dbo.history h2 where histdate>@enddate and ratio= ''-1''
						and H2.Tipnumber = Current_Month_Activity.Tipnumber )'

	exec sp_executesql @SQL, N'@enddate datetime', @enddate = @enddate


	fetch next from csrdb into @db, @tipfirst
END

close csrdb

deallocate csrdb

/* Load the calculate the adjusted ending balance        */
update newtnb.dbo.Current_Month_Activity
	set adjustedendingpoints=endingpoints - increases + decreases
GO
