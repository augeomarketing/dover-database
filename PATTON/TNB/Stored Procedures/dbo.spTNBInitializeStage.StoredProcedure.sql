USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spTNBInitializeStage]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spTNBInitializeStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spTNBInitializeStage]
		@MonthStart datetime
AS 


Declare @dbName varchar(50)
Declare @SQLCmnd nvarchar(4000)

declare @strMonthStart varchar(10)

set @strMonthStart = right( '00' + cast(month(@monthStart) as varchar(2)), 2) + '/' + 
			    right( '00' + cast(day(@MonthStart) as varchar(2)), 2) + '/' + 
			    cast( year(@MonthStart) as varchar(4))


truncate table dbo.Customer_Stage
truncate table dbo.Affiliat_Stage
truncate table dbo.History_Stage

truncate table dbo.customer_stage_deleted
truncate table dbo.affiliat_stage_deleted
truncate table dbo.history_stage_deleted

truncate table dbo.OneTimeBonuses_Stage

--
-- Set all TNB databases to DBAvailable = "N"
--
update dbpi
	set DBAvailable = 'N'
from rewardsnow.dbo.DBProcessInfo dbpi left outer join dbo.TipsToExclude tte
		on dbpi.dbnumber = tte.tipfirst
where dbnamenexl = 'NewTNB' and tte.tipfirst is null and isnull(sid_fiprodstatus_statuscode, '')  not in ('X', 'I')


declare csrDBName cursor FAST_FORWARD for
	select dbnamepatton from rewardsnow.dbo.DBProcessInfo dbpi left outer join dbo.TipsToExclude tte
		on dbpi.dbnumber = tte.tipfirst
	where dbnamenexl = 'NewTNB' and tte.tipfirst is null  and isnull(sid_fiprodstatus_statuscode, '')  not in ('X', 'I')
	order by dbnamepatton
	

open csrDBName

fetch next from csrDBName into @DbName

while @@FETCH_STATUS = 0
BEGIN
print 'Begin Processing database: ' + @DbName
	-- Load the stage tables
	set @SQLCmnd = 'Insert into dbo.Customer_Stage Select * from [' +  @dbName + '].dbo.Customer'
	Exec sp_executeSql @SQLCmnd 

	set @SQLCmnd = 'Update dbo.Customer_Stage Set  RunAvaliableNew = 0 '
	Exec sp_executeSql @SQLCmnd 

	set @SQLCmnd = 'Insert into dbo.Affiliat_Stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag)
				 Select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, AffiliatFlag from [' + @dbName + '].dbo.Affiliat 
				 where acctid not in (''9999999999999998'', ''9999999999999999'')'
	Exec sp_executeSql @SQLCmnd 

	set @SQLCmnd = 'Insert into dbo.History_stage (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
				 Select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage from [' + @dbName  + '].dbo.History where Histdate >= ' + char(39) + @strMonthStart + char(39)
	Exec sp_executeSql @SQLCmnd 

	set @SQLCmnd = 'Update dbo.History_stage set SecId = ' + char(39) +  'OLD' + char(39) 
	Exec sp_executeSql @SQLCmnd 

--	set @SQLCmnd = 'Insert into dbo.OnetimeBonuses_Stage Select * from [' + @dbName + '].dbo.OnetimeBonuses'
--	Exec sp_executeSql @SQLCmnd

print 'Complete Processing database: ' + @DbName
print ' '
print ' '

	fetch next from csrDBName into @DbName
END

close csrDBName

deallocate csrDBName


-- Set statuses to active for statuses that are not A or C
update dbo.affiliat_stage
	set acctstatus = 'A'
where acctstatus not in ('C', 'A')
GO
