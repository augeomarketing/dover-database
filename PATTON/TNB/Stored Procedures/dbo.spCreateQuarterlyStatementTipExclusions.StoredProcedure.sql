USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spCreateQuarterlyStatementTipExclusions]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spCreateQuarterlyStatementTipExclusions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCreateQuarterlyStatementTipExclusions]

As


create table #ExcludeZeroBalances
	(TipFirst			varchar(3) primary key)
	
create table #ExcludeNoActivity
	(TipFirst			varchar(3) primary key)


truncate table dbo.quarterly_statement_exclusions

insert into #ExcludeZeroBalances
select distinct tipfirst
from dbo.statementrun
where excludezerobalances = 'Y'

--SEB 5/2014 sproc to use new expiring points

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select distinct qsfa.tipnumber
from dbo.Quarterly_Statement_File_Audit qsfa join #excludezerobalances tmp
	on left(qsfa.tipnumber,3) = tmp.tipfirst
left outer join rewardsnow.dbo.RNIExpirationProjection xp
    on qsfa.tipnumber = xp.tipnumber
where pointsbegin = 0 and pointsend = 0 and isnull(xp.points_expiring , 0) = 0



insert into #ExcludeNoActivity
select distinct tipfirst
from dbo.statementrun
where excludenoactivity = 'Y'

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select distinct qsfa.tipnumber
from dbo.Quarterly_Statement_File_Audit qsfa join #ExcludeNoActivity tmp
	on left(qsfa.tipnumber,3) = tmp.tipfirst

left outer join dbo.quarterly_statement_exclusions qse
	on qsfa.tipnumber = qse.tipnumber

left outer join rewardsnow.dbo.RNIExpirationProjection xp
    on qsfa.tipnumber = xp.tipnumber

where qse.tipnumber is null
and qsfa.PointsIncreased = 0 and qsfa.PointsDecreased = 0 and qsfa.pointsexpire = 0 and isnull(xp.points_expiring, 0) = 0

---
-- Add in exclusions for E-Statements
---

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select estmt.tipnumber
from newtnb.dbo.EStatementTips estmt join newtnb.dbo.assoc ass
    on left(estmt.tipnumber,3) = ass.tipfirst
    and 'Y' = ass.ExcludeEStatement
left outer join newtnb.dbo.quarterly_statement_exclusions qse
    on estmt.tipnumber = qse.tipnumber
where qse.tipnumber is null
GO
