USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateDBProcessInfo]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spUpdateDBProcessInfo]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spUpdateDBProcessInfo]
	(@Flag		varchar(1))
AS

update db
	set dbavailable = @Flag
from rewardsnow.dbo.dbprocessinfo db left outer join dbo.TipsToExclude tte
	on db.dbnumber = tte.tipfirst and sid_FiProdStatus_statuscode not in ('I', 'X')
where dbnamenexl = 'NewTNB'  and	 tte.tipfirst is null
GO
