USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spCreateWebTables]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spCreateWebTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCreateWebTables]

As

	declare @db		nvarchar(100)
	declare @sql		nvarchar(4000)

	if exists(select 1 from dbo.sysobjects where name = 'web_account' and xtype = 'U')
		drop table dbo.web_account

	if exists(select 1 from dbo.sysobjects where name = 'web_Customer' and xtype = 'U')
		drop table dbo.web_Customer

	CREATE TABLE newtnb.dbo.web_Account(
		[TipNumber] [char](20) NOT NULL,
		[LastName] [varchar](50) NOT NULL,
		[LastSix] [char](6) NOT NULL,
		[SSNLast4] [char](4) NULL,
		[RecNum] [int] IDENTITY(1,1) NOT NULL,
	 CONSTRAINT [PK_web_Account] PRIMARY KEY CLUSTERED 
	(
		[TipNumber] ASC,
		[LastName] ASC,
		[LastSix] ASC
	)WITH FILLFACTOR = 90 ON [PRIMARY]
	) ON [PRIMARY]



	CREATE TABLE newtnb.dbo.web_Customer(
		[TipNumber] [char](20) NOT NULL primary key,
		[TipFirst] [char](3) NOT NULL,
		[TipLast] [char](17) NOT NULL,
		[Name1] [varchar](50) NOT NULL,
		[Name2] [varchar](50) NULL,
		[Name3] [varchar](50) NULL,
		[Name4] [varchar](50) NULL,
		[Name5] [varchar](50) NULL,
		[Address1] [varchar](50) NOT NULL,
		[Address2] [varchar](50) NULL,
		[Address3] [varchar](50) NULL,
		[CityStateZip] [varchar](50) NULL,
		[ZipCode] [char](10) NULL,
		[EarnedBalance] [int] NULL,
		[Redeemed] [int] NULL,
		[AvailableBal] [int] NULL,
		[Status] [char](2) NULL,
		[Segment] [char](2) NULL,
		[city] [char](50) NULL,
		[state] [char](5) NULL)



	declare csrDB cursor FAST_FORWARD for
		select quotename(dbnamepatton) dbnamepatton
		from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
			on dbpi.dbnumber = tte.tipfirst
		where tte.tipfirst is null 
		and dbnamenexl = 'newtnb' and isnull(sid_fiprodstatus_statuscode, '')  not in ('X', 'I')
		

	open csrDB

	fetch next from csrDB into @DB
	while @@FETCH_STATUS = 0
	BEGIN
	
print 'START: ' + @db

		set @sql = '
		insert into newtnb.dbo.web_customer
			(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
			 EarnedBalance, Redeemed, AvailableBal, Status, city, state)
		select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
				address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
		from ' + @db + '.dbo.customer	
		where status in ( ''A'', ''S'')'
		exec sp_executesql @SQL

		
		set @sql = '
		insert into newtnb.dbo.web_account (TipNumber, LastName, LastSix, SSNLast4)
		select aff.tipnumber, isnull(aff.lastname, ''NotAssigned'') , right(acctid,6), null
		from ' + @db + '.dbo.affiliat aff join ' + @db + '.dbo.customer cus
			on aff.tipnumber = cus.tipnumber
		where cus.status in( ''A'', ''S'') '
		exec sp_executesql @sql
print 'END: ' + @db

		fetch next from csrDB into @DB
	END

	close csrDB
	deallocate csrDB
GO
