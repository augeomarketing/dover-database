USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePoints_Standard]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spExpirePoints_Standard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spExpirePoints_Standard]
	(@TipFirst		varchar(3),
	 @MonthEndDate		datetime)

AS

--
-- Expire points for Standard Point expiration processing
--
GO
