USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spTruncateImportTables]    Script Date: 02/09/2010 08:17:05 ******/
if object_id('spTruncateImporttables') is not null
    DROP PROCEDURE [dbo].[spTruncateImportTables]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[spTruncateImportTables]

as

truncate table dbo.impBonus

truncate table dbo.impCustomer
truncate table dbo.impCustomer_Agent
truncate table dbo.impCustomer_Cust
truncate table dbo.impCustomer_DebitCust
truncate table dbo.impCustomer_DowCust
truncate table dbo.impCustomer_Error
truncate table dbo.impCustomer_Purge
truncate table dbo.impCustomer_Purge_Pending
truncate table dbo.impCustomer_R2UCust


truncate table dbo.impTransaction
truncate table dbo.impTransaction_Agent
truncate table dbo.impTransaction_DebitTran
truncate table dbo.impTransaction_DowTran
truncate table dbo.impTransaction_Error
truncate table dbo.impTransaction_R2UTran
truncate table dbo.impTransaction_Tran

truncate table dbo.impWelcome_R2UWelcome

truncate table dbo.imp_TNBSummary

truncate table dbo.nameprs

truncate table dbo.impDeletedCards
truncate table dbo.impDeletedCards_Complex



GO
