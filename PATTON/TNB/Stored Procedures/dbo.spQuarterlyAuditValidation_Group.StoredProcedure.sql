use newtnb
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spQuarterlyAuditValidation_Group' and xtype = 'P')
	drop procedure dbo.spQuarterlyAuditValidation_Group
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
CREATE PROCEDURE [dbo].spQuarterlyAuditValidation_Group 
	@TipFirst			varchar(3)

AS

delete from dbo.Quarterly_Audit_ErrorFile

insert into dbo.Quarterly_Audit_ErrorFile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
 PointsReturned, PointsSubtracted, PointsDecreased, Errormsg, Currentend)
select qsf.Tipnumber, PointsBegin, PointsEnd, PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
 PointsReturned, PointsSubtracted, PointsDecreased, 'Ending Balances do not match', AdjustedEndingPoints
from dbo.quarterly_statement_file qsf join dbo.current_month_activity cma
	on qsf.tipnumber = cma.tipnumber
where pointsend != cma.adjustedendingpoints
