USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePointsExtractPHB]    Script Date: 02/09/2010 08:17:04 ******/
DROP PROCEDURE [dbo].[spExpirePointsExtractPHB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Create the Expering Points Table                              */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spExpirePointsExtractPHB] 
		--@MonthBeg VARCHAR(10), @NumberOfYears int  
		@DBName		nvarchar(50),
		@Trandate		datetime

AS   

declare @SQL			nvarchar(4000)

declare @MonthBeg VARCHAR(10)

SET @MonthBeg = '12/01/2008'

declare @NumberOfYears int

set @NumberOfYears = '3' 

--declare @trandate datetime
--
--set @trandate = (select cast(min(histdate) as datetime) from history where trancode like 't%')

declare @yeardiff int

set @yeardiff = year(@monthbeg) - year(@trandate)

if month(@trandate) < 7
      set @yeardiff = @yeardiff + 1

declare @ratio float

set @ratio = cast(@yeardiff as float)/ 4
print @yeardiff
print 'Ratio:'
print @ratio

Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
Declare @PointsToExpireNext int
Declare @AddPointsNext int
Declare @PointsprevExpired int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @expirationdatenext nvarchar(25)
declare @intday int
declare @intmonth int
declare @intmonthnext int
declare @intyear int
declare @ExpireDate DATETIME
declare @MonthEndDate DATETIME
declare @MonthEndDatenext DATETIME


set @MonthEndDate = cast(@MonthBeg as datetime)
set @MonthEndDate = Dateadd(month, 1, @MonthBeg)
set @MonthEndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @MonthEndDate)),121)
set @MonthEndDatenext = cast(@MonthBeg as datetime)
set @MonthEndDatenext = Dateadd(month, 2, @MonthBeg)

print 'month end date'
print @MonthEndDate
print 'month end date next'
print @MonthEndDatenext

set @expirationdate = cast(@MonthEndDate as datetime)
set @expirationdatenext = cast(@MonthEndDatenext as datetime)
set @expirationdate = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdate)),121)

print 'Expiration Date: ' + @expirationdate
set @expirationdatenext = convert(nvarchar(25),(Dateadd(year, -@NumberOfYears, @expirationdatenext)),121)
set @expirationdatenext = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdatenext)),121)

 
	set @SQL = 'TRUNCATE TABLE ' + @dbname + '.dbo.expiringpoints'
	print @SQL
	exec sp_executesql @SQL

	set @SQL = 'INSERT   into ' + @dbname + '.dbo.expiringpoints
       SELECT tipnumber, sum(points * ratio) as addpoints,
      ''0'' AS REDPOINTS, ''0'' AS POINTSTOEXPIRE, ''0'' as prevExpired, DateOfExpire = @expirationdate,
      ''0'' as PointstoExpireNext,''0'' as addpointsnext, ''0'' as EXPTODATE, ''0'' as ExpiredRefunded
      from ' + @dbname + '.dbo.history
      where 
      (trancode like(''t%'') and
           trancode <> ''IR'')
      group by tipnumber'
	print @SQL
	exec sp_executesql @SQL, N'@expirationdate datetime', @ExpirationDate = @ExpirationDate


	set @SQL = '
      UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
             FROM ' + @dbname + '.dbo.HISTORY 
             WHERE
             (trancode  like(''R%'') or
              trancode = ''IR'')  
             AND TIPNUMBER = ' + @dbname + '.dbo.expiringpoints.TIPNUMBER
             and histdate < ''10/1/2008 00:00:00'') 
      WHERE EXISTS  (SELECT *
             FROM ' + @dbname + '.dbo.HISTORY 
             WHERE
             (trancode  like(''R%'') or
              trancode = ''IR'')  
             AND TIPNUMBER = ' + @dbname + '.dbo.expiringpoints.TIPNUMBER
             and histdate < ''10/1/2008 00:00:00'')' 
	print @SQL
	exec sp_executesql @sql


	set @sql = '
      UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET prevExpired = ISNULL((SELECT SUM(POINTS* RATIO) 
             FROM ' + @dbname + '.dbo.HISTORY 
             WHERE
              trancode = ''XP''   
             AND TIPNUMBER = ' + @dbname + '.dbo.expiringpoints.TIPNUMBER),0)
      WHERE EXISTS  (SELECT *
             FROM ' + @dbname + '.dbo.HISTORY 
             WHERE
              trancode = ''XP''  
             AND TIPNUMBER = ' + @dbname + '.dbo.expiringpoints.TIPNUMBER)'
	print @SQL
	exec sp_executesql @sql


--	set @SQL = '
--      UPDATE ' + @dbname + '.dbo.expiringpoints  
--      SET addpointsnext = (SELECT SUM(POINTS* RATIO * .25) 
--             FROM ' + @dbname + '.dbo.HISTORY 
--             WHERE
--             histdate < @expirationdatenext and trancode like (''t%'')
--             AND TIPNUMBER = ' + @dbname + '.dbo.expiringpoints.TIPNUMBER)'
--	print @SQL
--	exec sp_executesql @sql, N'@ExpirationDateNext datetime', @ExpirationDateNext = @ExpirationDateNext


	set @sql = ' 
      UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET ExpiredRefunded = (SELECT SUM(POINTS* RATIO) 
             FROM ' + @dbname + '.dbo.HISTORY 
             WHERE
             trancode = ''XF''
             AND TIPNUMBER = ' + @dbname + '.dbo.expiringpoints.TIPNUMBER)'
	print @SQL
	exec sp_executesql @sql


	set @sql = 
      'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET PREVEXPIRED = (PREVEXPIRED * -1)'
	print @SQL
	exec sp_executesql @sql


	set @sql = 
     'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET POINTSTOEXPIRE = cast((@ratio*ADDPOINTS + REDPOINTS - PREVEXPIRED) as int)'
	print @SQL
	exec sp_executesql @sql, N'@Ratio float', @ratio = @Ratio


	set @sql = 
      'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET EXPTODATE = (POINTSTOEXPIRE + PREVEXPIRED)'
	print @SQL
	exec sp_executesql @sql


	set @sql = 
     'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET POINTSTOEXPIRE = ''0''
      WHERE
      POINTSTOEXPIRE IS NULL 
      or 
      POINTSTOEXPIRE < ''0'' '
	print @SQL
	exec sp_executesql @sql


	set @sql = 
     'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET POINTSTOEXPIREnext = (ADDPOINTSnext + REDPOINTS  - (PREVEXPIRED + POINTSTOEXPIRE))'
	print @SQL
	exec sp_executesql @sql


	set @sql =
      'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET POINTSTOEXPIREnext = ''0''
      WHERE
      POINTSTOEXPIREnext IS NULL 
      or 
      POINTSTOEXPIREnext < ''0'' '
	print @SQL
	exec sp_executesql @sql


	set @sql = 
     'UPDATE ' + @dbname + '.dbo.expiringpoints  
      SET EXPTODATE = ''0''
      WHERE
      EXPTODATE IS NULL 
      or 
      EXPTODATE < ''0'' '
	print @SQL
	exec sp_executesql @sql
GO
