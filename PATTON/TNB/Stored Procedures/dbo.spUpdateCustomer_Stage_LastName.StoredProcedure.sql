USE [NewTNB]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCustomer_Stage_LastName]    Script Date: 02/09/2010 08:17:05 ******/
DROP PROCEDURE [dbo].[spUpdateCustomer_Stage_LastName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spUpdateCustomer_Stage_LastName]

as


update stg
	set LASTNAME = pers.LASTNAME
from dbo.customer_stage stg join dbo.nameprs pers
	on stg.tipnumber = pers.tipnumber
GO
