use newtnb
GO

if exists(select 1 from sys.objects where name = 'usp_QueueLiabilityReports' and type = 'P')
    drop procedure dbo.usp_QueueLiabilityReports
GO

create procedure dbo.usp_QUeueLiabilityReports
	   @StartDate			  datetime,
	   @EndDate			  datetime

AS

declare @strStartDate	   varchar(10) = ''
declare @strEndDate		   varchar(10) = ''

--
-- Convert dates to strings, as the reportqueue is looking for the date values to be varchar
set @strStartDate = cast(year(@startdate) as varchar(4)) + '/' +
			     right('00' + cast(month(@startdate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@startdate) as varchar(2)), 2)

set @strEndDate = cast(year(@EndDate) as varchar(4)) + '/' +
			     right('00' + cast(month(@EndDate) as varchar(2)), 2) + '/' +
				right('00' + cast(day(@EndDate) as varchar(2)), 2)

Insert into maintenance.dbo.reportqueue
    (dim_reportqueue_tipfirst, dim_reportqueue_startdate, dim_reportqueue_enddate, 
	dim_reportqueue_email, dim_reportqueue_created, dim_reportqueue_active)
select distinct tipfirst, @strstartdate, @strenddate, 'pbutler@rewardsnow.com', getdate(), 1
from newtnb.dbo.assoc
