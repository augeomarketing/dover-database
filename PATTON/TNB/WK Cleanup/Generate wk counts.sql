--select * from rewardsnow.dbo.dbprocessinfo

declare @sql		nvarchar(4000)
declare @tipfirst	nvarchar(3)
declare @db		nvarchar(50)
declare @wkgroupnm	nvarchar(50)
declare @Count		int

declare csrDB cursor FAST_FORWARD for
	select dbnumber, quotename(dbnamepatton) dbname, welcomekitgroupname
	from rewardsnow.dbo.dbprocessinfo dbpi left outer join newtnb.dbo.tipstoexclude tte
		on dbpi.dbnumber = tte.tipfirst
	where tte.tipfirst is null and dbnamenexl = 'newtnb'
	and generatewelcomekit = 'Y'
	order by dbnumber

open csrDB

fetch next from csrDB into @tipfirst, @db, @wkgroupnm
while @@FETCH_STATUS = 0
BEGIN

	set @sql = '
		set @count = (select count(*) from ' + @db + '.dbo.customer where dateadded between ''11/29/2008'' and  ''12/01/2008'' )'

	--print @sql

	exec sp_executesql @sql, N'@count int output', @count = @count output

	print @tipfirst + '	' + @db + '	' + cast(@count as nvarchar(10)) + '	' + isnull(@wkgroupnm, '')

	fetch next from csrdb into @tipfirst, @db, @wkgroupnm
END

close csrdb
deallocate csrdb