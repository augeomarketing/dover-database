select act.acctid ccacctnbr, Aff_TipNumber, His_TipNumber, his.*
into #CorrectingTxns
from dbo.AcctIDs_with_Conflicting_Tips act join newtnb.dbo.history his
	on act.his_tipnumber = his.tipnumber
	and act.acctid = his.acctid
where histdate > '10/30/2008'


insert into dbo.HistoryTxns2Add_DE
(tipnumber, AcctId, histdate, Trancode, trancount, Points, Description, SECID, ratio, Overage)
select his_tipnumber, ccacctnbr, '01/01/2009' as histdate, 'DE' as TranCode, 1 as trancount, sum(points * ratio) as Points ,
		'Corr Invalid Tip Assignment' as Description, cast('' as varchar(50)) as SECID, -1 as ratio, 0 as Overage 
from #correctingtxns
group by his_tipnumber, ccacctnbr
order by his_tipnumber




insert into dbo.HistoryTxns2Add_IE
(tipnumber, AcctId, histdate, Trancode, trancount, Points, Description, SECID, ratio, Overage)
select aff_tipnumber, ccacctnbr, '01/01/2009' as histdate, 'IE' as Trancode, 1 as trancount, sum(points * ratio) as Points,
		'Corr Invalid Tip Assignment' as Description, cast('' as varchar(50)) as SECID, 1 as ratio, 0 as Overage 
from #correctingtxns
group by aff_tipnumber, ccacctnbr
order by aff_tipnumber


select *
into CorrectingTxns20090223
from #correctingtxns






--
-- Backout points from incorrect TIP
--
select wtf.tipnumber, cast('' as varchar(25)) as AcctId, '01/01/2009' as histdate, 
		case
			when sum(points * ratio) >= 0 then 'DE'
			else 'IE'
		end as Trancode, 1 as trancount, abs(sum(points * ratio)) as Points, 'Corr Invalid Tip Assignment' as Description,
		cast('' as varchar(50)) as SECID, 
		case
			when sum(points * ratio) >= 0 then -1
			else 1
		end as ratio, 0 as Overage
--into newtnbwtf.dbo.HistoryTxns2Add_DE
from newtnbwtf.dbo.HistoryCorrectionsNeeded wtf
group by wtf.tipnumber
order by wtf.tipnumber

