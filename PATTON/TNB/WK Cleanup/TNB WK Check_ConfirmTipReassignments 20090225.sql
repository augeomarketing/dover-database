declare @tipfirstie		nvarchar(3)
declare @tipfirstde		nvarchar(3)
declare @sql			nvarchar(4000)
declare @dbie			nvarchar(50)
declare @dbde			nvarchar(50)

set @tipfirstie = '136'
set @tipfirstde = '161'

set @dbie = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirstie)
set @dbde = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirstde)

set @sql = '
select ie.tipnumber, c1.acctname1, c1.acctname2, c1.dateadded, de.tipnumber, c2.acctname1, c2.acctname2, c2.dateadded, ie.*, de.*
from newtnbwtf.dbo.HistoryTxns2Add_IE ie join newtnbwtf.dbo.HistoryTxns2Add_DE de
	on ie.acctid = de.acctid

join ' + @dbie + '.dbo.customer c1
	on ie.tipnumber = c1.tipnumber

join ' + @dbde + '.dbo.customer c2
	on de.tipnumber = c2.tipnumber

where left(ie.tipnumber,3) = ' + char(39) + @tipfirstie + char(39) + 'and left(de.tipnumber,3) = ' + char(39) + @tipfirstde + char(39)

exec sp_executesql @sql
--
--select sum(points)
--from newtnbwtf.dbo.HistoryTxns2Add_IE
--
--
--select sum(points)
--from newtnbwtf.dbo.HistoryTxns2Add_de