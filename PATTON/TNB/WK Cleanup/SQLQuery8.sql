ok - need to update the statement table



select *
from dbo.Monthly_Statement_File

select count(distinct tipnumber) from newtnbwtf.dbo.HistoryTxns2Add_DE 
select sum(points)
from newtnbwtf.dbo.HistoryTxns2Add_DE de join newtnb.dbo.monthly_statement_file msf
	on de.tipnumber = msf.tipnumber


select count(distinct tipnumber) from newtnbwtf.dbo.HistoryTxns2Add_iE 
select sum(points)
from newtnbwtf.dbo.HistoryTxns2Add_IE ie join newtnb.dbo.monthly_statement_file msf
	on ie.tipnumber = msf.tipnumber


-- variance 58,602

select *
into bkup_monthly_statement_file_20090226
from monthly_statement_file    --  (110662 row(s) affected)

delete from dbo.monthly_statement_file
insert into dbo.monthly_statement_file
select *
from bkup_monthly_statement_file_20090226

select count(*) from monthly_statement_file



begin tran

--drop table #iepoints
--drop table #depoints

select tipnumber, sum(points) points
into #iepoints
from newtnbwtf.dbo.historytxns2add_ie
group by tipnumber

select tipnumber, sum(points) points
into #depoints
from newtnbwtf.dbo.historytxns2add_de
group by tipnumber


update msf
	set pointsend = pointsend + ie.points,
		pointsadded = pointsadded + ie.points,
		pointsincreased = pointsincreased + ie.points
from #iepoints ie join newtnb.dbo.monthly_statement_file msf
	on ie.tipnumber = msf.tipnumber


update msf
	set pointsend = pointsend - de.points,
		pointssubtracted = pointssubtracted + de.points,
		pointsdecreased = pointsdecreased + de.points
from #depoints de join newtnb.dbo.monthly_statement_file msf
	on de.tipnumber = msf.tipnumber

rollback tran   --       commit tran

select * from newtnbwtf.dbo.historytxns2add_de

select ie.*
from newtnbwtf.dbo.historytxns2add_ie ie left outer join newtnb.dbo.monthly_statement_file msf
	on ie.tipnumber = msf.tipnumber
where msf.tipnumber is null



select de.tipnumber --sum(points)
into #missingtips
from newtnbwtf.dbo.historytxns2add_de de left outer join newtnb.dbo.monthly_statement_file msf
	on de.tipnumber = msf.tipnumber
where msf.tipnumber is null
order by de.tipnumber



select *
from [135texasbayarea].dbo.customer cus join #missingtips mt
	on cus.tipnumber = mt.tipnumber
where left(mt.tipnumber,3) = '135'