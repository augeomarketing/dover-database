select cus.tipnumber cust_tipnbr, imp.tipprefix zz_tipprefix, imp.tipnumber zz_tipnumber, 
		imp.ccacctnbr zz_ccacctnbr,  imp2.tipprefix new_tipprefix, imp2.tipnumber new_tipnumber
from zznewtnb.dbo.customer cus join zznewtnb.dbo.impcustomer imp
	on cus.tipnumber = imp.tipnumber	
	and cus.dateadded > '2008/11/29'

join newtnb.dbo.impcustomer imp2
	on imp2.ccacctnbr = imp.ccacctnbr

where left(cus.tipnumber,3) != imp2.tipprefix