truncate table rewardsnow.dbo.expiringpoints

drop table #TfrPointExpireTips 
create table #TfrPointExpireTips (TipFirst varchar(3) primary key)
insert into #TfrPointExpireTips
select '121'
union
select '124'
union
select '127'
union
select '128'
union
select '140'
union
select '141'
union
select '143'
union
select '147'
union
select '148'
union
select '149'
union
select '152'
union
select '153'
union
select '154'
union
select '155'
union
select '157'
union
select '158'
union
select '159'
union
select '182'
union
select '183'
union
select '184'
union
select '185'

declare @sql nvarchar(4000)
declare @db nvarchar(50)

declare csrTIP cursor fast_forward for
	select quotename(dbnamepatton)
	from #TfrPointExpireTips tmp join rewardsnow.dbo.dbprocessinfo dbpi
		on tmp.tipfirst = dbpi.dbnumber

open csrTIP

fetch next from csrTIP into @db

while @@FETCH_STATUS = 0
BEGIN

	set @sql = '
		insert into rewardsnow.dbo.expiringpoints
		(tipnumber, AddPoints, REDPOINTS, POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, PointsToExpireNext, EXPTODATE, ExpiredRefunded, dbnameonnexl)
		select tipnumber, addpoints, REDPOINTS, POINTSTOEXPIRE, PREVEXPIRED, DateofExpire, PointsToExpireNext, 
		EXPTODATE, ExpiredRefunded, ''NewTNB''
		from ' + @db + '.dbo.expiringpoints'

	print @sql

	exec sp_executesql @sql



	fetch next from csrTIP into @db
END

close csrTIP
deallocate csrTIP


select *
from rewardsnow.dbo.expiringpoints


