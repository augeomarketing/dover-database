-- run from the 1st run of TNB for Jan.  where we know the tips are fubar
select *
into newtnbwtf.dbo.impcustomer_WrongTipAssignments
from impcustomer
where tipprefix != left(tipnumber,3)
order by tipprefix

--------------------------------------------------------------

create table newtnbwtf.dbo.CardTipPrefix
	(ccacctnbr			varchar(25) not null,
	 oldccacctnbr			varchar(25) not null,
	 tipprefix			varchar(3)  not null)

insert into  newtnbwtf.dbo.CardTipPrefix
select ccacctnbr, oldccacctnbr, tipprefix
from newtnb.dbo.impcustomer



