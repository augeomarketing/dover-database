USE [NewTNB]
GO
/****** Object:  UserDefinedFunction [dbo].[fnConvertTransactionAmount]    Script Date: 02/09/2010 08:18:42 ******/
DROP FUNCTION [dbo].[fnConvertTransactionAmount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fnConvertTransactionAmount]
		(@txnAmount varchar(15))
returns money

as

BEGIN

declare @txnAmountMoney		money

declare @dollars			money
declare @cents				money

set @dollars = cast(left(@txnAmount, len(@txnAmount) - 3) as money)

if ltrim(rtrim(@txnAmount)) like '%-%'  -- Amount is negative
begin
	set @cents = cast( substring(ltrim(rtrim(@txnAmount)), len(ltrim(rtrim(@txnAmount))) - 2, 2) as money) * -1
	set @dollars = @dollars * -1
end
else
begin
	set @cents = cast( substring(ltrim(rtrim(@txnAmount)), len(ltrim(rtrim(@txnAmount))) - 1, 2) as money)
end
set @txnAmountMoney = @dollars + (@cents / 100)


return @txnAmountMoney
END
GO
