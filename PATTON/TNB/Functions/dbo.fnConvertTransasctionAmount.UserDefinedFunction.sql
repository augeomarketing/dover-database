use newtnb
GO


if exists(select 1 from dbo.sysobjects where [name] = 'fnConvertTransactionAmount' and xtype = 'fn')
	drop function dbo.fnConvertTransactionAmount
GO


create function dbo.fnConvertTransactionAmount
		(@txnAmount varchar(15))
returns money

as

BEGIN

declare @txnAmountMoney		money

declare @dollars			money
declare @cents				money

set @dollars = cast(left(@txnAmount, len(@txnAmount) - 3) as money)

if ltrim(rtrim(@txnAmount)) like '%-%'  -- Amount is negative
begin
	set @cents = cast( substring(ltrim(rtrim(@txnAmount)), len(ltrim(rtrim(@txnAmount))) - 2, 2) as money) * -1
	set @dollars = @dollars * -1
end
else
begin
	set @cents = cast( substring(ltrim(rtrim(@txnAmount)), len(ltrim(rtrim(@txnAmount))) - 1, 2) as money)
end
set @txnAmountMoney = @dollars + (@cents / 100)


return @txnAmountMoney
END