USE [NewTNB]
GO
/****** Object:  UserDefinedFunction [dbo].[fnPivotAssocNbrstoString]    Script Date: 02/09/2010 08:18:42 ******/
DROP FUNCTION [dbo].[fnPivotAssocNbrstoString]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fnPivotAssocNbrstoString] (@TipFirst varchar(3))
	returns varchar(100)
as

BEGIN
	declare @AssocString		varchar(100)

	declare @ctr				int
	declare @NbrAssocsPerTip		int
	declare @HldAssoc			varchar(3)
	declare @NewAssoc			varchar(3)


	set @NbrAssocsPerTip = (select count(*) from dbo.assoc where tipfirst = @TipFirst)
	set @ctr = 1
	set @hldAssoc = ''

	while @ctr <= @NbrAssocsPerTip
	BEGIN

		set @NewAssoc = (select top 1 assocnum from dbo.assoc where TipFirst = @TipFirst and assocnum > @hldAssoc order by assocnum)

		if @ctr = 1
			set @Assocstring = @NewAssoc
		else
			set @AssocString = @AssocString + ', ' + @NewAssoc

		set @HldAssoc = @NewAssoc

		set @ctr = @ctr + 1
	END

	return @AssocString
END
GO
