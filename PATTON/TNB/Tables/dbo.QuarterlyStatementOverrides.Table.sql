USE [NewTNB]
GO
/****** Object:  Table [dbo].[QuarterlyStatementOverrides]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[QuarterlyStatementOverrides] DROP CONSTRAINT [DF_QuarterlyStatementOverrides_CreateDate]
GO
ALTER TABLE [dbo].[QuarterlyStatementOverrides] DROP CONSTRAINT [DF_QuarterlyStatementOverrides_LastModifiedDate]
GO
DROP TABLE [dbo].[QuarterlyStatementOverrides]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuarterlyStatementOverrides](
	[TipFirst] [varchar](3) NOT NULL,
	[StatementTemplatePath] [varchar](255) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_QuarterlyStatementOverrides] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[QuarterlyStatementOverrides] ADD  CONSTRAINT [DF_QuarterlyStatementOverrides_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[QuarterlyStatementOverrides] ADD  CONSTRAINT [DF_QuarterlyStatementOverrides_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO
