USE [NewTNB]
GO
/****** Object:  Table [dbo].[imp_TNBSummary]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_NbrNonStatAcctsCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_NbrStatusAcctsCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_NbrTotalTxnsCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_TotalTxnAmountCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_NbrNonStatAcctsDebit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_NbrStatusAcctsDebit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_NbrTotalTxnsDebit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] DROP CONSTRAINT [DF_imp_TNBSummary_TotalTxnAmountDebit]
GO
DROP TABLE [dbo].[imp_TNBSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[imp_TNBSummary](
	[AssocNbr] [int] NULL,
	[NbrNonStatAcctsCredit] [int] NOT NULL,
	[NbrStatusAcctsCredit] [int] NOT NULL,
	[NbrTotalTxnsCredit] [int] NOT NULL,
	[TotalTxnAmountCredit] [numeric](18, 2) NOT NULL,
	[NbrNonStatAcctsDebit] [int] NOT NULL,
	[NbrStatusAcctsDebit] [int] NOT NULL,
	[NbrTotalTxnsDebit] [int] NOT NULL,
	[TotalTxnAmountDebit] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_NbrNonStatAcctsCredit]  DEFAULT (0) FOR [NbrNonStatAcctsCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_NbrStatusAcctsCredit]  DEFAULT (0) FOR [NbrStatusAcctsCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_NbrTotalTxnsCredit]  DEFAULT (0) FOR [NbrTotalTxnsCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_TotalTxnAmountCredit]  DEFAULT (0.00) FOR [TotalTxnAmountCredit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_NbrNonStatAcctsDebit]  DEFAULT (0) FOR [NbrNonStatAcctsDebit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_NbrStatusAcctsDebit]  DEFAULT (0) FOR [NbrStatusAcctsDebit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_NbrTotalTxnsDebit]  DEFAULT (0) FOR [NbrTotalTxnsDebit]
GO
ALTER TABLE [dbo].[imp_TNBSummary] ADD  CONSTRAINT [DF_imp_TNBSummary_TotalTxnAmountDebit]  DEFAULT (0.00) FOR [TotalTxnAmountDebit]
GO
