USE [NewTNB]
GO
/****** Object:  Table [dbo].[impTransaction_Complex]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[impTransaction_Complex]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impTransaction_Complex](
    sid_impTransaction_Complex_id bigint IDENTITY(1,1) NOT NULL,
    dim_impTransaction_Complex_histdate	varchar(8) not null,
    dim_impTransaction_Complex_acctid		varchar(16) not null,
    dim_impTransaction_Complex_transcode	varchar(3) not null,
    dim_impTransaction_Complex_transdescr	varchar(50) not null,
    dim_impTransaction_Complex_amount		varchar(11) not null,


	--[AcctNbr] [varchar](15) NULL,
	--[FolioNbr] [varchar](9) NULL,
	--[RowType] [varchar](3) NULL,
	--[CashReceived] [varchar](9) NULL,
	--[CashDisbursed] [varchar](9) NULL,
	--[ChecksReceived] [varchar](9) NULL,
	--[N_O] [varchar](2) NULL,
	--[ChecksIssued] [varchar](9) NULL,
	--[NetCashTransact] [varchar](10) NULL,
	--[NetNCSHTransactin] [varchar](11) NULL,
	--[LoadCDGLShareGL] [varchar](11) NULL,
	--[InterestFees] [varchar](9) NULL,
	--[Balance] [varchar](12) NULL,
	--[Miscellaneous] [varchar](15) NULL,
	--[Ignore01] [varchar](1) NULL,
 CONSTRAINT [PK_impTransaction_Complex] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_Complex_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
