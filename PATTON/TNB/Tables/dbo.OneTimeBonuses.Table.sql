USE [NewTNB]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[OneTimeBonuses] DROP CONSTRAINT [FK_OneTimeBonuses_OneTimeBonuses]
GO
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [varchar](2) NOT NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OneTimeBonuses]  WITH NOCHECK ADD  CONSTRAINT [FK_OneTimeBonuses_OneTimeBonuses] FOREIGN KEY([Trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[OneTimeBonuses] CHECK CONSTRAINT [FK_OneTimeBonuses_OneTimeBonuses]
GO
