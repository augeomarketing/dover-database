USE [NewTNB]
GO
/****** Object:  Table [dbo].[PointExpireControl]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[PointExpireControl] DROP CONSTRAINT [DF_PointExpireControl_DateEntered]
GO
ALTER TABLE [dbo].[PointExpireControl] DROP CONSTRAINT [DF_PointExpireControl_DateLastModified]
GO
DROP TABLE [dbo].[PointExpireControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PointExpireControl](
	[sid_PointExpireControl_Id] [int] IDENTITY(1,1) NOT NULL,
	[PointExpireControlNm] [varchar](256) NOT NULL,
	[PointExpireStoredProc] [varchar](255) NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_PointExpireControl] PRIMARY KEY CLUSTERED 
(
	[sid_PointExpireControl_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[PointExpireControl] ADD  CONSTRAINT [DF_PointExpireControl_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO
ALTER TABLE [dbo].[PointExpireControl] ADD  CONSTRAINT [DF_PointExpireControl_DateLastModified]  DEFAULT (getdate()) FOR [DateLastModified]
GO
