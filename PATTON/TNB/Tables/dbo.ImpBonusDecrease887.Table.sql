USE [NewTNB]
GO
/****** Object:  Table [dbo].[ImpBonusDecrease887]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[ImpBonusDecrease887]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImpBonusDecrease887](
	[Associate Number] [nvarchar](255) NULL,
	[System] [nvarchar](255) NULL,
	[Prin] [nvarchar](255) NULL,
	[Transaction Date] [float] NULL,
	[Credit Card Number] [nvarchar](255) NULL,
	[Transaction Code] [nvarchar](255) NULL,
	[Transaction Description] [nvarchar](255) NULL,
	[Transaction Amount] [float] NULL,
	[TxnAmount] [float] NULL,
	[Account Status] [nvarchar](255) NULL,
	[Miscellaneous 3] [nvarchar](255) NULL,
	[Credit Union Number] [nvarchar](255) NULL,
	[txndate] [datetime] NULL
) ON [PRIMARY]
GO
