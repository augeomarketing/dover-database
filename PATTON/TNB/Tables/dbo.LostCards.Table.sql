USE [NewTNB]
GO
/****** Object:  Table [dbo].[LostCards]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[LostCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LostCards](
	[sid_LostCards_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ASSNUM] [nvarchar](3) NULL,
	[FULLTIP] [nvarchar](15) NULL,
	[TFPRE] [nvarchar](3) NULL,
	[TFNUM] [nvarchar](7) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[NAME1] [nvarchar](35) NULL,
	[NAME2] [nvarchar](35) NULL,
	[ESTATUS] [nvarchar](1) NULL,
	[ISTATUS] [nvarchar](1) NULL,
	[OLDCCNUM] [nvarchar](16) NULL,
	[ADDRESS1] [nvarchar](32) NULL,
	[ADDRESS2] [nvarchar](32) NULL,
	[ADDRESS6] [nvarchar](32) NULL,
	[CITY] [nvarchar](32) NULL,
	[STATE] [nvarchar](32) NULL,
	[ZIP] [nvarchar](5) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[SSNUM] [nvarchar](9) NULL,
	[CYCLEMERCH] [nvarchar](12) NULL,
	[CHECKING] [nvarchar](16) NULL,
	[CUNUM] [nvarchar](4) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_LostCards_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
