USE [NewTNB]
GO
/****** Object:  Table [dbo].[wrkQuarterlyStatementTipPoints]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[wrkQuarterlyStatementTipPoints] DROP CONSTRAINT [DF__wrkQuarte__point__7CE47361]
GO
DROP TABLE [dbo].[wrkQuarterlyStatementTipPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkQuarterlyStatementTipPoints](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NULL,
	[ratio] [int] NULL,
	[points] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[wrkQuarterlyStatementTipPoints] ADD  DEFAULT (0) FOR [points]
GO
