USE [NewTNB]
GO
/****** Object:  Table [dbo].[impCustomer_R2UCust]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impCustomer_R2UCust] DROP CONSTRAINT [DF_impCustomer_R2UCust_DelinquentCycles]
GO
ALTER TABLE [dbo].[impCustomer_R2UCust] DROP CONSTRAINT [DF_impCustomer_R2UCust_DelinquentDays]
GO
DROP TABLE [dbo].[impCustomer_R2UCust]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impCustomer_R2UCust](
	[sid_impCustomer_R2UCust_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](35) NULL,
	[SecondaryNm] [varchar](35) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](32) NULL,
	[AddressLine2] [varchar](32) NULL,
	[City] [varchar](32) NULL,
	[StateCd] [varchar](32) NULL,
	[Zip] [varchar](5) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[AssocNbr] [varchar](3) NULL,
	[CheckingNbr] [varchar](16) NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[DelinquentCycles] [varchar](2) NULL,
	[DelinquentDays] [varchar](3) NULL,
	[GroupID] [varchar](13) NULL,
	[RelationshipControl] [varchar](1) NULL,
	[DependentStrategy] [varchar](4) NULL,
	[Column 17] [varchar](2) NULL,
 CONSTRAINT [PK__impCustomer_R2UC__023D5A04] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_R2UCust_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impCustomer_R2UCust] ADD  CONSTRAINT [DF_impCustomer_R2UCust_DelinquentCycles]  DEFAULT (0) FOR [DelinquentCycles]
GO
ALTER TABLE [dbo].[impCustomer_R2UCust] ADD  CONSTRAINT [DF_impCustomer_R2UCust_DelinquentDays]  DEFAULT (0) FOR [DelinquentDays]
GO
