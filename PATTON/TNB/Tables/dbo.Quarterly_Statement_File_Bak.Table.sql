USE [NewTNB]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File_Bak]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsPurchased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsBonus]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsReturned]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] DROP CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsDecreased]
GO
DROP TABLE [dbo].[Quarterly_Statement_File_Bak]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Statement_File_Bak](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [smalldatetime] NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[AcctID] [char](16) NULL,
	[BankNum] [char](3) NULL,
 CONSTRAINT [PK_Quarterly_Statement_File_Bak] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsPurchased]  DEFAULT (0) FOR [PointsPurchased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsBonus]  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsReturned]  DEFAULT (0) FOR [PointsReturned]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_Bak] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Bak_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
GO
