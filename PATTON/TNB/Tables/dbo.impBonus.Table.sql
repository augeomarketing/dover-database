USE [NewTNB]
GO
/****** Object:  Table [dbo].[impBonus]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[impBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impBonus](
	[sid_impBonus_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AcctId] [varchar](25) NULL,
	[Points] [decimal](18, 0) NULL,
	[TranDescription] [varchar](50) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL,
	[DateAdded] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_impBonus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
