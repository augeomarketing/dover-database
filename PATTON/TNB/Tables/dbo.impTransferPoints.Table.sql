USE [NewTNB]
GO
/****** Object:  Table [dbo].[impTransferPoints]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impTransferPoints] DROP CONSTRAINT [DF_impTransferPoints_TransferPoints]
GO
DROP TABLE [dbo].[impTransferPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impTransferPoints](
	[sid_ImpTransaction_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [nvarchar](16) NOT NULL,
	[PrimaryNm] [nvarchar](35) NOT NULL,
	[SecondaryNm] [nvarchar](35) NULL,
	[ExternalSts] [nvarchar](1) NULL,
	[InternalSts] [nvarchar](1) NULL,
	[OldCCAcctNbr] [nvarchar](16) NULL,
	[AddressLine1] [nvarchar](32) NULL,
	[AddressLine2] [nvarchar](32) NULL,
	[City] [nvarchar](32) NULL,
	[StateCd] [nvarchar](32) NULL,
	[Zip] [nvarchar](10) NULL,
	[Phone1] [nvarchar](10) NULL,
	[Phone2] [nvarchar](10) NULL,
	[PrimarySSN] [nvarchar](9) NULL,
	[AssocNbr] [nvarchar](3) NOT NULL,
	[DDANbr] [nvarchar](16) NULL,
	[Misc3] [nvarchar](7) NULL,
	[CreditUnionNbr] [nvarchar](4) NULL,
	[TransferPoints] [int] NOT NULL,
	[TipNumber] [nvarchar](15) NULL,
 CONSTRAINT [PK__impTransferPoint__48C5B0DD] PRIMARY KEY CLUSTERED 
(
	[sid_ImpTransaction_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impTransferPoints] ADD  CONSTRAINT [DF_impTransferPoints_TransferPoints]  DEFAULT (0) FOR [TransferPoints]
GO
