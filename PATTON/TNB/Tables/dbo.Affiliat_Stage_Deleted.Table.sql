USE [NewTNB]
GO
/****** Object:  Table [dbo].[Affiliat_Stage_Deleted]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[Affiliat_Stage_Deleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Affiliat_Stage_Deleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
