USE [NewTNB]
GO

/****** Object:  Table [dbo].[impDeletedCards]    Script Date: 09/08/2010 11:54:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impDeletedCards]') AND type in (N'U'))
DROP TABLE [dbo].[impDeletedCards]
GO

USE [NewTNB]
GO

/****** Object:  Table [dbo].[impDeletedCards]    Script Date: 09/08/2010 11:54:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impDeletedCards](
	[sid_impdeletedcards_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ccacctnbr] [varchar](16) NOT NULL,
	[primarynm] [varchar](35) NOT NULL,
	[primaryssn] [varchar](9) NOT NULL,
	[assocnbr] [varchar](3) NOT NULL,
	[checkingnbr] [varchar](16) NOT NULL,
	[creditunionnbr] [varchar](4) NOT NULL,
 CONSTRAINT [PK_impDeletedCards] PRIMARY KEY CLUSTERED 
(
	[sid_impdeletedcards_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


