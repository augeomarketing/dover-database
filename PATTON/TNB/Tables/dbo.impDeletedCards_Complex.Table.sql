USE [NewTNB]
GO

/****** Object:  Table [dbo].[impDeletedCards_Complex]    Script Date: 10/29/2010 16:59:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impDeletedCards_Complex]') AND type in (N'U'))
DROP TABLE [dbo].[impDeletedCards_Complex]
GO

USE [NewTNB]
GO

/****** Object:  Table [dbo].[impDeletedCards_Complex]    Script Date: 10/29/2010 16:59:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impDeletedCards_Complex](
	[DDA] [varchar](25) NULL,
	[AcctName1] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zipcode] [varchar](10) NULL,
	[AcctID] [varchar](16) NULL,
	[Ignore] [varchar](50) NULL
) ON [PRIMARY]

GO

