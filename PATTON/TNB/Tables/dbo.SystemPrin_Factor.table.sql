USE [NewTNB]
GO
/****** Object:  Table [dbo].[SystemPrin_Factor]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[SystemPrin_Factor] DROP CONSTRAINT [DF_SystemPrin_Factor_Point]
GO
DROP TABLE [dbo].[SystemPrin_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemPrin_Factor](
	[System] [varchar](4) NOT NULL,
	[Prin] [varchar](4) NOT NULL,
	[PointFactor] [decimal](3, 2) NOT NULL,
 CONSTRAINT [PK_SystemPrin_Factor] PRIMARY KEY CLUSTERED 
(
	[System] ASC,
	[Prin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SystemPrin_Factor] ADD  CONSTRAINT [DF_SystemPrin_Factor_Point]  DEFAULT (1) FOR [PointFactor]
GO
