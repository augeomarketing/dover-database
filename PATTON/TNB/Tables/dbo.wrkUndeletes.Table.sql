USE [NewTNB]
GO
/****** Object:  Table [dbo].[wrkUndeletes]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[wrkUndeletes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkUndeletes](
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
