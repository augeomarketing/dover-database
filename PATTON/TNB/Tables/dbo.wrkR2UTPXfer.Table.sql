USE [NewTNB]
GO
/****** Object:  Table [dbo].[wrkR2UTPXfer]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[wrkR2UTPXfer] DROP CONSTRAINT [DF_wrkR2UTPXfer_Processed]
GO
DROP TABLE [dbo].[wrkR2UTPXfer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkR2UTPXfer](
	[CuNo] [nvarchar](4) NULL,
	[System] [nvarchar](4) NULL,
	[Prin] [nvarchar](4) NULL,
	[AcctNo] [nvarchar](25) NULL,
	[ExtStat] [nvarchar](1) NULL,
	[CrossRefAcctNo1] [nvarchar](25) NULL,
	[Misc5] [nvarchar](5) NULL,
	[PrimaryName] [nvarchar](50) NULL,
	[Addr1] [nvarchar](50) NULL,
	[Addr2] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](2) NULL,
	[Zip] [nvarchar](10) NULL,
	[FMDate] [datetime] NULL,
	[TipNumber] [varchar](15) NULL,
	[CrossRefAcctNo3] [nvarchar](25) NULL,
	[NewTipNumber] [varchar](15) NULL,
	[Processed] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[wrkR2UTPXfer] ADD  CONSTRAINT [DF_wrkR2UTPXfer_Processed]  DEFAULT ('N') FOR [Processed]
GO
