USE [NewTNB]
GO
/****** Object:  Table [dbo].[ReportType]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[ReportType] DROP CONSTRAINT [DF_ReportType_CreateDt]
GO
DROP TABLE [dbo].[ReportType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReportType](
	[ReportTypeCd] [varchar](1) NOT NULL,
	[ReporttypeDesc] [varchar](255) NOT NULL,
	[CreateDt] [datetime] NOT NULL,
	[LastUpdateDt] [datetime] NULL,
 CONSTRAINT [PK_ReportType] PRIMARY KEY CLUSTERED 
(
	[ReportTypeCd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ReportType] ADD  CONSTRAINT [DF_ReportType_CreateDt]  DEFAULT (getdate()) FOR [CreateDt]
GO
