USE [NewTNB]
GO
/****** Object:  Table [dbo].[ComplexTrancodeLookup]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[ComplexTrancodeLookup] DROP CONSTRAINT [FK_ComplexTrancodeLookup_TranType]
GO
DROP TABLE [dbo].[ComplexTrancodeLookup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ComplexTrancodeLookup](
	[sid_ComplexTranCodeLookup_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_ComplexTranCode_Lookup_TranType] [varchar](15) NOT NULL,
	[sid_TranType_TranCode] [varchar](2) NOT NULL,
 CONSTRAINT [PK_ComplexTrancodeLookup] PRIMARY KEY CLUSTERED 
(
	[sid_ComplexTranCodeLookup_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ComplexTrancodeLookup]  WITH CHECK ADD  CONSTRAINT [FK_ComplexTrancodeLookup_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[ComplexTrancodeLookup] CHECK CONSTRAINT [FK_ComplexTrancodeLookup_TranType]
GO
