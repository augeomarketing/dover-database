USE [NewTNB]
GO
/****** Object:  Table [dbo].[impCustomer_Purge]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impCustomer_Purge] DROP CONSTRAINT [DF_impCustomer_Purge_DateAdded]
GO
DROP TABLE [dbo].[impCustomer_Purge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impCustomer_Purge](
	[TipNumber] [varchar](15) NOT NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[PrimaryNm] [varchar](35) NOT NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impCustomer_Purge] ADD  CONSTRAINT [DF_impCustomer_Purge_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
