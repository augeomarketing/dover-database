USE [NewTNB]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assoc_QuarterlyStatementFormat]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assoc]'))
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [FK_Assoc_QuarterlyStatementFormat]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assoc_ReportType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assoc]'))
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [FK_Assoc_ReportType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assoc_StatementOption]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assoc]'))
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [FK_Assoc_StatementOption]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Assoc_ExpireTransferPoints_YorN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assoc]'))
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [CK_Assoc_ExpireTransferPoints_YorN]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_Assoc_InactivePurgeFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assoc]'))
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [CK_Assoc_InactivePurgeFlag]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Assoc_CombineNames]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [DF_Assoc_CombineNames]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Assoc_DDAMatch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [DF_Assoc_DDAMatch]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Assoc_ExpireTransferPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [DF_Assoc_ExpireTransferPoints]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Assoc_ExcludeEStatement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [DF_Assoc_ExcludeEStatement]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Assoc_InactivePurgeFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [DF_Assoc_InactivePurgeFlag]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Assoc_InactivePurgePeriodMonths]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Assoc] DROP CONSTRAINT [DF_Assoc_InactivePurgePeriodMonths]
END

GO

USE [NewTNB]
GO

/****** Object:  Table [dbo].[Assoc]    Script Date: 06/03/2010 10:15:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assoc]') AND type in (N'U'))
DROP TABLE [dbo].[Assoc]
GO

USE [NewTNB]
GO

/****** Object:  Table [dbo].[Assoc]    Script Date: 06/03/2010 10:15:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Assoc](
	[AssocNum] [varchar](4) NOT NULL,
	[TIPFirst] [varchar](3) NOT NULL,
	[CUNum] [varchar](4) NULL,
	[CombineNames] [varchar](1) NOT NULL,
	[DDAMatch] [varchar](1) NOT NULL,
	[ReporttypeCd] [varchar](1) NOT NULL,
	[sid_QuarterlyStatementFormat_id] [int] NOT NULL,
	[sid_StatementOption_Code] [int] NULL,
	[ExpireTransferPoints] [varchar](1) NULL,
	[ExcludeEStatement] [varchar](1) NOT NULL,
	[InactivePurgeFlag] [varchar](1) NOT NULL,
	[InactivePurgePeriodMonths] [int] NOT NULL,
 CONSTRAINT [PK_Assoc_1] PRIMARY KEY CLUSTERED 
(
	[AssocNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [NewTNB]
/****** Object:  Index [IX_Assoc_TipFirst_AssocNum]    Script Date: 06/03/2010 10:15:51 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Assoc_TipFirst_AssocNum] ON [dbo].[Assoc] 
(
	[TIPFirst] ASC,
	[AssocNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Assoc]  WITH NOCHECK ADD  CONSTRAINT [FK_Assoc_QuarterlyStatementFormat] FOREIGN KEY([sid_StatementOption_Code])
REFERENCES [dbo].[StatementOption] ([sid_StatementOption_Code])
GO

ALTER TABLE [dbo].[Assoc] CHECK CONSTRAINT [FK_Assoc_QuarterlyStatementFormat]
GO

ALTER TABLE [dbo].[Assoc]  WITH NOCHECK ADD  CONSTRAINT [FK_Assoc_ReportType] FOREIGN KEY([ReporttypeCd])
REFERENCES [dbo].[ReportType] ([ReportTypeCd])
GO

ALTER TABLE [dbo].[Assoc] CHECK CONSTRAINT [FK_Assoc_ReportType]
GO

ALTER TABLE [dbo].[Assoc]  WITH CHECK ADD  CONSTRAINT [FK_Assoc_StatementOption] FOREIGN KEY([sid_StatementOption_Code])
REFERENCES [dbo].[StatementOption] ([sid_StatementOption_Code])
GO

ALTER TABLE [dbo].[Assoc] CHECK CONSTRAINT [FK_Assoc_StatementOption]
GO

ALTER TABLE [dbo].[Assoc]  WITH NOCHECK ADD  CONSTRAINT [CK_Assoc_ExpireTransferPoints_YorN] CHECK  (([ExpireTransferPoints]='N' OR [ExpireTransferPoints]='Y'))
GO

ALTER TABLE [dbo].[Assoc] CHECK CONSTRAINT [CK_Assoc_ExpireTransferPoints_YorN]
GO

ALTER TABLE [dbo].[Assoc]  WITH CHECK ADD  CONSTRAINT [CK_Assoc_InactivePurgeFlag] CHECK  (([InactivePurgeFlag]='N' OR [InactivePurgeFlag]='Y'))
GO

ALTER TABLE [dbo].[Assoc] CHECK CONSTRAINT [CK_Assoc_InactivePurgeFlag]
GO

ALTER TABLE [dbo].[Assoc] ADD  CONSTRAINT [DF_Assoc_CombineNames]  DEFAULT ('N') FOR [CombineNames]
GO

ALTER TABLE [dbo].[Assoc] ADD  CONSTRAINT [DF_Assoc_DDAMatch]  DEFAULT ('N') FOR [DDAMatch]
GO

ALTER TABLE [dbo].[Assoc] ADD  CONSTRAINT [DF_Assoc_ExpireTransferPoints]  DEFAULT ('N') FOR [ExpireTransferPoints]
GO

ALTER TABLE [dbo].[Assoc] ADD  CONSTRAINT [DF_Assoc_ExcludeEStatement]  DEFAULT ('N') FOR [ExcludeEStatement]
GO

ALTER TABLE [dbo].[Assoc] ADD  CONSTRAINT [DF_Assoc_InactivePurgeFlag]  DEFAULT ('N') FOR [InactivePurgeFlag]
GO

ALTER TABLE [dbo].[Assoc] ADD  CONSTRAINT [DF_Assoc_InactivePurgePeriodMonths]  DEFAULT ((0)) FOR [InactivePurgePeriodMonths]
GO

