USE [NewTNB]
GO
/****** Object:  Table [dbo].[TipFirstPointExpireControl]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[TipFirstPointExpireControl] DROP CONSTRAINT [FK_TipFirstPointExpireControl_PointExpireControl]
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] DROP CONSTRAINT [DF_TipFirstPointExpireControl_TipFirstPointExpireControlExpirationDate]
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] DROP CONSTRAINT [DF_TipFirstPointExpireControl_DateEntered]
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] DROP CONSTRAINT [DF_TipFirstPointExpireControl_DateLastModified]
GO
DROP TABLE [dbo].[TipFirstPointExpireControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipFirstPointExpireControl](
	[TipFirst] [varchar](3) NOT NULL,
	[sid_PointExpireControl_Id] [int] NOT NULL,
	[TipFirstPointExpireControlEffectiveDate] [datetime] NOT NULL,
	[TipFirstPointExpireControlExpirationDate] [datetime] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_TipFirstPointExpireControl] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC,
	[sid_PointExpireControl_Id] ASC,
	[TipFirstPointExpireControlEffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl]  WITH CHECK ADD  CONSTRAINT [FK_TipFirstPointExpireControl_PointExpireControl] FOREIGN KEY([sid_PointExpireControl_Id])
REFERENCES [dbo].[PointExpireControl] ([sid_PointExpireControl_Id])
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] CHECK CONSTRAINT [FK_TipFirstPointExpireControl_PointExpireControl]
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] ADD  CONSTRAINT [DF_TipFirstPointExpireControl_TipFirstPointExpireControlExpirationDate]  DEFAULT ('12/31/9999') FOR [TipFirstPointExpireControlExpirationDate]
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] ADD  CONSTRAINT [DF_TipFirstPointExpireControl_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO
ALTER TABLE [dbo].[TipFirstPointExpireControl] ADD  CONSTRAINT [DF_TipFirstPointExpireControl_DateLastModified]  DEFAULT (getdate()) FOR [DateLastModified]
GO
