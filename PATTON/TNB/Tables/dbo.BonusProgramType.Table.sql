USE [NewTNB]
GO
/****** Object:  Table [dbo].[BonusProgramType]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[BonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]
GO
ALTER TABLE [dbo].[BonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]
GO
DROP TABLE [dbo].[BonusProgramType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusProgramType](
	[sid_BonusProgramType_ID] [varchar](1) NOT NULL,
	[dim_BonusProgramType_Name] [varchar](255) NOT NULL,
	[dim_BonusProgramType_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgramType_LastUpdated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgramType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgramType_DateAdded]
GO
ALTER TABLE [dbo].[BonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgramType_LastUpdated]
GO
