USE [NewTNB]
GO
/****** Object:  Table [dbo].[impCustomer_Complex]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[impCustomer_Complex]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impCustomer_Complex](
	[impCustomer_Complex_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AcctNbr] [varchar](25) NULL,
	[AcctName] [varchar](35) NULL,
	[AddressLine1] [varchar](35) NULL,
	[AddressLine2] [varchar](35) NULL,
	[City] [varchar](35) NULL,
	[StateCd] [varchar](2) NULL,
	[ZipCd] [varchar](10) NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[TipFirst] [varchar](3) NULL,
 CONSTRAINT [PK_impCustomer_Complex] PRIMARY KEY CLUSTERED 
(
	[impCustomer_Complex_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
