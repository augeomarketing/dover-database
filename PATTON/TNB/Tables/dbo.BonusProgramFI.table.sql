USE [NewTNB]
GO
/****** Object:  Table [dbo].[BonusProgramFI]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [FK_BonusProgramFI_BonusProgram]
GO
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]
GO
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]
GO
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]
GO
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]
GO
DROP TABLE [dbo].[BonusProgramFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusProgramFI](
	[sid_BonusProgramFI_TipFirst] [varchar](3) NOT NULL,
	[sid_BonusProgram_ID] [int] NOT NULL,
	[dim_BonusProgramFI_Effectivedate] [datetime] NOT NULL,
	[dim_BonusProgramFI_ExpirationDate] [datetime] NOT NULL,
	[dim_BonusProgramFI_BonusPoints] [int] NOT NULL,
	[dim_BonusProgramFI_PointMultiplier] [int] NOT NULL,
	[dim_BonusProgramFI_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgramFI_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BonusProgramFI] PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgramFI_TipFirst] ASC,
	[sid_BonusProgram_ID] ASC,
	[dim_BonusProgramFI_Effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BonusProgramFI]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgramFI_BonusProgram] FOREIGN KEY([sid_BonusProgram_ID])
REFERENCES [dbo].[BonusProgram] ([sid_BonusProgram_ID])
GO
ALTER TABLE [dbo].[BonusProgramFI] CHECK CONSTRAINT [FK_BonusProgramFI_BonusProgram]
GO
ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]  DEFAULT (0) FOR [dim_BonusProgramFI_BonusPoints]
GO
ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]  DEFAULT (1) FOR [dim_BonusProgramFI_PointMultiplier]
GO
ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgramFI_DateAdded]
GO
ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgramFI_LastUpdated]
GO
