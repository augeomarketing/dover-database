USE [NewTNB]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[TransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransStandard](
	[TIP] [nvarchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctNum] [nvarchar](25) NOT NULL,
	[TranCode] [nvarchar](2) NOT NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](20) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL,
 CONSTRAINT [PK_TransStandard] PRIMARY KEY CLUSTERED 
(
	[TIP] ASC,
	[TranDate] ASC,
	[AcctNum] ASC,
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
