USE [NewTNB]
GO
/****** Object:  Table [dbo].[bkup_purge_pending]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[bkup_purge_pending]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bkup_purge_pending](
	[TipNumber] [varchar](15) NOT NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[PrimaryNm] [varchar](35) NOT NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
