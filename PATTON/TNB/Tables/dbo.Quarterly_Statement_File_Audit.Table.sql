USE [NewTNB]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File_Audit]    Script Date: 07/23/2014 09:46:19 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBegin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsEnd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsPurchasedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBonusDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsPurchasedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBonusCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBonusCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_POINTSBONUSMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_POINTSBONUSMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_POINTSBONUSMN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsIncreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsRedeemed]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsReturnedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsReturnedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsSubtracted]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsDecreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PurchasedPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PurchasedPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_PurchasedPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_Points2Expire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_Points2Expire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] DROP CONSTRAINT [DF_Quarterly_Statement_File_Audit_Points2Expire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File_Audit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File_Audit](
	[Tipnumber] [varchar](50) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](20) NULL,
	[STDATE] [date] NULL,
	[PointsBegin] [int] NOT NULL,
	[PointsEnd] [int] NOT NULL,
	[PointsBonus] [int] NOT NULL,
	[PointsPurchasedDB] [int] NOT NULL,
	[PointsBonusDB] [int] NOT NULL,
	[PointsPurchasedCR] [int] NOT NULL,
	[PointsBonusCR] [int] NOT NULL,
	[POINTSBONUSMN] [int] NOT NULL,
	[PointsAdded] [int] NOT NULL,
	[PointsIncreased] [int] NOT NULL,
	[PointsRedeemed] [int] NOT NULL,
	[PointsReturnedCR] [int] NOT NULL,
	[PointsReturnedDB] [int] NOT NULL,
	[PointsSubtracted] [int] NOT NULL,
	[PointsDecreased] [int] NOT NULL,
	[PointsExpire] [int] NOT NULL,
	[AcctID] [char](16) NULL,
	[BankNum] [varchar](100) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[status] [char](1) NULL,
	[PurchasedPoints] [int] NOT NULL,
	[Points2Expire] [int] NOT NULL,
 CONSTRAINT [PK_Quarterly_Statement_File_Audit] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBonusDB]  DEFAULT ((0)) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsBonusCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsBonusCR]  DEFAULT ((0)) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_POINTSBONUSMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_POINTSBONUSMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_POINTSBONUSMN]  DEFAULT ((0)) FOR [POINTSBONUSMN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PointsExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_PurchasedPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_PurchasedPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_PurchasedPoints]  DEFAULT ((0)) FOR [PurchasedPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_Audit_Points2Expire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_Audit]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_Audit_Points2Expire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_Audit] ADD  CONSTRAINT [DF_Quarterly_Statement_File_Audit_Points2Expire]  DEFAULT ((0)) FOR [Points2Expire]
END


End
GO
