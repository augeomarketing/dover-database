USE [NewTNB]
GO
/****** Object:  Table [dbo].[BonusProgram]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [FK_BonusProgram_BonusProgramType]
GO
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]
GO
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]
GO
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]
GO
DROP TABLE [dbo].[BonusProgram]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BonusProgram](
	[sid_BonusProgram_ID] [int] IDENTITY(1,1) NOT NULL,
	[dim_BonusProgram_Description] [varchar](255) NOT NULL,
	[dim_BonusProgramEffectiveDate] [datetime] NOT NULL,
	[dim_BonusProgramExpirationDate] [datetime] NOT NULL,
	[sid_BonusProgramType_ID] [varchar](1) NOT NULL,
	[sid_TranType_TranCode] [varchar](2) NOT NULL,
	[dim_BonusProgram_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgram_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgram_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[BonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgram_BonusProgramType] FOREIGN KEY([sid_BonusProgramType_ID])
REFERENCES [dbo].[BonusProgramType] ([sid_BonusProgramType_ID])
GO
ALTER TABLE [dbo].[BonusProgram] CHECK CONSTRAINT [FK_BonusProgram_BonusProgramType]
GO
ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]  DEFAULT ('12/31/9999 23:59:59') FOR [dim_BonusProgramExpirationDate]
GO
ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgram_DateAdded]
GO
ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgram_LastUpdated]
GO
