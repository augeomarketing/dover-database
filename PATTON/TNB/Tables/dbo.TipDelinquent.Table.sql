USE [NewTNB]
GO
/****** Object:  Table [dbo].[TipDelinquent]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[TipDelinquent] DROP CONSTRAINT [DF_TipDelinquent_DelinquentEffectiveDate]
GO
ALTER TABLE [dbo].[TipDelinquent] DROP CONSTRAINT [DF_TipDelinquent_TipDelinquentCycles]
GO
ALTER TABLE [dbo].[TipDelinquent] DROP CONSTRAINT [DF_TipDelinquent_TipDelinquentDays]
GO
ALTER TABLE [dbo].[TipDelinquent] DROP CONSTRAINT [DF_TipDelinquent_DelinquentExpirationDate]
GO
ALTER TABLE [dbo].[TipDelinquent] DROP CONSTRAINT [DF_TipDelinquent_TipDelinquentDateAdded]
GO
ALTER TABLE [dbo].[TipDelinquent] DROP CONSTRAINT [DF_TipDelinquent_TipDelinquentDateModified]
GO
DROP TABLE [dbo].[TipDelinquent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipDelinquent](
	[TipFirst] [varchar](3) NOT NULL,
	[TipDelinquentEffectiveDate] [datetime] NOT NULL,
	[TipDelinquentCycles] [int] NOT NULL,
	[TipDelinquentDays] [int] NOT NULL,
	[TipDelinquentExpirationDate] [datetime] NOT NULL,
	[TipDelinquentDateAdded] [datetime] NOT NULL,
	[TipDelinquentDateModified] [datetime] NOT NULL,
 CONSTRAINT [PK_TipDelinquent] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC,
	[TipDelinquentEffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TipDelinquent] ADD  CONSTRAINT [DF_TipDelinquent_DelinquentEffectiveDate]  DEFAULT (getdate()) FOR [TipDelinquentEffectiveDate]
GO
ALTER TABLE [dbo].[TipDelinquent] ADD  CONSTRAINT [DF_TipDelinquent_TipDelinquentCycles]  DEFAULT (0) FOR [TipDelinquentCycles]
GO
ALTER TABLE [dbo].[TipDelinquent] ADD  CONSTRAINT [DF_TipDelinquent_TipDelinquentDays]  DEFAULT (0) FOR [TipDelinquentDays]
GO
ALTER TABLE [dbo].[TipDelinquent] ADD  CONSTRAINT [DF_TipDelinquent_DelinquentExpirationDate]  DEFAULT (12 / 31 / 9999) FOR [TipDelinquentExpirationDate]
GO
ALTER TABLE [dbo].[TipDelinquent] ADD  CONSTRAINT [DF_TipDelinquent_TipDelinquentDateAdded]  DEFAULT (getdate()) FOR [TipDelinquentDateAdded]
GO
ALTER TABLE [dbo].[TipDelinquent] ADD  CONSTRAINT [DF_TipDelinquent_TipDelinquentDateModified]  DEFAULT (getdate()) FOR [TipDelinquentDateModified]
GO
