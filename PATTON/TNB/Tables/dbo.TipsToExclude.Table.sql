USE [NewTNB]
GO
/****** Object:  Table [dbo].[TipsToExclude]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[TipsToExclude] DROP CONSTRAINT [DF_TipsToExclude_DateAdded]
GO
DROP TABLE [dbo].[TipsToExclude]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipsToExclude](
	[TipFirst] [varchar](3) NOT NULL,
	[ClientCode] [varchar](15) NOT NULL,
	[ExcludeReason] [varchar](2000) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateLastModified] [datetime] NULL,
 CONSTRAINT [PK_TipsToExclude] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TipsToExclude] ADD  CONSTRAINT [DF_TipsToExclude_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
