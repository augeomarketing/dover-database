USE [NewTNB]
GO
/****** Object:  Table [dbo].[StatementOption]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[StatementOption] DROP CONSTRAINT [DF_StatementOption_dim_StatementOption_DateAdded]
GO
ALTER TABLE [dbo].[StatementOption] DROP CONSTRAINT [DF_StatementOption_dim_StatementOption_LastUpdated]
GO
DROP TABLE [dbo].[StatementOption]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatementOption](
	[sid_StatementOption_Code] [int] NOT NULL,
	[dim_StatementOption_Description] [varchar](255) NOT NULL,
	[dim_StatementOption_DateAdded] [datetime] NOT NULL,
	[dim_StatementOption_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_StatementOption] PRIMARY KEY CLUSTERED 
(
	[sid_StatementOption_Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[StatementOption] ADD  CONSTRAINT [DF_StatementOption_dim_StatementOption_DateAdded]  DEFAULT (getdate()) FOR [dim_StatementOption_DateAdded]
GO
ALTER TABLE [dbo].[StatementOption] ADD  CONSTRAINT [DF_StatementOption_dim_StatementOption_LastUpdated]  DEFAULT (getdate()) FOR [dim_StatementOption_LastUpdated]
GO
