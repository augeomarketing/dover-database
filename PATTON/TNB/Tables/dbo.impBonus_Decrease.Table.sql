USE [NewTNB]
GO
/****** Object:  Table [dbo].[impBonus_Decrease]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impBonus_Decrease] DROP CONSTRAINT [DF_impBonus_Decrease_TransactionAmount]
GO
DROP TABLE [dbo].[impBonus_Decrease]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impBonus_Decrease](
	[sid_ImpBonus_Decrease_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NOT NULL,
	[System] [varchar](4) NOT NULL,
	[Prin] [varchar](4) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCode] [varchar](3) NOT NULL,
	[TransactionDescription] [varchar](255) NOT NULL,
	[TransactionAmount] [decimal](18, 2) NOT NULL,
	[AccountStatus] [varchar](2) NULL,
	[Miscellaneous3] [varchar](7) NULL,
	[CreditUnionNumber] [varchar](4) NOT NULL,
	[TransactionAmountString] [varchar](20) NULL,
 CONSTRAINT [PK_impBonus_Decrease] PRIMARY KEY CLUSTERED 
(
	[sid_ImpBonus_Decrease_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impBonus_Decrease] ADD  CONSTRAINT [DF_impBonus_Decrease_TransactionAmount]  DEFAULT (0.00) FOR [TransactionAmount]
GO
