USE [NewTNB]
GO
/****** Object:  Table [dbo].[assoc_B4R2UTPPurge]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[assoc_B4R2UTPPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[assoc_B4R2UTPPurge](
	[AssocNum] [varchar](4) NOT NULL,
	[TIPFirst] [varchar](3) NOT NULL,
	[CUNum] [varchar](4) NULL,
	[CombineNames] [varchar](1) NOT NULL,
	[DDAMatch] [varchar](1) NOT NULL,
	[ReporttypeCd] [varchar](1) NOT NULL,
	[sid_QuarterlyStatementFormat_id] [int] NOT NULL,
	[sid_StatementOption_Code] [int] NULL,
	[ExpireTransferPoints] [varchar](1) NULL,
	[ExcludeEStatement] [varchar](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
