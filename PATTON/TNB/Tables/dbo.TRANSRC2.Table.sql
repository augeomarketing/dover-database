USE [NewTNB]
GO
/****** Object:  Table [dbo].[TRANSRC2]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[TRANSRC2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRANSRC2](
	[DATE1] [smalldatetime] NULL,
	[ACCTNO] [nvarchar](16) NULL,
	[TRANCODE] [nvarchar](3) NULL,
	[DISCRIPT] [nvarchar](50) NULL,
	[TRANAMT] [float] NULL
) ON [PRIMARY]
GO
