USE [NewTNB]
GO
/****** Object:  Table [dbo].[CashBackInstitutions]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[CashBackInstitutions] DROP CONSTRAINT [DF_CashBackInstitutions_DateAdded]
GO
DROP TABLE [dbo].[CashBackInstitutions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CashBackInstitutions](
	[CashBackInstitutionId] [int] IDENTITY(1,1) NOT NULL,
	[AssocNum] [varchar](4) NOT NULL,
	[CUNum] [varchar](4) NOT NULL,
	[WelcomeKitFileName] [varchar](50) NULL,
	[DateAdded] [datetime] NOT NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[CashBackInstitutions] ADD  CONSTRAINT [DF_CashBackInstitutions_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
