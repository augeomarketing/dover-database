USE [NewTNB]
GO
/****** Object:  Table [dbo].[web_Account]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[web_Account]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[web_Account](
	[TipNumber] [char](20) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [char](6) NOT NULL,
	[SSNLast4] [char](4) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_web_Account] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC,
	[LastName] ASC,
	[LastSix] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
