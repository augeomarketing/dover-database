USE [NewTNB]
GO
/****** Object:  Table [dbo].[QuarterlyStatementFormat]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[QuarterlyStatementFormat] DROP CONSTRAINT [DF_QuarterlyStatementFormat_dim_QuarterlyStatementFormat_DateAdded]
GO
DROP TABLE [dbo].[QuarterlyStatementFormat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuarterlyStatementFormat](
	[sid_QuarterlyStatementFormat_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_QuarterlyStatementFormat_Name] [varchar](255) NOT NULL,
	[dim_QuarterlyStatementFormat_DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_QuarterlyStatementFormat] PRIMARY KEY CLUSTERED 
(
	[sid_QuarterlyStatementFormat_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[QuarterlyStatementFormat] ADD  CONSTRAINT [DF_QuarterlyStatementFormat_dim_QuarterlyStatementFormat_DateAdded]  DEFAULT (getdate()) FOR [dim_QuarterlyStatementFormat_DateAdded]
GO
