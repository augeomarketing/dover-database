USE [NewTNB]
GO
/****** Object:  Table [dbo].[qsfHistorySummary]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[qsfHistorySummary] DROP CONSTRAINT [DF_qsfHistorySummary_Points]
GO
DROP TABLE [dbo].[qsfHistorySummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[qsfHistorySummary](
	[TipNumber] [varchar](15) NOT NULL,
	[TranCode] [varchar](2) NOT NULL,
	[Points] [bigint] NOT NULL,
 CONSTRAINT [PK_qsfHistorySummary] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC,
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[qsfHistorySummary] ADD  CONSTRAINT [DF_qsfHistorySummary_Points]  DEFAULT (0) FOR [Points]
GO
