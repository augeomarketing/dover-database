USE [NewTNB]
GO
/****** Object:  Table [dbo].[impCustomer_DowCust]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[impCustomer_DowCust]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impCustomer_DowCust](
	[sid_impCustomer_DowCust_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](35) NULL,
	[SecondaryNm] [varchar](35) NULL,
	[Filler01] [varchar](5) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[Filler02] [varchar](45) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[Filler03] [varchar](1) NULL,
	[TravelFreeNbr] [varchar](10) NULL,
	[AddressLine1] [varchar](32) NULL,
	[AddressLine2] [varchar](32) NULL,
	[City] [varchar](32) NULL,
	[StateCd] [varchar](32) NULL,
	[Zip] [varchar](5) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[Filler04] [varchar](34) NULL,
	[Column 18] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_DowCust_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
