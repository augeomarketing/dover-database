USE [NewTNB]
GO
/****** Object:  Table [dbo].[impTransaction_DowTran]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impTransaction_DowTran] DROP CONSTRAINT [DF_impTransaction_DowTran_TransactionAmt]
GO
DROP TABLE [dbo].[impTransaction_DowTran]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impTransaction_DowTran](
	[sid_impTransaction_DowTran_id] [bigint] IDENTITY(1,1) NOT NULL,
	[TravelFreeNbr] [varchar](6) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NOT NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [varchar](10) NOT NULL,
	[Column 6] [varchar](2) NULL,
 CONSTRAINT [PK__impTransaction_D__09DE7BCC] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_DowTran_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impTransaction_DowTran] ADD  CONSTRAINT [DF_impTransaction_DowTran_TransactionAmt]  DEFAULT (0.00) FOR [TransactionAmt]
GO
