USE [NewTNB]
GO
/****** Object:  Table [dbo].[impCustomer_USNewMexico]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[impCustomer_USNewMexico]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impCustomer_USNewMexico](
	[sid_impCustomer_USNewMexico_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [nvarchar](16) NULL,
	[PrimaryNm] [nvarchar](35) NULL,
	[SecondaryNm] [nvarchar](35) NULL,
	[ExternalSts] [nvarchar](1) NULL,
	[InternalSts] [nvarchar](1) NULL,
	[OldCCAcctNbr] [nvarchar](16) NULL,
	[AddressLine1] [nvarchar](32) NULL,
	[AddressLine2] [nvarchar](32) NULL,
	[City] [nvarchar](32) NULL,
	[StateCd] [nvarchar](32) NULL,
	[Zip] [nvarchar](5) NULL,
	[Phone1] [nvarchar](10) NULL,
	[Phone2] [nvarchar](10) NULL,
	[PrimarySSN] [nvarchar](9) NULL,
	[AssocNbr] [nvarchar](3) NULL,
	[CheckingNbr] [nvarchar](16) NULL,
	[CreditUnionNbr] [nvarchar](4) NULL,
	[Prin] [nvarchar](4) NULL,
	[DDA] [nvarchar](16) NULL,
 CONSTRAINT [PK__impCustomer_USNe__4F72AE6C] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_USNewMexico_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
