USE [NewTNB]
GO
/****** Object:  Table [dbo].[wrkLaDOTD_DebitDeletes]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[wrkLaDOTD_DebitDeletes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkLaDOTD_DebitDeletes](
	[AcctNo] [nvarchar](255) NULL,
	[PrimaryName] [nvarchar](255) NULL,
	[Spouse] [nvarchar](255) NULL,
	[ExtStat] [nvarchar](255) NULL,
	[InternalStat] [nvarchar](255) NULL,
	[CrossRefAcctNo3] [nvarchar](255) NULL,
	[Addr1] [nvarchar](255) NULL,
	[Addr2] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[Zip] [nvarchar](15) NULL,
	[Phone1] [nvarchar](255) NULL,
	[Phone2] [nvarchar](255) NULL,
	[PrimarySSN] [nvarchar](255) NULL,
	[Assoc] [nvarchar](255) NULL,
	[DDA] [nvarchar](255) NULL,
	[CUNum] [nvarchar](255) NULL,
	[TipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
