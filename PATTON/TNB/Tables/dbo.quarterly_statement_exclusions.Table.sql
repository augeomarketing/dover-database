USE [NewTNB]
GO
/****** Object:  Table [dbo].[quarterly_statement_exclusions]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[quarterly_statement_exclusions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[quarterly_statement_exclusions](
	[Tipnumber] [varchar](15) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
