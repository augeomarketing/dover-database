USE [NewTNB]
GO
/****** Object:  Table [dbo].[AssocDelinquent]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[AssocDelinquent] DROP CONSTRAINT [FK_AssocDelinquent_Assoc]
GO
ALTER TABLE [dbo].[AssocDelinquent] DROP CONSTRAINT [CK_AssocDelinquent_AssocDelinquentExpirationDate]
GO
ALTER TABLE [dbo].[AssocDelinquent] DROP CONSTRAINT [CK_AssocDelinquent_ZeroPoints]
GO
ALTER TABLE [dbo].[AssocDelinquent] DROP CONSTRAINT [DF_AssocDelinquent_ZeroPoints]
GO
ALTER TABLE [dbo].[AssocDelinquent] DROP CONSTRAINT [DF_AssocDelinquent_AssocDelinquentEffectiveDate]
GO
ALTER TABLE [dbo].[AssocDelinquent] DROP CONSTRAINT [DF_AssocDelinquent_AssocDelinquentExpirationDate]
GO
DROP TABLE [dbo].[AssocDelinquent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssocDelinquent](
	[AssocNum] [varchar](4) NOT NULL,
	[DelinquentCycles] [int] NOT NULL,
	[ZeroPoints] [varchar](1) NOT NULL,
	[AssocDelinquentEffectiveDate] [datetime] NOT NULL,
	[AssocDelinquentExpirationDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AssocDelinquent] PRIMARY KEY CLUSTERED 
(
	[AssocNum] ASC,
	[AssocDelinquentEffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AssocDelinquent]  WITH NOCHECK ADD  CONSTRAINT [FK_AssocDelinquent_Assoc] FOREIGN KEY([AssocNum])
REFERENCES [dbo].[Assoc] ([AssocNum])
GO
ALTER TABLE [dbo].[AssocDelinquent] CHECK CONSTRAINT [FK_AssocDelinquent_Assoc]
GO
ALTER TABLE [dbo].[AssocDelinquent]  WITH CHECK ADD  CONSTRAINT [CK_AssocDelinquent_AssocDelinquentExpirationDate] CHECK  (([AssocDelinquentExpirationDate] > [AssocDelinquentEffectiveDate]))
GO
ALTER TABLE [dbo].[AssocDelinquent] CHECK CONSTRAINT [CK_AssocDelinquent_AssocDelinquentExpirationDate]
GO
ALTER TABLE [dbo].[AssocDelinquent]  WITH CHECK ADD  CONSTRAINT [CK_AssocDelinquent_ZeroPoints] CHECK  (([ZeroPoints] = 'N' or [ZeroPoints] = 'Y'))
GO
ALTER TABLE [dbo].[AssocDelinquent] CHECK CONSTRAINT [CK_AssocDelinquent_ZeroPoints]
GO
ALTER TABLE [dbo].[AssocDelinquent] ADD  CONSTRAINT [DF_AssocDelinquent_ZeroPoints]  DEFAULT ('N') FOR [ZeroPoints]
GO
ALTER TABLE [dbo].[AssocDelinquent] ADD  CONSTRAINT [DF_AssocDelinquent_AssocDelinquentEffectiveDate]  DEFAULT (getdate()) FOR [AssocDelinquentEffectiveDate]
GO
ALTER TABLE [dbo].[AssocDelinquent] ADD  CONSTRAINT [DF_AssocDelinquent_AssocDelinquentExpirationDate]  DEFAULT ('12/31/9999 23:59:59') FOR [AssocDelinquentExpirationDate]
GO
