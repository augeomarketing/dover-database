USE [NewTNB]
GO
/****** Object:  Table [dbo].[History_Stage]    Script Date: 02/09/2010 08:17:57 ******/
DROP TABLE [dbo].[History_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[History_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [int] NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [numeric](18, 2) NULL,
	[Overage] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
