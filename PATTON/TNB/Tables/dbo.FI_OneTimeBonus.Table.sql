IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_FI_OneTimeBonus_TranType]') AND type = 'F')
ALTER TABLE [dbo].[FI_OneTimeBonus] DROP CONSTRAINT [FK_FI_OneTimeBonus_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FI_OneTimeBonus]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[FI_OneTimeBonus]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FI_OneTimeBonus](
	[TipFirst] [varchar](3) NOT NULL,
	[TranCode] [varchar](2) NOT NULL,
	[OneTimeBonusStartDate] [datetime] NOT NULL,
	[OneTimeBonusExpirationDate] [datetime] NOT NULL CONSTRAINT [DF_FI_OneTimeBonus_OneTimeBonusExpirationDate]  DEFAULT ('12/31/9999'),
	[BonusPointAward] [int] NOT NULL CONSTRAINT [DF_FI_OneTimeBonus_BonusPointAward]  DEFAULT (0),
	[DateEntered] [datetime] NOT NULL CONSTRAINT [DF_FI_OneTimeBonus_DateEntered]  DEFAULT (getdate()),
	[DateLastModified] [datetime] NOT NULL CONSTRAINT [DF_FI_OneTimeBonus_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_FI_OneTimeBonus] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC,
	[TranCode] ASC,
	[OneTimeBonusStartDate] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[tru_FI_OneTimeBonus]
   ON  [dbo].[FI_OneTimeBonus]
   AFTER update
AS 
BEGIN
	SET NOCOUNT ON;

	update fi
		set DateLastModified = getdate()
	from inserted ins join dbo.FI_OneTimeBonus fi
		on ins.tipfirst = fi.tipfirst
		and ins.trancode = fi.trancode
		and ins.onetimebonusstartdate = fi.onetimebonusstartdate

END

GO
ALTER TABLE [dbo].[FI_OneTimeBonus]  WITH NOCHECK ADD  CONSTRAINT [FK_FI_OneTimeBonus_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[FI_OneTimeBonus] CHECK CONSTRAINT [FK_FI_OneTimeBonus_TranType]
