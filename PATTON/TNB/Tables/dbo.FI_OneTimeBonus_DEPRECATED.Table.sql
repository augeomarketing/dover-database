USE [NewTNB]
GO
/****** Object:  Table [dbo].[FI_OneTimeBonus_DEPRECATED]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] DROP CONSTRAINT [FK_FI_OneTimeBonus_TranType]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] DROP CONSTRAINT [DF_FI_OneTimeBonus_OneTimeBonusExpirationDate]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] DROP CONSTRAINT [DF_FI_OneTimeBonus_BonusPointAward]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] DROP CONSTRAINT [DF_FI_OneTimeBonus_DateEntered]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] DROP CONSTRAINT [DF_FI_OneTimeBonus_DateLastModified]
GO
DROP TABLE [dbo].[FI_OneTimeBonus_DEPRECATED]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FI_OneTimeBonus_DEPRECATED](
	[TipFirst] [varchar](3) NOT NULL,
	[TranCode] [varchar](2) NOT NULL,
	[OneTimeBonusStartDate] [datetime] NOT NULL,
	[OneTimeBonusExpirationDate] [datetime] NOT NULL,
	[BonusPointAward] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
	[DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_FI_OneTimeBonus] PRIMARY KEY CLUSTERED 
(
	[TipFirst] ASC,
	[TranCode] ASC,
	[OneTimeBonusStartDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED]  WITH NOCHECK ADD  CONSTRAINT [FK_FI_OneTimeBonus_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] CHECK CONSTRAINT [FK_FI_OneTimeBonus_TranType]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] ADD  CONSTRAINT [DF_FI_OneTimeBonus_OneTimeBonusExpirationDate]  DEFAULT ('12/31/9999') FOR [OneTimeBonusExpirationDate]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] ADD  CONSTRAINT [DF_FI_OneTimeBonus_BonusPointAward]  DEFAULT (0) FOR [BonusPointAward]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] ADD  CONSTRAINT [DF_FI_OneTimeBonus_DateEntered]  DEFAULT (getdate()) FOR [DateEntered]
GO
ALTER TABLE [dbo].[FI_OneTimeBonus_DEPRECATED] ADD  CONSTRAINT [DF_FI_OneTimeBonus_DateLastModified]  DEFAULT (getdate()) FOR [DateLastModified]
GO
