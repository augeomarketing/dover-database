USE [NewTNB]
GO
/****** Object:  Table [dbo].[R2UTPWindDown_CardXref]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[R2UTPWindDown_CardXref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[R2UTPWindDown_CardXref](
	[TipNumber] [varchar](15) NULL,
	[CCAcctNbr] [varchar](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
