USE [NewTNB]
GO
/****** Object:  Table [dbo].[StatementRun]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [CK_StatementRun_ExcludeNoActivity]
GO
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [CK_StatementRun_ExcludeZeroBalances]
GO
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_ExcludeZeroBalances]
GO
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_ExcludeNoActivity]
GO
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_StatementRunComplete]
GO
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_DateAdded]
GO
ALTER TABLE [dbo].[StatementRun] DROP CONSTRAINT [DF_StatementRun_UpdateDate]
GO
DROP TABLE [dbo].[StatementRun]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatementRun](
	[sid_StatementRun_id] [int] IDENTITY(1,1) NOT NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[StatementStartDate] [datetime] NOT NULL,
	[StatementEndDate] [datetime] NOT NULL,
	[ExcludeZeroBalances] [varchar](1) NULL,
	[ExcludeNoActivity] [varchar](1) NULL,
	[StatementRunComplete] [varchar](1) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_StatementRun] PRIMARY KEY CLUSTERED 
(
	[sid_StatementRun_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[StatementRun]  WITH CHECK ADD  CONSTRAINT [CK_StatementRun_ExcludeNoActivity] CHECK  (([ExcludeNoActivity] = 'N' or [ExcludeNoActivity] = 'Y'))
GO
ALTER TABLE [dbo].[StatementRun] CHECK CONSTRAINT [CK_StatementRun_ExcludeNoActivity]
GO
ALTER TABLE [dbo].[StatementRun]  WITH CHECK ADD  CONSTRAINT [CK_StatementRun_ExcludeZeroBalances] CHECK  (([ExcludeZeroBalances] = 'N' or [ExcludeZeroBalances] = 'Y'))
GO
ALTER TABLE [dbo].[StatementRun] CHECK CONSTRAINT [CK_StatementRun_ExcludeZeroBalances]
GO
ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_ExcludeZeroBalances]  DEFAULT ('N') FOR [ExcludeZeroBalances]
GO
ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_ExcludeNoActivity]  DEFAULT ('N') FOR [ExcludeNoActivity]
GO
ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_StatementRunComplete]  DEFAULT ('N') FOR [StatementRunComplete]
GO
ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[StatementRun] ADD  CONSTRAINT [DF_StatementRun_UpdateDate]  DEFAULT (getdate()) FOR [UpdateDate]
GO
