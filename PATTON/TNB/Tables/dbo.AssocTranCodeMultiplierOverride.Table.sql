USE [NewTNB]
GO
/****** Object:  Table [dbo].[AssocTranCodeMultiplierOverride]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[AssocTranCodeMultiplierOverride] DROP CONSTRAINT [FK_AssocTranCodeMultiplierOverride_Assoc]
GO
ALTER TABLE [dbo].[AssocTranCodeMultiplierOverride] DROP CONSTRAINT [DF_AssocTranCodeMultiplierOverride_TransactionMultiplier]
GO
DROP TABLE [dbo].[AssocTranCodeMultiplierOverride]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssocTranCodeMultiplierOverride](
	[AssocNbr] [varchar](4) NOT NULL,
	[TranCode] [varchar](2) NOT NULL,
	[TransactionMultiplier] [float] NOT NULL,
 CONSTRAINT [PK_AssocTranCodeMultiplierOverride] PRIMARY KEY CLUSTERED 
(
	[AssocNbr] ASC,
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AssocTranCodeMultiplierOverride]  WITH NOCHECK ADD  CONSTRAINT [FK_AssocTranCodeMultiplierOverride_Assoc] FOREIGN KEY([AssocNbr])
REFERENCES [dbo].[Assoc] ([AssocNum])
GO
ALTER TABLE [dbo].[AssocTranCodeMultiplierOverride] CHECK CONSTRAINT [FK_AssocTranCodeMultiplierOverride_Assoc]
GO
ALTER TABLE [dbo].[AssocTranCodeMultiplierOverride] ADD  CONSTRAINT [DF_AssocTranCodeMultiplierOverride_TransactionMultiplier]  DEFAULT (1) FOR [TransactionMultiplier]
GO
