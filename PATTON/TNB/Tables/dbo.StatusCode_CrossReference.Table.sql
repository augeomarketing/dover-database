USE [NewTNB]
GO
/****** Object:  Table [dbo].[StatusCode_CrossReference]    Script Date: 02/09/2010 08:17:58 ******/
ALTER TABLE [dbo].[StatusCode_CrossReference] DROP CONSTRAINT [DF_StatusCode_CrossReference_LastUpdateDt]
GO
DROP TABLE [dbo].[StatusCode_CrossReference]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StatusCode_CrossReference](
	[sid_StatusCode_CrossReference_id] [bigint] IDENTITY(1,1) NOT NULL,
	[StatusCodeType] [varchar](1) NOT NULL,
	[TNBStatusCd] [varchar](1) NOT NULL,
	[RNStatusCd] [varchar](1) NOT NULL,
	[LastUpdateDt] [datetime] NOT NULL,
 CONSTRAINT [PK_StatusCode_CrossReference] PRIMARY KEY CLUSTERED 
(
	[sid_StatusCode_CrossReference_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[StatusCode_CrossReference] ADD  CONSTRAINT [DF_StatusCode_CrossReference_LastUpdateDt]  DEFAULT (getdate()) FOR [LastUpdateDt]
GO
