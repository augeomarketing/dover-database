USE [NewTNB]
GO
/****** Object:  Table [dbo].[wrkWelcomeKits]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[wrkWelcomeKits]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkWelcomeKits](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
