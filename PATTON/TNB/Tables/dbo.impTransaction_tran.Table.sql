USE [NewTNB]
GO
/****** Object:  Table [dbo].[impTransaction_tran]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impTransaction_tran] DROP CONSTRAINT [DF_impTransaction_tran_TransactionAmt]
GO
DROP TABLE [dbo].[impTransaction_tran]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impTransaction_tran](
	[sid_impTransaction_Tran_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NOT NULL,
	[System] [varchar](4) NOT NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NOT NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [varchar](10) NOT NULL,
	[AccountSts] [varchar](1) NOT NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[Misc3] [varchar](7) NULL,
 CONSTRAINT [PK__impTransaction_t__0BC6C43E] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_Tran_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impTransaction_tran] ADD  CONSTRAINT [DF_impTransaction_tran_TransactionAmt]  DEFAULT (0.00) FOR [TransactionAmt]
GO
