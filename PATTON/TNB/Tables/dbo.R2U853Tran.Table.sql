USE [NewTNB]
GO
/****** Object:  Table [dbo].[R2U853Tran]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[R2U853Tran]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[R2U853Tran](
	[AssocNbr] [varchar](3) NULL,
	[System] [varchar](4) NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [varchar](10) NULL,
	[AccountSts] [varchar](1) NULL,
	[CUNum] [varchar](4) NULL,
	[SIC] [varchar](4) NULL,
	[Ignore] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
