USE [NewTNB]
GO
/****** Object:  Table [dbo].[wrk_TipsMissing200903]    Script Date: 02/09/2010 08:17:58 ******/
DROP TABLE [dbo].[wrk_TipsMissing200903]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrk_TipsMissing200903](
	[tipnumber] [char](20) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
