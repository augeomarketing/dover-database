USE [NewTNB]
GO
/****** Object:  Table [dbo].[impTransaction_R2UTran]    Script Date: 02/09/2010 08:17:57 ******/
ALTER TABLE [dbo].[impTransaction_R2UTran] DROP CONSTRAINT [DF_impTransaction_R2UTran_TransactionAmt]
GO
DROP TABLE [dbo].[impTransaction_R2UTran]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impTransaction_R2UTran](
	[sid_impTransaction_R2UTran_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NOT NULL,
	[System] [varchar](4) NOT NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NOT NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [varchar](10) NOT NULL,
	[AccountSts] [varchar](1) NOT NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[MerchantSICCode] [varchar](4) NULL,
	[Column 10] [varchar](2) NULL,
 CONSTRAINT [PK__impTransaction_R__0425A276] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_R2UTran_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impTransaction_R2UTran] ADD  CONSTRAINT [DF_impTransaction_R2UTran_TransactionAmt]  DEFAULT (0.00) FOR [TransactionAmt]
GO
