USE [605ActorsFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImport]    Script Date: 01/09/2014 10:05:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetupTransactionDataForImport]
GO

USE [605ActorsFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImport]    Script Date: 01/09/2014 10:05:45 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[pSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 7/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* Changed logic to group by tip and pan */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return  and omit 500000 transactioins*/

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 05/2012   */
/* REVISION: 3 */
/* SCAN: SEB003 */
/* Change  Set tip to null and add date criteria  */

/*                                        */
-- S Blanchette  7/2013  use RNITransaction view

truncate table transwork
truncate table transstandard

select *
into #tempTrans
from Rewardsnow.dbo.vw_605_TRAN_SOURCE_100 with (nolock) /* CWH - If view is missing, regenerate it */
where Trandate<=cast(@EndDate as DATE) /* SEB005 */


/* CWH */
insert into transwork(TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID)
select TRANDATE, MSGTYPE, PAN, left(PROCESSINGCODE,6), right(AMTTRAN,12), SIC, NETID, 0, 1, TERMID, ACCEPTORID
from #tempTrans
where 
	sic<>'6011' 
	and 
	(
		(
			processingcode in ('000000', '002000', '200020', '200040') 
				and (left(msgtype,2) in ('02', '04'))
		)
			OR left(PROCESSINGCODE,5) in ('00500', '02500')
	)

--SEB 9/13 in above section changed ('005000', '025000') to ('00500', '02500')

drop table #temptrans

update TRANSWORK
set TIPNUMBER = afs.TIPNUMBER
from TRANSWORK tw join AFFILIAT afs on tw.PAN = afs.ACCTID

delete from TRANSWORK
where TIPNUMBER is null


-- Signature Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid in('MCI', 'VNT') 

-- PIN Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid not in('MCI', 'VNT', 'CCC') 

/*CWH*/
-- Credit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid in('CCC') 

--Put to standard transtaction file format purchases.
--SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, '67', NumberOfTrans, points, 'DEBIT', '1', ' ' from transwork
--where processingcode in ('000000', '002000', '500000', '500020', '500010') 		        	


--CWH
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' from transwork
where processingcode in ('00500')
group by tipnumber, Pan


-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' from transwork
/* SEB002 where processingcode in ('000000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02', '04')) */		        	
/* SEB002 */ where processingcode in ('000000', '002000') and left(msgtype,2) in ('02') 		        	
group by tipnumber, Pan
	
--Put to standard transtaction file format returns.
--SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, '37', NumberOfTrans, points, 'DEBIT', '-1', ' ' from transwork
--where processingcode in ('200020', '200040')

-- CWH
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' from transwork
where processingcode in ('02500')
group by tipnumber, Pan

-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' from transwork
/* SEB002 where processingcode in ('200020', '200040') */
where processingcode in ('200020', '200040') or left(msgtype,2) in ('04')
group by tipnumber, Pan

delete from Transstandard
where TRANAMT='0'


GO


