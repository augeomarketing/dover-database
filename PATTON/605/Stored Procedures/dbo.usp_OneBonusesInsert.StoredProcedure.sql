USE [605ActorsFCUConsumer]
GO
/****** Object:  StoredProcedure [dbo].[usp_OneBonusesInsert]    Script Date: 03/30/2012 10:02:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OneBonusesInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_OneBonusesInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OneBonusesInsert]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_OneBonusesInsert]
AS

INSERT INTO OneTimeBonuses(tipnumber, trancode)
SELECT s.tipnumber, ''BE'' -- SEB001 3/2012 Change BT to BE
FROM RN1.COOP.dbo.[1security] s
LEFT OUTER JOIN OneTimeBonuses b
ON s.tipnumber = b.TipNumber
WHERE s.EmailStatement = ''Y'' AND s.tipnumber like ''605%''
	AND b.TipNumber IS NULL
' 
END
GO
