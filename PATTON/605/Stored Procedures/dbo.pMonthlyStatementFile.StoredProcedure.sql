USE [605ActorsFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[pMonthlyStatementFile]    Script Date: 07/17/2017 08:42:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMonthlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pMonthlyStatementFile]
GO

USE [605ActorsFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[pMonthlyStatementFile]    Script Date: 07/17/2017 08:42:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[pMonthlyStatementFile] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3)
--CREATE PROCEDURE pMonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10), @Tipfirst char(3), @Segment char(1)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/
/*******************************************************************************/
/* SEB001 7/07 Added check for acctstatus when getting gard number
*/
/*******************************************************************************/
/*******************************************************************************/
/* SEB002 8/07 Added logic to put bonuses in appropriate column */
/*******************************************************************************/

/*******************************************************************************/
/* SEB003 9/07 Added logic to capture FA  */
/*******************************************************************************/

/*******************************************************************************/
/* SEB004 11/10 Added logic to capture XP  */
/*******************************************************************************/

/*******************************************************************************/
/* SEB005 9/13 Change to Acctnum  */
/*******************************************************************************/


Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--set @DBName='zz617OmniCCUConsumer'

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Monthly_Statement_File '
exec sp_executesql @SQLTruncate


if exists(select name from sysobjects where name='wrkhist')
begin 
	truncate table wrkhist
end

set @SQLSelect='insert into wrkhist
		select tipnumber, trancode, sum(points) as points from ' + QuoteName(@DBName) + N'.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode '
Exec sp_executesql @SQLSelect, N'@StartDate DateTime, @EndDate DateTime', @StartDate=@StartDate, @EndDate=@EndDate 

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), left(zipcode,5) 
				from ' + QuoteName(@DBName) + N'.dbo.customer '
Exec sp_executesql @SQLInsert

IF @Tipfirst='608' or @Tipfirst='611' or @Tipfirst='615' or @Tipfirst='213'
Begin
	set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File 
		set acctnum = rtrim(b.MISC1) 
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.customer b 
		where a.tipnumber = b.tipnumber '
	Exec sp_executesql @SQLUpdate
END
ELSE
	BEGIN
		set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File 
			set acctnum = rtrim(b.acctid) 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.affiliat b 
			where a.tipnumber = b.tipnumber and b.acctstatus=''A'' '
		Exec sp_executesql @SQLUpdate
	END

set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set lastfour=right(rtrim(acctnum),4) '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   63       */

set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchasedcr=
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''63'' ' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchaseddb=
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''67'' ' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with bonuses            */
/* SEB002 */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonuscr=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and (trancode=''FJ'' )) 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and (trancode = ''FJ'')) '
	Exec sp_executesql @SQLUpdate 

/* SEB002 */
SET	@SQLUpdate	= N'
			UPDATE	' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusDB =
																	(select	sum(points) 
																	 from	wrkhist 
																	 where	tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
																	  and	(
																			(trancode like ''B%'' and trancode <> ''BX'')
																			or	trancode= ''NW'' 
																			or	(trancode like ''[FGH]%'' and trancode NOT IN(''FJ'',''G9'',''H9'') ))
																	) 	
			WHERE	exists	(select *
							 from wrkhist 
							 where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
								and (
									(trancode like ''B%'' and trancode <> ''BX'') 
									or trancode= ''NW'' 
									or (trancode like ''[FGH]%'' and trancode NOT IN(''FJ'',''G9'',''H9'') ))
							) 
					'
EXEC	sp_executesql @SQLUpdate 

SET	@SQLUpdate	=	N'
			UPDATE ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusDB = pointsbonusDB -
																	(select	sum(points) 
																	 from	wrkhist 
																	 where	tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
																		and (trancode in (''BX'',''G9'',''H9'' ))
																	)	 	
			WHERE	exists	(select	* 
							 from	wrkhist 
							 where	tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
							 and	(trancode in (''BX'',''G9'',''H9'' ))
							)
						'
						
EXEC sp_executesql @SQLUpdate 


/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded=
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''IE'' ' 	
	Exec sp_executesql @SQLUpdate 

set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded = pointsadded +
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''TR'' ' 	
	Exec sp_executesql @SQLUpdate 

--Transfers
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded = pointsadded +
		isnull(b.points,0)
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''TP'' ' 	
	Exec sp_executesql @SQLUpdate 


--Purchased Points
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded = pointsadded +
		isnull(b.points,0)
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''PP'' ' 	
	Exec sp_executesql @SQLUpdate 



/* Load the statmement file with total point increases */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded '
	Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and ( trancode like ''R%'' or  trancode = ''IR'')   ) 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and (trancode like ''R%''  or  trancode = ''IR'')  ) '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=pointsredeemed-
		b.points 
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''DR'' ' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturnedcr=
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''33'' ' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturneddb=
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''37'' ' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with minus adjustments   */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointssubtracted=
		b.points
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''DE'' ' 	
	Exec sp_executesql @SQLUpdate 

/*********************************/
/* start seb004                  */
/*********************************/

	/* Load the statmement file with minus adjustments  XP */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointssubtracted= pointssubtracted +
			b.points
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''XP'' ' 	
		Exec sp_executesql @SQLUpdate 
/*********************************/
/* end seb004                  */
/*********************************/


/* Load the statmement file with total point decreases */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbegin = b.' + Quotename(@MonthBucket) + N' 
		from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table b
		where a.tipnumber = b.tipnumber '
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased '
exec sp_executesql @SQLUpdate


GO


