USE [605ActorsFCUConsumer]
GO

/****** Object:  StoredProcedure [dbo].[pImportCustomer_StageData]    Script Date: 02/10/2011 12:29:25 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spImportCustomerData_stage] @TipFirst CHAR(3)
AS 

IF @TipFirst <> '605'
	BEGIN
		RAISERROR('Procedure Restricted to 605ActorsFCUConsumer', 20, 1)
		RETURN -1
	END
 
--DEDUPLICATE CUSTIN BY ACCOUNT AND TIPNUMBER

DECLARE @Order BIGINT

SET @Order = 0
UPDATE CUSTIN 
	SET @Order = @Order + 1
		, MISC3 = @Order

DELETE ci
FROM custin ci
LEFT OUTER JOIN
(
	SELECT acct_num, tipnumber, MIN(misc3) AS misc3 FROM CUSTIN
	GROUP BY ACCT_NUM, TIPNUMBER
) u
ON u.TIPNUMBER = ci.TIPNUMBER
	AND u.TIPNUMBER = ci.TIPNUMBER
	AND u.MISC3 = ci.misc3
WHERE u.MISC3 IS NULL 

--reset misc3
UPDATE CUSTIN SET MISC3 = NULL
	
--Handle malformed fields or those not matching between debit and credit processes
UPDATE ci
	SET MISC1 = good.MISC1
FROM custin ci
INNER JOIN
(SELECT DISTINCT tipnumber, misc1 FROM CUSTIN WHERE rewardsnow.dbo.ufn_CharIsNullOrEmpty(misc1) = 0) good
ON good.TIPNUMBER= ci.TIPNUMBER

UPDATE ci
SET HOMEPHONE = good.homephone
FROM custin ci
INNER JOIN 
(SELECT DISTINCT tipnumber, ISNULL(homephone, '') AS homephone FROM CUSTIN WHERE HOMEPHONE NOT LIKE '%-%') good
ON good.TIPNUMBER = ci.TIPNUMBER

UPDATE ci
SET WORKPHONE = good.workphone
FROM custin ci
INNER JOIN 
(SELECT DISTINCT tipnumber, workphone FROM CUSTIN WHERE rewardsnow.dbo.ufn_CharIsNullOrEmpty(misc1) = 0) good
ON good.TIPNUMBER = ci.TIPNUMBER

--COORDINATE VALUES BETWEEN DEBIT AND CREDIT
DECLARE @fv TABLE
(
	myid BIGINT IDENTITY(1,1)
	, tipnumber NVARCHAR(16)
	, nameacct1 NVARCHAR(40) 
	, nameacct2 NVARCHAR(40) 
	, nameacct3 NVARCHAR(40) 
	, nameacct4 NVARCHAR(40) 
	, nameacct5 NVARCHAR(40) 
	, nameacct6 NVARCHAR(40) 
	, lastname NVARCHAR(40) 
	, address1 NVARCHAR(40) 
	, address2 NVARCHAR(40) 
	, address4 NVARCHAR(40) 
	, city NVARCHAR(38) 
	, [STATE] CHAR(2) 
	, zip NVARCHAR(15) 
	, homephone NVARCHAR(10) 
	, workphone NVARCHAR(10) 
)
	
INSERT @fv
	(tipnumber, address1, address2, address4, city, homephone, [STATE], zip, nameacct1, nameacct2, nameacct3, nameacct4, nameacct5, nameacct6, workphone)
SELECT
	tipnumber, address1, address2, address4, city, homephone, [STATE], zip, nameacct1, nameacct2, nameacct3, nameacct4, nameacct5, nameacct6, workphone
FROM
	CUSTIN
ORDER BY DATEADDED DESC, MISC3 DESC

--apply values based on first entry in temp table (which is last added)
UPDATE ci
SET ADDRESS1 = fv.address1
	, ADDRESS2 = fv.address2
	, ADDRESS4 = fv.address4
	, CITY = fv.city
	, [STATE] = fv.[state]
	, ZIP = fv.zip
	, HOMEPHONE = fv.homephone
	, nameacct1 = fv.nameacct1
	, nameacct2 = fv.nameacct2
	, nameacct3 = fv.nameacct3
	, nameacct4 = fv.nameacct4
	, nameacct5 = fv.nameacct5
	, nameacct6 = fv.nameacct6
	, workphone = fv.workphone
FROM custin ci
INNER JOIN @fv fv ON ci.TIPNUMBER = fv.tipnumber
INNER JOIN 
	(SELECT MIN(myid) AS myid, tipnumber FROM @fv GROUP BY tipnumber) src
ON fv.myid = src.myid
 
MERGE Customer_Stage AS TARGET
USING (
	SELECT DISTINCT 
		ISNULL(NAMEACCT1, '') AS nameacct1
		, ISNULL(NAMEACCT2, '') AS nameacct2
		, ISNULL(NAMEACCT3, '') AS nameacct3
		, ISNULL(NAMEACCT4, '') AS nameacct4
		, ISNULL(NAMEACCT5, '') AS nameacct5
		, ISNULL(NAMEACCT6, '') AS nameacct6
		, ISNULL(TIPNUMBER, '') AS tipnumber
		, ISNULL(ADDRESS1, '') AS address1
		, ISNULL(ADDRESS2, '') AS address2
		, ISNULL(ADDRESS4, '') AS address4		
		, ISNULL(CITY, '') AS city
		, ISNULL(STATE, '') AS STATE
		, ISNULL(ZIP, '') AS zip
		, ISNULL(LASTNAME, '') AS lastname
		, ISNULL(HOMEPHONE, '') AS homephone
		, ISNULL(WORKPHONE, '') AS workphone
		, ISNULL(MISC1, '') AS misc1
		, ISNULL(MISC2, '') AS misc2
		, ISNULL(MISC3, '') AS misc3
	FROM CUSTIN WHERE Rewardsnow.dbo.ufn_CharIsNullOrEmpty(TIPNUMBER) = 0) AS SOURCE
ON (TARGET.TIPNUMBER = SOURCE.TIPNUMBER)
WHEN MATCHED AND 
	(
		ISNULL(TARGET.ACCTNAME1, '')<>SOURCE.NAMEACCT1 
		OR ISNULL(TARGET.AcctName2, '')<>SOURCE.NAMEACCT2 
		OR ISNULL(TARGET.AcctName3, '')<>SOURCE.NAMEACCT3
		OR ISNULL(TARGET.AcctName4, '')<>SOURCE.NAMEACCT4
		OR ISNULL(TARGET.AcctName5, '')<>SOURCE.NAMEACCT5
		OR ISNULL(TARGET.AcctName6, '')<>SOURCE.NAMEACCT6
		OR ISNULL(TARGET.Address1, '')<>SOURCE.Address1
		OR ISNULL(TARGET.Address2, '')<>SOURCE.Address2
		OR ISNULL(TARGET.Address4, '')<>SOURCE.Address4
		OR RewardsNow.dbo.ufn_Trim(ISNULL(TARGET.City, ''))<>RewardsNow.dbo.ufn_Trim(SOURCE.City)
		OR RewardsNow.dbo.ufn_Trim(ISNULL(TARGET.State, ''))<>RewardsNow.dbo.ufn_Trim(SOURCE.STATE)
		OR RewardsNow.dbo.ufn_Trim(ISNULL(TARGET.Zipcode, ''))<>RewardsNow.dbo.ufn_Trim(SOURCE.zip)
		OR ISNULL(TARGET.HomePhone, '')<>SOURCE.HomePhone
		OR ISNULL(TARGET.WorkPhone, '')<>SOURCE.WorkPhone
		OR ISNULL(TARGET.lastname, '')<>SOURCE.lastname
		OR ISNULL(TARGET.misc2, '')<>SOURCE.misc2
		OR ISNULL(TARGET.misc3, '')<>SOURCE.misc3
	)
THEN
UPDATE
	SET TARGET.ACCTNAME1 = SOURCE.NAMEACCT1
		, TARGET.ACCTNAME2 = SOURCE.NAMEACCT2
		, TARGET.AcctName3 = SOURCE.NAMEACCT3
		, TARGET.AcctName4 = SOURCE.NAMEACCT4
		, TARGET.AcctName5 = SOURCE.NAMEACCT5
		, TARGET.AcctName6 = SOURCE.NAMEACCT6
		, TARGET.Address1 = SOURCE.Address1
		, TARGET.Address2 = SOURCE.Address2
		, TARGET.Address4 = SOURCE.Address4
		, TARGET.City = RewardsNow.dbo.ufn_Trim(SOURCE.City)
		, TARGET.State = RewardsNow.dbo.ufn_Trim(SOURCE.STATE)
		, TARGET.Zipcode = RewardsNow.dbo.ufn_Trim(SOURCE.zip)
		, TARGET.HomePhone = SOURCE.HomePhone
		, TARGET.WorkPhone = SOURCE.WorkPhone
		, TARGET.lastname = SOURCE.lastname
		, TARGET.misc2 = SOURCE.misc2
		, TARGET.misc3 = SOURCE.misc3
WHEN NOT MATCHED BY TARGET THEN
	INSERT (
		TIPNUMBER
		, RunAvailable
		, RunBalance
		, RunRedeemed
		, TipFirst
		, TipLast
		, AcctName1
		, AcctName2
		, AcctName3
		, AcctName4
		, AcctName5
		, AcctName6
		, Address1
		, Address2
		, Address4
		, City
		, [STATE]
		, Zipcode
		, [Status]
		, HomePhone
		, WorkPhone
		, DateAdded
		, Lastname
		, Misc1
		, Misc2
		, Misc3
	)	
	VALUES
	(
		SOURCE.tipnumber
		, '0'
		, '0'
		, '0'
		, LEFT(SOURCE.tipnumber,3)
		, RIGHT(SOURCE.tipnumber,12)
		, SOURCE.NAMEACCT1
		, SOURCE.NAMEACCT2
		, SOURCE.NAMEACCT3
		, SOURCE.NAMEACCT4
		, SOURCE.NAMEACCT5
		, SOURCE.NAMEACCT6
		, SOURCE.ADDRESS1
		, SOURCE.ADDRESS2
		, SOURCE.ADDRESS4
		, RewardsNow.dbo.ufn_Trim(SOURCE.City)
		, RewardsNow.dbo.ufn_Trim(SOURCE.state)
		, RewardsNow.dbo.ufn_Trim(SOURCE.zip)
		, 'A'
		, SOURCE.HomePhone
		, SOURCE.WorkPhone
		, cast(rewardsnow.dbo.ufn_getlastofprevmonth(GETDATE()) as date)
		, SOURCE.LastName
		, SOURCE.Misc1
		, SOURCE.Misc2
		, SOURCE.Misc3
	);

--Affiliat_Stage table

--Fix misaligned tip numbers:
UPDATE ci
SET TIPNUMBER = badtips.oldertip
FROM custin ci
INNER JOIN
	(
		SELECT a.ACCT_NUM, a.TIPNUMBER newertip, b.TIPNUMBER oldertip
		FROM CUSTIN a INNER JOIN CUSTIN b ON a.ACCT_NUM = b.ACCT_NUM AND a.TIPNUMBER > b.TIPNUMBER
	) badtips
ON ci.TIPNUMBER = badtips.newertip

UPDATE ci
SET LASTNAME = aff.LastName
FROM CUSTIN ci
INNER JOIN Affiliat_Stage aff
ON aff.TIPNUMBER = ci.TIPNUMBER
INNER JOIN
(
	SELECT acct_num, tipnumber FROM CUSTIN
	GROUP BY ACCT_NUM, TIPNUMBER
	HAVING COUNT(*) > 1
) d
ON ci.ACCT_NUM = d.ACCT_NUM
AND ci.TIPNUMBER = d.TIPNUMBER

MERGE Affiliat_Stage AS TARGET
USING 
(
	SELECT DISTINCT 
		ACCT_NUM
		, STATUS
		, TIPNUMBER
		, ISNULL(LASTNAME, '') AS LASTNAME
		, ISNULL(SEGCODE, '') AS SEGCODE
	FROM CUSTIN 
	WHERE RewardsNow.dbo.ufn_CharIsNullOrEmpty(ACCT_NUM) = 0
) AS SOURCE

ON (TARGET.ACCTID = SOURCE.ACCT_NUM)
WHEN MATCHED AND 
	(
		TARGET.lastname <> SOURCE.lastname
		OR TARGET.acctstatus <> SOURCE.STATUS
	)
THEN
UPDATE
	SET TARGET.lastname = SOURCE.lastname
		, TARGET.ACCTSTATUS = SOURCE.STATUS

WHEN NOT MATCHED BY TARGET THEN
INSERT 
(
	ACCTID
	, TIPNUMBER
	, ACCTTYPE
	, DATEADDED
	, ACCTSTATUS
	, ACCTTYPEDESC
	, LASTNAME
	, YTDEARNED
)
VALUES
(
	SOURCE.ACCT_NUM
	, SOURCE.TIPNUMBER
	, CASE SOURCE.SEGCODE WHEN 'C' THEN 'Credit' ELSE 'Debit' END
	, CONVERT(VARCHAR(10), RewardsNow.dbo.ufn_GetLastOfPrevMonth(GETDATE()), 101)
	, 'A'
	, CASE SOURCE.SEGCODE WHEN 'C' THEN 'Credit Card' ELSE 'Debit Card' END
	, SOURCE.LASTNAME
	, '0'
);

--Fix for improperly flagged credit cards
UPDATE Affiliat_Stage SET AcctType = 'Credit', AcctTypeDesc = 'Credit Card'
FROM Affiliat_Stage
INNER JOIN HISTORY
ON Affiliat_Stage.ACCTID = HISTORY.ACCTID
WHERE HISTORY.TRANCODE in ('33', '63')
	and (Affiliat_Stage.AcctType <> 'Credit' OR Affiliat_Stage.AcctTypeDesc <> 'Credit Card')

--Update Status Description	
UPDATE Customer_Stage
SET StatusDescription=(SELECT statusdescription FROM status WHERE status=Customer_Stage.status)

GO
