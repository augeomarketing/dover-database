USE [605ActorsFCUConsumer]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateEStatementBonus]    Script Date: 03/30/2012 10:02:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGenerateEStatementBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGenerateEStatementBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGenerateEStatementBonus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGenerateEStatementBonus] @EndDate varchar(10)
AS 
/*************************************************************/
/*                                                           */
/*   Procedure to generate the E-Statement Bonus             */
/*                                                           */
/*                                                           */
/*************************************************************/
declare @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10)
set @Trandate=@EndDate
set @bonuspoints=''1000''
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select tipnumber
from OneTimeBonuses
where trancode=''BE'' and dateawarded is null -- SEB001 3/2012 Change BT to BE
/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber 
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		Update Customer 
		set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
		where tipnumber = @Tipnumber
		if exists(select tipnumber from customer where tipnumber=@tipnumber)
		Begin
			INSERT INTO history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        			Values(@Tipnumber, @Trandate, ''BE'', ''1'', @BonusPoints, ''1'', ''Bonus Email Statements'', ''0'') -- SEB001 3/2012 Change BT to BE
 
			update OneTimeBonuses
			set DateAwarded=@Trandate
			where current of Tip_crsr
 		End
		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
end
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr' 
END
GO
