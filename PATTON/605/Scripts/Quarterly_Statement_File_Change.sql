/*
   Monday, October 15, 201210:41:33 AM
   User: 
   Server: doolittle\rn
   Database: 605ActorsFCUConsumer
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Quarterly_Statement_File ADD
	PointsBonusMN numeric(18, 0) NULL
GO
ALTER TABLE dbo.Quarterly_Statement_File ADD CONSTRAINT
	DF_Quarterly_Statement_File_PointsBonusMN DEFAULT (0) FOR PointsBonusMN
GO
ALTER TABLE dbo.Quarterly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
