USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete from RewardsNow.dbo.[SSIS Configurations]
where ConfigurationFilter like '605%'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'605_Actors_ImportAndAudit', N'\\patton\ops\6CO\Output\ErrorFiles\_tmpMissingDDAs_SSIS.XLS', N'\Package.Connections[_tmpMissingDDAs_SSIS.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'\\patton\ops\SSIS_Packages\605Actors\605Actors\Audit.dtsx', N'\Package.Connections[Audit.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'0', N'\Package.Variables[User::ErrCnt].Properties[Value]', N'Int32' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'605', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=605ActorsFCUConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{6BC95C30-04F0-4932-A224-73EA0C7AF996}236722-SQLCLUS2\RN.605ActorsFCUConsumer;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{32C66EE0-3C4B-4A98-BE3A-848A37E7CF63}236722-SQLCLUS2\RN.COOPWork1;Auto Translate=False;', N'\Package.Connections[COOPWORK].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{8FF0A412-4CDD-4EAB-9547-D2A6184E9203}236722-SQLCLUS2\RN.RewardsNow1;Auto Translate=False;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5ff42387-e726-4648-85b1-05161d935cbc}SSIS Config;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'ExcelFilePath=\\patton\ops\6CO\Output\ErrorFiles\_tmpMissingDDAs_SSIS.XLS;FormatType=Xls;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[tmpMissingDDA_SSIS.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'S', N'\Package.Variables[User::ProductionFlag].Properties[Value]', N'String' UNION ALL
SELECT N'605_Actors_ImportAndAudit', N'STD', N'\Package.Variables[User::DateType].Properties[Value]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'605', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=605ActorsFCUConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{6BC95C30-04F0-4932-A224-73EA0C7AF996}236722-SQLCLUS2\RN.605ActorsFCUConsumer;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{32C66EE0-3C4B-4A98-BE3A-848A37E7CF63}236722-SQLCLUS2\RN.COOPWork1;Auto Translate=False;', N'\Package.Connections[COOPWORK].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{8FF0A412-4CDD-4EAB-9547-D2A6184E9203}236722-SQLCLUS2\RN.RewardsNow1;Auto Translate=False;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5ff42387-e726-4648-85b1-05161d935cbc}SSIS Config;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'P', N'\Package.Variables[User::ProductionFlag].Properties[Value]', N'String' UNION ALL
SELECT N'605_Actors_PostStageToProduction', N'STD', N'\Package.Variables[User::DateType].Properties[Value]', N'String' UNION ALL
SELECT N'605_Audit', N'\\patton\ops\6CO\Output\AuditFiles\_AuditM_tmp_SSIS.XLS', N'\Package.Connections[_AuditM_tmp_SSIS.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'\\patton\ops\6CO\Output\AuditFiles\Summary\_tmpSummaryM_SSIS.xls', N'\Package.Connections[_tmpSummaryM_SSIS.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'0', N'\Package.Variables[User::ErrCnt].Properties[Value]', N'Int32' UNION ALL
SELECT N'605_Audit', N'605', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'605_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=605ActorsFCUConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{6BC95C30-04F0-4932-A224-73EA0C7AF996}236722-SQLCLUS2\RN.605ActorsFCUConsumer;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{32C66EE0-3C4B-4A98-BE3A-848A37E7CF63}236722-SQLCLUS2\RN.COOPWork1;', N'\Package.Connections[COOPWORK].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{8FF0A412-4CDD-4EAB-9547-D2A6184E9203}236722-SQLCLUS2\RN.RewardsNow1;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5ff42387-e726-4648-85b1-05161d935cbc}SSIS Config;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'ExcelFilePath=\\patton\ops\6CO\Output\AuditFiles\_AuditM_tmp_SSIS.XLS;FormatType=Xls;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[AuditM_tmp_SSIS.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Audit', N'ExcelFilePath=\\patton\ops\6CO\Output\AuditFiles\Summary\_tmpSummaryM_SSIS.xls;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[tmpSummaryM_SSIS.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Audit', N'0', N'\Package.Variables[User::ErrCnt].Properties[Value]', N'Int32' UNION ALL
SELECT N'605_Quarterly_Audit', N'605', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'605_Quarterly_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=605ActorsFCUConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Quarterly_Audit-{D4D0745E-8BAA-4FFC-95B3-B737DF9F2666}236722-SQLCLUS2\RN.631Munising;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Quarterly_Audit-{FCE61DF1-608A-4DCA-B013-309D95ECC69E}236722-SQLCLUS2\RN.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Quarterly_Audit-{8857BC35-09CB-42E4-ABC3-8228D12D1924}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Audit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Quarterly_Audit-{F5C7E512-A13C-4B8D-A7AA-01B384AB9913}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Audit', N'ExcelFilePath=\\patton\ops\6CO\Output\QuarterlyFiles\QTRAUDITExpM_SSIS.xls;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[QTRAUDITExpM_SSIS.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Statement', N'605', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'605_Quarterly_Statement', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=605ActorsFCUConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Quarterly_Statement-{734BACE0-4362-48B1-BA20-38AE3503B527}236722-SQLCLUS2\RN.631Munising;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Statement', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Quarterly_Statement-{60943E9F-FFBF-48F7-AB47-BB5D67B206FE}236722-SQLCLUS2\RN.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Statement', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Quarterly_Statement-{395DCB57-CBC7-4A4C-941B-BE6EBAE39AB2}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Statement', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Quarterly_Statement-{CE9DF3FE-336A-4C45-9ACF-58BBB05A1F17}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'605_Quarterly_Statement', N'ExcelFilePath=\\patton\ops\6CO\Output\Statements\STATEMENTExpM.XLS;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[STATEMENTExpM.XLS].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

