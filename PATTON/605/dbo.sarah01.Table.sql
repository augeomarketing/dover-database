SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sarah01](
	[tfno] [nvarchar](15) NULL,
	[trandate] [nvarchar](10) NULL,
	[acct_num] [nvarchar](25) NULL,
	[trancode] [nvarchar](2) NULL,
	[tranamt] [nchar](15) NULL
) ON [PRIMARY]
GO
