USE [605ActorsFCUConsumer]
GO
/****** Object:  Table [dbo].[Current_Month_Activity_wrk]    Script Date: 10/15/2012 11:53:58 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_wrk_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity_wrk]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_wrk_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_wrk] DROP CONSTRAINT [DF_Current_Month_Activity_wrk_Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity_wrk]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity_wrk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity_wrk]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity_wrk](
	[Tipnumber] [nvarchar](15) NULL,
	[Points] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_wrk_Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity_wrk]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_wrk_Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_wrk] ADD  CONSTRAINT [DF_Current_Month_Activity_wrk_Points]  DEFAULT ((0)) FOR [Points]
END


End
GO
