USE Maintenance
GO

IF OBJECT_ID(N'PerlFileLoad') IS NOT NULL
	DROP TABLE PerlFileLoad
GO

CREATE TABLE dbo.PerlFileLoad
(
	sid_perlfileload_id BIGINT IDENTITY(1,1)
	, sid_dbprocessinfo_dbnumber VARCHAR(3)
	, sid_rniimportfiletype_id INT -- 0 = DETERMINE FROM FILENAME, 1 = ACCT, 2 - TRAN, 3 - APRG, 4 - TPRG, 5 - BNS (BONUS)
	, dim_perlfileload_processingdate DATE
	, dim_perlfileload_loadtype VARCHAR(10) --DELIMITED OR FIXED
	, dim_perlfileload_delimiter VARCHAR(1) --HOW TO HANDLE TAB AS A DELIMITER?
	, dim_perlfileload_trimheader INT -- 0 = NO 1 = YES
	, dim_perlfileload_trimfooter INT -- 0 = NO 1 = YES
	, dim_perlfileload_postloadcleanup INT -- 0 = NO 1 = YES
	, dim_perlfileload_preload VARCHAR(1000)
	, dim_perlfileload_postload VARCHAR(1000)	
	, dim_perlfileload_filename VARCHAR(1000)
	, dim_perlfileload_status INT NOT NULL DEFAULT(0)
)

GO
/*
INSERT INTO Maintenance.dbo.PerlFileLoad
(
	sid_dbprocessinfo_dbnumber
	, sid_rniimportfiletype_id
	, dim_perlfileload_processingdate
	, dim_perlfileload_loadtype
	, dim_perlfileload_delimiter
	, dim_perlfileload_trimheader
	, dim_perlfileload_trimfooter
	, dim_perlfileload_postloadcleanup
	, dim_perlfileload_preload
	, dim_perlfileload_postload
	, dim_perlfileload_filename
)
VALUES
(
	'250'
	, 1
	, '5/31/2012'
	, 'DELIMITED'
	, '|'
	, 0
	, 0
	, 1
	, NULL
	, NULL
	, '\\doolittle\ops\248\Input\248_ACCT_20120521_1.CSV'	
)
*/


sid_dbprocessinfo_dbnumber - REQUIRED: tipfirst (will be used in loading)
sid_rniimportfiletype_id - REQUIRED: File type that is being loaded 1 - Demographics, 2 - Transaction, 5 - Bonus
dim_perlfileload_processingdate - REQUIRED: End Date for processing
dim_perlfileload_loadtype - REQUIRED:FIXED OR DELIMITED
dim_perlfileload_delimiter - OPTIONAL:  IF DELIMITED and not filled, comma will be used
dim_perlfileload_trimheader - NOT IMPLEMENTED AT THIS TIME
dim_perlfileload_preload - NOT IMPLEMENTED AT THIS TIME
dim_perlfileload_postload - NOT IMPLEMENTED AT THIS TIME
dim_perlfileload_filename - REQUIRED: File to load
dim_perlfileload_status - ReadOnly 0 = Waiting, -1 = In Process, 1 = Loaded



