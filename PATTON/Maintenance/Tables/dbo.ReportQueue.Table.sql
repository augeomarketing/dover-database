USE [Maintenance]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[reportqueue]    Script Date: 04/02/2013 15:26:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[reportqueue]') AND type in (N'U'))
DROP TABLE [dbo].[reportqueue]
GO



CREATE TABLE [dbo].[reportqueue](
	[sid_reportqueue_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_reportqueue_tipfirst] [varchar](3) NOT NULL,
	[dim_reportqueue_startdate] [date] NOT NULL,
	[dim_reportqueue_enddate] [date] NOT NULL,
	[dim_reportqueue_email] [varchar](max) NOT NULL,
	[dim_reportqueue_created] [datetime] NOT NULL,
	[dim_reportqueue_lastmodified] [datetime] NOT NULL,
	[dim_reportqueue_runat] [datetime] NOT NULL,
	[dim_reportqueue_active] [int] NOT NULL,
	[dim_reportqueue_errorcount] [int] NOT NULL,
 CONSTRAINT [PK_reportqueue] PRIMARY KEY CLUSTERED 
(
	[sid_reportqueue_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[reportqueue] ADD  CONSTRAINT [DF_reportqueue_dim_reportqueue_email]  DEFAULT ('itops@rewardsnow.com') FOR [dim_reportqueue_email]
GO

ALTER TABLE [dbo].[reportqueue] ADD  CONSTRAINT [DF_reportqueue_dim_reportqueue_created]  DEFAULT (getdate()) FOR [dim_reportqueue_created]
GO

ALTER TABLE [dbo].[reportqueue] ADD  CONSTRAINT [DF_reportqueue_dim_reportqueue_lastmodified]  DEFAULT (getdate()) FOR [dim_reportqueue_lastmodified]
GO

ALTER TABLE [dbo].[reportqueue] ADD  CONSTRAINT [DF_reportqueue_dim_reportqueue_runat]  DEFAULT (((1)/(1))/(2000)) FOR [dim_reportqueue_runat]
GO

ALTER TABLE [dbo].[reportqueue] ADD  CONSTRAINT [DF_reportqueue_dim_reportqueue_active]  DEFAULT ((1)) FOR [dim_reportqueue_active]
GO

ALTER TABLE [dbo].[reportqueue] ADD  CONSTRAINT [DF_reportqueue_dim_reportqueue_errorcount]  DEFAULT ((-1)) FOR [dim_reportqueue_errorcount]
GO


SET ANSI_PADDING OFF
GO
