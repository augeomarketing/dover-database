USE [Maintenance]
GO

IF OBJECT_ID(N'PerlFileMove') IS NOT NULL
	DROP TABLE PerlFileMove
GO

CREATE TABLE PerlFileMove
(
	sid_perlfilemove_id BIGINT IDENTITY(1,1) PRIMARY KEY
	, dim_perlfilemove_source VARCHAR(255) NOT NULL
	, dim_perlfilemove_target VARCHAR(255) NOT NULL
	, dim_perlfilemove_archive VARCHAR(255)
	, dim_perlfilemove_sent INT NOT NULL DEFAULT(0)
	, dim_perlfilemove_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_perlfilemove_lastmodified DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_perlfilemove_lastmodifiedby VARCHAR(255)
)
GO

