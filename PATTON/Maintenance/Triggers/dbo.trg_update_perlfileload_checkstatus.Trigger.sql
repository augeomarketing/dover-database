USE [Maintenance]
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRG_UPDATE_PERLFILELOAD_CHECKSTATUS]'))
DROP TRIGGER [dbo].[TRG_UPDATE_PERLFILELOAD_CHECKSTATUS]
GO

CREATE TRIGGER [dbo].[TRG_UPDATE_PERLFILELOAD_CHECKSTATUS]
ON [dbo].[PerlFileLoad]
FOR UPDATE
AS
BEGIN
	INSERT INTO Maintenance.dbo.PerlEmail (dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
	SELECT
		I.sid_dbprocessinfo_dbnumber + ' file load ' + 
			CASE I.dim_perlfileload_status
				WHEN 1 THEN  'SUCCESS'
				WHEN 99 THEN 'FAILURE'
			END
		, REPLACE('File: ' + I.dim_perlfileload_filename + ' was<status>loaded successfully.', '<status>', CASE I.dim_perlfileload_status WHEN 1 THEN ' ' WHEN 99 THEN ' NOT ' END) 
		, MDT.dbo.ufn_GetProcessorEmailForTipFirst(I.sid_dbprocessinfo_dbnumber)
		, 'itops@rewardsnow.com'		
	FROM
		INSERTED I 
	WHERE I.dim_perlfileload_status IN (1, 99)
END


GO


