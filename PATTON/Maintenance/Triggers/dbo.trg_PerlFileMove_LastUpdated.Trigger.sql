USE [Maintenance]
GO

IF OBJECT_ID(N'trg_PerlFileMove_LastUpdated') IS NOT NULL
	DROP TRIGGER trg_PerlFileMove_LastUpdated
GO

CREATE TRIGGER [dbo].[trg_PerlFileMove_LastUpdated]  
   ON  dbo.PerlFileMove  
   AFTER INSERT, UPDATE  
AS   
BEGIN  
 SET NOCOUNT ON;  
  
 UPDATE pfm  
  SET dim_perlfilemove_lastmodified = GETDATE(),  
      dim_perlfilemove_lastmodifiedby = SYSTEM_USER  
 FROM inserted ins JOIN dbo.PerlFileMove pfm  
        ON ins.sid_perlfilemove_id = pfm.sid_perlfilemove_id  
END  
GO
