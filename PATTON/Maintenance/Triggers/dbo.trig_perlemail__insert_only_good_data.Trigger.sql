USE [Maintenance]
GO

if object_id('trig_perlemail__insert_only_good_data') is not null
	drop trigger trig_perlemail__insert_only_good_data
		ON Maintenance.dbo.perlemail

GO
		
		
CREATE TRIGGER trig_perlemail__insert_only_good_data
ON Maintenance.dbo.perlemail
INSTEAD OF INSERT
AS
BEGIN
	INSERT INTO perlemail 
	(
		dim_perlemail_subject
		, dim_perlemail_body
		, dim_perlemail_to
		, dim_perlemail_from
		, dim_perlemail_created
		, dim_perlemail_bcc
		, dim_perlemail_lastmodified
		, dim_perlemail_senddate
		, dim_perlemail_sent
		, dim_perlemail_attachment
	)
	SELECT
		CASE --Subject 
			WHEN maintenance.dbo.ufn_PerlEmailScrubAddresses (I.dim_perlemail_to) = '' THEN '**ERR: RECEPIENT (' + ISNULL(I.dim_perlemail_to, 'NULL') + ') - ' + I.dim_perlemail_subject
			WHEN ISNULL(I.dim_perlemail_body, '') = '' THEN '**ERR: BODY - ' + I.dim_perlemail_subject
			ELSE I.dim_perlemail_subject
		END AS dim_perlemail_subject
		, CASE --Body
			WHEN ISNULL(I.dim_perlemail_body, '') = '' THEN '**ERR: EMPTY EMAIL BODY'
			ELSE I.dim_perlemail_body
		END AS dim_perlemail_body
		, CASE --To
			WHEN maintenance.dbo.ufn_PerlEmailScrubAddresses (I.dim_perlemail_to) = '' OR ISNULL(I.dim_perlemail_body, '') = '' THEN 
				CASE WHEN maintenance.dbo.ufn_perlemailscrubaddresses (I.dim_perlemail_from) = '' 
					THEN 'opslogs@rewardsnow.com' 
					ELSE I.dim_perlemail_from 
				END
			ELSE
				maintenance.dbo.ufn_PerlEmailScrubAddresses (I.dim_perlemail_to)			
			END as dim_perlemail_to
		, CASE --From
			WHEN maintenance.dbo.ufn_PerlEmailScrubAddresses (I.dim_perlemail_from) = '' THEN 'opslogs@rewardsnow.com'
			ELSE I.dim_perlemail_from
		END AS dim_perlemail_from
		, I.dim_perlemail_created
		, I.dim_perlemail_bcc
		, I.dim_perlemail_lastmodified
		, I.dim_perlemail_senddate
		, I.dim_perlemail_sent
		, I.dim_perlemail_attachment
	FROM INSERTED I
END
