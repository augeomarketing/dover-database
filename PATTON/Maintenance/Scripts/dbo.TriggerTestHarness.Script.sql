USE [Maintenance]
GO


BEGIN TRANSACTION

	INSERT INTO perlemail (dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
	SELECT 'TEST BAD TO', 'Testing Perl Email Trigger', 'badrecepient', 'cheit@rewardsnow.com'
	UNION SELECT 'TEST BAD TO BAD FROM', 'Testing Perl Email Trigger', 'badrecepient', 'badsender'
	UNION SELECT 'TEST BAD BODY', '', 'pbutler@rewardsnow.com', 'cheit@rewardsnow.com'
	UNION SELECT 'TEST DUPE TO', 'Testing Perl Email Trigger', 'pbutler@rewardsnow.com, pbutler@rewardsnow.com, cheit@rewardsnow.com', 'cheit@rewardsnow.com'
	UNION SELECT 'TEST EMPTY TO', 'Testing Perl Email Trigger', '', 'cheit@rewardsnow.com'
	UNION SELECT 'TEST NULL TO', 'Testing Perl Email Trigger', NULL, 'cheit@rewardsnow.com'
	UNION SELECT 'TEST NULL BODY', NULL, 'pbutler@rewardsnow.com', 'cheit@rewardsnow.com'
	UNION SELECT 'TEST BAD FROM', 'Testing Perl Email Trigger', 'pbutler@rewardsnow.com', 'bobwehadababyitsaboy'

	SELECT * FROM perlemail order by sid_perlemail_id desc
	
ROLLBACK
