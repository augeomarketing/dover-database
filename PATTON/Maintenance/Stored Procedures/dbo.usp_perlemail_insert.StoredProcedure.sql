USE [Maintenance]
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_perlemail_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_perlemail_Insert]
GO

/****** Object:  StoredProcedure [dbo].[usp_perlemail_Insert]    Script Date: 4/20/2015 11:48:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 4.16.2015
-- Description:	Inserts new record into perlemail table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_perlemail_Insert] 
	-- Add the parameters for the stored procedure here
	@Subject VARCHAR(255), 
	@Body VARCHAR(MAX),
	@To VARCHAR(1024),
	@From VARCHAR(50),
	@Attachment VARCHAR(1024) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Maintenance.dbo.perlemail
	(
		dim_perlemail_body,
		dim_perlemail_from,
		dim_perlemail_to,
		dim_perlemail_subject,
		dim_perlemail_attachment
	)
	VALUES
	(
		@Body,
		@From,
		@To,
		@Subject,
		@Attachment
	)

END

GO



