USE [Maintenance]
GO

IF OBJECT_ID(N'ufn_PerlEmailScrubAddresses') IS NOT NULL
	DROP FUNCTION ufn_PerlEmailScrubAddresses
GO

CREATE FUNCTION ufn_PerlEmailScrubAddresses (@emailAddress VARCHAR(1024))
RETURNS VARCHAR(1024)
AS
BEGIN
	DECLARE @output VARCHAR(1024)
	
	SET @emailAddress = REPLACE(@emailAddress, ',', ';')
	
	SELECT @output = ISNULL(STUFF
	(
	
		(
			SELECT DISTINCT ';' + LTRIM(RTRIM(Item)) 
			FROM RewardsNow.dbo.Split(@emailAddress, ';')
			WHERE Item LIKE '%_@_%_.__%' 
				AND Item NOT LIKE '%[^a-z,0-9,@,.,|-,|_]%' ESCAPE '|'
			FOR XML PATH('')
		)
	
		, 1, 1, ''
	), '')

	RETURN @output
END
GO
