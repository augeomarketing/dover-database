/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 03/26/2009 11:12:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT (0),
	[Increases] [int] NULL CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT (0),
	[Decreases] [int] NULL CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT (0),
	[AdjustedEndingPoints] [int] NULL CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT (0)
) ON [PRIMARY]
GO
