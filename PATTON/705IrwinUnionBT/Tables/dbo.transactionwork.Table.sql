/****** Object:  Table [dbo].[transactionwork]    Script Date: 03/26/2009 11:13:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](25) NOT NULL,
	[SystemBankIdentifier] [varchar](4) NOT NULL,
	[PrincipleBankIdentifier] [varchar](4) NOT NULL,
	[AgentBankIdentifier] [varchar](4) NOT NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 0) NOT NULL CONSTRAINT [DF_transactionwork_Purchase]  DEFAULT (0),
	[Returns] [decimal](18, 0) NOT NULL CONSTRAINT [DF_transactionwork_Returns]  DEFAULT (0),
	[Bonus] [decimal](18, 0) NOT NULL CONSTRAINT [DF_transactionwork_Bonus]  DEFAULT (0),
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
