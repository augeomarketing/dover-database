/****** Object:  Table [dbo].[LostCards]    Script Date: 03/26/2009 11:13:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LostCards](
	[OldAcctID] [varchar](25) NULL,
	[NewAcctID] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
