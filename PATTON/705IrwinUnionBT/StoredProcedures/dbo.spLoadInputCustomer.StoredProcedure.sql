USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[spLoadInputCustomer]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadInputCustomer]
AS

truncate table input_customer

INSERT INTO Input_Customer (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_consumer

INSERT INTO Input_Customer (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Commercial

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_consumer b
WHERE     a.tipnumber = b.tipnumber

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber

update input_customer set statusdescription = 'Active[A]' where statuscode = 'A'

UPDATE    Input_Customer SET dateadded =  (SELECT dateadded FROM CUSTOMER_Stage
WHERE  customer_stage.tipnumber = input_customer.tipnumber)
GO
