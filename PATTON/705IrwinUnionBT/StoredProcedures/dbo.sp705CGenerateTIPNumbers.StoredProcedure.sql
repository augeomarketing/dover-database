USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[sp705CGenerateTIPNumbers]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp705CGenerateTIPNumbers]
AS 

update commercialwork 
set Tipnumber = b.tipnumber
from commercialwork a,affiliat b
where a.cr3 = b.acctid

update commercialwork 
set Tipnumber = b.tipnumber
from commercialwork a,affiliat b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber, tipnumber
from commercialwork

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 705, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 705000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 705, @newnum

update commercialwork 
set Tipnumber = b.tipnumber
from commercialwork a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')
GO
