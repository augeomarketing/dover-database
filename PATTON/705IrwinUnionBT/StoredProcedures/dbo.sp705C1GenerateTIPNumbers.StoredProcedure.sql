USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[sp705C1GenerateTIPNumbers]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp705C1GenerateTIPNumbers]
AS 

update comrollupwork 
set Tipnumber = b.tipnumber
from comrollupwork a,affiliat b
where a.cr3 = b.acctid

update comrollupwork 
set Tipnumber = b.tipnumber
from comrollupwork a,affiliat b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update comrollupwork 
set Tipnumber = b.tipnumber
from comrollupwork a,affiliat b
where a.misc8 = b.secid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select distinct misc8 as custid, tipnumber
from comrollupwork where tipnumber is null or tipnumber = ' '

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 705, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 705000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 705, @newnum

update comrollupwork 
set Tipnumber = b.tipnumber
from comrollupwork a,gentip b
where a.misc8 = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
