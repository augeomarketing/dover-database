USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10)
AS

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1)

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], 'BI', '1', convert(char(15), [bonus])  from Input_Transaction 
where (bonus <> '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '63', '1', convert(char(15), [purchase])  from Input_Transaction
where (purchase > '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '33','1', convert(char(15), [returns])  from Input_Transaction
where ([returns] > '0')

/******************************************************************************/
-- Load the TransStandard table with  FTUB Customers
/******************************************************************************/
set @Trandate=@dateadded
--set @bonuspoints='5000'

truncate table ftub_work_table

insert ftub_work_table (tipnumber) 
select distinct(tipnumber) from input_transaction
where ([Principal Bank Identifier] IN ('300', '6800')) AND
 ([Agent Bank Identifier] = '1000') AND (Purchase > '0') AND (NOT EXISTS
(SELECT  * FROM   onetimebonuses_stage                            
WHERE      tipnumber = input_transaction.tipnumber))

update ftub_work_table set acctid = b.cardnumber
from ftub_work_table a, input_transaction b
where a.tipnumber = b.tipnumber
	
INSERT INTO transstandard (Tip,tranDate,acctnum,TranCode,Trannum,tranamt,Ratio,trantype)
select Tipnumber, @Trandate, acctid,'BF', '1', '500', '1', 'Bonus for First Trans Activity' from ftub_work_table

truncate table ftub_work_table

insert ftub_work_table (tipnumber) 
select distinct(tipnumber) from input_transaction
where ([Principal Bank Identifier] IN ('310', '1100')) AND
 ([Agent Bank Identifier] = '0') AND (Purchase > '0') AND (NOT EXISTS
(SELECT  * FROM   onetimebonuses_stage                            
WHERE      tipnumber = input_transaction.tipnumber))

update ftub_work_table set acctid = b.cardnumber
from ftub_work_table a, input_transaction b
where a.tipnumber = b.tipnumber

INSERT INTO transstandard (Tip,tranDate,acctnum,TranCode,Trannum,tranamt,Ratio,trantype)
select Tipnumber, @Trandate, acctid,'BF', '1', '1000', '1', 'Bonus for First Trans Activity' from ftub_work_table

Insert into onetimebonuses_stage (tipnumber,Trancode,AcctID,Dateawarded)
select tip,trancode,acctnum,trandate from transstandard where trancode = 'BF'
/*                                                                            */
/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

SET QUOTED_IDENTIFIER OFF
GO
