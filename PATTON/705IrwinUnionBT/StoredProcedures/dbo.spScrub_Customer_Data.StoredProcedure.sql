USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[spScrub_Customer_Data]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spScrub_Customer_Data]
AS

/*****************************************************/
/*    Scrub Consumer Customer Information            */
/*****************************************************/   

UPDATE roll_consumer SET lstname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1,1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE   roll_consumer SET lstname = ACCTNAME1 
WHERE     (lstname = 'LLC') OR (lstname = 'BankCard') OR (lstname = 'INC') OR (lstname = 'INC.') OR (lstname = 'LTD') OR (lstname = 'Payable') or (lstname = ' ') or (lstname is null) 

UPDATE roll_consumer SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE roll_consumer SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

UPDATE roll_consumer SET Address4 = ltrim(rtrim(City + '  ' + State + ' ' + left(zipcode,5)  + '-' + right(zipcode,4)))

UPDATE roll_consumer SET internalstatuscode = 'A'

update roll_consumer set segmentcode = 'CN'

/*****************************************************/
/*  Scrub Commercial Customer Information            */
/*****************************************************/   
update roll_commercial set acctname3 = acctname2 where (acctname3 is null or acctname3 = ' ')

update roll_commercial set acctname2 = ' ' where acctname3 = acctname2

update roll_commercial set acctname2 = acctname1 where (acctname2 is null or acctname2 = ' ')

update roll_commercial set acctname1 = ' ' where acctname2 = acctname1

update roll_commercial set acctname1 = companyname where (acctname1 is null or acctname1 = ' ')

UPDATE roll_commercial SET lstname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1,1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE roll_commercial SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

update roll_commercial set lstname = acctname1 where lstname is null

UPDATE roll_commercial SET Address4 = ltrim(rtrim(City + '  ' + State +  ' ' + left(zipcode,5)  + '-' + right(zipcode,4)))

UPDATE roll_commercial SET statuscode = 'A'

update roll_commercial set segmentcode = 'CM'

UPDATE roll_commercial SET lstname = ACCTNAME1
WHERE (lstname = 'LLC') OR (lstname = 'BankCard') OR
(lstname = 'INC') OR (lstname = 'LTD') OR (lstname = 'Payable')

UPDATE roll_commercial SET lstname = ACCTNAME1
WHERE len(lstname) = 2

UPDATE    roll_commercial SET CustID = CompanyId 
where misc8 ='Y'

exec spRemovePunctuation
GO
