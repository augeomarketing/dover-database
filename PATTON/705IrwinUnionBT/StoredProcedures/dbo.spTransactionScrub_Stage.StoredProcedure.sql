USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionScrub_Stage]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[spTransactionScrub_Stage]
 AS

--------------- Input Transaction table

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (tipnumber is null or tipnumber = ' ') or
	      ((purchase is null  and [returns] is null and bonus is null))
	       
Delete from Input_Transaction
where (cardnumber is null or cardnumber = ' ') or
      (tipnumber is null or tipnumber = ' ') or
      ((purchase is null  or purchase =  0) and ([Returns] is null or [Returns] = 0) and (Bonus is null or Bonus = 0))


/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transaction
set  purchase = round(purchase,0),  [Returns] =  round([returns],0), bonus = round(Bonus,0)
GO
