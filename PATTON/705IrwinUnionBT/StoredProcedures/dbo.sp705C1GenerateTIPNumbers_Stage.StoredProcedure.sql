USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[sp705C1GenerateTIPNumbers_Stage]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp705C1GenerateTIPNumbers_Stage]
AS 

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,affiliat_stage b
where a.cr3 = b.acctid

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,affiliat_stage b
where a.misc8 = b.secid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select distinct misc8 as custid, tipnumber
from roll_commercial_rollup where tipnumber is null or tipnumber = ' '

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 705, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 705000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 705, @newnum

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,gentip b
where a.misc8 = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
