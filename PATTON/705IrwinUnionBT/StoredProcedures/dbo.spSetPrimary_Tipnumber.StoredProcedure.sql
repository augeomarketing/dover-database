USE [705IrwinUnionBT]
GO
/****** Object:  StoredProcedure [dbo].[spSetPrimary_Tipnumber]    Script Date: 09/21/2009 10:16:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetPrimary_Tipnumber] 
AS 


declare @wtip_cnt numeric

set @wtip_cnt = (Select max(tip_cnt) from comb_in ) 
 
update comb_in set tip_pri = (select tip_sec from comb_in where
tip_cnt = @wtip_cnt)

delete comb_in where tip_cnt = @wtip_cnt

delete comb_in where tip_pri = tip_sec
GO
