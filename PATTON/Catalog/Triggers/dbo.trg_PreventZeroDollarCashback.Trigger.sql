USE Catalog
GO

-- =============================================
-- Author:		Chris Heit
-- Create date: 2013-01-24
-- Description:	Prevents Insertion of CashBacks with 0 Cash Value
-- =============================================
CREATE TRIGGER dbo.trg_PreventZeroDollarCashback 
   ON  dbo.catalog 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (SELECT COUNT(*) FROM INSERTED WHERE dim_catalog_code LIKE 'CASH%' AND dim_catalog_cashvalue = 0) > 0
	BEGIN
		RAISERROR('CashBack Items MUST have an associated Cash Value.', 16, 1);
		ROLLBACK TRANSACTION;
	END;
END;
GO
