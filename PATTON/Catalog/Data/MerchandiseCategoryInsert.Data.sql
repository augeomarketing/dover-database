USE Catalog
GO

DECLARE @Household VARCHAR(50) = 'Appliances � household'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Household,
	@Household,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Household

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15100
FROM
	dbo.category
WHERE 
	dim_category_name = @Household

DECLARE @Kitchen VARCHAR(50) = 'Appliances � kitchen'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Kitchen,
	@Kitchen,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Kitchen

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15200
FROM
	dbo.category
WHERE 
	dim_category_name = @Kitchen

DECLARE @Bose VARCHAR(50) = 'Bose�'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Bose,
	@Bose,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Bose

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15250
FROM
	dbo.category
WHERE 
	dim_category_name = @Bose

DECLARE @CoinsDesc VARCHAR(250) = 'Coins & collectibles (catalog prices only, no margins)'
DECLARE @Coins VARCHAR(30) = 'Coins & collectibles'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Coins,
	@CoinsDesc,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Coins

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15360
FROM
	dbo.category
WHERE 
	dim_category_name = @Coins

DECLARE @Audio VARCHAR(50) = 'Electronics � audio' 

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Audio,
	@Audio,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Audio

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15500
FROM
	dbo.category
WHERE 
	dim_category_name = @Audio

DECLARE @Televisions VARCHAR(50) = 'Televisions'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Televisions,
	@Televisions,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Televisions

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15510
FROM
	dbo.category
WHERE 
	dim_category_name = @Televisions

DECLARE @Video VARCHAR(50) = 'Electronics � video'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Video,
	@Video,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Video

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15550
FROM
	dbo.category
WHERE 
	dim_category_name = @Video

DECLARE @Games VARCHAR(50) = 'Video systems/games'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Games,
	@Games,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Games

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15555
FROM
	dbo.category
WHERE 
	dim_category_name = @Games

DECLARE @Fashion VARCHAR(50) = 'Fashion'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Fashion,
	@Fashion,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Fashion

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15600
FROM
	dbo.category
WHERE 
	dim_category_name = @Fashion

DECLARE @Foods VARCHAR(50) = 'Gourmet Foods'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Foods,
	@Foods,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Foods

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15601
FROM
	dbo.category
WHERE 
	dim_category_name = @Foods

DECLARE @Energy VARCHAR(50) = 'Energy saving'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Energy,
	@Energy,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Energy

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15612
FROM
	dbo.category
WHERE 
	dim_category_name = @Energy

DECLARE @Hardware VARCHAR(50) = 'Hardware'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Hardware,
	@Hardware,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Hardware

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15650
FROM
	dbo.category
WHERE 
	dim_category_name = @Hardware

DECLARE @Health VARCHAR(50) = 'Health & beauty'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Health,
	@Health,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Health

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15800
FROM
	dbo.category
WHERE 
	dim_category_name = @Health

DECLARE @Home VARCHAR(50) = 'Home furnishing'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Home,
	@Home,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Home

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15850
FROM
	dbo.category
WHERE 
	dim_category_name = @Home

DECLARE @Housewares VARCHAR(50) = 'Housewares'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Housewares,
	@Housewares,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Housewares

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15875
FROM
	dbo.category
WHERE 
	dim_category_name = @Housewares

DECLARE @Watches VARCHAR(50) = 'Watches-men''s'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Watches,
	@Watches,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Watches

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	15955
FROM
	dbo.category
WHERE 
	dim_category_name = @Watches

DECLARE @Children VARCHAR(50) = 'Children''s (Juvenile)'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Children,
	@Children,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Children

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16000
FROM
	dbo.category
WHERE 
	dim_category_name = @Children

DECLARE @Lawn VARCHAR(50) = 'Lawn & garden'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Lawn,
	@Lawn,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Lawn

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16100
FROM
	dbo.category
WHERE 
	dim_category_name = @Lawn

DECLARE @Luggage VARCHAR(50) = 'Luggage & leather'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Luggage,
	@Luggage,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Luggage

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16200
FROM
	dbo.category
WHERE 
	dim_category_name = @Luggage

DECLARE @Office VARCHAR(50) = 'Office products'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Office,
	@Office,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Office

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16300
FROM
	dbo.category
WHERE 
	dim_category_name = @Office

DECLARE @Photography VARCHAR(50) = 'Photography' 

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Photography,
	@Photography,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Photography

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16400
FROM
	dbo.category
WHERE 
	dim_category_name = @Photography

DECLARE @Safety VARCHAR(50) = 'Safety & security'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Safety,
	@Safety,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Safety

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16500
FROM
	dbo.category
WHERE 
	dim_category_name = @Safety

DECLARE @Sport VARCHAR(50) = 'Sporting goods'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Sport,
	@Sport,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Sport

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	16600
FROM
	dbo.category
WHERE 
	dim_category_name = @Sport

DECLARE @AppleDesc VARCHAR(50) = 'Apple� (MBF margin is 15%)'
DECLARE @Apple VARCHAR(50) = 'Apple�'

INSERT INTO dbo.category
(
	[dim_category_name],
	[dim_category_description],
	[dim_category_created],
	[dim_category_lastmodified],
	[dim_category_active],
	[sid_userinfo_id],
	[dim_category_imagehighdef]
)
VALUES
(
	@Apple,
	@AppleDesc,
	GETDATE(),
	GETDATE(),
	1,
	0,
	''
)

INSERT INTO dbo.categorygroupinfo
(
	[sid_category_id],
	[sid_groupinfo_id],
	[dim_categorygroupinfo_created],
	[dim_categorygroupinfo_lastmodified]
)
SELECT
	sid_category_id,
	1, -- For Merchandise
	GETDATE(),
	GETDATE()
FROM
	dbo.category 
WHERE
	dim_category_name = @Apple

INSERT INTO dbo.categorybrand
(
	[sid_category_id],
	[branding_number]
)
SELECT
	sid_category_id,
	26952
FROM
	dbo.category
WHERE 
	dim_category_name = @Apple

