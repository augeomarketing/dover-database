/*
   Wednesday, April 13, 201110:45:16 AM
   User: 
   Server: Doolittle\Rn
   Database: Catalog
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_created
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_lastmodified
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_active
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_parentid
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_sid_userinfo_id
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_brochure
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_cashvalue
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_sid_catalog_routing
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_cost
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_shipping
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_handling
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_msrp
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_HIshipping
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_weight
GO
ALTER TABLE dbo.editcatalog
	DROP CONSTRAINT DF_editcatalog_dim_catalog_featured
GO
CREATE TABLE dbo.Tmp_editcatalog
	(
	sid_editcatalog_id int NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	sid_catalog_id int NOT NULL,
	dim_catalog_code varchar(20) NOT NULL,
	dim_catalog_trancode char(2) NOT NULL,
	dim_catalog_dollars decimal(10, 2) NOT NULL,
	dim_catalog_imagelocation varchar(1024) NOT NULL,
	dim_catalog_imagealign varchar(50) NOT NULL,
	dim_catalog_created datetime NOT NULL,
	dim_catalog_lastmodified datetime NOT NULL,
	dim_catalog_active int NOT NULL,
	dim_catalog_parentid int NOT NULL,
	sid_status_id int NOT NULL,
	sid_userinfo_id int NOT NULL,
	dim_catalog_brochure int NOT NULL,
	dim_catalog_cashvalue decimal(18, 2) NOT NULL,
	sid_routing_id int NOT NULL,
	dim_catalog_cost decimal(18, 2) NOT NULL,
	dim_catalog_shipping decimal(18, 2) NOT NULL,
	dim_catalog_handling decimal(18, 2) NOT NULL,
	dim_catalog_msrp decimal(18, 2) NOT NULL,
	dim_catalog_HIshipping decimal(18, 2) NOT NULL,
	dim_catalog_weight decimal(18, 2) NOT NULL,
	dim_catalog_featured int NOT NULL,
	dim_catalog_auditcomment varchar(1024) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_editcatalog SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_created DEFAULT (getdate()) FOR dim_catalog_created
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_lastmodified DEFAULT (getdate()) FOR dim_catalog_lastmodified
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_active DEFAULT ((1)) FOR dim_catalog_active
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_parentid DEFAULT ((-1)) FOR dim_catalog_parentid
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_sid_userinfo_id DEFAULT ((0)) FOR sid_userinfo_id
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_brochure DEFAULT ((0)) FOR dim_catalog_brochure
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_cashvalue DEFAULT ((0.00)) FOR dim_catalog_cashvalue
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_sid_catalog_routing DEFAULT ((1)) FOR sid_routing_id
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_cost DEFAULT ((0.0)) FOR dim_catalog_cost
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_shipping DEFAULT ((0.0)) FOR dim_catalog_shipping
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_handling DEFAULT ((0.0)) FOR dim_catalog_handling
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_msrp DEFAULT ((0.0)) FOR dim_catalog_msrp
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_HIshipping DEFAULT ((0.0)) FOR dim_catalog_HIshipping
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_weight DEFAULT ((0.0)) FOR dim_catalog_weight
GO
ALTER TABLE dbo.Tmp_editcatalog ADD CONSTRAINT
	DF_editcatalog_dim_catalog_featured DEFAULT ((0)) FOR dim_catalog_featured
GO
SET IDENTITY_INSERT dbo.Tmp_editcatalog ON
GO
IF EXISTS(SELECT * FROM dbo.editcatalog)
	 EXEC('INSERT INTO dbo.Tmp_editcatalog (sid_editcatalog_id, sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created, dim_catalog_lastmodified, dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_cashvalue, sid_routing_id, dim_catalog_cost, dim_catalog_shipping, dim_catalog_handling, dim_catalog_msrp, dim_catalog_HIshipping, dim_catalog_weight, dim_catalog_featured, dim_catalog_auditcomment)
		SELECT sid_editcatalog_id, sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created, dim_catalog_lastmodified, dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_cashvalue, sid_catalog_routing, dim_catalog_cost, dim_catalog_shipping, dim_catalog_handling, dim_catalog_msrp, dim_catalog_HIshipping, dim_catalog_weight, dim_catalog_featured, dim_catalog_auditcomment FROM dbo.editcatalog WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_editcatalog OFF
GO
DROP TABLE dbo.editcatalog
GO
EXECUTE sp_rename N'dbo.Tmp_editcatalog', N'editcatalog', 'OBJECT' 
GO
ALTER TABLE dbo.editcatalog ADD CONSTRAINT
	PK_editcatalog PRIMARY KEY CLUSTERED 
	(
	sid_editcatalog_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 99, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE TRIGGER [dbo].[TRIG_editcatalog_UPDATE] ON dbo.editcatalog 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
/*
      DECLARE @sid_editcatalog_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_editcatalog_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_editcatalog_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE editcatalog SET dim_catalog_lastmodified = getdate() WHERE sid_editcatalog_id = @sid_editcatalog_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_editcatalog_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
*/
	UPDATE c
		SET dim_catalog_lastmodified = getdate()
	FROM dbo.editcatalog c JOIN deleted del
		ON c.sid_editcatalog_id = del.sid_editcatalog_id

 END
GO
COMMIT
