insert into catalog.dbo.cataloggroupinfo(sid_catalog_id, sid_groupinfo_id)
select distinct c.sid_catalog_id, cg.sid_groupinfo_id
from catalog.dbo.catalog c
join catalog.dbo.catalogcategory cc
on c.sid_catalog_id = cc.sid_catalog_id
join catalog.dbo.categorygroupinfo cg
on cc.sid_category_id = cg.sid_category_id
where c.dim_catalog_active = 1
and cc.dim_catalogcategory_active = 1

