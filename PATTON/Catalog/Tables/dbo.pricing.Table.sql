USE [Catalog]
GO
/****** Object:  Table [dbo].[pricing]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pricing](
	[sid_pricing_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[sid_vendor_id] [int] NOT NULL,
	[dim_pricing_cost] [money] NOT NULL,
	[dim_pricing_shipping] [money] NOT NULL,
	[dim_pricing_handling] [money] NOT NULL,
	[dim_pricing_MSRP] [money] NOT NULL,
	[dim_pricing_HIshipping] [money] NOT NULL,
	[dim_pricing_weight] [int] NOT NULL,
	[dim_pricing_active] [int] NOT NULL,
	[dim_pricing_lastmodified] [datetime] NOT NULL,
	[dim_pricing_created] [datetime] NOT NULL
) ON [PRIMARY]
GO
