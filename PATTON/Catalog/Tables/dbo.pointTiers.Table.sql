USE [Catalog]
GO
/****** Object:  Table [dbo].[pointTiers]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pointTiers](
	[sid_pointTiers_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_pointTiers_tierFamily] [int] NOT NULL,
	[dim_pointTiers_pointTier] [int] NOT NULL,
	[dim_pointTiers_created] [datetime] NOT NULL,
	[dim_pointTiers_lastModified] [datetime] NOT NULL,
	[dim_pointTiers_active] [int] NOT NULL,
 CONSTRAINT [PK_pointTiers] PRIMARY KEY CLUSTERED 
(
	[sid_pointTiers_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
