USE [Catalog]
GO
/****** Object:  Table [dbo].[loyaltytip]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loyaltytip](
	[sid_loyaltytip_id] [int] IDENTITY(500,1) NOT FOR REPLICATION NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[dim_loyaltytip_prefix] [char](3) NOT NULL,
	[dim_loyaltytip_finame] [varchar](255) NOT NULL,
	[dim_loyaltytip_created] [datetime] NOT NULL,
	[dim_loyaltytip_lastmodified] [datetime] NOT NULL,
	[dim_loyaltytip_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_LoyaltyTip] PRIMARY KEY CLUSTERED 
(
	[sid_loyaltytip_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_loyaltytip_active_prefix_id] ON [dbo].[loyaltytip] 
(
	[dim_loyaltytip_active] ASC,
	[dim_loyaltytip_prefix] ASC,
	[sid_loyalty_id] ASC
) ON [PRIMARY]
GO
