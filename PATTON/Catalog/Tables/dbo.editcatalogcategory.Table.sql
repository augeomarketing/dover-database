USE [Catalog]
GO
/****** Object:  Table [dbo].[editcatalogcategory]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[editcatalogcategory](
	[sid_editcatalogcategory_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalogcategory_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_catalogcategory_created] [datetime] NOT NULL,
	[dim_catalogcategory_lastmodified] [datetime] NOT NULL,
	[dim_catalogcategory_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_editcatalogcategory] PRIMARY KEY CLUSTERED 
(
	[sid_editcatalogcategory_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
