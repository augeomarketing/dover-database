USE [Catalog]
GO
/****** Object:  Table [dbo].[archiveuserinfo]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archiveuserinfo](
	[sid_archiveuserinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_userinfo_fname] [varchar](50) NOT NULL,
	[dim_userinfo_lname] [varchar](50) NOT NULL,
	[dim_userinfo_email] [varchar](50) NOT NULL,
	[dim_userinfo_username] [varchar](50) NOT NULL,
	[dim_userinfo_password] [varchar](255) NOT NULL,
	[dim_userinfo_lastlogin] [datetime] NOT NULL,
	[dim_userinfo_penultimatelogin] [datetime] NOT NULL,
	[dim_userinfo_created] [datetime] NOT NULL,
	[dim_userinfo_lastmodified] [datetime] NOT NULL,
	[dim_userinfo_active] [int] NOT NULL,
	[dim_archiveuserinfo_created] [datetime] NOT NULL,
	[dim_archiveuserinfo_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
