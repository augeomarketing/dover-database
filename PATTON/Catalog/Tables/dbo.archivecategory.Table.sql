USE [Catalog]
GO
/****** Object:  Table [dbo].[archivecategory]    Script Date: 07/30/2009 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archivecategory](
	[sid_archivecategory_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_category_name] [varchar](30) NOT NULL,
	[dim_category_description] [varchar](300) NOT NULL,
	[dim_category_imagelocation] [varchar](1024) NULL,
	[dim_category_created] [datetime] NOT NULL,
	[dim_category_lastmodified] [datetime] NOT NULL,
	[dim_category_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_category_imagehighdef] [varchar](1024) NULL,
	[dim_category_randomrotation] [int] NOT NULL,
	[dim_archivecategory_created] [datetime] NOT NULL,
	[dim_archivecategory_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_archivecategory] PRIMARY KEY CLUSTERED 
(
	[sid_archivecategory_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
