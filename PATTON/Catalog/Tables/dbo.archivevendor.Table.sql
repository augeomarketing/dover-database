USE [Catalog]
GO
/****** Object:  Table [dbo].[archivevendor]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archivevendor](
	[sid_archivevendor_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_vendor_id] [int] NOT NULL,
	[dim_vendor_name] [varchar](50) NOT NULL,
	[dim_vendor_code] [char](5) NOT NULL,
	[dim_vendor_contact] [varchar](50) NOT NULL,
	[dim_vendor_created] [datetime] NOT NULL,
	[dim_vendor_lastmodified] [datetime] NOT NULL,
	[dim_vendor_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_archivevendor_created] [datetime] NOT NULL,
	[dim_archivevendor_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_archivevendor] PRIMARY KEY CLUSTERED 
(
	[sid_archivevendor_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
