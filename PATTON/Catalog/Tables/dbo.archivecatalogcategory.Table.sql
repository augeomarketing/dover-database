USE [Catalog]
GO
/****** Object:  Table [dbo].[archivecatalogcategory]    Script Date: 07/30/2009 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[archivecatalogcategory](
	[sid_archivecatalogcategory_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalogcategory_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_catalogcategory_created] [datetime] NOT NULL,
	[dim_catalogcategory_lastmodified] [datetime] NOT NULL,
	[dim_catalogcategory_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_archivecatalogcategory_created] [datetime] NOT NULL,
	[dim_archivecatalogcategory_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
