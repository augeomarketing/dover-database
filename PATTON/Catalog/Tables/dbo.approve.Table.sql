USE [Catalog]
GO
/****** Object:  Table [dbo].[approve]    Script Date: 07/30/2009 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[approve](
	[sid_approve_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_approve_created] [datetime] NOT NULL,
	[dim_approve_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_approve] PRIMARY KEY CLUSTERED 
(
	[sid_approve_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
