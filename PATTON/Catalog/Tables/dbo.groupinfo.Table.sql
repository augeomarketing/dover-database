USE [Catalog]
GO
/****** Object:  Table [dbo].[groupinfo]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[groupinfo](
	[sid_groupinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_groupinfo_name] [varchar](50) NOT NULL,
	[dim_groupinfo_description] [varchar](1024) NOT NULL,
	[dim_groupinfo_created] [datetime] NOT NULL,
	[dim_groupinfo_lastmodified] [datetime] NOT NULL,
	[dim_groupinfo_active] [int] NOT NULL,
	[dim_groupinfo_trancode] [varchar](2) NOT NULL,
 CONSTRAINT [PK_groupinfo] PRIMARY KEY CLUSTERED 
(
	[sid_groupinfo_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
