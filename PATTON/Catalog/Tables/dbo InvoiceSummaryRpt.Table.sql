USE [Catalog]
GO

/****** Object:  Table [dbo].[InvoiceSummaryRpt]    Script Date: 07/20/2011 10:09:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceSummaryRpt]') AND type in (N'U'))
DROP TABLE [dbo].[InvoiceSummaryRpt]
GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[InvoiceSummaryRpt]    Script Date: 07/20/2011 10:09:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[InvoiceSummaryRpt](
	[tipfirst] [varchar](3) NOT NULL,
	[clientname] [varchar](256) NULL,
	[dim_groupinfo_description] [varchar](1024) NOT NULL,
	[trancode] [varchar](2) NULL,
	[SumQty] [int] NULL,
	[SumPoints] [int] NULL,
	[SumTotal] [decimal](38, 4) NULL,
	[StartDate] [date],
	[EndDate]  [date]
) ON [PRIMARY]

GO


