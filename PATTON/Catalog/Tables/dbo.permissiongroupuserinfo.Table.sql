USE [Catalog]
GO
/****** Object:  Table [dbo].[permissiongroupuserinfo]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissiongroupuserinfo](
	[sid_permissiongroupuserinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_permissiongroupuserinfo_created] [datetime] NOT NULL,
	[dim_permissiongroupuserinfo_lastmodified] [datetime] NOT NULL,
	[dim_permissiongroupuserinfo_active] [int] NOT NULL,
 CONSTRAINT [PK_permissiongroupuserinfo] PRIMARY KEY CLUSTERED 
(
	[sid_permissiongroupuserinfo_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
