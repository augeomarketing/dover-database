USE [Catalog]
GO
/****** Object:  Table [dbo].[archivepermissiongrouprole]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[archivepermissiongrouprole](
	[sid_archivepermissiongrouprole_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_permissiongrouprole_id] [int] NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[sid_role_id] [int] NOT NULL,
	[dim_permissiongrouprole_created] [datetime] NOT NULL,
	[dim_permissiongrouprole_lastmodified] [datetime] NOT NULL,
	[dim_permissiongrouprole_active] [int] NOT NULL,
	[dim_archivepermissiongrouprole_created] [datetime] NOT NULL,
	[dim_archivepermissiongrouprole_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_archivepermissiongrouprole] PRIMARY KEY CLUSTERED 
(
	[sid_archivepermissiongrouprole_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
