USE [Catalog]
GO
/****** Object:  Table [dbo].[categorygroupinfo]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categorygroupinfo](
	[sid_categorygroupinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[sid_groupinfo_id] [int] NOT NULL,
	[dim_categorygroupinfo_created] [datetime] NOT NULL,
	[dim_categorygroupinfo_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_categorygroupinfo] PRIMARY KEY CLUSTERED 
(
	[sid_categorygroupinfo_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catid_groupid] ON [dbo].[categorygroupinfo] 
(
	[sid_category_id] ASC,
	[sid_groupinfo_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
