USE [Catalog]
GO
/****** Object:  Table [dbo].[editcatalogdescription]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[editcatalogdescription](
	[sid_editcatalogdescription_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalogdescription_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_catalogdescription_name] [varchar](1024) NOT NULL,
	[dim_catalogdescription_description] [varchar](4096) NOT NULL,
	[sid_languageinfo_id] [int] NOT NULL,
	[dim_catalogdescription_created] [datetime] NOT NULL,
	[dim_catalogdescription_lastmodified] [datetime] NOT NULL,
	[dim_catalogdescription_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_editcatalogdescription] PRIMARY KEY CLUSTERED 
(
	[sid_editcatalogdescription_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [_dta_index_editcatalogdescription_179_452196661__K3_K6] ON [dbo].[editcatalogdescription] 
(
	[sid_catalog_id] ASC,
	[sid_languageinfo_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_editcatalogdescription_179_452196661__K6_K3] ON [dbo].[editcatalogdescription] 
(
	[sid_languageinfo_id] ASC,
	[sid_catalog_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
