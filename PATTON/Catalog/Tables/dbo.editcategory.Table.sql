USE [Catalog]
GO
/****** Object:  Table [dbo].[editcategory]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[editcategory](
	[sid_editcategory_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_category_name] [varchar](20) NOT NULL,
	[dim_category_description] [varchar](300) NOT NULL,
	[dim_category_created] [datetime] NOT NULL,
	[dim_category_lastmodified] [datetime] NOT NULL,
	[dim_category_active] [int] NOT NULL,
	[dim_editcategory_created] [datetime] NOT NULL,
	[dim_editcategory_lastmodified] [datetime] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
