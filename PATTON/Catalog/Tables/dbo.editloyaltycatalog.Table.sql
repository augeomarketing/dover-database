USE [Catalog]
GO
/****** Object:  Table [dbo].[editloyaltycatalog]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[editloyaltycatalog](
	[sid_editloyaltycatalog_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dwid_loyaltycatalog_id] [int] NULL,
	[sid_loyaltycatalog_id] [int] NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_loyaltycatalog_pointvalue] [int] NOT NULL,
	[dim_loyaltycatalog_bonus] [int] NOT NULL,
	[dim_loyaltycatalog_created] [datetime] NOT NULL,
	[dim_loyaltycatalog_lastmodified] [datetime] NOT NULL,
	[dim_loyaltycatalog_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_editloyaltycatalog] PRIMARY KEY CLUSTERED 
(
	[sid_editloyaltycatalog_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [editloyaltycatalog3] ON [dbo].[editloyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
