USE [Catalog]
GO
/****** Object:  Table [dbo].[permissiongrouprole]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[permissiongrouprole](
	[sid_permissiongrouprole_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[sid_role_id] [int] NOT NULL,
	[dim_permissiongrouprole_created] [datetime] NOT NULL,
	[dim_permissiongrouprole_lastmodified] [datetime] NOT NULL,
	[dim_permissiongrouprole_active] [int] NOT NULL,
 CONSTRAINT [PK_permissiongrouprole] PRIMARY KEY CLUSTERED 
(
	[sid_permissiongrouprole_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
