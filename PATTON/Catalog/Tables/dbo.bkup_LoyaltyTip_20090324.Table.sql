USE [Catalog]
GO
/****** Object:  Table [dbo].[bkup_LoyaltyTip_20090324]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bkup_LoyaltyTip_20090324](
	[sid_loyaltytip_id] [int] IDENTITY(500,1) NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[dim_loyaltytip_prefix] [char](3) NOT NULL,
	[dim_loyaltytip_finame] [varchar](255) NOT NULL,
	[dim_loyaltytip_created] [datetime] NOT NULL,
	[dim_loyaltytip_lastmodified] [datetime] NOT NULL,
	[dim_loyaltytip_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
