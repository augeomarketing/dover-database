USE [Catalog]
GO

/****** Object:  Table [dbo].[categorybrand]    Script Date: 6/9/2015 10:31:24 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[categorybrand]') AND type in (N'U'))
DROP TABLE [dbo].[categorybrand]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[categorybrand](
	[sid_category_id] [int] NOT NULL,
	[branding_number] [int] NOT NULL,
 CONSTRAINT [PK_categorybrand] PRIMARY KEY CLUSTERED 
(
	[sid_category_id] ASC,
	[branding_number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[categorybrand]  WITH CHECK ADD  CONSTRAINT [FK_categorybrand_category] FOREIGN KEY([sid_category_id])
REFERENCES [dbo].[category] ([sid_category_id])
GO

ALTER TABLE [dbo].[categorybrand] CHECK CONSTRAINT [FK_categorybrand_category]
GO



