USE [Catalog]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_catalog_routing_sid_routing_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[catalog]'))
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [FK_catalog_routing_sid_routing_id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_catalog_status_sid_status_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[catalog]'))
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [FK_catalog_status_sid_status_id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_catalog_userinfo_sid_userinfo-id]') AND parent_object_id = OBJECT_ID(N'[dbo].[catalog]'))
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [FK_catalog_userinfo_sid_userinfo-id]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogCode]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogTrancode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogTrancode]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogDollars]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogDollars]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogImageLocation]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogImageLocation]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_imagealign]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_imagealign]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogCreated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogCreated]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogLastModified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogLastModified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogActive]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Catalog_CatalogParentID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_Catalog_CatalogParentID]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_sdi_userinfo_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_sdi_userinfo_id]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_brochure]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_brochure]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_cashvalue]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_cashvalue]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_sid_routing_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_sid_routing_id]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_cost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_cost]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_shipping0.00]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_shipping0.00]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_handling]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_handling]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_msrp]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_msrp]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_HIshipping]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_HIshipping]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_catalog_dim_catalog_weight]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[catalog] DROP CONSTRAINT [DF_catalog_dim_catalog_weight]
END

GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[catalog]    Script Date: 10/05/2010 17:36:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalog]') AND type in (N'U'))
DROP TABLE [dbo].[catalog]
GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[catalog]    Script Date: 10/05/2010 17:36:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[catalog](
	[sid_catalog_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_catalog_code] [varchar](20) NOT NULL,
	[dim_catalog_trancode] [char](2) NOT NULL,
	[dim_catalog_dollars] [decimal](10, 2) NOT NULL,
	[dim_catalog_imagelocation] [varchar](1024) NOT NULL,
	[dim_catalog_imagealign] [varchar](50) NOT NULL,
	[dim_catalog_created] [datetime] NOT NULL,
	[dim_catalog_lastmodified] [datetime] NOT NULL,
	[dim_catalog_active] [int] NOT NULL,
	[dim_catalog_parentid] [int] NOT NULL,
	[sid_status_id] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_catalog_brochure] [int] NOT NULL,
	[dim_catalog_cashvalue] [decimal](18, 2) NOT NULL,
	[sid_routing_id] [int] NOT NULL,
	[dim_catalog_cost] [decimal](18, 2) NOT NULL,
	[dim_catalog_shipping] [decimal](18, 2) NOT NULL,
	[dim_catalog_handling] [decimal](18, 2) NOT NULL,
	[dim_catalog_msrp] [decimal](18, 2) NOT NULL,
	[dim_catalog_HIshipping] [decimal](18, 2) NOT NULL,
	[dim_catalog_weight] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Catalog] PRIMARY KEY CLUSTERED 
(
	[sid_catalog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [Catalog]
/****** Object:  Index [_dta_index_catalog_179_1607676775__K11_K9_K1_K2]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [_dta_index_catalog_179_1607676775__K11_K9_K1_K2] ON [dbo].[catalog] 
(
	[sid_status_id] ASC,
	[dim_catalog_active] ASC,
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [Catalog]
/****** Object:  Index [_dta_index_catalog_179_1607676775__K9_K1_K2]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [_dta_index_catalog_179_1607676775__K9_K1_K2] ON [dbo].[catalog] 
(
	[dim_catalog_active] ASC,
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [Catalog]
/****** Object:  Index [_dta_index_catalog_179_1607676775__K9_K1_K2_K11]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [_dta_index_catalog_179_1607676775__K9_K1_K2_K11] ON [dbo].[catalog] 
(
	[dim_catalog_active] ASC,
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC,
	[sid_status_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [Catalog]
/****** Object:  Index [idx_catalog_code_trancode_dollars_imagelocation_imagealign_active_status_userinfo_brochure]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [idx_catalog_code_trancode_dollars_imagelocation_imagealign_active_status_userinfo_brochure] ON [dbo].[catalog] 
(
	[dim_catalog_code] ASC,
	[dim_catalog_trancode] ASC,
	[dim_catalog_dollars] ASC,
	[dim_catalog_imagelocation] ASC,
	[dim_catalog_imagealign] ASC,
	[dim_catalog_active] ASC,
	[sid_status_id] ASC,
	[sid_userinfo_id] ASC,
	[dim_catalog_brochure] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [Catalog]
/****** Object:  Index [idx_catalog_trancode_dollars_imagelocation_imagealign_active_status_userinfo_brochure_id]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [idx_catalog_trancode_dollars_imagelocation_imagealign_active_status_userinfo_brochure_id] ON [dbo].[catalog] 
(
	[sid_catalog_id] ASC,
	[dim_catalog_trancode] ASC,
	[dim_catalog_dollars] ASC,
	[dim_catalog_imagelocation] ASC,
	[dim_catalog_imagealign] ASC,
	[dim_catalog_active] ASC,
	[sid_status_id] ASC,
	[sid_userinfo_id] ASC,
	[dim_catalog_brochure] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [Catalog]
/****** Object:  Index [ix_catalog_catalog_code_includedCols]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [ix_catalog_catalog_code_includedCols] ON [dbo].[catalog] 
(
	[dim_catalog_code] ASC
)
INCLUDE ( [sid_catalog_id],
[dim_catalog_trancode],
[dim_catalog_dollars],
[dim_catalog_cashvalue],
[dim_catalog_cost],
[dim_catalog_shipping],
[dim_catalog_handling],
[dim_catalog_msrp],
[dim_catalog_HIshipping],
[dim_catalog_weight]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


USE [Catalog]
/****** Object:  Index [ix_catalog_sid_catalog_id_dim_catalog_code]    Script Date: 10/05/2010 17:36:24 ******/
CREATE NONCLUSTERED INDEX [ix_catalog_sid_catalog_id_dim_catalog_code] ON [dbo].[catalog] 
(
	[sid_catalog_id] ASC,
	[dim_catalog_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

/****** Object:  Trigger [dbo].[TRIG_Catalog_UPDATE]    Script Date: 10/05/2010 17:36:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_Catalog_UPDATE] ON [dbo].[catalog] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
/*
      DECLARE @sid_catalog_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_catalog_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_catalog_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
	INSERT INTO archivecatalog
	SELECT *, getdate(), getdate() FROM DELETED WHERE sid_catalog_id = @sid_catalog_id
        UPDATE Catalog SET dim_catalog_lastmodified = getdate() WHERE sid_catalog_id = @sid_catalog_id 
        FETCH NEXT FROM UPD_QUERY INTO @sid_catalog_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
*/
	INSERT INTO dbo.archivecatalog (sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created, dim_catalog_lastmodified, dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_cashvalue, sid_routing_id, dim_catalog_cost, dim_catalog_shipping, dim_catalog_handling, dim_catalog_msrp, dim_catalog_HIshipping, dim_catalog_weight,dim_archivecatalog_created,dim_archivecatalog_lastmodified)
	SELECT sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created, dim_catalog_lastmodified, dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_cashvalue, sid_routing_id, dim_catalog_cost, dim_catalog_shipping, dim_catalog_handling, dim_catalog_msrp, dim_catalog_HIshipping, dim_catalog_weight, getdate(), getdate()
	FROM deleted

	UPDATE c
		SET dim_catalog_lastmodified = getdate()
	FROM dbo.catalog c JOIN deleted del
		ON c.sid_catalog_id = del.sid_catalog_id
 END

GO

ALTER TABLE [dbo].[catalog]  WITH CHECK ADD  CONSTRAINT [FK_catalog_routing_sid_routing_id] FOREIGN KEY([sid_routing_id])
REFERENCES [dbo].[routing] ([sid_routing_id])
GO

ALTER TABLE [dbo].[catalog] CHECK CONSTRAINT [FK_catalog_routing_sid_routing_id]
GO

ALTER TABLE [dbo].[catalog]  WITH CHECK ADD  CONSTRAINT [FK_catalog_status_sid_status_id] FOREIGN KEY([sid_status_id])
REFERENCES [dbo].[status] ([sid_status_id])
GO

ALTER TABLE [dbo].[catalog] CHECK CONSTRAINT [FK_catalog_status_sid_status_id]
GO

ALTER TABLE [dbo].[catalog]  WITH CHECK ADD  CONSTRAINT [FK_catalog_userinfo_sid_userinfo-id] FOREIGN KEY([sid_userinfo_id])
REFERENCES [dbo].[userinfo] ([sid_userinfo_id])
GO

ALTER TABLE [dbo].[catalog] CHECK CONSTRAINT [FK_catalog_userinfo_sid_userinfo-id]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogCode]  DEFAULT ('ABC') FOR [dim_catalog_code]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogTrancode]  DEFAULT ('RM') FOR [dim_catalog_trancode]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogDollars]  DEFAULT (0.0) FOR [dim_catalog_dollars]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogImageLocation]  DEFAULT ('../image') FOR [dim_catalog_imagelocation]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_imagealign]  DEFAULT ('right') FOR [dim_catalog_imagealign]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogCreated]  DEFAULT (getdate()) FOR [dim_catalog_created]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogLastModified]  DEFAULT (getdate()) FOR [dim_catalog_lastmodified]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogActive]  DEFAULT (1) FOR [dim_catalog_active]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_Catalog_CatalogParentID]  DEFAULT ((-1)) FOR [dim_catalog_parentid]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_sdi_userinfo_id]  DEFAULT (0) FOR [sid_userinfo_id]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_brochure]  DEFAULT (0) FOR [dim_catalog_brochure]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_cashvalue]  DEFAULT ((0.00)) FOR [dim_catalog_cashvalue]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_sid_routing_id]  DEFAULT ((1)) FOR [sid_routing_id]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_cost]  DEFAULT ((0.00)) FOR [dim_catalog_cost]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_shipping0.00]  DEFAULT ((0.00)) FOR [dim_catalog_shipping]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_handling]  DEFAULT ((0.00)) FOR [dim_catalog_handling]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_msrp]  DEFAULT ((0.00)) FOR [dim_catalog_msrp]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_HIshipping]  DEFAULT ((0.00)) FOR [dim_catalog_HIshipping]
GO

ALTER TABLE [dbo].[catalog] ADD  CONSTRAINT [DF_catalog_dim_catalog_weight]  DEFAULT ((0.00)) FOR [dim_catalog_weight]
GO


