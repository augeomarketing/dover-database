USE [Catalog]
GO
/****** Object:  Table [dbo].[catalogcategory]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catalogcategory](
	[sid_catalogcategory_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[sid_category_id] [int] NOT NULL,
	[dim_catalogcategory_created] [datetime] NOT NULL,
	[dim_catalogcategory_lastmodified] [datetime] NOT NULL,
	[dim_catalogcategory_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_CatalogCategory] PRIMARY KEY CLUSTERED 
(
	[sid_catalogcategory_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalogcategory_179_331148225__K3_K6_K2_K1] ON [dbo].[catalogcategory] 
(
	[sid_category_id] ASC,
	[dim_catalogcategory_active] ASC,
	[sid_catalog_id] ASC,
	[sid_catalogcategory_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catalogcategory_catalog_category] ON [dbo].[catalogcategory] 
(
	[sid_category_id] ASC,
	[sid_catalog_id] ASC
) ON [PRIMARY]
GO
