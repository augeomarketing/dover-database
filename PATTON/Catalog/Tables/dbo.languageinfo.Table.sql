USE [Catalog]
GO
/****** Object:  Table [dbo].[languageinfo]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[languageinfo](
	[sid_languageinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_languageinfo_name] [varchar](20) NOT NULL,
	[dim_languageinfo_created] [datetime] NOT NULL,
	[dim_languageinfo_lastmodified] [datetime] NOT NULL,
	[dim_languageinfo_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_LanguageInfo] PRIMARY KEY CLUSTERED 
(
	[sid_languageinfo_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
