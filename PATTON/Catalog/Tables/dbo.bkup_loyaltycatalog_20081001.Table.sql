USE [Catalog]
GO
/****** Object:  Table [dbo].[bkup_loyaltycatalog_20081001]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bkup_loyaltycatalog_20081001](
	[dwid_loyaltycatalog_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_loyaltycatalog_id] [int] NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_loyaltycatalog_pointvalue] [int] NOT NULL,
	[dim_loyaltycatalog_bonus] [int] NOT NULL,
	[dim_loyaltycatalog_created] [datetime] NOT NULL,
	[dim_loyaltycatalog_lastmodified] [datetime] NOT NULL,
	[dim_loyaltycatalog_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL
) ON [PRIMARY]
GO
