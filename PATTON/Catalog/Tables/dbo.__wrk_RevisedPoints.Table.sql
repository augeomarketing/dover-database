USE [Catalog]
GO
/****** Object:  Table [dbo].[__wrk_RevisedPoints]    Script Date: 07/30/2009 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__wrk_RevisedPoints](
	[catalogcode] [varchar](20) NOT NULL,
	[ItemDescription] [varchar](255) NULL,
	[Re-REVISED PTS] [int] NULL,
 CONSTRAINT [PK___wrk_RevisedPoints] PRIMARY KEY CLUSTERED 
(
	[catalogcode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [ix_wrk_revisedpoints_catalogcode_points] ON [dbo].[__wrk_RevisedPoints] 
(
	[catalogcode] ASC,
	[Re-REVISED PTS] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
