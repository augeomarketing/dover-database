USE [Catalog]
GO
/****** Object:  Table [dbo].[archivecatalog]    Script Date: 07/30/2009 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archivecatalog](
	[sid_archivecatalog_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_catalog_code] [varchar](50) NOT NULL,
	[dim_catalog_trancode] [char](2) NOT NULL,
	[dim_catalog_dollars] [decimal](10, 2) NOT NULL,
	[dim_catalog_imagelocation] [varchar](1024) NOT NULL,
	[dim_catalog_imagealign] [varchar](50) NOT NULL,
	[dim_catalog_created] [datetime] NOT NULL,
	[dim_catalog_lastmodified] [datetime] NOT NULL,
	[dim_catalog_active] [datetime] NOT NULL,
	[dim_catalog_parentid] [int] NOT NULL,
	[sid_status_id] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_catalog_brochure] [int] NOT NULL,
	[dim_archivecatalog_created] [datetime] NOT NULL,
	[dim_archivecatalog_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_cataloghistory] PRIMARY KEY CLUSTERED 
(
	[sid_archivecatalog_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
