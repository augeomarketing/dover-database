USE [Catalog]
GO
/****** Object:  Table [dbo].[archivestatus]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archivestatus](
	[sid_archivestatus_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_status_id] [int] NOT NULL,
	[dim_status_name] [varchar](255) NOT NULL,
	[dim_status_description] [varchar](255) NULL,
	[dim_status_image] [varchar](255) NOT NULL,
	[dim_status_active] [int] NOT NULL,
	[dim_status_created] [datetime] NOT NULL,
	[dim_status_lastmodified] [datetime] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_archivestatus_created] [datetime] NOT NULL,
	[dim_archivestatus_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
