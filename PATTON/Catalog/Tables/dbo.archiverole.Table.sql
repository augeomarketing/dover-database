USE [Catalog]
GO
/****** Object:  Table [dbo].[archiverole]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archiverole](
	[sid_archiverole_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_role_id] [int] NOT NULL,
	[dim_role_name] [varchar](50) NOT NULL,
	[dim_role_description] [varchar](255) NOT NULL,
	[dim_role_created] [datetime] NOT NULL,
	[dim_role_lastmodified] [datetime] NOT NULL,
	[dim_role_active] [int] NOT NULL,
	[dim_archiverole_created] [datetime] NOT NULL,
	[dim_archiverole_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
