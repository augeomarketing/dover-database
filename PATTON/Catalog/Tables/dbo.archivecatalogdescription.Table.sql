USE [Catalog]
GO
/****** Object:  Table [dbo].[archivecatalogdescription]    Script Date: 07/30/2009 11:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archivecatalogdescription](
	[sid_archivecatalogdescription_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalogdescription_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_catalogdescription_name] [varchar](1024) NOT NULL,
	[dim_catalogdescription_description] [varchar](4096) NOT NULL,
	[sid_languageinfo_id] [int] NOT NULL,
	[dim_catalogdescription_created] [datetime] NOT NULL,
	[dim_catalogdescription_lastmodified] [datetime] NOT NULL,
	[dim_catalogdescription_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_archivecatalogdescription_created] [datetime] NOT NULL,
	[dim_archivecatalogdescription_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
