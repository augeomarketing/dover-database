USE [Catalog]
GO
/****** Object:  Table [dbo].[archivepermissiongroupuserinfo]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[archivepermissiongroupuserinfo](
	[sid_archivepermissiongroupuserinfo_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_permissiongroupuserinfo_id] [int] NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_permissiongroupuserinfo_created] [datetime] NOT NULL,
	[dim_permissiongroup_lastmodified] [datetime] NOT NULL,
	[dim_permissiongroupuserinfo_active] [int] NOT NULL,
	[dim_archivepermissiongroupuserinfo_created] [datetime] NOT NULL,
	[dim_archivepermissiongroup_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]
GO
