USE [Catalog]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_fipricing_groupinfo_sid_groupinfo_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[fipricing]'))
ALTER TABLE [dbo].[fipricing] DROP CONSTRAINT [FK_fipricing_groupinfo_sid_groupinfo_id]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_fipricing_pricetype_sid_pricetype_id]') AND parent_object_id = OBJECT_ID(N'[dbo].[fipricing]'))
ALTER TABLE [dbo].[fipricing] DROP CONSTRAINT [FK_fipricing_pricetype_sid_pricetype_id]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_fipricing_dim_fipricing_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[fipricing] DROP CONSTRAINT [DF_fipricing_dim_fipricing_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_dim_pricing_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[fipricing] DROP CONSTRAINT [DF_Table_1_dim_pricing_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Table_1_dim_pricing_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[fipricing] DROP CONSTRAINT [DF_Table_1_dim_pricing_active]
END

GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[fipricing]    Script Date: 07/01/2010 17:27:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fipricing]') AND type in (N'U'))
DROP TABLE [dbo].[fipricing]
GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[fipricing]    Script Date: 07/01/2010 17:27:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[fipricing](
	[sid_groupinfo_id] [int] NOT NULL,
	[sid_pricetype_id] [int] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[dim_fipricing_value] [decimal](18, 4) NOT NULL,
	[dim_fipricing_created] [datetime] NOT NULL,
	[dim_fipricing_lastmodified] [datetime] NOT NULL,
	[dim_fipricing_active] [int] NOT NULL,
 CONSTRAINT [PK_fipricing] PRIMARY KEY CLUSTERED 
(
	[sid_groupinfo_id] ASC,
	[sid_pricetype_id] ASC,
	[sid_dbprocessinfo_dbnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[TRIG_fipricing_UPDATE]    Script Date: 07/01/2010 17:27:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_fipricing_UPDATE] ON [dbo].[fipricing] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 

	UPDATE fipricing
      SET dim_fipricing_lastmodified = getdate()
		FROM dbo.fipricing INNER JOIN deleted del
      ON fipricing.sid_groupinfo_id = del.sid_groupinfo_id
        AND fipricing.sid_pricetype_id = del.sid_pricetype_id
        AND fipricing.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber

END

GO

ALTER TABLE [dbo].[fipricing]  WITH CHECK ADD  CONSTRAINT [FK_fipricing_groupinfo_sid_groupinfo_id] FOREIGN KEY([sid_groupinfo_id])
REFERENCES [dbo].[groupinfo] ([sid_groupinfo_id])
GO

ALTER TABLE [dbo].[fipricing] CHECK CONSTRAINT [FK_fipricing_groupinfo_sid_groupinfo_id]
GO

ALTER TABLE [dbo].[fipricing]  WITH CHECK ADD  CONSTRAINT [FK_fipricing_pricetype_sid_pricetype_id] FOREIGN KEY([sid_pricetype_id])
REFERENCES [dbo].[pricetype] ([sid_pricetype_id])
GO

ALTER TABLE [dbo].[fipricing] CHECK CONSTRAINT [FK_fipricing_pricetype_sid_pricetype_id]
GO

ALTER TABLE [dbo].[fipricing] ADD  CONSTRAINT [DF_fipricing_dim_fipricing_created]  DEFAULT (getdate()) FOR [dim_fipricing_created]
GO

ALTER TABLE [dbo].[fipricing] ADD  CONSTRAINT [DF_Table_1_dim_pricing_lastmodified]  DEFAULT (getdate()) FOR [dim_fipricing_lastmodified]
GO

ALTER TABLE [dbo].[fipricing] ADD  CONSTRAINT [DF_Table_1_dim_pricing_active]  DEFAULT ((1)) FOR [dim_fipricing_active]
GO


