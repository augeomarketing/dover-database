USE [Catalog]
GO
/****** Object:  Table [dbo].[editcatalogstate]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[editcatalogstate](
	[sid_editcatalogstate_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_editcatalog_id] [int] NOT NULL,
	[sid_state_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_editcatalogstate_created] [datetime] NOT NULL,
	[dim_editcatalogstate_lastmodified] [datetime] NOT NULL,
	[dim_editcatalogstate_active] [int] NOT NULL,
 CONSTRAINT [PK_editcatalogstate] PRIMARY KEY CLUSTERED 
(
	[sid_editcatalogstate_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_editcatalogstate_179_836198029__K2_K3] ON [dbo].[editcatalogstate] 
(
	[sid_editcatalog_id] ASC,
	[sid_state_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
