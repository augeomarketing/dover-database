USE [Catalog]
GO
/****** Object:  Table [dbo].[loyaltycatalog]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[loyaltycatalog](
	[dwid_loyaltycatalog_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_loyaltycatalog_id] [int] NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_loyaltycatalog_pointvalue] [int] NOT NULL,
	[dim_loyaltycatalog_bonus] [int] NOT NULL,
	[dim_loyaltycatalog_created] [datetime] NOT NULL,
	[dim_loyaltycatalog_lastmodified] [datetime] NOT NULL,
	[dim_loyaltycatalog_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_loyaltycatalog_1] PRIMARY KEY CLUSTERED 
(
	[dwid_loyaltycatalog_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_loyaltycatalog_179_1204199340__K3_K4_K5_K6] ON [dbo].[loyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC,
	[dim_loyaltycatalog_pointvalue] ASC,
	[dim_loyaltycatalog_bonus] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_loyaltycatalog_lid_cid_active_point] ON [dbo].[loyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC,
	[dim_loyaltycatalog_active] ASC,
	[dim_loyaltycatalog_pointvalue] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_loyaltycatlaog_sid_loyalty_id_sid_catalog_id] ON [dbo].[loyaltycatalog] 
(
	[sid_loyalty_id] ASC,
	[sid_catalog_id] ASC,
	[dim_loyaltycatalog_pointvalue] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
