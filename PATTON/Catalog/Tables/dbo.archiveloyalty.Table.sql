USE [Catalog]
GO
/****** Object:  Table [dbo].[archiveloyalty]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archiveloyalty](
	[sid_archiveloyalty_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_loyalty_id] [int] NOT NULL,
	[dim_loyalty_name] [varchar](50) NOT NULL,
	[dim_loyalty_description] [varchar](250) NOT NULL,
	[dim_loyalty_liab] [int] NOT NULL,
	[dim_loyalty_created] [datetime] NOT NULL,
	[dim_loyalty_lastmodified] [datetime] NOT NULL,
	[dim_loyalty_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_archiveloyalty_created] [datetime] NOT NULL,
	[dim_archiveloyalty_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_archiveloyalty] PRIMARY KEY CLUSTERED 
(
	[sid_archiveloyalty_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_Description', @value=N'Loyalty In A Box' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'archiveloyalty', @level2type=N'COLUMN',@level2name=N'dim_loyalty_liab'
GO
