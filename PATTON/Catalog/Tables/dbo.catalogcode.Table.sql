USE [Catalog]
GO

/****** Object:  Table [dbo].[catalogcode]    Script Date: 7/1/2015 12:44:55 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[catalogcode]') AND type in (N'U'))
DROP TABLE [dbo].[catalogcode]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[catalogcode](
	[sid_catalogcode_id] [int] IDENTITY(1,1) NOT NULL,
	[long_catalog_code] [varchar](100) NOT NULL,
	[dim_catalog_code] [varchar](20) NOT NULL,
 CONSTRAINT [PK_catalogcode] PRIMARY KEY CLUSTERED 
(
	[sid_catalogcode_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



