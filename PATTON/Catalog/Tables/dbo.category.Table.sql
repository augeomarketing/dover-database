USE [Catalog]
GO
/****** Object:  Table [dbo].[category]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[category](
	[sid_category_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_category_name] [varchar](30) NOT NULL,
	[dim_category_description] [varchar](300) NOT NULL,
	[dim_category_imagelocation] [varchar](1024) NULL,
	[dim_category_created] [datetime] NOT NULL,
	[dim_category_lastmodified] [datetime] NOT NULL,
	[dim_category_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
	[dim_category_imagehighdef] [varchar](1024) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[sid_category_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
