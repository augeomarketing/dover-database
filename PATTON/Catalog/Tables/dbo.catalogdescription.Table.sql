USE [Catalog]
GO
/****** Object:  Table [dbo].[catalogdescription]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catalogdescription](
	[sid_catalogdescription_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_catalog_id] [int] NOT NULL,
	[dim_catalogdescription_name] [varchar](1024) NOT NULL,
	[dim_catalogdescription_description] [varchar](4096) NOT NULL,
	[sid_languageinfo_id] [int] NOT NULL,
	[dim_catalogdescription_created] [datetime] NOT NULL,
	[dim_catalogdescription_lastmodified] [datetime] NOT NULL,
	[dim_catalogdescription_active] [int] NOT NULL,
	[sid_userinfo_id] [int] NOT NULL,
 CONSTRAINT [PK_CatalogDescription] PRIMARY KEY CLUSTERED 
(
	[sid_catalogdescription_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [_dta_index_catalogdescription_179_340196262__K5_K2] ON [dbo].[catalogdescription] 
(
	[sid_languageinfo_id] ASC,
	[sid_catalog_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_catalogdescription_cid_name] ON [dbo].[catalogdescription] 
(
	[sid_catalog_id] ASC,
	[dim_catalogdescription_name] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
