USE [Catalog]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cataloggroupinfo_dim_cataloggroupinfo_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cataloggroupinfo] DROP CONSTRAINT [DF_cataloggroupinfo_dim_cataloggroupinfo_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_cataloggroupinfo_dim_cataloggroupinfo_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cataloggroupinfo] DROP CONSTRAINT [DF_cataloggroupinfo_dim_cataloggroupinfo_lastmodified]
END

GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[cataloggroupinfo]    Script Date: 08/10/2011 17:57:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cataloggroupinfo]') AND type in (N'U'))
DROP TABLE [dbo].[cataloggroupinfo]
GO

USE [Catalog]
GO

/****** Object:  Table [dbo].[cataloggroupinfo]    Script Date: 08/10/2011 17:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cataloggroupinfo](
	[sid_catalog_id] [int] NOT NULL,
	[sid_groupinfo_id] [int] NOT NULL,
	[dim_cataloggroupinfo_created] [datetime] NOT NULL,
	[dim_cataloggroupinfo_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_cataloggroupinfo] PRIMARY KEY CLUSTERED 
(
	[sid_catalog_id] ASC,
	[sid_groupinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[cataloggroupinfo] ADD  CONSTRAINT [DF_cataloggroupinfo_dim_cataloggroupinfo_created]  DEFAULT (getdate()) FOR [dim_cataloggroupinfo_created]
GO

ALTER TABLE [dbo].[cataloggroupinfo] ADD  CONSTRAINT [DF_cataloggroupinfo_dim_cataloggroupinfo_lastmodified]  DEFAULT (getdate()) FOR [dim_cataloggroupinfo_lastmodified]
GO


