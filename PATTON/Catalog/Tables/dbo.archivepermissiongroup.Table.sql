USE [Catalog]
GO
/****** Object:  Table [dbo].[archivepermissiongroup]    Script Date: 07/30/2009 11:45:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[archivepermissiongroup](
	[sid_archivepermissiongroup_id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sid_permissiongroup_id] [int] NOT NULL,
	[dim_permissiongroup_name] [varchar](50) NOT NULL,
	[dim_permissiongroup_description] [varchar](255) NOT NULL,
	[dim_permissiongroup_created] [datetime] NOT NULL,
	[dim_permissiongroup_lastmodified] [datetime] NOT NULL,
	[dim_permissiongroup_active] [int] NOT NULL,
	[dim_archivepermissiongroup_created] [datetime] NOT NULL,
	[dim_archivepermissiongroup_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_archivepermissiongroup] PRIMARY KEY CLUSTERED 
(
	[sid_archivepermissiongroup_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
