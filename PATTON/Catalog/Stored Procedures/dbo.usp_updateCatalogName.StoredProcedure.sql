USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateCatalogName]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_updateCatalogName]
	@catalogId INT, @catalogName VARCHAR(100), @languageId INT, @userId INT
AS 
BEGIN 
	UPDATE editcatalogdescription SET dim_catalogdescription_name = @catalogName, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId
	IF(@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO editcatalogdescription
		SELECT TOP 1 * FROM catalogDescription WHERE sid_catalog_id = @catalogId
		IF(@@ROWCOUNT = 0)
		BEGIN
			INSERT INTO catalogdescription (dim_catalogdescription_name, dim_catalogdescription_description, sid_languageinfo_id, sid_userinfo_id, dim_catalogdescription_active, sid_catalog_id)
			VALUES (@catalogName, '', @languageId, @userId, 0, @catalogId)

			INSERT INTO editcatalogdescription (sid_catalogdescription_id, dim_catalogdescription_name, dim_catalogdescription_description, sid_languageinfo_id, sid_userinfo_id, dim_catalogdescription_active, sid_catalog_id)
			VALUES (SCOPE_IDENTITY(), @catalogName, '', @languageId, @userId, 1, @catalogId)
		END
		UPDATE editcatalogdescription SET dim_catalogdescription_name = @catalogName, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId
	END
END
GO
