USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_approveItem]    Script Date: 04/19/2011 13:47:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[usp_approveItem]
	@catalogId INT, @languageId INT, @userId INT
AS 
BEGIN 
  /*
    APPROVE CATALOG
  */
--print 'AC'
  UPDATE catalog.dbo.catalog
    SET dim_catalog_code = a.dim_catalog_code,
        dim_catalog_trancode = a.dim_catalog_trancode,
        dim_catalog_dollars = a.dim_catalog_dollars,
        dim_catalog_imagelocation = a.dim_catalog_imagelocation,
        dim_catalog_imagealign = a.dim_catalog_imagealign,
        dim_catalog_active = a.dim_catalog_active,
        sid_status_id = a.sid_status_id,
        sid_userinfo_id = a.sid_userinfo_id,
        dim_catalog_brochure = a.dim_catalog_brochure,
        dim_catalog_featured = a.dim_catalog_featured,
        sid_routing_id = a.sid_routing_id,
        dim_catalog_shipping = a.dim_catalog_shipping,
        dim_catalog_HIshipping = a.dim_catalog_HIshipping,
        dim_catalog_cashvalue = a.dim_catalog_cashvalue,
        dim_catalog_msrp = a.dim_catalog_msrp,
        dim_catalog_handling = a.dim_catalog_handling,
        dim_catalog_cost = a.dim_catalog_cost,
        dim_catalog_weight = a.dim_catalog_weight
    FROM
      catalog.dbo.editcatalog a
        INNER JOIN catalog.dbo.catalog b 
          ON a.sid_Catalog_id = b.sid_catalog_id
    WHERE a.sid_Catalog_id = @catalogid

  INSERT INTO catalog.catalog.dbo.editcatalog (sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_active, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_featured, sid_routing_id, dim_catalog_shipping, dim_catalog_HIshipping ,dim_catalog_msrp ,dim_catalog_handling,dim_catalog_cost ,dim_catalog_weight,dim_catalog_cashvalue )
  SELECT sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_active, sid_status_id, sid_userinfo_id, dim_catalog_brochure, dim_catalog_featured, sid_routing_id, dim_catalog_shipping, dim_catalog_HIshipping ,dim_catalog_msrp ,dim_catalog_handling,dim_catalog_cost ,dim_catalog_weight, dim_catalog_cashvalue
  FROM catalog.dbo.catalog
  WHERE sid_catalog_id = @catalogid

  DELETE FROM catalog.dbo.editcatalog 
    WHERE sid_catalog_id = @catalogId

  /*
    APPROVE CATALOGDESCRIPTION
  */
--print 'ACD'
  UPDATE catalog.dbo.catalogdescription
    SET dim_catalogdescription_name = a.dim_catalogdescription_name,
        dim_catalogdescription_description = a.dim_catalogdescription_description,
        sid_userinfo_id = a.sid_userinfo_id
    FROM
      catalog.dbo.editcatalogdescription a
        INNER JOIN catalog.dbo.catalogdescription b
          ON a.sid_catalog_id = b.sid_catalog_id
            AND a.sid_languageinfo_id = b.sid_languageinfo_id
    WHERE a.sid_catalog_id = @catalogId
      AND a.sid_languageinfo_id = @languageId

  INSERT INTO catalog.catalog.dbo.editcatalogdescription (sid_catalogdescription_id, sid_catalog_id, dim_catalogdescription_name, dim_catalogdescription_description, sid_userinfo_id, sid_languageinfo_id)
  SELECT sid_catalogdescription_id, sid_catalog_id, dim_catalogdescription_name, dim_catalogdescription_description, sid_userinfo_id, sid_languageinfo_id 
    FROM catalog.dbo.catalogdescription
    WHERE sid_catalog_id = @catalogId
      AND sid_languageinfo_id = @languageId

  DELETE FROM catalog.dbo.editcatalogdescription
    WHERE sid_catalog_id = @catalogId
      AND sid_languageinfo_id = @languageId

  /*
    APPROVE CATALOGCATEGORY
  */
  --print 'ACC'
  DECLARE @CATCOUNT INT
  SELECT @CATCOUNT = COUNT(dim_catalogcategory_active) FROM catalog.dbo.editcatalogcategory WHERE sid_catalog_id = @catalogId

  IF(@CATCOUNT > 0)
  BEGIN
    UPDATE catalog.dbo.catalogcategory
      SET dim_catalogcategory_active = a.dim_catalogcategory_active
      FROM
        catalog.dbo.editcatalogcategory a
          INNER JOIN catalog.dbo.catalogcategory b
            ON a.sid_catalog_id = b.sid_catalog_id
            AND a.sid_category_id = b.sid_category_id
      WHERE a.sid_catalog_id = @catalogId
      
    UPDATE catalog.dbo.catalogcategory
      SET dim_catalogcategory_active = 0
      WHERE sid_catalog_id = @catalogId
      AND dim_catalogcategory_active = 1
      AND sid_category_id NOT IN (
                                    SELECT sid_category_id 
                                      FROM  catalog.dbo.editcatalogcategory 
                                      WHERE sid_catalog_id = @catalogId)

    INSERT INTO catalog.dbo.catalogcategory (sid_catalog_id, sid_category_id, sid_userinfo_id)
    SELECT sid_catalog_id, sid_category_id, sid_userinfo_id
      FROM catalog.dbo.editcatalogcategory 
      WHERE sid_catalog_id = @catalogId
      AND sid_category_id NOT IN (SELECT sid_category_id 
                                    FROM  catalog.dbo.catalogcategory 
                                    WHERE sid_catalog_id = @catalogId)
  END

  INSERT INTO catalog.catalog.dbo.editcatalogcategory (sid_catalogcategory_id, sid_catalog_id, sid_category_id, sid_userinfo_id, dim_catalogcategory_active)
  SELECT sid_catalogcategory_id, sid_catalog_id, sid_category_id, sid_userinfo_id, dim_catalogcategory_active
    FROM catalog.dbo.catalogcategory 
    WHERE sid_catalog_id = @catalogId

  DELETE FROM catalog.dbo.editcatalogcategory 
    WHERE sid_catalog_id = @catalogId

  /*
    APPROVE LOYALTYCATALOG
  */
  --print 'ALC'
  UPDATE catalog.dbo.loyaltycatalog
    SET dim_loyaltycatalog_active = a.dim_loyaltycatalog_active,
        dim_loyaltycatalog_pointvalue = a.dim_loyaltycatalog_pointvalue,
        dim_loyaltycatalog_bonus = a.dim_loyaltycatalog_bonus,
        sid_userinfo_id = a.sid_userinfo_id
    FROM
      catalog.dbo.editloyaltycatalog a
        INNER JOIN catalog.dbo.loyaltycatalog b
          ON a.sid_loyaltycatalog_id = b.sid_loyaltycatalog_id
    WHERE a.sid_catalog_id = @catalogId

  INSERT INTO  catalog.catalog.dbo.editloyaltycatalog (dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus,sid_userinfo_id, dim_loyaltycatalog_active)
  SELECT dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, sid_userinfo_id, dim_loyaltycatalog_active
    FROM catalog.dbo.loyaltycatalog 
    WHERE sid_catalog_id = @catalogId


  DELETE FROM catalog.dbo.editloyaltycatalog 
    WHERE sid_catalog_id = @catalogId

  /*
    INSERT APPROVAL
  */
  --print 'A'
  INSERT INTO catalog.dbo.approve (sid_catalog_id, sid_userinfo_id)
  VALUES (@catalogId, @userId)

  INSERT INTO catalog.catalog.dbo.ready (sid_catalog_id, sid_userinfo_id, sid_languageinfo_id)
  VALUES (@catalogId, @userId, @languageid)

END


