USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_CategoryBrand_Get]    Script Date: 7/1/2015 1:02:32 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CategoryBrand_Get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CategoryBrand_Get]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 6.10.2015
-- Description:	Gets all the Augeo brand numbers
-- =============================================
CREATE PROCEDURE [dbo].[usp_CategoryBrand_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT
		branding_number AS BrandingNumber
	FROM
		dbo.categorybrand
	ORDER BY 
		BrandingNumber
END


GO



