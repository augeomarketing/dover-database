USE catalog

if exists(select 1 from dbo.sysobjects where name = 'usp_PointValueUpdate' and xtype = 'P')
	drop procedure dbo.usp_PointValueUpdate
GO

-- =============================================
-- Author:		Shawn Smith
-- Create date: November 30, 2009
-- Description:	Calculates Point Tier
-- =============================================
CREATE PROCEDURE dbo.usp_PointValueUpdate
@sid_catalog_id int = 0, 
@dim_catalog_code varchar(20) = ' ',             
@sid_vendor_id int = 0,                 
@dim_pricing_cost money =  0,           
@dim_pricing_shipping money =  0,       
@dim_pricing_handling money =  0,       
@dim_pricing_MSRP money =  0,           
@dim_pricing_HIshipping money =  0,     
@dim_pricing_weight int =  0  

AS
 
UPDATE catalog.dbo.PRICING
SET dim_catalog_code = @dim_catalog_code, sid_vendor_id = @sid_vendor_id, dim_pricing_cost = @dim_pricing_cost, dim_pricing_shipping = @dim_pricing_shipping, dim_pricing_handling = @dim_pricing_handling, dim_pricing_MSRP = @dim_pricing_MSRP, dim_pricing_HIshipping = @dim_pricing_HIshipping, dim_pricing_weight = @dim_pricing_weight
WHERE sid_catalog_id = @sid_catalog_id

IF @@ROWCOUNT = 0
	INSERT INTO catalog.dbo.PRICING (sid_catalog_id, dim_catalog_code, sid_vendor_id, dim_pricing_cost, dim_pricing_shipping, dim_pricing_handling, dim_pricing_MSRP, dim_pricing_HIshipping, dim_pricing_weight)
		VALUES(@sid_catalog_id, @dim_catalog_code, @sid_vendor_id, @dim_pricing_cost,@dim_pricing_shipping ,@dim_pricing_handling,@dim_pricing_MSRP, @dim_pricing_HIshipping ,@dim_pricing_weight)
GO

/*
EXEC dbo.usp_PointValueUpdate 1, 'BFG9000', 1,19,20,21,90,5,6

Select *   from catalog.dbo.PRICING
where sid_catalog_id = 1
order by dim_pricing_created
*/