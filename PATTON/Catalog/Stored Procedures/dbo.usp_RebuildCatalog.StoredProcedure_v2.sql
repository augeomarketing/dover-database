USE [Catalog]
GO

if exists(select 1 from dbo.sysobjects where name = 'usp_RebuildCatalog' and xtype = 'P')
    drop procedure dbo.usp_RebuildCatalog_v2
GO


/****** Object:  StoredProcedure [dbo].[usp_RebuildCatalog]    Script Date: 08/04/2009 10:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_RebuildCatalog_v2]
  @templateTipnumber VARCHAR(3),
  @newTipnumber VARCHAR(3),
  @groupList Varchar(20) = ''
    
AS
BEGIN
  DECLARE @sid_loyaltycatalog_id_seed INT
  DECLARE @sql NVARCHAR(4000)
  DECLARE @loyaltyid	INT
  DECLARE @template_sid  INT

  DECLARE @debug INT
  SET @debug = 1

  SET @template_sid =  (SELECT TOP 1 sid_loyalty_id FROM catalog.dbo.loyaltytip WITH(NOLOCK) WHERE dim_loyaltytip_prefix = @templateTipnumber AND dim_loyaltytip_active = 1 )
  SET @loyaltyid = (SELECT TOP 1 sid_loyalty_id FROM catalog.dbo.loyaltytip WITH(NOLOCK) WHERE dim_loyaltytip_prefix = @newTipnumber AND dim_loyaltytip_active = 1)
  IF @debug = 1 
  BEGIN
    PRINT 'Template Loyalty Id: ' + CONVERT(VARCHAR(20), @template_sid)
    PRINT 'New Loyalty Id: ' + CONVERT(VARCHAR(20), @loyaltyid)
  END
  
  -- get max sid_loyaltycatalog_id from loyaltycatalog id.  increment by 1.  this
  -- becomes the identity seed for the temp table
  SET @sid_loyaltycatalog_id_seed = (SELECT MAX(sid_loyaltycatalog_id) + 1 FROM catalog.dbo.loyaltycatalog)
  IF @debug = 1 
  BEGIN
    PRINT 'Seed: ' + CONVERT(VARCHAR(20), @sid_loyaltycatalog_id_seed)
  END

  -- drop table if it exists.  This is necessary so the identity seed can be set properly
  -- truncate won't work
  IF exists(SELECT 1 FROM catalog.dbo.sysobjects WHERE [name] = 'tmp_loyaltycatalog')
  BEGIN
    IF @debug = 1 
    BEGIN
      PRINT 'DROP tmp_loyaltycatalog NEEDED'
    END
	  DROP TABLE catalog.dbo.tmp_loyaltycatalog
    IF @debug = 1 
    BEGIN
      PRINT 'DROP COMPLETE'
    END
  END
  -- Dynamic SQL to create table.  Using dynamic SQL to allow us to specify the identity seed
  SET @sql = '
	  CREATE TABLE catalog.[dbo].[tmp_loyaltycatalog](
		  [sid_loyaltycatalog_id] [INT] IDENTITY(' + CAST(@sid_loyaltycatalog_id_seed AS NVARCHAR(10)) + ', 1) NOT NULL,
		  [sid_loyalty_id] [INT] NOT NULL,
		  [sid_catalog_id] [INT] NOT NULL,
		  [dim_loyaltycatalog_pointvalue] [INT],
		  [dim_loyaltycatalog_bonus] [INT],
		  [dim_loyaltycatalog_active] [INT]
	  )'
  EXEC sp_executesql @sql
  SET @sql = 'CREATE INDEX idx_tmp_cover ON catalog.[dbo].[tmp_loyaltycatalog] (sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)'
  EXEC sp_executesql @sql


  IF @debug = 1 
  BEGIN
    PRINT 'Adding Items to TMP table'
  END

  INSERT INTO catalog.dbo.tmp_loyaltycatalog
  (sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
  SELECT DISTINCT @loyaltyid, a.sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
    FROM catalog.dbo.loyaltycatalog a WITH(NOLOCK) 
      INNER JOIN catalog.dbo.catalogcategory b WITH(NOLOCK) 
        ON a.sid_catalog_id = b.sid_catalog_id
      INNER JOIN catalog.dbo.categorygroupinfo c WITH(NOLOCK) 
        ON b.sid_category_id = c.sid_category_id
    WHERE sid_loyalty_id = @template_sid  
      AND	dim_loyaltycatalog_active = 1
      AND a.sid_catalog_id NOT IN (SELECT sid_catalog_id FROM catalog.dbo.loyaltycatalog WHERE sid_loyalty_id = @loyaltyid)
--      AND c.sid_groupinfo_id IN (SELECT item FROM rewardsnow.dbo.ufn_split(@grouplist))

  IF @debug = 1 
  BEGIN
    PRINT 'DONE Adding Items to TMP table'
  END
      

    ----------------------------------------------------------------------
  -- Now insert result set INTO the catalog.dbo.loyaltycatalog table
  ----------------------------------------------------------------------
  IF @debug = 1 
  BEGIN
    PRINT 'INSERT into local LoyaltyCatalog'
  END

  INSERT INTO catalog.dbo.loyaltycatalog
    (sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
  SELECT DISTINCT sid_loyaltycatalog_id, sid_loyalty_id, a.sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active
    FROM catalog.dbo.tmp_loyaltycatalog a WITH(NOLOCK) 
      INNER JOIN catalog.dbo.catalogcategory b WITH(NOLOCK) 
        ON a.sid_catalog_id = b.sid_catalog_id
      INNER JOIN catalog.dbo.categorygroupinfo c WITH(NOLOCK) 
        ON b.sid_category_id = c.sid_category_id
    WHERE c.sid_groupinfo_id IN (SELECT item FROM rewardsnow.dbo.ufn_split(@grouplist, ','))  

  IF @debug = 1 
  BEGIN
    PRINT 'INSERT into remote LoyaltyCatalog'
  END


  -- Add new loyalty in a box program to RN1
  INSERT INTO RN1.catalog.dbo.loyaltycatalog 
    (dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active)
  SELECT DISTINCT lc.dwid_loyaltycatalog_id, lc.sid_loyaltycatalog_id, lc.sid_loyalty_id, lc.sid_catalog_id, lc.dim_loyaltycatalog_pointvalue, lc.dim_loyaltycatalog_bonus, lc.dim_loyaltycatalog_active

    FROM catalog.dbo.loyaltycatalog lc left outer join rn1.catalog.dbo.loyaltycatalog tlc
        ON tlc.sid_loyaltycatalog_id = lc.sid_loyaltycatalog_id
          AND tlc.sid_loyalty_id = lc.sid_loyalty_id
          AND tlc.sid_catalog_id = lc.sid_catalog_id

      INNER JOIN catalog.dbo.catalogcategory b WITH(NOLOCK) 
        ON lc.sid_catalog_id = b.sid_catalog_id

      INNER JOIN catalog.dbo.categorygroupinfo c WITH(NOLOCK) 
        ON b.sid_category_id = c.sid_category_id

    WHERE tlc.sid_loyaltycatalog_id is null
    and lc.sid_loyalty_id = @loyaltyid
    and c.sid_groupinfo_id IN (SELECT item FROM rewardsnow.dbo.ufn_split(@grouplist, ','))  
END
