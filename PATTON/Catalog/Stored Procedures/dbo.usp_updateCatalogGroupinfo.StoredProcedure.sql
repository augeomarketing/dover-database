USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateCatalogGroupinfo]    Script Date: 06/12/2013 16:05:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_updateCatalogGroupinfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_updateCatalogGroupinfo]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateCatalogGroupinfo]    Script Date: 06/12/2013 16:05:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_updateCatalogGroupinfo]
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO catalog.dbo.cataloggroupinfo (sid_catalog_id, sid_groupinfo_id)
	SELECT DISTINCT c.sid_catalog_id, cg.sid_groupinfo_id 
	FROM catalog.dbo.catalog c 
	INNER JOIN catalog.dbo.catalogcategory cc 
		ON c.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN catalog.dbo.categorygroupinfo cg 
		ON cc.sid_category_id = cg.sid_category_id
	LEFT OUTER JOIN catalog.dbo.cataloggroupinfo cgi
		ON c.sid_catalog_id = cgi.sid_catalog_id
	WHERE cgi.sid_catalog_id IS NULL 
		AND dim_catalog_active = 1
		AND dim_catalogcategory_active = 1
		AND sid_status_id = 1


	INSERT INTO RN1.catalog.dbo.cataloggroupinfo (sid_catalog_id, sid_groupinfo_id)
	SELECT DISTINCT c.sid_catalog_id, cg.sid_groupinfo_id 
	FROM catalog.dbo.catalog c 
	INNER JOIN catalog.dbo.catalogcategory cc 
		ON c.sid_catalog_id = cc.sid_catalog_id
	INNER JOIN catalog.dbo.categorygroupinfo cg 
		ON cc.sid_category_id = cg.sid_category_id
	LEFT OUTER JOIN RN1.catalog.dbo.cataloggroupinfo cgi
		ON c.sid_catalog_id = cgi.sid_catalog_id
	WHERE cgi.sid_catalog_id IS NULL 
		AND dim_catalog_active = 1
		AND dim_catalogcategory_active = 1
		AND sid_status_id = 1
	
END

GO


-- exec catalog.dbo.usp_updateCatalogGroupinfo
-- When run, will add sid_catalog_id/sid_groupinfo_id combos to table, dated for runtime