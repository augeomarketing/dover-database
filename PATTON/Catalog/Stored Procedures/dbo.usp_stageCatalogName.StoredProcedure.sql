USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_stageCatalogName]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_stageCatalogName]
	@catalogid INT, @itemName varchar(255), @NewCatalogDescriptionId INT output
AS 
BEGIN 
	DECLARE @NewEditCatalogId INT
	INSERT INTO catalog.dbo.catalogdescription (sid_catalog_id, dim_catalogdescription_name, dim_catalogdescription_description,sid_languageinfo_id, sid_userinfo_id)  
	VALUES (@catalogid, @itemname, '',1,1)
	SET @NewCatalogDescriptionId = SCOPE_IDENTITY()		
	INSERT INTO catalog.dbo.editcatalogdescription (sid_catalogdescription_id, sid_catalog_id, dim_catalogdescription_name, dim_catalogdescription_description,sid_languageinfo_id, sid_userinfo_id)  
	VALUES (@NewCatalogDescriptionId, @catalogid, @itemname, '',1,1)
	
	SELECT @NewEditCatalogId = sid_editcatalog_id FROM editcatalog WHERE sid_catalog_id = @catalogid

	UPDATE editcatalogstate
		SET sid_state_id = 1
		WHERE sid_editcatalog_id = @NewEditCatalogId
	IF(@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO editcatalogstate (sid_catalog_id, sid_editcatalog_id, sid_state_id)
		VALUES (@catalogid, @NewEditCatalogId, 1)
	END
END
GO
