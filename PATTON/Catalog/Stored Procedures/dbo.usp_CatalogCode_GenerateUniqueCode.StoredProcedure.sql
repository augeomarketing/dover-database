USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_CatalogCode_GenerateUniqueCode]    Script Date: 7/1/2015 1:17:07 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CatalogCode_GenerateUniqueCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CatalogCode_GenerateUniqueCode]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 6.28.2015
-- Description:	Generates and returns a shortened 
--              unique catalog code to be put in 
--              the catalog table.
--
-- Note:  Only gets called for catalog code 
--        longer than 20 characters in length.
-- =============================================
CREATE PROCEDURE [dbo].[usp_CatalogCode_GenerateUniqueCode] 
	-- Add the parameters for the stored procedure here
	@LongCatalogCode VARCHAR(100),
	@UniquePrefix VARCHAR(4) = 'PRE-'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @NextCatalogCode VARCHAR(20)
	SELECT @NextCatalogCode = dim_catalog_code FROM dbo.catalogCode WHERE @LongCatalogCode = long_catalog_code

	IF @NextCatalogCode IS NOT NULL
	BEGIN
		SELECT @NextCatalogCode AS CatalogCode
		RETURN
	END

	-- Generate unique identifier to form new
	DECLARE @Filler VARCHAR(20) = '--------------------' 
	DECLARE @NextSeedValue INTEGER
	SELECT @NextSeedValue = IDENT_CURRENT('catalogcode')

	IF (SELECT COUNT(1) FROM catalogcode) > 0
	BEGIN
		SET @NextSeedValue = @NextSeedValue + 1
	END
	
	DECLARE @NextSeed VARCHAR(16) = CONVERT(VARCHAR(16), @NextSeedValue)

	SET @NextCatalogCode = @UniquePrefix + SUBSTRING(@Filler, 1, 16 - LEN(@NextSeed)) + @NextSeed    

	INSERT INTO dbo.catalogcode
	(
		long_catalog_code,
		dim_catalog_code
	)
	VALUES
	(
		@LongCatalogCode,
		@NextCatalogCode
	)

	SELECT @NextCatalogCode AS CatalogCode
END

GO



