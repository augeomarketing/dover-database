USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoyaltyCatalog_GetNextLoyaltyCatalogID]    Script Date: 7/1/2015 1:13:49 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoyaltyCatalog_GetNextLoyaltyCatalogID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoyaltyCatalog_GetNextLoyaltyCatalogID]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 6.26.2015
-- Description:	Gets next availble sid_loyaltycatalog_id 
--              from loyaltycatalog table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_LoyaltyCatalog_GetNextLoyaltyCatalogID] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    SELECT (ISNULL(MAX(sid_loyaltycatalog_id), 0) + 1) AS InitialLoyaltyCatalogID  FROM dbo.loyaltycatalog  
END

GO



