USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_Catalog_GetPremcoItems]    Script Date: 6/10/2015 9:31:28 AM ******/

if exists(select 1 from dbo.sysobjects where name = 'usp_Catalog_GetPremcoItems' and xtype = 'P')
    drop procedure dbo.usp_Catalog_GetPremcoItems
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 6.8.2015
-- Description:	Gets Premco catalog items.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Catalog_GetPremcoItems] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT
		dim_catalogdescription_name AS CatalogName,
		dim_catalogdescription_description AS CatalogDescription,
		dim_catalog_shipping AS CatalogShipping,
		dim_catalog_msrp AS CatalogMSRP,
		dim_catalog_imagelocation AS CatalogImage,
		(SELECT ISNULL(branding_number, '') FROM dbo.categorybrand AS C WHERE C.sid_category_id = CA.sid_category_id) AS BrandNumber,
		dim_catalog_code AS CatalogCode,
		dim_catalog_cost AS CatalogCost,
		dim_catalog_weight AS CatalogWeight,
		sid_status_id AS CatalogStatus
	FROM
		dbo.catalog AS CAT
	INNER JOIN 
		dbo.catalogcategory AS CA ON CA.sid_catalog_id = CAT.sid_catalog_id
	INNER JOIN 
		dbo.catalogdescription AS CD ON CD.sid_catalog_id = CAT.sid_catalog_id
	WHERE
		dim_catalog_code LIKE 'PRE-%' AND
		dim_catalog_trancode = 'RM'
	ORDER BY 
		CatalogCode
END

GO



