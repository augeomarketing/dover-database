USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateCatalogCategory]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_updateCatalogCategory]
	@catalogId INT, @categoryId INT, @userId INT
AS 
BEGIN 
	UPDATE editcatalogcategory SET dim_catalogcategory_active = 1, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId AND sid_category_id = @categoryId
	IF(@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO editcatalogcategory
		SELECT TOP 1 * FROM catalogcategory WHERE sid_category_id = @categoryId AND sid_catalog_id = @catalogId
		IF(@@ROWCOUNT = 0)
			BEGIN
				INSERT INTO catalogcategory (sid_catalog_id, sid_category_id, dim_catalogcategory_active, sid_userinfo_id)
				VALUES (@catalogId, @categoryId, 0, @userId)

				INSERT INTO editcatalogcategory (sid_catalogcategory_id, sid_catalog_id, sid_category_id, dim_catalogcategory_active, sid_userinfo_id)
				VALUES (SCOPE_IDENTITY(),@catalogId, @categoryId, 1, @userId)
			END
		ELSE
			BEGIN
				UPDATE editcatalogcategory SET sid_userinfo_id = @userId, dim_catalogcategory_active = 1 WHERE sid_catalog_id = @catalogId AND sid_category_id = @categoryId
			END
	END
END
GO
