USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateRouting]    Script Date: 04/19/2011 13:53:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_updateRouting]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_updateRouting]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_updateRouting]    Script Date: 04/19/2011 13:53:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE   PROCEDURE [dbo].[usp_updateRouting]
	@catalogId INT, @routingId INT, @userId INT
AS 
BEGIN 
	UPDATE editcatalog SET sid_routing_id = @routingId, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId
	IF(@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO editcatalog
		SELECT TOP 1 *,NULL FROM catalog WHERE sid_catalog_id = @catalogId
		UPDATE editcatalog SET sid_routing_id = @routingId, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId
	END
END

GO

