USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_addCatalogEntryByCode]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE      PROCEDURE [dbo].[usp_addCatalogEntryByCode]
	@catalogCode VARCHAR(20), @userId INT, @trancode char(2), @NewCatalogId INT output
AS 
BEGIN 
	INSERT INTO catalog.dbo.catalog (dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_active, sid_userinfo_id, sid_status_id)  
	VALUES (@catalogCode, @trancode, 0,'','right', 0,@userId, 1)
	SET @newCatalogId = SCOPE_IDENTITY()		
	INSERT INTO catalog.dbo.editcatalog (sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_active, sid_userinfo_id, sid_status_id)  
	VALUES (@newCatalogId, @catalogCode, @trancode, 0,'','right', 1,@userId, 1)
END
GO
