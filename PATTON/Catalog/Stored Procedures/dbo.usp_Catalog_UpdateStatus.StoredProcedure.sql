USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_Catalog_UpdateStatus]    Script Date: 7/1/2015 1:09:40 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Catalog_UpdateStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Catalog_UpdateStatus]
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 6.25.2015
-- Description:	Updates status in catalog.
--
-- XML Sample:
-- <Catalog>
--   <Row code="" sid_status_id="" />
-- </Catalog>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Catalog_UpdateStatus] 
	-- Add the parameters for the stored procedure here
	@StatusUpdates XML 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	CREATE TABLE #CatalogStatusUpdates
	(
		[dim_catalog_code] varchar(20),
		[sid_status_id] int
	)

	INSERT INTO #CatalogStatusUpdates
	(
		[dim_catalog_code],
		[sid_status_id]
	)
	SELECT
		StatusUpdates.n.value('@code', 'varchar(20)') AS [dim_catalog_code],
		StatusUpdates.n.value('@sid_status_id', 'int') AS [sid_status_id]
	FROM
		@StatusUpdates.nodes('/Catalog/Row') as StatusUpdates(n);

	UPDATE CAT SET CAT.sid_status_id = STAT.sid_status_id
	FROM [catalog] AS CAT
	INNER JOIN #CatalogStatusUpdates AS STAT ON STAT.dim_catalog_code = CAT.dim_catalog_code 


	
END


GO



