USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_stageLoyaltyCatalog]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     PROCEDURE [dbo].[usp_stageLoyaltyCatalog]
	@catalogid INT, @tipprefix CHAR(3), @points INT, @bonus INT
AS 
BEGIN 
	DECLARE @loyaltyid INT
	DECLARE @NewLoyaltyCatalogId INT
	DECLARE @MAXLCID INT
	SELECT @MAXLCID = MAX(sid_loyaltycatalog_id) FROM catalog.dbo.loyaltycatalog
	SELECT @loyaltyid = sid_loyalty_id FROM loyaltytip WHERE dim_loyaltytip_prefix = @tipprefix
	SELECT * FROM catalog.dbo.loyaltycatalog WHERE sid_catalog_id = @catalogid AND sid_loyalty_id = @loyaltyid
	IF(@@ROWCOUNT = 0)
	BEGIN
		SET @MAXLCID = @MAXLCID + 1
		INSERT INTO catalog.dbo.loyaltycatalog (sid_loyaltycatalog_id, sid_catalog_id, sid_loyalty_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus)
		VALUES (@MAXLCID, @catalogid, @loyaltyid, @points, @bonus)
		SET @NewLoyaltyCatalogId = SCOPE_IDENTITY()		
		INSERT INTO catalog.dbo.editloyaltycatalog (dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_catalog_id, sid_loyalty_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus)
		VALUES (@NewLoyaltyCatalogId, @MAXLCID, @catalogid, @loyaltyid, @points, @bonus)
	END

END
GO
