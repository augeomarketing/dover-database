USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_getUserRole]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_getUserRole]
	@userId INT
AS 
BEGIN 
	SELECT DISTINCT dim_role_name 
		FROM permissiongroupuserinfo a
			INNER JOIN permissiongrouprole b
				ON a.sid_permissiongroup_id = b.sid_permissiongroup_id
			INNER JOIN  role c
				ON b.sid_role_id = c.sid_role_id
		WHERE a.sid_userinfo_id = @userId
END
GO
