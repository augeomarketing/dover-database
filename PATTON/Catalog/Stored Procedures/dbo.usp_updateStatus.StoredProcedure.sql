USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateStatus]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE   PROCEDURE [dbo].[usp_updateStatus]
	@catalogId INT, @statusId INT, @userId INT
AS 
BEGIN 
	UPDATE editcatalog SET sid_status_id = @statusId, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId
	IF(@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO editcatalog
		SELECT TOP 1 *,NULL FROM catalog WHERE sid_catalog_id = @catalogId
		UPDATE editcatalog SET sid_status_id = @statusId, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId
	END
END
GO
