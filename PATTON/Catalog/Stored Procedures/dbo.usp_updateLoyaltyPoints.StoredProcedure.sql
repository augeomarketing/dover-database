USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_updateLoyaltyPoints]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_updateLoyaltyPoints]
	@catalogId INT, @loyaltyId INT, @points INT, @bonus INT, @userId INT
AS 
BEGIN 
	UPDATE editloyaltycatalog SET dim_loyaltycatalog_pointvalue = @points, dim_loyaltycatalog_bonus = @bonus, sid_userinfo_id = @userId WHERE sid_catalog_id = @catalogId AND sid_loyalty_id = @loyaltyId
	IF(@@ROWCOUNT = 0)
	BEGIN
		INSERT INTO editloyaltycatalog
		SELECT TOP 1 * FROM loyaltycatalog WHERE sid_loyalty_id = @loyaltyId AND sid_catalog_id = @catalogId
		IF(@@ROWCOUNT = 0)
			BEGIN
				DECLARE @maxLoyaltyCatalogId INT
				SET @maxLoyaltyCatalogId = (SELECT MAX(sid_loyaltycatalog_id) FROM loyaltycatalog) + 1
				INSERT INTO loyaltycatalog (sid_loyaltycatalog_id, sid_catalog_id, sid_loyalty_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active, sid_userinfo_id)
				VALUES (@maxLoyaltyCatalogId, @catalogId, @loyaltyId, @points, @bonus, 0, @userId)

				INSERT INTO editloyaltycatalog (sid_loyaltycatalog_id, sid_catalog_id, sid_loyalty_id, dim_loyaltycatalog_pointvalue, dim_loyaltycatalog_bonus, dim_loyaltycatalog_active, sid_userinfo_id)
				VALUES (@maxLoyaltyCatalogId,@catalogId, @loyaltyId, @points, @bonus, 1, @userId)
			END
		ELSE
			BEGIN
				UPDATE editloyaltycatalog SET dim_loyaltycatalog_pointvalue = @points, dim_loyaltycatalog_bonus = @bonus, sid_userinfo_id = @userId, dim_loyaltycatalog_active = 1 WHERE sid_catalog_id = @catalogId AND sid_loyalty_id = @loyaltyId
			END
	END
END
GO
