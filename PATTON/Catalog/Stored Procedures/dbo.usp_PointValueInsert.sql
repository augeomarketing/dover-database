use catalog

if exists(select 1 from dbo.sysobjects where name = 'spPointValueInsert' and xtype = 'P')
	drop procedure dbo.spPointValueInsert
GO

-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,2,2009>
-- Description:	<Calculates Point Tier>
-- =============================================
CREATE PROCEDURE dbo.[spPointValueInsert]
@sid_catalog_id int = 0,                
@sid_vendor_id int = 0,                 
@dim_pricing_cost money =  0,           
@dim_pricing_shipping money =  0,       
@dim_pricing_handling money =  0,       
@dim_pricing_MSRP money =  0,           
@dim_pricing_HIshipping money =  0,     
@dim_pricing_weight int =  0  

AS
 
INSERT INTO catalog.dbo.PRICING(sid_catalog_id, sid_vendor_id, dim_pricing_cost, dim_pricing_shipping, dim_pricing_handling, dim_pricing_MSRP, dim_pricing_HIshipping, dim_pricing_weight)
	VALUES(@sid_catalog_id, @sid_vendor_id, @dim_pricing_cost,@dim_pricing_shipping ,@dim_pricing_handling,@dim_pricing_MSRP, @dim_pricing_HIshipping ,@dim_pricing_weight)
GO



/*
EXEC dbo.spPointValueInsert 1,1,19,20,21,90,5,6

Select *   from catalog.dbo.PRICING
where sid_catalog_id = 1
order by dim_pricing_created
*/