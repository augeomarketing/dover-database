USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_InvoiceReport_CrossCheck]    Script Date: 06/29/2011 11:15:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InvoiceReport_CrossCheck]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InvoiceReport_CrossCheck]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_InvoiceReport_CrossCheck]    Script Date: 06/29/2011 11:15:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[usp_InvoiceReport_CrossCheck]
    @figroupID		  int,
    @startdate		    datetime,
    @enddate		    datetime

AS

select fg.dim_figroup_name, m.TipFirst, d.ClientCode, d.ClientName, m.TranCode,
t.description, (m.TipFirst + '-' + d.ClientName) as FullName,
sum(m.points) as  Points,
sum(m.CatalogQty) as ttlQty
,sum(m.CatalogQty  * m.points) as TotalPoints
  from Fullfillment.dbo.Main m
  join Rewardsnow.dbo.dbprocessinfo d   on m.TipFirst = d.DBNumber
  join Rewardsnow.dbo.TranType t on m.TranCode = t.TranCode
  join rn1.management.dbo.figroupfi fgf on (fgf.sid_dbprocessinfo_dbnumber = d.DBNumber
    and fgf.dim_figroupfi_active = 1)
  join rn1.management.dbo.figroup fg on fg.sid_figroup_id = fgf.sid_figroup_id
       and fg.sid_figrouptype_id = 2
	   and fg.dim_figroup_active = 1

where  histdate >= @startdate
and HistDate < dateadd(day,1,@EndDate)   
and m.TranCode not in ('RT','RU','RV','RQ')
and m.TranCode like 'R%'
and m.ItemNumber <> 'CLASS FI FULLFILLED'
and fgf.sid_figroup_id = @figroupID
group by fg.dim_figroup_name,  tipfirst , d.ClientCode, d.ClientName,m.TranCode,t.Description 
order by  fg.dim_figroup_name,m.tipfirst
GO


