USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpsertCatalogItem]    Script Date: 7/1/2015 12:47:44 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpsertCatalogItem]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpsertCatalogItem]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 6/8/2015
-- Description:	For upserts to the catalog.
--
-- XML Sample: 
--<Catalog>
--	<Row code="" name="" description="" sid_loyaltycatalog_id="" trancode="" poinst="" cost="" shipping="" handling="" msrp="" image="" hishipping="" weight="" cashvalue="" sid_status_id="" branding_number="" />
--</Catalog>
--
--
-- HISTORY:
--          6/12/2015:  NAPT - Added transaction code
--          7/6/2015:   NAPT - Added point insertion code.
--          7/17/2015:  NAPT - Added shipping method code for APO items.
-- =============================================
CREATE PROCEDURE [dbo].[usp_UpsertCatalogItem]
	-- Add the parameters for the stored procedure here
	@CatalogUpdates XML
AS
BEGIN
	SET NOCOUNT ON;

	/* Temp table for catalog entries */
	CREATE TABLE #UpsertCatalog
	(
		[dim_catalog_code] varchar(20),
		[dim_catalog_trancode] [char](2),
		[dim_catalog_dollars] decimal(10, 2),
		[dim_catalog_imagelocation] varchar(1024),
		[dim_catalog_imagealign] varchar(50),
		[dim_catalog_parentid] int,
		[sid_status_id] int,
		[dim_catalog_brochure] int,
		[dim_catalog_cashvalue] decimal(18, 2),
		[sid_routing_id] int,
		[dim_catalog_cost] decimal(18, 2),
		[dim_catalog_shipping] decimal(18, 2),
		[dim_catalog_handling] decimal(18, 2),
		[dim_catalog_msrp] decimal(18, 2),
		[dim_catalog_HIshipping] decimal(18, 2),
		[dim_catalog_weight] decimal(18, 2),
		[dim_catalog_featured] int,
		[dim_catalogdescription_name] varchar(1024),
		[dim_catalogdescription_description] varchar(8000),
		[sid_category_id] int,
		[sid_loyaltycatalog_id] int,
		[dim_loyaltycatalog_pointvalue] int,
		[method] varchar(10),
		[branding_number] int
	)

	/* Load the XML into the temp table */
	INSERT INTO #UpsertCatalog
	(
		[dim_catalog_code]
	   ,[dim_catalog_trancode]
	   ,[dim_catalog_dollars]
	   ,[dim_catalog_imagelocation]
	   ,[dim_catalog_imagealign]
	   ,[dim_catalog_parentid]
	   ,[sid_status_id]
	   ,[dim_catalog_brochure]
	   ,[dim_catalog_cashvalue]
	   ,[sid_routing_id]
	   ,[dim_catalog_cost]
	   ,[dim_catalog_shipping]
	   ,[dim_catalog_handling]
	   ,[dim_catalog_msrp]
	   ,[dim_catalog_HIshipping]
	   ,[dim_catalog_weight]
	   ,[dim_catalog_featured]
	   ,[dim_catalogdescription_name]
	   ,[dim_catalogdescription_description]
	   ,[sid_loyaltycatalog_id]
	   ,[dim_loyaltycatalog_pointvalue]
	   ,[method]
	   ,[branding_number]
	)
	SELECT
		CatalogUpdates.n.value('@code', 'varchar(20)') AS [dim_catalog_code],
				
		CatalogUpdates.n.value('@trancode', 'char(2)') AS [dim_catalog_trancode],
		
		0.00 AS [dim_catalog_dollars],
						
		CatalogUpdates.n.value('@image', 'varchar(1024)') AS [dim_catalog_imagelocation],
		
		'right' AS [dim_catalog_imagealign],
		
		-1 as [dim_catalog_parentid],
		
		CatalogUpdates.n.value('@sid_status_id', 'int') AS [sid_status_id],
		
		0 AS [dim_catalog_brochure],
		
		CatalogUpdates.n.value('@cashvalue', 'decimal(18,2)') AS [dim_catalog_cashvalue],
		
		3 as [sid_routing_id],
		
		CatalogUpdates.n.value('@cost', 'decimal(18,2)') AS [dim_catalog_cost],
		
		CatalogUpdates.n.value('@shipping', 'decimal(18,2)') AS [dim_catalog_shipping],
		
		CatalogUpdates.n.value('@handling', 'decimal(18,2)') AS [dim_catalog_handling],
		
		CatalogUpdates.n.value('@msrp', 'decimal(18,2)') AS [dim_catalog_msrp],

		CatalogUpdates.n.value('@hishipping', 'decimal(18,2)') AS [dim_catalog_HIshipping],
		
		CatalogUpdates.n.value('@weight', 'decimal(18,2)') AS [dim_catalog_weight],
		
		0 AS [dim_catalog_featured],

		CatalogUpdates.n.value('@name', 'varchar(1024)') AS [dim_catalogdescription_name],
		
		CatalogUpdates.n.value('@description', 'varchar(8000)') AS [dim_catalogdescription_description],

		CatalogUpdates.n.value('@sid_loyaltycatalog_id', 'int') AS [sid_loyaltycatalog_id],

		CatalogUpdates.n.value('@points', 'int') AS [dim_loyaltycatalog_pointvalue],
		
		CatalogUpdates.n.value('@method', 'varchar(10)') AS [method],

		CatalogUpdates.n.value('@branding_number', 'int') AS [branding_number]
		
	FROM
	
		@CatalogUpdates.nodes('/Catalog/Row') as CatalogUpdates(n);
		
	/* Set the optional, non-nullable fields to an appropriate default value */
	UPDATE #UpsertCatalog SET [dim_catalog_handling] = 0.00 WHERE [dim_catalog_handling] IS NULL;
	UPDATE #UpsertCatalog SET [dim_catalog_HIshipping] = 0.00 WHERE [dim_catalog_HIshipping] IS NULL;
	UPDATE #UpsertCatalog SET [dim_catalog_cashvalue] = 0.00 WHERE [dim_catalog_cashvalue] IS NULL;
	UPDATE #UpsertCatalog SET [dim_catalog_weight] = 0.00 WHERE [dim_catalog_weight] IS NULL;
	
	/* RNI TOTAL COST Calculation */
	UPDATE #UpsertCatalog 
	SET [dim_catalog_dollars] = (([dim_catalog_cost] * 1.07) + [dim_catalog_shipping] + [dim_catalog_handling]) * 1.2;
	
	/* sid_category_id lookup */
	UPDATE tmp
	SET tmp.sid_category_id = cb.sid_category_id
	FROM #UpsertCatalog as tmp
	JOIN categorybrand as cb on cb.branding_number = tmp.branding_number;
	
	/* fill those items without sid_category_ids */
	UPDATE #UpsertCatalog
	SET sid_category_id = 0
	WHERE sid_category_id is null;

	-- The following table variable is needed to return values used in the usp_UpsertCatalogItemInRN1 SPR on RN1
	DECLARE @CatalogResults TABLE
	(
		dim_catalog_code VARCHAR(100),
		sid_catalog_id INTEGER,
		sid_category_id INTEGER,
		sid_catalogdescription_id INTEGER,
		sid_catalogcategory_id INTEGER,
		sid_loyaltycatalog_id INTEGER,
		dwid_loyaltycatalog_id INTEGER,
		sid_catalogcategory_id2 INTEGER -- Used for additional catalogcategory item from U items
	)

	BEGIN TRANSACTION
		
	/* UPDATE catalog items */
	UPDATE C
	SET 
	   C.dim_catalog_trancode = tmp.dim_catalog_trancode
	  ,C.dim_catalog_dollars = tmp.dim_catalog_dollars
	  ,C.dim_catalog_imagelocation = tmp.dim_catalog_imagelocation
	  ,C.dim_catalog_imagealign = tmp.dim_catalog_imagealign
	  ,C.dim_catalog_parentid = tmp.dim_catalog_parentid
	  ,C.sid_status_id = tmp.sid_status_id
	  ,C.dim_catalog_brochure = tmp.dim_catalog_brochure
	  ,C.dim_catalog_cashvalue = tmp.dim_catalog_cashvalue
	  ,C.sid_routing_id = tmp.sid_routing_id
	  ,C.dim_catalog_cost = tmp.dim_catalog_cost
	  ,C.dim_catalog_shipping = tmp.dim_catalog_shipping
	  ,C.dim_catalog_handling = tmp.dim_catalog_handling
	  ,C.dim_catalog_msrp = tmp.dim_catalog_msrp
	  ,C.dim_catalog_HIshipping = tmp.dim_catalog_HIshipping
	  ,C.dim_catalog_weight = tmp.dim_catalog_weight
	  ,C.dim_catalog_featured = tmp.dim_catalog_featured
	FROM
		[catalog] AS C
		INNER JOIN #UpsertCatalog AS tmp ON C.dim_catalog_code = tmp.dim_catalog_code;
	
	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
	
	/* UPDATE the descriptions */
	UPDATE cd
	SET
		cd.dim_catalogdescription_name = tmp.dim_catalogdescription_name,
		cd.dim_catalogdescription_description = tmp.dim_catalogdescription_description
	FROM
		catalogdescription as cd
		inner join [catalog] as c on c.sid_catalog_id = cd.sid_catalog_id
		inner join #UpsertCatalog as tmp on tmp.dim_catalog_code = c.dim_catalog_code;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
			
	/* INSERT the rest of the items in the tmp table to the catalog table. */
	INSERT INTO [catalog]
           ([dim_catalog_code]
           ,[dim_catalog_trancode]
           ,[dim_catalog_dollars]
           ,[dim_catalog_imagelocation]
           ,[dim_catalog_imagealign]
           ,[dim_catalog_parentid]
           ,[sid_status_id]
           ,[dim_catalog_brochure]
           ,[dim_catalog_cashvalue]
           ,[sid_routing_id]
           ,[dim_catalog_cost]
           ,[dim_catalog_shipping]
           ,[dim_catalog_handling]
           ,[dim_catalog_msrp]
           ,[dim_catalog_HIshipping]
           ,[dim_catalog_weight]
           ,[dim_catalog_featured])
	SELECT
			tmp.dim_catalog_code
           ,tmp.dim_catalog_trancode
           ,tmp.dim_catalog_dollars
           ,tmp.dim_catalog_imagelocation
           ,tmp.dim_catalog_imagealign
           ,tmp.dim_catalog_parentid
           ,tmp.sid_status_id
           ,tmp.dim_catalog_brochure
           ,tmp.dim_catalog_cashvalue
           ,tmp.sid_routing_id
           ,tmp.dim_catalog_cost
           ,tmp.dim_catalog_shipping
           ,tmp.dim_catalog_handling
           ,tmp.dim_catalog_msrp
           ,tmp.dim_catalog_HIshipping
           ,tmp.dim_catalog_weight
           ,tmp.dim_catalog_featured 
	FROM 
		#UpsertCatalog as tmp
		LEFT JOIN [catalog] AS c ON C.dim_catalog_code = tmp.dim_catalog_code
	WHERE
		c.sid_catalog_id IS NULL;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	INSERT INTO @CatalogResults
	(
		[dim_catalog_code],
		[sid_catalog_id]
	)
	SELECT
		[dim_catalog_code],
		[sid_catalog_id]
	FROM
		[catalog]
	WHERE
		dim_catalog_code LIKE 'PRE-%'

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
	
	/* INSERT catalogdescription  */
	INSERT INTO catalogdescription
           (sid_catalog_id
           ,dim_catalogdescription_name
           ,dim_catalogdescription_description
           ,sid_languageinfo_id)
    SELECT
		 pattonCatalog.sid_catalog_id
		,tmp.dim_catalogdescription_name
		,tmp.dim_catalogdescription_description
		,1
	FROM
		[catalog] as pattonCatalog
		INNER JOIN #UpsertCatalog AS tmp ON pattonCatalog.dim_catalog_code = tmp.dim_catalog_code
		LEFT JOIN catalogdescription as cd on cd.sid_catalog_id = pattonCatalog.sid_catalog_id
	WHERE
		cd.sid_catalog_id IS NULL;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
	
	UPDATE C SET C.sid_catalogdescription_id = D.sid_catalogdescription_id
	FROM @CatalogResults AS C
	INNER JOIN catalogdescription AS D ON D.sid_catalog_id = C.sid_catalog_id

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
	
	/* INSERT catalogcategory */
	/* by rebuilding the records in catalogcategory entirely */
	DELETE FROM catalogcategory
	WHERE
		sid_catalog_id IN ( SELECT c.sid_catalog_id
							FROM [catalog] as c
							JOIN #UpsertCatalog as tmp ON c.dim_catalog_code = tmp.dim_catalog_code );

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END					
															
	INSERT INTO catalogcategory (sid_catalog_id, sid_category_id, dim_catalogcategory_active)
	SELECT
		c.sid_catalog_id, tmp.sid_category_id, 1
	FROM
		#UpsertCatalog as tmp
		JOIN [catalog] as c on tmp.dim_catalog_code = c.dim_catalog_code
	WHERE
		tmp.sid_category_id > 0;

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
		
	UPDATE C SET C.sid_catalogcategory_id = CC.sid_catalogcategory_id, C.sid_category_id = CC.sid_category_id
	FROM @CatalogResults AS C
	INNER JOIN catalogcategory AS CC ON C.sid_catalog_id = CC.sid_catalog_id

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	INSERT INTO catalogcategory (sid_catalog_id, sid_category_id, dim_catalogcategory_active)
	SELECT
		c.sid_catalog_id, 104, 1
	FROM
		#UpsertCatalog AS tmp
	JOIN [catalog] AS c ON tmp.dim_catalog_code = c.dim_catalog_code
	WHERE
		tmp.sid_category_id > 0 AND tmp.method = 'U' 

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	UPDATE C SET C.sid_catalogcategory_id2 = CC.sid_catalogcategory_id
	FROM @CatalogResults AS C
	INNER JOIN catalogcategory AS CC ON C.sid_catalog_id = CC.sid_catalog_id
	WHERE CC.sid_category_id = 104

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END
		
	DECLARE @loyaltyid INT = 165; /* 165 = REBA */
	
	-- makes sure that each sid_catalog_id is assigned to @loyaltyid at least once.
	INSERT INTO loyaltycatalog 
		(sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
	SELECT
		tmp.sid_loyaltycatalog_id, @loyaltyid, c.sid_catalog_id, tmp.dim_loyaltycatalog_pointvalue
	FROM
		#UpsertCatalog as tmp
		join [catalog] as c on c.dim_catalog_code = tmp.dim_catalog_code
	WHERE tmp.sid_loyaltycatalog_id > 0

	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	UPDATE C SET C.sid_loyaltycatalog_id = L.sid_loyaltycatalog_id, C.dwid_loyaltycatalog_id = L.dwid_loyaltycatalog_id
	FROM @CatalogResults AS C
	INNER JOIN loyaltycatalog AS L ON L.sid_catalog_id = C.sid_catalog_id
							
	IF @@ERROR <> 0
	BEGIN
	ROLLBACK TRANSACTION
	RETURN
	END

	COMMIT TRANSACTION

	-- Return lookup ids used to update RN1 side
	SELECT
		[dim_catalog_code] AS CatalogCode,
		[sid_category_id] AS CategoryID,
		[sid_catalog_id] AS CatalogID,
		[sid_catalogdescription_id] AS CatalogDescriptionID,
		[sid_catalogcategory_id] AS CatalogCategoryID,
		[sid_loyaltycatalog_id] AS LoyaltyCatalogID,
		[dwid_loyaltycatalog_id] AS LoyaltyCatalogSID,
		ISNULL([sid_catalogcategory_id2], -1) AS CatalogCategoryID2
	FROM
		@CatalogResults

END

GO
