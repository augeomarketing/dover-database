USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[usp_stageItem]    Script Date: 07/30/2009 11:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090721
-- Description:	Stage Item
-- =============================================
CREATE PROCEDURE [dbo].[usp_stageItem]
  @catalogId INT
AS
BEGIN
	SET NOCOUNT ON;
  DECLARE @newId INT

  INSERT INTO catalog.dbo.editcatalog (sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created,dim_catalog_lastmodified,dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure)
  SELECT sid_catalog_id, dim_catalog_code, dim_catalog_trancode, dim_catalog_dollars, dim_catalog_imagelocation, dim_catalog_imagealign, dim_catalog_created,dim_catalog_lastmodified,dim_catalog_active, dim_catalog_parentid, sid_status_id, sid_userinfo_id, dim_catalog_brochure
    FROM catalog.dbo.catalog
    WHERE sid_catalog_id = @catalogId
      AND sid_catalog_id NOT IN (SELECT sid_catalog_id FROM catalog.dbo.editcatalog)

  SELECT @newid = SCOPE_IDENTITY() 
  
  INSERT INTO catalog.dbo.editcatalogstate (sid_editcatalog_id, sid_catalog_id, sid_state_id)
  VALUES (@newId, @catalogId, 1 )
  
END
GO
