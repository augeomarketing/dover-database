use catalog

if exists(select 1 from dbo.sysobjects where name = 'spPointValueCalc' and xtype = 'P')
	drop procedure dbo.spPointValueCalc
GO

-- =============================================
-- Author:		<Oman>
-- Create date: <Sept,2,2009>
-- Description:	<Calculates Point Tier>
-- =============================================
CREATE PROCEDURE dbo.[spPointValueCalc]
@dim_pricing_cost money = 0,       
@dim_pricing_shipping money = 0,   
@dim_pricing_handling money = 0,
@dim_pointtiers_tierfamily int = 1,
@costaspoints money OUTPUT

AS

declare @pt money

declare @salestax			decimal(5,4)
declare @markup				decimal(5,4)
declare @pointconversion	decimal(5,4)

set @salestax = 1.07  -- 7% sales tax
set @markup = 1.20 -- 20% markup
set @pointconversion = 0.008  -- pctg used to convert to points

set @costaspoints = ((@dim_pricing_cost * @salestax + @dim_pricing_shipping + @dim_pricing_handling) * @markup) / @pointconversion

set @pt = 
		cast((select top 1 dim_pointtiers_pointtier
		from catalog.dbo.pointtiers
		where dim_pointtiers_tierfamily = @dim_pointtiers_tierfamily
		and dim_pointtiers_pointtier >= @costaspoints
		order by dim_pointTiers_pointTier) as int)


if @costaspoints < @pt
	set @costaspoints = @pt

return @costaspoints

GO



/*


declare @costaspoints int
set @costaspoints = 0

EXEC dbo.spPointValueCalc 8.5, 10.59,2.00,1, @costaspoints OUTPUT

select @costaspoints



*/