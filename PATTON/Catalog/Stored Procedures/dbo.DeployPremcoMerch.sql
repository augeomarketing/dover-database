USE [Catalog]
GO
/****** Object:  StoredProcedure [dbo].[DeployPremcoMerchandise]    Script Date: 02/04/2016 08:24:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Blaise Swanwick
-- Create date: 9/2/2015
-- Description:	This sproc finds new Premco Merch in the Catalog Table
--				and assigns it to the appropriate programs.
--
-- Tables affected by this sproc:
--   catalog.dbo.loyaltycatalog 
--   rn1.catalog.dbo.loyaltycatalog
-- =============================================
CREATE PROCEDURE [dbo].[DeployPremcoMerchandise]
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	-- Get the date of the most recent batch of premco items...
	DECLARE @PremcoInitialLoadDate DATE;
	SELECT @PremcoInitialLoadDate = CAST(MAX(dim_catalog_created) as DATE)
	FROM
		[catalog]
	WHERE
		dim_catalog_code LIKE 'PRE-%';

	-- Drop our temp work table if it already exists
	IF OBJECT_ID('tempdb.dbo.#lc', 'U') IS NOT NULL
	  DROP TABLE #lc; 

	-- Create our temp work table	
	CREATE TABLE #lc (
		sid_loyalty_id int, 
		sid_catalog_id int, 
		dim_loyaltycatalog_pointvalue int, 
		sid_loyaltycatalog_id int);
		
	-- Initialize the #lc table.
	-- We want to see every loyalty program (that currently has merch in it)
	-- alongside every premco catalog item added on @PremcoInitialLoadDate
	INSERT INTO #lc
		(sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
	SELECT DISTINCT 
		l.sid_loyalty_id, c.sid_catalog_id, lc.dim_loyaltycatalog_pointvalue
	FROM
		[catalog] as c
	JOIN (	SELECT DISTINCT sid_loyalty_id -- this join gets every loyalty id that has a merch item with a point value > 0
			FROM
				loyaltycatalog as lc
				join catalogcategory as cc on lc.sid_catalog_id = cc.sid_catalog_id
				join categorygroupinfo as cgi on cc.sid_category_id = cgi.sid_category_id
			WHERE
				cgi.sid_groupinfo_id = 1
				AND lc.dim_loyaltycatalog_pointvalue > 0) as l on 1=1 -- SO EVERY CATALOG RECORD JOINS TO EVERY LOYALTY RECORD
	JOIN
		loyaltycatalog as lc on lc.sid_catalog_id = c.sid_catalog_id
	WHERE
		c.dim_catalog_code LIKE 'PRE-%' -- Premco merch
		and CAST(c.dim_catalog_created AS DATE) = @PremcoInitialLoadDate -- added on this date
	ORDER BY
		l.sid_loyalty_id, c.sid_catalog_id;
		

	-- some programs have point constraints on the items they carry...
		
		
	-- START: DELETE ANY ITEMS THAT DON'T MEET CLIENT'S CATALOG MAX/MIN POINT VALUE CONSTRAINTS
		-- remove merch that costs more than 150000 points
		DELETE LC
		FROM #lc AS LC
		WHERE
			sid_loyalty_id IN ( SELECT sid_loyalty_id 
								FROM loyaltytip 
								WHERE dim_loyaltytip_prefix IN
									( '519','520','603','605','611','624','631','633','638' ))
			AND dim_loyaltycatalog_pointvalue > 150000;
			
		-- remove merch that costs more than 75000 points
		DELETE LC
		FROM #lc AS LC
		WHERE
			sid_loyalty_id IN ( SELECT sid_loyalty_id 
								FROM loyaltytip 
								WHERE dim_loyaltytip_prefix IN
									( '519','520' ))
			AND dim_loyaltycatalog_pointvalue > 75000;		
		
		-- remove merch that costs less than 2000 points
		DELETE LC
		FROM #lc AS LC
		WHERE
			sid_loyalty_id IN ( SELECT sid_loyalty_id 
								FROM loyaltytip 
								WHERE dim_loyaltytip_prefix IN
									( '519' ))
			AND dim_loyaltycatalog_pointvalue < 2000
			AND dim_loyaltycatalog_pointvalue > 0;
		
		-- remove items that cost less than 5000 points
		DELETE LC
		FROM #lc AS LC
		WHERE
			sid_loyalty_id IN ( SELECT sid_loyalty_id 
								FROM loyaltytip 
								WHERE dim_loyaltytip_prefix IN
									( '520' ))
			AND dim_loyaltycatalog_pointvalue < 5000
			AND dim_loyaltycatalog_pointvalue > 0;
		
	-- /END: DELETE ANY ITEMS THAT DON'T MEET CLIENT'S CATALOG MAX/MIN POINT VALUE CONSTRAINTS	

	-- We need to double the point values for one program (OMNI).
	UPDATE #lc 
	SET dim_loyaltycatalog_pointvalue = dim_loyaltycatalog_pointvalue * 2
	WHERE
		sid_loyalty_id = 37;
		
	-- and let's make flippin' sure that we don't put merch in fidelity or kroger
	DELETE FROM #lc
	WHERE
		sid_loyalty_id IN (14,133);
				
	-- Now that's over with, add an identity column to the temp table
	ALTER TABLE #lc
	ADD rownum INT IDENTITY(1,1)
			
	-- And do the loyaltycatalog identity BULLSHIT (using rownum)
	DECLARE @m INT;

	-- get new sid_loyaltycatalog_ids
	SELECT @m = MAX(sid_loyaltycatalog_id) FROM loyaltycatalog;
	UPDATE #lc
	SET
		sid_loyaltycatalog_id = @m + rownum;  -- NOTE: Intellisense is not too intelligent...
		
	-- Sanity check we aren't inserting catalog items twice	
	-- The catalog items in #lc should not yet be assigned to any programs besides REBA...
	CREATE INDEX IDX ON #lc(sid_catalog_id);

	IF EXISTS ( SELECT TOP 1 *
				FROM
					loyaltycatalog as lc
				WHERE
					sid_catalog_id IN (
						SELECT DISTINCT sid_catalog_id FROM #lc
					)
					and	lc.sid_loyalty_id <> 165
	)
	BEGIN
		print 'Sanity Check Failed!  WTF?'
	END
	ELSE
	BEGIN
		print 'Sanity Check Passed!  Time to Rock and Roll...'
	
		BEGIN TRAN
		
		-- insert patton
		INSERT INTO loyaltycatalog
			(sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
		SELECT
			sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue
		FROM
			#lc;

		-- insert rn1 (using dwid generated from patton)
		INSERT INTO rn1.catalog.dbo.loyaltycatalog
			(dwid_loyaltycatalog_id, sid_loyaltycatalog_id, sid_loyalty_id, sid_catalog_id, dim_loyaltycatalog_pointvalue)
		SELECT
			LC.dwid_loyaltycatalog_id, #lc.sid_loyaltycatalog_id, #lc.sid_loyalty_id, #lc.sid_catalog_id, #lc.dim_loyaltycatalog_pointvalue
		FROM
			#lc
			JOIN loyaltycatalog as LC on LC.sid_loyaltycatalog_id = #lc.sid_loyaltycatalog_id;		
			
		-- For extra credit, run these to make sure identities are still in sync.
		DECLARE @sid1 INT;
		DECLARE @sid2 INT;	
		DECLARE @dwid1 INT;
		DECLARE @dwid2 INT;	
		
		SELECT @sid1 = MAX(sid_loyaltycatalog_id) FROM [loyaltycatalog];
		SELECT @sid2 = MAX(sid_loyaltycatalog_id) FROM [rn1].[catalog].[dbo].[loyaltycatalog];
		SELECT @dwid1 = MAX(dwid_loyaltycatalog_id) FROM [loyaltycatalog];
		SELECT @dwid2 = MAX(dwid_loyaltycatalog_id) FROM [rn1].[catalog].[dbo].[loyaltycatalog];
		
		IF @sid1 <> @sid2 OR @dwid1 <> @dwid2
		BEGIN
			PRINT 'IDENTITY SANITY CHECK FAILED!  ROLLING BACK TRANSACTION...'
			ROLLBACK TRAN
			RAISERROR ('IDENTITY SANITY CHECK FAILED!', 11,1);
		END
		ELSE
		BEGIN
			PRINT 'IDENTITY SANITY CHECK PASSED!  COMMITING TRANSACTION...'
			COMMIT TRAN

			-- output a list of programs these items were assigned to
			select distinct LT.dim_loyaltytip_prefix, DBPI.ClientName
			from
				loyaltycatalog as LC
				JOIN catalog as C on C.sid_catalog_id = LC.sid_catalog_id
				JOIN loyaltytip as LT on LC.sid_loyalty_id = LT.sid_loyalty_id
				JOIN RewardsNow.dbo.dbprocessinfo as DBPI on DBPI.DBNumber = LT.dim_loyaltytip_prefix AND DBPI.sid_FiProdStatus_statuscode in ('P','V')
			where
				C.dim_catalog_code like 'PRE-%'
				AND LC.dim_loyaltycatalog_pointvalue > 0
				AND C.dim_catalog_created = @PremcoInitialLoadDate;
		END
	END
	PRINT 'THE END.'
END