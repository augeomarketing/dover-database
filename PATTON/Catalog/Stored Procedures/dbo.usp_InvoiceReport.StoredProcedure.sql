USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_InvoiceReport]    Script Date: 07/21/2011 15:05:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_InvoiceReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_InvoiceReport]
GO

USE [Catalog]
GO

/****** Object:  StoredProcedure [dbo].[usp_InvoiceReport]    Script Date: 07/21/2011 15:05:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE procedure [dbo].[usp_InvoiceReport]
    @sid_figroup_id	    int,
    @startdate		    datetime,
    @enddate		    datetime

AS

/* NOTE:
TRAVEL REDEMPTIONS ARE NOT INCLUDED IN THIS REPORT

*/


-- 
if object_id('tempdb..#invoicereportgroups') is not null
    drop table #invoicereportgroups

--CREATE TEMPORARY TABLES
-- This table is the header data.  All TPMs, all tips within TPM, all product groups (groupinfo name)
create table #invoicereportgroups
    (sid_invoicereportgroups	  int identity(1,1) primary key,
     sid_figroup_id			  int,
     dim_figroup_name	       varchar(50),
     tipfirst				  varchar(3),
     clientname			  varchar(256),
     dim_groupinfo_description  varchar(1024),
     sid_groupinfo_id		  int)


--

if object_id('tempdb..#invoicereport') is not null
    drop table #invoicereport
    
--  This table will be used
-- as a base to then find the bonus items
CREATE TABLE #invoicereport(
	[sid_figroup_id] [int] ,
	[dim_figroup_name] [varchar](50) ,
	[tipfirst] [varchar](3) ,
	[clientname] [varchar](256) ,
	[dim_groupinfo_description] [varchar](1024) ,
	[trancode] [varchar](2) ,
	[tipnumber] [varchar](15) ,
	[name1] [varchar](50) ,
	[histdate] [smalldatetime] ,
	[itemnumber] [varchar](20) ,
	[catalogdesc] [varchar](300) ,
	[catalogqty] [int] ,
	[pointsperredemption] [int] ,
	[pointsredeemed] [int] ,
	[dim_catalog_dollars] [decimal](10, 2) ,
	[dim_catalog_cashvalue] [decimal](18, 2) ,
	[dim_catalog_cost] [decimal](18, 2) ,
	[dim_catalog_shipping] [decimal](18, 2) ,
	[dim_catalog_handling] [decimal](18, 2) ,
	[dim_catalog_hishipping] [decimal](18, 2) ,
	[dim_catalog_weight] [decimal](18, 2) ,
	[dim_fipricing_value] [float] ,
	[dim_pricetype_name] [varchar](50) ,
	[dim_pricetype_description] [varchar](1024) ,
	[isbonusitem] [int] ,
	[travelcashvalue] [smallmoney] ,
	[orderid] bigint,
	[sid_groupinfo_id]	[int],
	[bonus_pricetype_name] [varchar](50),
	[bonus_pricetype_description] [varchar](1024) ,
	[bonus_fipricing_value] [float] ,
	[sid_loyalty_id]  [int],
	[sid_catalog_id]   [int]
) 

  

if object_id('tempdb..#invoicePrices') is not null
    drop table #invoicePrices
    
CREATE TABLE #invoicePrices (
	[sid_figroup_id] [int] NOT NULL,
	[dim_figroup_name] [varchar](50) NOT NULL,
	[tipfirst] [varchar](3) NOT NULL,
	[clientname] [varchar](256) NULL,
	[dim_groupinfo_description] [varchar](1024) NOT NULL,
	[trancode] [varchar](2) NULL,
	[tipnumber] [char](15) NULL,
	[name1] [varchar](50) NULL,
	[histdate] [smalldatetime] NULL,
	[itemnumber] [varchar](20) NULL,
	[catalogdesc] [varchar](300) NULL,
	[catalogqty] [int] NULL,
	[pointsperredemption] [int] NULL,
	[pointsredeemed] [int] NULL,
	[dim_catalog_dollars] [decimal](10, 2) NULL,
	[dim_catalog_cashvalue] [decimal](18, 2) NULL,
	[dim_catalog_cost] [decimal](18, 2) NULL,
	[dim_catalog_shipping] [decimal](18, 2) NULL,
	[dim_catalog_handling] [decimal](18, 2) NULL,
	[dim_catalog_hishipping] [decimal](18, 2) NULL,
	[dim_catalog_weight] [decimal](18, 2) NULL,
	[dim_fipricing_value] [decimal](18, 4) NULL,
	[dim_pricetype_name] [varchar](50) NULL,
	[dim_pricetype_description] [varchar](1024) NULL,
	[isbonusitem] [int] NULL,
	[travelcashvalue] [smallmoney] NULL,
	[orderid] bigint NULL,
	[bonusPricingValue] [decimal](18, 4) NULL 
) 
 


-- Populate the invoice header temp table

insert into #invoicereportgroups
(sid_figroup_id, dim_figroup_name, tipfirst, clientname, dim_groupinfo_description, sid_groupinfo_id)     
select distinct fg.sid_figroup_id, fg.dim_figroup_name, fgf.sid_dbprocessinfo_dbnumber TipFirst, 
				dbpi.clientname, dim_groupinfo_description, sid_groupinfo_id


from rn1.management.dbo.figroup fg join rn1.management.dbo.figroupfi fgf
    on  fg.sid_figroup_id = fgf.sid_figroup_id
    and fg.sid_figrouptype_id = 2
    and fg.dim_figroup_active = 1
    --addd below check   d.irish 6/29/2011
    and fgf.dim_figroupfi_active = 1

join rewardsnow.dbo.dbprocessinfo dbpi
    on fgf.sid_dbprocessinfo_dbnumber = dbpi.dbnumber
    and sid_fiprodstatus_statuscode in ('P', 'V')

join (select sid_groupinfo_id, dim_groupinfo_description 
	   from catalog.dbo.groupinfo 
	   -- per dave, no longer used/needed so remove college & ringtone
	    where dim_groupinfo_description not in ('College','Ringtones')
      union
      select 999999 as sid_groupinfo_id, 'Bonus Items' as dim_groupinfo_description) gi
    on 1=1  -- do this for a cartesian join

where fg.sid_figroup_id = @sid_figroup_id

--===========================================================================================
--+++populate the main driving table
--
-- Generate report output
-- d.irish 6/1/2011 using left outer joins below in case the item is not in the catalog or the price is missing
-- d.irish 6/21/11 add fullfillment.orderid to select since sometimes this will differentiate an
-- order enough with the distinct. 
-- d.irish 6/21/11 add union clause to include any items that have been removed
--   from catalog and archived into archivecatalog 
-- d.irish 6/22/2011 add case statment to put bonus items into the 'bonus item' category'

insert into #invoicereport
( sid_figroup_id, dim_figroup_name, tipfirst,clientname,dim_groupinfo_description,
  	   trancode, tipnumber,name1, histdate, itemnumber,catalogdesc, catalogqty, pointsperredemption,
	   pointsredeemed, dim_catalog_dollars, dim_catalog_cashvalue, dim_catalog_cost, dim_catalog_shipping,
	   dim_catalog_handling, dim_catalog_hishipping, dim_catalog_weight, dim_fipricing_value, dim_pricetype_name,
	   dim_pricetype_description, isbonusitem, travelcashvalue,orderid,sid_groupinfo_id,
	    sid_catalog_id,sid_loyalty_id
)
select irg.sid_figroup_id, irg.dim_figroup_name, irg.tipfirst, irg.clientname,irg.dim_groupinfo_description,
  	   vw.trancode, vw.tipnumber, vw.name1, vw.histdate, vw.itemnumber, vw.catalogdesc, vw.catalogqty, vw.pointsperredemption,
	   vw.pointsredeemed, vw.dim_catalog_dollars, vw.dim_catalog_cashvalue, vw.dim_catalog_cost, vw.dim_catalog_shipping,
	   vw.dim_catalog_handling, vw.dim_catalog_hishipping, vw.dim_catalog_weight, vw.dim_fipricing_value, dim_pricetype_name,
	   vw.dim_pricetype_description, vw.isbonusitem, vw.travelcashvalue,vw.orderid,vw.sid_groupinfo_id, vw.sid_catalog_id,vw.sid_loyalty_id
from #invoicereportgroups irg left outer join
    (
    ----BELOW PULLS IN ANY REDEMPTIONS WHOSE ITEMNUMBER IS STILL ACTIVE  
	  select distinct fg.sid_figroup_id, fgf.sid_dbprocessinfo_dbnumber as TipFirst,
			 m.trancode, m.tipnumber, m.name1, m.histdate, m.itemnumber, m.catalogdesc, cgi.sid_groupinfo_id, m.catalogqty, m.points PointsPerRedemption, 
			 (m.catalogqty * m.points) PointsRedeemed, dim_catalog_dollars, dim_catalog_cashvalue,
			 dim_catalog_cost, dim_catalog_shipping, dim_catalog_handling, dim_catalog_hishipping,
			 dim_catalog_weight, fp.dim_fipricing_value, dim_pricetype_name, dim_pricetype_description,
			 lc.dim_loyaltycatalog_bonus isBonusItem, m.cashvalue as TravelCashValue,m.OrderID,
			 c.sid_catalog_id,lt.sid_loyalty_id

	   from [rn1].management.dbo.figroup fg join [rn1].management.dbo.figroupfi fgf
		  on fg.sid_figroup_id = fgf.sid_figroup_id
		  and '2' = sid_figrouptype_id
		  and 1 = fg.dim_figroup_active

	   join rewardsnow.dbo.dbprocessinfo dbpi	
		  on fgf.sid_dbprocessinfo_dbnumber = dbpi.dbnumber

	   left outer join fullfillment.dbo.main m
		  on left(m.tipnumber,3) = fgf.sid_dbprocessinfo_dbnumber

	   left outer join Catalog.dbo.catalog c
		  on CASE 
			   when (LEFT(m.itemnumber,2) = 'GC')	then 'GC' + c.dim_catalog_code 
			   else c.dim_catalog_code
		end = m.itemnumber
		  and 1 = c.dim_catalog_active

	   left outer join dbo.catalogcategory cc
		  on c.sid_catalog_id = cc.sid_catalog_id
		  and 1 = cc.dim_catalogcategory_active

	  left outer  join dbo.categorygroupinfo cgi
		  on cc.sid_category_id = cgi.sid_category_id

	   left outer join dbo.fipricing fp
		  on cgi.sid_groupinfo_id = fp.sid_groupinfo_id
		  and fgf.sid_dbprocessinfo_dbnumber = fp.sid_dbprocessinfo_dbnumber
		  and 1 = fp.dim_fipricing_active
		  --d.irish add line below 6/23/11
		  and fp.sid_pricetype_id != 5  ---  This excludes bonus pricing for now.  this will get rectified after temp table is built.


	 left outer   join dbo.pricetype pt
		  on fp.sid_pricetype_id = pt.sid_pricetype_id
		  and 1 = pt.dim_pricetype_active

	  left outer  join dbo.loyaltytip lt
		  on lt.dim_loyaltytip_prefix = fgf.sid_dbprocessinfo_dbnumber
		  and 1 = lt.dim_loyaltytip_active

	  left outer  join dbo.loyaltycatalog lc
		  on lt.sid_loyalty_id = lc.sid_loyalty_id
		  and c.sid_catalog_id = lc.sid_catalog_id
		  and 1 = lC.dim_loyaltycatalog_active
		  and lC.dim_loyaltycatalog_pointvalue > 0

	   where m.histdate >= @startdate and histdate  < dateadd(day,1,@enddate)
	   
   UNION ALL
--BELOW PULLS IN ANY REDEMPTIONS WHOSE ITEMNUMBER HAS BEEN ARCHIVED  
    select distinct fg.sid_figroup_id, fgf.sid_dbprocessinfo_dbnumber as TipFirst,
			 m.trancode, m.tipnumber, m.name1, m.histdate, m.itemnumber, m.catalogdesc, cgi.sid_groupinfo_id, m.catalogqty, m.points PointsPerRedemption, 
			 (m.catalogqty * m.points) PointsRedeemed, dim_catalog_dollars, dim_catalog_cashvalue,
			 dim_catalog_cost, dim_catalog_shipping, dim_catalog_handling, dim_catalog_hishipping,
			 dim_catalog_weight, fp.dim_fipricing_value, dim_pricetype_name, dim_pricetype_description,
			 lc.dim_loyaltycatalog_bonus isBonusItem, m.cashvalue as TravelCashValue,m.OrderID,
             c.sid_catalog_id,lt.sid_loyalty_id
	   from [rn1].management.dbo.figroup fg join [rn1].management.dbo.figroupfi fgf
		  on fg.sid_figroup_id = fgf.sid_figroup_id
		  and '2' = sid_figrouptype_id
		  and 1 = fg.dim_figroup_active

	   join rewardsnow.dbo.dbprocessinfo dbpi	
		  on fgf.sid_dbprocessinfo_dbnumber = dbpi.dbnumber

	   left outer join fullfillment.dbo.main m
		  on left(m.tipnumber,3) = fgf.sid_dbprocessinfo_dbnumber

	   left outer join Catalog.dbo.ARCHIVECATALOG c
		  on CASE 
			   when (LEFT(m.itemnumber,2) = 'GC')	then 'GC' + c.dim_catalog_code 
			   else c.dim_catalog_code
		end = m.itemnumber
		  and 1 = c.dim_catalog_active

	   left outer join dbo.catalogcategory cc
		  on c.sid_catalog_id = cc.sid_catalog_id
		  and 1 = cc.dim_catalogcategory_active

	  left outer  join dbo.categorygroupinfo cgi
		  on cc.sid_category_id = cgi.sid_category_id

	   left outer join dbo.fipricing fp
		  on cgi.sid_groupinfo_id = fp.sid_groupinfo_id
		  and fgf.sid_dbprocessinfo_dbnumber = fp.sid_dbprocessinfo_dbnumber
		  and 1 = fp.dim_fipricing_active
		--d.irish add line below 6/23/11
		  and fp.sid_pricetype_id != 5  ---  This excludes bonus pricing for now.  this will get rectified after temp table is built.

	 left outer   join dbo.pricetype pt
		  on fp.sid_pricetype_id = pt.sid_pricetype_id
		  and 1 = pt.dim_pricetype_active

	  left outer  join dbo.loyaltytip lt
		  on lt.dim_loyaltytip_prefix = fgf.sid_dbprocessinfo_dbnumber
		  and 1 = lt.dim_loyaltytip_active

	  left outer  join dbo.loyaltycatalog lc
		  on lt.sid_loyalty_id = lc.sid_loyalty_id
		  and c.sid_catalog_id = lc.sid_catalog_id
		  and 1 = lC.dim_loyaltycatalog_active
		  and lC.dim_loyaltycatalog_pointvalue > 0

	   where m.histdate >= @startdate and histdate  < dateadd(day,1,@enddate) 
	   and c.dim_archivecatalog_created = (select top 1dim_archivecatalog_created from catalog.dbo.archivecatalog acc
	   where acc.dim_catalog_code = c.dim_catalog_code)
	   and c.dim_catalog_code not in (select dim_catalog_code from catalog.dbo.catalog )
	 
	   
    ) vw
on irg.sid_figroup_id = vw.sid_figroup_id
and irg.tipfirst = vw.tipfirst
and irg.sid_groupinfo_id = vw.sid_groupinfo_id

order by irg.sid_figroup_id, irg.tipfirst, irg.dim_groupinfo_description
  
  
  --========================================
  --pull in all the pricing amounts
insert into #invoicePrices	 
(  [sid_figroup_id],[dim_figroup_name] ,[tipfirst],[clientname] ,[dim_groupinfo_description] ,[trancode]
   ,[tipnumber],[name1] ,[histdate] ,[itemnumber] ,[catalogdesc],[catalogqty] ,[pointsperredemption]
   ,[pointsredeemed]  ,[dim_catalog_dollars],[dim_catalog_cashvalue],[dim_catalog_cost] ,[dim_catalog_shipping]
   ,[dim_catalog_handling] ,[dim_catalog_hishipping] ,[dim_catalog_weight],[dim_fipricing_value] ,[dim_pricetype_name]
   ,[dim_pricetype_description] ,[isbonusitem],[travelcashvalue],[orderid]  ,[bonusPricingValue]
   )
 
select ir.sid_figroup_id, ir.dim_figroup_name, ir.tipfirst,ir.clientname  ,ir.dim_groupinfo_description, 
    	   ir.trancode, ir.tipnumber,ir.name1, ir.histdate, ir.itemnumber,ir.catalogdesc, ir.catalogqty, ir.pointsperredemption,
	   ir.pointsredeemed, ir.dim_catalog_dollars, ir.dim_catalog_cashvalue, ir.dim_catalog_cost, ir.dim_catalog_shipping,
	   ir.dim_catalog_handling, ir.dim_catalog_hishipping, ir.dim_catalog_weight, 
	 ir.dim_fipricing_value, ir.dim_pricetype_name, ir.dim_pricetype_description,
	 ir.isbonusitem, ir.travelcashvalue,ir.orderid,fp.dim_fipricing_value as bonusPricingValue
 
from #invoicereport ir
left outer join fipricing fp on fp.sid_dbprocessinfo_dbnumber = ir.tipfirst
					and fp.sid_groupinfo_id = ir.sid_groupinfo_id
					and fp.sid_pricetype_id = 5  --bonus pricing

order by ir.tipnumber,ir.isbonusitem

 

 -- now display report
 
 select  [sid_figroup_id],[dim_figroup_name] ,[tipfirst],[clientname] ,[dim_groupinfo_description] ,[trancode]
   ,[tipnumber],[name1] ,[histdate] ,[itemnumber] ,[catalogdesc],[catalogqty] ,[pointsperredemption]
   ,[pointsredeemed]  ,[dim_catalog_dollars],[dim_catalog_cashvalue],[dim_catalog_cost] ,[dim_catalog_shipping]
   ,[dim_catalog_handling] ,[dim_catalog_hishipping] ,[dim_catalog_weight],[dim_fipricing_value] ,[dim_pricetype_name]
   ,[dim_pricetype_description] ,[isbonusitem],[travelcashvalue],[orderid]  ,[bonusPricingValue]
   from #invoicePrices
   where tipnumber IS NOT NULL
 
GO


