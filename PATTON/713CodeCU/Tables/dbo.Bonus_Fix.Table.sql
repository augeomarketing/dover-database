USE [713CodeCU]
GO
/****** Object:  Table [dbo].[Bonus_Fix]    Script Date: 03/17/2010 16:03:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bonus_Fix](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[oldtrancode] [varchar](2) NULL,
	[newTrancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
