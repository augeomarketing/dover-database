USE [713CodeCU]
GO
/****** Object:  Table [dbo].[Platinum]    Script Date: 03/17/2010 16:03:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Platinum](
	[CardNumber] [char](25) NULL,
	[CustID] [char](9) NULL,
	[TipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
