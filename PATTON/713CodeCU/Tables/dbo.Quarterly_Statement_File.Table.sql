USE [713CodeCU]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 02/17/2012 09:24:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) NULL DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL DEFAULT (0),
	[PointsPurchased] [decimal](18, 0) NULL DEFAULT (0),
	[PointsBonus] [decimal](18, 0) NULL DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL DEFAULT (0),
	[PointsReturned] [decimal](18, 0) NULL DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL DEFAULT (0),
	[ExpPts] [decimal](18, 0) NULL DEFAULT (0),
	[PointsBonusMN] [int] DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
