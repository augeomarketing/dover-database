USE [713CodeCU]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 02/17/2012 09:24:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO

CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL DEFAULT (0),
	[PointsPurchased] [decimal](18, 0) NULL DEFAULT (0),
	[PointsBonus] [decimal](18, 0) NULL DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL DEFAULT (0),
	[PointsReturned] [decimal](18, 0) NULL DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL DEFAULT (0),
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL DEFAULT (0),
	[PointsBonusMN] [int] DEFAULT (0)

) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
