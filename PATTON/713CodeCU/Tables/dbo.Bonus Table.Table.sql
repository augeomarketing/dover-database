USE [713CodeCU]
GO
/****** Object:  Table [dbo].[Bonus Table]    Script Date: 03/17/2010 16:03:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bonus Table](
	[TipNumber] [varchar](15) NULL,
	[CustID] [varchar](9) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
