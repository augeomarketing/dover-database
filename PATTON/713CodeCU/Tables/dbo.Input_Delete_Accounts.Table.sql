USE [713CodeCU]
GO
/****** Object:  Table [dbo].[Input_Delete_Accounts]    Script Date: 03/17/2010 16:03:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Delete_Accounts](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardNumber] [nvarchar](255) NULL,
	[SystembankID] [float] NULL,
	[Principlebankid] [float] NULL,
	[AgentBankID] [float] NULL,
	[AcctName1] [nvarchar](255) NULL,
	[AcctName2] [nvarchar](50) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL,
	[HomePhone] [nvarchar](255) NULL,
	[StatusCode] [nvarchar](255) NULL,
	[External Status Code] [nvarchar](255) NULL,
	[CustID] [nvarchar](255) NULL,
	[Secondary Social Security Number Text] [nvarchar](255) NULL,
	[Account Identifier2] [nvarchar](255) NULL,
	[External Status Code Last Change Date] [nvarchar](255) NULL,
	[Miscellaneous Seventh Text] [nvarchar](255) NULL,
	[TipNumber] [nvarchar](15) NULL,
	[Lastname] [nvarchar](50) NULL,
	[Address4] [nvarchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[StatusDescription] [nvarchar](50) NULL
) ON [PRIMARY]
GO
