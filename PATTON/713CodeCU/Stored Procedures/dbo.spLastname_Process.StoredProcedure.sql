USE [713CodeCU]
GO
/****** Object:  StoredProcedure [dbo].[spLastname_Process]    Script Date: 03/17/2010 16:00:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLastname_Process]
 AS

UPDATE    input_customer
SET              lastname = SUBSTRING(acctname1, 
1, CHARINDEX(',', acctname1) - 1)
WHERE     SUBSTRING(acctname1, 
1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE 
'%,%'

update roll_customer set lstname = b.lastname
from roll_customer a, input_customer b
where a.tipnumber = b.tipnumber
GO
