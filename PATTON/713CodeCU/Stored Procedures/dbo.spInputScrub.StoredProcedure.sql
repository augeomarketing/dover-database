USE [713CodeCU]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 03/17/2010 16:00:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[spInputScrub] AS

/*******************************************************************************************/
/* Changes: 1) add zipcode to addr4                                                        */
/*******************************************************************************************/

Truncate Table  Input_Customer_error
Truncate Table Input_Transactions_error

update Input_Customer
set AcctName1=replace(AcctName1,char(39), ' '), Address1=replace(Address1,char(39), ' '),
 Address2=replace( Address2,char(39), ' '),   City=replace(City,char(39), ' ')

update Input_Customer
set AcctName1=replace(AcctName1,char(140), ' '),  Address1=replace( Address1,char(140), ' '),
 Address2=replace( Address2,char(140), ' '),   City=replace(City,char(140), ' ')
  
update Input_Customer
set AcctName1=replace(AcctName1,char(44), ' '),  Address1=replace( Address1,char(44), ' '),
 Address2=replace( Address2,char(44), ' '),  City=replace(City,char(44), ' ')
  
update Input_Customer
set AcctName1=replace(AcctName1,char(46), ' '),  Address1=replace( Address1,char(46), ' '),
 Address2=replace( Address2,char(46), ' '),   City=replace(City,char(46), ' ')

update Input_Customer
set AcctName1=replace(AcctName1,char(34), ' '),  Address1=replace( Address1,char(34), ' '),
 Address2=replace( Address2,char(34), ' '),   City=replace(City,char(34), ' ')

update Input_Customer
set AcctName1=replace(AcctName1,char(35), ' '),  Address1=replace( Address1,char(35), ' '),
 Address2=replace( Address2,char(35), ' '),   City=replace(City,char(35), ' ')

update Input_Customer set zipcode = left(zipcode,5) + '-' + right(zipcode,4)
where len(zipcode) > 5

--Change (1)
update input_customer set address4 = rtrim(city) + ' ' + rtrim(state) + ' ' + left(rtrim(zipcode),5)

--------------- Input Customer table

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_Customer_error 
	select * from Input_Customer 
	where (custid is null or custid = ' ') or  
	      (Cardnumber is null or Cardnumber = ' ') or
	      (AcctName1 is null or AcctName1 = ' ')

delete from Input_Customer 
where (custid is null or custid = ' ') or  
      (cardnumber is null or cardnumber = ' ') or
      (AcctName1 is null or AcctName1 = ' ')
GO
