USE [713CodeCU]
GO

/****** Object:  StoredProcedure [dbo].[spSetQuarterlyAuditFileName]    Script Date: 04/09/2010 16:39:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetQuarterlyAuditFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetQuarterlyAuditFileName]
GO

USE [713CodeCU]
GO

/****** Object:  StoredProcedure [dbo].[spSetQuarterlyAuditFileName]    Script Date: 04/09/2010 16:39:35 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spSetQuarterlyAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='S713' + @currentdate + '.xls'
 
set @newname='O:\713\Output\QuarterlyFiles\Audit.bat ' + @filename
set @ConnectionString='O:\713\Output\QuarterlyFiles\' + @filename

GO


