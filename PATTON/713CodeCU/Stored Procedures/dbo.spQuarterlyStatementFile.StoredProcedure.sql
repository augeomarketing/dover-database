USE [713CodeCU]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spCombineTips]    Script Date: 02/17/2012 12:09:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatementFile]
GO

CREATE PROCEDURE [dbo].[spQuarterlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 2/16/2012  Added Fling Points 
*/
/*******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city,state,zip)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state) , zipcode
from customer

/* Load the statmement file with purchases          */
update Quarterly_Statement_File
set pointspurchased=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like'6%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like '6%')

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like'[BM]%' )
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like '[BM]%' )

/* Load the statmement file with bonusesMN           */
update Quarterly_Statement_File
set pointsbonusMN=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ('F0','F9','G0','G9'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ('F0','F9','G0','G9'))

/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='IE')

/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased=pointspurchased + pointsbonus + pointsadded+pointsbonusMN

/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with returns            */
update Quarterly_Statement_File
set pointsreturned=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like '3%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like '3%')

/* Load the statmement file with decrease redemptions          */
update Quarterly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='DE')

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set ExpPts =(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='XP')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='XP')


/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted + ExpPts 

/* Load the statmement file with the Beginning balance for the Month */
/*update Quarterly_Statement_File
set pointsbegin=(select BeginningPoints from QTRBeginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber) */
set @SQLUpdate=N'update Quarterly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table 
where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
GO
