USE [713CodeCU]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spCombineTips]    Script Date: 02/17/2012 12:09:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp713StageMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp713StageMonthlyAuditValidation]
GO

CREATE PROCEDURE [dbo].[sp713StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchased numeric(9), @pointsbonus numeric(9), 
	@pointsadded numeric(9), @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturned numeric(9), @pointssubtracted numeric(9), 
	@pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) , @pointsBonusMN numeric(9)

delete from Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchased, pointsbonus, pointsadded, pointsincreased, 
					pointsredeemed, pointsreturned, 	pointssubtracted, pointsdecreased , pointsBonusMN
				from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased,
								 @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased, @pointsBonusMN
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_ErrorFile
			( Tipnumber, PointsBegin, PointsEnd, PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, 
					PointsReturned, PointsSubtracted, PointsDecreased, Errormsg, Currentend, PointsBonusMN ) 
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, 
       				@pointsreturned, @pointssubtracted, @pointsdecreased, @errmsg, @currentend,@pointsBonusMN)
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, 
										@pointsredeemed,	@pointsreturned, @pointssubtracted, @pointsdecreased,@pointsBonusMN

	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
