USE [713CodeCU]
GO
/****** Object:  StoredProcedure [dbo].[sp713StageMonthlyStatement]    Script Date: 02/16/2012 15:46:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp713StageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp713StageMonthlyStatement]
GO

/****** Object:  StoredProcedure [dbo].[sp713StageMonthlyStatement]    Script Date: 03/17/2010 16:00:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- RDT 2/16/2012  Added Fling Points 
*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[sp713StageMonthlyStatement]  @StartDateParm char(10), @EndDateParm char(10)

AS 


Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006


set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin= Month(@StartDate)
set @MonthBucket='MonthBeg' + @monthbegin



/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city, state, zipcode)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state) , left(zipcode,5)
from customer_stage

/* Load the statmement file with purchases          */
update Monthly_Statement_File
set pointspurchased=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode like'6%')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode like'6%')

/* Load the statmement file with bonuses            */
update Monthly_Statement_File
set pointsbonus=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like'[BM]%' )
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like '[BM]%' )

/* Load the statmement file with pointsBonusMN            */
update Monthly_Statement_File
set pointsBonusMN = (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ( 'F0', 'G0' , 'F9', 'G9') )
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ( 'F0','G0' , 'F9', 'G9')) 


/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode='IE' or trancode = 'XF'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and (trancode='IE' or trancode = 'XF'))

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased=pointspurchased + pointsbonus + pointsadded + pointsBonusMN

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with decrease redemptions          */
update Monthly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

/* Load the statmement file with returns            */
update Monthly_Statement_File
set pointsreturned=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode like '3%')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and trancode like '3%')


/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and
 histdate>=@startdate and histdate<=@enddate and (trancode='XP' or trancode='DE'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and 
histdate>=@startdate and histdate<=@enddate and  (trancode='XP' or trancode='DE'))

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
/*update Monthly_Statement_File
set pointsbegin=(select BeginningPoints from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber) */
set @SQLUpdate=N'update Monthly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
GO
