USE [713CodeCU]
GO
/****** Object:  StoredProcedure [dbo].[spNEBADelete]    Script Date: 03/17/2010 16:00:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spNEBADelete] @DateInput as datetime AS   

--CREATE PROCEDURE spNEBADelete @DateInput as datetime AS   
--declare @DateInput as datetime
--set @DateInput = '03/24/2008'
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat set AcctStatus = 'A' where AcctStatus is null


	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer set Status = 'A' where Status is null


	/*  **************************************  */
	-- Mark affiliat records for deletion with "C"
	update Affiliat	
	set Acctstatus = 'C'
	where tipnumber in 
	(select tipnumber from customer where status = 'C')
	--where exists 
	--	( select tipnumber from customer
	--	  where  status = 'C')
	


	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat 
	 where tipnumber in 
	(select tipnumber from customer where status = 'C')
	

	/*  **************************************  */
	Delete from affiliat where acctstatus = 'C'
	
		
	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 'C'

	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history 
	where tipnumber in 
	(select tipnumber from customer where status = 'C')



	/*  **************************************  */
	-- Delete History
	Delete from history where tipnumber in (select tipnumber from customer where status = 'C')
	


	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @DateDeleted 
	from customer WHERE  status = 'C'


	

	/*  **************************************  */
	-- Delete Customer
	Delete from customer where status = 'C'
	/* Return 0 */
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
