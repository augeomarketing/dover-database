USE [713CodeCU]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionScrub]    Script Date: 03/17/2010 16:00:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTransactionScrub]
 AS

--------------- Input Transaction table

Insert into Input_Transactions_error 
	select * from Input_Transactions 
	where (cardnumber is null or cardnumber = ' ') or
	      (tipnumber is null or tipnumber = ' ') or
	      (purchase is null  or [returns] is null or bonus is null)
	       
Delete from Input_Transactions
where (cardnumber is null or cardnumber = ' ') or
      (tipnumber is null or tipnumber = ' ') or
      (purchase is null  or (purchase =  0 and [Returns] = 0 and Bonus = 0))


/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transactions 
set  purchase = round(purchase,0),  [Returns] =  round([returns],0), bonus = round(Bonus,0)
GO
