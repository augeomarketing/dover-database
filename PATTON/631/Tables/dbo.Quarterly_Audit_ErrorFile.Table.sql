USE [631Munising]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 05/04/2011 11:00:12 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Begin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_End]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Net]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Bonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_CRD_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_CRD_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_DBT_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_DBT_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_ADJ_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[TOT_Begin] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]  DEFAULT ((0)),
	[TOT_End] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]  DEFAULT ((0)),
	[TOT_Add] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]  DEFAULT ((0)),
	[TOT_Subtract] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]  DEFAULT ((0)),
	[TOT_Net] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]  DEFAULT ((0)),
	[TOT_Bonus] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]  DEFAULT ((0)),
	[TOT_Redeem] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]  DEFAULT ((0)),
	[TOT_ToExpire] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]  DEFAULT ((0)),
	[CRD_Purch] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]  DEFAULT ((0)),
	[CRD_Return] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]  DEFAULT ((0)),
	[DBT_Purch] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]  DEFAULT ((0)),
	[DBT_Return] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]  DEFAULT ((0)),
	[ADJ_Add] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]  DEFAULT ((0)),
	[ADJ_Subtract] [numeric](18, 0) NULL CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]  DEFAULT ((0)),
	[ErrorMsg] [nvarchar](50) NULL,
	[CurrentEnd] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Quarterly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
