/****** Object:  Table [dbo].[deletionfile]    Script Date: 06/26/2009 08:33:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[deletionfile](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[RunAvailable] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
