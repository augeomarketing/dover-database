/****** Object:  Table [dbo].[DeletedReport]    Script Date: 06/26/2009 08:33:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeletedReport](
	[Tipnumber] [nvarchar](255) NULL,
	[Acctname1] [nvarchar](255) NULL,
	[Acctname2] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[PointsEnd] [float] NULL,
	[PointsNow] [float] NULL
) ON [PRIMARY]
GO
