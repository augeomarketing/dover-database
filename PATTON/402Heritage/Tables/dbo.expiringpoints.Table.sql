/****** Object:  Table [dbo].[expiringpoints]    Script Date: 06/26/2009 08:33:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[expiringpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[DateofExpire] [datetime] NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[POINTSTOEXPIRE] [float] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
