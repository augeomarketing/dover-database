/****** Object:  Table [dbo].[MonthlySummary]    Script Date: 06/26/2009 08:33:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlySummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[CRPurchases] [decimal](18, 2) NULL CONSTRAINT [DF_MonthlySummary_CRPurchases]  DEFAULT (0),
	[CRReturned] [decimal](18, 2) NULL CONSTRAINT [DF_MonthlySummary_CRReturned]  DEFAULT (0),
	[DBPurchases] [decimal](18, 2) NULL CONSTRAINT [DF_MonthlySummary_DBPurchases]  DEFAULT (0),
	[DBReturned] [decimal](18, 0) NULL CONSTRAINT [DF_MonthlySummary_DBReturned]  DEFAULT (0),
	[PLPurchases] [decimal](18, 0) NULL CONSTRAINT [DF_MonthlySummary_PLPurchases]  DEFAULT (0),
	[PLReturned] [decimal](18, 0) NULL CONSTRAINT [DF_MonthlySummary_PLReturned]  DEFAULT (0),
	[Bonuses] [decimal](18, 0) NULL CONSTRAINT [DF_MonthlySummary_Bonuses]  DEFAULT (0)
) ON [PRIMARY]
GO
