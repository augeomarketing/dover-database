/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 06/26/2009 08:33:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [char](40) NULL,
	[Acctname4] [char](40) NULL,
	[Acctname5] [char](40) NULL,
	[Acctname6] [char](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[CRPointsPurchased] [decimal](18, 0) NULL,
	[PLPointsPurchased] [decimal](18, 0) NULL,
	[DbPointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[CRPointsReturned] [decimal](18, 0) NULL,
	[PLPointsReturned] [decimal](18, 0) NULL,
	[DBPointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[ExpPnts] [decimal](18, 0) NULL,
	[Status] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
