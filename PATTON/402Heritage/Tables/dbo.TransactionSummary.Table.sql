/****** Object:  Table [dbo].[TransactionSummary]    Script Date: 06/26/2009 08:34:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionSummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[CRPurchases] [decimal](18, 2) NULL,
	[CRReturned] [decimal](18, 2) NULL,
	[DBPurchases] [decimal](18, 2) NULL,
	[DBReturned] [decimal](18, 0) NULL,
	[PLPurchases] [decimal](18, 0) NULL,
	[PLReturned] [decimal](18, 0) NULL,
	[Bonuses] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
