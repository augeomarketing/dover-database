/****** Object:  Table [dbo].[MarketingBonus]    Script Date: 06/26/2009 08:33:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MarketingBonus](
	[TipNumber] [varchar](15) NULL,
	[CardNumber] [varchar](25) NULL,
	[TransDate] [varchar](10) NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [varchar](4) NULL,
	[Points] [char](15) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL,
	[Overage] [decimal](18, 0) NULL CONSTRAINT [DF_MarketingBonus_Overage]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
