/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 06/26/2009 08:32:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT_Stage](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NOT NULL CONSTRAINT [DF_AFFILIAT_Stage_YTDEarned]  DEFAULT (0),
	[CustID] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
