/****** Object:  Table [dbo].[BegBalanceCheck]    Script Date: 06/26/2009 08:33:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BegBalanceCheck](
	[OldBegBalance] [decimal](18, 0) NULL,
	[NewBegBalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
