/****** Object:  Table [dbo].[Monthly_Audit_Error_File]    Script Date: 06/26/2009 08:33:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Audit_Error_File](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[CRPointsPurchased] [decimal](18, 0) NULL,
	[PLPointsPurchased] [decimal](18, 0) NULL,
	[DbPointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[CRPointsReturned] [decimal](18, 0) NULL,
	[PLPointsReturned] [decimal](18, 0) NULL,
	[DBPointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[ErrorMsg] [varchar](50) NULL,
	[CurrentEnd] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
