/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 06/26/2009 08:31:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [TranType] ) 
select  [TipNumber], @dateadded, [cardnumber], [TranCode], [trancount], convert(char(15), [Points]), [description]  from Input_Transactions 


/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
--Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
--on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode
GO
