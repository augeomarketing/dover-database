/****** Object:  StoredProcedure [dbo].[spTransactionSummary]    Script Date: 06/26/2009 08:31:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTransactionSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table TransactionSummary

insert TransactionSummary (dateadded)
values (@enddate)

update  TransactionSummary set crpurchases = (select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '63' )
update  TransactionSummary set dbpurchases = (select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '67')
update  TransactionSummary set plpurchases = (select SUM(CONVERT(int, points)) FROM transactions  WHERE trancode = '61')

update   TransactionSummary set crreturned =(select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '33')
update   TransactionSummary set dbreturned =(select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '37')
update   TransactionSummary set plreturned =(select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '31')

update reportsummary set purchases = (select (crpurchases + dbpurchases + plpurchases) from transactionsummary)
update reportsummary set returned = (select (crreturned + dbreturned + plreturned) from transactionsummary)
GO
