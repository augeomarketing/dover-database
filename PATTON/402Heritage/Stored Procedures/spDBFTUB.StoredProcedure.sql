USE [402Heritage]
GO
/****** Object:  StoredProcedure [dbo].[spDBFTUB]    Script Date: 11/05/2009 09:25:06 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[spDBFTUB]  @PointValue numeric(9), @ProcessDate nvarchar(10)
as
/****************************************************************************/
/*                                                                          */
/* Procedure to insert monthly transactions based on the input transactions */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

--declare @pointvalue numeric(9)
--declare @processdate nvarchar(10)
--set @pointvalue = 100
--set @processdate = '07/31/2009'

declare @ProcessInd numeric(9)
set @ProcessInd = 0

insert marketingbonus(tipnumber)
select distinct(tipnumber) from input_transactions 
where trancode = '67' and points <> 0 order by tipnumber

delete from marketingbonus
where tipnumber in(select tipnumber from onetimebonuses_stage)

delete from marketingbonus
where tipnumber is null or tipnumber = ' '


set @ProcessInd = (Select count(*) from dbo.MarketingBonus)

if (@processIND > 0)
	Begin
		
		insert dbo.input_transactions(tipnumber)
		select tipnumber from marketingbonus
		
		update dbo.input_transactions set transdate = @processdate, TranCode = 'BF',
			TranCount = '1', points = @pointvalue,
			[Description] = 'Bonus First Time Debit_Credit Card Use',ratio = '1'
			where trancode is null
			
		insert dbo.onetimebonuses_stage(tipnumber)
			select tipnumber from dbo.marketingbonus
					
	end
	
GO
