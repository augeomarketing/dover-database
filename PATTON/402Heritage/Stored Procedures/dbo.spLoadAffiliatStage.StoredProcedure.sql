USE [402Heritage]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 10/21/2010 09:28:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

USE [402Heritage]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 10/21/2010 09:28:25 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/*  *******************************************************************************/
/*  Description: Copies data from roll_customer to the Affiliat_stage table       */
/*	Affiliat_Stage	- Insert , update                                             */
/*                                                                                */
/*  Changes: 1) Update affiliat_stage secid with any replacement cards            */
/*  *******************************************************************************/

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, DateAdded, Accttype, secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.acctid, c.TipNumber, @monthend, accttype, c.last6,  'A', accttypedesc, c.LastName, 0, c.custid
	from roll_Customer c where c.acctid not in ( Select acctid from Affiliat_Stage)

/*** Change(1) Update the Affiliat_Stage secid with any new replacement cards (last6)  ****/

  Update AFS Set secid = last6 
  from affiliat_stage afs Join Roll_customer rc on afs.acctid = rc.Acctid
  where (afs.acctid = rc.Acctid) and (SECID != last6)
 
GO


