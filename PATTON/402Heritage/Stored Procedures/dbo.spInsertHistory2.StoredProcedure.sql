/****** Object:  StoredProcedure [dbo].[spInsertHistory2]    Script Date: 06/26/2009 08:31:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spInsertHistory2]  @ProcessIND nvarchar(1)
as
/****************************************************************************/
/*                                                                          */
/* Procedure to insert monthly transactions based on the input transactions */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

     if (@processIND = 1)
	Begin
		insert history (tipnumber,acctid,histdate,trancode,trancount,points,[description],secid,ratio,overage)
		         select tipnumber,cardnumber as acctid,transdate as histdate,trancode,trancount,points,[description],secid,ratio,overage from transactions

		update 	cus
			set runavailable = dbo.fnSumPointsByTip(tipnumber),
			      runbalance = dbo.fnSumPointsByTip(tipnumber) + runredeemed
		from dbo.customer cus 
		where cus.tipnumber in (select distinct tipnumber from dbo.transactions)
	end

     Else
	Begin
		insert history (tipnumber,acctid,histdate,trancode,trancount,points,[description],secid,ratio,overage)
		          select tipnumber,cardnumber as acctid,transdate as histdate,trancode,trancount,points,[description],secid,ratio,overage from marketingbonus

		update 	cus
			set runavailable = runavailable + 100,
			      runbalance = runbalance + 100
		from dbo.customer cus 
		where cus.tipnumber in (select distinct tipnumber from dbo.marketingbonus)
	end
GO
