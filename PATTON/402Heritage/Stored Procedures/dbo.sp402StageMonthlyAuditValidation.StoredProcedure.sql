/****** Object:  StoredProcedure [dbo].[sp402StageMonthlyAuditValidation]    Script Date: 06/26/2009 08:31:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp402StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @crpointspurchased numeric(9), @plpointspurchased numeric(9), @dbpointspurchased numeric(9), 
@pointsbonus numeric(9), @pointsadded numeric(9), @pointsincreased numeric(9), @pointsredeemed numeric(9), @crpointsreturned numeric(9), 
@plpointsreturned numeric(9), @dbpointsreturned numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) 

delete from Monthly_Audit_Error_File

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, crpointspurchased, plpointspurchased, dbpointspurchased, pointsbonus, pointsadded, pointsincreased, 
	   pointsredeemed, crpointsreturned, plpointsreturned, dbpointsreturned, pointssubtracted, pointsdecreased 
from Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @crpointspurchased, @plpointspurchased, @dbpointspurchased, @pointsbonus, @pointsadded, 
	@pointsincreased, @pointsredeemed, @crpointsreturned, @plpointsreturned, @dbpointsreturned, @pointssubtracted, @pointsdecreased
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Monthly_Audit_Error_File
       			values(@Tipnumber, @pointsbegin, @pointsend, @crpointspurchased, @plpointspurchased, @dbpointspurchased, @pointsbonus, 
		@pointsadded, @pointsincreased, @pointsredeemed, @crpointsreturned, @plpointsreturned, @dbpointsreturned, @pointssubtracted, @pointsdecreased,
		@errmsg, @currentend)
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @crpointspurchased, @plpointspurchased, @dbpointspurchased, @pointsbonus, @pointsadded, 
	@pointsincreased, @pointsredeemed, @crpointsreturned, @plpointsreturned, @dbpointsreturned, @pointssubtracted, @pointsdecreased
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
