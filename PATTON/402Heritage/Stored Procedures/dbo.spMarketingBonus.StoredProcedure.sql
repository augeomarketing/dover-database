/****** Object:  StoredProcedure [dbo].[spMarketingBonus]    Script Date: 06/26/2009 08:31:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMarketingBonus]  @PointValue numeric(9), @ProcessDate nvarchar(10)
as
/****************************************************************************/
/*                                                                          */
/* Procedure to insert monthly transactions based on the input transactions */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

--truncate table marketingbonus

--declare @pointvalue numeric(9)
--declare @processdate nvarchar(10)
--set @pointvalue = 100
--set @processdate = '09/30/2008'

declare @ProcessInd numeric(9)

set @ProcessInd = (Select count(*) from dbo.MarketingBonus)

if (@processIND > 0)
	Begin
		update dbo.marketingbonus set Tipnumber = b.tipnumber
			FROM dbo.marketingbonus a, affiliat_stage b
			WHERE a.cardnumber = b.acctid

		update dbo.marketingbonus set transdate = @processdate, TranCode = 'BI',
			TranCount = '1', points = @pointvalue, [Description] = 'Marketing Bonus'
			
		insert dbo.input_transactions_error(cardnumber,trancode,trancount,points,tipnumber,
 			transdate,[description])
			select cardnumber,trancode,trancount,points,tipnumber,
			transdate,[description]from dbo.marketingbonus
			where (TipNumber IS NULL) or (cardnumber IS NULL)
		
		delete from dbo.marketingbonus
			WHERE (TipNumber IS NULL) or (cardnumber IS NULL)

		insert dbo.input_transactions(cardnumber,trancode,trancount,points,tipnumber,
 			transdate,[description])
			select cardnumber,trancode,trancount,points,tipnumber,
			transdate,[description] from dbo.marketingbonus
					
	end
GO
