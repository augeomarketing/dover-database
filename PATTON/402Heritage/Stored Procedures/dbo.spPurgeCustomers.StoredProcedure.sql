/****** Object:  StoredProcedure [dbo].[spPurgeCustomers]    Script Date: 06/26/2009 08:31:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurgeCustomers]  AS   

Declare @DateDeleted as varchar(10)
set @DateDeleted = '05/30/2009'

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateDeleted) = 1 
   Begin
	--Set @DateDeleted = @DateInput 

	/*  **************************************  */
	-- Mark affiliat records for deletion with "C"
	--update Affiliat	
	--set Acctstatus = 'C'
	--where tipnumber in 
	--(select tipnumber from customer where status = 'C')
	--where exists 
	--	( select tipnumber from customer
	--	  where  status = 'C')
	


	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat 
	 where tipnumber in 
	(select tipnumber from customer where status = 'C')
	

	/*  **************************************  */
	Delete from affiliat  where tipnumber in 
	(select tipnumber from customer where status = 'C')
	
	
		
	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 'C'

	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history 
	where tipnumber in 
	(select tipnumber from customer where status = 'C')



	/*  **************************************  */
	-- Delete History
	Delete from history where tipnumber in (select tipnumber from customer where status = 'C')
	


	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @DateDeleted 
	from customer WHERE  status = 'C'


	

	/*  **************************************  */
	-- Delete Customer
	Delete from customer where status = 'C'
	/* Return 0 */
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
