/****** Object:  StoredProcedure [dbo].[spMonthlyReportSummary]    Script Date: 06/26/2009 08:31:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyReportSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table ReportSummary

insert ReportSummary (dateadded)
values (@enddate)

update  ReportSummary set purchases = (select SUM(CONVERT(int, pointspurchased)) FROM monthly_statement_file)

update  ReportSummary set returned =(select SUM(CONVERT(int, pointsreturned)) FROM monthly_statement_file)

update ReportSummary set bonuses = (select sum(convert(int,pointsbonus)) from  monthly_statement_file)

update BegBalanceCheck set newbegbalance = (select sum(convert(int,pointsbegin)) from monthly_statement_file)
GO
