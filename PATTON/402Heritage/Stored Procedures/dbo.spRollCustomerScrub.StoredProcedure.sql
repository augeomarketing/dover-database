use [402Heritage] 
go
/****** Object:  StoredProcedure [dbo].[spRollCustomerScrub]    ***/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollCustomerScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRollCustomerScrub]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRollCustomerScrub] @enddate varchar(10)
 AS

/********************************************************************************************/
-- CHANGES:
-- RDT 4/15/2011 Changed 1Bridge Codes to Card Bin numbers. 
/********************************************************************************************/

update roll_customer  set accttype = 'Credit', accttypedesc = 'Credit Card'
-- RDT 4/15/2011 where left(acctid,6) = '410960' or left(acctid,6) = '000802'
where left(acctid,6) = '410960' 

update  roll_customer  set accttype = 'Credit', accttypedesc = 'Platnium Credit Card'
-- RDT 4/15/2011 where left(acctid,6) = '000804'
where left(acctid,6) = '449747'

update roll_customer  set accttype = 'Debit', accttypedesc = 'Debit Card'
-- RDT 4/15/2011 where left(acctid,6) = '000803'
where left(acctid,6) = '426403'

update roll_customer set DateAdded =  (select dateadded from  CUSTOMER_stage
where customer_stage.tipnumber = roll_customer.tipnumber)

update roll_customer set dateadded = CONVERT(datetime, @enddate)
where  (dateadded is NULL)
GO
