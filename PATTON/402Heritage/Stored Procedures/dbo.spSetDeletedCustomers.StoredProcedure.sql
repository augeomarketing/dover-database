/****** Object:  StoredProcedure [dbo].[spSetDeletedCustomers]    Script Date: 06/26/2009 08:31:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetDeletedCustomers]   AS

update customer set status = 'P',statusdescription = 'Pending Deletion'
where status = 'D'
GO
