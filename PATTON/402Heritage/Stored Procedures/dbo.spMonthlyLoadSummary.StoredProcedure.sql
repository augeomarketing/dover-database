/****** Object:  StoredProcedure [dbo].[spMonthlyLoadSummary]    Script Date: 06/26/2009 08:31:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spMonthlyLoadSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table MonthlySummary

insert MonthlySummary (dateadded)
values (@enddate)

update  MonthlySummary set crpurchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '63'  and histdate = @enddate)
update  MonthlySummary set dbpurchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '67'  and histdate = @enddate)
update  MonthlySummary set plpurchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '61'  and histdate = @enddate)

update   MonthlySummary set crreturned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '33' and histdate =  @enddate)
update   MonthlySummary set dbreturned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '37' and histdate =  @enddate)
update   MonthlySummary set plreturned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '31' and histdate =  @enddate)

update   MonthlySummary set bonuses =(select SUM(CONVERT(int, points)) FROM history WHERE (trancode = 'BO' or trancode = 'BI') and histdate =  @enddate)
GO
