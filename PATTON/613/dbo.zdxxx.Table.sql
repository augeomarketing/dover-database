SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zdxxx](
	[ssn] [nchar](25) NULL,
	[ACCTID] [varchar](25) NULL,
	[PtsThruOct] [float] NULL,
	[NovPts] [float] NULL,
	[PointsThruNov4] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
