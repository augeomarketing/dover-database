SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrksig](
	[tipnumber] [char](15) NULL,
	[trans] [numeric](38, 0) NULL,
	[amount] [decimal](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
