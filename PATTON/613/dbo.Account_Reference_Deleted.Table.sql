SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account_Reference_Deleted](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
