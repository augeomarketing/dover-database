SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zTip_SSN_Cards](
	[tipnumber] [nchar](15) NOT NULL,
	[SSN] [nchar](25) NOT NULL,
	[Cardnumber] [nchar](25) NOT NULL
) ON [PRIMARY]
GO
