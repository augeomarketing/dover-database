SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zpSetupRemainingToTransWork_CECONVERSION] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
--this gets run for the transaction records for 11/1-11/4


truncate table transwork
truncate table transstandard


insert into transwork (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID)
select TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID 
from COOPWork.dbo.TransDetailHold
where left(pan,6)='553854' and sic<>'6011' and processingcode in ('000000', '002000', '200020', '200040') and (left(msgtype,2) in ('02', '04')) and Trandate>=@StartDate and Trandate<=@EndDate

-- Signature Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid in('MCI', 'VNT') 

-- DELETE PIN Debit
DELETE transwork
where netid not in('MCI', 'VNT') 


/* make ths change effective Nov 1 (Oct data) per RT ticket #975    */
-- PIN Debit
/*   old code below. Removed for Oct-08 data processing
update transwork
set points=ROUND(((amounttran/100)/3), 10, 0)
where netid not in('MCI', 'VNT') 
*/


--Put to standard transtaction file format purchases.
-- SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, '67', NumberOfTrans, points, 'DEBIT', '1', ' ' from transwork
--where processingcode in ('000000', '002000', '500000', '500020', '500010') 		        	

-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' from transwork
/* SEB002 where processingcode in ('000000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02', '04')) */		        	
/* SEB002 */ where processingcode in ('000000', '002000') and left(msgtype,2) in ('02') 		        	
group by tipnumber, Pan
	
--Put to standard transtaction file format returns.
-- SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, '37', NumberOfTrans, points, 'DEBIT', '-1', ' ' from transwork
--where processingcode in ('200020', '200040')

-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' from transwork
/* SEB002 where processingcode in ('200020', '200040') */
/* SEB002 */ where processingcode in ('200020', '200040') or left(msgtype,2) in ('04')
group by tipnumber, Pan
GO
