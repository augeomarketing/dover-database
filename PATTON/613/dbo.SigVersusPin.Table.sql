SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SigVersusPin](
	[tipnumber] [nchar](15) NULL,
	[sigtrans] [numeric](18, 0) NULL,
	[sigdollars] [money] NULL,
	[pintrans] [numeric](18, 0) NULL,
	[pindollars] [money] NULL
) ON [PRIMARY]
GO
