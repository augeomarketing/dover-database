USE [DBAWorkDB]
GO
/****** Object:  Table [dbo].[ddllog]    Script Date: 03/03/2010 11:51:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ddllog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ddllog](
	[sid_ddllog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ddllog_posttime] [datetime] NULL,
	[dim_ddllog_dbuser] [nvarchar](250) NULL,
	[dim_ddllog_dbname] [nvarchar](100) NULL,
	[dim_ddllog_ddlevent] [nvarchar](250) NULL,
	[dim_ddllog_ddltsql] [nvarchar](max) NULL,
 CONSTRAINT [PK__ddllog__6F0497EB7F60ED59] PRIMARY KEY CLUSTERED 
(
	[sid_ddllog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
