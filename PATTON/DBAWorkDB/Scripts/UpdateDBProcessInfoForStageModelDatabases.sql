DECLARE @db TABLE
(
	sid_db_id INT IDENTITY (1,1) PRIMARY KEY
	, dim_db_dbnamepatton VARCHAR(50)
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
)

DECLARE 
	@sid_db_id INT = 1
	, @maxdbid INT
	, @dim_db_dbnamepatton VARCHAR(50)
	, @sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @sqlUpdate NVARCHAR(MAX)	
	, @debug BIT = 1

IF @debug = 1
	RAISERROR('Inserting Existing Database Names into Temp Table',0,1)

INSERT INTO @db (dim_db_dbnamepatton, sid_dbprocessinfo_dbnumber)
SELECT DBNamePatton, DBNumber
FROM RewardsNow.dbo.dbprocessinfo dbpi
INNER JOIN sys.databases sdb
ON dbpi.DBNamePatton = sdb.name

SELECT @maxdbid = MAX(sid_db_id) FROM @db

IF @debug = 1
	RAISERROR('%i Databases to process',0,1, @maxdbid)


WHILE @sid_db_id <= @maxdbid
BEGIN

	IF @debug = 1
		RAISERROR('---> Processing Database #%i',0,1, @sid_db_id)
	

	SELECT @sqlUpdate = REPLACE(REPLACE(
		'IF (SELECT COUNT(*) from [<DBNAME>].INFORMATION_SCHEMA.TABLES where TABLE_NAME = ''CUSTOMER_STAGE'') > 0 '
		+ '	BEGIN '
		+ '		UPDATE RewardsNow.dbo.dbprocessinfo SET IsStageModel = 1 WHERE DBNumber = ''<DBNUMBER>'''
		+ '	END '
		, '<DBNAME>', dim_db_dbnamepatton)
		, '<DBNUMBER>', sid_dbprocessinfo_dbnumber)
	FROM
		@db
	WHERE sid_db_id = @sid_db_id

	EXEC sp_executesql @sqlUpdate	

	SET @sid_db_id = @sid_db_id + 1
END

IF @debug = 1
	RAISERROR('Processing Complete',0,1)
	
SELECT * FROM RewardsNow.dbo.dbprocessinfo WHERE IsStageModel = 1
