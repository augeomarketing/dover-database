
IF EXISTS (SELECT 1 FROM sys.server_triggers WHERE name = 'trig_ddllog_insert')
    drop trigger trig_ddllog_insert on all server
GO

create TRIGGER trig_ddllog_insert
ON all server
FOR CREATE_FUNCTION,
    ALTER_FUNCTION,
    DROP_FUNCTION,
    CREATE_PROCEDURE,
    ALTER_PROCEDURE,
    DROP_PROCEDURE,
    RENAME,
    CREATE_TABLE,
    ALTER_TABLE,
    DROP_TABLE,
    CREATE_TRIGGER,
    ALTER_TRIGGER,
    DROP_TRIGGER,
    CREATE_VIEW,
    ALTER_VIEW,
    DROP_VIEW,
    CREATE_DATABASE,
    ALTER_DATABASE,
    DROP_DATABASE

AS


DECLARE @data XML

SET @data = EVENTDATA()

INSERT into dbaworkdb.dbo.ddllog
   (dim_ddllog_posttime, dim_ddllog_dbuser, dim_ddllog_dbname, dim_ddllog_ddlevent, dim_ddllog_ddltsql) 
   VALUES 
   (GETDATE(), 
   @data.value('(/EVENT_INSTANCE/LoginName)[1]', 'nvarchar(250)'),
   @data.value('(/EVENT_INSTANCE/DatabaseName)[1]', 'nvarchar(100)'),
   @data.value('(/EVENT_INSTANCE/EventType)[1]', 'nvarchar(250)'), 
   @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(max)') ) ;
GO
