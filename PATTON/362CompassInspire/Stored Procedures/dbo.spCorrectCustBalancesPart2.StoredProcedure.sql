/****** Object:  StoredProcedure [dbo].[spCorrectCustBalancesPart2]    Script Date: 02/20/2009 15:52:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
create PROCEDURE [dbo].[spCorrectCustBalancesPart2]  AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @OLDESTDATE DATETIME

declare @RunredeemedHst numeric(10)
 

 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From wrktable1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber,@RunRedeemedin,@RunredeemedHst	
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunBalanceNew = '0'
set @RunBalanceold = '0'
set @RunRedeemed = '0'

set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 	
)

set @RunRedeemed  = 
(	select sum(points) from history 
	where @tipnumber = history.tipnumber 
	and (history.trancode like('R%'))
)

if @RunAvailiableNew is null 
set @RunAvailiableNew = '0'

if @RunRedeemed is null 
set @RunRedeemed = '0'
		
Update Customer 
set
 runbalance  = @RunAvailiableNew + @RunRedeemed
 ,runavailable = @RunAvailiableNew
 ,runredeemed = @RunRedeemed
where tipnumber = @TipNumber 


Fetch custfix_crsr  
into  @tipnumber,@RunRedeemedin,@RunredeemedHst		
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  account_crsr
deallocate  account_crsr
GO
