/****** Object:  StoredProcedure [dbo].[spBuildNameFileForPersonatorAPI]    Script Date: 02/20/2009 15:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the PERSNAME file to pass to the personatorAPI for 360ComPassPoints                */
/* */
/*   - Read cccust  */
/*  - Update PERSNAME        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBuildNameFileForPersonatorAPI] AS


Declare @BankNum char(5)
Declare @Name1 char(40)
Declare @Name2 char(40)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(16)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Product char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt  char(12)
Declare @PurchCnt  char(3)
Declare @ReturnAmt  char(12)
Declare @RetunCnt  char(3)
Declare @NetAmt  char(12)
Declare @NetCnt  char(3)
Declare @Multiplier  char(1)
Declare @ProductType char(2)
Declare @FixPurchAmt  char(12)
Declare @FixReturnAmt  char(3)
Declare @DateAdded char(10)
Declare @Rating char(10)
Declare @CityState char(50)
Declare @CreditAmt  char(10)
Declare @CreditCnt char(3)
Declare @DebitAmt  char(12)
Declare @DebitCnt char(3)
Declare @Hotflag char(1)
Declare @AcctType char(1)
Declare @CardNum char(16)
Declare @POSTDATE char(10)
Declare @Dda char(20)
declare @COUNT nvarchar(10)


/*   - DECLARE CURSOR AND OPEN TABLE of Customer Data */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr

	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @lastName, @POSTDATE 

	

IF @@FETCH_STATUS = 1
	goto Fetch_Error

set @COUNT = '0'
set @LastName = ' '
set @CardNum = '0'
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	


/* IF @COUNT =  '50000'
GoTo EndPROC  */

/* BUILD THE NAME FILE TO PASS TO THE PERSONATORAPI    */

	 Insert into PERSNAME
            (
	     ACCTNUM,
	     TIPNUMBER,
	     FULLNAME	     
            )
	  values
	    (
             @AcctNum,  @TipNumber, @lastName
	    )
      
	         

	
	
	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @lastName, @POSTDATE 


set @COUNT = (@COUNT + 1) 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
