/****** Object:  StoredProcedure [dbo].[spProcessCustCredit]    Script Date: 02/20/2009 15:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
/* CREATE PROCEDURE spProcessCustCredit AS   */
CREATE PROCEDURE [dbo].[spProcessCustCredit] AS
/* input */
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @AFFACCTID char(25)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Product char(2)
Declare @MAXTIP char(15)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchCnt int
Declare @ReturnAmt float
Declare @RetunCnt int
Declare @NetAmt float
Declare @NetCnt int
Declare @Multiplier real
Declare @ProductType char(2)
Declare @TierPurchAmt float
Declare @TierReturnAmt float
Declare @FixPurchAmt float
Declare @FixReturnAmt float
Declare @DateAdded char(10)
Declare @DATERUN char(10)
Declare @fmtspace char(1) 
Declare @fmtzero  int
Declare @Rating char(2)
Declare @Status char(2) 
/* Output */
Declare @TranAmt int
Declare @LIABILITYTOTAL int
Declare @TOTPURCHAMT int
Declare @TOTPURCHCNT int
Declare @TOTRETAMT int
Declare @TOTRETCNT int
Declare @TOTNETAMT int
Declare @TOTNETCNT int
Declare @TranCode char(2)
Declare @CardType char(20)
Declare @Ratio  numeric
Declare @AccTid varchar(25)
Declare @SecId varchar(10)
Declare @AcctStatus Varchar(1)
Declare @AcctTypeDecs varchar(50)
Declare @YTDEarned float(8)
Declare @SSNum char(13)


BEGIN 
	execute maxtip @MAXTIP
	set @LIABILITYTOTAL = '0'
	set @DATERUN = GETDATE()
	/*  - UPDATE CUST CREDIT TABLE with TIPNUMBER from AFFILIAT       */

	update cccust		      
	set
	    cccust.TIPNUMBER = af.TipNumber
	from dbo.AFFILIAT as af
	inner JOIN dbo.cccust as cust
	on af.ACCTID = cust.acctnum  
	where af.ACCTID in (select cust.ACCTNUM from cccust)

	/*  - UPDATE CUST CREDIT TABLE with CreditTrans Fields       */
	update cccust	
	set
	     cccust.PURCHAMT = '0'
	    ,cccust.PURCHCNT = '0'
                 ,cccust.RETURNAMT = '0'
                 ,cccust.RETURNCNT = '0'
                 ,cccust.NETAMT = '0'
	    ,cccust.NETCNT = '0'	
	

	update cccust	
	set
	     cccust.PURCHAMT = ct.PURCHAMT
	    ,cccust.PURCHCNT = ct.PURCHCNT
                 ,cccust.RETURNAMT = ct.RETURNAMT
                 ,cccust.RETURNCNT = ct.RETURNCNT
                 ,cccust.NETAMT = ct.NETAMT
	    ,cccust.NETCNT = ct.NETCNT
                 ,cccust.MULTIPLIER = ct.MULTIPLIER
	    ,@NetAmt = ct.NETAMT
	 /*   ,@LIABILITYTOTAL = (@LIABILITYTOTAL + @NetAmt)
	    ,@TOTPURCHAMT =(@TOTPURCHAMT + ct.PURCHAMT)
                 ,@TOTPURCHCNT = (@TOTPURCHCNT + ct.PURCHCNT) 
                 ,@TOTRETAMT = (@TOTRETAMT + ct.RETURNAMT)
                 ,@TOTRETCNT = (@TOTRETCNT + ct.RETURNCNT)
                 ,@TOTNETAMT = (@TOTNETAMT + ct.NETAMT)
                 ,@TOTNETCNT = (@TOTNETAMT + ct.NETAMT) */
	from dbo.cctran as ct
	inner JOIN dbo.cccust as cust
	on ct.ACCTNUM = cust.acctnum  
	where ct.ACCTNUM in (select cust.ACCTNUM from cccust) 

/*  - UPDATE CUST CREDIT TABLE with TIPNUMBER from AFFILIAT Based on CUST.OLDCCNUM       */

	update cccust	
	set
	    cccust.TIPNUMBER = af.TipNumber
	from dbo.AFFILIAT as af
	inner JOIN dbo.cccust as cust
	on af.ACCTID = cust.acctnum  
	where af.ACCTID in (select cust.OLDCCNUM from cccust)

/*  - UPDATE CUST CREDIT TABLE with NEW TIPNUMBER from MAXTIP       */

	update cccust	
	set
	    cccust.TIPNUMBER = (@MAXTIP + 1)
	from dbo.AFFILIAT as af
	inner JOIN dbo.cccust as cust
	on af.ACCTID = cust.acctnum  
	where af.ACCTID not in (select cust.ACCTNUM from cccust)


	update cccust	
	set
	    cccust.MULTIPLIER = '1'
	where cccust.MULTIPLIER  is null
 

END /*while */
PRINT 'total Liability is '
PRINT  @LIABILITYTOTAL

EndPROC:
GO
