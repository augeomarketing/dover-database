/****** Object:  StoredProcedure [dbo].[spCOMPASSLASTACTIVITY]    Script Date: 02/20/2009 15:52:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 10/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spCOMPASSLASTACTIVITY]  AS  





DELETE FROM COMPASSLASTACTIVITY

		
INSERT INTO COMPASSLASTACTIVITY
(
 ACCTID
,LASTACTIVITY
)
SELECT
 ACCTID
,MAX(HISTDATE)
 from history
GROUP BY ACCTID
GO
