/****** Object:  StoredProcedure [dbo].[spGenerateEmailBonuses]    Script Date: 02/20/2009 15:52:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGenerateEmailBonuses] AS 

Declare @DateIn datetime 
set @DateIn = getdate() 
declare @Tipnumber char(15), @Acctid varchar(25), @Trandate datetime, @Points numeric(9)
/*set @Trandate=@Datein	*/
set @Trandate=@Datein
set @Points='750'
	
/*                                                                            */
/* Setup Cursor for processing                                                */

/*declare Customer_crsr cursor for 
select 	CUSTOMER.TIPNUMBER, CUSTOMER.AcctID
from      EMLSTMT  INNER JOIN
             CUSTOMER ON CUSTOMER.TIPNUMBER = EMLSTMT.TIPNUMBER 
where   CUSTOMER.TIPNUMBER  not in (select TIPNUMBER from HISTORY where TRANCODE = 'BE' )
*/

--declare Customer_crsr cursor for 
declare EMLSTMT_csr cursor for 
select 	TIPNUMBER
from      EMLSTMT  
where   TIPNUMBER  not in (select TIPNUMBER from HISTORY where TRANCODE = 'BE' )

/*                                                                            */
--open Customer_crsr
open EMLSTMT_csr 

/*                                                                            */
--fetch Customer_crsr into @Tipnumber, @Acctid 
fetch EMLSTMT_csr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */

	update customer	
		set RunAvailable = RunAvailable + @Points, RunBalance=RunBalance + @Points  
		where tipnumber = @Tipnumber

--	Insert Into History(TipNumber,AcctId,HistDate,TranCode,TranCount,Points,Ratio,Description)
--        	Values(@Tipnumber, @Acctid, @Trandate, 'BE', '1', @Points, '1', 'Bonus E Statements')

	Insert Into History(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description)
        	Values(@Tipnumber, @Trandate, 'BE', '1', @Points, '1', 'Bonus E Statements')
 
Next_Record:
--	fetch Customer_crsr into @Tipnumber
	fetch EMLSTMT_csr into @Tipnumber
end

Fetch_Error:
--close Customer_crsr
close EMLSTMT_csr 

--deallocate Customer_crsr
deallocate EMLSTMT_csr
GO
