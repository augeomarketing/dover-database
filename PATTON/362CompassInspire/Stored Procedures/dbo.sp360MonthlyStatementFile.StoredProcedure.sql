/****** Object:  StoredProcedure [dbo].[sp360MonthlyStatementFile]    Script Date: 02/20/2009 15:52:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*******************************************************************************/
/*                                */
/*  7/26/06 added View to improve speed                               */
/* RDT 01/04/07 Added Time to date for better datetime accuracy */
/*******************************************************************************/

-- RDT 01/04/07 CREATE PROCEDURE sp360MonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10)
CREATE PROCEDURE [dbo].[sp360MonthlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)

AS 

Declare @StartDate DateTime 						--RDT 01/04/07
Declare @EndDate DateTime						--RDT 01/04/07
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 01/04/07 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 01/04/07


Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)
Declare @BEPOINTS int
Declare @BIPOINTS int
--RDT 01/04/07 set @MonthBegin = month(Convert(datetime, @StartDate) )
set @MonthBegin = month( @StartDate) 		 --RDT 01/04/07


/* Load the statement file from the customer table  */
delete from Monthly_Statement_File


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, N'IsView') = 1) 
Begin 
	drop view [dbo].[view_history_TranCode]
End


/* Create View */
set @SQLDynamic = 'create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between '''+convert( char(23), @StartDate,21 ) +''' and '''+ convert(char(23),@EndDate,21) +''' group by tipnumber, trancode'
print @SQLDynamic
exec sp_executesql @SQLDynamic


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, N'IsView') = 1) 
Begin 
	print 'View created'
End



insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer

-- TESTING TESTING TESTING
-- where tipnumber in ('360000000000093')
-- TESTING TESTING TESTING
update Monthly_Statement_File
set
PointsBegin = '0',Pointsend = '0',PointsPurchasedCR = '0',PointsPurchasedDB = '0',PointsBonus = '0',PointsAdded = '0',
PointsIncreased = '0',PointsRedeemed = '0',PointsReturnedCR = '0',PointsReturnedDB = '0',PointsSubtracted = '0',
PointsDecreased = '0',PointsBonus0A = '0',PointsBonus0B = '0',PointsBonus0C = '0',PointsBonus0D = '0',PointsBonus0E = '0',
PointsBonus0F = '0',PointsBonus0G = '0',PointsBonus0H = '0',PointsBonus0I = '0',PointsBonus0J = '0',PointsBonus0K = '0',
PointsBonus0L = '0',PointsBonus0M = '0',PointsBonus0N = '0',PointFloor = '0',PointsToExpire = '0',DateOfExpiration = ' ',
pointsexpired = '0'

/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File 
set pointspurchasedCR 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '63' 

/* Load the statmement file CREDIT with returns            */
update Monthly_Statement_File 
set pointsreturnedCR
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode ='33'

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File 
set pointspurchasedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '67'

/* Load the statmement file DEBIT with returns            */
update Monthly_Statement_File 
set pointsreturnedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '37'

/* Load the statmement file with bonuses            */
 

update Monthly_Statement_File 
set pointsbonus
=   view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BE' 


update Monthly_Statement_File 
set pointsbonus 
=   pointsbonus + view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'BI'  

/* Load the statmement file with 0A bonuses            */
update Monthly_Statement_File 
set pointsbonus0A
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode  = '0A'

/* Load the statmement file with 0B bonuses            */
update Monthly_Statement_File 
set pointsbonus0B
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0B'


/* Load the statmement file with 0C bonuses            */
update Monthly_Statement_File 
set pointsbonus0C
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0C'


/* Load the statmement file with 0D bonuses            */
update Monthly_Statement_File 
set pointsbonus0D
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0D'


/* Load the statmement file with 0E bonuses            */
update Monthly_Statement_File 
set pointsbonus0E =
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0E'


/* Load the statmement file with 0F bonuses            */
update Monthly_Statement_File 
set pointsbonus0F
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0F'


/* Load the statmement file with 0G bonuses            */
update Monthly_Statement_File 
set pointsbonus0G
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0G'


/* Load the statmement file with 0H bonuses            */
update Monthly_Statement_File 
set pointsbonus0H
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0H'


/* Load the statmement file with 0I bonuses            */
update Monthly_Statement_File 
set pointsbonus0I
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0I'

/* Load the statmement file with 0J bonuses            */
update Monthly_Statement_File 
set pointsbonus0J
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0J'

/* Load the statmement file with 0K bonuses            */
update Monthly_Statement_File 
set pointsbonus0K
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0K'

/* Load the statmement file with 0L bonuses            */
update Monthly_Statement_File 
set pointsbonus0L
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0L'

/* Load the statmement file with 0M bonuses            */
update Monthly_Statement_File 
set pointsbonus0M
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0M'

/* Load the statmement file with 0N bonuses            */
update Monthly_Statement_File 
set pointsbonus0N
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0N'

/* Load the statmement file with plus adjustments */
update Monthly_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='IE'  )


/* Add  DECREASED REDEEMED to PointsAdded     
update Monthly_Statement_File 
set  pointsadded=pointsadded + 
 view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'
*/

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsbonus0A + pointsbonus0B + pointsbonus0C + pointsbonus0D
+ pointsbonus0E + pointsbonus0F + pointsbonus0G + pointsbonus0H + pointsbonus0I + pointsbonus0J + pointsbonus0K + pointsbonus0L
+ pointsbonus0M + pointsbonus0N


/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RP' 
   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RV'
   
/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RU' 
  

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RT'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RS'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RQ'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RM'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RI'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RC'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RB'   

/* Load the statmement file with redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=   pointsredeemed + view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'RD'   


/* Load the statmement file with Increases to redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'IR' 


/* subtract DECREASED REDEEMED to from Redeemed*/
update Monthly_Statement_File 
set pointsredeemed
= pointsredeemed - view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DE'

/* Load the statmement file with Expired Points          */
update Monthly_Statement_File 
set pointsexpired
=   view_history_TranCode.TranCodePoints   from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
 view_history_TranCode.trancode = 'XP' 

/* Add EP to  minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted 
= pointssubtracted +  view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'EP'

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted +  pointsexpired


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin = (select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased


/* Drop the view */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, N'IsView') = 1) 
Begin 
	drop view [dbo].[view_history_TranCode]
End
GO
