/****** Object:  StoredProcedure [dbo].[spCalculateDEBITTotals]    Script Date: 02/20/2009 15:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the DCTRANTOT File for 360ComPassPoints                    */

/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spCalculateDEBITTotals]  AS  




/* Output */

Declare @TOTRECS float
Declare @TOTCREDITAMT float
Declare @TOTCREDITCNT float
Declare @TOTDEBITAMT float
Declare @TOTDEBITCNT float
Declare @MULTIPLIER float
BEGIN 
	

	set @TOTRECS = '0' 
	set @TOTCREDITAMT = '0'
	set @TOTCREDITCNT  = '0'
	set @TOTDEBITAMT =  '0'
	set @TOTDEBITCNT =  '0' 
	set @MULTIPLIER = .5

	
	select 	    
             @TOTRECS = (@TOTRECS + 1)            
	    ,@TOTCREDITAMT = (@TOTCREDITAMT + CREDITAMT)
	    ,@TOTCREDITCNT = (@TOTCREDITCNT + CREDITCNT)
	    ,@TOTDEBITAMT = (@TOTDEBITAMT + DEBITAMT)
	    ,@TOTDEBITCNT = (@TOTDEBITCNT + DEBITCNT) 
	from DCTRAN 
	
	   set @TOTDEBITAMT = (@TOTDEBITAMT * @MULTIPLIER)
	    set @TOTCREDITAMT = (@TOTCREDITAMT * @MULTIPLIER)   


	 INSERT into DCTRANTOT	
	    (	     
                  TOTRECS 
                 ,CREDITAMT
                 ,CREDITCNT
                 ,DEBITAMT  
                 ,DEBITCNT           
	    )
	values
           (             
             @TOTRECS
            ,@TOTCREDITAMT
            ,@TOTCREDITCNT
            ,@TOTDEBITAMT 
            ,@TOTDEBITCNT            
	    ) 
 

END /*while */


EndPROC:
GO
