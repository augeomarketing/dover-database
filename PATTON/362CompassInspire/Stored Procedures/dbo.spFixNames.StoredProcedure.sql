/****** Object:  StoredProcedure [dbo].[spFixNames]    Script Date: 02/20/2009 15:52:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spFixNames]
AS 

/******************************************************************************/
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
declare @TIPNUMBER nchar(8), @NAME1 nvarchar(40), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)

delete from MERGENAME

declare cccust_crsr cursor
for select TIPNUMBER, NAME1
from cccust

/*                                                                            */
open cccust_crsr
/*                                                                            */
fetch cccust_crsr into @TIPNUMBER, @NAME1
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from MERGENAME where TIPNUMBER=@TIPNUMBER)
	begin
		insert MERGENAME(TIPNUMBER, na1) values(@TIPNUMBER, @NAME1)
		goto Next_Record
	end
	
	else
		if exists(select * from MERGENAME where TIPNUMBER=@TIPNUMBER and (substring(na2,1,1) like ' ' or na2 is null))
		begin
			update MERGENAME
			set na2=@NAME1
			where TIPNUMBER=@TIPNUMBER
			goto Next_Record
		end
		else
			if exists(select * from MERGENAME where TIPNUMBER=@TIPNUMBER and (substring(na3,1,1) like ' ' or na3 is null))
			begin
				update MERGENAME
				set na3=@NAME1
				where TIPNUMBER=@TIPNUMBER
				goto Next_Record
			end
			else
				if exists(select * from MERGENAME where TIPNUMBER=@TIPNUMBER and (substring(na4,1,1) like ' ' or na4 is null))
				begin
					update MERGENAME
					set na4=@NAME1
					where TIPNUMBER=@TIPNUMBER
					goto Next_Record
				end
				else
					if exists(select * from MERGENAME where TIPNUMBER=@TIPNUMBER and (substring(na5,1,1) like ' ' or na5 is null))
					begin
						update MERGENAME
						set na5=@NAME1
						where TIPNUMBER=@TIPNUMBER
						goto Next_Record
					end
					else
						if exists(select * from MERGENAME where TIPNUMBER=@TIPNUMBER and (substring(na6,1,1) like ' ' or na6 is null))
						begin
							update MERGENAME
							set na6=@NAME1
							where TIPNUMBER=@TIPNUMBER
							goto Next_Record
						end
		
	goto Next_Record	
			
Next_Record:
	fetch cccust_crsr into @TIPNUMBER, @NAME1
end

Fetch_Error:
close cccust_crsr
deallocate cccust_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update MERGENAME
set na2=null
where na2=na1

update MERGENAME
set na3=null
where na3=na1 or na3=na2

update MERGENAME
set na4=null
where na4=na1 or na4=na2 or na4=na3

update MERGENAME
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update MERGENAME
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update MERGENAME
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or substring(na1,1,1) like ' '

	update MERGENAME
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or substring(na2,1,1) like ' '

	update MERGENAME
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or substring(na3,1,1) like ' '

	update MERGENAME
	set na4=na5, na5=na6, na6=null
	where na4 is null or substring(na4,1,1) like ' '

	update MERGENAME
	set na5=na6, na6=null
	where na5 is null or substring(na5,1,1) like ' '

	set @count= @count + 1
end

/******************************************************************************/
/* Section to populate names in CUSTOMER table                                */
/******************************************************************************/

update CUSTOMER
set ACCTNAME1=(select na1 from MERGENAME where TIPNUMBER=MERGENAME.TIPNUMBER),
    ACCTNAME2=(select na2 from MERGENAME where TIPNUMBER=MERGENAME.TIPNUMBER),
    ACCTNAME3=(select na3 from MERGENAME where TIPNUMBER=MERGENAME.TIPNUMBER),
    ACCTNAME4=(select na4 from MERGENAME where TIPNUMBER=MERGENAME.TIPNUMBER),
    ACCTNAME6=(select na5 from MERGENAME where TIPNUMBER=MERGENAME.TIPNUMBER),
    ACCTNAME6=(select na6 from MERGENAME where TIPNUMBER=MERGENAME.TIPNUMBER)
GO
