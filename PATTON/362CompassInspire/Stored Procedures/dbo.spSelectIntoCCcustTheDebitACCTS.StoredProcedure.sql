/****** Object:  StoredProcedure [dbo].[spSelectIntoCCcustTheDebitACCTS]    Script Date: 02/20/2009 15:52:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 10/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spSelectIntoCCcustTheDebitACCTS]  AS



		

             
	 insert into cccust	
(		
          ACCTNUM  
	, DDA 
	, NAME1 
	, NAME2 
	, ADDRESS1 
	, ADDRESS2 
 	,CITYSTATE 
 	,ZIP 
 	,HOTFLAG 
	,TIPNUMBER
	,ACCTTYPE
	)
	select
          ACCTNUM  
	, DDA 
	, NAME1 
	, NAME2 
	, ADDRESS1 
	, ADDRESS2 
 	,CITYSTATE 
 	,ZIP 
 	,HOTFLAG
	,'0'    
	,'D'   	      	
	from DCCUST
GO
