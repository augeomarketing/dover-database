/****** Object:  StoredProcedure [dbo].[spBuildAcctTipTemp]    Script Date: 02/20/2009 15:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  B.QUINN  */
/* DATE: 9/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBuildAcctTipTemp] AS

/* input */
Declare @TipFound nvarchar(1)
Declare @TRANDESC char(40)
Declare @RunDate datetime
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchCnt int
Declare @ReturnAmt float
Declare @RetunCnt int
Declare @NetAmt float
Declare @NetCnt int
Declare @Multiplier real
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt char(10)
Declare @CreditCnt char(3)
Declare @DebitAmt char(12)
Declare @DebitCnt char(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @RunAvailable char(10)
Declare @RunBalance   char(10)
Declare @RunRedeemed   char(10)
Declare @POINTS char(5)
Declare @OVERAGE char(5)
Declare @afTranAmt int
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned float(8)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR char (8)
Declare @RunBalanceNew char(8)

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 
	set @TipFound = 'N'

	/*  - Check For Affiliat Record       */
	select
	   @TipNumber = TIPNUMBER
	   ,@TipFound = 'y'
	from
	  AFFILIAT
	where
	   ACCTID = @AcctNum 
	

	if @TipFound = 'N'	
	   insert into AcctTipTemp
	          ( ACCTNUM )
	   values
		  ( @AcctNum )
	 
           
	

	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
close  AFFILIAT_crsr
deallocate  AFFILIAT_crsr
GO
