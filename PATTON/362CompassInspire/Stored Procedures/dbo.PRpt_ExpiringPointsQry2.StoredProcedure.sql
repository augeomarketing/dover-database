/****** Object:  StoredProcedure [dbo].[PRpt_ExpiringPointsQry2]    Script Date: 02/20/2009 15:52:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[PRpt_ExpiringPointsQry2] @dtReportDate   DATETIME, @ClientID CHAR(3) AS    
 
--Stored procedure to build EXPIRING Points and place in ExpiringPointsTable for a specified client and month/year

/* declare @dtReportDate   DATETIME
Declare @ClientID CHAR(3)
set @dtReportDate = '03/31/2007'
set @ClientID = '360'   */


-- Use this to create Expiring Points Table --
DECLARE @dtMonthStart as datetime
DECLARE @dtMonthEnd as datetime
DECLARE @dtMonthStartXP as datetime
DECLARE @dtMonthEndXP as datetime
-- DECLARE @dtLastMoStart as datetime
-- DECLARE @dtLastMoEnd as datetime
DECLARE @dtRunDate as datetime
DECLARE @RunDate varchar(25)
Declare @Viewname varchar(25)
DECLARE @strMonth CHAR(5)                       -- Temp for constructing dates
DECLARE @strMonthAsStr VARCHAR(10)              -- Temp for constructing dates
DECLARE @strYear CHAR(4)                        -- Temp for constructing dates
DECLARE @strLMonth CHAR(5)                      -- Temp for constructing dates; last month
DECLARE @strLYear CHAR(4)                       -- Temp for constructing dates; last year
DECLARE @intMonth INT
DECLARE @intMonthXP INT
DECLARE @intYearXP INT                         -- Temp for constructing dates for expiration
DECLARE @intDayXP INT
DECLARE @intYear INT                            -- Temp for constructing dates
DECLARE @intLastMonth INT                       -- Temp for constructing dates
DECLARE @intLastYear INT                        -- Temp for constructing dates
DECLARE @strParamDef NVARCHAR(2500)             -- Parameter string for dynamic SQL
DECLARE @strStmt NVARCHAR(2500)                 -- Statement string for dynamic SQL
DECLARE @strDBLoc VARCHAR(100)                  -- DB location  
DECLARE @strDBName VARCHAR(100)                 -- DB name 
DECLARE @strDBLocName VARCHAR(100)              -- DB location and name 
DECLARE @strHistoryRef VARCHAR(100)             -- Reference to the DB/History table
DECLARE @strCustomerRef VARCHAR(100)            -- Reference to the DB/Customer table
DECLARE @strExpiringPointsRef VARCHAR(100)      -- Reference to the DB/ExpiringPoints table
DECLARE @strCustDelRef VARCHAR(100)		-- Reference to the DB/CustomerDeleted table
DECLARE @strAcctTypeColNm VARCHAR(100)		-- Column name is the Affiliat table for the column that denotes credit vs. debit cards
						--	'AcctType' is the Std value, 'CardType' is the Compass value
DECLARE @strXsqlRV VARCHAR(100)			-- For the returned value when using sp_executesql


declare @total_add as numeric 
declare @total_Subtracts as numeric 
DECLARE @PointsToExpire nvarchar(9) 
DECLARE @ExpireDate datetime
DECLARE @DateRun datetime
DECLARE @Debit_Overage NUMERIC(18,0) 
Declare @TipNumber nvarchar(15)
Declare @DateOfExpire char(15) 
Declare @DateExpire datetime             

/* Figure out the month/year, and the previous month/year, as ints */
SET @intMonth = DATEPART(month, @dtReportDate)
SET @intMonthXP = DATEPART(month, @dtReportDate)
SET @intDayXP = DATEPART(day, @dtReportDate)
SET @strMonth = dbo.fnRptGetMoKey(@intMonth)
SET @strMonthAsStr = dbo.fnRptMoAsStr(@intMonth)
SET @intYear = DATEPART(year, @dtReportDate)
SET @strYear = CAST(@intYear AS CHAR(4))

SET @intYearXP = (@strYear - 3)


Set @DateOfExpire = (ltrim(@intMonth) + '/' + ltrim(@intDayXP)+ '/' + ltrim(@intYearXP))
set @DateExpire = cast(@DateOfExpire as datetime)

                -- Set the year string for the Liablility record
If @intMonth = 1
        Begin
          SET @intLastMonth = 12
          SET @intLastYear = @intYear - 1
        End
Else
        Begin
          SET @intLastMonth = @intMonth - 1
          SET @intLastYear = @intYear
        End
SET @strLMonth = dbo.fnRptGetMoKey(@intLastMonth)
SET @strLYear = CAST(@intLastYear AS CHAR(4))           -- Set the year string for the Liablility record



set @dtMonthEnd = dbo.fnRptMoAsStr(@intMonth) + dbo.fnRptMoLast(@intMonth, @intYear) + ', ' + 
        CAST(@intYear AS CHAR) + ' 23:59:59.997'
set @dtMonthStart = dbo.fnRptMoAsStr(@intMonth) + '1, ' + 
        CAST(@intYear AS CHAR) + ' 00:00:00'

SET @dtRunDate = GETDATE()
set @RunDate = @dtRunDate

-- Now set up the variables we need for dynamic sql statements


-- DB location; the machine name in form [MachineName]
SET @strDBLoc = (SELECT ClientDBLocation FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- DB name; the database name in form [DBName]
SET @strDBName = (SELECT ClientDBName FROM [PATTON\RN].[RewardsNOW].[dbo].[RptCtlClients] 
                WHERE (ClientNum = @ClientID)) 


-- Now build the fully qualied names for the client tables we will reference
SET @strDBLocName = @strDBLoc + '.' + @strDBName 
SET @strHistoryRef = @strDBLocName + '.[dbo].[History]' 
SET @strCustomerRef = @strDBLocName + '.[dbo].[Customer]'
SET @strExpiringPointsRef = @strDBLocName + '.[dbo].[ExpiringPoints]' 
 


SET @strParamDef = N'@dtMoStrt DATETIME, @dtMoEnd DATETIME, @strReturnedVal VARCHAR(100) OUTPUT '    -- The parameter definitions for most queries


/* clear nulls from temp table    */
set @Viewname = ('view_Expiring_Points'  +  rtrim(@ClientID))

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[@Viewname]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[@Viewname] 




/* Create View */


SET @strStmt = N'create view ' + @Viewname 
SET @strStmt = @strStmt + N' as Select tipnumber, PointsToExpire,DateOfExpire,DateRun from   ' + @strExpiringPointsRef 


EXECUTE sp_executesql @stmt = @strStmt
        
SET @strStmt = N'Declare VXP_crsr cursor for Select * From ' + @Viewname 
EXECUTE sp_executesql @stmt = @strStmt

/* Declare VXP_crsr cursor */


Open VXP_crsr

Fetch VXP_crsr  
	into  @TipNumber ,@PointsToExpire, @ExpireDate, @DateRun
 	


IF @@FETCH_STATUS = 1
	goto Fetch_Error



	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	   


-- Credit card purchases
SET @strStmt = N'SELECT @strReturnedVal = SUM(POINTS) FROM ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode like (''R%'') or trancode not = (''IR'')) AND '
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  
  
print @strStmt
EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
set @total_add = CAST(@strXsqlRV AS NUMERIC) 
IF @total_add IS NULL SET @total_add = 0.0



SET @strStmt = N'SELECT @strReturnedVal = SUM(Points) FROM  ' + @strHistoryRef 
SET @strStmt = @strStmt + N' WHERE (trancode like((''R%'')trancode = ''IR'') AND ' 
SET @strStmt = @strStmt + N'histdate < '''
SET @strStmt = @strStmt +   rtrim(@DateExpire)
SET @strStmt = @strStmt + N''''  
SET @strStmt = @strStmt + N' and tipnumber = ''' + @TipNumber
SET @strStmt = @strStmt + N''''  

EXECUTE sp_executesql @stmt = @strStmt, @params = @strParamDef, 
        @dtMoStrt = @dtMonthStart, 
        @dtMoEnd = @dtMonthEnd, 
	@strReturnedVal = @strXsqlRV OUTPUT 		-- Results
SET @total_Subtracts = CAST(@strXsqlRV AS NUMERIC) 
IF @total_Subtracts IS NULL SET @total_Subtracts = 0.0



set @PointsToExpire = (@total_add - @total_Subtracts)

SET @strStmt = N'update  ' + @strExpiringPointsRef

SET @strStmt = @strStmt + N' set PointsToExpire = ' + @PointsToExpire 
set @strStmt = @strStmt + N', DateOfExpire = ''' + rtrim(@DateofExpire)
set @strStmt = @strStmt + N''', DateRun = ''' + @RunDate 
set @strStmt = @strStmt + N''' where tipnumber = ''' + @TipNumber
set @strStmt = @strStmt + N''''
print @strStmt
EXECUTE sp_executesql @stmt = @strStmt

 


-- SELECT * FROM [PATTON\RN].[RewardsNOW].[dbo].RptLiability
 
Fetch VXP_crsr  
	into  @TipNumber ,@PointsToExpire, @ExpireDate, @DateRun
 	  	

END /*while */

	


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  VXP_crsr
deallocate  VXP_crsr
GO
