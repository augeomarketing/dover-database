/****** Object:  StoredProcedure [dbo].[spBACreateExpireTable]    Script Date: 02/20/2009 15:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Create the Expiring Points Extract and update                                 */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBACreateExpireTable] @DateOfExpire NVARCHAR(10)  AS   

--declare @DateOfExpire NVARCHAR(10) 
--SET @DateOfExpire = '02/29/2008'
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME


set @ExpireDate = cast(@DateOfExpire as datetime)

SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 3
set @expirationdate = (rtrim(@intYear) + '-' + rtrim(@intmonth) + '-' + rtrim(@intday) +   ' 23:59:59.997')

/*print 'year'
print @intYear
print 'month'
print @intmonth
print 'day'
print @intday
print '@expirationdate'
print @expirationdate */

	TRUNCATE TABLE expiringpoints

	INSERT   into expiringpoints
	 SELECT tipnumber, sum(points * ratio) as addpoints,
	'0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as PREVEXPIRED, @ExpireDate as dateofexpire
	from history
	where histdate < @expirationdate and (trancode not like('R%') and
	     trancode <> 'IR'and trancode <> 'XP')
	group by tipnumber


	UPDATE expiringpoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 



	UPDATE expiringpoints  
	SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 trancode  = 'XP' 
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 trancode  = 'XP' 
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 


	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = (ADDPOINTS - PREVEXPIRED + REDPOINTS)



	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE IS NULL 

	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE < '0' 






	update BIAnnual_Statement_File		      
	set
	    POINTSTOEXPIRE = XP.POINTSTOEXPIRE,
	    DATEOFEXPIRATION = @expirationdate
	from dbo.expiringpoints as XP
	inner JOIN dbo.BIAnnual_Statement_File as BAS
	on XP.TIPNUMBER = BAS.TIPNUMBER  
	where XP.TIPNUMBER in (select BAS.TIPNUMBER from BIAnnual_Statement_File)
GO
