/****** Object:  StoredProcedure [dbo].[sp360MonthlyAuditValidationNEW]    Script Date: 02/20/2009 15:52:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp360MonthlyAuditValidationNEW] AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

delete from Monthly_Audit_ErrorFile 
                                                                          


	insert into Monthly_Audit_ErrorFile 
	(
 	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, errormsg
	, currentend
	, pointsbonus0A
	, pointsbonus0B
	, pointsbonus0C
	, pointsbonus0D
	, pointsbonus0E
	, pointsbonus0F
	, pointsbonus0G
	, pointsbonus0H
	, pointsbonus0I
	, pointsbonus0J
	, pointsbonus0K 
	, pointsbonus0L
	 ,pointsbonus0M
 	, pointsbonus0N
 ) 
	select
	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, 'Beginning and END DO NOT MATCH'
	, pointsend
	, pointsbonus0A
	, pointsbonus0B
	, pointsbonus0C
	, pointsbonus0D
	, pointsbonus0E
	, pointsbonus0F
	, pointsbonus0G
	, pointsbonus0H
	, pointsbonus0I
	, pointsbonus0J
	, pointsbonus0K 
	, pointsbonus0L
	 ,pointsbonus0M
 	, pointsbonus0N
	from
	 Monthly_Statement_File
	 where pointsend <>
	 (select AdjustedEndingPoints from CURRENT_MONTH_ACTIVITY 	  
	  where Monthly_Statement_File.Tipnumber = CURRENT_MONTH_ACTIVITY.TIPNUMBER)
GO
