/****** Object:  StoredProcedure [dbo].[spProcessCompassData]    Script Date: 02/20/2009 15:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the  Files for 360ComPassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMERStage        */
/*  -  Update AFFILIATStage       */ 
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessCompassData] AS

/* input */
Declare @TRANDESC char(40)
Declare @RunDate datetime
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt float
Declare @PurchCnt int
Declare @ReturnAmt float
Declare @RetunCnt int
Declare @NetAmt float
Declare @NetCnt int
Declare @Multiplier real
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt char(10)
Declare @CreditCnt char(3)
Declare @DebitAmt char(12)
Declare @DebitCnt char(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @RunAvailable char(10)
Declare @RunBalance   char(10)
Declare @RunRedeemed   char(10)
Declare @POINTS char(5)
Declare @OVERAGE char(5)
Declare @afTranAmt int
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned float(8)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR char (8)
Declare @RunBalanceNew char(8)

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 

	/*  - Check For Affiliat Record       */
	select
	   @TipNumber = TIPNUMBER
	   ,@YTDEarned = YTDEarned
	from
	  AFFILIAT
	where
	   ACCTID = @AcctNum 
	and @AcctType = 'c'	

	if exists (select dbo.Customer.TIPNUMBER from dbo.Customer 
                    where dbo.Customer.TIPNUMBER = @TipNumber)	
            Begin

/*   APPLY THE COMPASS BUSINESS LOGIC                                          */

		Update Customer
		Set STATUS = 'A' 
		From cccust_crsr
		Where AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and @OverLimit = 'Y'		
		
		Update Customer
		Set STATUS = 'O' 
		From cccust_crsr
		Where AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and @OverLimit = 'Y'

		Update Customer
		Set STATUS = 'D' 
		From cccust_crsr 
		Where AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and  @DelFlag = 'Y'    
 

		Update Customer
		Set STATUS = 'L' 
		From cccust_crsr 
		Where  AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and @LostStolen= 'Y' 

		Update Customer
		Set STATUS = 'F' 
		From cccust_crsr 
		Where  AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and @Fraud = 'Y' 

		Update Customer
		Set STATUS = 'C' 
		From cccust_crsr 
		Where AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and @Closed = 'Y' 

		Update Customer
		Set STATUS = 'B' 
		From cccust_crsr 
		Where AFFILIAT.TIPNUMBER = Customer.TIPNUMBER and @Bankrupt = 'Y' 

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT       */

		if (@NETAMT + @YTDEarned) > @MAXPOINTSPERYEAR
		   Begin
		      set @OVERAGE = (@NETAMT + @YTDEarned - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + @NETAMT)
	           End
		else
	           Begin
		      set @RunAvailable = @RunAvailable + (@NETAMT * @Multiplier)
		      set @RUNBALANCE = @RUNBALANCE + (@NETAMT * @Multiplier)
		      set @RunBalanceNew = @RunBalanceNew + (@NETAMT * @Multiplier)		      
	           end  

/*  UPDATE THE CUSTOMER RECORD WITH THE CREDIT TRANSACTION DATA          */

		Update Customer
		Set 
		   RunAvailable = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE
		   ,RunBalanceNew = @RunBalanceNew
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @NAME1  
	           ,ACCTNAME2 = @NAME2  
	           ,ADDRESS1 = @ADDRESS1  
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIP = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE	           
		From cccust_crsr 
		Where AFFILIAT.TIPNUMBER = Customer.TIPNUMBER

/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '63'

/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */

		IF @NetAmt > '0'
		Begin
	           set @POINTS = (@NETAMT * @MULTIPLIER) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@NetCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,'COMPASS'
		    ,@MULTIPLIER
	            ,@OVERAGE
	            )
		End
  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '33'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

		IF @ReturnAmt > '0'
		Begin
		   set @POINTS = (@RETURNAMT * @MULTIPLIER) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,'COMPASS'
		    ,@MULTIPLIER
	            ,@OVERAGE
	            )
		End
	
            end 	
else
 Begin 

/* IF THERE IS NO AFFILIAT RECORD THE COUSTOMER DID NOT EXIST AND IS ADDED TO THE PROPER FILES */

           IF @AcctType = 'c'
           BEGIN      
	      Insert into AFFILIAT
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         SSNum
                )
	      values
 		(
		@AcctNum,
 		@tipNumber,
		@LastName,
 		@DateAdded,
 		@AcctStatus,
		@YTDEarned,
 		@SSNum
		)
/* */
/*  				- Create CUSTOMER     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER OVER LIMIT      */
/* This is an add to the customer file and an overage should not occur at this time  */
/* But I am Checking in the off chance it does                                       */

		if (@NETAMT + @YTDEarned) > @MAXPOINTSPERYEAR
		   Begin
		      set @OVERAGE = (@NETAMT + @YTDEarned - @MAXPOINTSPERYEAR) 
		      set @YTDEarned = (@YTDEarned + @NETAMT)
	           End
		else
	           Begin
		      set @RunAvailable = @RunAvailable + (@NETAMT * @Multiplier)
		      set @RUNBALANCE = @RUNBALANCE + (@NETAMT * @Multiplier)
		      set @RunBalanceNew = @RunBalanceNew + (@NETAMT * @Multiplier)		      
	           end  
	
	                /*Add New Customers 
/* */                                                     */
		Insert into CUSTOMER  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2  	
	/*	,ADDRESS4   */
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	/*	,MISC1 		
		,MISC2		
		,MISC3    */		
		,DATEADDED
		,RUNAVAILABLE
		,RUNBALANCE
		,RUNREDEEMED
	      )		
		values
		 (
		@Lastname, @Name1, @Name2,
 		@Address1, @Address2,@City,
 		@State, @Zip, @HomePhone,
 		@DateAdded, @RunAvailable,
 		@RunBalance, @RunRedeemed
	         )
           END 
	     	
	         
/*  OBTAIN THE TRANSACTION DISCRIPTION USING THE TRANCODE                */

  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '63'

/*  				- Create HISTORY     						   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */
		IF @NetAmt > '0'
		Begin
	           set @POINTS = (@NETAMT * @MULTIPLIER) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@NetCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,'COMPASS'
		    ,@MULTIPLIER
	            ,@OVERAGE
	            )
		End
  		Select 
		   @TRANDESC = Description
	        From TranType
		where
		   TranCode = '33'

/*  		ADD HISTORY RECORD FOR RETURNS     			   */

		IF @ReturnAmt > '0'
		Begin
		   set @POINTS = (@RETURNAMT * @MULTIPLIER) 
		   Insert into HISTORY
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNumber
	            ,@AcctNum
	            ,@RunDate
	            ,'63'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,'COMPASS'
		    ,@MULTIPLIER
	            ,@OVERAGE
	            )
		End
	
 END
	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
close  AFFILIAT_crsr
deallocate  AFFILIAT_crsr
GO
