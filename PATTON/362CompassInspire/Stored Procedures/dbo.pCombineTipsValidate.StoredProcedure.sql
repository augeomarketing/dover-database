/****** Object:  StoredProcedure [dbo].[pCombineTipsValidate]    Script Date: 02/20/2009 15:52:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                                                                            */
/*                   SQL TO validate COMPASS COMBINES                          */
/*                                                                            */
/* BY:  R.Tremblay*/
/* DATE: 8/2006                                                               */
/* REVISION: 0                                                                */
/* Processes comb_in table -RDT                                                                   */
/* Check for errors only */
/******************************************************************************/	
/*                                                                            */
 CREATE PROCEDURE [dbo].[pCombineTipsValidate] AS
/******************************************************************************/	
/*                                                                            */
declare @a_TIPPRI varchar(15), @a_TIPSEC varchar(15), @a_TIPSWITCH varchar(15), @a_errmsg varchar(80)
declare @a_namep varchar(40), @a_namep1 varchar(40), @a_names varchar(40), @a_names1 varchar(40), @a_addrp varchar(40), @a_addrs varchar(40)     
declare @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int
declare @DateToday datetime
SET @DateToday = GetDate()
/******************************************************************************/	
/* set  errmsg to space because you can't add text to a null field. You can replace, but not add */
update comb_in set errmsg = ''

update comb_in		
set errmsg ='Pri Tip blank.' 
where (len(rtrim(TIP_PRI)) = 0 or TIP_PRI = NULL) and (len(rtrim(errmsg)) = 0 or errmsg = '' )

update comb_in		
set errmsg ='Sec Tip blank.' 
where (len(rtrim(TIP_SEC)) = 0 or TIP_SEC = NULL) and (len(rtrim(errmsg)) = 0 or errmsg = '')

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING COMB_IN TABLE                               */
/*                                                                            */
declare combine_crsr cursor
for select TIP_PRI, TIP_SEC, ERRMSG
from comb_in
--WHERE (len(rtrim(errmsg)) = 0 or errmsg = '' )
for update

/*                                                                            */
open combine_crsr
/*                                                                            */
fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg 
/******************************************************************************/	
/* MAIN PROCESSING  VERIFICATION                                              */
if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	
	IF  not EXISTS(SELECT * FROM Customer WHERE TIPnumber = @a_TIPPRI)
		begin
		update comb_in	
		set errmsg = errmsg + 'No Pri Tip in Cust.' 
		where current of combine_crsr
		goto Next_Record
		end

	IF  NOT EXISTS(SELECT * FROM Customer WHERE TIPnumber = @a_TIPSEC)
		begin
		update comb_in	
		set errmsg= errmsg + ' No SEC Tip in Cust.' 
		where current of combine_crsr
		goto Next_Record
		end

	IF  not EXISTS(SELECT * FROM Affiliat WHERE TIPnumber = @a_TIPPRI)
		begin
		update comb_in	
		set errmsg=errmsg + ' No PRI Tip in Affil.' 
		where current of combine_crsr
		goto Next_Record
		end

	IF  NOT EXISTS(SELECT * FROM Affiliat WHERE TIPnumber = @a_TIPSEC)
		begin
		update comb_in	
		set errmsg=errmsg + ' No SEC Tip in Affil.' 
		where current of combine_crsr
		goto Next_Record
		end

/* GET NAMES AND ADDRESSES FOR COMPARISON  */

	select @a_namep = ACCTNAME1, @a_namep1 = ACCTNAME2, @a_addrp = ADDRESS1 from customer where tipnumber = @a_TIPPRI
	select @a_names = ACCTNAME1, @a_names1 = ACCTNAME2, @a_addrs = ADDRESS1 from customer where tipnumber = @a_TIPSEC
	

	IF  (@a_namep <> @a_names) 
		begin
		update comb_in	
		set errmsg=errmsg + ' Names no match.' 
		where current of combine_crsr
		goto Next_Record
		end

	IF  (@a_addrp <> @a_addrs) 
		begin
		update comb_in	
		set errmsg=errmsg + ' Addr no match.' 
		where current of combine_crsr
		goto Next_Record
		end 

	IF  (@a_namep <> @a_names) and (@a_addrp <> @a_addrs)
	        	begin
        		update comb_in    
		set errmsg= errmsg + ' Names and addr no match.'
	        	where current of combine_crsr
        		goto Next_Record
	        	end

Next_Record:
		fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg
end


Fetch_Error:
close combine_crsr
deallocate combine_crsr

/* Copy error messages to comb_err */

insert into Comb_err
( Tip_Pri, Tip_Sec, Errmsg, trandate)
select Tip_pri, Tip_Sec, Errmsg, @DateToday from comb_in where  len(rtrim(errmsg ) ) > 0 

/* delete records with error messages */
Delete from Comb_in where  len(rtrim(errmsg ) ) > 0
GO
