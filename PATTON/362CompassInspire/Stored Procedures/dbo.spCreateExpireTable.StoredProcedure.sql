/****** Object:  StoredProcedure [dbo].[spCreateExpireTable]    Script Date: 02/20/2009 15:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will CHECK THE INPUT RECORD VALUES                                 */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCreateExpireTable] @DateOfExpire VARCHAR(10)  AS   

--declare @DateOfExpire VARCHAR(10)
--SET @DateOfExpire = '4/30/2007'
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME


set @ExpireDate = convert(datetime, @DateOfExpire)
SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 3
set @expirationdate = (rtrim(@intYear) + '-' + rtrim(@intmonth) + '-' + rtrim(@intday) +   ' 23:59:59.997')


--print 'year'
--print @intYear
--print 'month'
--print @intmonth
--print 'day'
--print @intday
--print '@expirationdate'
--print @expirationdate 

	TRUNCATE TABLE expiringpoints

	INSERT   into expiringpoints
	 SELECT tipnumber, sum(points * ratio) as addpoints,
	'0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, DateOfExpire = @DateOfExpire
	from history
	where histdate < @expirationdate and (trancode not like('R%') and
	     trancode <> 'IR')
	group by tipnumber


	UPDATE expiringpoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = expiringpoints.TIPNUMBER) 


	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = (ADDPOINTS + REDPOINTS)


	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE IS NULL 

	UPDATE expiringpoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE < '0' 

 


	update Monthly_Statement_File		      
	set
	    POINTSTOEXPIRE = XP.POINTSTOEXPIRE,
	    DATEOFEXPIRATION = @ExpireDate
	from dbo.expiringpoints as XP
	inner JOIN dbo.Monthly_Statement_File as BAS
	on XP.TIPNUMBER = BAS.TIPNUMBER  
	where XP.TIPNUMBER in (select BAS.TIPNUMBER from Monthly_Statement_File)
GO
