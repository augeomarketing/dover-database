/****** Object:  StoredProcedure [dbo].[MaxTip]    Script Date: 02/20/2009 15:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update theMAXTIP TABLE CREATED FOR 360ComPassPoints Processing                     */
/*     Updating the TIPNUMBER From The Customer File Using             */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/


CREATE PROCEDURE [dbo].[MaxTip]   @POSTDATE nvarchar(10) AS
declare @MaxTip numeric(15)





	select 
	    @MaxTip = (max(TIPNUMBER))
	 from AFFILIAT


	insert into MaxTipNumber
	(
	MaxTipNumber
	,RunDate
	)
	values
	(
	@MaxTip
	,@POSTDATE
	)
GO
