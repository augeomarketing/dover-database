/****** Object:  StoredProcedure [dbo].[spProcessCustDebitACCTS]    Script Date: 02/20/2009 15:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spProcessCustDebitACCTS] @POSTDATE nvarchar(10)AS
/* input */
/*********XXXXXXXXXXXXX**/
 /* input */
Declare @BankNum char(5)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @OldAcctNum char(25)
Declare @LetterType char(1)
Declare @Product char(2)
Declare @TipNumber char(15)
Declare @LastName char(50)
Declare @PurchAmt char(12)
Declare @PurchCnt char(3)
Declare @ReturnAmt char(12)
Declare @RetunCnt char(3)
Declare @NetAmt char(12)
Declare @NetCnt char(3)
Declare @DEBACCTIN nvarchar(10)
Declare @HOTFLAGNONZERO nvarchar(10)

/*********XXXXXXXXXXXXX**/
Declare @DATERUN char(10)
Declare @DDA CHAR(20)
Declare @CITYSTATE char(50)
Declare @CITY char(25)
Declare @STATE char(2)
Declare @HOTFLAG CHAR(1)
Declare @CREDITAMT char(10)
Declare @CREDITCNT CHAR(3)
Declare @DEBITAMT char(12)
Declare @DEBITCNT CHAR(3)
Declare @ACCTNUM char(16)
Declare @NAME1 char(50)
Declare @NAME2 char(50)
Declare @ADDRESS1 char(50)
Declare @ADDRESS2 char(50)
Declare @ZIP char(20)
Declare @MISC1 char(20)
Declare @SOMECODE char(2)
Declare @MISC2 char(20)
Declare @MULTIPLIER char(8)
Declare @OLDCCNUM char(16)
Declare @ACCTTYPE char(16)
Declare DCCUST_crsr cursor
for Select *
From DCCUST
Open DCCUST_crsr
Fetch DCCUST_crsr  
into 	 @ACCTNUM, @DDA, @NAME1, @NAME2, @ADDRESS1, @ADDRESS2, @CITYSTATE, @ZIP,
         @HOTFLAG, @MISC1, @MISC2, @SOMECODE

IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @DEBACCTIN = '0'
	SET @HOTFLAGNONZERO = '0'

/*                   */
while @@FETCH_STATUS = 0
begin 	
	set @OLDCCNUM = '0'	
	set @BankNum = '0'	
	set @HouseHold = ' '	
	set @DelFlag = ' '
	set @OverLimit = ' '
	set @LostStolen = ' '
	set @Fraud = ' '
	set @Closed = ' '
	set @Bankrupt = ' '	
	set @LetterType = ' '
	set @Product = ' '
	set @TipNumber = '0'
	set @LastName = ' '
	set @PurchAmt = '0'
	set @PurchCnt = '0' 
	set @ReturnAmt = '0'
	set @RetunCnt = '0'
	set @NetAmt = '0'
	set @NetCnt = '0'
	set @ACCTTYPE = 'd'
	set @DEBACCTIN = (@DEBACCTIN + 1)
              If   @HOTFLAG <>  '0'
	   set @HOTFLAGNONZERO = (@HOTFLAGNONZERO + 1) 

		

             
	 insert into cccust
	(				
          ACCTNUM, DDA, NAME1, NAME2, ADDRESS1, ADDRESS2, CITYSTATE, ZIP,
         HOTFLAG, CITY, STATE, OLDCCNUM, BankNum, BehSeg, HouseHold, 
	 HomePhone, DelFlag, OverLimit ,LostStolen ,Fraud ,Closed, Bankrupt,
	 LetterType, TipNumber,PurchAmt, PurchCnt, ReturnAmt, returncnt,
	 NetAmt ,NetCnt ,ACCTTYPE
	) 	
	values
	(
	  @ACCTNUM, @DDA, @NAME1, @NAME2, @ADDRESS1, @ADDRESS2, @CITYSTATE, @ZIP,
         @HOTFLAG, @CITY, @STATE, @OLDCCNUM, @BankNum, @BehSeg, @HouseHold, 
	 @HomePhone, @DelFlag, @OverLimit ,@LostStolen ,@Fraud ,@Closed, @Bankrupt,
	 @LetterType, @TipNumber, @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	 @NetAmt ,@NetCnt ,@ACCTTYPE 
	)	       	      	


		


Fetch DCCUST_crsr  
into 	 @ACCTNUM, @DDA, @NAME1, @NAME2, @ADDRESS1, @ADDRESS2, @CITYSTATE, @ZIP,
         @HOTFLAG, @MISC1, @MISC2, @SOMECODE

END /*while */

	update TIPCOUNTS
	SET		
	  DEBACCTIN = @DEBACCTIN
	  ,HOTFLAGNONZERO = @HOTFLAGNONZERO 	
	WHERE
	  POSTDATE = @POSTDATE

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  DCCUST_crsr
deallocate DCCUST_crsr
GO
