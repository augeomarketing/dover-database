/****** Object:  StoredProcedure [dbo].[spSendMailNotification]    Script Date: 02/20/2009 15:52:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update The Pearle mail table when an error occurs in the PickNClik Porcess   */
/*     Requesting ythet the error report berun            */
/* BY:  B.QUINN  */
/* DATE: 3/2008   */
/* REVISION: 0 */
/* */
/******************************************************************************/


CREATE PROCEDURE [dbo].[spSendMailNotification]     AS
declare @Errorcount char(1)



	begin
		insert into [PATTON\RN].[Maintenance].[dbo].[PerleMail] 
	(
	 dim_perlemail_subject
	,dim_perlemail_body
	,dim_perlemail_to
	,dim_perlemail_from
	,dim_perlemail_bcc
	)
	values
	(
	  '362 Compass Inspire Processing Complete'
	 ,'Please Send EMail Notifications as scheduled'
	 ,'ssmith@rewardsnow.com;Melissa.Wilson@compassbank.com'
	 ,'bquinn@rewardsnow.com'
	 ,1
	)
	end
GO
