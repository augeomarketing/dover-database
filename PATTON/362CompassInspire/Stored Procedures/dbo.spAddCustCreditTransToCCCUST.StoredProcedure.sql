/****** Object:  StoredProcedure [dbo].[spAddCustCreditTransToCCCUST]    Script Date: 02/20/2009 15:52:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Credit Trans Info                */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAddCustCreditTransToCCCUST] @POSTDATE NVARCHAR(10) AS
/* input */

Declare @MAXTIP char(15)
Declare @DATERUN char(10)
Declare @PURCHAMT float
Declare @PURCHCNT char(5)
Declare @RETURNAMT float
Declare @RETURNCNT char(5)
Declare @NETAMT float
Declare @NETCNT char(5)
Declare @MULTIPLIER real
Declare @ACCTNUM char(16)
Declare @ACCTTYPE char(1)
DECLARE @CREDITTRANCNT NVARCHAR (10)

BEGIN 
	
	
	
	
	/*set @DATERUN = GETDATE() */

	SET @ACCTTYPE = 'c'
	SET @CREDITTRANCNT = '0'
	

	update cccust	
	set	         
	     cccust.PURCHAMT = ct.PURCHAMT
                 ,cccust.PURCHCNT =  ct.PURCHCNT
                 ,cccust.RETURNAMT =  ct.RETURNAMT
                 ,cccust.RETURNCNT =  ct.RETURNCNT
                 ,cccust.NETAMT =  ct.NETAMT                 
	     ,cccust.NETCNT =  ct.NETCNT
                 ,cccust.MULTIPLIER =  ct.MULTIPLIER 	
	    ,cccust.ACCTTYPE =  @ACCTTYPE
                  ,cccust.DDA =  ' '
	    ,cccust.HOTFLAG =  ' '	
	    ,cccust.CITYSTATE =  ' ' 
	    ,@CREDITTRANCNT = (@CREDITTRANCNT + 1)	
	from dbo.cctran as ct
	inner join dbo.cccust as cust
	on ct.ACCTNUM = cust.ACCTNUM 	     
	where ct.ACCTNUM in (select cust.ACCTNUM from cccust)

	update cccust	
	set	         	    
	    cccust.ACCTTYPE =  @ACCTTYPE 	
	from dbo.cctran as ct
	inner join dbo.cccust as cust
	on ct.ACCTNUM = cust.ACCTNUM 	     
	where ct.ACCTNUM in (select cust.ACCTNUM from cccust)
	and cust.ACCTTYPE is null


	update cccust	
	set	         	    
	   multiplier =  '1' 	   
	where multiplier is null




             UPDATE TIPCOUNTS
	SET
	 CREDITTRANCNT = @CREDITTRANCNT
	WHERE POSTDATE = @POSTDATE  

END /*while */

EndPROC:
GO
