USE [362CompassInspire]
GO
/****** Object:  StoredProcedure [dbo].[Compass_Combines_Validate]    Script Date: 02/20/2009 15:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*                                                                            */
/*                   SQL TO PROCESS VALIDATE COMPASS COMBINES                 */
/*                                                                            */
/*                                                                            */
/* BY:  S.BLANCHETTE                                                          */
/* DATE: 3/2005                                                               */
/* REVISION: 0                                                                */
/*                                                                            */
/*                                                                            */
/******************************************************************************/	
/*                                                                            */
CREATE PROCEDURE [dbo].[Compass_Combines_Validate] @TIPPRI varchar(15), @TIPSEC varchar(15), @ErrorMessage varchar(80) OUTPUT
AS
-- Declare and initialize a variable to hold @@ERROR.
DECLARE @ErrorSave INT
SET @ErrorSave = 0
SET @ErrorMessage = ' '

declare @a_namep varchar(40), @a_names varchar(40), @a_addrp varchar(40), @a_addrs varchar(40)     
/******************************************************************************/	

if len(rtrim(@TIPPRI)) = 0 or @TIPPRI is NULL
	begin
		set @ErrorMessage ='Primary Tip Number is blank.' 
		goto EndRun
	end

if len(rtrim(@TIPSEC)) = 0 or @TIPSEC is NULL
	begin
		set @ErrorMessage ='Secondary Tip Number is blank.' 
		goto EndRun	
	end
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
IF  not EXISTS(SELECT * FROM Customer WHERE TIPnumber = @TIPPRI)
	begin
		set @ErrorMessage ='Primary Tip Number does not exist in Customer Table.' 
		goto EndRun		
	end

IF  NOT EXISTS(SELECT * FROM Customer WHERE TIPnumber = @TIPSEC)
	begin
		set @ErrorMessage ='Secondary Tip Number does not exist in Customer Table.' 
		goto EndRun		
	end

IF  not EXISTS(SELECT * FROM Affiliat WHERE TIPnumber = @TIPPRI)
	begin
		set @ErrorMessage ='Primary Tip Number does not exist in Affiliate Table.'	 
		goto EndRun		
	end

IF  NOT EXISTS(SELECT * FROM Affiliat WHERE TIPnumber = @TIPSEC)
	begin
		set @ErrorMessage ='Secondary Tip Number does not exist in Affiliate Table.'	 
		goto EndRun		
	end
EndRun:
RETURN
GO
