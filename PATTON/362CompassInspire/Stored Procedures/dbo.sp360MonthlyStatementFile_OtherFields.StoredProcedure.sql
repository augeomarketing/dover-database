/****** Object:  StoredProcedure [dbo].[sp360MonthlyStatementFile_OtherFields]    Script Date: 02/20/2009 15:52:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/*  Update the account id in the monthly_statment_file from affiliat                             */
/*Rich T                              */
/*******************************************************************************/

create PROCEDURE [dbo].[sp360MonthlyStatementFile_OtherFields] AS 
Declare  @SQLDynamic nvarchar(1000)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_affiliat]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_affiliat]

/* Create View */
set @SQLDynamic = 'create view view_affiliat as (select tipnumber, Max(acctid)as acctid from affiliat group by tipnumber )'
exec sp_executesql @SQLDynamic


/* Update the Monthly_Statement_File with account id from view  */
update Monthly_Statement_File 
set acctid = view_affiliat.acctid
from Monthly_Statement_File, view_affiliat where Monthly_Statement_File.tipnumber = view_affiliat.tipnumber 

/* Kill the View.   Die View !  Die ! */
drop view view_affiliat

/* Update the Monthly_Statement_File with Last Four  */
update Monthly_Statement_File set lastfour = Right( RTrim(acctid),4) 

/* Update the Monthly_Statement_File with STATUS   */
update Monthly_Statement_File 
set status = customer.status 
from Monthly_Statement_File, customer where Monthly_Statement_File.tipnumber = customer.tipnumber


/* Update the Monthly_Statement_File with FLOOR */
Update Monthly_Statement_File set PointFloor = floor(PointsEnd/750 ) * 750


/*  */
GO
