/****** Object:  StoredProcedure [dbo].[spPURGECALC]    Script Date: 02/20/2009 15:52:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Calculate the Monthly Purge Amount for Compass     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 3/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spPURGECALC] @POSTDATE NVARCHAR(10) AS

--declare @POSTDATE NVARCHAR(10)
--set @POSTDATE = '2/28/2007'

DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
declare @pointstopurge int
declare @countofpurge int



	drop table wrktab


	set @pointstopurge = 0
	set @countofpurge = 0


	select distinct acctid as acctid, '               ' as tipnumber 
	into wrktab
	from accountdeleteinput

	UPDATE wrktab 
	SET wrktab.tipnumber = affiliat.tipnumber
	 FROM affiliat 
 	WHERE
	affiliat.acctid = wrktab.acctid

	delete from wrktab 
	where tipnumber = ' ' 
	

	--select * from wrktab

	select 
	@pointstopurge = (@pointstopurge + c.runbalance),
	@countofpurge = (@countofpurge + 1)
	from customer as c
	inner join wrktab as w
	on c.tipnumber = w.tipnumber
	where c.tipnumber in (select w.tipnumber from wrktab)

--print 'count'
--print @countofpurge
--print 'amt'
--print @pointstopurge

	set @FIELDNAME = 'NUMBER OF ACCOUNTS TO BE PURGED'
	set @FIELDVALUE = @countofpurge



	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)

	set @FIELDNAME = 'ESTIMATED POINTS TO BE PURGED'
	set @FIELDVALUE = @pointstopurge

	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)
GO
