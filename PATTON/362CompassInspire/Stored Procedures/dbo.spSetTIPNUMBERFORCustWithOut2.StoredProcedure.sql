/****** Object:  StoredProcedure [dbo].[spSetTIPNUMBERFORCustWithOut2]    Script Date: 02/20/2009 15:52:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customers without a TIPNUMBER           */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spSetTIPNUMBERFORCustWithOut2]  AS
/* input */

/* Declare @MaxTip char(15)  */
Declare @DATERUN datetime
Declare @Tipnumber numeric(15)
Declare @newnum nvarchar(15)
Declare @MaxTip nvarchar(15)
Declare @TIPSFROMACCTNUM numeric(15)
Declare @TIPSFROMOLD numeric(15)
Declare @TIPSNEWDEB numeric(15)
Declare @TIPSNEWCRED numeric(15)
Declare @POSTDATE CHAR(10)
/* set @MaxTip = '0' */

	SET @POSTDATE = '6/30/2006'
	Set @DATERUN = @POSTDATE	
	set @Tipnumber = '0'
	SET @TIPSFROMACCTNUM = '0'
	SET @TIPSFROMOLD = '0'
	SET @TIPSNEWDEB = '0'
	SET @TIPSNEWCRED = '0'

	select
	  @Tipnumber = MaxTipNumber       
	from MAXTIPNUMBER

	
 
	

/*  - UPDATE CUST CREDIT TABLE with NEW TIPNUMBER from MAXTIP       */
 	 
	   
	     
	    update cccust	
	    set	
		@Tipnumber = (@Tipnumber + 1)
		,TIPNUMBER =  @Tipnumber
                          ,@TIPSNEWCRED = (@TIPSNEWCRED + 1)
                            WHERE TIPNUMBER  = '0' and
		  ACCTTYPE = 'c'  
 
 	 
	  
	       
	    update cccust	
	    set	
		@Tipnumber = (@Tipnumber + 1)
		,TIPNUMBER =  @Tipnumber
	            ,@TIPSNEWDEB = (@TIPSNEWDEB + 1)
                           WHERE TIPNUMBER  = '0' and
	              ACCTTYPE = 'd'  
    	  


/* UPDATE the MAXTIP Table with the last TIPNUMBER Assigned */

insert into MaxTipNumber
	(
	MaxTipNumber
	,RunDate
	)
	values
	(
	@Tipnumber
	,@DATERUN
	)

/* insert into TIPCOUNTS
	(
	 TIPSNEWDEB
	,TIPSNEWCRED
	,POSTDATE
	)
	values
	(
	 @TIPSNEWDEB
	,@TIPSNEWCRED
	,@DATERUN
	)  */
GO
