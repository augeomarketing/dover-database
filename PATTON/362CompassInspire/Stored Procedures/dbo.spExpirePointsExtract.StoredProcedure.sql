/****** Object:  StoredProcedure [dbo].[spExpirePointsExtract]    Script Date: 02/20/2009 15:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This will Create the Expired Points Table for Monthly Processing          */
/*     This process will take the current monthenddate and subtract 3 years     */
/*     All points older than this date are subject to expiration                */
/*    %%%%%% Note This version of the procedure creates the expiring points     */
/*           table based on the current month end year minus 3 to expire points */
/*           this table is then used BY spProcessExpiredPoints to update the    */
/*           Customer and History Tables                                        */
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/
CREATE PROCEDURE [dbo].[spExpirePointsExtract] @DateOfExpire NVARCHAR(10)  AS   

--declare @DateOfExpire NVARCHAR(10) 
--SET @DateOfExpire = '2/28/2007'
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME


set @ExpireDate = cast(@DateOfExpire as datetime)

SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 3
set @expirationdate = (rtrim(@intYear) + '-' + rtrim(@intmonth) + '-' + rtrim(@intday) +   ' 23:59:59.997')


--print 'year'
--print @intYear
--print 'month'
--print @intmonth
--print 'day'
--print @intday
--print '@expirationdate'
--print @expirationdate 

	TRUNCATE TABLE ExpiredPoints

	INSERT   into ExpiredPoints
	 SELECT tipnumber, sum(points * ratio) as addpoints,
	'0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, @ExpireDate as dateofexpire
	from history
	where histdate < @expirationdate and (trancode not like('R%') and
	     trancode <> 'IR')
	group by tipnumber


	UPDATE ExpiredPoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = ExpiredPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 (trancode  like('R%') or
   		  trancode = 'IR')  
	  	 AND TIPNUMBER = ExpiredPoints.TIPNUMBER) 


	UPDATE ExpiredPoints  
	SET POINTSTOEXPIRE = (ADDPOINTS + REDPOINTS),
	    dateofexpire = @ExpireDate


	UPDATE ExpiredPoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE IS NULL 

	UPDATE ExpiredPoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE < '0' 

	delete from ExpiredPoints 	
	WHERE
	POINTSTOEXPIRE = '0'
GO
