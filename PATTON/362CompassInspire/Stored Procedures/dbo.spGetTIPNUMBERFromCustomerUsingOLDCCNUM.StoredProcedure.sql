/****** Object:  StoredProcedure [dbo].[spGetTIPNUMBERFromCustomerUsingOLDCCNUM]    Script Date: 02/20/2009 15:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*     Updating the TIPNUMBER From The Customer File Using OLDCCNUM            */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spGetTIPNUMBERFromCustomerUsingOLDCCNUM] @POSTDATE NVARCHAR(10)  AS
/* input */


Declare @TIPSFROMOLD char(10)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)
declare @rundate datetime
BEGIN 
	
	SET @TIPSFROMOLD = '0'	
	set @rundate = getdate()
	
/*  - UPDATE CUST CREDIT TABLE with TIPNUMBER from AFFILIAT Based on CUST.OLDCCNUM       */

	update cccust	
	set
	    cccust.TIPNUMBER = af.TipNumber
	    ,@POSTDATE = POSTDATE
	    ,@TIPSFROMOLD =(@TIPSFROMOLD + 1)
	from dbo.AFFILIAT as af
	inner JOIN dbo.cccust as cust
	on af.ACCTID = cust.OLDCCNUM  
	where af.ACCTID in (select cust.OLDCCNUM from cccust) AND
	cust.TIPNUMBER = '0'
	

	update TIPCOUNTS
	  Set
	    TIPSFROMOLD = @TIPSFROMOLD	    
	where
	    POSTDATE = @POSTDATE

	set @FIELDNAME = 'TIPS FOUND USING OLD ACCOUNT NUMBER' 
 	set @FIELDVALUE = @TIPSFROMOLD

	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	  ,Rundate
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	  ,@Rundate
	)

	

END /*while */


EndPROC:
GO
