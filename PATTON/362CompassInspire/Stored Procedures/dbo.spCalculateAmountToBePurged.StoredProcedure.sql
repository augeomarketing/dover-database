/****** Object:  StoredProcedure [dbo].[spCalculateAmountToBePurged]    Script Date: 02/20/2009 15:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spCalculateAmountToBePurged] @POSTDATE NVARCHAR(10) AS   

/*********XXXXXXXXXXXXX**/
 /* input */
/*********XXXXXXXXXXXXX**/
DECLARE @PURGEPOINTS NUMERIC(10)	
DECLARE @PURGECNT NUMERIC(10)
DECLARE @ACCTID NVARCHAR(16)
DECLARE @DDA NVARCHAR(25)
DECLARE @FIELDNAME NVARCHAR(50)
DECLARE @FIELDVALUE NUMERIC(10)

	set @PURGEPOINTS = '0'	
	set @PURGECNT = '0'	


Declare Purge_crsr cursor
for Select *
From ACCOUNTDELETEINPUT
Open Purge_crsr
Fetch Purge_crsr  
into 	 @ACCTID, @DDA

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                   */
while @@FETCH_STATUS = 0
begin 	

	
	SELECT 
	@PURGEPOINTS = @PURGEPOINTS + YTDEarned
	,@PURGECNT = @PURGECNT + '1'
	FROM AFFILIAT
	WHERE 
	 ACCTID = @ACCTID

	

	   
		


Fetch Purge_crsr  
into 	 @ACCTID, @DDA


END /*while */


	set @FIELDNAME = 'NUMBER OF ACCOUNTS TO BE PURGED'
	set @FIELDVALUE = @PURGECNT

	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)

	set @FIELDNAME = 'TOTAL POINTS TO BE PURGED'
	set @FIELDVALUE = @PURGEPOINTS

	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,FILL
	  ,POSTDATE
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	)



GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Purge_crsr
deallocate Purge_crsr
GO
