/****** Object:  StoredProcedure [dbo].[spPersonatorAPI]    Script Date: 02/20/2009 15:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This will Update the PRSNAME file  for 360ComPassPoints                */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/*      THE KEY USED IN THIS PROCESS IS ONLY A TEMPORY KEY VALID UNTIL 8/14/2006    BQ */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spPersonatorAPI] AS  


declare @PersLoc varchar(64)
Declare @Register varchar(32)
declare @hName int
declare @Error int







BEGIN 	
	
-- Name Split Example (2000).Sql
--
-- This example demonstrates how to split names in a table. 


-- The table, NAMEPRS, was Created IN spBuildNameFileForPersonatorAPI


-- Change the following line to reflect your installation location of the
--   Personator API and registration string:

set @PersLoc = 'c:\PersAPI\'

/* TEMP KEY FOLLOWS REPLACE STRING AND COMMENT BEFORE MOVE TO PRODUCTION */

set @Register = 'D4D9D3D6CED5ACCAC9C8CFCC029B'

-- Register the API. Note that you MUST use a valid registration
--   key when testing the Right Fielder API with SQL Server. Contact
--   us at 781-545-7300 for more information:
set @Error = master.dbo.fPersRegister(@Register)

-- Specify the location of the Personator lookup tables (usually
--   a good idea, even if not necessary):
set @Error = master.dbo.fPersFileLoc(@PersLoc)

-- Initialize fielding session:
set @hName = master.dbo.fPersInitName(0, 1, 1)

if @hName = 0
begin
	set @Error = master.dbo.fPersLastError()
	raiserror ('PersInitName failed: %d', 16, 1, @Error)
	return
end

-- NAMEPRS's structure:
--   FULLNAME   nVarChar 40  <Input Full Name>
--   PRE1       nVarChar 40  <Output Prefix 1>
--   FN1        nVarChar 40  <Output First Name 1>
--   MN1        nVarChar 40  <Output Middle Name 1>
--   LN1        nVarChar 40  <Output Last Name 1>
--   SUF1       nVarChar 40  <Output Suffix 1>
--   PRE2       nVarChar 40  <Output Prefix 2>
--   FN2        nVarChar 40  <Output First Name 2>
--   MN2        nVarChar 40  <Output Middle Name 2>
--   LN2        nVarChar 40  <Output Last Name 2>
--   SUF2       nVarChar 40  <Output Suffix 2>
--   SALUTATION nVarChar 40  <Output Salutation>
--   TIPNUMBER  nVarChar 15  <INPUT FROM PREPROCESSING>
--   ACCTNUM    nVarChar 16  <INPUT FROM CLIENT>
/* use Demos */
 

update PERSNAME set  Pre1 = master.dbo.fPersPre1(@hName, FullName), 
	Fn1 = master.dbo.fPersFn1(@hName, FullName),  
 	Mn1 = master.dbo.fPersMn1(@hName, FullName),
	Ln1 = master.dbo.fPersLn1(@hName, FullName),
	Suf1 = master.dbo.fPersSuf1(@hName, FullName),
	Pre2 = master.dbo.fPersPre2(@hName, FullName),
	Fn2 = master.dbo.fPersFn2(@hName, FullName),
	Mn2 = master.dbo.fPersMn2(@hName, FullName),
	Ln2 = master.dbo.fPersLn2(@hName, FullName),
	Suf2 = master.dbo.fPersSuf2(@hName, FullName)  

-- All done:
set @Error = master.dbo.fPersCloseName(@hName)	 
     
	         
end
GO
