/****** Object:  StoredProcedure [dbo].[spZeroOutCCCustTransFields]    Script Date: 02/20/2009 15:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File for 360ComPassPoints                     */
/*    Updating the Cust Trans Fields where they are null                     */
/*  - Update CCCUST        */
/*  - Update HistoryStage          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spZeroOutCCCustTransFields] @POSTDATE NVARCHAR(10) AS    

declare @DATERUN char(10)

BEGIN 

	set @DATERUN = @POSTDATE

	/*  - UPDATE CUST CREDIT TABLE with CreditTrans Fields       */
	update cccust	
	set
	     cccust.PURCHAMT = '0'
	    ,cccust.PURCHCNT = '0'
                 ,cccust.RETURNAMT = '0'
                 ,cccust.RETURNCNT = '0'
                 ,cccust.NETAMT = '0'
	    ,cccust.NETCNT = '0'	
	    ,cccust.CREDITAMT = '0'
	    ,cccust.CREDITCNT = '0'
	    ,cccust.DEBITAMT = '0'
	    ,cccust.DEBITCNT = '0'
	    ,cccust.LASTNAME = ' '
	    ,cccust.EDITERRORSFLAG = ' '


END /*while */


EndPROC:
GO
