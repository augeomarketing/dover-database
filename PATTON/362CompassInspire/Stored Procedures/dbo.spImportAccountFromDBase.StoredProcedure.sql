/****** Object:  StoredProcedure [dbo].[spImportAccountFromDBase]    Script Date: 02/20/2009 15:52:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*  IMPORT INTO POINTS NOW FROM DBASE PROCESSING  */
/* */
/*    This imports data from Input_Customer into the account table*/
/*    it only ADDS account data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 3/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportAccountFromDBase] AS

/*  add accounts */

	Insert into Affiliat
(
	ACCTID ,	
	TIPNUMBER,
	LASTNAME,
	CARDTYPE,
	DATEADDED,
	ACCTSTATUS,
	YTDEarned
)
select 
	ACCTNUM,
	TIPNUMBER,
	LASTNAME,
	CARDTYPE,
	TRANDATE,
	STATUS,
	0
from  Input_Customer 
	where Input_Customer.ACCTNUM not in (select ACCTID from Affiliat)
GO
