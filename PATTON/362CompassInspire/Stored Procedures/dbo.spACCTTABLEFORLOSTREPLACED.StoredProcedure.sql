/****** Object:  StoredProcedure [dbo].[spACCTTABLEFORLOSTREPLACED]    Script Date: 02/20/2009 15:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will COUNT THE NUMBER OF ACCOUNTS TO BE REPLACED AND CREATE A TABLE CONTAINING THEM     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spACCTTABLEFORLOSTREPLACED] AS  

declare  @FIELDNAME nvarchar(50)
declare  @FIELDVALUE numeric(10)
declare  @POSTDATE  nvarchar(10)
declare @accountstobereplaced int
declare @CARRYOVERPOINTS int
declare @rundate datetime

	DELETE FROM ACCTTABLEFORLOSTREPLACED
	set @rundate = getdate()
	SET @accountstobereplaced = '0'
	SET @CARRYOVERPOINTS = '0'
             
	 insert into ACCTTABLEFORLOSTREPLACED
	(				
          TIPNUMBER,
          ACCTNUM,
          OLDACCTNUM,
	  POSTDATE
	)	 	
	SELECT	 
	  TIPNUMBER,
 	  ACCTNUM,
	  OLDCCNUM,
	  POSTDATE	       	      	
	FROM CCCUST
	WHERE ACCTNUM <> ' ' AND
	      OLDCCNUM <> ' '

 	
	SELECT	 
	  @accountstobereplaced = @accountstobereplaced + '1',
	  @POSTDATE = POSTDATE	 	       	      	
	FROM ACCTTABLEFORLOSTREPLACED





	SELECT
	@CARRYOVERPOINTS = (@CARRYOVERPOINTS + AF.YTDEarned)
	from dbo.AFFILIAT as AF
	inner join dbo.ACCTTABLEFORLOSTREPLACED as AR
	on AF.ACCTID = AR.OLDACCTNUM 	     
	where AF.ACCTID in (select AR.OLDACCTNUM from ACCTTABLEFORLOSTREPLACED)








		
	set @FIELDNAME = 'NUMBER OF ACCOUNTS TO BE REPLACED'
	set @FIELDVALUE = @accountstobereplaced  
  
	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,fill
	  ,POSTDATE
	  ,Rundate
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	  ,@Rundate
	)


	set @FIELDNAME = 'TOTAL POINTS TO BE MOVED TO NEW CARDS'
	set @FIELDVALUE = @CARRYOVERPOINTS  
  
	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,fill
	  ,POSTDATE
	  ,Rundate
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,' '
	  ,@POSTDATE 
	  ,@rundate
	)
GO
