/****** Object:  StoredProcedure [dbo].[spBuildNameFileForPersonator2]    Script Date: 02/20/2009 15:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the nameprs file to pass to personator for 360ComPassPoints                */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBuildNameFileForPersonator2] AS



	


BEGIN 	

	 Insert into nameprs
            (
	     ACCTNUM,
	     TIPNUMBER,
	     NAME1,                   
             LASTNAME 	     
            )
	  select	   
              AcctNum,
 	      tipNumber,
              Name1,
              LastName
	  from cccust
	 

	 Insert into addrprs
          (
	     ACCTNUM 
	     ,TIPNUMBER 
	     ,CITYSTATE                 	   
          )
	  select	  
	     ACCTNUM 
	     ,TIPNUMBER 
	     ,CITYSTATE 
	  from cccust
	  where   CityState <> ' '
	     or   CityState <> null               
	 
	         

	
	
	
	
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
GO
