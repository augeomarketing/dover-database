/****** Object:  StoredProcedure [dbo].[spGetTipNumberForNewAccounts]    Script Date: 02/20/2009 15:52:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spGetTipNumberForNewAccounts] AS   

/* input */
Declare @TRANDESC char(50)
Declare @RunDate dateTIME
Declare @RunDate2  dateTIME
Declare @BankNum char(5)
Declare @Name1 char(50)
Declare @Name2 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @BehSeg char(3)
Declare @HouseHold char(15)
Declare @HomePhone char(12)
Declare @DelFlag char(1)
Declare @OverLimit char(1)
Declare @LostStolen char(1)
Declare @Fraud char(1)
Declare @Closed char(1)
Declare @Bankrupt char(1)
Declare @AcctNum char(25)
Declare @OldAcctNum varchar(25)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TipNumber varchar(15)
Declare @LastName char(50)
Declare @PurchAmt float(10)
Declare @PurchCnt char(10)
Declare @ReturnAmt float(10)
Declare @RetunCnt char(10)
Declare @NetAmt float(10)
Declare @NetCnt char(10)
Declare @Multiplier float(3)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt numeric(10)
Declare @CreditCnt numeric(3)
Declare @DebitAmt numeric(12)
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @AcctStatus char(1)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailable char(10)
Declare @RunBalance   char(10)
Declare @RunRedeemed   char(10)
Declare @POINTS numeric(5)
Declare @OVERAGE numeric(5)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afCardType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afFound Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @YTDEarned numeric(10)
Declare @SSNum char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
Declare @SECID char(40)
Declare @POSTDATE char(10)
Declare @CREDACCTSADDED numeric(09)
Declare @CREDACCTSUPDATED numeric(09)
Declare @CREDACCTSPROCESSED numeric(09)
Declare @EDITERRORSFLAG nvarchar(1)
Declare @LASTRECNAME NVARCHAR(50)
Declare @LASTRECNAME2 NVARCHAR(50)
Declare @LASTRECADDR1 NVARCHAR(50)
Declare @LASTRECADDR2 NVARCHAR(50)
Declare @LASTRECCITY NVARCHAR(50)
Declare @LASTRECSTATE NVARCHAR(50)
Declare @LASTRECZIP NVARCHAR(50)
Declare @LASTRECTIP NVARCHAR(15)
Declare @Tip numeric(15)

Declare @newnum nvarchar(15)
Declare @MaxTip nvarchar(15)
Declare @TIPSFROMACCTNUM numeric(15)
Declare @TIPSFROMOLD numeric(15)
Declare @TIPSNEWDEB numeric(15)
Declare @TIPSNEWCRED numeric(15)
declare @FIELDNAME NVARCHAR(25)
DECLARE @FIELDVALUE NUMERIC(10)
DECLARE @TOTNEWACCTS NUMERIC(10)
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare cccust_crsr cursor
for Select *
From cccust ORDER BY NAME2

Open cccust_crsr
/*                  */



Fetch cccust_crsr  
into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME, @POSTDATE , @EDITERRORSFLAG, @AcctStatus

IF @@FETCH_STATUS = 1
	goto Fetch_Error 



/*	select
	@Tip = MaxTipNumber
	 ,@Tipnumber = CAST(@TIP AS INT)       
	from MAXTIPNUMBER */
	set @RunDate2 = getdate()
	set @Tipnumber = '362000000000000'
	/* set @Tipnumber = cast(@Tip as numeric) */


	set @Tipnumber = '0'
	SET @TIPSFROMACCTNUM = '0'

	SET @TIPSNEWDEB = '0'
	SET @TIPSNEWCRED = '0'
	SET @TOTNEWACCTS = '0'
	set @rundate2 = getdate()
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	SET @LASTRECNAME = ' '
	SET @LASTRECNAME2 = ' '
	SET @LASTRECADDR1 = ' '
	SET @LASTRECADDR2 = ' '
	SET @LASTRECCITY = ' '
	SET @LASTRECSTATE = ' '
	SET @LASTRECZIP = ' '
	SET @LASTRECTIP = ' '

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN


	IF  @LASTRECNAME = @NAME1
	AND @LASTRECNAME2 = @NAME2
	AND @LASTRECADDR1 = @Address1
	AND @LASTRECADDR2 = @Address2
	AND @LASTRECCITY = @City
	AND @LASTRECSTATE = @State
	AND @LASTRECZIP = @Zip
	 BEGIN
	    update cccust	
	    set	
		 TIPNUMBER =  @LASTRECTIP
		,@POSTDATE = POSTDATE
           	 WHERE TIPNUMBER = '0'
                 or TIPNUMBER  is null
	 END
	ELSE
	 BEGIN 
	    update cccust	
	    set	  
		 @Tipnumber = (@Tipnumber + 1)
		,TIPNUMBER =  @Tipnumber
                ,@TIPSNEWCRED = (@TIPSNEWCRED + 1)
		,@POSTDATE = POSTDATE
	        ,@LASTRECNAME = @NAME1
	        ,@LASTRECNAME2 = @NAME2
	        ,@LASTRECADDR1 = @Address1
	        ,@LASTRECADDR2 = @Address2
	        ,@LASTRECCITY = @City
	        ,@LASTRECSTATE = @State
	        ,@LASTRECZIP = @Zip
	        ,@LASTRECTIP = @Tipnumber
           	 WHERE TIPNUMBER = '0'
	         or  TIPNUMBER  is null			  
	 END



	IF @AcctType = 'c'
         SET   @TIPSNEWCRED = (@TIPSNEWCRED + 1)

	IF @AcctType = 'd'
         SET   @TIPSNEWDEB = (@TIPSNEWDEB + 1) 



/******  IF THE DELETE FLAG IS 'Y' THE NETAMT IS NOT ZEROED OUT. NETAMT IS USED FOR CONTROL TOTAL REPORTING */
            

FETCH_NEXT:
	
	Fetch cccust_crsr  
	into  	@BankNum, @Name1, @Name2, @Address1, @Address2,
	@City, @State, @Zip, @BehSeg, @HouseHold, @HomePhone, 
	@DelFlag, @OverLimit, @LostStolen, @Fraud, @Closed,
	@Bankrupt, @AcctNum, @OldAcctNum, @LetterType, @Rating, @TipNumber,
	@PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt,
	@NetAmt, @NetCnt, @Multiplier, @AcctType, @DDA, @CityState,
	@HotFlag,  @CreditAmt, @CreditCnt, @DebitAmt, @DebitCnt, @LASTNAME, @POSTDATE, @EDITERRORSFLAG, @AcctStatus

END /*while */


	
/* UPDATE the MAXTIP Table with the last TIPNUMBER Assigned */

insert into MaxTipNumber
	(
	MaxTipNumber
	,RunDate
	)
	values
	(
	@Tipnumber
	,@POSTDATE
	)

         UPDATE TIPCOUNTS
	   Set	   
	    TIPSNEWDEB = @TIPSNEWDEB
	   ,TIPSNEWCRED = @TIPSNEWCRED	
	Where
	    @POSTDATE = POSTDATE

	set @FIELDNAME = 'TOTAL NEW ACCOUNTS'
	SET @TOTNEWACCTS = (@TIPSNEWDEB + @TIPSNEWCRED)
 	set @FIELDVALUE = @TOTNEWACCTS



	set @FIELDNAME = 'NEW DEBIT ACCOUNTS' 
 	set @FIELDVALUE = @TIPSNEWDEB

	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,RunDate 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,@RunDate2
	)

	set @FIELDNAME = 'NEW CREDIT ACCOUNTS' 
 	set @FIELDVALUE = @TIPSNEWCRED

	INSERT INTO COMPASSINSPIREMONTHLYTOTALS
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	 ,RunDate 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	 ,@RunDate2
	)
   

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  cccust_crsr
deallocate  cccust_crsr
GO
