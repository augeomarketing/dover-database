/****** Object:  StoredProcedure [dbo].[spReturnNameFileFromPersonator]    Script Date: 02/20/2009 15:52:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the cccust file with names from personator            */
/* */
/*   - Read cccust  */
/*  - Update nameprs        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spReturnNameFileFromPersonator] AS



Declare @Name1 char(50)
Declare @AcctNum char(18)
Declare @LastName char(40)
Declare @TipNumber char(15)
Declare @CardNum char(1)





/*   - DECLARE CURSOR AND OPEN TABLE  */
Declare namesper_crsr cursor
for Select *
From namesper

Open namesper_crsr

	Fetch namesper_crsr  
	into  @TipNumber, @Name1, @LastName, @AcctNum, @CardNum

	

IF @@FETCH_STATUS = 1
	goto Fetch_Error



/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	
	
	 update cccust 
            set	     
             cccust.LASTNAME = @LastName 
	  where
	      cccust.TIPNUMBER = @Acctnum            

	
		
	Fetch namesper_crsr  
	into  	@TipNumber, @Name1, @LastName, @AcctNum, @CardNum

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  namesper_crsr
deallocate  namesper_crsr
GO
