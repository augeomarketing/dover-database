/****** Object:  StoredProcedure [dbo].[spCompassCustomerFixPOINTS]    Script Date: 02/20/2009 15:52:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will CHECK THE INPUT RECORD VALUES                                 */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCompassCustomerFixPOINTS]   AS  

/* declare @POSTDATE NVARCHAR(10) */
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @RunBalance   numeric(10)
Declare @RunRedeemed   numeric(10)
Declare @LastStmtDate datetime 
Declare @NextStmtDate datetime 
Declare @STATUS char(1) 
Declare @DATEADDED  datetime 
Declare @LASTNAME varchar(40)  
Declare @TIPFIRST varchar(3) 
Declare @TIPLAST varchar(12)
Declare @ACCTNAME1 char(50)
Declare @ACCTNAME2 char(50)
Declare @ACCTNAME3 char(50)
Declare @ACCTNAME4 char(50)
Declare @ACCTNAME5 char(50)
Declare @ACCTNAME6 char(50)
Declare @Address1 char(50)
Declare @Address2 char(50)
Declare @Address3 char(50)
Declare @Address4 char(50)
Declare @City char(50)
Declare @State char(10)
Declare @Zip char(20)
Declare @StatusDescription varchar(50)
Declare @HOMEPHONE char(10)
Declare @WORKPHONE char(10)
Declare @BusinessFlag char(1)
Declare @EmployeeFlag char(1)
Declare @SegmentCode char(2)
Declare @ComboStmt char(1)
Declare @RewardsOnline char(1)
Declare @NOTES char(50)
Declare @BonusFlag char(1)
Declare @Misc1 varchar(20)
Declare @Misc2 varchar(20)
Declare @Misc3 varchar(20)
Declare @Misc4 varchar(20)
Declare @Misc5 varchar(20)
Declare @RunBalanceNew varchar(10)
Declare @RunAvailiableNew varchar(10)
Declare @OLDESTDATE DATETIME

Declare Customer_crsr cursor
for Select *
From Customer

Open Customer_crsr

Fetch Customer_crsr  
	into  @TipNumber ,@RunAvailable ,@RunBalance ,@RunRedeemed ,@LastStmtDate ,@NextStmtDate,
	      @STATUS ,@DATEADDED ,@LASTNAME ,@TIPFIRST ,@TIPLAST, 
	      @ACCTNAME1 ,@ACCTNAME2 ,@ACCTNAME3 ,@ACCTNAME4 ,@ACCTNAME5 ,@ACCTNAME6,
              @Address1 ,@Address2 ,@Address3 ,@Address4,
              @City ,@State ,@Zip ,@StatusDescription,
              @HOMEPHONE ,@WORKPHONE ,@BusinessFlag ,@EmployeeFlag ,@SegmentCode ,@ComboStmt,
	      @RewardsOnline ,@NOTES ,@BonusFlag ,@Misc1 ,@Misc2 ,@Misc3 ,@Misc4, @Misc5,
              @RunBalanceNew ,@RunAvailiableNew   
 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error



	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	SET @RunAvailiableNew = '0'	
		
	select 
	   @RunAvailiableNew = sum(points * ratio)
	from
	history
	where
	tipnumber = @TipNumber
	
	IF @RunAvailiableNew IS NULL
	   BEGIN
	   SET @RunAvailiableNew = '0'
	   END

	update customer
	set
	   RunAvailable = @RunAvailiableNew,
	   Runbalance = @RunAvailiableNew
	where
	   tipnumber = @TipNumber





	
	Fetch Customer_crsr  
	into  @TipNumber ,@RunAvailable ,@RunBalance ,@RunRedeemed ,@LastStmtDate ,@NextStmtDate,
	      @STATUS ,@DATEADDED ,@LASTNAME ,@TIPFIRST ,@TIPLAST, 
	      @ACCTNAME1 ,@ACCTNAME2 ,@ACCTNAME3 ,@ACCTNAME4 ,@ACCTNAME5 ,@ACCTNAME6,
              @Address1 ,@Address2 ,@Address3 ,@Address4,
              @City ,@State ,@Zip ,@StatusDescription,
              @HOMEPHONE ,@WORKPHONE ,@BusinessFlag ,@EmployeeFlag ,@SegmentCode ,@ComboStmt,
	      @RewardsOnline ,@NOTES ,@BonusFlag ,@Misc1 ,@Misc2 ,@Misc3 ,@Misc4, @Misc5,
              @RunBalanceNew ,@RunAvailiableNew   
 	  	

END /*while */

	


GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Customer_crsr
deallocate  Customer_crsr
GO
