/****** Object:  Table [dbo].[expiringpoints]    Script Date: 02/20/2009 15:54:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[expiringpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateofExpire] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
