/****** Object:  Table [dbo].[nameprs]    Script Date: 02/20/2009 15:55:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nameprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[NAME1] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[CARDNUM] [nvarchar](16) NULL
) ON [PRIMARY]
GO
