/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 02/20/2009 15:52:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](16) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
