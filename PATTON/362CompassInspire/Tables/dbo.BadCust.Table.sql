/****** Object:  Table [dbo].[BadCust]    Script Date: 02/20/2009 15:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BadCust](
	[Banknum] [char](5) NULL,
	[TipNumber] [varchar](15) NULL,
	[Acctid] [varchar](18) NULL,
	[Name1] [char](40) NULL,
	[CardType] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
