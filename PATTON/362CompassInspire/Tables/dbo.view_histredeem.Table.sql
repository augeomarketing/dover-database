/****** Object:  Table [dbo].[view_histredeem]    Script Date: 02/20/2009 15:56:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[view_histredeem](
	[tipnumber] [varchar](15) NOT NULL,
	[lastredeem] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
