/****** Object:  Table [dbo].[DCTRANTOT]    Script Date: 02/20/2009 15:54:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DCTRANTOT](
	[CREDITAMT] [decimal](12, 2) NULL,
	[CREDITCNT] [decimal](12, 2) NULL,
	[DEBITAMT] [decimal](12, 2) NULL,
	[DEBITCNT] [decimal](12, 2) NULL,
	[TOTRECS] [decimal](12, 0) NULL
) ON [PRIMARY]
GO
