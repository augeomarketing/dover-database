/****** Object:  Table [dbo].[CompassDuplicateInputData]    Script Date: 02/20/2009 15:53:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompassDuplicateInputData](
	[NAME1] [char](50) NULL,
	[ADDRESS1] [char](50) NULL,
	[ADDRESS2] [char](50) NULL,
	[CITY] [char](50) NULL,
	[STATE] [char](10) NULL,
	[ZIP] [char](20) NULL,
	[ACCTNUM] [char](18) NULL,
	[OLDCCNUM] [char](18) NULL,
	[TIPNUMBER] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
