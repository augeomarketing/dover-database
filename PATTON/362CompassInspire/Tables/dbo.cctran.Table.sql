/****** Object:  Table [dbo].[cctran]    Script Date: 02/20/2009 15:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cctran](
	[BEHSEG] [char](3) NULL,
	[HOUSEHOLD] [char](15) NULL,
	[PURCHAMT] [char](12) NULL,
	[PURCHCNT] [char](5) NULL,
	[RETURNAMT] [char](12) NULL,
	[RETURNCNT] [char](5) NULL,
	[NETAMT] [char](12) NULL,
	[NETCNT] [char](5) NULL,
	[ACCTNUM] [char](16) NULL,
	[MULTIPLIER] [char](6) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
