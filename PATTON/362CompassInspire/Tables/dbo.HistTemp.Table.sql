/****** Object:  Table [dbo].[HistTemp]    Script Date: 02/20/2009 15:54:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistTemp](
	[SlNo] [decimal](18, 0) NULL,
	[TipNumber] [varchar](15) NULL,
	[AcctId] [varchar](25) NULL,
	[Histdate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[TranCode] [varchar](2) NULL,
	[IncDec] [nvarchar](1) NULL,
	[TypeCode] [nvarchar](1) NULL,
	[ExpiryFlag] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
