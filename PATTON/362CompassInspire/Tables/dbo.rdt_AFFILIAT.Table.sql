/****** Object:  Table [dbo].[rdt_AFFILIAT]    Script Date: 02/20/2009 15:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rdt_AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [char](50) NULL,
	[CardType] [varchar](20) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[PrimaryFlag] [varchar](10) NULL,
	[SecId] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[YTDEarned] [float] NOT NULL,
	[SSNum] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
