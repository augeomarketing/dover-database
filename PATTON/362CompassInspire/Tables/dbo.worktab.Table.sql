/****** Object:  Table [dbo].[worktab]    Script Date: 02/20/2009 15:56:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[worktab](
	[tipnumber] [varchar](15) NOT NULL,
	[tranpoints] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
