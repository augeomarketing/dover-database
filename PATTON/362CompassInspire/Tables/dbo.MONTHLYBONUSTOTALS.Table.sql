/****** Object:  Table [dbo].[MONTHLYBONUSTOTALS]    Script Date: 02/20/2009 15:55:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MONTHLYBONUSTOTALS](
	[BONUSTOTAL] [decimal](18, 0) NULL,
	[BITOTAL] [decimal](18, 0) NULL,
	[BICOUNT] [decimal](18, 0) NULL,
	[OATOTAL] [decimal](18, 0) NULL,
	[OACOUNT] [decimal](18, 0) NULL,
	[OBTOTAL] [decimal](18, 0) NULL,
	[OBCOUNT] [decimal](18, 0) NULL,
	[OCTOTAL] [decimal](18, 0) NULL,
	[OCCOUNT] [decimal](18, 0) NULL,
	[ODTOTAL] [decimal](18, 0) NULL,
	[ODCOUNT] [decimal](18, 0) NULL,
	[OETOTAL] [decimal](18, 0) NULL,
	[OECOUNT] [decimal](18, 0) NULL,
	[OFTOTAL] [decimal](18, 0) NULL,
	[OFCOUNT] [decimal](18, 0) NULL,
	[OGTOTAL] [decimal](18, 0) NULL,
	[OGCOUNT] [decimal](18, 0) NULL,
	[OHTOTAL] [decimal](18, 0) NULL,
	[OHCOUNT] [decimal](18, 0) NULL,
	[OITOTAL] [decimal](18, 0) NULL,
	[OICOUNT] [decimal](18, 0) NULL,
	[OJTOTAL] [decimal](18, 0) NULL,
	[OJCOUNT] [decimal](18, 0) NULL,
	[OKTOTAL] [decimal](18, 0) NULL,
	[OKCOUNT] [decimal](18, 0) NULL,
	[OLTOTAL] [decimal](18, 0) NULL,
	[OLCOUNT] [decimal](18, 0) NULL,
	[OMTOTAL] [decimal](18, 0) NULL,
	[OMCOUNT] [decimal](18, 0) NULL,
	[ONTOTAL] [decimal](18, 0) NULL,
	[ONCOUNT] [decimal](18, 0) NULL,
	[POSTDATE] [nvarchar](10) NULL,
	[BONUSCOUNT] [decimal](18, 0) NULL,
	[BDTOTAL] [decimal](18, 0) NULL,
	[BDCOUNT] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
