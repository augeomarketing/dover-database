/****** Object:  Table [dbo].[ACCOUNTFIX2]    Script Date: 02/20/2009 15:52:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCOUNTFIX2](
	[TIPNUMBER] [nvarchar](15) NULL,
	[RUNAVAILABLE] [numeric](18, 0) NULL,
	[HISTPOINTS] [numeric](18, 0) NULL,
	[DIFF] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
