USE [646]
GO
/****** Object:  StoredProcedure [dbo].[spLoadBeginningBalance]    Script Date: 12/21/2011 15:33:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadBeginningBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadBeginningBalance]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadBeginningBalance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spLoadBeginningBalance] @MonthBeginDate nchar(10)
AS 
/*Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)  chg 6/15/2006   */
Declare @MonthBucket char(10), @MonthBegin char(2), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)
set @MonthBegin = month(Convert(datetime, @MonthBeginDate) ) + 1
if CONVERT( int , @MonthBegin)=''13'' 
	begin
	set @monthbegin=''1''
end	
set @MonthBucket=''MonthBeg'' + @monthbegin
set @SQLUpdate=N''update Beginning_Balance_Table set ''+ Quotename(@MonthBucket) + N''= (select pointsend from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber) where exists(select * from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber)''
set @SQLInsert=N''insert into Beginning_Balance_Table (Tipnumber, '' + Quotename(@MonthBucket) + '') select tipnumber, pointsend from Monthly_Statement_File where not exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)''
exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert
' 
END
GO
