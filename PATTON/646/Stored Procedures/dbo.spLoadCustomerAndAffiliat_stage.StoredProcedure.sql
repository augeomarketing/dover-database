USE [646]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerAndAffiliat_stage]    Script Date: 04/23/2013 10:14:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerAndAffiliat_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage]
GO

USE [646]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerAndAffiliat_stage]    Script Date: 04/23/2013 10:14:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage]   
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
	, @EndDate DateTime
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/* Update all of the Customer_Stage records to a status of ''C'' .... As new records are inserted they have a status of A from the Custin table, 
As records are udated, they already have  a status of A in Custin too 
Anything remaining means that this is the first month that they HAVEN''T appeared, their status is already C and they will be added 
to the Customer_Closed table with MonthEndToDelete set to Two months beyond the EndDate passed in
*/

--Get the dateToDelete (x months out based on Client.ClosedMonths)
	declare @tmpDate datetime,@NumMonths smallint, @DateClosed datetime, @DateToDelete datetime
	declare @DBName varchar(50), @sqlUpdate nvarchar(max), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max), @sqlDeclare nvarchar(max), @sqlif nvarchar(max)

	set @NumMonths =(SELECT ClosedMonths from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst) 

	set @DBName = (SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)
					
	select @tmpDate=dateadd(d,1,@Enddate)
	select @tmpDate=dateadd(m,@NumMonths,@tmpDate)
	select @DateToDelete=dateadd(d,-1,@tmpDate)   --usually 2 months beyond the Close date
	set @DateClosed=@EndDate

-- SET STATUS TO C 
	Update dbo.customer_stage 
	set status='C'

	Update dbo.Affiliat_stage 
	set acctstatus='C'


-- Update/Insert CUSTOMER TABLE
	update dbo.customer_stage
	set Status=uci.STATUS, AcctName1=uci.NAMEACCT1, AcctName2=uci.NAMEACCT2, AcctName3=uci.NAMEACCT3
	, AcctName4	= uci.NAMEACCT4, AcctName5=uci.NAMEACCT5, AcctName6=uci.NAMEACCT6, Address1=uci.ADDRESS1
	, Address2=uci.ADDRESS2, Address4=uci.ADDRESS4, City=rtrim(uci.CITY), State=rtrim(uci.STATE)
	, Zipcode=rtrim(uci.ZIP), HomePhone=uci.HOMEPHONE, WorkPhone=uci.WORKPHONE, lastname=uci.LASTNAME
	, misc1=uci.MISC1, misc2=uci.MISC2, misc3=uci.MISC3
	from dbo.CUSTOMER_Stage cs join ufn_UniqueCustInRecord() uci on cs.TIPNUMBER=uci.TIPNUMBER
	where cs.tipnumber=uci.TIPNUMBER

	INSERT INTO dbo.Customer_stage (AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Status, TIPNumber, TIPFirst, TIPLast,  Address1, Address2, Address4, City, State, Zipcode,  LASTNAME, HomePhone, WorkPhone, DateAdded, Misc1, Misc2, Misc3)
	select 
		ufn.NAMEACCT1, ufn.NAMEACCT2, ufn.NAMEACCT3, ufn.NAMEACCT4, ufn.NAMEACCT5, ufn.NAMEACCT6, ufn.STATUS,
		ufn.TIPNUMBER, ufn.TIPFIRST, ufn.TIPLAST, ufn.ADDRESS1, ufn.ADDRESS2, ufn.ADDRESS4, ufn.CITY, ufn.STATE, ufn.ZIP, ufn.LASTNAME,
		ufn.HOMEPHONE, ufn.WORKPHONE, ufn.DATEADDED, ufn.MISC1, ufn.MISC2, ufn.MISC3
	From ufn_UniqueCustInRecord() ufn
	left outer join CUSTOMER_Stage cs on ufn.TIPNUMBER = cs.TIPNUMBER
	where cs.TIPNUMBER is null 
	
	update CUSTOMER_Stage
	set RunAvailable=0
	where RunAvailable is null
	 
	update CUSTOMER_Stage
	set RUNBALANCE=0
	where RUNBALANCE is null

	update CUSTOMER_Stage
	set RunRedeemed=0
	where RunRedeemed is null
	
	update CUSTOMER_Stage
	set Misc2 = csin.MISC2 
	from dbo.CUSTOMER_Stage cs join CUSTIN csin on cs.TIPNUMBER=csin.TIPNUMBER
	where LEN(csin.MISC2)>0 AND LEN(cs.Misc2)=0 
	
-- Update/Insert AFFILIAT TABLE
	update dbo.AFFILIAT_stage	
	set lastname=ci.lastname, acctstatus=ci.status, SECID=left(ltrim(ci.MISC1),9), CustID=isnull(right(rtrim(ci.MISC2),13),'')
	from dbo.CUSTIN ci
	where acctid=acct_num

	INSERT INTO dbo.affiliat_stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned, SECID, CustID) 
	select ci.ACCT_NUM, ci.TIPNUMBER, 'Debit', cast(ci.DateAdded as datetime), 'A', 'Debit Card', ci.LASTNAME, '0', left(ltrim(ci.MISC1),9), isnull(right(rtrim(ci.MISC2),13),'') 
	from dbo.CUSTIN ci left outer join affiliat_stage afs on ci.ACCT_NUM = afs.ACCTID
	where afs.TIPNUMBER is null 

-- Insert CUSTOMER CLOSED TABLE
	INSERT into dbo.Customer_Closed 
	select cs.Tipnumber, @DateClosed, @DateToDelete 
	from dbo.Customer_stage cs left outer join dbo.Customer_Closed cc on cs.tipnumber = cc.tipnumber
	where cs.Status='C' AND cc.tipnumber is null 

--see if any of the closed customers have transactions for > DateToDelete...if so , bump their DateToDelete value up by a month
	Update dbo.Customer_Closed 
	set DateToDelete=dateadd(d,-1,dateadd(m,1,dateadd(d,1,DateToDelete))) 
	where tipnumber in (select Tipnumber from dbo.History_stage where histdate >= @DateToDelete) 

-- REMOVE FROM CUSTOMER CLOSED TABLE IF TIPNUMBER HAS STATUS OF A 
	Delete from dbo.Customer_Closed
	where TipNumber in (select TipNumber from dbo.CUSTOMER_Stage where STATUS='A') 

--Update the statusDescription field
	Update dbo.Customer_Stage 
	set StatusDescription = rs.StatusDescription 
	from dbo.Customer_Stage cs join Rewardsnow.dbo.Status rs on cs.status=rs.status 

--Fix * in address2 field
	Update dbo.Customer_Stage 
	set Address2= (CASE WHEN Address2 LIKE '%[0-9a-Z]%' THEN Address2 
						ELSE REPLACE(Address2, '*', '') 
						END ) 

END


GO


