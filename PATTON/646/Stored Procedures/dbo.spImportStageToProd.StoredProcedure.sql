USE [646]
GO
/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 12/21/2011 15:33:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportStageToProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportStageToProd]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportStageToProd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */
/*************************************************/
/* S Blanchette                                  */
/* 3/2011                                        */
/* Added code to zero out RunBalance in stage    */
/* SEB001                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 12/2011                                        */
/* Added code to zero out RunBalancenew and      */
/* RunAvailableNew in Customer after points add  */
/* SEB002                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 5/2012                                        */
/* Added field to update acctstatus in affiliat  */
/* SEB003                                        */
/*************************************************/

CREATE PROCEDURE [dbo].[spImportStageToProd] @TipFirst char(3)
AS 
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 
/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )
----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
Update Affiliat 
set YTDEarned = S.YTDEarned from Affiliat_Stage S join Affiliat A on S.Tipnumber = A.Tipnumber

--	SEB003 Update Existing accounts with ACCTSTATUS from Affiliat_Stage
Update Affiliat 
set Acctstatus = S.Acctstatus 
from Affiliat_Stage S join Affiliat A on S.Tipnumber = A.Tipnumber and S.Acctid = A.acctid

--	Insert New Affiliat accounts
Insert into Affiliat 
	select * from Affiliat_Stage where 
		AcctID not in (select AcctID from Affiliat )

------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
Update Customer 
Set 
Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

-- Set the RunAvailable and RunBalance to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
/***************************/
/* START SEB001            */
/***************************/
	--Update dbo.Customer_Stage 	
	--	Set RunAvailable = 0 

	Update dbo.Customer_Stage 	
		Set RunAvailable = 0, RunBalance=0
/***************************/
/* END SEB001           */
/***************************/
	
--	Insert New Customers from Customers_Stage
Insert into Customer 
	select * from Customer_Stage where 
	Tipnumber not in (select Tipnumber from Customer )
--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
Update Customer 
	set RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

/***************************/
/* START SEB002            */
/***************************/

Update Customer
set RunAvaliableNew = 0, RunBalanceNew = 0

/***************************/
/* END SEB002           */
/***************************/

----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
Insert Into History 
	select * from History_Stage 
	where SecID = ''NEW''
-- Set SecID in History_Stage so it doesn''t get double posted. 
Update History_Stage  set SECID = ''Imported to Production ''+ convert(char(20), GetDate(), 120)  where SecID = ''NEW'' and Tipnumber not in (select tipnumber from HistoryDeleted)
----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
-- and the tipnumber isn''t in the customer deleted table
insert into OneTimeBonuses 
select * from OneTimeBonuses_stage 
where not exists
	( select * from OneTimeBonuses 
	where OneTimeBonuses_stage.Tipnumber = OneTimeBonuses.Tipnumber and 
	OneTimeBonuses_stage.Trancode = OneTimeBonuses.Trancode)
-- Truncate Stage Tables so''s we don''t double post.
truncate table Customer_Stage
truncate table Affiliat_Stage
truncate table History_Stage
truncate table OneTimeBonuses_stage
--' 
END
GO
