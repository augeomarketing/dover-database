USE [646]
GO
/****** Object:  Table [dbo].[PointsFactor]    Script Date: 12/21/2011 15:29:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PointsFactor_Factor]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PointsFactor_Factor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] DROP CONSTRAINT [DF_PointsFactor_Factor]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsFactor]') AND type in (N'U'))
DROP TABLE [dbo].[PointsFactor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsFactor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PointsFactor](
	[PAN] [nchar](16) NOT NULL,
	[Factor] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PointsFactor_Factor]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PointsFactor_Factor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] ADD  CONSTRAINT [DF_PointsFactor_Factor]  DEFAULT ((0)) FOR [Factor]
END


End
GO
