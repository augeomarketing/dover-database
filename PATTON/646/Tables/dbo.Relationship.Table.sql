USE [646]
GO
/****** Object:  Table [dbo].[Relationship]    Script Date: 12/21/2011 15:29:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Relationship_Factor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Relationship]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Relationship_Factor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Relationship] DROP CONSTRAINT [DF_Relationship_Factor]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND type in (N'U'))
DROP TABLE [dbo].[Relationship]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Relationship]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Relationship](
	[Acct] [nchar](16) NOT NULL,
	[Factor] [numeric](4, 2) NOT NULL,
 CONSTRAINT [PK_Relationship] PRIMARY KEY CLUSTERED 
(
	[Acct] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Relationship_Factor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Relationship]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Relationship_Factor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Relationship] ADD  CONSTRAINT [DF_Relationship_Factor]  DEFAULT ((1.00)) FOR [Factor]
END


End
GO
