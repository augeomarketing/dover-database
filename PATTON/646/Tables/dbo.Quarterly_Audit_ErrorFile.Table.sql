USE [646]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 12/21/2011 15:29:27 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Begin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Begin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_End]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_End]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Net]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Net]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Bonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Bonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Redeem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_CRD_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_CRD_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_CRD_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_CRD_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_DBT_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_DBT_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_DBT_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_DBT_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_ADJ_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_ADJ_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[TOT_Begin] [numeric](18, 0) NULL,
	[TOT_End] [numeric](18, 0) NULL,
	[TOT_Add] [numeric](18, 0) NULL,
	[TOT_Subtract] [numeric](18, 0) NULL,
	[TOT_Net] [numeric](18, 0) NULL,
	[TOT_Bonus] [numeric](18, 0) NULL,
	[TOT_Redeem] [numeric](18, 0) NULL,
	[TOT_ToExpire] [numeric](18, 0) NULL,
	[CRD_Purch] [numeric](18, 0) NULL,
	[CRD_Return] [numeric](18, 0) NULL,
	[DBT_Purch] [numeric](18, 0) NULL,
	[DBT_Return] [numeric](18, 0) NULL,
	[ADJ_Add] [numeric](18, 0) NULL,
	[ADJ_Subtract] [numeric](18, 0) NULL,
	[ErrorMsg] [nvarchar](50) NULL,
	[CurrentEnd] [numeric](18, 0) NULL,
 CONSTRAINT [PK_Quarterly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Begin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Begin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Begin]  DEFAULT ((0)) FOR [TOT_Begin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_End]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_End]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_End]  DEFAULT ((0)) FOR [TOT_End]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Add]  DEFAULT ((0)) FOR [TOT_Add]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Subtract]  DEFAULT ((0)) FOR [TOT_Subtract]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Net]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Net]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Net]  DEFAULT ((0)) FOR [TOT_Net]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Bonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Bonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Bonus]  DEFAULT ((0)) FOR [TOT_Bonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_Redeem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_Redeem]  DEFAULT ((0)) FOR [TOT_Redeem]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_TOT_ToExpire]  DEFAULT ((0)) FOR [TOT_ToExpire]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_CRD_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_CRD_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Purch]  DEFAULT ((0)) FOR [CRD_Purch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_CRD_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_CRD_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_CRD_Return]  DEFAULT ((0)) FOR [CRD_Return]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_DBT_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_DBT_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Purch]  DEFAULT ((0)) FOR [DBT_Purch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_DBT_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_DBT_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_DBT_Return]  DEFAULT ((0)) FOR [DBT_Return]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_ADJ_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_ADJ_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Add]  DEFAULT ((0)) FOR [ADJ_Add]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_ADJ_Subtract]  DEFAULT ((0)) FOR [ADJ_Subtract]
END


End
GO
