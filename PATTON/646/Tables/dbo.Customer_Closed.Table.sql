USE [646]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 12/21/2011 15:29:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_Closed]') AND type in (N'U'))
DROP TABLE [dbo].[Customer_Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer_Closed]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
