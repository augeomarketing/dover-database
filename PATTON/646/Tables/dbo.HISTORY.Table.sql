USE [646]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 12/21/2011 15:29:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
