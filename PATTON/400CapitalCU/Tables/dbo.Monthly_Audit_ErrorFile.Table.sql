USE [400CapitalCU]
GO

/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 03/16/2011 09:55:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO

USE [400CapitalCU]
GO

/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 03/16/2011 09:55:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SET ANSI_PADDING ON
--GO

CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [bigint] NULL,
	[PointsEnd] [bigint] NULL,
	[PointsPurchased] [bigint] NULL,
	[PointsBonus] [bigint] NULL,
	[PointsBonusMN] [bigint] NULL,
	[PointsAdded] [bigint] NULL,
	[PointsIncreased] [bigint] NULL,
	[PointsRedeemed] [bigint] NULL,
	[PointsReturned] [bigint] NULL,
	[PointsSubtracted] [bigint] NULL,
	[PointsDecreased] [bigint] NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [bigint] NULL
) ON [PRIMARY]

GO

--SET ANSI_PADDING OFF
--GO


