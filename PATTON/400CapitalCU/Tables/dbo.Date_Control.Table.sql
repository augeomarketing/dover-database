USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[Date_Control]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Date_Control](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[Start_Date] [char](10) NULL,
	[End_Date] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
