USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[HistTemp]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistTemp](
	[SlNo] [numeric](18, 0) NULL,
	[TipNumber] [varchar](15) NULL,
	[Histdate] [datetime] NULL,
	[Points] [numeric](18, 0) NULL,
	[TranCode] [varchar](2) NULL,
	[IncDec] [nvarchar](1) NULL,
	[TypeCode] [nvarchar](1) NULL,
	[ExpiryFlag] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
