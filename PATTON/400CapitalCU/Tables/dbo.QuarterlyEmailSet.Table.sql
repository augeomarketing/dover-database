USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[QuarterlyEmailSet]    Script Date: 10/13/2009 10:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuarterlyEmailSet](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Tipnumber] [nvarchar](15) NOT NULL,
	[EmailStatement] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[QuarterlyEmailSet] ADD  CONSTRAINT [DF_QuarterlyEmailSet_EmailStatement]  DEFAULT ('Y') FOR [EmailStatement]
GO
