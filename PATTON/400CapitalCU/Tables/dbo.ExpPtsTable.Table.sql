USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[ExpPtsTable]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExpPtsTable](
	[Tipnumber] [varchar](15) NOT NULL,
	[DatexpiringStrt] [datetime] NULL,
	[DatexpiringEnd] [datetime] NULL,
	[runredeemed] [int] NULL,
	[PtsExpiring] [int] NULL,
	[PtsRetired] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ExpPtsTable] ADD  CONSTRAINT [DF_ExpPtsTable_runredeemed]  DEFAULT (0) FOR [runredeemed]
GO
ALTER TABLE [dbo].[ExpPtsTable] ADD  CONSTRAINT [DF_ExpPtsTable_PtsExpiring]  DEFAULT (0) FOR [PtsExpiring]
GO
ALTER TABLE [dbo].[ExpPtsTable] ADD  CONSTRAINT [DF_ExpPtsTable_PtsRetired]  DEFAULT (0) FOR [PtsRetired]
GO
