USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[capwork]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[capwork](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZipCode] [varchar](15) NULL,
	[STATUS] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
