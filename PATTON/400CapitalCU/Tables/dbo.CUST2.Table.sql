USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[CUST2]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUST2](
	[MATCH1] [nvarchar](6) NULL,
	[MATCH2] [nvarchar](6) NULL,
	[MATCH3] [nvarchar](6) NULL,
	[MATCH4] [nvarchar](6) NULL,
	[MATCH5] [nvarchar](6) NULL,
	[RNUM] [nvarchar](6) NULL,
	[CUSTNUM] [nvarchar](17) NULL,
	[OLDCUST] [nvarchar](17) NULL,
	[STATUS] [nvarchar](6) NULL,
	[NEWCUST] [nvarchar](6) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[TIPNUMBER] [nvarchar](17) NULL,
	[BANK] [nvarchar](6) NULL,
	[BIN] [nvarchar](6) NULL,
	[ACCTNUM] [nvarchar](19) NULL,
	[NAME1] [nvarchar](31) NULL,
	[NAME2] [nvarchar](32) NULL,
	[NAME3] [nvarchar](6) NULL,
	[NAME4] [nvarchar](6) NULL,
	[NAME5] [nvarchar](6) NULL,
	[NAME6] [nvarchar](6) NULL,
	[ADDRESS1] [nvarchar](40) NULL,
	[CSZ] [nvarchar](50) NULL,
	[CITY] [nvarchar](30) NULL,
	[STATE] [nvarchar](2) NULL,
	[ZIP] [nvarchar](9) NULL,
	[PHONE] [nvarchar](10) NULL,
	[LAST6] [nvarchar](6) NULL
) ON [PRIMARY]
GO
