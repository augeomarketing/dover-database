USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT_Stage](
	[ACCTID] [char](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NULL,
	[CustID] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
