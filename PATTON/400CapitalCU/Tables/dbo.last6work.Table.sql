USE [400CapitalCU]
GO
/****** Object:  Table [dbo].[last6work]    Script Date: 10/13/2009 10:27:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[last6work](
	[CUSTNUM] [nvarchar](20) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[LAST6] [nvarchar](6) NULL,
	[NAME1] [nvarchar](40) NULL,
	[NAME2] [nvarchar](40) NULL,
	[NAME3] [nvarchar](40) NULL,
	[NAME4] [nvarchar](40) NULL,
	[NAME5] [nvarchar](40) NULL,
	[NAME6] [nvarchar](40) NULL,
	[STATUS] [nvarchar](1) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[ADDRESS1] [nvarchar](50) NULL,
	[ADDRESS2] [nvarchar](50) NULL,
	[ADDRESS3] [nvarchar](50) NULL,
	[CSZ] [nvarchar](40) NULL,
	[ZIP] [nvarchar](15) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[PHONE] [nvarchar](10) NULL,
	[WORKPHONE] [nvarchar](10) NULL,
	[PERIOD] [nvarchar](10) NULL,
	[BUSFLAG] [nvarchar](1) NULL,
	[SEGCODE] [nvarchar](2) NULL,
	[EMAIL] [nvarchar](40) NULL,
	[CURRBAL] [nvarchar](11) NULL,
	[PAYMENT] [nvarchar](11) NULL,
	[FEE] [nvarchar](11) NULL,
	[ISSUE_DT] [nvarchar](10) NULL,
	[ACTIVE_DT] [nvarchar](10) NULL,
	[MISC1] [nvarchar](20) NULL,
	[MISC2] [nvarchar](20) NULL,
	[MISC3] [nvarchar](20) NULL
) ON [PRIMARY]
GO
