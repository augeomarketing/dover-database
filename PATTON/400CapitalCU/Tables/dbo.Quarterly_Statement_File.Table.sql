USE [400CapitalCU]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMN]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturned]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_ExpPnts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_ExpPnts]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_RewardsOnline]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_RewardsOnline]
END

GO

USE [400CapitalCU]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 03/16/2011 09:57:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

USE [400CapitalCU]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 03/16/2011 09:57:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SET ANSI_PADDING ON
--GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [char](40) NULL,
	[Acctname4] [char](40) NULL,
	[Acctname5] [char](40) NULL,
	[Acctname6] [char](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[PointsBegin] [bigint] NULL,
	[PointsEnd] [bigint] NULL,
	[PointsPurchased] [bigint] NULL,
	[PointsBonus] [bigint] NULL,
	[PointsBonusMN] [bigint] NULL,
	[PointsAdded] [bigint] NULL,
	[PointsIncreased] [bigint] NULL,
	[PointsRedeemed] [bigint] NULL,
	[PointsReturned] [bigint] NULL,
	[PointsSubtracted] [bigint] NULL,
	[PointsDecreased] [bigint] NULL,
	[ExpPnts] [bigint] NULL,
	[Status] [varchar](10) NULL,
	[RewardsOnline] [char](1) NULL
) ON [PRIMARY]

GO

--SET ANSI_PADDING OFF
--GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchased]  DEFAULT ((0)) FOR [PointsPurchased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturned]  DEFAULT ((0)) FOR [PointsReturned]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_ExpPnts]  DEFAULT ((0)) FOR [ExpPnts]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_RewardsOnline]  DEFAULT ('N') FOR [RewardsOnline]
GO


