USE [400CapitalCU]
GO
/****** Object:  StoredProcedure [dbo].[spSetQuarterlyAuditFileName]    Script Date: 10/13/2009 10:24:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetQuarterlyAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='S400' + @currentdate + '.xls'
 
set @newname='O:\400\Output\QuarterlyFiles\Audit.bat ' + @filename
set @ConnectionString='O:\400\Output\QuarterlyFiles\' + @filename
GO
