USE [400CapitalCU]
GO
/****** Object:  StoredProcedure [dbo].[spDateControl]    Script Date: 10/13/2009 10:24:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDateControl]  @start_Date char(10), @end_date char(10)
AS

insert date_control (start_date, end_date)
values (@start_date, @end_date)
GO
