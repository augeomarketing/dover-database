USE [400CapitalCU]
GO
/****** Object:  StoredProcedure [dbo].[spExpiringPoints]    Script Date: 10/13/2009 10:24:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spExpiringPoints]  @EndDateParm varchar(10)
AS    

--  Uncomment  the code necessary to run within quiry analyzer

declare @enddate datetime
declare @expenddate datetime
declare @expbegindate datetime
declare @eligilblepnts numeric (9)
declare @workdate varchar(10)

set @workdate = '01/01/' + convert(varchar,(select pointexpirationyear from client))

--set @enddate = convert(datetime,'01/31/2008')

set @enddate = convert(datetime, @EndDateParm + ' 23:59:59:990' )

set @expenddate =  dateadd(year, -3,@enddate)
set @expbegindate =  convert(datetime, @workdate + ' 00:00:00:001')

delete expptstable

INSERT INTO ExpPtsTable(Tipnumber, DatexpiringStrt, DatexpiringEnd)
SELECT TIPNUMBER, @expbegindate as DatexpiringStrt,  @expenddate AS datexpiringend
from customer

update expptstable set PtsExpiring = (select sum(points*ratio) from history
 			where (histdate <= @expenddate) and
 			expptstable.tipnumber = history.tipnumber)

update expptstable set PtsExpiring = '0' where (PtsExpiring is null or PtsExpiring < '0')

UPDATE ExpPtsTable SET runredeemed = (SELECT runredeemed
                                      FROM customer
                                      WHERE customer.tipnumber = expptstable.tipnumber)

UPDATE  ExpPtsTable
SET PtsRetired = (PtsExpiring - runredeemed) where PtsExpiring > runredeemed
GO
