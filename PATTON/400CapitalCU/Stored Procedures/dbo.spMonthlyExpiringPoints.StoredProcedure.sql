USE [400CapitalCU]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyExpiringPoints]    Script Date: 01/04/2010 11:52:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
Alter PROCEDURE [dbo].[spMonthlyExpiringPoints]
AS

update Monthly_Statement_File set ExpPnts = POINTSTOEXPIRE
from Monthly_Statement_File m join EXPIRINGPOINTS e
on m.Tipnumber = e.tipnumber

GO