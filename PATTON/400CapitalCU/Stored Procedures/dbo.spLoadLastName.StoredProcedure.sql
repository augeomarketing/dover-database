USE [400CapitalCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadLastName]    Script Date: 12/18/2009 16:27:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[spLoadLastName]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Insert newlastname (acctid,tipnumber, acctname1)
		select acctid,tipnumber, acctname1 from roll_customer
		
	update newlastname set reversedname = reverse(rtrim(ltrim(acctname1)))
		
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastspace >= 0
	
	update newlastname set reversedname = reverse(left(acctname1, len(acctname1) -3))
	where lastname = 'JR' or lastname = 'SR' or lastname = 'II' 
	
	update newlastname set reversedname = reverse(left(acctname1, len(acctname1) -4))
	where lastname = 'INC' or lastname = 'III'  
	 
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	where lastspace <= 3
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where (lastname = 'JR' or lastname = 'SR' or lastname = 'II' or lastname = 'III')
		   	and lastspace > 0
	
	update newlastname set lastname = acctname1 where lastname = 'INC' or lastname = 'CO'
	
	update newlastname set lastname = reverse(reversedname) where lastspace <= 1
	
	/*  Load lastname into Input_Customer Table */
	
	update roll_customer set lastname = n.lastname 
	from newlastname n join roll_customer c on c.tipnumber = n.tipnumber

END
