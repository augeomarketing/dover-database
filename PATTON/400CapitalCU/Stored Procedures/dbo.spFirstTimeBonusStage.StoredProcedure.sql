USE [400CapitalCU]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  StoredProcedure [dbo].[spFirstTimeBonusStage]    Script Date: 12/27/2013 10:15:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFirstTimeBonusStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFirstTimeBonusStage]
GO

CREATE PROCEDURE [dbo].[spFirstTimeBonusStage]  @EndDateParm varchar(10)
AS 

/****************************************************************************/
/*                                                                                                              */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                                                              */
/*  JIRA CAPITALCU-14    
400/404 Capital Credit Union would also like to increase their First Use Bonus 
from 500 points to 1,000 points for purchases made starting on December 1. */
/****************************************************************************/

--declare @startdate datetime
declare @enddate datetime


--set @Startdate = convert(datetime, @StartDateparm  + ' 00:00:00:001')    
set @Enddate = convert(datetime, @EndDateparm +' 00:00:00:000' ) 

declare @BonusPoints numeric(9), @Tipnumber char(15), @Trandate datetime, @Acctid char(16), @ProcessFlag char(1)

set @Trandate=@EndDate
-- set @bonuspoints='500'
set @bonuspoints='1000'
	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber
from input_transactions 
where trancode in ('63', '33') 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag='N'		
		if not exists(select * from OneTimeBonuses_stage where tipnumber=@tipnumber AND trancode='BF')
		Begin 
			set @ProcessFlag='Y' 
		End 	  

		if @ProcessFlag='Y'
		Begin
 
			INSERT INTO input_transactions(TipNumber,TransDate,TranCode,TranCount,Points,Ratio,Description,SecID,Overage)
        		Values(@Tipnumber, @Trandate, 'BF', '1', @BonusPoints, '1', 'Bonus for First Trans Activity', 'NEW','0') 
 
			INSERT INTO OneTimeBonuses_stage(TipNumber,TranCode, DateAwarded)
        		Values(@Tipnumber, 'BF', @Trandate)

		End

		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO

