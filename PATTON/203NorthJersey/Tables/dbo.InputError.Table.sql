USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[InputError]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InputError](
	[CardNumber] [varchar](25) NOT NULL,
	[CustID] [varchar](10) NULL,
	[Name1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[Name3] [varchar](40) NULL,
	[Name4] [varchar](40) NULL,
	[Name5] [varchar](40) NULL,
	[Name6] [varchar](40) NULL,
	[PhoneNumber] [varchar](10) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[TranCount] [int] NULL,
	[Purchase] [money] NULL,
	[Address4] [varchar](40) NULL,
	[StatusCode] [varchar](1) NULL,
	[Tipnumber] [varchar](15) NULL,
	[lastname] [nvarchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
