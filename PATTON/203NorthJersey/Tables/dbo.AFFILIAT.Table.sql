USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](16) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[AcctTYPE] [varchar](20) NULL,
	[DATEADDED] [datetime] NULL,
	[PrimaryFlag] [varchar](10) NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
