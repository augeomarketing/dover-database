USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[duplicatecustid]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[duplicatecustid](
	[custid] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
