USE [203NorthJersey] 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[debitcard]    Script Date: 04/10/2012 15:51:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_CardMemberNumber]') AND type in (N'U'))
DROP TABLE [dbo].[Input_CardMemberNumber]

GO



CREATE TABLE [dbo].[Input_CardMemberNumber](
	[Col1] [varchar](max) NULL,
	[Col2] [varchar](max) NULL,
	[Col3] [varchar](max) NULL,
	[Col4] [varchar](max) NULL,
	[Col5] [varchar](max) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


