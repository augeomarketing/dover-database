USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[CustIDConversion]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustIDConversion](
	[Rowid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[AcctNum] [varchar](10) NULL,
	[CardNum] [varchar](16) NULL,
	[SSN] [varchar](10) NULL,
	[Tipnumber] [varchar](15) NULL,
	[TipNumber2] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
