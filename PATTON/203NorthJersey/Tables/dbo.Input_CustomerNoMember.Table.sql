USE [203NorthJersey] 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Users]    Script Date: 04/10/2012 15:49:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_CustomerNoMember]') AND type in (N'U'))
DROP TABLE [dbo].[Input_CustomerNoMember]
GO


CREATE TABLE [dbo].[Input_CustomerNoMember](
	[Col1] [varchar](max) NULL,
	[Col2] [varchar](max) NULL,
	[Col3] [varchar](max) NULL,
	[Col4] [varchar](max) NULL,
	[Col5] [varchar](max) NULL,
	[Col6] [varchar](max) NULL,
	[Col7] [varchar](max) NULL,
	[Col8] [varchar](max) NULL,
	[Col9] [varchar](max) NULL,
	[Col10] [varchar](max) NULL,
	[Col11] [varchar](max) NULL,
	[Col12] [varchar](max) NULL,
	[Col13] [varchar](max) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


