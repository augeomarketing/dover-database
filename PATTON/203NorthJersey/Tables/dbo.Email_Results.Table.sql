USE [203NorthJersey]
GO

/****** Object:  Table [dbo].[Email_Results]    Script Date: 07/13/2010 16:31:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Email_Results]') AND type in (N'U'))
DROP TABLE [dbo].[Email_Results]
GO

USE [203NorthJersey]
GO

/****** Object:  Table [dbo].[Email_Results]    Script Date: 07/13/2010 16:31:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Email_Results](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctName] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
 CONSTRAINT [PK_Results] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


