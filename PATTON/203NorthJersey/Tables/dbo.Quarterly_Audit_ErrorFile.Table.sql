USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchased]  DEFAULT (0) FOR [PointsPurchased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBonus]  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturned]  DEFAULT (0) FOR [PointsReturned]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_Currentend]  DEFAULT (0) FOR [Currentend]
GO
