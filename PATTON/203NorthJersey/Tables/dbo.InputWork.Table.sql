USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[InputWork]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InputWork](
	[CardNumber] [varchar](25) NOT NULL,
	[CustID] [varchar](10) NULL,
	[Name] [varchar](40) NULL,
	[PhoneNumber] [varchar](10) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[TranCount] [numeric](18, 0) NULL,
	[Purchase] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
