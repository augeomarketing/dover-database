USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[CUSTOMERWORK]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTOMERWORK](
	[CardNumber] [varchar](25) NULL,
	[Name] [varchar](40) NULL,
	[JointMbrName] [varchar](40) NULL,
	[NameFill] [varchar](160) NULL,
	[StatusCode] [char](1) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[address4] [varchar](40) NULL,
	[ZipCode] [varchar](15) NULL,
	[lastname] [char](40) NULL,
	[PhoneNumber] [varchar](10) NULL,
	[WorkTelephone] [varchar](10) NULL,
	[DateAdded] [datetime] NULL,
	[Open1] [varchar](90) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[CustID] [varchar](9) NULL,
	[Misc7] [varchar](8) NULL,
	[Points] [float] NULL,
	[Trancount] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
