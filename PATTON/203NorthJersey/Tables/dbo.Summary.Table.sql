USE [203NorthJersey]
GO

/****** Object:  Table [dbo].[Summary]    Script Date: 12/20/2010 10:38:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Summary]') AND type in (N'U'))
DROP TABLE [dbo].[Summary]
GO

USE [203NorthJersey]
GO

/****** Object:  Table [dbo].[Summary]    Script Date: 12/20/2010 10:38:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Summary](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TranDate] [nvarchar](10) NULL,
	[Total_Rows_Input] [decimal](10, 0) NULL,
	[New_Customers] [decimal](18, 0) NULL,
	[Input_Purchases] [decimal](18, 2) NULL,
	[Input_Returns] [decimal](18, 2) NULL,
	[Stmt_Purchases] [decimal](18, 2) NULL,
	[Stmt_Returns] [decimal](18, 2) NULL,
	[PointsExpired] [decimal] (18,0) Null 
) ON [PRIMARY]

GO


