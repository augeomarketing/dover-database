USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[cardnumber] [varchar](16) NULL,
	[ssn] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Purchase] [decimal](18, 2) NULL,
	[points] [decimal](18, 2) NULL,
	[trancnt] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
