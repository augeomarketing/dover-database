USE [203NorthJersey]
GO
/****** Object:  Table [dbo].[CustomerDeleted]    Script Date: 10/13/2009 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerDeleted](
	[TIPNumber] [varchar](15) NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [decimal](10, 0) NULL,
	[RunRedeemed] [decimal](10, 0) NULL,
	[RunAvailable] [decimal](10, 0) NULL,
	[AvlLstStm] [float] NULL,
	[AvlLstEom] [float] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[AcctID] [varchar](25) NULL,
	[DateAdded] [datetime] NULL,
	[Notes] [text] NULL,
	[ComboStmt] [char](1) NULL,
	[CardType] [varchar](20) NULL,
	[Username] [varchar](25) NULL,
	[Password] [varchar](10) NULL,
	[SecretQxn] [varchar](20) NULL,
	[SecretAnswer] [varchar](50) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmailAddr] [varchar](40) NULL,
	[EmailAddr2] [varchar](40) NULL,
	[EmailStmt] [char](1) NULL,
	[EmailEarnOther] [char](1) NULL,
	[RewardsOnlineName] [varchar](50) NULL,
	[Region] [varchar](25) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BusinessFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[CurrentEOMBalance] [float] NULL,
	[PaymentEOMBalance] [float] NULL,
	[FeeEOMBalance] [float] NULL,
	[CardIssueDate] [datetime] NULL,
	[CardActivitationDate] [datetime] NULL,
	[CardActivityLastDate] [datetime] NULL,
	[ShippingAddr] [varchar](50) NULL,
	[ShippingCity] [varchar](50) NULL,
	[ShippingState] [char](2) NULL,
	[ShippingZip] [varchar](50) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
