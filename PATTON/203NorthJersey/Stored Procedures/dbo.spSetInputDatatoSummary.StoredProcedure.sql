-- ================================================
-- Set Summary Input Data
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Dan Foster
-- Create date: 10/27/2009
-- Description:	Set Summary Input Data
-- =============================================
ALTER PROCEDURE spSetInputDatatoSummary @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    insert summary (input_purchases)
	select sum(purchase) from roll_customer where purchase > 0

	update summary set input_Returns = (select sum(purchase) from roll_customer where purchase < 0)
	WHERE rowid = (select max(rowid) from summary)

	update summary set trandate = @processdate WHERE rowid = (select max(rowid) from summary)
	
	update summary set total_rows_input = (select count(*) from roll_customer)
	
END
GO
