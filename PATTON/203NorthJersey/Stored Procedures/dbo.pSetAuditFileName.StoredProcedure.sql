USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[pSetAuditFileName]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pSetAuditFileName] @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='M203' + @currentdate + '.xls'
 
set @newname='O:\203\Output\AuditFiles\Audit.bat ' + @filename
set @ConnectionString='O:\203\Output\AuditFiles\' + @filename
GO
