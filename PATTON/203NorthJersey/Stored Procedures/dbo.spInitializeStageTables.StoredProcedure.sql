USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spInitializeStageTables]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from production tables to Stage tables.   */
/*  Tables:  
	Customer 	 - select 
	Affiliat		- select
	History		- select records from history where the histdate > month begin date
	OneTimeBonuses - select
	Customer_Stage - Truncate, Insert 
	Affiliat_Stage	- Truncate, Insert 
	History_Stage	- Truncate, Insert  
	OneTimeBonuses_Stage - Truncate, Insert

*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spInitializeStageTables] @TipFirst char(4), @MonthEnd char(20),  @spErrMsgr varchar(80) Output AS 

Declare  @DD char(2), @MM char(2), @YYYY char(4), @MonthBeg DateTime
Declare @dbName varchar(25)
Declare @SQLCmnd nvarchar(1000)
Declare @Row Int
set @DD = '01'
set @MM = Month(@MonthEnd)
set @YYYY = Year(@MonthEnd)
set @MonthBeg = convert(datetime, @MM+'/'+@DD+'/'+@YYYY+' 00:00:00:000' )	
Declare @MonthBegChar char(20)

set @MonthBegChar = Convert(char(20), @MonthBeg, 120)
print @MonthBegChar 

-- Get dbname from dbProcessInfo
Set @DbName = RTrim( (Select DbNamePatton from DBProcessInfo where dbNumber =Rtrim( @TipFirst)) )

set @spErrMsgr = 'no'

If @DbName is null 
Begin
	-- No Records Found
	set @spErrMsgr = 'Tip Not found in dbProcessInfo' 
	return -100 
End

-- Clear the Stage tables
set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName)   + N'.dbo.Customer_Stage' 
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName)   + N'.dbo.Affiliat_Stage'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName)   + N'.dbo.History_Stage'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Truncate table ' + QuoteName(@dbName)   + N'.dbo.OneTimeBonuses_Stage'
Exec sp_executeSql @SQLCmnd 

-- Load the stage tables
set @SQLCmnd = 'Insert into '+ QuoteName(@dbName) + N'.dbo.Customer_Stage Select * from '+QuoteName(@dbName) + N'.dbo.Customer'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Update '+ QuoteName(@dbName) + N'.dbo.Customer_Stage Set  RunAvaliableNew = 0 '
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName) + N'.dbo.Affiliat_Stage Select * from '+QuoteName(@dbName) + N'.dbo.Affiliat'
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName) + N'.dbo.History_stage Select * from '+QuoteName(@dbName) + N'.dbo.History where Histdate >= ''' + @MonthBegChar +''''
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Update '+ QuoteName(@dbName) + N'.dbo.History_stage set SecId = ''OLD'' '
Exec sp_executeSql @SQLCmnd 

set @SQLCmnd = 'Insert into '+ QuoteName(@dbName) + N'.dbo.OnetimeBonuses_Stage Select * from '+QuoteName(@dbName) + N'.dbo.OnetimeBonuses'
Exec sp_executeSql @SQLCmnd
GO
