USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[sp203LoadCustID]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp203LoadCustID]
AS 

update inputwork
set custid = b.custid
from inputwork a,affiliat b
where a.cardnumber = b.acctid and (a.custid is null or a.custid = ' ')

update inputwork
set custid = b.tin
from inputwork a,tinwork b
where a.cardnumber = b.cardnumber and (a.custid is null or a.custid = ' ')

insert inputerror
  select * from inputwork where custid is null or custid = ' '

delete inputwork where custid is null or custid = ' '
GO
