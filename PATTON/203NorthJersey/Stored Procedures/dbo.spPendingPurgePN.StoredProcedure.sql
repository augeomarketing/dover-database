USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
        
/* BY:  D Foster  */
/* DATE: 09/2009   */
/* REVISION: 0 */
/* pending purge procedure for FI's Using Points Now                          */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */

declare @tipnumber nvarchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge(tipnumber)
select tipnumber from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

update customer set status = 'C', statusdescription = 'Closed'
from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
where @purgedate >= (select max(histdate) from history
where c.tipnumber  = tipnumber)

delete from PendingPurge
    where tipnumber in (select tipnumber from customer where status = 'C')
GO
