USE [203NorthJersey]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[spScrub_Transactions]    Script Date: 04/10/2012 15:42:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_AssignMemberNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_AssignMemberNumbers]
GO

USE [203NorthJersey]
GO

/****** Object:  StoredProcedure [dbo].[spScrub_Transactions]    Script Date: 04/10/2012 15:42:24 ******/

CREATE PROCEDURE [dbo].[usp_AssignMemberNumbers]
 AS

----- Remove commas from col13 
Update Input_CustomerNoMember 
set Col13 = REPLACE ( col13, ',','') 
Where CHARINDEX(',',Col13) > 1 

-------------------- Roll'em up to one row. 

select 
Col1, Col2, Col3, Col4, Col5, Col6, Col7, Col8, Col9, Col10, Col11, sum(Convert( integer ,  Col12)) as col12, SUM(Convert( decimal(8,2) , col13))as col13
into #TRoll 
from Input_CustomerNoMember 
group by Col1, Col2, Col3, Col4, Col5, Col6, Col7, Col8, Col9, Col10, Col11

select * from #Troll
truncate table Input_CustomerNoMember 

insert into Input_CustomerNoMember 
select * from #Troll 

------------------------ update col1 
Update Input_CustomerNoMember set Col1 = null 

Update Input_CustomerNoMember 
set Col1 = dc.col2																		-----<<< JIRA NJFCU-20  was Col1=Col3
from Input_CustomerNoMember Us 
	join Input_CardMemberNumber dc 	on Us.Col2 = dc.Col1	-----<<< JIRA NJFCU-20 was Col2=Col2

--- Update Input_CustomerNoMember column1 by card#
Update Input_CustomerNoMember 
set Col1 = Custid 
from Input_CustomerNoMember us 
	Join [203NorthJersey].dbo.AFFILIAT af on us.Col2 = af.ACCTID 
where us.Col1 is null 

--- Update Input_CustomerNoMember column1 by member#
Update Input_CustomerNoMember 
set Col1 = Custid 
from Input_CustomerNoMember us 
	Join [203NorthJersey].dbo.AFFILIAT af on us.Col3 = af.ACCTID 
where us.Col1 is null 

Update Input_CustomerNoMember 
set Col3 = null 
where Col3 in ('00075','00010')

Update Input_CustomerNoMember 
set Col3 = Col1 
where Col1 is not null 

---- Update from the affiliat table on card number 
Update Input_CustomerNoMember 
set col3 = af.acctid 
from Input_CustomerNoMember join affiliat af  on Col2 = af.ACCTID 
where Col3 is null  

-------------------






