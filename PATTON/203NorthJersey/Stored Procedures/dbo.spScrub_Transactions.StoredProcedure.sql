USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spScrub_Transactions]    Script Date: 12/23/2009 12:19:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spScrub_Transactions]
 AS

INSERT INTO Input_Transaction(cardnumber, ssn, Tipnumber, Purchase, trancnt)
SELECT  CardNumber, CustID AS ssn, Tipnumber, Purchase, TranCount AS trancnt
FROM Roll_Customer

update input_transaction set points = round(( purchase/2),0)

update input_transaction set trancode = '67' where points >= '0'

update input_transaction set trancode = '37' where points <  '0'

-- update input_transaction set points = (-1 * points) where trancode = '37'

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (points is null )
	       
Delete from Input_Transaction
where (cardnumber is null or cardnumber = ' ') or
      (points is null  or points =  0)

--if (select count(*) from input_transaction_error) > '0'
 --  set @QCFlag1 = '1'