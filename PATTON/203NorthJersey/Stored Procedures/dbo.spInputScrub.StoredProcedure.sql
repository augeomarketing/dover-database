USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrub]  @enddate varchar(10)
as

Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error


--------------- Input Customer table

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (custid is null or custid = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (name1 is null or name1 = ' ') or (tipnumber is null or tipnumber = ' ')

delete from Input_customer 
where (custid is null or custid = ' ') or  
      (cardnumber is null or cardnumber = ' ') or
      (name1 is null or name1 = ' ') or (tipnumber is null or tipnumber = ' ')

update input_customer set phonenumber =  Left(Input_Customer.phonenumber,3) + substring(Input_Customer.phonenumber,5,3) + right(Input_Customer.phonenumber,4)

update   input_customer set  DateAdded =  (select  dateadded  FROM customer_stage
where input_customer.tipnumber = customer_stage.tipnumber)

update input_customer  set  DateAdded = CONVERT(datetime, @enddate)
where  (DateAdded is NULL) or dateadded = ' '

update input_customer set statuscode = 'A'

update input_customer set address4 = city  + ' ' + state
GO
