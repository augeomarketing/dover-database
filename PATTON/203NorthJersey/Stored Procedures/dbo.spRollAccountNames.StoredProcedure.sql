USE [203NorthJersey]
GO

/****** Object:  StoredProcedure [dbo].[spRollAccountNames]    Script Date: 03/21/2011 11:29:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollAccountNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRollAccountNames]
GO

USE [203NorthJersey]
GO

/****** Object:  StoredProcedure [dbo].[spRollAccountNames]    Script Date: 03/21/2011 11:29:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spRollAccountNames]  @enddate varchar(10)
AS
	      
INSERT INTO Input_Customer(tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Customer

UPDATE input_customer SET cardnumber = b.cardnumber,custid = b.custid, name1 = b.name1,
lastname = b.lastname,address1 = b.address1, address2 = b.address2, city = b.city,[state] = b.[state],
zipcode = b.zipcode,phonenumber = b.phonenumber,address4 = b.address4,statuscode = b.statuscode
FROM input_customer a, roll_customer b
WHERE a.tipnumber = b.tipnumber

update input_customer  set name2 =  b.name1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.name1)and 
(a.name2 is null or a.name2 = ' ') 

update input_customer  set name3 =  b.name1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and (a.name3 is null or a.name3 = ' ')

update input_customer  set name4 =  b.name1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and rtrim(b.name1) != rtrim(a.name3) and 
(a.name4 is null or a.name4 = ' ')

update input_customer  set name5 =  b.name1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and rtrim(b.name1) != rtrim(a.name3) and 
rtrim(b.name1) != rtrim(a.name4) and (a.name5 is null or a.name5 = ' ')

update input_customer  set name6 =  b.name1 
from input_customer a, roll_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.name1) and 
rtrim(b.name1) != rtrim(a.name2) and rtrim(b.name1) != rtrim(a.name3) and 
rtrim(b.name1) != rtrim(a.name4) and rtrim(b.name1) != rtrim(a.name5) and
(a.name6 is null or a.name6 = ' ')

update input_customer set DateAdded = (select  dateadded  FROM customer_stage
where input_customer.tipnumber = customer_stage.tipnumber)

update input_customer  set  DateAdded = CONVERT(datetime, @enddate)
where  (DateAdded is NULL) or dateadded = ' '

update input_customer set phonenumber =  Left(Input_Customer.phonenumber,3) + substring(Input_Customer.phonenumber,5,3) + right(Input_Customer.phonenumber,4)


GO


