USE [203NorthJersey]
GO

/****** Object:  StoredProcedure [dbo].[sp203CustomerScrub]    Script Date: 03/21/2011 13:59:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp203CustomerScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp203CustomerScrub]
GO

USE [203NorthJersey]
GO

/****** Object:  StoredProcedure [dbo].[sp203CustomerScrub]    Script Date: 03/21/2011 13:59:48 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp203CustomerScrub]
 AS
/******************************************************************************************************************/
-- Changes: 1) code introduced to remove any blank rows introduced through the input Excel spreadsheet 03/21/2011
--			2) Code added to handle accountname1 that contains only a single name assumably the lastname.  
/******************************************************************************************************************/

-- Truncate Table  Input_Customer_error table removed
Truncate Table Roll_CustomerError
Truncate Table Input_Transaction_error


--CHANGE (1)
/***********************************************/
/*	Remove any invalid lines from roll_customer  */
/***********************************************/
Insert into Roll_CustomerError 
	select * from roll_customer 
	where (cardnumber is null or LEN(cardnumber) <= 0) or
	      (name1 is null or name1 = ' ') or (ZipCode is null or LEN(zipcode) <= 1)

delete from Roll_Customer
where (cardnumber is null or LEN(cardnumber) <= 0) or
	      (name1 is null or name1 = ' ') or (ZipCode is null or LEN(zipcode) <= 1) 
	      
/*******************************************************************************/
/*   Clean primary account number to be 10 digits in length to restore any leading spaces*/
/*******************************************************************************/

UPDATE   roll_customer
SET custid = REPLICATE('0', 10 - LEN(RTRIM(custid))) + custid where len(custid) < 10

/***************************************************************************************/
/*   Create Lastname and reformat name1 from lastname, firstname to firstname, lastname*/
/***************************************************************************************/

update roll_customer set lastname = (LEFT(NAME1, CHARINDEX(' ', name1 ) -1)) 
where len(name1) > 1 and CHARINDEX(' ', name1) -1 > 0

--CHANGE (2)
update Roll_Customer set LastName = Name1
where LastName is null 

update roll_customer set name1 =
substring(name1,CHARINDEX(' ', left(name1, len(name1) -1) ), len(name1)) + ' ' + lastname
where lastname not in('LLC','INC')

--CHANGE (2)
update Roll_Customer set Name1 = LastName
where Name1 is null

update roll_customer set lastname = name1 where lastname = 'LLC' or lastname = 'INC'

UPDATE roll_customer SET address4 = RTRIM(City) + ' ' + State + ' ' + ltrim(rtrim(zipcode))

update roll_customer set statuscode = 'A'

/* remove punctuation */
update roll_customer
set [name1]=replace([name1],char(39), ' '), address1=replace(address1,char(39), ' '), address2=replace(address2,char(39), ' '),
address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update roll_customer
set [name1]=replace([name1],char(140), ' '), address1=replace(address1,char(140), ' '), address2=replace(address2,char(140), ' '),
address4=replace(address4,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update roll_customer
set [name1]=replace([name1],char(44), ' '), address1=replace(address1,char(44), ' '), address2=replace(address2,char(44), ' '),
address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update roll_customer
set [name1]=replace([name1],char(46), ' '), address1=replace(address1,char(46), ' '), address2=replace(address2,char(46), ' '),
address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')






GO


