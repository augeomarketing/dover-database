USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spSetDeletedCustomers]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetDeletedCustomers]   AS

update affiliat_stage set acctstatus = 'C' 
where acctid in(select distinct cardnumber from input_delete_accts)  

update customer_stage set status = 'D',statusdescription = 'Deletion Pending'
where (status = 'A') and (not exists (select * from affiliat_stage where tipnumber = customer_stage.tipnumber and
(acctstatus = 'A' or acctstatus is null))) 

update customer_stage set status = 'A',statusdescription = 'Active [A]'
where (status = 'D') and (exists (select * from affiliat_stage where tipnumber = customer_stage.tipnumber and
(acctstatus = 'A' or acctstatus is null))) 

insert pending_purge_accts (tipnumber)
select tipnumber from customer 
where tipnumber not in(select tipnumber from pending_purge_accts) and status = 'D'

update pending_purge_accts set cyclenumber = cyclenumber + 1

update customer_stage set status = 'P',statusdescription = 'Pending Deletion'
where status = 'D' and tipnumber in(select tipnumber from pending_purge_accts where cyclenumber >= 3)

delete pending_purge_accts where cyclenumber >= 3 or  exists(select tipnumber from customer_stage where status = 'A'and
pending_purge_accts.tipnumber = customer_stage.tipnumber)
GO
