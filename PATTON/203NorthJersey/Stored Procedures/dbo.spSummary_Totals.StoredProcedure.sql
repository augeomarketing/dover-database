USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spSummary_Totals]    Script Date: 10/28/2009 13:34:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[spSummary_Totals]  @monthend varchar(10)
 AS


update summary set new_customers = (select count(*) from customer_stage
where dateadded = @monthend) where  rowid = (select max(rowid)from summary)

update summary set stmt_purchases = (select sum(pointspurchased) from monthly_statement_file
 where  trandate = @monthend ) where  rowid = (select max(rowid)from summary)

update summary set stmt_returns = (select sum(pointsreturned) from monthly_statement_file
 where  trandate = @monthend ) where  rowid = (select max(rowid)from summary)
 
update Summary set pointsexpired = (select SUM(pointsexpire) from monthly_statement_file
 where  trandate = @monthend ) where  rowid = (select max(rowid)from summary)


go