USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 12/24/2009 11:02:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  10/28/2009 */
/* Author:  D Foster */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

ALTER PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 

Insert Into Affiliat_Stage(AcctID,tipnumber)
select  distinct(cardnumber),tipnumber from roll_Customer  
where cardnumber not in ( Select acctid from Affiliat_Stage) order by tipnumber

/************ Insert New Accounts into Affiliat Stage  ***********/
update Affiliat_Stage set AcctType = 'Debit', DateAdded = @monthend,  secid = right(c.custid,4),
AcctStatus = c.Statuscode, AcctTypeDesc = 'Debit Card', LastName =  c.LastName, YTDEarned = 0,
CustID =c.custid
from affiliat_stage a join roll_Customer c 
on a.tipnumber = c.tipnumber where dateadded is null

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set Accttypedesc = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType