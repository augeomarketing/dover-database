USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spInsertHistory]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spInsertHistory] 
as
/****************************************************************************/
/*                                                                          */
/* Procedure to insert monthly transactions based on the input transactions */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

	  insert history (tipnumber,acctid,histdate,trancode,trancount,points,[description],secid,ratio,overage)
	       select tipnumber,cardnumber as acctid,transdate as histdate,trancode,trancount,points,[description],secid,ratio,overage from transactions

	update 	cus
		set runavailable = dbo.fnSumPointsByTip(tipnumber),
		runbalance = dbo.fnSumPointsByTip(tipnumber) + runredeemed
	from dbo.customer cus 
	where cus.tipnumber in (select distinct tipnumber from dbo.transactions)
GO
