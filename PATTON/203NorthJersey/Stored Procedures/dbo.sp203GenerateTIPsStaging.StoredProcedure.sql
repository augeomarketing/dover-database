USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[sp203GenerateTIPsStaging]    Script Date: 12/24/2009 10:25:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp203GenerateTIPsStaging]
AS 

declare @LastTipUsed char(15)
declare @newnum bigint

update Roll_Customer
set Tipnumber = a.tipnumber
from Roll_Customer r join AFFILIAT_Stage a on r.cardnumber = a.ACCTID 
where (r.tipnumber is null or r.tipnumber = ' ')

update Roll_Customer
set Tipnumber = a.tipnumber
from Roll_Customer r join AFFILIAT_Stage a on r.custid = a.custid
where (r.tipnumber is null) and (r.custid is not null)

truncate table GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer where custid is not null and Tipnumber is null

	/*    Create new tip          */

exec rewardsnow.dbo.spGetLastTipNumberUsed 203, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 203000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 203, @newnum

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ') 

truncate table GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber as acctid, tipnumber	
from roll_customer
where cardnumber is not null and Tipnumber is null

	/*    Create new tip          */
	
exec rewardsnow.dbo.spGetLastTipNumberUsed 203, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 203000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 203, @newnum

update  roll_customer
set Tipnumber = b.tipnumber
from  roll_customer a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

