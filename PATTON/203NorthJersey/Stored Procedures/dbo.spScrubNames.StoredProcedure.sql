USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spScrubNames]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spScrubNames]
AS

/* load last name from lstnamework table */

UPDATE    roll_customer SET lastname = b.lastname
FROM roll_customer a, lstnamework b
WHERE a.tipnumber = b.tipnumber

/* remove punctuation */
update roll_customer
set [name1]=replace([name1],char(39), ' '), address1=replace(address1,char(39), ' '), address2=replace(address2,char(39), ' '),
address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update roll_customer
set [name1]=replace([name1],char(140), ' '), address1=replace(address1,char(140), ' '), address2=replace(address2,char(140), ' '),
address4=replace(address4,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update roll_customer
set [name1]=replace([name1],char(44), ' '), address1=replace(address1,char(44), ' '), address2=replace(address2,char(44), ' '),
address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update roll_customer
set [name1]=replace([name1],char(46), ' '), address1=replace(address1,char(46), ' '), address2=replace(address2,char(46), ' '),
address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

UPDATE Roll_CUSTOMER SET [NAME1] = Upper(RTRIM(SUBSTRING([NAME1], CHARINDEX(' ', [NAME1]) + 1, LEN(RTRIM([NAME1])))) + ' ' + SUBSTRING([NAME1], 1, CHARINDEX(' ',[NAME1]) - 1)) WHERE     (SUBSTRING([NAME1], 1, 1) NOT LIKE ' ') AND ([NAME1] IS NOT NULL) AND ([NAME1] LIKE '% %')

update roll_customer set [name1] = ltrim([name1]), [name2] = ltrim([name2]), [name3] = ltrim([name3]), [name4] = ltrim([name4]), [name5] = ltrim([name5]), [name6] = ltrim([name6])
GO
