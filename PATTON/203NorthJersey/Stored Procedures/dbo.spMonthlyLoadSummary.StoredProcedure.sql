USE [203NorthJersey]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyLoadSummary]    Script Date: 10/13/2009 10:19:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyLoadSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table MonthlySummary

insert MonthlySummary (dateadded)
values (@enddate)

update  MonthlySummary set purchases = (select SUM(CONVERT(int, points)) FROM history WHERE trancode = '67'  and histdate = @enddate)

update   MonthlySummary set returned =(select SUM(CONVERT(int, points)) FROM history WHERE trancode = '37' and histdate =  @enddate)

update MonthlySummary set bonuses = (select sum(convert(int,points)) from history where histdate = @enddate and trancode like 'B%')
GO
