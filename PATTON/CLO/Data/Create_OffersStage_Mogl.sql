USE [CLO]
GO

/****** Object:  Table [dbo].[OffersStage_Mogl]    Script Date: 08/22/2016 13:52:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OffersStage_Mogl](
	[sid_MoglOffers_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_MoglOffers_OfferId] [bigint] NOT NULL,
	[dim_MoglOffers_OfferName] [varchar](50) NULL,
	[dim_MoglOffers_OfferDesc] [varchar](255) NULL,
	[dim_MoglOffers_Discount] [varchar](50) NULL,
	[dim_MoglOffers_DiscountValue] [decimal](16, 2) NULL,
	[dim_MoglOffers_ActiveFrom] [datetime] NULL,
	[dim_MoglOffers_ActiveTo] [datetime] NULL,
	[dim_MoglOffers_Image] [varchar](255) NULL,
	[dim_MoglOffers_CategoryId] [varchar](255) NULL,
	[dim_MoglOffers_MerchantName] [varchar](100) NULL,
	[dim_MoglOffers_MerchantDesc] [varchar](4000) NULL,
	[dim_MoglOffers_MerchantId] [varchar](255) NULL,
	[dim_MoglOffers_MerchantAddress] [varchar](255) NULL,
	[dim_MoglOffers_MerchantCity] [varchar](50) NULL,
	[dim_MoglOffers_MerchantState] [varchar](15) NULL,
	[dim_MoglOffers_MerchantZipCode] [varchar](10) NULL,
	[dim_MoglOffers_MerchantPhone] [varchar](50) NULL,
	[dim_MoglOffers_MerchantEmail] [varchar](50) NULL,
	[dim_MoglOffers_MerchantWebsite] [varchar](255) NULL,
	[dim_MoglOffers_GeoLat] [decimal](16, 2) NULL,
	[dim_MoglOffers_GeoLong] [decimal](16, 2) NULL,
	[dim_MoglOffers_Status] [varchar](20) NULL,
	[dim_MoglOffers_AcceptedCards] [varchar](30) NULL,
	[dim_MoglOffers_HasBeenLoaded] [bit] NULL,
 CONSTRAINT [PK_OffersStage_Mogl] PRIMARY KEY CLUSTERED 
(
	[sid_MoglOffers_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


