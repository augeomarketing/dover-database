USE [CLO]
GO

/****** Object:  Table [dbo].[OfferCategories]    Script Date: 08/29/2016 10:17:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

--== create the table ==============================================
CREATE TABLE [dbo].[OfferCategories](
	[sid_OfferCategories_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_OfferCategories_CategoryId] [int] NOT NULL,
	[dim_OfferCategories_Category] [varchar](50) NOT NULL,
 CONSTRAINT [PK_OfferCategories] PRIMARY KEY CLUSTERED 
(
	[sid_OfferCategories_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--== fill the table with the normal categories =====================
insert into dbo.OfferCategories (dim_OfferCategories_CategoryId, dim_OfferCategories_Category) 
values 
(4, 'AUTOMOTIVE SALES & SERVICES'), 
(33, 'BEAUTY, SPAS & SKIN CARE'), 
(47, 'BUSINESS SERVICES'), 
(62, 'COMPUTERS, TV''s & ELECTRONICS'), 
(77, 'CONSTRUCTION & REMODELING'), 
(86, 'EDUCATION'), 
(105, 'ENTERTAINMENT & ARTS'), 
(120, 'EVENT PLANNING & SERVICES'), 
(130, 'FINANCIAL SERVICES'), 
(136, 'FITNESS & INSTRUCTION'), 
(143, 'FOOD & BEVERAGE'), 
(675, 'General'), 
(155, 'HEALTH & MEDICAL'), 
(444, 'HOME APPLIANCES'), 
(184, 'HOME IMPROVEMENTS & REPAIRS'), 
(209, 'HOUSEHOLD & PERSONAL SERVICES'), 
(205, 'LEGAL SERVICES'), 
(485, 'PETS & ANIMALS'), 
(245, 'RECREATION & SPORTING GOODS'), 
(266, 'RESTAURANTS'), 
(345, 'RETAIL SHOPPING'), 
(648, 'TATTOO & BODY PIERCINGS'), 
(383, 'TRAVEL, TRANSPORTATION & LODGING');
go



SET ANSI_PADDING OFF
GO

