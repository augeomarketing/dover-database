USE [CLO]
GO

/****** Object:  Table [dbo].[OfferCategoryRelation]    Script Date: 08/29/2016 10:15:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OfferCategoryRelation](
	[sid_Offer_ID] [bigint] NOT NULL,
	[sid_OfferCategories_Id] [bigint] NOT NULL
) ON [PRIMARY]

GO


