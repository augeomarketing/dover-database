USE [CLO]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Offer_Vendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Offer]'))
ALTER TABLE [dbo].[Offer] DROP CONSTRAINT [FK_Offer_Vendor]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[Offer]    Script Date: 10/13/2016 14:18:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Offer]') AND type in (N'U'))
DROP TABLE [dbo].[Offer]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[Offer]    Script Date: 10/13/2016 14:18:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Offer](
	[sid_Offer_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_Offer_IdOffer] [bigint] NULL,
	[sid_Offer_Location_Id] [int] NULL,
	[sid_Vendor_Vendor_Id] [bigint] NULL,
	[dim_Offer_Name] [varchar](50) NULL,
	[dim_Offer_Description] [varchar](255) NOT NULL,
	[dim_Offer_OfferType] [varchar](80) NULL,
	[sid_Offer_Merchant_Id] [varchar](255) NOT NULL,
	[dim_Offer_MerchantName] [varchar](100) NULL,
	[dim_Offer_Image] [varchar](255) NULL,
	[dim_Offer_Discount] [varchar](50) NULL,
	[dim_Offer_DiscountValue] [decimal](16, 2) NULL,
	[dim_Offer_ActiveFrom] [datetime] NULL,
	[dim_Offer_ActiveTo] [datetime] NULL,
	[dim_Offer_MerchantDescription] [varchar](4000) NULL,
	[dim_Offer_GeoLat] [decimal](16, 2) NULL,
	[dim_Offer_GeoLong] [decimal](16, 2) NULL,
	[dim_Offer_Address] [varchar](255) NULL,
	[dim_Offer_City] [varchar](50) NULL,
	[dim_Offer_State] [varchar](15) NULL,
	[dim_Offer_zip] [varchar](10) NULL,
	[dim_Offer_Email] [varchar](50) NULL,
	[dim_Offer_Phone] [varchar](50) NULL,
	[dim_Offer_Website] [varchar](255) NULL,
	[dim_Offer_Status] [varchar](20) NULL,
	[dim_Offer_Cap] [int] NULL,
	[dim_Offer_DollarCap] [varchar](20) NULL,
	[dim_Offer_AcceptedCards] [varchar](30) NULL,
 CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED 
(
	[sid_Offer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the offer from the vendor (Note: may not be unique from one vendor to the next)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Offer', @level2type=N'COLUMN',@level2name=N'sid_Offer_IdOffer'
GO

ALTER TABLE [dbo].[Offer]  WITH CHECK ADD  CONSTRAINT [FK_Offer_Vendor] FOREIGN KEY([sid_Vendor_Vendor_Id])
REFERENCES [dbo].[Vendor] ([sid_Vendor_ID])
GO

ALTER TABLE [dbo].[Offer] CHECK CONSTRAINT [FK_Offer_Vendor]
GO


