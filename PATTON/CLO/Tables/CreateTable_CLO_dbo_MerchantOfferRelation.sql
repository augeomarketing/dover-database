USE [CLO]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MerchantOfferRelation_Merchant]') AND parent_object_id = OBJECT_ID(N'[dbo].[MerchantOfferRelation]'))
ALTER TABLE [dbo].[MerchantOfferRelation] DROP CONSTRAINT [FK_MerchantOfferRelation_Merchant]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MerchantOfferRelation_Offer]') AND parent_object_id = OBJECT_ID(N'[dbo].[MerchantOfferRelation]'))
ALTER TABLE [dbo].[MerchantOfferRelation] DROP CONSTRAINT [FK_MerchantOfferRelation_Offer]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[MerchantOfferRelation]    Script Date: 10/13/2016 14:26:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantOfferRelation]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantOfferRelation]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[MerchantOfferRelation]    Script Date: 10/13/2016 14:26:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantOfferRelation](
	[sid_MerchantOfferRelation_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_Merchant_ID] [bigint] NOT NULL,
	[sid_Offer_ID] [bigint] NOT NULL,
 CONSTRAINT [PK_MerchantOfferRelation] PRIMARY KEY CLUSTERED 
(
	[sid_MerchantOfferRelation_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MerchantOfferRelation]  WITH CHECK ADD  CONSTRAINT [FK_MerchantOfferRelation_Merchant] FOREIGN KEY([sid_Merchant_ID])
REFERENCES [dbo].[Merchant] ([sid_Merchant_ID])
GO

ALTER TABLE [dbo].[MerchantOfferRelation] CHECK CONSTRAINT [FK_MerchantOfferRelation_Merchant]
GO

ALTER TABLE [dbo].[MerchantOfferRelation]  WITH CHECK ADD  CONSTRAINT [FK_MerchantOfferRelation_Offer] FOREIGN KEY([sid_Offer_ID])
REFERENCES [dbo].[Offer] ([sid_Offer_ID])
GO

ALTER TABLE [dbo].[MerchantOfferRelation] CHECK CONSTRAINT [FK_MerchantOfferRelation_Offer]
GO


