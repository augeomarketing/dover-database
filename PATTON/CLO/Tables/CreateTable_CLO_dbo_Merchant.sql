USE [CLO]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Merchant_Vendor]') AND parent_object_id = OBJECT_ID(N'[dbo].[Merchant]'))
ALTER TABLE [dbo].[Merchant] DROP CONSTRAINT [FK_Merchant_Vendor]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Merchant__dim_En__592635D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Merchant] DROP CONSTRAINT [DF__Merchant__dim_En__592635D8]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Merchant__dim_En__5A1A5A11]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Merchant] DROP CONSTRAINT [DF__Merchant__dim_En__5A1A5A11]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Merchant_dim_Merchant_Status]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Merchant] DROP CONSTRAINT [DF_Merchant_dim_Merchant_Status]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Merchant_dim_Merchant_IsOptIn]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Merchant] DROP CONSTRAINT [DF_Merchant_dim_Merchant_IsOptIn]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Merchant_dim_Merchant_IsOnlineMid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Merchant] DROP CONSTRAINT [DF_Merchant_dim_Merchant_IsOnlineMid]
END

GO

USE [CLO]
GO

/****** Object:  Table [dbo].[Merchant]    Script Date: 10/13/2016 14:24:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Merchant]') AND type in (N'U'))
DROP TABLE [dbo].[Merchant]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[Merchant]    Script Date: 10/13/2016 14:24:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Merchant](
	[sid_Merchant_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_Merchant_MerchantName] [varchar](50) NOT NULL,
	[dim_Merchant_MerchantId] [varchar](20) NULL,
	[sid_Vendor_Id] [bigint] NOT NULL,
	[dim_EnrollmentType_Open] [bit] NOT NULL,
	[dim_EnrollmentType_Closed] [bit] NOT NULL,
	[dim_Merchant_Status] [varchar](10) NULL,
	[dim_Merchant_IsOptIn] [bit] NULL,
	[dim_Merchant_IsOnlineMid] [bit] NULL,
	[dim_Merchant_ActiveFrom] [datetime] NULL,
	[dim_Merchant_ActiveTo] [datetime] NULL,
 CONSTRAINT [PK_Merchant] PRIMARY KEY CLUSTERED 
(
	[sid_Merchant_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Merchant]  WITH CHECK ADD  CONSTRAINT [FK_Merchant_Vendor] FOREIGN KEY([sid_Vendor_Id])
REFERENCES [dbo].[Vendor] ([sid_Vendor_ID])
GO

ALTER TABLE [dbo].[Merchant] CHECK CONSTRAINT [FK_Merchant_Vendor]
GO

ALTER TABLE [dbo].[Merchant] ADD  CONSTRAINT [DF__Merchant__dim_En__592635D8]  DEFAULT ((0)) FOR [dim_EnrollmentType_Open]
GO

ALTER TABLE [dbo].[Merchant] ADD  CONSTRAINT [DF__Merchant__dim_En__5A1A5A11]  DEFAULT ((1)) FOR [dim_EnrollmentType_Closed]
GO

ALTER TABLE [dbo].[Merchant] ADD  CONSTRAINT [DF_Merchant_dim_Merchant_Status]  DEFAULT ('active') FOR [dim_Merchant_Status]
GO

ALTER TABLE [dbo].[Merchant] ADD  CONSTRAINT [DF_Merchant_dim_Merchant_IsOptIn]  DEFAULT ((0)) FOR [dim_Merchant_IsOptIn]
GO

ALTER TABLE [dbo].[Merchant] ADD  CONSTRAINT [DF_Merchant_dim_Merchant_IsOnlineMid]  DEFAULT ((0)) FOR [dim_Merchant_IsOnlineMid]
GO


