USE [CLO]
GO

/****** Object:  Table [dbo].[Offer]    Script Date: 08/29/2016 10:16:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Offer](
	[sid_Offer_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_Offer_IdOffer] [bigint] NULL,
	[sid_Offer_Location_Id] [int] NULL,
	[sid_Vendor_Vendor_Id] [int] NULL,
	[dim_Offer_Name] [varchar](50) NULL,
	[dim_Offer_Description] [varchar](255) NOT NULL,
	[dim_Offer_OfferType] [varchar](80) NULL,
	[sid_Offer_Merchant_Id] [varchar](255) NOT NULL,
	[dim_Offer_MerchantName] [varchar](100) NULL,
	[dim_Offer_Image] [varchar](255) NULL,
	[dim_Offer_Discount] [varchar](50) NULL,
	[dim_Offer_DiscountValue] [decimal](16, 2) NULL,
	[dim_Offer_ActiveFrom] [datetime] NULL,
	[dim_Offer_ActiveTo] [datetime] NULL,
	[dim_Offer_MerchantDescription] [varchar](4000) NULL,
	[dim_Offer_GeoLat] [decimal](16, 2) NULL,
	[dim_Offer_GeoLong] [decimal](16, 2) NULL,
	[dim_Offer_Address] [varchar](255) NULL,
	[dim_Offer_City] [varchar](50) NULL,
	[dim_Offer_State] [varchar](15) NULL,
	[dim_Offer_zip] [varchar](10) NULL,
	[dim_Offer_Email] [varchar](50) NULL,
	[dim_Offer_Phone] [varchar](50) NULL,
	[dim_Offer_Website] [varchar](255) NULL,
	[dim_Offer_Status] [varchar](20) NULL,
	[dim_Offer_Cap] [int] NULL,
	[dim_Offer_DollarCap] [varchar](20) NULL,
	[dim_Offer_AcceptedCards] [varchar](30) NULL,
 CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED 
(
	[sid_Offer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The ID of the offer from the vendor (Note: may not be unique from one vendor to the next)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Offer', @level2type=N'COLUMN',@level2name=N'sid_Offer_IdOffer'
GO


