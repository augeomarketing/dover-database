USE [CLO]
GO

/****** Object:  Table [dbo].[MerchantOfferRelationStage]    Script Date: 10/13/2016 14:27:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantOfferRelationStage]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantOfferRelationStage]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[MerchantOfferRelationStage]    Script Date: 10/13/2016 14:27:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MerchantOfferRelationStage](
	[dim_Merchant_MerchantId] [varchar](20) NOT NULL,
	[sid_Offer_IdOffer] [bigint] NOT NULL,
	[sid_Vendor_ID] [bigint] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


