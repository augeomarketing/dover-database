USE [CLO]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MerchantStage_Mogl_EnrollTypeFlag_Open]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantStage_Mogl] DROP CONSTRAINT [DF_MerchantStage_Mogl_EnrollTypeFlag_Open]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MerchantStage_Mogl_EnrollTypeFlag_Closed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantStage_Mogl] DROP CONSTRAINT [DF_MerchantStage_Mogl_EnrollTypeFlag_Closed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MerchantStage_Mogl_MerchantStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantStage_Mogl] DROP CONSTRAINT [DF_MerchantStage_Mogl_MerchantStatus]
END

GO

USE [CLO]
GO

/****** Object:  Table [dbo].[MerchantStage_Mogl]    Script Date: 10/19/2016 13:02:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantStage_Mogl]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantStage_Mogl]
GO

USE [CLO]
GO

/****** Object:  Table [dbo].[MerchantStage_Mogl]    Script Date: 10/19/2016 13:02:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MerchantStage_Mogl](
	[StageID] [int] IDENTITY(1,1) NOT NULL,
	[MerchantName] [varchar](50) NULL,
	[MerchantID] [varchar](20) NOT NULL,
	[VendorID] [bigint] NOT NULL,
	[EnrollTypeFlag_Open] [bit] NOT NULL,
	[EnrollTypeFlag_Closed] [bit] NOT NULL,
	[MerchantStatus] [varchar](10) NOT NULL,
	[DateFromSourceFile] [datetime] NULL,
 CONSTRAINT [PK_MerchantStage_Mogl] PRIMARY KEY CLUSTERED 
(
	[StageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[MerchantStage_Mogl] ADD  CONSTRAINT [DF_MerchantStage_Mogl_EnrollTypeFlag_Open]  DEFAULT ((1)) FOR [EnrollTypeFlag_Open]
GO

ALTER TABLE [dbo].[MerchantStage_Mogl] ADD  CONSTRAINT [DF_MerchantStage_Mogl_EnrollTypeFlag_Closed]  DEFAULT ((1)) FOR [EnrollTypeFlag_Closed]
GO

ALTER TABLE [dbo].[MerchantStage_Mogl] ADD  CONSTRAINT [DF_MerchantStage_Mogl_MerchantStatus]  DEFAULT ('ACTIVE') FOR [MerchantStatus]
GO


