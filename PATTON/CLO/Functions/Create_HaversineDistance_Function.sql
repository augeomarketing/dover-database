USE [CLO]
GO

/****** Object:  UserDefinedFunction [dbo].[HaversineDistance]    Script Date: 08/29/2016 10:29:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE function [dbo].[HaversineDistance]
(
	@Latitude1  float,
	@Longitude1 float,
	@Latitude2  float,
	@Longitude2 float
)
returns float
as
begin
	declare @radius float

	declare @lon  float
	declare @lat  float

	declare @a float
	declare @distance float

	-- Sets average radius of Earth in Miles
	set @radius = 3960.0E

	-- Convert degrees to radians
	set @lon = radians( @Longitude2 - @Longitude1 )
	set @lat = radians( @Latitude2 - @Latitude1 )

	set @a = (sin(@lat/2.0E) * sin(@lat/2.0E)) + cos(radians(@Latitude1)) * cos(radians(@Latitude2)) * sin(@lon / 2.0E) * sin(@lon / 2.0E)
	
	set @distance =	@radius * (2.0E * asin(case when 1.0E < @a then 1.0E else sqrt(@a) end ))

	return @distance -- distance in miles

end


GO


