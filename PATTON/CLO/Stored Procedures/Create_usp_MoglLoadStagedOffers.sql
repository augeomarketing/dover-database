USE [CLO]
GO

/****** Object:  StoredProcedure [dbo].[usp_MoglLoadStagedOffers]    Script Date: 09/12/2016 14:53:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Tipa
-- Create date: 22 Aug 2016
-- Description:	Uses the data on the Mogl staging 
-- table to update the Mogl offers on the main 
-- offer table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_MoglLoadStagedOffers] 
AS
BEGIN

	SET NOCOUNT ON;
	
	declare @UnprocessedCount as int;
	declare @MoglVendorID as int;
	declare @RestaurantCategoryID as int;

    --== see if there's anything to update or insert ==========================
    select @UnprocessedCount = COUNT(osm.sid_MoglOffers_Id) 
    from dbo.OffersStage_Mogl osm 
    where osm.dim_MoglOffers_HasBeenLoaded is null 
       or osm.dim_MoglOffers_HasBeenLoaded = 0;
    
    print 'there are ' + cast(@UnprocessedCount as varchar(10)) + ' offers to process';
    
    --== get the right vendor ID =============================================
    select @MoglVendorID = v.sid_Vendor_ID 
    from dbo.Vendor v 
    where v.dim_Vendor_VendorName = 'Mogl';
    
    print 'Mogl vendor ID: ' + cast(@MoglVendorID as varchar(20));
    
    --== get the right category ID for restaurants ===========================
    select @RestaurantCategoryID = oc.sid_OfferCategories_Id 
    from OfferCategories oc 
    where oc.dim_OfferCategories_CategoryId = 266; --current Code for restaurants.
    
    
    --== if we have anything to work on... ===================================
    if @UnprocessedCount > 0
    begin
    
    --== update the Offer table so that missing Mogl offers are inactive =============
    
		update dbo.Offer 
		   set dim_Offer_Status = 'INACTIVE' 
		 where sid_Vendor_Vendor_Id = @MoglVendorID 
		   and sid_Offer_IdOffer not in (
		       select dim_MoglOffers_OfferId from dbo.OffersStage_Mogl 
		       where dim_MoglOffers_HasBeenLoaded is null 
		          or dim_MoglOffers_HasBeenLoaded = 0
		   );
    
    
    --== try the update of existing offers on the Offer table =================
		update dbo.Offer 
		   set Offer.dim_Offer_AcceptedCards = osm.dim_MoglOffers_AcceptedCards, 
		       --Offer.dim_Offer_ActiveFrom = osm.dim_MoglOffers_ActiveFrom, 
		       --Offer.dim_Offer_ActiveTo = osm.dim_MoglOffers_ActiveTo, 
		       Offer.dim_Offer_ActiveTo = case 
		                                      when osm.dim_MoglOffers_Status = 'INACTIVE' 
		                                       and Offer.dim_Offer_Status = 'ACTIVE' 
		                                      then SYSDATETIME() 
		                                      else Offer.dim_Offer_ActiveTo
		                                  end,
		       Offer.dim_Offer_Address = osm.dim_MoglOffers_MerchantAddress, 
		       Offer.dim_Offer_Cap = null,  
		       Offer.dim_Offer_City = osm.dim_MoglOffers_MerchantCity, 
		       Offer.dim_Offer_Description = osm.dim_MoglOffers_OfferDesc, 
		       Offer.dim_Offer_Discount = osm.dim_MoglOffers_Discount, 
		       Offer.dim_Offer_DiscountValue = osm.dim_MoglOffers_DiscountValue, 
		       Offer.dim_Offer_DollarCap = null, 
		       Offer.dim_Offer_Email = osm.dim_MoglOffers_MerchantEmail, 
		       Offer.dim_Offer_GeoLat = osm.dim_MoglOffers_GeoLat, 
		       Offer.dim_Offer_GeoLong = osm.dim_MoglOffers_GeoLong, 
		       Offer.dim_Offer_Image = osm.dim_MoglOffers_Image, 
		       Offer.dim_Offer_MerchantDescription = osm.dim_MoglOffers_MerchantDesc, 
		       Offer.dim_Offer_MerchantName = osm.dim_MoglOffers_MerchantName, 
		       Offer.dim_Offer_Name = osm.dim_MoglOffers_OfferName, 
		       Offer.dim_Offer_OfferType = null, 
		       Offer.dim_Offer_Phone = osm.dim_MoglOffers_MerchantPhone, 
		       Offer.dim_Offer_State = osm.dim_MoglOffers_MerchantState, 
		       Offer.dim_Offer_Status = osm.dim_MoglOffers_Status, 
		       Offer.dim_Offer_Website = osm.dim_MoglOffers_MerchantWebsite, 
		       Offer.dim_Offer_zip = osm.dim_MoglOffers_MerchantZipCode, 
		       Offer.sid_Offer_Location_Id = 0, 
		       Offer.sid_Offer_Merchant_Id = osm.dim_MoglOffers_MerchantId 
		       
		  from dbo.OffersStage_Mogl osm, dbo.Offer 
		 where Offer.sid_Vendor_Vendor_Id = @MoglVendorID 
		   and Offer.sid_Offer_IdOffer = osm.dim_MoglOffers_OfferId 
		   and (osm.dim_MoglOffers_HasBeenLoaded is null 
			or osm.dim_MoglOffers_HasBeenLoaded = 0);

	--== try the insert of new offers into the Offer table ====================
		insert into dbo.Offer (
			sid_Offer_IdOffer, sid_Offer_Location_Id, sid_Vendor_Vendor_Id, 
			dim_Offer_AcceptedCards, dim_Offer_ActiveFrom, dim_Offer_ActiveTo, 
			dim_Offer_Address, dim_Offer_Cap, 
			dim_Offer_City, dim_Offer_Description, dim_Offer_Discount, 
			dim_Offer_DiscountValue, dim_Offer_DollarCap, dim_Offer_Email, 
			dim_Offer_GeoLat, dim_Offer_GeoLong, dim_Offer_Image, 
			dim_Offer_MerchantDescription, dim_Offer_MerchantName, dim_Offer_Name, 
			dim_Offer_OfferType, dim_Offer_Phone, dim_Offer_State, 
			dim_Offer_Status, dim_Offer_Website, dim_Offer_zip, 
			sid_Offer_Merchant_Id
		)
		select osm.dim_MoglOffers_OfferId, 
		       0, --location id
		       @MoglVendorID, 
		       osm.dim_MoglOffers_AcceptedCards, 
		       osm.dim_MoglOffers_ActiveFrom, 
		       osm.dim_MoglOffers_ActiveTo, 
		       osm.dim_MoglOffers_MerchantAddress, 
		       null, --offer cap 
		       osm.dim_MoglOffers_MerchantCity, 
		       osm.dim_MoglOffers_OfferDesc, 
		       osm.dim_MoglOffers_Discount, 
		       osm.dim_MoglOffers_DiscountValue, 
		       null, --dollar cap
		       osm.dim_MoglOffers_MerchantEmail, 
		       osm.dim_MoglOffers_GeoLat, 
		       osm.dim_MoglOffers_GeoLong, 
		       osm.dim_MoglOffers_Image, 
		       osm.dim_MoglOffers_MerchantDesc, 
		       osm.dim_MoglOffers_MerchantName, 
		       osm.dim_MoglOffers_OfferName, 
		       null, --offer type
		       osm.dim_MoglOffers_MerchantPhone, 
		       osm.dim_MoglOffers_MerchantState, 
		       osm.dim_MoglOffers_Status, 
		       osm.dim_MoglOffers_MerchantWebsite, 
		       osm.dim_MoglOffers_MerchantZipCode, 
		       osm.dim_MoglOffers_MerchantId
		from OffersStage_Mogl osm 
		where (osm.dim_MoglOffers_HasBeenLoaded is null 
           or osm.dim_MoglOffers_HasBeenLoaded = 0)
          and osm.dim_MoglOffers_OfferId not in (
            --This should get us the list of existing offers on the Offer table, so we can avoiding inserting again
			select Offer.sid_Offer_IdOffer 
			  from Offer 
			 where Offer.sid_Vendor_Vendor_Id = @MoglVendorID
          );
    
    --== insert offer categories into the relationship table =================
		insert into OfferCategoryRelation (sid_Offer_ID, sid_OfferCategories_Id) 
		select o.sid_Offer_ID, @RestaurantCategoryID 
		from Offer o 
		where o.sid_Vendor_Vendor_Id = @MoglVendorID 
		    and o.sid_Offer_ID not in (
			select ocr.sid_Offer_ID 
			from OfferCategoryRelation ocr
		);
		
    
    --== Finally, update the staging table so they list as processed =========
		update dbo.OffersStage_Mogl 
		set dim_MoglOffers_HasBeenLoaded = 1;
		
	end --end the if statement that checks to be sure we have records to work on.

END

GO


