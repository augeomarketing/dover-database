USE [CLO]
GO

/****** Object:  StoredProcedure [dbo].[usp_MoglLoadStagedMerchants]    Script Date: 10/19/2016 13:01:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MoglLoadStagedMerchants]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MoglLoadStagedMerchants]
GO

USE [CLO]
GO

/****** Object:  StoredProcedure [dbo].[usp_MoglLoadStagedMerchants]    Script Date: 10/19/2016 13:01:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Tipa
-- Create date: 15 Sept 2016
-- Description:	Uses the data on the merchant 
--              staging table for Mogl to update 
--              the Merchant table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_MoglLoadStagedMerchants] 
AS
BEGIN

	SET NOCOUNT ON;

    declare @UnprocessedCount as int;
    declare @MoglVendorID as int;
    declare @SourceFileDate as datetime;
    declare @PreviousDay as datetime;
	
	--== see if there's anything to update or insert ==========================
	select @UnprocessedCount = COUNT(msm.StageID) 
	from dbo.MerchantStage_Mogl msm;
	
	--== get the right vendor ID ==============================================
    select @MoglVendorID = v.sid_Vendor_ID 
    from dbo.Vendor v 
    where v.dim_Vendor_VendorName = 'Mogl';
    
    --== get the source file date from the staging table ======================
    select @SourceFileDate = MAX(msm.DateFromSourceFile) 
    from CLO.dbo.MerchantStage_Mogl msm;
    
    set @PreviousDay = DATEADD(dd, -1, @SourceFileDate);
	
	if @UnprocessedCount > 0
	begin
	
		--== update the merchant table so that merchants who are not represented get deactivated ==========
		update dbo.Merchant 
		   set dim_Merchant_Status = 'inactive', 
		       dim_Merchant_ActiveTo = @PreviousDay  
		 where sid_Vendor_Id = @MoglVendorID 
		   and dim_Merchant_MerchantId not in (
		       select msm.MerchantID 
		       from dbo.MerchantStage_Mogl msm
		   );
		
		--== insert any new merchants =====================================================================
		insert into dbo.Merchant 
		(dim_Merchant_MerchantName, dim_Merchant_MerchantId, sid_Vendor_Id, 
		 dim_EnrollmentType_Open, dim_EnrollmentType_Closed, dim_Merchant_Status, 
		 dim_Merchant_IsOptIn, dim_Merchant_IsOnlineMid, dim_Merchant_ActiveFrom)
	    select msm.MerchantName, 
	           msm.MerchantID, 
	           msm.VendorID, 
	           msm.EnrollTypeFlag_Open, 
	           msm.EnrollTypeFlag_Closed, 
	           msm.MerchantStatus, 
	           0, 0, @SourceFileDate 
	      from dbo.MerchantStage_Mogl msm 
	     where msm.MerchantID not in (
			select m.dim_Merchant_MerchantId 
			from dbo.Merchant m 
			where sid_Vendor_Id = @MoglVendorID
	     );
	
	end
    
END

GO


