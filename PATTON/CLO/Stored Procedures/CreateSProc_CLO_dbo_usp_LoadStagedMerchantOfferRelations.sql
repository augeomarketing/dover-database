USE [CLO]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadStagedMerchantOfferRelations]    Script Date: 10/13/2016 14:30:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadStagedMerchantOfferRelations]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadStagedMerchantOfferRelations]
GO

USE [CLO]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadStagedMerchantOfferRelations]    Script Date: 10/13/2016 14:30:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kevin Tipa
-- Create date: 19 Sept 2016
-- Description:	Merges staged offer merchant relations from staging into the MerchantOfferRelation table
-- =============================================
CREATE PROCEDURE [dbo].[usp_LoadStagedMerchantOfferRelations] 
	@VendorID bigint = -1,  
	@VendorName varchar(25) = ''
AS
BEGIN
	SET NOCOUNT ON;

    declare @VerifiedVendorID as bigint; --the final variable that will be used in the query
    declare @LookupCount as int; --just used when trying to find the ID we want
    declare @FoundVendorID as bit;
    
    set @FoundVendorID = 0;
    
    select @LookupCount = COUNT(sid_Vendor_ID) 
    from CLO.dbo.Vendor 
    where sid_Vendor_ID = @VendorID;
    
    if (1 = @LookupCount) 
    begin
		set @VerifiedVendorID = @VendorID;
		set @FoundVendorID = 1;
    end
    
    if (0 = @FoundVendorID)--means we should try checking the VendorName parameter to try to find a Vendor ID
    begin
		select @LookupCount = COUNT(sid_Vendor_ID) 
		from CLO.dbo.Vendor 
		where dim_Vendor_VendorName like '%' + @VendorName + '%';
		
		if (1 = @LookupCount)
		begin
			select @VerifiedVendorID = sid_Vendor_ID 
			from CLO.dbo.Vendor 
			where dim_Vendor_VendorName like '%' + @VendorName + '%';
			
			set @FoundVendorID = 1;
		end
    end
    
    if (1 = @FoundVendorID)
    begin
		--== the main work =================================================================
		insert into CLO.dbo.MerchantOfferRelation 
		select m.sid_Merchant_ID, o.sid_Offer_ID 
		from MerchantOfferRelationStage mors 
		inner join Merchant m on mors.dim_Merchant_MerchantId = m.dim_Merchant_MerchantId and mors.sid_Vendor_ID = m.sid_Vendor_Id 
		inner join Offer o on mors.sid_Offer_IdOffer = o.sid_Offer_IdOffer and mors.sid_Vendor_ID = o.sid_Vendor_Vendor_Id 
		where mors.sid_Vendor_ID = @VerifiedVendorID 
		  and 0 = (select COUNT(mor.sid_Merchant_ID) from CLO.dbo.MerchantOfferRelation mor 
				   where mor.sid_Merchant_ID = m.sid_Merchant_ID and mor.sid_Offer_ID = o.sid_Offer_ID);
		--==================================================================================
    end
    
    
END

GO


