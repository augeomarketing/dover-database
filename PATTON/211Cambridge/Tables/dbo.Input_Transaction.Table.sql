USE [211Cambridge]
GO
/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[Input_Transaction_Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Card Nbr] [varchar](255) NULL,
	[Primary Acct Nbr] [varchar](255) NULL,
	[SUM Sum Trn Cnt] [varchar](255) NULL,
	[SUM Sum Tran Amt] [float] NULL,
	[Process Typ Cd] [varchar](255) NULL,
	[Network Id] [varchar](255) NULL,
	[TranCode] [char](2) NULL,
	[TipNumber] [varchar](50) NULL,
	[Points] [decimal](18, 2) NULL,
	[RoundedSumTranAmt] [int] NULL,
 CONSTRAINT [PK_Input_Transaction] PRIMARY KEY CLUSTERED 
(
	[Input_Transaction_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
