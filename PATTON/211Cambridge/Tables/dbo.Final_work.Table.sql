USE [211Cambridge]
GO
/****** Object:  Table [dbo].[Final_work]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Final_work](
	[Acct Nbr] [float] NULL,
	[ ] [float] NULL,
	[F3] [float] NULL,
	[F4] [nvarchar](255) NULL,
	[Nm Line 1] [nvarchar](255) NULL,
	[F6] [nvarchar](255) NULL,
	[F7] [float] NULL,
	[F8] [nvarchar](255) NULL,
	[F9] [nvarchar](255) NULL,
	[F10] [nvarchar](255) NULL,
	[F11] [nvarchar](255) NULL,
	[F12] [nvarchar](255) NULL,
	[F13] [nvarchar](255) NULL,
	[F14] [nvarchar](255) NULL,
	[F15] [nvarchar](255) NULL,
	[F16] [float] NULL,
	[F17] [nvarchar](255) NULL,
	[F18] [nvarchar](255) NULL,
	[F19] [nvarchar](255) NULL,
	[F20] [nvarchar](255) NULL,
	[F21] [float] NULL,
	[F22] [nvarchar](255) NULL
) ON [PRIMARY]
GO
