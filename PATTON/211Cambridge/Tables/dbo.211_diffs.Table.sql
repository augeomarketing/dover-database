USE [211Cambridge]
GO
/****** Object:  Table [dbo].[211_diffs]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[211_diffs](
	["TipNumber"] [varchar](50) NULL,
	[" ACCTNAME1"] [varchar](50) NULL,
	[" ACCTNAME2"] [varchar](50) NULL,
	[" ACCTNAME3"] [varchar](50) NULL,
	[" ACCTNAME4"] [varchar](50) NULL,
	[" ACCTNAME5"] [varchar](50) NULL,
	[" ACCTNAME6"] [varchar](50) NULL,
	[" ADDRESS1"] [varchar](50) NULL,
	[" ADDRESS2"] [varchar](50) NULL,
	[" CITY"] [varchar](50) NULL,
	[" State"] [varchar](50) NULL,
	[" ZipCode"] [varchar](50) NULL,
	[" BeginningPoints"] [varchar](50) NULL,
	[" EndingPointBalance"] [varchar](50) NULL,
	[" DebitPurchase"] [varchar](50) NULL,
	[" DebitBonus"] [varchar](50) NULL,
	[" DebitReturn"] [varchar](50) NULL,
	[" Redeemed"] [varchar](50) NULL,
	[" Adjustments"] [varchar](50) NULL,
	[" emailaddress"] [varchar](50) NULL,
	[" emailstatement"] [varchar](50) NULL,
	[" AcctId001"] [varchar](50) NULL,
	[" DDANbr001"] [varchar](50) NULL,
	[" AcctId002"] [varchar](50) NULL,
	[" DDANbr002"] [varchar](50) NULL,
	[" AcctId003"] [varchar](50) NULL,
	[" DDANbr003"] [varchar](50) NULL,
	[" AcctId004"] [varchar](50) NULL,
	[" DDANbr004"] [varchar](50) NULL,
	[" AcctId005"] [varchar](50) NULL,
	[" DDANbr005"] [varchar](50) NULL,
	[" AcctId006"] [varchar](50) NULL,
	[" DDANbr006"] [varchar](50) NULL,
	[" AcctId007"] [varchar](50) NULL,
	[" DDANbr007"] [varchar](50) NULL,
	[" AcctId008"] [varchar](50) NULL,
	[" DDANbr008"] [varchar](50) NULL,
	[" AcctId009"] [varchar](50) NULL,
	[" DDANbr009"] [varchar](50) NULL,
	[" AcctId010"] [varchar](50) NULL,
	[" DDANbr010"] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
