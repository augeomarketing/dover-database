USE [211Cambridge]
GO
/****** Object:  Table [dbo].[input_purge]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_purge](
	[input_purge_id] [int] IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NULL,
	[purgeDate] [datetime] NOT NULL,
	[custid] [varchar](20) NULL,
	[currentdate] [datetime] NOT NULL,
 CONSTRAINT [PK_input_purge] PRIMARY KEY CLUSTERED 
(
	[input_purge_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_inputpurge_tipnumber_custid] ON [dbo].[input_purge] 
(
	[tipnumber] ASC,
	[custid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[input_purge] ADD  CONSTRAINT [DF_input_purge_purgeDate]  DEFAULT (getdate()) FOR [purgeDate]
GO
ALTER TABLE [dbo].[input_purge] ADD  CONSTRAINT [DF_input_purge_currentdate]  DEFAULT (getdate()) FOR [currentdate]
GO
