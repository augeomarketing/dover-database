USE [211Cambridge]
GO
/****** Object:  Table [dbo].[Initial Rewards Program Mailing File CSB 11-7-07]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Initial Rewards Program Mailing File CSB 11-7-07](
	[Col001] [varchar](255) NULL,
	[Col002] [varchar](255) NULL,
	[Col003] [varchar](255) NULL,
	[Col004] [varchar](255) NULL,
	[Col005] [varchar](255) NULL,
	[Col006] [varchar](255) NULL,
	[Col007] [varchar](255) NULL,
	[Col008] [varchar](255) NULL,
	[Col009] [varchar](255) NULL,
	[Col010] [varchar](255) NULL,
	[Col011] [varchar](255) NULL,
	[Col012] [varchar](255) NULL,
	[Col013] [varchar](255) NULL,
	[Col014] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
