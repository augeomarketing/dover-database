USE [211Cambridge]
GO
/****** Object:  Table [dbo].[purge_pending]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[purge_pending](
	[purge_pending_id] [int] IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[purgedate] [datetime] NOT NULL,
 CONSTRAINT [PK_purge_pending] PRIMARY KEY CLUSTERED 
(
	[purge_pending_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [idx_purgepending_tipnumber_purgedate] ON [dbo].[purge_pending] 
(
	[tipnumber] ASC,
	[purgedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
