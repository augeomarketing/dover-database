USE [211Cambridge]
GO
/****** Object:  Table [dbo].[__tmp_AccountsbyTipStage]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__tmp_AccountsbyTipStage](
	[TipNumber] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[CITY] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[BeginningPoints] [int] NULL,
	[EndingPointBalance] [int] NULL,
	[DebitPurchase] [int] NULL,
	[DebitBonus] [int] NULL,
	[DebitReturn] [int] NULL,
	[Redeemed] [int] NULL,
	[Adjustments] [int] NULL,
	[emailaddress] [varchar](40) NULL,
	[emailstatement] [char](1) NULL,
	[AcctId001] [nvarchar](25) NULL,
	[DDANbr001] [nvarchar](25) NULL,
	[AcctId002] [nvarchar](25) NULL,
	[DDANbr002] [nvarchar](25) NULL,
	[AcctId003] [nvarchar](25) NULL,
	[DDANbr003] [nvarchar](25) NULL,
	[AcctId004] [nvarchar](25) NULL,
	[DDANbr004] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
