USE [211Cambridge]
GO
/****** Object:  Table [dbo].[Input_Customer_Error]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer_Error](
	[Input_Customer_Error_Id] [int] NOT NULL,
	[Acct Nbr] [varchar](255) NULL,
	[Br Nbr] [varchar](255) NULL,
	[Card Nbr] [varchar](255) NULL,
	[Acct Stat Cd] [varchar](255) NULL,
	[Mbr Nm] [varchar](255) NULL,
	[Card Stat Cd] [varchar](255) NULL,
	[Acct Nbr1] [varchar](255) NULL,
	[Acct Status Flg] [varchar](255) NULL,
	[Nm Line 1] [varchar](255) NULL,
	[Nm Line 2] [varchar](255) NULL,
	[Nm Line 3] [varchar](255) NULL,
	[Addr Line 1] [varchar](255) NULL,
	[Addr Line 2] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[St Cd] [varchar](255) NULL,
	[Zip Cd] [varchar](255) NULL,
	[Close Dt] [varchar](255) NULL,
	[Last Maint 1 Dt] [varchar](255) NULL,
	[Last Maint 1 Typ Cd] [varchar](255) NULL,
	[Close Reason Cd] [varchar](255) NULL,
	[Open Date] [datetime] NULL,
	[Legacy] [varchar](50) NULL,
	[TipNumber] [varchar](50) NULL,
	[Replace DDA] [char](1) NULL,
	[ErrorReason] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
