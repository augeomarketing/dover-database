USE [211Cambridge]
GO
/****** Object:  Table [dbo].[zzPurgeByDDA]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[zzPurgeByDDA](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [char](25) NOT NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
