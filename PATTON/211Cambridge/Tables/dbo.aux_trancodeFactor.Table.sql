USE [211Cambridge]
GO
/****** Object:  Table [dbo].[aux_trancodeFactor]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[aux_trancodeFactor](
	[aux_trancodeFactorId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[trancode] [char](2) NULL,
	[pointfactor] [decimal](18, 2) NULL,
 CONSTRAINT [PK_aux_trancodeFactor] PRIMARY KEY CLUSTERED 
(
	[aux_trancodeFactorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
