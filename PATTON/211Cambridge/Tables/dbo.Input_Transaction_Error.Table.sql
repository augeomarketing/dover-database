USE [211Cambridge]
GO
/****** Object:  Table [dbo].[Input_Transaction_Error]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction_Error](
	[Input_Transaction_Id] [int] NULL,
	[Card Nbr] [varchar](255) NULL,
	[Primary Acct Nbr] [varchar](255) NULL,
	[SUM Sum Trn Cnt] [varchar](255) NULL,
	[SUM Sum Tran Amt] [float] NULL,
	[Process Typ Cd] [varchar](255) NULL,
	[Network Id] [varchar](255) NULL,
	[TranCode] [char](2) NULL,
	[TipNumber] [varchar](50) NULL,
	[Points] [decimal](18, 2) NULL,
	[RoundedSumTranAmt] [int] NULL,
	[ErrorReason] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
