USE [211Cambridge]
GO
/****** Object:  Table [dbo].[Input_Mailingfile]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Mailingfile](
	[Acct Nbr] [varchar](50) NULL,
	[Br Nbr] [varchar](50) NULL,
	[card nbr] [varchar](50) NULL,
	[sum sum trn cnt] [int] NULL,
	[mbr nm] [varchar](50) NULL,
	[nm line 1] [varchar](50) NULL,
	[nm line 2] [varchar](50) NULL,
	[nm line 3] [varchar](50) NULL,
	[addr line 1] [varchar](50) NULL,
	[addr line 2] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[st cd] [varchar](50) NULL,
	[zip cd] [varchar](50) NULL,
	[acct nbr1] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
