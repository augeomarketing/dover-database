USE [211Cambridge]
GO
/****** Object:  Table [dbo].[PURGE_TIP]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PURGE_TIP](
	[TIPNUMBER] [varchar](255) NULL,
	[NAME1] [varchar](255) NULL,
	[NAME2] [varchar](255) NULL,
	[ADDRESS1] [varchar](255) NULL,
	[CITY] [varchar](255) NULL,
	[STATE] [varchar](255) NULL,
	[ZIP] [varchar](255) NULL,
	[Col008] [varchar](255) NULL,
	[Col009] [varchar](255) NULL,
	[Col010] [varchar](255) NULL,
	[Col011] [varchar](255) NULL,
	[Col012] [varchar](255) NULL,
	[Col013] [varchar](255) NULL,
	[Col014] [varchar](255) NULL,
	[Col015] [varchar](255) NULL,
	[Col016] [varchar](255) NULL,
	[Col017] [varchar](255) NULL,
	[Col018] [varchar](255) NULL,
	[Col019] [varchar](255) NULL,
	[Col020] [varchar](255) NULL,
	[Col021] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
