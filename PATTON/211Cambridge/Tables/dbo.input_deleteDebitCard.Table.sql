USE [211Cambridge]
GO
/****** Object:  Table [dbo].[input_deleteDebitCard]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_deleteDebitCard](
	[input_deleteDebitCard_id] [int] IDENTITY(1,1) NOT NULL,
	[acctid] [varchar](25) NULL,
 CONSTRAINT [PK_input_deleteDebitCard] PRIMARY KEY CLUSTERED 
(
	[input_deleteDebitCard_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
