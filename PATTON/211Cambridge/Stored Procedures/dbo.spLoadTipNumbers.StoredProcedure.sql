USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
********************************************************
 This updates the TIPNUMBER in the input_CustTran table   
********************************************************
*/

CREATE    PROCEDURE [dbo].[spLoadTipNumbers] AS

DECLARE @NewTip bigint
DECLARE @TipPrefix char(3)

UPDATE aux_uniqueDDA
SET TIPNUMBER = AFFILIAT_Stage.TIPNUMBER
  FROM AFFILIAT_Stage
	  INNER JOIN aux_uniqueDDA
	  ON AFFILIAT_Stage.[CUSTID] = aux_uniqueDDA.[CUSTID]
  WHERE aux_uniqueDDA.TIPNUMBER IS NULL
  
SELECT @TipPrefix = TipFirst FROM Client
EXEC rewardsnow.dbo.[spGetLastTipNumberUsed] @TipPrefix, @NewTip OUTPUT

If @NewTip is NULL Set @NewTip = @TipPrefix + '000000000000'

/* Assign Get New Tips to new customers */
UPDATE aux_uniqueDDA
SET @NewTip = ( @NewTip + 1 ),
	TIPNUMBER =  @NewTip 
	WHERE TipNumber IS NULL

-- Update LastTip -- 
--UPDATE Client SET LastTipNumberUsed = @NewTip  
EXEC rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip

/* Load Tipnumbers to Input_Transaction AND Input_customer Table */

UPDATE Input_Customer
SET Input_Customer.TIPNUMBER = aux_uniqueDDA.TIPNUMBER
FROM Input_Customer
	INNER JOIN aux_uniqueDDA
	ON Input_Customer.[Acct Nbr] = aux_uniqueDDA.[CUSTID]
WHERE Input_Customer.TIPNUMBER IS NULL

UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = aux_uniqueDDA.TIPNUMBER
FROM Input_Transaction
	INNER JOIN aux_uniqueDDA
	ON Input_Transaction.[Primary Acct Nbr] = aux_uniqueDDA.[CUSTID]
WHERE Input_Transaction.TIPNUMBER IS NULL
GO
