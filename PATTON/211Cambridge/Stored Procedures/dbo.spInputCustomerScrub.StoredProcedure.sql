USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spInputCustomerScrub]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
****************************************************************************
    This scrubs (cleans up) the inport_Customer and Input_Transaction tables
**************************************************************************** 
*/	
CREATE       PROCEDURE [dbo].[spInputCustomerScrub] AS

DECLARE @TipFirst CHAR(3)
SELECT @TipFirst = tipfirst FROM Client

TRUNCATE TABLE  Input_Customer_error

/*  UPDATE DDA Replacements */
UPDATE affiliat_stage 
SET custid = input_customer.[Acct Nbr]
  FROM affiliat_stage 
    INNER JOIN input_customer
	    ON affiliat_stage.[acctid] = input_customer.[card nbr]
WHERE [replace dda] = 'Y'

/* Remove Empty Rows */
DELETE FROM Input_Customer
WHERE [Acct Nbr] IS NULL

/* Remove input_customer records with Member Name null */
INSERT INTO Input_customer_error 
SELECT *, 'Member Name not Specified' 
FROM Input_Customer 
WHERE [Mbr Nm] IS NULL

DELETE FROM Input_Customer WHERE [Mbr Nm] IS NULL

INSERT INTO Input_customer_error 
SELECT *, 'Acct Nbr Different For Existing Card Nbr'
FROM Input_Customer a
WHERE [Card Nbr] IN (SELECT [Card Nbr] FROM affiliat_stage b WHERE a.[Card Nbr] = b.[AcctId] AND a.[Acct Nbr] <> b.[CustId]  )

DELETE FROM Input_Customer WHERE [Card Nbr] IN 
(SELECT [Card Nbr] FROM Input_Customer a WHERE [Card Nbr] IN (SELECT DISTINCT [Card Nbr] FROM affiliat_stage b WHERE a.[Card Nbr] = b.[AcctId] AND a.[Acct Nbr] <> b.[CustId]  ))

INSERT INTO Input_customer_error
SELECT *, 'Customer Opt Out Request'
FROM Input_Customer a
WHERE [Acct Nbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

DELETE FROM Input_Customer 
WHERE [Acct Nbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

INSERT INTO Input_customer_error 
SELECT *, 'Truncating Address1 to 40 Characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM([Addr Line 1]))) > 40

INSERT INTO Input_customer_error 
SELECT *, 'Truncating Address2 to 40 Characters'
FROM Input_Customer a
WHERE LEN(LTRIM(RTRIM([Addr Line 2]))) > 40


/* remove leading and trailing spaces in names and addresses */ 
UPDATE Input_Customer 
	SET
	[Mbr Nm] = LTRIM(RTRIM([Mbr Nm])) , 
	[Card Nbr] = LTRIM(RTRIM([Card Nbr])) , 
	[Nm Line 1] = LTRIM(RTRIM([Nm Line 1])) , 
	[Nm Line 2] = LTRIM(RTRIM([Nm Line 2])) , 
	[Nm Line 3] = LTRIM(RTRIM([Nm Line 3])) , 
	[Addr Line 1] = LEFT(LTRIM(RTRIM([Addr Line 1])), 40) , 
	[Addr Line 2] = LEFT(LTRIM(RTRIM([Addr Line 2])), 40) , 
	[City] = LTRIM(RTRIM([City])),
	[St Cd] = LTRIM(RTRIM([St Cd])),
	[Zip Cd] = LTRIM(RTRIM([Zip Cd]))
GO
