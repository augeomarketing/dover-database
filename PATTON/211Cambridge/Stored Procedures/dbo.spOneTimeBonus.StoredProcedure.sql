USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spOneTimeBonus]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
   This Stored Procedure awards Bonuses  to PointsNow Tables 

	Award Points if 1st use
	New check (debit) card: points  on 1st use
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 
-- Add a bonus for all cards = 'N' 
-- add a record in the onetime bonus for all cards = 'N'

****************************************************************************
*/

ALTER PROCEDURE [dbo].[spOneTimeBonus] @DateAdded CHAR(10), @BonusAmt INT, @TranCode CHAR(2) AS

DECLARE @SQLDynamic NVARCHAR(1000)
DECLARE @Tipnumber CHAR(15)
DECLARE @AcctId	CHAR(16)
DECLARE @TrancodeDesc CHAR(20)
DECLARE @Ratio FLOAT

DECLARE @tmpTable TABLE (
	tipnumber varchar(15)
)


IF @TranCode = 'BE'
BEGIN
	INSERT INTO @tmpTable ( tipnumber ) 
	SELECT tipnumber FROM (
		SELECT re.TipNumber FROM RN1_EstatementTip re LEFT OUTER JOIN History h ON re.TipNumber = h.TIPNUMBER
			 AND h.TRANCODE = @trancode
			WHERE h.TIPNUMBER IS NULL
	) AS tmp		
END
ELSE
BEGIN
	INSERT INTO @tmpTable ( tipnumber ) 
	SELECT TIPNUMBER FROM (
		SELECT c.TipNumber FROM customer_stage c LEFT OUTER JOIN History h ON c.TIPNUMBER = h.TIPNUMBER 
			AND h.TRANCODE = @trancode
		WHERE h.TIPNUMBER IS NULL
	) AS tmp
		
END

-- Retrieve the Trancode fields
SELECT @TrancodeDesc = [Description], @Ratio = Ratio FROM RewardsNOW.dbo.Trantype WHERE trancode = @TranCode

UPDATE cs 
SET RunAvaliableNew = RunAvaliableNew + @BonusAmt
FROM CUSTOMER_Stage cs JOIN @tmpTable tt ON cs.TIPNUMBER = tt.tipnumber
	WHERE dateadded = @DateAdded 

INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,[Description],Overage,SecID)
SELECT DISTINCT ic.Tipnumber, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101), @TranCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW' 
FROM input_customer ic 
	JOIN @tmpTable tt
	ON ic.TipNumber = tt.tipnumber

INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
SELECT DISTINCT ic.Tipnumber, @TranCode, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101) 
	FROM input_Customer ic
	JOIN @tmpTable tt
	ON ic.TipNumber = tt.tipnumber
GO
