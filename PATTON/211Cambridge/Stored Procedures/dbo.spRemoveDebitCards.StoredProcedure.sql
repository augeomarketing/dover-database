USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveDebitCards]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090420
-- Description:	Remove Debit Cards From Affiliat
-- =============================================
CREATE PROCEDURE [dbo].[spRemoveDebitCards]
AS
BEGIN
  INSERT INTO AffiliatDeleted (TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
  SELECT TipNumber, AcctType, DateAdded, SecID, a.AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, getdate()
    FROM Affiliat a
      INNER JOIN input_deleteDebitCard iddc
        ON a.acctid = iddc.acctid
  
  DELETE FROM Affiliat
    WHERE acctid IN (SELECT acctid FROM input_deleteDebitCard)
END
GO
