USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  
**************************************************************
Copies data from input_transaction to the Affiliat_stage table
  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
**************************************************************  
*/

CREATE     PROCEDURE [dbo].[spLoadAffiliatStage] @MonthEnd CHAR(20) AS 

/************ Insert New Accounts into Affiliat Stage  ***********/
INSERT INTO Affiliat_Stage
  (AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, YTDEarned, CustID)
SELECT Distinct t.[Card Nbr], t.TipNumber, A.AcctTypeDesc, @MonthEnd, c.[Acct Stat Cd], TT.[Description] ,  0, t.[Primary Acct Nbr]
  FROM Input_Customer c 
    INNER JOIN Input_Transaction t 
      ON c.Tipnumber = t.Tipnumber
    INNER JOIN AcctType A
      ON t.Trancode = A.AcctType
    INNER JOIN TranType TT
      ON TT.Trancode = T.Trancode 
  WHERE t.[Card Nbr] NOT IN ( SELECT ACCTID FROM Affiliat_Stage)

INSERT INTO Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, YTDEarned, CustID)
SELECT DISTINCT c.[card nbr], c.tipnumber, 'BONUS', @MonthEnd, c.[Acct Stat Cd], 'Bonus Points', 0, c.[Acct Nbr]
FROM Input_Customer c
  INNER JOIN Customer_Stage cs
    ON c.tipnumber = cs.tipnumber
WHERE c.[Card Nbr] NOT IN ( SELECT ACCTID FROM Affiliat_Stage)

INSERT INTO affiliat_stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, CustID)
SELECT DISTINCT [card nbr], c.tipnumber, 'BONUS', @MonthEnd, [Acct Stat Cd], 'Bonus Points', [Acct Nbr]  
  FROM input_customer c
    INNER JOIN Customer_Stage cs
    ON c.tipnumber = cs.tipnumber
  WHERE c.tipnumber NOT IN (SELECT tipnumber FROM affiliat_stage)
GO
