USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewCardStage]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
   This Stored Procedure awards Bonuses  to PointsNow Tables 

	Award Points if 1st use
	New check (debit) card: points  on 1st use
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 
-- Add a bonus for all cards = 'N' 
-- add a record in the onetime bonus for all cards = 'N'

****************************************************************************
*/

CREATE PROCEDURE [dbo].[spBonusNewCardStage] @DateAdded CHAR(10), @BonusAmt INT, @TranCode CHAR(2) AS

DECLARE @SQLDynamic NVARCHAR(1000)
DECLARE @Tipnumber CHAR(15)
DECLARE @AcctId	CHAR(16)
DECLARE @TrancodeDesc CHAR(20)
DECLARE @Ratio FLOAT

-- Retrieve the Trancode fields
SELECT @TrancodeDesc = [Description], @Ratio = Ratio FROM RewardsNOW.dbo.Trantype WHERE trancode = @TranCode

UPDATE Customer_Stage
SET RunAvaliableNew = RunAvaliableNew + @BonusAmt
	WHERE dateadded = @DateAdded 
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses WHERE Trancode = @TranCode )

INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,[Description],Overage,SecID)
( 
  SELECT DISTINCT Tipnumber, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101), @TranCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW' 
	FROM input_customer 
	WHERE tipnumber IN 
		(
			SELECT tipnumber FROM customer_stage 	WHERE dateadded = @DateAdded 
			AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses_Stage WHERE Trancode = @TranCode )
		)
)

INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
( SELECT DISTINCT Tipnumber, @TranCode, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101) 
	FROM input_Customer
	WHERE tipnumber IN 
		(
		  SELECT tipnumber FROM customer_stage WHERE dateadded = @DateAdded 
		    AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses_Stage WHERE Trancode = @TranCode )
		)
)
/*
UPDATE Customer_Stage
SET RunAvaliableNew = RunAvaliableNew + @BonusAmt  
	WHERE tipnumber IN 
		( SELECT c.Tipnumber FROM Input_transaction t 
			INNER JOIN input_customer c ON t.[card nbr] = c.[card nbr]
			WHERE c.[Card Stat Cd] = 'O'
			UNION
		  SELECT tipnumber FROM input_customer WHERE [open date] between dateadd(m, -1, (dateadd(d, 1, @dateadded) )) AND @dateadded
				AND [card stat cd] = 'O' -- ONLY ADD Open Card Records
		)
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses WHERE Trancode = @TranCode )


INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,[Description],Overage,SecID)
( SELECT DISTINCT Tipnumber, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101), @TranCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW' 
	FROM input_customer 
	WHERE tipnumber IN 
		( SELECT c.Tipnumber FROM Input_transaction t 
			INNER JOIN input_customer c ON t.[card nbr] = c.[card nbr]
			WHERE c.[Card Stat Cd] = 'O'
			UNION
		  SELECT tipnumber FROM input_customer WHERE [open date] between dateadd(m, -1, (dateadd(d, 1, @dateadded) )) AND @dateadded
				AND [card stat cd] = 'O' -- ONLY ADD Open Card Records
		)
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses WHERE Trancode = @TranCode )
)

INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
( SELECT DISTINCT  Tipnumber, @TranCode, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101) 
	FROM input_Customer
	WHERE tipnumber IN 
		( SELECT c.Tipnumber FROM Input_transaction t 
			INNER JOIN input_customer c ON t.[card nbr] = c.[card nbr]
			WHERE c.[Card Stat Cd] = 'O'
			UNION
		  SELECT tipnumber FROM input_customer WHERE [open date] between dateadd(m, -1, (dateadd(d, 1, @dateadded) )) AND @dateadded
				AND [card stat cd] = 'O' -- ONLY ADD Open Card Records
		)
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses WHERE Trancode = @TranCode )
)
OPTION (MAXDOP 1)
*/
GO
