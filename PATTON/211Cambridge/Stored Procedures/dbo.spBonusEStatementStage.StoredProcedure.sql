USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spBonusEStatementStage]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
   This Stored Procedure awards Bonuses  to PointsNow Tables 

	Award Points if E-Statment Selected

-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 

****************************************************************************
*/
CREATE  PROCEDURE [dbo].[spBonusEStatementStage] @DateAdded CHAR(10), @BonusAmt INT, @TranCode CHAR(2) AS

DECLARE @Tipnumber CHAR(15)
DECLARE @AcctId	CHAR(16)
DECLARE @TrancodeDesc CHAR(20)
DECLARE @Ratio FLOAT

-- Retrieve the Trancode fields
SELECT @TrancodeDesc = [Description], @Ratio = Ratio FROM RewardsNOW.dbo.Trantype WHERE trancode = @TranCode

UPDATE Customer_Stage
  SET RunAvaliableNew = RunAvaliableNew + @BonusAmt  
  WHERE 
    tipnumber IN ( SELECT Tipnumber FROM RN1_EstatementTip )
      AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses_Stage WHERE Trancode = @TranCode )
	
INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,[Description],Overage,SecID)
( SELECT DISTINCT Tipnumber, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101), @TranCode, '1', @BonusAmt, @Ratio, @TrancodeDesc, '0','NEW' 
	FROM input_customer 
	WHERE tipnumber IN ( SELECT Tipnumber FROM RN1_EstatementTip )
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses_Stage WHERE Trancode = @TranCode ))
 
INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
( SELECT DISTINCT Tipnumber, @TranCode, [Acct Nbr], CONVERT(CHAR(10), @DateAdded,101) 
	FROM input_customer
	WHERE tipnumber IN ( SELECT Tipnumber FROM RN1_EstatementTip )
		AND tipnumber NOT IN ( SELECT Tipnumber FROM OneTimeBonuses_stage WHERE Trancode = @TranCode ))
GO
