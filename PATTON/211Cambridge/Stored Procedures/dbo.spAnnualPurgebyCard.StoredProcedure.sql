USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spAnnualPurgebyCard]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAnnualPurgebyCard]  AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
/*  **************************************  */
Declare @DateDeleted as datetime
declare @custid nvarchar(13)
set @custid = '0000000000000'
set @DateDeleted = '12-30-2009'
declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)


 		set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''ZZAffiliat1'')
		Begin
			drop TABLE .dbo.ZZAffiliat1 
		End '
		exec sp_executesql @SQLIf

		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''ZZAffiliat2'')
		Begin
			drop TABLE .dbo.ZZAffiliat2 
		End '
		exec sp_executesql @SQLIf
		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''ZZAffiliat3'')
		Begin
			drop TABLE .dbo.ZZAffiliat3  
		End '
		exec sp_executesql @SQLIf

		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''AccountDeleteInput'')
		Begin
			truncate TABLE .dbo.AccountDeleteInput
		End '
		exec sp_executesql @SQLIf


select * into ZZAffiliat1 from AFFILIAT

update ZZAffiliat1 set acctstatus = '9' where acctid in (select debacctnum from PURGE_DEBIT)

delete from ZZAffiliat1 where acctstatus = '9'

select distinct(TIPNUMBER) into ZZAffiliat2  
from affiliat where tipnumber  not in (select distinct(tipnumber) from ZZAffiliat1)

select acctid into ZZAffiliat3 from affiliat where tipnumber in (select tipnumber from ZZAffiliat2)

delete from PURGE_DEBIT where debacctnum  in (select acctid from ZZAffiliat3)

 insert  into AccountDeleteInput
 (acctid ,dda)
 select acctid,'0000000000000' from AFFILIAT where acctid in (select debacctnum from dbo.PURGE_DEBIT)
 




/*  This code from here to the end of the proc is cloned from the general purge  */

	-- Set Affiliat records that are null to active 
	update Affiliat set AcctStatus = 'A' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer set Status = 'A' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat	
	set Acctstatus = '9'
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat.acctid = AccountDeleteInput.acctid )
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat 
	 WHERE  affiliat.acctstatus = '9'
	
	/*  **************************************  */
	Delete from affiliat where acctstatus = '9'
	
	/*  **************************************  */
	-- Find Customers without accounts and mark for delete 
	update Customer
	set Status='9'
	where tipnumber not in 
		(select  distinct(tipnumber)  from affiliat )
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @DateDeleted 
	from customer WHERE  status = '9'


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history 
	where tipnumber in 
	(select tipnumber from customer where status = '9')
	/*  **************************************  */
	-- Delete History
	Delete from history where tipnumber in (select tipnumber from customer where status = '9')
	
	/*  **************************************  */
	-- Delete Customer
	Delete from customer where status = '9'
	/* Return 0 */
GO
