USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spStageMonthlyAuditValidation]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[spStageMonthlyAuditValidation] @errorCount int OUTPUT
     
AS

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

TRUNCATE TABLE Monthly_Audit_ErrorFile

INSERT INTO Monthly_Audit_ErrorFile (Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Errormsg, Currentend)
SELECT Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased, 'Ending Balances do not match', (SELECT SUM(AdjustedEndingPoints) FROM Current_Month_Activity WHERE tipnumber = msf.tipnumber)
  FROM Monthly_Statement_File msf
  WHERE pointsend <> (SELECT SUM(AdjustedEndingPoints) FROM Current_Month_Activity WHERE tipnumber = msf.tipnumber)
  
set @errorCount = @@ROWCOUNT

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
/*
TRUNCATE TABLE Monthly_Audit_ErrorFile

DECLARE @Tipnumber NCHAR(15), @pointsbegin NUMERIC(9), @pointsend NUMERIC(9), @pointspurchasedCR NUMERIC(9),@pointspurchasedDB NUMERIC(9), @pointsbonus NUMERIC(9), @pointsadded NUMERIC(9),
 @pointsincreased NUMERIC(9), @pointsredeemed NUMERIC(9), @pointsreturnedCR NUMERIC(9), @pointsreturnedDB NUMERIC(9), @pointssubtracted NUMERIC(9), @pointsdecreased NUMERIC(9), @errmsg VARCHAR(50), @currentend NUMERIC(9)


DECLARE tip_crsr CURSOR
FOR SELECT Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased 
FROM Monthly_Statement_File
OPEN tip_crsr

FETCH tip_crsr INTO @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased

IF @@FETCH_STATUS = 1
	GOTO Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @errmsg = NULL
	SET @currentend = '0'
IF @pointsend <> (SELECT SUM(AdjustedEndingPoints) FROM Current_Month_Activity WHERE tipnumber = @tipnumber)
		BEGIN
			SET @errmsg='Ending Balances do not match'
			SET @currentend=(SELECT SUM(AdjustedEndingPoints) FROM Current_Month_Activity WHERE tipnumber = @tipnumber)		
			INSERT INTO Monthly_Audit_ErrorFile
			VALUES(  @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased, @errmsg, @currentend )
			GOTO Next_Record
		END
		
	GOTO Next_Record
	Next_Record:
	FETCH tip_crsr INTO @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased
END

GOTO Fetch_Error

Fetch_Error:
CLOSE  tip_crsr
DEALLOCATE tip_crsr
*/
GO
