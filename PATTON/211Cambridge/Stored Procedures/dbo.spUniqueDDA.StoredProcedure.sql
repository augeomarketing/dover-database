USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spUniqueDDA]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 11/21/07 */
/* Author: Allen B  */
/*  **************************************  */
/*  **************************************  */

CREATE  PROCEDURE [dbo].[spUniqueDDA]
AS 
TRUNCATE TABLE aux_uniquedda
INSERT INTO aux_uniquedda (CUSTID) 
SELECT DISTINCT [Acct Nbr] FROM Input_Customer
GO
