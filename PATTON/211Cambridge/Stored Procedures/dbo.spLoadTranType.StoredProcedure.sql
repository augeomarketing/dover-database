USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTranType]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadTranType]
AS 


truncate table trantype
insert into trantype select *  from Rewardsnow.dbo.trantype
GO
