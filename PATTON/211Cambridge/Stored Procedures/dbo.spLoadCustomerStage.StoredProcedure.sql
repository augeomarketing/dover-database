USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
****************************************************************************
   This imports data from input_custTran into the customer_STAGE  table
   it only updates the customer demographic data   

   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  
****************************************************************************
*/	

CREATE         PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

UPDATE Customer_Stage
SET 
--LASTNAME 	= LEFT(RTRIM(Input_Customer.LASTNAME),40)
ACCTNAME1 	= LEFT(RTRIM(Input_Customer.[Nm Line 1]),40 )
,ACCTNAME2 	= LEFT(RTRIM(Input_Customer.[Nm Line 2]),40 )
,ACCTNAME3 	= LEFT(RTRIM(Input_Customer.[Nm Line 3]),40 )
,ADDRESS1 	= Input_Customer.[Addr Line 1]
,ADDRESS2 	= Input_Customer.[Addr Line 2]
,ADDRESS4       = LEFT(LTRIM(RTRIM( Input_Customer.[City])) + ' ' + LTRIM(RTRIM( Input_Customer.[St Cd])) + ' ' + LTRIM( RTRIM( Input_Customer.[Zip Cd])) , 40 )
,CITY 		= Input_Customer.[City]
,STATE		= LEFT(Input_Customer.[St Cd],2)
,ZIPCODE 	= LTRIM(Input_Customer.[Zip Cd])
,EmployeeFlag	= Input_Customer.[Acct Status Flg]
,STATUS		= Input_Customer.[Acct Stat Cd]
,MISC2		= Input_Customer.[Br Nbr]
FROM Input_Customer
WHERE Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/*Add New Customers  -- based on INPUT_TRANSACTION                                                     */
INSERT INTO Customer_Stage
(
	TIPNUMBER, 
	TIPFIRST, 
	TIPLAST, 
--	LASTNAME,
	ACCTNAME1, 
	ACCTNAME2, 
	ACCTNAME3, 
	ADDRESS1, 
	ADDRESS2, 
	ADDRESS4,
	CITY, 
	STATE, 
	ZIPCODE , 
--	EMPLOYEEFLAG, 
	DATEADDED, 
--	STATUS, 
	MISC2, 
	RUNAVAILABLE, 
	RUNBALANCE, 
	RUNREDEEMED, 
	RunAvaliableNew
)
SELECT 
	DISTINCT TIPNUMBER, 
	LEFT(TIPNUMBER,3), 
	RIGHT(RTRIM(TIPNUMBER),6), 
--	LEFT(RTRIM(LASTNAME),40),
	LEFT(RTRIM(Input_Customer.[Nm Line 1]),40) , 
	LEFT(RTRIM(Input_Customer.[Nm Line 2]),40) , 
	LEFT(RTRIM(Input_Customer.[Nm Line 3]),40) , 
	LEFT(RTRIM([Addr Line 1]),40), 
	LEFT(RTRIM([Addr Line 2]),40), 
	LEFT( LTRIM(RTRIM(City)) + ' ' + LTRIM(RTRIM([St Cd])) + ' ' + LTRIM( RTRIM([Zip Cd])),40),
	[City], 
	LEFT([St Cd],2), 
	RTRIM([Zip Cd]),
--	[Acct Status Flg], 
	@EndDate, 
--	[Acct Stat Cd],  
--	[Acct Status Flg], 
	[Br Nbr]
	,0
	,0
	,0
	,0
	FROM  Input_Customer 
	WHERE 1=1
		AND Input_Customer.tipnumber NOT IN (SELECT TIPNUMBER FROM Customer_Stage)
		AND Input_Customer.tipnumber IN (SELECT TIPNUMBER FROM Input_Transaction)
		AND Input_Customer.[Acct Stat Cd] IN ('Y','N','R')

/*Add New Customers  -- based on OPEN_DATE                                                     */

INSERT INTO Customer_Stage
(
	TIPNUMBER, 
	TIPFIRST, 
	TIPLAST, 
--	LASTNAME,
	ACCTNAME1, 
	ACCTNAME2, 
	ACCTNAME3, 
	ADDRESS1, 
	ADDRESS2, 
	ADDRESS4,
	CITY, 
	STATE, 
	ZIPCODE , 
--	EMPLOYEEFLAG, 
	DATEADDED, 
--	STATUS, 
	MISC2, 
	RUNAVAILABLE, 
	RUNBALANCE, 
	RUNREDEEMED, 
	RunAvaliableNew
)
SELECT 
	DISTINCT TIPNUMBER, 
	LEFT(TIPNUMBER,3), 
	RIGHT(RTRIM(TIPNUMBER),6), 
--	LEFT(RTRIM(LASTNAME),40),
	LEFT(RTRIM(Input_Customer.[Nm Line 1]),40) , 
	LEFT(RTRIM(Input_Customer.[Nm Line 2]),40) , 
	LEFT(RTRIM(Input_Customer.[Nm Line 3]),40) , 
	LEFT(RTRIM([Addr Line 1]),40), 
	LEFT(RTRIM([Addr Line 2]),40), 
	LEFT( LTRIM(RTRIM(City)) + ' ' + LTRIM(RTRIM([St Cd])) + ' ' + LTRIM( RTRIM([Zip Cd])),40),
	[City], 
	LEFT([St Cd],2), 
	RTRIM([Zip Cd]),
--	[Acct Status Flg], 
	@EndDate, 
--	[Acct Stat Cd],  
--	[Acct Status Flg], 
	[Br Nbr]
	,0
	,0
	,0
	,0
	FROM  Input_Customer 
	WHERE 1=1
		AND Input_Customer.TIPNUMBER NOT IN (SELECT TIPNUMBER FROM Customer_Stage)
		AND Input_Customer.[Open Date] BETWEEN DATEADD(m,-1,DATEADD(d, 1, @EndDate)) AND @EndDate
		AND Input_Customer.[Card Stat Cd] = 'O'
		AND Input_Customer.[Acct Stat Cd] IN ('Y','N','R')

/*Add New Customers  -- based on LEGACY                                                     */

INSERT INTO Customer_Stage
(
	TIPNUMBER, 
	TIPFIRST, 
	TIPLAST, 
--	LASTNAME,
	ACCTNAME1, 
	ACCTNAME2, 
	ACCTNAME3, 
	ADDRESS1, 
	ADDRESS2, 
	ADDRESS4,
	CITY, 
	STATE, 
	ZIPCODE , 
--	EMPLOYEEFLAG, 
	DATEADDED, 
--	STATUS, 
	MISC2, 
	RUNAVAILABLE, 
	RUNBALANCE, 
	RUNREDEEMED, 
	RunAvaliableNew
)
SELECT 
	DISTINCT TIPNUMBER, 
	LEFT(TIPNUMBER,3), 
	RIGHT(RTRIM(TIPNUMBER),6), 
--	LEFT(RTRIM(LASTNAME),40),
	LEFT(RTRIM(Input_Customer.[Nm Line 1]),40) , 
	LEFT(RTRIM(Input_Customer.[Nm Line 2]),40) , 
	LEFT(RTRIM(Input_Customer.[Nm Line 3]),40) , 
	LEFT(RTRIM([Addr Line 1]),40), 
	LEFT(RTRIM([Addr Line 2]),40), 
	LEFT( LTRIM(RTRIM(City)) + ' ' + LTRIM(RTRIM([St Cd])) + ' ' + LTRIM( RTRIM([Zip Cd])),40),
	[City], 
	LEFT([St Cd],2), 
	RTRIM([Zip Cd]),
--	[Acct Status Flg], 
	@EndDate, 
--	[Acct Stat Cd],  
--	[Acct Status Flg], 
	[Br Nbr]
	,0
	,0
	,0
	,0
	FROM  Input_Customer 
	WHERE 1=1
		AND Input_Customer.TIPNUMBER NOT IN (SELECT TIPNUMBER FROM Customer_Stage)
		AND Input_Customer.Legacy IS NULL

/* set Default status to A */
UPDATE Customer_Stage
SET STATUS = 'A' 
WHERE STATUS IS NULL 

UPDATE Customer_Stage
SET Status = a.[Acct Stat Cd] 
FROM Input_Customer a
WHERE customer_stage.tipnumber = a.tipnumber

/*
should be part of the export to web, not data process
but has been put back into the dataprocess to keep in line with 
points now

Y = active card
N = card not activated
R = pre-active card

*/

UPDATE Customer_Stage
SET status = 'A'
WHERE status IN ('Y','N','R')

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
UPDATE Customer_Stage
SET StatusDescription = 
S.StatusDescription 
FROM status S INNER JOIN Customer_Stage C on S.Status = C.Status

/* Move Address2 to address1 if address1 is null */
UPDATE Customer_Stage 
SET 
Address1 = Address2, 
Address2 = NULL
WHERE address1 IS NULL

UPDATE Customer_Stage
SET ZipCode = '0' + ZipCode
WHERE len(zipcode) = 4

UPDATE Customer_Stage
SET Address4 = City + ' ' + State + ' ' + ZipCode
GO
