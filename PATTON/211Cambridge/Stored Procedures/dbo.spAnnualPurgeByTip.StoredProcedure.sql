USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spAnnualPurgeByTip]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAnnualPurgeByTip] as

DECLARE @DateDeleted datetime
declare @SQLIf nvarchar(1000)
declare @tipnumber nvarchar(15)
declare @acctid nvarchar(16)
declare @custid nvarchar(13)
set @custid = '0000000000000'


SET @DateDeleted = '12-30-2009'

-- DROP WORK TABLE IF IT EXISTS

	set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''zzPurgeByTip'')
		Begin
			TRUNCATE TABLE .dbo.zzPurgeByTip 
		End '
	exec sp_executesql @SQLIf
	
	select tipnumber,acctid into zzPurgeByTip 
	from AFFILIAT where TIPNUMBER in (select  distinct(tipnumber)  from PURGE_TIP)

-- UPDATE STATUS OF CUSTOMERS TO BE DELETED TO '9'. Customer is deleted LAST

	update Customer 
	set Status='9'
	where tipnumber  in 
		(select  distinct(tipnumber)  from PURGE_TIP)


-- HISTORY PROCESSING (INSERT RECORDS TO BE DELETED INTO DELETION FILES)


	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history  
	where tipnumber in 
	(select tipnumber from customer  where status = '9')
	
	
	/*  **************************************  */
	-- Delete History
	Delete from history  where tipnumber in (select tipnumber from customer  where status = '9')

-- AFFILIAT PROCESSING (INSERT RECORDS TO BE DELETED INTO DELETION FILES) 

	update Affiliat 	
	set Acctstatus = '9'
	where acctid in ( select acctid from zzPurgeByTip) 

	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat  
	 WHERE  affiliat .acctstatus = '9'
	
	/*  **************************************  */
	Delete from affiliat  where acctstatus = '9'
	
-- PROCESS CUSTOMER (INSERT RECORDS TO BE DELETED INTO DELETION FILES)

	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,' ',RunAvaliableNew, @DateDeleted 
	from customer  WHERE  status = '9'


	-- Delete CUSTOMER
	
	Delete from customer  where status = '9'
GO
