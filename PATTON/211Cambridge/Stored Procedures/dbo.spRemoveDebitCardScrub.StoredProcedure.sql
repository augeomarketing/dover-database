USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveDebitCardScrub]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Allen Barriere
-- Create date: 20090420
-- Description:	Scrub Remove Debit Cards
-- =============================================
CREATE PROCEDURE [dbo].[spRemoveDebitCardScrub]
AS
BEGIN
  INSERT INTO input_deleteDebitCard_error (acctid, reason)
  SELECT acctid, 'Card Not Found' 
    FROM input_deleteDebitCard idd
    WHERE idd.acctid NOT IN (SELECT acctid FROM affiliat)
  
  DELETE FROM input_deleteDebitCard
    WHERE acctid NOT IN (SELECT acctid FROM affiliat)
  
  --Don't Delete "Last Card" ... if only 1 card exists in Affiliat for TIP, don't delete it
  UPDATE affiliat 
  SET secid = 'D'
  WHERE acctid IN (SELECT acctid FROM input_deletedebitcard)

  INSERT INTO input_deleteDebitCard_error (acctid, reason)
  SELECT DISTINCT idd.acctid, 'Last Card(s) in System' 
    FROM affiliat a
      INNER JOIN input_deleteDebitCard idd
        ON a.acctid = idd.acctid
    WHERE a.tipnumber IN (
                          SELECT t.tipnumber
                            FROM affiliat t
                            WHERE secid = 'D'
                            GROUP BY tipnumber, secid
                            HAVING COUNT(DISTINCT acctid) = (SELECT COUNT(DISTINCT acctid) FROM affiliat WHERE tipnumber = t.tipnumber)
        )

  DELETE FROM input_deleteDebitCard
    WHERE acctid IN (  
        SELECT idd.acctid 
          FROM affiliat a 
            INNER JOIN input_deleteDebitCard idd 
              ON a.acctid = idd.acctid 
          WHERE a.tipnumber IN ( SELECT t.tipnumber
                                    FROM affiliat t
                                    WHERE secid = 'D'
                                    GROUP BY tipnumber, secid 
                                    HAVING COUNT(DISTINCT acctid) = (SELECT COUNT(DISTINCT acctid) FROM affiliat WHERE tipnumber = t.tipnumber)))

END
GO
