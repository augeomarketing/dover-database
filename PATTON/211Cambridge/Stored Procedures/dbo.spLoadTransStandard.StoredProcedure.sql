USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*
****************************************************************************
    Load the TransStandard Table from the Transaction_input table
****************************************************************************
*/
CREATE   PROCEDURE [dbo].[spLoadTransStandard] @DateAdded CHAR(10) AS

-- Clear TransStandard 
TRUNCATE TABLE TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
INSERT TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
SELECT  [TipNumber], @DateAdded, [Card Nbr], [TranCode], Sum( Convert(int, [SUM Sum Trn Cnt] ) ) , CONVERT(CHAR(15), Sum(Convert ( int, [Points])) ) 
FROM Input_Transaction 
Group by [TipNumber], [Card Nbr], [TranCode]

-- Set the TranType to the Description found in the RewardsNow.TranCode table
UPDATE TransStandard 
  SET TranType = LTRIM(RTRIM(R.[Description])),
      Ratio = R.Ratio
  FROM RewardsNow.dbo.Trantype R 
    INNER JOIN TransStandard T 
      ON R.TranCode = T.Trancode
GO
