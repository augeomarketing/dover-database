USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[sp211StatementPlusReport]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Paul Butler / Allen Barriere
-- Create date: 07/21/2008
-- Description:	Custom Report for Cambridge to show DDA / Card Numbers for each Member, as well as 
--				the monthly / quarterly statement data
-- =============================================
CREATE PROCEDURE [dbo].[sp211StatementPlusReport] @startdate CHAR(10), @enddate CHAR(10)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @tip				VARCHAR(15)
	DECLARE @maxAccountsPerTip	INT
	DECLARE @sql				NVARCHAR(4000)
	DECLARE @ctr				INT
	DECLARE @hldacctnbr			CHAR(25)
	DECLARE @acctid			CHAR(25)
	DECLARE @DDANbr			char(25)

----------------------------------------------------------------------------------
-- Find the maximum number of accounts assigned to a tip
-- Scan the entire table, so that the report that invokes this function
-- returns a consistent number of columns for each tip reported
----------------------------------------------------------------------------------
-- Note, @tip is specified here because its needed by the group by. 
	SELECT distinct TOP 1 @tip = tipnumber, @maxAccountsPerTip = COUNT(distinct acctid)
	FROM dbo.affiliat
	GROUP BY tipnumber
	ORDER BY COUNT(distinct acctid) DESC

-- if temp table already exists, delete it
	IF EXISTS(SELECT [name] FROM dbo.sysobjects WHERE name LIKE '__tmp_AccountsByTip%')
		DROP TABLE dbo.__tmp_AccountsbyTip

-- create the shell of the table
	CREATE TABLE dbo.__tmp_AccountsbyTip
	(
		TipNumber			VARCHAR(15) PRIMARY KEY,
		ACCTNAME1			VARCHAR(40),
		ACCTNAME2			VARCHAR(40),
		ACCTNAME3			VARCHAR(40),
		ACCTNAME4			VARCHAR(40),
		ACCTNAME5			VARCHAR(40),
		ACCTNAME6			VARCHAR(40),
		ADDRESS1			VARCHAR(40),
		ADDRESS2			VARCHAR(40),
		CITY				VARCHAR(40),
		State			VARCHAR(2),
		ZipCode			VARCHAR(15),
		BeginningPoints	INT,
		EndingPointBalance	INT,
		DebitPurchase		INT,
		DebitBonus		INT,
		DebitReturn		INT,
		Redeemed			INT,
		Adjustments		INT,
		emailaddress		VARCHAR(40),
		emailstatement		CHAR(1)
	)
		

-- initialize the dynamic sql to issue an ALTER TABLE
	SET @sql = 'ALTER TABLE __tmp_AccountsbyTip ADD '

-- Build the necessary number of AcctID columns via dynamic sql
	SET @ctr = 1
	WHILE @ctr <= @maxAccountsPerTip
	BEGIN
		IF @ctr > 1
			SET @sql = @sql + ', '

		SET @sql = @sql + ' AcctId' + RIGHT('000' + CAST(@ctr AS NVARCHAR(3)), 3) + '   NVARCHAR(25), '
		set @sql = @SQL + ' DDANbr' + right('000' + cast(@ctr as nvarchar(3)), 3) + '   NVARCHAR(25) '
		SET @ctr = @ctr + 1
	END

-- alter the temp table
	EXEC sp_executesql @sql

----------------------------------------------------------------------------------
-- Now populate the temp table
----------------------------------------------------------------------------------
	INSERT INTO dbo.__tmp_AccountsbyTip
	(	
		tipnumber, 
		ACCTNAME1, 
		ACCTNAME2, 
		ACCTNAME3, 
		ACCTNAME4, 
		ACCTNAME5, 
		ACCTNAME6, 
		ADDRESS1, 
		ADDRESS2,
		City, 
		State, 
		ZipCode,
		BeginningPoints,
		EndingPointBalance,
		DebitPurchase,
		DebitBonus,
		DebitReturn,
		Redeemed,
		Adjustments,
		emailaddress,
		emailstatement
	)
	SELECT 
		c.tipnumber, 
		c.ACCTNAME1, 
		c.ACCTNAME2, 
		c.ACCTNAME3, 
		c.ACCTNAME4, 
		c.ACCTNAME5, 
		c.ACCTNAME6, 
		c.ADDRESS1, 
		c.ADDRESS2,
		c.City, 
		c.State, 
		c.ZipCode,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate < @startdate) AS BeginningPoints,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate <= @enddate) AS EndingPointBalance,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate BETWEEN @startdate AND @enddate AND trancode IN ('37','67')) AS DebitPurchase,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate BETWEEN @startdate AND @enddate AND trancode LIKE ('B%')) AS Bonus,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate BETWEEN @startdate AND @enddate AND trancode IN ('33','63')) AS Returns,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate BETWEEN @startdate AND @enddate AND trancode LIKE ('R%')) AS Redeemed,
		(SELECT SUM(points*ratio) FROM dbo.history h WHERE h.tipnumber = c.tipnumber AND histdate BETWEEN @startdate AND @enddate AND trancode IN ('IE', 'DE')) AS Adjustments,
		COALESCE(rn.email,'no email provided') AS emailaddress,
		COALESCE(rn.emailstatement,'n') AS emailstatment
	FROM dbo.customer c LEFT OUTER JOIN rn1.cambridge.dbo.[1security] rn ON c.tipnumber = rn.tipnumber

--SELECT GETDATE() AS Cursor_Start_Time
-- Loop through customer & affiliat table by tip number.  this will be used to get all of the account #s for each tip
	DECLARE csrAccts CURSOR FAST_FORWARD FOR
	SELECT cus.tipnumber 
		FROM dbo.customer cus 
			JOIN dbo.affiliat aff
				ON cus.tipnumber = aff.tipnumber
		ORDER BY cus.tipnumber

	OPEN csrAccts
	FETCH next FROM csrAccts INTO @Tip

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @ctr = 1  -- set counter to 1, will be used to increment up to the maxAccountsPerTip
		SET @hldacctnbr = '0' -- initialize holdacctnbr - used to prevent dupe acct nbrs in the result set
		WHILE @ctr <= @maxAccountsPerTip
		BEGIN
			-- get the next account number for the tip specified from the cursor
			SET @acctid = (SELECT TOP 1 acctid FROM dbo.affiliat WHERE tipnumber = @tip AND acctid > @hldacctnbr ORDER BY acctid)
			SET @DDANbr = (SELECT TOP 1 custid FROM dbo.affiliat WHERE tipnumber = @tip AND acctid > @hldacctnbr ORDER BY acctid)
			-- if there is an account number - update the temp table
			IF @acctid IS NULL
			BEGIN
				SET @ctr = @maxAccountsPerTip  -- do this to kick out of the loop
			END
			ELSE
			BEGIN
				-- build dynamic sql statement for update.  the acctid column is dynamically built in the update stmt
				-- based on the counter.  this is how I pivot the account numbers from a table set into a row
				SET @sql =	'UPDATE dbo.__tmp_AccountsbyTip SET acctid' + RIGHT('000' + CAST(@ctr AS NVARCHAR(3)), 3) + ' =  ' + CHAR(39) + @acctid + CHAR(39) + ', ' +
							'  DDANbr' + right('000' + cast(@ctr as nvarchar(3)), 3) + ' =  ' + char(39) + @DDANbr + char(39) + 
							'  where tipnumber = ' + CHAR(39) + @tip + CHAR(39)
				--print @sql
				EXEC sp_executesql @sql
				-- update the hold acct number column to the last used acct number.  used to prevent dupe acct numbers
				-- in the result set
				SET @hldacctnbr = @acctid
			END
			SET @ctr = @ctr + 1
		END
		FETCH next FROM csrAccts INTO @Tip
	END
	CLOSE csrAccts
	DEALLOCATE csrAccts
--SELECT getdate() as Cursor_END_Time
END
GO
