USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 03/01/2011 13:18:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
****************************************************************************
    This scrubs (cleans up) the inport_Customer and Input_Transaction tables
**************************************************************************** 
*/	
CREATE       PROCEDURE [dbo].[spInputScrub] AS

DECLARE @TipFirst CHAR(3)
SELECT @TipFirst = tipfirst FROM Client

TRUNCATE TABLE  Input_Customer_error
TRUNCATE TABLE Input_Transaction_error

/*  UPDATE DDA Replacements */
UPDATE affiliat_stage 
SET custid = input_customer.[Acct Nbr]
  FROM affiliat_stage 
    INNER JOIN input_customer
	    ON affiliat_stage.[acctid] = input_customer.[card nbr]
WHERE [replace dda] = 'Y'

/* Remove Empty Rows */
DELETE FROM Input_Customer
WHERE [Acct Nbr] IS NULL

/* Remove input_customer records with Member Name null */
INSERT INTO Input_customer_error 
SELECT *, 'Member Name not Specified' 
FROM Input_Customer 
WHERE [Mbr Nm] IS NULL

DELETE FROM Input_Customer WHERE [Mbr Nm] IS NULL

INSERT INTO Input_customer_error 
SELECT *, 'Acct Nbr Different For Existing Card Nbr'
FROM Input_Customer a
WHERE [Card Nbr] IN (SELECT [Card Nbr] FROM affiliat_stage b WHERE a.[Card Nbr] = b.[AcctId] AND a.[Acct Nbr] <> b.[CustId]  )

DELETE FROM Input_Customer WHERE [Card Nbr] IN 
(SELECT [Card Nbr] FROM Input_Customer a WHERE [Card Nbr] IN (SELECT DISTINCT [Card Nbr] FROM affiliat_stage b WHERE a.[Card Nbr] = b.[AcctId] AND a.[Acct Nbr] <> b.[CustId]  ))

INSERT INTO Input_customer_error
SELECT *, 'Customer Opt Out Request'
FROM Input_Customer a
WHERE [Acct Nbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

DELETE FROM Input_Customer 
WHERE [Acct Nbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

/* remove leading and trailing spaces in names and addresses */ 
UPDATE Input_Customer 
	SET
	[Mbr Nm] = LTRIM(RTRIM([Mbr Nm])) , 
	[Card Nbr] = LTRIM(RTRIM([Card Nbr])) , 
	[Nm Line 1] = LTRIM(RTRIM([Nm Line 1])) , 
	[Nm Line 2] = LTRIM(RTRIM([Nm Line 2])) , 
	[Nm Line 3] = LTRIM(RTRIM([Nm Line 3])) , 
	[Addr Line 1] = LTRIM(RTRIM([Addr Line 1])) , 
	[Addr Line 2] = LTRIM(RTRIM([Addr Line 2])) , 
	[City] = LTRIM(RTRIM([City])),
	[St Cd] = LTRIM(RTRIM([St Cd])),
	[Zip Cd] = LTRIM(RTRIM([Zip Cd]))

INSERT INTO Input_Transaction_error 
SELECT *, 'Customer DDA Not In Member File' 
FROM Input_Transaction 
WHERE [Primary Acct Nbr] NOT IN (SELECT [Acct Nbr] FROM Input_Customer) 

DELETE FROM Input_Transaction 
WHERE [Primary Acct Nbr] NOT IN (SELECT [Acct Nbr] FROM Input_Customer) 

INSERT INTO Input_Transaction_error
SELECT *, 'Multiple Primary Acct Nbr for Card Nbr'
FROM Input_Transaction
WHERE [Card Nbr] IN (SELECT [Card Nbr] FROM input_transaction GROUP BY [card nbr] HAVING COUNT(DISTINCT([Primary Acct Nbr])) > 1)

DELETE FROM Input_Transaction WHERE [Card Nbr] IN (SELECT [Card Nbr] FROM input_transaction GROUP BY [card nbr] HAVING COUNT(DISTINCT([Primary Acct Nbr])) > 1)

INSERT INTO Input_Transaction_error
SELECT *, 'Primary Acct Nbr Different For Existing Card Nbr'
FROM Input_Transaction a
WHERE [Card Nbr] IN (SELECT [Card Nbr] FROM affiliat_stage b WHERE a.[Card Nbr] = b.[AcctId] AND a.[Primary Acct Nbr] <> b.[CustId]  )

DELETE FROM Input_Transaction WHERE [Card Nbr] IN 
(SELECT [Card Nbr] FROM input_transaction a WHERE [Card Nbr] IN (SELECT DISTINCT [Card Nbr] FROM affiliat_stage b WHERE a.[Card Nbr] = b.[AcctId] AND a.[Primary Acct Nbr] <> b.[CustId]  ))

INSERT INTO Input_Transaction_error
SELECT *, 'Customer Opt Out Request'
FROM Input_Transaction a
WHERE [Primary Acct Nbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

DELETE FROM Input_Transaction 
WHERE [Primary Acct Nbr] IN (SELECT [AcctId] FROM RewardsNOW.dbo.OptOutTracking WHERE TipPrefix = @tipFirst)

--- Delete Duplicate Transactions from input_transaction if the purchcnt = 0 
DELETE FROM Input_Transaction 
	WHERE [Card Nbr] IN ( SELECT [Card Nbr] FROM Input_Transaction GROUP BY [Card Nbr] HAVING COUNT(*) > 1) 
	AND [SUM Sum Trn Cnt] = 0 
/*
--AGB 20100407
--No longer necessary, ALL unknown NETWORK ID / PROCESS TYP CD combinations are to be treated as DEBIT/PIN

INSERT INTO Input_Transaction_error
SELECT *, 'Network Id Not Recognized'
FROM Input_Transaction a
WHERE [Network Id] NOT IN (SELECT [Network Id] FROM aux_trancode)

DELETE FROM Input_Transaction 
	WHERE [Network Id] NOT IN (SELECT [Network Id] FROM aux_trancode)

INSERT INTO Input_Transaction_error
SELECT *, 'Process Typ Cd Not Recognized'
FROM Input_Transaction a
WHERE [Process Typ Cd] NOT IN (SELECT [Process Typ Cd] FROM aux_trancode)

DELETE FROM Input_Transaction 
	WHERE [Process Typ Cd] NOT IN (SELECT [Process Typ Cd] FROM aux_trancode)


INSERT INTO Input_Transaction_error
SELECT *, 'Network Id / Process Typ Cd Combination Error'
FROM Input_Transaction a
  WHERE LTRIM(RTRIM([Process Typ Cd])) + '' + LTRIM(RTRIM([Network Id])) NOT IN (SELECT LTRIM(RTRIM([Process Typ Cd])) + '' + LTRIM(RTRIM([Network Id])) FROM aux_trancode)

DELETE FROM Input_Transaction 
	WHERE LTRIM(RTRIM([Process Typ Cd])) + '' + LTRIM(RTRIM([Network Id])) NOT IN (SELECT RTRIM(LTRIM([Process Typ Cd])) + '' +  LTRIM(RTRIM([Network Id])) FROM aux_trancode)

*/
/* Replace null with 0 on purhase and trancounts in transaction */
UPDATE Input_Transaction 
SET [SUM Sum Trn Cnt]  = 0 
WHERE [SUM Sum Trn Cnt] IS NULL

/* Round the amounts */
UPDATE Input_Transaction 
SET [SUM Sum Tran Amt] = ROUND([SUM Sum Tran Amt],0)

-- Convert Trancodes to Product Codes
UPDATE Input_Transaction 
	SET TranCode = aux_trancode.trancode
	FROM
		aux_trancode INNER JOIN Input_Transaction
		ON aux_trancode.[Network ID] = Input_Transaction.[Network Id]
			AND aux_trancode.[Process Typ Cd] = Input_Transaction.[Process Typ Cd]
--AGB 20100407
--ALL unknown NETWORK ID / PROCESS TYP CD combinations are to be treated as DEBIT/PIN (RT #174108)
UPDATE Input_Transaction
  SET TranCode = '37'
    WHERE TranCode IS NULL
		AND Input_Transaction.[Process Typ Cd] = '20'

UPDATE Input_Transaction
  SET TranCode = '67'
    WHERE TranCode IS NULL
GO
