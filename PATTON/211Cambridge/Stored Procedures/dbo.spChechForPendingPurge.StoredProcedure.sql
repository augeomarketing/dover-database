USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spChechForPendingPurge]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spChechForPendingPurge]  AS 
declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)
declare @rundate nvarchar(10)
set @rundate = '2009-12-31'

		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''Account1'')
		Begin
			drop TABLE .dbo.Account1
		End '
		exec sp_executesql @SQLIf 
		
		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''Account2'')
		Begin
			drop TABLE .dbo.Account2
		End '
		exec sp_executesql @SQLIf 
		
		
		set @SQLIf=N'if exists(select * from .dbo.sysobjects where xtype=''u'' and name = ''AccountDeleteInput'')
		Begin
			truncate TABLE .dbo.AccountDeleteInput
		End '
		exec sp_executesql @SQLIf 

 
 insert into AccountDeleteInput
 (acctid ,dda)
 select acctid,'0' from AFFILIAT where TIPNUMBER in (select TIPNUMBER from dbo.PURGE_TIP)
 
 
 insert into AccountDeleteInput
 (acctid,dda)
 select acctid,'0' from AFFILIAT where custid in (select dda from dbo.PURGE_DDA)
 
  insert into AccountDeleteInput
 (acctid,dda)
 select acctid,'0' from AFFILIAT where acctid in (select debacctnum from dbo.PURGE_DEBIT)
GO
