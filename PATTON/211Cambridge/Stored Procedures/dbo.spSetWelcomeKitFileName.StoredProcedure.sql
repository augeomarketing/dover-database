USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE  PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

DECLARE @Filename CHAR(50), @currentdate NCHAR(4), @workmonth NCHAR(2), @workyear NCHAR(2), @endingDate CHAR(10), @totalNumber INT

SET @endingDate=(SELECT TOP 1 datein FROM DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day

SET @workmonth = LEFT(@endingdate,2)
SET @workyear = RIGHT(@endingdate,2)

SET @currentdate = @workmonth + @workyear

SELECT 	@totalNumber = count(*)
FROM customer WHERE (YEAR(DATEADDED) = YEAR(@endingDate) AND MONTH(DATEADDED) = MONTH(@endingDate) AND UPPER(STATUS) <> 'C') 


SET @filename = 'W' + @TipPrefix + @currentdate + '_' + CONVERT(VARCHAR(10),@totalNumber) + '.xls'
SET @newname = 'O:\' + @TipPreFix + '\Output\WelcomeKit\Welcome.bat ' + @filename
SET @ConnectionString = 'O:\' + @TipPrefix + '\Output\WelcomeKit\' + @filename
GO
