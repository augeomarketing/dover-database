USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE    PROCEDURE [dbo].[spStageMonthlyStatement] @StartDateParm CHAR(10), @EndDateParm CHAR(10)
AS 

DECLARE  @MonthBegin CHAR(2),  @SQLUpdate NVARCHAR(1000)
DECLARE @StartDate DATETIME
DECLARE @EndDate DATETIME
SET @Startdate = CONVERT(DATETIME, @StartDateParm + ' 00:00:00:001')
SET @Enddate = CONVERT(DATETIME, @EndDateParm + ' 23:59:59:998' )

SET @MonthBegin = MONTH(CONVERT(DATETIME, @StartDate) )

/* Load the statement file FROM the customer table  */
TRUNCATE TABLE Monthly_Statement_File

INSERT INTO Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
SELECT tipnumber, acctname1, acctname2, address1, address2, address3, (RTRIM(city) + ' ' + RTRIM(state) + ' ' + zipcode)
FROM customer_Stage

/* Load the statmement file with CREDIT purchases          */
UPDATE Monthly_Statement_File
SET pointspurchasedCR =	ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate<=@enddate AND trancode='63'),0),
	pointsreturnedCR =	ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='33'), 0),
	pointspurchasedDB = ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode = '67'), 0),
	pointsreturnedDB =	ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='37'), 0),
	pointsbonus =		ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND (LEFT(trancode,1) = 'B' OR LEFT(trancode,1) = 'F' OR LEFT(trancode,1) = 'G') ), 0),
	pointsadded =		ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='IE'),0)
						+ ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber = Monthly_Statement_File.tipnumber AND histdate >= @startdate AND histdate <= @enddate AND trancode='DR'),0),
	pointsredeemed =	ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND LEFT(trancode,1) = 'R'),0),
	pointssubtracted =	ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='DE'),0) 
						 + ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='IR'),0)
						 + ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='EP'),0),
	PointsExpire =		ISNULL((SELECT SUM(points) FROM History_Stage WHERE tipnumber=Monthly_Statement_File.tipnumber AND histdate>=@startdate AND histdate<=@enddate AND trancode='XP'),0)

/* Load the statmement file with total point increases */
UPDATE Monthly_Statement_File
SET pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded, 
	pointsdecreased= pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire

/* Load the statmement file with the Beginning balance for the Month */
SET @SQLUpdate=N'UPDATE Monthly_Statement_File
SET pointsbegin=(SELECT monthbeg'+ @MonthBegin + N' FROM Beginning_Balance_Table WHERE tipnumber=Monthly_Statement_File.tipnumber)
WHERE EXISTS(SELECT 1 FROM Beginning_Balance_Table WHERE tipnumber=Monthly_Statement_File.tipnumber)'

EXEC sp_executesql @SQLUpdate

UPDATE  Monthly_Statement_File
SET pointsend = pointsbegin + pointsincreased - pointsdecreased

UPDATE Monthly_Statement_file 
SET acctid = a.acctid FROM affiliat_stage a WHERE Monthly_Statement_file.tipnumber = a.tipnumber
GO
