USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spAnnualPurgeByDDA]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spAnnualPurgeByDDA] as


DECLARE @DateDeleted datetime
declare @SQLIf nvarchar(1000)
declare @tipnumber nvarchar(15)
declare @acctid nvarchar(16)
declare @custid nvarchar(13)
set @custid = '0000000000000'


SET @DateDeleted = '12-30-2009'

-- DROP WORK TABLE IF IT EXISTS

	set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''zzPurgeByDDA'')
		Begin
			drop table .dbo.zzPurgeByDDA 
		End '
	exec sp_executesql @SQLIf
	
	select tipnumber,acctid,custid into zzPurgeByDDA 
	from AFFILIAT where CUSTID in (select  distinct(DDA)  from PURGE_DDA)



-- AFFILIAT PROCESSING (INSERT RECORDS TO BE DELETED INTO DELETION FILES) 

	update Affiliat 	
	set Acctstatus = '9'
	where acctid in ( select acctid from zzPurgeByDDA) 

	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat  
	 WHERE  affiliat .acctstatus = '9'
	
	/*  **************************************  */
	Delete from affiliat  where acctstatus = '9'
	
	
-- HISTORY PROCESSING (INSERT RECORDS TO BE DELETED INTO DELETION FILES)


	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history  
	where tipnumber NOT in 
	(select tipnumber from AFFILIAT)
	
	
	/*  **************************************  */
	-- Delete History
	Delete from history  where tipnumber NOT in (select tipnumber from AFFILIAT)	
	
-- PROCESS CUSTOMER (INSERT RECORDS TO BE DELETED INTO DELETION FILES)

	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,' ',RunAvaliableNew, @DateDeleted 
	from customer   
	where tipnumber NOT in 
	(select tipnumber from AFFILIAT)

	-- Delete CUSTOMER
	
	Delete from customer  where tipnumber NOT in (select tipnumber from AFFILIAT)
GO
