USE [211Cambridge]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateLastname]    Script Date: 03/01/2011 13:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Barriere,Allen
-- Create date: 20110228
-- Description:	Lastname parser, remove reliance on Personator
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateLastname]
AS
BEGIN
	SET NOCOUNT ON;

	update CUSTOMER
		set lastname = RIGHT(rtrim(replace(replace(replace(replace(replace(ACCTNAME1,' JR', ''), ' SR', ''), ' II', ''), ' III', ''), ' IV','')), len(rtrim(replace(replace(replace(replace(replace(ACCTNAME1,' JR', ''), ' SR', ''), ' II', ''), ' III', ''), ' IV',''))) - RewardsNow.dbo.RAT(' ', rtrim(replace(replace(replace(replace(replace(ACCTNAME1,' JR', ''), ' SR', ''), ' II', ''), ' III', ''), ' IV','')), 1))

	update AFFILIAT
		set LastName = (select LastName from CUSTOMER where TIPNUMBER = AFFILIAT.TIPNUMBER)
		where LastName is null or LastName = ''
END
GO
