USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[CARDSIN2]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CARDSIN2](
	[TIPFIRST] [nvarchar](3) NULL,
	[TIPMID] [nvarchar](7) NULL,
	[TIPLAST] [nvarchar](5) NULL,
	[TIPNUMBER] [nchar](15) NULL,
	[CIFNUMBER] [nvarchar](20) NULL,
	[SSN] [nvarchar](9) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[NA1] [nvarchar](40) NULL,
	[NA2] [nvarchar](40) NULL,
	[NA3] [nvarchar](40) NULL,
	[NA4] [nvarchar](40) NULL,
	[NA5] [nvarchar](40) NULL,
	[NA6] [nvarchar](40) NULL,
	[ADDR1] [nvarchar](40) NULL,
	[ADDR2] [nvarchar](40) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[ZIP] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[PERIOD] [nvarchar](14) NULL,
	[LASTNAME] [nvarchar](30) NULL,
	[Cardtype] [char](1) NULL,
	[joint] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
