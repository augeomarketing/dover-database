USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
