USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[wrktab2]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrktab2](
	[ssn] [nvarchar](9) NULL,
	[na1] [nvarchar](40) NULL,
	[na2] [nvarchar](40) NULL,
	[joint] [varchar](1) NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
