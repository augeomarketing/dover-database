USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[Transum]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transum](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [nchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[NUMPURCH] [nchar](6) NULL,
	[AMTPURCH] [numeric](7, 0) NULL,
	[NUMCR] [nchar](6) NULL,
	[AMTCR] [numeric](7, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [numeric](5, 0) NULL,
	[cardtype] [char](1) NULL,
	[SigvsPin] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [NUMPURCH]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [AMTPURCH]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [NUMCR]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [AMTCR]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [ratio]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [overage]
GO
