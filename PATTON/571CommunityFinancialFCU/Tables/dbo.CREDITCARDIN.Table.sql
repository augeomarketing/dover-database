USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[CREDITCARDIN]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CREDITCARDIN](
	[CIFNUMBER] [nvarchar](20) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[SSN] [nvarchar](9) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[FirstName] [nvarchar](40) NULL,
	[LastName] [nvarchar](40) NULL,
	[ADDR1] [nvarchar](40) NULL,
	[ADDR2] [nvarchar](40) NULL,
	[CITY] [nvarchar](40) NULL,
	[STATE] [nvarchar](2) NULL,
	[ZIP] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[PERIOD] [nvarchar](14) NULL,
	[PrimaryFlag] [nvarchar](1) NULL,
	[Name1] [nvarchar](40) NULL,
	[name2] [nvarchar](40) NULL,
	[name3] [nvarchar](40) NULL,
	[name4] [nvarchar](40) NULL,
	[name5] [nvarchar](40) NULL,
	[name6] [nvarchar](40) NULL,
	[tipfirst] [nchar](3) NULL
) ON [PRIMARY]
GO
