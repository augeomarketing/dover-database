USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[transactionin]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transactionin](
	[ACCTNUM] [nvarchar](25) NULL,
	[Trantype] [nchar](2) NULL,
	[amount] [numeric](18, 2) NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
