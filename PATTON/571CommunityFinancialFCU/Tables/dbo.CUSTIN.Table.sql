USE [571CommunityFinancialFCU]
GO
/****** Object:  Table [dbo].[CUSTIN]    Script Date: 09/25/2009 13:51:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTIN](
	[ACCT_NUM] [nvarchar](25) NULL,
	[NAMEACCT1] [nvarchar](40) NULL,
	[NAMEACCT2] [nvarchar](40) NULL,
	[NAMEACCT3] [nvarchar](40) NULL,
	[NAMEACCT4] [nvarchar](40) NULL,
	[NAMEACCT5] [nvarchar](40) NULL,
	[NAMEACCT6] [nvarchar](40) NULL,
	[STATUS] [nvarchar](1) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[ADDRESS1] [nvarchar](40) NULL,
	[ADDRESS2] [nvarchar](40) NULL,
	[ADDRESS4] [nvarchar](40) NULL,
	[CITY] [nvarchar](38) NULL,
	[STATE] [char](2) NULL,
	[ZIP] [nvarchar](15) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[HOMEPHONE] [nvarchar](10) NULL,
	[WORKPHONE] [nvarchar](10) NULL,
	[DATEADDED] [nvarchar](10) NULL,
	[BUSINESFLG] [nvarchar](1) NULL,
	[SEGCODE] [nvarchar](2) NULL,
	[EMAIL] [nvarchar](40) NULL,
	[CUREOMBAL] [nvarchar](11) NULL,
	[PAYEOMBAL] [nvarchar](11) NULL,
	[FEEEOMBAL] [nvarchar](11) NULL,
	[CRDISUEDT] [nvarchar](10) NULL,
	[CRDACTIVDT] [nvarchar](10) NULL,
	[MISC1] [nvarchar](20) NULL,
	[MISC2] [nvarchar](20) NULL,
	[MISC3] [nvarchar](20) NULL,
	[DDANUM] [nchar](13) NULL,
	[SSN] [nchar](9) NULL,
	[CardType] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
