USE [571CommunityFinancialFCU]
GO
/****** Object:  StoredProcedure [dbo].[spCreditCardProcess]    Script Date: 09/25/2009 13:51:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spCreditCardProcess]
AS


truncate table wrktab1

insert into wrktab1 (CIFNUMBER)
select distinct CIFNUMBER
from creditcardin

update wrktab1
set na1 = rtrim(b.firstName) + ' ' + rtrim(b.lastname)
from wrktab1 a, creditcardin b
where a.cifnumber=b.cifnumber and b.Primaryflag='P' and b.firstname is not null and len(b.firstname)>'0' and len(rtrim(b.firstName) + ' ' + rtrim(lastname))<='40'

update wrktab1
set na1 = rtrim(b.lastname) 
from wrktab1 a, creditcardin b
where a.cifnumber=b.cifnumber and b.Primaryflag='P' and ( (b.firstname is null or len(b.firstname)='0' ) or len(rtrim(b.firstName) + ' ' + rtrim(lastname))>'40')

update wrktab1
set na2 = rtrim(b.firstName) + ' ' + rtrim(b.lastname)
from wrktab1 a, creditcardin b
where a.cifnumber=b.cifnumber and b.Primaryflag='S' and b.firstname is not null and len(b.firstname)>'0' and len(rtrim(b.firstName) + ' ' + rtrim(lastname))<='40'

update wrktab1
set na2 = rtrim(b.lastname) 
from wrktab1 a, creditcardin b
where a.cifnumber=b.cifnumber and b.Primaryflag='S' and ( (b.firstname is null or len(b.firstname)='0' ) or len(rtrim(b.firstName) + ' ' + rtrim(lastname))>'40')

update creditcardin
set name1=b.na1, name2=b.na2, name3=b.na3, name4=b.na4, name5=b.na5, name6=b.na6
from creditcardin a,  wrktab1 b
where a.cifnumber=b.cifnumber 

delete from creditcardin
where PrimaryFlag='S'

INSERT INTO Cardsin(TIPFIRST, CIFNUMBER, SSN, ACCTNUM, NA1, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, Cardtype)
Select tipfirst, cifnumber, ssn, acctnum, Name1, Name2, Name3, Name4, Name5, Name6, addr1, addr2, rtrim(city) + ' ' + State, zip, status, Phone1, Phone2, 'C'
from creditcardin
GO
