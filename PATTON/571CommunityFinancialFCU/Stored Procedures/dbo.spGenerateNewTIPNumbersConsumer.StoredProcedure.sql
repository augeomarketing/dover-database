USE [571CommunityFinancialFCU]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumer]    Script Date: 09/25/2009 13:51:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to employ last tip used instead of max tip
CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumer] @tipfirst char(3)
AS 

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)


/****************       Start SEB001    ******************/
--set @SQLSelect='SELECT @newnum = max(TIPNUMBER)
--		from ' + QuoteName(@DBName) + N'.dbo.affiliat 
--		where left(tipnumber,3)=@TipFirst '
--Exec sp_executesql @SQLSelect, N'@newnum bigint output, @Tipfirst char(3)', @newnum=@newnum output, @TipFirst=@Tipfirst

--if @newnum is null
--	begin
--	set @newnum=@TipFirst + '000000000000'
--	end
--set @newnum = @newnum + 1

declare @LastTipUsed char(15)

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @Newnum = cast(@LastTipUsed as bigint) + 1  
/*********  End SEB001  *************************/

update cardsin
set joint='J'
where NA2 is not null and substring(na2,1,1) not like ' ' and len(rtrim(NA2))>'0'

update cardsin
set joint='S'
where NA2 is null or substring(na2,1,1) like ' '

update cardsin
set NA2=' '
where NA2 is null

delete from cardsin2

insert into cardsin2 
select * from cardsin
order by tipfirst, ssn, na1, na2, joint


drop table wrktab2 

select distinct ssn, na1, na2, joint, tipnumber
into wrktab2
from cardsin2
where tipfirst=@tipFirst and tipnumber is null 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from wrktab2
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @NewTip = Cast(@Newnum as char)
		set @NewTip = replicate('0',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip   /* Added 11/29/2006 */
		
		update wrktab2	
		set tipnumber = @Newtip 
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update cardsin2
set tipnumber=(select tipnumber from wrktab2 where rtrim(ssn)=rtrim(cardsin2.ssn) 
		and rtrim(na1)=rtrim(cardsin2.na1) 
		and rtrim(na2)=rtrim(cardsin2.na2) 
		and rtrim(joint)=rtrim(cardsin2.joint) )
where ssn is not null and tipnumber is null

update cardsin2
set tipnumber=b.tipnumber 
from cardsin2 a, wrktab2 b
where a.ssn is not null and a.tipnumber is null and b.ssn=a.ssn and b.na1=a.na1 and b.na2=a.na2 and b.joint=a.joint

delete from cardsin

insert into cardsin 
select * from cardsin2
order by tipfirst, tipnumber, ssn, na1, na2, joint

exec RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip  /*SEB001 */
GO
