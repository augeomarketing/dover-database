USE [571CommunityFinancialFCU]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransactionData]    Script Date: 09/25/2009 13:51:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spImportTransactionData]  @EndDate char(10)
AS 


declare @sqlinsert varchar(1000), @sqlupdate varchar(1000)
truncate table transum

Update transactionin
set tipnumber = b.tipnumber
from transactionin a, affiliat b
where a.acctnum=b.acctid

update transactionin
set trantype='63'
where trantype='P '

update transactionin
set trantype='33'
where trantype='R '

delete from transactionin
where trantype not in ('33', '63')

delete from transactionin where tipnumber is null


/*********************************************************************/
/*  Get transaction data into a standard format                      */
/*********************************************************************/
insert into transum (tipnumber, acctno, trancode, AMTPURCH)
select distinct tipnumber, acctnum, trantype, amount
from transactionin
group by tipnumber, acctnum, trantype, amount
/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/

update transum
set ratio='1', description='Credit Card Purchase', histdate=@EndDate, NUMPURCH='1' 
where trancode='63'

INSERT INTO history
select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, ' ', ratio, overage
from transum
where trancode='63'


/*********************************************************************/
/*  Process Credit Cards Returns                                     */
/*********************************************************************/

update transum
set ratio='-1', description='Credit Card Returns', histdate=@EndDate, amtcr=AMTPURCH, NUMCR='1', AMTPURCH='0' 
where trancode='33'

INSERT INTO history
select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, ' ', ratio, overage
from transum
where trancode='33' 

delete from transum
where trancode is null

/*********************************************************************/
/*  Update Customer                                                  */
/*********************************************************************/
update customer 
set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber = customer.tipnumber), 
	runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber = customer.tipnumber) 
where exists(select * from transum where tipnumber = customer.tipnumber)
GO
