USE [571CommunityFinancialFCU]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveApostrophesinCardsin]    Script Date: 09/25/2009 13:51:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRemoveApostrophesinCardsin] AS
update cardsin
set NA1=replace(na1,char(39), ' '),na2=replace(na2,char(39), ' '), NA3=replace(NA3,char(39), ' '),NA4=replace(NA4,char(39), ' '), NA5=replace(NA5,char(39), ' '),NA6=replace(NA6,char(39), ' '), lastname=replace(lastname,char(39), ' '), addr1=replace(addr1,char(39), ' '), addr2=replace(addr2,char(39), ' '), citystate=replace(citystate,char(39), ' ')
GO
