USE [571CommunityFinancialFCU]
GO
/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbers]    Script Date: 09/25/2009 13:51:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetExistingTipNumbers] 
AS
 /* Revision                                   */
/* By Sarah Blanchette                         */
/* Date 11/24/2007                             */
/* SCAN  SEB001                                */
/* Remove name check on auto combine per Doug at FBOP   */


/*  Get tipnumber based on account number      */
update cardsin 
set tipnumber=b.tipnumber
from cardsin a, affiliat b
where a.tipnumber is null and a.acctnum=b.acctid 

update cardsin 
set tipnumber=b.tipnumber
from cardsin a, affiliat b
where a.tipnumber is null and a.ssn=b.secid 

delete from cardsin
where tipnumber is null and status<>'A'
GO
