USE [571CommunityFinancialFCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCUSTIN]    Script Date: 09/25/2009 13:51:13 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadCUSTIN] @dateadded char(10)
AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO LOAD CUSTIN TABLE                                            */
/*                                                                            */
/******************************************************************************/

Truncate Table metavantework.dbo.custin

update Cardsin
set CityState=rtrim(replace(CityState,char(44), ''))

update Cardsin
set CityState='..........'
where len(Citystate)='0' or len(rtrim(citystate))<'3'

-- Add to CUSTIN TABLE
INSERT INTO metavantework.dbo.custin (ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, HomePhone, WorkPhone, DateAdded, DDANUM, SSN, Cardtype ) 
select distinct Acctnum, NA1, NA2, NA3, NA4, NA5, NA6, Status, TIPNUMBER, Addr1, Addr2, (rtrim(citystate) + ' ' + rtrim(zip)), left(citystate,(len(citystate)-3)), right(rtrim(CityState),2), ZIP, LASTNAME, Phone1, Phone2, @DateAdded, cifnumber, SSN, Cardtype
from Cardsin
where status<>'C'
order by tipnumber

update metavantework.dbo.custin
set address4=rtrim(replace(address4,char(44), ''))

delete from metavantework.dbo.custin
where tipnumber is null
GO
