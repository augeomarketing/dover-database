USE [MDT]
GO

/****** Object:  View [dbo].[vw_maplogitemsstdfimonthlydata]    Script Date: 12/20/2010 12:26:22 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_maplogitemsstdfimonthlydata]'))
DROP VIEW [dbo].[vw_maplogitemsstdfimonthlydata]
GO

USE [MDT]
GO

/****** Object:  View [dbo].[vw_maplogitemsstdfimonthlydata]    Script Date: 12/20/2010 12:26:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_maplogitemsstdfimonthlydata]
AS
	SELECT
		li.LogItem
		, li.LogItemID
		, li.LogNote
		, li.SeqNum
		, map.sid_maplogitemsstdfimonthlydata_id
		, map.sid_maplogitemsstdfimonthlydata_LogItemID
		, map.dim_maplogitemsstdfimonthlydata_fimonthlydatacolumn
	FROM MDT.dbo.maplogitemsstdfimonthlydata map
	INNER JOIN MDT.dbo.LogItemsSTD li
		ON map.sid_maplogitemsstdfimonthlydata_LogItemID = li.LogItemID

GO


