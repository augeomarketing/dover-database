SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pCheckForHoliday] 
@DateToCheck datetime,
@IsHoliday bit out
AS
Declare @HolidayExists int
select @HolidayExists =count(*) from tHolidays where holiday=@DateToCheck
if @HolidayExists>0  set  @HolidayExists=1
set @IsHoliday=@HolidayExists
GO
