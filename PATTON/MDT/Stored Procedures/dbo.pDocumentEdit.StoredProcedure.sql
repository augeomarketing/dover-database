USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pDocumentEdit]    Script Date: 04/21/2010 13:57:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pDocumentEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pDocumentEdit]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pDocumentEdit]    Script Date: 04/21/2010 13:57:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[pDocumentEdit]
@FormMethod varchar(15) ,
@UserID int=null , 
@DocID int=null , 
@DocTitle varchar(200) ,
@DocUNC varchar(200) ,
@DocText varchar(8000) ,
@DocGroupCodeID int,

@ReturnString varchar(255) output,
@NewPKVal Int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION

declare @DocGroupCodeIDExists int , @CurrDateTime datetime=GetDate()
select @DocGroupCodeIDExists =COUNT(*) from DocsGroups where DocID=@DocID  AND DocGroupCodeID=@DocGroupCodeID 
-------------------------------------Create
if @FormMethod ='CREATE'
begin
Insert into Docs ( DocTitle, DocUNC, DocText, DocGroupCodeID, LastUserID )
	 VALUES(  @DocTitle, @DocUNC, @DocText, @DocGroupCodeID, @UserID)
	SET @NewPKVal = scope_identity()
	set @DocID=@NewPKVal 
	
	if @DocGroupCodeIDExists=0 	
	Insert Into DocsGroups(DocID,DocGroupCodeID)
		 VALUES(   @DocID, @DocGroupCodeID)	
--select * from DocsGroups		 
end
-------------------------------------------
-------------------------------------Update
if @FormMethod ='UPDATE'
Begin
		insert Into Docs_Log( DocID, DocTitle, DocUNC, DocText, DocGroupCodeID, LastUpdated, LastUserID, IsActive)
			select DocID, DocTitle, DocUNC, DocText, DocGroupCodeID, LastUpdated, LastUserID, IsActive
				from Docs where DocID=@DocID
	
		
		Update Docs set 
		DocTitle 	= 	@DocTitle, 
		DocUNC		=	@DocUNC,
		DocText		=	@DocText,
		DocGroupCodeID=@DocGroupCodeID,
		lastupdated	=	@CurrDateTime
		where DocID=	@DocID
		------------------------
		if @DocGroupCodeIDExists=0 	
		Insert Into DocsGroups(DocID,DocGroupCodeID)
			VALUES(   @DocID, @DocGroupCodeID)	
			
			
End
------------------------
if @FormMethod ='DELETE'
	Begin
		DELETE Docs  where DocID	=	@DocID 
		DELETE DocsGroups  where DocID	=	@DocID
		
	End
--------------------------------------------------------------------------

if @FormMethod ='DELETEGROUP'
	Begin
		DELETE DocsGroups  where DocGroupCodeID=@DocGroupCodeID and DocID=@DocID
		and DocGroupCodeID<>(select DocGroupCodeID from Docs where DocID=@DocID)

	End
	
--------------------------------------------------------------------------
if @FormMethod ='DEACTIVATE'
	Begin
		Update Docs set IsActive=0 where DocID=@DocID
	End

--------------------------------------------------------------------------

-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO


