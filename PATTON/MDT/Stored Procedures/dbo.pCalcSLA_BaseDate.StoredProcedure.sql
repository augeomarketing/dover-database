SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pCalcSLA_BaseDate] 
@DBNumber char(3),
@MonthYear nvarchar(7),
@SLADate datetime out
AS
declare @FirstDayOfMonth datetime, @SLA varchar(50), @NumDay int, @X int, @BDCtr int, @Daynum int,@tmpDate datetime
--look up the SLA and NumDay value in FI_SLA
select @NumDay=Numday, @SLA=SLA 
from FI 
where DBNumber=@DBNumber
declare @DD char(2), @MM char(2), @iMM int, @YYYY char(4), @iYYYY int
set @DD='01'
set @MM=substring(@MonthYear,1,2)
set @YYYY=substring(@MonthYear,4,7)
if @MM='12'
	begin
		set @MM='01'
		set @iYYYY=convert(int,@YYYY)
		set @iYYYY=@iYYYY+1
		set @YYYY=convert (char(4),@iYYYY)
	End
else
	begin
		set @iMM=convert(int,@MM)
		set @iMM=@iMM+1
		set @MM=convert(char(2),@iMM)
	end
Set @FirstDayOfMonth= @MM + '/' + @DD + '/' + @YYYY
--Set @FirstDayOfMonth=substring(@MonthYear,1,2) + '/01/' + substring(@MonthYear,4,7)
 
if @SLA='DOM'    --if this is DAY OF THE MONTH --OK
begin
	--go out an additional N-1 days from the first	
	set @SLADate = convert(nvarchar(10),dateadd(d, @NumDay,@FirstDayOfMonth)-1,101)
End
if @SLA='MM'    --if this is MID- MONTH --OK
begin
	--go out an additional N-1 days from the first	
	set @SLADate = substring(@MonthYear,1,2) + '/15/' + substring(@MonthYear,4,7)
End
if @SLA='LDOM'    --Last day of the month --OK
begin
	--go out an additional N-1 days from the first	
	set @SLADate = convert(nvarchar(10),dateadd(d, -1,@FirstDayOfMonth),101)
End
if @SLA='BD'    --if this is DAY OF THE MONTH  --OK
begin
	Declare @IsHoliday bit
	if substring(convert(varchar(20),@FirstDayOfMonth,101),1,5)='01/01'
		set @FirstDayOfMonth='01/02/' + @YYYY
		--print @FirstDayOfMonth
	set @x=0
	set @BDCtr=0
	set @IsHoliday=0
	set @tmpDate=@FirstDayOfMonth
	while  @BDCtr < @NumDay
		Begin
			-- see if the day (starting with @FirstDayOfMonth) is a businessDau
			select @DayNum= datepart(dw,dateadd(d,@X,@tmpDate))
			if @DayNum not in (1,7) 
				begin
					declare @t datetime
					set @t=dateadd(dd, @X,@tmpDate)
					exec pCheckForHoliday @t, @IsHoliday = @IsHoliday out
					if @IsHoliday=0	 set @BDCtr=@BDCtr+1
				End
			
		set @X=@X + 1
		end
	
	--go out an additional N-1 days from the first	
	set @SLADate = convert(nvarchar(10),dateadd(d, @X,@FirstDayOfMonth)-1,101)
End
if @SLA='MAFF'    --if this is the monday after the first friday
begin
	set @x=0
	set @BDCtr=0
	select @DayNum= datepart(dw,@FirstDayOfMonth)
	if @DayNum=6 --if the first day of the month is Friday, Just add 3 days to this
		set @SLADate = dateadd(d, 3,@FirstDayOfMonth)
	else
		Begin
			
			set @tmpDate=@FirstDayOfMonth
			while  @DayNum<>6 
				Begin
					set @X=@X + 1
					--
					select @DayNum= datepart(dw,dateadd(d,@X,@tmpDate))
					
				
				end
			
			--go out an additional 3 days from the first	Friday
			set @SLADate = convert(nvarchar(10),dateadd(d, @X,@FirstDayOfMonth)+3,101)
			------------------------------make sure the monday isn't a holiday. if it is, Add one day to it
								
					exec pCheckForHoliday @SLADate, @IsHoliday = @IsHoliday out
					if @IsHoliday<>0	 set @SLADate=dateadd(d, 1,@SLADate)
	
		End
End
GO
