USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pLoadMonthlyFI_record]    Script Date: 10/03/2014 16:51:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pLoadMonthlyFI_record]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pLoadMonthlyFI_record]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pLoadMonthlyFI_record]    Script Date: 10/03/2014 16:51:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pLoadMonthlyFI_record] 
@DBNumber char(3),
@MonthYear nvarchar(7), 
@SLA datetime,
@DeleteFlag int
 
AS
--@SLA is the Base date calculated for the date that the Import files are due
Declare
@DBNamePatton nchar(50),
@SLAForFilesReceived nchar(50),
@ActDateRec_SLA int,
@AuditFileSent_SLA int,
@AuditFileApproved_SLA int,
@PostedToWeb_SLA int,
@FilesToRNProd_SLA int,
@LiabSummSent_SLA int,
@EmailStmtSent_SLA int,
@QStmtToRNProd_SLA int,
@DBProcessorID int,
@IsActive bit,
@IsMonthlyApprovalReq bit,
@ProgramName varchar(50)
---------------------------------
if @DeleteFlag =1
	Begin
		
		delete FIMonthlyData where MonthYear=@MonthYear and DBNumber=@DBNumber
		
		
	End
Else
Begin


Select 
@DBNamePatton 	=DBNamePatton,
@SLAForFilesReceived =SLAForFilesReceived ,
@ActDateRec_SLA	=ActDateRec_SLA,
@AuditFileSent_SLA	=AuditFileSent_SLA,
@AuditFileApproved_SLA =AuditFileApproved_SLA,
@PostedToWeb_SLA	=PostedToWeb_SLA,
@FilesToRNProd_SLA	=FilesToRNProd_SLA,
@LiabSummSent_SLA 	=LiabSummSent_SLA,
@EmailStmtSent_SLA	=EmailStmtSent_SLA,
@QStmtToRNProd_SLA=QStmtToRNProd_SLA,
@DBProcessorID	=DBProcessorID,
@IsActive		=IsActive,
@IsMonthlyApprovalReq=IsMonthlyApprovalReq,
@ProgramName		= ProgramName
from FI where DBNumber=@DBNumber
-----------------------------------------------
Declare @IFDD nchar(10), 
		@ADR  nchar(10) , 
		@AFS  nchar(10) ,  
		@AFA  nchar(10), 
		@PTW  nchar(10) ,  
		@FTP  nchar(10) , 
		@LSS  nchar(10), 
		@ESS  nchar(10), 
		@QSP nchar(10)

--Delete any existing record for this FI/Month
DELETE from  FIMonthlyData where DBNumber=@DBNumber and MonthYear=@MonthYear
--DELETE from  LogFIMonthly where DBNumber=@DBNumber and MonthYear=@MonthYear

--calls pCalcBusinessDayDiff   which takes parameters of the number of days to add  and the previous items date and calcs the date that item is due
set @IFDD = convert(nvarchar(10),@SLA,101)  --@SLA coming into function as a parameter
print @IFDD


--exec pCalcBusinessDayDiff  @ActDateRec_SLA, @IFDD, @ResultDate=@ADR out
--exec pCalcBusinessDayDiff  @AuditFileSent_SLA, @ADR,  @ResultDate=@AFS out
--exec pCalcBusinessDayDiff  @AuditFileApproved_SLA, @AFS,  @ResultDate=@AFA out
--exec pCalcBusinessDayDiff  @PostedToWeb_SLA, @AFA,  @ResultDate=@PTW out
--exec pCalcBusinessDayDiff  @FilesToRNProd_SLA, @PTW,  @ResultDate=@FTP out
--exec pCalcBusinessDayDiff  @LiabSummSent_SLA, @FTP,  @ResultDate=@LSS out
--exec pCalcBusinessDayDiff  @QStmtToRNProd_SLA, @LSS,  @ResultDate=@QSP out
--exec pCalcBusinessDayDiff  @EmailStmtSent_SLA, @QSP,  @ResultDate=@ESS out
----------------

--returns the value into the variable by passing in the PREVIOUS Calced value as parm along with the SLA days from FIEdit.asp (FI table)
SET @ADR = CONVERT(nchar(10),dbo.CalculateSLADate(@ActDateRec_SLA, @IFDD), 101)
SET @AFS = CONVERT(nchar(10),dbo.CalculateSLADate(@AuditFileSent_SLA, @ADR), 101)
SET @AFA = CONVERT(nchar(10),dbo.CalculateSLADate(@AuditFileApproved_SLA, @AFS), 101)
SET @PTW = CONVERT(nchar(10),dbo.CalculateSLADate(@PostedToWeb_SLA, @AFA), 101)
SET @FTP = CONVERT(nchar(10),dbo.CalculateSLADate(@FilesToRNProd_SLA, @PTW), 101)
SET @LSS = CONVERT(nchar(10),dbo.CalculateSLADate(@LiabSummSent_SLA, @FTP), 101)
SET @QSP = CONVERT(nchar(10),dbo.CalculateSLADate(@QStmtToRNProd_SLA, @LSS), 101)
SET @ESS = CONVERT(nchar(10),dbo.CalculateSLADate(@EmailStmtSent_SLA, @QSP ), 101)



-----------------

 Insert Into FIMonthlyData (DBNumber , DBNamePatton, MonthYear  , IsActive, IsMonthlyApprovalReq, DBProcessorID, ProgramName, SLAForFilesReceived, ImpFilesDueDate,  ImpFilesDueDate_SLADate , ActDateRec_SLADate , AuditFileSent_SLADate , AuditFileApproved_SLADate , PostedToWeb_SLADate , FilesToRNProd_SLADate , LiabSummSent_SLADate , EmailStmtSent_SLADate, QStmtToRNProd_SLADate )
	values( @DBNumber, @DBNamePatton, @MonthYear, @IsActive, @IsMonthlyApprovalReq, @DBProcessorID, @ProgramName, @SLAForFilesReceived, @IFDD, @IFDD, @ADR , @AFS ,  @AFA , @PTW,  @FTP , @LSS , @ESS, @QSP )
declare @mdid int
Set @mdid=@@identity
exec pMDColorUpdate 'ActDateRec', @mdid
exec pMDColorUpdate 'AuditFileSent', @mdid
exec pMDColorUpdate 'AuditFileApproved', @mdid
exec pMDColorUpdate 'PostedToWeb', @mdid
exec pMDColorUpdate 'FilesToRNProd', @mdid
exec pMDColorUpdate 'LiabSummSent', @mdid
exec pMDColorUpdate 'EmailStmtSent', @mdid
exec pMDColorUpdate 'QStmtToRNProd', @mdid

--------------------------------


--Deletes and re-Copies this months data from  LogItems into LogFIMonthly
exec pLogItemsReBuild @DBNumber,@MonthYear,@mdid

END
/*
-- insert the standard Logitems that don't exist already
Insert into LogItems (  LogItem,  LogNote, DBNumber, SeqNum, IsStdItem )
	Select LogItem, LogNote, @DBNumber, SeqNum,1 from LogItemsSTD where LogItemID not in (Select LogitemID from LogItems where DBNumber=@DBNumber)  


DELETE from  LogFIMonthly where DBNumber=@DBNumber and MonthYear=@MonthYear
--create the LogFIMonthly records based on LogItems
Insert into LogFIMonthly (MDID,MonthYear,DBNumber,LogItem,LogNote,SeqNum,IsStdItem )
		Select @mdid , @MonthYear,@DBNumber,LogItem,LogNote,SeqNum,IsStdItem from LogItems where DBNumber=@DBNumber

*/

GO


