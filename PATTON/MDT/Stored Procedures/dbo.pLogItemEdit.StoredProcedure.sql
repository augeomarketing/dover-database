SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[pLogItemEdit]
@FormMethod varchar(15) ,
@LogItemID int=null , 
@LogItem varchar(200) ,
@IsActive bit, 
@IsLiabLink bit, 
@DBNumber varchar(3), 
@LogNote nvarchar(1000),

@ReturnString varchar(255) output,
@NewPKVal Int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod ='CREATE'
begin
Insert into LogItemsFI (  LogItem,  IsActive, DBNumber,  LogNote, IsLiabLink )
	 VALUES(   @LogItem,  @IsActive, @DBNumber, @LogNote, @IsLiabLink)
	SET @NewPKVal = @@IDENTITY

	--Do an Insert into LogItems if this is a new one. allows for adding new items while Sequencing 
	declare @MaxSeqNum Int
	select @MaxSeqNum =Max(SeqNum) +1  from LogItems where DBNumber=@DBNumber
	
	Insert Into LogItems(LogItemID,LogItem, LogNote, DBNumber, SeqNum, IsStdItem )
		 VALUES(   @NewPKVal, @LogItem , @LogNote, @DBNumber, @MaxSeqNum, 0)	
end
-------------------------------------------
-------------------------------------Update
if @FormMethod ='UPDATE'
Begin
		Update LogItemsFI set 
		LogItem 	= 	@LogItem, 
		IsActive		=	@IsActive,
		DBNumber	=	@DBNumber, 
		LogNote	=	@LogNote,
		IsLiabLink	=	@IsLiabLink
		where LogItemID=	@LogItemID
		------------------------
Update LogItems set
		LogItem 	= 	@LogItem, 
		DBNumber	=	@DBNumber, 
		LogNote	=	@LogNote,
		IsLiabLink	=@IsLiabLink
		where LogItemID=	@LogItemID
		And DBNumber=@DBNumber
		
End
------------------------
if @FormMethod ='DELETE'
	Begin
		DELETE LogItemsFI  where LogItemID	=	@LogItemID and DBNumber=@DBNumber
		DELETE LogItems  where LogItemID	=	@LogItemID and DBNumber=@DBNumber
	End
--------------------------------------------------------------------------
-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
