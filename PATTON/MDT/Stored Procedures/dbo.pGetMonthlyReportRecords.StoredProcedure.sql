SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetMonthlyReportRecords]  
	@UserID int=null,
	@MonthYear varchar(7) =null, 
	@SortCol varchar(50) =null,
	@TipFirst varchar(3)=null,
	@DBProcessorID int =null,
	@ProgramName varchar(50)=null
AS

declare @SQLSelect nvarchar(1000) ,  @UserGroupCode varchar(1)

Select @UserGroupCode =UserGroupCode from Userinfo where sid_userinfo_id=@UserID
set @SortCol='FIMonthlyData.' + @SortCol

if @UserGroupCode not in ('M')
begin	
	--if Nothing but Month is selected
	If @TipFirst is null AND @DBProcessorID is null AND @ProgramName is null
	begin
		set @SQLSelect ='SELECT FIMonthlyData.*  ,  userinfo.dim_userinfo_fname AS fname,  FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber WHERE  FIMonthlyData.IsActive=1 and MonthYear =@MonthYear order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @SortCol=@SortCol
	End
	-- if more than one of the criteria below are met, the FIRST one run is the recordset shown on the web page 
	--Tipfirst Selected 
	If @TipFirst IS NOT NULL 
	begin
		set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname, FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber WHERE  FIMonthlyData.IsActive=1 and FIMonthlyData.DBNumber=@TipFirst order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@TipFirst varchar(3), @SortCol nvarchar(25) ', @TipFirst=@TipFirst, @SortCol=@SortCol
	End
	--Processor Selected
	If @DBProcessorID is not null --and @TipFirst IS NULL and @ProgramName is null
	begin
		set @SQLSelect ='SELECT FIMonthlyData.*,  userinfo.dim_userinfo_fname AS fname, FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and FIMonthlyData.DBProcessorID=@DBProcessorID order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @DBProcessorID int, @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @DBProcessorID=@DBProcessorID, @SortCol=@SortCol
	End
	--ProgramName Selected
	If @ProgramName is not null 
	begin
		set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname, FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and ProgramName=@ProgramName order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @ProgramName varchar(50), @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @ProgramName=@ProgramName, @SortCol=@SortCol
	End
	print @SQLSelect
end

if @UserGroupCode ='M'
begin	
	set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname, FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id 
							INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber  
			WHERE  FIMonthlyData.DBNumber  in (select FI.DBNumber from FI where PMUserID=@UserID )
			AND FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear  order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@UserID int,@MonthYear nvarchar(7), @ProgramName varchar(50), @SortCol nvarchar(25) ', @UserID=@UserID,@MonthYear=@MonthYear, @ProgramName=@ProgramName, @SortCol=@SortCol
	print 'M-SQL:' + @SQLSelect

End
GO
