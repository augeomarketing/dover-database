USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[usp_RecalcSLASFromDateReceived]    Script Date: 01/11/2011 15:29:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RecalcSLASFromDateReceived]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RecalcSLAs]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[usp_RecalcSLASFromDateReceived]    Script Date: 01/11/2011 15:29:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RecalcSLASFromDateReceived]
	@DBNumber char(3),
	@MonthYear nvarchar(7), 
	@ADR datetime --ActualDateReceived
AS
SET NOCOUNT ON
---------------------------
-- DATABASE INFO / SLA'S
---------------------------
DECLARE @DBNamePatton nchar(50)
DECLARE @AuditFileSent_SLA int
DECLARE @AuditFileApproved_SLA int
DECLARE @PostedToWeb_SLA int
DECLARE @FilesToRNProd_SLA int
DECLARE @LiabSummSent_SLA int
DECLARE @EmailStmtSent_SLA int
DECLARE @QStmtToRNProd_SLA int
DECLARE @DBProcessorID int
DECLARE @IsActive bit
DECLARE @IsMonthlyApprovalReq bit
DECLARE @ProgramName varchar(50)
---------------------------------
--         DATES
---------------------------------
DECLARE @AFS  datetime
DECLARE @AFA  datetime
DECLARE @PTW  datetime
DECLARE @FTP  datetime
DECLARE @LSS  datetime
DECLARE @ESS  datetime
DECLARE @QSP datetime

declare @mdid int --Monthly Data Line ID

Select 
	@DBNamePatton = DBNamePatton
	, @AuditFileSent_SLA = AuditFileSent_SLA
	, @AuditFileApproved_SLA = AuditFileApproved_SLA
	, @PostedToWeb_SLA = PostedToWeb_SLA
	, @FilesToRNProd_SLA = FilesToRNProd_SLA
	, @LiabSummSent_SLA = LiabSummSent_SLA
	, @EmailStmtSent_SLA = EmailStmtSent_SLA
	, @QStmtToRNProd_SLA = QStmtToRNProd_SLA
	, @DBProcessorID	=DBProcessorID
	, @IsActive		=IsActive
	, @IsMonthlyApprovalReq=IsMonthlyApprovalReq
	, @ProgramName		= ProgramName
from FI 
	where DBNumber=@DBNumber

SET @AFS = dbo.CalculateSLADate(@AuditFileSent_SLA, @ADR)
SET @AFA = dbo.CalculateSLADate(@AuditFileApproved_SLA, @AFS)
SET @PTW = dbo.CalculateSLADate(@PostedToWeb_SLA, @AFA)
SET @FTP = dbo.CalculateSLADate(@FilesToRNProd_SLA, @PTW)
SET @LSS = dbo.CalculateSLADate(@LiabSummSent_SLA, @FTP)
SET @ESS = dbo.CalculateSLADate(@EmailStmtSent_SLA, @LSS)
SET @QSP = dbo.CalculateSLADate(@QStmtToRNProd_SLA, @ESS)

UPDATE FIMonthlyData
SET AuditFileSent_SLADate = CONVERT(nchar(10), @AFS, 101)
	, AuditFileApproved_SLADate = CONVERT(nchar(10), @AFA, 101)
	, PostedToWeb_SLADate =  CONVERT(nchar(10), @PTW, 101)
	, FilesToRNProd_SLADate = CONVERT(nchar(10), @FTP, 101)
	, LiabSummSent_SLADate = CONVERT(nchar(10), @LSS, 101) 
	, EmailStmtSent_SLADate = CONVERT(nchar(10), @ESS, 101) 
	, QStmtToRNProd_SLADate = CONVERT(nchar(10), @QSP, 101) 
WHERE
	DBNumber = @DBNumber
	AND MonthYear = @MonthYear
	
SELECT @mdid = mdID FROM FIMonthlyData WHERE DBNumber = @DBNumber AND MonthYear = @MonthYear

exec pMDColorUpdate 'ActDateRec', @mdid
exec pMDColorUpdate 'AuditFileSent', @mdid
exec pMDColorUpdate 'AuditFileApproved', @mdid
exec pMDColorUpdate 'PostedToWeb', @mdid
exec pMDColorUpdate 'FilesToRNProd', @mdid
exec pMDColorUpdate 'LiabSummSent', @mdid
exec pMDColorUpdate 'EmailStmtSent', @mdid
exec pMDColorUpdate 'QStmtToRNProd', @mdid


GO


