SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pDateFieldPopupEdit]
@UserID int,
@FieldName nvarchar(50),
@TableName nvarchar(50),
@PKName nvarchar(50),
@PKval int,
@ReturnString nvarchar(255) out
 AS
declare @FldVal datetime  -- this is used to store whether or not the entry sequence is bad by checking what the fieldname being passed in is and checking to see if the prior one has a value or not
if @Fieldname='AuditFileSent' 
	begin
		select @FldVal=ActDateRec from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='AuditFileApproved' 
	begin
		select @FldVal=AuditFileSent from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='PostedToWeb' 
	begin
		select @FldVal=AuditFileApproved from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='FilesToRNProd' 
	begin
		select @FldVal=PostedToWeb from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='LiabSummSent' 
	begin
		select @FldVal=FilesToRNProd from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='EmailStmtSent' 
	begin
		select @FldVal=LiabSummSent from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='QStmtToRNProd' 
	begin
		select @FldVal=EmailStmtSent from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end

declare @SQLUpdate nvarchar(1000), @LastUserName nvarchar(50)
select @LastUserName=dim_userinfo_Username from userInfo where sid_userinfo_id=@UserID
set @SQLUpdate='update ' + @TableName + ' SET ' + @FieldName + ' = GetDate(), LastUpdated=GetDate(), LastUserName=''' + @LastUserName + '''  where ' + @PKName +'=' + convert(nvarchar(10),@PKVal )
Exec sp_executesql @SQLUpdate, N'@TableName nvarchar(50), @FieldName nvarchar(50), @PKName nvarchar(50), @PKval int, @LastUserName nvarchar(50) ', @TableName=@TableName, @FieldName=@FieldName ,@PKName=@PKName, @PKval=@PKval,  @LastUserName= @LastUserName
----
if @TableName='FIMonthlyData'
	insert into FIMonthlyData_history 
		Select mdID, DBNumber, MonthYear, SLAForFilesReceived, ImpFilesDueDate, ActDateRec, Comments, AuditFileSent, AuditFileApproved, PostedToWeb, FilesToRNProd, LiabSummSent, EmailStmtSent, QStmtToRNProd, LastUpdated, LastUserName  from FIMonthlyData where mdID=@PKVal
--------------------------
-- calc the color for this field and update the _color field for it
exec pMDColorUpdate @FieldName , @PKval
-------------------------
GO
