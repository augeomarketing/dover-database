USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pMonthYear_CheckAdd]    Script Date: 11/05/2014 14:36:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMonthYear_CheckAdd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pMonthYear_CheckAdd]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pMonthYear_CheckAdd]    Script Date: 11/05/2014 14:36:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pMonthYear_CheckAdd]
	-- Add the parameters for the stored procedure here

AS
/* sample call
exec pMonthYear_CheckAdd 
select * from tluMonthYear order by id desc
--delete from tluMonthYear  where id>=96
*/

	BEGIN
		declare @MonthYear varchar(7), @MonthYear_dt datetime, @NewMonthYear varchar(7)
		declare @CurrDate varchar(7), @CurrDate_dt datetime, @ProcessStart_dt datetime, @PivotDate_dt datetime
		
		-- get the largest date from tluMonthYear and use this as starting point
		;with a as
		(
			select  MonthYear, right(MonthYear,4) + left(MonthYear,2) as YearMonth  from tluMonthYear
		)
		select @MonthYear = MonthYear from a where YearMonth=( select  MAX(YearMonth) from a)
		set @CurrDate_dt= GETDATE()
		select @MonthYear_dt=CAST(left(@MonthYear,2) + '/01/' + right(@MonthYear, 4) as datetime)
		set @ProcessStart_dt =  DATEADD(mm, DATEDIFF(mm, 0, @MonthYear_dt) + 1, 0)
		set @PivotDate_dt =  DATEADD(mm, DATEDIFF(mm, 0, @ProcessStart_dt) + 1, 0)
		
		print '@CurrDate_dt:' + convert(varchar,@CurrDate_dt ,101)
		print '@MonthYear_dt:' + convert(varchar,@MonthYear_dt ,101)
		print '@ProcessStart_dt:' + convert(varchar,@ProcessStart_dt ,101)
		print '@PivotDate_dt:' + convert(varchar,@PivotDate_dt ,101)
		
		if @CurrDate_dt>= @PivotDate_dt
		begin
			set @NewMonthYear = RIGHT('00'+cast(ISNULL(MONTH(@ProcessStart_dt),'') as varchar(2)),2) + '-' + RIGHT('0000'+cast(ISNULL(YEAR(@ProcessStart_dt),'') as varchar(4)),4)
		end
		
		print '@NewMonthYear:' + @NewMonthYear
		-- make sure we arent' going to add the same value twice
		declare @RecCount int
		select @RecCount =COUNT(*) from tluMonthYear where MonthYear =@NewMonthYear
		if @RecCount=0 and ISNULL(@NewMonthYear,'') <> '' 
			begin
				insert into tluMonthYear (MonthYear) values(@NewMonthYear)
				print 'rec added'
			end
			--select * from tluMonthYear order by id desc
			--delete from tluMonthYear where id=94
		
END

GO


