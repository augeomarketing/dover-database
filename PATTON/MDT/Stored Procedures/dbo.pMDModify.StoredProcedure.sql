SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pMDModify] 
@UserID int =null,
@FormMethod varchar(20),
@MDID integer,
--@SLAForFilesReceived nchar(10)=null,
@ImpFilesDueDate nchar(10)=null,
@ActDateRec DateTime=null,
@Comments nchar(2000)=null,
@AuditFileSent DateTime = null,
@AuditFileApproved DateTime = null,
@PostedToWeb DateTime = null,
@FilesToRNProd DateTime = null,
@LiabSummSent DateTime = null,
@EmailStmtSent DateTime = null,
@QStmtToRNProd datetime=null,
@ReturnString varchar(255) = NULL output ,
@NewPKVal int =null output 
AS
declare @UserName varchar(50), @FldVal datetime
select @UserName = dim_Userinfo_username from UserInfo where sid_Userinfo_id=@UserID
/*
IF @FormMethod='INSERT'
begin
	insert into FIMonthlyData (  SLAForFilesReceived , ImpFilesDueDate , ActDateRec , Comments , AuditFileSent , AuditFileApproved , PostedToWeb , FilesToRNProd , LiabSummSent , EmailStmtSent)
	values (   @SLAForFilesReceived , @ImpFilesDueDate , @ActDateRec , @Comments , @AuditFileSent , @AuditFileApproved , @PostedToWeb , @FilesToRNProd , @LiabSummSent , @EmailStmtSent )
	--select @PKVal=@@Identity
end
*/
if @AuditFileSent is not null
	begin
		select @FldVal=ActDateRec from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
	end
if @AuditFileApproved is not null
	begin
		select @FldVal=AuditFileSent from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
	end
if @PostedToWeb is not null
	begin
		select @FldVal=AuditFileApproved from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
	end
if @FilesToRNProd is not null
	begin
		select @FldVal=PostedToWeb from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
			
	end
if @LiabSummSent is not null
	begin
		select @FldVal=FilesToRNProd from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
	end
if @EmailStmtSent is not null
	begin
		select @FldVal=LiabSummSent from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
	end

if @QStmtToRNProd is not null
	begin
		select @FldVal=EmailStmtSent from FIMonthlyData where mdid=@mdid
		if @Fldval is null 
		begin
			set @ReturnString='Items must be filled out in sequence'
			return
		end
	end



IF @FormMethod='UPDATE'
begin
	insert into FIMonthlyData_history 
		Select mdID, DBNumber, MonthYear, SLAForFilesReceived, ImpFilesDueDate, ActDateRec, Comments, AuditFileSent, AuditFileApproved, PostedToWeb, FilesToRNProd, LiabSummSent, EmailStmtSent, QStmtToRNProd , LastUpdated, LastUserName  from FIMonthlyData where mdID=@MDID
		--Select FIMonthlyData.* from FIMonthlyData where mdID=@MDID
	UPDATE FIMonthlyData set
		--SLAForFilesReceived = rtrim(@SLAForFilesReceived) ,
		ImpFilesDueDate = rtrim(@ImpFilesDueDate) ,
		ActDateRec = rtrim(@ActDateRec) ,
		Comments = rtrim( @Comments ) ,
		AuditFileSent = rtrim( @AuditFileSent ) ,
		AuditFileApproved = rtrim( @AuditFileApproved ) ,
		PostedToWeb = rtrim( @PostedToWeb ) ,
		FilesToRNProd = rtrim( @FilesToRNProd ) ,
		LiabSummSent = rtrim( @LiabSummSent ) ,
		EmailStmtSent = rtrim( @EmailStmtSent ) ,
		QStmtToRNProd= rtrim( @QStmtToRNProd ) ,
		LastUpdated= GetDate() ,
		LastUserName = @UserName
	WHERE MDID=@MDID
end
-------------------------------------------------------
--update the field color if the value was updated
 if @ActDateRec is not null exec  pMDColorUpdate  'ActDateRec', @MDID 
 if @AuditFileSent is not null exec  pMDColorUpdate  'AuditFileSent', @MDID 
 if @AuditFileApproved is not null exec  pMDColorUpdate  'AuditFileApproved', @MDID 
 if @PostedToWeb is not null exec  pMDColorUpdate  'PostedToWeb', @MDID 
 if @FilesToRNProd is not null exec  pMDColorUpdate  'FilesToRNProd', @MDID 
 if @LiabSummSent is not null exec  pMDColorUpdate  'LiabSummSent', @MDID 
 if @EmailStmtSent is not null exec  pMDColorUpdate  'EmailStmtSent', @MDID 
 if @QStmtToRNProd is not null exec  pMDColorUpdate  'QStmtToRNProd', @MDID 
return
GO
