SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pEmailReminder] 
AS

Declare @MailFrom varchar(50)
set @MailFrom ='OpsLogs@Rewardsnow.com'

insert into Maintenance.dbo.Perlemail (dim_perlemail_to, dim_perlemail_subject , dim_perlemail_body, dim_perlemail_from )


SELECT     
userinfo.dim_userinfo_username + '@Rewardsnow.com' AS MailTo, 
FIReminders.DBNumber + '-' + convert(varchar(10),FIReminders.ReminderStartDate,101)+ '-' + substring(FIReminders.ReminderNote,1,15) + '...' as Subj,
FIReminders.ReminderNote as MailBody, @MailFrom         
FROM         FIReminders INNER JOIN
                      FI ON FIReminders.DBNumber = FI.DBNumber INNER JOIN
                      userinfo ON FI.DBProcessorID = userinfo.sid_userinfo_id
WHERE     (FIReminders.IsEmailReminder = 1) AND (FIReminders.IsReminderOn = 1) AND (FIReminders.ReminderStartDate < GETDATE())
GO
