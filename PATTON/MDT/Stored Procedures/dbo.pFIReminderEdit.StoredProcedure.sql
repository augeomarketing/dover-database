USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pFIReminderEdit]    Script Date: 10/07/2014 12:19:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pFIReminderEdit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pFIReminderEdit]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pFIReminderEdit]    Script Date: 10/07/2014 12:19:14 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[pFIReminderEdit]
@UserID int,
@FormMethod varchar(15) ,
@FIReminderID int,
@DBNumber varchar(3) , 
@ReminderStartDate varchar(25)=NULL , 
@IsReminderOn bit=1, 
@ReminderNote varchar(1000)=NULL,
@IsEmailReminder bit=0,
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
/*sample call
DECLARE @UserID int, @FormMethod varchar(15) ,@FIReminderID int, @DBNumber varchar(3) , @ReminderStartDate varchar(25) , 
@IsReminderOn bit , @ReminderNote varchar(1000), @IsEmailReminder bit , @ReturnString varchar(255) , @NewPKVal int 
set @userid=1; set @FormMethod='CREATE'; set @FIReminderID=null; set @DBNumber='221'; set @ReminderStartDate='10/02/2014';
set @IsReminderOn =1; set @ReminderNote='CCCC'; set @IsEmailReminder=0;

exec pFIReminderEdit @UserID ,
@FormMethod  ,
@FIReminderID,
@DBNumber  , 
@ReminderStartDate , 
@IsReminderOn , 
@ReminderNote ,
@IsEmailReminder ,
@ReturnString  output,
@NewPKVal  output 

print  '@ReturnString:' + @ReturnString 
print  '@NewPKVal:' + cast (@NewPKVal as varchar)
*/

SET XACT_ABORT ON
BEGIN TRANSACTION


-------------------------------------Create

if @FormMethod ='CREATE'
begin
Insert into FIReminders (DBNumber, ReminderStartDate, IsReminderOn, ReminderNote, IsEmailReminder )
	 VALUES(   @DBNumber, @ReminderStartDate, @IsReminderOn, @ReminderNote , @IsEmailReminder )
	SET @FIReminderID = @@Identity 
	SET @NewPKVal = @FIReminderID
	
end
-------------------------------------------
-------------------------------------Update
if @FormMethod ='UPDATE'
Begin
		Update FIReminders set 
		DBNumber 	= 	@DBNumber, 
		ReminderStartDate=	@ReminderStartDate, 
		IsReminderOn		=@IsReminderOn, 
		ReminderNote		=@ReminderNote,
		IsEmailReminder		=@IsEmailReminder
		where FIReminderID	=	@FIReminderID
		------------------------

End
------------------------
if @FormMethod ='DELETE'
	Begin
		DELETE FIReminders  where FIReminderID	=	@FIReminderID
	End
--------------------------------------------------------------------------
-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO


