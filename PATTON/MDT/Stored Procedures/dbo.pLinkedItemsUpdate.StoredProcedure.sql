SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pLinkedItemsUpdate]
@DBNumber varchar(3),
@LogItem varchar(100),
@LogNote varchar(2000),
@MonthYear  varchar(7),
@NoteDate datetime
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION


--Declare @LogItem nvarchar(100), @LogNote nvarchar(2000),
declare  @SQLUpdate nvarchar(2000)
--select @LogItem=LogItem, @LogNote=LogNote from LogItems where LogItemID=@LogItemID

---WORKED
--Set @SQLUpdate='Update LogFIMonthly set NoteDate=''' + convert(varchar(25),@NoteDate,101) + ''', LogNote=''' + @LogNote + ''' WHERE MonthYear=''' +@MonthYear + ''' AND DBNumber in (Select DBNumber from LogItemsLinked where LogItem=''' + @LogItem + ''' AND IsActive=1) AND  LogItem=''' + @LogItem + ''''

declare @DTLinked datetime
Select @DTLinked =DTLinked from LogItemsLinked where DBNumber=@DBNumber and LogItem=@LogItem

Set @SQLUpdate='Update LogFIMonthly set 
	NoteDate=@NoteDate, 
	LogNote=@LogNote
	WHERE MonthYear=@MonthYear 
		AND DBNumber in (Select DBNumber from LogItemsLinked where LogItem=@LogItem AND DTLinked=@DTLinked AND IsActive=1) 
		AND  LogItem=@LogItem '

--print @SQLUpdate
Exec sp_executesql @SQLUpdate , N'@NoteDate datetime, @LogNote nvarchar(1000), @MonthYear  varchar(7), @LogItem nvarchar(100) , @DTLinked datetime', @NoteDate=@NoteDate, @LogNote=@LogNote, @MonthYear=@MonthYear, @LogItem=@LogItem , @DTLinked=@DTLinked

-------------
--need to update FIMonthlyData for all of these FIs 
--open a cursor of MDID's for StandardItems for the logitem and Update the color and value for it

	set @SQLUpdate=''

	Declare @MDID integer, @FieldName nvarchar(50)
	if @LogItem='DATE DATA FILES RECEIVED' set @FieldName='ActDateRec'
	if @LogItem='AUDIT FILES SENT' set @FieldName='AuditFileSent'
	if @LogItem='AUDIT FILES APPROVED' set @FieldName='AuditFileApproved'
	if @LogItem='POSTED TO WEB' set @FieldName='PostedtoWeb'
	if @LogItem='FILES TO RN PROD' set @FieldName='FilestoRNProd'
	if @LogItem='LIABILITY SENT' set @FieldName='LiabSummSent'
	if @LogItem='EMAIL STMT SENT' set @FieldName='EmailStmtSent'
	if @LogItem='Q-STMT SENT' set @FieldName='QStmtToRNProd'






	declare C cursor
	for select MDID 
	from FIMonthlyData 
	WHERE MonthYear=@MonthYear 
		AND DBNumber in (Select DBNumber from LogItemsLinked where LogItem=@LogItem AND DTLinked=@DTLinked AND IsActive=1) 
	--	AND  LogItem=@LogItem 
	--	AND IsStdItem=1
	
	/*                                                                            */
	open C
	/*                                                                            */
	fetch C into @MDID
                                      
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	
	while @@FETCH_STATUS = 0
	begin	
		--do steps here  (add update to field LastUserName)
		set @SQLUpdate='Update FIMonthlyData set ' +  @FieldName + '= @NoteDate,  LastUpdated=GetDate() where MDID=@MDID'

		print @SQLUpdate
		Exec sp_executesql @SQLUpdate , N' @NoteDate datetime, @MDID int',  @NoteDate=@NoteDate, @MDID=@MDID
		
		exec pMDColorUpdate @FieldName , @MDID



	goto Next_Record
	
	Next_Record:
			fetch C into @MDID
	end
	
	
	Fetch_Error:
	close C
	deallocate C


COMMIT TRANSACTION
GO
