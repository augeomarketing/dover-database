SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pRecordEdit]
@UserID int =null,
@FormMethod varchar(20),
@MDID integer,
@Comments nchar(2000)=null,
@ReturnString varchar(255) = NULL output ,
@NewPKVal int =null output 
AS
declare @UserName varchar(50), @FldVal datetime
select @UserName = dim_Userinfo_username from UserInfo where sid_Userinfo_id=@UserID


IF @FormMethod='UPDATE'
begin
	insert into FIMonthlyData_history 
		Select mdID, DBNumber, MonthYear, SLAForFilesReceived, ImpFilesDueDate, ActDateRec, Comments, AuditFileSent, AuditFileApproved, PostedToWeb, FilesToRNProd, LiabSummSent, EmailStmtSent, QStmtToRNProd , LastUpdated, LastUserName  from FIMonthlyData where mdID=@MDID
		--Select FIMonthlyData.* from FIMonthlyData where mdID=@MDID
	UPDATE FIMonthlyData set
		Comments = rtrim( @Comments ) ,
		LastUpdated= GetDate() ,
		LastUserName = @UserName
	WHERE MDID=@MDID
end

return
GO
