SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pCloneLog]
@FormMethod varchar(15) ,
@DBNumber varchar(3), 
@DBTarget varchar(3), 
@ReturnString varchar(255) output
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod ='CLONE'
begin

--Clone/insert the FI specific LogItems
delete LogItemsFI  where DBNumber=@DBTarget
Insert into LogItemsFI (  LogItem, LogNote,  DBNumber,  IsActive )
	Select   LogItem, LogNote,  @DBTarget, IsActive from LogItemsFI  where DBNumber=@DBNumber


--Clone/insert the ALL  LogItems FI and STD
delete LogItems  where DBNumber=@DBTarget

Insert into LogItems (  LogItemID, LogItem, LogNote,  DBNumber, SeqNum, IsStdItem )
	Select   LogItemID, LogItem, LogNote,  @DBTarget, SeqNum, IsStdItem from LogItems  where DBNumber=@DBNumber and IsStdItem=1


--get the maximum sequence number and one Up it for every inserted record

declare @MaxSeqnum int
select @MaxSeqNum=Max(SeqNum) from LogItems where DBNumber=@DBTarget

Insert into LogItems (  LogItemID, LogItem, LogNote,  DBNumber, SeqNum, IsStdItem )
	Select   LogItemID, LogItem, LogNote,  @DBTarget,-1, 0 
	from LogItemsFI  
	where DBNumber=@DBTarget and LogItemID not in (Select LogItemID from LogItems where DBNumber=@DBTarget)


--put the original source DB Item and SeqNumber into tmp table
Select DBNumber,LogItem, SeqNum into #LI  from LogItems where DBNumber=@DBNumber

update LogItems set SeqNum=#LI.SeqNum from #LI join LogItems on #LI.LogItem=LogItems.LogItem WHERE LogItems.DBNumber=@DBTarget
drop table #LI
/*
update LogItems
	set  @MaxSeqNum = ( @MaxSeqNum + 1 ),
	      SeqNum =  @MaxSeqNum
	where SeqNum is null 
	and DBNumber=@DBTarget
*/




/*

Insert into LogItems (  LogItemID, LogItem, LogNote,  DBNumber, SeqNum, IsStdItem )
	Select   LogItemID, LogItem, LogNote,  @DBTarget, SeqNum, 0 
	from LogItemsFI  
	where DBNumber=@DBTarget and LogItemID not in (Select LogItemID from LogItems where DBNumber=@DBTarget)

*/
	
end

--------------------------------------------------------------------------
-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
