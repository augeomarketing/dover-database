SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[pUserEdit]
@FormMethod varchar(15) ,
@UserID int , 
@UserName varchar(50) , 
@UserPW varchar(10) ,
@FName varchar(50) ,
@LName varchar(50) , 
@IsAdmin bit,
@IsActive bit,
@IsReadOnly bit,
@UserGroupCode varchar(1),
@AppUserID int, --the id of the person using the app
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
Declare @AppUserIsReadOnly bit
select @AppUserIsReadOnly=IsReadOnly from userinfo where sid_userinfo_id=@AppUserID
-------------------------------------Create
--FormMethod,  UserID, FullName, Email, UserPW, SecLvl, IsAdmin, IsActive
if @FormMethod ='CREATE'
begin
Insert into UserInfo ( dim_userinfo_username, dim_userinfo_password, dim_userinfo_fname, dim_userinfo_lname, IsAdmin, IsActive, IsReadOnly, UserGroupCode)
	 VALUES(          @UserName,                  @UserPW,               @FName,            @LName,          @IsAdmin, @IsActive, @IsReadOnly, @UserGroupCode)
	SET @UserID = @@Identity 
	SET @NewPKVal = @UserID
	
end
-------------------------------------------
-------------------------------------Update
if @FormMethod ='UPDATE'
Begin
	-- UserName,  UserLogin,  UserPW,  SecLvl, Email,  EmpID	
		Update UserInfo set 
		dim_userinfo_UserName	=	@UserName, 
		dim_userinfo_Password		=	@UserPW,
		dim_userinfo_FName		=	@FName,
		dim_userinfo_LName		=	@LName,
		IsAdmin		=	@IsAdmin,
		IsActive		=	@IsActive,
		IsReadOnly	=	@IsReadOnly,
		UserGroupCode	= 	@UserGroupCode
		where sid_userinfo_id	=	@UserID
		-----
End
------------------------
if @FormMethod ='DELETE'
	Begin
		DELETE Userinfo  where sid_userinfo_id	=	@UserID
	End
--------------------------------------------------------------------------
-- exec pLogEvent @AdminUserID, 'tUsers', 'UserID', @UserID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
