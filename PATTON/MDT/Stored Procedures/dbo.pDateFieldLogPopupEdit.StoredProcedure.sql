SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pDateFieldLogPopupEdit]
@UserID int,
@MDID int,
@LogItemID int,

@ReturnString nvarchar(255) out
 AS

declare @LogItem nvarchar(100), @TableName varchar(50), @FieldName varchar(20)
set @TableName='LogFIMonthly'

Select  @LogItem=LogItem  from LogFIMonthly where LogItemID=@LogItemID


declare @SQLUpdate nvarchar(1000), @LastUserName nvarchar(50)
select @LastUserName=dim_userinfo_Username from userInfo where sid_userinfo_id=@UserID
set @SQLUpdate='update ' + @TableName + ' SET  NoteDate = GetDate()  where LogItemID=' + convert(nvarchar(10),@LogItemID )
print @SQLUpdate
Exec sp_executesql @SQLUpdate, N'@TableName varchar(50), @LogItemID  int ', @TableName=@TableName,@LogItemID=@LogItemID

----------------------------
---See if this is a standard item and determine which field in FIMonthlyData needs to get updated



----
/*
if @LogItem='Audit File Sent' 
Begin
set @Fieldname='AuditFileSent' 
	begin
		Update FIMonthlyData Set 
		Update  where mdid=@PKVal
		if @Fldval is null return
	end
end

if @LogItem='Audit File Approved' 
Begin
set @Fieldname='AuditFileApproved' 
	begin
		select @FldVal=AuditFileSent from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
End

if @LogItem='Posted To web' 
Begin
set  @Fieldname='PostedToWeb' 
	begin
		select @FldVal=AuditFileApproved from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
End


if @LogItem='Files To RN Prod' 
Begin
Set @Fieldname='FilesToRNProd' 
	begin
		select @FldVal=PostedToWeb from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
End


if @Fieldname='LiabSummSent' 
	begin
		select @FldVal=FilesToRNProd from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='EmailStmtSent' 
	begin
		select @FldVal=LiabSummSent from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end
if @Fieldname='QStmtToRNProd' 
	begin
		select @FldVal=EmailStmtSent from FIMonthlyData where mdid=@PKVal
		if @Fldval is null return
	end

--------------------------
-- calc the color for this field and update the _color field for it
if @TableName='FIMonthlyData'
	insert into FIMonthlyData_history 
		Select mdID, DBNumber, MonthYear, SLAForFilesReceived, ImpFilesDueDate, ActDateRec, Comments, AuditFileSent, AuditFileApproved, PostedToWeb, FilesToRNProd, LiabSummSent, EmailStmtSent, QStmtToRNProd, LastUpdated, LastUserName  from FIMonthlyData where mdID=@MDID

exec pMDColorUpdate @FieldName , @PKval

*/
-------------------------
GO
