USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pRefreshAll_CellColor]    Script Date: 01/28/2010 15:40:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pRefreshAll_CellColor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pRefreshAll_CellColor]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pRefreshAll_CellColor]    Script Date: 01/28/2010 15:40:58 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[pRefreshAll_CellColor]
@MonthYear nvarchar(7)

AS

--if nothing is passed in (called from the nightly job), calc the default MonthYear to refresh
if @MonthYear Is null
begin
	declare @CurrDate datetime, @MM varchar(4), @YYYY varchar(4)
	set @CurrDate=getdate()


	set @MM=month(@CurrDate)
	--if this is January, set the month to Dec and set year to previous year
	if @MM='1'
		begin
			set @YYYY=year(@CurrDate)-1
			set @MM='12'
		end
	--otherwise, set to last month	
	else
		begin
			set @YYYY=year(@CurrDate)
			set @MM=@MM-1
		end

	if LEN(rtrim(ltrim(@MM)))=1
		begin
			set @MM='0'+ @MM 
		end
		
	set @MonthYear = @MM + '-' + @YYYY
End






	
	declare @mdid int
	
	declare C cursor
	for select MDID from FIMonthlyData where MonthYear=@MonthYear
	
	/*                                                                            */
	open C
	/*                                                                            */
	fetch C into @MDID
	/******************************************************************************/	
	/* MAIN PROCESSING  VERIFICATION                                              */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	
	while @@FETCH_STATUS = 0
	begin	


		exec pMDColorUpdate 'ActDateRec', @mdid
		exec pMDColorUpdate 'AuditFileSent', @mdid
		exec pMDColorUpdate 'AuditFileApproved', @mdid
		exec pMDColorUpdate 'PostedToWeb', @mdid
		exec pMDColorUpdate 'FilesToRNProd', @mdid
		exec pMDColorUpdate 'LiabSummSent', @mdid
		exec pMDColorUpdate 'EmailStmtSent', @mdid
		exec pMDColorUpdate 'QStmtToRNProd', @mdid	


	goto Next_Record

	Next_Record:
			fetch C into @MDID
	end
	
	
	Fetch_Error:
	close C
	deallocate C

GO


