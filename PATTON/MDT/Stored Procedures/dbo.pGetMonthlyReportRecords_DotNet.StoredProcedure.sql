SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetMonthlyReportRecords_DotNet]  
	@UserID int=null,
	@MonthYear varchar(7) =null, 
	@SortCol varchar(50) =null,
	@TipFirst varchar(3)=null,
	@DBProcessorID int =null,
	@ProgramName varchar(50)=null
AS

declare @SQLSelect nvarchar(1000) ,  @UserGroupCode varchar(1)

Select @UserGroupCode =UserGroupCode from Userinfo where sid_userinfo_id=@UserID

if @UserGroupCode not in ('M')
begin	

	--Processor Selected
	If @DBProcessorID is not null --and @TipFirst IS NULL and @ProgramName is null
	begin
		set @SQLSelect ='SELECT FIMonthlyData.*,  userinfo.dim_userinfo_fname AS fname
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and DBProcessorID=@DBProcessorID order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @DBProcessorID int, @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @DBProcessorID=@DBProcessorID, @SortCol=@SortCol
	End
	--ProgramName Selected
end

if @UserGroupCode ='M'
begin	
	set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   
			WHERE  DBNumber  in (select DBNumber from FI where PMUserID=@UserID )
			AND FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear  order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@UserID int,@MonthYear nvarchar(7), @ProgramName varchar(50), @SortCol nvarchar(25) ', @UserID=@UserID,@MonthYear=@MonthYear, @ProgramName=@ProgramName, @SortCol=@SortCol
	print 'M-SQL:' + @SQLSelect

End
GO
