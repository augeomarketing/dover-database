SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pGetReminderinfo]
@DBNumber varchar(3) , 
@MonthYear varchar(3) , 
@CellImage varchar(200) output,
@ReturnString varchar(255) output

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
--------------------Look up the reminder summary info and create a string witht ther return message
declare @AnchorOpen varchar(200), @AnchorClose varchar(10),@NumOverdue int

select @NumOverdue =Count(*) from FIReminders 
where IsReminderOn=1
and DBNumber=@DBNumber
and ReminderStartDate<GetDate()

Set @ReturnString=''
set @AnchorClose='</a>'
set @AnchorOpen='<A href=''../DataTracking/ReminderEdit.asp?FormMethod=CREATE&MonthYear=' + @MonthYear + '&FISelector=' + @DBNumber + '&DBNumber=' + @DBNumber + '''>'

if @NumOverDue>0 
	begin
		set @ReturnString='Reminders-' + convert(varchar(3),@NumOverdue) + ' overdue  '
		set @CellImage=@AnchorOpen + '<img src=''../images/Flag.gif'' border=0 alt=''' + @ReturnString + '''>'  + @AnchorClose
	end

if @NumOverDue=0 
	begin
		set @ReturnString='Reminders-' +  convert(varchar(3),@NumOverdue) + ' overdue'
		set @CellImage=@AnchorOpen + '<img src=''../images/More.jpg'' border=0 height=20 width=20  alt=''' + @ReturnString + '''>' + @AnchorClose
	end

COMMIT TRANSACTION
GO
