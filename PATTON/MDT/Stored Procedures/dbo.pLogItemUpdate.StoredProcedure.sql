SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pLogItemUpdate]
@UserID int =null,
@FormMethod varchar(15) ,
@LogItemID int=null , 
@NoteDate dateTime=null,
@LogNote nvarchar(1000),

@ReturnString varchar(255) output,
@NewPKVal Varchar(3) output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------

declare @UserName varchar(50)
select @UserName = dim_Userinfo_username from UserInfo where sid_Userinfo_id=@UserID


-------------------------------------------
-------------------------------------Update
if @FormMethod ='UPDATE'
Begin
		Update LogFIMonthly set 
		NoteDate	=	@NoteDate, 
		LogNote	=	@LogNote
		where LogItemID=	@LogItemID

		------------------------
declare @MDID int, @LogItem nvarchar(100)
Select @MDID=MDID , @LogItem=LogItem  from LogFIMonthly where LogItemID=@LogItemID

	Declare @MonthYear varchar(7), @DBNumber varchar(3)
	select @MonthYear=MonthYear, @DBNumber=DBNumber  from LogFIMonthly where LogItemID=@LogItemID
	Print @LogItem


	If @LogItem='DATE DATA FILES RECEIVED'
	Begin
		Update FIMonthlyData set ActDateRec=@NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'ActDateRec' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End

	If @LogItem='AUDIT FILES SENT'
	Begin
		Update FIMonthlyData set AuditFileSent=@NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'AuditFileSent' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End
	If @LogItem='AUDIT FILES APPROVED'
	Begin
		Update FIMonthlyData set AuditFileApproved=@NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'AuditFileApproved' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End
	If @LogItem='POSTED TO WEB'
	Begin
		Update FIMonthlyData set PostedtoWeb = @NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'PostedtoWeb' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
		
	End
	If @LogItem='FILES TO RN PROD'
	Begin
		Update FIMonthlyData set FilestoRNProd =@NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'FilestoRNProd' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End
	If @LogItem='LIABILITY SENT'
	Begin
		--Update FIMonthlyData set LiabSummSent=@NoteDate where MDID=@MDID
		Update FIMonthlyData set LiabSummSent=@NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'LiabSummSent' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End
	If @LogItem='EMAIL STMT SENT'
	Begin
		Update FIMonthlyData set EmailStmtSent = @NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'EmailStmtSent' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End
	If @LogItem='Q-STMT SENT'
	Begin
		Update FIMonthlyData set QStmtToRNProd = @NoteDate, LastUserName=@UserName, LastUpdated=GetDate() where MDID=@MDID
		exec pMDColorUpdate 'QStmtToRNProd' , @MDID
		exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	End

------------------------------Linked Items

	exec pLinkedItemsUpdate	@DBNumber, @LogItem, @LogNote, @MonthYear,	@NoteDate
	
End
------------------------------END Linked Items
if @FormMethod ='DELETE'
	Begin
		DELETE LogItemsFI  where LogItemID	=	@LogItemID
	End
--------------------------------------------------------------------------
	---create the history record
	insert into FIMonthlyData_history 
		Select mdID, DBNumber, MonthYear, SLAForFilesReceived, ImpFilesDueDate, ActDateRec, Comments, AuditFileSent, AuditFileApproved, PostedToWeb, FilesToRNProd, LiabSummSent, EmailStmtSent, QStmtToRNProd , LastUpdated, LastUserName  from FIMonthlyData where mdID=@MDID



-------------------------------------------------------------------------
-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
