SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pEmailStatementSent_BulkUpdate]
@UserID int =null,
@FormMethod varchar(15) ,
@MonthYear varchar(7)=null, 
@DateCompleted dateTime=null,
@FieldName nvarchar(100),
@WhereClauseSQL nvarchar(1000),

@ReturnString varchar(255) output,
@NewPKVal Varchar(3) output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------

declare @UserName varchar(50), @SQLBase nvarchar(1000), @SQLUpdate nvarchar(1000), @LastUserName varchar(50)
select @UserName = dim_Userinfo_username from UserInfo where sid_Userinfo_id=@UserID
 

if @FormMethod='UPDATEBULK'
Begin

set @SQLBase='update FIMonthlyData SET ' + @FieldName + '=@DateCompleted, LastUserName=@userName, LastUpdated=GetDate() where MonthYear=@MonthYear and '
set @SQLUpdate = @SQLBase  + @WhereClauseSQL

print @SQLUpdate

Exec sp_executesql @SQLUpdate, N'@FieldName nvarchar(50), @MonthYear nvarchar(7),  @DateCompleted DateTime, @UserName nvarchar(50) ', @FieldName=@FieldName, @MonthYear=@MonthYear,@DateCompleted=@DateCompleted, @UserName=@UserName

--============================================
--Create history records for all of the same ones
	declare @mdid int, @SQL_Cursor nvarchar(1000)

 set @SQL_Cursor= 'declare C cursor for 
	select MDID from FIMonthlyData where MonthYear=@MonthYear and ' + @WhereClauseSQL
EXEC sp_executesql  @SQL_Cursor, N'@MonthYear nvarchar(7)', @MonthYear=@MonthYear


	/*                                                                            */
	open C
	/*                                                                            */
	fetch C into @MDID
	/******************************************************************************/	
	/* MAIN PROCESSING  VERIFICATION                                              */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	
	while @@FETCH_STATUS = 0
	begin	
	--create the history record
	insert into FIMonthlyData_history 
		Select mdID, DBNumber, MonthYear, SLAForFilesReceived, ImpFilesDueDate, ActDateRec, Comments, AuditFileSent, AuditFileApproved, PostedToWeb, FilesToRNProd, LiabSummSent, EmailStmtSent, QStmtToRNProd , LastUpdated, LastUserName  from FIMonthlyData where mdID=@MDID
		--update the color
		exec pMDColorUpdate 'EmailStmtSent', @mdid

	goto Next_Record

	Next_Record:
			fetch C into @MDID
	end
	
	
	Fetch_Error:
	close C
	deallocate C



End








-------------------------------------------------------------------------
-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
