SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE  [dbo].[pLogin]
@UserName varchar(50) , 
@UserPW varchar(10) 

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION

SELECT UserInfo.*  FROM UserInfo
where IsActive=1 and dim_userinfo_password=@UserPW and dim_userinfo_UserName=@UserName


COMMIT TRANSACTION
GO
