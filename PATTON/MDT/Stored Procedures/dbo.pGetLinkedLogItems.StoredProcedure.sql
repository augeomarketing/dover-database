SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetLinkedLogItems] 
	@LogItemID int=null,
	@DBNumber varchar(3) =null
AS

Declare @LogItem varchar(50), @DTLinked datetime

Select @LogItem=LogItem from LogItems where logItemID=@LogItemID and DBNumber=@DBNumber
Select @DTLinked =DTLinked from LogItemsLinked where DBNumber=@DBNumber and LogItem=@LogItem


Select * from LogItemsLinked where LogItem=@LogItem and DTLinked=@DTLinked--and DBNumber=@DBNumber
GO
