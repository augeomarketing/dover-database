USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pReportsA]    Script Date: 02/17/2010 13:32:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pReportsA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pReportsA]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pReportsA]    Script Date: 02/17/2010 13:32:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[pReportsA]  
	@UserID int=null,
	@MonthYear varchar(7) =null, 
	@SortCol varchar(50) =null,
	@TipFirst varchar(3)=null,
	@DBProcessorID int =null,
	@ProgramName varchar(50)=null
AS
--this sproc is used by the RedAndEmpty page. the Hex val in the dynamiic sql below indicates the Pink color that is set in pMDColorUpdate
declare @SQLSelect nvarchar(1000) ,  @UserGroupCode varchar(1), @FieldName varchar(30)

set @FieldName=substring(@SortCol,1,CHARINDEX ( '_' , @SortCol )-1    )
Select @UserGroupCode =UserGroupCode from Userinfo where sid_userinfo_id=@UserID


	--retrun recordset for the selected month where the selected column has a NULL 
	--value and the Color of the field is RED (pink now)
	set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   
			WHERE FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear  
			AND ' + @SortCol + '=''#FFC2C2''  
			AND ' + @FieldName + ' IS NULL 
			order by DBNumber'
		Exec sp_executesql @SQLSelect, N'@UserID int,@MonthYear nvarchar(7), @ProgramName varchar(50), @SortCol nvarchar(25), @FieldName nvarchar(25) ', @UserID=@UserID,@MonthYear=@MonthYear, @ProgramName=@ProgramName, @SortCol=@SortCol, @FieldName=@FieldName
	print 'M-SQL:' + @SQLSelect




GO


