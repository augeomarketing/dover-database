USE [MDT]
GO
/****** Object:  StoredProcedure [dbo].[pStatementReminderCreation]    Script Date: 10/02/2014 11:29:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	10/02/14
	1)changed @ReminderStartDate to default to the FIRST instead of the 10th
	2)changed the default value of IsEmailReminder to 0 instead of 1
*/
ALTER PROCEDURE  [dbo].[pStatementReminderCreation]
@MonthYear Varchar(7) 
AS 

declare @ReminderStartDate date
set @MonthYear = RTRIM(@MonthYear)
set @ReminderStartDate = convert(date, LEFT(@MonthYear, 2) + '/01/' + RIGHT(@MonthYear, 4)) -- changed from 10 to 01 so reminders would start on the first
set @ReminderStartDate = DATEADD(m, 1, @ReminderStartDate)

Begin
	INSERT INTO FIReminders (DBNumber, ReminderStartDate, IsReminderOn, ReminderNote, IsEmailReminder)
	SELECT 
		LEFT(Tipfirst, 3)
		, @ReminderStartDate
		, 1
		, LEFT(TipFirst, 3) 
			+ ' - Quarterly/Periodic Statement Due: ' 
			+ RTRIM(DataRange) 
			+ '   PREVIOUS QTY-- ' 
			+ LTRIM(RTRIM(CONVERT(VARCHAR,Quantity)))
		,	0	   --
		
	FROM
		StatementsDue
	WHERE
		MonthYear = @MonthYear
		AND Tipfirst IS NOT NULL
End
