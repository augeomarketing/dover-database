SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pEmailPassword]
@Email varchar(50), 
@ReturnString varchar(255) output
AS
		declare @IsValidEmail int
		Select @IsValidEmail=count(*) from tUsers where Email=@Email and IsActive=1
		
		if @IsValidEmail=0
			begin
				 set @ReturnString='There are no active users with this email address. Please contact your system adminitrator.'
				return
			end
		if @IsValidEmail > 1
			begin
				set @ReturnString='There is more than one user in the system with this email address. Please contact your system adminitrator.'
				return
			end
		if @IsValidEmail = 1
		begin
			declare @MsgSubj varchar(200), @MsgBody varchar(200), @Password varchar(20)
			Select @Password=UserPW from tUsers where Email=@Email
			set @MsgSubj='Your lost password'
			set @MsgBody='You password is ' + @Password
			print 'Email :' + @Email + char(10)
			print 'Subject :' + @MsgSubj + char(10)
			print 'Body:' + @MsgBody + char(10)
			set @ReturnString='Your password has been sent to your email address: '+ @Email
			return
			exec .master..xp_sendmail @recipients = @Email, @subject=@MsgSubj, @message=@MsgBody
		end
GO
