USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pDocumentSearch]    Script Date: 04/21/2010 13:58:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pDocumentSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pDocumentSearch]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pDocumentSearch]    Script Date: 04/21/2010 13:58:30 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[pDocumentSearch]  
	@UserID int=null,
	@DocGroupCodeID int =null, 
	@SearchText varchar(200) =null,
	@ReturnString varchar(255) output 
AS

declare @SQLSelect nvarchar(1000) 


	

	If @DocGroupCodeID IS NOT NULL AND @SearchText IS  NULL 
	begin

		
			set @SQLSelect ='SELECT Docs.*, tluDocGroupCodes.DocGroupCode, userinfo.dim_userinfo_username as username	 
				From Docs  join UserInfo on docs.LastUserID=UserInfo.sid_userinfo_id
				join tluDocGroupCodes on tluDocGroupCodes.DocGroupCodeID=Docs.DocGroupCodeID
				WHERE  DocID in (select DocID from DocsGroups where DocGroupCodeID=@DocGroupCodeID) 
				AND docs.ISACTIVE=1 order by DocTitle desc'
			
			Exec sp_executesql @SQLSelect, N'@DocGroupCodeID int ', @DocGroupCodeID =@DocGroupCodeID

	End
	
	If @DocGroupCodeID IS  NULL AND @SearchText IS NOT NULL 
	begin

			set @SQLSelect ='SELECT Docs.*, tluDocGroupCodes.DocGroupCode, userinfo.dim_userinfo_username as username	 
				From Docs join UserInfo on docs.LastUserID=UserInfo.sid_userinfo_id
				join tluDocGroupCodes on tluDocGroupCodes.DocGroupCodeID=Docs.DocGroupCodeID
				WHERE  (DocTitle like ''%' + @SearchText + '%'' 
					OR DocText like ''%' + @SearchText + '%'' ) AND docs.ISACTIVE=1 order by DocTitle desc
				
				
				
				'
			Exec sp_executesql @SQLSelect, N'@SearchText Varchar(200) ', @SearchText =@SearchText

	End	

	If @DocGroupCodeID IS NOT NULL AND @SearchText IS NOT NULL 
	begin
		set @SQLSelect ='SELECT Docs.*, tluDocGroupCodes.DocGroupCode, userinfo.dim_userinfo_username as username	 
			From Docs join UserInfo on docs.LastUserID=UserInfo.sid_userinfo_id
			join tluDocGroupCodes on tluDocGroupCodes.DocGroupCodeID=Docs.DocGroupCodeID
			WHERE  (DocTitle like ''%' + @SearchText + '%'' OR DocText like ''%' + @SearchText + '%'') 
			AND 
				(DocID in (select DocID from DocsGroups where DocGroupCodeID=@DocGroupCodeID)
				or
				DocID in (select DocID from Docs where DocGroupCodeID=@DocGroupCodeID)	)
			 AND docs.ISACTIVE=1  order by DocTitle desc'
		Exec sp_executesql @SQLSelect, N'@SearchText Varchar(200), @DocGroupCodeID int ', @SearchText =@SearchText,@DocGroupCodeID =@DocGroupCodeID
	End	



/*	
	--Processor Selected
	If @DBProcessorID is not null --and @TipFirst IS NULL and @ProgramName is null
	begin
		set @SQLSelect ='SELECT FIMonthlyData.*,  userinfo.dim_userinfo_fname AS fname, FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and FIMonthlyData.DBProcessorID=@DBProcessorID order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @DBProcessorID int, @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @DBProcessorID=@DBProcessorID, @SortCol=@SortCol
	End
	--ProgramName Selected
	If @ProgramName is not null 
	begin
		set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname, FI.IsMonthlyApprovalReq
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   INNER JOIN  FI  on FIMonthlyData.DBNumber=FI.DBNumber WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and ProgramName=@ProgramName order by ' + @SortCol + '  desc'
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @ProgramName varchar(50), @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @ProgramName=@ProgramName, @SortCol=@SortCol

*/
	print @SQLSelect


GO


