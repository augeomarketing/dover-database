SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pLogItemsReBuild]
@DBNumber varchar(3), 
@MonthYear varchar(7)=NULL,
@mdid int=NULL

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION


declare @CurrSeqNum int, @NewSeqNum int, @MaxSeqNum int, @MinSeqNum int, @MaxItemID int, @MinItemID int
Declare @LogItemID int, @LogItem nvarchar(50), @LogNote nvarchar(1000)



if @MonthYear IS NULL
begin
	--DELETE ALL LogItems for the DBNumber
	Delete LogItems where DBNumber=@DBNumber
	
	-----------------------
	--Insert standard items
	-------------------------
	Insert into LogItems (  LogItemID,LogItem,  LogNote, DBNumber, SeqNum, IsStdItem )
		Select LogItemID, LogItem, LogNote, @DBNumber, SeqNum,1 from LogItemsSTD where LogItemID not in (Select LogitemID from LogItems where DBNumber=@DBNumber)
	-------------------------
	
	
	--Determine the next SequenceNumber and One-up it for each LogItemFI entry that gets inserted into Logitems 
	Select @MaxSeqNum=Max(SeqNum) from LogItems where DBNumber=@DBNumber 
	
		declare C cursor
		for 
			SELECT  LogItemID,LogItem, LogNote from LogItemsFI where DBNumber=@DBNumber 
			AND  LogItemID not in (Select LogitemID from LogItems where DBNumber=@DBNumber)
	
		open C
		fetch C into @LogItemID, @LogItem, @LogNote
		if @@FETCH_STATUS = 1
			goto Fetch_Error
		
		while @@FETCH_STATUS = 0
		begin	
	
			Set @MaxSeqNum=@MaxSeqNum +1
			
			Insert into LogItems (  LogItemID, LogItem,  LogNote, DBNumber, SeqNum, IsStdItem )
				values( @LogItemID, @LogItem, @LogNote, @DBNumber, @MaxSeqNum, 0 )
			
	
			goto Next_Record
		
		
		
		Next_Record:
			fetch C into @LogItemID, @LogItem, @LogNote
		end
		
		
		Fetch_Error:
		close C
		deallocate C

End
	
--also , add ACTIVE but new FI items and delete inactive FI items


if @MonthYear IS NOT NULL
begin
	Delete LogFIMonthly where MonthYear=@MonthYear and DBNumber=@DBNumber
	Insert into LogFIMonthly (MDID,MonthYear,DBNumber,LogItem,LogNote,SeqNum,IsStdItem )
			Select  @MDID, @MonthYear,@DBNumber,LogItem,LogNote,SeqNum,IsStdItem from LogItems where DBNumber=@DBNumber
End




COMMIT TRANSACTION
GO
