SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetMonthlyReportRecords_VIEWONLY]  
	@UserID int=null,
	@MonthYear varchar(7) =null, 
	@SortCol varchar(50) =null,
	@TipFirst varchar(3)=null,
	@DBProcessorID int =null,
	@ProgramName varchar(50)=null
AS

declare @SQLSelect nvarchar(1000) ,  @UserGroupCode varchar(1)


	--if Nothing but Month is selected
	If @TipFirst is null AND @DBProcessorID is null AND @ProgramName is null
	begin
		set @SQLSelect ='SELECT FIMonthlyData.*  ,  userinfo.dim_userinfo_fname AS fname
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  WHERE  FIMonthlyData.IsActive=1 and MonthYear =@MonthYear order by ' + @SortCol + ''
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @SortCol=@SortCol
	End
	-- if more than one of the criteria below are met, the FIRST one run is the recordset shown on the web page 
	--Tipfirst Selected 
	If @TipFirst IS NOT NULL 
	begin
		set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   WHERE  FIMonthlyData.IsActive=1 and DBNumber=@TipFirst order by ' + @SortCol + ' '
		Exec sp_executesql @SQLSelect, N'@TipFirst varchar(3), @SortCol nvarchar(25) ', @TipFirst=@TipFirst, @SortCol=@SortCol
	End
	--Processor Selected
	If @DBProcessorID is not null --and @TipFirst IS NULL and @ProgramName is null
	begin
		set @SQLSelect ='SELECT FIMonthlyData.*,  userinfo.dim_userinfo_fname AS fname
	 From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id  WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and DBProcessorID=@DBProcessorID order by ' + @SortCol + ''
		Exec sp_executesql @SQLSelect, N'@MonthYear nvarchar(7), @DBProcessorID int, @SortCol nvarchar(25) ', @MonthYear=@MonthYear, @DBProcessorID=@DBProcessorID, @SortCol=@SortCol
	End
	--ProgramName Selected
	If @ProgramName is not null 
	begin
		declare @SQLCondition nvarchar(500)
		Select @SQLCondition=SQLCondition from luTPM where ProgramName=@ProgramName

		set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname
							From FIMonthlyData  INNER JOIN
	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   WHERE  ' + @SQLCondition + ' AND FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear  order by DBNumber'

--		set @SQLSelect ='SELECT FIMonthlyData.* ,  userinfo.dim_userinfo_fname AS fname
--							From FIMonthlyData  INNER JOIN
--	                      userinfo ON FIMonthlyData.DBProcessorID = userinfo.sid_userinfo_id   WHERE  FIMonthlyData.IsActive=1 and   MonthYear =@MonthYear and ProgramName=@ProgramName order by ' + @SortCol + ''


		Exec sp_executesql @SQLSelect, N'@SQLCondition varchar(500), @MonthYear nvarchar(7),  @SortCol nvarchar(25) ', @SQLCondition=@SQLCondition, @MonthYear=@MonthYear,  @SortCol=@SortCol
	End
	print @SQLSelect
GO
