SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE  [dbo].[pLinkLogItems]
@FormMethod varchar(15) , 
@LogItem nvarchar(100) ,
@DBNumber varchar(3), 
@LinkSQL nvarchar(1000),

@ReturnString varchar(255) output,
@NewPKVal Varchar(3) output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION


Declare @SQLDeleteBase nvarchar(250),@SQLInsertBase nvarchar(250)
declare @SQLDelete nvarchar(1000),@SQLInsert nvarchar(1000), @DTLinked datetime, @old_DTLinked datetime

set @DTLinked=GetDate()
Select @old_DTLinked=DTLinked from LogItemsLinked where DBNumber=@DBNumber and LogItem=@LogItem

Set @SQLDelete='DELETE LogItemsLinked 
	WHERE LogItem=@LogItem  AND DTLinked=@Old_DTLinked'
/*
Set @SQLInsertBase='Insert INTO LogItemsLinked (Logitem,DBNumber,LinkSQL) 
	SELECT ''' + @LogItem + ''', DBNumber,''' + @LinkSQL + '''  from FI 
	WHERE ' 
*/
Set @SQLInsertBase='Insert INTO LogItemsLinked (Logitem,DBNumber, DTLinked) 
	SELECT  @LogItem , DBNumber, @DTLinked  from FI 
	WHERE IsActive=1 AND ' 
set @SQLInsert=@SQLInsertBase + @LinkSQL

/*
Set @SQLBase='Insert INTO LogItemsLinked (Logitem,DBNumber,LinkSQL) 
	SELECT @LogItem, DBNumber, @LinkSQL from FI 
	WHERE ' + @LinkSQL
*/
print @SQLDelete--, N'@LogItem nvarchar(100)', @LogItem=@LogItem
print ' '
print @SQLInsert
Exec sp_executesql @SQLDelete , N'@old_DTLinked datetime, @LogItem nvarchar(100)', @old_DTLinked=@old_DTLinked, @LogItem=@LogItem
Exec sp_executesql @SQLInsert  , N'@DTLinked datetime, @LogItem nvarchar(100) ', @DTLinked=@DTLinked, @LogItem=@LogItem
-------------------------------------Create
--if @FormMethod ='CREATE'

-------------------------------------------
-------------------------------------Update


IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
