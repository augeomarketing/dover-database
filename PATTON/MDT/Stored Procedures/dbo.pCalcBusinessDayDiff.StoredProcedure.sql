SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pCalcBusinessDayDiff] 
@NumDays int ,
@PrevStepDate char(10),
@ResultDate char(10) out
AS
declare @X int, @BDCtr int, @DayNum int, @tmpDate datetime, @IsHoliday bit
	set @x=0
	set @BDCtr=0
	set @IsHoliday=0
	set @tmpDate=@PrevStepDate
	while  @BDCtr <= @NumDays
		Begin
			set @IsHoliday=0
			-- see if the day (starting with @FirstDayOfMonth) is a businessDau
			--this could use a call out to a table to not increment the business day counter if the day in the loop is a holiday
			select @DayNum= datepart(dw,dateadd(d,@X,@tmpDate))
			if @DayNum not in (1,7) 
				begin
					declare @t datetime
					set @t=dateadd(dd, @X,@tmpDate)
					exec pCheckForHoliday @t, @IsHoliday = @IsHoliday out
					if @IsHoliday=0	set @BDCtr=@BDCtr+1
					
				end
		set @X=@X + 1
		end
	
	--go out an additional N-1 days from the first	
	set @ResultDate = convert(nvarchar(10),dateadd(d, @X,@PrevStepDate)-1,101)
GO
