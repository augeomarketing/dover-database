SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[pFIEdit]
@UserID int,
@FormMethod varchar(15) ,
@DBNumber varchar(3) , 
@DBNamePatton varchar(50) , 
@IsActive bit, 
@IsMonthlyApprovalReq bit,
@DBProcessorID int=null , 
@NumDay int,
@SLA varchar(50),
@SLAForFilesReceived varchar(50)=null ,
@ActDateRec_SLA int=null,
@AuditFileSent_SLA int=null,
@AuditFileApproved_SLA int=null,
@PostedToWeb_SLA int=null,
@FilesToRNProd_SLA int=null,
@LiabSummSent_SLA int=null,
@EmailStmtSent_SLA int=null,
@QStmtToRNProd_SLA int=null,
@PMUserID int=null,
@OpsUserID int=null,
@MDID int=null,
@ReturnString varchar(255) output,
@NewPKVal Varchar(3) output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod ='CREATE'
begin
Insert into FI ( DBNumber, DBNamePatton, IsActive, IsMonthlyApprovalReq, DBProcessorID, NumDay, SLA, SLAForFilesReceived,  ActDateRec_SLA, AuditFileSent_SLA, AuditFileApproved_SLA, PostedToWeb_SLA, FilesToRNProd_SLA, LiabSummSent_SLA,  EmailStmtSent_SLA , QStmtToRNProd_SLA, PMUserID, OpsUserID )
	 VALUES(   @DBNumber, @DBNamePatton, @IsActive, @IsMonthlyApprovalReq, @DBProcessorID, @NumDay, @SLA, @SLAForFilesReceived,  @ActDateRec_SLA, @AuditFileSent_SLA, @AuditFileApproved_SLA, @PostedToWeb_SLA, @FilesToRNProd_SLA, @LiabSummSent_SLA,  @EmailStmtSent_SLA , @QStmtToRNProd_SLA, @PMUserID, @OpsUserID)
	--SET @DBID = @@Identity 
	SET @NewPKVal = @DBNumber
	
end
-------------------------------------------
-------------------------------------Update
--if the Isactive value has changed then update the FIMonthlyRecords
declare @OrigIsActiveVal bit
select @OrigIsActiveVal =IsActive from FI where DBNumber=@DBNumber
if @OrigIsActiveVal <>@IsActive update FIMonthlyData set IsActive=@IsActive where DBNumber=@DBNumber
----------------------------------
if @FormMethod ='UPDATE'
Begin
		Update FI set 
		DBNumber 	= 	@DBNumber, 
		DBNamePatton	=	@DBNamePatton, 
		IsActive	=	@IsActive, 
		IsMonthlyApprovalReq=@IsMonthlyApprovalReq,
		DBProcessorID	=	@DBProcessorID, 
		NumDay	=	@NumDay,
		SLA		=	@SLA,
		SLAForFilesReceived=	@SLAForFilesReceived, 
		ActDateRec_SLA	=	@ActDateRec_SLA, 
		AuditFileSent_SLA=	@AuditFileSent_SLA, 
		AuditFileApproved_SLA=	@AuditFileApproved_SLA, 
		PostedToWeb_SLA	=	@PostedToWeb_SLA, 
		FilesToRNProd_SLA=	@FilesToRNProd_SLA, 
		LiabSummSent_SLA=	@LiabSummSent_SLA,  
		EmailStmtSent_SLA=	@EmailStmtSent_SLA,  
		QStmtToRNProd_SLA=	@QStmtToRNProd_SLA,
		PMUserID	=@PMUserID,
		OpsUserID	=	@OpsUserID,
		LastUpdated	=	GetDate(),
		LastUserID	=	@UserID
		where DBNumber	=	@DBNumber


		update FIMonthlyData set IsActive=@IsActive where DBNumber=@DBNumber
		------------------------
		if @MDID is not null 
			update FIMonthlyData set DBProcessorID=@DBProcessorID where mdid=@MDID
End
------------------------
if @FormMethod ='DELETE'
	Begin
		DELETE FI  where DBNumber	=	@DBNumber
	End
--------------------------------------------------------------------------
-- exec pLogEvent @UserID, 'tReports', 'DBNumber', @DBNumber, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
