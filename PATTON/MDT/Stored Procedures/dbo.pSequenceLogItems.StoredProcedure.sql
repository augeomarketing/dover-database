SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[pSequenceLogItems]
@FormMethod varchar(15) ,
@LogItemID int=null , 
@DBNumber varchar(3), 
@Direction varchar(4),

@ReturnString varchar(255) output

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION


declare @CurrSeqNum int, @NewSeqNum int, @MaxSeqNum int, @MinSeqNum int, @MaxItemID int, @MinItemID int

-------------------------------------Create
Declare @LogItem nvarchar(50), @LogNote nvarchar(1000)

-------------------------------------------
select @MinSeqNum=Min(SeqNum) from LogItems where DBNumber=@DBNumber 
select @MinItemID=Min(LogItemID) from LogItems where DBNumber=@DBNumber 
select @MaxSeqNum=Max(SeqNum) from LogItems where DBNumber=@DBNumber 
select @MaxItemID=Max(LogItemID) from LogItems where DBNumber=@DBNumber 
-------------------------------------Update

declare @CurrLogItemID int, @NewLogItemID int
if @FormMethod ='SEQUENCE'
Begin
	if @Direction='UP'
	Begin
	select @CurrSeqNum=SeqNum from LogItems where LogItemID=@LogItemID  and DBNumber=@DBNumber 
	Set @NewSeqNum=@CurrSeqNum-1
	select @CurrLogItemID =@LogItemID
	select @NewLogItemID=LogItemID from LogItems where SeqNum=@NewSeqNum  and DBNumber=@DBNumber 
					    										
		if @CurrSeqNum<> @MinSeqNum
		begin			--increment the SeqNum of  all but the Current record  
			Update LogItems set SeqNum=@NewSeqNum where DBNumber=@DBNumber and LogItemID=@CurrLogItemID
			Update LogItems set SeqNum=@CurrSeqNum where DBNumber=@DBNumber and LogItemID=@NewLogItemID
		End
	End

	if @Direction='DOWN'
	Begin
	select @CurrSeqNum=SeqNum from LogItems where LogItemID=@LogItemID  and DBNumber=@DBNumber 
	Set @NewSeqNum=@CurrSeqNum+1
	select @CurrLogItemID =@LogItemID
	select @NewLogItemID=LogItemID from LogItems where SeqNum=@NewSeqNum  and DBNumber=@DBNumber 
					    										
		if @CurrSeqNum<> @MaxSeqNum
		begin			--increment the SeqNum of  all but the Current record  
			Update LogItems set SeqNum=@NewSeqNum where DBNumber=@DBNumber and LogItemID=@CurrLogItemID
			Update LogItems set SeqNum=@CurrSeqNum where DBNumber=@DBNumber and LogItemID=@NewLogItemID
		End
	End


End
------------------------
--------------------------------------------------------------------------
if @FormMethod ='DELETE'
Begin
 	--Delete the Record from both LogItems and LogItemsFI

	declare @SeqNumDeleted int
	Select @SeqNumDeleted =SeqNum from LogItems  where LogItemID=@LogItemID and DBNumber=@DBNumber

	DELETE  Logitems where LogItemID=@LogItemID and DBNumber=@DBNumber
	--Re-sequence LogItems
	update LogItems set SeqNum =SeqNum-1 where SeqNum > @SeqNumDeleted and DBNumber=@DBNumber
	

	DELETE  LogitemsFI where LogItemID=@LogItemID and DBNumber=@DBNumber
End


IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= 'ERROR OCCURRED' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
GO
