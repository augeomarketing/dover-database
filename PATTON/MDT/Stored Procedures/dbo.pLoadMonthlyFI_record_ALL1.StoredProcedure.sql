SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pLoadMonthlyFI_record_ALL1]
	@UserID int,
	@MonthYear nvarchar(7),
	@DeleteFlag int=null
 AS

if @DeleteFlag is not null
	Begin
		--delete FIMonthlyData_Notes where MDID in (select mdid from FIMonthlyData where MonthYear=@MonthYear)
		delete FIMonthlyData_Email where MDID in (select mdid from FIMonthlyData where MonthYear=@MonthYear)
		delete FIMonthlyData where MonthYear=@MonthYear and DBProcessorID=@UserID
		--delete FIMonthlyData_history where MonthYear=@MonthYear 
		
	End
Else
Begin
	
	declare @DBNumber char(3)
	
	
	
	declare C cursor
	for select DBNumber 
	from FI WHERE  Isactive=1 and DBProcessorID=@UserID
	
	/*                                                                            */
	open C
	/*                                                                            */
	fetch C into @DBNumber
	/******************************************************************************/	
	/* MAIN PROCESSING  VERIFICATION                                              */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	
	while @@FETCH_STATUS = 0
	begin	
		declare @SLA datetime
		--if there are no entries for this month then just add them
		if not exists (Select * from FIMonthlyData where DBNumber=@DBNumber and MonthYear=@MonthYear )
			begin
				
				exec pCalcSLA_BaseDate @DBNumber, @MonthYear, @SLADate = @SLA out
				exec pLoadMonthlyFI_record @DBNumber, @MonthYear,  @SLA
			end
		Else	--otherwise, Delete the existing and re-create
			begin
				DELETE from  FIMonthlyData where DBNumber=@DBNumber and MonthYear=@MonthYear
	
				
				exec pCalcSLA_BaseDate @DBNumber, @MonthYear, @SLADate = @SLA out
				exec pLoadMonthlyFI_record @DBNumber, @MonthYear, @SLA
			End
	goto Next_Record
	
	
	
	Next_Record:
			fetch C into @DBNumber
	end
	
	
	Fetch_Error:
	close C
	deallocate C
End
GO
