USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pMDColorUpdate]    Script Date: 02/15/2010 10:45:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pMDColorUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pMDColorUpdate]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[pMDColorUpdate]    Script Date: 02/15/2010 10:45:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pMDColorUpdate] 
@FieldName nvarchar(50),
@PKval int
AS

-- this sproc takes a field name and updates the color value for that field based on the Field_SLADate value relative to today's date
declare @FieldDate datetime, @Field_SLADate nvarchar(25),@dtField datetime, @dtSLA datetime, @Color nvarchar(10)
declare @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000)
set @Color=''


set @SQLSelect ='Select  @FieldDate = ' + @FieldName + ', @Field_SLADate='+ @FieldName + '_SLADate FROM FIMonthlyData where MDID=' + convert(nvarchar(25),@PKVal)
--print @SQLSelect
Exec sp_executesql @SQLSelect, N'@FieldName nvarchar(50), @Field_SLADate nvarchar(50) output, @PKVal int, @FieldDate Datetime output', @FieldName=@FieldName, @Field_SLADate=@Field_SLADate output,  @PKVal=@PKVal ,@FieldDate=@FieldDate output
set @dtSLA=convert(datetime,@Field_SLADate)
set @FieldDate=convert(datetime,convert(varchar(25),@FieldDate,101))
if (@FieldDate is  null)
Begin
	if @dtSLA = convert(datetime,convert(varchar(25),GetDate(),101)) set @Color='#FFFF6B'--yellow
	if @dtSLA < convert(datetime,convert(varchar(25),GetDate(),101)) set @Color='#FFC2C2' --'RED'
	if @dtSLA > convert(datetime,convert(varchar(25),GetDate(),101)) set @Color='#B5FF6B' --green
End
Else
Begin
	if @dtSLA = @FieldDate set @Color='#B5FF6B' --green
	if @dtSLA < @FieldDate set @Color='#FFC2C2' --pinkinsh
	if @dtSLA > @FieldDate set @Color='#B5FF6B'   --green
End
--declare @FieldDate nvarchar(25), @Field_SLADate nvarchar(25)
--Select  @FieldDate = ActDateRec, @Field_SLADate=ActDateRec_SLADate FROM FIMonthlyData where MDID=15
--print 'FD=' + convert(varchar(25),@FieldDate,101)
--print 'SLa:' + convert(varchar(25),@dtSLA,101)
--print 'Color:'+ @Color
set @SQLUpdate='update FIMonthlyData SET ' + @FieldName + '_color=@Color where mdid=' + convert(nvarchar(10),@PKVal )
--print @SQLUpdate
Exec sp_executesql @SQLUpdate, N'@FieldName nvarchar(50), @Color nvarchar(10),  @PKval int ', @FieldName=@FieldName, @Color=@Color ,@PKval=@PKval

GO


