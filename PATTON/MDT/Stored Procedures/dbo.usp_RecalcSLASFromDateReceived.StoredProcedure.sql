USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[usp_RecalcSLASFromDateReceived]    Script Date: 10/03/2014 16:53:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RecalcSLASFromDateReceived]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RecalcSLASFromDateReceived]
GO

USE [MDT]
GO

/****** Object:  StoredProcedure [dbo].[usp_RecalcSLASFromDateReceived]    Script Date: 10/03/2014 16:53:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_RecalcSLASFromDateReceived]
	@DBNumber char(3),
	@MonthYear nvarchar(7), 
	@ADR datetime --ActualDateReceived
AS
SET NOCOUNT ON
---------------------------
-- DATABASE INFO / SLA'S
---------------------------
DECLARE @DBNamePatton nchar(50)
DECLARE @AuditFileSent_SLA int
DECLARE @AuditFileApproved_SLA int
DECLARE @PostedToWeb_SLA int
DECLARE @FilesToRNProd_SLA int
DECLARE @LiabSummSent_SLA int
DECLARE @EmailStmtSent_SLA int
DECLARE @QStmtToRNProd_SLA int
DECLARE @DBProcessorID int
DECLARE @IsActive bit
DECLARE @IsMonthlyApprovalReq bit
DECLARE @ProgramName varchar(50)
---------------------------------
--         DATES
---------------------------------

DECLARE @AFS  nchar(10),  
		@AFA  nchar(10), 
		@PTW  nchar(10),  
		@FTP  nchar(10), 
		@LSS  nchar(10), 
		@ESS  nchar(10), 
		@QSP  nchar(10)

declare @mdid int --Monthly Data Line ID

Select 
	@DBNamePatton = DBNamePatton
	, @AuditFileSent_SLA = AuditFileSent_SLA
	, @AuditFileApproved_SLA = AuditFileApproved_SLA
	, @PostedToWeb_SLA = PostedToWeb_SLA
	, @FilesToRNProd_SLA = FilesToRNProd_SLA
	, @LiabSummSent_SLA = LiabSummSent_SLA
	, @EmailStmtSent_SLA = EmailStmtSent_SLA
	, @QStmtToRNProd_SLA = QStmtToRNProd_SLA
	, @DBProcessorID	=DBProcessorID
	, @IsActive		=IsActive
	, @IsMonthlyApprovalReq=IsMonthlyApprovalReq
	, @ProgramName		= ProgramName
from FI 
	where DBNumber=@DBNumber

SET @AFS = CONVERT(nchar(10),dbo.CalculateSLADate(@AuditFileSent_SLA, @ADR), 101)
SET @AFA = CONVERT(nchar(10),dbo.CalculateSLADate(@AuditFileApproved_SLA, @AFS), 101)
SET @PTW = CONVERT(nchar(10),dbo.CalculateSLADate(@PostedToWeb_SLA, @AFA), 101)
SET @FTP = CONVERT(nchar(10),dbo.CalculateSLADate(@FilesToRNProd_SLA, @PTW), 101)
SET @LSS = CONVERT(nchar(10),dbo.CalculateSLADate(@LiabSummSent_SLA, @FTP), 101)
SET @QSP = CONVERT(nchar(10),dbo.CalculateSLADate(@QStmtToRNProd_SLA, @LSS), 101)
SET @ESS = CONVERT(nchar(10),dbo.CalculateSLADate(@EmailStmtSent_SLA, @QSP ), 101)

UPDATE FIMonthlyData
SET AuditFileSent_SLADate		= @AFS
	, AuditFileApproved_SLADate = @AFA
	, PostedToWeb_SLADate		= @PTW
	, FilesToRNProd_SLADate		= @FTP
	, LiabSummSent_SLADate		= @LSS 
	, EmailStmtSent_SLADate		= @ESS
	, QStmtToRNProd_SLADate		= @QSP
WHERE
	DBNumber = @DBNumber
	AND MonthYear = @MonthYear
	
SELECT @mdid = mdID FROM FIMonthlyData WHERE DBNumber = @DBNumber AND MonthYear = @MonthYear

exec pMDColorUpdate 'ActDateRec', @mdid
exec pMDColorUpdate 'AuditFileSent', @mdid
exec pMDColorUpdate 'AuditFileApproved', @mdid
exec pMDColorUpdate 'PostedToWeb', @mdid
exec pMDColorUpdate 'FilesToRNProd', @mdid
exec pMDColorUpdate 'LiabSummSent', @mdid
exec pMDColorUpdate 'EmailStmtSent', @mdid
exec pMDColorUpdate 'QStmtToRNProd', @mdid




GO


