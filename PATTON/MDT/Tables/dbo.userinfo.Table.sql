SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userinfo](
	[sid_userinfo_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_userinfo_username] [varchar](50) NOT NULL,
	[dim_userinfo_password] [varchar](50) NOT NULL,
	[dim_userinfo_fname] [varchar](50) NOT NULL,
	[dim_userinfo_lname] [varchar](50) NOT NULL,
	[IsAdmin] [int] NULL,
	[IsActive] [bit] NULL,
	[IsReadOnly] [bit] NULL,
	[UserGroupCode] [varchar](1) NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[userinfo] ADD  CONSTRAINT [DF_userinfo_IsAdmin]  DEFAULT (0) FOR [IsAdmin]
GO
