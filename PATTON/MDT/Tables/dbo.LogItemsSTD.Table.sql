SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogItemsSTD](
	[LogItemID] [int] NOT NULL,
	[LogItem] [nvarchar](100) NULL,
	[SeqNum] [int] NULL,
	[LogNote] [nvarchar](1000) NULL,
 CONSTRAINT [PK_LogItemsSTD] PRIMARY KEY CLUSTERED 
(
	[LogItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'used to determin sequence and to identify which Monthly step to auto complete' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogItemsSTD', @level2type=N'COLUMN',@level2name=N'LogItemID'
GO
