USE [MDT]
GO

/****** Object:  Table [dbo].[maplogitemsstdfimonthlydata]    Script Date: 12/20/2010 12:14:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[maplogitemsstdfimonthlydata](
	[sid_maplogitemsstdfimonthlydata_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_maplogitemsstdfimonthlydata_LogItemID] [int] NOT NULL,
	[dim_maplogitemsstdfimonthlydata_fimonthlydatacolumn] [varchar](50) NOT NULL,
 CONSTRAINT [pk_maplogitemsstdfimonthlydata_ID] PRIMARY KEY CLUSTERED 
(
	[sid_maplogitemsstdfimonthlydata_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [uc_maplogitemsstdfimonthlydata_LogItemIDfimonthlydatacolumn] UNIQUE NONCLUSTERED 
(
	[sid_maplogitemsstdfimonthlydata_LogItemID] ASC,
	[dim_maplogitemsstdfimonthlydata_fimonthlydatacolumn] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[maplogitemsstdfimonthlydata]  WITH CHECK ADD  CONSTRAINT [fk_maplogitemsstdfimonthlydata_LogItemID] FOREIGN KEY([sid_maplogitemsstdfimonthlydata_LogItemID])
REFERENCES [dbo].[LogItemsSTD] ([LogItemID])
GO

ALTER TABLE [dbo].[maplogitemsstdfimonthlydata] CHECK CONSTRAINT [fk_maplogitemsstdfimonthlydata_LogItemID]
GO


