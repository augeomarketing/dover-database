SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FIMonthlyData_Email](
	[MDEmailID] [int] IDENTITY(1,1) NOT NULL,
	[mdID] [char](10) NULL,
	[EmailMsg] [nvarchar](2000) NULL,
	[MailTo] [nvarchar](1000) NULL,
	[LastUserName] [varchar](50) NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FIMonthlyData_Email] ADD  CONSTRAINT [DF_FIMonthlyData_Email_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
