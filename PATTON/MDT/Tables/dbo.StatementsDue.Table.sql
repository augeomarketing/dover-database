USE [MDT]
GO
/****** Object:  Table [dbo].[StatementsDue]    Script Date: 05/23/2011 15:55:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatementsDue_Quantity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatementsDue] DROP CONSTRAINT [DF_StatementsDue_Quantity]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatementsDue]') AND type in (N'U'))
DROP TABLE [dbo].[StatementsDue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatementsDue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StatementsDue](
	[Processor] [nvarchar](255) NULL,
	[Tipfirst] [nvarchar](10) NULL,
	[DataRange] [nvarchar](200) NULL,
	[MonthYear] [nvarchar](7) NULL,
	[Quantity] [nchar](15) NULL CONSTRAINT [DF_StatementsDue_Quantity]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
