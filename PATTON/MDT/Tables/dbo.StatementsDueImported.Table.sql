SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StatementsDueImported](
	[StatementYearMonth] [nvarchar](7) NOT NULL
) ON [PRIMARY]
GO
