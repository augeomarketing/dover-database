USE [MDT]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Docs_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Docs] DROP CONSTRAINT [DF_Docs_LastUpdated]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Docs_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Docs] DROP CONSTRAINT [DF_Docs_IsActive]
END

GO

USE [MDT]
GO

/****** Object:  Table [dbo].[Docs]    Script Date: 04/21/2010 13:54:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Docs]') AND type in (N'U'))
DROP TABLE [dbo].[Docs]
GO

USE [MDT]
GO

/****** Object:  Table [dbo].[Docs]    Script Date: 04/21/2010 13:54:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Docs](
	[DocID] [int] IDENTITY(1,1) NOT NULL,
	[DocTitle] [varchar](200) NOT NULL,
	[DocUNC] [varchar](200) NULL,
	[DocText] [varchar](8000) NOT NULL,
	[DocGroupCodeID] [int] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LastUserID] [int] NOT NULL,
	[IsActive] [int] NOT NULL,
 CONSTRAINT [PK_Docs] PRIMARY KEY CLUSTERED 
(
	[DocID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Docs] ADD  CONSTRAINT [DF_Docs_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO

ALTER TABLE [dbo].[Docs] ADD  CONSTRAINT [DF_Docs_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO


