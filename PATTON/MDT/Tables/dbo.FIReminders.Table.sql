SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FIReminders](
	[FIReminderID] [int] IDENTITY(1,1) NOT NULL,
	[DBNumber] [nvarchar](3) NULL,
	[ReminderStartDate] [datetime] NULL,
	[IsReminderOn] [bit] NULL,
	[ReminderNote] [nvarchar](1000) NULL,
	[IsEmailReminder] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FIReminders] ADD  CONSTRAINT [DF_FIReminders_IsEmailReminder]  DEFAULT (0) FOR [IsEmailReminder]
GO
