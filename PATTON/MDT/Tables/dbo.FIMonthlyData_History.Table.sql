SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FIMonthlyData_History](
	[mdID] [int] NOT NULL,
	[DBNumber] [nchar](3) NULL,
	[MonthYear] [nvarchar](7) NULL,
	[SLAForFilesReceived] [nchar](50) NULL,
	[ImpFilesDueDate] [nchar](10) NULL,
	[ActDateRec] [nchar](10) NULL,
	[Comments] [nvarchar](4000) NULL,
	[AuditFileSent] [nchar](10) NULL,
	[AuditFileApproved] [nchar](10) NULL,
	[PostedToWeb] [nchar](10) NULL,
	[FilesToRNProd] [nchar](10) NULL,
	[LiabSummSent] [nchar](10) NULL,
	[EmailStmtSent] [nchar](10) NULL,
	[QStmtToRNProd] [nchar](10) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserName] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
