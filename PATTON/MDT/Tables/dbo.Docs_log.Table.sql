USE [MDT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Docs_Log_Docs]') AND parent_object_id = OBJECT_ID(N'[dbo].[Docs_Log]'))
ALTER TABLE [dbo].[Docs_Log] DROP CONSTRAINT [FK_Docs_Log_Docs]
GO

USE [MDT]
GO

/****** Object:  Table [dbo].[Docs_Log]    Script Date: 04/21/2010 13:55:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Docs_Log]') AND type in (N'U'))
DROP TABLE [dbo].[Docs_Log]
GO

USE [MDT]
GO

/****** Object:  Table [dbo].[Docs_Log]    Script Date: 04/21/2010 13:55:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Docs_Log](
	[DocID] [int] NOT NULL,
	[DocTitle] [varchar](200) NOT NULL,
	[DocUNC] [varchar](200) NULL,
	[DocText] [varchar](8000) NOT NULL,
	[DocGroupCodeID] [int] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LastUserID] [int] NOT NULL,
	[IsActive] [int] NOT NULL,
 CONSTRAINT [PK_Docs_Log] PRIMARY KEY CLUSTERED 
(
	[DocID] ASC,
	[LastUpdated] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [MDT]
/****** Object:  Index [IX_Docs_Log_PK]    Script Date: 04/21/2010 13:55:17 ******/
CREATE NONCLUSTERED INDEX [IX_Docs_Log_PK] ON [dbo].[Docs_Log] 
(
	[DocID] ASC,
	[LastUpdated] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Docs_Log]  WITH CHECK ADD  CONSTRAINT [FK_Docs_Log_Docs] FOREIGN KEY([DocID])
REFERENCES [dbo].[Docs] ([DocID])
GO

ALTER TABLE [dbo].[Docs_Log] CHECK CONSTRAINT [FK_Docs_Log_Docs]
GO


