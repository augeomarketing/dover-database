SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FIMonthlyData](
	[mdID] [int] IDENTITY(1,1) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL,
	[DBNamePatton] [nvarchar](50) NULL,
	[MonthYear] [nvarchar](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsMonthlyApprovalReq] [bit] NOT NULL,
	[DBProcessorID] [int] NULL,
	[ProgramName] [varchar](50) NULL,
	[SLAForFilesReceived] [nchar](50) NULL,
	[ImpFilesDueDate] [nchar](10) NULL,
	[ActDateRec] [datetime] NULL,
	[ActDateRec_color] [nvarchar](50) NULL,
	[Comments] [nvarchar](4000) NULL,
	[AuditFileSent] [datetime] NULL,
	[AuditFileSent_color] [nvarchar](10) NULL,
	[AuditFileApproved] [datetime] NULL,
	[AuditFileApproved_color] [nvarchar](10) NULL,
	[PostedToWeb] [datetime] NULL,
	[PostedToWeb_color] [nvarchar](10) NULL,
	[FilesToRNProd] [datetime] NULL,
	[FilesToRNProd_color] [nvarchar](10) NULL,
	[LiabSummSent] [datetime] NULL,
	[LiabSummSent_color] [nvarchar](10) NULL,
	[EmailStmtSent] [datetime] NULL,
	[EmailStmtSent_color] [nvarchar](10) NULL,
	[QStmtToRNProd] [datetime] NULL,
	[QStmtToRNProd_color] [nvarchar](10) NULL,
	[ImpFilesDueDate_SLADate] [nchar](10) NULL,
	[ActDateRec_SLADate] [nchar](10) NULL,
	[AuditFileSent_SLADate] [nchar](10) NULL,
	[AuditFileApproved_SLADate] [nchar](10) NULL,
	[PostedToWeb_SLADate] [nchar](10) NULL,
	[FilesToRNProd_SLADate] [nchar](10) NULL,
	[LiabSummSent_SLADate] [nchar](10) NULL,
	[EmailStmtSent_SLADate] [nchar](10) NULL,
	[QStmtToRNProd_SLADate] [nchar](10) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserName] [varchar](50) NULL,
 CONSTRAINT [PK_FIMonthlyData] PRIMARY KEY CLUSTERED 
(
	[mdID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY],
 CONSTRAINT [IX_FIMonthlyData_DBNum_MthYr] UNIQUE NONCLUSTERED 
(
	[DBNumber] ASC,
	[MonthYear] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
