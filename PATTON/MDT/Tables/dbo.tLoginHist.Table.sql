SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tLoginHist](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](40) NULL,
	[IPAddr] [varchar](50) NULL,
	[Logdate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[tLoginHist] ADD  CONSTRAINT [DF_tLoginHist_Logdate]  DEFAULT (getdate()) FOR [Logdate]
GO
