SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FI](
	[DBNumber] [varchar](3) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[IsActive] [bit] NOT NULL,
	[IsMonthlyApprovalReq] [bit] NULL,
	[DBProcessorID] [int] NULL,
	[NumDay] [int] NULL,
	[SLA] [varchar](50) NULL,
	[SLAForFilesReceived] [varchar](50) NULL,
	[ActDateRec_SLA] [int] NULL,
	[AuditFileSent_SLA] [int] NULL,
	[AuditFileApproved_SLA] [int] NULL,
	[PostedToWeb_SLA] [int] NULL,
	[FilesToRNProd_SLA] [int] NULL,
	[LiabSummSent_SLA] [int] NULL,
	[EmailStmtSent_SLA] [int] NULL,
	[QStmtToRNProd_SLA] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserID] [int] NULL,
	[ProgramName] [varchar](50) NULL,
	[PMUserID] [int] NULL,
	[OpsUserID] [int] NULL,
 CONSTRAINT [PK_FI] PRIMARY KEY CLUSTERED 
(
	[DBNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FI] ADD  CONSTRAINT [DF_FI_IsActive]  DEFAULT (1) FOR [IsActive]
GO
ALTER TABLE [dbo].[FI] ADD  CONSTRAINT [DF_FI_MonthlyAuditApprovalReq]  DEFAULT (1) FOR [IsMonthlyApprovalReq]
GO
ALTER TABLE [dbo].[FI] ADD  CONSTRAINT [DF_FI_IsPartOfStagingDiagnostics]  DEFAULT ((1)) FOR [IsPartOfStagingDiagnostics]
GO
