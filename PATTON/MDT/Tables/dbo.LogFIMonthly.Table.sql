SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogFIMonthly](
	[LogItemID] [int] IDENTITY(1,1) NOT NULL,
	[mdID] [int] NULL,
	[MonthYear] [nvarchar](7) NULL,
	[DBNumber] [nvarchar](3) NULL,
	[LogItem] [nvarchar](100) NULL,
	[LogNote] [nvarchar](1000) NULL,
	[SeqNum] [int] NULL,
	[NoteDate] [datetime] NULL,
	[IsStdItem] [bit] NULL,
	[IsLiabLink] [bit] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date this log item was completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogFIMonthly', @level2type=N'COLUMN',@level2name=N'NoteDate'
GO
ALTER TABLE [dbo].[LogFIMonthly] ADD  CONSTRAINT [DF_LogFIMonthly_IsStdItem]  DEFAULT (0) FOR [IsStdItem]
GO
ALTER TABLE [dbo].[LogFIMonthly] ADD  CONSTRAINT [DF_LogFIMonthly_IsLiabLink]  DEFAULT (0) FOR [IsLiabLink]
GO
