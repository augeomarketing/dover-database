SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogItemsLinked](
	[LinkID] [int] IDENTITY(1,1) NOT NULL,
	[LogItem] [nvarchar](100) NOT NULL,
	[DBNumber] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LinkSQL] [nvarchar](500) NULL,
	[DTLinked] [datetime] NULL,
 CONSTRAINT [PK_LogItemsLinked] PRIMARY KEY CLUSTERED 
(
	[LinkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[LogItemsLinked] ADD  CONSTRAINT [DF_LogItemsLinked_IsActive]  DEFAULT (1) FOR [IsActive]
GO
