USE [MDT]
GO

/****** Object:  Table [dbo].[tluDocGroupCodes]    Script Date: 04/21/2010 13:53:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tluDocGroupCodes]') AND type in (N'U'))
DROP TABLE [dbo].[tluDocGroupCodes]
GO

USE [MDT]
GO

/****** Object:  Table [dbo].[tluDocGroupCodes]    Script Date: 04/21/2010 13:53:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tluDocGroupCodes](
	[DocGroupCodeID] [int] IDENTITY(1,1) NOT NULL,
	[DocGroupCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tluDocGroupCodes] PRIMARY KEY CLUSTERED 
(
	[DocGroupCodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


