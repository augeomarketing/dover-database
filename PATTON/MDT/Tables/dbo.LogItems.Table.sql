SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogItems](
	[LogItemID] [int] NOT NULL,
	[LogItem] [nvarchar](100) NOT NULL,
	[LogNote] [nvarchar](1000) NULL,
	[DBNumber] [varchar](3) NOT NULL,
	[SeqNum] [int] NULL,
	[IsStdItem] [bit] NULL,
	[IsLiabLink] [bit] NULL,
 CONSTRAINT [PK_LogItems] PRIMARY KEY CLUSTERED 
(
	[LogItem] ASC,
	[DBNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[LogItems] ADD  CONSTRAINT [DF_LogItems_IsStdItem]  DEFAULT (0) FOR [IsStdItem]
GO
ALTER TABLE [dbo].[LogItems] ADD  CONSTRAINT [DF_LogItems_IsLiabLink]  DEFAULT (0) FOR [IsLiabLink]
GO
