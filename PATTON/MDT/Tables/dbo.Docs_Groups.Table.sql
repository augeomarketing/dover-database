USE [MDT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocsGroups_Docs]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocsGroups]'))
ALTER TABLE [dbo].[DocsGroups] DROP CONSTRAINT [FK_DocsGroups_Docs]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DocsGroups_tluDocGroupCodes]') AND parent_object_id = OBJECT_ID(N'[dbo].[DocsGroups]'))
ALTER TABLE [dbo].[DocsGroups] DROP CONSTRAINT [FK_DocsGroups_tluDocGroupCodes]
GO

USE [MDT]
GO

/****** Object:  Table [dbo].[DocsGroups]    Script Date: 04/21/2010 13:55:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DocsGroups]') AND type in (N'U'))
DROP TABLE [dbo].[DocsGroups]
GO

USE [MDT]
GO

/****** Object:  Table [dbo].[DocsGroups]    Script Date: 04/21/2010 13:55:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DocsGroups](
	[DocsGroupsID] [int] IDENTITY(1,1) NOT NULL,
	[DocID] [int] NOT NULL,
	[DocGroupCodeID] [int] NOT NULL,
 CONSTRAINT [PK_DocsGroups] PRIMARY KEY CLUSTERED 
(
	[DocsGroupsID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DocsGroups]  WITH CHECK ADD  CONSTRAINT [FK_DocsGroups_Docs] FOREIGN KEY([DocID])
REFERENCES [dbo].[Docs] ([DocID])
GO

ALTER TABLE [dbo].[DocsGroups] CHECK CONSTRAINT [FK_DocsGroups_Docs]
GO

ALTER TABLE [dbo].[DocsGroups]  WITH CHECK ADD  CONSTRAINT [FK_DocsGroups_tluDocGroupCodes] FOREIGN KEY([DocGroupCodeID])
REFERENCES [dbo].[tluDocGroupCodes] ([DocGroupCodeID])
GO

ALTER TABLE [dbo].[DocsGroups] CHECK CONSTRAINT [FK_DocsGroups_tluDocGroupCodes]
GO


