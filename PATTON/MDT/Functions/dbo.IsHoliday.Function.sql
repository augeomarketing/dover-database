USE [MDT]
GO

/****** Object:  UserDefinedFunction [dbo].[IsHoliday]    Script Date: 01/11/2011 15:29:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsHoliday]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsHoliday]
GO

USE [MDT]
GO

/****** Object:  UserDefinedFunction [dbo].[IsHoliday]    Script Date: 01/11/2011 15:29:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[IsHoliday] (@DateToCheck datetime)
RETURNS int
AS
BEGIN
	DECLARE @out int	
	SET @out = (SELECT COUNT(*) FROM tHolidays WHERE Holiday = @DateToCheck)
	
	IF @out > 0
		set @out = 1

	RETURN @out
END

GO


