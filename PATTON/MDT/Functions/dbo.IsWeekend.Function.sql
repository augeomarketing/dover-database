USE [MDT]
GO

/****** Object:  UserDefinedFunction [dbo].[IsWeekend]    Script Date: 01/11/2011 15:29:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsWeekend]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[IsWeekend]
GO

USE [MDT]
GO

/****** Object:  UserDefinedFunction [dbo].[IsWeekend]    Script Date: 01/11/2011 15:29:23 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[IsWeekend](@inputDate datetime)
RETURNS int
AS
BEGIN
	declare @out INT
	SET @out = 0
	
	declare @dw INT
	set @dw = DATEPART(dw, @inputDate)
	
	IF @dw in (1,7)
		SET @out = 1

	RETURN @out
END

GO


