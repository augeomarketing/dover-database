USE MDT
GO

IF OBJECT_ID(N'ufn_GetProcessorEmailForTipFirst') IS NOT NULL
	DROP FUNCTION ufn_GetProcessorEmailForTipFirst
GO

CREATE FUNCTION ufn_GetProcessorEmailForTipFirst
(
	@tipfirst VARCHAR(3)
) RETURNS VARCHAR(255)
AS
BEGIN
	DECLARE @out VARCHAR(255)
	DECLARE @username VARCHAR(100)
	
	SELECT 
		@username = dim_userinfo_username
	FROM FI tip
	INNER JOIN userinfo usr
		ON tip.DBProcessorID = usr.sid_userinfo_id
	WHERE 
		tip.DBNumber = @tipfirst
		
		
	IF ISNULL(@username, '') = ''
		SET @username = 'itops'
	
	SET @out = @username  + '@rewardsnow.com'
	
	RETURN @out

END
GO
