USE [MDT]
GO

/****** Object:  UserDefinedFunction [dbo].[CalculateSLADate]    Script Date: 01/11/2011 15:29:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateSLADate]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[CalculateSLADate]
GO

USE [MDT]
GO

/****** Object:  UserDefinedFunction [dbo].[CalculateSLADate]    Script Date: 01/11/2011 15:29:10 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE FUNCTION [dbo].[CalculateSLADate] (@slaDays INT, @lastMilestoneDate DATETIME)
RETURNS DATETIME
AS
BEGIN
	declare @out DATETIME	
	SET @out = @lastMilestoneDate
	
	DECLARE @dayCount int
	declare @tempDate datetime
	
	set @dayCount = 1
	set @tempDate = @lastMilestoneDate
	
	while @slaDays > 0
	begin
		set @tempDate = DATEADD(d, @dayCount, @lastMileStoneDate)
		
		if dbo.IsHoliday(@tempdate) = 0 AND dbo.IsWeekend(@tempdate) = 0
			set @slaDays = @slaDays - 1
			
		set @dayCount = @dayCount + 1
	end
	
	set @out = @tempDate
	
	RETURN @out	
END

GO


