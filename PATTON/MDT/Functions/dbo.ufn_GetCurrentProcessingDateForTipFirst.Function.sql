USE [MDT]
GO

IF OBJECT_ID(N'ufn_GetCurrentProcessingDateForTipFirst') IS NOT NULL
	DROP FUNCTION ufn_GetCurrentProcessingDateForTipFirst
GO

CREATE FUNCTION ufn_GetCurrentProcessingDateForTipFirst (@dbnumber VARCHAR(3)) RETURNS DATE
AS
BEGIN
	DECLARE @OUTPUT DATE
	
	SELECT @OUTPUT = CONVERT(DATE, REPLACE(MonthYear, '-', '/01/'))
	FROM MDT.dbo.FIMonthlyData md
	INNER JOIN
	(
		SELECT DBNUMBER, MAX(mdID) as mdID
		FROM MDT.dbo.FIMonthlyData 
		GROUP BY DBNUMBER
	) mxid
	ON md.mdID = mxid.mdID
	WHERE md.dbnumber = @dbnumber
	
	RETURN @OUTPUT
END

--SELECT MDT.dbo.ufn_GetCurrentProcessingDateForTipFirst ('51J')

