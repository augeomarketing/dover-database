USE [MDT]
GO

/****** Object:  Trigger [trg_FIMonthlyData_RecalcSLASFromDateReceived]    Script Date: 01/11/2011 16:45:15 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_FIMonthlyData_RecalcSLASFromDateReceived]'))
DROP TRIGGER [dbo].[trg_FIMonthlyData_RecalcSLASFromDateReceived]
GO

USE [MDT]
GO

/****** Object:  Trigger [dbo].[trg_FIMonthlyData_RecalcSLASFromDateReceived]    Script Date: 01/11/2011 16:45:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trg_FIMonthlyData_RecalcSLASFromDateReceived]
	ON [dbo].[FIMonthlyData]
AFTER INSERT, UPDATE
AS
DECLARE @temp TABLE (
	myid int identity(1,1)
	, dbnumber NCHAR(10)
	, monthYear NVARCHAR(7)
	, actdaterec datetime
)
	DECLARE @myid int
	declare @dbnumber nchar(10)
	declare @monthYear nvarchar(7)
	declare @actdaterec datetime

	INSERT INTO @temp(dbnumber, monthYear, actdaterec)
	SELECT i.DBNumber, i.MonthYear, i.ActDateRec
	FROM inserted i
	INNER JOIN deleted d
	ON
		i.DBNumber = d.DBNumber
		and i.MonthYear = d.MonthYear
		and i.ActDateRec <> d.ActDateRec
		
	set @myid = (select top 1 myid FROM @temp)
	while @myid is not null
	begin
		select @dbnumber = dbnumber, @monthYear = monthYear, @actdaterec = actdaterec from @temp where myid = @myid
	
		EXEC usp_RecalcSLASFromDateReceived @dbnumber, @monthyear, @actdaterec
		
		delete from @temp where myid = @myid		
		set @myid = (select top 1 myid FROM @temp)
	end
		

GO


