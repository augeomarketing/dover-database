USE [MDT]
GO

/****** Object:  Trigger [trg_FIMonthlyData_RecalcSLASFromDateApproved]    Script Date: 01/11/2011 16:45:08 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_FIMonthlyData_RecalcSLASFromDateApproved]'))
DROP TRIGGER [dbo].[trg_FIMonthlyData_RecalcSLASFromDateApproved]
GO

USE [MDT]
GO

/****** Object:  Trigger [dbo].[trg_FIMonthlyData_RecalcSLASFromDateApproved]    Script Date: 01/11/2011 16:45:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trg_FIMonthlyData_RecalcSLASFromDateApproved]
	ON [dbo].[FIMonthlyData]
AFTER INSERT, UPDATE
AS
DECLARE @temp TABLE (
	myid int identity(1,1)
	, dbnumber NCHAR(10)
	, monthYear NVARCHAR(7)
	, AuditFileApproved datetime
)
	DECLARE @myid int
	declare @dbnumber nchar(10)
	declare @monthYear nvarchar(7)
	declare @AuditFileApproved datetime

	INSERT INTO @temp(dbnumber, monthYear, AuditFileApproved)
	SELECT i.DBNumber, i.MonthYear, i.AuditFileApproved
	FROM inserted i
	INNER JOIN deleted d
	ON
		i.DBNumber = d.DBNumber
		and i.MonthYear = d.MonthYear
		and i.AuditFileApproved <> d.AuditFileApproved
		
	set @myid = (select top 1 myid FROM @temp)
	while @myid is not null
	begin
		select @dbnumber = dbnumber, @monthYear = monthYear, @AuditFileApproved = AuditFileApproved from @temp where myid = @myid
	
		EXEC usp_RecalcSLASFromDateApproved @dbnumber, @monthyear, @auditfileapproved
		
		delete from @temp where myid = @myid		
		set @myid = (select top 1 myid FROM @temp)
	end

GO


