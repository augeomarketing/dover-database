USE [MDT];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[maplogitemsstdfimonthlydata]([sid_maplogitemsstdfimonthlydata_LogItemID], [dim_maplogitemsstdfimonthlydata_fimonthlydatacolumn])
SELECT 1, N'ActDateRec' UNION ALL
SELECT 2, N'AuditFileSent' UNION ALL
SELECT 3, N'AuditFileApproved' UNION ALL
SELECT 4, N'PostedToWeb' UNION ALL
SELECT 5, N'FilesToRNProd' UNION ALL
SELECT 6, N'LiabSummSent' UNION ALL
SELECT 7, N'EmailStmtSent' UNION ALL
SELECT 8, N'QStmtToRNProd'
COMMIT;
GO

