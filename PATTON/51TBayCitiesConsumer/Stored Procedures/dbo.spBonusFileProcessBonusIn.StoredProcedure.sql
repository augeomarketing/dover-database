USE [51TBayCitiesConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcessBonusIn]    Script Date: 09/24/2009 14:48:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spBonusFileProcessBonusIn]
	-- Add the parameters for the stored procedure here
	@datein char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName nchar(100)

	--set @Datein='07/31/2007'
	set @DBName='51TBayCitiesConsumer'

	/******************************************************************************/
	/*                                                                            */
	/*    THIS IS TO process the Bonus input file Step 2                          */
	/*    This actually adds records to the History table and updates the balance */
	/*                                                                            */
	/******************************************************************************/
	delete from metavantework.dbo.transumbonus

	insert into metavantework.dbo.transumbonus (tipnumber, acctno, amtdebit)
	select tipnumber, [Card Number], [10000 Pts]	
	from metavantework.dbo.[BonusIn]
	where left(tipnumber,3)='51T'

	update metavantework.dbo.transumbonus
	set ratio='1', trancode='BI', description='Bonus Points Increase', histdate=@datein, numdebit='1', overage='0'

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
				from metavantework.dbo.transumbonus '
	Exec sp_executesql @SQLInsert  
	 
	set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.customer
		set runavailable=runavailable + (select sum(amtdebit) from metavantework.dbo.transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from metavantework.dbo.transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber)
		where exists(select tipnumber from metavantework.dbo.transumbonus where tipnumber=' + QuoteName(@DBName) + N'.dbo.customer.tipnumber)'
	Exec sp_executesql @SQLupdate

END
GO
