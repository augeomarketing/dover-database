USE [51TBayCitiesConsumer]
GO
/****** Object:  Table [dbo].[bonusin]    Script Date: 09/24/2009 14:48:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bonusin](
	[Name] [nvarchar](255) NULL,
	[Card Number] [nvarchar](255) NULL,
	[DDA] [nvarchar](255) NULL,
	[ssn] [float] NULL,
	[address1] [nvarchar](255) NULL,
	[address2] [nvarchar](255) NULL,
	[address3] [nvarchar](255) NULL,
	[City State Zip] [nvarchar](255) NULL,
	[10000 Pts] [char](15) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
