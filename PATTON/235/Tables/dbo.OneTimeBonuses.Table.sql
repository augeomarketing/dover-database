USE [235]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 08/13/2010 14:54:54 ******/
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
