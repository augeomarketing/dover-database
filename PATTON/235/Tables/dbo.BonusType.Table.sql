USE [235]
GO
/****** Object:  Table [dbo].[BonusType]    Script Date: 08/13/2010 14:54:54 ******/
ALTER TABLE [dbo].[BonusType] DROP CONSTRAINT [DF_BonusType_dim_BonusType_DateAdded]
GO
DROP TABLE [dbo].[BonusType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BonusType](
	[sid_BonusType_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_BonusType_TranCode] [nvarchar](2) NOT NULL,
	[dim_BonusType_Description] [varchar](255) NOT NULL,
	[dim_BonusType_DateAdded] [datetime] NOT NULL,
	[dim_BonusType_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusType] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BonusType] ADD  CONSTRAINT [DF_BonusType_dim_BonusType_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusType_DateAdded]
GO
