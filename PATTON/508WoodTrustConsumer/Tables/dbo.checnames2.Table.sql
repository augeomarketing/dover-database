USE [508WoodTrustConsumer]
GO
/****** Object:  Table [dbo].[checnames2]    Script Date: 09/23/2009 17:13:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[checnames2](
	[misc1] [varchar](20) NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
