USE [506RabobankConsumer]
GO
/****** Object:  Table [dbo].[wrknomatch2]    Script Date: 09/23/2009 17:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrknomatch2](
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[wrknomatch2] ADD [ssn] [varchar](9) NULL
GO
SET ANSI_PADDING OFF
GO
