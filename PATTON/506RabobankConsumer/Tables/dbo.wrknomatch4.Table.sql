USE [506RabobankConsumer]
GO
/****** Object:  Table [dbo].[wrknomatch4]    Script Date: 09/23/2009 17:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrknomatch4](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[acctname3] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[address4] [varchar](40) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[wrknomatch4] ADD [ssn] [varchar](9) NULL
GO
SET ANSI_PADDING OFF
GO
