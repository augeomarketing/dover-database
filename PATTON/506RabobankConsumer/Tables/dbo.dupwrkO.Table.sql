USE [506RabobankConsumer]
GO
/****** Object:  Table [dbo].[dupwrkO]    Script Date: 09/23/2009 17:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dupwrkO](
	[ddanum] [nvarchar](11) NULL,
	[tipnumber] [nvarchar](15) NULL,
	[begbalpri] [int] NULL,
	[tipnumber2] [char](15) NULL,
	[begbalsec] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
