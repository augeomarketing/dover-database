USE [A02]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 09/20/2011 11:40:28 ******/
DROP TABLE [dbo].[PointsExpireFrequency]
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 09/20/2011 11:40:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd], [PointsExpireFrequencyNm]) VALUES (N'ME', N'Month End')
INSERT [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd], [PointsExpireFrequencyNm]) VALUES (N'MM', N'Mid Month')
INSERT [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd], [PointsExpireFrequencyNm]) VALUES (N'YE', N'Year End')
