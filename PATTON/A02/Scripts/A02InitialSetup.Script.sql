
USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO


--BEGIN TRANSACTION;
--INSERT INTO [dbo].[dbprocessinfo]
--([DBNumber], [DBNamePatton], [DBNameNEXL], [DBAvailable], [ClientCode], [ClientName], [ProgramName], [DBLocationPatton], [DBLocationNexl], [PointExpirationYears], [DateJoined], [MinRedeemNeeded], [MaxPointsPerYear], [LastTipNumberUsed], [PointsExpireFrequencyCd], [ClosedMonths], [WelcomeKitGroupName], [GenerateWelcomeKit], [sid_FiProdStatus_statuscode], [IsStageModel], [ExtractGiftCards], [ExtractCashBack], [RNProgramName], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [Logo], [Landing], [TermsPage], [FaqPage], [EarnPage], [Business], [StatementDefault], [StatementNum], [CustomerServicePhone], [ContactPerson1], [ContactPerson2], [ContactPhone1], [ContactPhone2], [ContactEmail1], [ContactEmail2], [Address1], [Address2], [Address3], [Address4], [City], [State], [ZipCode], [ServerName], [TransferHistToRn1], [CalcDailyExpire], [hasonlinebooking])
--SELECT 
--N'239', N'239', N'MutualSavings', N'Y', N'MutualSavings', N'Mutual Savings Credit Union', N'RewardsNOW', N'239', N'MutualSavings', 3, '20101001 00:00:00.000', 750, 0, N'239000000000000', N'ME', NULL, NULL, N'Y', N'V', 1, N'N', N'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'Y', N'N', 0
--COMMIT;
--RAISERROR (N'[dbo].[dbprocessinfo]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
--GO

SELECT * FROM A02.dbo.Client
USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[RptCtlClients]([RnName], [ClientNum], [FormalName], [ClientDBName], [ClientDBLocation], [OnlClientDBName], [OnlClientDBLocation])
SELECT N'RISMedia', N'A02', N'Coldwell Banker High Country Realty', N'[A02]', N'[Doolittle\RN]', N'RISMedia', N'[RN1]'
COMMIT;
RAISERROR (N'[dbo].[RptCtlClients]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

USE [239];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

--BEGIN TRANSACTION;
--INSERT INTO [dbo].[PointsExpireFrequency]([PointsExpireFrequencyCd], [PointsExpireFrequencyNm])
--SELECT N'ME', N'Month End' UNION ALL
--SELECT N'MM', N'Mid Month' UNION ALL
--SELECT N'YE', N'Year End'
--COMMIT;
--RAISERROR (N'[dbo].[PointsExpireFrequency]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
--GO


USE [239];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

--BEGIN TRANSACTION;
--INSERT INTO [dbo].[Client]([ClientCode], [ClientName], [Description], [TipFirst], [Address1], [Address2], [Address3], [Address4], [City], [State], [Zipcode], [Phone1], [Phone2], [ContactPerson1], [ContactPerson2], [ContactEmail1], [ContactEmail2], [DateJoined], [RNProgramName], [TermsConditions], [PointsUpdatedDT], [MinRedeemNeeded], [TravelFlag], [MerchandiseFlag], [TravelIncMinPoints], [MerchandiseBonusMinPoints], [MaxPointsPerYear], [PointExpirationYears], [ClientID], [Pass], [ServerName], [DbName], [UserName], [Password], [PointsExpire], [LastTipNumberUsed], [PointsExpireFrequencyCd], [ClosedMonths], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [logo], [landing], [termspage], [faqpage], [earnpage], [Business], [StatementDefault], [StmtNum], [CustomerServicePhone])
--SELECT N'MutualSavings', N'Mutual Savings Credit Union', N'Mutual Savings Credit Union', N'239', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20101001 00:00:00.000', N'RewardsNOW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, N'239', N'Test', NULL, NULL, NULL, NULL, NULL, N'239000000000000', N'ME', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
--COMMIT;
--RAISERROR (N'[dbo].[Client]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
--GO


-- NOTE:  Need to add yourself into the deliverables.dbo.processor table
-- NOTE:  Need to add FI contact into deliverables.dbo.contact table so redemption reports would be emailed
--        If you want them to go to yourself, add yourself as a contact.
-- Both of these entries MUST be done before the insert below!!!

--select * from Deliverables.dbo.Processor (17-DEV 19-PROD)

USE [Deliverables];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO



select * from Deliverables.dbo.fiinfo where dim_fiinfo_tip = '239'
select * from contact where dim_contact_email = 'cheit@rewardsnow.com'

--363 DEV --363 PROD
--select * from Deliverables.dbo.Processor (17-DEV 19-PROD)

--SET IDENTITY_INSERT [dbo].[fiinfo] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[fiinfo]([dim_fiinfo_tip], [dim_fiinfo_dbname], [dim_fiinfo_shrtdbname], [dim_fiinfo_netpath], [sid_processor_id], [sid_contact_id], [dim_fiinfo_delivtype], [dim_fiinfo_created], [dim_fiinfo_active], [dim_fiinfo_lastmodified])
--DEV
--SELECT N'A02', N'A02', N'RISMedia', N'c:\reports\bin', 17, 363, N'Email', '20110801', 1, '20110831'
--PROD
SELECT N'A02', N'A02', N'RISMedia', N'c:\reports\bin', 19, 363, N'Email', '20110801', 1, '20110831'
COMMIT;
RAISERROR (N'[dbo].[fiinfo]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

--SET IDENTITY_INSERT [dbo].[fiinfo] OFF;

SELECT * FROM Deliverables.dbo.fiinfo WHERE dim_fiinfo_tip = 'A02'




USE [Deliverables];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
select * from liab

DECLARE @fiinfoid INT
SELECT @fiinfoid = sid_fiinfo_id FROM Deliverables.dbo.fiinfo where dim_fiinfo_tip = 'A02'

--SET IDENTITY_INSERT [dbo].[liab] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[liab]([sid_fiinfo_id], [dim_liab_format], [dim_liab_fileformat], [dim_liab_upload], [dim_liab_lastmodified], [dim_liab_created], [dim_liab_active])
SELECT @fiinfoid, N'A02 GnrlLiability yyyymm', N'PDF', N'Email', GETDATE(), GETDATE(), 1 UNION ALL
SELECT @fiinfoid, N'A02 Account Dist of Points yyyymm', N'PDF', N'Email', GETDATE(), GETDATE(), 1 UNION ALL
SELECT @fiinfoid, N'A02 ShoppingFLING_Detail_yyyymm', N'PDF', N'Email', GETDATE(), GETDATE(), 1
COMMIT;
RAISERROR (N'[dbo].[liab]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

--SET IDENTITY_INSERT [dbo].[liab] OFF;


--USE [Deliverables];
--SET NOCOUNT ON;
--SET XACT_ABORT ON;
--GO

--SET IDENTITY_INSERT [dbo].[Job] ON;
--BEGIN TRANSACTION;
--INSERT INTO [dbo].[Job]([sid_job_id], [sid_creport_id], [sid_fiinfo_id], [dim_Job_format], [dim_job_fileformat], [dim_job_upload], [dim_job_lastmodified], [dim_job_created], [dim_job_active])
--SELECT 1482, 1, N'884', N'239 Mutual Savings Travel Breakdown', N'PDf', N'Email', '20101007 11:07:56.623', '20101007 11:07:56.623', 1 UNION ALL
--SELECT 1483, 2, N'884', N'239 Mutual Savings Merchandise Breakdown', N'PDf', N'Email', '20101007 11:08:18.123', '20101007 11:08:18.123', 1 UNION ALL
--SELECT 1484, 3, N'884', N'239 Mutual Savings Gift Card Breakdown', N'PDF', N'Email', '20101007 11:08:40.677', '20101007 11:08:40.677', 1 UNION ALL
--SELECT 1485, 4, N'884', N'239 Mutual Savings Ringtone Breakdown', N'PDf', N'Email', '20101007 11:09:05.417', '20101007 11:09:05.417', 1 UNION ALL
--SELECT 1486, 5, N'884', N'239 Mutual Savings Charity Breakdown', N'PDF', N'Email', '20101007 11:09:33.413', '20101007 11:09:33.413', 1
--COMMIT;
--RAISERROR (N'[dbo].[Job]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
--GO

--SET IDENTITY_INSERT [dbo].[Job] OFF;


---
-- Last but not least - go into MDT, Admin and set up the FI
---
