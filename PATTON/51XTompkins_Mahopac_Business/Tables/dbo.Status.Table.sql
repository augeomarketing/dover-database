USE [51XTompkins_Mahopac_Business]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 09/24/2009 14:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
