USE [51XTompkins_Mahopac_Business]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 09/24/2009 14:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg1]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg2]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg3]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg4]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg5]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg6]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg7]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg8]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg9]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg10]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg11]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg12]
GO
