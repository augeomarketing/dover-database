USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [dbo].[SSIS Configurations]
where [ConfigurationFilter] like '267%'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'267_Ops_D01_AutoProcessDemographics', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-267_Ops_D01_AutoProcessDemographics-{BE23543E-3C87-401C-A421-548F356A0376}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'Data Source=patton\rn;Initial Catalog=267;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-267_Ops_D01_AutoProcessDemographics-{7BD72AE7-40EC-4D60-915B-B87399344118}patton\rn.267;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'ExcelFilePath=\\patton\ops\267\Output\Audit\Summary\_TmpSummaryM_SSIS.xls;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[Audit Summary Spreadsheet].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'ExcelFilePath=\\patton\ops\267\Output\Audit\_AUDITM_tmp_SSIS.xls;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[Audit Spreadsheet].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'267', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'', N'\Package.Variables[User::ProcessingDate].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'9/1/2013 12:00:00 AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'9/30/2013 12:00:00 AM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'\\patton\ops\267\input\', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'DELIMITED', N'\Package.Variables[User::LoadType].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'0', N'\Package.Variables[User::LoadStatus].Properties[Value]', N'Int32' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'\\214249-web3\users\267TriStateDental\TO_RN\', N'\Package.Variables[User::FTPFilePathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'\\214249-web3\users\267TriStateDental\TO_RN\', N'\Package.Variables[User::FTPFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'1', N'\Package.Variables[User::FileType].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'\\patton\ops\267\input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N',', N'\Package.Variables[User::Delimiter].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'\\patton\ops\267\input\', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_AutoProcessDemographics', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-267_Ops_D01_AutoProcessDemographics-{BE23543E-3C87-401C-A421-548F356A0376}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'Data Source=patton\rn;Initial Catalog=267;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-267_Ops_D01_AutoProcessDemographics-{7BD72AE7-40EC-4D60-915B-B87399344118}patton\rn.267;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'267', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'\\patton\ops\267\input\', N'\Package.Variables[User::SourceForArchive].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'9/1/2013 12:00:00 AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'9/30/2013 12:00:00 AM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'\\patton\ops\267\input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'', N'\Package.Variables[User::FileNameToDelete].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'', N'\Package.Variables[User::FileNameToArchive].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'\\patton\ops\267\input\', N'\Package.Variables[User::FileNameAndPathToDelete].Properties[Value]', N'String' UNION ALL
SELECT N'267_Ops_D01_PostStageToProduction', N'Archive_2013_09', N'\Package.Variables[User::ArchiveFolderName].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

