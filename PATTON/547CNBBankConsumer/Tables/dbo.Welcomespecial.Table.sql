USE [547CNBBankConsumer]
GO
/****** Object:  Table [dbo].[Welcomespecial]    Script Date: 09/25/2009 11:37:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Welcomespecial](
	[TIPNUMBER] [nvarchar](255) NULL,
	[ACCTNAME1] [nvarchar](255) NULL,
	[ACCTNAME2] [nvarchar](255) NULL,
	[ACCTNAME3] [nvarchar](255) NULL,
	[ACCTNAME4] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL
) ON [PRIMARY]
GO
