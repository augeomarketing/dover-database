USE [547CNBBankConsumer]
GO
/****** Object:  Table [dbo].[DeletedCards]    Script Date: 09/25/2009 11:37:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeletedCards](
	[CARD] [nvarchar](255) NULL,
	[TIP] [nvarchar](255) NULL
) ON [PRIMARY]
GO
