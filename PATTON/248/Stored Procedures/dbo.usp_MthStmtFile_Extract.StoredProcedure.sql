USE [248]
GO
/****** Object:  StoredProcedure [dbo].[usp_MthStmtFile_Extract]    Script Date: 07/14/2015 10:08:57 ******/
DROP PROCEDURE [dbo].[usp_MthStmtFile_Extract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Create statement file that must be exported to the FI
-- =============================================
CREATE PROCEDURE [dbo].[usp_MthStmtFile_Extract]
	@MonthEndingDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @EnddateOut varchar(8)
	
    set @EnddateOut = convert(varchar(8),@MonthEndingDate,112)

	truncate table wrkstatement

	insert into wrkstatement (REWARDSNUMBER,ACCOUNTNBR,NAME, MonthEndDate)
	select AFS.tipnumber, ltrim(rtrim(AFS.acctid)), CSS.ACCTNAME1, @EnddateOut
	from AFFILIAT_Stage AFS
	JOIN CUSTOMER_Stage CSS
	ON AFS.TIPNUMBER = CSS.TIPNUMBER
	where AFS.AcctTypeDesc='MEMBER'
		and AFS.AcctStatus='A'
	order by AFS.TIPNUMBER

	update wrkstatement
	set PREVIOUSPTS = abs(msf.PointsBegin)
			, DEBITCARD = (msf.PointsPurchasedDB - msf.PointsReturnedDB)
			, CREDITCARD = (msf.PointsPurchasedCR - msf.PointsReturnedCR)
			, BONUS		=	(msf.PointsBonus) 
			, [LOCAL/NATIONAL]	=	(msf.PointsBonusMn)
			, PTSADJ = (msf.PointsAdded + msf.PurchasePoints +  - msf.PointsSubtracted )
			, REDEEMED = (msf.PointsRedeemed)
			, TOTAL = (msf.PointsEnd)
	from wrkstatement wks join Monthly_Statement_File msf on wks.REWARDSNUMBER=msf.Tipnumber

DECLARE @sql table (tip varchar(15),i1 int,i2 int)

INSERT INTO @sql
	(tip, i1, i2)
EXEC	RewardsNow.dbo.usp_ExpirePoints_report_multipleinterval '248', 2, 1

UPDATE	qsf
SET		EXPIRING = s.i2
FROM	wrkstatement qsf join @sql s on qsf.REWARDSNUMBER = s.tip

END
GO
