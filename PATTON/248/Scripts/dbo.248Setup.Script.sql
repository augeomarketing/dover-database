USE [RewardsNow]
GO

exec usp_RNIRawImportDataDefinition_InsertStandardLayouts '248', '226'

insert into RewardsNow.dbo.RNIRawImportDataDefinitionWhereClause (sid_rnirawimportdatadefinitiontype_id, sid_rniimportfiletype_id, dim_rnirawimportdatadefinition_version, sid_dbprocessinfo_dbnumber, dim_rnirawimportdatadefinitionwhereclause_whereclause)
select 2, 1, 226, '248', 'dim_rnirawimport_field02 not like ''248_ACCT%'''

EXEC usp_GenerateRawDataViews '248'
/***********************************************/
/* Load RNICustomerLoadSource                  */
/***********************************************/
INSERT INTO [RewardsNow].[dbo].[RNICustomerLoadSource]
(
	[sid_dbprocessinfo_dbnumber]
	, [dim_rnicustomerloadsource_sourcename]
	, [dim_rnicustomerloadsource_fileload]
)
VALUES
(
	'248'
	,'vw_248_ACCT_TARGET_226'
	, 0
)

SELECT * FROM RewardsNow.dbo.RNICustomerLoadSource
/***********************************************/
/* Load RNICustomerLoadColumn                  */
/* the sid_rnicustomerloadsource_id used below */
/* is arrived at from the previous step        */
/***********************************************/

BEGIN

	DECLARE @loadsourceid BIGINT

	SELECT @loadsourceid = sid_rnicustomerloadsource_id
	FROM RNICustomerLoadSource
	WHERE sid_dbprocessinfo_dbnumber = '248'
		AND dim_rnicustomerloadsource_sourcename = 'vw_248_ACCT_TARGET_226'
		
	DELETE FROM RNICustomerLoadColumn where sid_rnicustomerloadsource_id = @loadsourceid

	INSERT INTO RNICustomerLoadColumn
	(
		sid_rnicustomerloadsource_id
		, dim_rnicustomerloadcolumn_sourcecolumn
		, dim_rnicustomerloadcolumn_targetcolumn
		, dim_rnicustomerloadcolumn_fidbcustcolumn
		, dim_rnicustomerloadcolumn_keyflag
		, dim_rnicustomerloadcolumn_loadtoaffiliat
		, dim_rnicustomerloadcolumn_affiliataccttype
		, dim_rnicustomerloadcolumn_primaryorder
	  , dim_rnicustomerloadcolumn_required
	)
	SELECT @loadsourceid, 'PortfolioNumber', 'dim_RNICustomer_Portfolio', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'MemberNumber', 'dim_RNICustomer_Member', NULL, 1, 1, 'MEMBER', 0, 1
	UNION SELECT @loadsourceid, 'PrimaryIndicatorId', 'dim_RNICustomer_PrimaryId', NULL, 0, 1, 'PRIMARYID', 0, 1
	UNION SELECT @loadsourceid, 'PrimaryIndicator','dim_RNICustomer_PrimaryIndicator', NULL, 1, 0, NULL, 1, 0
	UNION SELECT @loadsourceid, 'NAME1','dim_RNICustomer_Name1', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'NAME2','dim_RNICustomer_Name2', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'NAME3','dim_RNICustomer_Name3', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'NAME4','dim_RNICustomer_Name4', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'ADDRESS1','dim_RNICustomer_Address1', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'ADDRESS2','dim_RNICustomer_Address2', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'ADDRESS3','dim_RNICustomer_Address3', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'City','dim_RNICustomer_City', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'StateRegion','dim_RNICustomer_StateRegion', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'CountryCode','dim_RNICustomer_CountryCode', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'PostalCode','dim_RNICustomer_PostalCode', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'PrimaryPhone','dim_RNICustomer_PriPhone', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'PrimaryMobilePhone','dim_RNICustomer_PriMobilPhone', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'CustomerCode','dim_RNICustomer_CustomerCode', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'BusinessFlag','dim_RNICustomer_BusinessFlag', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'EmployeeFlag','dim_RNICustomer_EmployeeFlag', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'InstitutionID','dim_RNICustomer_InstitutionID', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'CardNumber','dim_RNICustomer_CardNumber', NULL, 1, 1, 'DEBIT', 0, 0
	UNION SELECT @loadsourceid, 'EmailAddress','dim_RNICustomer_EmailAddress', NULL, 0, 0, NULL, 0, 0
	UNION SELECT @loadsourceid, 'CustomerType','dim_RNICustomer_CustomerType', NULL, 0, 0, NULL, 0, 0

	SELECT * FROM RNICustomerLoadColumn WHERE sid_rnicustomerloadsource_id = @loadsourceid
END

/***********************************************/
/* Load RNIHouseholding                        */
/***********************************************/
INSERT INTO RNIHouseholding (sid_dbprocessinfo_dbnumber, dim_rnihouseholding_precedence, dim_rnihouseholding_field01, dim_rnihouseholding_field02)
SELECT '248', 1, 'dim_RNICustomer_Member', 'dim_RNICustomer_PrimaryId'


/***********************************************/
/* Load RNIProcessingParameters TABLE          */
/*   RNICUSTOMERDEFAULTSTATUS = Default status */
/*     0 if default status comes from data     */
/*   RNICUSTOMER_NAMEFORMATTYPE                */ 		
/*     value for ufn_FormatName                */
/***********************************************/
INSERT INTO Rewardsnow.dbo.RNIProcessingParameter 
(
	[sid_dbprocessinfo_dbnumber]
    ,[dim_rniprocessingparameter_key]
    ,[dim_rniprocessingparameter_value]
    ,[dim_rniprocessingparameter_active]
)
SELECT '248', 'RNICUSTOMERDEFAULTSTATUS','1',1
UNION SELECT '248', 'EMAIL_PROCESS_NOTIFY', 'ssmith@rewardsnow.com', 1
UNION SELECT '248', 'DEFAULT_TXNCURRENCY', 'USD', 1

SELECT * FROM RNIProcessingParameter WHERE sid_dbprocessinfo_dbnumber  = '248'

--PTW MAPPING
INSERT INTO mappingdefinition 
(
	sid_dbprocessinfo_dbnumber
	, sid_mappingtable_id
	, sid_mappingdestination_id
	, sid_mappingsource_id
	, dim_mappingdefinition_active
)
SELECT 
	'248'
	, (select sid_mappingtable_id from mappingtable where dim_mappingtable_tablename = 'account')
	, (select sid_mappingdestination_id from mappingdestination where dim_mappingdestination_columnname = 'LastSix')
	, (select sid_mappingsource_id from mappingsource where dim_mappingsource_description = 'Last Six of Card Affliat (DEBIT)')
	, 1
UNION SELECT 
	'248'
	, (select sid_mappingtable_id from mappingtable where dim_mappingtable_tablename = 'account')
	, (select sid_mappingdestination_id from mappingdestination where dim_mappingdestination_columnname = 'SSNLast4')
	, (select sid_mappingsource_id from mappingsource where dim_mappingsource_description = 'Last 4 of SSN Affliat (PRIMARYID)')
	, 1
UNION SELECT 
	'248'
	, (select sid_mappingtable_id from mappingtable where dim_mappingtable_tablename = 'account')
	, (select sid_mappingdestination_id from mappingdestination where dim_mappingdestination_columnname = 'MemberNumber')
	, (select sid_mappingsource_id from mappingsource where dim_mappingsource_description = 'ACCTID from Affiliat (ACCTTYPE=MEMBER)')
	, 1

