USE [Rewardsnow]
GO

/****** Object:  View [dbo].[vw_248_TRAN_TARGET_2_226]    Script Date: 09/10/2013 10:37:44 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_248_TRAN_TARGET_2_226]'))
DROP VIEW [dbo].[vw_248_TRAN_TARGET_2_226]
GO

USE [Rewardsnow]
GO

/****** Object:  View [dbo].[vw_248_TRAN_TARGET_2_226]    Script Date: 09/10/2013 10:37:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_248_TRAN_TARGET_2_226]
AS
	SELECT 
	v2.sid_rnirawimportstatus_id, 
	v2.dim_rnirawimport_processingenddate, 
	v2.sid_rnirawimport_id, 
	''								as PortfolioNumber, 
	''								as MemberNumber, 
	V200.SSNPrimary					AS PrimaryIndicatorID,
	''								as RNICustomerNumber, 
	'22262226'						as ProcessingCode, 
	v2.CardNumber					as Cardnumber, 
	v2.PurchaseDate					as TransactionDate,
	''								as TransferCardNumber, 
	Case v2.TransactionAmountLocalSign 
		When '-' THen 0
		Else	1
	End 							as TransactionCode, 
	''								as DDANumber, 
	v2.DestinationTransactionAmount * case when v2.TransactionAmountLocalSign = '-' THEN -1 ELSE 1 END as TransactionAmount,
	1								as TransactionCount, 
	v2.MerchantName					as TransactionDescription, 
	v2.MerchantCountryCode			as CurrencyCode,
	v2.CardAcceptorID				as MerchantID, 
	v2.MicrofilmReferenceNumber		as 	TransactionIdentifier, 
	v2.AuthorizationNumber			as AuthorizationCode, 	
	1								As TransactionActionCode 	
from vw_248_TRAN_TARGET_2 v2 
	join vw_248_ACCT_TARGET_200 v200 on v2.CardNumber = v200.AccountNumber 
where LTrim(RTrim(v2.CardCode )) = '1'  
		and v2.ATMIndicator in ( ' ','0','') 
		and len(rtrim(v200.SSNPrimary )) > 1  
		and PostFlag = '0'




GO


