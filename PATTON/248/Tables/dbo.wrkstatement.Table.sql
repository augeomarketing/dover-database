USE [248]
GO
/****** Object:  Table [dbo].[wrkstatement]    Script Date: 07/10/2015 16:25:59 ******/
DROP TABLE [dbo].[wrkstatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkstatement](
	[REWARDSNUMBER] [varchar](15) NOT NULL,
	[ACCOUNTNBR] [varchar](16) NOT NULL,
	[NAME] [varchar](40) NOT NULL,
	[PREVIOUSPTS] [varchar](11) NULL,
	[DEBITCARD] [varchar](11) NULL,
	[CREDITCARD] [varchar](11) NULL,
	[BONUS] [varchar](11) NULL,
	[LOCAL/NATIONAL] [varchar](11) NULL,
	[PTSADJ] [varchar](11) NULL,
	[REDEEMED] [varchar](11) NULL,
	[TOTAL] [varchar](11) NULL,
	[EXPIRING] [varchar](11) NULL,
	[MonthEndDate] [varchar](8) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
