USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[AcctTemp]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[AcctTemp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctTemp](
	[ACCTNUM] [varchar](18) NOT NULL
) ON [PRIMARY]
GO
