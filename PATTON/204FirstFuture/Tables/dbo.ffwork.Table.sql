USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[ffwork]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[ffwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ffwork](
	[tipnumber] [varchar](15) NOT NULL,
	[totpur] [float] NULL,
	[dateadded] [varchar](23) NOT NULL
) ON [PRIMARY]
GO
