USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[AcctType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[CardType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
