USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[DCTRAN]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[DCTRAN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DCTRAN](
	[CREDITAMT] [char](10) NULL,
	[FILLER1] [char](9) NULL,
	[CREDITCNT] [char](3) NULL,
	[FILLER2] [char](15) NULL,
	[DEBITAMT] [char](12) NULL,
	[FILLER3] [char](10) NULL,
	[DEBITCNT] [char](3) NULL,
	[FILLER4] [char](17) NULL,
	[ACCTNUM] [char](16) NULL,
	[MULTIPLIER] [char](8) NULL
) ON [PRIMARY]
GO
