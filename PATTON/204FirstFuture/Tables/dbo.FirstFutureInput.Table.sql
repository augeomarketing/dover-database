USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[FirstFutureInput]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[FirstFutureInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirstFutureInput](
	[TIPNUMBER] [varchar](8) NULL,
	[FIRSTNAME] [varchar](40) NULL,
	[LASTNAME] [varchar](40) NULL,
	[ADDRESS] [varchar](40) NULL,
	[CITY] [varchar](20) NULL,
	[STATE] [varchar](2) NULL,
	[ZIP] [varchar](5) NULL,
	[ZIPPLUS4] [varchar](4) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[STATUS] [varchar](2) NULL,
	[LASTFOURSOCIAL] [varchar](4) NULL,
	[BUSSFLAG] [varchar](1) NULL,
	[EMPFLAG] [varchar](1) NULL,
	[INSTID] [varchar](10) NULL,
	[CARDLASTSIX] [varchar](6) NULL,
	[PURAMT] [nvarchar](15) NULL,
	[PURCNT] [varchar](3) NULL,
	[RETAMT] [nvarchar](15) NULL,
	[RETCNT] [varchar](3) NULL,
	[BONUSAMT] [nvarchar](15) NULL
) ON [PRIMARY]
GO
