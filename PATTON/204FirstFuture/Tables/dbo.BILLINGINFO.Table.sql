USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[BILLINGINFO]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[BILLINGINFO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BILLINGINFO](
	[FieldDescription] [nvarchar](60) NULL,
	[FILLER] [nvarchar](1) NULL,
	[FieldValue] [numeric](18, 0) NULL,
	[ASOFDATE] [nvarchar](50) NULL
) ON [PRIMARY]
GO
