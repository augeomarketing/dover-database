USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[FirstFutureInputNEW]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[FirstFutureInputNEW]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirstFutureInputNEW](
	[TIPNUMBER] [varchar](8) NULL,
	[FIRSTNAME] [varchar](40) NULL,
	[LASTNAME] [varchar](40) NULL,
	[ADDRESS] [varchar](40) NULL,
	[CITY] [varchar](20) NULL,
	[STATE] [varchar](2) NULL,
	[ZIP] [varchar](5) NULL,
	[ZIPPLUS4] [varchar](4) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[STATUS] [varchar](2) NULL,
	[LASTFOURSOCIAL] [varchar](4) NULL,
	[BUSSFLAG] [varchar](1) NULL,
	[EMPFLAG] [varchar](1) NULL,
	[INSTID] [varchar](10) NULL,
	[CARDLASTSIXCredit] [varchar](6) NULL,
	[PURAMTCredit] [nvarchar](15) NULL,
	[PURCNTCredit] [varchar](3) NULL,
	[RETAMTCredit] [nvarchar](15) NULL,
	[RETCNTCredit] [varchar](3) NULL,
	[CARDLASTSIXDebit] [varchar](6) NULL,
	[PURAMTDebit] [nvarchar](15) NULL,
	[PURCNTDebit] [varchar](3) NULL,
	[RETAMTDebit] [nvarchar](15) NULL,
	[RETCNTDebit] [varchar](3) NULL,
	[BONUSAMT] [nvarchar](15) NULL
) ON [PRIMARY]
GO
