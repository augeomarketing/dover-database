USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[Input_Customer]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[Input_Customer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[AcctNum] [char](25) NULL,
	[Name1] [char](40) NULL,
	[Name2] [char](40) NULL,
	[Name3] [char](40) NULL,
	[Name4] [char](40) NULL,
	[Name5] [char](40) NULL,
	[Name6] [char](40) NULL,
	[Status] [char](1) NULL,
	[Tipnumber] [char](15) NULL,
	[Address1] [char](40) NULL,
	[Address2] [char](40) NULL,
	[Address3] [char](40) NULL,
	[City] [char](40) NULL,
	[state] [char](2) NULL,
	[Zip] [char](15) NULL,
	[Lastname] [char](40) NULL,
	[HomePhone] [char](12) NOT NULL,
	[WorkPhone] [char](10) NULL,
	[TranDate] [char](10) NULL,
	[BusFlag] [char](1) NULL,
	[BehSeg] [char](1) NULL,
	[email] [char](10) NULL,
	[curbal] [decimal](18, 0) NULL,
	[payment] [decimal](18, 0) NULL,
	[fee] [decimal](18, 0) NULL,
	[issueDate] [char](10) NULL,
	[Acivet_date] [char](10) NULL,
	[cardtype] [char](20) NULL
) ON [PRIMARY]
GO
