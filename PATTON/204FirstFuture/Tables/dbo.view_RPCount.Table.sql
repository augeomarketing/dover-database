USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[view_RPCount]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[view_RPCount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[view_RPCount](
	[tipnumber] [varchar](15) NOT NULL,
	[rpcount] [int] NULL
) ON [PRIMARY]
GO
