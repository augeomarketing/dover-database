USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[AffiliatDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NOT NULL,
	[LastName] [char](50) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
