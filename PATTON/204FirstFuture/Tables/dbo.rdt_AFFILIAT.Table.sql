USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[rdt_AFFILIAT]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[rdt_AFFILIAT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rdt_AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [char](50) NULL,
	[CardType] [varchar](20) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[PrimaryFlag] [varchar](10) NULL,
	[SecId] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[YTDEarned] [float] NOT NULL,
	[SSNum] [char](13) NULL
) ON [PRIMARY]
GO
