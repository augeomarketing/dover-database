USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[AccountDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
