USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[FirstFutureMonthlyTotals]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[FirstFutureMonthlyTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirstFutureMonthlyTotals](
	[FIELDNAME] [char](35) NULL,
	[FIELDVALUE] [decimal](18, 0) NULL,
	[FILL] [nvarchar](3) NULL,
	[POSTDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
