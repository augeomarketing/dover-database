USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[MaxTipNumber]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[MaxTipNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MaxTipNumber](
	[MaxTipNumber] [nvarchar](15) NOT NULL,
	[RunDate] [datetime] NULL
) ON [PRIMARY]
GO
