USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[HistTemp]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[HistTemp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistTemp](
	[SlNo] [decimal](18, 0) NULL,
	[TipNumber] [varchar](15) NULL,
	[AcctId] [varchar](25) NULL,
	[Histdate] [datetime] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[TranCode] [varchar](2) NULL,
	[IncDec] [nvarchar](1) NULL,
	[TypeCode] [nvarchar](1) NULL,
	[ExpiryFlag] [varchar](2) NULL
) ON [PRIMARY]
GO
