USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[HISTORY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
