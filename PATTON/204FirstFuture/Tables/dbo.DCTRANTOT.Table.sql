USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[DCTRANTOT]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[DCTRANTOT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DCTRANTOT](
	[CREDITAMT] [decimal](12, 2) NULL,
	[CREDITCNT] [decimal](12, 2) NULL,
	[DEBITAMT] [decimal](12, 2) NULL,
	[DEBITCNT] [decimal](12, 2) NULL,
	[TOTRECS] [decimal](12, 0) NULL
) ON [PRIMARY]
GO
