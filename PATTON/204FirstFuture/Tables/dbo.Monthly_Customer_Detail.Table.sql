USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[Monthly_Customer_Detail]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[Monthly_Customer_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Customer_Detail](
	[tipnumber] [varchar](15) NOT NULL,
	[runavailable] [int] NULL,
	[acctname1] [varchar](40) NOT NULL,
	[address1] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL,
 CONSTRAINT [PK_Monthly_Customer_Detail] PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
