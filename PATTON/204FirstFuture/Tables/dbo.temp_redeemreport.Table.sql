USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[temp_redeemreport]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[temp_redeemreport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_redeemreport](
	[tipnumber] [char](16) NOT NULL,
	[name1] [char](50) NULL,
	[acctid] [char](16) NULL,
	[redeemdate] [datetime] NULL,
	[redeemcount] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
