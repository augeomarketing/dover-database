USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[cctran]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[cctran]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cctran](
	[BEHSEG] [char](3) NULL,
	[HOUSEHOLD] [char](15) NULL,
	[PURCHAMT] [char](12) NULL,
	[PURCHCNT] [char](5) NULL,
	[RETURNAMT] [char](12) NULL,
	[RETURNCNT] [char](5) NULL,
	[NETAMT] [char](12) NULL,
	[NETCNT] [char](5) NULL,
	[ACCTNUM] [char](16) NULL,
	[MULTIPLIER] [char](6) NULL
) ON [PRIMARY]
GO
