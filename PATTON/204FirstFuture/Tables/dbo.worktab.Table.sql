USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[worktab]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[worktab]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[worktab](
	[tipnumber] [varchar](15) NOT NULL,
	[tranpoints] [float] NULL
) ON [PRIMARY]
GO
