USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[Status]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
