USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[DCCUST]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[DCCUST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DCCUST](
	[ACCTNUM] [char](19) NULL,
	[DDA] [char](20) NULL,
	[NAME1] [char](60) NULL,
	[NAME2] [char](50) NULL,
	[ADDRESS1] [char](50) NULL,
	[ADDRESS2] [char](50) NULL,
	[CITYSTATE] [char](50) NULL,
	[ZIP] [char](20) NULL,
	[HOTFLAG] [char](1) NULL,
	[MISC1] [char](2) NULL,
	[MISC2] [char](17) NULL,
	[SOMECODE] [char](2) NULL
) ON [PRIMARY]
GO
