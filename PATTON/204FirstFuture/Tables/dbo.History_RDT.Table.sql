USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[History_RDT]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[History_RDT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History_RDT](
	[TipNumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
