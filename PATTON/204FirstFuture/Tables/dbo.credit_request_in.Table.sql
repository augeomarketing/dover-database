USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[credit_request_in]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[credit_request_in]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[credit_request_in](
	[tipnumber] [char](15) NOT NULL,
	[name1] [varchar](50) NULL,
	[name2] [varchar](50) NULL,
	[CERTVALUE] [char](10) NULL,
	[LAST4] [char](4) NULL,
	[LAST6ONFIL] [char](6) NULL,
	[TRANSDATE] [char](20) NULL,
	[NOTES] [char](10) NULL,
	[ACCOUNT16] [char](16) NULL,
	[CARDTYPE] [char](15) NULL
) ON [PRIMARY]
GO
