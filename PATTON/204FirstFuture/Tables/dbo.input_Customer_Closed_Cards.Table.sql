USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[input_Customer_Closed_Cards]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[input_Customer_Closed_Cards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[input_Customer_Closed_Cards](
	[Input_Rewards_Number] [nvarchar](15) NULL,
	[Input_Name] [varchar](50) NULL,
	[Filler] [varchar](50) NULL
) ON [PRIMARY]
GO
