USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[OnlHistory]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[OnlHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OnlHistory](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NOT NULL,
	[TransID] [int] NULL,
	[TranCode] [char](2) NULL,
	[CatalogCode] [varchar](15) NULL,
	[CatalogDesc] [varchar](150) NULL,
	[CatalogQty] [int] NULL
) ON [PRIMARY]
GO
