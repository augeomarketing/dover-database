USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[ffnamefile]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[ffnamefile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ffnamefile](
	[tipnumber] [varchar](8) NULL,
	[firstname] [varchar](40) NULL,
	[cardlastsix] [varchar](6) NULL
) ON [PRIMARY]
GO
