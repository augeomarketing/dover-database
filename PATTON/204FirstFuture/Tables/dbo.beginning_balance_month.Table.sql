USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[beginning_balance_month]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
