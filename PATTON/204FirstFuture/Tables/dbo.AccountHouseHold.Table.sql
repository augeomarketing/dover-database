USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[AccountHouseHold]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[AccountHouseHold]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountHouseHold](
	[acctid] [char](16) NULL,
	[HouseHoldNumber] [char](25) NULL,
	[AccountType] [char](10) NULL
) ON [PRIMARY]
GO
