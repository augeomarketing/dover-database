USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[CreditRequest_Temp]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[CreditRequest_Temp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditRequest_Temp](
	[Tipnumber] [char](15) NULL,
	[name1] [char](50) NULL,
	[name2] [char](50) NULL,
	[amount] [float] NULL,
	[lastsix] [char](6) NULL,
	[fullaccount] [char](16) NULL
) ON [PRIMARY]
GO
