USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[To_Be_Deleted]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[To_Be_Deleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[To_Be_Deleted](
	[RewardsNumber] [varchar](8) NULL,
	[acctid] [varchar](25) NOT NULL,
	[lastname] [char](50) NULL
) ON [PRIMARY]
GO
