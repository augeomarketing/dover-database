USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[wrkfile]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[wrkfile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkfile](
	[tipnumber] [varchar](15) NOT NULL,
	[numaccts] [int] NULL
) ON [PRIMARY]
GO
