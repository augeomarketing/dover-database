USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[CC_Temp_Delete]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[CC_Temp_Delete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CC_Temp_Delete](
	[CC_Rewards_Num] [nchar](8) NULL,
	[CC_Cust_Name] [nchar](40) NULL
) ON [PRIMARY]
GO
