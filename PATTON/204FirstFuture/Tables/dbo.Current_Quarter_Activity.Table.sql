USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[Current_Quarter_Activity]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[Current_Quarter_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Quarter_Activity](
	[Tipnumber] [nchar](15) NOT NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
