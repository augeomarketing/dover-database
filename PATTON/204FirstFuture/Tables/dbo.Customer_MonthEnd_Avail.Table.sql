USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[Customer_MonthEnd_Avail]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[Customer_MonthEnd_Avail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_MonthEnd_Avail](
	[tipnumber] [char](15) NOT NULL,
	[monthend] [datetime] NULL,
	[pointsavailable] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
