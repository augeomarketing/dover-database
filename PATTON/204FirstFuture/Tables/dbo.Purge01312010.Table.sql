USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[Purge01312010]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[Purge01312010]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Purge01312010](
	[TIPNumber] [varchar](15) NOT NULL,
	[AcctName1] [varchar](40) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
