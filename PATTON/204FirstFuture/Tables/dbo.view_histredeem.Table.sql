USE [204FirstFutureCreditUnion]
GO
/****** Object:  Table [dbo].[view_histredeem]    Script Date: 06/30/2011 10:36:15 ******/
DROP TABLE [dbo].[view_histredeem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[view_histredeem](
	[tipnumber] [varchar](15) NOT NULL,
	[lastredeem] [datetime] NULL
) ON [PRIMARY]
GO
