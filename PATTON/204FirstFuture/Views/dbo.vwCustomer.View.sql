USE [204FirstFutureCreditUnion]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 06/30/2011 10:36:16 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [204FirstFutureCreditUnion].dbo.customer
GO
