USE [204FirstFutureCreditUnion]
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 06/30/2011 10:36:16 ******/
DROP VIEW [dbo].[vwCustomerdeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [204FirstFutureCreditUnion].dbo.Customerdeleted
GO
