USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spREMOVEDUPLICATESFROMPURGEFILES]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spREMOVEDUPLICATESFROMPURGEFILES]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This REMOVE THE DUPLICATES FROM THE ACCOUNTDELETINPUT TABLE FOR COMPASSS     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 11/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
create PROCEDURE [dbo].[spREMOVEDUPLICATESFROMPURGEFILES]  AS   
		
	Truncate Table ACCOUNTDELETEINPUT2
             
	 insert into ACCOUNTDELETEINPUT2	
	(		
          ACCTID 

	)
	select
          DISTINCT(ACCTID)   	      	
	from ACCOUNTDELETEINPUT
GO
