USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spCalculateMonthlyControlTotals]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spCalculateMonthlyControlTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the PREPOSTCONTROLTOTALS TABLE for 204FirstFutureCreditUnion            */
/*  - calculate prepost control totals for verification                                        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
  
CREATE PROCEDURE [dbo].[spCalculateMonthlyControlTotals]  @POSTDATE nvarchar(10)  AS  




/* Output */
/*Declare @POSTDATE NVARCHAR(10) */
Declare @MULTIPLIER float
Declare @TOTMONTHLYNET float
Declare @TOTMONTHLYCNT float
Declare @TOTPURCHAMT float
Declare @TOTPURCHCNT float
Declare @TOTRETAMT float
Declare @TOTRETCNT float
Declare @TOTNETAMT float
Declare @TOTNETCNT float
Declare @TOTCREDITAMT float
Declare @TOTCREDITCNT float
Declare @TOTDEBITAMT float
Declare @TOTDEBITCNT float
Declare @FIELDNAME NVARCHAR(50)
Declare @FIELDVALUE FLOAT(9)



BEGIN 
	/* SET @POSTDATE = '8/31/2006' */
	set @TOTMONTHLYNET = '0'
	set @TOTMONTHLYCNT = '0'
	set @TOTPURCHAMT = '0'
	set @TOTPURCHCNT = '0'
	set @TOTRETAMT = '0'
	set @TOTRETCNT = '0'
	set @TOTNETAMT = '0'
	set @TOTNETAMT = '0'
	set @TOTNETCNT = '0' 
	set @TOTCREDITAMT = '0'
	set @TOTCREDITCNT  = '0'
	set @TOTDEBITAMT =  '0'
	set @TOTDEBITCNT =  '0' 
	set @MULTIPLIER = '1'



	
	select	
	     @TOTMONTHLYCNT = (@TOTMONTHLYCNT + 1)
	    ,@TOTPURCHAMT = (@TOTPURCHAMT + PURAMT)
            ,@TOTPURCHCNT = (@TOTPURCHCNT + PURCNT) 
            ,@TOTRETAMT = (@TOTRETAMT + RETAMT)
            ,@TOTRETCNT = (@TOTRETCNT + RETCNT) 
	from FirstFutureInput 
	
	set  @TOTMONTHLYNET = (@TOTPURCHAMT - @TOTRETAMT)



 
	/*SET @FIELDNAME = 'POSTDATE'
	SET @POSTDATE = @POSTDATE

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE 
	  ,@POSTDATE 
	  ,' '
	)    */


	set @FIELDNAME = 'TOTAL MONTHLY INPUT COUNT'
	set @FIELDVALUE = @TOTMONTHLYCNT
	SET @POSTDATE = @POSTDATE

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' '
	)
	
	set @FIELDNAME = 'TOTAL MONTHLY NET AMOUNT'
	set @FIELDVALUE = @TOTMONTHLYNET
	SET @POSTDATE = @POSTDATE

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' '
	)


  
	set @FIELDNAME = 'TOTAL PURCHASE AMOUNT'
	set @FIELDVALUE = @TOTPURCHAMT 

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	) 

	set @FIELDNAME = 'TOTAL PURCHASE COUNT' 
	set @FIELDVALUE = @TOTPURCHCNT

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL 
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE 
	  ,@POSTDATE
	  ,' ' 
	) 
 
	set @FIELDNAME = 'TOTAL RETURN AMOUNT'  
	set @FIELDVALUE = @TOTRETAMT

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE 
	  ,@POSTDATE
	  ,' '
	)

	set @FIELDNAME = 'TOTAL RETURN COUNT' 
 	set @FIELDVALUE = @TOTRETCNT

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	



END /*while */


EndPROC:
GO
