USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spGENERATEBILLINGINFO]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spGENERATEBILLINGINFO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will The Monthly input from Compass for duplicate Records                    */
/* */
/*   - Read cccust  */
/* BY:  B.QUINN  */
/* DATE: 10/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spGENERATEBILLINGINFO] @POSTDATE char(10) AS   

/* Declare @POSTDATE char(10)
set @POSTDATE = '12/31/2006'  */
Declare @TOTBILLABLEACCOUNTS INT
Declare @TOTACCOUNTS INT
DECLARE @TOTCREDACCTS INT
DECLARE @TOTGROUPEDDEBITS INT
DECLARE @TOTDEBITS INT
DECLARE @TOTDEBITSNODDA INT
DECLARE @TOTDEBITSWITHDDA INT
DECLARE @TOTEMAILACCTS INT
DECLARE @FieldDescription NVARCHAR(60)
DECLARE @FieldvALUE NUMERIC(09)
DECLARE @FILLER NVARCHAR(1)
DECLARE @TOTRNNUMBER INT


	DELETE FROM BILLINGINFO

	SET @TOTBILLABLEACCOUNTS = '0'
	set @TOTACCOUNTS = '0'
	set @TOTCREDACCTS = '0'
	set @TOTGROUPEDDEBITS = '0'
	set @TOTDEBITS = '0'
	set @TOTDEBITSNODDA = '0'
	set @TOTDEBITSWITHDDA = '0'
	set @TOTEMAILACCTS = '0'
	SET @TOTRNNUMBER = '0'
	SET @FILLER = ' '
	
	SELECT
	  @TOTEMAILACCTS = (@TOTEMAILACCTS + '1')
	FROM  [PATTON\RN].[RN1BACKUP].[dbo].[1SECURITY] as S1
	inner join dbo.customer as cust
	on S1.TIPNUMBER = cust.TIPNUMBER 	     
	where S1.TIPNUMBER  in (select cust.TIPNUMBER from customer)
	and 	  EmailStatement = 'Y'

	select
	 @TOTDEBITS = (@TOTDEBITS + '1')
	from affiliat
	where AcctType like('DEB%')
	

	select
	 @TOTDEBITSNODDA = (@TOTDEBITSNODDA + '1')
	from affiliat
	where AcctType like('DEB%') 
	AND CustID IS NULL
	OR  CustID = ' '
	OR  CustID = '0'
	

	select
	 @TOTDEBITSWITHDDA = (@TOTDEBITSWITHDDA + '1')
	from affiliat
	where AcctType like('DEB%') 
	AND CustID <> NULL
	OR  CustID <> ' '
	OR  CustID <> '0'

	select
	 @TOTGROUPEDDEBITS = (@TOTGROUPEDDEBITS + '1')
	from affiliat
	where AcctType like('DEB%') 
	GROUP BY CustID


	select
	@TOTCREDACCTS = (@TOTCREDACCTS + '1')
	from affiliat
	where AcctType like('CRED%')

	
 	select
	@TOTACCOUNTS = COUNT(ACCTID)
	from affiliat

	SELECT
	@TOTRNNUMBER = COUNT(TIPNUMBER)
	FROM CUSTOMER


/*  PRODUCE REPORT ENTRIES  */


	/*  #1  */
	SET @FieldDescription = 'TOTAL AFFILIAT RECORDS'
	SET @FieldValue = @TOTACCOUNTS

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)



	/*   #2   */
	SET @FieldDescription = 'TOTAL DEBIT WITH DDA'
	SET @FieldValue = @TOTGROUPEDDEBITS

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)


	
	/*    #3  */
	SET @FieldDescription = 'TOTAL DEBITCARDS WITHOUT A DDA NUMBER'
	SET @FieldValue = @TOTDEBITSNODDA

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)



	/*   #4   */
	SET @FieldDescription = 'TOTAL CREDIT ACCOUNTS'
	SET @FieldValue = @TOTCREDACCTS

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)

	



	/*  #5  */
	SET @TOTBILLABLEACCOUNTS = (@TOTGROUPEDDEBITS + @TOTDEBITSNODDA + @TOTCREDACCTS)
	SET @FieldDescription = 'TOT BILLABLE '
	SET @FieldValue = @TOTBILLABLEACCOUNTS

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)


	/*   #6   */
	SET @FieldDescription = 'TOTAL RN# ON FILE'
	SET @FieldValue = @TOTRNNUMBER

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)

	




	/*    #7  */
	SET @FieldDescription = 'TOTEMAILACCTS'
	SET @FieldValue = @TOTEMAILACCTS

	INSERT INTO BILLINGINFO
	(
	  FieldDescription  
	 ,FieldValue
	 ,Filler  
	 ,ASOFDATE
	)
	VALUES
	(
	  @FieldDescription 
	 ,@FieldValue 
	 ,' '  
	 ,@POSTDATE 
	)
GO
