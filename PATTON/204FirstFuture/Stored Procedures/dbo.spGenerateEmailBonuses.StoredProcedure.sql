USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateEmailBonuses]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spGenerateEmailBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGenerateEmailBonuses] @monthendDate nvarchar(25) AS 

Declare @DateIn datetime 
set @DateIn = @monthendDate  
declare @Tipnumber char(15), @Acctid varchar(25), @Trandate datetime, @Points numeric(9)
/*set @Trandate=@Datein	*/
set @Trandate=@Datein
set @Points='750'
	
/*                                                                            */
/* Setup Cursor for processing                                                */

/*declare Customer_crsr cursor for 
select 	CUSTOMER.TIPNUMBER, CUSTOMER.AcctID
from      EMLSTMT  INNER JOIN
             CUSTOMER ON CUSTOMER.TIPNUMBER = EMLSTMT.TIPNUMBER 
where   CUSTOMER.TIPNUMBER  not in (select TIPNUMBER from HISTORY where TRANCODE = 'BE' )
*/

--declare Customer_crsr cursor for 
declare EMLSTMT_csr cursor for 
select 	TIPNUMBER
from      EMLSTMT  
where   TIPNUMBER  not in (select TIPNUMBER from HISTORY where TRANCODE = 'BE' )

/*                                                                            */
--open Customer_crsr
open EMLSTMT_csr 

/*                                                                            */
--fetch Customer_crsr into @Tipnumber, @Acctid 
fetch EMLSTMT_csr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */

	update customer	
		set RunAvailable = RunAvailable + @Points, RunBalance=RunBalance + @Points  
		where tipnumber = @Tipnumber

--	Insert Into History(TipNumber,AcctId,HistDate,TranCode,TranCount,Points,Ratio,Description)
--        	Values(@Tipnumber, @Acctid, @Trandate, 'BE', '1', @Points, '1', 'Bonus E Statements')

	Insert Into History(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,overage)
        	Values(@Tipnumber, @Trandate, 'BE', '1', @Points, '1', 'Bonus E Statements','0')
 
Next_Record:
--	fetch Customer_crsr into @Tipnumber
	fetch EMLSTMT_csr into @Tipnumber
end

Fetch_Error:
--close Customer_crsr
close EMLSTMT_csr 

--deallocate Customer_crsr
deallocate EMLSTMT_csr
GO
