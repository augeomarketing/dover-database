USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateCustomerList]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spGenerateCustomerList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenerateCustomerList]  as

Declare @DateIn datetime 
set @DateIn = GETDATE()  

	

if exists(select * from  .dbo.sysobjects where xtype='u' and name = 'Monthly_Customer_Detail')
Begin
truncate TABLE .dbo.Monthly_Customer_Detail 
End 

insert into Monthly_Customer_Detail 
(tipnumber,runavailable,acctname1,address1,city,state,zipcode)
select  tipnumber,runavailable,acctname1,address1,city,state,zipcode 
from customer
GO
