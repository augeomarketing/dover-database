USE [204FirstFutureCreditUnion]
GO

/****** Object:  StoredProcedure [dbo].[sp204MonthlyStatementFile]    Script Date: 01/02/2013 13:25:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp204MonthlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp204MonthlyStatementFile]
GO

USE [204FirstFutureCreditUnion]
GO

/****** Object:  StoredProcedure [dbo].[sp204MonthlyStatementFile]    Script Date: 01/02/2013 13:25:48 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/* 11/03/2008 All bonus's rolled up into BONUS points at the request of fullfillment BJQ */
/*******************************************************************************/
/*  01/08/2007 Clone created for First Future Credit Union Monthly Statements  */
/*  7/26/06 added View to improve speed                               */
/* RDT 01/04/07 Added Time to date for better datetime accuracy */
/*******************************************************************************/

-- RDT 01/04/07 CREATE PROCEDURE sp360MonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10)
CREATE PROCEDURE [dbo].[sp204MonthlyStatementFile] @StartDateParm   char(10), @EndDateParm   char(10) AS  

--declare @StartDateParm   char(23)
--declare @EndDateParm   char(23)
--set @StartDateParm = '2008-02-01'
--set @EndDateParm  = '2008-02-29' 
Declare @StartDate char(23)						--RDT 01/04/07
Declare @EndDate char(23)						--RDT 01/04/07
set @StartDate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 01/04/07 
set @EndDate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 01/04/07


Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)

--RDT 01/04/07 set @MonthBegin = month(Convert(datetime, @StartDate) )
set @MonthBegin = month( @StartDate) 		 --RDT 01/04/07


/* Load the statement file from the customer table  */
delete from Monthly_Statement_File


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = 'create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history 
where histdate between '''+@StartDate+''' and '''+ @EndDate+''' group by tipnumber, trancode'
/* where histdate between '''+convert( char(10), @StartDate,101 ) +''' and '''+ convert(char(10),@EndDate,101) +''' group by tipnumber, trancode'*/
PRINT @StartDateParm
PRINT @EndDateParm
print @StartDate
print @EndDate
exec sp_executesql @SQLDynamic

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer

-- TESTING TESTING TESTING
-- where tipnumber in ('360000000000093')
-- TESTING TESTING TESTING
update Monthly_Statement_File
set
PointsBegin = '0',Pointsend = '0',PointsPurchasedCR = '0',PointsPurchasedDB = '0',PointsBonus = '0',PointsAdded = '0',
PointsIncreased = '0',PointsRedeemed = '0',PointsReturnedCR = '0',PointsReturnedDB = '0',PointsSubtracted = '0',
PointsDecreased = '0',PointsBonus0A = '0',PointsBonus0B = '0',PointsBonus0C = '0',PointsBonus0D = '0',PointsBonus0E = '0',
PointsBonus0F = '0',PointsBonus0G = '0',PointsBonus0H = '0',PointsBonus0I = '0',PointsBonus0J = '0',PointsBonus0K = '0',
PointsBonus0L = '0',PointsBonus0M = '0',PointsBonus0N = '0',PointFloor = '0'

/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File 
set pointspurchasedCR 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '63' 

/* Load the statmement file CREDIT with returns            */
update Monthly_Statement_File 
set pointsreturnedCR
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode ='33'

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File 
set pointspurchasedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '67'

/* Load the statmement file DEBIT with returns            */
update Monthly_Statement_File 
set pointsreturnedDB
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '37'

/* Load the statmement file with bonuses            */
update Monthly_Statement_File 
set pointsbonus
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode like 'B%'

/* Load the statmement file with 0A bonuses            */
update Monthly_Statement_File 
set pointsbonus0A
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode  = '0A'

/* Load the statmement file with 0B bonuses            */
update Monthly_Statement_File 
set pointsbonus0B
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0B'


/* Load the statmement file with 0C bonuses            */
update Monthly_Statement_File 
set pointsbonus0C
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0C'


/* Load the statmement file with 0D bonuses            */
update Monthly_Statement_File 
set pointsbonus0D
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0D'


/* Load the statmement file with 0E bonuses            */
update Monthly_Statement_File 
set pointsbonus0E =
view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0E'


/* Load the statmement file with 0F bonuses            */
update Monthly_Statement_File 
set pointsbonus0F
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0F'


/* Load the statmement file with 0G bonuses            */
update Monthly_Statement_File 
set pointsbonus0G
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0G'


/* Load the statmement file with 0H bonuses            */
update Monthly_Statement_File 
set pointsbonus0H
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0H'


/* Load the statmement file with 0I bonuses            */
update Monthly_Statement_File 
set pointsbonus0I
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0I'

/* Load the statmement file with 0J bonuses            */
update Monthly_Statement_File 
set pointsbonus0J
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0J'

/* Load the statmement file with 0K bonuses            */
update Monthly_Statement_File 
set pointsbonus0K
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0K'

/* Load the statmement file with 0L bonuses            */
update Monthly_Statement_File 
set pointsbonus0L
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0L'

/* Load the statmement file with 0M bonuses            */
update Monthly_Statement_File 
set pointsbonus0M
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0M'

/* Load the statmement file with 0N bonuses            */
update Monthly_Statement_File 
set pointsbonus0N
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = '0N'

/* Load the statmement file with plus adjustments */
update Monthly_Statement_File 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
( view_history_TranCode.trancode ='IE'  )


/* Add  DECREASED REDEEMED to PointsAdded     
update Monthly_Statement_File 
set  pointsadded=pointsadded + 
 view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'
*/

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsbonus0A + pointsbonus0B + pointsbonus0C + pointsbonus0D
+ pointsbonus0E + pointsbonus0F + pointsbonus0G + pointsbonus0H + pointsbonus0I + pointsbonus0J + pointsbonus0K + pointsbonus0L
+ pointsbonus0M + pointsbonus0N


/* Load the statmement file with total bonus increases */
update Monthly_Statement_File
set pointsbonus=   pointsbonus + pointsbonus0A + pointsbonus0B + pointsbonus0C + pointsbonus0D
+ pointsbonus0E + pointsbonus0F + pointsbonus0G + pointsbonus0H + pointsbonus0I + pointsbonus0J + pointsbonus0K + pointsbonus0L
+ pointsbonus0M + pointsbonus0N

/* Load the statmement file with redemptions          */
 
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RB'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RC'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RD'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RE'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RG'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RI'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RM'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RP'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RQ'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RT'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RS'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RU'
  
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
--view_history_TranCode.trancode like('R%')
view_history_TranCode.trancode ='RV'
  


/* Load the statmement file with Increases to redemptions          */
update Monthly_Statement_File 
set pointsredeemed
=  pointsredeemed + view_history_TranCode.TranCodePoints  from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'IR' 


/* subtract DECREASED REDEEMED to from Redeemed*/
update Monthly_Statement_File 
set pointsredeemed
= pointsredeemed - view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DR'

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted
= view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'DE'

/* Add EP to  minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted 
=   view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'EP'

/* Add XP to  minus adjustments    */
update Monthly_Statement_File 
set pointssubtracted 
= pointssubtracted +  view_history_TranCode.TranCodePoints from Monthly_Statement_File, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File.tipnumber and 
view_history_TranCode.trancode = 'XP'

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin = (select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased


/* Drop the view */
drop view view_history_TranCode

GO


