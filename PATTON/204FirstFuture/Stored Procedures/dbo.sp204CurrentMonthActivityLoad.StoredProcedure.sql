USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[sp204CurrentMonthActivityLoad]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[sp204CurrentMonthActivityLoad]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--RDT 01/04/07 CREATE PROCEDURE sp360CurrentMonthActivityLoad @EndDate varchar(10)
CREATE PROCEDURE [dbo].[sp204CurrentMonthActivityLoad] @EndDateParm varchar(10)	--RDT 01/04/07
AS

delete from Current_Month_Activity

Declare @EndDate Datetime 						--RDT 01/04/07
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 01/04/07

insert into Current_Month_Activity (Tipnumber, EndingPoints)
select tipnumber, runavailable 
from Customer
-- TESTING TESTING TESTING
--where tipnumber in ('360000000012247','360000000012545','360000000085806','360000000131062','360000000143363','360000000144468','360000000151285','360000000156216','360000000157283','360000000163627','360000000165296',
--'360000000174734','360000000178319','360000000210109','360000000224483','360000000339991','360000000341244','360000000405084','360000000405250','360000000406021',
--'360000000406352','360000000406363','360000000406367','360000000406417','360000000406440','360000000406455')
-- TESTING TESTING TESTING
update Current_Month_Activity
set
Increases = '0'
,Decreases = '0'
,AdjustedEndingPoints = '0'

/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
