USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spLoadClosedCustomersForPurge]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spLoadClosedCustomersForPurge]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
-- This is a new module which will import Customer_Closed_Cards and then load
-- AccountDeleteInput with the accounts to be closed. This table will then be used
-- as an update to Input_Purge in spPurgeClosedCustomers prior to the purging of accounts
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spLoadClosedCustomersForPurge]   AS

Declare @SQLDynamic nvarchar(2000)
Declare @SQLIf nvarchar(2000)
Declare @Tipnumber 	char(15)


Truncate Table accountdeleteinput

insert  into accountdeleteinput
(acctid, 
dda)
select left(acctid,16),
left(custid,8) 
from affiliat 
where tipnumber in (select input_rewards_number from dbo.input_Customer_Closed_Cards)
GO
