USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spProcessFirstFutureMonthlyData_Stage]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spProcessFirstFutureMonthlyData_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For FirstFutureCreditUnion                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER_STAGE      */
/*  -  Update AFFILIAT_STAGE       */ 
/*  - Update HISTORY_STAGE          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessFirstFutureMonthlyData_Stage] @POSTDATE nvarchar(10) AS    

/* input */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '03/31/2007'    
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @FIRSTNAME char(40)
Declare @LASTNAME char(40)
Declare @FULLNAME nvarchar(40)
Declare @STREETADDR char(40)
Declare @City char(20)
Declare @State char(2)
Declare @Zip char(5)
Declare @ZipPLUS4 char(5)
Declare @WorkPhone char(12)
Declare @HomePhone char(12)
DECLARE @RATIO NUMERIC(1)
Declare @EMPFLAG char(1)
Declare @BUSFLAG char(1)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TIPLASTEIGHT char(8)
Declare @TIPFIRSTSEVEN char(7)  
Declare @TIPNUM char(15)
Declare @maxtipnumber char(15)
Declare @PurchAmt numeric(10)
Declare @PurchCnt numeric(10)
Declare @ReturnAmt numeric(10)
Declare @RetunCnt numeric(10)
Declare @Bonus numeric(10)
Declare @NetCnt numeric(10)
Declare @Multiplier numeric(1)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @CityState char(50)
Declare @HotFlag char(1)
Declare @CreditAmt numeric(10)
Declare @CreditCnt numeric(3)
Declare @DebitAmt numeric(10)
Declare @DebitCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailable int
Declare @RunBalance   int
Declare @RunRedeemed   int
Declare @POINTS numeric(5)
Declare @OVERAGE numeric(5)
Declare @YTDEarned numeric(10)
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @RunBalanceNew char(8)
declare @Address3 nvarchar(40)
declare @Address4 nvarchar(40)
Declare @CREDACCTSADDED numeric(09)
Declare @CREDACCTSUPDATED numeric(09)
Declare @CREDACCTSPROCESSED numeric(09)
Declare @DEBACCTSADDED numeric(09)
Declare @DEBACCTSUPDATED numeric(09)
declare @ReturnamtN numeric(10)
declare @PurchamtN numeric(10)
declare @Result numeric(10)
Declare @DEBACCTSPROCESSED numeric(09)
declare @SECID NVARCHAR(5)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare FF_CRSR cursor 
for Select *
From FirstFutureInput 

Open FF_CRSR
/*                  */



Fetch FF_CRSR  
into 
	@TIPLASTEIGHT, @FIRSTNAME, @LASTNAME, @STREETADDR, 
	@City, @State, @Zip, @ZipPLUS4, @HomePhone,
	@WorkPhone, @STATUS, @SSLAST4, @BUSFLAG, @EMPFLAG,
	@INSTID, @CARDLASTSIX, @PurchAmt, @PurchCnt, @ReturnAmt,
	@RetunCnt, @Bonus  

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'FIRSTFUTURE'
	SET @RunDate = @POSTDATE
	SET @DateAdded = @POSTDATE 	
	SET @RunBalanceNew = 0		
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0
	SET @YTDEarned = 0
	SET @afAcctType = 'Credit Card'
	SET @CREDACCTSADDED = 0
	SET @CREDACCTSUPDATED = 0
	SET @CREDACCTSPROCESSED = 0
	SET @TIPFIRSTSEVEN = '2040000'
	set @Multiplier = '1'

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
	set @afFound = ' '
	set @FULLNAME = (rtrim(@FIRSTNAME)+' '+rtrim(@LASTNAME))

	SET @TIPNUM = (@TIPFIRSTSEVEN + @TIPLASTEIGHT)
	SET @RUNBALANCE = '0'		
	SET @RunAvailable = '0'
	SET @YTDEarned = '0'
	set @OVERAGE = '0' 
	SET @afFound = ' '
	if @Multiplier is null
	or @Multiplier = ' '
	or @Multiplier = '0'
	   set @Multiplier = '1'
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @ReturnamtN = (@Returnamt / 100)
	set @PurchAmtN = (@PurchAmt / 100)


	/*  - Check For AFFILIAT_STAGE Record       */
	select
	   @TIPNUM = TIPNUMBER
	   ,@afFound = 'y'
	   ,@YTDEarned = YTDEarned
	from
	  AFFILIAT_STAGE
	where
	   ACCTID = @CARDLASTSIX 



/*   APPLY THE FIRST FUTURE BUSINESS LOGIC */

	if @afFound = 'y'
            Begin                                     
 		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER_STAGE
		Where
		   TIPNUMBER = @TIPNUM      
		

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client
		where clientcode = 'FirstFuture'


		IF @RUNBALANCE is NULL
	           SET @RUNBALANCE = 0
		IF @RunAvailable is NULL
	           SET @RunAvailable = 0
		IF @RunRedeemed is null
	           SET @RunRedeemed = 0


/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER_STAGE OVER LIMIT       */



	
		IF @ReturnamtN > '0'
		  Begin
		    set @RESULT = @ReturnAmtN * @Multiplier
	            set @RESULT = round(@RESULT, 0)
		    SET @YTDEarned = @YTDEarned - @RESULT
		    SET @RunAvailable = @RunAvailable - @RESULT
		    SET @RUNBALANCE = @RUNBALANCE - @RESULT
		  end
	
/*print ' '
print '@MAXPOINTSPERYEAR'
print @MAXPOINTSPERYEAR
print '@RESULT'
print @RESULT
print '@OVERAGE'
print @OVERAGE
print '@PurchAmtN'
print @PurchAmtN
print '@ReturnamtN'
print @ReturnamtN */

		set @RESULT = '0'
	        set @OVERAGE = '0'


		if @PurchAmtN > '0'
		begin
		  set @RESULT = (@PurchAmtN * @Multiplier)
	            set @RESULT = round(@RESULT, 0)

	          if  (@RESULT + @YTDEarned) > @MAXPOINTSPERYEAR
		    Begin
		      set @OVERAGE = (@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR 
		      set @YTDEarned = (@YTDEarned + @RESULT) - @OVERAGE
	            End		 
		  else
	             Begin
		      set @RunAvailable = @RunAvailable + @RESULT
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = @YTDEarned + @RESULT 		      		      
	             End  
		end



		
		SET @CREDACCTSUPDATED = (@CREDACCTSUPDATED + 1)

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @STATUS



/* Process Bonus Amounts                                                 */

		IF @Bonus > '0'
		  Begin
		    set @RESULT = (@Bonus * @Multiplier)
		    SET @RunAvailable = @RunAvailable + @RESULT
		    SET @RUNBALANCE = @RUNBALANCE + @RESULT
		  end
	

/*  UPDATE THE CUSTOMER_STAGE RECORD WITH THE CREDIT TRANSACTION DATA          */

		Update CUSTOMER_STAGE
		Set 
		   RunAvaliableNew = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @FULLNAME  
	           ,ACCTNAME2 = ' '  
	           ,ADDRESS1 = @STREETADDR
	           ,ADDRESS2 = ' ' 
	           ,ADDRESS3 = @address3
	           ,ADDRESS4 = @address4 
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE      
		Where @TipNum = CUSTOMER_STAGE.TIPNUMBER


/*  				- Create HISTORY_STAGE     			   */
/*  		ADD HISTORY_STAGE RECORD FOR PURCHASES     			   */

		set @POINTS = '0'

		
		IF @PurchAmtN > '0'
	        and  @OVERAGE > '0'	        
		Begin	
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '63'
	           set @POINTS = ((@PurchAmtN * @Multiplier)-  @OVERAGE)
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUM
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
	         END	        
	        ELSE
		IF @PurchAmtN > '0'
	        BEGIN
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '63'
	           set @POINTS = (@PurchAmtN * @MULTIPLIER) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUM
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End




/*  		ADD HISTORY_STAGE RECORD FOR RETURNS     			   */

		set @POINTS = '0'


		IF @ReturnAmtN > '0'
		Begin
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '33'
		   set @POINTS = (@ReturnAmtN * @MULTIPLIER) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUM
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'33'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		/*    ,@MULTIPLIER */
		    ,'-1'
	            ,'0'
	            )
		End

  		IF @Bonus > '0'
		Begin
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '0A'
		   set @POINTS = (@Bonus * @MULTIPLIER) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNum
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'0A'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End   


                if @YTDEarned is null
	           set @YTDEarned = 0

		Update AFFILIAT_STAGE
		Set 
		     YTDEarned = @YTDEarned
		    ,AcctStatus = @STATUS 		
		Where
		     TIPNUMBER = @TIPNUM
		and  right(ACCTID,6) = @CARDLASTSIX     
	
            
		set @RESULT = '0'
	        set @OVERAGE = '0'

	  end  
else

/* IF THERE IS NO AFFILIAT_STAGE RECORD THE COUSTOMER DID NOT EXIST AND IS ADDED TO THE PROPER FILES */

 
         
           BEGIN 
	        SET @RUNBALANCE = 0		
	        SET @RunAvailable = 0		
	        SET @RunRedeemed = 0
	        SET @YTDEarned = 0
	        SET @OVERAGE = 0
		
		Set @STATUS = 'A'	
	
		select 
		   @RunAvailable = RunAvailable
		  ,@RUNBALANCE  = RUNBALANCE
		From
		   CUSTOMER_STAGE
		Where
		   TIPNUMBER = @TIPNUM      

/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */
		select
		    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
		From
		    client
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER_STAGE OVER LIMIT       */


		IF @ReturnamtN > '0'
		  Begin
		    set @RESULT = (@ReturnAmtN * @Multiplier)
	            set @RESULT = round(@RESULT, 0)

		    SET @YTDEarned = @YTDEarned - @RESULT
		    SET @RunAvailable = @RunAvailable - @RESULT
		    SET @RUNBALANCE = @RUNBALANCE - @RESULT
		  end


		set @RESULT = '0'
	        set @OVERAGE = '0'

/*print ' '
print '@MAXPOINTSPERYEAR'
print @MAXPOINTSPERYEAR
print '@RESULT'
print @RESULT
print '@OVERAGE'
print @OVERAGE
print '@PurchAmtN'
print @PurchAmtN
print '@ReturnamtN'
print @ReturnamtN */

		if @PurchAmtN > '0'
		begin
		  set @RESULT = (@PurchAmtN * @Multiplier)
	            set @RESULT = round(@RESULT, 0)	
 
	          if  (@RESULT + @YTDEarned) > @MAXPOINTSPERYEAR
		    Begin
		      set @OVERAGE = (@RESULT + @YTDEarned) - @MAXPOINTSPERYEAR 
		      set @YTDEarned = (@YTDEarned + @RESULT) - @OVERAGE
	            End		 
		  else
	             Begin
		      set @RunAvailable = @RunAvailable + @RESULT
		      set @RUNBALANCE = @RUNBALANCE + @RESULT
		      set @YTDEarned = @YTDEarned + @RESULT 		      		      
	             End  
		end
		
	                /*        Add New CUSTOMER_STAGEs To The AFFILIAT_STAGE Table    */           

                  if @YTDEarned is null
	             set @YTDEarned = 0


		Insert into AFFILIAT_STAGE
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType
                )
	        values
 		(
		@CARDLASTSIX,
 		@TipNum,
		@LastName,
 		@DateAdded,
 		@STATUS,
		@YTDEarned,
 		@SSLAST4,
		@afAcctType
		)

/*   Get The STATUS DESCRIPTION                                           */

		Select
		   @STATUSDESCRIPTION = STATUSDESCRIPTION
		from
	           Status
		where
		   STATUS = @Status


/* */
/*  				- Create CUSTOMER_STAGE     				   */
/* DETERMINE IF THE NETAMT ON THE INPUT RECORD WILL PUT THE CUSTOMER_STAGE OVER LIMIT      */
/* This is an add to the CUSTOMER_STAGE file and an overage should not occur at this time  */
/* But I am Checking in the off chance it does 

                                      */
		SET @CREDACCTSADDED = (@CREDACCTSADDED + 1)
		set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
		

	        if @RUNBALANCE is null
	           set @RUNBALANCE = 0
	        if @RUNAVAILABLE is null
	           set @RUNAVAILABLE = 0  

  
		set @RESULT = '0'


/* Process Bonus Amounts                                                 */

		IF @Bonus > '0'
		  Begin
		    set @RESULT = (@Bonus * @Multiplier)
	            set @RESULT = round(@RESULT, 0)
		    SET @RunAvailable = @RunAvailable + @RESULT
		    SET @RUNBALANCE = @RUNBALANCE + @RESULT
		  end
	

	if exists (select dbo.CUSTOMER_STAGE.TIPNUMBER from dbo.CUSTOMER_STAGE 
                    where dbo.CUSTOMER_STAGE.TIPNUMBER = @TipNum)	
            Begin
		Update CUSTOMER_STAGE
		Set 
		   RunAvaliableNew = @RunAvailable
		   ,RUNBALANCE = @RUNBALANCE 		   
	           ,LASTNAME = @LASTNAME
	           ,ACCTNAME1 = @FULLNAME  
	           ,ACCTNAME2 = ' '  
	           ,ADDRESS1 = @STREETADDR 
	           ,ADDRESS2 = ' '
		   ,ADDRESS3 = @Address3
		   ,ADDRESS4 = @Address4
	           ,CITY = @CITY  
	           ,STATE = @STATE  
	           ,ZIPCODE = @ZIP  
	           ,HOMEPHONE = @HOMEPHONE      
		Where @TipNum = CUSTOMER_STAGE.TIPNUMBER
	    END		
	else
	    Begin
		Insert into CUSTOMER_STAGE  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 	
		,ADDRESS1 
		,ADDRESS2  	
	        ,ADDRESS3
	        ,ADDRESS4     
		,CITY 		
		,STATE		
		,ZIPCODE 	
		,HOMEPHONE 	
	 	,MISC1 		
		,MISC2		
		,MISC3 
		,MISC4
		,MISC5    		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
		,laststmtdate
		,nextstmtdate
		,acctname3
		,acctname4
		,acctname5
		,acctname6
		,businessflag
		,employeeflag
		,segmentcode
		,combostmt
		,rewardsonline
		,notes
		,runavaliablenew
	      )		
		values
		 (
		@Lastname, @FULLNAME, ' ',
 		@STREETADDR, ' ',@address3, @address4,	
 		@City, @State, @Zip, @HomePhone,
		' ', ' ', ' ', ' ', ' ',
             /* MISC1 thru MISC5 above are set to BLANK */
 		@DateAdded, '0',
 		@RunBalance, @RunRedeemed,
		@TIPNUM, @STATUS,
		left(@TipNum,3), right(@TipNum,12),
		@STATUSDESCRIPTION,
    /* laststmtdate thru runbalancenew are initialized here to eliminate nuls from the CUSTOMER_STAGE table*/
		' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',  @RunAvailable
	         )


            END 
	     	


	if not exists (select dbo.beginning_balance_table.TIPNUMBER from dbo.beginning_balance_table 
                    where dbo.beginning_balance_table.TIPNUMBER = @TipNum)
	   Begin	
		insert into beginning_balance_table
		( tipnumber,
		  Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
		  Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 
		)
		values
		(@tipnum,'0','0','0','0','0','0','0','0','0','0','0','0')
	   End



		set @POINTS = '0'

/*  		- Create HISTORY_STAGE   		  			   */
/*  		ADD HISTORY_STAGE RECORD FOR PURCHASES     			   */


		
		IF @PurchAmtN > '0'
	        and  @OVERAGE > '0'	        
		Begin	
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '63'

	           set @POINTS = ((@PurchAmtN * @Multiplier)-  @OVERAGE) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNum
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,@OVERAGE
	            )
	         END	        
	        ELSE
		IF @PurchAmtN > '0'
	        BEGIN
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '63'

	           set @POINTS = (@PurchAmtN * @MULTIPLIER) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNum
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'63'
	            ,@PurchCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End





/*  		ADD HISTORY_STAGE RECORD FOR RETURNS     			   */

		set @POINTS = '0'


  		IF @ReturnAmtN > '0'
		Begin
  		   Select 
		      @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '33'  
		   set @POINTS = (@ReturnAmtN * @MULTIPLIER) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNum
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'33'
	            ,@RetunCnt
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'-1'
	            ,'0'
	            )
		End  


 

  		IF @Bonus > '0'
		Begin
  		   Select 
		   @TRANDESC = Description
	           From TranType
		   where
		   TranCode = '0A'  
		   set @POINTS = (@Bonus * @MULTIPLIER) 
		   Insert into HISTORY_STAGE
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TipNum
	            ,@CARDLASTSIX
	            ,@RunDate
	            ,'0A'
	            ,'1'
	            ,@POINTS
		    ,@TRANDESC	            
	            ,@SECID
		    ,'1'
	            ,'0'
	            )
		End   

	end	 
	 


 

        

FETCH_NEXT:

	set @PurchAmt = '0'
	set @ReturnAmt = '0'
	set @Bonus = '0'
	set @PurchCnt = '0'
	set @RetunCnt = '0'
	set @TRANDESC = ' '
	
	Fetch FF_CRSR  
	into 
	@TIPLASTEIGHT, @FIRSTNAME, @LASTNAME, @STREETADDR, 
	@City, @State, @Zip, @ZipPLUS4, @HomePhone,
	@WorkPhone, @STATUS, @SSLAST4, @BUSFLAG, @EMPFLAG,
	@INSTID, @CARDLASTSIX, @PurchAmt, @PurchCnt, @ReturnAmt,
	@RetunCnt, @Bonus  

END /*while */


	 Insert into MONTHLYPROCESSINGCOUNTS
	  (
	   POSTDATE
	  ,CREDACCTSADDED
	  ,CREDACCTSUPDATED
	  ,CREDACCTSPROCESSED
	  ,DEBACCTSADDED
	  ,DEBACCTSUPDATED
	  ,DEBACCTSPROCESSED
	  )
	VALUES
	  (
	   @POSTDATE
	  ,@CREDACCTSADDED
	  ,@CREDACCTSUPDATED
	  ,@CREDACCTSPROCESSED
	  ,@DEBACCTSADDED
	  ,@DEBACCTSUPDATED
	  ,@DEBACCTSPROCESSED
	  ) 
	
	set @maxtipnumber = '0'

	select @maxtipnumber = max(tipnumber)
	from CUSTOMER_STAGE

	update client
	set lasttipnumberused = @maxtipnumber   

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  FF_CRSR
deallocate  FF_CRSR
GO
