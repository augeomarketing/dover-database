USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spManuallyDeleteCustomer]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spManuallyDeleteCustomer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spManuallyDeleteCustomer] @Tipnumber char(15), @DBName char(50)
as

Declare @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000), @SQLUpdate nvarchar(1000)

/* Set Customer Status*/
set @SQLUpdate=N'update '+ QuoteName(@DBNAME) + N' .dbo.Customer set status=''C'' where tipnumber=@Tipnumber'
	exec sp_executesql @SQLUpdate, N'@Tipnumber char(15)', @tipnumber=@tipnumber

/* Insert into CustomerDeleted */
set @SQLInsert=N'Insert into '+ QuoteName(@DBNAME) + N' .dbo.Customerdeleted SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, getdate() 
		FROM '+ QuoteName(@DBNAME) + N' .dbo.Customer where status=''C'' '
	exec sp_executesql @SQLInsert 

/* Insert into AffiliatDeleted */
set @SQLInsert=N'Insert into '+ QuoteName(@DBNAME) + N' .dbo.Affiliatdeleted (TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, Datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, getdate() 
	FROM '+ QuoteName(@DBNAME) + N' .dbo.affiliat where exists(select * from '+ QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber='+ QuoteName(@DBNAME) + N' .dbo.affiliat.tipnumber and status=''C'')'
	exec sp_executesql @SQLInsert

/* Insert into HistoryDeleted */
set @SQLInsert=N'Insert into '+ QuoteName(@DBNAME) + N' .dbo.Historydeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, getdate() 
	FROM '+ QuoteName(@DBNAME) + N' .dbo.History where exists(select * from '+ QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber='+ QuoteName(@DBNAME) + N' .dbo.History.tipnumber and status=''C'')'
	exec sp_executesql @SQLInsert

/* Insert into Account_Reference_Deleted */
set @SQLInsert=N'Insert into '+ QuoteName(@DBNAME) + N' .dbo.Account_Reference_deleted SELECT TIPNumber, Acctnumber, TIPFirst, getdate() 
		FROM '+ QuoteName(@DBNAME) + N' .dbo.Account_Reference where tipnumber=@Tipnumber '
	exec sp_executesql @SQLInsert, N'@Tipnumber char(15)', @Tipnumber=@Tipnumber 


/* Delete from Affiliat */
set @SQLDelete=N'delete from '+ QuoteName(@DBNAME) + N' .dbo.Affiliat 
		where exists(select * from '+ QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber='+ QuoteName(@DBNAME) + N' .dbo.affiliat.tipnumber and status=''C'')'
	exec sp_executesql @SQLDelete

/* Delete from History */
set @SQLDelete=N'delete from '+ QuoteName(@DBNAME) + N' .dbo.History 
		where exists(select * from '+ QuoteName(@DBNAME) + N' .dbo.Customer where tipnumber='+ QuoteName(@DBNAME) + N' .dbo.History.tipnumber and status=''C'')'
	exec sp_executesql @SQLDelete

/* Delete from Customer */
set @SQLDelete=N'delete from '+ QuoteName(@DBNAME) + N' .dbo.Customer 
		where status=''C'' '
	exec sp_executesql @SQLDelete

/* Delete from Account_Reference */
set @SQLDelete=N'delete from '+ QuoteName(@DBNAME) + N' .dbo.Account_Reference 
		where tipnumber=@Tipnumber '
	exec sp_executesql @SQLDelete, N'@Tipnumber char(15)', @Tipnumber=@Tipnumber
GO
