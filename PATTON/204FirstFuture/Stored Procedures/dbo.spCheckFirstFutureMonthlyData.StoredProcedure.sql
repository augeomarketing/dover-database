USE [204FirstFutureCreditUnion]
GO
/****** Object:  StoredProcedure [dbo].[spCheckFirstFutureMonthlyData]    Script Date: 06/30/2011 10:36:11 ******/
DROP PROCEDURE [dbo].[spCheckFirstFutureMonthlyData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly Credit Trans For 360CompassPoints                    */
/* */
/*   - Read cccust  */
/*  - Update CUSTOMER      */
/*  -  Update AFFILIAT       */ 
/*  - Update History          */
/* BY:  B.QUINN  */
/* DATE: 6/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCheckFirstFutureMonthlyData] @POSTDATE nvarchar(10)AS 
/* input */
/* DECLARE @POSTDATE nvarchar(10)
SET @POSTDATE = '12/31/2006'  */ 
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @FIRSTNAME char(40)
Declare @LASTNAME char(40)
Declare @FULLNAME nvarchar(40)
Declare @STREETADDR char(40)
Declare @City char(20)
Declare @State char(2)
Declare @Zip char(5)
Declare @ZipPLUS4 char(4)
Declare @WorkPhone char(10)
Declare @HomePhone char(10)
DECLARE @RATIO NUMERIC(1)
Declare @EMPFLAG char(1)
Declare @BUSFLAG char(1)
Declare @LetterType char(1)
Declare @Rating char(2)
Declare @TIPLASTEIGHT char(8)
Declare @TIPFIRSTSEVEN char(7)  
Declare @TIPNUM char(15)
Declare @PurchAmt float
Declare @PurchAmtN  float
Declare @PurchCnt float 
Declare @ReturnAmt float
Declare @ReturnAmtN  float
Declare @RetunCnt float 
Declare @TOTPurchAmt float
Declare @TOTPurchCnt float
Declare @TOTReturnAmt float
Declare @TOTRetunCnt float
Declare @TOTBonus float
Declare @TOTBonuscnt float
Declare @Bonus float
Declare @STATUS nvarchar(2)
Declare @ACCTSNEW numeric(09)
Declare @INPUTSTATUSA numeric(09)
Declare @INPUTSTATUSI numeric(09)
Declare @INPUTSTATUSD numeric(09)
Declare @INPUTRECCNT numeric(09)
Declare @ACCTSFOUND numeric(09)
Declare @afFound nvarchar(1)
Declare @SSLAST4 char(4)
Declare @FIELDNAME NVARCHAR(25)
Declare @FIELDVALUE NUMERIC(25)

Declare @CARDLASTSIX char(6)
Declare @acctid char(14)



	delete from AccountDeleteInput


	SET @TIPFIRSTSEVEN = '2040000'
	SET @PurchAmt = '0'
	SET @PurchAmtN = '0'
	SET @PurchCnt = '0'
	SET @ReturnAmt = '0'
	SET @ReturnAmtN = '0'
	SET @ReturnAmt = '0'
	set @TOTBonus = '0'
	SET @TOTBonuscnt = '0'

/*   - DECLARE CURSOR AND OPEN TABLES  */

Declare FF_CRSR cursor
for Select *
From FirstFutureInput

Open FF_CRSR
/*                  */



Fetch FF_CRSR  
into 
	@TIPLASTEIGHT, @FIRSTNAME, @LASTNAME, @STREETADDR, 
	@City, @State, @Zip, @ZipPLUS4, @HomePhone,
	@WorkPhone, @STATUS, @SSLAST4, @BUSFLAG, @EMPFLAG,
	@INSTID, @CARDLASTSIX, @PurchAmt, @PurchCnt, @ReturnAmt,
	@RetunCnt, @Bonus  

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	SET @TIPNUM = (@TIPFIRSTSEVEN + @TIPLASTEIGHT)

	DELETE FROM FirstFutureMonthlyTotals


	set @ACCTSFOUND ='0'
	set @ACCTSNEW ='0'
	SET @ACCTSFOUND = '0'
	SET @INPUTRECCNT = '0'
	SET @TOTPurchAmt = '0'
	SET @TOTPurchCnt = '0'
	SET @TOTReturnAmt = '0'
	SET @TOTRetunCnt = '0'
	SET @TOTRetunCnt = '0'
	SET @TOTRetunCnt = '0'
	SET @INPUTSTATUSA = '0'
	SET @INPUTSTATUSI = '0'
	SET @INPUTSTATUSD = '0'
	SET @INPUTRECCNT = '0'
	SET @ACCTSFOUND = '0'


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
	set @PurchAmtN = cast(@PurchAmt as numeric(15,2))
	set @ReturnAmtN =  cast(@ReturnAmt as numeric(15,2))
	set @PurchAmtN = @PurchAmtN /100
	set @ReturnAmtN = @ReturnAmtN /100
	SET @TIPNUM = (@TIPFIRSTSEVEN + @TIPLASTEIGHT)
	SET @acctid = (@TIPLASTEIGHT + @CARDLASTSIX)

if @PurchAmtN > '0' or
   @ReturnAmtN > '0'
 Begin
Print 'amounts'
print @PurchAmtN
print @ReturnAmtN
print @PurchAmt 
print @ReturnAmt 
end  

	IF @PurchAmtN IS NULL
	SET @PurchAmtN = '0'

	IF @PurchCnt IS NULL
	OR @PurchCnt = ' '
	SET @PurchCnt = '0'

	IF @RetunCnt IS NULL
	OR @RetunCnt = ' '
	SET @RetunCnt = '0'

	IF @ReturnAmtN IS NULL
	SET @ReturnAmtN = '0'

	SET @TIPNUM = (@TIPFIRSTSEVEN +  @TIPLASTEIGHT)
	SET @afFound = ' '
	

	/*  - Check For Affiliat Record       */
	select
	   @ACCTSFOUND = (@ACCTSFOUND + '1')
 	   ,@afFound    = 'y'
	from
	  AFFILIAT
	where
	   ACCTID = @acctid 

	SET @INPUTRECCNT = (@INPUTRECCNT + '1')

/*   APPLY THE FIRST FUTURE BUSINESS LOGIC */

	if @afFound = ' '
           set @ACCTSNEW = (@ACCTSNEW + '1')

	IF @STATUS = 'A'
           set @INPUTSTATUSA = (@INPUTSTATUSA + '1')	

	IF @STATUS = 'I'
           set @INPUTSTATUSI = (@INPUTSTATUSI + '1')

	if @Bonus > '0'
	begin
        set @TOTBonus = (@TOTBonus + @Bonus)
	set @TOTBonuscnt = (@TOTBonuscnt + 1)
	end



	IF @STATUS = 'D'
	   Begin
              set @INPUTSTATUSD = (@INPUTSTATUSD + '1')
	      insert into AccountDeleteInput
	      (acctid,dda)
	      values
	       (@acctid,'0')
	   end
	    
	 
	SET @TOTPurchAmt = (@TOTPurchAmt + @PurchAmtN)
	SET @TOTPurchCnt = (@TOTPurchCnt + @PurchCnt)
	SET @TOTRetunCnt = (@TOTRetunCnt + @RetunCnt)
	SET @TOTReturnAmt = (@TOTReturnAmt + @ReturnAmtN)


        

FETCH_NEXT:
	
	Fetch FF_CRSR  
	into 
	@TIPLASTEIGHT, @FIRSTNAME, @LASTNAME, @STREETADDR, 
	@City, @State, @Zip, @ZipPLUS4, @HomePhone,
	@WorkPhone, @STATUS, @SSLAST4, @BUSFLAG, @EMPFLAG,
	@INSTID, @CARDLASTSIX, @PurchAmt, @PurchCnt, @ReturnAmt,
	@RetunCnt, @Bonus  

END /*while */


	set @FIELDNAME = 'EXISTING ACCOUNTS ON FILE' 
 	set @FIELDVALUE = @ACCTSFOUND

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)


	SET @ACCTSNEW = ((@INPUTRECCNT - @ACCTSFOUND) - @INPUTSTATUSD)
	set @FIELDNAME = 'ACCOUNTS TO BE ADDED' 
 	set @FIELDVALUE = @ACCTSNEW

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)


	set @FIELDNAME = 'TOTAL PURCHASE AMOUNT' 
 	set @FIELDVALUE = @TOTPurchAmt

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	set @FIELDNAME = 'TOTAL PURCHASE COUNT' 
 	set @FIELDVALUE = @TOTPurchCnt

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	set @FIELDNAME = 'TOTAL RETURN AMOUNT' 
 	set @FIELDVALUE = @TOTReturnAmt

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	set @FIELDNAME = 'TOTAL RETURN COUNT' 
 	set @FIELDVALUE = @TOTRetunCnt

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	set @FIELDNAME = 'TOTAL NET AMOUNT' 
 	set @FIELDVALUE = (@TOTPurchAmt - @TOTReturnAmt)

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)


	set @FIELDNAME = 'INPUT STATUS (A)' 
 	set @FIELDVALUE = @INPUTSTATUSA

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	set @FIELDNAME = 'INPUT STATUS (I)' 
 	set @FIELDVALUE = @INPUTSTATUSI

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

	set @FIELDNAME = 'INPUT STATUS (D)' 
 	set @FIELDVALUE = @INPUTSTATUSD

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)


	set @FIELDNAME = 'TOTAL BONUS AMOUNT' 
 	set @FIELDVALUE = @TOTBonus

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)


	set @FIELDNAME =  'BONUS COUNT' 
 	set @FIELDVALUE = @TOTBonuscnt

	INSERT INTO FirstFutureMonthlyTotals
	(
	   FIELDNAME
	  ,FIELDVALUE
	  ,POSTDATE
	  ,FILL
	)
	VALUES
	(
	   @FIELDNAME
	  ,@FIELDVALUE
	  ,@POSTDATE 
	  ,' ' 
	)

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  FF_CRSR
deallocate  FF_CRSR
GO
