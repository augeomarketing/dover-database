USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[sp403GenTIPNumbersStage]    Script Date: 10/13/2009 10:39:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp403GenTIPNumbersStage]
AS 


update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.acctid = b.acctid 

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 403, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 403000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 403, @newnum

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
