USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 10/13/2009 10:39:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals

*/
/*  **************************************  */

CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst char(3), @enddate char(10)
AS 

Declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear from client ) 

/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
 insert into history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select Tip, Acctnum, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 From TransStandard

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update History_Stage
		set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM History_Stage H JOIN Affiliat_Stage A on H.Tipnumber = A.Tipnumber
		where A.YTDEarned + H.Points > @MaxPointsPerYear 
End

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
Update Affiliat_Stage
	set YTDEarned  = A.YTDEarned  + H.Points 
	FROM HISTORY_STAGE H JOIN AFFILIAT_Stage A on H.Tipnumber = A.Tipnumber

-- Update History_Stage Points = Points - Overage
Update History_Stage 
	Set Points = Points - Overage where Points > Overage

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update Customer_Stage Set RunAvaliableNew  = 0 where RunAvaliableNew  is null
Update Customer_Stage Set RunAvailable  = 0 where RunAvailable is null


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_histpoints]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_histpoints]

/* Create View */
set @SQLStmt = 'Create view vw_histpoints as select tipnumber, sum(points*ratio) as points from history_stage where secid = ''NEW''group by tipnumber'
exec sp_executesql @SQLStmt


Update customer_stage 
Set RunAvailable  = RunAvailable + v.Points 
From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber


Update customer_stage 
Set RunAvaliableNew  = v.Points 
From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber

drop view [dbo].[vw_histpoints]
GO
