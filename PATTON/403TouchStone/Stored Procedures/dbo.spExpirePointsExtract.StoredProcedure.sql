USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[spExpirePointsExtract]    Script Date: 10/13/2009 10:39:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/**********************************************************************************************/
/*    This will Create the Expired Points Table for expired points Processing          */
/*     This process will take the current monthenddate and subtract the  years      */
/*     years inpu.  All points older than this date are subject to expiration                */
/*                                                                                                                           */
/*     Note This version of the procedure creates the expiring points                     */
/*           table based on the current month end year minus input years                 */
/*           this table is then used BY spProcessExpiredPoints to update the            */
/*           Customer and History Tables                                                                   */
/* BY:  B.QUINN   Modified by DRF  Date 03/2008                                                                                                  */
/* DATE: 3/2007                                                                                                  */
/* REVISION: 1                                                                                                    */
/*                                                                                                                          */
/*********************************************************************************************/
CREATE PROCEDURE [dbo].[spExpirePointsExtract] @DateOfExpire VARCHAR(10)  AS   

--declare @DateOfExpire VARCHAR(10) 
SET @DateOfExpire = '12/31/2009'
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME

set @ExpireDate = convert(datetime, @DateOfExpire+' 23:59:59:990' ) 
--set @expirationdate = Dateadd(month, 1, @ExpireDate)
set @expirationdate = @ExpireDate
set @expirationdate = convert(nvarchar(25),(Dateadd(year, -3, @expirationdate)),121)
set @expirationdate = convert(nvarchar(25),(Dateadd(millisecond, -3, @expirationdate)),121)



--SET @intYear = DATEPART(year, @ExpireDate)
--SET @intmonth = DATEPART(month, @ExpireDate)
--SET @intday = DATEPART(day, @ExpireDate)
--set @intYear = @intYear - 3
--set @expirationdate = (rtrim(@intYear) + '-' + rtrim(@intmonth) + '-' + rtrim(@intday) +   ' 23:59:59.997')



	TRUNCATE TABLE ExpiringPoints

	INSERT   into ExpiringPoints
	 SELECT tipnumber, @ExpirationDate as dateofexpire, sum(points * ratio) as addpoints, 
	'0' AS REDPOINTS, '0' AS POINTSTOEXPIRE, '0' as PREVEXPIRED
	from history
	where histdate <= @expirationdate and ratio = '1'
	group by tipnumber


	UPDATE ExpiringPoints  
	SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 (trancode  ='XP') 
	  	 AND TIPNUMBER = ExpiringPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		  trancode  ='XP'
	  	 AND TIPNUMBER = ExpiringPoints.TIPNUMBER) 

	UPDATE ExpiringPoints  
	SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
		 FROM HISTORY 
		 WHERE
  		 ratio = '-1' and trancode <> 'XP'
	  	 AND TIPNUMBER = ExpiringPoints.TIPNUMBER) 
	WHERE EXISTS  (SELECT *
		 FROM HISTORY 
		 WHERE
  		 ratio = '-1' and trancode <> 'XP' 
	  	 AND TIPNUMBER = ExpiringPoints.TIPNUMBER)


	UPDATE ExpiringPoints  
	SET POINTSTOEXPIRE = (ADDPOINTS + REDPOINTS + PREVEXPIRED)

	UPDATE ExpiringPoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE IS NULL 

	UPDATE ExpiringPoints  
	SET POINTSTOEXPIRE = '0'
	WHERE
	POINTSTOEXPIRE < '0'
GO
