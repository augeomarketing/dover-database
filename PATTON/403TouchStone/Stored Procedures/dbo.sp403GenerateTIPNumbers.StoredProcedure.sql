USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[sp403GenerateTIPNumbers]    Script Date: 10/13/2009 10:39:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp403GenerateTIPNumbers]
AS 

update customerwork 
set Tipnumber = b.tipnumber
from customerwork a,affiliat b
where a.acctid = b.acctid 

update customerwork 
set Tipnumber = b.tipnumber
from customerwork a,affiliat b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from customerwork

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 403, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 403000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 403, @newnum

update customerwork 
set Tipnumber = b.tipnumber
from customerwork a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
