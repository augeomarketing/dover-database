USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[spLoadInputCustomer]    Script Date: 12/16/2009 11:13:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dan Foster>
-- Create date: <12/15/2009>
-- Description:	<Load Input_Customer Table>
-- =============================================
ALTER PROCEDURE [dbo].[spLoadInputCustomer]
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO Input_Customer(tipnumber)
		SELECT DISTINCT tipnumber FROM Roll_Customer
	
	UPDATE input_customer SET custid = b.custid,acctid = b.acctid, acctname1 = b.acctname1,
	acctname2 = b.acctname2,acctname3 = b.acctname3, acctname4 = b.acctname4,
	acctname5 = b.acctname5, acctname6 = b.acctname6,lastname = b.lastname, address1 = b.address1,
	address2 = b.address2,address4 = b.address4,city = b.city,[state] = b.[state],zip= b.zip, homephone = b.homephone,
	workphone = b.workphone,[status]= b.[status],statusdescription = b.statusdescription,
	last6 = b.last6, dateadded = b.dateadded
	FROM input_customer a, roll_customer b
	WHERE a.tipnumber = b.tipnumber AND a.acctname1 IS NULL
	
	update input_customer  set acctname2 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
	(a.acctname2 is null or a.acctname2 = ' ') 

	update input_customer  set acctname3 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and (a.acctname3 is null or a.acctname3 = ' ')

	update input_customer  set acctname4 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
	(a.acctname4 is null or a.acctname4 = ' ')

	update input_customer  set acctname5 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
	rtrim(b.acctname1) != rtrim(a.acctname4) and (a.acctname5 is null or a.acctname5 = ' ')

	update input_customer  set acctname6 =  b.acctname1 
	from input_customer a, roll_customer b 
	where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
	rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
	rtrim(b.acctname1) != rtrim(a.acctname4) and rtrim(b.acctname1) != rtrim(a.acctname5) and
	(a.acctname6 is null or a.acctname6 = ' ')
		
END

GO
