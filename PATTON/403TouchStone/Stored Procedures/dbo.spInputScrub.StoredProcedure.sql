USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 12/16/2009 15:27:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[spInputScrub] @processdate varchar(10)
AS

Truncate Table  input_customer_error
Truncate Table Input_Transactions_error

update roll_customer
set [acctname1]=replace([acctname1],char(39), ' '),[acctname2]=replace([acctname2],char(39), ' '),
	[acctname3]=replace([acctname3],char(39), ' '),[acctname4]=replace([acctname4],char(39), ' '),
	[acctname5]=replace([acctname5],char(39), ' '),[acctname6]=replace([acctname6],char(39), ' '),
	 address1=replace(address1,char(39), ' '),address2=replace(address2,char(39), ' '),
	 city=replace(city,char(39), ' ')
	 
update roll_customer
set [acctname1]=replace([acctname1],char(96), ' '),[acctname2]=replace([acctname2],char(96), ' '),
	[acctname3]=replace([acctname3],char(96), ' '),[acctname4]=replace([acctname4],char(96), ' '),
	[acctname5]=replace([acctname5],char(96), ' '),[acctname6]=replace([acctname6],char(96), ' '),
	 address1=replace(address1,char(96), ' '),address2=replace(address2,char(96), ' '),
	 city=replace(city,char(96), ' ')
  
update roll_customer
set [acctname1]=replace([acctname1],char(44), ' '),[acctname2]=replace([acctname2],char(44), ' '),
	[acctname3]=replace([acctname3],char(44), ' '),[acctname4]=replace([acctname4],char(44), ' '),
	[acctname5]=replace([acctname5],char(44), ' '),[acctname6]=replace([acctname6],char(44), ' '),
	 address1=replace(address1,char(44), ' '),address2=replace(address2,char(44), ' '),
	 city=replace(city,char(44), ' ')
  
update roll_customer
set [acctname1]=replace([acctname1],char(46), ' '),[acctname2]=replace([acctname2],char(46), ' '),
	[acctname3]=replace([acctname3],char(46), ' '),[acctname4]=replace([acctname4],char(46), ' '),
	[acctname5]=replace([acctname5],char(46), ' '),[acctname6]=replace([acctname6],char(46), ' '),
	 address1=replace(address1,char(46), ' '),address2=replace(address2,char(46), ' '),
	 city=replace(city,char(46), ' ')

update roll_customer
set [acctname1]=replace([acctname1],char(34), ' '),[acctname2]=replace([acctname2],char(34), ' '),
	[acctname3]=replace([acctname3],char(34), ' '),[acctname4]=replace([acctname4],char(34), ' '),
	[acctname5]=replace([acctname5],char(34), ' '),[acctname6]=replace([acctname6],char(34), ' '),
	 address1=replace(address1,char(34), ' '),address2=replace(address2,char(34), ' '),
	 city=replace(city,char(34), ' ')

update roll_customer
set [acctname1]=replace([acctname1],char(35), ' '),[acctname2]=replace([acctname2],char(35), ' '),
	[acctname3]=replace([acctname3],char(35), ' '),[acctname4]=replace([acctname4],char(35), ' '),
	[acctname5]=replace([acctname5],char(35), ' '),[acctname6]=replace([acctname6],char(35), ' '),
	 address1=replace(address1,char(35), ' '),address2=replace(address2,char(35), ' '),
	 city=replace(city,char(35), ' ')

update roll_customer set homephone =  Left(roll_customer.homephone,3) + substring(roll_customer.homephone,5,3) + right(roll_customer.homephone,4)

update roll_customer set workphone  = Left(roll_customer.workphone,3) + substring(roll_customer.workphone,5,3) + right(roll_customer.workphone,4)

update roll_customer set zip = rtrim(ltrim(zip))

update roll_customer set zip = left(roll_customer.zip,5) + '-' + right(roll_customer.zip,4) where len(roll_customer.zip) = 9

update roll_customer set address4 = 
left(ltrim(rtrim( roll_Customer.CITY))+' ' +ltrim(rtrim( roll_Customer.STATE))+' ' +ltrim( rtrim( roll_Customer.ZIP)) , 40 )

update roll_customer set status = 'A', statusdescription = 'Active'

UPDATE roll_Customer SET dateadded = (SELECT dateadded FROM CUSTOMER_stage                            
WHERE customer_stage.tipnumber = roll_customer.tipnumber)

UPDATE roll_CUSTOMER set dateadded = @processdate WHERE (dateadded IS NULL)

--------------- Input Customer table

/********************************************************************/
/* Remove roll_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from roll_customer 
	where (last6 is null or last6 = ' ') or  
	      (acctid is null or acctid = ' ') or
	      (acctname1 is null or acctname1 = ' ')

delete from roll_customer 
where (last6 is null or last6 = ' ') or  
      (acctid is null or acctid = ' ') or
      (acctname1 is null or acctname1 = ' ')

--------------- Input Transaction table

UPDATE input_transactions SET tipnumber = b.tipnumber
FROM input_transactions a, roll_customer b
WHERE a.cardnumber = b.acctid

Insert into Input_Transactions_error 
	select * from Input_Transactions 
	where (cardnumber is null or cardnumber = ' ') or
		  (tipnumber is null or tipnumber = ' ') or
	      (points is null )
	       
Delete from Input_Transactions
where (cardnumber is null or cardnumber = ' ') or
	  (tipnumber is null or tipnumber = ' ') or
      (points is null  or points =  0)

/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transactions 
set  points = round(points,0)

Update input_transactions
set trancode = '63' 
where trancode = 'P'

Update input_transactions
set trancode = '33' 
where trancode = 'R'