USE [403TouchStone]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionQC]    Script Date: 10/13/2009 10:39:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTransactionQC]   @ENDDATE varchar(10) 
as

declare @QCFlag varchar(1)
set @QCFlag = '0'

if @QCFlag = '0'
	Begin
		
		if (select count(*) from transactions where 	(tipnumber is null or tipnumber = ' ') or (transdate is null or transdate = ' ') or
			(cardnumber is null or cardnumber = ' ') or (trancode is null or trancode = ' ') or
			(points is null or points = ' ') or (ratio is null or ratio = ' ') or 
			(trancount is null or trancode = ' ')  ) > 0
		set @QCFlag = '1'
	end

if @QCFlag = '0'
	if (select max(histdate) from history) = @enddate
		Begin
			set @QCFlag = '2'
		end

Update qcreferencetable set qcflag = @qcflag
GO
