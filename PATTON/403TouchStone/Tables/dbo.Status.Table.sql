USE [403TouchStone]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 10/13/2009 10:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
