USE [403TouchStone]
GO
/****** Object:  Table [dbo].[BegBalanceCheck]    Script Date: 10/13/2009 10:40:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BegBalanceCheck](
	[OldBegBalance] [decimal](18, 0) NULL,
	[NewBegBalance] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
