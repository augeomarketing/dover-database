USE [403TouchStone]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 10/13/2009 10:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[custid] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
