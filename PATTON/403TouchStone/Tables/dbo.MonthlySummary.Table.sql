USE [403TouchStone]
GO
/****** Object:  Table [dbo].[MonthlySummary]    Script Date: 10/13/2009 10:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlySummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[Purchases] [decimal](18, 2) NULL,
	[Returned] [decimal](18, 2) NULL,
	[Bonuses] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_transactionwork_Purchases]  DEFAULT (0) FOR [Purchases]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_transactionwork_Returned]  DEFAULT (0) FOR [Returned]
GO
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_transactionwork_Bonuses]  DEFAULT (0) FOR [Bonuses]
GO
