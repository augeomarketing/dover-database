USE [222FirstFinancialAuto]
GO

/****** Object:  View [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]    Script Date: 05/12/2009 17:02:46 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]
GO

/****** Object:  View [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]    Script Date: 05/12/2009 17:02:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]

as


SELECT     TIPNUMBER, ACCTID, SUM(YTDEarned) AS YTDEarned
FROM         dbo.Affiliat_Stage
GROUP BY TIPNUMBER, ACCTID

GO


