USE [222FirstFinancialAuto]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 05/12/2009 17:02:50 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vw_histpoints]') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[vw_histpoints]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 05/12/2009 17:02:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

	Create view [dbo].[vw_histpoints] as 
		select tipnumber, sum(points * ratio) as points 
	from dbo.history_stage 
	where secid = 'NEW' group by tipnumber

GO


