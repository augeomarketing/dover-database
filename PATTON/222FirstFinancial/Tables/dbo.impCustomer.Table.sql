USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer]    Script Date: 05/12/2009 11:42:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer]
GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer]    Script Date: 05/12/2009 11:42:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer](
	[sid_impCustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[AssocNbr] [varchar](3) NULL,
	[CheckingNbr] [varchar](16) NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[TipPrefix] [varchar](3) NULL,
	[TipNumber] [varchar](15) NULL,
	[AddressLine6] [varchar](40) NULL,
	[RewardsNowSts] [varchar](1) NULL,
	[LastName] [varchar](40) NULL,
	[Misc3] [varchar](7) NULL,
 CONSTRAINT [PK__impCustomer] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_CCAcctNbr]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_CCAcctNbr] ON [dbo].[impCustomer] 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [ix_impcustomer_ccacctnbr_tipnumber]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [ix_impcustomer_ccacctnbr_tipnumber] ON [dbo].[impCustomer] 
(
	[CCAcctNbr] ASC,
	[TipNumber] ASC
) ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [ix_impcustomer_checkingnbr_assocNbr_primarynm_primaryssn]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [ix_impcustomer_checkingnbr_assocNbr_primarynm_primaryssn] ON [dbo].[impCustomer] 
(
	[CheckingNbr] ASC,
	[AssocNbr] ASC,
	[PrimaryNm] ASC,
	[PrimarySSN] ASC
) ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_CheckingNbr_TipNumber]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_CheckingNbr_TipNumber] ON [dbo].[impCustomer] 
(
	[CheckingNbr] ASC,
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [ix_impcustomer_checkingnbr_tipprefix_tipnumber_sid_impcustomer_id_ccacctnbr_oldccacctnbr]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [ix_impcustomer_checkingnbr_tipprefix_tipnumber_sid_impcustomer_id_ccacctnbr_oldccacctnbr] ON [dbo].[impCustomer] 
(
	[CheckingNbr] ASC,
	[TipPrefix] ASC,
	[TipNumber] ASC,
	[sid_impCustomer_id] ASC,
	[CCAcctNbr] ASC,
	[OldCCAcctNbr] ASC,
	[RewardsNowSts] ASC
) ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [ix_impcustomer_oldccacctnbr_tipnumber]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [ix_impcustomer_oldccacctnbr_tipnumber] ON [dbo].[impCustomer] 
(
	[OldCCAcctNbr] ASC,
	[TipNumber] ASC
) ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_RewardsNowSts_Sid_ImpCustomerId]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_RewardsNowSts_Sid_ImpCustomerId] ON [dbo].[impCustomer] 
(
	[RewardsNowSts] ASC,
	[sid_impCustomer_id] ASC
) ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_TipNumber]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_TipNumber] ON [dbo].[impCustomer] 
(
	[TipNumber] ASC,
	[RewardsNowSts] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_TipPrefix]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_TipPrefix] ON [dbo].[impCustomer] 
(
	[TipPrefix] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [ix_impcustomer_tipprefix_ccacctnbr_tipnumber]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [ix_impcustomer_tipprefix_ccacctnbr_tipnumber] ON [dbo].[impCustomer] 
(
	[TipPrefix] ASC,
	[CCAcctNbr] ASC,
	[TipNumber] ASC
) ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [ix_impcustomer_tipprefix_oldccacctnbr_tipnumber]    Script Date: 05/12/2009 11:42:25 ******/
CREATE NONCLUSTERED INDEX [ix_impcustomer_tipprefix_oldccacctnbr_tipnumber] ON [dbo].[impCustomer] 
(
	[TipPrefix] ASC,
	[OldCCAcctNbr] ASC,
	[TipNumber] ASC
) ON [PRIMARY]
GO


