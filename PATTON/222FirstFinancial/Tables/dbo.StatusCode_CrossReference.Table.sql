use [222FirstFinancialAuto]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_StatusCode_CrossReference_LastUpdateDt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StatusCode_CrossReference] DROP CONSTRAINT [DF_StatusCode_CrossReference_LastUpdateDt]
END

GO

use [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[StatusCode_CrossReference]    Script Date: 05/12/2009 11:31:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[StatusCode_CrossReference]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[StatusCode_CrossReference]
GO

use [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[StatusCode_CrossReference]    Script Date: 05/12/2009 11:31:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StatusCode_CrossReference](
	[sid_StatusCode_CrossReference_id] [bigint] IDENTITY(1,1) NOT NULL,
	[StatusCodeType] [varchar](1) NOT NULL,
	[TNBStatusCd] [varchar](1) NOT NULL,
	[RNStatusCd] [varchar](1) NOT NULL,
	[LastUpdateDt] [datetime] NOT NULL,
 CONSTRAINT [PK_StatusCode_CrossReference] PRIMARY KEY CLUSTERED 
(
	[sid_StatusCode_CrossReference_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


use [222FirstFinancialAuto]
/****** Object:  Index [IX_StatusCode_CrossReference_StatusCd_StatusCodeType_RNStatusCd]    Script Date: 05/12/2009 11:31:22 ******/
CREATE NONCLUSTERED INDEX [IX_StatusCode_CrossReference_StatusCd_StatusCodeType_RNStatusCd] ON [dbo].[StatusCode_CrossReference] 
(
	[TNBStatusCd] ASC,
	[StatusCodeType] ASC,
	[RNStatusCd] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[StatusCode_CrossReference] ADD  CONSTRAINT [DF_StatusCode_CrossReference_LastUpdateDt]  DEFAULT (getdate()) FOR [LastUpdateDt]
GO

