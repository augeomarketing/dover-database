USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer_Error]    Script Date: 05/12/2009 11:39:46 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer_Error]
GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer_Error]    Script Date: 05/12/2009 11:39:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer_Error](
	[sid_impCustomer_Agent_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[AssocNbr] [varchar](3) NULL,
	[CheckingNbr] [varchar](16) NULL,
	[TipPrefix] [varchar](3) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK__impCustomer_Erro__1B0907CE] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_Agent_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

