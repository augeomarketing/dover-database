USE [222FirstFinancialAuto]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_impCustomer_Purge_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[impCustomer_Purge] DROP CONSTRAINT [DF_impCustomer_Purge_DateAdded]
END

GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer_Purge]    Script Date: 05/12/2009 15:45:50 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer_Purge]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer_Purge]
GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer_Purge]    Script Date: 05/12/2009 15:45:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer_Purge](
	[TipNumber] [varchar](15) NOT NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[PrimaryNm] [varchar](35) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_impCustomer_Purge] PRIMARY KEY CLUSTERED 
(
	[CCAcctNbr] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_Purge_CCAcctNbr]    Script Date: 05/12/2009 15:45:50 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_Purge_CCAcctNbr] ON [dbo].[impCustomer_Purge] 
(
	[CCAcctNbr] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_Purge_TipNumber]    Script Date: 05/12/2009 15:45:50 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_Purge_TipNumber] ON [dbo].[impCustomer_Purge] 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[impCustomer_Purge] ADD  CONSTRAINT [DF_impCustomer_Purge_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


