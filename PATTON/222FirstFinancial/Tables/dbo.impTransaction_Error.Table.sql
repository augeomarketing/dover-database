USE [222FirstFinancialAuto]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_impTransaction_Error_TransactionAmt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[impTransaction_Error] DROP CONSTRAINT [DF_impTransaction_Error_TransactionAmt]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__impTransa__Point__1920BF5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[impTransaction_Error] DROP CONSTRAINT [DF__impTransa__Point__1920BF5C]
END

GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impTransaction_Error]    Script Date: 05/12/2009 11:41:38 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impTransaction_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impTransaction_Error]
GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impTransaction_Error]    Script Date: 05/12/2009 11:41:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impTransaction_Error](
	[sid_impTransaction_Agent_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NULL,
	[System] [varchar](4) NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [varchar](10) NULL,
	[AccountSts] [varchar](1) NULL,
	[TipNumber] [varchar](15) NULL,
	[Points] [bigint] NULL,
	[AcctNbr] [varchar](25) NULL,
 CONSTRAINT [PK__impTransaction_E__182C9B23] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_Agent_id] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[impTransaction_Error] ADD  CONSTRAINT [DF_impTransaction_Error_TransactionAmt]  DEFAULT (0.00) FOR [TransactionAmt]
GO

ALTER TABLE [dbo].[impTransaction_Error] ADD  CONSTRAINT [DF__impTransa__Point__1920BF5C]  DEFAULT (0) FOR [Points]
GO


