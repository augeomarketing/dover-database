USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[nameprs]    Script Date: 05/12/2009 17:46:52 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[nameprs]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[nameprs]
GO


/****** Object:  Table [dbo].[nameprs]    Script Date: 05/12/2009 17:46:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[nameprs](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[CSNA1] [varchar](40) NULL,
	[LASTNAME] [varchar](40) NULL,
	[ACCTNO] [varchar](16) NULL,
	[CARDNO] [varchar](16) NULL,
PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


