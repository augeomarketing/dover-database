USE [222FirstFinancialAuto]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_impCustomer_Purge_pending_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[impCustomer_Purge_pending] DROP CONSTRAINT [DF_impCustomer_Purge_pending_DateAdded]
END

GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer_Purge_pending]    Script Date: 05/12/2009 15:45:55 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomer_Purge_pending]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomer_Purge_pending]
GO

USE [222FirstFinancialAuto]
GO

/****** Object:  Table [dbo].[impCustomer_Purge_pending]    Script Date: 05/12/2009 15:45:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[impCustomer_Purge_pending](
	[TipNumber] [varchar](15) NOT NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[PrimaryNm] [varchar](35) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_impCustomer_Purge_pending] PRIMARY KEY CLUSTERED 
(
	[CCAcctNbr] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_Purge_pending_CCAcctNbr]    Script Date: 05/12/2009 15:45:55 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_Purge_pending_CCAcctNbr] ON [dbo].[impCustomer_Purge_pending] 
(
	[CCAcctNbr] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO


USE [222FirstFinancialAuto]
/****** Object:  Index [IX_impCustomer_Purge_pending_TipNumber]    Script Date: 05/12/2009 15:45:55 ******/
CREATE NONCLUSTERED INDEX [IX_impCustomer_Purge_pending_TipNumber] ON [dbo].[impCustomer_Purge_pending] 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[impCustomer_Purge_pending] ADD  CONSTRAINT [DF_impCustomer_Purge_pending_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


