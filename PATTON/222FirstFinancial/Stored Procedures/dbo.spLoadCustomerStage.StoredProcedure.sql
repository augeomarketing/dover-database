use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadCustomerStage' and xtype = 'P')
	drop procedure dbo.spLoadCustomerStage
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table		*/
/*    it only updates the customer demographic data						*/
/*																*/
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE						*/
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 1/2007													*/
/* REVISION: 1														*/
/* 6/22/07 South Florida now sending the FULL name in the Name field.			*/
/*																*/
/* 4/15/08 Paul H. Butler:  Copied & modified for use with LOC FCU			*/
/********************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] 

	@EndDate DateTime 
	
AS

/* Update Existing Customers                                         */

Update cstg
	set	lastname		= ltrim(rtrim(imp.LastName)),
		acctname1		= ltrim(rtrim(imp.PrimaryNm)),
		acctname2		= ltrim(rtrim(imp.SecondaryNm)),
		acctname3		= ltrim(rtrim(imp.AcctName3)),
		acctname4		= ltrim(rtrim(imp.AcctName4)),
		acctname5		= ltrim(rtrim(imp.AcctName5)),
		acctname6		= ltrim(rtrim(imp.AcctName6)),
		address1		= ltrim(rtrim(imp.AddressLine1)),	
		address2		= ltrim(rtrim(imp.AddressLine2)),
		address4		= left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.StateCd)) + ' ' + ltrim(rtrim(imp.Zip)), 40),
		city			= ltrim(rtrim(imp.City)),
		[state]			= ltrim(rtrim(imp.StateCd)),
		zipcode		= ltrim(rtrim(imp.Zip)),
		homephone		= ltrim(rtrim(imp.Phone1)),
		workphone		= ltrim(rtrim(imp.Phone2)),
		misc2		= right(ltrim(rtrim(imp.CCAcctNbr)), 6),
		misc1		= right(ltrim(rtrim(imp.PrimarySSN)), 4),
		[status]		= case
						when ltrim(rtrim(imp.RewardsNowSts)) = 'S' then 'S'
						else 'A'
					  end
from dbo.Customer_Stage cstg join dbo.impCustomer imp
on	cstg.tipnumber = imp.tipnumber
-- PHB 05/11/2009
--where isnull(imp.RewardsNowSts, '') not in ('Z', 'C') -- don't do closed accounts


declare @Tip		varchar(15)

declare csrInsert cursor FAST_FORWARD for	
	select distinct imp.tipnumber
	from dbo.impcustomer imp left outer join dbo.customer_stage stg
		on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null

open csrInsert

fetch next from csrInsert into @Tip
while @@FETCH_STATUS = 0
BEGIN

	insert into dbo.Customer_Stage
	(tipnumber, tipfirst, tiplast, lastname, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6,
	 address1, address2, address4, city, [state], zipcode, homephone, workphone, misc2, misc1, [status], dateadded, 
	 runavailable, runbalance, runredeemed, runavaliableNew)
	select top 1 tipnumber, tipprefix, right(tipnumber,12), lastname, primarynm, secondarynm, acctname3, acctname4,
			   acctname5, acctname6, addressline1, addressline2,
			   left(ltrim(rtrim(imp.City)) + ' ' + ltrim(rtrim(imp.StateCd)) + ' ' + ltrim(rtrim(imp.Zip)), 40) as AddressLine4,
			   city, statecd, zip, phone1, phone2, right(ltrim(rtrim(imp.CCAcctNbr)), 6) as misc2, right(ltrim(rtrim(PrimarySSN)), 4) as misc1, 
			   case
				when ltrim(rtrim(imp.RewardsNowSts)) = 'S' then 'S'
				else 'A'
			   end, @EndDate, 0, 0, 0, 0
	from dbo.impcustomer imp
	where tipnumber = @Tip
	


	fetch next from csrInsert into @Tip
END

close csrInsert
deallocate csrInsert




/* set Default status to A */
Update dbo.Customer_Stage
	Set [STATUS] = 'A' 
Where [STATUS] IS NULL  or [status] = ''


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.[status] S join dbo.Customer_Stage cstg
on S.[Status] = cstg.[Status]
