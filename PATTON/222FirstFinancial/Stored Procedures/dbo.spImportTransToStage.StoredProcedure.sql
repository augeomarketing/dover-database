use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spImportTransToStage' and xtype = 'P')
	drop procedure dbo.spImportTransToStage
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*********************************************/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/*									*/
/* Modifications						*/
/* 16 Apr 2008 PHB						*/
/* Copied from 208SouthFlorida & Modified	*/
/* for use with 218 LOC FCU				*/
/*********************************************/
/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/


CREATE PROCEDURE [dbo].[spImportTransToStage] 
AS 

Declare @MaxPointsPerYear decimal(18,0), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
Declare @dbName varchar(50) 
declare @TipFirst varchar(3)
Declare @SQLStmt nvarchar(2000) 


/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
insert into dbo.history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
		select Tip, Acctnum, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 
		From dbo.TransStandard


	/*    Get max Points per year from client table                               */
	set @MaxPointsPerYear = ( Select MaxPointsPerYear from dbo.client ) 

	/*  Update History_stage points and overage if over MaxPointsPerYear */
	-- Calc overage
	If @MaxPointsPerYear > 0 
	Begin 
		update H
			set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM dbo.History_Stage H JOIN dbo.vw_Affiliat_Stage_TipNumber_SumYTDEarned A 
			on H.Tipnumber = A.Tipnumber
			and h.acctid = a.acctid
		where A.YTDEarned + H.Points > @MaxPointsPerYear 
		and h.secid = 'NEW'

	End

	/***************************** AFFILIAT_STAGE *****************************/
	-- Update Affiliat YTDEarned 
	Update A
		set YTDEarned  = A.YTDEarned  + H.Points 
	FROM dbo.HISTORY_STAGE H JOIN dbo.AFFILIAT_Stage A 
		on H.Tipnumber = A.Tipnumber
		and h.acctid = a.acctid

	-- Update History_Stage Points = Points - Overage
	Update dbo.History_Stage 
		Set Points = Points - Overage 
	where Points >= Overage


	/***************************** CUSTOMER_STAGE *****************************/
	/* Update the Customer_Stage RunAvailable  with new history points  */ 
	Update dbo.Customer_Stage 
		Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
			RunAvailable		= isnull(RunAvailable, 0)

	Update C
		Set	RunAvailable  = RunAvailable + isnull(v.Points, 0),
			RunAvaliableNew = isnull(v.Points, 0)
	From dbo.Customer_Stage C join dbo.vw_histpoints V 
		on C.Tipnumber = V.Tipnumber


