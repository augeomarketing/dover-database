use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spCalculatePoints' and xtype = 'P')
	drop procedure dbo.spCalculatePoints
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/*  **************************************	*/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* 4/15/08 PHB - Modified to work with		*/
/*				LOC FCU				*/
/*  **************************************	*/
/*  Calculates points in input_transaction	*/
/*  **************************************	*/
CREATE PROCEDURE [dbo].[spCalculatePoints]  

AS   

	Update imp 
		set Points = round(imp.TransactionAmt * F.pointFactor , 0)
	from dbo.impTransaction imp join dbo.Trancode_factor F 
	on imp.RNTrancode = f.Trancode
	
--
-- Using TNB's provided processing status rules, points do NOT get
-- get calculated on these statuses
--
	update imptransaction	
		set points = 0
	where accountsts in ('B', 'C', 'E', 'F', 'I', 'U', 'Z')
