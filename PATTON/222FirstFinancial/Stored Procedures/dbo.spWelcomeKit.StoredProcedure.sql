USE [222FirstFinancialAuto]
GO

/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 05/19/2009 16:00:49 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spWelcomeKit]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spWelcomeKit]
GO

USE [222FirstFinancialAuto]
GO

/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 05/19/2009 16:00:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*  RDT 3/16/07  */

/* 

This retrieves all customers that were added in the previous month
It does not do an exact date match on the custom date added column

  */
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate datetime, @TipFirst nchar(3)
AS 


Truncate Table dbo.Welcomekit 

insert into dbo.Welcomekit 
SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
		ADDRESS2, ADDRESS3, City, State, ZipCode 
FROM dbo.customer 
WHERE (Year(DATEADDED) = Year(@EndDate)
	AND Month(DATEADDED) = Month(@EndDate)
	AND Upper(STATUS) <> 'C') 

-- Update DateForAudit table with Month End processing date
update dbo.DateforAudit 
	set Datein=@EndDate

GO


