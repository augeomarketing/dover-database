use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadTransStandard' and xtype = 'P')
	drop procedure dbo.spLoadTransStandard
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded datetime AS


-- Clear TransStandard 
Truncate table dbo.TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  cus.[TipNumber], @DateAdded, txn.[CCAcctNbr], [RNTranCode], count(*) , sum([Points]), @DateAdded
from dbo.impTransaction txn join dbo.customer_stage cus
	on txn.tipnumber = cus.tipnumber
where Status in ('A', 'S') -- active or suspended only
group by cus.[TipNumber], txn.[CCAcctNbr], [RNTranCode]


-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update dbo.TransStandard 
	set	TranType = left(R.Description, 20),
		Ratio = R.Ratio 
from RewardsNow.dbo.Trantype R join dbo.TransStandard T 
on R.TranCode = T.Trancode
