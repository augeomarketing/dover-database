use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where name = 'spLoadTipNumbers' and xtype = 'P')
	drop procedure dbo.spLoadTipNumbers
go

create procedure dbo.spLoadTipNumbers
		@TipPrefix		varchar(3)
as

declare @NewTip varchar(15)

-------------------------------------------------------------
-- Assign tips to known accounts
-------------------------------------------------------------

--
-- Lookup tipnumber using credit card# in affiliat_stage
--
update cus
	set tipnumber = stg.tipnumber
from dbo.impcustomer cus join dbo.affiliat_stage stg
	on cus.ccacctnbr = stg.acctid
	and cus.tipprefix = left(stg.tipnumber,3)

--
-- Lookup tipnumber using old credit card# in affiliat_stage
--
update cus
	set tipnumber = stg.tipnumber
from dbo.impcustomer cus join dbo.affiliat_stage stg
	on cus.oldccacctnbr = stg.acctid
	and cus.tipprefix = left(stg.tipnumber,3)
where isnull(cus.tipnumber, '')  = ''
and cus.oldccacctnbr != ''


-------------------------------------------------------------
-- Generate new TIPs
-------------------------------------------------------------

-- Get last used tip number
set @NewTip = ( select LastTipNumberUsed  from dbo.Client )

-- Should it come back as null, initialize it to 12 zeroes
If @NewTip is NULL  
	Set @NewTip = @TipPrefix+'000000000000'

-- drop table #AssignTipsToDDA
create table #AssignTipsToDDA (ccacctnbr varchar(25) primary key, TipNumber varchar(15) null)

insert into #AssignTipsToDDA
select distinct ccacctnbr, space(15) as TipNumber
from dbo.impcustomer imp
where imp.tipnumber is null

update #AssignTipsToDDA
	set @NewTip = TipNumber = cast( cast(@NewTip as bigint) + 1 as varchar(15))


/* Assign Get New Tips to new customers */
update imp
	set  tipnumber = tmp.tipnumber,
		  TipPrefix = left(tmp.tipnumber,3)
from #AssignTipsToDDA tmp join dbo.impcustomer imp
	on tmp.ccacctnbr = imp.ccacctnbr

-- Update LastTip -- 
Update dbo.Client 
	set LastTipNumberUsed = @NewTip  


-- Update imptransaction table with TIPNumbers
update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impCustomer cus
	on txn.ccacctnbr = cus.ccacctnbr

GO


