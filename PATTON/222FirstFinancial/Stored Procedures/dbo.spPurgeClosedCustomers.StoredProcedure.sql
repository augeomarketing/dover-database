use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spPurgeClosedCustomers' and xtype = 'P')
	drop procedure dbo.spPurgeClosedCustomers
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read impCustomer_Purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.

--RDT 3/21/08 and tipnumber Not In ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

--PHB 4/15/08 refactored sql inserts/updates.  Changed to match the tables used for 218LOCFCU

/******************************************************************************/
create PROCEDURE [dbo].[spPurgeClosedCustomers]  
	@Production_Flag	char(1), 
	@DateDeleted		datetime,
	@TipFirst			varchar(3) = null

AS

Declare @SQLDynamic nvarchar(4000)
declare @SQL		nvarchar(4000)
Declare @DBName	varchar(50)

Declare @Tipnumber 	char(15)


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin

	truncate table dbo.impcustomer_Purge


	insert into dbo.impCustomer_Purge
	(TipNumber, CCAcctNbr, PrimaryNm)
	select TipNumber, CCAcctNbr, PrimaryNm
	from dbo.impCustomer_Purge_Pending ipp 
	

	truncate table dbo.impCustomer_Purge_Pending
	
	-- Get all the opt outs
	Insert Into dbo.impCustomer_Purge
	(TipNumber, CCAcctNbr, PrimaryNm)
	Select TipNumber, AcctId, FirstName
	from rewardsnow.dbo.OptOutTracking oot 
	where left(oot.tipnumber,3) = '222' and tipnumber not in (select distinct tipnumber 
													from dbo.history 
													where histdate > @DateDeleted and trancode not in ('RQ'))



	delete cus
	from dbo.impCustomer cus join dbo.impCustomer_Purge prg
		on cus.ccacctnbr = prg.ccacctnbr
		

	delete txn
	from dbo.impTransaction txn join dbo.impCustomer_Purge prg
		on txn.ccacctnbr = prg.ccacctnbr


	delete aff
	from dbo.affiliat_stage aff join dbo.impCustomer_Purge dlt
		on aff.tipnumber = dlt.tipnumber
		and aff.acctid = dlt.ccacctnbr


	delete cus
	from dbo.customer_stage cus join dbo.impCustomer_Purge dlt
		on cus.tipnumber = dlt.tipnumber


	delete his
	from dbo.history_stage his join dbo.impCustomer_Purge dlt
		on his.tipnumber = dlt.tipnumber


End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin



	set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

	-- copy any impCustomer_Purge_pending into impCustomer_Purge
	 
	Insert into dbo.impCustomer_Purge
	(TipNumber, CCAcctNbr, PrimaryNm)
	select TipNumber, CCAcctNbr, PrimaryNm
	from dbo.impCustomer_Purge_Pending
	where left(tipnumber,3) = @TipFirst

	-- Clear impCustomer_Purge_Pending 
	Truncate Table dbo.impCustomer_Purge_Pending

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(TipNumber, CCAcctNbr, PrimaryNm)
	select distinct imp.TipNumber, CCAcctNbr, PrimaryNm
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	'RQ' != his.trancode
		where left(imp.tipnumber,3) = @TipFirst


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	set @sql = '
	Delete imp
		from dbo.impCustomer_Purge imp join ' + @dbname + '.dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @DateDeleted
			and	his.trancode != ''RQ''
		where left(imp.tipnumber,3) = @TipFirst'
	exec sp_executesql @SQL, N'@datedeleted datetime, @TipFirst varchar(3)', @datedeleted = @datedeleted, @TipFirst = @tipfirst



	-- Insert customer to customerdeleted
	set @SQL = '
	Insert Into ' + @dbname + '.dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select distinct c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, c.DATEADDED, 
			c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline,
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from ' + @dbname + '.dbo.Customer c join dbo.impcustomer_purge prg on c.tipnumber = prg.tipnumber
	where left(c.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

	exec sp_executesql @sql, N'@DateDeleted datetime', @DateDeleted = @DateDeleted




	-- Insert affiliat to affiliatdeleted 
	set @SQL = '
	Insert Into ' + @dbname + '.dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select distinct AcctId, aff.TipNumber, AcctType, aff.DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
	from ' + @dbname + '.dbo.Affiliat aff join dbo.impCustomer_Purge prg
		on aff.tipnumber = prg.tipnumber
	where left(aff.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

	exec sp_executesql @sql, N'@DateDeleted datetime', @DateDeleted = @DateDeleted

	-- copy history to historyDeleted 
	set @SQL = '
	Insert Into ' + @dbname + '.dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from ' + @dbname + '.dbo.History H join (	select distinct tipnumber
										from dbo.impcustomer_purge) prg
		on h.tipnumber = prg.tipnumber
	where left(h.tipnumber,3) = ' + char(39) + @tipfirst + char(39)

	exec sp_executesql @sql, N'@DateDeleted datetime', @DateDeleted = @DateDeleted


	-- Delete from customer 
	set @sql = '
	Delete c
	from ' + @dbname + '.dbo.Customer c join (	select tipnumber
										from dbo.impcustomer_purge ) prg
		on c.tipnumber = prg.tipnumber'

	exec sp_executesql @sql


	-- Delete records from affiliat 
	set @SQL = '
	Delete aff
	from ' + @dbname + '.dbo.Affiliat aff join dbo.impCustomer_Purge prg
		on aff.tipnumber = prg.tipnumber'

	exec sp_executesql @SQL


	-- Delete records from History 
	set @SQL = '
	Delete h
	from ' + @dbname + '.dbo.History h join (	select tipnumber
										from dbo.impcustomer_purge ) prg
		on h.tipnumber = prg.tipnumber'
	
	exec sp_executesql @SQL


	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	set @SQL = '
	Update c
		set status = ''C''
	from ' + @dbname + '.dbo.customer c join dbo.impCustomer_Purge_Pending prg
		on c.tipnumber = prg.tipnumber'

	exec sp_executesql @SQL


End
