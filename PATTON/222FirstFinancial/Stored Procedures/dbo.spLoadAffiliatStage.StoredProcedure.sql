use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadAffiliatStage' and xtype = 'P')
	drop procedure dbo.spLoadAffiliatStage
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


/*  ******************************************/
/* Date:  4/1/07						*/
/* Author:  Rich T						*/
/*  ******************************************/

/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*
	4/15/2008 PHB:  Copied & modified for use w/ 218LOCFCU
*/
/*********************************************/


CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   

	@MonthEnd datetime  

AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select distinct t.CCAcctNbr, t.TipNumber, 'CREDIT',  @MonthEnd, 
			ltrim(rtrim(c.status)), 'CREDIT CARD' accttype, isnull(c.LastName, 'NOT FOUND'), 0

	from dbo.customer_stage c join dbo.impCustomer t 
		on c.Tipnumber = t.Tipnumber

	left outer join dbo.Affiliat_Stage aff
	on t.CCAcctNbr = aff.AcctId

	where aff.AcctId is null 


--
-- For Lost/Stolen cards - those card#s found in OLDCCACCTNBR, mark them as "C" in the affiliat table
--
update aff
	set acctstatus = 'C'
from dbo.impcustomer imp join dbo.affiliat_stage aff
	on imp.oldccacctnbr = aff.acctid
where acctstatus = 'A'


--
-- mark the card closed if there is NOT a "old ccacctnbr" specfied
--
update aff
	set acctstatus = 'C'
from dbo.impcustomer imp join dbo.affiliat_stage aff
	on imp.ccacctnbr = aff.acctid
where RewardsNowSts = 'C' and ( len(OldCCAcctNbr) = 0 OR oldccacctnbr like '0000000000%')

