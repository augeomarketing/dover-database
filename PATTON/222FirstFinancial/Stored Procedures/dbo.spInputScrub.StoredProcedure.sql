use [222FirstFinancialAuto]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spInputScrub' and xtype = 'P')	
	drop procedure dbo.spInputScrub
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO



/********************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables  */
/*																*/
/********************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS

/* clear error tables */
Truncate Table  ImpCustomer_Error
Truncate Table ImpTransaction_Error


--------------- Input Customer table
/* Remove input_customer records with Name = null */
Insert into dbo.ImpCustomer_error
(CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, AddressLine2, City, StateCd, Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr)
	select  CCAcctNbr, PrimaryNm, SecondaryNm, ExternalSts, InternalSts, OldCCAcctNbr, AddressLine1, AddressLine2, City, StateCd, Zip, Phone1, Phone2, PrimarySSN, AssocNbr, CheckingNbr
	from dbo.ImpCustomer 
	where (PrimaryNm is null) OR (CCAcctNbr is null) OR (CCAcctNbr = '')  or (ccacctnbr = 'null')

delete from dbo.ImpCustomer 
	where (PrimaryNm is null) OR (CCAcctNbr is null) OR (CCAcctNbr = '') or (ccacctnbr = 'null')


/* remove leading and trailing spaces in names and addresses */ 
Update dbo.ImpCustomer 
	set	CCAcctNbr =		ltrim(rtrim(CCAcctNbr)), 
		PrimaryNm =		ltrim(rtrim(PrimaryNm)), 
		SecondaryNm =		ltrim(rtrim(SecondaryNm)), 
		OldCCAcctNbr =		ltrim(rtrim(OldCCAcctNbr)),	
		AddressLine1 =		ltrim(rtrim(AddressLine1)), 
		AddressLine2 =		ltrim(rtrim(AddressLine2)), 
		City =			ltrim(rtrim(City)), 
		StateCd =			ltrim(rtrim(StateCd)), 
		Zip =			ltrim(rtrim(Zip)),		
		Phone1 =			ltrim(rtrim(Phone1)), 
		Phone2 =			ltrim(rtrim(Phone2)), 
		PrimarySSN =		ltrim(rtrim(PrimarySSN)), 
		AssocNbr =		ltrim(rtrim(AssocNbr)), 
		CheckingNbr =		ltrim(rtrim(CheckingNbr)), 
		CreditUnionNbr =	ltrim(rtrim(CreditUnionNbr))

/* Check if there are transactions which there are no matching customers */
Insert into dbo.ImpTransaction_error 
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts)
select txn.AssocNbr, txn.System, txn.Prin, txn.TransactionDt, txn.CCAcctNbr, txn.TransactionCd, txn.TransactionDesc, 
		cast(txn.TransactionAmt as varchar(10)) as TxnAmt, txn.AccountSts 
from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
	on txn.CCAcctNbr = cus.CCAcctNbr

left outer join dbo.impcustomer cus2
	on txn.ccacctnbr = cus2.oldccacctnbr 

left outer join dbo.affiliat_stage aff
	on txn.ccacctnbr = aff.acctid
where ( (cus.CCAcctNbr is null and cus2.oldccacctnbr is null and aff.tipnumber is null) or txn.ccacctnbr = '')





/* Check if there are transactions without valid tran codes */
Insert into dbo.ImpTransaction_error 
(AssocNbr, System, Prin, TransactionDt, CCAcctNbr, TransactionCd, TransactionDesc, TransactionAmt, AccountSts, AcctNbr)

select txn.AssocNbr, txn.System, txn.Prin, txn.TransactionDt, txn.CCAcctNbr, txn.TransactionCd, txn.TransactionDesc, 
		cast(txn.TransactionAmt as varchar(10)) as TxnAmt, txn.AccountSts, txn.AcctNbr
from dbo.ImpTransaction txn
where transactioncd is null


Delete txn
from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
	on txn.CCAcctNbr = cus.CCAcctNbr

left outer join dbo.impcustomer cus2
	on txn.ccacctnbr = cus2.oldccacctnbr 

left outer join dbo.affiliat_stage aff
	on txn.ccacctnbr = aff.acctid
where ( (cus.CCAcctNbr is null and cus2.oldccacctnbr is null and aff.tipnumber is null) or txn.ccacctnbr = '') 


delete from dbo.impTransaction
	where transactioncd is null



------------------------------------------------------------
-- Change checking DDA to empty string where it is all zeroes
------------------------------------------------------------
update dbo.impcustomer
	set CheckingNbr = ''
where checkingNbr is null or checkingNbr =  '0000000000000000' 


------------------------------------------------------------
-- Change OLD CC Acct# from null to empty string
------------------------------------------------------------
update dbo.impcustomer
	set oldccacctnbr = ''
where oldccacctnbr is null or oldccacctnbr = '0000000000000000'


------------------------------------------------------------
-- Update status codes based on StatusCode_CrossReference
------------------------------------------------------------

Update cus
	set RewardsNowSts = xref.RNStatusCd
from dbo.StatusCode_CrossREference xref join dbo.impCustomer cus
on cus.ExternalSts = xref.TNBStatusCd
and 'E' = xref.statuscodetype


Update cus
	set RewardsNowSts = xref.RNStatusCd
from dbo.StatusCode_CrossREference xref join dbo.impCustomer cus
on cus.InternalSts = xref.TNBStatusCd
and 'I' = xref.statuscodetype
where isnull(rewardsnowsts, '') = ''  -- only update rewardsnowsts if there isn't an external status


update dbo.impcustomer
	set rewardsnowsts = 'A'
where rewardsnowsts is null


------------------------------------------------------------
-- Translate TNB transaction codes to RewardsNow tran codes
------------------------------------------------------------
update dbo.impTransaction
	set RNTranCode = case
					when TransactionCd = '253' then '67'
					when TransactionCd = '255' then '37'
					when TransactionCd = '256' then '37'
				  end
where system in('3528', '4882')

update dbo.impTransaction
	set RNTranCode = case
					when TransactionCd = '253' then '63'
					when TransactionCd = '255' then '33'
					when TransactionCd = '256' then '33'
				  end
where system not in('3528', '4882')

update dbo.impTransaction
	set RNTranCode = case
					when TransactionCd in ('PCV', 'PCP') then '67'
					when TransactioNCd in ('RET') then '37'
				  end
where transactioncd in('PCV', 'PCP', 'RET')

--
-- Remove tranasctions that have null trancodes
-- Right now, this is just TNB TransactionCode 280 which is adjustments
delete from dbo.impTransaction
where RNTranCode is null


------------------------------------------------------------
-- Update Transaction Amounts
------------------------------------------------------------

-- Convert to dollars 
update dbo.impTransaction
	set TransactionAmt = abs( cast(TransactionAmt as money) / cast(100 as money))

