USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_krogerhistory]    Script Date: 09/27/2012 09:53:41 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_krogerhistory]'))
DROP VIEW [dbo].[vw_krogerhistory]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_krogerhistory]    Script Date: 09/27/2012 09:53:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_krogerhistory]

AS

(
Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, NULL as DateDeleted from [52JKroegerPersonalFinance].dbo.HISTORY
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, isnull(DateDeleted,'') from [52JKroegerPersonalFinance].dbo.HistoryDeleted
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, NULL as DateDeleted from [52R].dbo.HISTORY
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, isnull(DateDeleted,'') from [52R].dbo.HistoryDeleted
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, NULL as DateDeleted from [52U].dbo.HISTORY
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, isnull(DateDeleted,'') from [52U].dbo.HistoryDeleted
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, NULL as DateDeleted from [52V].dbo.HISTORY
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, isnull(DateDeleted,'') from [52V].dbo.HistoryDeleted
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, NULL as DateDeleted from [52W].dbo.HISTORY
union	Select tipnumber, acctid, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, isnull(DateDeleted,'') from [52W].dbo.HistoryDeleted
)

GO


