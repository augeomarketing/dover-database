USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2ktranimportraw]    Script Date: 11/15/2010 16:35:08 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_b2ktranimportraw]'))
DROP VIEW [dbo].[vw_b2ktranimportraw]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2ktranimportraw]    Script Date: 11/15/2010 16:35:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_b2ktranimportraw]
AS
SELECT
	dim_b2ktranimportraw_filedate
	, dim_b2ktranimportraw_corpid
	, dim_b2ktranimportraw_agentinstitutionid
	, dim_b2ktranimportraw_accountnumber
	, dim_b2ktranimportraw_trandate
	, dim_b2ktranimportraw_transactionpostdate                      
	, dim_b2ktranimportraw_transactionsign                          
	, dim_b2ktranimportraw_transactionamount                        
	, dim_b2ktranimportraw_prescoredearnings                        
	, dim_b2ktranimportraw_merchantname                             
	, dim_b2ktranimportraw_merchantcity                             
	, dim_b2ktranimportraw_merchantstate                            
	, dim_b2ktranimportraw_merchantcountry                          
	, dim_b2ktranimportraw_mccsiccode                               
	, dim_b2ktranimportraw_industrycode                             
	, dim_b2ktranimportraw_transactioncode                          
	, dim_b2ktranimportraw_reasoncode                               
	, dim_b2ktranimportraw_transactioncurrencyidentifier            
	, dim_b2ktranimportraw_pricingplanid                            
	, dim_b2ktranimportraw_transactiontype                          
	, dim_b2ktranimportraw_referencenumber                          
	, dim_b2ktranimportraw_eciindicator                             
	, dim_b2ktranimportraw_vendorindicator                          
	, dim_b2ktranimportraw_filler1                                  
	, dim_b2ktranimportraw_merchantid                               
	, dim_b2ktranimportraw_filler2                                  
FROM
	b2ktranimportraw

GO


