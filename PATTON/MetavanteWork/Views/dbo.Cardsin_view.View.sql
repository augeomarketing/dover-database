USE [MetavanteWork]
GO
/****** Object:  View [dbo].[Cardsin_view]    Script Date: 09/21/2009 10:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Cardsin_view] AS 
			SELECT tipnumber, ssn, ddanum, na1, na2, na3, na4, na5, na6
			FROM cardsin
			group by tipnumber, ssn, ddanum, na1, na2, na3, na4, na5, na6
GO
