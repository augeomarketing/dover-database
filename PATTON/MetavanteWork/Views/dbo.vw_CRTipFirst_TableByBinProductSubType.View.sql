USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_CRTipFirst_TableByBinProductSubType]    Script Date: 12/01/2010 11:06:03 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_CRTipFirst_TableByBinProductSubType]'))
DROP VIEW [dbo].[vw_CRTipFirst_TableByBinProductSubType]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_CRTipFirst_TableByBinProductSubType]    Script Date: 12/01/2010 11:06:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_CRTipFirst_TableByBinProductSubType]
AS
	SELECT Bin, ProductLine, SubProductType
	FROM CRTipFirst_Table
	WHERE ProductLine is not null
	GROUP BY Bin, ProductLine, SubProductType

GO


