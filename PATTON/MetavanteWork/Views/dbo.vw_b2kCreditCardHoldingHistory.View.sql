USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kCreditCardHoldingHistory]    Script Date: 12/07/2010 14:02:23 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_b2kCreditCardHoldingHistory]'))
DROP VIEW [dbo].[vw_b2kCreditCardHoldingHistory]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kCreditCardHoldingHistory]    Script Date: 12/07/2010 14:02:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_b2kCreditCardHoldingHistory]  
as  
select  
 crtf.bank as bank  
 , crtf.agent as agent  
 , crtf.tipfirst as tipfirst  
 , demog.dim_b2kdemog_binplan as bin  
 , RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_primarysocialsecuritynumber) as ssn  
 , demog.dim_b2kdemog_cardnumber as acctnum  
 , demog.dim_b2kdemog_primarylastname as lastname  
 , RewardsNow.dbo.ufn_FormatName(2, demog.dim_b2kdemog_primaryfirstname, demog.dim_b2kdemog_primarylastname, demog.dim_b2kdemog_primarymiddleinitial) as na1  
 , RewardsNow.dbo.ufn_FormatName(2, demog.dim_b2kdemog_secondaryfirstname, demog.dim_b2kdemog_secondarylastname, demog.dim_b2kdemog_secondarymiddleinitial) as na2  
 , demog.dim_b2kdemog_rnstatus [status]  
 , demog.dim_b2kdemog_oldcardnumber as oldcc  
 , demog.dim_b2kdemog_addressline1 as addr1  
 , demog.dim_b2kdemog_addressline2 as addr2  
 , demog.dim_b2kdemog_addressline3 as addr3  
 , rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_city) + ' ' + rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_state) as citystate  
 , rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_city) as city  
 , rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_state) as [state]  
 , demog.dim_b2kdemog_zipcode as zip  
 , tru.num_purchases as numpurch  
 , tru.amt_purchases as purch  
 , tru.num_returns as numret  
 , tru.amt_returns as amtret  
 , tru.period as period   
 , case   
  when cast(RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_homephonenumber) as bigint) = 0   
   then ''  
   else RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_homephonenumber)  
  end as phone1  
 , case   
  when cast(RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_workphonenumber) as bigint) = 0   
   then ''  
   else RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_workphonenumber)  
  end as phone2  
from b2kDemog demog  
inner join vw_CRTipFirstTable_BankAgentByBin crtf  
 on demog.dim_b2kdemog_binplan = crtf.bin  
inner join vw_b2kTransactionRawHistoryRollupByRatioType tru  
 on demog.dim_b2kdemog_cardnumber = tru.dim_b2ktranimportraw_accountnumber  

GO


