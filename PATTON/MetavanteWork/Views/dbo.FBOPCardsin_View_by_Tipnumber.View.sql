USE [MetavanteWork]
GO
/****** Object:  View [dbo].[FBOPCardsin_View_by_Tipnumber]    Script Date: 09/21/2009 10:35:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[FBOPCardsin_View_by_Tipnumber]
AS
SELECT     TOP 100 PERCENT dbo.FBOPCARDSIN.*, TIPNUMBER AS Expr1, SSN AS Expr2, DDANUM AS Expr3
FROM         dbo.FBOPCARDSIN
ORDER BY TIPNUMBER, SSN, DDANUM
GO
