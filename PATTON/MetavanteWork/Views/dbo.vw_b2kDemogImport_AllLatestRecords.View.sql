USE [MetavanteWork]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID(N'vw_b2kDemogImport_AllLatestRecords') IS NOT NULL
	DROP VIEW vw_b2kDemogImport_AllLatestRecords
GO

CREATE VIEW [dbo].[vw_b2kDemogImport_AllLatestRecords]
AS

SELECT
	dmg.sid_b2kdemogimportraw_id
	, dmg.dim_b2kdemogimportraw_importdate
	, dmg.dim_b2kdemogimportraw_filedate
	, dmg.dim_b2kdemogimportraw_recordtype
	, dmg.dim_b2kdemogimportraw_relationshipaccount
	, dmg.dim_b2kdemogimportraw_cardnumber
	, dmg.dim_b2kdemogimportraw_primarycardnumber
	, dmg.dim_b2kdemogimportraw_cardtype
	, dmg.dim_b2kdemogimportraw_accounttype
	, dmg.dim_b2kdemogimportraw_producttype
	, dmg.dim_b2kdemogimportraw_productline
	, dmg.dim_b2kdemogimportraw_subproducttype
	, dmg.dim_b2kdemogimportraw_programid
	, dmg.dim_b2kdemogimportraw_primarynameprefix
	, dmg.dim_b2kdemogimportraw_primaryfirstname
	, dmg.dim_b2kdemogimportraw_primarymiddleinitial
	, dmg.dim_b2kdemogimportraw_primarylastname
	, dmg.dim_b2kdemogimportraw_primarynamesuffix
	, dmg.dim_b2kdemogimportraw_secondarynameprefix
	, dmg.dim_b2kdemogimportraw_secondaryfirstname
	, dmg.dim_b2kdemogimportraw_secondarymiddleinitial
	, dmg.dim_b2kdemogimportraw_secondarylastname
	, dmg.dim_b2kdemogimportraw_secondarynamesuffix
	, dmg.dim_b2kdemogimportraw_addressline1
	, dmg.dim_b2kdemogimportraw_addressline2
	, dmg.dim_b2kdemogimportraw_addressline3
	, dmg.dim_b2kdemogimportraw_addressline4
	, dmg.dim_b2kdemogimportraw_city
	, dmg.dim_b2kdemogimportraw_state
	, dmg.dim_b2kdemogimportraw_country
	, dmg.dim_b2kdemogimportraw_zipcode
	, dmg.dim_b2kdemogimportraw_homephonenumber
	, dmg.dim_b2kdemogimportraw_workphonenumber
	, dmg.dim_b2kdemogimportraw_workphoneextension
	, dmg.dim_b2kdemogimportraw_emailaddress
	, dmg.dim_b2kdemogimportraw_scprogramstatus
	, dmg.dim_b2kdemogimportraw_scprogramstatuschangedate
	, dmg.dim_b2kdemogimportraw_hostaccountstatus
	, dmg.dim_b2kdemogimportraw_hostacctstatusdate
	, dmg.dim_b2kdemogimportraw_scparticipationflag
	, dmg.dim_b2kdemogimportraw_primarysocialsecuritynumber
	, dmg.dim_b2kdemogimportraw_secondarysocialsecuritynumber
	, dmg.dim_b2kdemogimportraw_primarygender
	, dmg.dim_b2kdemogimportraw_secondarygender
	, dmg.dim_b2kdemogimportraw_birthdate
	, dmg.dim_b2kdemogimportraw_accountopendate
	, dmg.dim_b2kdemogimportraw_languagepreference
	, dmg.dim_b2kdemogimportraw_effectivedate
	, dmg.dim_b2kdemogimportraw_purchaserate
	, dmg.dim_b2kdemogimportraw_newcardnumber
	, dmg.dim_b2kdemogimportraw_associationid
	, dmg.dim_b2kdemogimportraw_corpid
	, dmg.dim_b2kdemogimportraw_agentinstitutionid
	, dmg.dim_b2kdemogimportraw_billcode
	, dmg.dim_b2kdemogimportraw_binplan
	, dmg.dim_b2kdemogimportraw_scenrollmentdate
	, dmg.dim_b2kdemogimportraw_laststatementdate
	, dmg.dim_b2kdemogimportraw_transferdate
	, dmg.dim_b2kdemogimportraw_filler1
	, dmg.dim_b2kdemogimportraw_primaryhouseholdcardnumber
	, dmg.dim_b2kdemogimportraw_filler2
	, dmg.dim_b2kdemogimportraw_householdcardtype
	, dmg.dim_b2kdemogimportraw_institutionid
	, dmg.dim_b2kdemogimportraw_vipindicator
	, dmg.dim_b2kdemogimportraw_filler3
	, dmg.dim_b2kdemogimportraw_source
FROM
	vw_b2kDemogImport_AllRawRecords dmg
INNER JOIN 
(	
	SELECT dim_b2kdemogimportraw_cardnumber, MAX(dim_b2kdemogimportraw_filedate) maxfiledate
	FROM vw_b2kDemogImport_AllRawRecords
	GROUP BY dim_b2kdemogimportraw_cardnumber
) maxdate
ON dmg.dim_b2kdemogimportraw_cardnumber = maxdate.dim_b2kdemogimportraw_cardnumber
	and dmg.dim_b2kdemogimportraw_filedate = maxdate.maxfiledate
INNER JOIN
(
	SELECT max(sid_b2kdemogimportraw_id) maximportid, dim_b2kdemogimportraw_filedate, dim_b2kdemogimportraw_cardnumber
	from vw_b2kDemogImport_AllRawRecords
	group by dim_b2kdemogimportraw_filedate, dim_b2kdemogimportraw_cardnumber
) maxid
ON 
	maxid.dim_b2kdemogimportraw_cardnumber = maxdate.dim_b2kdemogimportraw_cardnumber
	and maxid.dim_b2kdemogimportraw_filedate = maxdate.maxfiledate
	and dmg.sid_b2kdemogimportraw_id = maxid.maximportid

GO


