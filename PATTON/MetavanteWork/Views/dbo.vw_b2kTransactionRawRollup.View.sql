USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kTransactionRawRollup]    Script Date: 02/08/2011 17:09:23 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_b2kTransactionRawRollup]'))
DROP VIEW [dbo].[vw_b2kTransactionRawRollup]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kTransactionRawRollup]    Script Date: 02/08/2011 17:09:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_b2kTransactionRawRollup]
--with schemabinding
as
select
	tir.dim_b2ktranimportraw_accountnumber as dim_b2ktranimportraw_accountnumber
	, tt.sid_tranreasoncodexref_trancode
	, sum(RewardsNow.dbo.ufn_CalculatePoints(tir.dim_b2ktranimportraw_transactionamount, 1, 2)) as points
	, COUNT(*) as numtrans
	, tt.Description
	, tt.Ratio
	, CONVERT(VARCHAR(10), RewardsNow.dbo.ufn_GetLastOfMonth(cast(tir.dim_b2ktranimportraw_filedate as datetime)), 101) as period
from dbo.b2kTranImportRaw tir
inner join dbo.vw_TranReasonCodeXrefTranType tt
	on tir.dim_b2ktranimportraw_transactioncode = tt.dim_tranreasoncodexref_trancodein
	and tir.dim_b2ktranimportraw_reasoncode = tt.dim_tranreasoncodexref_reasoncodein
group by
	tir.dim_b2ktranimportraw_accountnumber
	, tt.sid_tranreasoncodexref_trancode
	, tt.Description
	, tt.Ratio
	, RewardsNow.dbo.ufn_GetLastOfMonth(cast(tir.dim_b2ktranimportraw_filedate as datetime))

GO


