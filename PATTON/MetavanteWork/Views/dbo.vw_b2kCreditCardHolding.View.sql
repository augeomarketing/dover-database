USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kCreditCardHolding]    Script Date: 08/29/2011 10:59:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_b2kCreditCardHolding]
AS
SELECT
	demog.dim_b2kdemog_tipfirst as tipfirst
	, demog.dim_b2kdemog_binplan AS bin
	, LEFT(RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_primarysocialsecuritynumber), 9) AS ssn
	, demog.dim_b2kdemog_cardnumber AS acctnum
	, demog.dim_b2kdemog_primarylastname AS lastname
	, RewardsNow.dbo.ufn_FormatName(2, demog.dim_b2kdemog_primaryfirstname, demog.dim_b2kdemog_primarylastname, demog.dim_b2kdemog_primarymiddleinitial) AS na1
	, RewardsNow.dbo.ufn_FormatName(2, demog.dim_b2kdemog_secondaryfirstname, demog.dim_b2kdemog_secondarylastname, demog.dim_b2kdemog_secondarymiddleinitial) AS na2
	, demog.dim_b2kdemog_rnstatus [status]
	, demog.dim_b2kdemog_oldcardnumber AS oldcc
	, demog.dim_b2kdemog_addressline1 AS addr1
	, demog.dim_b2kdemog_addressline2 AS addr2
	, demog.dim_b2kdemog_addressline3 AS addr3
	, rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_city) + ' ' + rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_state) AS citystate
	, rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_city) AS city
	, rewardsnow.dbo.ufn_Trim(demog.dim_b2kdemog_state) AS [STATE]
	, demog.dim_b2kdemog_zipcode AS zip
	, ISNULL(tru.num_purchases, 0) AS numpurch
	, ISNULL(tru.amt_purchases, 0) AS purch
	, ISNULL(tru.num_returns, 0) AS numret
	, ISNULL(tru.amt_returns, 0) AS amtret
	, CASE 
		WHEN CAST(RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_homephonenumber) AS BIGINT) = 0 
			THEN ''
			ELSE RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_homephonenumber)
		END AS phone1
	, CASE 
		WHEN CAST(RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_workphonenumber) AS BIGINT) = 0 
			THEN ''
			ELSE RewardsNow.dbo.ufn_RemoveAlpha(demog.dim_b2kdemog_workphonenumber)
		END AS phone2
FROM b2kDemog demog
LEFT OUTER JOIN vw_b2kTransactionRawRollupByRatioType tru
	ON demog.dim_b2kdemog_cardnumber = tru.dim_b2ktranimportraw_accountnumber


GO


