USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_CRTipFirstTable_BankAgentByBin]    Script Date: 11/05/2010 16:44:38 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_CRTipFirstTable_BankAgentByBin]'))
DROP VIEW [dbo].[vw_CRTipFirstTable_BankAgentByBin]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_CRTipFirstTable_BankAgentByBin]    Script Date: 11/05/2010 16:44:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_CRTipFirstTable_BankAgentByBin]
AS

	SELECT LEFT(t1.bankagent,4) AS bank
		, rewardsnow.dbo.ufn_pad(RIGHT(t1.bankagent, 4), 'L', 4, ' ') AS agent
		, bin
		, tipfirst
	FROM
		(SELECT MIN(bank+agent) AS bankagent
			, BIN
			, tipfirst
			FROM CRTipFirst_Table 
			GROUP BY bin, Tipfirst
		) t1
	WHERE Bin IS NOT NULL


GO

