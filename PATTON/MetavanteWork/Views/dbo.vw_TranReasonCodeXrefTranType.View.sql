USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_TranReasonCodeXrefTranType]    Script Date: 11/15/2010 16:38:00 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_TranReasonCodeXrefTranType]'))
DROP VIEW [dbo].[vw_TranReasonCodeXrefTranType]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_TranReasonCodeXrefTranType]    Script Date: 11/15/2010 16:38:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_TranReasonCodeXrefTranType]
with schemabinding
as
	select
		x.sid_tranreasoncodexref_id
		, x.dim_tranreasoncodexref_trancodein
		, x.dim_tranreasoncodexref_reasoncodein
		, x.sid_tranreasoncodexref_trancode
		, t.Description
		, t.Points
		, t.Ratio
	from dbo.TranReasonCodeXref x
	inner join dbo.TranType t
		on x.sid_tranreasoncodexref_trancode = t.TranCode

GO


