USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kTransactionRawRollupByRatioType]    Script Date: 12/07/2010 13:46:17 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_b2kTransactionRawRollupByRatioType]'))
DROP VIEW [dbo].[vw_b2kTransactionRawRollupByRatioType]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kTransactionRawRollupByRatioType]    Script Date: 12/07/2010 13:46:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_b2kTransactionRawRollupByRatioType]
as
select 
	tr.dim_b2ktranimportraw_accountnumber
	, tr.period as period
	, min(isnull(purch.numtrans, 0)) as num_purchases
	, min(ISNULL(purch.points, 0)) as amt_purchases
	, min(isnull(ret.numtrans, 0)) as num_returns
	, min(isnull(ret.points, 0)) as amt_returns
from vw_b2kTransactionRawRollup tr
left outer join vw_b2kTransactionRawRollup purch
	on tr.dim_b2ktranimportraw_accountnumber = purch.dim_b2ktranimportraw_accountnumber
	and purch.Ratio > 0
left outer join vw_b2kTransactionRawRollup ret
	on tr.dim_b2ktranimportraw_accountnumber = ret.dim_b2ktranimportraw_accountnumber
	and ret.Ratio < 0
group by 
	tr.dim_b2ktranimportraw_accountnumber
	, tr.period

GO


