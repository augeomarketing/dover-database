USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_CRTipFirstTable_TipFirstByProdLineSubType]    Script Date: 12/13/2010 14:15:04 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_CRTipFirstTable_TipFirstByProdLineSubType]'))
DROP VIEW [dbo].[vw_CRTipFirstTable_TipFirstByProdLineSubType]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_CRTipFirstTable_TipFirstByProdLineSubType]    Script Date: 12/13/2010 14:15:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_CRTipFirstTable_TipFirstByProdLineSubType]
AS

	SELECT Tipfirst, ProductLine, SubProductType, Bin, Bank, Agent
	FROM CRTipFirst_Table
	WHERE tipfirst IS NOT NULL AND productline IS NOT NULL AND subproducttype IS NOT NULL AND BIN IS NOT NULL
	GROUP BY Tipfirst, ProductLine, SubProductType, Bin, Bank, Agent



GO


