USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kdemogimportraw]    Script Date: 11/15/2010 16:34:25 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_b2kdemogimportraw]'))
DROP VIEW [dbo].[vw_b2kdemogimportraw]
GO

USE [MetavanteWork]
GO

/****** Object:  View [dbo].[vw_b2kdemogimportraw]    Script Date: 11/15/2010 16:34:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_b2kdemogimportraw]
AS
SELECT
	dim_b2kdemogimportraw_filedate
	, dim_b2kdemogimportraw_recordtype
	, dim_b2kdemogimportraw_relationshipaccount
	, dim_b2kdemogimportraw_cardnumber
	, dim_b2kdemogimportraw_primarycardnumber
	, dim_b2kdemogimportraw_cardtype
	, dim_b2kdemogimportraw_accounttype
	, dim_b2kdemogimportraw_producttype
	, dim_b2kdemogimportraw_productline
	, dim_b2kdemogimportraw_subproducttype
	, dim_b2kdemogimportraw_programid
	, dim_b2kdemogimportraw_primarynameprefix
	, dim_b2kdemogimportraw_primaryfirstname
	, dim_b2kdemogimportraw_primarymiddleinitial
	, dim_b2kdemogimportraw_primarylastname
	, dim_b2kdemogimportraw_primarynamesuffix
	, dim_b2kdemogimportraw_secondarynameprefix
	, dim_b2kdemogimportraw_secondaryfirstname
	, dim_b2kdemogimportraw_secondarymiddleinitial
	, dim_b2kdemogimportraw_secondarylastname
	, dim_b2kdemogimportraw_secondarynamesuffix
	, dim_b2kdemogimportraw_addressline1
	, dim_b2kdemogimportraw_addressline2
	, dim_b2kdemogimportraw_addressline3
	, dim_b2kdemogimportraw_addressline4
	, dim_b2kdemogimportraw_city
	, dim_b2kdemogimportraw_state
	, dim_b2kdemogimportraw_country
	, dim_b2kdemogimportraw_zipcode
	, dim_b2kdemogimportraw_homephonenumber
	, dim_b2kdemogimportraw_workphonenumber
	, dim_b2kdemogimportraw_workphoneextension
	, dim_b2kdemogimportraw_emailaddress
	, dim_b2kdemogimportraw_scprogramstatus
	, dim_b2kdemogimportraw_scprogramstatuschangedate
	, dim_b2kdemogimportraw_hostaccountstatus
	, dim_b2kdemogimportraw_hostacctstatusdate
	, dim_b2kdemogimportraw_scparticipationflag
	, dim_b2kdemogimportraw_primarysocialsecuritynumber
	, dim_b2kdemogimportraw_secondarysocialsecuritynumber
	, dim_b2kdemogimportraw_primarygender
	, dim_b2kdemogimportraw_secondarygender
	, dim_b2kdemogimportraw_birthdate
	, dim_b2kdemogimportraw_accountopendate
	, dim_b2kdemogimportraw_languagepreference
	, dim_b2kdemogimportraw_effectivedate
	, dim_b2kdemogimportraw_purchaserate
	, dim_b2kdemogimportraw_newcardnumber
	, dim_b2kdemogimportraw_associationid
	, dim_b2kdemogimportraw_corpid
	, dim_b2kdemogimportraw_agentinstitutionid
	, dim_b2kdemogimportraw_billcode
	, dim_b2kdemogimportraw_binplan
	, dim_b2kdemogimportraw_scenrollmentdate
	, dim_b2kdemogimportraw_laststatementdate
	, dim_b2kdemogimportraw_transferdate
	, dim_b2kdemogimportraw_filler1
	, dim_b2kdemogimportraw_primaryhouseholdcardnumber
	, dim_b2kdemogimportraw_filler2
	, dim_b2kdemogimportraw_householdcardtype
	, dim_b2kdemogimportraw_institutionid
	, dim_b2kdemogimportraw_vipindicator
	, dim_b2kdemogimportraw_filler3
FROM
	b2kDemogimportraw

GO


