USE [MetavanteWork]
GO

IF OBJECT_ID(N'vw_B2KBinMapping') IS NOT NULL
	DROP VIEW vw_B2KBinMapping
GO

/****** Object:  View [dbo].[vw_B2KBinMapping] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW vw_B2KBinMapping
AS
	SELECT Bin, ProductLine, SubProductType, TipFirst
	FROM CRTipFirst_Table
	WHERE (Bin + ProductLine + SubProductType + Tipfirst) IS NOT NULL
	GROUP BY Bin, ProductLine, SubProductType, Tipfirst

GO


