USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_UpdateCustClosed]    Script Date: 01/26/2012 14:10:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_UpdateCustClosed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_UpdateCustClosed]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_UpdateCustClosed]    Script Date: 01/26/2012 14:10:54 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_FIS_UpdateCustClosed] 
	@TipFirst varchar(3), @Enddate varchar(10)

AS

DECLARE	@SQL nvarchar(max), 
		@DateClosed datetime, 
		@DateToDelete nchar(6), 
		@newmonth nchar(2), 
		@newyear nchar(4), 
		@Count smallint

SET	@dateclosed = @Enddate

SET	@Count = (SELECT  ClosedMonths from [Rewardsnow].[dbo].[dbprocessinfo] where DBNumber=@TipFirst) 
SET	@newmonth = cast(datepart(month,(DATEADD(mm, @Count, @dateclosed))) as char(2)) 
SET	@newyear = cast(datepart(yyyy,(DATEADD(mm, @Count, @dateclosed))) as char(4)) 
IF	CONVERT(int, @newmonth) < '10' 
	BEGIN
		SET	@newmonth = '0' + left(@newmonth,1)
	END
SET	@DateToDelete = @newmonth + @newyear
/* Get rid of records that now have an active status */
SET	@SQL = N'
	DELETE FROM	[<<DBNAME>>].dbo.Customer_Closed
 	WHERE		tipnumber in (SELECT tipnumber FROM [<<DBNAME>>].dbo.customer WHERE	status = ''A'')
 	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL
/* Insert records that have a closed status and are not in the Customer_Closed table */
SET	@SQL = N'
	INSERT INTO	[<<DBNAME>>].dbo.Customer_Closed (Tipnumber, DateClosed, DateToDelete)
 	SELECT	Tipnumber, ''<<DATECLOSED>>'', ''<<DATETODELETE>>'' 
 	FROM	[<<DBNAME>>].dbo.customer 
	WHERE	status = ''C'' 
		and	tipnumber not in (SELECT DISTINCT tipnumber FROM [<<DBNAME>>].dbo.Customer_Closed)
	'  
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
SET		@SQL =	REPLACE(@SQL, '<<DATECLOSED>>', @DateClosed)	
SET		@SQL =	REPLACE(@SQL, '<<DATETODELETE>>', @DateToDelete)	
EXEC	sp_executesql @SQL
GO


