USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pSetWelcomeKitFileNameE]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileNameE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetWelcomeKitFileNameE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileNameE]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pSetWelcomeKitFileNameE] 
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output, 
	@ConnectionString nchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
declare @DBName varchar(100), @NumRecs numeric, @SQLSelect nvarchar(1000)
set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

set @SQLSelect=''set @NumRecs=(select count(*) from '' + QuoteName(@DBName) + N''.dbo.Welcomekit) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 

set @currentdate=(select top 1 datein from DateforAudit) 
set @currentdate=REPLACE(@currentdate,''/'',''_'') 


set @filename=''W'' + @TipPrefix + ''_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''
 
set @newname=''O:\5xx\Output\WelcomeKits\WelcomeE.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\WelcomeKits\'' + @filename
END
' 
END
GO
