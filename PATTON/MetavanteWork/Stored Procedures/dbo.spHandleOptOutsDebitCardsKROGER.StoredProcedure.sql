USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHandleOptOutsDebitCardsKROGER]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsDebitCardsKROGER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHandleOptOutsDebitCardsKROGER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsDebitCardsKROGER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/**********************************************************************/
/*  BY S.Blanchette                                                   */
/*  Date 9/2008                                                       */
/*  SCAN SEB001                                                       */
/*  Reason  To add logic to check DDA for opt out.                    */
/*********************************************************************/ 
CREATE PROCEDURE [dbo].[spHandleOptOutsDebitCardsKROGER] 
	-- Add the parameters for the stored procedure here
	@TipPrefix char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

/* Remove DebitCardInKroger for Opt Out people  */
delete from  debitcardinKroger
where exists(select * from optoutcontrol where acctnumber= debitcardinKroger.[Primary Acct Number])

/****************************/
/* START SEB001             */
/****************************/
delete from  debitcardinKroger
where exists(select * from DDAOptOutControl where DDANUMBER= debitcardinKroger.[DDA Acct Number] and left(tipnumber,3)=@TipPrefix )
END
' 
END
GO
