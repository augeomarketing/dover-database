USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerGet3pt]    Script Date: 12/06/2010 14:31:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGet3pt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerGet3pt]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerGet3pt]    Script Date: 12/06/2010 14:31:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S Blanchette
-- Create date: 11/09
-- Description:	Kroeger Get 3 point file
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerGet3pt]
	-- Add the parameters for the stored procedure here
	@Errorcount int output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @error int
	
	Truncate table dbo.Kroeger3pt
	
	insert into dbo.Kroeger3pt	(LoyaltyNumber, Amount, Sign, DateIn)
	select substring(col001,3,16), cast(substring(Col001,20,14) as float) , substring(Col001,34,1), substring(Col001,35,7) 
	from dbo.KroegerGenericIn
	where left(col001,2)='02'
	
	set @Errorcount=(select substring(col001,15,10) from dbo.KroegerGenericIn where left(col001,2)='09') - 2 - (select count(*) from dbo.Kroeger3pt)

END

GO


