USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisSetAuditFileName]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisSetAuditFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisSetAuditFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisSetAuditFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spHarrisSetAuditFileName]  @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename=''M'' + @TipPrefix + @currentdate + ''.xls''
 
set @newname=''O:\5xx\Output\AuditFiles\AuditRev01.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\AuditFiles\'' + @filename' 
END
GO
