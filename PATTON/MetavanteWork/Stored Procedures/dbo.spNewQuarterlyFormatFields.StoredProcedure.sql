USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewQuarterlyFormatFields]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyFormatFields]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewQuarterlyFormatFields]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyFormatFields]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 7/09
-- Description:	Zero fill the fields
-- =============================================
CREATE PROCEDURE [dbo].[spNewQuarterlyFormatFields]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table dbo.New_Quarterly_Statement_Detail_Out

	insert into dbo.New_Quarterly_Statement_Detail_Out
	select * from dbo.New_Quarterly_Statement_Detail
	
	update dbo.New_Quarterly_Statement_Detail_Out
	set pointsbegin = REPLICATE(''0'', (9-len(pointsbegin))) + pointsbegin
	
	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsEnd = REPLICATE(''0'', (9-len(PointsEnd))) + PointsEnd

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedCR = REPLICATE(''0'', (9-len(PointsPurchasedCR))) + PointsPurchasedCR

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedDB = REPLICATE(''0'', (9-len(PointsPurchasedDB))) + PointsPurchasedDB

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedHE = REPLICATE(''0'', (9-len(PointsPurchasedHE))) + PointsPurchasedHE

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedBUS = REPLICATE(''0'', (9-len(PointsPurchasedBUS))) + PointsPurchasedBUS

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusCR = REPLICATE(''0'', (9-len(PointsBonusCR))) + PointsBonusCR

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusDB = REPLICATE(''0'', (9-len(PointsBonusDB))) + PointsBonusDB

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusHE = REPLICATE(''0'', (9-len(PointsBonusHE))) + PointsBonusHE

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusBUS = REPLICATE(''0'', (9-len(PointsBonusBUS))) + PointsBonusBUS

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusEmpCR = REPLICATE(''0'', (9-len(PointsBonusEmpCR))) + PointsBonusEmpCR

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusEmpDB = REPLICATE(''0'', (9-len(PointsBonusEmpDB))) + PointsBonusEmpDB

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonus = REPLICATE(''0'', (9-len(PointsBonus))) + PointsBonus

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsAdded = REPLICATE(''0'', (9-len(PointsAdded))) + PointsAdded

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsIncreased = REPLICATE(''0'', (9-len(PointsIncreased))) + PointsIncreased

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsRedeemed = REPLICATE(''0'', (9-len(PointsRedeemed))) + PointsRedeemed

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedCR = REPLICATE(''0'', (9-len(PointsReturnedCR))) + PointsReturnedCR

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedDB = REPLICATE(''0'', (9-len(PointsReturnedDB))) + PointsReturnedDB

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedHE = REPLICATE(''0'', (9-len(PointsReturnedHE))) + PointsReturnedHE

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedBUS = REPLICATE(''0'', (9-len(PointsReturnedBUS))) + PointsReturnedBUS

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsSubtracted = REPLICATE(''0'', (9-len(PointsSubtracted))) + PointsSubtracted

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsDecreased = REPLICATE(''0'', (9-len(PointsDecreased))) + PointsDecreased

	update dbo.New_Quarterly_Statement_Detail_Out
	set PNTDEBIT = REPLICATE(''0'', (9-len(PNTDEBIT))) + PNTDEBIT

	update dbo.New_Quarterly_Statement_Detail_Out
	set PNTMORT = REPLICATE(''0'', (9-len(PNTMORT))) + PNTMORT

	update dbo.New_Quarterly_Statement_Detail_Out
	set PNTHOME = REPLICATE(''0'', (9-len(PNTHOME))) + PNTHOME

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsToExpire = REPLICATE(''0'', (9-len(PointsToExpire))) + PointsToExpire

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusP2UPlus = REPLICATE(''0'', (9-len(PointsBonusP2UPlus))) + PointsBonusP2UPlus

	
	update dbo.New_Quarterly_Statement_Detail_Out
	set pointsbegin = case
						when charindex(''-'', pointsbegin) > 0 then ''-'' +  right( replace(pointsbegin, ''-'', ''0''), len(pointsbegin) - 1)
						else ''+'' + right( replace(pointsbegin, ''-'', ''0''), len(pointsbegin) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsEnd = case
						when charindex(''-'', PointsEnd) > 0 then ''-'' +  right( replace(PointsEnd, ''-'', ''0''), len(PointsEnd) - 1)
						else ''+'' + right( replace(PointsEnd, ''-'', ''0''), len(PointsEnd) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedCR = case
						when charindex(''-'', PointsPurchasedCR) > 0 then ''-'' +  right( replace(PointsPurchasedCR, ''-'', ''0''), len(PointsPurchasedCR) - 1)
						else ''+'' + right( replace(PointsPurchasedCR, ''-'', ''0''), len(PointsPurchasedCR) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedDB = case
						when charindex(''-'', PointsPurchasedDB) > 0 then ''-'' +  right( replace(PointsPurchasedDB, ''-'', ''0''), len(PointsPurchasedDB) - 1)
						else ''+'' + right( replace(PointsPurchasedDB, ''-'', ''0''), len(PointsPurchasedDB) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedHE = case
						when charindex(''-'', PointsPurchasedHE) > 0 then ''-'' +  right( replace(PointsPurchasedHE, ''-'', ''0''), len(PointsPurchasedHE) - 1)
						else ''+'' + right( replace(PointsPurchasedHE, ''-'', ''0''), len(PointsPurchasedHE) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsPurchasedBUS = case
						when charindex(''-'', PointsPurchasedBUS) > 0 then ''-'' +  right( replace(PointsPurchasedBUS, ''-'', ''0''), len(PointsPurchasedBUS) - 1)
						else ''+'' + right( replace(PointsPurchasedBUS, ''-'', ''0''), len(PointsPurchasedBUS) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusCR = case
						when charindex(''-'', PointsBonusCR) > 0 then ''-'' +  right( replace(PointsBonusCR, ''-'', ''0''), len(PointsBonusCR) - 1)
						else ''+'' + right( replace(PointsBonusCR, ''-'', ''0''), len(PointsBonusCR) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusDB = case
						when charindex(''-'', PointsBonusDB) > 0 then ''-'' +  right( replace(PointsBonusDB, ''-'', ''0''), len(PointsBonusDB) - 1)
						else ''+'' + right( replace(PointsBonusDB, ''-'', ''0''), len(PointsBonusDB) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusHE = case
						when charindex(''-'', PointsBonusHE) > 0 then ''-'' +  right( replace(PointsBonusHE, ''-'', ''0''), len(PointsBonusHE) - 1)
						else ''+'' + right( replace(PointsBonusHE, ''-'', ''0''), len(PointsBonusHE) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusBUS = case
						when charindex(''-'', PointsBonusBUS) > 0 then ''-'' +  right( replace(PointsBonusBUS, ''-'', ''0''), len(PointsBonusBUS) - 1)
						else ''+'' + right( replace(PointsBonusBUS, ''-'', ''0''), len(PointsBonusBUS) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusEmpCR = case
						when charindex(''-'', PointsBonusEmpCR) > 0 then ''-'' +  right( replace(PointsBonusEmpCR, ''-'', ''0''), len(PointsBonusEmpCR) - 1)
						else ''+'' + right( replace(PointsBonusEmpCR, ''-'', ''0''), len(PointsBonusEmpCR) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusEmpDB = case
						when charindex(''-'', PointsBonusEmpDB) > 0 then ''-'' +  right( replace(PointsBonusEmpDB, ''-'', ''0''), len(PointsBonusEmpDB) - 1)
						else ''+'' + right( replace(PointsBonusEmpDB, ''-'', ''0''), len(PointsBonusEmpDB) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonus = case
						when charindex(''-'', PointsBonus) > 0 then ''-'' +  right( replace(PointsBonus, ''-'', ''0''), len(PointsBonus) - 1)
						else ''+'' + right( replace(PointsBonus, ''-'', ''0''), len(PointsBonus) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsAdded = case
						when charindex(''-'', PointsAdded) > 0 then ''-'' +  right( replace(PointsAdded, ''-'', ''0''), len(PointsAdded) - 1)
						else ''+'' + right( replace(PointsAdded, ''-'', ''0''), len(PointsAdded) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsIncreased = case
						when charindex(''-'', PointsIncreased) > 0 then ''-'' +  right( replace(PointsIncreased, ''-'', ''0''), len(PointsIncreased) - 1)
						else ''+'' + right( replace(PointsIncreased, ''-'', ''0''), len(PointsIncreased) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsRedeemed = case
						when charindex(''-'', PointsRedeemed) > 0 then ''-'' +  right( replace(PointsRedeemed, ''-'', ''0''), len(PointsRedeemed) - 1)
						else ''+'' + right( replace(PointsRedeemed, ''-'', ''0''), len(PointsRedeemed) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedCR = case
						when charindex(''-'', PointsReturnedCR) > 0 then ''-'' +  right( replace(PointsReturnedCR, ''-'', ''0''), len(PointsReturnedCR) - 1)
						else ''+'' + right( replace(PointsReturnedCR, ''-'', ''0''), len(PointsReturnedCR) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedDB = case
						when charindex(''-'', PointsReturnedDB) > 0 then ''-'' +  right( replace(PointsReturnedDB, ''-'', ''0''), len(PointsReturnedDB) - 1)
						else ''+'' + right( replace(PointsReturnedDB, ''-'', ''0''), len(PointsReturnedDB) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedHE = case
						when charindex(''-'', PointsReturnedHE) > 0 then ''-'' +  right( replace(PointsReturnedHE, ''-'', ''0''), len(PointsReturnedHE) - 1)
						else ''+'' + right( replace(PointsReturnedHE, ''-'', ''0''), len(PointsReturnedHE) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsReturnedBUS = case
						when charindex(''-'', PointsReturnedBUS) > 0 then ''-'' +  right( replace(PointsReturnedBUS, ''-'', ''0''), len(PointsReturnedBUS) - 1)
						else ''+'' + right( replace(PointsReturnedBUS, ''-'', ''0''), len(PointsReturnedBUS) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsSubtracted = case
						when charindex(''-'', PointsSubtracted) > 0 then ''-'' +  right( replace(PointsSubtracted, ''-'', ''0''), len(PointsSubtracted) - 1)
						else ''+'' + right( replace(PointsSubtracted, ''-'', ''0''), len(PointsSubtracted) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsDecreased = case
						when charindex(''-'', PointsDecreased) > 0 then ''-'' +  right( replace(PointsDecreased, ''-'', ''0''), len(PointsDecreased) - 1)
						else ''+'' + right( replace(PointsDecreased, ''-'', ''0''), len(PointsDecreased) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PNTDEBIT = case
						when charindex(''-'', PNTDEBIT) > 0 then ''-'' +  right( replace(PNTDEBIT, ''-'', ''0''), len(PNTDEBIT) - 1)
						else ''+'' + right( replace(PNTDEBIT, ''-'', ''0''), len(PNTDEBIT) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PNTMORT = case
						when charindex(''-'', PNTMORT) > 0 then ''-'' +  right( replace(PNTMORT, ''-'', ''0''), len(PNTMORT) - 1)
						else ''+'' + right( replace(PNTMORT, ''-'', ''0''), len(PNTMORT) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PNTHOME = case
						when charindex(''-'', PNTHOME) > 0 then ''-'' +  right( replace(PNTHOME, ''-'', ''0''), len(PNTHOME) - 1)
						else ''+'' + right( replace(PNTHOME, ''-'', ''0''), len(PNTHOME) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsToExpire = case
						when charindex(''-'', PointsToExpire) > 0 then ''-'' +  right( replace(PointsToExpire, ''-'', ''0''), len(PointsToExpire) - 1)
						else ''+'' + right( replace(PointsToExpire, ''-'', ''0''), len(PointsToExpire) - 1)
					  end 

	update dbo.New_Quarterly_Statement_Detail_Out
	set PointsBonusP2UPlus = case
						when charindex(''-'', PointsBonusP2UPlus) > 0 then ''-'' +  right( replace(PointsBonusP2UPlus, ''-'', ''0''), len(PointsBonusP2UPlus) - 1)
						else ''+'' + right( replace(PointsBonusP2UPlus, ''-'', ''0''), len(PointsBonusP2UPlus) - 1)
					  end 

END
' 
END
GO
