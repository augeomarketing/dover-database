USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kCleanupTranRawPostImport]    Script Date: 11/15/2010 13:19:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kCleanupTranRawPostImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kCleanupTranRawPostImport]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kCleanupTranRawPostImport]    Script Date: 11/15/2010 13:19:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_b2kCleanupTranRawPostImport]
AS

UPDATE b2kTranImportRaw
SET
	dim_b2ktranimportraw_filedate = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_filedate)
	, dim_b2ktranimportraw_corpid = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_corpid)
	, dim_b2ktranimportraw_agentinstitutionid = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_agentinstitutionid)
	, dim_b2ktranimportraw_accountnumber = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_accountnumber)
	, dim_b2ktranimportraw_trandate = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_trandate)
	, dim_b2ktranimportraw_transactionpostdate = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_transactionpostdate)
	, dim_b2ktranimportraw_transactionsign = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_transactionsign)
	, dim_b2ktranimportraw_transactionamount = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_transactionamount)
	, dim_b2ktranimportraw_prescoredearnings = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_prescoredearnings)
	, dim_b2ktranimportraw_merchantname = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_merchantname)
	, dim_b2ktranimportraw_merchantcity = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_merchantcity)
	, dim_b2ktranimportraw_merchantstate = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_merchantstate)
	, dim_b2ktranimportraw_merchantcountry = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_merchantcountry)
	, dim_b2ktranimportraw_mccsiccode = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_mccsiccode)
	, dim_b2ktranimportraw_industrycode = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_industrycode)
	, dim_b2ktranimportraw_transactioncode = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_transactioncode)
	, dim_b2ktranimportraw_reasoncode = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_reasoncode)
	, dim_b2ktranimportraw_transactioncurrencyidentifier = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_transactioncurrencyidentifier)
	, dim_b2ktranimportraw_pricingplanid = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_pricingplanid)
	, dim_b2ktranimportraw_transactiontype = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_transactiontype)
	, dim_b2ktranimportraw_referencenumber = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_referencenumber)
	, dim_b2ktranimportraw_eciindicator = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_eciindicator)
	, dim_b2ktranimportraw_vendorindicator = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_vendorindicator)
	, dim_b2ktranimportraw_filler1 = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_filler1)
	, dim_b2ktranimportraw_merchantid = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_merchantid)
	, dim_b2ktranimportraw_filler2 = RewardsNow.dbo.ufn_Trim(dim_b2ktranimportraw_filler2)


GO


