USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteCombineAffiliatTables]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteCombineAffiliatTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteCombineAffiliatTables]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteCombineAffiliatTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spMetavanteCombineAffiliatTables]
as

delete from affiliat

insert into affiliat
select * from [500AmericanTrust].dbo.affiliat


insert into affiliat
select * from [501IllinoisNational].dbo.affiliat


insert into affiliat
select * from [502LaytonBank].dbo.affiliat


insert into affiliat
select * from [503Marin].dbo.affiliat


insert into affiliat
select * from [504HudsonConsumer].dbo.affiliat


insert into affiliat
select * from [505HudsonCommercial].dbo.affiliat


insert into affiliat
select * from [506RabobankConsumer].dbo.affiliat


insert into affiliat
select * from [507RabobankCommercial].dbo.affiliat


insert into affiliat
select * from [508WoodtrustConsumer].dbo.affiliat

insert into affiliat
select * from [509WoodtrustCommercial].dbo.affiliat


insert into affiliat
select * from [510MarinCommercial].dbo.affiliat


insert into affiliat
select * from [511NEBATConsumer].dbo.affiliat


insert into affiliat
select * from [512NEBATCommercial].dbo.affiliat


insert into affiliat
select * from [513LaytonCommercial].dbo.affiliat


insert into affiliat
select * from [515LandmarkConsumer].dbo.affiliat' 
END
GO
