USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kScrubDemographicsRaw]    Script Date: 12/13/2010 14:12:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kScrubDemographicsRaw]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kScrubDemographicsRaw]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kScrubDemographicsRaw]    Script Date: 12/13/2010 14:12:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_b2kScrubDemographicsRaw]
AS
	--REMOVE DEMOGRAPHIC RECORDS NOT IN PROGRAM
	DELETE dmgr
	FROM b2kDemogImportRaw dmgr
	INNER JOIN vw_CRTipFirstTable_BankAgentByBin bab
	ON LEFT(dmgr.dim_b2kdemogimportraw_binplan, 6) = bab.bin
	LEFT OUTER JOIN vw_CRTipFirstTable_TipFirstByProdLineSubType tfprd
	ON
		bab.tipfirst = tfprd.Tipfirst
		AND dmgr.dim_b2kdemogimportraw_productline = tfprd.ProductLine
		AND dmgr.dim_b2kdemogimportraw_subproducttype = tfprd.SubProductType
	WHERE
		tfprd.Tipfirst IS NULL


GO


