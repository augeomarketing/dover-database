USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateForPointsRedeemed]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateForPointsRedeemed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateForPointsRedeemed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateForPointsRedeemed]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateForPointsRedeemed]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Customer 
	set AvailableBal = AvailableBal - 
 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber AND 
	       OnlHistory.CopyFlag is null
	 Group by Tipnumber  ), 

	Redeemed = Redeemed + 
 	(Select Sum (OnlHistory.Points * OnlHistory.CatalogQty) 
	 From  OnlHistory 
	 Where OnlHistory.tipnumber = Customer.Tipnumber AND 
	       OnlHistory.CopyFlag is null
	 Group by Tipnumber  )

where tipnumber in (select tipnumber from onlhistory where CopyFlag is Null) and left(tipnumber,3)=@TipFirst
END
' 
END
GO
