USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcessAffinity]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessAffinity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcessAffinity]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessAffinity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spBonusFileProcessAffinity] @DateIn varchar(10), @TipFirst char(3)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 06/2009   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* add code to remove transaction records if tipnumber not in customer  */

Declare @DBName varchar(100), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, CRDHLR_NBR, points	
from [BonusTip&Points]
where left(tipnumber,3) = @TipFirst

update transumbonus
set ratio=''1'', trancode=''FA'', description=''Merchant Reward'', histdate=@datein, numdebit=''1'', overage=''0''

/*********************************************************************/
/*  start SEB001 Remove records that do not have a customer record   */
/*********************************************************************/
set @SQLDelete=''Delete from dbo.transumbonus
				where tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.customer) ''
Exec sp_executesql @SQLDelete
/*********************************************************************/
/*  end SEB001 Remove records that do not have a customer record   */
/*********************************************************************/

set @sqlInsert=N''insert into '' + QuoteName(@DBName) + N'' .dbo.history 
		select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '''' '''', ratio, overage
		from transumbonus ''
exec sp_executesql @SQLInsert

set @sqlUpdate=N''update '' + QuoteName(@DBName) + N'' .dbo.customer
		set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber= '' + QuoteName(@DBName) + N'' .dbo.customer.tipnumber), 
			runbalance= runbalance + (select sum(amtdebit) from transumbonus where tipnumber= '' + QuoteName(@DBName) + N'' .dbo.customer.tipnumber)
		where exists(select tipnumber from transumbonus where tipnumber= '' + QuoteName(@DBName) + N'' .dbo.customer.tipnumber) ''
exec sp_executesql @sqlUpdate' 
END
GO
