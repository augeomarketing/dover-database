USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pGetExistingTipNumbers113006]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetExistingTipNumbers113006]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetExistingTipNumbers113006]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetExistingTipNumbers113006]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGetExistingTipNumbers113006] 
AS


/*  Get tipnumber based on account number  RETAIL */
update cust11
set tipnumber=affiliat.tipnumber
from cust11, affiliat
where (cust11.Tipnumber is null or len(rtrim(cust11.tipnumber)) = 0) and cust11.acctnum is not null and left(cust11.acctnum,1) not in(''0'', '' '', ''9'') and cust11.acctnum=affiliat.acctid 

/*  Get tipnumber based on old account number  RETAIL */
update cust11
set tipnumber=affiliat.tipnumber
from cust11, affiliat
where (cust11.Tipnumber is null or len(rtrim(cust11.tipnumber)) = 0) and cust11.oldcc is not null and left(cust11.oldcc,1) not in(''0'', '' '', ''9'') and cust11.oldcc=affiliat.acctid 

/*  Get tipnumber based on ddanumber  RETAIL   */
update cust11
set tipnumber=affiliat.tipnumber
from cust11, affiliat
where (cust11.Tipnumber is null or len(rtrim(cust11.tipnumber)) = 0) and cust11.ddanum is not null and left(cust11.ddanum,11) not in(''00000000000'') and cust11.ddanum=affiliat.custid and cust11.tipfirst=left(affiliat.tipnumber,3)

/*  Get tipnumber based on ssn na1 na2 joint are the same  RETAIL     */
drop table wrktab1

select distinct ssn, na1, na2, joint, tipnumber 
into wrktab1
from cust11
where (ssn is not null and left(ssn,3) not in (''000'', ''   '', ''999'')) and cust11.tipnumber is not null and not exists(select * from CorporatePrefixes where TipPrefix=TipFirst)

update cust11
set tipnumber=wrktab1.tipnumber
from cust11, wrktab1
where (cust11.ssn is not null and left(cust11.ssn,1) not in (''000'', ''   '', ''999'')) and cust11.tipnumber is null and cust11.ssn=wrktab1.ssn and (cust11.na1=wrktab1.na1 and cust11.na2=wrktab1.na2) and cust11.joint=wrktab1.joint 

/* Removed 11/20/2006  Per Bryan at MV if a card is being closed because of lost or stolen must add points from closed and new records to reflect the spend */
/* delete from cardsin
where tipnumber is null and status<>''A'' */' 
END
GO
