USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisEnrollErrorMultiples]    Script Date: 01/11/2010 17:14:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisEnrollErrorMultiples]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisEnrollErrorMultiples]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisEnrollErrorMultiples]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 3/09
-- Description:	To extract errors for multiples and feed to Metavante
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisEnrollErrorMultiples] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @trackcount int

	set @trackcount = (select max(sid_selfenrollerror_id) from [rn1].metavante.dbo.selfenrollerror)

	truncate table metavantework.dbo.selfenrollerrorMultiple

	insert into metavantework.dbo.selfenrollerrorMultiple
	select *
	from [rn1].metavante.dbo.selfenrollerror
	where sid_selfenrollerror_id <= @trackcount and dim_selfenrollerror_found <> ''0''

	delete from [rn1].metavante.dbo.selfenrollerror
	where sid_selfenrollerror_id <= @trackcount and dim_selfenrollerror_found <> ''0''

END
' 
END
GO
