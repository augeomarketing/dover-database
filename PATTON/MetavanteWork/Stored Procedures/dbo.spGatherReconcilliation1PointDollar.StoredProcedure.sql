USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation1PointDollar]    Script Date: 08/02/2011 08:41:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation1PointDollar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGatherReconcilliation1PointDollar]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation1PointDollar]    Script Date: 08/02/2011 08:41:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		S Blanchette
-- Create date: 12/09
-- Description:	Gather reconcilliation Numbers
-- =============================================
CREATE PROCEDURE [dbo].[spGatherReconcilliation1PointDollar] 
	-- Add the parameters for the stored procedure here
	@DateIn char(10), @dbname nvarchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @sql nvarchar(max)
	
	Set @sql = '
    if '''+@DateIn+''' in (select ProcessDate from ['+@dbname+'].dbo.KroegerReconcilliationData)
			update ['+@dbname+'].dbo.KroegerReconcilliationData
			set ValueInPoint1 = ISNULL(ValueInPoint1, 0) +	(
			ISNULL((select sum(cast((TransactionAmount) as int)) from dbo.Kroger_Daily_Card_Transaction_Detail 
				where DebitCreditIndicator = ''D''), 0)
			-
			ISNULL((select sum(cast((TransactionAmount) as int)) from dbo.Kroger_Daily_Card_Transaction_Detail 
				where DebitCreditIndicator = ''C''), 0)		)  
	where '''+@DateIn+''' in (select ProcessDate from ['+@dbname+'].dbo.KroegerReconcilliationData)
	else
			insert into ['+@dbname+'].dbo.KroegerReconcilliationData (ProcessDate, ValueInPoint1)
			select '''+@DateIn+''', (ISNULL((select sum(cast((TransactionAmount) as int)) from dbo.Kroger_Daily_Card_Transaction_Detail 
							where DebitCreditIndicator = ''D''), 0)
							-
							ISNULL((select sum(cast((TransactionAmount) as int)) from dbo.Kroger_Daily_Card_Transaction_Detail 
							where DebitCreditIndicator = ''C''), 0))

	update ['+@dbname+'].dbo.KroegerReconcilliationData
	set ValueInPoint2 = ISNULL(ValueInPoint2, 0) + (
			ISNULL((select sum(cast((TransactionAmount) as int)) from dbo.Kroger_Daily_Card_Transaction_Detail 
				where DebitCreditIndicator = ''D'' and LEFT(RTRIM(LTRIM(Filler)),1) = ''K''), 0)
			-
			ISNULL((select sum(cast((TransactionAmount) as int)) from dbo.Kroger_Daily_Card_Transaction_Detail 
				where DebitCreditIndicator = ''C'' and LEFT(RTRIM(LTRIM(Filler)),1) = ''K''), 0)
														)  
	where '''+@DateIn+''' in (select ProcessDate from ['+@dbname+'].dbo.KroegerReconcilliationData)
	'

	Exec sp_executesql @sql



END



GO


