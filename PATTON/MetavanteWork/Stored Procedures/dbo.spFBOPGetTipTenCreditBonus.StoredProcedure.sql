USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGetTipTenCreditBonus]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipTenCreditBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGetTipTenCreditBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipTenCreditBonus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spFBOPGetTipTenCreditBonus] @TipFirst char(3), @Bank char(3)
as
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 01/2008   */
/* REVISION: 0 */
/* SCAN:  */
/* Script to get tipnumbers from affiliat table by either card# or ssn  */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateToDelete char(6), @dateclosed datetime, @newmonth nchar(2), @newyear nchar(4)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLUpdate=N''update  TenCreditBonus
		set tipnumber=b.tipnumber 
		from TenCreditBonus a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and AcctNumber is not null and len(AcctNumber)>''''1'''' and AcctNumber<>''''000000000'''' and AcctNumber=acctid ''
exec sp_executesql @SQLUpdate' 
END
GO
