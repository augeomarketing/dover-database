USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteHELOCProcess1]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteHELOCProcess1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteHELOCProcess1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteHELOCProcess1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    This Stored Procedure Process Heloc cards                                */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 0 */
--

CREATE Procedure [dbo].[spMetavanteHELOCProcess1] 
as

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
-- Changed logic to concatenate overflow address to primary address
--
/*********  Begin SEB002  *************************/
update HELOCcardin
set addr1=rtrim(addr1) + rtrim(Addr1Ext), addr2=rtrim(addr2) + rtrim(Addr2Ext)
/*********  End SEB002  *************************/

/*           Convert Trancode                                                     */
update HELOCCardIn
set Trancode=''FN'' 
where trancode=''001''

update HELOCCardIn
set Trancode=''FR'' 
where trancode=''004''

update HELOCCardin	
set numpurch=''1'', purch=points
where trancode=''FN''

update HELOCCardin	
set numret=''1'', amtret=points
where trancode=''FR''

--update HELOCCardin
--set ddanum=savnum
--where ddanum=''00000000000''

delete from HELOCCardin
where ssn=''987654321''

update HELOCCardin
set status=''C''
where status=''B''

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/
update HELOCCardin
set TIPMID=''0000000''


/* rearrange name from  Last, First to First Last */
update HELOCCardin
set NA1=rtrim(substring(na1,charindex('','',na1)+2,len(rtrim(na1)))) + '' '' + substring(na1,1,charindex('','',na1)-1)
where substring(na1,1,1) not like '' '' and na1 is not null and NA1 LIKE ''%,%''
update HELOCCardin
set na1=ltrim(na1)

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Begin logic to roll up multiple records into one by Account number      */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/

DELETE FROM FBOPDebitRollUp
DELETE FROM FBOPDebitRollUpa

insert into FBOPDebitRollUp 
SELECT acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr, SigvsPin 
FROM HELOCCardin
GROUP BY acctnum, SigvsPin
ORDER BY acctnum, SigvsPin

/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */

truncate table FBOPAccountRollUp

insert into FBOPAccountRollUp 
select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS, SigvsPin
FROM HELOCCardin
ORDER BY acctnum


DELETE FROM HELOCCardin2

INSERT into HELOCCardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS, SigvsPin)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS, a.SigvsPin		        	
from FBOPAccountRollUp a, FBOPDebitRollUp b
where a.acctnum=b.acctnum and a.SigvsPin=b.SigvsPin 

/*                                         	                */
/*  End logic to roll up multiple records into one by NA2       */
/*                                                        	*/

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update HELOCCardin2
set joint=''J''
where NA2 is not null and substring(na2,1,1) not like '' ''

update HELOCCardin2
set joint=''S''
where NA2 is null or substring(na2,1,1) like '' ''' 
END
GO
