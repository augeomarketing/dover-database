USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spRetreiveAutoProcessDetail]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRetreiveAutoProcessDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRetreiveAutoProcessDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRetreiveAutoProcessDetail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spRetreiveAutoProcessDetail] 
	-- Add the parameters for the stored procedure here
	@TipFirstIn char(3),
	@TipFirst			varchar (3)				output,
	@DBName1			varchar (100)			output,
	@DBName2			varchar(100)			output,
	@SourceFileString	varchar(100)			output,
	@BackupNameString	varchar(100)			output,
	@MonthBegDate		varchar(10)				output,
	@MonthEndDate		varchar(10)				output,
	@RNClientCode		varchar(100)			output,
	@MissingNameOut	varchar (200)			output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Insert statements for procedure here

		set @TipFirst = @TipFirstIn
		set @DBName1 = (select rtrim(DBName1) from autoprocessdetail where tipfirst=@TipFirstIn)
		set @DBName2 = (select rtrim(DBName2) from autoprocessdetail where tipfirst=@TipFirstIn)
		set @SourceFileString = (select rtrim(SourceFileNameString) from autoprocessdetail where tipfirst=@TipFirstIn)
		set @BackupNameString= (select rtrim(BackupNameString) from autoprocessdetail where tipfirst=@TipFirstIn)
		set @MonthBegDate = (select MonthBeginingDate from autoprocessdetail where tipfirst=@TipFirstIn)
		set @MonthEndDate = (select MonthEndingDate from autoprocessdetail where tipfirst=@TipFirstIn)
		set @RNClientCode = (select rtrim(RNClientCode) from autoprocessdetail where tipfirst=@TipFirstIn)
		set @MissingNameOut = (select rtrim(MissingNameOut) from autoprocessdetail where tipfirst=@TipFirstIn)
END' 
END
GO
