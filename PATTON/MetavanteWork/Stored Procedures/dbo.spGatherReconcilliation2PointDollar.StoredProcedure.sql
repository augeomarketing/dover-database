USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation2PointDollar]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation2PointDollar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGatherReconcilliation2PointDollar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation2PointDollar]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--
-- Modified: 5/2010
-- By: S Blanchette
-- Reason:  To fix dealing with returns
-- =============================================
CREATE PROCEDURE [dbo].[spGatherReconcilliation2PointDollar]  
	-- Add the parameters for the stored procedure here
	@DateIn char(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

		update [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData
		set ValueInPoint2 = ISNULL(ValueInPoint2, 0) + (
														  ISNULL((select sum(cast((Amount) as int)) from dbo.Kroeger2PT where sign = ''+''), 0)
														  -
														  ISNULL((select sum(cast((Amount) as int)) from dbo.Kroeger2PT where sign = ''-''), 0)
														)  
		where @DateIn in (select ProcessDate from [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData)
	
END
' 
END
GO
