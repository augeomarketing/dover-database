USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spPrepareTransactionData]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepareTransactionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPrepareTransactionData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepareTransactionData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spPrepareTransactionData] 
AS 

truncate table transum
delete from cardsin where tipnumber is null

insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype)
select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype	
from MetavanteWork.dbo.cardsin
group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype


/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''63'', description=''Credit Card Purchase'', amtpurch=ROUND(AMTPURCH/100, 0)
where amtpurch>0 and cardtype=''C''

/*********************************************************************/
/*  Process Credit Cards Returns                                     */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''33'', description=''Credit Card Returns'', amtcr=ROUND(AMTcr/100, 0)
where amtcr>0 and cardtype=''C''

/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''67'', description=''Debit Card Purchase'', amtpurch=round(((AMTPURCH/100)/2),0) 
where amtpurch>0 and cardtype=''D''

/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''37'', description=''Debit Card Returns'', amtcr=round(((AMTCR/100)/2),0)
where amtcr>0 and cardtype=''D''' 
END
GO
