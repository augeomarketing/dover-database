USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spCheckBatchTotals]    Script Date: 12/09/2014 08:48:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCheckBatchTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCheckBatchTotals]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spCheckBatchTotals]    Script Date: 12/09/2014 08:48:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spCheckBatchTotals]
		
AS

BEGIN
	SET NOCOUNT ON;

	DECLARE	@Debitcount numeric(18,0)
		,	@Creditcount numeric(18,0)
		,	@Debitamount numeric (18,2)
		,	@Creditamount numeric (18,0)
		,	@BatchDebitcount numeric(18,0)
		,	@BatchCreditcount numeric(18,0)
		,	@BatchDebitamount numeric (18,2)
		,	@BatchCreditamount numeric (18,0)
	
	TRUNCATE TABLE	dbo.Kroger_Daily_Batch_Errors

	SET @Debitcount		= (select COUNT(*) from dbo.Kroger_Daily_Card_Transaction_Detail where DebitCreditIndicator='D')
	SET @Debitamount	= (select SUM(TransactionAmount) from dbo.Kroger_Daily_Card_Transaction_Detail where DebitCreditIndicator='D')
	SET @creditcount	= (select COUNT(*) from dbo.Kroger_Daily_Card_Transaction_Detail where DebitCreditIndicator='C')
	SET @Creditamount	= (select SUM(TransactionAmount) from dbo.Kroger_Daily_Card_Transaction_Detail where DebitCreditIndicator='C')

	SET @BatchDebitcount	= (select SUM(BatchDebitTransactionCount) from dbo.Kroger_Daily_Batch_Trailer)
	SET @BatchDebitAmount	= (select SUM(BatchDebitTransactionAmount)/100 from dbo.Kroger_Daily_Batch_Trailer)
	SET @BatchCreditcount	= (select SUM(BatchCreditTransactionCount) from dbo.Kroger_Daily_Batch_Trailer)
	SET @BatchCreditAmount	= (select SUM(BatchCreditTransactionAmount)/100 from dbo.Kroger_Daily_Batch_Trailer)

	IF	@Debitcount <> @BatchDebitcount 
	OR	@Debitamount <> @BatchDebitamount
	OR	@Creditcount <> @BatchCreditcount
	OR	@Creditamount <> @BatchCreditamount
		BEGIN
			INSERT INTO	dbo.Kroger_Daily_Batch_Errors
			VALUES ('1')
		END

END

GO


