USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerCreateCouponTrailer]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCreateCouponTrailer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerCreateCouponTrailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCreateCouponTrailer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 11/09
-- Description:	Coupon Trailer
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerCreateCouponTrailer] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		
	Truncate Table Kroeger_Coupon_Request_Trailer

	Declare @Reccount char(10), @rec char(10), @totpts char(10), @totalpoints char(10)
    
	set @Rec = (select count(*) from dbo.Kroeger_Coupon_Request_Detail)
	set @Reccount = REPLICATE(''0'', 9- LEN(@Rec) ) + @Rec
	
	set @totpts = (select sum(cast(PointsRedeemed as int)) from dbo.Kroeger_Coupon_Request_Detail)
	set @totalpoints = REPLICATE(''0'', 9- LEN(@totpts) ) + @totpts

	Insert into Kroeger_Coupon_Request_Trailer
	Values (''T'', @Reccount, @totalpoints)
END
' 
END
GO
