USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_TIPassignment]    Script Date: 05/08/2012 08:04:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_TIPassignment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_TIPassignment]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_TIPassignment]    Script Date: 05/08/2012 08:04:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[usp_FIS_TIPassignment] 
	@TipFirst varchar(3)
AS

/* Change statement to get top 1 because transactions are getting 2 different tips */
DECLARE	@DBName			varchar(50)		,
		@SQLUpdate		nvarchar(max)	,
		@LastTipUsed	varchar(15)		,
		@NewTip			varchar(15)		,
		@tiplast		varchar(12)		,
		@acct			varchar(16)		,
		@DDA			varchar(16)		,
		@TIP			varchar(15)		,
		@SSN			varchar(10)		,
		@NA1			varchar(40)		,
		@NA2			varchar(40)		,
		@JOINT			varchar(1)		,
		@DI_HH_id		int	= (Select DI_HH_id from Packagecodes where tipfirst = @tipfirst)


SET		@DBName		=	(SELECT rtrim(DBNamePatton) FROM RewardsNow.dbo.DBProcessInfo WHERE DBNumber = @TipFirst)


/*		***GENERATE AND ASSIGN EXISTING TIPNUMBERS***		*/
/*  Get tipnumber based on account number      */
SET	@SQLUpdate =	'UPDATE	ci 
					 SET	tipnumber = aff.tipnumber
					 FROM	cardsin ci JOIN [<<DBNAME>>].dbo.affiliat aff ON ci.acctnum = aff.acctid
					 WHERE	ci.tipnumber is null'
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', @DBName)					 
EXEC	sp_executesql @SQLUpdate

/*  Get tipnumber based on old account number  */
SET	@SQLUpdate =	'UPDATE	ci 
					 SET	tipnumber = aff.tipnumber
					 FROM	cardsin ci JOIN [<<DBNAME>>].dbo.affiliat aff ON ci.oldcc = aff.acctid
					 WHERE	ci.tipnumber is null'
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', @DBName)					 
EXEC	sp_executesql @SQLUpdate

/*  Get tipnumber based on existing DDA number  */
IF @DI_HH_id in (1,3)
	BEGIN
		SET	@SQLUpdate =	'UPDATE	ci
							 SET	TIPNUMBER = aff.tipnumber	
							 FROM	CARDSIN ci
									INNER JOIN
									(
										SELECT		custid, MIN(tipnumber) as tipnumber
										FROM		[<<DBNAME>>].dbo.AFFILIAT
										WHERE		rewardsnow.dbo.ufn_CharIsNullOrEmpty(replace(custid, ''0'', '''')) = 0
										GROUP BY	custid
									) aff
										ON	ci.DDANUM = aff.CustID
									LEFT OUTER JOIN CorporatePrefixes cp
										ON ci.tipfirst = cp.TipPrefix
							 WHERE	ci.TIPNUMBER is null and cp.TipPrefix is null'						
		SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', @DBName)					 
		EXEC sp_executesql @SQLUpdate
	END

/*  Get tipnumber based on ssn na1 na2 joint are the same      */
IF @DI_HH_id in (2,3)
	BEGIN
		UPDATE	cardsin
		SET		na2 = ' '
		WHERE	na2 is null or Len(na2)<'1'

		DROP TABLE	wrktab1n  --wrktab2

		SELECT DISTINCT	ssn, na1, na2, joint, tipnumber 
		INTO			wrktab1n --wrktab2
		FROM			cardsin
		WHERE			ssn is not null 
					and	ssn not like '00000%' 
					and	left(ssn,3)<> '999' 
					and tipnumber is not null 
					and not exists(SELECT * FROM CorporatePrefixes WHERE TipPrefix = TipFirst)

		UPDATE	cardsin
		SET		tipnumber = (SELECT TOP 1	tipnumber 
							 FROM			wrktab1n  --wrktab2 
							 WHERE			ssn = cardsin.ssn 
										and	((na1=cardsin.na1 and na2=cardsin.na2) or (na2=cardsin.na1 and na1=cardsin.na2))
							)
		WHERE	ssn is not null 
			and	left(ssn,3) not in ('000', '   ', '999') 
			and tipnumber is null 
			and exists(SELECT * FROM wrktab1n WHERE ssn = cardsin.ssn and ((na1=cardsin.na1 and na2=cardsin.na2) or (na2=cardsin.na1 and na1=cardsin.na2)) )
	END

DELETE FROM	cardsin
WHERE		tipnumber is null 
		and status <> 'A'

/*		***GENERATE AND ASSIGN NEW TIPNUMBERS***		*/
EXEC	rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
SELECT	@LastTipUsed as LastTipUsed

SET		@NewTip		=	@LastTipUsed
SET		@tiplast	=	Right(@LastTipUsed, 12)

UPDATE	cardsin		SET na2 = ' '	WHERE	na2 is null
UPDATE	cardsin		SET	na3 = ' '	WHERE	na3 is null
UPDATE	cardsin		SET	na4 = ' '	WHERE	na4 is null
UPDATE	cardsin		SET	na5 = ' '	WHERE	na5 is null
UPDATE	cardsin		SET	na6 = ' '	WHERE	na6 is null

--ASSIGN TIPNUMBERS TO ALL NEW CARDS WITH HOUSEHOLDING USING OLDCC VALUES
UPDATE	a
SET		@Tiplast = (@Tiplast + 1),
		TIPNUMBER = @tipfirst + RewardsNow.dbo.ufn_Pad(@Tiplast, 'L', 12, '0')
FROM	MetavanteWork.dbo.CARDSIN a JOIN MetavanteWork.dbo.CARDSIN b ON a.ACCTNUM = b.OLDCC
WHERE	a.TIPNUMBER is null
	and a.OLDCC is null
	and	a.TIPFIRST = @tipfirst
	
UPDATE	a
SET		TIPNUMBER = b.TIPNUMBER
FROM	MetavanteWork.dbo.CARDSIN a JOIN MetavanteWork.dbo.CARDSIN b ON a.OLDCC = b.ACCTNUM
WHERE	a.TIPNUMBER is null and b.TIPNUMBER is not null

WHILE (Select COUNT(*) from CARDSIN where TIPNUMBER is null) > 0
	BEGIN
		SET @acct	=	(Select top 1 ACCTNUM from CARDSIN where TIPNUMBER is null)
		SELECT	TOP 1	@DDA	=	DDANUM,
						@SSN	=	SSN,
						@NA1	=	NA1,
						@NA2	=	NA2,
						@JOINT	=	JOINT		
		FROM	CARDSIN
		WHERE	ACCTNUM = @acct
		SET @TIP	=	CASE
							WHEN @DI_HH_id = 0
								THEN	NULL
							WHEN @DI_HH_id = 1
								THEN	(Select MIN(TIPNUMBER) from CARDSIN 
										 where	DDANUM = @DDA and TIPNUMBER is not null)
							WHEN @DI_HH_id = 2
								THEN	(Select MIN(TIPNUMBER) from CARDSIN 
										 where	SSN = @SSN and NA1 = @NA1 and NA2 = @NA2 and JOINT = @JOINT and TIPNUMBER is not null)
							WHEN @DI_HH_id = 3
								THEN	(Select MIN(TIPNUMBER) from CARDSIN 
										 where	(DDANUM = @DDA and TIPNUMBER is not null)
											or	(SSN = @SSN and NA1 = @NA1 and NA2 = @NA2 and JOINT = @JOINT and TIPNUMBER is not null))
						END					
											
		UPDATE	CARDSIN
		SET		TIPNUMBER = @TIP
		FROM	CARDSIN 
		WHERE	ACCTNUM = @acct and TIPNUMBER is null

		IF (Select tipnumber from CARDSIN where ACCTNUM = @acct) is null
			BEGIN
				SET @tiplast = @tiplast +1
				SET @newtip = left(ltrim(@NewTip), 3) + right(REPLICATE ('0', 12) + Cast(@tiplast as varchar(12)),12)
				UPDATE CARDSIN SET TIPNUMBER = @newtip WHERE ACCTNUM = @acct
			END
	END


EXEC	RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip


GO


