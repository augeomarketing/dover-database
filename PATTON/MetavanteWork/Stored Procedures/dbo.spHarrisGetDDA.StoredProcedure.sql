USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisGetDDA]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisGetDDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisGetDDA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisGetDDA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S.Blanchette
-- Create date: 11/2008
-- Description:	Get DDA number from affiliat table
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisGetDDA]

AS
BEGIN
Declare @TipFirst char(3),@DBName varchar(100), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000)


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	-- Read all records from the Harris FI control file
	declare HarrisFI_crsr cursor 
	for select TipFirst
	from HarrisFI

	open HarrisFI_crsr
	/*                                                                            */
	fetch HarrisFI_crsr into @TipFirst 

	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
	begin	

			-- Get Data base name for this Harris FI
			set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
						where DBNumber=@TipFirst)

			 --Update customer status to reflect that customer is enrolled.
				set @sqlupdate=''Update selfenroll
						set dim_selfenroll_DDA_number = (select top 1 custid from '' + QuoteName(@DBName) + N''.dbo.affiliat where tipnumber = selfenroll.dim_selfenroll_tipnumber)
						where left(dim_selfenroll_tipnumber,3)=@TipFirst and len(dim_selfenroll_DDA_number)= ''''0'''' and exists (select * from '' + QuoteName(@DBName) + N''.dbo.affiliat where tipnumber = selfenroll.dim_selfenroll_tipnumber) ''	
				Exec sp_executesql @sqlupdate, N''@TipFirst char(3)'', @TipFirst=@TipFirst  

				
		fetch HarrisFI_crsr into @TipFirst 

	end	

	Fetch_Error:
	close HarrisFI_crsr
	deallocate HarrisFI_crsr

END
' 
END
GO
