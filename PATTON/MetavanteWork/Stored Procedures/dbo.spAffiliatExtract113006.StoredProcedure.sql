USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spAffiliatExtract113006]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAffiliatExtract113006]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAffiliatExtract113006]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAffiliatExtract113006]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spAffiliatExtract113006]
as

Truncate table Affiliat

Declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)

DECLARE cur_databases CURSOR FOR
	select name from master.dbo.sysdatabases where name not in (''tempdb'') and name like ''5%'' and name <>''5xx''
	OPEN cur_databases 
	fetch next  from cur_databases into @DBName
	
WHILE (@@FETCH_STATUS=0)
	BEGIN
		set @SQLInsert=''INSERT INTO Affiliat
        		select * 
			from '' + QuoteName(@DBName) + N''.dbo.affiliat ''
			Exec sp_executesql @SQLInsert 

		fetch next  from cur_databases into @DBName
	END

close cur_databases
deallocate cur_databases' 
END
GO
