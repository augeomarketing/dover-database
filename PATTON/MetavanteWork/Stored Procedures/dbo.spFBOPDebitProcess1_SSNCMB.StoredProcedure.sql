USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPDebitProcess1_SSNCMB]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPDebitProcess1_SSNCMB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPDebitProcess1_SSNCMB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPDebitProcess1_SSNCMB]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPDebitProcess1_SSNCMB] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
-- Changed logic to concatenate overflow address to primary address
--
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 3 */
/* SCAN: SEB003 */
-- Changed logic to omit closed records from being part of name generation

/*********  Begin SEB002  *************************/
update FBOPdebitcardin
	set addr1=rtrim(addr1) + rtrim(Addr1Ext), addr2=rtrim(addr2) + rtrim(Addr2Ext),
	TIPMID=''0000000'' /** PHB added this from update stmt below PHB **/
/*********  End SEB002  *************************/
Declare  @SQLDynamic nvarchar(1000)

/*           Convert Trancode                                                     */
update FBOPDebitCardIn
	set FBOPDebitCardIn.Trancode=TrancodeXref.trancodeout 
from FBOPDebitCardIn, TrancodeXref 
where FBOPdebitcardin.trancode=TrancodeXref.trancodeIn 

update FBOPdebitcardin	
set numpurch=''1'', purch=points
where trancode=''67''

update FBOPdebitcardin	
set numret=''1'', amtret=points
where trancode=''37''


update FBOPdebitcardin
set ddanum=savnum
where ddanum=''00000000000''

/** PHB  How about creating a SSN exclusion table?  That way SSN#s aren''t hard coded.  gives more flexibility PHB **/
delete from FBOPdebitcardin
where ssn=''987654321''

update FBOPdebitcardin
set status=''C''
where status=''B''

/* rearrange name from  Last, First to First Last */
update FBOPdebitcardin
set NA1=rtrim(substring(na1,charindex('','',na1)+2,len(rtrim(na1)))) + '' '' + substring(na1,1,charindex('','',na1)-1)
where substring(na1,1,1) not like '' '' and na1 is not null and NA1 LIKE ''%,%''

update FBOPdebitcardin
	set na1=ltrim(na1)  /** PHB  add rtrim also?  PHB **/

/** PHB  Move this up to the top?  Be less processing PHB **/
delete from FBOPdebitcardin
where DDANUM is Null or DDANUM=''00000000000''


DELETE FROM FBOPDebitRollUp
DELETE FROM FBOPDebitRollUpa

insert into FBOPDebitRollUp 
SELECT acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr, SigvsPin 
FROM FBOPdebitcardin
GROUP BY acctnum, SigvsPin
ORDER BY acctnum, SigvsPin
/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */
DELETE FROM FBOPAccountRollUp

insert into FBOPAccountRollUp 
select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS, SigvsPin
FROM FBOPdebitcardin
ORDER BY acctnum

DELETE FROM FBOPdebitcardin2

INSERT into FBOPdebitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS, SigvsPin)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS, a.SigvsPin	        	
from FBOPAccountRollUp a, FBOPDebitRollUp b
where a.acctnum=b.acctnum and a.SigvsPin=b.SigvsPin
/*                                         	                */
/*  End logic to roll up multiple records into one by NA2       */
/*                                                        	*/
/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update FBOPdebitcardin2
set joint=''J''
where NA2 is not null and substring(na2,1,1) not like '' ''
update FBOPdebitcardin2
set joint=''S''
where NA2 is null or substring(na2,1,1) like '' ''
END
' 
END
GO
