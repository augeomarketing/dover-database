USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransactionDataKroeger_Daily]    Script Date: 04/17/2012 11:26:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataKroeger_Daily]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionDataKroeger_Daily]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransactionDataKroeger_Daily]    Script Date: 04/17/2012 11:26:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- S Blanchette
-- 7/2010
-- Added check for null tip
-- SEB001
CREATE PROCEDURE [dbo].[spImportTransactionDataKroeger_Daily] 
	-- Add the parameters for the stored procedure here
	@TipFirst char (3), 
	@EndDate char(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	truncate table transum

	insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	select	(select tipnumber from dbo.cardsin where ACCTNUM = dbo.Kroger_Daily_Card_Transaction_Detail.CardNumber),
			CardNumber,
			@EndDate,
			'1',
			cast (TransactionAmount as int) ,
			'0',
			'0',
			'D',
			' '
	from dbo.Kroger_Daily_Card_Transaction_Detail
	where DebitCreditIndicator = 'D' and TransactionAmount>0

-- New code to add transaction records for 2P earnings	
	insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	select	(select tipnumber from dbo.cardsin where ACCTNUM = dbo.Kroger_Daily_Card_Transaction_Detail.CardNumber),
			CardNumber,
			@EndDate,
			'1',
			cast (TransactionAmount as int) ,
			'0',
			'0',
			'G',
			' '
	from dbo.Kroger_Daily_Card_Transaction_Detail
	where DebitCreditIndicator = 'D' and TransactionAmount>0 and LEFT(RTRIM(LTRIM(Filler)),1) = 'K'
--End new code

	insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	select	(select tipnumber from dbo.cardsin where ACCTNUM = dbo.Kroger_Daily_Card_Transaction_Detail.CardNumber),
			CardNumber,
			@EndDate,
			'0',
			'0',
			'1',
			cast (TransactionAmount as int) ,
			'D',
			' '
	from dbo.Kroger_Daily_Card_Transaction_Detail
	where DebitCreditIndicator = 'C' and TransactionAmount>0

-- New code to add transaction records for 2P credits
	insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	select	(select tipnumber from dbo.cardsin where ACCTNUM = dbo.Kroger_Daily_Card_Transaction_Detail.CardNumber),
			CardNumber,
			@EndDate,
			'0',
			'0',
			'1',
			cast (TransactionAmount as int) ,
			'G',
			' '
	from dbo.Kroger_Daily_Card_Transaction_Detail
	where DebitCreditIndicator = 'C' and TransactionAmount>0 and LEFT(RTRIM(LTRIM(Filler)),1) = 'K'
--End new code

	set @SQLDelete='Delete from dbo.transum
					where tipnumber not in (select tipnumber from ' + QuoteName(@DBName) + N'.dbo.customer) '
	Exec sp_executesql @SQLDelete

	update transum
	set ratio='1', trancode='67', description='Debit Card Purchase' 
	where amtpurch>0 and cardtype='D' 

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        		select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
			from transum
			where trancode=''67'' and tipnumber is not null' /* SEB001 */
	Exec sp_executesql @SQLInsert

-- New code to add 2P earnings into History	
	update transum
	set ratio='1', trancode='G2', description='2 Point' 
	where amtpurch>0 and cardtype='G' 

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        		select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
			from transum
			where trancode=''G2'' and tipnumber is not null'
	Exec sp_executesql @SQLInsert	
--End new code

	update transum
	set ratio='-1', trancode='37', description='Debit Card Returns'
	where amtcr>0 and cardtype='D'

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        		select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
			from transum
			where trancode=''37'' and tipnumber is not null' /* SEB001 */
	Exec sp_executesql @SQLInsert

-- New code to add 2P credits into History	
	update transum
	set ratio='-1', trancode='G7', description='2 Point Returns'
	where amtcr>0 and cardtype='G'

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        		select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
			from transum
			where trancode=''G7'' and tipnumber is not null'
	Exec sp_executesql @SQLInsert	
--End new code

	/***********************/
	/*  end section SEB001  */
	/***********************/

	delete from transum
	where trancode is null

	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.customer 
			set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber), 
				runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber) 
			where exists(select * from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber) '
	Exec sp_executesql @SQLUpdate


END



GO


