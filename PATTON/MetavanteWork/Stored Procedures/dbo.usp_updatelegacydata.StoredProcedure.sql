USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_updatelegacydata]    Script Date: 04/09/2015 10:37:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_updatelegacydata] 
	@tip VARCHAR(3)
	
AS

BEGIN
	IF @tip in (Select sid_dbprocessinfo_dbnumber from RewardsNow.dbo.RNICustomerLegacyMapSource)
		BEGIN
			EXEC RewardsNow.dbo.usp_RNICustomerUpsertLegacyData @tip
		END
END	

GO


