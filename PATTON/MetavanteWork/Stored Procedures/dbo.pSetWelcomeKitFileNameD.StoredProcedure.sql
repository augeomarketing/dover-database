USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pSetWelcomeKitFileNameD]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileNameD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetWelcomeKitFileNameD]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileNameD]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSetWelcomeKitFileNameD] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS
/*********************************************************/
/* Date: 7/08                                            */
/* By: S. Blanchette                                     */
/* SCAN: SEB001                                          */
/* REASON: to put record count as part of name           */
/*                                                       */
/*********************************************************/

declare @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
/*****************/
/* Start SEB001  */
/*****************/
declare @DBName varchar(100), @NumRecs numeric, @SQLSelect nvarchar(1000)
set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

set @SQLSelect=''set @NumRecs=(select count(*) from '' + QuoteName(@DBName) + N''.dbo.Welcomekit) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 
/*****************/
/* End SEB001  */
/*****************/

/*****************/
/* Start SEB002  */
/*****************/

-- set @endingDate=(select top 1 datein from DateforAudit) 
--Section to put together the current date.  Handle problem with date returning a one position month and day
--
-- set @workmonth=left(@endingdate,2) 
-- set @workyear=right(@endingdate,2) 
-- set @currentdate=@workmonth + @workyear 

set @currentdate=(select top 1 datein from DateforAudit) 
set @currentdate=REPLACE(@currentdate,''/'',''_'') 

/*****************/
/* END SEB002  */
/*****************/
/* set @filename=''W'' + @TipPrefix + @currentdate + ''.xls'' */
/* SEB001 */set @filename=''W'' + @TipPrefix + ''_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''
 
set @newname=''O:\5xx\Output\WelcomeKits\Welcomed.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\WelcomeKits\'' + @filename' 
END
GO
