USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSarahTest]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSarahTest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSarahTest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSarahTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spSarahTest] @TipFirst varchar(3)
AS 

execute spGenerateNewTIPNumbersConsumerDDA1 @TipFirst

' 
END
GO
