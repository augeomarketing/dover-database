USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFileMER]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFileMER]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatementFileMER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFileMER]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 4/09
-- Description:	4/09 Added logic to include bonus for Merchant rewards                                     */
-- =============================================
CREATE PROCEDURE [dbo].[spQuarterlyStatementFileMER]
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10),
	@EndDateParm char(10),
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

/*******************************************************************************/
/* SEB004 10/08 Added logic to deal with bonus reversals                        */
/*******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket=''MonthBeg'' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Quarterly_Statement_File ''
exec sp_executesql @SQLTruncate


if exists(select name from sysobjects where name=''wrkhist'')
begin 
	truncate table wrkhist
end

set @SQLSelect=''insert into wrkhist
		select tipnumber, trancode, sum(points) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip )
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), left(zipcode,5) 
				from '' + QuoteName(@DBName) + N''.dbo.customer ''
Exec sp_executesql @SQLInsert

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File 
	set acctnum = rtrim(b.acctid) 
	from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
	where a.tipnumber = b.tipnumber and b.acctstatus=''''A'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set lastfour=right(rtrim(acctnum),4) ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   63       */

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointspurchasedcr=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''63'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointspurchaseddb=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''67'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with bonuses            */

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonuscr=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and ((trancode like ''''B%'''' and trancode<>''''BX'''') or trancode= ''''NW'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and ((trancode like ''''B%'''' and trancode<>''''BX'''') or trancode= ''''NW'''')) ''
	Exec sp_executesql @SQLUpdate 

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonuscr=pointsbonuscr - 
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and trancode = ''''BX'''') 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and trancode = ''''BX'''') ''
	Exec sp_executesql @SQLUpdate 

/*******************************/
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonusmer=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and trancode = ''''FA'''') 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and trancode = ''''FA'''') ''
	Exec sp_executesql @SQLUpdate 
/*******************************/

/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsadded=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''IE'''' '' 	
	Exec sp_executesql @SQLUpdate 

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsadded = pointsadded +
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''TP'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point increases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded + pointsbonusmer''
	Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsredeemed=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and trancode like ''''R%'''') 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and trancode like ''''R%'''') ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsredeemed=pointsredeemed-
		b.points 
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''DR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file increased redeemed         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsredeemed=pointsredeemed+
		b.points 
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''IR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsreturnedcr=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''33'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsreturneddb=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''37'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with minus adjustments    */  --Updated 1/19/09 RBM - Added trancode XP to script for calculation
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointssubtracted=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode in (''''DE'''',''''XP'''') '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point decreases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbegin = b.'' + Quotename(@MonthBucket) + N'' 
		from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
		where a.tipnumber = b.tipnumber ''
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased ''
exec sp_executesql @SQLUpdate
END
' 
END
GO
