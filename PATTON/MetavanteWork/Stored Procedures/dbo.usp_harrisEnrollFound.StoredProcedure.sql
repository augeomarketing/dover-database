USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_harrisEnrollFound]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_harrisEnrollFound]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_harrisEnrollFound]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_harrisEnrollFound]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Barriere, Allen
-- Create date: 20090725
-- Description:	fill in harris e-mail
-- =============================================
CREATE PROCEDURE [dbo].[usp_harrisEnrollFound]
@name varchar(50),
@tipnumber varchar(15),
@address1 varchar(50),
@address2 varchar(50),
@citystatezip varchar(100),
@email varchar(50),
@match varchar(1)
AS
BEGIN
	SET NOCOUNT ON;

 If @match = ''1''
  Begin
   SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(dim_harrisemail_enrolled, ''<<< name >>>'',@name), ''<<< tipnumber >>>'',@tipnumber), ''<<< address1 >>>'',@address1), ''<<< address2 >>>'',@address2), ''<<< csz >>>'',@citystatezip), ''<<< email >>>'',@email), '''','''')
   FROM metavantework.dbo.harrisemail
  End
 Else
  Begin
   SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(dim_harrisemail_nomatch, ''<<< name >>>'',@name), ''<<< tipnumber >>>'',@tipnumber), ''<<< address1 >>>'',@address1), ''<<< address2 >>>'',@address2), ''<<< csz >>>'',@citystatezip), ''<<< email >>>'',@email), '''','''')
   FROM metavantework.dbo.harrisemail
  End  
END
' 
END
GO
