USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kBackOutTransactionRawImport]    Script Date: 12/10/2010 13:00:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kBackOutTransactionRawImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kBackOutTransactionRawImport]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kBackOutTransactionRawImport]    Script Date: 12/10/2010 13:00:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[usp_b2kBackOutTransactionRawImport]
	@processingStartDate datetime
	, @processingEndDate datetime
as
begin
	declare @start varchar(10)
	declare @end varchar(10)
	
	set @start = CONVERT(varchar(10), @processingStartDate, 112)
	set @end = CONVERT(varchar(10), @processingEndDate, 112)
	
	delete from b2kTranImportRaw where dim_b2ktranimportraw_filedate between @start and @end
	
	if (select COUNT(*) from b2kTranImportRawHistory where dim_b2ktranimportraw_filedate between @start and @end) > 0
		begin
			raiserror('History Files Exist for %s to %s, check processing tables for data.', 16, 1, @start, @end)
		end	

end


GO

