USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_Kroger_pilot_synchlasttip]    Script Date: 08/22/2016 08:38:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Kroger_pilot_synchlasttip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Kroger_pilot_synchlasttip]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_Kroger_pilot_synchlasttip]    Script Date: 08/22/2016 08:38:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Kroger_pilot_synchlasttip]

AS

DECLARE @tbl table (tipnumber_old varchar(15), tipnumber_new varchar(15))

DECLARE @tip1 bigint
	,	@tip2 bigint

INSERT INTO	@tbl	(tipnumber_old)
SELECT LastTipNumberUsed FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber in ('52J','5KJ','52R','5KR','52W','5KW')

SELECT @tip1 = RIGHT(tipnumber_old, 12) FROM @tbl WHERE tipnumber_old like '52J%'
SELECT @tip2 = RIGHT(tipnumber_old, 12) FROM @tbl WHERE tipnumber_old like '5KJ%'
UPDATE @tbl SET tipnumber_new = LEFT(tipnumber_old, 3) + RIGHT(replicate('0',12) + cast(case when @tip1>@tip2 then @tip1 else @tip2 end as varchar(15)),12) Where tipnumber_old like '5_J%'

SELECT @tip1 = RIGHT(tipnumber_old, 12) FROM @tbl WHERE tipnumber_old like '52R%'
SELECT @tip2 = RIGHT(tipnumber_old, 12) FROM @tbl WHERE tipnumber_old like '5KR%'
UPDATE @tbl SET tipnumber_new = LEFT(tipnumber_old, 3) + RIGHT(replicate('0',12) + cast(case when @tip1>@tip2 then @tip1 else @tip2 end as varchar(15)),12) Where tipnumber_old like '5_R%'

SELECT @tip1 = RIGHT(tipnumber_old, 12) FROM @tbl WHERE tipnumber_old like '52W%'
SELECT @tip2 = RIGHT(tipnumber_old, 12) FROM @tbl WHERE tipnumber_old like '5KW%'
UPDATE @tbl SET tipnumber_new = LEFT(tipnumber_old, 3) + RIGHT(replicate('0',12) + cast(case when @tip1>@tip2 then @tip1 else @tip2 end as varchar(15)),12) Where tipnumber_old like '5_W%'

UPDATE	a
SET		LastTipNumberUsed = tipnumber_new
FROM	RewardsNow.dbo.dbprocessinfo a JOIN @tbl b ON a.DBNumber = LEFT(b.tipnumber_new, 3)

GO


