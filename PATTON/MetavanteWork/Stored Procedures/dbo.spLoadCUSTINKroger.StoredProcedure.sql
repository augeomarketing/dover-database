USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCUSTINKroger]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCUSTINKroger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCUSTINKroger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCUSTINKroger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCUSTINKroger] 
	-- Add the parameters for the stored procedure here
	@dateadded char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Truncate Table custin

update Cardsin
set CityState=rtrim(replace(CityState,char(44), ''''))

update Cardsin
set CityState=''..........''
where len(Citystate)=''0'' or len(rtrim(citystate))<''3''

-- Add to CUSTIN TABLE
INSERT INTO custin (ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, HomePhone, WorkPhone, DateAdded, DDANUM, SSN, Cardtype, misc2 ) 
select distinct Acctnum, NA1, NA2, NA3, NA4, NA5, '' '', Status, TIPNUMBER, Addr1, Addr2, (rtrim(citystate) + '' '' + rtrim(zip)), left(citystate,(len(citystate)-3)), right(rtrim(CityState),2), ZIP, LASTNAME, Phone1, Phone2, @DateAdded, DDANUM, SSN, Cardtype, NA6
from Cardsin
where status<>''C''
order by tipnumber

update custin
set address4=rtrim(replace(address4,char(44), ''''))

delete from custin
where tipnumber is null
END
' 
END
GO
