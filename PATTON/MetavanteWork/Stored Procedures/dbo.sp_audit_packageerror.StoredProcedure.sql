USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[sp_audit_packageerror]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_audit_packageerror]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_audit_packageerror]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_audit_packageerror]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE procedure [dbo].[sp_audit_packageerror]
	@tipfirst varchar(3),
	@ErrCount int,
	@ErrMonth datetime
	

as
declare @Rundate datetime
Set @rundate = getdate()

Insert into PackageError
Select @tipfirst as Tipfirst, @ErrCount as Errcount, @ErrMonth as ErrMonth, @Rundate as Rundate

' 
END
GO
