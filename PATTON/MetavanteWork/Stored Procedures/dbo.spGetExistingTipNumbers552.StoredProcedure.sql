USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbers552]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbers552]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetExistingTipNumbers552]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbers552]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGetExistingTipNumbers552] @TipFirst char(3)
AS
 /* Revision                                   */
/* By Sarah Blanchette                         */
/* Date 11/24/2007                             */
/* SCAN  SEB001                                */
/* Remove name check on auto combine per Doug at FBOP   */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/*  Get tipnumber based on account number      */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.acctnum=b.acctid ''
Exec sp_executesql @SQLUpdate

/*  Get tipnumber based on old account number  */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.oldcc=b.acctid ''
Exec sp_executesql @SQLUpdate


/*  Get tipnumber based on ssn na1 na2 joint are the same      */
update cardsin
set na2='' ''
where na2 is null

drop table wrktab1n

select distinct ssn, na1, na2, joint, tipnumber 
--select distinct ssn, tipnumber 
into wrktab1n
from cardsin
where ssn is not null and ssn not like ''00000%'' and  left(ssn,3)<> ''999'' and tipnumber is not null 

update cardsin
set cardsin.tipnumber=wrktab1n.tipnumber
from cardsin, wrktab1n
--where cardsin.ssn is not null and cardsin.ssn not like ''00000%'' and  left(cardsin.ssn,3)<> ''999'' and cardsin.tipnumber is null and cardsin.ssn=wrktab1n.ssn 
where (cardsin.ssn is not null and left(cardsin.ssn,3) not in (''000'', ''   '', ''999'')) and cardsin.tipnumber is null and cardsin.ssn=wrktab1n.ssn and ((cardsin.na1=wrktab1n.na1 and cardsin.na2=wrktab1n.na2) or (cardsin.na1=wrktab1n.na2 and cardsin.na2=wrktab1n.na1)) 

delete from cardsin
where tipnumber is null and status<>''A''' 
END
GO
