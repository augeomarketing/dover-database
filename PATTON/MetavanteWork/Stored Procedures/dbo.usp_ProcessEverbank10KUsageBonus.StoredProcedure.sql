USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProcessEverbank10KUsageBonus]    Script Date: 11/09/2011 09:36:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ProcessEverbank10KUsageBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ProcessEverbank10KUsageBonus]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ProcessEverbank10KUsageBonus]    Script Date: 11/09/2011 09:36:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_ProcessEverbank10KUsageBonus]
AS

DECLARE @processingenddate DATE

--Handle legacy date strings
DECLARE @begindate NCHAR(10)
DECLARE @enddate NCHAR(10)
DECLARE @bonusProgramID INT

SELECT @bonusProgramID = sid_rnibonusprogram_id FROM Rewardsnow.dbo.RNIBonusProgram where dim_rnibonusprogram_description = 'Everbank 10K Usage Bonus'

EXEC Metavantework.dbo.spGetDataProcessDates @begindate out, @enddate out

SET @processingenddate = CONVERT(DATE, @enddate)

--INSERT NEW POTENTIAL PARTICIPANTS
INSERT INTO Rewardsnow.dbo.RNIBonusProgramParticipant
(
	sid_customer_tipnumber	
	, sid_rnibonusprogramfi_id
	, dim_rnibonusprogramparticipant_isbonusawarded
	, dim_rnibonusprogramparticipant_remainingpointstoearn
)
SELECT c.TIPNUMBER, elig.sid_rnibonusprogramfi_id, 0, 1
FROM METAVANTEWORK.DBO.b2kDemog d
INNER JOIN
	(
		Select a.TIPNUMBER, a.ACCTID, MIN(c.DATEADDED) DATEADDED 
		from [529EverbankConsumer].dbo.CUSTOMER c
		inner join [529EverbankConsumer].dbo.AFFILIAT a
		on c.TIPNUMBER = a.TIPNUMBER
		group by a.TIPNUMBER, a.ACCTID
		
		UNION ALL
		
		select a.TIPNUMBER, a.ACCTID, MIN(c.DATEADDED) DATEADDED 
		from [52T].dbo.CUSTOMER c
		inner join [52T].dbo.AFFILIAT a
		on c.TIPNUMBER = a.TIPNUMBER
		group by a.TIPNUMBER, a.ACCTID
	) c
ON d.dim_b2kdemog_cardnumber = c.ACCTID
INNER JOIN	
	(
		SELECT sid_dbprocessinfo_dbnumber, dim_rnibonusprogram_effectivedate
			, dim_rnibonusprogram_expirationdate, dim_rnibonusprogramfi_effectivedate
			, dim_rnibonusprogramfi_expirationdate, dim_rnibonusprogramcriteria_effectivedate
			, dim_rnibonusprogramcriteria_expirationdate, dim_rnibonusprogramcriteria_code01
			, dim_rnibonusprogramcriteria_code02, dim_rnibonusprogramcriteria_code03
			, dim_rnibonusprogramfi_duration, bpfi.sid_rnibonusprogramfi_id
		FROM Rewardsnow.dbo.RNIBonusProgramFI bpfi
		INNER JOIN Rewardsnow.dbo.RNIBonusProgram bp
			ON bp.sid_rnibonusprogram_id = bpfi.sid_rnibonusprogram_id
		INNER JOIN Rewardsnow.dbo.RNIBonusProgramCriteria bpc
			ON bpfi.sid_rnibonusprogramfi_id = bpc.sid_rnibonusprogramfi_id
		where 
			sid_dbprocessinfo_dbnumber in ('529', '52T')
			and bp.sid_rnibonusprogram_id = @bonusProgramID
			and @processingenddate between dim_rnibonusprogram_effectivedate and dim_rnibonusprogram_expirationdate
			and @processingenddate between dim_rnibonusprogramfi_effectivedate and dim_rnibonusprogramfi_expirationdate
			and @processingenddate between dim_rnibonusprogramcriteria_effectivedate and dim_rnibonusprogramcriteria_expirationdate
	) elig
ON d.dim_b2kdemog_binplan = elig.dim_rnibonusprogramcriteria_code01
	AND d.dim_b2kdemog_productline = elig.dim_rnibonusprogramcriteria_code02
	AND d.dim_b2kdemog_subproducttype = elig.dim_rnibonusprogramcriteria_code03
	AND d.dim_b2kdemog_tipfirst = elig.sid_dbprocessinfo_dbnumber
LEFT OUTER JOIN Rewardsnow.dbo.RNIBonusProgramParticipant p
ON c.TIPNUMBER = p.sid_customer_tipnumber
WHERE
	@processingenddate BETWEEN DATEADDED AND Rewardsnow.dbo.ufn_GetLastOfMonth(DATEADD(m, dim_rnibonusprogramfi_duration, DATEADDED))
	AND p.sid_customer_tipnumber IS NULL

--UPDATE ELIGIBILITY	
UPDATE p
SET dim_rnibonusprogramparticipant_remainingpointstoearn = 0
FROM Rewardsnow.dbo.RNIBonusProgramParticipant p
INNER JOIN
(
	SELECT TIPNUMBER, ACCTID, HISTDATE FROM [529EverbankConsumer].dbo.HISTORY h
	INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap m ON h.TRANCODE = m.sid_trantype_trancode
	WHERE m.dim_rnitrancodegroup_name = 'INCREASE_PURCHASE'

	UNION ALL 
	SELECT TIPNUMBER, ACCTID, HISTDATE FROM [529EverbankConsumer].dbo.HISTORY h
	INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap m ON h.TRANCODE = m.sid_trantype_trancode
	WHERE m.dim_rnitrancodegroup_name = 'INCREASE_PURCHASE'
) hist
ON p.sid_customer_tipnumber = hist.TIPNUMBER
INNER JOIN Rewardsnow.dbo.RNIBonusProgramFI bpfi
ON p.sid_rnibonusprogramfi_id = bpfi.sid_rnibonusprogramfi_id
INNER JOIN
	(SELECT TIPNUMBER, DATEADDED FROM [529EverbankConsumer].dbo.CUSTOMER
		UNION SELECT TIPNUMBER, DATEADDED FROM [52T].dbo.CUSTOMER) cust
ON p.sid_customer_tipnumber = cust.TIPNUMBER

WHERE @processingenddate BETWEEN bpfi.dim_rnibonusprogramfi_effectivedate and bpfi.dim_rnibonusprogramfi_expirationdate
	AND hist.HISTDATE BETWEEN cust.DATEADDED and RewardsNow.dbo.ufn_GetLastOfMonth(DATEADD(m, bpfi.dim_rnibonusprogramfi_duration, cust.DATEADDED))
	AND p.dim_rnibonusprogramparticipant_remainingpointstoearn > 0
	AND ISNULL(p.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0
	AND p.dim_rnibonusprogramparticipant_dateawarded IS NULL
	AND @processingenddate >= DATEADD(m, bpfi.dim_rnibonusprogramfi_duration, cust.DATEADDED)
	AND bpfi.sid_rnibonusprogram_id = @bonusProgramID

--AWARD UNAWARDED POINTS	
--PUT POINTS TO AWARD INTO TEMP TABLE
DECLARE @award TABLE
(
	myid int identity(1,1) not null primary key
	, tipfirst varchar(3)
	, dbnamepatton varchar(50)
	, tipnumber varchar(15)
	, trancode varchar(2)
	, trandesc varchar(50)
	, points int
)
INSERT INTO @award(tipfirst, dbnamepatton, tipnumber, trancode, trandesc, points)
SELECT
	bpfi.sid_dbprocessinfo_dbnumber as tipfirst
	, dbpi.dbnamepatton as dbnamepatton
	, bpp.sid_customer_tipnumber as tipnumber
	, bp.sid_trantype_tranCode as trancode
	, bp.dim_rnibonusprogram_description as trandesc
	, bpfi.dim_rnibonusprogramfi_bonuspoints as points
FROM Rewardsnow.dbo.RNIBonusProgramParticipant bpp
INNER JOIN Rewardsnow.dbo.RNIBonusProgramFI bpfi on 
	bpp.sid_rnibonusprogramfi_id = bpfi.sid_rnibonusprogramfi_id
INNER JOIN Rewardsnow.dbo.RNIBonusProgram bp
	ON bpfi.sid_rnibonusprogram_id = bpp.sid_rnibonusprogramfi_id	
INNER JOIN Rewardsnow.dbo.dbprocessinfo dbpi
	ON bpfi.sid_dbprocessinfo_dbnumber = dbpi.DBNumber
WHERE isnull(bpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0
	and bpp.dim_rnibonusprogramparticipant_dateawarded IS NULL
	and bpfi.sid_rnibonusprogram_id = @bonusProgramID
	and bpp.dim_rnibonusprogramparticipant_remainingpointstoearn = 0
	

	
DECLARE @myid INT
DECLARE @maxid INT
DECLARE @sqlTemplate NVARCHAR(MAX)
DECLARE @sqlExec NVARCHAR(MAX)
DECLARE @sqlUpdate NVARCHAR(MAX)

SET @sqlTemplate = 'INSERT INTO [<DBNAME>].dbo.HISTORY (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TRANCOUNT, POINTS, DESCRIPTION, RATIO, OVERAGE) 
					VALUES (''<TIPNUMBER>'', NULL, ''<HISTDATE>'', ''<TRANCODE>'', 1, <POINTS>, ''<TRANDESC>'', 1, 0)'

SELECT @maxid = MAX(myid) FROM @award

SET @myid = 1
WHILE @myid <= @maxid
BEGIN
	SELECT @sqlExec = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sqlTemplate
		, '<TIPFIRST>', tipfirst)
		, '<TIPNUMBER>', tipnumber)
		, '<TRANCODE>', trancode)
		, '<POINTS>', points)
		, '<HISTDATE>', CONVERT(VARCHAR(10), @processingEndDate, 101))
		, '<TRANDESC>', trandesc)
		, '<DBNAME>', dbnamepatton)
	FROM @award WHERE myid = @myid
	
	SELECT @sqlUpdate = REPLACE(REPLACE(REPLACE(
		'UPDATE [<DBNAME>].dbo.Customer SET Runbalance = Runbalance + <POINTS>, RunAvailable = RunAvailable + <POINTS> WHERE TIPNUMBER = ''<TIPNUMBER>'''
		, '<DBNAME>', dbnamepatton)
		, '<POINTS>', points)
		, '<TIPNUMBER>', tipnumber)
	FROM @award WHERE myid = @myid

	EXEC sp_executesql @sqlExec
	EXEC sp_executesql @sqlUpdate

	SET @myid = @myid + 1
END

DECLARE @awardDate DATETIME = getdate()

UPDATE bpp
SET bpp.dim_rnibonusprogramparticipant_isbonusawarded = 1
	, bpp.dim_rnibonusprogramparticipant_dateawarded = @awardDate
FROM Rewardsnow.dbo.RNIBonusProgramParticipant bpp
INNER JOIN Rewardsnow.dbo.RNIBonusProgramFI bpfi
	ON bpp.sid_rnibonusprogramfi_id = bpfi.sid_rnibonusprogramfi_id
INNER JOIN Rewardsnow.dbo.RNIBonusProgram bp
	ON bpfi.sid_rnibonusprogram_id = bp.sid_rnibonusprogram_id
		AND bp.sid_rnibonusprogram_id = @bonusProgramID
INNER JOIN @award a
	ON bpp.sid_customer_tipnumber = a.tipnumber
		AND bp.sid_trantype_trancode = a.trancode
		AND bpfi.sid_dbprocessinfo_dbnumber = a.tipfirst
WHERE
	ISNULL(bpp.dim_rnibonusprogramparticipant_isbonusawarded, 0) = 0
	AND bpp.dim_rnibonusprogramparticipant_dateawarded is null

GO


