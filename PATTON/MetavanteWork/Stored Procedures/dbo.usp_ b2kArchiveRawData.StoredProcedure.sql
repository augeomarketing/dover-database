USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kArchiveRawData]    Script Date: 01/17/2011 12:05:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kArchiveRawData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kArchiveRawData]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kArchiveRawData]    Script Date: 01/17/2011 12:05:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_b2kArchiveRawData]
AS
BEGIN
/*	INSERT INTO b2kDemogImportRawHistory
	(
		sid_b2kdemogimportraw_id
		, dim_b2kdemogimportraw_importdate
		, dim_b2kdemogimportraw_filedate
		, dim_b2kdemogimportraw_recordtype
		, dim_b2kdemogimportraw_relationshipaccount
		, dim_b2kdemogimportraw_cardnumber
		, dim_b2kdemogimportraw_primarycardnumber
		, dim_b2kdemogimportraw_cardtype
		, dim_b2kdemogimportraw_accounttype
		, dim_b2kdemogimportraw_producttype
		, dim_b2kdemogimportraw_productline
		, dim_b2kdemogimportraw_subproducttype
		, dim_b2kdemogimportraw_programid
		, dim_b2kdemogimportraw_primarynameprefix
		, dim_b2kdemogimportraw_primaryfirstname
		, dim_b2kdemogimportraw_primarymiddleinitial
		, dim_b2kdemogimportraw_primarylastname
		, dim_b2kdemogimportraw_primarynamesuffix
		, dim_b2kdemogimportraw_secondarynameprefix
		, dim_b2kdemogimportraw_secondaryfirstname
		, dim_b2kdemogimportraw_secondarymiddleinitial
		, dim_b2kdemogimportraw_secondarylastname
		, dim_b2kdemogimportraw_secondarynamesuffix
		, dim_b2kdemogimportraw_addressline1
		, dim_b2kdemogimportraw_addressline2
		, dim_b2kdemogimportraw_addressline3
		, dim_b2kdemogimportraw_addressline4
		, dim_b2kdemogimportraw_city
		, dim_b2kdemogimportraw_state
		, dim_b2kdemogimportraw_country
		, dim_b2kdemogimportraw_zipcode
		, dim_b2kdemogimportraw_homephonenumber
		, dim_b2kdemogimportraw_workphonenumber
		, dim_b2kdemogimportraw_workphoneextension
		, dim_b2kdemogimportraw_emailaddress
		, dim_b2kdemogimportraw_scprogramstatus
		, dim_b2kdemogimportraw_scprogramstatuschangedate
		, dim_b2kdemogimportraw_hostaccountstatus
		, dim_b2kdemogimportraw_hostacctstatusdate
		, dim_b2kdemogimportraw_scparticipationflag
		, dim_b2kdemogimportraw_primarysocialsecuritynumber
		, dim_b2kdemogimportraw_secondarysocialsecuritynumber
		, dim_b2kdemogimportraw_primarygender
		, dim_b2kdemogimportraw_secondarygender
		, dim_b2kdemogimportraw_birthdate
		, dim_b2kdemogimportraw_accountopendate
		, dim_b2kdemogimportraw_languagepreference
		, dim_b2kdemogimportraw_effectivedate
		, dim_b2kdemogimportraw_purchaserate
		, dim_b2kdemogimportraw_newcardnumber
		, dim_b2kdemogimportraw_associationid
		, dim_b2kdemogimportraw_corpid
		, dim_b2kdemogimportraw_agentinstitutionid
		, dim_b2kdemogimportraw_billcode
		, dim_b2kdemogimportraw_binplan
		, dim_b2kdemogimportraw_scenrollmentdate
		, dim_b2kdemogimportraw_laststatementdate
		, dim_b2kdemogimportraw_transferdate
		, dim_b2kdemogimportraw_filler1
		, dim_b2kdemogimportraw_primaryhouseholdcardnumber
		, dim_b2kdemogimportraw_filler2
		, dim_b2kdemogimportraw_householdcardtype
		, dim_b2kdemogimportraw_institutionid
		, dim_b2kdemogimportraw_vipindicator
		, dim_b2kdemogimportraw_filler3
	)
	SELECT
		sid_b2kdemogimportraw_id
		, dim_b2kdemogimportraw_importdate
		, dim_b2kdemogimportraw_filedate
		, dim_b2kdemogimportraw_recordtype
		, dim_b2kdemogimportraw_relationshipaccount
		, dim_b2kdemogimportraw_cardnumber
		, dim_b2kdemogimportraw_primarycardnumber
		, dim_b2kdemogimportraw_cardtype
		, dim_b2kdemogimportraw_accounttype
		, dim_b2kdemogimportraw_producttype
		, dim_b2kdemogimportraw_productline
		, dim_b2kdemogimportraw_subproducttype
		, dim_b2kdemogimportraw_programid
		, dim_b2kdemogimportraw_primarynameprefix
		, dim_b2kdemogimportraw_primaryfirstname
		, dim_b2kdemogimportraw_primarymiddleinitial
		, dim_b2kdemogimportraw_primarylastname
		, dim_b2kdemogimportraw_primarynamesuffix
		, dim_b2kdemogimportraw_secondarynameprefix
		, dim_b2kdemogimportraw_secondaryfirstname
		, dim_b2kdemogimportraw_secondarymiddleinitial
		, dim_b2kdemogimportraw_secondarylastname
		, dim_b2kdemogimportraw_secondarynamesuffix
		, dim_b2kdemogimportraw_addressline1
		, dim_b2kdemogimportraw_addressline2
		, dim_b2kdemogimportraw_addressline3
		, dim_b2kdemogimportraw_addressline4
		, dim_b2kdemogimportraw_city
		, dim_b2kdemogimportraw_state
		, dim_b2kdemogimportraw_country
		, dim_b2kdemogimportraw_zipcode
		, dim_b2kdemogimportraw_homephonenumber
		, dim_b2kdemogimportraw_workphonenumber
		, dim_b2kdemogimportraw_workphoneextension
		, dim_b2kdemogimportraw_emailaddress
		, dim_b2kdemogimportraw_scprogramstatus
		, dim_b2kdemogimportraw_scprogramstatuschangedate
		, dim_b2kdemogimportraw_hostaccountstatus
		, dim_b2kdemogimportraw_hostacctstatusdate
		, dim_b2kdemogimportraw_scparticipationflag
		, dim_b2kdemogimportraw_primarysocialsecuritynumber
		, dim_b2kdemogimportraw_secondarysocialsecuritynumber
		, dim_b2kdemogimportraw_primarygender
		, dim_b2kdemogimportraw_secondarygender
		, dim_b2kdemogimportraw_birthdate
		, dim_b2kdemogimportraw_accountopendate
		, dim_b2kdemogimportraw_languagepreference
		, dim_b2kdemogimportraw_effectivedate
		, dim_b2kdemogimportraw_purchaserate
		, dim_b2kdemogimportraw_newcardnumber
		, dim_b2kdemogimportraw_associationid
		, dim_b2kdemogimportraw_corpid
		, dim_b2kdemogimportraw_agentinstitutionid
		, dim_b2kdemogimportraw_billcode
		, dim_b2kdemogimportraw_binplan
		, dim_b2kdemogimportraw_scenrollmentdate
		, dim_b2kdemogimportraw_laststatementdate
		, dim_b2kdemogimportraw_transferdate
		, dim_b2kdemogimportraw_filler1
		, dim_b2kdemogimportraw_primaryhouseholdcardnumber
		, dim_b2kdemogimportraw_filler2
		, dim_b2kdemogimportraw_householdcardtype
		, dim_b2kdemogimportraw_institutionid
		, dim_b2kdemogimportraw_vipindicator
		, dim_b2kdemogimportraw_filler3
	FROM
		b2kDemogImportRaw
		
	DELETE iRaw
	FROM b2kDemogImportRaw iRaw
	INNER JOIN b2kDemogImportRawHistory hRaw
		ON iRaw.sid_b2kdemogimportraw_id = hRaw.sid_b2kdemogimportraw_id
*/

	INSERT INTO b2kTranImportRawHistory
	(
		sid_b2ktranimportraw_id
		, dim_b2ktranimportraw_importdate
		, dim_b2ktranimportraw_filedate
		, dim_b2ktranimportraw_corpid
		, dim_b2ktranimportraw_agentinstitutionid
		, dim_b2ktranimportraw_accountnumber
		, dim_b2ktranimportraw_trandate
		, dim_b2ktranimportraw_transactionpostdate
		, dim_b2ktranimportraw_transactionsign
		, dim_b2ktranimportraw_transactionamount
		, dim_b2ktranimportraw_prescoredearnings
		, dim_b2ktranimportraw_merchantname
		, dim_b2ktranimportraw_merchantcity
		, dim_b2ktranimportraw_merchantstate
		, dim_b2ktranimportraw_merchantcountry
		, dim_b2ktranimportraw_mccsiccode
		, dim_b2ktranimportraw_industrycode
		, dim_b2ktranimportraw_transactioncode
		, dim_b2ktranimportraw_reasoncode
		, dim_b2ktranimportraw_transactioncurrencyidentifier
		, dim_b2ktranimportraw_pricingplanid
		, dim_b2ktranimportraw_transactiontype
		, dim_b2ktranimportraw_referencenumber
		, dim_b2ktranimportraw_eciindicator
		, dim_b2ktranimportraw_vendorindicator
		, dim_b2ktranimportraw_filler1
		, dim_b2ktranimportraw_merchantid
		, dim_b2ktranimportraw_filler2
	)
	SELECT
		traw.sid_b2ktranimportraw_id
		, traw.dim_b2ktranimportraw_importdate
		, traw.dim_b2ktranimportraw_filedate
		, traw.dim_b2ktranimportraw_corpid
		, traw.dim_b2ktranimportraw_agentinstitutionid
		, traw.dim_b2ktranimportraw_accountnumber
		, traw.dim_b2ktranimportraw_trandate
		, traw.dim_b2ktranimportraw_transactionpostdate
		, traw.dim_b2ktranimportraw_transactionsign
		, traw.dim_b2ktranimportraw_transactionamount
		, traw.dim_b2ktranimportraw_prescoredearnings
		, traw.dim_b2ktranimportraw_merchantname
		, traw.dim_b2ktranimportraw_merchantcity
		, traw.dim_b2ktranimportraw_merchantstate
		, traw.dim_b2ktranimportraw_merchantcountry
		, traw.dim_b2ktranimportraw_mccsiccode
		, traw.dim_b2ktranimportraw_industrycode
		, traw.dim_b2ktranimportraw_transactioncode
		, traw.dim_b2ktranimportraw_reasoncode
		, traw.dim_b2ktranimportraw_transactioncurrencyidentifier
		, traw.dim_b2ktranimportraw_pricingplanid
		, traw.dim_b2ktranimportraw_transactiontype
		, traw.dim_b2ktranimportraw_referencenumber
		, traw.dim_b2ktranimportraw_eciindicator
		, traw.dim_b2ktranimportraw_vendorindicator
		, traw.dim_b2ktranimportraw_filler1
		, traw.dim_b2ktranimportraw_merchantid
		, traw.dim_b2ktranimportraw_filler2
	FROM
		b2kTranImportRaw traw
	LEFT OUTER JOIN b2kTranImportRawHistory hraw
		ON traw.sid_b2ktranimportraw_id = hraw.sid_b2ktranimportraw_id
	WHERE
		hraw.sid_b2ktranimportraw_id IS NULL
		
	DELETE FROM b2kTranImportRaw

END

GO

