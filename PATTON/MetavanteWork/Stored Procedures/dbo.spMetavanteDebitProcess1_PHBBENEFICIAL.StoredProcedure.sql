USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteDebitProcess1_PHBBENEFICIAL]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcess1_PHBBENEFICIAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteDebitProcess1_PHBBENEFICIAL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcess1_PHBBENEFICIAL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spMetavanteDebitProcess1_PHBBENEFICIAL]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   -- Insert statements for procedure here
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
-- Changed logic to concatenate overflow address to primary address
--
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 3 */
/* SCAN: SEB003 */
-- Changed logic to omit closed records from being part of name generation

--
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2009   */
/* REVISION: 4 */
/* SCAN: SEB004 */
-- Changed logic to omit closed records from being part of name generation

/*********  Begin SEB002  *************************/
update debitcardin
	set addr1=rtrim(addr1) + rtrim(Addr1Ext), addr2=rtrim(addr2) + rtrim(Addr2Ext), na1=ltrim(na1), na2=ltrim(na2), 
	TIPMID=''0000000'' /** PHB added this from update stmt below PHB **/
/*********  End SEB002  *************************/
Declare  @SQLDynamic nvarchar(1000)
--drop the view [Debit_view] if it exists and re-create it

/**  PHB why not leave the view in place?
--if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[Debit_view]'') and OBJECTPROPERTY(id, N''IsView'') = 1) drop view [dbo].[Debit_view]
--set @SQLDynamic = ''CREATE VIEW Debit_view AS 
--			SELECT ddanum, acctnum, na1, na2, status
--			FROM debitcardin
--			group by ddanum, acctnum, na1, na2, status ''
--exec sp_executesql @SQLDynamic

PHB **/

/*           Convert Trancode                                                     */
update DebitCardIn
	set DebitCardIn.Trancode=TrancodeXref.trancodeout 
from DebitCardIn, TrancodeXref 
where debitcardin.trancode=TrancodeXref.trancodeIn 

/* update DebitCardIn
	set Trancode=''67''
	where trancode=''004'' 

update DebitCardIn
	set Trancode=''37''
	where trancode=''001''    Taken out 9/30/09     */

update debitcardin	
set numpurch=''1'', purch=points
where trancode=''67''

update debitcardin	
set numret=''1'', amtret=points
where trancode=''37''


--update debitcardin
--set ddanum=savnum
--where ddanum=''00000000000''

/** PHB  How about creating a SSN exclusion table?  That way SSN#s aren''t hard coded.  gives more flexibility PHB **/
--delete from debitcardin
--where ssn=''987654321''


update debitcardin
set status=''C''
where status<>''1''

update debitcardin
set status=''A''
where status = ''1''

/************************************************************************************************/
/*    Need to get date from input variable                                                      */
/*                                                                                              */
/************************************************************************************************/
/** PHB  move this up above to minimize # trips through table on updates
update debitcardin
set TIPMID=''0000000''
PHB **/

/* rearrange name from  Last, First to First Last */
declare @NameLast char(20), @nameFirst char(20), @nameMI char(20), @na1 char (26), @namework char(26)

--set @NameLast=ltrim(substring(@na1,1,charindex('' '',@na1))) 
--set @namework = ltrim(right(@na1, (26-len(@NameLast))))
--set @nameFirst= ltrim(substring(@namework,1,charindex('' '',@namework))) 
--
--SET @nameFirst = RTRIM(@nameFirst) + '' '' + rtrim(@nameMI)

--update debitcardin
--set na1 = ltrim(rtrim(substring( ltrim(na1), charindex('' '',ltrim(na1)), (24 - charindex('' '',ltrim(na1)) )))) +
--			'' '' +  ltrim(rtrim(substring( ltrim(na1), 1, charindex('' '',ltrim(na1))))), 
--	na2 = ltrim(rtrim(substring( ltrim(na2), charindex('' '',ltrim(na2)), (24 - charindex('' '',ltrim(na2)) )))) +
--			'' '' +  ltrim(rtrim(substring( ltrim(na2), 1, charindex('' '',ltrim(na2)))))

update debitcardin
set na1 = ltrim(rtrim(substring(na1, 16, 10)) ) + '' '' +  ltrim(rtrim(substring(na1, 26, 1))) + '' '' +  ltrim(rtrim(substring(na1, 1, 15))),
	na2 = ltrim(rtrim(substring(na2, 16, 10)) ) + '' '' +  ltrim(rtrim(substring(na2, 26, 1))) + '' '' +  ltrim(rtrim(substring(na2, 1, 15)))

update debitcardin
set na1 = REPLACE(na1, ''  '', '' ''), 
    na2 = REPLACE(na2, ''  '', '' '')

/******************************************************/
/* Section to make status code the same for like DDA# */
/******************************************************/
--drop table debitwrk
--select DDANUM, STATUS 
--into debitwrk
--from debitcardin
--order by DDANUM, STATUS
--update debitcardin	
--set debitcardin.STATUS=debitwrk.STATUS
--from debitcardin, debitwrk
--where debitcardin.DDANUM=debitwrk.DDANUM

/** PHB  Move this up to the top?  Be less processing PHB **/
delete from debitcardin
where DDANUM is Null or DDANUM=''00000000000''

/** PHB
/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
--declare @DDANUM nchar(11), @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40)
--declare @HLDDDA char(11), @reccnt bigint, @seq bigint
--
--truncate table ddachk
--
--insert into ddachk (ddanum, namein, seq)
--select distinct ddanum, na1,''1''
--from Debit_view
--where ddanum is not null and left(ddanum,11)<>''00000000000'' and na1 is not null and status<>''C'' /* SEB003 */
--
--insert into ddachk (ddanum, namein, seq)
--select distinct ddanum, na2, ''2''
--from Debit_view
--where ddanum is not null and left(ddanum,11)<>''00000000000'' and na2 is not null and status<>''C''  /* SEB003 */
--
--
--DROP VIEW Debit_view  /** PHB  why drop the view and recreate it?  no overhead leaving it in place **/
--drop table ddachkwrk  /** PHB do a if exists for ddachkwrk?  better yet, create table (with a primary key and indexes if needed) then truncate it before you use it PHB **/
--
--select * 
--into ddachkwrk
--from ddachk
--order by ddanum, seq asc
--
--declare ddachk_crsr cursor  
--for select DDANUM, seq 
--from ddachkwrk
--for update
--/*                                                                            */
--open ddachk_crsr
--/*                                                                            */
--fetch ddachk_crsr into @DDANUM, @seq
--/*                                                                            */
--set @reccnt=''0''
--set @hlddda='' ''
--if @@FETCH_STATUS = 1
--	goto Fetch_Error
--/*                                                                            */
--while @@FETCH_STATUS = 0
--begin
--	if @ddanum <> @hlddda
--	Begin
--		set @reccnt=''0''
--		set @hlddda = @ddanum
--	end 
--	set @reccnt= @reccnt + 1
--	update ddachkwrk
--		set seq = @reccnt 
--		where current of ddachk_crsr
--		goto Next_Record
--Next_Record:
--	fetch ddachk_crsr into @DDANUM, @seq
--end
--Fetch_Error:
--close  ddachk_crsr
--deallocate  ddachk_crsr	 	
--
--truncate table ddaname
--
--insert into ddaname (DDANUM)
--select distinct ddanum
--from ddachkwrk
--
--update ddaname
--set na1=namein
--from ddaname a, ddachkwrk b
--where a.ddanum = b.ddanum and seq=1
--
--update ddaname
--set na2=namein
--from ddaname a, ddachkwrk b
--where a.ddanum = b.ddanum and seq=2
--
--update ddaname
--set na3=namein
--from ddaname a, ddachkwrk b
--where a.ddanum = b.ddanum and seq=3
--
--update ddaname
--set na4=namein
--from ddaname a, ddachkwrk b
--where a.ddanum = b.ddanum and seq=4
--
--update ddaname
--set na5=namein
--from ddaname a, ddachkwrk b
--where a.ddanum = b.ddanum and seq=5
--
--update ddaname
--set na6=namein
--from ddaname a, ddachkwrk b
--where a.ddanum = b.ddanum and seq=6
--/******************************************************/
--/* Section to remove duplicate names on same record   */
--/******************************************************/
--update ddaname
--set na2=null
--where na2=na1
--
--update ddaname
--set na3=null
--where na3=na1 or na3=na2
--
--update ddaname
--set na4=null
--where na4=na1 or na4=na2 or na4=na3
--
--update ddaname
--set na5=null
--where na5=na1 or na5=na2 or na5=na3 or na5=na4
--
--update ddaname
--set na6=null
--where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5
--/******************************************************************************/
--/* Section to move names to the beginning of the name fields on same record   */
--/******************************************************************************/
--declare @count numeric(1,0)
--set @count=0
--while @count<5
--begin
--	update ddaname
--	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
--	where na1 is null or substring(na1,1,1) like '' ''
--	update ddaname
--	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
--	where na2 is null or substring(na2,1,1) like '' ''
--	update ddaname
--	set na3=na4, na4=na5, na5=na6, na6=null
--	where na3 is null or substring(na3,1,1) like '' ''
--	update ddaname
--	set na4=na5, na5=na6, na6=null
--	where na4 is null or substring(na4,1,1) like '' ''
--	update ddaname
--	set na5=na6, na6=null
--	where na5 is null or substring(na5,1,1) like '' ''
--	set @count= @count + 1
--end
--/******************************************************************************/
--/* Section to populate names into debitcardin table                           */
--/******************************************************************************/
--update debitcardin
--set debitcardin.na1=ddaname.na1, debitcardin.na2=ddaname.na2, debitcardin.na3=ddaname.na3, debitcardin.na4=ddaname.na4, debitcardin.na5=ddaname.na5, debitcardin.na6=ddaname.na6 
--from debitcardin, ddaname
--where debitcardin.ddanum=ddaname.ddanum and debitcardin.status<>''C'' /* SEB003 */

PHB **/

declare @dda			varchar(25)

declare csrDDA cursor FAST_FORWARD for
	select distinct ddanum from dbo.debitcardin

open csrDDA

fetch next from csrDDA into @dda

while @@FETCH_STATUS = 0
BEGIN
	update imp
		set	NA1 =  tmp.name1,
			NA2 = tmp.name2,
			NA3 = tmp.name3,
			NA4 = tmp.name4,
			NA5 = tmp.name5,
			NA6 = tmp.name6
			--SSN = tmp.SSN
	from [dbo].[fnLinkCheckingNames] (@dda) tmp join dbo.debitcardin imp
		on tmp.DDA = imp.ddanum 
		--where imp.status<>''C'' /* SEB004 */

	fetch next from csrDDA into @dda
END

close csrDDA

deallocate csrDDA






/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Begin logic to roll up multiple records into one by Account number      */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
/*declare @TIPFIRST nvarchar (3), @TIPMID nvarchar (7), @TIPLAST nvarchar (5), @TIPNUMBER nvarchar (15), @TYPE nvarchar (2), @SSN nvarchar (9), @SAVNUM nvarchar (11), @BANK nvarchar (4), @CLIENT nvarchar (11), @ACCTNUM nvarchar (25), @OLDCC nvarchar (16), @LASTNAME nvarchar (30), @PERIOD nvarchar (14), @ADDR1 nvarchar (40), @ADDR2 nvarchar (40), @ADDR3 nvarchar (40), @CITYSTATE nvarchar (40), @ZIP nvarchar (10), @PHONE1 nvarchar (10), @PHONE2 nvarchar (10), @NUMPURCH nvarchar (9), @PURCH float, @NUMRET nvarchar (9), @AMTRET float, @TRANCODE char (3), @POINTS char (6)
declare @sNUMPURCH nvarchar(9), @sAMTPURCH float, @sNUMCR nvarchar(9), @sAMTCR float
declare @AMTPURCH float, @NUMCR nvarchar(9), @AMTCR float */

/** PHB   Use temp tables instead?  **/

DELETE FROM DebitRollUp
DELETE FROM DebitRollUpa

insert into DebitRollUp 
SELECT acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr, SigvsPin 
FROM debitcardin
GROUP BY acctnum, SigvsPin
ORDER BY acctnum, SigvsPin
/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */
DELETE FROM AccountRollUp

insert into AccountRollUp 
select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS, SigvsPin
FROM debitcardin
ORDER BY acctnum

DELETE FROM debitcardin2

INSERT into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS, SigvsPin)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS, a.SigvsPin	        	
from AccountRollUp a, DebitRollUp b
where a.acctnum=b.acctnum and a.SigvsPin=b.SigvsPin
/*                                         	                */
/*  End logic to roll up multiple records into one by NA2       */
/*                                                        	*/
/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update debitcardin2
set joint=''J''
where NA2 is not null and substring(na2,1,1) not like '' ''
update debitcardin2
set joint=''S''
where NA2 is null or substring(na2,1,1) like '' ''
END


' 
END
GO
