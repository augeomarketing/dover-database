USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spDealMissingNames]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealMissingNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDealMissingNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDealMissingNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spDealMissingNames] 
	-- Add the parameters for the stored procedure here
	@a_TipPrefix varchar(3)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @sqlif nvarchar(2000), @sqlimport nvarchar(2000), @sqldelete nvarchar(2000), @sqlTruncate nvarchar(2000), @DBName nvarchar(100)
    -- Insert statements for procedure here

/* Get DBName */
	Begin			
		set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
		where DBNumber=@a_TipPrefix)
	End		

/* If error table exists drop it */
	set @SQLIf=N''if not exists(select * from '' + QuoteName(@DBName) + N'' .dbo.sysobjects where xtype=''''u'''' and name = ''''MissingNames'''')
		Begin
			CREATE TABLE '' + QuoteName(@DBName) + N'' .dbo.Missingnames (
				[ssn] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[bank] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[agent] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[ddanum] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[acctnum] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[addr1] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[citystate] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
				[zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
			) ON [PRIMARY]
			 
		End ''
	exec sp_executesql @SQLIf 

/* Truncate the table  */
	set @SQLTruncate = ''Truncate TABLE '' + QuoteName(@DBName) + N'' .dbo.Missingnames ''
	exec sp_executesql @SQLTruncate

/* If blank names push to error file and remove from cardsin table */
	if exists (select * from metavantework.dbo.cardsin where na1 is null)
		Begin
			set @sqlimport = ''Insert into '' + QuoteName(@DBName) + N'' .dbo.Missingnames
								select ssn, bank, agent, ddanum, acctnum, addr1, citystate, zip
								from Metavantework.dbo.cardsin
								where na1 is null AND STATUS = ''''A'''' ''
			exec sp_executesql @sqlimport

			set @sqldelete = ''delete from Metavantework.dbo.cardsin
								where na1 is null ''
			exec sp_executesql @sqldelete
		End

END
' 
END
GO
