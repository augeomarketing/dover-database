USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersKroger]    Script Date: 01/28/2011 13:21:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersKroger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersKroger]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersKroger]    Script Date: 01/28/2011 13:21:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersKroger] 
	-- Add the parameters for the stored procedure here
	@tipfirst char(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)
/* SB003 */declare @tiplast varchar(12)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)


declare @LastTipUsed char(15), @tipnumber varchar(15) /* SEB003 */

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

/*****************************************************/
/*  SEB003            */
/*                    */
set @tipnumber = @LastTipUsed

--CWH BEGIN
--Set @tiplast = Right(@tipnumber, 12)
--Set @tiplast = @tiplast + 1

--Set @tipnumber = rtrim(@tipfirst) + @tiplast
--set @tipnumber=left(ltrim(@tipnumber), 3) + REPLICATE ('0', (15- len(@tipnumber) )) + right(rtrim(@tipnumber), (len(@tipnumber)-3))
--CWH END

/*                    */
/**************************************************/
/*  End SEB003 */
--set @Newnum = cast(@LastTipUsed as bigint) + 1  
/*********  End SEB001  *************************/

drop table wrktab22 

select distinct acctnum, tipnumber
into wrktab22
from cardsin
where tipfirst=@tipFirst and tipnumber is null or len(rtrim(tipnumber)) = 0

--CWH BEGIN
declare @newtiplast bigint
set @newtiplast = CAST(right(@LastTipUsed, 12) as bigint)

update 
	wrktab22
set 
	@newtiplast = (@newtiplast + 1)
	, tipnumber = @tipfirst + RewardsNow.dbo.ufn_Pad(@newtiplast, 'L', 12, '0')


/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */

--declare tip_crsr cursor
--for select tipnumber
--from wrktab22
--for update

--/*                                                                            */
--open tip_crsr
--/*                                                                            */
--fetch tip_crsr 
--/******************************************************************************/	
--/*                                                                            */
--/* MAIN PROCESSING  VERIFICATION                                              */
--/*                                                                            */
--if @@FETCH_STATUS = 1
--	goto Fetch_Error
--/*                                                                            */
--while @@FETCH_STATUS = 0
--	begin	
--		set @NewTip = @tipnumber
				
--		update wrktab22	
--		set tipnumber = @Newtip 
--		where current of tip_crsr
		
--		Set @tiplast = Right(@tipnumber, 12)
--		Set @tiplast = @tiplast + 1

--		Set @tipnumber = rtrim(@tipfirst) + @tiplast
--		set @tipnumber=left(ltrim(@tipnumber), 3) + REPLICATE ('0', (15- len(@tipnumber) )) + right(rtrim(@tipnumber), (len(@tipnumber)-3))
		
--		goto Next_Record
--Next_Record:
--		fetch tip_crsr
--	end

--Fetch_Error:
--close  tip_crsr
--deallocate  tip_crsr


--update cardsin
--set tipnumber=b.tipnumber 
--from cardsin a, wrktab22 b
--where a.acctnum is not null and a.tipnumber is null and b.acctnum=a.acctnum 

--CWH END

update CARDSIN
SET TIPNUMBER = WT.tipnumber
FROM CARDSIN ci
INNER JOIN wrktab22 WT
	ON ci.ACCTNUM = wt.acctnum
WHERE
	ci.ACCTNUM is not null
	AND ci.TIPNUMBER is null
	AND wt.tipnumber is not null

-- CWH exec RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip  

SELECT @LastTipUsed = MAX(tipnumber) FROM wrktab22

EXEC RewardsNow.dbo.spPutLastTipNumberUsed @tipfirst, @LastTipUsed
END

GO


