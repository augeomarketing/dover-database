USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation1PointPoint]    Script Date: 07/29/2011 13:40:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation1PointPoint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGatherReconcilliation1PointPoint]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation1PointPoint]    Script Date: 07/29/2011 13:40:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		S Blanchette
-- Create date: 12/09
-- Description:	Gather reconcilliation Numbers
-- =============================================
CREATE PROCEDURE [dbo].[spGatherReconcilliation1PointPoint]
	-- Add the parameters for the stored procedure here
	@DateIn char(10), @dbname nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @sql nvarchar(max)
	
	Set @sql = '    
	update ['+@dbname+'].dbo.KroegerReconcilliationData
	set ValueOutPoint1 = isnull((select sum(points*ratio) from ['+@dbname+'].dbo.history where trancode in (''37'',''67'') and histdate = '''+@DateIn+''' ),0)
	where ProcessDate = '''+@DateIn+'''
	
	update ['+@dbname+'].dbo.KroegerReconcilliationData
	set ValueOutPoint2 = isnull(ValueOutPoint2,0) + isnull((select sum(points*ratio) from ['+@dbname+'].dbo.history where trancode in (''G2'',''G7'') and histdate = '''+@DateIn+''' ),0)
	where ProcessDate = '''+@DateIn+'''
	'
	
	Exec sp_executesql @sql
	
END



GO


