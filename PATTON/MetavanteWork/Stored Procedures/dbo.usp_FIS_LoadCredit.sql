USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_LoadCredit]    Script Date: 03/13/2012 10:01:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_LoadCredit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_LoadCredit]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_LoadCredit]    Script Date: 03/13/2012 10:01:01 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_FIS_LoadCredit] 
	@Tipfirst varchar(3)
	
AS

DECLARE @SQL nvarchar(max)

TRUNCATE TABLE	Cardsin

--populate the table Cardsin with recs from table CreditCardIn where Tipfirst=@Tipfirst
SET	@SQL = '
	INSERT INTO	Cardsin
		(
		TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, JOINT, SSN, BANK, AGENT,
		DDANUM, ACCTNUM, OLDCC, NA1, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, 
		CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, PERIOD, LASTNAME, CARDTYPE, SIGvsPIN
		) 
	SELECT		TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, JOINT, SSN, BANK, AGENT,
				DDANUM, ACCTNUM, OLDCC, NA1, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, 
				CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, PERIOD, LASTNAME, ''C'', ''''
	FROM		creditcardin
	WHERE		Tipfirst = ''<<TIPFIRST>>''
	ORDER BY	Tipfirst, ssn
	'
SET		@SQL =	REPLACE(@SQL, '<<TIPFIRST>>', @Tipfirst)	
EXEC	sp_executesql @SQL


GO


