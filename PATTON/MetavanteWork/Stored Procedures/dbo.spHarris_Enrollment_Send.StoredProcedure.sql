USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spHarris_Enrollment_Send]    Script Date: 01/06/2011 08:54:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarris_Enrollment_Send]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarris_Enrollment_Send]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spHarris_Enrollment_Send]    Script Date: 01/06/2011 08:54:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2009
-- Description:	Create send file for SelfEnroll
-- =============================================
-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	If K29 only pass 6 digits
-- SCAN: SEB001
-- =============================================
CREATE PROCEDURE [dbo].[spHarris_Enrollment_Send] 
	-- Add the parameters for the stored procedure here
	@tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(1000), @time char(8), @timeA char(6), @curdte char(8), @Institution_ID char(5), @RewardPlan char(3) 

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	set @time = CONVERT (char(8), getdate(), 108 )
	set @timeA = left(@time,2) + substring(@time,4,2) + right(rtrim(@time),2)

	set @curdte = CONVERT ( char(8), getdate(), 112 )

	set @Institution_ID = 
		CASE
			when @tipfirst = '51J' then '00K30'
			when @tipfirst = '51K' then '00K31'
			when @tipfirst = '594' then '00K29'
		end

	set @RewardPlan = 
		CASE
			when @tipfirst = '51J' then 'K30'
			when @tipfirst = '51K' then 'K31'
			when @tipfirst = '594' then 'K29'
		end

	set @sqlinsert = N'insert into Harrisselfenrollsend (
										[Institution_Id] ,
										[Cardholder_Account_Number] ,
										[Update_Date] ,
										[Update_Time] ,
										[Prefix] ,
										[Reward_Plan] ,
										[Entollment_Date] ,
										[sid_selfenroll_id] ,
										[dim_selfenroll_tipnumber] ,
										[dim_selfenroll_firstname] ,
										[dim_selfenroll_lastname]  ,
										[dim_selfenroll_address1]  ,
										[dim_selfenroll_address2]  ,
										[dim_selfenroll_city] ,
										[dim_selfenroll_state] ,
										[dim_selfenroll_zipcode] ,
										[dim_selfenroll_ssnlast4] ,
										[dim_selfenroll_cardlastsix] ,
										[dim_selfenroll_email] ,
										[dim_selfenroll_created] ,
										[dim_selfenroll_dda_number] 
									)
																			
	select 
									''' + @Institution_ID + ''', 
									acctid, ' +
									@curdte + ', ' + 
									@timeA  + ',		
									left(acctid,9) , ''' +
									@RewardPlan + ''', 
									CONVERT ( char(8), [dim_selfenroll_created], 112 ) , 
									[sid_selfenroll_id] ,
									[dim_selfenroll_tipnumber] ,
									[dim_selfenroll_firstname] ,
									[dim_selfenroll_lastname]  ,
									[dim_selfenroll_address1]  ,
									[dim_selfenroll_address2]  ,
									[dim_selfenroll_city] ,
									[dim_selfenroll_state] ,
									[dim_selfenroll_zipcode] ,
									[dim_selfenroll_ssnlast4] ,
									[dim_selfenroll_cardlastsix] ,
									[dim_selfenroll_email] ,
									[dim_selfenroll_created] ,
									[dim_selfenroll_dda_number] 
									
	from ' + quotename(@DBName) + N'.dbo.affiliat join selfenroll on tipnumber = [dim_selfenroll_tipnumber] 
	where [dim_selfenroll_tipnumber] like ''' + @Tipfirst + '%'' and acctstatus = ''A'''

	Exec sp_executesql @SQLInsert
/*
------------------------------------------------
--START SEB001
------------------------------------------------
	if @tipfirst = '594'
	Begin
		update Harrisselfenrollsend 
							set [Prefix] = left([Prefix],6) 
							where Reward_Plan = 'K29' 
		End
------------------------------------------------
--END SEB001
------------------------------------------------
*/

--CWH
	update Harrisselfenrollsend 
	set [Prefix] = MetavanteWork.dbo.ufn_resizeBin(left([dim_selfenroll_tipnumber], 3), [Prefix])

END

GO

