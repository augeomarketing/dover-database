USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_WebRegistration]    Script Date: 01/24/2012 13:44:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_Bonus_WebRegistration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_Bonus_WebRegistration]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_WebRegistration]    Script Date: 01/24/2012 13:44:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_FIS_Bonus_WebRegistration] 
	@startDate varchar(10), @EndDate varchar(10), @tipfirst varchar(3), @Trancode varchar(2)

AS 

/****************************************************************************/
/*   Procedure to generate the Activation bonus based on first use          */
/****************************************************************************/
DECLARE	@SQL nvarchar(max), 
		@BonusPoints numeric(9)

SET		@BonusPoints = (SELECT  OnlineRegistrationBonus from DBProcessInfo where DBNumber=@TipFirst)

IF	@BonusPoints > 0 
	BEGIN
		TRUNCATE TABLE Web_Registration
		
		SET	@SQL = N'
			INSERT INTO Web_Registration
						(tipnumber)
			SELECT		tipnumber
			FROM		[RN1].[<<DBNAME2>>].dbo.[1security]
			WHERE		password is not null 
				and		regdate > = ''<<STARTDATE>>'' 
				and		regdate <= ''<<ENDDATE>>''
				and		left(tipnumber,3)= ''<<TIPFIRST>>''
				and		tipnumber in (SELECT tipnumber FROM [<<DBNAME>>].dbo.customer) 
				and		tipnumber not in (SELECT TIPNUMBER FROM [<<DBNAME>>].dbo.OneTimeBonuses WHERE trancode = ''<<TRANCODE>>'')
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME2>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<STARTDATE>>', @startDate)							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)							
		SET		@SQL =	REPLACE(@SQL, '<<TIPFIRST>>', @tipfirst)	
		SET		@SQL =	REPLACE(@SQL, '<<TRANCODE>>', @Trancode)						
		EXEC	sp_executesql @SQL

		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.history
						(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
			SELECT		TIPnumber, ''<<ENDDATE>>'', ''<<TRANCODE>>'', ''1'', <<BONUSPTS>>, ''1'', ''<<DESCRIPTION>>'', ''0''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber in (SELECT tipnumber FROM Web_Registration)
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<DESCRIPTION>>', (SELECT  Description from Trantype where Trancode = @Trancode))
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)
		SET		@SQL =	REPLACE(@SQL, '<<TRANCODE>>', @Trancode)							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)
		EXEC	sp_executesql @SQL  
 
		SET @sql = N'
			UPDATE	[<<DBNAME>>].dbo.Customer 
			SET		RunAvailable = RunAvailable + ''<<BONUSPTS>>'', 
					RunBalance = RunBalance + ''<<BONUSPTS>>''  
			WHERE	tipnumber in (SELECT tipnumber FROM Web_Registration)
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)							
		EXEC	sp_executesql @SQL


		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.OneTimeBonuses
						(TipNumber,TranCode, DateAwarded)
			SELECT		TIPnumber, ''<<TRANCODE>>'', ''<<ENDDATE>>''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber in (SELECT tipnumber FROM Web_Registration)

			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)
		SET		@SQL =	REPLACE(@SQL, '<<TRANCODE>>', @Trancode)							
		EXEC	sp_executesql @SQL
	END

GO


