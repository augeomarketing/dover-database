USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kRestoreTransactionsFromArchiveByMonth]    Script Date: 08/29/2011 12:30:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kRestoreTransactionsFromArchiveByMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kRestoreTransactionsFromArchiveByMonth]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kRestoreTransactionsFromArchiveByMonth]    Script Date: 08/29/2011 12:30:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_b2kRestoreTransactionsFromArchiveByMonth]
	@processingEnd DATETIME
AS
BEGIN
	SET NOCOUNT ON
	
	EXEC usp_b2kArchiveRawData
	
	DECLARE @year VARCHAR(4)
	DECLARE @month VARCHAR(2)
	DECLARE @recordsAffected INT
	
	SET @year = CAST(DATEPART(YYYY, @processingEnd) AS VARCHAR)
	set @month = RewardsNow.dbo.ufn_Pad(CAST(datepart(mm, @processingEnd) as varchar), 'L', 2, '0')
	
	SET IDENTITY_INSERT b2kTranImportRaw ON	
	
	--select ', ' + column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'b2kTranImportRaw'
	
	INSERT INTO b2kTranImportRaw
	(
		sid_b2ktranimportraw_id
		, dim_b2ktranimportraw_importdate
		, dim_b2ktranimportraw_filedate
		, dim_b2ktranimportraw_corpid
		, dim_b2ktranimportraw_agentinstitutionid
		, dim_b2ktranimportraw_accountnumber
		, dim_b2ktranimportraw_trandate
		, dim_b2ktranimportraw_transactionpostdate
		, dim_b2ktranimportraw_transactionsign
		, dim_b2ktranimportraw_transactionamount
		, dim_b2ktranimportraw_prescoredearnings
		, dim_b2ktranimportraw_merchantname
		, dim_b2ktranimportraw_merchantcity
		, dim_b2ktranimportraw_merchantstate
		, dim_b2ktranimportraw_merchantcountry
		, dim_b2ktranimportraw_mccsiccode
		, dim_b2ktranimportraw_industrycode
		, dim_b2ktranimportraw_transactioncode
		, dim_b2ktranimportraw_reasoncode
		, dim_b2ktranimportraw_transactioncurrencyidentifier
		, dim_b2ktranimportraw_pricingplanid
		, dim_b2ktranimportraw_transactiontype
		, dim_b2ktranimportraw_referencenumber
		, dim_b2ktranimportraw_eciindicator
		, dim_b2ktranimportraw_vendorindicator
		, dim_b2ktranimportraw_filler1
		, dim_b2ktranimportraw_merchantid
		, dim_b2ktranimportraw_filler2
	)
	SELECT
		sid_b2ktranimportraw_id
		, dim_b2ktranimportraw_importdate
		, dim_b2ktranimportraw_filedate
		, dim_b2ktranimportraw_corpid
		, dim_b2ktranimportraw_agentinstitutionid
		, dim_b2ktranimportraw_accountnumber
		, dim_b2ktranimportraw_trandate
		, dim_b2ktranimportraw_transactionpostdate
		, dim_b2ktranimportraw_transactionsign
		, dim_b2ktranimportraw_transactionamount
		, dim_b2ktranimportraw_prescoredearnings
		, dim_b2ktranimportraw_merchantname
		, dim_b2ktranimportraw_merchantcity
		, dim_b2ktranimportraw_merchantstate
		, dim_b2ktranimportraw_merchantcountry
		, dim_b2ktranimportraw_mccsiccode
		, dim_b2ktranimportraw_industrycode
		, dim_b2ktranimportraw_transactioncode
		, dim_b2ktranimportraw_reasoncode
		, dim_b2ktranimportraw_transactioncurrencyidentifier
		, dim_b2ktranimportraw_pricingplanid
		, dim_b2ktranimportraw_transactiontype
		, dim_b2ktranimportraw_referencenumber
		, dim_b2ktranimportraw_eciindicator
		, dim_b2ktranimportraw_vendorindicator
		, dim_b2ktranimportraw_filler1
		, dim_b2ktranimportraw_merchantid
		, dim_b2ktranimportraw_filler2
	FROM
		b2kTranImportRawHistory
	INNER JOIN b2kDemog
		ON dim_b2kdemog_cardnumber = dim_b2ktranimportraw_accountnumber
	WHERE
		LEFT(dim_b2ktranimportraw_filedate, 4) = @year
		AND substring(dim_b2ktranimportraw_filedate, 5, 2) = @month
	SET @recordsAffected = @@ROWCOUNT
	
	PRINT cast(@recordsAffected as varchar) + ' Records Restored'

	SET IDENTITY_INSERT b2kTranImportRaw OFF

END
GO


