USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyStatementFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spMonthlyStatementFile] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3)
--CREATE PROCEDURE pMonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10), @Tipfirst char(3), @Segment char(1)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

/* Date : 9/2007           */
/* By :  S.Blanchette      */
/* Scan: SEB001            */
/* Reason:  Add capture of "FA" transactions  */

/*******************************************************************************/
/* SEB 2/2008                                                                  */
/* SCAN SEB002                                                                 */
/* added code to create a summary record                                       */
/*******************************************************************************/


/*******************************************************************************/
/* SEB003 8/08 Added logic to put bonuses in appropriate column based on segment code*/
/*******************************************************************************/

/*******************************************************************************/
/* SEB004 8/08 Added logic to deal with bonus reversals                        */
/*******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket=''MonthBeg'' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Monthly_Statement_File ''
exec sp_executesql @SQLTruncate


if exists(select name from sysobjects where name=''wrkhist'')
begin 
	truncate table wrkhist
end

set @SQLSelect=''insert into wrkhist
		select tipnumber, trancode, sum(points*ratio) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip, lastfour)
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), left(zipcode,5), segmentcode 
				from '' + QuoteName(@DBName) + N''.dbo.customer where status <> ''''X'''' order by tipnumber ''
Exec sp_executesql @SQLInsert

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File 
	set acctnum = rtrim(b.acctid), DDANUM = rtrim(b.custid) 
	from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
	where a.tipnumber = b.tipnumber and b.acctstatus=''''A'''' ''
Exec sp_executesql @SQLUpdate


/*******************************/
/* START PURCHASES             */
/*******************************/
/* Load the statmement file with CREDIT purchases   63       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointspurchasedcr=
		isnull((Select sum(points) from wrkhist 
		where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''61'''',''''62'''',''''63'''')),0) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointspurchaseddb=
		isnull((Select sum(points) from wrkhist 
		where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''64'''',''''65'''',''''66'''',''''67'''',''''6B'''')),0) '' 	
	Exec sp_executesql @SQLUpdate 

/*******************************/
/* START BONUSES               */
/*******************************/
/* Load the statmement file with CREDIT Bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonuscr=
		isnull((select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''B%'''' or trancode= ''''NW'''')),0) 	
		where left(lastfour,1)=''''C'''' and exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''B%'''' or trancode= ''''NW'''')) ''
	Exec sp_executesql @SQLUpdate 
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonuscr= 0 
		where pointsbonuscr is null'' 	
	Exec sp_executesql @SQLUpdate 	
/* Load the statmement file with DEBIT Bonuses             */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusdb=
		isnull((select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''B%'''' or trancode in (''''NW'''',''''FU''''))),0) 	
		where left(lastfour,1)<>''''C'''' and exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''B%'''' or trancode in (''''NW'''',''''FU''''))) ''
	Exec sp_executesql @SQLUpdate 
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusdb= 0 
		where pointsbonusdb is null'' 	
	Exec sp_executesql @SQLUpdate 	
/* Load the statmement file with MERCHANT Bonuses          */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusmer=
		isnull((select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and trancode = ''''FA''''),0) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and trancode = ''''FA'''') ''
	Exec sp_executesql @SQLUpdate 
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusmer= 0 
		where pointsbonusmer is null'' 	
	Exec sp_executesql @SQLUpdate 

/*******************************/
/* START ADJUSTMENTS PLUS      */
/*******************************/
/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsadded=
		isnull((Select sum(points) from wrkhist
		where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''IE'''',''''TP'''')),0) '' 	
	Exec sp_executesql @SQLUpdate 

/*******************************/
/* TOTAL POINT INCREASES       */
/*******************************/
/* Load the statmement file with total point increases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsbonusmer + pointsadded ''
	Exec sp_executesql @SQLUpdate

/************************************************************************************************************************/

/*******************************/
/* START REDEMPTIONS           */
/*******************************/
/* Load the statmement file with redemptions          */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsredeemed=
		isnull((select sum(points*-1) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''R%'''' or trancode in (''''IR'''',''''DR'''',''''DZ''''))),0) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''R%'''' or trancode in (''''IR'''',''''DR'''',''''DZ''''))) ''
	Exec sp_executesql @SQLUpdate 
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsredeemed= 0 
		where pointsredeemed is null'' 	
	Exec sp_executesql @SQLUpdate 

/*******************************/
/* START RETURNS     */
/*******************************/
/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsreturnedcr=
		isnull((Select sum(points*-1) from wrkhist 
		where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''31'''',''''32'''',''''33'''')),0) '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT returns             */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsreturneddb=
		isnull((Select sum(points*-1) from wrkhist 
		where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''34'''',''''35'''',''''36'''',''''37'''',''''3B'''')),0) '' 	
	Exec sp_executesql @SQLUpdate 

/*******************************/
/* START ADJUSTMENTS MINUS     */
/*******************************/
/* Load the statmement file with minus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointssubtracted=
		isnull((Select sum(points*-1) from wrkhist 
		where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''DE'''',''''XP'''')),0) '' 	
	Exec sp_executesql @SQLUpdate 

/*******************************/
/* TOTAL POINT DECREASES       */
/*******************************/
/* Load the statmement file with total point decreases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted ''
Exec sp_executesql @SQLUpdate

/************************************************************************************************************************/

/*******************************/
/* START BEGIN BALANCE         */
/*******************************/
/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbegin = isnull((b.'' + Quotename(@MonthBucket) + N''),0) 
		from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
		where a.tipnumber = b.tipnumber ''
exec sp_executesql @SQLUpdate

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbegin = 0 
		where pointsbegin is null''
exec sp_executesql @SQLUpdate

/*******************************/
/* START END BALANCE           */
/*******************************/
/* Load the statmement file with ending points */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsend=isnull((pointsbegin + pointsincreased - pointsdecreased),0) ''
exec sp_executesql @SQLUpdate




/* Create summary record */
/**********************************/
/*  This section added  SEB002    */
/**********************************/

--Truncate table AuditSummary


set @SQLUpdate=N''insert into '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File 
		select ''''999999999999999'''' 
		, ''''SUMMARY RECORD'''' 
		, ''''TOTALS'''' 
		, '''' '''' 
		, '''' '''' 
		, '''' '''' 
		, '''' ''''	
		, '''' '''' 
		, sum(PointsBegin) 
		, sum(PointsEnd) 
		, sum(PointsPurchasedCR) 
		, sum(PointsBonusCR) 
		, sum(PointsAdded) 
		, sum(PointsPurchasedDB) 
		, sum(PointsBonusDB) 
		, sum(PointsBonusMER) 
		, sum(PointsIncreased) 
		, sum(PointsRedeemed) 
		, sum(PointsReturnedCR) 
		, sum(PointsSubtracted) 
		, sum(PointsReturnedDB) 
		, sum(PointsDecreased) 
		, '''' '''' 
		, '''' '''' 
		, '''' ''''
		, '''' ''''
		, ''''0'''' 
		from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File ''
exec sp_executesql @SQLUpdate

--set @SQLUpdate=N''insert into '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File
--		select *
--		from AuditSummary ''
--exec sp_executesql @SQLUpdate

' 
END
GO
