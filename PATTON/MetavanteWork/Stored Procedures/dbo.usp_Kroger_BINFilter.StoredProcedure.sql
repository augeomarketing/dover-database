USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_Kroger_BINFilter]    Script Date: 12/08/2014 14:20:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Kroger_BINFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Kroger_BINFilter]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_Kroger_BINFilter]    Script Date: 12/08/2014 14:20:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Kroger_BINFilter]

@tipfirst varchar(3)

AS

DECLARE @clientid varchar(5)
	,	@filler varchar(383)

SELECT @clientid =	CASE 
						WHEN @tipfirst in ('52U','52V') THEN '00G4U'
						WHEN @tipfirst in ('52J','52R','52W') THEN '00G9M'
					END

SELECT @filler =	CASE 
						WHEN @tipfirst in ('52J','52U') THEN '123'
						WHEN @tipfirst in ('52R','52V') THEN 'RLP'
						WHEN @tipfirst in ('52W') THEN 'FRD'
					END

--Delete all Batch Headers not associated with Processing TIP
DELETE FROM Kroger_Daily_Source
WHERE	SUBSTRING(col001, 13, 12) = 'BATCHHEADER' 
	AND	(RTRIM(LTRIM(SUBSTRING(col001, 47, 5))) <> @clientid OR RTRIM(LTRIM(SUBSTRING(col001, 118, 383))) <> @filler)

--Delete all Batch Trailers not associated with Processing TIP				
DECLARE @increment	INT = 1
	,	@value		VARCHAR(20)
DECLARE	@tbl		TABLE		(tblid  INT IDENTITY(1,1), recordsequence VARCHAR(20))
DECLARE @stbl		TABLE		(recordsequence VARCHAR(20))

INSERT INTO @tbl (recordsequence)
	SELECT SUBSTRING(col001, 3, 10) FROM Kroger_Daily_Source WHERE SUBSTRING(col001, 13, 12) = 'BATCHHEADER'

WHILE	@increment <= (Select MAX(tblid) FROM @tbl)
	BEGIN
		SELECT @value = recordsequence FROM @tbl WHERE tblid = @increment
		INSERT INTO @stbl (recordsequence)
			SELECT MIN(SUBSTRING(col001, 3, 10)) 
			FROM Kroger_Daily_Source 
			WHERE SUBSTRING(col001, 13, 12) = 'BATCHTRAILER' AND SUBSTRING(col001, 3, 10) > @value
		SET	@increment += 1
	END	
	
DELETE FROM Kroger_Daily_Source
WHERE SUBSTRING(col001, 13, 12) = 'BATCHTRAILER'
	AND	SUBSTRING(col001, 3, 10) NOT IN (select recordsequence from @stbl)
	
--Delete all Detail Records not associated with Processing TIP
DELETE FROM Kroger_Daily_Source 
WHERE	SUBSTRING(col001, 27,(select distinct LEN(BIN) from [dbo].[DebitBins] where TipFirst = @tipfirst)) not in 
			(select BIN from [dbo].[DebitBins] where TipFirst = @tipfirst)
				AND substring(col001, 13, 12) NOT in ('FILEHEADER','FILETRAILER','BATCHHEADER','BATCHTRAILER')

GO
