USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFixBeginningBalance]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFixBeginningBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFixBeginningBalance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFixBeginningBalance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	load 0 into null fields
-- =============================================
CREATE PROCEDURE [dbo].[spFixBeginningBalance]
	-- Add the parameters for the stored procedure here
	@tipFirst char(3)AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @DBName nvarchar (100), @SQLUpdate nvarchar(2000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

Declare @x int, @MonthBucket nchar(10)
set @X=0
	While CONVERT( int , @X) < ''12''
	Begin
		set @x=@x+1
		set @MonthBucket=''MonthBeg'' + CAST(@X AS varchar(2)) 
		print @MonthBucket
		set @SQLUpdate=N''update ''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table
				set '' + @MonthBucket + N''= ''''0''''
				where '' + @MonthBucket + N'' is null''
		exec sp_executesql @SQLUpdate

	End

END
' 
END
GO
