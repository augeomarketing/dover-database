USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation3PointDollar]    Script Date: 08/04/2011 14:56:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation3PointDollar]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGatherReconcilliation3PointDollar]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation3PointDollar]    Script Date: 08/04/2011 14:56:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGatherReconcilliation3PointDollar]
	-- Add the parameters for the stored procedure here
@DateIn char(10), @dbname nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    Declare @sql nvarchar(4000)
---------NEW CODE----------    
    Declare @dbname2 varchar(50) = (Select dbnamepatton from RewardsNow.dbo.dbprocessinfo 
									where DBNumber in ('52J','52U') and DBNamePatton <> @dbname)
	
	Set @sql = ' 
	update ['+@dbname+'].dbo.KroegerReconcilliationData
	set ValueInPoint3 = ISNULL(ValueInPoint3, 0) + (
													  ISNULL((select sum(cast((Amount) as int)) from dbo.Kroeger3PT where sign = ''+'' and loyaltynumber not in (Select misc2 from ['+@dbname2+'].dbo.customer)), 0)
													  -
													  ISNULL((select sum(cast((Amount) as int)) from dbo.Kroeger3PT where sign = ''-'' and loyaltynumber not in (Select misc2 from ['+@dbname2+'].dbo.customer)), 0)
													) 
	where '''+@DateIn+''' in (select ProcessDate from ['+@dbname+'].dbo.KroegerReconcilliationData)
	'

	Exec sp_executesql @sql
	
END


GO

