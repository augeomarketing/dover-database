USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteSetStatusForClosedFile]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteSetStatusForClosedFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteSetStatusForClosedFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteSetStatusForClosedFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spMetavanteSetStatusForClosedFile]
as

update custin
set status=''A''
where status is null and status1 in (''LG'', ''AV'', ''NR'', ''CC'', ''PC'', ''S2'', ''T2'', ''T3'', ''V4'', ''V5'', ''V7'', ''S '')

update custin
set status=''C''
where status is null and status1 in (''M9'', ''V9'', ''B9'', ''FA'', ''P9'', ''Q9'', ''R9'', ''Y9'')

update custin
set status=''L''
where status is null and status1 in (''F1'', ''FA'')

delete from custin
where status not in (''A'', ''C'')

' 
END
GO
