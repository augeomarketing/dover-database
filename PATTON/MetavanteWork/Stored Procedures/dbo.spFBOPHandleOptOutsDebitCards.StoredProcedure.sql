USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPHandleOptOutsDebitCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPHandleOptOutsDebitCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPHandleOptOutsDebitCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPHandleOptOutsDebitCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spFBOPHandleOptOutsDebitCards] @TipPrefix char(3)
as

/**********************************************************************/
/*  BY S.Blanchette                                                   */
/*  Date 9/2008                                                       */
/*  SCAN SEB001                                                       */
/*  Reason  To add logic to check DDA for opt out.                    */
/*********************************************************************/ 
/* Remove CreditCardIn for Opt Out people  */
delete from  FBOPdebitcardin
where exists(select * from optoutcontrol where acctnumber= FBOPdebitcardin.acctnum)

/****************************/
/* START SEB001             */
/****************************/
delete from  FBOPdebitcardin
where exists(select * from DDAOptOutControl where DDANUMBER= FBOPdebitcardin.DDANUM and left(tipnumber,3)=@TipPrefix )' 
END
GO
