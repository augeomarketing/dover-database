USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewQuarterlyStatementFile]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewQuarterlyStatementFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyStatementFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spNewQuarterlyStatementFile] 
-- Add the parameters for the stored procedure here
				@StartDateParm char(10), 
				@EndDateParm char(10), 
				@Tipfirst char(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

	declare @DBName varchar(50), 
			@SQLUpdate nvarchar(1000),  
			@MonthBucket char(10), 
			@SQLTruncate nvarchar(1000), 
			@SQLInsert nvarchar(1000), 
			@monthbegin char(2), 
			@SQLSelect nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
	set @MonthBucket=''MonthBeg'' + @monthbegin

	/*******************************************************************************/
	/*******************************************************************************/
	/*                                                                             */
	/*          ISSUES WITH ADJUSTMENTS                                            */
	/*                                                                             */
	/*******************************************************************************/
	/*******************************************************************************/
	/* Load the statement file from the customer table  */
	Truncate Table dbo.New_Quarterly_Statement_Detail 


	if exists(select name from sysobjects where name=''wrkhist'')
	begin 
		truncate table dbo.wrkhist
	end

	set @SQLSelect=''insert into wrkhist
			select tipnumber, trancode, sum(points) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
	Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

	set @SQLInsert=''INSERT INTO dbo.New_Quarterly_Statement_Detail(RecordType, tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        				select ''''6'''', tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + left(zipcode,5)), left(zipcode,5)
					from '' + QuoteName(@DBName) + N''.dbo.customer where status=''''A'''' order by tipnumber asc''
	Exec sp_executesql @SQLInsert

	--set @SQLUpdate=''Update dbo.New_Quarterly_Statement_Detail 
	--	set acctnum = rtrim(b.acctid), ddanum=rtrim(b.custid) 
	--	from dbo.New_Quarterly_Statement_Detail a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
	--	where a.tipnumber = b.tipnumber and b.acctstatus=''''A'''' ''
	--Exec sp_executesql @SQLUpdate

	--set @SQLUpdate=''Update dbo.New_Quarterly_Statement_Detail set lastfour=right(rtrim(acctnum),4) ''
	--Exec sp_executesql @SQLUpdate

	/* Load the statmement file with CREDIT purchases   63       */

	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointspurchasedcr=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode=''''63'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT purchases   67       */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointspurchaseddb=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode=''''67'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with HELOC purchases   67       */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointspurchasedhe=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode=''''6H'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Business purchases   6B       */
	/* SEB003  */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointspurchasedbus=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode=''''6B'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with bonuses            */
	/* SEB002  */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbonuscr=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and trancode in (''''FJ'''', ''''FG'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and trancode in (''''FJ'''', ''''FG'''')) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with debit bonuses            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbonusdb=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and trancode in(''''FC'''', ''''FD'''', ''''FI'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and trancode in(''''FC'''', ''''FD'''', ''''FI'''' )) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with HELOC bonuses            */
	/* SEB002 */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbonushe=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode = ''''FH'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode = ''''FH'''' )) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with business bonuses            */
	/* SEB003   */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbonusbus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and trancode in(''''F5'''', ''''F6'''', ''''F7'''', ''''F8'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and trancode in(''''F5'''', ''''F6'''', ''''F7'''', ''''F8'''')) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Employee Incentive CR            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set PointsBonusEmpCR=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''FF''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''FF'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Employee Incentive DB            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set PointsBonusEmpDB=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''FE''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''FE'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Points2U Plus bonus           */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set PointsBonusP2UPlus =
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''FA''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''FA'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Generic bonuses            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode like ''''B%'''' and trancode<>''''BX'''') ) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode like ''''B%'''' and trancode<>''''BX'''') ) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Generic bonuses            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbonus= pointsbonus - 
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''BX''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and (trancode in (''''BX'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with plus adjustments    */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsadded=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''IE'''' '' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsadded = pointsadded +
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''TR'''' '' 	
		Exec sp_executesql @SQLUpdate 
		
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsadded = pointsadded +
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''TP'''' '' 	
		Exec sp_executesql @SQLUpdate 


	/* Load the statmement file with total point increases */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsincreased= pointspurchasedcr + pointspurchaseddb + pointspurchasedhe + pointspurchasedbus + pointsbonuscr + pointsbonusdb + pointsbonushe + pointsbonusbus + PointsBonusEmpCR + PointsBonusEmpDB + PointsBonusP2UPlus + pointsbonus + pointsadded ''
		Exec sp_executesql @SQLUpdate

	/* Load the statmement file with redemptions          */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsredeemed=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.New_Quarterly_Statement_Detail.tipnumber and trancode like ''''R%'''') 	
			where exists(select * from wrkhist where tipnumber=dbo.New_Quarterly_Statement_Detail.tipnumber and trancode like ''''R%'''') ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsredeemed=pointsredeemed +
			b.points 
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''IR'''' '' 	
		Exec sp_executesql @SQLUpdate 

	
	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsredeemed=pointsredeemed -
			b.points 
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''DR'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with CREDIT returns            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsreturnedcr=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''33'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT returns            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsreturneddb=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''37'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with HELOC returns            */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsreturnedhe=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''3H'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Business returns            */
	/* SEB003   */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsreturnedbus=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''3B'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with minus adjustments    */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointssubtracted=
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''DE'''' '' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointssubtracted=pointssubtracted +
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''FR'''' '' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointssubtracted=pointssubtracted +
			b.points
			from dbo.New_Quarterly_Statement_Detail a, wrkhist b 
			where a.tipnumber = b.tipnumber and b.trancode = ''''XP'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with total point decreases */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointsreturnedhe + pointsreturnedbus + pointssubtracted ''
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with the Beginning balance for the Month */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsbegin = b.'' + Quotename(@MonthBucket) + N'' 
			from dbo.New_Quarterly_Statement_Detail a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
			where a.tipnumber = b.tipnumber ''
	exec sp_executesql @SQLUpdate

	/* Load the statmement file with ending points */
	set @SQLUpdate=N''update dbo.New_Quarterly_Statement_Detail set pointsend= pointsbegin + pointsincreased - pointsdecreased ''
	exec sp_executesql @SQLUpdate

	update New_Quarterly_Statement_Detail
	set pointsbegin = REPLICATE(''0'', 9- LEN(PointsBegin)) +  PointsBegin
		, PointsEnd = REPLICATE(''0'', 9- LEN(PointsEnd)) +  PointsEnd
		, PointsPurchasedCR = REPLICATE(''0'', 9- LEN(PointsPurchasedCR) ) + PointsPurchasedCR
		, PointsPurchasedDB = REPLICATE(''0'', 9- LEN(PointsPurchasedDB) ) + PointsPurchasedDB
		, PointsPurchasedHE = REPLICATE(''0'', 9- LEN(PointsPurchasedHE) ) + PointsPurchasedHE
		, PointsPurchasedbus = REPLICATE(''0'', 9- LEN(PointsPurchasedbus) ) + PointsPurchasedbus
		, PointsBonusCR = REPLICATE(''0'', 9- LEN(PointsBonusCR) ) + PointsBonusCR
		, PointsBonusDB = REPLICATE(''0'', 9- LEN(PointsBonusDB) ) + PointsBonusDB
		, PointsBonusHE = REPLICATE(''0'', 9- LEN(PointsBonusHE) ) + PointsBonusHE
		, PointsBonusbus = REPLICATE(''0'', 9- LEN(PointsBonusbus) ) + PointsBonusbus
		, PointsBonusEmpCR = REPLICATE(''0'', 9- LEN(PointsBonusEmpCR) ) + PointsBonusEmpCR
		, PointsBonusEmpDB = REPLICATE(''0'', 9- LEN(PointsBonusEmpDB) ) + PointsBonusEmpDB
		, PointsBonus = REPLICATE(''0'', 9- LEN(PointsBonus) ) + PointsBonus
		, PointsAdded = REPLICATE(''0'', 9- LEN(PointsAdded) ) + PointsAdded
		, PointsIncreased =REPLICATE(''0'', 9- LEN(PointsIncreased) ) + PointsIncreased
		, PointsRedeemed = REPLICATE(''0'', 9- LEN(PointsRedeemed) ) + PointsRedeemed
		, PointsReturnedCR = REPLICATE(''0'', 9- LEN(PointsReturnedCR) ) + PointsReturnedCR
		, PointsReturnedDB = REPLICATE(''0'', 9- LEN(PointsReturnedDB) ) + PointsReturnedDB
		, PointsReturnedHE = REPLICATE(''0'', 9- LEN(PointsReturnedHE) ) + PointsReturnedHE
		, PointsReturnedbus = REPLICATE(''0'', 9- LEN(PointsReturnedbus) ) + PointsReturnedbus
		, PointsSubtracted = REPLICATE(''0'', 9- LEN(PointsSubtracted) ) + PointsSubtracted
		, PointsDecreased = REPLICATE(''0'', 9- LEN(PointsDecreased) ) + PointsDecreased
		, PNTDEBIT = REPLICATE(''0'', 9- LEN(PNTDEBIT)) +  PNTDEBIT
		, PNTMORT = REPLICATE(''0'', 9- LEN(PNTMORT) ) + PNTMORT
		, PNTHOME = REPLICATE(''0'', 9- LEN(PNTHOME) ) + PNTHOME
		, PointsToExpire = REPLICATE(''0'', 9- LEN(PointsToExpire)) +  PointsToExpire
		, PointsBonusP2UPlus = REPLICATE (''0'', 9- LEN(PointsBonusP2UPlus)) +  PointsBonusP2UPlus

END
' 
END
GO
