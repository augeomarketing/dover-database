USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetQuarterProcessDates]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetQuarterProcessDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetQuarterProcessDates]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetQuarterProcessDates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGetQuarterProcessDates] @BeginDate nchar(10) output, @EndDate nchar(10) output
AS

set @BeginDate=(select top 1 QuarterBeginDate from ProcessingDates)
set @EndDate=(select top 1 QuarterEndDate from ProcessingDates)' 
END
GO
