USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_auditname]    Script Date: 02/23/2012 09:07:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ralphs_auditname]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ralphs_auditname]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_auditname]    Script Date: 02/23/2012 09:07:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[usp_ralphs_auditname]
	@tip varchar(3)

AS

Declare @date datetime = 	(Select MAX(CAST(processdate as datetime)) from [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData_Central where TipFirst = @tip)
Declare @string varchar(10)

Set @string =	Case
					when @tip = '52R'
						then 'RP'
					when @tip = '52W'
						then 'FM'
				end	

Select '\\Patton\Ops\5xx\input\Kroeger_Bonus\REW-G9MPoints' + @string + '.'
+ Right('0'+cast(datepart(yy,@date)as varchar(4)),4)
+ Right('0'+cast(datepart(mm,@date)as varchar(2)),2)
+ Right('0'+cast(datepart(dd,@date)as varchar(2)),2)
+ Right('0'+cast(datepart(hh,@date)as varchar(2)),2)
+ Right('0'+cast(datepart(mi,@date)as varchar(2)),2)
+ Right('0'+cast(datepart(ss,@date)as varchar(2)),2)



GO


