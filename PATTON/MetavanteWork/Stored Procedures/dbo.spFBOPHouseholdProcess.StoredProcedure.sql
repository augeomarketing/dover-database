USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPHouseholdProcess]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPHouseholdProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPHouseholdProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPHouseholdProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*                                                                            */
/*    THIS IS TO DO HOUSEHOLDING PROCESS FOR FBOP                             */
/*                                                                            */
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 0 */
/* SCAN:  */
/*  */

CREATE Procedure [dbo].[spFBOPHouseholdProcess] @Bank char(3)
AS

Declare @DBName nvarchar(100), @SQLUpdate nvarchar(1000), @Continue nchar(1)

SELECT   @DBName = 
	CASE @bank
        	WHEN ''032'' THEN ''559CaliforniaNationalConsumer''
       	 	WHEN ''334'' THEN ''561SanDiegoNationalConsumer''
       		WHEN ''402'' THEN ''557ParkNationalConsumer''
       		WHEN ''585'' THEN ''563PacificNationalConsumer''
       	END

update FBOPManualCombine
set oldtipnumber = null, newtipnumber = null

/* Load tipnumbers       */
set @sqlupdate=N''update FBOPManualCombine set OldTipNumber = b.tipnumber
				from FBOPManualCombine a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
				where rtrim(a.bank)=@bank and rtrim(a.cardnumber)=rtrim(b.ACCTID) ''
Exec sp_executesql @sqlupdate,N''@bank char(3)'', @bank=@bank 

truncate Table FBOPWork

/* Get distinct account# */
insert into FBOPWork (bank, account)
select distinct bank, account
from FBOPManualCombine
where oldtipnumber is not null and bank = @bank

/* Get the earliest tipnumber for a given account */
update FBOPWork
set tipnumber = (select top 1 oldtipnumber from FBOPManualCombine where bank=FBOPWork.bank and account=FBOPWork.account and oldtipnumber is not null order by bank, account, oldtipnumber)
where exists (select * from FBOPManualCombine where bank=FBOPWork.bank and account=FBOPWork.account)

/* update table with the primary tipnumber for the account# */
update FBOPManualCombine
set Newtipnumber=b.tipnumber
from FBOPManualCombine a, FBOPWork b
where a.bank = b.bank AND a.account = b.account

truncate table workcombine
/* process only those records that have a different tipnumber */
insert into workcombine (PrimaryTip, SecondaryTip, Errmsg)
select distinct newtipnumber, oldtipnumber, '' ''
from FBOPManualCombine
where bank = @bank and oldtipnumber <> newtipnumber' 
END
GO
