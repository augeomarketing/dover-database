USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spCreateQuarterlyStatementHeader]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateQuarterlyStatementHeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementHeader]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 6/16/09
-- Description:	Create Header record for new Statement Format
-- =============================================
CREATE PROCEDURE [dbo].[spCreateQuarterlyStatementHeader]
	-- Add the parameters for the stored procedure here
	@Qtr char (1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	Declare @Date char(8), @Time char(8), @TimeB char(8), @Batchnum numeric (9), @Batch char(10)

	Truncate Table New_Quarterly_Statement_Header

	set @Batchnum = ( select top 1 StatementFileNumber from New_Quarterly_Statement_Control_Number)
	set @batchnum = @Batchnum + 1
	
	Update 	New_Quarterly_Statement_Control_Number
	set StatementFileNumber = @batchnum

	set @Batch = REPLICATE(''0'', 10 - LEN(@batchnum)) + cast(@batchnum as char)
	set @Date = CONVERT ( char(8), getdate(), 112 ) 
	set @time = CONVERT (char(8), getdate(), 108 )
	set @time = left(@time,2) + substring(@time,4,2) + right(rtrim(@time),2)
	set @timeB = rtrim(@time)
								
	Insert into New_Quarterly_Statement_Header
	Values (''5'', ''REWARDS-STMTS-AD'', @Batch, @Date, @timeB, ''REW01'', ''Q'' + @Qtr)
END
' 
END
GO
