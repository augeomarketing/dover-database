USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spUpdatePointsPosted]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePointsPosted]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdatePointsPosted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdatePointsPosted]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
	-- Add the parameters for the stored procedure here
CREATE PROCEDURE [dbo].[spUpdatePointsPosted] 
	@Clientcode varchar(75), 
	@MonthEndingDate smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
/*
	declare @Pointsupdated smalldatetime
	set @pointsupdated = (select getdate() - day(getdate()))
*/	
	update [rn1].metavante.dbo.client
	set pointsupdated = @MonthEndingDate --@pointsupdated
	where clientcode = @ClientCode

END
' 
END
GO
