USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spHandleOptOuts]    Script Date: 10/14/2011 08:48:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOuts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHandleOptOuts]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spHandleOptOuts]    Script Date: 10/14/2011 08:48:12 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[spHandleOptOuts]
as

/* Remove CreditCardIn for Opt Out people  */
delete from  CreditCardIn
where exists(select * from optoutcontrol where acctnumber= CreditCardIn.acctnum and LEFT(tipnumber, 3) = CREDITCARDIN.TIPFIRST)
GO


