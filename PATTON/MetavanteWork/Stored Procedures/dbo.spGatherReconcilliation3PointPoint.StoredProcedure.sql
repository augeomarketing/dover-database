USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation3PointPoint]    Script Date: 07/29/2011 13:48:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation3PointPoint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGatherReconcilliation3PointPoint]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation3PointPoint]    Script Date: 07/29/2011 13:48:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGatherReconcilliation3PointPoint]
	-- Add the parameters for the stored procedure here
@DateIn char(10), @dbname nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    Declare @sql nvarchar(max)
	
	Set @sql = ' 
	update ['+@dbname+'].dbo.KroegerReconcilliationData
	set ValueOutPoint3 = isnull(ValueOutPoint3,0) + isnull((select sum(points*ratio) from ['+@dbname+'].dbo.history where trancode in (''G3'',''G8'') and histdate = '''+@DateIn+''' ),0)
	where ProcessDate = '''+@DateIn+'''
	'
	
	Exec sp_executesql @sql
	
END


GO


