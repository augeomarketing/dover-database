USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPTotalExpPoints]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPTotalExpPoints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPTotalExpPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPTotalExpPoints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	Provide total for pointstoexpire
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPTotalExpPoints]
	-- Add the parameters for the stored procedure here
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	declare @DBName varchar(100), @SQLUpdate nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@Tipfirst)

	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement 
			set pointstoexpire = (select sum(pointstoexpire) from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement)
			where tipnumber = ''''999999999999999'''' ''
	exec sp_executesql @SQLUpdate

END
' 
END
GO
