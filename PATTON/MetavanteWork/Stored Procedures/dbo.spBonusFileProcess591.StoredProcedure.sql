USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcess591]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcess591]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcess591]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcess591]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spBonusFileProcess591] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, CRDHLR_NBR, points	
from [BonusTip&Points]

update transumbonus
set ratio=''1'', trancode=''BI'', description=''Bonus Points Increase'', histdate=@datein, numdebit=''1'', overage=''0''

insert into [591UBMichigan].dbo.history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
from transumbonus

update [591UBMichigan].dbo.customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=[591UBMichigan].dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=[591UBMichigan].dbo.customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=[591UBMichigan].dbo.customer.tipnumber)' 
END
GO
