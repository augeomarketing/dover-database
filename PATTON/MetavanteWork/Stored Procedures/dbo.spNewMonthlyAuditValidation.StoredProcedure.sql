USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewMonthlyAuditValidation]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewMonthlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewMonthlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2009
-- Description:	THIS IS TO make sure the Monthly file balances
-- =============================================
CREATE PROCEDURE [dbo].[spNewMonthlyAuditValidation] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @DBName varchar(50), 
		@SQLInsert nvarchar(2000), 
		@SQLTruncate nvarchar(2000), 
		@errmsg varchar(50)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate=''Truncate Table '' + QuoteName(@DBName) + N''.dbo.Monthly_Audit_ErrorFile ''
Exec sp_executesql @SQLTruncate

set @errmsg=''Ending Balances do not match''

set @SQLInsert = N''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Monthly_Audit_ErrorFile
	select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
	from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File a, dbo.Current_Month_Activity b
	where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints ''
Exec sp_executesql @SQLInsert, N''@errmsg varchar(50)'', @errmsg = @errmsg
END
' 
END
GO
