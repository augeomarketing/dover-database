USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[sp_Kroeger_NameAddr_Kickback_filename]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Kroeger_NameAddr_Kickback_filename]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_Kroeger_NameAddr_Kickback_filename]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_Kroeger_NameAddr_Kickback_filename]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Kroeger_NameAddr_Kickback_filename] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare	@tipFirst varchar(3),
	        @DatePart datetime
	set @DatePart = GETDATE()
		
	Select ''O:\5xx\Input\Kroeger_Bonus\NameAddress\Kickback\Kroeger_Name_Kickback_''
	+ Right(''0''+cast(year(@DatePart)as varchar(4)),4)
	+ Right(''0''+cast(month(@DatePart)as varchar(2)),2)
	+ Right(''0''+cast(day(@DatePart)as varchar(2)),2)
	+ Right(''0''+cast(datepart(hh,@DatePart)as varchar(2)),2)
	+ Right(''0''+cast(datepart(MINUTE,@DatePart)as varchar(2)),2)
	+ Right(''0''+cast(datepart(SECOND,@DatePart)as varchar(2)),2)
	+ ''.txt''

END
' 
END
GO
