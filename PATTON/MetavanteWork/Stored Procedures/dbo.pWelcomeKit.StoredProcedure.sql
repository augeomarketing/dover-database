USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pWelcomeKit]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pWelcomeKit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Welcomekit ''
exec sp_executesql @SQLTruncate

set @sqlInsert=N''insert into '' + QuoteName(@DBName) + N'' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5) FROM '' + QuoteName(@DBName) + N'' .dbo.customer WHERE (DATEADDED = @EndDate AND STATUS <> ''''C'''') ''
exec sp_executesql @SQLInsert, N''@Enddate nchar(10)'',@Enddate=@Enddate' 
END
GO
