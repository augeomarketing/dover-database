USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPUpdateAffiliatStatusForClosedCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPUpdateAffiliatStatusForClosedCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPUpdateAffiliatStatusForClosedCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPUpdateAffiliatStatusForClosedCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/9
-- Description:	Convert for FBOP
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPUpdateAffiliatStatusForClosedCards] 
	-- Add the parameters for the stored procedure here
	@Tipfirst char(3) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
declare @DBName varchar(50), @SQLupdate nvarchar(2000), @SQLIf nvarchar(2000)
set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)


set @sqlupdate=N''update '' + QuoteName(@DBName) + N''.dbo.affiliat
			set ACCTstatus=''''C''''
			where acctid in (select acctnum from FBOPcardsin WHERE status=''''C'''') ''
Exec sp_executesql @SQLUPDATE

--delete from cardsin
--where status=''C''
END
' 
END
GO
