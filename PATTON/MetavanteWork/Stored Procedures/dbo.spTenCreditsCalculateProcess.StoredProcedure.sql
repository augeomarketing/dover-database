USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spTenCreditsCalculateProcess]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTenCreditsCalculateProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spTenCreditsCalculateProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spTenCreditsCalculateProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spTenCreditsCalculateProcess] 
AS


/******************************************************************************/
/*                                                                            */
/*    THIS IS TO calculate the bonus points based on number of transactions   */
/*    times 1000 points upto a max of 10,000.                                 */
/*                                                                            */
/******************************************************************************/

delete from TenCreditBonus
where points is null or ltrim(rtrim(points))<''1'' or ltrim(rtrim(points))>''999'' 

update TenCreditBonus
set pointstopost= cast(ltrim(rtrim(points)) as float) * 1000 

update TenCreditBonus
set pointstopost=''10000''
where pointstopost>''10000''' 
END
GO
