USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisSetCustomerStatusForEnrolled]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisSetCustomerStatusForEnrolled]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisSetCustomerStatusForEnrolled]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisSetCustomerStatusForEnrolled]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S.Blanchette
-- Create date: 11/2008
-- Description:	To update status code to active if Tip Number is in enroll file
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisSetCustomerStatusForEnrolled]
	-- Add the parameters for the stored procedure here
	
AS

Declare @TipFirst char(3),@DBName varchar(100), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000)


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	-- Read all records from the Harris FI control file
	declare HarrisFI_crsr cursor 
	for select TipFirst
	from HarrisFI

	open HarrisFI_crsr
	/*                                                                            */
	fetch HarrisFI_crsr into @TipFirst 

	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
	begin	

			-- Get Data base name for this Harris FI
			set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
						where DBNumber=@TipFirst)

			 --Update customer status to reflect that customer is enrolled.
				set @sqlupdate=''Update '' + QuoteName(@DBName) + N''.dbo.Customer
						set status=''''A''''
						where status <> ''''A'''' and tipnumber in (select dim_selfenroll_tipnumber from selfenroll) ''	
				Exec sp_executesql @sqlupdate  

				-- Insert a zero point record into history to help identify that this tip is enrolled.
				set @sqlinsert=''Insert into '' + QuoteName(@DBName) + N''.dbo.history (tipnumber, histdate, trancode, trancount, points, description, ratio, overage)
								select dim_selfenroll_tipnumber, dim_selfenroll_created, ''''TP'''', ''''1'''', ''''0'''', ''''Transfer Standard'''', ''''1'''', ''''0''''
								from selfenroll
								where left(dim_selfenroll_tipnumber,3) = @TipFirst and dim_selfenroll_tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.history) ''
				Exec sp_executesql @sqlinsert, N''@TipFirst char(3)'', 	@TipFirst=@TipFirst
		
		fetch HarrisFI_crsr into @TipFirst 

	end	

	Fetch_Error:
	close HarrisFI_crsr
	deallocate HarrisFI_crsr

' 
END
GO
