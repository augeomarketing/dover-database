USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcessBonusIn]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessBonusIn]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcessBonusIn]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessBonusIn]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spBonusFileProcessBonusIn] 
AS

declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DateIn varchar(10), @DBName nchar(100)

set @Datein=''05/31/2007''
set @DBName=''525UnionBankConsumer''

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, [Card Number], [10000 Pts]	
from [BonusIn]

update transumbonus
set ratio=''1'', trancode=''BI'', description=''Bonus Points Increase'', histdate=@datein, numdebit=''1'', overage=''0''

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '''' '''', ratio, overage
			from transumbonus''
Exec sp_executesql @SQLInsert  
 
set @SQLUpdate=''update '' + QuoteName(@DBName) + N''.dbo.customer
	set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)
	where exists(select tipnumber from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)''
Exec sp_executesql @SQLupdate' 
END
GO
