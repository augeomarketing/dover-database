USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateOnlineBonuses]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateOnlineBonuses]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateOnlineBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateOnlineBonuses]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGenerateOnlineBonuses] @EndDate varchar(10)
AS 

/*************************************************************/
/*                                                           */
/*   Procedure to generate the Online sign up bonus and      */
/*   the E-Statement sign up bonus                           */
/*                                                           */
/*************************************************************/
declare @CISNO nchar(8), @ACCTNO nchar(16), @trancode char(2), @TRANNO nchar(6), @TRANAMT float(8), @Tipnumber varchar(15), @ratio float(8), @overage numeric(5), 
 @description varchar(50), @awarded char(1), @secid varchar(50)

set @tranno=''1''
set @ratio=''1''
set @overage=''0''
set @acctno='' ''
set @secid='' ''
/*************************************************************/
/*   Section to do Online sign up bonus                      */
/*************************************************************/
set @trancode=''BS''
set @tranamt=''500''
set @description=''Online Registration Bonus''
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Signon_Bonus_Table TABLE                         */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from Web_Registration
where awarded is null
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @tipnumber
/******************************************************************************/	

if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	if left(@tipnumber,3)=''508'' and @tipnumber in (select tipnumber from [508WoodtrustConsumer].dbo.Customer)
		begin
			insert into [508WoodtrustConsumer].dbo.history
			values(@tipnumber, @acctno, @enddate, @trancode, @tranno, @tranamt, @description, @secid, @ratio, @overage)
			
			update [508WoodtrustConsumer].dbo.customer
			set runavailable=runavailable + @tranamt, runbalance=runbalance + @tranamt
			where tipnumber=@tipnumber
		
			update Web_Registration
			set awarded=''Y''
			where current of tip_crsr

			goto Next_Record
		end
	else
	if left(@tipnumber,3)=''509'' and @tipnumber in (select tipnumber from [509WoodtrustCommercial].dbo.Customer)
		begin
			insert into [509WoodtrustCommercial].dbo.history
			values(@tipnumber, @acctno, @enddate, @trancode, @tranno, @tranamt, @description, @secid, @ratio, @overage)
			
			update [509WoodtrustCommercial].dbo.customer
			set runavailable=runavailable + @tranamt, runbalance=runbalance + @tranamt
			where tipnumber=@tipnumber
		
			update Web_Registration
			set awarded=''Y''
			where current of tip_crsr
			
			goto Next_Record
		end
	else
	if left(@tipnumber,3)=''511'' and @tipnumber in (select tipnumber from [511NEBATConsumer].dbo.Customer)
		begin
			insert into [511NEBATConsumer].dbo.history
			values(@tipnumber, @acctno, @enddate, @trancode, @tranno, @tranamt, @description, @secid, @ratio, @overage)
			
			update [511NEBATConsumer].dbo.customer
			set runavailable=runavailable + @tranamt, runbalance=runbalance + @tranamt
			where tipnumber=@tipnumber

			update Web_Registration
			set awarded=''Y''
			where current of tip_crsr
		
			goto Next_Record
		end
	else
	if left(@tipnumber,3)=''512'' and @tipnumber in (select tipnumber from [512NEBATCommercial].dbo.Customer)
		begin
			insert into [512NEBATCommercial].dbo.history
			values(@tipnumber, @acctno, @enddate, @trancode, @tranno, @tranamt, @description, @secid, @ratio, @overage)
			
			update [512NEBATCommercial].dbo.customer
			set runavailable=runavailable + @tranamt, runbalance=runbalance + @tranamt
			where tipnumber=@tipnumber
		
			update Web_Registration
			set awarded=''Y''
			where current of tip_crsr

			goto Next_Record
		end
	

Next_Record:
		fetch tip_crsr into @tipnumber
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr' 
END
GO
