USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPCaptureOverdueFlag]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPCaptureOverdueFlag]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPCaptureOverdueFlag]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPCaptureOverdueFlag]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 6/2009
-- Description:	Capture cards that are overdue
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPCaptureOverdueFlag]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   -- Insert statements for procedure here

	declare @SQLUpdate nvarchar(1000), @time char(8), @timeA char(6), @curdte char(8), @Institution_ID char(5), @RewardPlan char(3) 

	--Truncate table dbo.FBOP_Overdue_Flag

	insert into dbo.FBOP_Overdue_Flag (Acctid)
	select distinct substring(Acctnum, 6, 16)
	from TS2Creditin
	where OverdueFlag <>0 and substring(Acctnum, 6, 16) not in (select acctid from dbo.FBOP_Overdue_Flag)

	delete from dbo.FBOP_Overdue_Flag
	where acctid in (select distinct substring(Acctnum, 6, 16) from TS2Creditin where OverdueFlag =0)

END
' 
END
GO
