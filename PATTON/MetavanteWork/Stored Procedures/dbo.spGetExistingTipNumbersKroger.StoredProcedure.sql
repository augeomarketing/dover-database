USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbersKroger]    Script Date: 04/12/2010 08:22:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbersKroger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetExistingTipNumbersKroger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbersKroger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Sarah Blanchette
-- Create date: 10/28/09
-- Description:	<Description,,>

-- Date: 4/12/2010
-- By: S. Blanchette
-- Change: Add check for Loyalty number
-- SCAN: SEB001
-- =============================================
CREATE PROCEDURE [dbo].[spGetExistingTipNumbersKroger] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Insert statements for procedure here
declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/*  Get tipnumber based on account number      */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.acctnum=b.acctid ''
Exec sp_executesql @SQLUpdate

/*  Get tipnumber based on old account number  */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.oldcc=b.acctid ''
Exec sp_executesql @SQLUpdate

/* Get tipnumber based on Loyalty number */
/* SCAN SEB001 */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.customer b
		where a.tipnumber is null and a.NA6=b.misc2 ''
Exec sp_executesql @SQLUpdate

delete from cardsin
where tipnumber is null and status<>''A''

END
' 
END
GO
