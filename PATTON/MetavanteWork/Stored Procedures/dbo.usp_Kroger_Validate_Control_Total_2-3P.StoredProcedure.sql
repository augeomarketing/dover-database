USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_Kroger_Validate_Control_Total_2-3P]    Script Date: 06/29/2016 15:48:16 ******/
DROP PROCEDURE [dbo].[usp_Kroger_Validate_Control_Total_2-3P]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[usp_Kroger_Validate_Control_Total_2-3P] 
		@TipForRun		char(3),
--		@type			char(1),
		@EndDate  date,
        @errorrows	    int = 0 OUTPUT 

AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Records Loaded Match the trailer record                         */
/*                                                                            */
/******************************************************************************/
 
declare @CountDet		int
declare @CountTot		int
declare @SumDet		int
declare @SumTot		int


-- THIS SECTION CHECKS TO SEE THAT ALL THAT WAS SENT WAS LOADED TO RAWIMPORT
-- IF MATCH THEN CONTINUES OTHERWISE IT RETURNS ERROR.
set @CountDet = (select COUNT(*) from Rewardsnow.dbo.RNIRawImport With (NoLock) 
								where sid_dbprocessinfo_dbnumber='5KK'
								and sid_rniimportfiletype_id=5)

set @CountTot = (select cast(substring(dim_rnirawimport_field01,15,10) as int)  from rewardsnow.dbo.RNIRawImport With (NoLock) 
							where sid_dbprocessinfo_dbnumber='5KK'
							and sid_rniimportfiletype_id=5
							and left(dim_rnirawimport_field01,2)='09')
							

--CHECK IF WHAT WAS LOADED MATCHES THE TRAILER RECORD
if (@CountDet <>@CountTot)
	Begin
		set @errorrows = 1
		Return
	End
else 
	set @errorrows = 0

-- Generate the total number
	set @SumTot = 						
	cast(
		(select isnull(sum(cast(substring(dim_rnirawimport_field01,20,14) as decimal (14,2)) * 100),'0')  from rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber='5KK'
									and sid_rniimportfiletype_id=5
									and left(dim_rnirawimport_field01,2)='02'
									and SUBSTRING(dim_rnirawimport_field01,34,1) = '+' )
		-							
		(select isnull(sum(cast(substring(dim_rnirawimport_field01,20,14) as decimal (14,2)) * 100),'0')   from rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber='5KK'
									and sid_rniimportfiletype_id=5
									and left(dim_rnirawimport_field01,2)='02'
									and SUBSTRING(dim_rnirawimport_field01,34,1) = '-')
	as int)

--THIS LOADS AMOUNTS INTO RECONCILLIATION FILE	
--KROGER
	--If @type = '2' 
	--Begin	
	--	insert into [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
	--			([TipFirst],[ProcessDate], [ValueInPoint2])
	--	select 	@TipForRun, CAST(getdate() as DATE), @SumTot
	--End
	--else
	--Begin	
		update [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
		set [ValueInPoint3] = @SumTot
		where TipFirst = @TipForRun and ProcessDate = @EndDate
		
		--insert into [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
		--		([TipFirst],[ProcessDate], [ValueInPoint3])
		--select 	@TipForRun, CAST(getdate() as DATE), @SumTot
	--End	


--THIS SETS THE TIP FIRST
	update Rewardsnow.dbo.RNIRawImport
	set sid_dbprocessinfo_dbnumber= @TipForRun
	where sid_dbprocessinfo_dbnumber='5KK'
	and sid_rniimportfiletype_id = 5
	
-- This is to delete the Header and trailer record if counts are good.			
	Delete from Rewardsnow.dbo.RNIRawImport 
	where sid_dbprocessinfo_dbnumber = @TipForRun
	and sid_rniimportfiletype_id = 5
	and left(dim_rnirawimport_field01,2)in ('01','09')
GO
