USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHandleOptOutsDebitCardsLakeside]    Script Date: 07/12/2010 16:25:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsDebitCardsLakeside]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHandleOptOutsDebitCardsLakeside]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsDebitCardsLakeside]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2010
-- Description:	Opt outs for Lakeside
-- =============================================
CREATE PROCEDURE [dbo].[spHandleOptOutsDebitCardsLakeside]
	-- Add the parameters for the stored procedure here
	@TipPrefix char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	/* Remove DebitCardInKroger for Opt Out people  */
	delete from  debitcardinLakeside
	where exists(select * from optoutcontrol where acctnumber= debitcardinLakeside.[Primary Acct Number])

	/****************************/
	/* START SEB001             */
	/****************************/
	delete from  debitcardinLakeside
	where exists(select * from DDAOptOutControl where DDANUMBER= debitcardinLakeside.[DDA Acct Number] and left(tipnumber,3)=@TipPrefix )

END
' 
END
GO
