USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[pWelcomeKitWintrust]    Script Date: 12/16/2010 10:19:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKitWintrust]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pWelcomeKitWintrust]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[pWelcomeKitWintrust]    Script Date: 12/16/2010 10:19:28 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pWelcomeKitWintrust] @EndDate varchar(10), @TipFirst nchar(3)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)


set @sqlInsert=N'insert into dbo.Welcomekit_work SELECT distinct c.TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5), custid FROM ' + QuoteName(@DBName) + N' .dbo.customer c join ' + QuoteName(@DBName) + N' .dbo.affiliat a on c.tipnumber = a.tipnumber WHERE (c.DATEADDED = @EndDate AND STATUS <> ''C'') '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate

GO


