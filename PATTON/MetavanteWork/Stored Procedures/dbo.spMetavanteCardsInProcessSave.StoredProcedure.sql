USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteCardsInProcessSave]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteCardsInProcessSave]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteCardsInProcessSave]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteCardsInProcessSave]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spMetavanteCardsInProcessSave]
as  


/******************************************************/
/* Section to get all names on same record            */
/******************************************************/
declare @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @tipnumber nchar(15)

delete from cardname

declare Cardsin_View_by_Tipnumber_crsr cursor
for select tipnumber, NA1, NA2,NA3, NA4, NA5, NA6
from Cardsin_View_by_Tipnumber
where status<>''C''
/* where ddanum is not null */
/* for update */ 
/*                                                                            */
open Cardsin_View_by_Tipnumber_crsr
/*                                                                            */
fetch Cardsin_View_by_Tipnumber_crsr into @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from cardname where Tipnumber=@Tipnumber)
	begin
		insert cardname(Tipnumber, NA1, NA2, NA3, NA4, NA5, NA6) values(@Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6)
		goto Next_Record1
	end
	
	else
		if exists(select * from Cardname where Tipnumber=@Tipnumber and (substring(NA2,1,1) like '' '' or NA2 is null))
		begin
			update Cardname
			set NA2=@NA1, NA3=@NA2, NA4=@NA3, NA5=@NA4, NA6=@NA5
			where Tipnumber=@Tipnumber
			goto Next_Record1
		end
		else
			if exists(select * from Cardname where Tipnumber=@Tipnumber and (substring(NA3,1,1) like '' '' or NA3 is null))
			begin
				update Cardname
				set NA3=@NA1, NA4=@NA2, NA5=@NA3, NA6=@NA4
				where Tipnumber=@Tipnumber
				goto Next_Record1
			end
			else
				if exists(select * from Cardname where Tipnumber=@Tipnumber and (substring(NA4,1,1) like '' '' or NA4 is null))
				begin
					update Cardname
					set NA4=@NA1, NA5=@NA2, NA6=@NA3
					where Tipnumber=@Tipnumber
					goto Next_Record1
				end
				else
					if exists(select * from Cardname where Tipnumber=@Tipnumber and (substring(NA5,1,1) like '' '' or NA5 is null))
					begin
						update Cardname
						set NA5=@NA1, NA6=@NA2
						where Tipnumber=@Tipnumber
						goto Next_Record1
					end
					else
						if exists(select * from Cardname where Tipnumber=@Tipnumber and (substring(NA6,1,1) like '' '' or NA6 is null))
						begin
							update Cardname
							set NA6=@NA1
							where Tipnumber=@Tipnumber
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch Cardsin_View_by_Tipnumber_crsr into @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6
	
end

Fetch_Error1:
close Cardsin_View_by_Tipnumber_crsr
deallocate Cardsin_View_by_Tipnumber_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update Cardname
set na2=null
where na2=na1

update Cardname
set na3=null
where na3=na1 or na3=na2

update Cardname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update Cardname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update Cardname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update Cardname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na1 is null or substring(na1,1,1) like '' ''

	update Cardname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
	where na2 is null or substring(na2,1,1) like '' ''

	update Cardname
	set na3=na4, na4=na5, na5=na6, na6=null
	where na3 is null or substring(na3,1,1) like '' ''

	update Cardname
	set na4=na5, na5=na6, na6=null
	where na4 is null or substring(na4,1,1) like '' ''

	update Cardname
	set na5=na6, na6=null
	where na5 is null or substring(na5,1,1) like '' ''

	set @count= @count + 1
end

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Update names to the Cardsin Table                                       */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
	update Cardsin
	set NA1=(select NA1 from Cardname where Tipnumber=Cardsin.Tipnumber),
	    NA2=(select NA2 from Cardname where Tipnumber=Cardsin.Tipnumber),
	    NA3=(select NA3 from Cardname where Tipnumber=Cardsin.Tipnumber),
	    NA4=(select NA4 from Cardname where Tipnumber=Cardsin.Tipnumber),
	    NA5=(select NA5 from Cardname where Tipnumber=Cardsin.Tipnumber),
	    NA6=(select NA6 from Cardname where Tipnumber=Cardsin.Tipnumber)

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/


update Cardsin
set joint=''J''
where NA2 is not null and substring(na2,1,1) not like '' ''

update Cardsin
set joint=''S''
where NA2 is null or substring(na2,1,1) like '' ''' 
END
GO
