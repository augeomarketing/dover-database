USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_ImportDemographics]    Script Date: 01/18/2012 10:22:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_ImportDemographics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_ImportDemographics]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_ImportDemographics]    Script Date: 01/18/2012 10:22:18 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_FIS_ImportDemographics] 
	@Tipfirst varchar(3)

AS 

DECLARE @DBName varchar(50),	@SQLUpdate nvarchar(2000),	@SQLInsert nvarchar(2000),	@SQLSet nvarchar(2000),			@SQLIf nvarchar(2000),
		@SQLCursor nvarchar(2000)

IF OBJECT_ID(N'tempdb..#custin') IS NOT NULL
	DROP TABLE #custin

CREATE TABLE #custin
	(
	CUSTIN_ID BIGINT IDENTITY(1,1) PRIMARY KEY, 
	[ACCT_NUM] [nvarchar](25) NULL,	[NAMEACCT1] [nvarchar](40) NULL,	[NAMEACCT2] [nvarchar](40) NULL,	[NAMEACCT3] [nvarchar](40) NULL,
	[NAMEACCT4] [nvarchar](40) NULL,[NAMEACCT5] [nvarchar](40) NULL,	[NAMEACCT6] [nvarchar](40) NULL,	[STATUS] [nvarchar](1) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,[ADDRESS1] [nvarchar](40) NULL,		[ADDRESS2] [nvarchar](40) NULL,		[ADDRESS4] [nvarchar](40) NULL,
	[CITY] [nvarchar](38) NULL,		[STATE] [char](2) NULL,				[ZIP] [nvarchar](15) NULL,			[LASTNAME] [nvarchar](40) NULL,
	[HOMEPHONE] [nvarchar](10) NULL,[WORKPHONE] [nvarchar](10) NULL,	[DATEADDED] [nvarchar](10) NULL,	[BUSINESFLG] [nvarchar](1) NULL,
	[SEGCODE] [nvarchar](2) NULL,	[EMAIL] [nvarchar](40) NULL,		[CUREOMBAL] [nvarchar](11) NULL,	[PAYEOMBAL] [nvarchar](11) NULL,
	[FEEEOMBAL] [nvarchar](11) NULL,[CRDISUEDT] [nvarchar](10) NULL,	[CRDACTIVDT] [nvarchar](10) NULL,	[MISC1] [nvarchar](20) NULL,
	[MISC2] [nvarchar](20) NULL,	[MISC3] [nvarchar](20) NULL,		[DDANUM] [nchar](13) NULL,			[SSN] [nchar](9) NULL,
	[CardType] [char](1) NULL
	)

DROP TABLE	wrkcustin

SELECT		*
INTO		wrkcustin
FROM		custin
ORDER BY	tipnumber, status desc


/*****************************************************************/
/*  DEAL WITH CUSTOMER FILE                                      */
/*****************************************************************/
SET	@SQLUpdate = '
	UPDATE [<<DBNAME>>].dbo.Customer
	SET
	AcctName1 = b.NAMEACCT1,	AcctName2 = b.NAMEACCT2,	AcctName3 = b.NAMEACCT3,
	AcctName4 = b.NAMEACCT4,	AcctName5 = b.NAMEACCT5,	AcctName6 = b.NAMEACCT6,
	Address1 = b.Address1,		Address2 = b.Address2,		Address4 = b.Address4,
	City = rtrim(b.City),		State = rtrim(b.STATE),		Zipcode = rtrim(b.zip),
	HomePhone = b.HomePhone,	WorkPhone = b.WorkPhone,	lastname = b.lastname,
	misc1 = b.SSN,				misc2 = b.misc2,			misc3 = b.misc3,
	status = b.STATUS
	FROM [<<DBNAME>>].dbo.customer a join wrkcustin b ON a.TIPNUMBER = b.TIPNUMBER 
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQLUpdate 


INSERT INTO #custin
	(
	ACCT_NUM,	NAMEACCT1,	NAMEACCT2,	NAMEACCT3,	NAMEACCT4,	NAMEACCT5,	NAMEACCT6,
	STATUS,		TIPNUMBER,	ADDRESS1,	ADDRESS2,	ADDRESS4,	CITY,		STATE,		ZIP,
	LASTNAME,	HOMEPHONE,	WORKPHONE,	DATEADDED,	BUSINESFLG,	SEGCODE,	EMAIL,
	CUREOMBAL,	PAYEOMBAL,	FEEEOMBAL,	CRDISUEDT,	CRDACTIVDT,	MISC1,		MISC2,		MISC3,
	DDANUM,		SSN,		CardType
	)
SELECT
	ACCT_NUM,	NAMEACCT1,	NAMEACCT2,	NAMEACCT3,	NAMEACCT4,	NAMEACCT5,	NAMEACCT6,
	STATUS,		TIPNUMBER,	ADDRESS1,	ADDRESS2,	ADDRESS4,	CITY,		STATE,		ZIP,
	LASTNAME,	HOMEPHONE,	WORKPHONE,	DATEADDED,	BUSINESFLG,	SEGCODE,	EMAIL,
	CUREOMBAL,	PAYEOMBAL,	FEEEOMBAL,	CRDISUEDT,	CRDACTIVDT,	MISC1,		MISC2,		MISC3,
	DDANUM,		SSN,		CardType
FROM	CUSTIN
WHERE	TIPNUMBER LIKE @tipfirst +'%'

DELETE CI
FROM #CUSTIN CI
LEFT OUTER JOIN
	(
	SELECT TIPNUMBER, MIN(CUSTIN_ID) MINID
	FROM #custin
	GROUP BY TIPNUMBER
	) MINIDS
ON 
	CI.TIPNUMBER = MINIDS.TIPNUMBER
	AND CI.CUSTIN_ID = MINIDS.MINID
WHERE
	MINIDS.MINID IS NULL

SET	@SQLUpdate = '		
	INSERT INTO [<<DBNAME>>].dbo.customer 
	(	
	TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast,
	AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6,
	Address1, Address2, Address4, City, State, Zipcode, Status, StatusDescription,
	HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3, SegmentCode
	)
	SELECT 
	ci.TIPNUMBER, ''0'', ''0'', ''0'', LEFT(ci.tipnumber,3), right(ci.tipnumber,12), 
	ci.NAMEACCT1, ci.NAMEACCT2, ci.NAMEACCT3, ci.NAMEACCT4, ci.NAMEACCT5, ci.NAMEACCT6,
	ci.ADDRESS1, ci.ADDRESS2, ci.ADDRESS4, ci.CITY, ci.STATE, ci.ZIP, ''A'', '' '',
	ci.HOMEPHONE, ci.WORKPHONE, cast(ci.DATEADDED as datetime), ci.LASTNAME, ci.SSN, ci.MISC2, ci.MISC3, ci.Cardtype
	FROM	#custin ci LEFT OUTER JOIN [<<DBNAME>>].dbo.customer c on ci.tipnumber = c.tipnumber
	WHERE	c.tipnumber is null		
	and	ci.tipnumber is not null 
	and	left(ci.tipnumber,1)<>'' ''
	'
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC sp_executesql @SQLUpdate

/*****************************************************************/
/*  UPDATE STATUS DESCRIPTION IN CUSTOMER TABLE                  */
/*****************************************************************/
SET	@SQLUpdate = '
	UPDATE [<<DBNAME>>].dbo.customer 
	SET	StatusDescription = (SELECT	StatusDescription 
							 FROM	[<<DBNAME>>].dbo.Status
							 WHERE	Status = [<<DBNAME>>].dbo.customer.STATUS)
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQLUpdate

/*****************************************************************/
/*  DEAL WITH AFFILIAT TABLE                                     */
/*****************************************************************/
SET	@SQLUpdate = '
	UPDATE [<<DBNAME>>].dbo.AFFILIAT 
	SET 
	lastname = b.lastname,	acctstatus = b.status,
	Secid = b.SSN,			CustID = b.DDANUM 
	FROM	[<<DBNAME>>].dbo.affiliat a JOIN wrkcustin b ON a.acctid = b.acct_num			
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQLUpdate 

TRUNCATE TABLE #custin

INSERT INTO #custin
	(
	ACCT_NUM,	NAMEACCT1,	NAMEACCT2,	NAMEACCT3,	NAMEACCT4,	NAMEACCT5,	NAMEACCT6,
	STATUS,		TIPNUMBER,	ADDRESS1,	ADDRESS2,	ADDRESS4,	CITY,		STATE,		ZIP,
	LASTNAME,	HOMEPHONE,	WORKPHONE,	DATEADDED,	BUSINESFLG,	SEGCODE,	EMAIL,
	CUREOMBAL,	PAYEOMBAL,	FEEEOMBAL,	CRDISUEDT,	CRDACTIVDT,	MISC1,		MISC2,		MISC3,
	DDANUM,		SSN,		CardType
	)
SELECT
	ACCT_NUM,	NAMEACCT1,	NAMEACCT2,	NAMEACCT3,	NAMEACCT4,	NAMEACCT5,	NAMEACCT6,
	STATUS,		TIPNUMBER,	ADDRESS1,	ADDRESS2,	ADDRESS4,	CITY,		STATE,		ZIP,
	LASTNAME,	HOMEPHONE,	WORKPHONE,	DATEADDED,	BUSINESFLG,	SEGCODE,	EMAIL,
	CUREOMBAL,	PAYEOMBAL,	FEEEOMBAL,	CRDISUEDT,	CRDACTIVDT,	MISC1,		MISC2,		MISC3,
	DDANUM,		SSN,		CardType
FROM	CUSTIN
WHERE	TIPNUMBER LIKE @tipfirst +'%'

DELETE CI
FROM #CUSTIN CI
LEFT OUTER JOIN
	(
	SELECT ACCT_NUM, MIN(CUSTIN_ID) MINID
	FROM #custin
	GROUP BY ACCT_NUM
	) MINIDS
ON 
	CI.ACCT_NUM = MINIDS.ACCT_NUM
	AND CI.CUSTIN_ID = MINIDS.MINID
WHERE
	MINIDS.MINID IS NULL

SET	@SQLUpdate = '		
	INSERT INTO [<<DBNAME>>].dbo.affiliat
	(
	ACCTID, TIPNUMBER, AcctType, DATEADDED, SecID,
	AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
	)
	SELECT 
	rtrim(ci.ACCT_NUM), ci.TIPNUMBER, ci.Cardtype, cast(ci.DATEADDED as datetime), ci.SSN, 
	''A'', '' '', ci.LASTNAME, ''0'', ci.DDANUM 
	FROM	#custin ci LEFT OUTER JOIN [<<DBNAME>>].dbo.affiliat a on ci.ACCT_NUM = a.ACCTID
	WHERE	A.ACCTID is null		
		and	ci.STATUS = ''A''
		and	ci.ACCT_NUM is not null 
		and	left(ci.ACCT_NUM,1)<>'' ''
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQLUpdate
		
DROP TABLE #custin

/*****************************************************************/
/*  UPDATE ACCOUNT TYPE IN AFFILIAT TABLE                        */
/*****************************************************************/
SET	@SQLUpdate = '
	UPDATE	[<<DBNAME>>].dbo.affiliat
	SET		accttype = ''Credit''
	WHERE	accttype = ''C''
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQLUpdate

SET	@SQLUpdate = '
	UPDATE	[<<DBNAME>>].dbo.affiliat
	SET		accttype = ''Debit''
	WHERE	accttype = ''D''
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
Exec	sp_executesql @SQLUpdate

/*****************************************************************/
/*  UPDATE ACCOUNT TYPE DESCRIPTION IN AFFILIAT TABLE            */
/*****************************************************************/
SET	@SQLUpdate = '
	UPDATE	[<<DBNAME>>].dbo.affiliat
	SET		accttypedesc = (SELECT	Accttypedesc 
							FROM	[<<DBNAME>>].dbo.accttype
							WHERE	accttype = [<<DBNAME>>].dbo.affiliat.Accttype)
	'
SET		@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQLUpdate

GO
