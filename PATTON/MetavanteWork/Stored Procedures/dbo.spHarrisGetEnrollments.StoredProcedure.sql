USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisGetEnrollments]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisGetEnrollments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisGetEnrollments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisGetEnrollments]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<S. Blanchette>
-- Create date: <11/08>
-- Description:	<Proc to gather enrollment data from web>
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisGetEnrollments] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

	Insert into SelfEnroll
	Select *, '''', '''' from SelfEnrollwork where dim_selfenroll_tipnumber not in (select dim_selfenroll_tipnumber from selfenroll)

	-- Update status in customer table to reflect people that have self enrolled
	--set @SQLUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer
	--					Set Status=''''A''''
	--					where Status=''''S'''' and 
	--							tipnumber in (select dim_selfenroll_tipnumber from SelfEnroll) ''
	set @SQLUpdate = ''Update cus
						set status = ''''A'''', dateadded = se.dim_selfenroll_created
						from '' + quotename(@DBName) + ''.dbo.customer cus join dbo.selfenroll se
						on cus.tipnumber = se.dim_selfenroll_tipnumber
						where status = ''''X''''   ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate = ''Update afl
						set acctstatus = ''''A''''
						from '' + quotename(@DBName) + ''.dbo.affiliat afl join dbo.selfenroll se
						on afl.tipnumber = se.dim_selfenroll_tipnumber
						where acctstatus = ''''X''''   ''
Exec sp_executesql @SQLUpdate

END' 
END
GO
