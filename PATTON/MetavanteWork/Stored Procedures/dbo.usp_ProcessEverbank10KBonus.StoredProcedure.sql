use MetavanteWork
go

IF OBJECT_ID(N'usp_ProcessEverbank10KBonus') IS NOT NULL
	DROP PROCEDURE usp_ProcessEverbank10KBonus
GO

CREATE PROCEDURE usp_ProcessEverbank10KBonus
AS

DECLARE @process varchar(255) = 'usp_ProcessEverbank10KBonus'
DECLARE @msg VARCHAR(MAX)

DELETE FROM RewardsNow.dbo.BatchDebugLog WHERE dim_batchdebuglog_process = @process

DECLARE @processingenddate DATE
--Handle legacy date strings
DECLARE @begindate NCHAR(10)
DECLARE @enddate NCHAR(10)

EXEC Metavantework.dbo.spGetDataProcessDates @begindate out, @enddate out

SET @processingenddate = CONVERT(DATE, @enddate)

set @msg = 'Process Begin for: ' + CONVERT(VARCHAR(10), @processingenddate, 101)
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg


set @msg = '---> Getting List of Procedures from RewardsNow.dbo.RNIBonusProgramFI'
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg

--GET LIST OF PROCEDURES FOR EVERBANK BONUS'S
DECLARE @proc TABLE
(
	sid_proc_id INT IDENTITY(1,1) PRIMARY KEY
	, dim_proc_procedure NVARCHAR(255)
)

INSERT INTO @proc (dim_proc_procedure)
SELECT DISTINCT dim_rnibonusprogramfi_storedprocedure FROM RewardsNow.dbo.RNIBonusProgramFI
WHERE sid_dbprocessinfo_dbnumber IN ('529', '52T')
	AND @processingenddate BETWEEN dim_rnibonusprogramfi_effectivedate and dim_rnibonusprogramfi_expirationdate

DECLARE @sid_proc_id INT = 1
DECLARE @max_sid_proc_id INT
DECLARE @dim_proc_procedure NVARCHAR(255)
DECLARE @err VARCHAR(MAX)

SELECT @max_sid_proc_id = MAX(sid_proc_id) FROM @proc

set @msg = '---> Procedures Found:' + CONVERT(varchar, @max_sid_proc_id)
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg


WHILE @sid_proc_id <= @max_sid_proc_id
BEGIN
	BEGIN TRY
		SELECT @dim_proc_procedure = dim_proc_procedure FROM @proc WHERE sid_proc_id = @sid_proc_id
		
		set @msg = '---> Executing Procedure:' + @dim_proc_procedure
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
		
		EXEC sp_executesql @dim_proc_procedure			
	
	END TRY
	BEGIN CATCH
		SET @err = ERROR_MESSAGE()
		
		set @msg = '--->---> ERROR EXECUTING PROCEDURE (:' + @dim_proc_procedure + ')'
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg

		set @msg = '--->---> ERROR:' + @err
		INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
		SELECT @process, @msg
	
	END CATCH

	SET @sid_proc_id = @sid_proc_id + 1
END

set @msg = 'Process End for: ' + CONVERT(VARCHAR(10), @processingenddate, 101)
INSERT INTO RewardsNow.dbo.BatchDebugLog (dim_batchdebuglog_process, dim_batchdebuglog_logentry)
SELECT @process, @msg


GO