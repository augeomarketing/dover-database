USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewCurrentMonthActivityLoad]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewCurrentMonthActivityLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewCurrentMonthActivityLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewCurrentMonthActivityLoad]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spNewCurrentMonthActivityLoad] 
	-- Add the parameters for the stored procedure here
	@EndDateParm varchar(10), 
	@TipFirst char(3) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @DBName varchar(50), 
			@SQLUpdate nvarchar(1000), 
			@SQLSelect nvarchar(1000), 
			@SQLTruncate nvarchar(1000), 
			@SQLInsert nvarchar(1000), 
			@monthbegin char(2)

	/*
	RDT 10/09/2006
	- Changed parameter to EndDateParm
	- added hh:mm:ss:mmm to End date
	*/

	Declare @EndDate DateTime                         --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006


	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	Truncate Table dbo.Current_Month_Activity 

	set @SQLInsert=''INSERT INTO dbo.Current_Month_Activity(tipnumber, EndingPoints)
       				select tipnumber, runavailable
				from '' + QuoteName(@DBName) + N''.dbo.customer ''
	Exec sp_executesql @SQLInsert
			
	/* Load the into wrkfile to accumulate points by tipnumber         */
	--drop table wrkcurrent
	if exists(select * from sysobjects where xtype=''u'' and name = ''wrkcurrent'')
			Begin
				drop table dbo.wrkcurrent 
			End 

	set @SQLSelect=N''select tipnumber, sum(points) as points
			into wrkcurrent
			from '' + QuoteName(@DBName) + N''.dbo.history
			where histdate>@enddate and ratio=''''1.0'''' 
			group by tipnumber ''
	Exec sp_executesql @SQLSelect, N''@EndDate datetime'', @EndDate=@EndDate

	/* Load the current activity table with increases for the current month         */
	set @SQLUpdate=N''update dbo.Current_Month_Activity set increases = increases +
			b.points
			from dbo.Current_Month_Activity a, wrkcurrent b 
			where b.tipnumber=a.tipnumber ''
	Exec sp_executesql @SQLUpdate

	/* Load the into wrkfile to accumulate points by tipnumber         */
	--drop table wrkcurrent
	if exists(select * from sysobjects where xtype=''u'' and name = ''wrkcurrent'')
			Begin
				drop table wrkcurrent 
			End 

	set @SQLSelect=N''select tipnumber, sum(points) as points
			into wrkcurrent
			from '' + QuoteName(@DBName) + N''.dbo.history
			where histdate>@enddate and ratio=''''-1.0'''' 
			group by tipnumber ''
	Exec sp_executesql @SQLSelect, N''@EndDate datetime'', @EndDate=@EndDate

	/* Load the current activity table with decreases for the current month         */
	set @SQLUpdate=N''update dbo.Current_Month_Activity set decreases = decreases +
			b.points
			from dbo.Current_Month_Activity a, wrkcurrent b
			where b.tipnumber=a.tipnumber ''
	Exec sp_executesql @SQLUpdate

	/* Load the calculate the adjusted ending balance        */
	--update Current_Month_Activity
	--set adjustedendingpoints=endingpoints - increases + decreases
	set @SQLUpdate=N''update dbo.Current_Month_Activity set adjustedendingpoints=endingpoints - increases + decreases ''
	Exec sp_executesql @SQLUpdate
END
' 
END
GO
