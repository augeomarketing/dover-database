USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_DeleteClosedTIPs]    Script Date: 01/26/2012 14:11:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_DeleteClosedTIPs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_DeleteClosedTIPs]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_DeleteClosedTIPs]    Script Date: 01/26/2012 14:11:20 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_FIS_DeleteClosedTIPs] 
	@TipFirst varchar(3), @Enddate varchar(10)

AS

DECLARE	@SQL nvarchar(max), 
		@datedeleted datetime,
		@DateToDelete char(6), 
		@dateclosed datetime, 
		@newmonth nchar(2), 
		@newyear nchar(4)

SET	@datedeleted = @Enddate

/********************************************************************/
/*  Code to build work table with tipnumbers that are to be deleted */
/********************************************************************/
SET	@newmonth = cast(datepart(month, @datedeleted) as char(2)) 
SET	@newyear = cast(datepart(yyyy,@datedeleted) as char(4)) 
IF	CONVERT(int, @newmonth) < '10' 
	BEGIN
		SET @newmonth = '0' + left(@newmonth,1)
	END
SET	@DateToDelete = @newmonth + @newyear
--Drop Table #ClosedCustomers
CREATE TABLE #ClosedCustomers (Tipnumber nchar(15))
SET	@SQL = N'
	INSERT INTO	[#ClosedCustomers] 
				(Tipnumber)
    SELECT		TIPNumber 
    FROM		[<<DBNAME>>].dbo.Customer_Closed 
    WHERE		DateToDelete <= ''<<DATETODELETE>>''
		and		tipnumber not in (SELECT tipnumber FROM [<<DBNAME>>].dbo.history WHERE CAST(histdate as DATE) > ''<<ENDDATE>>'')
    '
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
SET		@SQL =	REPLACE(@SQL, '<<DATETODELETE>>', @DateToDelete)	
SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @Enddate)
EXEC	sp_executesql @SQL


SET	@SQL = N'
	INSERT INTO	[<<DBNAME>>].dbo.CustomerDeleted 
    SELECT		TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, 
				Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, 
				HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, 
				Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, 
				RunBalanceNew, RunAvaliableNew, ''<<DATEDELETED>>'' 
	FROM		[<<DBNAME>>].dbo.Customer 
	WHERE		status = ''C'' and Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers])
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
SET		@SQL =	REPLACE(@SQL, '<<DATEDELETED>>', @datedeleted)	
EXEC sp_executesql @SQL

SET	@SQL = N'
	INSERT INTO [<<DBNAME>>].dbo.AffiliatDeleted 
				(TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
    SELECT		 TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, ''<<DATEDELETED>>''
	FROM		[<<DBNAME>>].dbo.affiliat 
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
SET		@SQL =	REPLACE(@SQL, '<<DATEDELETED>>', @datedeleted)	
EXEC	sp_executesql @SQL

SET	@SQL = N'
	INSERT INTO	[<<DBNAME>>].dbo.historyDeleted
				(TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
    SELECT		 TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, ''<<DATEDELETED>>''
	FROM		[<<DBNAME>>].dbo.history
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
SET		@SQL =	REPLACE(@SQL, '<<DATEDELETED>>', @datedeleted)	
EXEC	sp_executesql @SQL


SET	@SQL = N'
	DELETE FROM	[<<DBNAME>>].dbo.affiliat 
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL

SET	@SQL = N'
	DELETE FROM [<<DBNAME>>].dbo.history 
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL

SET	@SQL = N'
	DELETE FROM	[<<DBNAME>>].dbo.customer 
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL

SET	@SQL = N'
	DELETE FROM	[<<DBNAME>>].dbo.[Beginning_Balance_Table]
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL

SET	@SQL = N'
	DELETE FROM [<<DBNAME>>].dbo.[Customer_Closed]
	WHERE		Tipnumber in (SELECT Tipnumber FROM [#ClosedCustomers]) 
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL
GO


