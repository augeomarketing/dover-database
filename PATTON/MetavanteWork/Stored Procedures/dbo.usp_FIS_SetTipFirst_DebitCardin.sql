USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_SetTipFirst_DebitCardin]    Script Date: 05/25/2012 14:43:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_SetTipFirst_DebitCardin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_SetTipFirst_DebitCardin]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_SetTipFirst_DebitCardin]    Script Date: 05/25/2012 14:43:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		S.Blanchette
-- Create date: 6/11/2009
-- Description:	to populate tip first in debitcardin using lookup to table 
-- =============================================
CREATE PROCEDURE [dbo].[usp_FIS_SetTipFirst_DebitCardin]
	@Tipfirst varchar(3)
AS

BEGIN
	SET NOCOUNT ON;

	IF @Tipfirst not in (SELECT rtrim(TipFirst) FROM dbo.DebitBins)
		BEGIN
			UPDATE	dbo.DebitCardin
			SET		TipFirst = @Tipfirst
		END
	ELSE 
		BEGIN
			UPDATE	dci
			SET		TIPFIRST = dbs.Tipfirst
			FROM	dbo.DebitCardin dci
				INNER JOIN	dbo.DebitBins dbs
				ON	RTRIM(dci.CLIENT) LIKE RTRIM(dbs.BIN)			
			WHERE	dbs.TipFirst = @Tipfirst
		END		
		

	DELETE FROM	dbo.DebitCardin WHERE TipFirst is null

END



GO


