USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerPost3PTBonusFile]    Script Date: 12/06/2010 11:09:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerPost3PTBonusFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerPost3PTBonusFile]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerPost3PTBonusFile]    Script Date: 12/06/2010 11:09:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S. Blanchette
-- Create date: 11/06/2009
-- Description:	Post points for 3pt file
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerPost3PTBonusFile] 
	-- Add the parameters for the stored procedure here
	@DateIn varchar(10), 
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Declare @DBName varchar(100), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

delete from transumbonus

insert into transumbonus (tipnumber, acctno, histdate, trancode, numcredit, amtcredit, numdebit, amtdebit, description, ratio, overage)
select tipnumber, [LoyaltyNumber], @datein, 'G3', '0', '0', '1', cast(amount as int), (select description from rewardsnow.dbo.trantype where trancode='G3'), ratio='1', overage='0'
from [Kroeger3PT]
where left(tipnumber,3) = @TipFirst and Sign='+'

insert into transumbonus (tipnumber, acctno, histdate, trancode, numcredit, amtcredit, numdebit, amtdebit, description, ratio, overage)
select tipnumber, [LoyaltyNumber], @datein, 'G8', '1', cast(amount as int), '0', '0', (select description from rewardsnow.dbo.trantype where trancode='G8'), ratio='-1', overage='0'
from [Kroeger3PT]
where left(tipnumber,3) = @TipFirst and Sign='-'

/*********************************************************************/
/*  start SEB001 Remove records that do not have a customer record   */
/*********************************************************************/
set @SQLDelete='Delete from dbo.transumbonus
				where tipnumber not in (select tipnumber from ' + QuoteName(@DBName) + N'.dbo.customer) '
Exec sp_executesql @SQLDelete
/*********************************************************************/
/*  end SEB001 Remove records that do not have a customer record   */
/*********************************************************************/

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.history 
		select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
		from transumbonus where trancode=''G3'' '
exec sp_executesql @SQLInsert

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.history 
		select tipnumber, acctno, histdate, trancode, numcredit, amtcredit, description, '' '', ratio, overage
		from transumbonus where trancode=''G8'' '
exec sp_executesql @SQLInsert

set @sqlUpdate=N'update ' + QuoteName(@DBName) + N' .dbo.customer
		set runavailable=runavailable + (select sum(amtdebit - amtcredit) from transumbonus where tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber), 
			runbalance= runbalance + (select sum(amtdebit - amtcredit) from transumbonus where tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber)
		where exists(select tipnumber from transumbonus where tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber) '
exec sp_executesql @sqlUpdate

END

GO


