USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadCardsinWithDebitCards]    Script Date: 03/13/2012 11:13:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadCardsinWithDebitCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadCardsinWithDebitCards]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadCardsinWithDebitCards]    Script Date: 03/13/2012 11:13:31 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_LoadCardsinWithDebitCards]
as

--inserts records from debitcardin2 into Cardsin
insert into Cardsin(tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, 
					na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, 
					status, phone1, phone2, numpurch, purch, numret, amtret, period, Cardtype, SigvsPin)
select				tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, 
					na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, 
					status, phone1, phone2, numpurch, purch, numret, amtret, period, 'D', SigvsPin 
from	debitcardin2
order by ssn asc, ddanum asc


--inserts records from debitcardinLakeside into Cardsin
--insert into Cardsin(tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, 
--					na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, 
--					status, phone1, phone2, numpurch, purch, numret, amtret, period, Cardtype, SigvsPin)
--select				tipfirst, tipmid, ' ', [Social Security Number], [DDA Acct Number], [Primary Acct Number], [Cross reference card numer], 
--					[Full Name], [Secondary Name], ' ', ' ', ' ', [Rewards Member ID], [Address Line 1], [Address Line 2], ' ', [City / State], [Zip Code],
--					Status, [Home Phone], [Business Phone], numpurch, purch, numret, amtret, ' ', 'D', [Signature or PIN] 
--from	debitcardinLakeside

GO


