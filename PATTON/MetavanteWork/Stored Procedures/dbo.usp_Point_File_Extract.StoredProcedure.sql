USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_Point_File_Extract]    Script Date: 07/19/2016 14:28:42 ******/
DROP PROCEDURE [dbo].[usp_Point_File_Extract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Point_File_Extract]
	-- Add the parameters for the stored procedure here
AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DELETE FROM [52JKroegerPersonalFinance].dbo.Point_File_Template WHERE RunVersion > 7

UPDATE [52JKroegerPersonalFinance].dbo.Point_File_Template SET RunVersion += 1

INSERT INTO [52JKroegerPersonalFinance].dbo.Point_File_Template
(TransID, MemberNumber, RewardPoints, PointsLastUpdated, RunVersion)
SELECT	'3552' ,
		CAST(a.CustID as CHAR(10)), 
		CAST(ISNULL(h.POINTS,0) as CHAR(10)),
		CONVERT(CHAR(8),d.PointsUpdated,112),
		'1'
FROM	(select TIPNUMBER, MAX(ACCTID) as CustID from [52JKroegerPersonalFinance].dbo.AFFILIAT where AcctType = 'MEMBER' group by TIPNUMBER) a
	LEFT OUTER JOIN (select tipnumber, SUM(points*ratio) as POINTS from [52JKroegerPersonalFinance].dbo.HISTORY group by TIPNUMBER) h ON a.TIPNUMBER = h.TIPNUMBER
	INNER JOIN RewardsNow.dbo.dbprocessinfo d ON LEFT(a.tipnumber, 3) = d.DBNumber


INSERT INTO [52JKroegerPersonalFinance].dbo.Point_File_Template
(TransID, MemberNumber, RewardPoints, PointsLastUpdated, RunVersion)
SELECT	'3552' ,
		CAST(a.CustID as CHAR(10)),
		CAST(ISNULL(h.POINTS,0) as CHAR(10)),
		CONVERT(CHAR(8),d.PointsUpdated,112),
		'1'	
FROM	(select TIPNUMBER, MAX(ACCTID) as CustID from [52R].dbo.AFFILIAT where AcctType = 'MEMBER' group by TIPNUMBER) a
	LEFT OUTER JOIN (select tipnumber, SUM(points*ratio) as POINTS from [52R].dbo.HISTORY group by TIPNUMBER) h ON a.TIPNUMBER = h.TIPNUMBER
	INNER JOIN RewardsNow.dbo.dbprocessinfo d ON LEFT(a.tipnumber, 3) = d.DBNumber

INSERT INTO [52JKroegerPersonalFinance].dbo.Point_File_Template
(TransID, MemberNumber, RewardPoints, PointsLastUpdated, RunVersion)
SELECT	'3552' ,
		CAST(a.CustID as CHAR(10)),
		CAST(ISNULL(h.POINTS,0) as CHAR(10)),
		CONVERT(CHAR(8),d.PointsUpdated,112),
		'1'
FROM	(select TIPNUMBER, MAX(ACCTID) as CustID from [52W].dbo.AFFILIAT where AcctType = 'MEMBER' group by TIPNUMBER) a
	LEFT OUTER JOIN (select tipnumber, SUM(points*ratio) as POINTS from [52W].dbo.HISTORY group by TIPNUMBER) h ON a.TIPNUMBER = h.TIPNUMBER
	INNER JOIN RewardsNow.dbo.dbprocessinfo d ON LEFT(a.tipnumber, 3) = d.DBNumber	
GO
