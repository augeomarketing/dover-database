USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pSetWelcomeKitFileName1]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileName1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetWelcomeKitFileName1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetWelcomeKitFileName1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSetWelcomeKitFileName1] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS
/*********************************************************/
/* Date: 7/08                                            */
/* By: S. Blanchette                                     */
/* SCAN: SEB002                                          */
/* REASON: to put full date    as part of name           */
/*                                                       */
/*********************************************************/

declare @DBName varchar(100), @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @NumRecs numeric, @SQLSelect nvarchar(1000)

set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

set @SQLSelect=''set @NumRecs=(select count(*) from '' + QuoteName(@DBName) + N''.dbo.Welcomekit) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 

/*****************/
/* Start SEB002  */
/*****************/

-- set @endingDate=(select top 1 datein from DateforAudit) 
--Section to put together the current date.  Handle problem with date returning a one position month and day
--
-- set @workmonth=left(@endingdate,2) 
-- set @workyear=right(@endingdate,2) 
-- set @currentdate=@workmonth + @workyear 

set @currentdate=(select top 1 datein from DateforAudit) 
set @currentdate=REPLACE(@currentdate,''/'',''_'') 

/*****************/
/* END SEB002  */
/*****************/

set @filename=''W'' + @TipPrefix + ''_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''
 
set @newname=''O:\5xx\Output\WelcomeKits\Welcome.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\WelcomeKits\'' + @filename' 
END
GO
