USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSetStatementFileNameMer]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetStatementFileNameMer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetStatementFileNameMer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetStatementFileNameMer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 4/2009
-- Description:	Set Statement file name with Merchant bonus
-- =============================================
CREATE PROCEDURE [dbo].[spSetStatementFileNameMer]
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output, 
	@ConnectionString nchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

declare @DBName varchar(100), @NumRecs numeric, @SQLSelect nvarchar(1000)
set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

set @SQLSelect=''set @NumRecs=(select count(*) from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 

set @currentdate=(select top 1 datein from DateforAudit) 
set @currentdate=REPLACE(@currentdate,''/'',''_'') 

set @filename=''S'' + @TipPrefix + ''_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''
 
set @newname=''O:\5xx\Output\Statements\StatementMER.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\Statements\'' + @filename

END
' 
END
GO
