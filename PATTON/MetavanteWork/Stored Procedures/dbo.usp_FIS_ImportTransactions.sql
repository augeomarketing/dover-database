USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_ImportTransactions]    Script Date: 06/07/2012 10:59:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_ImportTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_ImportTransactions]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_ImportTransactions]    Script Date: 06/07/2012 10:59:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_FIS_ImportTransactions] 
	@TipFirst varchar(3), @StartDateParm varchar(10)

AS 

DECLARE	@SQL nvarchar(max), @Startdate datetime, @dbname varchar(50)
DECLARE @PointsFactor TABLE	
	(TIPfirst varchar(3), BIN nvarchar(9), Credit numeric(5, 2), SigFactor numeric(5, 2), PinFactor numeric(5, 2)) 

SET	@Startdate = convert(datetime, @Startdateparm + ' 00:00:00:001')  
SET @dbname = (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst)

TRUNCATE TABLE	transum
DELETE FROM		cardsin	WHERE tipnumber is null

/*********************************************************************/
/*  ASSIGN FACTORS									                 */
/*********************************************************************/
INSERT INTO	@PointsFactor	(TIPfirst, BIN, Credit, SigFactor, PinFactor)
SELECT		PF.DBNumber, PF.BIN, PF.Credit, PF.Sigfactor, PF.Pinfactor
FROM		PointsFactor PF
		INNER JOIN
			(
			SELECT		DBNUMBER, BIN, MAX(REVISION) REVISION
			FROM		PointsFactor
			WHERE		DBNumber = @tipfirst
				and		(Revision = 0
				or		(@Startdate >= Startdate and @Startdate <= Enddate))
			GROUP BY	DBNumber, BIN
			) REV
		ON		PF.BIN = REV.BIN
			AND	PF.DBNumber = REV.DBNumber
			AND	PF.Revision = REV.REVISION
							

/*********************************************************************/
/*  Get transaction data into a standard format                      */
/*********************************************************************/
SET	@SQL='
	INSERT INTO		transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	SELECT DISTINCT	tipnumber, acctnum, period, NUMPURCH, PURCH, ''0'', ''0'', cardtype, SigvsPin	
	FROM			cardsin
	WHERE			tipnumber in (SELECT tipnumber FROM [<<DBNAME>>].dbo.customer) and purch <> 0
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
EXEC	sp_executesql @SQL

SET	@SQL='
	INSERT INTO		transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	SELECT DISTINCT	tipnumber, acctnum, period, ''0'', ''0'', NUMRET, AMTRET, cardtype, SigvsPin	
	FROM			cardsin
	WHERE			tipnumber in (SELECT tipnumber FROM [<<DBNAME>>].dbo.customer) and amtret <> 0
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)					
EXEC	sp_executesql @SQL

/*********************************************************************/
/*  ASSIGN TRANCODES				                                 */
/*********************************************************************/
UPDATE	transum
SET		ratio = '1', trancode = '63', description = 'Credit Card Purchase', amtpurch = ROUND((AMTPURCH*B.Credit), 0)
FROM	transum a JOIN @pointsfactor b ON LEFT(rtrim(acctno),9) like b.BIN +'%'
WHERE	amtpurch > 0 and cardtype = 'C'

UPDATE	transum
SET		ratio = '1', trancode = '63', description = 'Credit Card Purchase', amtpurch = ROUND((AMTPURCH*B.Credit), 0)
FROM	transum a JOIN @pointsfactor b ON LEFT(a.TIPnumber, 3) = b.TIPfirst
WHERE	amtpurch > 0 and cardtype = 'C' and trancode is null and b.BIN is null

UPDATE	transum
SET		ratio = '-1', trancode = '33', description = 'Credit Card Returns', amtcr = ROUND((AMTcr*B.Credit), 0)
FROM	transum a JOIN @pointsfactor b ON LEFT(rtrim(acctno),9) like b.BIN +'%'
WHERE	amtcr > 0 and cardtype = 'C'

UPDATE	transum
SET		ratio = '-1', trancode = '33', description = 'Credit Card Returns', amtcr = ROUND((AMTcr*B.Credit), 0)
FROM	transum a JOIN @pointsfactor b ON LEFT(a.TIPnumber, 3) = b.TIPfirst
WHERE	amtcr > 0 and cardtype = 'C' and trancode is null and b.BIN is null

UPDATE	transum
SET		ratio = '1', trancode = '67', description = 'Debit Card Purchase SIG', amtpurch = round(((AMTPURCH/100)*B.Sigfactor),0) 
FROM	transum a JOIN @pointsfactor b ON LEFT(rtrim(acctno),9) like b.BIN +'%'
WHERE	amtpurch > 0 and cardtype = 'D' and (SigvsPin is null or len(SigvsPin) = '0' or SigvsPin = 'S')

UPDATE	transum
SET		ratio = '1', trancode = '67', description = 'Debit Card Purchase SIG', amtpurch = round(((AMTPURCH/100)*B.Sigfactor),0) 
FROM	transum a JOIN @pointsfactor b ON LEFT(a.TIPnumber, 3) = b.TIPfirst
WHERE	amtpurch > 0 and cardtype = 'D' and (SigvsPin is null or len(SigvsPin) = '0' or SigvsPin = 'S') and trancode is null and b.BIN is null

UPDATE	transum
SET		ratio = '1', trancode = '67', description = 'Debit Card Purchase PIN', amtpurch = round(((AMTPURCH/100)*B.Pinfactor),0) 
FROM	transum a JOIN @pointsfactor b ON LEFT(rtrim(acctno),9) like b.BIN +'%'
WHERE	amtpurch > 0 and cardtype = 'D' and SigvsPin = 'P'

UPDATE	transum
SET		ratio = '1', trancode = '67', description = 'Debit Card Purchase PIN', amtpurch = round(((AMTPURCH/100)*B.Pinfactor),0) 
FROM	transum a JOIN @pointsfactor b ON LEFT(a.TIPnumber, 3) = b.TIPfirst
WHERE	amtpurch > 0 and cardtype = 'D' and SigvsPin = 'P' and trancode is null and b.BIN is null

UPDATE	transum
SET		ratio='-1', trancode='37', description='Debit Card Returns SIG', amtcr=round(((AMTCR/100)*B.Sigfactor),0)
FROM	transum a JOIN @pointsfactor b ON LEFT(rtrim(acctno),9) like b.BIN +'%'
WHERE	amtcr > 0 and cardtype = 'D' and (SigvsPin is null or len(SigvsPin) = '0' or SigvsPin = 'S')

UPDATE	transum
SET		ratio='-1', trancode='37', description='Debit Card Returns SIG', amtcr=round(((AMTCR/100)*B.Sigfactor),0)
FROM	transum a JOIN @pointsfactor b ON LEFT(a.TIPnumber, 3) = b.TIPfirst
WHERE	amtcr > 0 and cardtype = 'D' and (SigvsPin is null or len(SigvsPin) = '0' or SigvsPin = 'S') and trancode is null and b.BIN is null

UPDATE	transum
SET		ratio = '-1', trancode = '37', description = 'Debit Card Returns PIN', amtcr = round(((AMTCR/100)*B.Pinfactor),0)
FROM	transum a JOIN @pointsfactor b ON LEFT(rtrim(acctno),9) like b.BIN + '%'
WHERE	amtcr > 0 and cardtype = 'D' and SigvsPin = 'P'

UPDATE	transum
SET		ratio = '-1', trancode = '37', description = 'Debit Card Returns PIN', amtcr = round(((AMTCR/100)*B.Pinfactor),0)
FROM	transum a JOIN @pointsfactor b ON LEFT(a.TIPnumber, 3) = b.TIPfirst
WHERE	amtcr > 0 and cardtype = 'D' and SigvsPin = 'P' and trancode is null and b.BIN is null

UPDATE	transum
SET		ratio = '1', trancode = '6H', description = 'HELOC Card Purchase', amtpurch = round((AMTPURCH/100),0) 
WHERE	amtpurch > 0 and cardtype = 'H'

UPDATE	transum
SET		ratio = '-1', trancode = '3H', description = 'HELOC Card Returns', amtcr = round((AMTCR/100),0)
WHERE	amtcr > 0 and cardtype = 'H'

DELETE FROM transum WHERE trancode is null

/*********************************************************************/
/*  PROCESS TRANSACTIONS		                                     */
/*********************************************************************/
SET	@SQL = '
		INSERT INTO [<<DBNAME>>].dbo.history
        SELECT		tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
		FROM		transum
		WHERE		trancode like ''6%''
		'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
EXEC	sp_executesql @SQL

SET	@SQL = '
		INSERT INTO [<<DBNAME>>].dbo.history
        SELECT		tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
		FROM		transum
		WHERE		trancode like ''3%''
		'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
EXEC	sp_executesql @SQL

--PROCESS BONUS POINT AWARDS
EXEC	usp_bonusprocessing @tipfirst, @dbname

/*********************************************************************/
/*  UPDATE CUSTOMER                                                  */
/*********************************************************************/
SET	@SQL = N'
		UPDATE	[<<DBNAME>>].dbo.customer 
		SET		runavailable = runavailable + (SELECT sum(amtpurch - amtcr) FROM transum WHERE tipnumber = [<<DBNAME>>].dbo.customer.tipnumber), 
				runbalance = runbalance + (SELECT sum(amtpurch - amtcr) FROM transum WHERE tipnumber = [<<DBNAME>>].dbo.customer.tipnumber) 
		WHERE	exists (SELECT * FROM transum WHERE tipnumber = [<<DBNAME>>].dbo.customer.tipnumber) '
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
EXEC	sp_executesql @SQL


GO


