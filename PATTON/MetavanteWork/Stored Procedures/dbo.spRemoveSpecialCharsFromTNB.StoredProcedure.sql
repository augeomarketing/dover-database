USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveSpecialCharsFromTNB]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveSpecialCharsFromTNB]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveSpecialCharsFromTNB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveSpecialCharsFromTNB]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spRemoveSpecialCharsFromTNB]
as

declare @tipfirst varchar(3)

DECLARE cur_databases CURSOR FOR
	--select left(name,3) from master.dbo.sysdatabases where name not in (''tempdb'') and (name like ''1%'' or name like ''000%'')
	select left(name,3) from master.dbo.sysdatabases where name not in (''tempdb'') and (name like ''000%'')
	OPEN cur_databases 
	fetch next  from cur_databases into @tipfirst
	
WHILE (@@FETCH_STATUS=0)
	BEGIN
   	    exec rewardsnow.dbo.spRemoveSpecialCharactersFromCustomer @tipfirst
	    exec rewardsnow.dbo.spRemoveSpecialCharactersFromAffiliat @tipfirst 
	    fetch next  from cur_databases into @tipfirst
	END

close cur_databases
deallocate cur_databases
' 
END
GO
