USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_b2kPopulateCreditCardIn]    Script Date: 04/11/2011 13:46:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [dbo].[usp_b2kPopulateCreditCardIn]
	@dataMonthEndDate date
as

begin
	insert into CREDITCARDIN
	(
		SSN
		, ACCTNUM
		, OLDCC
		, NA1
		, NA2
		, ADDR1
		, ADDR2
		, ADDR3
		, CITYSTATE
		, ZIP
		, STATUS
		, PHONE1
		, PHONE2
		, NUMPURCH
		, PURCH
		, NUMRET
		, AMTRET
		, PERIOD
		, LASTNAME
		, TIPFIRST
		, JOINT
	)
	SELECT
		ssn
		, acctnum
		, oldcc
		, na1
		, na2
		, addr1
		, addr2
		, addr3
		, citystate
		, zip
		, status
		, phone1
		, phone2
		, numpurch
		, purch
		, numret
		, amtret
		, convert(varchar(10), @dataMonthEndDate, 101) as period
		, lastname
		, tipfirst
		, CASE WHEN RewardsNow.dbo.ufn_CharIsNullOrEmpty(na2) = 0 THEN 'J' ELSE 'S' END as joint
	FROM
		vw_b2kCreditCardHolding
end


