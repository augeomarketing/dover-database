USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGetTipBillPayBonus]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipBillPayBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGetTipBillPayBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipBillPayBonus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spFBOPGetTipBillPayBonus] @TipFirst char(3), @Bank char(3)
as
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 10/2007   */
/* REVISION: 0 */
/* SCAN:  */
/* Script to get tipnumbers from affiliat table by either card# or ssn  */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateToDelete char(6), @dateclosed datetime, @newmonth nchar(2), @newyear nchar(4)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLUpdate=N''update  BillPayBonus
		set tipnumber=b.tipnumber 
		from BillPayBonus a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.bank=@Bank and [card #] is not null and len([card #])>''''1'''' and [card #]=acctid ''
exec sp_executesql @SQLUpdate, N''@Bank char(3)'', @Bank=@bank

set @SQLUpdate=N''update  BillPayBonus
		set tipnumber=b.tipnumber 
		from BillPayBonus a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.bank=@Bank and [Tax ID] is not null and len([Tax ID])>''''1'''' and [Tax ID]<>''''000000000'''' and [Tax ID]=secid ''
exec sp_executesql @SQLUpdate, N''@Bank char(3)'', @Bank=@bank

set @SQLUpdate=N''update  BillPayBonus
		set tipnumber=b.tipnumber 
		from BillPayBonus a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.bank=@Bank and [Acct #] is not null and len([Acct #])>''''1'''' and [Acct #]<>''''00000000000'''' and [Acct #]=custid ''
exec sp_executesql @SQLUpdate, N''@Bank char(3)'', @Bank=@bank' 
END
GO
