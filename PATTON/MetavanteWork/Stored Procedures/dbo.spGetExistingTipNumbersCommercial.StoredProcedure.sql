USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbersCommercial]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbersCommercial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetExistingTipNumbersCommercial]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbersCommercial]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGetExistingTipNumbersCommercial] @TipFirst char(3)
AS

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/*  Get tipnumber based on account number      */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.acctnum=b.acctid ''
Exec sp_executesql @SQLUpdate

/*  Get tipnumber based on old account number  */
set @SQLUpdate=''update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.oldcc=b.acctid ''
Exec sp_executesql @SQLUpdate

delete from cardsin
where tipnumber is null and status<>''A''' 
END
GO
