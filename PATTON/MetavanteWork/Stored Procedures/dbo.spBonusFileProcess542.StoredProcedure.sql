USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcess542]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcess542]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcess542]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcess542]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spBonusFileProcess542]
	-- Add the parameters for the stored procedure here
	@DateIn varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, CRDHLR_NBR, points	
from [BonusTip&Points]

update transumbonus
set ratio=''1'', trancode=''BI'', description=''Bonus Points Increase'', histdate=@datein, numdebit=''1'', overage=''0''

insert into [542FirstUnitedBTConsumer].dbo.history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
from transumbonus

update [542FirstUnitedBTConsumer].dbo.customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=[542FirstUnitedBTConsumer].dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=[542FirstUnitedBTConsumer].dbo.customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=[542FirstUnitedBTConsumer].dbo.customer.tipnumber)
END
' 
END
GO
