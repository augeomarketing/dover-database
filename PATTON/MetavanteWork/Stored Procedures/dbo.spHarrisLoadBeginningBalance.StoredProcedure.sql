USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisLoadBeginningBalance]    Script Date: 06/28/2010 10:24:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisLoadBeginningBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisLoadBeginningBalance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisLoadBeginningBalance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2010
-- Description:	Update begining balance table for staus X customers
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisLoadBeginningBalance] 
	-- Add the parameters for the stored procedure here
	@tipFirst char(3),
	@MonthBeginDate nchar(10), 
	@MonthEndDate nchar(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName varchar(50)

	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	set @MonthBegin=CONVERT( int , left(@MonthBeginDate,2)) + ''1''

		if CONVERT( int , @MonthBegin)=''13'' 
			begin
				set @monthbegin=''1''
			end	
			
	set @MonthBucket=''MonthBeg'' + @monthbegin

	set @SQLUpdate=N''update bbt
					set ''+ @MonthBucket + N'' = his.hispoints
					from ''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table bbt 
					join (select tipnumber, isnull( SUM(Points * ratio), 0) hispoints from ''+ Quotename(@DBName) + N''.dbo.HISTORY 
									where HISTDATE <=  @MonthEndDate and TIPNUMBER in (select TIPNUMBER from ''+ Quotename(@DBName) + N''.dbo.CUSTOMER where Status=''''X'''')
							group by TIPNUMBER) his
					on bbt.Tipnumber = his.TIPNUMBER ''
	exec sp_executesql @SQLUpdate, N''@MonthEndDate nvarchar(10)'', @MonthEndDate = @MonthEndDate 

END
' 
END
GO
