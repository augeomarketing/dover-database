USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransactionDataNew]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionDataNew]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataNew]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spImportTransactionDataNew] @TipFirst char(3), @StartDateParm char(10)
AS 

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 10/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* add code to deal with FBOP Heloc cards  */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 5/2008   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* add code to hadle different point factors by BIN using a table variable */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 06/2009   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* add code to remove transaction records if tipnumber not in customer  */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000)
declare @DBnumber char(3), @Revision smallint, @Begindate datetime, @Enddate datetime, @Startdate datetime, @BIN varchar(9), @CreditFactor numeric(18,2),@SigFactor numeric(18,2), @PinFactor numeric(18,2), @WorkFactorSig numeric(18,2), @WorkFactorPin numeric(18,2)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @Startdate = convert(datetime, @Startdateparm + '' 00:00:00:001'')  

/****************/
/* START SEB002 */
/****************/
DECLARE @PointsFactor TABLE (
	BIN nvarchar (9)  ,
	Credit numeric(5, 2) ,
	SigFactor numeric(5, 2) ,
	PinFactor numeric(5, 2) 
) 
/****************/
/* END  SEB002  */
/****************/

truncate table transum

delete from cardsin where tipnumber is null

/*                                                                            */
/*  Process to get the factor for debit cards TABLE                           */
/*                                                                            */
/****************/
/* START SEB002 */
/****************/
	--declare debit_crsr cursor
	--for select DBNumber, Revision, Startdate, Enddate, BIN, Credit, Sigfactor, Pinfactor
	--from PointsFactor
	--where DBNumber=@TipFirst
	--order by Revision desc
declare debit_crsr cursor
for select DBNumber, Revision, Startdate, Enddate, BIN, Credit, Sigfactor, Pinfactor
from PointsFactor
where DBNumber=@TipFirst
order by Revision desc
/****************/
/* END  SEB002  */
/****************/

/*                                                                            */
open debit_crsr
/*                                                                            */
/* SEB002  fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @BIN, @CreditFactor, @SigFactor, @PinFactor */
/* SEB002 */ fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @BIN, @CreditFactor, @SigFactor, @PinFactor
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @Begindate = convert(datetime, @BeginDate + '' 00:00:00:001'')    
		set @Enddate = convert(datetime, @Enddate + '' 23:59:59:990'')    

		if @Revision=''0''		
		Begin
			/****************/
			/* START SEB002 */
			/****************/
				--set @WorkFactorSig=@SigFactor
				--set @WorkFactorPin=@PinFactor
				INSERT INTO @PointsFactor (BIN, Credit, SigFactor, PinFactor)
				values 	(@BIN, @CreditFactor, @SigFactor, @PinFactor)	
				--goto Fetch_Error
				goto Next_Record
			/****************/
			/* END  SEB002  */
			/****************/
		
End

		if @Startdate>@Enddate
		Begin
			/****************/
			/* START SEB002 */
			/****************/
				--set @WorkFactorSig=(select SigFactor from DebitFactor where DBNumber=@TipFirst and Revision=''0'')
				--set @WorkFactorPin=(select PinFactor from DebitFactor where DBNumber=@TipFirst and Revision=''0'')
				INSERT INTO @PointsFactor (BIN, Credit, SigFactor, PinFactor)
				values 	(@BIN, @CreditFactor, @SigFactor, @PinFactor)	
				--goto Fetch_Error
				goto Next_Record
			/****************/
			/* END  SEB002  */
			/****************/		
		End

		if @Startdate>=@Begindate and @Startdate<=@Enddate
		Begin
			/****************/
			/* START SEB002 */
			/****************/
				--set @WorkFactorSig=@SigFactor
				--set @WorkFactorPin=@PinFactor
				INSERT INTO @PointsFactor (BIN, Credit, SigFactor, PinFactor)
				values 	(@BIN, @CreditFactor, @SigFactor, @PinFactor)	
				--goto Fetch_Error
				goto Next_Record		
			/****************/
			/* END  SEB002  */
			/****************/	
		End

		if @Startdate<@Begindate 
		Begin
			goto Next_Record
		End

Next_Record:
		/* SEB002  fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @BIN. @CreditFactor, @SigFactor, @PinFactor */
	/* SEB002 */ fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @BIN, @CreditFactor, @SigFactor, @PinFactor
	end

Fetch_Error:
close  debit_crsr
deallocate  debit_crsr

/*********************************************************************/
/*  Get transaction data into a standard format                      */
/*********************************************************************/
insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin	
from cardsin
group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin

/*********************************************************************/
/*  start SEB002 Remove records that do not have a customer record   */
/*********************************************************************/
set @SQLDelete=''Delete from dbo.transum
				where tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.customer) ''
Exec sp_executesql @SQLDelete
/*********************************************************************/
/*  end SEB002 Remove records that do not have a customer record   */
/*********************************************************************/

/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/
/****************/
/* START SEB002 */
/****************/
	--update transum
	--set ratio=''1'', trancode=''63'', description=''Credit Card Purchase'', amtpurch=ROUND((amtpurch), 0)
	--where amtcr>0 and cardtype=''C''
update transum
set ratio=''1'', trancode=''63'', description=''Credit Card Purchase'', amtpurch=ROUND((AMTPURCH*B.Credit), 0)
from transum a, @pointsfactor b
where amtpurch>0 and cardtype=''C'' and left(acctno,8) like b.BIN +''%''
/****************/
/* END  SEB002  */
/****************/	

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '''' '''', ratio, overage
		from transum
		where trancode=''''63'''' ''
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Credit Cards Returns                                     */
/*********************************************************************/
/****************/
/* START SEB002 */
/****************/
	--update transum
	--set ratio=''-1'', trancode=''33'', description=''Credit Card Returns'', amtcr=ROUND((AMTcr), 0)
	--where amtcr>0 and cardtype=''C''
update transum
set ratio=''-1'', trancode=''33'', description=''Credit Card Returns'', amtcr=ROUND((AMTcr*B.Credit), 0)
from transum a, @pointsfactor b
where amtcr>0 and cardtype=''C'' and left(rtrim(acctno),9) like b.BIN +''%''
/****************/
/* END  SEB002  */
/****************/	

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '''' '''', ratio, overage
		from transum
		where trancode=''''33'''' ''
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
/****************/
/* START SEB002 */
/****************/
	--update transum
	--set ratio=''1'', trancode=''67'', description=''Debit Card Purchase SIG'', amtpurch=round(((AMTPURCH/100)/@WorkfactorSig),0) 
	--where amtpurch>0 and cardtype=''D'' and (SigvsPin is null or len(SigvsPin)=''0'' or SigvsPin=''S'')
update transum
set ratio=''1'', trancode=''67'', description=''Debit Card Purchase SIG'', amtpurch=round(((AMTPURCH/100)*B.Sigfactor),0) 
from transum a, @pointsfactor b
where amtpurch>0 and cardtype=''D'' and (SigvsPin is null or len(SigvsPin)=''0'' or SigvsPin=''S'') and left(rtrim(acctno),9) like b.BIN +''%''

	--update transum
	--set ratio=''1'', trancode=''67'', description=''Debit Card Purchase PIN'', amtpurch=round(((AMTPURCH/100)/@WorkfactorPin),0) 
	--here amtpurch>0 and cardtype=''D'' and SigvsPin=''P''
update transum
set ratio=''1'', trancode=''67'', description=''Debit Card Purchase PIN'', amtpurch=round(((AMTPURCH/100)*B.Pinfactor),0) 
from transum a, @pointsfactor b
where amtpurch>0 and cardtype=''D'' and SigvsPin=''P'' and left(rtrim(acctno),9) like b.BIN +''%''
/****************/
/* END  SEB002  */
/****************/	

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '''' '''', ratio, overage
		from transum
		where trancode=''''67'''' ''
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
/****************/
/* START SEB002 */
/****************/
--update transum
--set ratio=''-1'', trancode=''37'', description=''Debit Card Returns SIG'', amtcr=round(((AMTCR/100)/@WorkfactorSig),0)
--where amtcr>0 and cardtype=''D'' and (SigvsPin is null or len(SigvsPin)=''0'' or SigvsPin=''S'')
update transum
set ratio=''-1'', trancode=''37'', description=''Debit Card Returns SIG'', amtcr=round(((AMTCR/100)*B.Sigfactor),0)
from transum a, @pointsfactor b
where amtcr>0 and cardtype=''D'' and (SigvsPin is null or len(SigvsPin)=''0'' or SigvsPin=''S'') and left(rtrim(acctno),9) like b.BIN +''%''

--update transum
--set ratio=''-1'', trancode=''37'', description=''Debit Card Returns PIN'', amtcr=round(((AMTCR/100)/@WorkfactorPin),0)
--where amtcr>0 and cardtype=''D'' and SigvsPin=''P''
update transum
set ratio=''-1'', trancode=''37'', description=''Debit Card Returns PIN'', amtcr=round(((AMTCR/100)*B.Pinfactor),0)
from transum a, @pointsfactor b
where amtcr>0 and cardtype=''D'' and SigvsPin=''P'' and left(rtrim(acctno),9) like b.BIN +''%''
/****************/
/* END  SEB002  */
/****************/	

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '''' '''', ratio, overage
		from transum
		where trancode=''''37'''' ''
Exec sp_executesql @SQLInsert


/***********************/
/*  start section SEB001  */
/***********************/

/*********************************************************************/
/*  Process HELOC Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''6H'', description=''HELOC Card Purchase'', amtpurch=round((AMTPURCH/100),0) 
where amtpurch>0 and cardtype=''H''

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '''' '''', ratio, overage
		from transum
		where trancode=''''6H'''' ''
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process HELOC Cards Returns                                      */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''3H'', description=''HELOC Card Returns'', amtcr=round((AMTCR/100),0)
where amtcr>0 and cardtype=''H''

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
       	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '''' '''', ratio, overage
		from transum
		where trancode=''''3H'''' ''
Exec sp_executesql @SQLInsert

/***********************/
/*  end section SEB001  */
/***********************/

delete from transum
where trancode is null

/*********************************************************************/
/*  Update Customer                                                  */
/*********************************************************************/
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.customer 
		set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber = '' + QuoteName(@DBName) + N''.dbo.customer.tipnumber), 
			runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber = '' + QuoteName(@DBName) + N''.dbo.customer.tipnumber) 
		where exists(select * from transum where tipnumber = '' + QuoteName(@DBName) + N''.dbo.customer.tipnumber) ''
Exec sp_executesql @SQLUpdate' 
END
GO
