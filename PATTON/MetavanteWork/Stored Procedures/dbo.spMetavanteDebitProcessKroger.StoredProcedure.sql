USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteDebitProcessKroger]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcessKroger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteDebitProcessKroger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcessKroger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S.Blanchette
-- Create date: 10/26/2009
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spMetavanteDebitProcessKroger] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
								      
/*           Convert Trancode                                                     */
update DebitCardInKroger
set DebitCardInKroger.[Transaction Code]=TrancodeXref.trancodeout 
from DebitCardInKroger, TrancodeXref 
where DebitCardInKroger.[Transaction Code]=TrancodeXref.trancodeIn 

update DebitCardInKroger	
set numpurch=''1'', purch=[Total Dollar Cents]
where [Transaction Code]=''67''

update DebitCardInKroger	
set numret=''1'', amtret=[Total Dollar Cents]
where [Transaction Code]=''37''

update DebitCardInKroger
set status=''A''
where [External Status]=''A''

update DebitCardInKroger
set status=''C''
where [External Status]=''B''

update DebitCardInKroger
set cardtype=''D''

update DebitCardInKroger
set TIPMID=''0000000''

/* rearrange name 1 from  Last, First to First Last */
update DebitCardInKroger
set [Full Name]=rtrim(substring([Full Name],charindex('','',[Full Name])+2,len(rtrim([Full Name])))) + '' '' + substring([Full Name],1,charindex('','',[Full Name])-1)
where substring([Full Name],1,1) not like '' '' and [Full Name] is not null and [Full Name] LIKE ''%,%''
update DebitCardInKroger
set [Full Name]=ltrim([Full Name])

/* rearrange name 2 from  Last, First to First Last */
update DebitCardInKroger
set [Secondary Name]=rtrim(substring([Secondary Name],charindex('','',[Secondary Name])+2,len(rtrim([Secondary Name])))) + '' '' + substring([Secondary Name],1,charindex('','',[Secondary Name])-1)
where substring([Secondary Name],1,1) not like '' '' and [Secondary Name] is not null and [Secondary Name] LIKE ''%,%''
update DebitCardInKroger
set [Secondary Name]=ltrim([Secondary Name])
END
' 
END
GO
