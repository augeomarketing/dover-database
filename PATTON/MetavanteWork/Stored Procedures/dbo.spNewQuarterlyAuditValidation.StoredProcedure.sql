USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewQuarterlyAuditValidation]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewQuarterlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	Quarter statement validation
-- =============================================
CREATE PROCEDURE [dbo].[spNewQuarterlyAuditValidation] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @errmsg varchar(50), @currentend numeric(9)

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	Truncate Table dbo.Quarterly_Audit_ErrorFile 
	
	set @errmsg=''Ending Balances do not match''

	INSERT INTO dbo.Quarterly_Audit_ErrorFile
		select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
		from dbo.New_Quarterly_Statement_Detail a, dbo.Current_Month_Activity b
		where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints 
	
END
' 
END
GO
