USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPQuarterlyStatementFileold1]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPQuarterlyStatementFileold1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPQuarterlyStatementFileold1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPQuarterlyStatementFileold1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPQuarterlyStatementFileold1] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket=''MonthBeg'' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.FBOPQuarterlyStatement ''
exec sp_executesql @SQLTruncate


if exists(select name from sysobjects where name=''wrkhist'')
begin 
	truncate table wrkhist
end

set @SQLSelect=''insert into wrkhist
		select tipnumber, trancode, sum(points) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), left(zipcode,5) 
				from '' + QuoteName(@DBName) + N''.dbo.customer order by tipnumber ''
Exec sp_executesql @SQLInsert

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement 
	set acctnum = rtrim(b.acctid), ddanum=rtrim(b.custid) 
	from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
	where a.tipnumber = b.tipnumber and b.acctstatus=''''A'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set lastfour=right(rtrim(acctnum),4) ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   63       */

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointspurchasedcr=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''63'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointspurchaseddb=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''67'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with HELOC purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointspurchasedhe=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''6H'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsbonuscr=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and trancode in (''''FJ'''', ''''FF'''', ''''FG'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and trancode in (''''FJ'''', ''''FF'''', ''''FG'''')) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with debit bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsbonusdb=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and trancode in(''''FC'''', ''''FD'''', ''''FI'''', ''''FE'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and trancode in(''''FC'''', ''''FD'''', ''''FI'''', ''''FE'''' )) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with HELOC bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsbonushe=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and (trancode = ''''FH'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and (trancode = ''''FH'''' )) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Generic bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsbonus=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and (trancode in (''''BI'''', ''''BE''''))) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and (trancode in (''''BI'''', ''''BE'''' ))) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsadded=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''IE'''' '' 	
	Exec sp_executesql @SQLUpdate 

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsadded = pointsadded +
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''TR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point increases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointspurchasedhe + pointsbonuscr + pointsbonusdb + pointsbonushe + pointsbonus + pointsadded ''
	Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsredeemed=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and trancode like ''''R%'''') 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement.tipnumber and trancode like ''''R%'''') ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsredeemed=pointsredeemed-
		b.points 
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''DR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsreturnedcr=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''33'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsreturneddb=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''37'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with HELOC returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsreturnedhe=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''3H'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with minus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointssubtracted=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''DE'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point decreases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointsreturnedhe + pointssubtracted ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsbegin = b.'' + Quotename(@MonthBucket) + N'' 
		from '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
		where a.tipnumber = b.tipnumber ''
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPQuarterlyStatement set pointsend=pointsbegin + pointsincreased - pointsdecreased ''
exec sp_executesql @SQLUpdate' 
END
GO
