USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGenNewTIPsCom13006]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenNewTIPsCom13006]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenNewTIPsCom13006]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenNewTIPsCom13006]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGenNewTIPsCom13006] 
AS

exec spGenerateNewTIPNumbersCommercial113006 ''505''

exec spGenerateNewTIPNumbersCommercial113006 ''507''

exec spGenerateNewTIPNumbersCommercial113006 ''509''

exec spGenerateNewTIPNumbersCommercial113006 ''512''

exec spGenerateNewTIPNumbersCommercial113006 ''513''

exec spGenerateNewTIPNumbersCommercial113006 ''520''

exec spGenerateNewTIPNumbersCommercial113006 ''526''

exec spGenerateNewTIPNumbersCommercial113006 ''528''

exec spGenerateNewTIPNumbersCommercial113006 ''532''' 
END
GO
