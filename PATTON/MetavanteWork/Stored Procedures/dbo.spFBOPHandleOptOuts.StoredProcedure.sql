USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPHandleOptOuts]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPHandleOptOuts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPHandleOptOuts]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPHandleOptOuts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[spFBOPHandleOptOuts]
as

/* Remove CreditCardIn for Opt Out people  */
delete from  FBOPCreditCardIn
where exists(select * from optoutcontrol where acctnumber= FBOPCreditCardIn.acctnum)' 
END
GO
