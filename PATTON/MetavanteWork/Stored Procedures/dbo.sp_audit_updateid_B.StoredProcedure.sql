USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[sp_audit_updateid_B]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_audit_updateid_B]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_audit_updateid_B]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_audit_updateid_B]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE procedure [dbo].[sp_audit_updateid_B]
	@tip varchar(3)

as

update autoprocessdetail
set	CA_id = pc.CA_id
from autoprocessdetail apd join packagecodes pc 
	on apd.tipfirst = pc.tipfirst
where pc.tipfirst = @tip

' 
END
GO
