USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[sp_audit_filename]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_audit_filename]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_audit_filename]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_audit_filename]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE procedure [dbo].[sp_audit_filename]
	@tip varchar(3),
	@date datetime

as

Select ''\\Patton\Ops\5xx\Output\AuditFiles\RewardsNOWAudit.''
+ @tip
+''_''
+ Right(''0''+cast(month(@date)as varchar(2)),2)
+ Right(''0''+cast(year(@date)as varchar(4)),2)
+ ''.xls''
' 
END
GO
