USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_NewCustomer]    Script Date: 01/24/2012 10:04:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_Bonus_NewCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_Bonus_NewCustomer]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_NewCustomer]    Script Date: 01/24/2012 10:04:25 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_FIS_Bonus_NewCustomer] 
	@EndDate varchar(10), @tipfirst varchar(3)

AS 

/****************************************************************************/
/*   Procedure to generate the Activation bonus based on first use          */
/****************************************************************************/

DECLARE	@SQL nvarchar(max), 
		@BonusPoints numeric(9),
		@dbname varchar(50)

SET		@BonusPoints = (SELECT ActivationBonus FROM DBProcessInfo where DBNumber = @TipFirst)
SET		@dbname = (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst)

IF		@BonusPoints > 0 
	BEGIN
		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.history 
						(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
			SELECT		TIPnumber, ''<<ENDDATE>>'', ''BA'', ''1'', <<BONUSPTS>>, ''1'', ''Activation Bonus'', ''0''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber not in (SELECT TIPNUMBER FROM [<<DBNAME>>].dbo.OneTimeBonuses WHERE trancode = ''BA'')
				and		dateadded = ''<<ENDDATE>>''
        					'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)	
		EXEC	sp_executesql @SQL
 
 		SET	@SQL = N'
			UPDATE [<<DBNAME>>].dbo.Customer 
			SET		RunAvailable = RunAvailable + <<BONUSPTS>>, 
					RunBalance = RunBalance + <<BONUSPTS>>  
			WHERE	tipnumber not in (SELECT TIPNUMBER FROM [<<DBNAME>>].dbo.OneTimeBonuses WHERE trancode = ''BA'')
				and dateadded = ''<<ENDDATE>>''
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)	
		EXEC	sp_executesql @SQL

		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.OneTimeBonuses
						(TipNumber, TranCode, DateAwarded)
			SELECT		TIPnumber, ''BA'', ''<<ENDDATE>>''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber not in (SELECT TIPNUMBER FROM [<<DBNAME>>].dbo.OneTimeBonuses WHERE trancode = ''BA'')
				and		dateadded = ''<<ENDDATE>>''
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', @dbname)							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)	
		EXEC	sp_executesql @SQL
	END
GO
