USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_kroger_dateupdated]    Script Date: 03/10/2014 14:41:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_kroger_dateupdated]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_kroger_dateupdated]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_kroger_dateupdated]    Script Date: 03/10/2014 14:41:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[usp_kroger_dateupdated] 
	@tipfirst varchar(3)

AS

BEGIN

	DECLARE	@ms varchar(2), 
			@me varchar(2), 
			@ds varchar(2),
			@de	varchar(2), 
			@ys varchar(4), 
			@ye varchar(4) 

	SELECT DISTINCT
			@ms = SUBSTRING(BatchDate,5,2), @me = SUBSTRING(BatchDate,5,2), 
			@ds = SUBSTRING(BatchDate,7,2), @de = SUBSTRING(BatchDate,7,2), 
			@ys = SUBSTRING(BatchDate,1,4), @ye = SUBSTRING(BatchDate,1,4)
	FROM dbo.Kroger_Daily_Batch_Header

	IF @tipfirst in ('52J','52U','53A')
		BEGIN
			IF @de < 14 
				BEGIN
					IF @me = 1
						BEGIN 
							SET @ys =  @ys - 1 
						END
					SET @ms = RIGHT('0' + CAST(CAST(@ms as int) -1 as varchar(2)), 2) 
					IF @ms = '00' 
						BEGIN 
							SET @ms = '12' 
						END
				END
			SET @ds = '14'
		END	

	IF @tipfirst in ('52R','52V','52W')
		BEGIN
			SET @ds = '01'
		END	

	UPDATE MetavanteWork.dbo.autoprocessdetail
	SET		MonthBeginingDate	= @ms + '/' + @ds + '/' + @ys,
			MonthEndingdate		= @me + '/' + @de + '/' + @ye
	WHERE	TipFirst = @tipfirst

	
END
GO


