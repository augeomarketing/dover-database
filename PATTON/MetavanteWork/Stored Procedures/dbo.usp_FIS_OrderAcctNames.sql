USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_OrderAcctNames]    Script Date: 03/13/2012 14:04:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_OrderAcctNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_OrderAcctNames]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_OrderAcctNames]    Script Date: 03/13/2012 14:04:57 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE Procedure [dbo].[usp_FIS_OrderAcctNames]
	@tipfirst varchar(3)
	
AS

DECLARE	@sql nvarchar(max)

SET	@sql = '
	UPDATE	[<<DBNAME>>].dbo.customer
	SET		acctname1 = b.acctname1,	acctname2 = b.acctname2,	acctname3 = null,
			acctname4 = null,			acctname5 = null,			acctname6 = null,
			lastname = b.lastname,		address1 = b.address1,		address2 = b.address2,
			address3 = b.address3,		address4 = b.address4,		city = b.city,
			state = b.state,			zipcode = b.zipcode
	FROM	[<<DBNAME>>].dbo.customer a JOIN MetavanteWork.dbo.Primaryhold b ON a.tipnumber = b.tipnumber
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL 
	

SET	@sql = '
	UPDATE	[<<DBNAME>>].dbo.affiliat
	SET		lastname = b.lastname
	FROM	[<<DBNAME>>].dbo.affiliat a JOIN MetavanteWork.dbo.Primaryhold b ON a.tipnumber = b.tipnumber
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL 
	

GO


