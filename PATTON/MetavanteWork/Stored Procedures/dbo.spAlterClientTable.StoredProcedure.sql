USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spAlterClientTable]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAlterClientTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAlterClientTable]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAlterClientTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spAlterClientTable]
as

declare @DBName varchar(100), @SQLAlter nvarchar(1000), @SQLUpdate nvarchar(1000)

DECLARE cur_databases CURSOR FOR
	select name from master.dbo.sysdatabases where name not in (''tempdb'') and name like ''xxx%'' 
	OPEN cur_databases 
	fetch next  from cur_databases into @DBName
	
WHILE (@@FETCH_STATUS=0)
	BEGIN

		set @SQLAlter=N''ALTER TABLE '' + Quotename(@DBName) + N''.dbo.Client ADD LastTipNumberUsed CHAR(15) NULL ''
		exec  sp_executesql @SQLAlter

		set @SQLUpdate=N''update '' + Quotename(@DBName) + N''.dbo.Client
				set LastTipNumberUsed=(select max(tipnumber) from '' + Quotename(@DBName) + N''.dbo.customer)''
		exec  sp_executesql @SQLUpdate

		fetch next  from cur_databases into @DBName
	END

close cur_databases
deallocate cur_databases' 
END
GO
