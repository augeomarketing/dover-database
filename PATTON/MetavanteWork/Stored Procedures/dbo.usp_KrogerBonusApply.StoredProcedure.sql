--exec usp_KrogerBonusApply
use [MetavanteWork]
go

IF OBJECT_ID(N'usp_KrogerBonusApply') IS NOT NULL
	DROP PROCEDURE usp_KrogerBonusApply
GO

CREATE PROCEDURE usp_KrogerBonusApply
AS

SET NOCOUNT ON

DECLARE @stmts TABLE (
	myid INT IDENTITY(1,1) PRIMARY KEY
	, stmt NVARCHAR(MAX)
)

DECLARE @datestmts TABLE(
	myid_dt INT IDENTITY(1,1) PRIMARY KEY
	, stmt_dt NVARCHAR(MAX)
)

--Populate Statements to Run

RAISERROR('Populating Statements', 0, 0) WITH NOWAIT

INSERT INTO @stmts(stmt)
SELECT REPLACE(REPLACE(REPLACE(
	'exec rn1.rewardsnow.dbo.portal_customer_adjust_save ''<TIPFIRST>'', ''<TIPNUMBER>'', <POINTS>, 330, ''BI'', ''FI Awarded Bonus'', 1, null'
	, '<TIPFIRST>', sid_dbprocessinfo_dbnumber)
	, '<TIPNUMBER>', dim_krogerbonuspreprocessing_tipnumber)
	, '<POINTS>', dim_krogerbonuspreprocessing_transactionamount)
FROM KrogerBonusPreProcessing
WHERE dim_krogerbonuspreprocessing_tipnumber <> 'DELETED'

RAISERROR('Populating HistDate update Statements', 0, 0) WITH NOWAIT

INSERT INTO @datestmts (stmt_dt)
SELECT
REPLACE(REPLACE(REPLACE(
	'UPDATE RN1.OnlineHistoryWork.dbo.Portal_Adjustments SET Histdate = ''<HISTDATE>'' WHERE tipnumber = ''<TIPNUMBER>'' AND trancode = ''BI'' and TranDesc = ''FI Awarded Bonus'' and points = <POINTS> and CAST(histdate as DATE) = CAST(getdate() as date) AND copyflag is null'
	, '<HISTDATE>', CONVERT(VARCHAR(10), dim_krogerbonuspreprocessing_histdate, 101))
	, '<TIPNUMBER>', dim_krogerbonuspreprocessing_tipnumber)
	, '<POINTS>', dim_krogerbonuspreprocessing_transactionamount)
FROM KrogerBonusPreProcessing
WHERE dim_krogerbonuspreprocessing_tipnumber <> 'DELETED'

--Apply Statements
declare @stmtcount int
declare @stmtcount_dt int
declare @myid int
declare @sql NVARCHAR(MAX)

set @stmtcount = (select COUNT(*) from @stmts)
while @stmtcount > 0
BEGIN
	
	RAISERROR('Executing Portal Adjustment (%d)', 0, 0, @stmtcount) WITH NOWAIT

	set @myid = (select top 1 myid from @stmts)
	select @sql = stmt from @stmts where myid = @myid
	
	exec sp_executesql @sql
	
	delete from @stmts where myid = @myid
	
	set @stmtcount = (select COUNT(*) from @stmts)		
END

--Update HISTDATES

SET @stmtcount_dt = (SELECT COUNT(*) from @datestmts)
WHILE @stmtcount_dt > 0
BEGIN
	RAISERROR('Executing Histdate Adjustment (%d)', 0, 0, @stmtcount_dt) WITH NOWAIT

	set @myid = (select top 1 myid_dt from @datestmts)
	select @sql = stmt_dt from @datestmts where myid_dt = @myid
	
	exec sp_executesql @sql
	
	delete from @datestmts where myid_dt = @myid
	
	set @stmtcount_dt = (select COUNT(*) from @datestmts)		
END


RAISERROR('Archiving Records', 0, 0) WITH NOWAIT


insert into KrogerBonusPreProcessingArchive 
(
	dim_krogerbonuspreprocessing_id
	,dim_krogerbonuspreprocessing_source
	,dim_krogerbonuspreprocessing_row
	,dim_krogerbonuspreprocessing_histdate
	,sid_dbprocessinfo_dbnumber
	,dim_krogerbonuspreprocessing_portfolionumber
	,dim_krogerbonuspreprocessing_membernumber
	,dim_krogerbonuspreprocessing_tinssn
	,dim_krogerbonuspreprocessing_producttype
	,dim_krogerbonuspreprocessing_cardnumber
	,dim_krogerbonuspreprocessing_transactiondate
	,dim_krogerbonuspreprocessing_transfercardnumber
	,dim_krogerbonuspreprocessing_transactioncode
	,dim_krogerbonuspreprocessing_ddanumber
	,dim_krogerbonuspreprocessing_transactionamount
	,dim_krogerbonuspreprocessing_transactioncount
	,dim_krogerbonuspreprocessing_transactiondescription
	,dim_krogerbonuspreprocessing_currencycode
	,dim_krogerbonuspreprocessing_merchantid
	,dim_krogerbonuspreprocessing_transactionid
	,dim_krogerbonuspreprocessing_authorizationcode
	,dim_krogerbonuspreprocessing_tipnumber
)
select
	dim_krogerbonuspreprocessing_id
	,dim_krogerbonuspreprocessing_source
	,dim_krogerbonuspreprocessing_row
	,dim_krogerbonuspreprocessing_histdate
	,sid_dbprocessinfo_dbnumber
	,dim_krogerbonuspreprocessing_portfolionumber
	,dim_krogerbonuspreprocessing_membernumber
	,dim_krogerbonuspreprocessing_tinssn
	,dim_krogerbonuspreprocessing_producttype
	,dim_krogerbonuspreprocessing_cardnumber
	,dim_krogerbonuspreprocessing_transactiondate
	,dim_krogerbonuspreprocessing_transfercardnumber
	,dim_krogerbonuspreprocessing_transactioncode
	,dim_krogerbonuspreprocessing_ddanumber
	,dim_krogerbonuspreprocessing_transactionamount
	,dim_krogerbonuspreprocessing_transactioncount
	,dim_krogerbonuspreprocessing_transactiondescription
	,dim_krogerbonuspreprocessing_currencycode
	,dim_krogerbonuspreprocessing_merchantid
	,dim_krogerbonuspreprocessing_transactionid
	,dim_krogerbonuspreprocessing_authorizationcode
	,dim_krogerbonuspreprocessing_tipnumber
	FROM KrogerBonusPreProcessing
WHERE dim_krogerbonuspreprocessing_tipnumber <> 'DELETED'

DELETE pp
FROM KrogerBonusPreProcessing pp
INNER JOIN KrogerBonusPreProcessingArchive ppa
ON pp.dim_krogerbonuspreprocessing_id = ppa.dim_krogerbonuspreprocessing_id

RAISERROR('Process Complete for %s', 0, 0, @tipfirst) WITH NOWAIT

