USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_UpdateAffiliatStatus]    Script Date: 01/11/2012 11:14:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_UpdateAffiliatStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_UpdateAffiliatStatus]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_UpdateAffiliatStatus]    Script Date: 01/11/2012 11:14:03 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_FIS_UpdateAffiliatStatus] @Tipfirst char(3) 
AS 
declare @SQLupdate nvarchar(max)


SET	@sqlupdate =	N'UPDATE	[<<DBNAME>>].dbo.affiliat
					  SET		acctstatus = ''C''
					  WHERE		acctid in (SELECT acctnum FROM cardsin WHERE status = ''C'') '
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
Exec sp_executesql @SQLUPDATE

GO


