USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[uspPre_LoadBeginning_Balance]    Script Date: 06/24/2010 10:03:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspPre_LoadBeginning_Balance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspPre_LoadBeginning_Balance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspPre_LoadBeginning_Balance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2010
-- Description:	Load Beginning Balance Table with all customers
-- =============================================
CREATE PROCEDURE [dbo].[uspPre_LoadBeginning_Balance]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(150), @SQLInsert nvarchar(2000)
	
	set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
				
	set @SQLInsert = N''insert into '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table
						select tipnumber, ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0'''', ''''0''''
						from '' + QuoteName(@DBName) + N''.dbo.customer
						where tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table) ''
	Exec sp_executesql @SQLInsert						

END
' 
END
GO
