USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spAllowForHotCardedCards]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAllowForHotCardedCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAllowForHotCardedCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAllowForHotCardedCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spAllowForHotCardedCards] @Tipfirst char(3) 
AS 

/******************************************************************/
/*                                                                */
/* BY S Blanchette                                                */
/* DATE 6/09                                                      */
/* SCAN SEB001                                                    */
/* REASON To get rid of the cursor processing                     */
/*                                                                */
/******************************************************************/
declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
declare @DBName varchar(50), @SQLSet nvarchar(2000), @SQLIf nvarchar(2000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(2000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

if exists(select * from dbo.sysobjects where xtype=''u'' and name = ''Hottemp'') 
	BEGIN
		drop table dbo.Hottemp 
	END 


set @SQLInsert=''select distinct tipnumber, oldcc, ssn, lastname, ddanum, period, cardtype
				into dbo.Hottemp
				from dbo.cardsin
				where Cardtype=''''D'''' 
						and status = ''''C'''' 
						and tipnumber is not null 
						and left(tipnumber,1)<>'''' '''' 
						and oldcc is not null and left(oldcc,1)<>'''' ''''
						and oldcc not in (select acctid from '' + QuoteName(@DBName) + N''.dbo.affiliat) ''
Exec sp_executesql @SQLInsert

set @SQLInsert = ''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SecID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					select oldcc, tipnumber, cardtype, period, ssn, ''''A'''', '''''''', lastname, ''''0'''', ddanum
					from dbo.Hottemp ''
Exec sp_executesql @SQLInsert


set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.affiliat
					set AcctType=''''Credit''''
					where AcctType=''''C'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.affiliat
					set AcctType=''''Debit''''
					where AcctType=''''D'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.affiliat
					set AcctTypeDesc = (select Accttypedesc 
											from '' + QuoteName(@DBName) + N''.dbo.accttype
        								where 	accttype='' + QuoteName(@DBName) + N''.dbo.affiliat.accttype) 
					where len(AcctTypeDesc) = ''''0'''' ''
Exec sp_executesql @SQLUpdate

/*  Commented out SEB001
declare Cardsin_crsr cursor
for select tipnumber, oldcc, ssn, lastname, ddanum, period, cardtype
from cardsin
where Cardtype=''D'' and status = ''C'' and tipnumber is not null and left(tipnumber,1)<>'' '' and oldcc is not null and left(oldcc,1)<>'' ''

open Cardsin_crsr
fetch Cardsin_crsr into @TIPNUMBER, @OLDCC, @SSN, @LastName, @DDANum, @Period, @CardType
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

/*  Set the ACCTTYPE                     */
	if @Cardtype=''C''
	Begin
		Set @Accttype=''Credit''
	end
	else
		if @Cardtype=''D''
		Begin
			set @Accttype=''Debit''
		end

/*    Set the Account Type description    */
	set @SQLSet=''set @accttypedesc=(select Accttypedesc 
				from '' + QuoteName(@DBName) + N''.dbo.accttype
        			where 	accttype=@Accttype) ''
	Exec sp_executesql @SQLSet, N''@accttypedesc nvarchar(50) output, @Accttype nchar(20)'', @accttypedesc=@accttypedesc output, @Accttype=@Accttype

	set @sqlif=''if (not exists (select acctid 
				from '' + QuoteName(@DBName) + N''.dbo.affiliat 
				where acctid= @OLDCC) and @OLDCC is not null and left(@OLDCC,1)<>'''' '''')
			begin
				-- Add to AFFILIAT TABLE
				INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SecID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
				values(@OLDCC, @tipnumber, @Accttype, cast(@Period as datetime), @SSN, ''''A'''', @accttypedesc, @lastname, ''''0'''', @DDANUM) 
			end ''
	Exec sp_executesql @SQLif, N''@OLDCC nvarchar(16), @TIPNUMBER nvarchar(15), @accttype char(20), @accttypedesc char(50), @Period nchar(14), @LASTNAME nvarchar(40), @SSN char(9), @DDANUM char(13)'', 
			@OLDCC=@OLDCC, @TIPNUMBER=@TIPNUMBER, @Period=@Period, @LASTNAME=@LASTNAME, @SSN=@SSN, @DDANUM=@DDANUM, @accttypedesc=@accttypedesc, @accttype=@accttype

goto Next_Record
Next_Record:
	fetch Cardsin_crsr into @TIPNUMBER, @OLDCC, @SSN, @LastName, @DDANum, @Period, @CardType

end

Fetch_Error:
close  Cardsin_crsr
deallocate  Cardsin_crsr */' 
END
GO
