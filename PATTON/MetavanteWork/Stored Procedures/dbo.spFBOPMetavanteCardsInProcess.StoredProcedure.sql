USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPMetavanteCardsInProcess]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPMetavanteCardsInProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPMetavanteCardsInProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPMetavanteCardsInProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spFBOPMetavanteCardsInProcess]
as  


/******************************************************/
/* Section to get all names on same record            */
/******************************************************/

/******************************************************/
/*  By S. Blanchette                                  */
/*  Date 8/08                                         */
/*  SCAN SEB001                                       */
/*  Reason  Change logic to fix error that is occuring*/
/*          with FBOP where it''s leaving a name blank */
/*          when populating names                     */
/******************************************************/


declare @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @tipnumber nchar(15)

delete from FBOPcardname

declare Cardsin_View_by_Tipnumber_crsr cursor
for select tipnumber, NA1, NA2,NA3, NA4, NA5, NA6
from FBOPCardsin_View_by_Tipnumber
where status<>''C''
/* where ddanum is not null */
/* for update */ 
/*                                                                            */
open Cardsin_View_by_Tipnumber_crsr
/*                                                                            */
fetch Cardsin_View_by_Tipnumber_crsr into @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
	if not exists(select * from FBOPcardname where Tipnumber=@Tipnumber)
	begin
		insert FBOPcardname(Tipnumber, NA1, NA2, NA3, NA4, NA5, NA6) values(@Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6)
		goto Next_Record1
	end
	
	else
/* SEB001       if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (substring(NA2,1,1) like '' '' or NA2 is null)) */
/* SEB001*/	if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (len(ltrim(rtrim(NA2)))=''0'' or NA2 is null))
		begin
			update FBOPcardname
			set NA2=@NA1, NA3=@NA2, NA4=@NA3, NA5=@NA4, NA6=@NA5
			where Tipnumber=@Tipnumber
			goto Next_Record1
		end
		else
/* SEB001 		if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (substring(NA3,1,1) like '' '' or NA3 is null)) */
/* SEB001*/		if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (len(ltrim(rtrim(NA3)))=''0'' or NA3 is null))
			begin
				update FBOPcardname
				set NA3=@NA1, NA4=@NA2, NA5=@NA3, NA6=@NA4
				where Tipnumber=@Tipnumber
				goto Next_Record1
			end
			else
/* SEB001 			if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (substring(NA4,1,1) like '' '' or NA4 is null)) */
/* SEB001*/			if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (len(ltrim(rtrim(NA4)))=''0'' or NA4 is null))
				begin
					update FBOPcardname
					set NA4=@NA1, NA5=@NA2, NA6=@NA3
					where Tipnumber=@Tipnumber
					goto Next_Record1
				end
				else
/* SEB001 				if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (substring(NA5,1,1) like '' '' or NA5 is null)) */
/* SEB001*/				if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (len(ltrim(rtrim(NA5)))=''0'' or NA5 is null))
					begin
						update FBOPcardname
						set NA5=@NA1, NA6=@NA2
						where Tipnumber=@Tipnumber
						goto Next_Record1
					end
					else
/* SEB001					if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (substring(NA6,1,1) like '' '' or NA6 is null)) */
/* SEB001*/					if exists(select * from FBOPcardname where Tipnumber=@Tipnumber and (len(ltrim(rtrim(NA6)))=''0'' or NA6 is null))
						begin
							update FBOPcardname
							set NA6=@NA1
							where Tipnumber=@Tipnumber
							goto Next_Record1
						end
		
		
			
Next_Record1:
	fetch Cardsin_View_by_Tipnumber_crsr into @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6
	
end

Fetch_Error1:
close Cardsin_View_by_Tipnumber_crsr
deallocate Cardsin_View_by_Tipnumber_crsr

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update FBOPcardname
set na2=null
where na2=na1

update FBOPcardname
set na3=null
where na3=na1 or na3=na2

update FBOPcardname
set na4=null
where na4=na1 or na4=na2 or na4=na3

update FBOPcardname
set na5=null
where na5=na1 or na5=na2 or na5=na3 or na5=na4

update FBOPcardname
set na6=null
where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

/******************************************************************************/
/* Section to move names to the beginning of the name fields on same record   */
/******************************************************************************/
declare @count numeric(1,0)
set @count=0

while @count<5
begin

	update FBOPcardname
	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
/* SEB001	where na1 is null or substring(na1,1,1) like '' '' */
/*SEB001 */	where na1 is null or len(ltrim(rtrim(na1))) = ''0''

	update FBOPcardname
	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
/* SEB001	where na2 is null or substring(na2,1,1) like '' '' */
/*SEB001 */	where na2 is null or len(ltrim(rtrim(na2))) = ''0''

	update FBOPcardname
	set na3=na4, na4=na5, na5=na6, na6=null
/* SEB001	where na3 is null or substring(na3,1,1) like '' '' */
/*SEB001 */	where na3 is null or len(ltrim(rtrim(na3))) = ''0''

	update FBOPcardname
	set na4=na5, na5=na6, na6=null
/* SEB001	where na4 is null or substring(na4,1,1) like '' '' */
/*SEB001 */	where na4 is null or len(ltrim(rtrim(na4))) = ''0''

	update FBOPcardname
	set na5=na6, na6=null
/* SEB001	where na5 is null or substring(na5,1,1) like '' '' */
/*SEB001 */	where na5 is null or len(ltrim(rtrim(na5))) = ''0''

	set @count= @count + 1
end

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Update names to the Cardsin Table                                       */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
	update FBOPCardsin
	set NA1=(select NA1 from FBOPcardname where Tipnumber=FBOPCardsin.Tipnumber),
	    NA2=(select NA2 from FBOPcardname where Tipnumber=FBOPCardsin.Tipnumber),
	    NA3=(select NA3 from FBOPcardname where Tipnumber=FBOPCardsin.Tipnumber),
	    NA4=(select NA4 from FBOPcardname where Tipnumber=FBOPCardsin.Tipnumber),
	    NA5=(select NA5 from FBOPcardname where Tipnumber=FBOPCardsin.Tipnumber),
	    NA6=(select NA6 from FBOPcardname where Tipnumber=FBOPCardsin.Tipnumber)

/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/


update FBOPCardsin
set joint=''J''
/*SEB001 where NA2 is not null and substring(na2,1,1) not like '' ''  */
/*SEB001 */ where NA2 is not null and len(ltrim(rtrim(na2))) > ''0''

update FBOPCardsin
set joint=''S''
/*SEB001 where NA2 is null or substring(na2,1,1) like '' '' */
/*SEB001 */ where NA2 is null or len(ltrim(rtrim(na2))) = ''0''' 
END
GO
