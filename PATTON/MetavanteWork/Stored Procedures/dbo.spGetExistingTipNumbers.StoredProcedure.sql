USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbers]    Script Date: 04/12/2011 09:03:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetExistingTipNumbers]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbers]    Script Date: 04/12/2011 09:03:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGetExistingTipNumbers] @TipFirst char(3)
AS
 /* Revision                                   */
/* By Sarah Blanchette                         */
/* Date 11/24/2007                             */
/* SCAN  SEB001                                */
/* Remove name check on auto combine per Doug at FBOP   */

/* By Sarah Blanchette                         */
/* Date 2/2009                             */
/* SCAN  SEB002                                */
/* Change statement to get top 1 because transactions are getting 2 different tips */
declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/*  Get tipnumber based on account number      */
set @SQLUpdate='update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, ' + QuoteName(@DBName) + N'.dbo.affiliat b
		where a.tipnumber is null and a.acctnum=b.acctid '
Exec sp_executesql @SQLUpdate

/*  Get tipnumber based on old account number  */
set @SQLUpdate='update cardsin 
		set tipnumber=b.tipnumber
		from cardsin a, ' + QuoteName(@DBName) + N'.dbo.affiliat b
		where a.tipnumber is null and a.oldcc=b.acctid '
Exec sp_executesql @SQLUpdate

/*  Get tipnumber based on existing DDA number  */
/* CWH, RBM 20110412 */
set @SQLUpdate = N'update CARDSIN set TIPNUMBER = aff.tipnumber	from CARDSIN ci
					inner join
					(
						select custid, MIN(tipnumber) as tipnumber
						from ' + Quotename(@DBName) + N'.dbo.AFFILIAT
						where rewardsnow.dbo.ufn_CharIsNullOrEmpty(replace(custid, ''0'', '''')) = 0
						group by custid
					) aff
					on ci.DDANUM = aff.CustID
					left outer join CorporatePrefixes cp
						on ci.tipfirst = cp.TipPrefix
					where ci.TIPNUMBER is null
						and cp.TipPrefix is null'						

exec sp_executesql @SQLUpdate

/*
/*  Get tipnumber based on ddanumber as long as joint codes are the same      */
drop table [wrktab3]

select distinct ddanum, tipnumber 
into [wrktab3]
from cardsin
where ddanum is not null and tipnumber is not null and not exists(select * from CorporatePrefixes where TipPrefix=TipFirst)

update cardsin
set tipnumber=[wrktab3].tipnumber
from cardsin, [wrktab3]
where cardsin.ddanum is not null and cardsin.ddanum not like '0000000000%' and cardsin.tipnumber is null and cardsin.ddanum=[wrktab3].ddanum 
*/

/*  Get tipnumber based on ssn na1 na2 joint are the same      */
update cardsin
set na2=' '
where na2 is null or Len(na2)<'1'  /* 09/15/2009 */

drop table wrktab1n

select distinct ssn, na1, na2, joint, tipnumber 
--select distinct ssn, tipnumber 
into wrktab1n
from cardsin
where ssn is not null and ssn not like '00000%' and  left(ssn,3)<> '999' and tipnumber is not null and not exists(select * from CorporatePrefixes where TipPrefix=TipFirst)

/* start SEB002 */
--update cardsin
--set cardsin.tipnumber=wrktab1n.tipnumber
--from cardsin, wrktab1n
----where cardsin.ssn is not null and cardsin.ssn not like '00000%' and  left(cardsin.ssn,3)<> '999' and cardsin.tipnumber is null and cardsin.ssn=wrktab1n.ssn 
--where (cardsin.ssn is not null and left(cardsin.ssn,3) not in ('000', '   ', '999')) and cardsin.tipnumber is null and cardsin.ssn=wrktab1n.ssn and ((cardsin.na1=wrktab1n.na1 and cardsin.na2=wrktab1n.na2) or (cardsin.na1=wrktab1n.na2 and cardsin.na2=wrktab1n.na1)) 

update cardsin
set tipnumber=(select top 1 tipnumber from wrktab1n where ssn=cardsin.ssn and ((na1=cardsin.na1 and na2=cardsin.na2) or (na2=cardsin.na1 and na1=cardsin.na2) ) )
where ssn is not null and left(ssn,3) not in ('000', '   ', '999') and tipnumber is null and exists( select * from wrktab1n where ssn=cardsin.ssn and ((na1=cardsin.na1 and na2=cardsin.na2) or (na2=cardsin.na1 and na1=cardsin.na2)) )

delete from cardsin
where tipnumber is null and status<>'A'
GO


