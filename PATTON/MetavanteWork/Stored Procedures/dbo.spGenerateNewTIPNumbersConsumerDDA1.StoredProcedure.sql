USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDA1]    Script Date: 05/08/2012 10:14:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersConsumerDDA1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDA1]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDA1]    Script Date: 05/08/2012 10:14:06 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDA1] @tipfirst varchar(3)
AS 

EXEC [dbo].[usp_FIS_TIPassignment] @tipfirst

GO


