USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHandleOptOutsKROGER_Daily]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsKROGER_Daily]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHandleOptOutsKROGER_Daily]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsKROGER_Daily]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spHandleOptOutsKROGER_Daily] 
	-- Add the parameters for the stored procedure here
	@TipPrefix char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here

/* Remove DebitCardInKroger for Opt Out people  */
	delete from  dbo.cardsin
	where exists(select * from optoutcontrol where acctnumber= dbo.Cardsin.ACCTNUM)

END
' 
END
GO
