USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spOptOutFileInputProcess]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutFileInputProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spOptOutFileInputProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutFileInputProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 12/08
-- Description:	Process the OptOutInputFile to Opt out participants
-- =============================================
CREATE PROCEDURE [dbo].[spOptOutFileInputProcess]
	-- Add the parameters for the stored procedure here
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Tipnumber			varchar(15)

	declare csrOptOut cursor FAST_FORWARD for
		select Tipnumber 
		from OptOutInputFile
	/*                                                                            */
	open csrOptOut
	/*                                                                            */
	fetch csrOptOut into @Tipnumber
	/*                                                                            */
	while @@FETCH_STATUS = 0
	begin

		exec spOptOutControlEntry @Tipnumber

		fetch csrOptOut into @Tipnumber
	
	End	

END
' 
END
GO
