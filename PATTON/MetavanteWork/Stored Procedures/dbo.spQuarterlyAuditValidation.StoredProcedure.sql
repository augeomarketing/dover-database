USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] @TipFirst char(15)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/

declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @errmsg varchar(50), @currentend numeric(9)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate=''Truncate Table '' + QuoteName(@DBName) + N''.dbo.Quarterly_Audit_ErrorFile ''
Exec sp_executesql @SQLTruncate

set @errmsg=''Ending Balances do not match''

set @SQLInsert = N''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Quarterly_Audit_ErrorFile
	select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
	from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity b
	where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints ''
Exec sp_executesql @SQLInsert, N''@errmsg varchar(50)'', @errmsg = @errmsg' 
END
GO
