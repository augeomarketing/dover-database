USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kProcessDemographicsMidMonth]    Script Date: 01/19/2011 15:37:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kProcessDemographicsMidMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kProcessDemographicsMidMonth]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kProcessDemographicsMidMonth]    Script Date: 01/19/2011 15:37:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_b2kProcessDemographicsMidMonth]
AS
BEGIN
	--New Customers
	INSERT INTO b2kDemogMidMonth
	(
		dim_b2kdemog_relationshipaccount
		, dim_b2kdemog_cardnumber
		, dim_b2kdemog_cardtype
		, dim_b2kdemog_accounttype
		, dim_b2kdemog_primaryfirstname
		, dim_b2kdemog_primarymiddleinitial
		, dim_b2kdemog_primarylastname
		, dim_b2kdemog_secondaryfirstname
		, dim_b2kdemog_secondarymiddleinitial
		, dim_b2kdemog_secondarylastname
		, dim_b2kdemog_addressline1
		, dim_b2kdemog_addressline2
		, dim_b2kdemog_addressline3
		, dim_b2kdemog_addressline4
		, dim_b2kdemog_city
		, dim_b2kdemog_state
		, dim_b2kdemog_country
		, dim_b2kdemog_zipcode
		, dim_b2kdemog_homephonenumber
		, dim_b2kdemog_workphonenumber
		, dim_b2kdemog_emailaddress
		, dim_b2kdemog_scprogramstatus
		, dim_b2kdemog_scprogramstatuschangedate
		, dim_b2kdemog_hostaccountstatus
		, dim_b2kdemog_hostacctstatusdate
		, dim_b2kdemog_scparticipationflag
		, dim_b2kdemog_primarysocialsecuritynumber
		, dim_b2kdemog_newcardnumber
		, dim_b2kdemog_associationid
		, dim_b2kdemog_corpid
		, dim_b2kdemog_agentinstitutionid
		, dim_b2kdemog_billcode
		, dim_b2kdemog_binplan
		, dim_b2kdemog_scenrollmentdate
		, dim_b2kdemog_rnstatus
		, dim_b2kdemog_lastmodified
		, dim_b2kdemog_importdate
		, dim_b2kdemog_filedate
		, dim_b2kdemog_productline
		, dim_b2kdemog_subproducttype
		, dim_b2kdemog_bank
		, dim_b2kdemog_agent
	)
	SELECT 
		dim_b2kdemogimportraw_relationshipaccount
		, dim_b2kdemogimportraw_cardnumber
		, dim_b2kdemogimportraw_cardtype
		, dim_b2kdemogimportraw_accounttype
		, dim_b2kdemogimportraw_primaryfirstname
		, dim_b2kdemogimportraw_primarymiddleinitial
		, dim_b2kdemogimportraw_primarylastname
		, dim_b2kdemogimportraw_secondaryfirstname
		, dim_b2kdemogimportraw_secondarymiddleinitial
		, dim_b2kdemogimportraw_secondarylastname
		, dim_b2kdemogimportraw_addressline1
		, dim_b2kdemogimportraw_addressline2
		, dim_b2kdemogimportraw_addressline3
		, dim_b2kdemogimportraw_addressline4
		, dim_b2kdemogimportraw_city
		, dim_b2kdemogimportraw_state
		, dim_b2kdemogimportraw_country
		, dim_b2kdemogimportraw_zipcode
		, dim_b2kdemogimportraw_homephonenumber
		, dim_b2kdemogimportraw_workphonenumber
		, dim_b2kdemogimportraw_emailaddress
		, dim_b2kdemogimportraw_scprogramstatus
		, dim_b2kdemogimportraw_scprogramstatuschangedate
		, dim_b2kdemogimportraw_hostaccountstatus
		, dim_b2kdemogimportraw_hostacctstatusdate
		, dim_b2kdemogimportraw_scparticipationflag
		, dim_b2kdemogimportraw_primarysocialsecuritynumber
		, dim_b2kdemogimportraw_newcardnumber
		, dim_b2kdemogimportraw_associationid
		, dim_b2kdemogimportraw_corpid
		, dim_b2kdemogimportraw_agentinstitutionid
		, dim_b2kdemogimportraw_billcode
		, left(dim_b2kdemogimportraw_binplan, 6) as dim_b2kdemogimportraw_binplan
		, dim_b2kdemogimportraw_scenrollmentdate
		, 'A' AS dim_b2kdemogimportraw_rnstatus
		, GETDATE() AS dim_b2kdemogimportraw_lastmodified
		, dim_b2kdemogimportraw_importdate
		, dim_b2kdemogimportraw_filedate
		, dmgraw.dim_b2kdemogimportraw_productline
		, dmgraw.dim_b2kdemogimportraw_subproducttype
		, tf.bank
		, tf.agent	
		
	FROM
		b2kDemogImportRawMidMonth dmgraw
	INNER JOIN vw_CRTipFirstTable_TipFirstByProdLineSubType tf
		ON dmgraw.dim_b2kdemogimportraw_binplan = tf.Bin
		and dmgraw.dim_b2kdemogimportraw_productline = tf.ProductLine
		and dmgraw.dim_b2kdemogimportraw_subproducttype = tf.SubProductType
	LEFT OUTER JOIN b2kDemogMidMonth demog
		ON dmgraw.dim_b2kdemogimportraw_cardnumber = demog.dim_b2kdemog_cardnumber	
	WHERE
		dim_b2kdemog_cardnumber IS NULL
		--AND dim_b2kdemogimportraw_recordtype = 'A'

	--Customers with New Cards
	INSERT INTO b2kDemogMidMonth
	(
		dim_b2kdemog_relationshipaccount
		, dim_b2kdemog_cardnumber
		, dim_b2kdemog_cardtype
		, dim_b2kdemog_accounttype
		, dim_b2kdemog_primaryfirstname
		, dim_b2kdemog_primarymiddleinitial
		, dim_b2kdemog_primarylastname
		, dim_b2kdemog_secondaryfirstname
		, dim_b2kdemog_secondarymiddleinitial
		, dim_b2kdemog_secondarylastname
		, dim_b2kdemog_addressline1
		, dim_b2kdemog_addressline2
		, dim_b2kdemog_addressline3
		, dim_b2kdemog_addressline4
		, dim_b2kdemog_city
		, dim_b2kdemog_state
		, dim_b2kdemog_country
		, dim_b2kdemog_zipcode
		, dim_b2kdemog_homephonenumber
		, dim_b2kdemog_workphonenumber
		, dim_b2kdemog_emailaddress
		, dim_b2kdemog_scprogramstatus
		, dim_b2kdemog_scprogramstatuschangedate
		, dim_b2kdemog_hostaccountstatus
		, dim_b2kdemog_hostacctstatusdate
		, dim_b2kdemog_scparticipationflag
		, dim_b2kdemog_primarysocialsecuritynumber
		, dim_b2kdemog_newcardnumber
		, dim_b2kdemog_associationid
		, dim_b2kdemog_corpid
		, dim_b2kdemog_agentinstitutionid
		, dim_b2kdemog_billcode
		, dim_b2kdemog_binplan
		, dim_b2kdemog_scenrollmentdate
		, dim_b2kdemog_rnstatus
		, dim_b2kdemog_lastmodified
		, dim_b2kdemog_importdate
		, dim_b2kdemog_filedate
		, dim_b2kdemog_productline
		, dim_b2kdemog_subproducttype
		, dim_b2kdemog_bank
		, dim_b2kdemog_agent
	)
	SELECT 
		dim_b2kdemogimportraw_relationshipaccount
		, dim_b2kdemogimportraw_newcardnumber as dim_b2kdemogimportraw_cardnumber
		, dim_b2kdemogimportraw_cardtype
		, dim_b2kdemogimportraw_accounttype
		, dim_b2kdemogimportraw_primaryfirstname
		, dim_b2kdemogimportraw_primarymiddleinitial
		, dim_b2kdemogimportraw_primarylastname
		, dim_b2kdemogimportraw_secondaryfirstname
		, dim_b2kdemogimportraw_secondarymiddleinitial
		, dim_b2kdemogimportraw_secondarylastname
		, dim_b2kdemogimportraw_addressline1
		, dim_b2kdemogimportraw_addressline2
		, dim_b2kdemogimportraw_addressline3
		, dim_b2kdemogimportraw_addressline4
		, dim_b2kdemogimportraw_city
		, dim_b2kdemogimportraw_state
		, dim_b2kdemogimportraw_country
		, dim_b2kdemogimportraw_zipcode
		, dim_b2kdemogimportraw_homephonenumber
		, dim_b2kdemogimportraw_workphonenumber
		, dim_b2kdemogimportraw_emailaddress
		, dim_b2kdemogimportraw_scprogramstatus
		, dim_b2kdemogimportraw_scprogramstatuschangedate
		, dim_b2kdemogimportraw_hostaccountstatus
		, dim_b2kdemogimportraw_hostacctstatusdate
		, dim_b2kdemogimportraw_scparticipationflag
		, dim_b2kdemogimportraw_primarysocialsecuritynumber
		, '' as dim_b2kdemogimportraw_newcardnumber
		, dim_b2kdemogimportraw_associationid
		, dim_b2kdemogimportraw_corpid
		, dim_b2kdemogimportraw_agentinstitutionid
		, dim_b2kdemogimportraw_billcode
		, left(dim_b2kdemogimportraw_binplan, 6) as dim_b2kdemogimportraw_binplan
		, dim_b2kdemogimportraw_scenrollmentdate
		, 'A' AS dim_b2kdemogimportraw_rnstatus
		, GETDATE() AS dim_b2kdemogimportraw_lastmodified
		, dim_b2kdemogimportraw_importdate
		, dim_b2kdemogimportraw_filedate
		, dmgraw.dim_b2kdemogimportraw_productline
		, dmgraw.dim_b2kdemogimportraw_subproducttype
		, tf.bank
		, tf.agent	
	FROM
		b2kDemogImportRawMidMonth dmgraw
	INNER JOIN vw_CRTipFirstTable_TipFirstByProdLineSubType tf
		ON dmgraw.dim_b2kdemogimportraw_binplan = tf.Bin
		and dmgraw.dim_b2kdemogimportraw_productline = tf.ProductLine
		and dmgraw.dim_b2kdemogimportraw_subproducttype = tf.SubProductType
	LEFT OUTER JOIN b2kDemog demog
		ON dmgraw.dim_b2kdemogimportraw_newcardnumber = demog.dim_b2kdemog_cardnumber	
	WHERE
		demog.dim_b2kdemog_cardnumber IS NULL
		and RewardsNow.dbo.ufn_CharIsNullOrEmpty(dmgraw.dim_b2kdemogimportraw_newcardnumber) = 0
		--AND dim_b2kdemogimportraw_recordtype = 'A'

	--Demographic Changes to Existing Accounts
	UPDATE b2kDemogMidMonth
	set
	  dim_b2kdemog_relationshipaccount = dim_b2kdemogimportraw_relationshipaccount
	  , dim_b2kdemog_cardtype = dim_b2kdemogimportraw_cardtype
	  , dim_b2kdemog_accounttype = dim_b2kdemogimportraw_accounttype
	  , dim_b2kdemog_primaryfirstname = dim_b2kdemogimportraw_primaryfirstname
	  , dim_b2kdemog_primarymiddleinitial = dim_b2kdemogimportraw_primarymiddleinitial
	  , dim_b2kdemog_primarylastname = dim_b2kdemogimportraw_primarylastname
	  , dim_b2kdemog_secondaryfirstname = dim_b2kdemogimportraw_secondaryfirstname
	  , dim_b2kdemog_secondarymiddleinitial = dim_b2kdemogimportraw_secondarymiddleinitial
	  , dim_b2kdemog_secondarylastname = dim_b2kdemogimportraw_secondarylastname
	  , dim_b2kdemog_addressline1 = dim_b2kdemogimportraw_addressline1
	  , dim_b2kdemog_addressline2 = dim_b2kdemogimportraw_addressline2
	  , dim_b2kdemog_addressline3 = dim_b2kdemogimportraw_addressline3
	  , dim_b2kdemog_addressline4 = dim_b2kdemogimportraw_addressline4
	  , dim_b2kdemog_city = dim_b2kdemogimportraw_city
	  , dim_b2kdemog_state = dim_b2kdemogimportraw_state
	  , dim_b2kdemog_country = dim_b2kdemogimportraw_country
	  , dim_b2kdemog_zipcode = dim_b2kdemogimportraw_zipcode
	  , dim_b2kdemog_homephonenumber = dim_b2kdemogimportraw_homephonenumber
	  , dim_b2kdemog_workphonenumber = dim_b2kdemogimportraw_workphonenumber
	  , dim_b2kdemog_emailaddress = dim_b2kdemogimportraw_emailaddress
	  , dim_b2kdemog_scprogramstatus = dim_b2kdemogimportraw_scprogramstatus
	  , dim_b2kdemog_scprogramstatuschangedate = dim_b2kdemogimportraw_scprogramstatuschangedate
	  , dim_b2kdemog_hostaccountstatus = dim_b2kdemogimportraw_hostaccountstatus
	  , dim_b2kdemog_hostacctstatusdate = dim_b2kdemogimportraw_hostacctstatusdate
	  , dim_b2kdemog_scparticipationflag = dim_b2kdemogimportraw_scparticipationflag
	  , dim_b2kdemog_primarysocialsecuritynumber = dim_b2kdemogimportraw_primarysocialsecuritynumber
	  , dim_b2kdemog_newcardnumber = dim_b2kdemogimportraw_newcardnumber
	  , dim_b2kdemog_associationid = dim_b2kdemogimportraw_associationid
	  , dim_b2kdemog_corpid = dim_b2kdemogimportraw_corpid
	  , dim_b2kdemog_agentinstitutionid = dim_b2kdemogimportraw_agentinstitutionid
	  , dim_b2kdemog_billcode = dim_b2kdemogimportraw_billcode
	  , dim_b2kdemog_binplan = left(dim_b2kdemogimportraw_binplan, 6)
	  , dim_b2kdemog_scenrollmentdate = dim_b2kdemogimportraw_scenrollmentdate
	  , dim_b2kdemog_importdate = dim_b2kdemogimportraw_importdate
	  , dim_b2kdemog_filedate = dim_b2kdemogimportraw_filedate
	  , dim_b2kdemog_lastmodified = getdate()
	FROM b2kDemogMidMonth dmg
	INNER JOIN b2kDemogImportRawMidMonth dmgr
	ON dmg.dim_b2kdemog_cardnumber = dmgr.dim_b2kdemogimportraw_cardnumber
	WHERE dmgr.dim_b2kdemogimportraw_recordtype = 'C'
	
	--Update Status based on Business Rules (Mapped from b2kImportStatusMapping...)
	UPDATE demog
		SET demog.dim_b2kdemog_rnstatus = map.dim_b2kimportstatusmapping_rnstatus
	FROM b2kDemogMidMonth demog
	INNER JOIN b2kImportStatusMapping map
		ON map.dim_b2kimportstatusmapping_scparticipationflag = demog.dim_b2kdemog_scparticipationflag
		and map.dim_b2kimportstatusmapping_scprogramstatus = demog.dim_b2kdemog_scprogramstatus
	WHERE demog.dim_b2kdemog_rnstatus != map.dim_b2kimportstatusmapping_rnstatus
	
	-- Set Old Card Number for appropriate accounts
	
	update b2kDemogMidMonth
	SET dim_b2kdemog_oldcardnumber = n.oldcard
	from b2kDemogMidMonth d
	inner join
	( 
		select dim_b2kdemog_cardnumber as oldcard, dim_b2kdemog_newcardnumber as cardnumber
		from b2kDemogMidMonth
		where RewardsNow.dbo.ufn_CharIsNullOrEmpty(dim_b2kdemog_newcardnumber) = 0
	) n
	on d.dim_b2kdemog_cardnumber = n.cardnumber
	
	
END


GO

