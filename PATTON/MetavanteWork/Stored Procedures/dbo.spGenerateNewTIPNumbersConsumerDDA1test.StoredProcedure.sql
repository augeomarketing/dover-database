USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDA1test]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersConsumerDDA1test]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDA1test]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersConsumerDDA1test]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDA1test] 
	-- Add the parameters for the stored procedure here
	@tipfirst varchar(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)
/* SB003 */declare @tiplast varchar(12)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/****************       Start SEB002    ******************/
--set @SQLSelect=''SELECT @newnum = max(TIPNUMBER)
--		from '' + QuoteName(@DBName) + N''.dbo.affiliat 
--		where left(tipnumber,3)=@TipFirst ''
--Exec sp_executesql @SQLSelect, N''@newnum bigint output, @Tipfirst char(3)'', @newnum=@newnum output, @TipFirst=@Tipfirst

--if @newnum is null
--	begin
--	set @newnum=@TipFirst + ''000000000000''
--	end
--set @newnum = @newnum + 1
truncate table checkparm

declare @LastTipUsed char(15), @tipnumber varchar(15) 

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

/*****************************************************/
/*  SEB003            */
/*                    */
set @tipnumber = @LastTipUsed
Set @tiplast = Right(@tipnumber, 12)
Set @tiplast = @tiplast + 1

Set @tipnumber = rtrim(@tipfirst) + @tiplast
set @tipnumber=left(ltrim(@tipnumber), 3) + REPLICATE (''0'', (15- len(@tipnumber) )) + right(rtrim(@tipnumber), (len(@tipnumber)-3))

--set @Newnum = cast(@LastTipUsed as bigint) + 1  

/*                    */
/**************************************************/
/*  End SEB003 */

/*********  End SEB002  *************************/

update cardsin
set na2='' ''
where na2 is null

update cardsin
set na3='' ''
where na3 is null

update cardsin
set na4='' ''
where na4 is null

update cardsin
set na5='' ''
where na5 is null

update cardsin
set na6='' ''
where na6 is null

delete from cardsin2

insert into cardsin2 
select * from cardsin
order by tipfirst, ddanum

drop table wrktab3

select distinct ddanum, tipnumber
into wrktab3
from cardsin2
where tipfirst=@tipFirst and tipnumber is null and ddanum is not null



/* Assign Get New Tips */
UPDATE  wrktab3
		set @tiplast = @tiplast + 1,  

		@NewTip = 	rtrim(@tipfirst) + Cast( ( @tiplast  + 1) as char(12) ),
		@NewTip=left(ltrim(@NewTip), 3) + REPLICATE (''0'', (15- len(@NewTip) )) + right(rtrim(@NewTip), (len(@NewTip)-3)),

	      TIPNUMBER =  @NewTip 
	Where TipNumber is null 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
--declare tip_crsr cursor
--for select tipnumber
--from wrktab3
--for update
--
--/*                                                                            */
--open tip_crsr
--/*                                                                            */
--fetch tip_crsr 
--/******************************************************************************/	
--/*                                                                            */
--/* MAIN PROCESSING  VERIFICATION                                              */
--/*                                                                            */
--if @@FETCH_STATUS = 1
--	goto Fetch_Error
--/*                                                                            */
--while @@FETCH_STATUS = 0
--	begin	
--		/*  SEB003 set @NewTip = Cast(@Newnum as char)
--		set @NewTip = replicate(''0'',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip   /* Added 11/29/2006 */  */
--		/* SEB003 */ set @NewTip = @tipnumber
--		
--		update wrktab3	
--		set tipnumber = @Newtip 
--		where current of tip_crsr
--		
--		
--		/* SEB003 set @newnum = @newnum + 1 */		
--/* SEB003 */
--	Set @tiplast = Right(@tipnumber, 12)
--	Set @tiplast = @tiplast + 1
--
--	Set @tipnumber = rtrim(@tipfirst) + @tiplast
--	set @tipnumber=left(ltrim(@tipnumber), 3) + REPLICATE (''0'', (15- len(@tipnumber) )) + right(rtrim(@tipnumber), (len(@tipnumber)-3))
--
--		
--/* SEB003 */		
--		goto Next_Record
--Next_Record:
--		fetch tip_crsr
--	end
--
--Fetch_Error:
--close  tip_crsr
--deallocate  tip_crsr
END
' 
END
GO
