USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGetExistingTipNumbers]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetExistingTipNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGetExistingTipNumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetExistingTipNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPGetExistingTipNumbers] @TipFirst char(3)
AS
 /* Revision                                   */
/* By Sarah Blanchette                         */
/* Date 11/24/2007                             */
/* SCAN  SEB001                                */
/* Remove name check on auto combine per Doug at FBOP   */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/*  Get tipnumber based on account number      */
set @SQLUpdate=''update FBOPcardsin 
		set tipnumber=b.tipnumber
		from FBOPcardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.acctnum=b.acctid ''
Exec sp_executesql @SQLUpdate

/*  Get tipnumber based on old account number  */
set @SQLUpdate=''update FBOPcardsin 
		set tipnumber=b.tipnumber
		from FBOPcardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and a.oldcc=b.acctid ''
Exec sp_executesql @SQLUpdate


set @SQLUpdate=''update FBOPcardsin 
		set tipnumber=b.tipnumber
		from FBOPcardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where ddanum is not null and ddanum not like ''''0000000000%'''' and a.tipnumber is null and a.ddanum=b.custid ''
Exec sp_executesql @SQLUpdate


set @SQLUpdate=''update FBOPcardsin 
		set tipnumber=b.tipnumber
		from FBOPcardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where ssn is not null and ssn not like ''''00000%'''' and  left(ssn,1)<> ''''9'''' and a.tipnumber is null and ssn=b.secid '' 
Exec sp_executesql @SQLUpdate


delete from FBOPcardsin
where tipnumber is null and status<>''A''' 
END
GO
