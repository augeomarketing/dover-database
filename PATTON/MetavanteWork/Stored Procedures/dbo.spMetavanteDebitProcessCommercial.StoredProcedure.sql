USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteDebitProcessCommercial]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcessCommercial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteDebitProcessCommercial]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcessCommercial]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spMetavanteDebitProcessCommercial]
as  


/*           Convert Trancode                                                     */
update DebitCardIn
set DebitCardIn.Trancode=TrancodeXref.trancodeout 
from DebitCardIn, TrancodeXref 
where debitcardin.trancode=TrancodeXref.trancodeIn 

update debitcardin	
set numpurch=''1'', purch=points
where trancode=''67''

update debitcardin	
set numret=''1'', amtret=points
where trancode=''37''

update debitcardin
set ddanum=savnum
where ddanum=''00000000000''

delete from debitcardin
where ssn=''987654321''

update debitcardin
set status=''C''
where status=''B''

/************************************************************************************************/
/*    Need to get date from input variable                                                                */
/*                                                                                                                            */
/************************************************************************************************/
update debitcardin
set TIPMID=''0000000''

/* rearrange name from  Last, First to First Last */
update debitcardin
set NA1=rtrim(substring(na1,charindex('','',na1)+2,len(rtrim(na1)))) + '' '' + substring(na1,1,charindex('','',na1)-1)
where substring(na1,1,1) not like '' '' and na1 is not null and NA1 LIKE ''%,%''

/******************************************************/
/* Section to remove duplicate names on same record   */
/******************************************************/

update debitcardin
set na2=null
where na2=na1

/******************************************************/
/* Section to roll up based on acct#                  */
/******************************************************/

--drop table DebitRollUp
DELETE FROM DebitRollUp
DELETE FROM DebitRollUpa

--SELECT acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr 
--into DebitRollUp 
--FROM debitcardin
--GROUP BY acctnum
--ORDER BY acctnum
insert into DebitRollUp 
SELECT acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr, SigvsPin 
FROM debitcardin
GROUP BY acctnum, SigvsPin
ORDER BY acctnum, SigvsPin

/*                                                                            */
/* New code to replace cursor processing for rolling acct demographic data  */
/*                                                                            */

--drop table AccountRollUp
DELETE FROM AccountRollUp

--select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS
--into AccountRollUp 
--FROM debitcardin
--ORDER BY acctnum
insert into AccountRollUp 
select distinct TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, TRANCODE, POINTS, SigvsPin
FROM debitcardin
ORDER BY acctnum


DELETE FROM debitcardin2

INSERT into debitcardin2(TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, NUMPURCH, PURCH, NUMRET, AMTRET, TRANCODE, POINTS)
Select TIPFIRST, TIPMID, TIPLAST, TIPNUMBER, TYPE, SSN, DDANUM, SAVNUM, BANK, CLIENT, a.ACCTNUM, OLDCC, NA1, LASTNAME, PERIOD, NA2, NA3, NA4, NA5, NA6, ADDR1, ADDR2, ADDR3, CITYSTATE, ZIP, STATUS, PHONE1, PHONE2, b.NUMPURCH, b.amtPURCH, b.NUMcr, b.AMTcr, TRANCODE, POINTS		        	
from AccountRollUp a, DebitRollUp b
where a.acctnum=b.acctnum  

/*                                                                                 */
/* End of New code to replace cursor processing for rolling acct demographic data  */
/*                                                                                 */


/********************************************************************************/
/*                                                      	                */
/*                              	                             	        */
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/*                                                       		  	*/
/*                                                        			*/
/********************************************************************************/
update debitcardin2
set joint=''J''
where NA2 is not null and substring(na2,1,1) not like '' ''

update debitcardin2
set joint=''S''
where NA2 is null or substring(na2,1,1) like '' ''' 
END
GO
