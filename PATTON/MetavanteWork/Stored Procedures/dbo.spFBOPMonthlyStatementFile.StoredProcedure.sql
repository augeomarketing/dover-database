USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPMonthlyStatementFile]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPMonthlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPMonthlyStatementFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPMonthlyStatementFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPMonthlyStatementFile] 
 @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3)
AS 
--set @StartDateParm = ''06/01/2009''
--set @EndDateParm = ''06/30/2009''
--set @Tipfirst = ''50A''

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

/*******************************************************************************/
/* SEB 2/2008                                                                  */
/* SCAN SEB001                                                                 */
/* added code to create a summary record                                       */
/*******************************************************************************/
/*******************************************************************************/
/* SEB 2/2008                                                                  */
/* SCAN SEB002                                                                 */
/* Changed to reflect new transcodes                                           */
/*******************************************************************************/
/*******************************************************************************/
/* SEB 6/2008                                                                  */
/* SCAN SEB003                                                                 */
/* Changed to reflect new transcodes for Business cards                        */
/*      3B is returns                                                          */
/*      6B is purchases                                                        */
/*      F5  Bonus Business Online Banking                                      */
/*      F6  Bonus Business Bill Pay                                            */
/*	F7  Bonus Business Direct Deposit                                      */
/*	F8  Bonus Business Ten Debit                                           */
/*******************************************************************************/
/*******************************************************************************/
/* SEB 8/2010                                                                  */
/* SCAN SEB004                                                                 */
/* Changed to reflect new of DZ                                                */
/*******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket=''MonthBeg'' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.FBOPStatement ''
exec sp_executesql @SQLTruncate


if exists(select name from sysobjects where name=''wrkhist'')
begin 
	truncate table wrkhist
end

set @SQLSelect=''insert into wrkhist
		select tipnumber, trancode, sum(points) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.FBOPStatement(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), left(zipcode,5) 
				from '' + QuoteName(@DBName) + N''.dbo.customer order by tipnumber ''
Exec sp_executesql @SQLInsert

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement 
	set acctnum = rtrim(b.acctid), ddanum=rtrim(b.custid) 
	from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
	where a.tipnumber = b.tipnumber and b.acctstatus=''''A'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set lastfour=right(rtrim(acctnum),4) ''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   63       */

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointspurchasedcr=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''63'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointspurchaseddb=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''67'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with HELOC purchases   67       */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointspurchasedhe=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''6H'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Business purchases   6B       */
/* SEB003  */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointspurchasedbus=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode=''''6B'''' '' 	
	Exec sp_executesql @SQLUpdate 


--set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonuscr=
--		(select sum(points) from wrkhist 
--   SEB002		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode= ''''FJ'''') 	
--		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode= ''''FJ'''') ''
--	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with bonuses            */
/* SEB002  */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonuscr=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode in (''''FJ'''', ''''FG'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode in (''''FJ'''', ''''FG'''')) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with debit bonuses            */
--set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonusdb=
--		(select sum(points) from wrkhist 
--   SEB002		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in(''''FC'''', ''''FD'''', ''''FI''''))) 	
--		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in(''''FC'''', ''''FD'''', ''''FI'''' ))) ''
--	Exec sp_executesql @SQLUpdate 


/* Load the statmement file with debit bonuses            */
/* SEB002   */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonusdb=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode in(''''FC'''', ''''FD'''', ''''FI'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode in(''''FC'''', ''''FD'''', ''''FI'''' )) ''
	Exec sp_executesql @SQLUpdate 


/* Load the statmement file with HELOC bonuses            */
--set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonushe=
--		(select sum(points) from wrkhist 
--  SEB002		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode = ''''FH'''')) 	
--		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode = ''''FH'''' )) ''
--	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with HELOC bonuses            */
/* SEB002 */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonushe=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode = ''''FH'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode = ''''FH'''' )) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with business bonuses            */
/* SEB003   */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonusbus=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode in(''''F5'''', ''''F6'''', ''''F7'''', ''''F8'''')) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and trancode in(''''F5'''', ''''F6'''', ''''F7'''', ''''F8'''')) ''
	Exec sp_executesql @SQLUpdate 



/* Load the statmement file with Employee Incentive CR            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set PointsBonusEmpCR=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''FF''''))) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''FF'''' ))) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Employee Incentive DB            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set PointsBonusEmpDB=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''FE''''))) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''FE'''' ))) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Generic bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonus=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''BI'''', ''''BE'''', ''''BT''''))) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''BI'''', ''''BE'''', ''''BT'''' ))) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Generic bonuses            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbonus=pointsbonus -
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''BX''''))) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode in (''''BX'''' ))) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsadded=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''IE'''' '' 	
	Exec sp_executesql @SQLUpdate 

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsadded = pointsadded +
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''TR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point increases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointspurchasedhe + pointspurchasedbus + pointsbonuscr + pointsbonusdb + pointsbonushe + pointsbonusbus + PointsBonusEmpCR + PointsBonusEmpDB + pointsbonus + pointsadded ''
	Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsredeemed=
		(select sum(points) from wrkhist 
		where tipnumber= '' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode like ''''R%'''' or trancode=''''IR'''') ) 	
		where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.FBOPStatement.tipnumber and (trancode like ''''R%'''' or trancode=''''IR'''') ) ''
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsredeemed=pointsredeemed-
		b.points 
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''DR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/*****************************************************/
/*                                                   */
/*  START SEB004                                     */
/*                                                   */
/*****************************************************/
/* Load the statmement file decreased redeemed CLASS DZ        */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsredeemed=pointsredeemed-
		b.points 
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''DZ'''' '' 	
	Exec sp_executesql @SQLUpdate 
/*****************************************************/
/*                                                   */
/*  END SEB004                                     */
/*                                                   */
/*****************************************************/

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsreturnedcr=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''33'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsreturneddb=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''37'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with HELOC returns            */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsreturnedhe=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''3H'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Business returns            */
/* SEB003   */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsreturnedbus=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''3B'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with minus adjustments    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointssubtracted=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''DE'''' '' 	
	Exec sp_executesql @SQLUpdate 

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointssubtracted=pointssubtracted +
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''FR'''' '' 	
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Points Having Expired    */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set PointsHavingExpired=
		b.points
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
		where a.tipnumber = b.tipnumber and b.trancode = ''''XP'''' '' 	
	Exec sp_executesql @SQLUpdate 

--set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointssubtracted=pointssubtracted +
--		b.points
--		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, wrkhist b 
--		where a.tipnumber = b.tipnumber and b.trancode = ''''BX'''' '' 	
--	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point decreases */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointsreturnedhe + pointsreturnedbus + pointssubtracted + PointsHavingExpired''
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsbegin = b.'' + Quotename(@MonthBucket) + N'' 
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
		where a.tipnumber = b.tipnumber ''
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.FBOPStatement set pointsend=pointsbegin + pointsincreased - pointsdecreased ''
exec sp_executesql @SQLUpdate


/* Create summary record */
/**********************************/
/*  This section added  SEB001    */
/**********************************/

Truncate table FBOPSummary


set @SQLUpdate=N''insert into '' + QuoteName(@DBName) + N''.dbo.FBOPStatement 
		select ''''999999999999999'''' 
		, ''''SUMMARY RECORD'''' 
		, ''''TOTALS'''' 
		, '''' '''' 
		, '''' '''' 
		, '''' '''' 
		, '''' ''''	
		, '''' '''' 
		, sum(PointsBegin) 
		, sum(PointsEnd) 
		, sum(PointsPurchasedCR) 
		, sum(PointsPurchasedDB) 
		, sum(PointsPurchasedHE) 
		, sum(PointsPurchasedbus) 
		, sum(PointsBonusCR) 
		, sum(PointsBonusDB) 
		, sum(PointsBonusHE) 
		, sum(PointsBonusbus) 
		, sum(PointsBonusEmpCR) 
		, sum(PointsBonusEmpDB) 
		, sum(PointsBonus) 
		, sum(PointsAdded) 
		, sum(PointsIncreased) 
		, sum(PointsRedeemed) 
		, sum(PointsReturnedCR) 
		, sum(PointsReturnedDB) 
		, sum(PointsReturnedHE) 
		, sum(PointsReturnedbus) 
		, sum(PointsSubtracted) 
		, sum(PointsDecreased) 
		, sum(PNTDEBIT) 
		, sum(PNTMORT) 
		, sum(PNTHOME) 
		, '''' '''' 
		, '''' ''''
		, '''' '''' 
		, '''' '''' 
		, 0 
		, sum(PointsHavingExpired) 
		from '' + QuoteName(@DBName) + N''.dbo.FBOPStatement ''
exec sp_executesql @SQLUpdate

' 
END
GO
