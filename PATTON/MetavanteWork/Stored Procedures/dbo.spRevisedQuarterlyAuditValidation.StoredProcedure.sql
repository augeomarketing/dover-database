USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spRevisedQuarterlyAuditValidation]    Script Date: 09/30/2010 11:28:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRevisedQuarterlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 9/2010
-- Description:	Validation
-- =============================================
CREATE PROCEDURE [dbo].[spRevisedQuarterlyAuditValidation] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @errmsg varchar(50), @currentend numeric(9)

	set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	Truncate Table dbo.Quarterly_Audit_ErrorFile 
	
	set @errmsg=''Ending Balances do not match''

	INSERT INTO dbo.Quarterly_Audit_ErrorFile
		select a.Tip, PNTBEG, PNTEND, CRPurch, CRBonus, AdjustAdd, DBPurch, DBBonus, TotalAdd, Redeem, ReturnCR, AdjustSub, ReturnDB, TotalSub, @errmsg, b.AdjustedEndingPoints
		from dbo.Revised_Quarterly_Statement a, dbo.Current_Month_Activity b
		where a.tip=b.tipnumber and a.PNTEND<> b.AdjustedEndingPoints 
	
END
' 
END
GO
