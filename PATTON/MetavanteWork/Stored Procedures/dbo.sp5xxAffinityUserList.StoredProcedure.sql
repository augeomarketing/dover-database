USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[sp5xxAffinityUserList]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp5xxAffinityUserList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp5xxAffinityUserList]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp5xxAffinityUserList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Allen Barriere
-- Create date: 3/5/2009
-- Description:	Get the list of users to send to Affinity
-- =============================================
CREATE PROCEDURE [dbo].[sp5xxAffinityUserList]
AS
BEGIN
  TRUNCATE TABLE MetavanteWork.dbo.affinityuserlist
  DECLARE @dbpatton varchar(50)
  DECLARE @tipfirst varchar(3)
  DECLARE @sql nvarchar(4000)
  DECLARE FI_TO_COLLECT CURSOR FAST_FORWARD FOR 
    SELECT dim_affinitybonus_tipfirst FROM rn1.rewardsnow.dbo.affinitybonus
    WHERE dim_affinitybonus_active = 1

  OPEN FI_TO_COLLECT 
    FETCH NEXT FROM FI_TO_COLLECT INTO @tipfirst
    WHILE (@@FETCH_STATUS <> -1)
    BEGIN
      SELECT TOP 1 @dbpatton = DBNamePatton FROM rewardsnow.dbo.dbprocessinfo
        WHERE dbnumber = @tipfirst

      SET @sql = ''INSERT INTO MetavanteWork.dbo.affinityuserlist (dim_affinityuserlist_RNNumber, dim_affinityuserlist_Runavailable, dim_affinityuserlist_RunBalance, dim_affinityuserlist_RunRedeemed, dim_affinityuserlist_status, dim_affinityuserlist_dateadded, dim_affinityuserlist_lastname, dim_affinityuserlist_Acctname1, dim_affinityuserlist_Acctname2, dim_affinityuserlist_Acctname3, dim_affinityuserlist_Acctname4, dim_affinityuserlist_Address1, dim_affinityuserlist_Address2, dim_affinityuserlist_Address3, dim_affinityuserlist_Address4, dim_affinityuserlist_City, dim_affinityuserlist_State, dim_affinityuserlist_zip, dim_affinityuserlist_acctid)
                  SELECT c.Tipnumber as RNNumber, 
		                LTRIM(RTRIM(COALESCE(Runavailable,0))) AS Runavailable, 
		                LTRIM(RTRIM(COALESCE(RunBalance,0))) AS RunBalance, 
		                LTRIM(RTRIM(COALESCE(RunRedeemed,0))) AS RunRedeemed, 
		                LTRIM(RTRIM(COALESCE(Status,''''''''))) AS Status, 
		                LTRIM(RTRIM(COALESCE(c.DateAdded,''''''''))) AS DateAdded,  
		                LTRIM(RTRIM(COALESCE(c.Lastname,''''''''))) AS Lastname,  
		                LTRIM(RTRIM(COALESCE(Acctname1, ''''''''))) AS Acctname1,  
		                LTRIM(RTRIM(COALESCE(Acctname2, ''''''''))) AS Acctname2,  
		                LTRIM(RTRIM(COALESCE(Acctname3, ''''''''))) AS Acctname3,  
		                LTRIM(RTRIM(COALESCE(Acctname4, ''''''''))) AS Acctname4,  
		                LTRIM(RTRIM(COALESCE(Address1,''''''''))) AS Address1,  
		                LTRIM(RTRIM(COALESCE(Address2,''''''''))) AS Address2,  
		                LTRIM(RTRIM(COALESCE(Address3,''''''''))) AS Address3,  
		                LTRIM(RTRIM(COALESCE(Address4, ''''''''))) AS Address4,  
		                LTRIM(RTRIM(COALESCE(City, ''''''''))) AS City,  
		                LTRIM(RTRIM(COALESCE(State,  ''''''''))) AS State,  
		                LTRIM(RTRIM(COALESCE(ZipCode,''''''''))) AS ZipCode,  
		                LTRIM(RTRIM(COALESCE(AcctId,''''''''))) AS AcctId
                    FROM '' + QUOTENAME(@dbpatton)+ ''.dbo.customer c LEFT OUTER JOIN '' + QUOTENAME(@dbpatton) + ''.dbo.affiliat a ON c.tipnumber = a.tipnumber''
      EXEC sp_executesql @sql
  --print @sql

      FETCH NEXT FROM FI_TO_COLLECT INTO @tipfirst
    END
  CLOSE FI_TO_COLLECT
  DEALLOCATE FI_TO_COLLECT
END
' 
END
GO
