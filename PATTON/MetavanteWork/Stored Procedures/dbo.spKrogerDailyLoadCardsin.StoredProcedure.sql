USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKrogerDailyLoadCardsin]    Script Date: 10/14/2010 08:56:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKrogerDailyLoadCardsin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKrogerDailyLoadCardsin]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKrogerDailyLoadCardsin]    Script Date: 10/14/2010 08:56:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spKrogerDailyLoadCardsin] 
	-- Add the parameters for the stored procedure here
	
@tipfirst varchar(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Truncate table [MetavanteWork].[dbo].[CARDSIN]

	/***************************************************/
	/*  Add contact info                               */
	/***************************************************/

	INSERT INTO [MetavanteWork].[dbo].[CARDSIN]
			   ([TIPFIRST]
			   ,[JOINT]
			   ,DDANUM
			   ,[ACCTNUM]
			   ,[OLDCC]
			   ,[NA6]
			   ,[STATUS]
			   ,[Cardtype]
			   )
	select	@tipfirst,
			'S',
			PrimaryDDA,
			CardNumber,
			left(ReplacementCard,16), 
			LoyaltyNumber,
			CardStatus,
			left(CardType,1)
	from dbo.Kroger_Daily_Card_Common_Detail

	update [MetavanteWork].[dbo].[CARDSIN]
	set STATUS='A'
	where STATUS in ('1', '4', '7', '8')

	update [MetavanteWork].[dbo].[CARDSIN]
	set STATUS='C'
	where STATUS in ('2', '9')

	/***************************************************/
	/*  Add contact info                               */
	/***************************************************/

	update [MetavanteWork].[dbo].[CARDSIN]
	set SSN = b.PrimarySSN,
		NA1 = b.PrimaryCardName, 
		NA2 = b.SecondaryCardholderName, 
		PHONE1 = b.HomePhoneNumber,
		PHONE2 = b.BusinessPhoneNumber,
		LASTNAME = b.PrimaryCardholderLastName
	from [MetavanteWork].[dbo].[CARDSIN] a, [MetavanteWork].dbo.Kroger_Daily_Card_Contact_Detail b
	where a.[NA6] = b.LoyaltyNumber and a.[ACCTNUM] = b.CardNumber

	/***************************************************/
	/*  Add address info                               */
	/***************************************************/
	update [MetavanteWork].[dbo].[CARDSIN]
	set ADDR1 = b.Address1,
		ADDR2 = b.Address2,
		ADDR3 = b.Address3,
		CITYSTATE = RTRIM(b.city) + ' ' + b.state,
		ZIP = b.Zip
	from [MetavanteWork].[dbo].[CARDSIN] a, [MetavanteWork].dbo.Kroger_Daily_Card_Address_Detail b
	where a.[NA6] = b.LoyaltyNumber and a.[ACCTNUM] = b.CardNumber

END


GO


