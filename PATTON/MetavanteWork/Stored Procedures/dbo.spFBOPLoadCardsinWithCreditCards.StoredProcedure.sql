USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPLoadCardsinWithCreditCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadCardsinWithCreditCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPLoadCardsinWithCreditCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadCardsinWithCreditCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPLoadCardsinWithCreditCards] @TipPrefix char(3)
as

Declare @DBName varchar(50), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000)


Truncate Table FBOPCardsin

--populate the table Cardsin with recs from table CreditCardIn where Tipfirst=@TipPrefix
set @SQLInsert=''INSERT INTO FBOPCardsin
				Select *, ''''C'''', ''''''''  from FBOPcreditcardin
				where Tipfirst=@TipPrefix
				order by Tipfirst, ssn''
exec sp_executesql @SQLInsert, N''@TipPrefix char(3)'', @TipPrefix= @TipPrefix' 
END
GO
