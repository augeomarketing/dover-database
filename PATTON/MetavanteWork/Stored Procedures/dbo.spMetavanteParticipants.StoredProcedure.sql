USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteParticipants]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteParticipants]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteParticipants]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteParticipants]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'Create Procedure [dbo].[spMetavanteParticipants]
as

Declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName varchar(100), @Tip nchar(3), @Credit int, @Creditout int, @Debit int, @Debitout int, @Total int, @Totalout int

truncate table Metavantework.dbo.MetavanteParticipants

DECLARE cDBName CURSOR FOR
--	SELECT  rtrim(DBName) from OnlineHistoryWork.dbo.DBProcessInfoRedemptions
--	SELECT  rtrim(DBNameNEXL) from RewardsNow.dbo.DBProcessInfo
	SELECT  distinct rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
	where dbnamepatton like ''5%''
	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName

WHILE (@@FETCH_STATUS=0)
	BEGIN
		if not exists (select name from master.dbo.sysdatabases where name not in (''tempdb'') and name like @DBName) /* 4/14/2007 */
		Begin
			goto nextrecord
		end

	set @Tip=left(@DBName,3)

	set  @SQLUpdate = ''Set  @Credit  = (  select count(*) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer where segmentcode=''''C'''') ''
	exec sp_executesql @SQLUpdate,N''@Credit int output'',  
		                  @Credit = @Creditout output 

	set  @SQLUpdate = ''Set  @Debit  = (  select count(*) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer where segmentcode<>''''C'''') ''
	exec sp_executesql @SQLUpdate,N''@Debit int output'',  
		                  @Debit = @Debitout output 

	set  @SQLUpdate = ''Set  @Total  = (  select count(*) from  ''+ QuoteName(rtrim(@DBName)) + N''.dbo.customer) ''
	exec sp_executesql @SQLUpdate,N''@Total int output'',  
		                  @Total = @Totalout output 

	insert into Metavantework.dbo.MetavanteParticipants values(@Tip, @Creditout, @Debitout, @Totalout) 

NEXTRECORD:	
		FETCH NEXT FROM cDBName INTO @DBName
	END

Fetch_Error:
CLOSE cDBName
DEALLOCATE cDBName' 
END
GO
