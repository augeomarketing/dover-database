USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerGetNameAddress_02]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGetNameAddress_02]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerGetNameAddress_02]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGetNameAddress_02]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerGetNameAddress_02] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Update [52JKroegerPersonalFinance].dbo.Customer
	Set
	  AcctName1	= rtrim(ltrim(replace(replace(replace((rtrim(b.FirstName) + '' '' + rtrim(b.MiddleName) + '' '' + RTRIM(b.LastName)),'' '',''<>''),''><'',''''),''<>'','' '')))
	, Address1	= b.Addr1
	, Address2	= b.Addr2
	, Address4	= rtrim(b.City) + '' '' + RTRIM(b.state) + '' '' + RTRIM(b.zip) 
	, City		= rtrim(b.City)
	, State		= rtrim(b.State)
	, Zipcode	= rtrim(b.zip)
	, HomePhone	= b.HomePhone
	, lastname	= b.lastname
	From [52JKroegerPersonalFinance].dbo.customer a, [MetavanteWork].[dbo].[KroegerNameAddr] b 
	Where a.TIPNUMBER = b.TIPNUMBER 

	Update [52JKroegerPersonalFinance].dbo.Customer
	Set
		misc2 = b.loyaltynumber
	From [52JKroegerPersonalFinance].dbo.customer a, [MetavanteWork].[dbo].[KroegerNameAddr] b 
	Where a.TIPNUMBER = b.TIPNUMBER	and b.oldLoyaltyNumber is not null 



/************************************************************/
/*     INSERT INTO CUSTOMER                                 */  
/************************************************************/
		INSERT INTO [52JKroegerPersonalFinance].dbo.Customer
		(	TIPNumber
			, Runavailable
			, Runbalance
			, Runredeemed
			, TIPFirst
			, TIPLast
			, AcctName1
			, AcctName2	
			, AcctName3
			, AcctName4
			, AcctName5
			, AcctName6
			, Address1
			, Address2
			, Address4
			, City
			, State
			, Zipcode
			, Status
			, StatusDescription
			, HomePhone
			, WorkPhone
			, DateAdded
			, Lastname
			, Misc1
			, Misc2
			, Misc3
			, SegmentCode
		)
		select tipnumber
			, ''0''
			, ''0''
			, ''0''
			, LEFT(tipnumber,3)
			, right(tipnumber,12)
			, rtrim(ltrim(replace(replace(replace((rtrim(FirstName) + '' '' + rtrim(MiddleName) + '' '' + RTRIM(LastName)),'' '',''<>''),''><'',''''),''<>'','' '')))
			, ''''
			, ''''
			, ''''
			, ''''
			, ''''
			, ADDR1
			, ADDR2
			, rtrim(City) + '' '' + RTRIM(state) + '' '' + RTRIM(zip) 
			, rtrim(City)
			, rtrim(state)
			, rtrim(zip)
			, ''A''
			, ''Active[A]''
			, HomePhone
			, ''''
			, getdate() 
			, RTRIM(LastName)
			, ''''
			, loyaltynumber
			, ''''
			, ''D''
		from  [MetavanteWork].[dbo].[KroegerNameAddr]
		where tipnumber is not null and tipnumber not in (select tipnumber from [52JKroegerPersonalFinance].dbo.Customer )
		
	Update [52JKroegerPersonalFinance].dbo.AFFILIAT
		Set LastName = b.LastName
	From [52JKroegerPersonalFinance].dbo.AFFILIAT a, [MetavanteWork].[dbo].[KroegerNameAddr] b 
	Where a.TIPNUMBER = b.TIPNUMBER 
/************************************************************/
/*     INSERT INTO AFFILIAT                                 */  
/************************************************************/

INSERT INTO [52JKroegerPersonalFinance].dbo.AFFILIAT
		(
			ACCTID
			, TIPNUMBER
			, AcctType
			, DATEADDED
			, SecID
			, AcctStatus
			, AcctTypeDesc
			, LastName
			, YTDEarned
			, CustID
		)
		select Cardnumber
			, tipnumber
			, ''Debit''
			, GETDATE()
			, ''''
			,''A''
			, ''Debit Card''
			, RTRIM(LastName)
			, ''0''
			, '''' 
		from  [MetavanteWork].[dbo].[KroegerNameAddr]
		where cardnumber is not null and Cardnumber not in (select acctid from [52JKroegerPersonalFinance].dbo.affiliat ) 
			
END
' 
END
GO
