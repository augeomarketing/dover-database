USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersCommercial1]    Script Date: 05/07/2012 10:41:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersCommercial1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersCommercial1]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersCommercial1]    Script Date: 05/07/2012 10:41:12 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersCommercial1] @tipfirst varchar(3)
AS

--CALLS OTHER COMMERCIAL SPROC.  THIS SPROC ONLY USED TO MAINTAIN EXISTING DTS REFERENCES
EXEC [dbo].[spGenerateNewTIPNumbersCommercial] @tipfirst


GO


