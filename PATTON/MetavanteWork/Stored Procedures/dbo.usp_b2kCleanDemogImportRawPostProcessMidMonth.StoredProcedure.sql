USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kCleanDemogImportRawPostProcessMidMonth]    Script Date: 01/19/2011 15:36:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kCleanDemogImportRawPostProcessMidMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kCleanDemogImportRawPostProcessMidMonth]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kCleanDemogImportRawPostProcessMidMonth]    Script Date: 01/19/2011 15:36:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_b2kCleanDemogImportRawPostProcessMidMonth]
AS
	TRUNCATE TABLE b2kDemogImportRawMidMonth

GO

