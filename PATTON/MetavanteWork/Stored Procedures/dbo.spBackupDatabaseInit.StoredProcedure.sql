USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBackupDatabaseInit]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupDatabaseInit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBackupDatabaseInit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupDatabaseInit]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spBackupDatabaseInit] @DBName varchar(100)
as

--declare @spath varchar(100), @sBackupPath varchar(100), @cleardbs varchar(100)
--set @sPath = ''D:\Program Files\Microsoft SQL Server\MSSQL$RN\BACKUP\''

--set @sBackupPath = @sPath + @DBName

backup database @DBName to disk = @DBName with init' 
END
GO
