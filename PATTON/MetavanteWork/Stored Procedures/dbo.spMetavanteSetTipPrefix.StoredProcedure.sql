USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spMetavanteSetTipPrefix]    Script Date: 03/03/2011 14:09:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteSetTipPrefix]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteSetTipPrefix]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spMetavanteSetTipPrefix]    Script Date: 03/03/2011 14:09:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spMetavanteSetTipPrefix]
as

update creditcardin
set tipfirst=(select tipfirst from CRTipFirst_Table where Bank=creditcardin.bank and agent=creditcardin.agent group by Tipfirst)
where RewardsNow.dbo.ufn_CharIsNullOrEmpty(TIPFIRST) = 1

update creditcardin
set tipmid='0000000'

delete from creditcardin
where tipfirst is null

Update creditcardin
set joint='J'
where NA2 is not null and len(rtrim(na2))<>'0'

update creditcardin
set joint='S'
where NA2 is null or len(rtrim(na2))='0'

GO


