USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pFormatNewFileStructure]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pFormatNewFileStructure]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pFormatNewFileStructure]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pFormatNewFileStructure]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pFormatNewFileStructure]
AS

Truncate Table Cust3

insert into Cust3(Bank, Agent, SSN, Acctnum, Oldcc, NA1, NA2, Status1, Status, Oldccpre, oldccpost, Addr1, addr2, CityState, City, State, Zip, Phone1, Phone2, Numpurch, Purch, Numret, Amtret, Period)
select distinct right(BK_NUM,4), right(AGT_BK_NUM,4), right(SSN,9), Acct_Num, '' '', Name1, Name2, '' '', left(CR_Rating,1), left(Acct_Num,6), right(Trans_Acct_Num,10), Addr1, Addr2, (rtrim(City)+'' ''+rtrim(State)), rtrim(City), rtrim(State), Zip, right(Home_Phone,10), right(Work_Phone,10), ''0'', ''0'', ''0'', ''0'', ''0''
from CreditIn

update Cust3
set oldccpre=''        '', oldccpost=''          ''
where oldccpost=''0000000000''

update Cust3
set oldcc=left(oldccpre,6)+oldccpost

update Cust3
set Numret=''0'', amtret=''0'', numpurch=''0'', purch=''0''
where exists(select * from creditin where acct_num=cust3.acctnum and deb_cred_stat in (''N'', ''X'') )

update Cust3
set Numpurch=1, purch=(select sum(amt*1) from creditin where acct_num=cust3.acctnum and deb_cred_stat=''D'')
where exists(select * from creditin where acct_num=cust3.acctnum and deb_cred_stat=''D'')

update Cust3
set Numret=1, amtret=(select sum(amt*1) from creditin where acct_num=cust3.acctnum and deb_cred_stat=''C'')
where exists(select * from creditin where acct_num=cust3.acctnum and deb_cred_stat=''C'')

update Cust3
set Numret=''0'', amtret=''0'', numpurch=''0'', purch=''0''
where status=''C''



/*update Cust3
set Numret=1, amtret=sum(b.amt*1)
from cust3 a, CreditIn b
where b.Acct_num=a.acctnum and b.Deb_Cred_Stat=''C'' */' 
END
GO
