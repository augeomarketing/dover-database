USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spCreateQuarterlyStatementTrailer]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementTrailer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateQuarterlyStatementTrailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementTrailer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		s. Blanchette
-- Create date: 6/2009
-- Description:	Create the Trailer Record for statements
-- =============================================
CREATE PROCEDURE [dbo].[spCreateQuarterlyStatementTrailer]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @Reccount char(10), @rec char(10)
    -- Insert statements for procedure here
	Truncate Table New_Quarterly_Statement_Trailer

	set @Rec = (select count(*) from New_Quarterly_Statement_Detail_out)
	set @Reccount = REPLICATE(''0'', 10- LEN(@Rec) ) + @Rec
	
	insert into New_Quarterly_Statement_Trailer
	values ( ''8'', @Reccount)
END
' 
END
GO
