USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBackupDatabases]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupDatabases]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBackupDatabases]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBackupDatabases]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spBackupDatabases]
as

declare @spath varchar(100), @sDBName varchar(100), @sBackupPath varchar(100), @cleardbs varchar(100)
set @sPath = ''D:\Program Files\Microsoft SQL Server\MSSQL$RN\BACKUP\''

DECLARE cur_databases CURSOR FOR
	select name from master.dbo.sysdatabases where name not in (''tempdb'') and name like ''5%'' 
	OPEN cur_databases 
	fetch next  from cur_databases into @sDBName
	
WHILE (@@FETCH_STATUS=0)
	BEGIN
-- Clear out backups in this folder
-- Uncomment if you like (clears out databases that have since been removed)
-- However this may defeat the purpose of this script, since the creation dates will have changed on all files
-- set @cleardbs = ''del "'' + @sPath + ''*.bak"''
-- exec master..xp_cmdshell @cleardbs
	   set @sBackupPath = @sPath + @sDBName
   	   backup database @sDBName to disk = @sBackupPath with init
   	   fetch next  from cur_databases into @sDBName
	END

close cur_databases
deallocate cur_databases' 
END
GO
