USE [MetavanteWork]
GO

IF OBJECT_ID(N'usp_b2kProcessDemographics') IS NOT NULL
	DROP PROCEDURE usp_b2kProcessDemographics
GO

CREATE PROCEDURE [dbo].[usp_b2kProcessDemographics]
AS

SET NOCOUNT ON

/*
--Get Settings
DECLARE @useBankAgent BIT
SELECT @useBankAgent = CONVERT(BIT, ISNULL(dim_b2kprocessingparameters_parametervalue, 0))
FROM b2kProcessingParameters
WHERE dim_b2kprocessingparameters_parametername = 'usebankagent'
*/

/***************************************************************************

Insert Key for New Cards based on CardNumber field.  Limit to valid bin map.

***************************************************************************/
INSERT INTO b2kDemog
(
	dim_b2kdemog_cardnumber
	, dim_b2kdemog_rnstatus
	, dim_b2kdemog_created
)
SELECT
	i.dim_b2kdemogimportraw_cardnumber
	,'N'
	, GETDATE()
FROM  vw_b2kDemogImport_AllLatestRecords i
INNER JOIN
	vw_B2KBinMapping binmap
ON
	LEFT(i.dim_b2kdemogimportraw_binplan, Rewardsnow.dbo.ufn_Least(CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR), ',')) 
	= LEFT(binmap.bin, Rewardsnow.dbo.ufn_Least(CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR), ','))
	AND i.dim_b2kdemogimportraw_productline = binmap.ProductLine
	AND i.dim_b2kdemogimportraw_subproducttype = binmap.SubProductType
LEFT OUTER JOIN b2kDemog dmg ON
	i.dim_b2kdemogimportraw_cardnumber = dmg.dim_b2kdemog_cardnumber
WHERE
	dmg.dim_b2kdemog_cardnumber IS NULL
GROUP BY
	i.dim_b2kdemogimportraw_cardnumber

/***************************************************************************

Insert Key for New Cards based on New Card field.  Limit to valid bin map.

***************************************************************************/
INSERT INTO b2kDemog (dim_b2kdemog_cardnumber, dim_b2kdemog_oldcardnumber, dim_b2kdemog_rnstatus, dim_b2kdemog_created)
SELECT
	i.dim_b2kdemogimportraw_newcardnumber AS dim_b2kdemogimportraw_cardnumber
	, i.dim_b2kdemogimportraw_cardnumber AS dim_b2kdemogimportraw_oldcardnumber
	, 'N'
	, GETDATE()
FROM vw_b2kDemogImport_AllLatestRecords i
INNER JOIN
	vw_B2KBinMapping binmap
ON
	LEFT(i.dim_b2kdemogimportraw_binplan, Rewardsnow.dbo.ufn_Least(CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR), ',')) 
	= LEFT(binmap.bin, Rewardsnow.dbo.ufn_Least(CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR), ','))
	AND i.dim_b2kdemogimportraw_productline = binmap.ProductLine
	AND i.dim_b2kdemogimportraw_subproducttype = binmap.SubProductType
LEFT OUTER JOIN b2kDemog dmg ON
	i.dim_b2kdemogimportraw_newcardnumber = dmg.dim_b2kdemog_cardnumber
WHERE
	rewardsnow.dbo.ufn_CharIsNullOrEmpty(i.dim_b2kdemogimportraw_newcardnumber) = 0
	AND dmg.dim_b2kdemog_cardnumber IS NULL
GROUP BY
	i.dim_b2kdemogimportraw_newcardnumber
	, i.dim_b2kdemogimportraw_cardnumber

/***************************************************************************

Update New Demographics based on Card Field

***************************************************************************/
UPDATE dmg
SET
  dim_b2kdemog_relationshipaccount = i.dim_b2kdemogimportraw_relationshipaccount
  , dim_b2kdemog_cardtype = i.dim_b2kdemogimportraw_cardtype
  , dim_b2kdemog_accounttype = i.dim_b2kdemogimportraw_accounttype
  , dim_b2kdemog_primaryfirstname = i.dim_b2kdemogimportraw_primaryfirstname
  , dim_b2kdemog_primarymiddleinitial = i.dim_b2kdemogimportraw_primarymiddleinitial
  , dim_b2kdemog_primarylastname = i.dim_b2kdemogimportraw_primarylastname
  , dim_b2kdemog_secondaryfirstname = i.dim_b2kdemogimportraw_secondaryfirstname
  , dim_b2kdemog_secondarymiddleinitial = i.dim_b2kdemogimportraw_secondarymiddleinitial
  , dim_b2kdemog_secondarylastname = i.dim_b2kdemogimportraw_secondarylastname
  , dim_b2kdemog_addressline1 = i.dim_b2kdemogimportraw_addressline1
  , dim_b2kdemog_addressline2 = i.dim_b2kdemogimportraw_addressline2
  , dim_b2kdemog_addressline3 = i.dim_b2kdemogimportraw_addressline3
  , dim_b2kdemog_addressline4 = i.dim_b2kdemogimportraw_addressline4
  , dim_b2kdemog_city = i.dim_b2kdemogimportraw_city
  , dim_b2kdemog_state = i.dim_b2kdemogimportraw_state
  , dim_b2kdemog_country = i.dim_b2kdemogimportraw_country
  , dim_b2kdemog_zipcode = i.dim_b2kdemogimportraw_zipcode
  , dim_b2kdemog_homephonenumber = i.dim_b2kdemogimportraw_homephonenumber
  , dim_b2kdemog_workphonenumber = i.dim_b2kdemogimportraw_workphonenumber
  , dim_b2kdemog_emailaddress = i.dim_b2kdemogimportraw_emailaddress
  , dim_b2kdemog_scprogramstatus = i.dim_b2kdemogimportraw_scprogramstatus
  , dim_b2kdemog_scprogramstatuschangedate = i.dim_b2kdemogimportraw_scprogramstatuschangedate
  , dim_b2kdemog_hostaccountstatus = i.dim_b2kdemogimportraw_hostaccountstatus
  , dim_b2kdemog_hostacctstatusdate = i.dim_b2kdemogimportraw_hostacctstatusdate
  , dim_b2kdemog_scparticipationflag = i.dim_b2kdemogimportraw_scparticipationflag
  , dim_b2kdemog_primarysocialsecuritynumber = i.dim_b2kdemogimportraw_primarysocialsecuritynumber
  , dim_b2kdemog_newcardnumber = i.dim_b2kdemogimportraw_newcardnumber
  , dim_b2kdemog_associationid = i.dim_b2kdemogimportraw_associationid
  , dim_b2kdemog_corpid = i.dim_b2kdemogimportraw_corpid
  , dim_b2kdemog_agentinstitutionid = i.dim_b2kdemogimportraw_agentinstitutionid
  , dim_b2kdemog_billcode = i.dim_b2kdemogimportraw_billcode
  , dim_b2kdemog_productline = binmap.ProductLine
  , dim_b2kdemog_subproducttype = binmap.SubProductType
  , dim_b2kdemog_binplan = binmap.Bin
  , dim_b2kdemog_scenrollmentdate = i.dim_b2kdemogimportraw_scenrollmentdate
  , dim_b2kdemog_bank = binmap.Tipfirst
  , dim_b2kdemog_filedate = i.dim_b2kdemogimportraw_filedate
FROM b2kDemog dmg
INNER JOIN vw_b2kDemogImport_AllLatestRecords i
  ON dmg.dim_b2kdemog_cardnumber = i.dim_b2kdemogimportraw_cardnumber
INNER JOIN vw_B2KBinMapping binmap
ON
	LEFT(i.dim_b2kdemogimportraw_binplan, Rewardsnow.dbo.ufn_Least((CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR)), ','))
	= LEFT(binmap.bin, Rewardsnow.dbo.ufn_Least((CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR)), ','))
	AND i.dim_b2kdemogimportraw_productline = binmap.ProductLine
	AND i.dim_b2kdemogimportraw_subproducttype = binmap.SubProductType
 WHERE dmg.dim_b2kdemog_rnstatus IN ('N');

/***************************************************************************

Update New Demographics based on New Card Field

***************************************************************************/
UPDATE dmg
SET
  dim_b2kdemog_relationshipaccount = i.dim_b2kdemogimportraw_relationshipaccount
  , dim_b2kdemog_cardtype = i.dim_b2kdemogimportraw_cardtype
  , dim_b2kdemog_accounttype = i.dim_b2kdemogimportraw_accounttype
  , dim_b2kdemog_primaryfirstname = i.dim_b2kdemogimportraw_primaryfirstname
  , dim_b2kdemog_primarymiddleinitial = i.dim_b2kdemogimportraw_primarymiddleinitial
  , dim_b2kdemog_primarylastname = i.dim_b2kdemogimportraw_primarylastname
  , dim_b2kdemog_secondaryfirstname = i.dim_b2kdemogimportraw_secondaryfirstname
  , dim_b2kdemog_secondarymiddleinitial = i.dim_b2kdemogimportraw_secondarymiddleinitial
  , dim_b2kdemog_secondarylastname = i.dim_b2kdemogimportraw_secondarylastname
  , dim_b2kdemog_addressline1 = i.dim_b2kdemogimportraw_addressline1
  , dim_b2kdemog_addressline2 = i.dim_b2kdemogimportraw_addressline2
  , dim_b2kdemog_addressline3 = i.dim_b2kdemogimportraw_addressline3
  , dim_b2kdemog_addressline4 = i.dim_b2kdemogimportraw_addressline4
  , dim_b2kdemog_city = i.dim_b2kdemogimportraw_city
  , dim_b2kdemog_state = i.dim_b2kdemogimportraw_state
  , dim_b2kdemog_country = i.dim_b2kdemogimportraw_country
  , dim_b2kdemog_zipcode = i.dim_b2kdemogimportraw_zipcode
  , dim_b2kdemog_homephonenumber = i.dim_b2kdemogimportraw_homephonenumber
  , dim_b2kdemog_workphonenumber = i.dim_b2kdemogimportraw_workphonenumber
  , dim_b2kdemog_emailaddress = i.dim_b2kdemogimportraw_emailaddress
  , dim_b2kdemog_scprogramstatus = i.dim_b2kdemogimportraw_scprogramstatus
  , dim_b2kdemog_scprogramstatuschangedate = i.dim_b2kdemogimportraw_scprogramstatuschangedate
  , dim_b2kdemog_hostaccountstatus = i.dim_b2kdemogimportraw_hostaccountstatus
  , dim_b2kdemog_hostacctstatusdate = i.dim_b2kdemogimportraw_hostacctstatusdate
  , dim_b2kdemog_scparticipationflag = i.dim_b2kdemogimportraw_scparticipationflag
  , dim_b2kdemog_primarysocialsecuritynumber = i.dim_b2kdemogimportraw_primarysocialsecuritynumber
  , dim_b2kdemog_newcardnumber = i.dim_b2kdemogimportraw_newcardnumber
  , dim_b2kdemog_associationid = i.dim_b2kdemogimportraw_associationid
  , dim_b2kdemog_corpid = i.dim_b2kdemogimportraw_corpid
  , dim_b2kdemog_agentinstitutionid = i.dim_b2kdemogimportraw_agentinstitutionid
  , dim_b2kdemog_billcode = i.dim_b2kdemogimportraw_billcode
  , dim_b2kdemog_productline = binmap.ProductLine
  , dim_b2kdemog_subproducttype = binmap.SubProductType
  , dim_b2kdemog_binplan = binmap.Bin
  , dim_b2kdemog_scenrollmentdate = i.dim_b2kdemogimportraw_scenrollmentdate
  , dim_b2kdemog_bank = binmap.Tipfirst
  , dim_b2kdemog_oldcardnumber = i.dim_b2kdemogimportraw_cardnumber
  , dim_b2kdemog_filedate = i.dim_b2kdemogimportraw_filedate
FROM b2kDemog dmg
INNER JOIN vw_b2kDemogImport_AllLatestRecords i
  ON dmg.dim_b2kdemog_cardnumber = i.dim_b2kdemogimportraw_newcardnumber
INNER JOIN vw_B2KBinMapping binmap
ON
	LEFT(i.dim_b2kdemogimportraw_binplan, Rewardsnow.dbo.ufn_Least((CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR)), ','))
	= LEFT(binmap.bin, Rewardsnow.dbo.ufn_Least((CAST(LEN(i.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR)), ','))
	AND i.dim_b2kdemogimportraw_productline = binmap.ProductLine
	AND i.dim_b2kdemogimportraw_subproducttype = binmap.SubProductType
 WHERE dmg.dim_b2kdemog_rnstatus IN ('N')


/***************************************************************************

       Update Demog Demographics Data from latest entry 
(do NOT overwrite any 'old or new card number' fields with blanks!)

***************************************************************************/

MERGE b2kDemog AS target
USING (
	SELECT allrec.dim_b2kdemogimportraw_cardnumber
	  , allrec.dim_b2kdemogimportraw_relationshipaccount
	  , allrec.dim_b2kdemogimportraw_cardtype
	  , allrec.dim_b2kdemogimportraw_accounttype
	  , allrec.dim_b2kdemogimportraw_primaryfirstname
	  , allrec.dim_b2kdemogimportraw_primarymiddleinitial
	  , allrec.dim_b2kdemogimportraw_primarylastname
	  , allrec.dim_b2kdemogimportraw_secondaryfirstname
	  , allrec.dim_b2kdemogimportraw_secondarymiddleinitial
	  , allrec.dim_b2kdemogimportraw_secondarylastname
	  , allrec.dim_b2kdemogimportraw_addressline1
	  , allrec.dim_b2kdemogimportraw_addressline2
	  , allrec.dim_b2kdemogimportraw_addressline3
	  , allrec.dim_b2kdemogimportraw_addressline4
	  , allrec.dim_b2kdemogimportraw_city
	  , allrec.dim_b2kdemogimportraw_state
	  , allrec.dim_b2kdemogimportraw_country
	  , allrec.dim_b2kdemogimportraw_zipcode
	  , allrec.dim_b2kdemogimportraw_homephonenumber
	  , allrec.dim_b2kdemogimportraw_workphonenumber
	  , allrec.dim_b2kdemogimportraw_emailaddress
	  , allrec.dim_b2kdemogimportraw_scprogramstatus
	  , allrec.dim_b2kdemogimportraw_scprogramstatuschangedate
	  , allrec.dim_b2kdemogimportraw_hostaccountstatus
	  , allrec.dim_b2kdemogimportraw_hostacctstatusdate
	  , allrec.dim_b2kdemogimportraw_scparticipationflag
	  , allrec.dim_b2kdemogimportraw_primarysocialsecuritynumber
	  , allrec.dim_b2kdemogimportraw_newcardnumber
	  , allrec.dim_b2kdemogimportraw_associationid
	  , allrec.dim_b2kdemogimportraw_corpid
	  , allrec.dim_b2kdemogimportraw_agentinstitutionid
	  , allrec.dim_b2kdemogimportraw_billcode
	  , left(allrec.dim_b2kdemogimportraw_binplan, 6) as dim_b2kdemogimportraw_binplan
	  , allrec.dim_b2kdemogimportraw_productline
	  , allrec.dim_b2kdemogimportraw_subproducttype
	  , allrec.dim_b2kdemogimportraw_scenrollmentdate
	  , binmap.Tipfirst as dim_b2kdemogimportraw_tipfirst
	  , allrec.dim_b2kdemogimportraw_filedate
  FROM
  	vw_b2kDemogImport_AllLatestRecords allrec
  INNER JOIN
  	vw_B2KBinMapping binmap
  	ON 
	LEFT(allrec.dim_b2kdemogimportraw_binplan, Rewardsnow.dbo.ufn_Least((CAST(LEN(allrec.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR)), ','))
	= LEFT(binmap.bin, Rewardsnow.dbo.ufn_Least((CAST(LEN(allrec.dim_b2kdemogimportraw_binplan) AS VARCHAR) + ',' + CAST(LEN(binmap.bin) AS VARCHAR)), ','))
	AND allrec.dim_b2kdemogimportraw_productline = binmap.ProductLine
	AND allrec.dim_b2kdemogimportraw_subproducttype = binmap.SubProductType
	) as source 
	(
		dim_b2kdemog_cardnumber, dim_b2kdemogimportraw_relationshipaccount, dim_b2kdemogimportraw_cardtype, dim_b2kdemogimportraw_accounttype
	  , dim_b2kdemogimportraw_primaryfirstname, dim_b2kdemogimportraw_primarymiddleinitial, dim_b2kdemogimportraw_primarylastname
	  , dim_b2kdemogimportraw_secondaryfirstname, dim_b2kdemogimportraw_secondarymiddleinitial, dim_b2kdemogimportraw_secondarylastname
	  , dim_b2kdemogimportraw_addressline1, dim_b2kdemogimportraw_addressline2, dim_b2kdemogimportraw_addressline3
	  , dim_b2kdemogimportraw_addressline4, dim_b2kdemogimportraw_city, dim_b2kdemogimportraw_state
	  , dim_b2kdemogimportraw_country, dim_b2kdemogimportraw_zipcode, dim_b2kdemogimportraw_homephonenumber
	  , dim_b2kdemogimportraw_workphonenumber, dim_b2kdemogimportraw_emailaddress, dim_b2kdemogimportraw_scprogramstatus
	  , dim_b2kdemogimportraw_scprogramstatuschangedate, dim_b2kdemogimportraw_hostaccountstatus, dim_b2kdemogimportraw_hostacctstatusdate
	  , dim_b2kdemogimportraw_scparticipationflag, dim_b2kdemogimportraw_primarysocialsecuritynumber, dim_b2kdemogimportraw_newcardnumber
	  , dim_b2kdemogimportraw_associationid, dim_b2kdemogimportraw_corpid, dim_b2kdemogimportraw_agentinstitutionid
	  , dim_b2kdemogimportraw_billcode, dim_b2kdemogimportraw_binplan, dim_b2kdemogimportraw_productline, dim_b2kdemogimportraw_subproducttype
	  , dim_b2kdemogimportraw_scenrollmentdate, dim_b2kdemogimportraw_tipfirst, dim_b2kdemogimportraw_filedate
	)		
  ON (target.dim_b2kdemog_cardnumber = source.dim_b2kdemog_cardnumber)
  WHEN MATCHED THEN
  	UPDATE SET 
  		dim_b2kdemog_relationshipaccount = source.dim_b2kdemogimportraw_relationshipaccount
  		, dim_b2kdemog_cardtype = source.dim_b2kdemogimportraw_cardtype
  		, dim_b2kdemog_accounttype = source.dim_b2kdemogimportraw_accounttype
  		, dim_b2kdemog_primaryfirstname = source.dim_b2kdemogimportraw_primaryfirstname
  		, dim_b2kdemog_primarymiddleinitial = source.dim_b2kdemogimportraw_primarymiddleinitial
  		, dim_b2kdemog_primarylastname = source.dim_b2kdemogimportraw_primarylastname
  		, dim_b2kdemog_secondaryfirstname = source.dim_b2kdemogimportraw_secondaryfirstname
  		, dim_b2kdemog_secondarymiddleinitial = source.dim_b2kdemogimportraw_secondarymiddleinitial
  		, dim_b2kdemog_secondarylastname = source.dim_b2kdemogimportraw_secondarylastname
  		, dim_b2kdemog_addressline1 = source.dim_b2kdemogimportraw_addressline1
  		, dim_b2kdemog_addressline2 = source.dim_b2kdemogimportraw_addressline2
  		, dim_b2kdemog_addressline3 = source.dim_b2kdemogimportraw_addressline3
  		, dim_b2kdemog_addressline4 = source.dim_b2kdemogimportraw_addressline4
  		, dim_b2kdemog_city = source.dim_b2kdemogimportraw_city
  		, dim_b2kdemog_state = source.dim_b2kdemogimportraw_state
  		, dim_b2kdemog_country = source.dim_b2kdemogimportraw_country
  		, dim_b2kdemog_zipcode = source.dim_b2kdemogimportraw_zipcode
  		, dim_b2kdemog_homephonenumber = source.dim_b2kdemogimportraw_homephonenumber
  		, dim_b2kdemog_workphonenumber = source.dim_b2kdemogimportraw_workphonenumber
  		, dim_b2kdemog_emailaddress = source.dim_b2kdemogimportraw_emailaddress
  		, dim_b2kdemog_scprogramstatus = source.dim_b2kdemogimportraw_scprogramstatus
  		, dim_b2kdemog_scprogramstatuschangedate = source.dim_b2kdemogimportraw_scprogramstatuschangedate
  		, dim_b2kdemog_hostaccountstatus = source.dim_b2kdemogimportraw_hostaccountstatus
  		, dim_b2kdemog_hostacctstatusdate = source.dim_b2kdemogimportraw_hostacctstatusdate
  		, dim_b2kdemog_scparticipationflag = source.dim_b2kdemogimportraw_scparticipationflag
  		, dim_b2kdemog_primarysocialsecuritynumber = source.dim_b2kdemogimportraw_primarysocialsecuritynumber
  		, dim_b2kdemog_newcardnumber = CASE WHEN RewardsNow.dbo.ufn_CharIsNullOrEmpty(source.dim_b2kdemogimportraw_newcardnumber) = 0 THEN source.dim_b2kdemogimportraw_newcardnumber ELSE target.dim_b2kdemog_newcardnumber END
  		, dim_b2kdemog_associationid = source.dim_b2kdemogimportraw_associationid
  		, dim_b2kdemog_corpid = source.dim_b2kdemogimportraw_corpid
  		, dim_b2kdemog_agentinstitutionid = source.dim_b2kdemogimportraw_agentinstitutionid
  		, dim_b2kdemog_billcode = source.dim_b2kdemogimportraw_billcode
  		, dim_b2kdemog_productline = source.dim_b2kdemogimportraw_productline
  		, dim_b2kdemog_subproducttype = source.dim_b2kdemogimportraw_subproducttype
  		, dim_b2kdemog_binplan = source.dim_b2kdemogimportraw_binplan
  		, dim_b2kdemog_scenrollmentdate = source.dim_b2kdemogimportraw_scenrollmentdate
  		, dim_b2kdemog_bank = source.dim_b2kdemogimportraw_tipfirst
  		, dim_b2kdemog_tipfirst = source.dim_b2kdemogimportraw_tipfirst
  		, dim_b2kdemog_filedate = source.dim_b2kdemogimportraw_filedate;


/***************************************************************************

                   update oldcardnumber if necessary
 (for any record that might associate old and new card, not just the first)

***************************************************************************/

UPDATE dmg
SET dim_b2kdemog_oldcardnumber = dmgAll.dim_b2kdemogimportraw_cardnumber
FROM b2kDemog dmg
INNER JOIN vw_b2kDemogImport_AllRawRecords dmgAll
	ON dmg.dim_b2kdemog_cardnumber = dmgAll.dim_b2kdemogimportraw_newcardnumber
WHERE
	RewardsNow.dbo.ufn_CharIsNullOrEmpty(dmgAll.dim_b2kdemogimportraw_newcardnumber) = 0

/***************************************************************************

      update RNStatus Column based on mapping table as necessary

***************************************************************************/

UPDATE demog
	SET demog.dim_b2kdemog_rnstatus = map.dim_b2kimportstatusmapping_rnstatus
FROM b2kDemog demog
INNER JOIN b2kImportStatusMapping MAP
	ON map.dim_b2kimportstatusmapping_scparticipationflag = demog.dim_b2kdemog_scparticipationflag
	AND map.dim_b2kimportstatusmapping_scprogramstatus = demog.dim_b2kdemog_scprogramstatus
WHERE demog.dim_b2kdemog_rnstatus != map.dim_b2kimportstatusmapping_rnstatus


GO

/* CODE HOLDING... LANDING SPOT FOR PASTED CODE ...

UPDATE dmg
SET dim_b2kdemog_oldcardnumber = dmgAll.dim_b2kdemogimportraw_cardnumber
FROM b2kDemog dmg
INNER JOIN vw_b2kDemogImport_AllRawRecords dmgAll
	ON dmg.dim_b2kdemog_cardnumber = dmgAll.dim_b2kdemogimportraw_newcardnumber
WHERE
	RewardsNow.dbo.ufn_CharIsNullOrEmpty(dmgAll.dim_b2kdemogimportraw_newcardnumber) = 0
GO

*/


