USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Special_QTR_Expired_Report]    Script Date: 01/11/2010 17:14:19 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Special_QTR_Expired_Report_PointsExpired]') AND parent_object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Special_QTR_Expired_Report_PointsExpired]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Special_QTR_Expired_Report] DROP CONSTRAINT [DF_Special_QTR_Expired_Report_PointsExpired]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Special_QTR_Expired_Report_PointsOutstanding]') AND parent_object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Special_QTR_Expired_Report_PointsOutstanding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Special_QTR_Expired_Report] DROP CONSTRAINT [DF_Special_QTR_Expired_Report_PointsOutstanding]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Special_QTR_Expired_Report_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Special_QTR_Expired_Report_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Special_QTR_Expired_Report] DROP CONSTRAINT [DF_Special_QTR_Expired_Report_PointsToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]') AND type in (N'U'))
DROP TABLE [dbo].[Special_QTR_Expired_Report]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Special_QTR_Expired_Report](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [nchar](40) NULL,
	[PointsExpired] [int] NULL,
	[PointsOutstanding] [int] NULL,
	[PointsToExpire] [int] NULL,
 CONSTRAINT [PK_Special_QTR_Expired_Report] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Special_QTR_Expired_Report_PointsExpired]') AND parent_object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Special_QTR_Expired_Report_PointsExpired]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Special_QTR_Expired_Report] ADD  CONSTRAINT [DF_Special_QTR_Expired_Report_PointsExpired]  DEFAULT ('0') FOR [PointsExpired]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Special_QTR_Expired_Report_PointsOutstanding]') AND parent_object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Special_QTR_Expired_Report_PointsOutstanding]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Special_QTR_Expired_Report] ADD  CONSTRAINT [DF_Special_QTR_Expired_Report_PointsOutstanding]  DEFAULT ('0') FOR [PointsOutstanding]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Special_QTR_Expired_Report_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Special_QTR_Expired_Report]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Special_QTR_Expired_Report_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Special_QTR_Expired_Report] ADD  CONSTRAINT [DF_Special_QTR_Expired_Report_PointsToExpire]  DEFAULT ('0') FOR [PointsToExpire]
END


End
GO
