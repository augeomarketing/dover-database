USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransactionData_NEW]    Script Date: 02/18/2011 11:30:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionData_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionData_NEW]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransactionData_NEW]    Script Date: 02/18/2011 11:30:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

	/*									       */
	/* BY:  S.Blanchette  */
	/* DATE: 10/2007   */
	/* REVISION: 1 */
	/* SCAN: SEB001 */
	/* add code to deal with FBOP Heloc cards  */

	/*									       */
	/* BY:  S.Blanchette  */
	/* DATE: 06/2009   */
	/* REVISION: 2 */
	/* SCAN: SEB002 */
	/* add code to remove transaction records if tipnumber not in customer  */
	/*									       */
	/* BY:  S.Blanchette  */
	/* DATE: 06/2009   */
	/* REVISION: 3 */
	/* SCAN: SEB003 */
	/* add code to remove transaction records if customer not enrolled  */
	/*									       */
	/* BY:  S.Blanchette  */
	/* DATE: 04/2010   */
	/* REVISION: 4 */
	/* SCAN: SEB004 */
	/* add code to handle the MaxYTD earning  */

CREATE PROCEDURE [dbo].[spImportTransactionData_NEW] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3), 
	@StartDateParm char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	--declare @TipFirst char(3), @StartDateParm char(10)
	--set @TipFirst='590'
	--set @StartDateParm='04/01/2010'
	
	declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000), @SQLSet nvarchar(1000)
	declare @DBnumber char(3), @Revision smallint, @Begindate datetime, @Enddate datetime, @Startdate datetime, @SigFactor numeric(18,2), @PinFactor numeric(18,2), @WorkFactorSig numeric(18,2), @WorkFactorPin numeric(18,2)
	/* SCAN: SEB004 */ declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
	/* SCAN: SEB004 */ declare @tipnumber char(15), @histdate char(10), @acctno char(16), @TRANCODE char(2), @NUMPURCH nchar(6), @AMTPURCH numeric(9,0), @description varchar(50), @RATIO float, @NUMCR nchar(6), @AMTCR numeric(9,0) 

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	set @Startdate = convert(datetime, @Startdateparm + ' 00:00:00:001')  

	truncate table transum

	delete from cardsin where tipnumber is null

/******************************************/
/* BEGIN BLOCK SCAN SEB004                */
/******************************************/
	set @MaxPointsPerYear=(select MaxPointsPerYear from Rewardsnow.dbo.dbprocessinfo where dbnumber=@TipFirst)
	If @MaxPointsPerYear='0'
		begin
		set @MaxPointsPerYear='999999999'
		end
/******************************************/
/* END BLOCK SCAN SEB004                */
/******************************************/

	/*                                                                            */
	/*  Process to get the factor for debit cards TABLE                           */
	/*                                                                            */
	declare debit_crsr cursor
	for select DBNumber, Revision, Startdate, Enddate, Sigfactor, Pinfactor
	from DebitFactor
	where DBNumber=@TipFirst
	order by Revision desc

	/*                                                                            */
	open debit_crsr
	/*                                                                            */
	fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @SigFactor, @PinFactor
	/******************************************************************************/	
	/*                                                                            */
	/* MAIN PROCESSING  VERIFICATION                                              */
	/*                                                                            */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
		begin	
			set @Begindate = convert(datetime, @BeginDate + ' 00:00:00:001')    
			set @Enddate = convert(datetime, @Enddate + ' 23:59:59:990')    

			if @Revision='0'		
			Begin
				set @WorkFactorSig=@SigFactor
				set @WorkFactorPin=@PinFactor
				goto Fetch_Error
			End

			if @Startdate>@Enddate
			Begin
				set @WorkFactorSig=(select SigFactor from DebitFactor where DBNumber=@TipFirst and Revision='0')
				set @WorkFactorPin=(select PinFactor from DebitFactor where DBNumber=@TipFirst and Revision='0')
				goto Fetch_Error
			End

			if @Startdate>=@Begindate and @Startdate<=@Enddate
			Begin
				set @WorkFactorSig=@SigFactor
				set @WorkFactorPin=@PinFactor
				goto Fetch_Error
			End

			if @Startdate<@Begindate 
			Begin
				goto Next_Record
			End

	Next_Record:
			fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @SigFactor, @PinFactor
		end

	Fetch_Error:
	close  debit_crsr
	deallocate  debit_crsr


	/*********************************************************************/
	/*  Get transaction data into a standard format                      */
	/*********************************************************************/
	insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
	select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin	
	from cardsin
	group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin

	/*********************************************************************/
	/*  start SEB002 Remove records that do not have a customer record   */
	/*********************************************************************/
	set @SQLDelete='Delete from dbo.transum
					where tipnumber not in (select tipnumber from ' + QuoteName(@DBName) + N'.dbo.customer) '
	Exec sp_executesql @SQLDelete
	/*********************************************************************/
	/*  end SEB002 Remove records that do not have a customer record   */
	/*********************************************************************/


/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
	update transum
	set ratio='1', trancode='67', description='Debit Card Purchase SIG', amtpurch=round(((AMTPURCH/100)/@WorkfactorSig),0) 
	where amtpurch>0 and cardtype='D' and (SigvsPin is null or len(SigvsPin)='0' or SigvsPin='S')

	update transum
	set ratio='1', trancode='67', description='Debit Card Purchase PIN', amtpurch=round(((AMTPURCH/100)/@WorkfactorPin),0) 
	where amtpurch>0 and cardtype='D' and SigvsPin='P'
--2/18/2011 Add Credit to process	
	update transum
	set ratio='1', trancode='63', description='Credit Card Purchase', amtpurch=ROUND((AMTPURCH), 0)
	where amtpurch>0 and cardtype='C'

	/******************************************/
	/* BEGIN BLOCK SCAN SEB004                */
	/******************************************/
		declare Tran_crsr cursor
		for select tipnumber, histdate, acctno, TRANCODE, NUMPURCH, AMTPURCH, description, RATIO, overage
		from transum
		where left(trancode,1) = '6'
		order by tipnumber, acctno
	/*                                                                            */
		open Tran_crsr
		fetch Tran_crsr into @tipnumber, @histdate, @acctno, @TRANCODE, @NUMPURCH, @AMTPURCH, @description, @RATIO, @overage
		 
		/******************************************************************************/	
		/*                                                                            */
		/* MAIN PROCESSING                                                            */
		/*                                                                            */
		if @@FETCH_STATUS = 1
			goto Fetch_Error67
		/*                                                                            */
		while @@FETCH_STATUS = 0
		begin
			/*********************************************************************/
			/*  Debit Card Purchase transaction                                             */
			/*********************************************************************/
			if (left(@Trancode,1) = '6')
			Begin
				/*********************************************************************/
				/*  Check MAX YTD Earned                                             */
				/*********************************************************************/
				set @SQLSet=N'set @YTDEarned=(select YTDEarned from ' + QuoteName(@DBName) + N'.dbo.affiliat where acctid=@acctno) '
				Exec sp_executesql @SQLSet, N'@acctno char(16), @YTDEarned numeric(9) output', @acctno=@acctno, @YTDEarned = @YTDEarned output

				if (@YTDEarned) >= @MAXPOINTSPERYEAR
					Begin
	        			set @OVERAGE = @AMTPURCH
	        			set @AmtToPost='0'
					
						set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
        						values(@tipnumber, @acctno, @histdate, @TRANCODE, @NUMPURCH, @AmtToPost, @description, '' '', @RATIO, @OVERAGE) '			
						Exec sp_executesql @SQLInsert, N'@tipnumber char(15), @acctno char(16), @histdate char(10), @TRANCODE char(2), @NUMPURCH nchar(6), @AmtToPost numeric (9), @description varchar(50), @RATIO float, @Overage numeric(9)',  
															@tipnumber = @tipnumber,
															@acctno = @acctno,
															@histdate = @histdate,
															@TRANCODE = @TRANCODE,
															@NUMPURCH = @NUMPURCH,
															@AmtToPost = @AmtToPost,
															@description = @description,
															@RATIO = @RATIO,
															@OVERAGE = @OVERAGE
						Goto NextRecord67
					End
			
				if (@YTDEarned + @AMTPURCH)<= @MAXPOINTSPERYEAR
					Begin
	        			set @OVERAGE = '0'
	        			set @AmtToPost=@AMTPURCH
						set @YTDEarned= @YTDEarned + @AmttoPost
						
						set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
        						values(@tipnumber, @acctno, @histdate, @TRANCODE, @NUMPURCH, @AmtToPost, @description, '' '', @RATIO, @OVERAGE) '			
						Exec sp_executesql @SQLInsert, N'@tipnumber char(15), @acctno char(16), @histdate char(10), @TRANCODE char(2), @NUMPURCH nchar(6), @AmtToPost numeric (9), @description varchar(50), @RATIO float, @Overage numeric(9)',  
															@tipnumber = @tipnumber,
															@acctno = @acctno,
															@histdate = @histdate,
															@TRANCODE = @TRANCODE,
															@NUMPURCH = @NUMPURCH,
															@AmtToPost = @AmtToPost,
															@description = @description,
															@RATIO = @RATIO,
															@OVERAGE = @OVERAGE
															
						set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.customer
										set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost 
										where tipnumber=@tipnumber'			
						Exec sp_executesql @SQLUpdate, N'@AmtToPost numeric (9), @tipnumber char(15) ', @AmtToPost=@AmtToPost, @tipnumber=@tipnumber
										
						set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.affiliat
										set YTDEarned=@YTDEarned 
										where acctid=@acctno	'
						Exec sp_executesql @SQLUpdate, N'@acctno char(16), @YTDEarned numeric(9) ', @acctno=@acctno, @YTDEarned = @YTDEarned				
											
						Goto NextRecord67
					End
					
				if (@YTDEarned + @AMTPURCH)> @MAXPOINTSPERYEAR
					Begin
	        			set @OVERAGE = (@YTDEarned + @AMTPURCH)- @MAXPOINTSPERYEAR
	        			set @AmtToPost=@AMTPURCH - @OVERAGE
						set @YTDEarned= @YTDEarned + @AmttoPost
						
						set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
        						values(@tipnumber, @acctno, @histdate, @TRANCODE, @NUMPURCH, @AmtToPost, @description, '' '', @RATIO, @OVERAGE) '			
						Exec sp_executesql @SQLInsert, N'@tipnumber char(15), @acctno char(16), @histdate char(10), @TRANCODE char(2), @NUMPURCH nchar(6), @AmtToPost numeric (9), @description varchar(50), @RATIO float, @Overage numeric(9)',  
															@tipnumber = @tipnumber,
															@acctno = @acctno,
															@histdate = @histdate,
															@TRANCODE = @TRANCODE,
															@NUMPURCH = @NUMPURCH,
															@AmtToPost = @AmtToPost,
															@description = @description,
															@RATIO = @RATIO,
															@OVERAGE = @OVERAGE
															
						set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.customer
										set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost 
										where tipnumber=@tipnumber '			
						Exec sp_executesql @SQLUpdate, N'@AmtToPost numeric (9), @tipnumber char(15) ', @AmtToPost=@AmtToPost, @tipnumber=@tipnumber
										
						set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.affiliat
										set YTDEarned=@YTDEarned 
										where acctid=@acctno	'
						Exec sp_executesql @SQLUpdate, N'@acctno char(16), @YTDEarned numeric(9) ', @acctno=@acctno, @YTDEarned = @YTDEarned				
					
						Goto NextRecord67
					End
			End
		
		NextRecord67:
			fetch Tran_crsr into @tipnumber, @histdate, @acctno, @TRANCODE, @NUMPURCH, @AMTPURCH, @description, @RATIO, @overage
		end
		
		Fetch_Error67:
		close  Tran_crsr
		deallocate  Tran_crsr

		--set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
		-- select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
		-- from transum
		-- where trancode=''67'' '
		--Exec sp_executesql @SQLInsert

	
	/******************************************/
	/* END BLOCK SCAN SEB004                  */
	/******************************************/


/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
	update transum
	set ratio='-1', trancode='37', description='Debit Card Returns SIG', amtcr=round(((AMTCR/100)/@WorkfactorSig),0)
	where amtcr>0 and cardtype='D' and (SigvsPin is null or len(SigvsPin)='0' or SigvsPin='S')

	update transum
	set ratio='-1', trancode='37', description='Debit Card Returns PIN', amtcr=round(((AMTCR/100)/@WorkfactorPin),0)
	where amtcr>0 and cardtype='D' and SigvsPin='P'
	
	update transum
	set ratio='-1', trancode='33', description='Credit Card Returns', amtcr=ROUND((AMTcr), 0)
	where amtcr>0 and cardtype='C'

	/******************************************/
	/* BEGIN BLOCK SCAN SEB004                */
	/******************************************/
		declare Tran_crsr cursor
		for select tipnumber, histdate, acctno, TRANCODE, NUMCR, AMTCR, description, RATIO, overage
		from transum
		where left(trancode,1) = '3'
		order by tipnumber, acctno
	/*                                                                            */
		open Tran_crsr
		fetch Tran_crsr into @tipnumber, @histdate, @acctno, @TRANCODE, @NUMCR, @AMTCR, @description, @RATIO, @overage
		 
		/******************************************************************************/	
		/*                                                                            */
		/* MAIN PROCESSING                                                            */
		/*                                                                            */
		if @@FETCH_STATUS = 1
			goto Fetch_Error37
		/*                                                                            */
		while @@FETCH_STATUS = 0
		begin
			/*********************************************************************/
			/*  Debit Card Purchase transaction                                             */
			/*********************************************************************/
			if (left(@Trancode,1) = '3')
			Begin
				/*********************************************************************/
				/*  Check MAX YTD Earned                                             */
				/*********************************************************************/
			   			set @OVERAGE = '0'
	        			set @AmtToPost=@AMTCR
					
						set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
        						values(@tipnumber, @acctno, @histdate, @TRANCODE, @NUMCR, @AmtToPost, @description, '' '', @RATIO, @OVERAGE) '			
						Exec sp_executesql @SQLInsert, N'@tipnumber char(15), @acctno char(16), @histdate char(10), @TRANCODE char(2), @NUMCR nchar(6), @AmtToPost numeric (9), @description varchar(50), @RATIO float, @Overage numeric(9)',  
															@tipnumber = @tipnumber,
															@acctno = @acctno,
															@histdate = @histdate,
															@TRANCODE = @TRANCODE,
															@NUMCR = @NUMCR,
															@AmtToPost = @AmtToPost,
															@description = @description,
															@RATIO = @RATIO,
															@OVERAGE = @OVERAGE


						set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.customer
										set runavailable=runavailable - @AmtToPost, runbalance=runbalance - @AmtToPost 
										where tipnumber=@tipnumber'			
						Exec sp_executesql @SQLUpdate, N'@AmtToPost numeric (9), @tipnumber char(15) ', @AmtToPost=@AmtToPost, @tipnumber=@tipnumber
										
						set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.affiliat
										set YTDEarned=YTDEarned - @AmtToPost
										where acctid=@acctno	'
						Exec sp_executesql @SQLUpdate, N'@acctno char(16), @AmtToPost numeric (9) ', @acctno=@acctno, @AmtToPost = @AmtToPost				
											
						Goto NextRecord37
			End
		
		NextRecord37:
			fetch Tran_crsr into @tipnumber, @histdate, @acctno, @TRANCODE, @NUMCR, @AMTCR, @description, @RATIO, @overage
		end
		
		Fetch_Error37:
		close  Tran_crsr
		deallocate  Tran_crsr

		--set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
		 --       		select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
			--		from transum
			--		where trancode=''37'' '
			--Exec sp_executesql @SQLInsert

		
	/******************************************/
	/* END BLOCK SCAN SEB004                  */
	/******************************************/
	delete from Transum
	where TRANCODE is null
	
--Process any Bonus Point awards (RBM 02/18/2011)
exec dbo.[usp_bonusprocessing] @tipfirst, @DBName
	

	/*********************************************************************/
	/*  Update Customer                                                  */
	/*********************************************************************/


	/******************************************/
	/* BEGIN BLOCK SCAN SEB004                */
	/******************************************/

		--set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.customer 
		--		set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber), 
		--			runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber) 
		--		where exists(select * from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber) '
		--Exec sp_executesql @SQLUpdate
		--END
		
	/******************************************/
	/* END BLOCK SCAN SEB004                  */
	/******************************************/
END



GO


