USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerCreateCouponDetail]    Script Date: 09/20/2011 08:12:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCreateCouponDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerCreateCouponDetail]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spKroegerCreateCouponDetail]    Script Date: 09/20/2011 08:12:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		S Blanchette
-- Create date: 12/09
-- Description:	Create coupon request file
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerCreateCouponDetail]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 		
	Truncate Table dbo.Kroeger_Coupon_Request_Detail
	 
	insert into dbo.Kroeger_Coupon_Request_Detail (RecordType, LoyaltyNumber, CardHolderName, PointsRedeemed, TransID)
	select 'D', b.misc2, b.ACCTNAME1, (points), transid
	from OnlineHistoryWork.dbo.OnlHistory a, [52JKroegerPersonalFinance].dbo.customer b
	where left(a.tipnumber, 3) = '52J' and a.KroegerCouponFlag is null and a.trancode = 'RK' and a.tipnumber = b.tipnumber
	
	insert into dbo.Kroeger_Coupon_Request_Detail (RecordType, LoyaltyNumber, CardHolderName, PointsRedeemed, TransID)
	select 'D', b.misc2, b.ACCTNAME1, (points), transid
	from OnlineHistoryWork.dbo.OnlHistory a, [52U].dbo.customer b
	where left(a.tipnumber, 3) = '52U' and a.KroegerCouponFlag is null and a.trancode = 'RK' and a.tipnumber = b.tipnumber

	update OnlineHistoryWork.dbo.OnlHistory
	set KroegerCouponFlag = getdate()
	where left(tipnumber, 3) in ('52J','52U') and KroegerCouponFlag is null and trancode = 'RK'
	
	
END

GO


