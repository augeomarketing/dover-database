USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_Kroger_Demographic_Set_TipFirst]    Script Date: 07/18/2016 07:51:58 ******/
DROP PROCEDURE [dbo].[usp_Kroger_Demographic_Set_TipFirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Kroger_Demographic_Set_TipFirst] 
		@Test			char(1)

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @TipKroger char(3)
	declare @TipRalph char(3)
	declare @TipFredMeyer char(3)

	--Controls tip first based on flag passed in
	if @Test = 'T'
		Begin
			update Rewardsnow.dbo.RNIRawImport
			set sid_dbprocessinfo_dbnumber = 
				case 
				when dim_rnirawimport_field01 like 'Kroger%' then '5KJ'
				when dim_rnirawimport_field01 like 'Ralph%' then '5KR'
				when dim_rnirawimport_field01 like 'Fred Meyer%' then '5KW'
				else '5KK'
				end
			where sid_dbprocessinfo_dbnumber='5KK'
			and sid_rniimportfiletype_id=1
		End
		Else
			Begin
			update Rewardsnow.dbo.RNIRawImport
			set sid_dbprocessinfo_dbnumber = 
				case 
				when dim_rnirawimport_field01 like 'Kroger%' then '52J'
				when dim_rnirawimport_field01 like 'Ralph%' then '52R'
				when dim_rnirawimport_field01 like 'Fred Meyer%' then '52W'
				else '5KK'
				end
			where sid_dbprocessinfo_dbnumber='5KK'
			and sid_rniimportfiletype_id=1
		End
END
GO
