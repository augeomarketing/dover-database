USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKrogerBringInDailySource]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKrogerBringInDailySource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKrogerBringInDailySource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKrogerBringInDailySource]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spKrogerBringInDailySource] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Truncate table [MetavanteWork].[dbo].[Kroger_Daily_Batch_Header]
	Truncate table [MetavanteWork].[dbo].[Kroger_Daily_Batch_Trailer]
	Truncate table [MetavanteWork].[dbo].[Kroger_Daily_Card_Common_Detail]
	Truncate table [MetavanteWork].[dbo].[Kroger_Daily_Card_Address_Detail]
	Truncate table [MetavanteWork].[dbo].[Kroger_Daily_Card_Contact_Detail]
	Truncate table [MetavanteWork].[dbo].[Kroger_Daily_Card_Transaction_Detail]
	
	
    -- Insert statements for procedure here
	INSERT INTO [MetavanteWork].[dbo].[Kroger_Daily_Batch_Header]
           ([RecordID]
           ,[RecordSequence]
           ,[SegmentID]
           ,[SegmentVersion]
           ,[BatchID]
           ,[ClientID]
           ,[ClientName]
           ,[ProcessorCode]
           ,[ProcessorName]
           ,[BatchDate]
           ,[Filler])
	 select 
		substring(col001,1,2), 
		substring(col001, 3, 10),
		substring(col001, 13, 12),
		substring(col001, 25, 2),
		substring(col001, 27, 20),
		substring(col001, 47, 5),
		substring(col001, 52, 35),
		substring(col001, 87, 3),
		substring(col001, 90, 20),
		substring(col001, 110, 8),
		substring(col001, 118, 383)
	from dbo.Kroger_Daily_Source
	where substring(col001, 13, 12) = ''BATCHHEADER ''
	
	INSERT INTO [MetavanteWork].[dbo].[Kroger_Daily_Batch_Trailer]
           ([RecordID]
           ,[RecordSequence]
           ,[SegmentID]
           ,[SegmentVersion]
           ,[BatchID]
           ,[ClientID]
           ,[ClientName]
           ,[BatchRecordCount]
           ,[ProcessorCode]
           ,[ProcessorName]
           ,[BatchDebitTransactionCount]
           ,[BatchDebitTransactionAmount]
           ,[BatchCreditTransactionCount]
           ,[BatchCreditTransactionAmount]
           ,[Filler])
	select
		substring(col001,1,2),
		substring(col001,3,10),
		substring(col001,13,12),
		substring(col001,25,2),
		substring(col001,27,20),
		substring(col001,47,5),
		substring(col001,52,35), 
		substring(col001,87,18), 
		substring(col001,105,3),
		substring(col001,108,20),
		substring(col001,128,18),
		substring(col001,146,18), 
		substring(col001,164,18),
		substring(col001,182,18),
		substring(col001,200,301)
	from dbo.Kroger_Daily_Source
	where substring(col001, 13, 12) = ''BATCHTRAILER'' 
	
	INSERT INTO [MetavanteWork].[dbo].[Kroger_Daily_Card_Common_Detail]
           ([RecordID]
           ,[RecordSequence]
           ,[SegmentID]
           ,[SegmentVersion]
           ,[CardNumber]
           ,[LoyaltyNumber]
           ,[ReferenceNumber]
           ,[PrimaryDDA]
           ,[PrimarySavings]
           ,[BIN]
           ,[CardType]
           ,[CardSubType]
           ,[CardStatus]
           ,[CardStatusReason]
           ,[CardExpirationDate]
           ,[OriginalIssueDate]
           ,[LastReissueDate]
           ,[DateLastUsed]
           ,[DateFirstUsed]
           ,[ReplacedCard]
           ,[ReplacementCard]
           ,[RewardPlan]
           ,[RewardID]
           ,[LoyaltyNumberII]
           ,[RewardsEnrollmentDate]
           ,[CardClosedDate]
           ,[CardClosedTime]
           ,[CardAssociationID]
           ,[Filler])
     Select
           substring(col001,1,2),
           substring(col001,3,10),
           substring(col001,13,12),
           substring(col001,25,2),
           substring(col001,27,19),
           substring(col001,46,21),
           substring(col001,67,19),
           substring(col001,86,11),
           substring(col001,97,11),
           substring(col001,108,11),
           substring(col001,119,2),
           substring(col001,121,2),
           substring(col001,123,1),
           substring(col001,124,1),
           substring(col001,125,8),
           substring(col001,133,8),
           substring(col001,141,8),
           substring(col001,149,8),
           substring(col001,157,8),
           substring(col001,165,19),
           substring(col001,184,19),
           substring(col001,203,3),
           substring(col001,206,9),
           substring(col001,215,24),
           substring(col001,239,8),
           substring(col001,247,8),
           substring(col001,255,4),
           substring(col001,259,6),
           substring(col001,265,236) 
	from dbo.Kroger_Daily_Source
	where substring(col001, 13, 12) = ''CRDCOMMON01 ''
	
	INSERT INTO [MetavanteWork].[dbo].[Kroger_Daily_Card_Address_Detail]
           ([RecordID]
           ,[RecordSequence]
           ,[SegmentID]
           ,[SegmentVersion]
           ,[CardNumber]
           ,[LoyaltyNumber]
           ,[ReferenceNumber]
           ,[PrimaryDDA]
           ,[PrimarySavings]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[City]
           ,[State]
           ,[Zip]
           ,[CountryCode]
           ,[CountryName]
           ,[Filler])
    select
           substring(col001,1,2),
           substring(col001,3, 10),
           substring(col001,13, 12),
           substring(col001,25,2),
           substring(col001,27,19),
           substring(col001,46,21),
           substring(col001,67,19),
           substring(col001,86, 11),
           substring(col001,97,11),
           substring(col001,108,40),
           substring(col001,148,40),
           substring(col001,188,40),
           substring(col001,228,20),
           substring(col001,248,2),
           substring(col001,250,9),
           substring(col001,259,3),
           substring(col001,262,40),
           substring(col001,302,199)
    from dbo.Kroger_Daily_Source
	where substring(col001, 13, 12) = ''CRDADDRESS01''       

	INSERT INTO [MetavanteWork].[dbo].[Kroger_Daily_Card_Contact_Detail]
           ([RecordID]
           ,[RecordSequence]
           ,[SegmentID]
           ,[SegmentVersion]
           ,[CardNumber]
           ,[LoyaltyNumber]
           ,[ReferenceNumber]
           ,[PrimaryDDA]
           ,[PrimarySavings]
           ,[PrimaryCardholderFirstName]
           ,[PrimaryCardholderLastName]
           ,[SecondaryCardholderName]
           ,[HomePhoneNumber]
           ,[BusinessPhoneNumber]
           ,[PrimarySSN]
           ,[PrimaryDOB]
           ,[PrimaryEmail]
           ,[AuthorizedRedeemer1]
           ,[AuthorizedRedeemer2]
           ,[PrimaryCardName]
           ,[Filler])
     select
           substring(col001,1,2),
           substring(col001,3,10),
           substring(col001,13,12),
           substring(col001,25,2),
           substring(col001,27,19),
           substring(col001,46,21),
           substring(col001,67,19),
           substring(col001,86,11),
           substring(col001,97,11),
           substring(col001,108,10),
           substring(col001,118,15),
           substring(col001,133,26),
           substring(col001,159,10),
           substring(col001,169,10),
           substring(col001,179,9),
           substring(col001,188,8),
           substring(col001,196,40),
           substring(col001,236,25),
           substring(col001,261,25),
           substring(col001,286,26),
           substring(col001,312,189)
	from dbo.Kroger_Daily_Source
	where substring(col001, 13, 12) = ''CRDCONTACT01''
	
	INSERT INTO [MetavanteWork].[dbo].[Kroger_Daily_Card_Transaction_Detail]
           ([RecordID]
           ,[RecordSequence]
           ,[SegmentID]
           ,[SegmentVersion]
           ,[CardNumber]
           ,[LoyaltyNumber]
           ,[ReferenceNumber]
           ,[PrimaryDDA]
           ,[PrimarySavings]
           ,[TransactionCode]
           ,[DebitCreditIndicator]
           ,[Pinned/NonPinnedIndicator]
           ,[TransactionAmount]
           ,[CashBackAmount]
           ,[CurrencyCode]
           ,[TerminalReference]
           ,[AuthorizationCode]
           ,[TransactionDate]
           ,[TransactionTime]
           ,[PostingDate]
           ,[TransactionType]
           ,[Filler])
     select
           substring(col001,1,2),
           substring(col001,3,10),
           substring(col001,13,12),
           substring(col001,25,2),
           substring(col001,27,19),
           substring(col001,46,21),
           substring(col001,67,19),
           substring(col001,86,11),
           substring(col001,97,11),
           substring(col001,108,4),
           substring(col001,112,1),
           substring(col001,113,1),
           substring(col001,114,9),
           substring(col001,123,9),
           substring(col001,132,3),
           substring(col001,135,12),
           substring(col001,147,8),
           substring(col001,155,8),
           substring(col001,163,6),
           substring(col001,169,8),
           substring(col001,177,5),
           substring(col001,182,319)
     from dbo.Kroger_Daily_Source
	 where substring(col001, 13, 12) = ''TRANCOMMON01''
	 
	 update [MetavanteWork].[dbo].[Kroger_Daily_Card_Transaction_Detail]
	 set [TransactionAmount] = [TransactionAmount]/100
	 
	
	

END
' 
END
GO
