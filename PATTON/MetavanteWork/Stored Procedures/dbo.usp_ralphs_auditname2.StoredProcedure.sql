USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_auditname2]    Script Date: 07/29/2011 14:15:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ralphs_auditname2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ralphs_auditname2]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_ralphs_auditname2]    Script Date: 07/29/2011 14:15:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_ralphs_auditname2]

AS

Declare @date datetime = (Select MAX(CAST(processdate as datetime)) from [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData_Central where TipFirst = '52R')
Declare @datejul varchar(3)=	RIGHT('000' + CAST(DATEPART(dy, @date) AS varchar(3)),3)


Select '\\Patton\Ops\5xx\input\Kroeger_Bonus\RewardsNOWCustom.RAL_J' + @datejul




GO


