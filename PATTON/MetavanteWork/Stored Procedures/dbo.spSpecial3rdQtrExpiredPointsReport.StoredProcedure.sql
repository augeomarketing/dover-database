USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSpecial3rdQtrExpiredPointsReport]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSpecial3rdQtrExpiredPointsReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSpecial3rdQtrExpiredPointsReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSpecial3rdQtrExpiredPointsReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2009
-- Description:	Special Expired points report
-- =============================================
CREATE PROCEDURE [dbo].[spSpecial3rdQtrExpiredPointsReport] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3),
	@MonthEndingDate char(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Truncate table Special_QTR_Expired_Report

	declare @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
	declare @DBName varchar(100), @NumRecs numeric, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)
	
	set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	set @SQLInsert = ''Insert into Special_QTR_Expired_Report ( Tipnumber, Acctname1, PointsOutstanding)
						select Tipnumber, Acctname1, RunAvailable
						from '' + QuoteName(@DBName) + N''.dbo.Customer 
						order by tipnumber asc''
	Exec sp_executesql @SQLInsert

	set @SQLUpdate = ''Update Special_QTR_Expired_Report
						set PointsExpired = (select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where trancode = ''''XP'''' and tipnumber = Special_QTR_Expired_Report.tipnumber)
						where exists (select * from '' + QuoteName(@DBName) + N''.dbo.history where trancode = ''''XP'''' and tipnumber = Special_QTR_Expired_Report.tipnumber) ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate = ''Update Special_QTR_Expired_Report
						set PointsExpired = PointsExpired - (select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where trancode = ''''XF'''' and tipnumber = Special_QTR_Expired_Report.tipnumber)
						where exists (select * from '' + QuoteName(@DBName) + N''.dbo.history where trancode = ''''XF'''' and tipnumber = Special_QTR_Expired_Report.tipnumber) ''
	Exec sp_executesql @SQLUpdate


	/* This is to get Points To Expire      */
	exec spSpecial_Qtr_Report_ExpirePoints_Extract @TipFirst, @MonthEndingDate
	
END
' 
END
GO
