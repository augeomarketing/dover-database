USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteSetAccountNumbers]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteSetAccountNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteSetAccountNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteSetAccountNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spMetavanteSetAccountNumbers]
as

update custin
set oldccpre=left(Acctnum,6)

update custin
set oldccpre=''        ''
where oldccpost = ''0000000000''

update custin
set oldccpost=''          ''
where oldccpost = ''0000000000''

update custin
set oldcc= rtrim(oldccpre) + rtrim(oldccpost)

update custin
set citystate=rtrim(city)+ '' '' +rtrim(state)' 
END
GO
