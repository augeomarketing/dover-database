USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kRepairNullMappings]    Script Date: 01/17/2011 12:12:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kRepairNullMappings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kRepairNullMappings]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kRepairNullMappings]    Script Date: 01/17/2011 12:12:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_b2kRepairNullMappings]
AS

SET NOCOUNT ON

BEGIN

	DECLARE @repairs INT

	UPDATE b2kDemog
	SET dim_b2kdemog_productline = draw.dim_b2kdemogimportraw_productline
		, dim_b2kdemog_subproducttype = draw.dim_b2kdemogimportraw_subproducttype
	FROM b2kDemog dmg
	INNER JOIN
	(
	select dim_b2kdemogimportraw_cardnumber, dim_b2kdemogimportraw_binplan, dim_b2kdemogimportraw_productline, dim_b2kdemogimportraw_subproducttype
	from b2kDemogImportRaw
		
	union all
	select dim_b2kdemogimportraw_cardnumber, dim_b2kdemogimportraw_binplan, dim_b2kdemogimportraw_productline, dim_b2kdemogimportraw_subproducttype
	from b2kDemogImportRawHistory
	) draw
	on dmg.dim_b2kdemog_cardnumber = draw.dim_b2kdemogimportraw_cardnumber

	update b2kDemog
	set dim_b2kdemog_bank = tf.Bank
		, dim_b2kdemog_agent = tf.Agent
	from b2kDemog dmg
	inner join vw_CRTipFirstTable_TipFirstByProdLineSubType tf
	on
		left(dmg.dim_b2kdemog_binplan, 6) = tf.Bin
		and dim_b2kdemog_productline = tf.ProductLine
		and dim_b2kdemog_subproducttype = tf.SubProductType
	where 
		dmg.dim_b2kdemog_bank is null
		or dmg.dim_b2kdemog_agent is null
		
	SET @repairs = @@ROWCOUNT

	PRINT 'Repaired ' + CAST(@repairs AS varchar) + ' mappings.'
END


GO

