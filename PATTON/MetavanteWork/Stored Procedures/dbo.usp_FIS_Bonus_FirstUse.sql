USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_FirstUse]    Script Date: 01/25/2012 10:24:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_Bonus_FirstUse]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_Bonus_FirstUse]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_FirstUse]    Script Date: 01/25/2012 10:24:30 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_FIS_Bonus_FirstUse] 
	@EndDate varchar(10), @tipfirst varchar(3)

AS 


/****************************************************************************/
/*   Procedure to generate the First Use bonus based on first use           */
/****************************************************************************/

declare @SQL nvarchar(max), 
		@BonusPoints numeric(9)

SET @BonusPoints = (SELECT FirstUseBonus FROM DBProcessInfo WHERE DBNumber = @TipFirst)

IF	@BonusPoints > 0 
	BEGIN
		TRUNCATE TABLE	wrkFirst

		SET	@SQL = N'
			INSERT INTO wrkFirst
						(tipnumber)
			SELECT		tipnumber
			FROM		[<<DBNAME>>].dbo.history
			WHERE		tipnumber not in (SELECT tipnumber FROM [<<DBNAME>>].dbo.onetimebonuses WHERE trancode in (''NW'',''BF'') and dateawarded is not null)
				and		trancode in (''63'',''67'')
		'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		EXEC	sp_executesql @SQL

		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.history
						(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
			SELECT		TIPnumber, ''<<ENDDATE>>'', ''BF'', ''1'', <<BONUSPTS>>, ''1'', ''Bonus First Time Debit_Credit Card Use'', ''0''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber in (SELECT tipnumber FROM wrkFirst)
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)
		EXEC	sp_executesql @SQL  

		SET @sql = N'
			UPDATE	[<<DBNAME>>].dbo.Customer 
			SET		RunAvailable = RunAvailable + ''<<BONUSPTS>>'', 
					RunBalance = RunBalance + ''<<BONUSPTS>>''  
			WHERE	tipnumber in (SELECT tipnumber FROM wrkFirst)
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)							
		EXEC	sp_executesql @SQL

		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.OneTimeBonuses
						(TipNumber, TranCode, DateAwarded)
			SELECT		TIPnumber, ''BF'', ''<<ENDDATE>>''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber in (SELECT tipnumber FROM wrkFirst)
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)
		EXEC	sp_executesql @SQL		
	END
GO


