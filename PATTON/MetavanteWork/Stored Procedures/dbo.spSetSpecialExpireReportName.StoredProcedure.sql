USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSetSpecialExpireReportName]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetSpecialExpireReportName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetSpecialExpireReportName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetSpecialExpireReportName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2009
-- Description:	Create name for Special Expire Point Report
-- =============================================
CREATE PROCEDURE [dbo].[spSetSpecialExpireReportName]
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output, 
	@ConnectionString nchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @Filename char(50), @currentdate nchar(10), @ExpireYear char(4)

	set @ExpireYear = year(getdate())
	set @currentdate = ''12_31_'' + @ExpireYear

	/*****************/
	
	set @filename=@TipPrefix + ''_'' + @currentdate + ''_SpecialExpire.xls''
		 
	set @newname=''O:\5xx\Output\SpecialReports\SpecialExpire.bat '' + @filename
	set @ConnectionString=''O:\5xx\Output\SpecialReports\'' + @filename
   
END
' 
END
GO
