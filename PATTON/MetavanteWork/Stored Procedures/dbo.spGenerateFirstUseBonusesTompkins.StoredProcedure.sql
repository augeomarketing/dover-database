USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateFirstUseBonusesTompkins]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateFirstUseBonusesTompkins]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateFirstUseBonusesTompkins]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateFirstUseBonusesTompkins]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGenerateFirstUseBonusesTompkins] @startDate varchar(10), @EndDate varchar(10), @tipfirst char(3)
AS 

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the First Use bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000), @SQLIF nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1)

set @Trandate=@EndDate

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  FirstUseBonus from DBProcessInfo
				where DBNumber=@TipFirst)

if @BonusPoints>0 
Begin
	
/*                                                                            */
/* Setup work file for processing                                                */
drop table wrkFirst

set @sqlinsert=N''select tipnumber into wrkFirst from '' + QuoteName(@DBName) + N'' .dbo.customer
			where dateadded=''''2008-04-30 00:00:00.000'''' and 
				tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N'' .dbo.onetimebonuses 
							where trancode=''''NW'''' and dateawarded is not null) ''
exec sp_executesql @SQLinsert

set @sqldelete=N''delete from wrkFirst
			where tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N'' .dbo.history 
							where trancode in (''''63'''', ''''67'''') ) ''
exec sp_executesql @sqldelete

/*                                                                            */
declare Tip_crsr cursor 
for select tipnumber
from wrkFirst 

open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag=''N''		
		set @sqlif=N''if not exists(select * from '' + QuoteName(@DBName) + N'' .dbo.OneTimeBonuses where tipnumber=@tipnumber and trancode = ''''NW'''') Begin set @ProcessFlag=''''Y'''' End ''  
		exec sp_executesql @SQLIf, N''@Tipnumber nchar(15), @ProcessFlag char(1) output'', @Tipnumber= @Tipnumber, @ProcessFlag=@ProcessFlag output


		if @ProcessFlag=''Y''
		Begin
 
		set @sqlupdate=N''Update '' + QuoteName(@DBName) + N'' .dbo.Customer set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
		where tipnumber = @Tipnumber''
		exec sp_executesql @SQLUpdate, N''@Tipnumber nchar(15), @BonusPoints numeric(9)'', @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints

		set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        	Values(@Tipnumber, @Trandate, ''''NW'''', ''''1'''', @BonusPoints, ''''1'''', ''''First Use Bonus'''', ''''0'''')''
		Exec sp_executesql @SQLInsert, N''@Trandate nchar(10), @Tipnumber nchar(15), @BonusPoints numeric(9)'', @Trandate= @Trandate, @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints
 
		set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses(TipNumber,TranCode, DateAwarded)
        	Values(@Tipnumber, ''''NW'''', @Trandate)''
		Exec sp_executesql @SQLInsert, N''@Trandate nchar(10), @Tipnumber nchar(15)'', @Trandate= @Trandate, @Tipnumber= @Tipnumber

		End

		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

End' 
END
GO
