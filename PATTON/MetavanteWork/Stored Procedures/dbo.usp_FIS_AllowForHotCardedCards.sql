USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_AllowForHotCardedCards]    Script Date: 12/30/2011 14:15:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_AllowForHotCardedCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_AllowForHotCardedCards]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_AllowForHotCardedCards]    Script Date: 12/30/2011 14:15:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_FIS_AllowForHotCardedCards] 
		@Tipfirst	varchar(3) 
AS 

DECLARE	@SQLUpdate	nvarchar(max)

SET	@SQLUpdate =	'INSERT INTO [<<DBNAME>>].dbo.affiliat 
										(ACCTID, TIPNUMBER, AcctType, DATEADDED, SecID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					 SELECT	 DISTINCT	 oldcc,  tipnumber, cardtype, period,    ssn,   ''A'',      '''',         lastname, ''0'',     ddanum
					 FROM	[MetavanteWork].dbo.cardsin
					 WHERE	Cardtype = ''D'' 
						and status = ''C'' 
						and tipnumber is not null 
						and left(tipnumber,1) <> '' '' 
						and oldcc is not null and left(oldcc,1) <> '' ''
						and oldcc not in (select acctid from [<<DBNAME>>].dbo.affiliat) '
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC sp_executesql @SQLUpdate


SET	@SQLUpdate =	'UPDATE [<<DBNAME>>].dbo.affiliat
					 SET	AcctType=''Credit''
					 WHERE	AcctType=''C'' '
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC sp_executesql @SQLUpdate

SET @SQLUpdate =	'UPDATE [<<DBNAME>>].dbo.affiliat
					 SET	AcctType=''Debit''
					 WHERE	AcctType=''D'' '
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC sp_executesql @SQLUpdate

SET	@SQLUpdate =	'UPDATE [<<DBNAME>>].dbo.affiliat
					 SET	AcctTypeDesc = (SELECT	Accttypedesc 
											FROM [<<DBNAME>>].dbo.accttype
        									WHERE	accttype=[<<DBNAME>>].dbo.affiliat.accttype) 
					where len(AcctTypeDesc) = ''0'' '
SET	@SQLUpdate =	REPLACE(@SQLUpdate, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
Exec sp_executesql @SQLUpdate


GO


