USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDA]    Script Date: 05/08/2012 10:14:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersConsumerDDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDA]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDA]    Script Date: 05/08/2012 10:14:02 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDA] @tipfirst char(3)
AS 

EXEC [dbo].[usp_FIS_TIPassignment] @tipfirst

GO