USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCardsinWithDebitCardsKroger]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCardsinWithDebitCardsKroger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCardsinWithDebitCardsKroger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCardsinWithDebitCardsKroger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCardsinWithDebitCardsKroger] 
	-- Add the parameters for the stored procedure here
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
--inserts records from debitcardin2 into Cardsin
insert into Cardsin(tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, Cardtype, SigvsPin)
select tipfirst, tipmid, '' '', [Social Security Number], [DDA Acct Number], [Primary Acct Number], [Cross reference card numer], [Full Name], [Secondary Name], '' '', '' '', '' '', [Rewards Member ID], [Address Line 1], [Address Line 2], '' '', [City / State], [Zip Code], Status, [Home Phone], [Business Phone], numpurch, purch, numret, amtret, '' '', ''D'', [Signature or PIN] from debitcardinKroger

END
' 
END
GO
