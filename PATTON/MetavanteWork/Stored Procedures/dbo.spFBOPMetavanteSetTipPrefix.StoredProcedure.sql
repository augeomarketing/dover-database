USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPMetavanteSetTipPrefix]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPMetavanteSetTipPrefix]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPMetavanteSetTipPrefix]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPMetavanteSetTipPrefix]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPMetavanteSetTipPrefix]
as

update FBOPcreditcardin
set tipfirst=(select tipfirst from CRTipFirst_Table where Bank=FBOPcreditcardin.bank and agent=FBOPcreditcardin.agent)

update FBOPcreditcardin
set tipmid=''0000000''

delete from FBOPcreditcardin
where tipfirst is null

Update FBOPcreditcardin
set joint=''J''
where NA2 is not null and len(rtrim(na2))<>''0''

update FBOPcreditcardin
set joint=''S''
where NA2 is null or len(rtrim(na2))=''0''' 
END
GO
