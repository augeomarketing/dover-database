USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spRevisedSetStatementFileName]    Script Date: 09/30/2010 11:28:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedSetStatementFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRevisedSetStatementFileName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedSetStatementFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 9/2010
-- Description:	Set up Statement file name
-- =============================================
CREATE PROCEDURE [dbo].[spRevisedSetStatementFileName] 
	@TipPrefix char (3), 
	@date datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
/*****************/
declare @DBName varchar(100), @NumRecs numeric, @SQLSelect nvarchar(1000)
set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

set @SQLSelect=''set @NumRecs=(select count(*) from dbo.Revised_Quarterly_Statement) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 

--set @filename=''S'' + @TipPrefix + ''_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''

Select ''\\Patton\Ops\5xx\Output\Statements\S''
+ @TipPrefix
+ ''_''
+ Right(''0''+cast(month(@date)as varchar(2)),2)
+ Right(''0''+cast(year(@date)as varchar(4)),2)
+ ''-'' 
+ rtrim(cast(@NumRecs as char(10)))
+ ''.xls''

END
' 
END
GO
