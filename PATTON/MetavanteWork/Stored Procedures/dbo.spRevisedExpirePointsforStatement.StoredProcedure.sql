USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spRevisedExpirePointsforStatement]    Script Date: 03/21/2014 11:16:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedExpirePointsforStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRevisedExpirePointsforStatement]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spRevisedExpirePointsforStatement]    Script Date: 03/21/2014 11:16:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spRevisedExpirePointsforStatement]	@Tipfirst varchar(3)

AS

BEGIN
	SET NOCOUNT ON;

	EXEC RewardsNow.dbo.usp_ExpirePoints @Tipfirst, 1

	UPDATE	s
	SET		ExpiringPts = e.points_expiring
	FROM	Revised_Quarterly_Statement s	INNER JOIN	RewardsNow.dbo.RNIExpirationProjection e
		ON	s.Tip = e.tipnumber

END

GO
