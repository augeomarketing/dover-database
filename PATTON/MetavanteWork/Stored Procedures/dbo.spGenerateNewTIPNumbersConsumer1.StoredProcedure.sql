USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumer1]    Script Date: 05/08/2012 10:13:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersConsumer1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumer1]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumer1]    Script Date: 05/08/2012 10:13:48 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumer1] @tipfirst char(3)
AS 

EXEC [dbo].[usp_FIS_TIPassignment] @tipfirst

GO