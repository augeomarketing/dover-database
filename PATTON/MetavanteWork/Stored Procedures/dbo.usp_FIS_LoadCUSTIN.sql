USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_LoadCUSTIN]    Script Date: 01/18/2012 08:57:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_LoadCUSTIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_LoadCUSTIN]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_LoadCUSTIN]    Script Date: 01/18/2012 08:57:10 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_FIS_LoadCUSTIN] 
	@dateadded varchar(10)

AS 

TRUNCATE TABLE	custin

DELETE FROM CARDSIN WHERE NA1 IS NULL

UPDATE	Cardsin
SET		CityState = rtrim(replace(CityState, char(44), ''))

UPDATE	Cardsin
SET		CityState = '..........'
WHERE	len(rtrim(citystate)) < '3'

-- Add to CUSTIN TABLE
INSERT INTO	custin (ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, 
					STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip,
					LASTNAME, HomePhone, WorkPhone, DateAdded, DDANUM, SSN, Cardtype ) 
SELECT DISTINCT		Acctnum, NA1, NA2, NA3, NA4, NA5, NA6, 
					Status, TIPNUMBER, Addr1, Addr2, (rtrim(citystate) + ' ' + rtrim(zip)), left(citystate,(len(citystate)-3)), right(rtrim(CityState),2), ZIP,
					LASTNAME, Phone1, Phone2, @DateAdded, DDANUM, SSN, Cardtype
FROM		Cardsin
WHERE		status <> 'C' and TIPNUMBER is not null
ORDER BY	tipnumber

GO


