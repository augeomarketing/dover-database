USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPFormatNewFileStructure]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPFormatNewFileStructure]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPFormatNewFileStructure]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPFormatNewFileStructure]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPFormatNewFileStructure]
AS

Truncate Table FBOPCust3

insert into FBOPCust3(Bank, Agent, SSN, Acctnum, Oldcc, NA1, NA2, Status1, Status, Oldccpre, oldccpost, Addr1, addr2, CityState, City, State, Zip, Phone1, Phone2, Numpurch, Purch, Numret, Amtret, Period)
select distinct right(ProviderID,4), left(ClientCD,4), right(SSN,9), right(Acctnum,16), right(AcctnumXFER,16), left(rtrim(Name1),25), left(rtrim(CoAppName),25), '' '', Status, '' '', '' '', left(rtrim(Address1),36), left(rtrim(Address2),36), (left(rtrim(City),37)+'' ''+rtrim(State)), left(rtrim(City),37), rtrim(State), left(zip,10), right(HomePhone,10), right(WorkPhone,10), ''0'', ''0'', ''0'', ''0'', ''0''
from TS2CreditIn

update FBOPCust3
set Numret=''0'', amtret=''0'', numpurch=''0'', purch=''0''
where exists(select * from TS2creditin where right(Acctnum,16)=FBOPcust3.acctnum and DBCRInd=''N'')

update FBOPCust3
set Numpurch=1, purch=(select sum(total*1) from TS2creditin where right(Acctnum,16)=FBOPcust3.acctnum and DBCRInd=''D'')
where exists(select * from TS2creditin where right(Acctnum,16)=FBOPcust3.acctnum and DBCRInd=''D'')

update FBOPCust3
set Numret=1, amtret=(select sum(total*1) from TS2creditin where right(Acctnum,16)=FBOPcust3.acctnum and DBCRInd=''C'')
where exists(select * from TS2creditin where right(Acctnum,16)=FBOPcust3.acctnum and DBCRInd=''C'')

update FBOPCust3
set Numret=''0'', amtret=''0'', numpurch=''0'', purch=''0''
where status=''C''' 
END
GO
