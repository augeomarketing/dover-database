USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kCleanupDemogRawPostImport]    Script Date: 12/01/2010 11:37:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kCleanupDemogRawPostImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kCleanupDemogRawPostImport]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kCleanupDemogRawPostImport]    Script Date: 12/01/2010 11:37:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_b2kCleanupDemogRawPostImport]
as
UPDATE b2kDemogImportRaw
SET
	dim_b2kdemogimportraw_filedate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_filedate)
	, dim_b2kdemogimportraw_recordtype = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_recordtype)
	, dim_b2kdemogimportraw_relationshipaccount = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_relationshipaccount)
	, dim_b2kdemogimportraw_cardnumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_cardnumber)
	, dim_b2kdemogimportraw_primarycardnumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarycardnumber)
	, dim_b2kdemogimportraw_cardtype = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_cardtype)
	, dim_b2kdemogimportraw_accounttype = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_accounttype)
	, dim_b2kdemogimportraw_producttype = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_producttype)
	, dim_b2kdemogimportraw_productline = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_productline)
	, dim_b2kdemogimportraw_subproducttype = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_subproducttype)
	, dim_b2kdemogimportraw_programid = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_programid)
	, dim_b2kdemogimportraw_primarynameprefix = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarynameprefix)
	, dim_b2kdemogimportraw_primaryfirstname = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primaryfirstname)
	, dim_b2kdemogimportraw_primarymiddleinitial = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarymiddleinitial)
	, dim_b2kdemogimportraw_primarylastname = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarylastname)
	, dim_b2kdemogimportraw_primarynamesuffix = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarynamesuffix)
	, dim_b2kdemogimportraw_secondarynameprefix = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondarynameprefix)
	, dim_b2kdemogimportraw_secondaryfirstname = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondaryfirstname)
	, dim_b2kdemogimportraw_secondarymiddleinitial = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondarymiddleinitial)
	, dim_b2kdemogimportraw_secondarylastname = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondarylastname)
	, dim_b2kdemogimportraw_secondarynamesuffix = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondarynamesuffix)
	, dim_b2kdemogimportraw_addressline1 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_addressline1)
	, dim_b2kdemogimportraw_addressline2 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_addressline2)
	, dim_b2kdemogimportraw_addressline3 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_addressline3)
	, dim_b2kdemogimportraw_addressline4 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_addressline4)
	, dim_b2kdemogimportraw_city = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_city)
	, dim_b2kdemogimportraw_state = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_state)
	, dim_b2kdemogimportraw_country = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_country)
	, dim_b2kdemogimportraw_zipcode = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_zipcode)
	, dim_b2kdemogimportraw_homephonenumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_homephonenumber)
	, dim_b2kdemogimportraw_workphonenumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_workphonenumber)
	, dim_b2kdemogimportraw_workphoneextension = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_workphoneextension)
	, dim_b2kdemogimportraw_emailaddress = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_emailaddress)
	, dim_b2kdemogimportraw_scprogramstatus = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_scprogramstatus)
	, dim_b2kdemogimportraw_scprogramstatuschangedate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_scprogramstatuschangedate)
	, dim_b2kdemogimportraw_hostaccountstatus = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_hostaccountstatus)
	, dim_b2kdemogimportraw_hostacctstatusdate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_hostacctstatusdate)
	, dim_b2kdemogimportraw_scparticipationflag = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_scparticipationflag)
	, dim_b2kdemogimportraw_primarysocialsecuritynumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarysocialsecuritynumber)
	, dim_b2kdemogimportraw_secondarysocialsecuritynumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondarysocialsecuritynumber)
	, dim_b2kdemogimportraw_primarygender = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primarygender)
	, dim_b2kdemogimportraw_secondarygender = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_secondarygender)
	, dim_b2kdemogimportraw_birthdate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_birthdate)
	, dim_b2kdemogimportraw_accountopendate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_accountopendate)
	, dim_b2kdemogimportraw_languagepreference = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_languagepreference)
	, dim_b2kdemogimportraw_effectivedate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_effectivedate)
	, dim_b2kdemogimportraw_purchaserate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_purchaserate)
	, dim_b2kdemogimportraw_newcardnumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_newcardnumber)
	, dim_b2kdemogimportraw_associationid = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_associationid)
	, dim_b2kdemogimportraw_corpid = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_corpid)
	, dim_b2kdemogimportraw_agentinstitutionid = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_agentinstitutionid)
	, dim_b2kdemogimportraw_billcode = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_billcode)
	, dim_b2kdemogimportraw_binplan = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_binplan)
	, dim_b2kdemogimportraw_scenrollmentdate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_scenrollmentdate)
	, dim_b2kdemogimportraw_laststatementdate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_laststatementdate)
	, dim_b2kdemogimportraw_transferdate = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_transferdate)
	, dim_b2kdemogimportraw_filler1 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_filler1)
	, dim_b2kdemogimportraw_primaryhouseholdcardnumber = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_primaryhouseholdcardnumber)
	, dim_b2kdemogimportraw_filler2 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_filler2)
	, dim_b2kdemogimportraw_householdcardtype = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_householdcardtype)
	, dim_b2kdemogimportraw_institutionid = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_institutionid)
	, dim_b2kdemogimportraw_vipindicator = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_vipindicator)
	, dim_b2kdemogimportraw_filler3 = RewardsNow.dbo.ufn_Trim(dim_b2kdemogimportraw_filler3)


--EXEC usp_b2kScrubDemographicsRaw

GO


