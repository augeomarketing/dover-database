USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_Kroger_Validate_Control_Total_1P]    Script Date: 07/18/2016 07:51:58 ******/
DROP PROCEDURE [dbo].[usp_Kroger_Validate_Control_Total_1P]
GO
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[usp_Kroger_Validate_Control_Total_1P] 
		@Test			char(1),
		@EndDate  date,
        @errorrows	    int = 0 OUTPUT 

AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Records Loaded Match the trailer record                         */
/*                                                                            */
/******************************************************************************/
 
declare @CountDet		int
declare @CountTot		int
declare @SumDet		int
declare @SumTot		int
declare @TipKroger char(3)
declare @TipRalph char(3)
declare @TipFredMeyer char(3)

--Controls tip first based on flag passed in
if @Test = 'T'
	Begin
		set @TipKroger = '5KJ'
		set @TipRalph = '5KR'
		set @TipFredMeyer = '5KW'
	End
	Else
		Begin
		set @TipKroger = '52J'
		set @TipRalph = '52R'
		set @TipFredMeyer = '52W'
	End

-- THIS SECTION CHECKS TO SEE THAT ALL THAT WAS SENT WAS LOADED TO RAWIMPORT
-- IF MATCH THEN CONTINUES OTHERWISE IT RETURNS ERROR.
set @CountDet = (select COUNT(*) from Rewardsnow.dbo.RNIRawImport With (NoLock) 
								where sid_dbprocessinfo_dbnumber='5KK'
								and sid_rniimportfiletype_id=2
								and dim_rnirawimport_field01!='Count'
								and LEN(dim_rnirawimport_field01)>0)
								
set @SumDet = (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
							where sid_dbprocessinfo_dbnumber='5KK'
							and sid_rniimportfiletype_id=2
							and dim_rnirawimport_field01!='Count')
							
set @CountTot = (select isnull(cast (dim_rnirawimport_field02 as int),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
								where sid_dbprocessinfo_dbnumber='5KK'
								and sid_rniimportfiletype_id=2
								and dim_rnirawimport_field01='Count')

set @SumTot = (select isnull(cast (dim_rnirawimport_field04 as int),'0')  from Rewardsnow.dbo.RNIRawImport With (NoLock) 
							where sid_dbprocessinfo_dbnumber='5KK'
							and sid_rniimportfiletype_id=2
							and dim_rnirawimport_field01='Count')

--CHECK IF WHAT WAS LOADED MATCHES THE TRAILER RECORD
if (@CountDet <>@CountTot or @SumDet<>@SumTot)
	Begin
		set @errorrows = 1
		Return
	End
else 
	set @errorrows = 0

--THIS SETS THE TIP FIRST
	update Rewardsnow.dbo.RNIRawImport
	set sid_dbprocessinfo_dbnumber=
		case 
		when dim_rnirawimport_field01 like 'Kroger%' then @TipKroger
		when dim_rnirawimport_field01 like 'Ralph%' then @TipRalph
		when dim_rnirawimport_field01 like 'Fred Meyer%' then @TipFredMeyer
		else '5KK'
		end
	where sid_dbprocessinfo_dbnumber = '5KK'
	and sid_rniimportfiletype_id = 2	

--THIS LOADS AMOUNTS INTO RECONCILLIATION FILE	
--KROGER	
	insert into [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
			([TipFirst],[ProcessDate], [ValueInPoint1], [ValueInPoint2])
	select 	@TipKroger, @EndDate
			, ( (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipKroger
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '1')
			  - 
				(select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipKroger
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '0')
			)
			, ( (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipKroger
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '1'
									and dim_rnirawimport_field14 like '%K%')
			  - 
				(select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipKroger
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '0'
									and dim_rnirawimport_field14 like '%K%')
			)
--RALPHS							
	insert into [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
			([TipFirst],[ProcessDate], [ValueInPoint1], [ValueInPoint2])
	select 	@TipRalph, @EndDate
			, ( (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipRalph
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '1')
			  - 
				(select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipRalph
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '0')
			)
			, ( (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipRalph
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '1'
									and dim_rnirawimport_field14 like '%K%')
			  - 
				(select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipRalph
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '0'
									and dim_rnirawimport_field14 like '%K%')
			)

--FRED MEYERS
	insert into [52JKroegerPersonalFinance].dbo.[KroegerReconcilliationData_Central]
			([TipFirst],[ProcessDate], [ValueInPoint1], [ValueInPoint2])
	select 	@TipFredMeyer, @EndDate
			, ( (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipFredMeyer
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '1')
			  - 
				(select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipFredMeyer
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '0')
			)
			, ( (select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipFredMeyer
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '1'
									and dim_rnirawimport_field14 like '%K%')
			  - 
				(select isnull(SUM(cast(dim_rnirawimport_field11 as int)),'0') from Rewardsnow.dbo.RNIRawImport With (NoLock) 
									where sid_dbprocessinfo_dbnumber=@TipFredMeyer
									and sid_rniimportfiletype_id=2
									and dim_rnirawimport_field01!='Count'
									and dim_rnirawimport_field09 = '0'
									and dim_rnirawimport_field14 like '%K%')
			)

-- This is to delete the trailer record if counts are good.			
	Delete from Rewardsnow.dbo.RNIRawImport 
	where sid_dbprocessinfo_dbnumber =
				case 
					when dim_rnirawimport_field01 like 'Kroger%' then @TipKroger
					when dim_rnirawimport_field01 like 'Ralph%' then @TipRalph
					when dim_rnirawimport_field01 like 'Fred Meyer%' then @TipFredMeyer
					else '5KK'
				end
	and sid_rniimportfiletype_id = 2
	and dim_rnirawimport_field01 = 'Count'

GO


