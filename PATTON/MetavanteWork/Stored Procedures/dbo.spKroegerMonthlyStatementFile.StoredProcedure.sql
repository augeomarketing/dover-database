USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerMonthlyStatementFile]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerMonthlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerMonthlyStatementFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerMonthlyStatementFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 11/09
-- Description:	Create Kroeger Monthly audit file.
--
-- Change 5/2010
-- BY S Blanchette
-- Remove status check on insert into Customer
-- SEB001
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerMonthlyStatementFile]  
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10), 
	@EndDateParm char(10), 
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here

	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

	declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
	set @MonthBucket=''MonthBeg'' + @monthbegin

	/*******************************************************************************/
	/*******************************************************************************/
	/*                                                                             */
	/*          ISSUES WITH ADJUSTMENTS                                            */
	/*                                                                             */
	/*******************************************************************************/
	/*******************************************************************************/
	/* Load the statement file from the customer table  */
	set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Monthly_Statement_File ''
	exec sp_executesql @SQLTruncate


	if exists(select name from sysobjects where name=''wrkhist'')
	begin 
		truncate table wrkhist
	end

	set @SQLSelect=''insert into wrkhist
			select tipnumber, trancode, sum(points*ratio) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
	Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

	set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip, LoyaltyNumber)
        				select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), left(zipcode,5), misc2 
					from '' + QuoteName(@DBName) + N''.dbo.customer order by tipnumber ''
	Exec sp_executesql @SQLInsert

	set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File 
		set acctnum = rtrim(b.acctid)
		from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.affiliat b 
		where a.tipnumber = b.tipnumber and b.acctstatus=''''A'''' ''
	Exec sp_executesql @SQLUpdate


	/*******************************/
	/* START PURCHASES             */
	/*******************************/
	/* Load the statmement file with DEBIT purchases   67       */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointspurchaseddb=
			isnull((Select sum(points) from wrkhist 
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''64'''',''''65'''',''''66'''',''''67'''',''''6B'''')),0) '' 	
		Exec sp_executesql @SQLUpdate 

	/*******************************/
	/* START BONUSES               */
	/*******************************/
	/* Load the statmement file with DEBIT Bonuses             */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusdb=
			isnull((select sum(points) from wrkhist 
			where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''B%'''' or trancode= ''''NW'''')),0) 	
			where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''B%'''' or trancode= ''''NW'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusdb= 0 
			where pointsbonusdb is null'' 	
		Exec sp_executesql @SQLUpdate 	
	/* Load the statmement file with MERCHANT Bonuses          */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusmer=
			isnull((select sum(points) from wrkhist 
			where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and trancode = ''''FA''''),0) 	
			where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and trancode = ''''FA'''') ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonusmer= 0 
			where pointsbonusmer is null'' 	
		Exec sp_executesql @SQLUpdate 
	/* Load the statmement file with 2 Point Bonuses             */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonus2PT=
			isnull((select sum(points) from wrkhist 
			where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode = ''''G2'''')),0) 	
			where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode = ''''G2'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonus2PT= 0 
			where pointsbonus2PT is null'' 	
		Exec sp_executesql @SQLUpdate 	
	/* Load the statmement file with 3 Point Bonuses             */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonus3PT=
			isnull((select sum(points) from wrkhist 
			where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode = ''''G3'''')),0) 	
			where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode = ''''G3'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbonus3PT= 0 
			where pointsbonus3PT is null'' 	
		Exec sp_executesql @SQLUpdate 	


	/*******************************/
	/* START ADJUSTMENTS PLUS      */
	/*******************************/
	/* Load the statmement file with plus adjustments    */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsadded=
			isnull((Select sum(points) from wrkhist
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''IE'''',''''TP'''')),0) '' 	
		Exec sp_executesql @SQLUpdate 

	/*******************************/
	/* TOTAL POINT INCREASES       */
	/*******************************/
	/* Load the statmement file with total point increases */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsbonusmer + pointsbonus2PT + pointsbonus3PT+ pointsadded ''
		Exec sp_executesql @SQLUpdate

	/************************************************************************************************************************/

	/*******************************/
	/* START REDEMPTIONS           */
	/*******************************/
	/* Load the statmement file with redemptions          */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsredeemed=
			isnull((select sum(points*-1) from wrkhist 
			where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''R%'''' or trancode in (''''IR'''',''''DR''''))),0) 	
			where exists(select * from wrkhist where tipnumber='' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber and (trancode like ''''R%'''' or trancode in (''''IR'''',''''DR''''))) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsredeemed= 0 
			where pointsredeemed is null'' 	
		Exec sp_executesql @SQLUpdate 

	/*******************************/
	/* START RETURNS     */
	/*******************************/
	/* Load the statmement file with CREDIT returns            */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsreturnedcr=
			isnull((Select sum(points*-1) from wrkhist 
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''31'''',''''32'''',''''33'''')),0) '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT returns             */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsreturneddb=
			isnull((Select sum(points*-1) from wrkhist 
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''34'''',''''35'''',''''36'''',''''37'''',''''3B'''', ''''GX'''')),0) '' 	
		Exec sp_executesql @SQLUpdate 

/* Load the statmement file with 2PT returns             */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set PointsReturned2PT=
			isnull((Select sum(points*-1) from wrkhist 
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''G7'''')),0) '' 	
		Exec sp_executesql @SQLUpdate 

/* Load the statmement file with 3PT returns             */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set PointsReturned3PT=
			isnull((Select sum(points*-1) from wrkhist 
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode in (''''G8'''')),0) '' 	
		Exec sp_executesql @SQLUpdate 

	/*******************************/
	/* START ADJUSTMENTS MINUS     */
	/*******************************/
	/* Load the statmement file with minus adjustments    */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointssubtracted=
			isnull((Select sum(points*-1) from wrkhist 
			where '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File.tipnumber = wrkhist.tipnumber and wrkhist.trancode = ''''DE''''),0) '' 	
		Exec sp_executesql @SQLUpdate 

	/*******************************/
	/* TOTAL POINT DECREASES       */
	/*******************************/
	/* Load the statmement file with total point decreases */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + PointsReturned2PT + PointsReturned3PT + pointssubtracted ''
	Exec sp_executesql @SQLUpdate

	/************************************************************************************************************************/

	/*******************************/
	/* START BEGIN BALANCE         */
	/*******************************/
	/* Load the statmement file with the Beginning balance for the Month */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbegin = isnull((b.'' + Quotename(@MonthBucket) + N''),0) 
			from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
			where a.tipnumber = b.tipnumber ''
	exec sp_executesql @SQLUpdate

	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsbegin = 0 
			where pointsbegin is null''
	exec sp_executesql @SQLUpdate

	/*******************************/
	/* START END BALANCE           */
	/*******************************/
	/* Load the statmement file with ending points */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File set pointsend=isnull((pointsbegin + pointsincreased - pointsdecreased),0) ''
	exec sp_executesql @SQLUpdate




	/* Create summary record */
	/**********************************/
	/*  This section added  SEB002    */
	/**********************************/

	--Truncate table AuditSummary


	set @SQLUpdate=N''insert into '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File 
			select ''''999999999999999'''' 
			, ''''SUMMARY RECORD'''' 
			, ''''TOTALS'''' 
			, '''' '''' 
			, '''' '''' 
			, '''' '''' 
			, '''' ''''	
			, '''' '''' 
			, sum(PointsBegin) 
			, sum(PointsEnd) 
			, sum(PointsPurchasedCR) 
			, sum(PointsBonusCR) 
			, sum(PointsAdded) 
			, sum(PointsPurchasedDB) 
			, sum(PointsBonusDB) 
			, sum(PointsBonus2PT)
			, sum(PointsBonus3PT)
			, sum(PointsBonusMER) 
			, sum(PointsIncreased) 
			, sum(PointsRedeemed) 
			, sum(PointsReturnedCR) 
			, sum(PointsSubtracted) 
			, sum(PointsReturnedDB) 
			, sum(PointsReturned2PT) 
			, sum(PointsReturned3PT) 
			, sum(PointsDecreased) 
			, ''''0'''' 
			, '''' ''''
			, '''' ''''
			from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File ''
	exec sp_executesql @SQLUpdate

	--set @SQLUpdate=N''insert into '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File
	--		select *
	--		from AuditSummary ''
	--exec sp_executesql @SQLUpdate

END
' 
END
GO
