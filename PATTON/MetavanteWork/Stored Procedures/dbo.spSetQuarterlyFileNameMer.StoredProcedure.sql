USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSetQuarterlyFileNameMer]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetQuarterlyFileNameMer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetQuarterlyFileNameMer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetQuarterlyFileNameMer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 4/09
-- Description:	Add Merchant bonus
-- =============================================
CREATE PROCEDURE [dbo].[spSetQuarterlyFileNameMer]
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output, 
	@ConnectionString nchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @Filename char(70), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename=''Q'' + @TipPrefix + @currentdate + ''.xls''
 
set @newname=''O:\5xx\Output\QuarterlyFiles\QTRMER.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\QuarterlyFiles\'' + @filename
END
' 
END
GO
