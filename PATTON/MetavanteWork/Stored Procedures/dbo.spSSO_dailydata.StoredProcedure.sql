USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSSO_dailydata]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSSO_dailydata]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSSO_dailydata]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSSO_dailydata]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[spSSO_dailydata]

as


--Create table [MetavanteWork].dbo.[SSO_work1] (RNI_Number char(15), Points_Avail bigint, AccountID char(16), Points_DateTime Char(16), Cust_Name Char(50))
--Create table [MetavanteWork].dbo.[SSO_work2] (RNI_Number char(15), Deleted_DateTime Char(16))
Truncate table [MetavanteWork].dbo.[SSO_work1]
Truncate table [MetavanteWork].dbo.[SSO_work2]

Select tipnumber, availablebal
into #tmpresults
from [rn1].metavante.dbo.customer

Create index tmpindex on #tmpresults (tipnumber)

--Cursor through DBProcessinfo and populate work tables
Declare curDBName cursor FAST_FORWARD
for Select dbnamePatton from rewardsnow.dbo.dbprocessinfo
where left(DBNumber, 1) = ''5'' and sid_FIProdStatus_StatusCode = ''P''

Declare @DBName varchar(100)
Declare @sqlcmd nvarchar(1000)

open curDBName
Fetch next from curDBName into @DBName
while @@Fetch_Status = 0
	Begin
	  set @dbname = rtrim(@dbname)
	  set @sqlcmd =	
		''Insert into [MetavanteWork].dbo.[SSO_work1]
		 Select	c.tipnumber as RNI_Number, t.availablebal as Points_Avail, a.Acctid as AccountID,
				Convert( char(16), GetDate() , 121) as Points_DateTime, RTrim(Acctname1 ) as Cust_Name
		 From [''+@DBName+''].dbo.Customer c join [''+@DBName+''].dbo.Affiliat a on c.tipnumber = a.tipnumber
				join #tmpresults t on c.tipnumber = t.tipnumber
		 Order by c.tipnumber''
--PRINT @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  set @sqlcmd =	
		''Insert into [MetavanteWork].dbo.[SSO_work2]
		 Select	tipnumber as RNI_Number, Convert( char(16), Datedeleted , 121) as Deleted_DateTime
		 From [''+@DBName+''].dbo.Customerdeleted
		 Order by tipnumber''
--PRINT @sqlcmd
	  EXECUTE sp_executesql @SqlCmd
	  fetch Next from curDBName into @DBName
	end
Close curDBName
Deallocate curDBName

Drop table #tmpresults' 
END
GO
