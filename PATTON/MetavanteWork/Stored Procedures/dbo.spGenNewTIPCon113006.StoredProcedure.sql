USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGenNewTIPCon113006]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenNewTIPCon113006]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenNewTIPCon113006]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenNewTIPCon113006]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGenNewTIPCon113006] 
AS

exec spGenerateNewTIPNumbersConsumer113006 ''500''

exec spGenerateNewTIPNumbersConsumer113006 ''501''

exec spGenerateNewTIPNumbersConsumer113006 ''502''

exec spGenerateNewTIPNumbersConsumer113006 ''503''

exec spGenerateNewTIPNumbersConsumer113006 ''504''

exec spGenerateNewTIPNumbersConsumer113006 ''506''

exec spGenerateNewTIPNumbersConsumer113006 ''508''

exec spGenerateNewTIPNumbersConsumer113006 ''511''

exec spGenerateNewTIPNumbersConsumer113006 ''515''

exec spGenerateNewTIPNumbersConsumer113006 ''517''

exec spGenerateNewTIPNumbersConsumer113006 ''521''

exec spGenerateNewTIPNumbersConsumer113006 ''523''

exec spGenerateNewTIPNumbersConsumer113006 ''525''

exec spGenerateNewTIPNumbersConsumer113006 ''529''

exec spGenerateNewTIPNumbersConsumer113006 ''531''

exec spGenerateNewTIPNumbersConsumer113006 ''533''

exec spGenerateNewTIPNumbersConsumer113006 ''535''

exec spGenerateNewTIPNumbersConsumer113006 ''537''' 
END
GO
