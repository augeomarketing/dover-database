USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransactionDataKroeger]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataKroeger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionDataKroeger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionDataKroeger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 12/09
-- Description:	Import 1 point transactions
-- =============================================
CREATE PROCEDURE [dbo].[spImportTransactionDataKroeger]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3), 
	@StartDateParm char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000)
declare @DBnumber char(3), @Revision smallint, @Begindate datetime, @Enddate datetime, @Startdate datetime, @SigFactor numeric(18,2), @PinFactor numeric(18,2), @WorkFactorSig numeric(18,2), @WorkFactorPin numeric(18,2)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @Startdate = convert(datetime, @Startdateparm + '' 00:00:00:001'')  

truncate table transum

delete from cardsin where tipnumber is null


/*********************************************************************/
/*  Get transaction data into a standard format                      */
/*********************************************************************/
insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin	
from cardsin
group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin

/*********************************************************************/
/*  start SEB002 Remove records that do not have a customer record   */
/*********************************************************************/
set @SQLDelete=''Delete from dbo.transum
				where tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.customer) ''
Exec sp_executesql @SQLDelete
/*********************************************************************/
/*  end SEB002 Remove records that do not have a customer record   */
/*********************************************************************/

/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio=''1'', trancode=''67'', description=''Debit Card Purchase'', amtpurch=cast((amtpurch/100) as int) 
where amtpurch>0 and cardtype=''D'' 

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '''' '''', ratio, overage
		from transum
		where trancode=''''67'''' ''
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
update transum
set ratio=''-1'', trancode=''37'', description=''Debit Card Returns'', amtcr=cast((amtcr/100) as int)
where amtcr>0 and cardtype=''D''

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history
        	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '''' '''', ratio, overage
		from transum
		where trancode=''''37'''' ''
Exec sp_executesql @SQLInsert

/***********************/
/*  end section SEB001  */
/***********************/

delete from transum
where trancode is null

/*********************************************************************/
/*  Update Customer                                                  */
/*********************************************************************/
set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.customer 
		set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber = '' + QuoteName(@DBName) + N''.dbo.customer.tipnumber), 
			runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber = '' + QuoteName(@DBName) + N''.dbo.customer.tipnumber) 
		where exists(select * from transum where tipnumber = '' + QuoteName(@DBName) + N''.dbo.customer.tipnumber) ''
Exec sp_executesql @SQLUpdate
END
' 
END
GO
