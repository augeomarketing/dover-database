USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPLoadCardsinWithDebitCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadCardsinWithDebitCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPLoadCardsinWithDebitCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadCardsinWithDebitCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPLoadCardsinWithDebitCards]
as

--inserts records from debitcardin2 into Cardsin
insert into FBOPCardsin(tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, Cardtype, SigvsPin)
select tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, ''D'', SigvsPin from FBOPdebitcardin2
order by ssn asc, ddanum asc' 
END
GO
