USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPLoadBeginningBalance]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadBeginningBalance]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPLoadBeginningBalance]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadBeginningBalance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPLoadBeginningBalance] @MonthBeginDate nchar(10), @tipFirst char(3)
AS

Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName varchar(50)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

--set @monthfix=CONVERT( int , left(@MonthBeginDate,2)) + ''1''
set @MonthBegin=CONVERT( int , left(@MonthBeginDate,2)) + ''1''

--set @MonthBegin=@Monthfix

/* if CONVERT( int , @MonthBegin)<''10'' 
	begin
	set @monthbegin=''0'' + left(@monthbegin,1)
	end	
else  */
	if CONVERT( int , @MonthBegin)=''13'' 
		begin
		set @monthbegin=''1''
		end	

set @MonthBucket=''MonthBeg'' + @monthbegin

set @SQLUpdate=N''update ''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table set ''+ Quotename(@MonthBucket) + N''= 
		(select pointsend from ''+ Quotename(@DBName) + N''.dbo.FBOPStatement where tipnumber= ''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table.tipnumber) 
		where exists(select * from ''+ Quotename(@DBName) + N''.dbo.FBOPStatement where tipnumber=''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table.tipnumber)''

set @SQLInsert=N''insert into ''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table (Tipnumber, '' + Quotename(@MonthBucket) + '') 
		select tipnumber, pointsend from ''+ Quotename(@DBName) + N''.dbo.FBOPStatement 
		where not exists(select * from ''+ Quotename(@DBName) + N''.dbo.Beginning_Balance_Table where tipnumber=''+ Quotename(@DBName) + N''.dbo.FBOPStatement.tipnumber)''

exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert' 
END
GO
