USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spImportCustomerData]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportCustomerData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerData]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spImportCustomerData] @Tipfirst char(3)
AS 

declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @StatusDesc nvarchar(50), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @TIPNUMBER_inserted nchar(15), @Acctid_inserted nchar(15)
declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLSet nvarchar(2000), @SQLIf nvarchar(2000), @SQLCursor nvarchar(2000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsNow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

drop table wrkcustin

select *
into wrkcustin
from custin
order by tipnumber, status desc


/*****************************************************************/
/*  DEAL WITH CUSTOMER FILE                                      */
/*	                                                         */
/*****************************************************************/

set @SQLUpdate=''
	Update '' + QuoteName(@DBName) + N''.dbo.Customer
	Set
	  AcctName1	= b.NAMEACCT1
	, AcctName2	= b.NAMEACCT2
	, AcctName3	= b.NAMEACCT3
	, AcctName4	= b.NAMEACCT4
	, AcctName5	= b.NAMEACCT5
	, AcctName6	= b.NAMEACCT6
	, Address1	= b.Address1
	, Address2	= b.Address2
	, Address4	= b.Address4
	, City		= rtrim(b.City)
	, State		= rtrim(b.STATE)
	, Zipcode	= rtrim(b.zip)
	, HomePhone	= b.HomePhone
	, WorkPhone	= b.WorkPhone
	, lastname	= b.lastname
	, misc1		= b.SSN
	, misc2		= b.misc2
	, misc3		= b.misc3
	, status	= b.STATUS
	From '' + QuoteName(@DBName) + N''.dbo.customer a, wrkcustin b 
	Where a.TIPNUMBER = b.TIPNUMBER ''
exec sp_executesql @SQLUpdate 

Set @TIPNUMBER_inserted = ''0''
	
set @SQLCursor = ''declare CUSTIN_crsr cursor
		for select ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3, DDANUM, SSN, Cardtype
		from custin
		where not exists(select * from '' + QuoteName(@DBName) + N''.dbo.customer where tipnumber=custin.tipnumber) and tipnumber is not null and left(tipnumber,1)<>'''' ''''  
		order by tipnumber, status asc ''
exec sp_executesql @SQLCursor
/*                                                                            */
open CUSTIN_crsr
fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3, @DDANUM, @SSN, @Cardtype

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
--if @@FETCH_STATUS = 1
--	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

	IF @TIPNUMBER_inserted <> @TIPNUMBER
	BEGIN
		set @SQLInsert=''		
		INSERT INTO '' + QuoteName(@DBName) + N''.dbo.customer 
		(	TIPNumber
			, Runavailable
			, Runbalance
			, Runredeemed
			, TIPFirst
			, TIPLast
			, AcctName1
			, AcctName2	
			, AcctName3
			, AcctName4
			, AcctName5
			, AcctName6
			, Address1
			, Address2
			, Address4
			, City
			, State
			, Zipcode
			, Status
			, StatusDescription
			, HomePhone
			, WorkPhone
			, DateAdded
			, Lastname
			, Misc1
			, Misc2
			, Misc3
			, SegmentCode
		)
		Values ( 
			@tipnumber
			, ''''0''''
			, ''''0''''
			, ''''0''''
			, LEFT(@tipnumber,3)
			, right(@tipnumber,12)
			, @NAMEACCT1
			, @NAMEACCT2
			, @NAMEACCT3
			, @NAMEACCT4
			, @NAMEACCT5
			, @NAMEACCT6
			, @ADDRESS1
			, @ADDRESS2
			, @ADDRESS4
			, rtrim(@City)
			, rtrim(@state)
			, rtrim(@zip)
			, ''''A''''
			, '''' ''''
			, @HomePhone
			, @WorkPhone
			, cast(@DateAdded as datetime)
			, @LastName
			, @SSN
			, @Misc2
			, @Misc3
			, @Cardtype) '' 
		exec sp_executesql @SQLInsert, N''@ACCT_NUM nvarchar(25)
						, @NAMEACCT1 nvarchar(40)
						, @NAMEACCT2 nvarchar(40)
						, @NAMEACCT3 nvarchar(40)
						, @NAMEACCT4 nvarchar(40)
						, @NAMEACCT5 nvarchar(40)
						, @NAMEACCT6 nvarchar(40)
						, @TIPNUMBER nvarchar(15)
						, @ADDRESS1 nvarchar(40)
						, @ADDRESS2 nvarchar(40)
						, @ADDRESS4 nvarchar(40)
						, @CITY nvarchar(38)
						, @STATE char(2)
						, @ZIP nvarchar(15)
						, @LASTNAME nvarchar(40)
						, @HOMEPHONE nvarchar(10)
						, @WORKPHONE nvarchar(10)
						, @DATEADDED nvarchar(10)
						, @MISC2 nvarchar(20)
						, @MISC3 nvarchar(20)
						, @SSN char(9)
						, @Cardtype char(1)'',
						@ACCT_NUM = @ACCT_NUM
						, @NAMEACCT1 = @NAMEACCT1
						, @NAMEACCT2 = @NAMEACCT2
						, @NAMEACCT3 = @NAMEACCT3
						, @NAMEACCT4 = @NAMEACCT4
						, @NAMEACCT5 = @NAMEACCT5
						, @NAMEACCT6 = @NAMEACCT6
						, @TIPNUMBER = @TIPNUMBER
						, @ADDRESS1 = @ADDRESS1
						, @ADDRESS2 = @ADDRESS2
						, @ADDRESS4 = @ADDRESS4
						, @CITY = @CITY
						, @STATE = @STATE
						, @ZIP = @ZIP
						, @LASTNAME = @LASTNAME
						, @HOMEPHONE = @HOMEPHONE
						, @WORKPHONE = @WORKPHONE
						, @DATEADDED = @DATEADDED 
						, @MISC2 = @MISC2
						, @MISC3 = @MISC3
						, @SSN = @SSN
						, @Cardtype = @Cardtype
	end					

	Set @TIPNUMBER_inserted = @Tipnumber

	goto Next_Record

Next_Record:
		fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3, @DDANUM, @SSN, @Cardtype

end

Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr

/*****************************************************************/
/*  UPDATE STATUS DESCRIPTION IN CUSTOMER TABLE                  */
/*	                                                         */
/*****************************************************************/

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.customer set StatusDescription=(select StatusDescription 
			from '' + QuoteName(@DBName) + N''.dbo.Status
        		where 	Status='' + QuoteName(@DBName) + N''.dbo.customer.STATUS) ''
Exec sp_executesql @SQLUpdate

/*****************************************************************/
/*  DEAL WITH AFFILIAT TABLE                                     */
/*	                                                         */
/*****************************************************************/

set @SQLUpdate=''
	update '' + QuoteName(@DBName) + N''.dbo.AFFILIAT 
	set 
	  lastname	= b.lastname
	, acctstatus	= b.status
	, Secid		= b.SSN
	, CustID	= b.DDANUM 
	from '' + QuoteName(@DBName) + N''.dbo.affiliat a, wrkcustin b			
	where a.acctid = b.acct_num   ''
exec sp_executesql @SQLUpdate 

/******************************************************************************/	
/*                                                                            */
/* SECTION TO INSERT AFFILIAT RECORDS                                         */
/*                                                                            */
/******************************************************************************/	
Set @Acctid_inserted = ''0''
	
set @SQLCursor = N''declare AFFILIAT_crsr cursor
			for select ACCT_NUM, TIPNUMBER, LASTNAME, DATEADDED, DDANUM, SSN, Cardtype
			from custin
			where not exists (select * from '' + QuoteName(@DBName) + N''.dbo.affiliat where rtrim(ACCTID)= rtrim(ACCT_NUM)) and status = ''''A'''' and ACCT_NUM is not null and left(ACCT_NUM,1)<>'''' '''' 
			order by ACCT_NUM, status asc '' 
exec sp_executesql @SQLCURSOR 
/*                                                                            */
open AFFILIAT_crsr
fetch AFFILIAT_crsr into @ACCT_NUM, @TIPNUMBER, @LASTNAME, @DATEADDED, @DDANUM, @SSN, @Cardtype

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
/*                                                                            */
while @@FETCH_STATUS = 0
begin

	IF @Acctid_inserted <> @ACCT_NUM
	BEGIN

		set @SQLInsert=''	
		INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat
		(
			ACCTID
			, TIPNUMBER
			, AcctType
			, DATEADDED
			, SecID
			, AcctStatus
			, AcctTypeDesc
			, LastName
			, YTDEarned
			, CustID
		)
		VALUES (
			rtrim(@ACCT_NUM)
			, @tipnumber
			, @Cardtype
			, cast(@DateAdded as datetime)
			, @SSN
			, ''''A''''
			, '''' ''''
			, @lastname
			, ''''0''''
			, @DDANUM 
		) ''
		exec sp_executesql @SQLInsert, N''@ACCT_NUM nvarchar(25)
						, @TIPNUMBER nvarchar(15)
						, @LASTNAME nvarchar(40)
						, @DATEADDED nvarchar(10)
						, @SSN char(9)
						, @DDANUM char(13)
						, @Cardtype char(1)'',
						@ACCT_NUM = @ACCT_NUM
						, @TIPNUMBER = @TIPNUMBER
						, @LASTNAME = @LASTNAME
						, @DATEADDED = @DATEADDED 
						, @SSN = @SSN
						, @DDANUM = @DDANUM
						, @Cardtype = @Cardtype 

	end					

	Set @Acctid_inserted = @ACCT_NUM

	goto Next_Record1

Next_Record1:
	fetch AFFILIAT_crsr into @ACCT_NUM, @TIPNUMBER, @LASTNAME, @DATEADDED, @DDANUM, @SSN, @Cardtype

end

Fetch_Error1:
close  AFFILIAT_crsr
deallocate  AFFILIAT_crsr

/*****************************************************************/
/*  UPDATE ACCOUNT TYPE IN AFFILIAT TABLE                        */
/*	                                                         */
/*****************************************************************/

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.affiliat set accttype=''''Credit''''
        		where 	accttype=''''C'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.affiliat set accttype=''''Debit''''
        		where 	accttype=''''D'''' ''
Exec sp_executesql @SQLUpdate

/*****************************************************************/
/*  UPDATE ACCOUNT TYPE DESCRIPTION IN AFFILIAT TABLE            */
/*	                                                         */
/*****************************************************************/

set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.affiliat set accttypedesc=(select Accttypedesc 
			from '' + QuoteName(@DBName) + N''.dbo.accttype
        		where 	accttype='' + QuoteName(@DBName) + N''.dbo.affiliat.Accttype) ''
Exec sp_executesql @SQLUpdate' 
END
GO
