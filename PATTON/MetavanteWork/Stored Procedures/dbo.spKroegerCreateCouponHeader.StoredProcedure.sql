USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerCreateCouponHeader]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCreateCouponHeader]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerCreateCouponHeader]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerCreateCouponHeader]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 11/09
-- Description:	Create header record for coupon request file
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerCreateCouponHeader] 
	-- Add the parameters for the stored procedure here
	@Datein char (10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Declare @Date char(8), @Time char(8), @TimeB char(8), @Batchnum numeric (9), @Batch char(10)

	Truncate Table Kroeger_Coupon_Request_Header

	Insert into Kroeger_Coupon_Request_Header
	Values (''H'', @Datein)
END

' 
END
GO
