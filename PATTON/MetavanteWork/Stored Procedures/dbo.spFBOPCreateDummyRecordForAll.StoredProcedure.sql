USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPCreateDummyRecordForAll]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPCreateDummyRecordForAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPCreateDummyRecordForAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPCreateDummyRecordForAll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPCreateDummyRecordForAll] 
	-- Add the parameters for the stored procedure here
	@tipfirst char(3),
	@Enddate char(10),
	@Bankin char(3)
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)
declare @Tipnumber char(15), @Banknbr char(3), @Name char (40), @SSN char(9), @LastName char(40), @accnt char(16)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber, banknbr, ssn, lastname
from EmployeeIncentivebonus
where banknbr=@BankIn and left(tipnumber,3) = @tipfirst and tipnumber is not null and ssn is not null and ssn <> ''000000000''

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @Banknbr, @SSN, @LastName
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	  /***********************************/
	  	
		set @SQLInsert=''if @ssn not in  (select acctid from '' + QuoteName(@DBName) + N''.dbo.affiliat) 
						Begin		
							INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat 
							(	Acctid
							, Tipnumber
							, AcctType
							, Dateadded
							, SecID
							, Acctstatus
							, LastName	
							)	
							Values(
							@ssn
							, @Tipnumber
							, ''''Dummy''''
							, @Enddate
							, @ssn
							, ''''A''''
							, @LastName
							) 
						END ''
			exec sp_executesql @SQLInsert, N'' @Tipnumber nvarchar(15)
								, @Enddate nvarchar(10)
								, @SSN char(9)
								, @Lastname Char(40) ''
								, @Tipnumber = @Tipnumber
								, @Enddate = @Enddate
								, @SSN = @SSN
								, @LASTNAME = @LASTNAME
								
			fetch tip_crsr into @Tipnumber, @Banknbr, @SSN, @LastName
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

END
' 
END
GO
