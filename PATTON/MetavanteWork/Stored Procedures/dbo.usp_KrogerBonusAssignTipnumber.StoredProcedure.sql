use MetavanteWork
go

IF OBJECT_ID(N'usp_KrogerBonusAssignTipnumber') IS NOT NULL
	DROP PROCEDURE usp_KrogerBonusAssignTipnumber
GO

CREATE PROCEDURE usp_KrogerBonusAssignTipnumber
AS
SET NOCOUNT ON

declare @sql nvarchar(max)
declare @tipcount int

DECLARE @tips TABLE (
	myid int not null identity(1,1) primary key
	, sid_dbprocessinfo_dbnumber varchar(3)
	, dim_dbprocessinfo_dbnamepatton varchar(255)
)

declare @myid int
	, @sid_dbprocessinfo_dbnumber varchar(3)
	, @dim_dbprocessinfo_dbnamepatton varchar(255)
	
RAISERROR('Populating Tips Table', 0, 0) WITH NOWAIT

INSERT INTO @tips (sid_dbprocessinfo_dbnumber, dim_dbprocessinfo_dbnamepatton)

SELECT pp.sid_dbprocessinfo_dbnumber, dbpi.DBNamePatton 
FROM Rewardsnow.dbo.RNIProcessingParameter pp
INNER JOIN Rewardsnow.dbo.dbprocessinfo dbpi
	ON pp.sid_dbprocessinfo_dbnumber = dbpi.DBNumber
WHERE dim_rniprocessingparameter_key = 'KROGERBONUSPROGRAM'
	AND dim_rniprocessingparameter_active = 1
	AND dim_rniprocessingparameter_value = 'Y'

SET @tipcount = (select COUNT(*) from @tips)
WHILE @tipcount > 0
BEGIN
	set @myid = (SELECT TOP 1 myid from @tips)

	select @sid_dbprocessinfo_dbnumber = sid_dbprocessinfo_dbnumber, @dim_dbprocessinfo_dbnamepatton = dim_dbprocessinfo_dbnamepatton
	from @tips where @myid = myid
	
	RAISERROR('Checking tip %s for matches', 0, 0, @sid_dbprocessinfo_dbnumber) WITH NOWAIT

	set @sql = 'UPDATE b '
	+ 'SET dim_krogerbonuspreprocessing_tipnumber = k.TIPNUMBER '
	+ '	, sid_dbprocessinfo_dbnumber = k.TIPFIRST '
	+ 'FROM [<DBNAME>].dbo.customer k '
	+ 'inner join KrogerBonusPreProcessing b '
	+ 'on k.Misc2 = b.dim_krogerbonuspreprocessing_membernumber '
	+ 'WHERE b.dim_krogerbonuspreprocessing_tipnumber is null '
	+ '		and k.Misc2 is not null '
	
	set @sql = REPLACE(@sql, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
	
	EXEC sp_executesql @sql
	
	RAISERROR('Checking tip %s for deleted accounts', 0, 0, @sid_dbprocessinfo_dbnumber) WITH NOWAIT

	set @sql = 'UPDATE b '
	+ 'SET dim_krogerbonuspreprocessing_tipnumber = ''DELETED'' '
	+ ', sid_dbprocessinfo_dbnumber = k.TIPFIRST '
	+ 'FROM [<DBNAME>].dbo.CustomerDeleted k '
	+ 'inner join KrogerBonusPreProcessing b '
	+ 'on k.Misc2 = b.dim_krogerbonuspreprocessing_membernumber '
	+ 'WHERE b.dim_krogerbonuspreprocessing_tipnumber is null '
	+ 'and k.Misc2 is not null '

	set @sql = REPLACE(@sql, '<DBNAME>', @dim_dbprocessinfo_dbnamepatton)
	
	EXEC sp_executesql @sql

	DELETE FROM @tips where myid = @myid	

END

RAISERROR('Process Complete', 0, 0) WITH NOWAIT



