USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGetTipsWithCreditCard]    Script Date: 06/30/2010 10:45:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipsWithCreditCard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGetTipsWithCreditCard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipsWithCreditCard]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/10
-- Description:	Capture Tips that have a credit card
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPGetTipsWithCreditCard] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(150), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(2000)

	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

	set @SQLInsert=N''insert into Metavantework.dbo.[Paper_Statements] (Tipnumber)
						select distinct tipnumber 
						from '' + QuoteName(@DBName) + N''.dbo.affiliat 
						where left(accttype,1) = ''''C'''' ''
	exec sp_executesql @SQLInsert
			
END
' 
END
GO
