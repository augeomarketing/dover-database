USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_bonusprocessing]    Script Date: 02/02/2011 10:36:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_bonusprocessing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_bonusprocessing]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_bonusprocessing]    Script Date: 02/02/2011 10:36:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_bonusprocessing]
            @Tipfirst	   varchar(3),
            @DBName         nvarchar(50)

AS

declare @SQL                  nvarchar(max)


create table #transum
(
    [tipnumber] [varchar](15) NULL,
    [acctno] [varchar](16) NULL,
    [histdate] [varchar](10) NULL,
    [trancode] [varchar](2) NULL,
    [NumPurch] [int] NULL default(0),
    [AmtPurch] [int] NULL default(0),
    [NUMCR] [varchar](6) NULL,
    [AMTCR] [int] NULL default(0),
    [description] [varchar](50) NULL,
    [ratio] int NULL,
    [overage] [int] NULL default(0),
    [cardtype] [char](1) NULL,
    [SigvsPin] [char](1) NULL
) 



--
-- Does the FI have any calculated bonuses?  sid_bonusprogramtype_id = 'C'

insert into #transum
(tipnumber, acctno, histdate, trancode, NUMPURCH, AMTPURCH, NUMCR, AMTCR, description, ratio, overage, cardtype, SigvsPin)

select tipnumber, acctno, histdate, sid_trantype_trancode, 1, AMTPURCH * (dim_BonusProgramFI_PointMultiplier - 1), 1, amtcr * (dim_bonusprogramFI_PointMultiplier - 1), dim_bonusprogram_description, ratio, overage, cardtype, SigvsPin
from MetavanteWork.dbo.BonusProgramFI bpf join MetavanteWork.dbo.Transum ts
		 on bpf.sid_BonusProgramFI_Bin = LEFT(acctno, len(bpf.sid_bonusprogramfi_bin))

join metavantework.dbo.BonusProgram bp
   on bpf.sid_bonusprogram_id = bp.sid_bonusprogram_id

where   left(ts.tipnumber,3) = @tipfirst
    and	   bp.sid_bonusprogramtype_id = 'C'
    and	   ts.histdate >= dim_BonusProgramFI_Effectivedate
    and	   ts.histdate <= dim_bonusprogramfi_expirationdate 

--
-- Add other bonus calculations below

Update #transum set trancode = 'BX' where ratio = -1



--
-- Now that all bonus txns are added to #transum, add these rows into the actual transum table
insert into metavantework.dbo.transum
(tipnumber, acctno, histdate, trancode, NUMPURCH, AMTPURCH, NUMCR, AMTCR, description, ratio, overage, cardtype, SigvsPin)
select tipnumber, acctno, histdate, trancode, NUMPURCH, AMTPURCH, NUMCR, AMTCR, description, ratio, overage, cardtype, SigvsPin
from #transum

--Add Bonus rows to FI history

	set @SQL='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
       		select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
			from #transum 
               where amtpurch != 0'
	Exec sp_executesql @SQL


	set @SQL='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
       		select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
			from #transum 
               where amtcr != 0'
	Exec sp_executesql @SQL


GO


