USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pWelcomeKitA]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKitA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pWelcomeKitA]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKitA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pWelcomeKitA] @StartDate varchar(10), @EndDate varchar(10), @TipFirst nchar(3)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)

drop table tippurchases

set @sqlSelect=N''SELECT tipnumber, sum(points * ratio) as total 
	INTO tippurchases from '' + QuoteName(@DBName) + N'' .dbo.history where histdate >= @StartDate and histdate <= @EndDate
	group by tipnumber''
exec sp_executesql @SQLSelect, N''@Startdate nchar(10), @Enddate nchar(10)'', @Startdate=@Startdate, @Enddate=@Enddate

update tippurchases
set total=''0''
where total<''0''

update tippurchases
set total=''0''
where total is null

set @sqlTruncate=N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Welcomekit ''
exec sp_executesql @SQLTruncate

set @sqlInsert=N''insert into '' + QuoteName(@DBName) + N'' .dbo.Welcomekit SELECT a.TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5), b.total 
	FROM '' + QuoteName(@DBName) + N'' .dbo.customer a left outer join metavantework.dbo.tippurchases b on a.tipnumber=b.tipnumber
	WHERE (DATEADDED = @EndDate AND STATUS <> ''''c'''') ''
exec sp_executesql @SQLInsert, N''@Enddate nchar(10)'',@Enddate=@Enddate' 
END
GO
