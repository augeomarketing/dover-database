USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kPrepTranImportRaw]    Script Date: 11/15/2010 13:22:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kPrepTranImportRaw]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kPrepTranImportRaw]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kPrepTranImportRaw]    Script Date: 11/15/2010 13:22:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_b2kPrepTranImportRaw]
AS

	INSERT INTO b2kTranImportRawHistory
	(
		sid_b2ktranimportraw_id
		, dim_b2ktranimportraw_importdate
		, dim_b2ktranimportraw_filedate
		, dim_b2ktranimportraw_corpid
		, dim_b2ktranimportraw_agentinstitutionid
		, dim_b2ktranimportraw_accountnumber
		, dim_b2ktranimportraw_trandate
		, dim_b2ktranimportraw_transactionpostdate
		, dim_b2ktranimportraw_transactionsign
		, dim_b2ktranimportraw_transactionamount
		, dim_b2ktranimportraw_prescoredearnings
		, dim_b2ktranimportraw_merchantname
		, dim_b2ktranimportraw_merchantcity
		, dim_b2ktranimportraw_merchantstate
		, dim_b2ktranimportraw_merchantcountry
		, dim_b2ktranimportraw_mccsiccode
		, dim_b2ktranimportraw_industrycode
		, dim_b2ktranimportraw_transactioncode
		, dim_b2ktranimportraw_reasoncode
		, dim_b2ktranimportraw_transactioncurrencyidentifier
		, dim_b2ktranimportraw_pricingplanid
		, dim_b2ktranimportraw_transactiontype
		, dim_b2ktranimportraw_referencenumber
		, dim_b2ktranimportraw_eciindicator
		, dim_b2ktranimportraw_vendorindicator
		, dim_b2ktranimportraw_filler1
		, dim_b2ktranimportraw_merchantid
		, dim_b2ktranimportraw_filler2
	)
	SELECT
		sid_b2ktranimportraw_id
		, dim_b2ktranimportraw_importdate
		, dim_b2ktranimportraw_filedate
		, dim_b2ktranimportraw_corpid
		, dim_b2ktranimportraw_agentinstitutionid
		, dim_b2ktranimportraw_accountnumber
		, dim_b2ktranimportraw_trandate
		, dim_b2ktranimportraw_transactionpostdate
		, dim_b2ktranimportraw_transactionsign
		, dim_b2ktranimportraw_transactionamount
		, dim_b2ktranimportraw_prescoredearnings
		, dim_b2ktranimportraw_merchantname
		, dim_b2ktranimportraw_merchantcity
		, dim_b2ktranimportraw_merchantstate
		, dim_b2ktranimportraw_merchantcountry
		, dim_b2ktranimportraw_mccsiccode
		, dim_b2ktranimportraw_industrycode
		, dim_b2ktranimportraw_transactioncode
		, dim_b2ktranimportraw_reasoncode
		, dim_b2ktranimportraw_transactioncurrencyidentifier
		, dim_b2ktranimportraw_pricingplanid
		, dim_b2ktranimportraw_transactiontype
		, dim_b2ktranimportraw_referencenumber
		, dim_b2ktranimportraw_eciindicator
		, dim_b2ktranimportraw_vendorindicator
		, dim_b2ktranimportraw_filler1
		, dim_b2ktranimportraw_merchantid
		, dim_b2ktranimportraw_filler2
	FROM
		b2kTranImportRaw
		
	DELETE traw
	FROM b2kTranImportRaw traw
	INNER JOIN b2kTranImportRawHistory thist 
	ON traw.sid_b2ktranimportraw_id = thist.sid_b2ktranimportraw_id

GO


