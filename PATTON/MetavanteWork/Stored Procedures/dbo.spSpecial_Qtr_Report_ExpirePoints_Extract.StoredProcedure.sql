USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spSpecial_Qtr_Report_ExpirePoints_Extract]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSpecial_Qtr_Report_ExpirePoints_Extract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSpecial_Qtr_Report_ExpirePoints_Extract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSpecial_Qtr_Report_ExpirePoints_Extract]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2009
-- Description:	Extract points to expire for special report
-- =============================================
CREATE PROCEDURE [dbo].[spSpecial_Qtr_Report_ExpirePoints_Extract] 
	-- Add the parameters for the stored procedure here
	@TipFirst NCHAR (3), 
	@MonthEndDate NVARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	/********************************************************************************/
/*    This will Create the Expired Points Table for Monthly Processing          */
/*     This process will take the current monthenddate and subtract 3 years     */
/*     All points older than this date are subject to expiration                */
/*    %%%%%% Note This version of the procedure creates the expiring points     */
/*           table based on the current month end year minus 3 to expire points */
/*           this table is then used BY spProcessExpiredPoints to update the    */
/*           Customer and History Tables                                        */
/* BY:  B.QUINN                                                                 */
/* DATE: 3/2007                                                                 */
/* REVISION: 0                                                                  */
/*                                                                              */
/********************************************************************************/


declare @DateOfExpire NVARCHAR(10), @ExpireYear char(4)
set @ExpireYear=right( @MonthEndDate, 4)
SET @DateOfExpire = ''12/31/'' + @ExpireYear
Declare @PointsEarned int
Declare @Pointsredeemed int
Declare @PointsToExpire int
DECLARE @SQLUpdate nvarchar(1000) 
DECLARE @SQLDynamic nvarchar(1000)
declare @expirationdate nvarchar(25)
declare @intday int
declare @intmonth int
declare @intyear int
declare @ExpireDate DATETIME
declare @DBName varchar(100)
declare @NumRecs numeric
declare @SQLInsert nvarchar(1000)

	

set @ExpireDate = cast(@DateOfExpire as datetime)

SET @intYear = DATEPART(year, @ExpireDate)
SET @intmonth = DATEPART(month, @ExpireDate)
SET @intday = DATEPART(day, @ExpireDate)
set @intYear = @intYear - 3
set @expirationdate = (rtrim(@intYear) + ''-'' + rtrim(@intmonth) + ''-'' + rtrim(@intday) +   '' 23:59:59.997'')

set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	set @SQLUpdate = ''TRUNCATE TABLE '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints ''
	Exec sp_executesql @SQLUpdate	

	set @SQLUpdate = ''INSERT into '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints
						 SELECT tipnumber, sum(points * ratio) as addpoints,
						''''0'''' AS REDPOINTS, ''''0'''' AS POINTSTOEXPIRE, ''''0'''' as PREVEXPIRED, @ExpireDate as dateofexpire
						from '' + QuoteName(@DBName) + N''.dbo.history
						where histdate < @expirationdate and (trancode not like(''''R%'''') and
						trancode <> ''''IR''''and trancode not like(''''XP''''))
						group by tipnumber ''
	Exec sp_executesql @SQLUpdate, N''@expirationdate nvarchar(25), @ExpireDate DATETIME'', @expirationdate = @expirationdate, @ExpireDate =  @ExpireDate

	set @SQLUpdate = ''UPDATE '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints  
						SET PREVEXPIRED = (SELECT SUM(POINTS* RATIO) 
							 FROM '' + QuoteName(@DBName) + N''.dbo.HISTORY 
							 WHERE
  							 (trancode  =''''XP'''') 
	  						 AND TIPNUMBER = '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints.TIPNUMBER) 
						WHERE EXISTS  (SELECT *
							 FROM '' + QuoteName(@DBName) + N''.dbo.HISTORY 
							 WHERE
  							  trancode  =''''XP''''
	  						 AND TIPNUMBER = '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints.TIPNUMBER) ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate =''UPDATE '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints  
						SET REDPOINTS = (SELECT SUM(POINTS* RATIO) 
							 FROM '' + QuoteName(@DBName) + N''.dbo.HISTORY 
							 WHERE
  							 (trancode  like(''''R%'''') or
   							  trancode = ''''IR'''')  
	  						 AND TIPNUMBER = '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints.TIPNUMBER) 
						WHERE EXISTS  (SELECT *
							 FROM '' + QuoteName(@DBName) + N''.dbo.HISTORY 
							 WHERE
  							 (trancode  like(''''R%'''') or
   							  trancode = ''''IR'''')  
	  						 AND TIPNUMBER = '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints.TIPNUMBER) ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate =''UPDATE '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints  
						SET POINTSTOEXPIRE = (ADDPOINTS + REDPOINTS + PREVEXPIRED),
							dateofexpire = @ExpireDate ''
	Exec sp_executesql @SQLUpdate, N''@ExpireDate DATETIME'', @ExpireDate = @ExpireDate


	set @SQLUpdate =''UPDATE '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints  
						SET POINTSTOEXPIRE = ''''0''''
						WHERE
						POINTSTOEXPIRE IS NULL ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate =''UPDATE '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints  
						SET POINTSTOEXPIRE = ''''0''''
						WHERE
						POINTSTOEXPIRE < ''''0'''' ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate =''delete from '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints 	
						WHERE
						POINTSTOEXPIRE = ''''0'''' ''
	Exec sp_executesql @SQLUpdate

	update Special_QTR_Expired_Report
	set PointsToExpire=''0''

	set @SQLUpdate =''update Special_QTR_Expired_Report
						set PointsToExpire=b.pointstoexpire
						from Special_QTR_Expired_Report a, '' + QuoteName(@DBName) + N''.dbo.ExpiredPoints b
						where a.tipnumber=b.tipnumber ''
	Exec sp_executesql @SQLUpdate

END
' 
END
GO
