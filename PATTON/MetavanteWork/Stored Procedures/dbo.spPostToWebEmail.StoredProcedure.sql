USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spPostToWebEmail]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPostToWebEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPostToWebEmail]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPostToWebEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spPostToWebEmail] @Enddate char(10), @TipFirst char(3), @To varchar(200), @From varchar(200) 
as

Declare @DBName varchar(150), @Subject varchar(200), @Body varchar(200)

set @DBName=(SELECT rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @Subject = ''Metavante -- '' +  @DBName
set @Body = ''Monthly data for period ending '' + @Enddate +  '' has been posted to the web.  Please send out E-stmt notifications''

insert into maintenance.dbo.perlemail (dim_perlemail_subject, 
					dim_perlemail_body, 
					dim_perlemail_to, 
					dim_perlemail_from
					) 
values (@Subject, 	
	@Body, 
	@To, 
	@From 
	)' 
END
GO
