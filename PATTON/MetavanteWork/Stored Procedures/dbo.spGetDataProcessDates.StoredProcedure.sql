USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetDataProcessDates]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDataProcessDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetDataProcessDates]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetDataProcessDates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGetDataProcessDates] @BeginDate nchar(10) output, @EndDate nchar(10) output
AS

set @BeginDate=(select top 1 DataMonthBeginDate from ProcessingDates)
set @EndDate=(select top 1 DataMonthEndDate from ProcessingDates)' 
END
GO
