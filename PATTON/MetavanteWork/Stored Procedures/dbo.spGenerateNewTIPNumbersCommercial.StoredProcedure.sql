USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersCommercial]    Script Date: 05/07/2012 10:41:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersCommercial]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersCommercial]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersCommercial]    Script Date: 05/07/2012 10:41:06 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersCommercial] @tipfirst varchar(3)
AS

DECLARE @NewTip varchar(15),
		@LastTipUsed varchar(15), 
		@newTiplast bigint

--RETRIEVE LASTTIPNUMBERUSED VALUE FROM DBPROCESSINFO
EXEC	rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
SET		@newTiplast = CAST(RIGHT(@LastTipUsed, 12) AS BIGINT)

--ASSIGN TIPNUMBERS TO ALL NEW CARDS WITH HOUSEHOLDING USING OLDCC VALUES
UPDATE	a
SET		@newTiplast = (@newTiplast + 1),
		TIPNUMBER = @tipfirst + RewardsNow.dbo.ufn_Pad(@newTiplast, 'L', 12, '0')
FROM	MetavanteWork.dbo.CARDSIN a JOIN MetavanteWork.dbo.CARDSIN b ON a.ACCTNUM = b.OLDCC
WHERE	a.TIPNUMBER is null
	and a.OLDCC is null
	and	a.TIPFIRST = @tipfirst
	
UPDATE	a
SET		TIPNUMBER = b.TIPNUMBER
FROM	MetavanteWork.dbo.CARDSIN a JOIN MetavanteWork.dbo.CARDSIN b ON a.OLDCC = b.ACCTNUM
WHERE	a.TIPNUMBER is null and b.TIPNUMBER is not null

/*
--SECTION TO HOUSEHOLD CARDS USING SSN DATA
UPDATE	a
SET		TIPNUMBER = b.TIPNUMBER
FROM	MetavanteWork.dbo.CARDSIN a JOIN MetavanteWork.dbo.CARDSIN b ON a.SSN = b.SSN
WHERE	a.TIPNUMBER is null and b.TIPNUMBER is not null and a.SSN is not null and a.SSN <> ''

DECLARE	@tmp TABLE (SSN varchar(16), tipnumber varchar(15))

INSERT INTO	@tmp
(SSN)
SELECT DISTINCT SSN FROM MetavanteWork.dbo.CARDSIN WHERE TIPFIRST = @tipfirst and TIPNUMBER is null and SSN is not null and SSN <> ''

UPDATE	@tmp
SET		@newTiplast = (@newTiplast + 1),
		TIPNUMBER = @tipfirst + RewardsNow.dbo.ufn_Pad(@newTiplast, 'L', 12, '0')

UPDATE	a
SET		a.tipnumber = b.tipnumber
FROM	MetavanteWork.dbo.CARDSIN a JOIN @tmp b ON a.SSN = b.SSN
WHERE	a.TIPNUMBER is null
*/

--SET TIPNUMBER FOR ALL REMAINING CARDS
UPDATE	MetavanteWork.dbo.CARDSIN
SET		@newTiplast = (@newTiplast + 1),
		TIPNUMBER = @tipfirst + RewardsNow.dbo.ufn_Pad(@newTiplast, 'L', 12, '0')
WHERE	TIPNUMBER is null

--UPDATE LASTTIPUSED VALUES IN DBPROCESSINFO
SET		@NewTip = @tipfirst + RewardsNow.dbo.ufn_Pad(@newTiplast, 'L', 12, '0')
EXEC	RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip

GO


