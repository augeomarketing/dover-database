USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransactionData]    Script Date: 12/02/2010 09:58:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransactionData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransactionData]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spImportTransactionData]    Script Date: 12/02/2010 09:58:34 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spImportTransactionData] @TipFirst char(3), @StartDateParm char(10)
AS 

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 10/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* add code to deal with FBOP Heloc cards  */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 06/2009   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* add code to remove transaction records if tipnumber not in customer  */
declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLInsert nvarchar(1000), @SQLDelete nvarchar(1000)
declare @DBnumber char(3), @Revision smallint, @Begindate datetime, @Enddate datetime, @Startdate datetime, @SigFactor numeric(18,2), @PinFactor numeric(18,2), @WorkFactorSig numeric(18,2), @WorkFactorPin numeric(18,2)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @Startdate = convert(datetime, @Startdateparm + ' 00:00:00:001')  

truncate table transum

delete from cardsin where tipnumber is null

/*                                                                            */
/*  Process to get the factor for debit cards TABLE                           */
/*                                                                            */
declare debit_crsr cursor
for select DBNumber, Revision, Startdate, Enddate, Sigfactor, Pinfactor
from DebitFactor
where DBNumber=@TipFirst
order by Revision desc

/*                                                                            */
open debit_crsr
/*                                                                            */
fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @SigFactor, @PinFactor
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @Begindate = convert(datetime, @BeginDate + ' 00:00:00:001')    
		set @Enddate = convert(datetime, @Enddate + ' 23:59:59:990')    

		if @Revision='0'		
		Begin
			set @WorkFactorSig=@SigFactor
			set @WorkFactorPin=@PinFactor
			goto Fetch_Error
		End

		if @Startdate>@Enddate
		Begin
			set @WorkFactorSig=(select SigFactor from DebitFactor where DBNumber=@TipFirst and Revision='0')
			set @WorkFactorPin=(select PinFactor from DebitFactor where DBNumber=@TipFirst and Revision='0')
			goto Fetch_Error
		End

		if @Startdate>=@Begindate and @Startdate<=@Enddate
		Begin
			set @WorkFactorSig=@SigFactor
			set @WorkFactorPin=@PinFactor
			goto Fetch_Error
		End

		if @Startdate<@Begindate 
		Begin
			goto Next_Record
		End

Next_Record:
		fetch debit_crsr into @DBnumber, @Revision, @Begindate, @Enddate, @SigFactor, @PinFactor
	end

Fetch_Error:
close  debit_crsr
deallocate  debit_crsr


/*********************************************************************/
/*  Get transaction data into a standard format                      */
/*********************************************************************/
insert into transum (tipnumber, acctno, histdate, NUMPURCH, AMTPURCH, NUMCR, AMTCR, cardtype, SigvsPin)
select distinct tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin	
from cardsin
group by tipnumber, acctnum, period, NUMPURCH, PURCH, NUMRET, AMTRET, cardtype, SigvsPin

/*********************************************************************/
/*  start SEB002 Remove records that do not have a customer record   */
/*********************************************************************/
set @SQLDelete='Delete from dbo.transum
				where tipnumber not in (select tipnumber from ' + QuoteName(@DBName) + N'.dbo.customer) '
Exec sp_executesql @SQLDelete
/*********************************************************************/
/*  end SEB002 Remove records that do not have a customer record   */
/*********************************************************************/

/*********************************************************************/
/*  Process Credit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio='1', trancode='63', description='Credit Card Purchase', amtpurch=ROUND((AMTPURCH), 0)
where amtpurch>0 and cardtype='C'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
		from transum
		where trancode=''63'' '
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Credit Cards Returns                                     */
/*********************************************************************/
update transum
set ratio='-1', trancode='33', description='Credit Card Returns', amtcr=ROUND((AMTcr), 0)
where amtcr>0 and cardtype='C'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
		from transum
		where trancode=''33'' '
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Debit Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio='1', trancode='67', description='Debit Card Purchase SIG', amtpurch=round(((AMTPURCH/100)/@WorkfactorSig),0) 
where amtpurch>0 and cardtype='D' and (SigvsPin is null or len(SigvsPin)='0' or SigvsPin='S')

update transum
set ratio='1', trancode='67', description='Debit Card Purchase PIN', amtpurch=round(((AMTPURCH/100)/@WorkfactorPin),0) 
where amtpurch>0 and cardtype='D' and SigvsPin='P'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
		from transum
		where trancode=''67'' '
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process Debit Cards Returns                                      */
/*********************************************************************/
update transum
set ratio='-1', trancode='37', description='Debit Card Returns SIG', amtcr=round(((AMTCR/100)/@WorkfactorSig),0)
where amtcr>0 and cardtype='D' and (SigvsPin is null or len(SigvsPin)='0' or SigvsPin='S')

update transum
set ratio='-1', trancode='37', description='Debit Card Returns PIN', amtcr=round(((AMTCR/100)/@WorkfactorPin),0)
where amtcr>0 and cardtype='D' and SigvsPin='P'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
		from transum
		where trancode=''37'' '
Exec sp_executesql @SQLInsert


/***********************/
/*  start section SEB001  */
/***********************/

/*********************************************************************/
/*  Process HELOC Cards Purchases                                   */
/*********************************************************************/
update transum
set ratio='1', trancode='6H', description='HELOC Card Purchase', amtpurch=round((AMTPURCH/100),0) 
where amtpurch>0 and cardtype='H'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
        	select tipnumber, acctno, histdate, trancode, numpurch, amtpurch, description, '' '', ratio, overage
		from transum
		where trancode=''6H'' '
Exec sp_executesql @SQLInsert

/*********************************************************************/
/*  Process HELOC Cards Returns                                      */
/*********************************************************************/
update transum
set ratio='-1', trancode='3H', description='HELOC Card Returns', amtcr=round((AMTCR/100),0)
where amtcr>0 and cardtype='H'

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history
       	select tipnumber, acctno, histdate, trancode, numcr, amtcr, description, '' '', ratio, overage
		from transum
		where trancode=''3H'' '
Exec sp_executesql @SQLInsert

/***********************/
/*  end section SEB001  */
/***********************/

delete from transum
where trancode is null

--Process any Bonus Point awards (RBM 12/02/2010)
exec dbo.[usp_bonusprocessing] @tipfirst, @DBName

/*********************************************************************/
/*  Update Customer                                                  */
/*********************************************************************/
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.customer 
		set runavailable=runavailable + (select sum(amtpurch-amtcr) from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber), 
			runbalance=runbalance + (select sum(amtpurch-amtcr) from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber) 
		where exists(select * from transum where tipnumber = ' + QuoteName(@DBName) + N'.dbo.customer.tipnumber) '
Exec sp_executesql @SQLUpdate
GO


