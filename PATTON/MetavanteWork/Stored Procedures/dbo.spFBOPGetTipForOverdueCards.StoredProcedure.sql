USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGetTipForOverdueCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipForOverdueCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGetTipForOverdueCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipForOverdueCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	Get tipnumber for cards with overdue balances
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPGetTipForOverdueCards]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLSet nvarchar(2000), @SQLIf nvarchar(2000), @SQLCursor nvarchar(2000)

	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsNow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)
	
	set @sqlupdate=N''Update dbo.FBOP_Overdue_Flag
						 set tipnumber = (select tipnumber from '' + QuoteName(@DBName) + N'' .dbo.affiliat where acctid = dbo.FBOP_Overdue_Flag.acctid) 
						where acctid in (select acctid from '' + QuoteName(@DBName) + N'' .dbo.affiliat) ''
	exec sp_executesql @SQLUpdate

END
' 
END
GO
