USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGetTipESTMTBonus]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipESTMTBonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGetTipESTMTBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGetTipESTMTBonus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[spFBOPGetTipESTMTBonus] @TipFirst char(3), @Bank char(3)
as
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 10/2007   */
/* REVISION: 0 */
/* SCAN:  */
/* Script to get tipnumbers from affiliat table by either card# or ssn  */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateToDelete char(6), @dateclosed datetime, @newmonth nchar(2), @newyear nchar(4)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLUpdate=N''update  ESTMTBONUS
		set tipnumber=b.tipnumber 
		from ESTMTBONUS a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
		where a.tipnumber is null and [Tax ID Number] is not null and len([Tax ID Number])>''''1'''' and [Tax ID Number]<>''''000000000'''' and [Tax ID Number]=secid ''
exec sp_executesql @SQLUpdate' 
END
GO
