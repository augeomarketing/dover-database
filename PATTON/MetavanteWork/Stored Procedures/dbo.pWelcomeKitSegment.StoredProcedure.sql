USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pWelcomeKitSegment]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKitSegment]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pWelcomeKitSegment]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pWelcomeKitSegment]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pWelcomeKitSegment] @EndDate varchar(10), @TipFirst nchar(3)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.WelcomekitSegment ''
exec sp_executesql @SQLTruncate

set @sqlInsert=N''insert into '' + QuoteName(@DBName) + N'' .dbo.WelcomekitSegment SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5), SegmentCode FROM '' + QuoteName(@DBName) + N'' .dbo.customer WHERE (DATEADDED = @EndDate AND STATUS <> ''''c'''') ''
exec sp_executesql @SQLInsert, N''@Enddate nchar(10)'',@Enddate=@Enddate


---------------------------------
--
-- SEB001
--
---------------------------------
if @TipFirst = ''546''
Begin
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N'' .dbo.WelcomekitSegment
						set Type =
						CASE Type
							 WHEN ''''C'''' THEN ''''1''''
							 WHEN ''''D'''' THEN ''''2''''
						End ''
	exec sp_executesql @SQLUpdate
End
' 
END
GO
