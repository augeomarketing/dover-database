USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_LRC_FIS_ProcessOptOuts]    Script Date: 05/21/2012 15:13:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LRC_FIS_ProcessOptOuts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LRC_FIS_ProcessOptOuts]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_LRC_FIS_ProcessOptOuts]    Script Date: 05/21/2012 15:13:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_LRC_FIS_ProcessOptOuts]

AS

DECLARE		@TIPnumber	varchar(15)
DECLARE		@tiptable	table
		(tipnumber varchar(15), done bit not null default(0))

INSERT INTO	@tiptable (tipnumber)
SELECT		oor.dim_optoutrequest_tipnumber
FROM		[RN1].Rewardsnow.dbo.optoutrequest oor join RewardsNow.dbo.dbprocessinfo dbp 
		ON	LEFT(oor.dim_optoutrequest_tipnumber, 3) = dbp.DBNumber
WHERE		oor.dim_optoutrequest_tipnumber like '5%'
		and oor.dim_optoutrequest_active = 1 
		and oor.sid_action_id = 1 
		and dbp.DBAvailable = 'Y'
GROUP BY	oor.dim_optoutrequest_tipnumber


SET			@TIPnumber = (select top 1 tipnumber from @tiptable where done = 0)
WHILE		@TIPnumber is not null
	BEGIN
		EXEC	MetavanteWork.dbo.spOptOutControlEntry @TIPnumber

		UPDATE	@tiptable
		SET		done = 1
		WHERE	tipnumber = @tipnumber

		SET		@TIPnumber = (select top 1 tipnumber from @tiptable where done = 0)
	END

UPDATE		oor
SET			oor.dim_optoutrequest_active = 0
FROM		[RN1].Rewardsnow.dbo.optoutrequest oor INNER JOIN @tiptable tt 
			ON	oor.dim_optoutrequest_tipnumber = tt.tipnumber
WHERE		oor.dim_optoutrequest_active = 1 
		and oor.sid_action_id = 1 
		and tt.done = 1


GO


