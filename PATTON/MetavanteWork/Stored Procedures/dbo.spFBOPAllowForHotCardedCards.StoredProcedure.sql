USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPAllowForHotCardedCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPAllowForHotCardedCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPAllowForHotCardedCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPAllowForHotCardedCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPAllowForHotCardedCards] @Tipfirst char(3) 
AS 

declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
declare @DBName varchar(50), @SQLSet nvarchar(2000), @SQLIf nvarchar(2000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

declare Cardsin_crsr cursor
for select tipnumber, oldcc, ssn, lastname, ddanum, period, cardtype
from FBOPcardsin
where Cardtype=''D'' and status = ''C'' and tipnumber is not null and left(tipnumber,1)<>'' '' and oldcc is not null and left(oldcc,1)<>'' ''

open Cardsin_crsr
fetch Cardsin_crsr into @TIPNUMBER, @OLDCC, @SSN, @LastName, @DDANum, @Period, @CardType
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

/*  Set the ACCTTYPE                     */
	if @Cardtype=''C''
	Begin
		Set @Accttype=''Credit''
	end
	else
		if @Cardtype=''D''
		Begin
			set @Accttype=''Debit''
		end

/*    Set the Account Type description    */
	set @SQLSet=''set @accttypedesc=(select Accttypedesc 
				from '' + QuoteName(@DBName) + N''.dbo.accttype
        			where 	accttype=@Accttype) ''
	Exec sp_executesql @SQLSet, N''@accttypedesc nvarchar(50) output, @Accttype nchar(20)'', @accttypedesc=@accttypedesc output, @Accttype=@Accttype

	set @sqlif=''if (not exists (select acctid 
				from '' + QuoteName(@DBName) + N''.dbo.affiliat 
				where acctid= @OLDCC) and @OLDCC is not null and left(@OLDCC,1)<>'''' '''')
			begin
				-- Add to AFFILIAT TABLE
				INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SecID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
				values(@OLDCC, @tipnumber, @Accttype, cast(@Period as datetime), @SSN, ''''A'''', @accttypedesc, @lastname, ''''0'''', @DDANUM) 
			end ''
	Exec sp_executesql @SQLif, N''@OLDCC nvarchar(16), @TIPNUMBER nvarchar(15), @accttype char(20), @accttypedesc char(50), @Period nchar(14), @LASTNAME nvarchar(40), @SSN char(9), @DDANUM char(13)'', 
			@OLDCC=@OLDCC, @TIPNUMBER=@TIPNUMBER, @Period=@Period, @LASTNAME=@LASTNAME, @SSN=@SSN, @DDANUM=@DDANUM, @accttypedesc=@accttypedesc, @accttype=@accttype

goto Next_Record
Next_Record:
	fetch Cardsin_crsr into @TIPNUMBER, @OLDCC, @SSN, @LastName, @DDANum, @Period, @CardType

end

Fetch_Error:
close  Cardsin_crsr
deallocate  Cardsin_crsr' 
END
GO
