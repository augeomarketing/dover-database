USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spWipeWorkTable]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spWipeWorkTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spWipeWorkTable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spWipeWorkTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spWipeWorkTable] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table dbo.CARDNAME
	truncate table dbo.CARDNAMEWRK
	truncate table dbo.CARDSIN
	truncate table dbo.CARDSIN2
	truncate table dbo.CREDITCARDIN
	truncate table dbo.CreditIn
	truncate table dbo.Current_Month_Activity
	truncate table dbo.CUST3
	truncate table dbo.CUST535
	truncate table dbo.CUSTIN
	truncate table dbo.CUSTIN525
	truncate table dbo.ddachk
	truncate table dbo.ddachkwrk
	truncate table dbo.DDANAME
	truncate table dbo.debitcardin
	truncate table dbo.debitcardin2
	truncate table dbo.debitcardinBeneficial
	truncate table dbo.debitcardinhold
	truncate table dbo.DebitRollUp
	truncate table dbo.debitrollupa
	truncate table dbo.DirectDepositBonus
	truncate table dbo.DirectDepositBonuserror
	truncate table dbo.EmployeeBonus
	truncate table dbo.EmployeeIncentiveBonus
	truncate table dbo.expiredpoints
	truncate table dbo.expiringpoints
	truncate table dbo.FBOPAccountRollUp
	truncate table dbo.FBOPCARDNAME
	truncate table dbo.FBOPCARDSIN
	truncate table dbo.FBOPCARDSIN2
	truncate table dbo.FBOPCREDITCARDIN
	truncate table dbo.FBOPCUST3
	truncate table dbo.FBOPCUSTIN
	truncate table dbo.FBOPdebitcardin
	truncate table dbo.FBOPdebitcardin2
	truncate table dbo.FBOPDebitRollUp
	truncate table dbo.FBOPdebitrollupa
	truncate table dbo.FBOPManualCombine
	truncate table dbo.FBOPMissing
	truncate table dbo.FBOPnameprs
	truncate table dbo.FBOPTransum
	truncate table dbo.Harrisselfenrollsend
	truncate table dbo.HELOCBonus
	truncate table dbo.HELOCcardin
	truncate table dbo.HELOCcardin2
	truncate table dbo.New_Quarterly_Statement_Detail
	truncate table dbo.New_Quarterly_Statement_Detail_Negatives
	truncate table dbo.New_Quarterly_Statement_Detail_Out
	truncate table dbo.Quarterly_Audit_ErrorFile
	truncate table dbo.selfenroll
	truncate table dbo.selfenrollsend
	truncate table dbo.selfenrollwork
	truncate table dbo.sendemail
	truncate table dbo.TenCreditBonus
	truncate table dbo.TenCreditBonusErrors
	truncate table dbo.TenDebitsBonus
	truncate table dbo.TenDebitsBonuserror
	truncate table dbo.Transum
	truncate table dbo.TransumAdjustment
	truncate table dbo.Transumbonus
	truncate table dbo.Transumhold
	truncate table dbo.TS2CreditIn
	truncate table dbo.wrk3tab
	truncate table dbo.wrkbonusestmt
	truncate table dbo.wrkcustin
	truncate table dbo.wrktab
	truncate table dbo.wrktab1
	truncate table dbo.wrktab1n
	truncate table dbo.wrktab2
	truncate table dbo.wrktab22
	truncate table dbo.wrktab2FBOP
	truncate table dbo.wrktab3
	truncate table dbo.wrktab3aFBOP
	truncate table dbo.wrktab3FBOP
	truncate table dbo.wrktab4

END
' 
END
GO
