USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_CardsInProcess]    Script Date: 12/06/2012 08:46:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_CardsInProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_CardsInProcess]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_CardsInProcess]    Script Date: 12/06/2012 08:46:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE Procedure [dbo].[usp_FIS_CardsInProcess]  
  
AS    
  
/********************************************************************************/  
/*   Update names to the Cardsin Table                                          */  
/*   Set Joint flag with "S" if NA2 is blank otherwise make it "J"              */
/*   Remove Business account CodeString from Name2 (Leaves Name2 Blank          */  
/********************************************************************************/  
UPDATE CI  
SET NA1 = nms.acctname1
	, NA2 = CASE WHEN nms.acctname2 LIKE 'BL A %' THEN '' ELSE nms.acctname2 END
	, NA3 = nms.acctname3 
	, NA4 = nms.acctname4 
	, NA5 = nms.acctname5 
	, NA6 = nms.acctname6
	, JOINT = CASE WHEN ISNULL(nms.acctname2, '') <> '' THEN 'J' ELSE 'S' END 
FROM CARDSIN CI 
INNER JOIN ufn_FIS_CustomerNamesForTipfirst() nms
ON CI.TIPNUMBER = nms.TIPNUMBER      

 
/********************************************************************************/  
/*   Set Joint flag with "S" if NA2 is blank otherwise make it "J"              */  
/********************************************************************************/  
/*
UPDATE Cardsin  
SET  joint = 'J'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) > 0  
  
UPDATE Cardsin  
SET  joint = 'S'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) = 0  
*/
/********************************************************************************/  
/*   Remove Business account CodeString from Name2 (Leaves Name2 Blank          */  
/********************************************************************************/  
/*
UPDATE	CARDSIN
SET		NA2 = ''
WHERE	NA2 LIKE 'BL A %'
*/



/*

DECLARE @NA1 nvarchar(40),   
  @NA2 nvarchar(40),   
  @NA3 nvarchar(40),   
  @NA4 nvarchar(40),   
  @NA5 nvarchar(40),   
  @NA6 nvarchar(40),   
  @tipnumber nvarchar(15)  
  
DELETE FROM cardname  
  
DECLARE  Cardsin_View_by_Tipnumber_crsr CURSOR  
FOR SELECT tipnumber, NA1, NA2,NA3, NA4, NA5, NA6  
FROM  Cardsin_View_by_Tipnumber  
WHERE  status <> 'C'  
  
OPEN  Cardsin_View_by_Tipnumber_crsr  
FETCH  Cardsin_View_by_Tipnumber_crsr INTO @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6  
  
IF @@FETCH_STATUS = 1  
 GOTO Fetch_Error1  
/*                                                                            */  
WHILE @@FETCH_STATUS = 0  
 BEGIN   
  IF not exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber)  
   BEGIN  
    INSERT cardname(Tipnumber, NA1, NA2, NA3, NA4, NA5, NA6)   
    VALUES (@Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6)  
    GOTO Next_Record1  
   END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA2,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA2 = @NA1, NA3 = @NA2, NA4 = @NA3, NA5 = @NA4, NA6 = @NA5  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA3,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA3 = @NA1, NA4 = @NA2, NA5 = @NA3, NA6 = @NA4  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA4,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA4 = @NA1, NA5 = @NA2, NA6 = @NA3  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA5,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA5 = @NA1, NA6 = @NA2  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA6,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA6 = @NA1  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
    
    
     
 NEXT_RECORD1:  
 FETCH Cardsin_View_by_Tipnumber_crsr INTO @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6  
   
 END  
  
FETCH_ERROR1:  
CLOSE  Cardsin_View_by_Tipnumber_crsr  
DEALLOCATE Cardsin_View_by_Tipnumber_crsr  
  
/******************************************************/  
/* Section to remove duplicate names on same record   */  
/******************************************************/  
  
UPDATE Cardname SET na2 = null WHERE na2 = na1  
UPDATE Cardname SET na3 = null WHERE na3 = na1 or na3 = na2  
UPDATE Cardname SET na4 = null WHERE na4 = na1 or na4 = na2 or na4 = na3  
UPDATE Cardname SET na5 = null WHERE na5 = na1 or na5 = na2 or na5 = na3 or na5 = na4  
UPDATE Cardname SET na6 = null WHERE na6 = na1 or na6 = na2 or na6 = na3 or na6 = na4 or na6 = na5  
  
/******************************************************************************/  
/* Section to move names to the beginning of the name fields on same record   */  
/******************************************************************************/  
UPDATE Cardname  
SET  na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA5,'')))) = '0'   
  
UPDATE Cardname  
SET  na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA4,'')))) = '0'  
  
UPDATE Cardname  
SET  na3 = na4, na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA3,'')))) = '0'  
  
UPDATE Cardname  
SET  na2 = na3, na3 = na4, na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA2,'')))) = '0'  
  
UPDATE Cardname  
SET  na1 = na2, na2 = na3, na3 = na4, na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA1,'')))) = '0'  
  
/********************************************************************************/  
/*   Update names to the Cardsin Table                                     */  
/********************************************************************************/  
UPDATE CI  
SET  NA1 = CN.NA1,  
  NA2 = CN.NA2,  
  NA3 = CN.NA3,  
  NA4 = CN.NA4,  
  NA5 = CN.NA5,  
  NA6 = CN.NA6   
FROM CARDSIN CI JOIN CARDNAME CN ON CI.TIPNUMBER = CN.TIPNUMBER      
  
/********************************************************************************/  
/*   Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */  
/********************************************************************************/  
UPDATE Cardsin  
SET  joint = 'J'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) > 0  
  
UPDATE Cardsin  
SET  joint = 'S'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) = 0  

*/  
GO


USE MetavanteWork
GO

IF OBJECT_ID(N'usp_FIS_CardsInProcess') IS NOT NULL
	DROP PROCEDURE usp_FIS_CardsInProcess
GO

  
CREATE Procedure [dbo].[usp_FIS_CardsInProcess]  
  
AS    
  
/********************************************************************************/  
/*   Update names to the Cardsin Table                                     */  
/********************************************************************************/  
UPDATE CI  
SET NA1 = nms.acctname1
	, NA2 = nms.acctname2 
	, NA3 = nms.acctname3 
	, NA4 = nms.acctname4 
	, NA5 = nms.acctname5 
	, NA6 = nms.acctname6 
FROM CARDSIN CI 
INNER JOIN ufn_FIS_CustomerNamesForTipfirst() nms
ON CI.TIPNUMBER = nms.TIPNUMBER      
  
/********************************************************************************/  
/*   Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */  
/********************************************************************************/  
UPDATE Cardsin  
SET  joint = 'J'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) > 0  
  
UPDATE Cardsin  
SET  joint = 'S'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) = 0  



/*

DECLARE @NA1 nvarchar(40),   
  @NA2 nvarchar(40),   
  @NA3 nvarchar(40),   
  @NA4 nvarchar(40),   
  @NA5 nvarchar(40),   
  @NA6 nvarchar(40),   
  @tipnumber nvarchar(15)  
  
DELETE FROM cardname  
  
DECLARE  Cardsin_View_by_Tipnumber_crsr CURSOR  
FOR SELECT tipnumber, NA1, NA2,NA3, NA4, NA5, NA6  
FROM  Cardsin_View_by_Tipnumber  
WHERE  status <> 'C'  
  
OPEN  Cardsin_View_by_Tipnumber_crsr  
FETCH  Cardsin_View_by_Tipnumber_crsr INTO @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6  
  
IF @@FETCH_STATUS = 1  
 GOTO Fetch_Error1  
/*                                                                            */  
WHILE @@FETCH_STATUS = 0  
 BEGIN   
  IF not exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber)  
   BEGIN  
    INSERT cardname(Tipnumber, NA1, NA2, NA3, NA4, NA5, NA6)   
    VALUES (@Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6)  
    GOTO Next_Record1  
   END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA2,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA2 = @NA1, NA3 = @NA2, NA4 = @NA3, NA5 = @NA4, NA6 = @NA5  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA3,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA3 = @NA1, NA4 = @NA2, NA5 = @NA3, NA6 = @NA4  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA4,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA4 = @NA1, NA5 = @NA2, NA6 = @NA3  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA5,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA5 = @NA1, NA6 = @NA2  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
  ELSE  
   IF exists(SELECT * FROM cardname WHERE Tipnumber = @Tipnumber and (len(ltrim(rtrim(isnull(NA6,'')))) = 0))  
    BEGIN  
     UPDATE Cardname  
     SET  NA6 = @NA1  
     WHERE Tipnumber = @Tipnumber  
     GOTO Next_Record1  
    END  
    
    
     
 NEXT_RECORD1:  
 FETCH Cardsin_View_by_Tipnumber_crsr INTO @Tipnumber, @NA1, @NA2, @NA3, @NA4, @NA5, @NA6  
   
 END  
  
FETCH_ERROR1:  
CLOSE  Cardsin_View_by_Tipnumber_crsr  
DEALLOCATE Cardsin_View_by_Tipnumber_crsr  
  
/******************************************************/  
/* Section to remove duplicate names on same record   */  
/******************************************************/  
  
UPDATE Cardname SET na2 = null WHERE na2 = na1  
UPDATE Cardname SET na3 = null WHERE na3 = na1 or na3 = na2  
UPDATE Cardname SET na4 = null WHERE na4 = na1 or na4 = na2 or na4 = na3  
UPDATE Cardname SET na5 = null WHERE na5 = na1 or na5 = na2 or na5 = na3 or na5 = na4  
UPDATE Cardname SET na6 = null WHERE na6 = na1 or na6 = na2 or na6 = na3 or na6 = na4 or na6 = na5  
  
/******************************************************************************/  
/* Section to move names to the beginning of the name fields on same record   */  
/******************************************************************************/  
UPDATE Cardname  
SET  na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA5,'')))) = '0'   
  
UPDATE Cardname  
SET  na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA4,'')))) = '0'  
  
UPDATE Cardname  
SET  na3 = na4, na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA3,'')))) = '0'  
  
UPDATE Cardname  
SET  na2 = na3, na3 = na4, na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA2,'')))) = '0'  
  
UPDATE Cardname  
SET  na1 = na2, na2 = na3, na3 = na4, na4 = na5, na5 = na6, na6 = null  
WHERE len(ltrim(rtrim(isnull(NA1,'')))) = '0'  
  
/********************************************************************************/  
/*   Update names to the Cardsin Table                                     */  
/********************************************************************************/  
UPDATE CI  
SET  NA1 = CN.NA1,  
  NA2 = CN.NA2,  
  NA3 = CN.NA3,  
  NA4 = CN.NA4,  
  NA5 = CN.NA5,  
  NA6 = CN.NA6   
FROM CARDSIN CI JOIN CARDNAME CN ON CI.TIPNUMBER = CN.TIPNUMBER      
  
/********************************************************************************/  
/*   Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */  
/********************************************************************************/  
UPDATE Cardsin  
SET  joint = 'J'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) > 0  
  
UPDATE Cardsin  
SET  joint = 'S'  
WHERE len(ltrim(rtrim(isnull(na2,'')))) = 0  

*/  