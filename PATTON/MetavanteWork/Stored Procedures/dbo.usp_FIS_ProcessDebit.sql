USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_ProcessDebit]    Script Date: 03/13/2012 10:52:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_ProcessDebit]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_ProcessDebit]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_ProcessDebit]    Script Date: 03/13/2012 10:52:44 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[usp_FIS_ProcessDebit] 

AS

DELETE FROM	DebitCardIn
WHERE		ssn = '987654321'

/*  ****Deletion not used in Commercial FIs (526,528,544,556)****   */
IF	(SELECT DISTINCT tipfirst FROM debitcardin) not in ('526','528','544','556')
	BEGIN
		DELETE FROM	DebitCardIn
		WHERE DDANUM is Null
	END

UPDATE	DebitCardIn
SET		addr1 = rtrim(addr1) + rtrim(isnull(Addr1Ext,'')), 
		addr2 = rtrim(addr2) + rtrim(isnull(Addr2Ext,'')),
		na1 = ltrim(rtrim(na1)),
		TIPMID = '0000000'

UPDATE	DebitCardIn
SET		DebitCardIn.Trancode = TrancodeXref.trancodeout 
FROM	DebitCardIn, TrancodeXref 
WHERE	DebitCardIn.trancode = TrancodeXref.trancodeIn 

UPDATE	DebitCardIn	
SET		numpurch = '1', 
		purch = points
WHERE	trancode = '67'

UPDATE	DebitCardIn	
SET		numret = '1',
		amtret = points
WHERE	trancode = '37'

UPDATE	DebitCardIn
SET		ddanum = savnum
WHERE	ddanum = '00000000000'

UPDATE	DebitCardIn
SET		[STATUS] = 'C'
WHERE	[STATUS] = 'B'

/* rearrange name from  Last, First to First Last */
UPDATE	DebitCardIn
SET		NA1 = rtrim(substring(na1,charindex(',',na1)+2,len(rtrim(na1)))) + ' ' + substring(na1,1,charindex(',',na1)-1)
WHERE	substring(na1,1,1) not like ' ' and na1 is not null and NA1 LIKE '%,%'

/*  ****Deletion not used in Commercial FIs (526,528,544,556)****   */
IF	(SELECT DISTINCT tipfirst FROM debitcardin) not in ('526','528','544','556')
	BEGIN
		DELETE FROM	DebitCardIn
		WHERE		DDANUM='00000000000'
	END

/******************************************************/
/* Section to get all names on same record            */
/******************************************************/

/*  ****Following section not used in Commercial FIs (526,528,544,556) -- Also not used in (532)****   */
IF	(SELECT DISTINCT tipfirst FROM debitcardin) not in ('526','528', '532','544','556')
	BEGIN
		DECLARE	@dda	varchar(25)

		DECLARE	csrDDA CURSOR	FAST_FORWARD FOR
			SELECT	distinct ddanum
			FROM	dbo.DebitCardIn

		OPEN	csrDDA

		FETCH NEXT	FROM	csrDDA
					INTO	@dda

		WHILE	@@FETCH_STATUS = 0
		BEGIN
			UPDATE	imp
			SET		NA1 = tmp.name1,
					NA2 = tmp.name2,
					NA3 = tmp.name3,
					NA4 = tmp.name4,
					NA5 = tmp.name5,
					NA6 = tmp.name6
					--SSN = tmp.SSN
			FROM	[dbo].[fnLinkCheckingNames] (@dda) tmp join dbo.debitcardin imp
				ON	tmp.DDA = imp.ddanum 
			WHERE	tmp.Name1 is not null	
				--where imp.status<>'C' /* SEB004 */

		FETCH NEXT	FROM	csrDDA
					INTO	@dda
		END

		CLOSE		csrDDA

		DEALLOCATE	csrDDA
	END


IF	(SELECT DISTINCT tipfirst FROM debitcardin) in ('526','546')
	BEGIN
		UPDATE	debitcardin
		SET		NA1 = NA2
		WHERE	NA2 is not null and len(NA2) > '0'
	END

UPDATE	debitcardin
SET		na2 = null
WHERE	na2 = na1

/********************************************************************************/
/*  	Begin logic to roll up multiple records into one by Account number      */
/********************************************************************************/
DELETE FROM	DebitRollUp
DELETE FROM	DebitRollUpa

INSERT INTO	DebitRollUp 
SELECT		acctnum, sum(cast (NUMPURCH as int)) as numpurch, SUM(PURCH) as amtpurch, sum(cast (NUMret as int)) as numcr, SUM(AMTret) as amtcr, SigvsPin 
FROM		debitcardin
GROUP BY	acctnum, SigvsPin
ORDER BY	acctnum, SigvsPin
/********************************************************************************/
/* New code to replace cursor processing for rolling acct demographic data		*/
/********************************************************************************/
DELETE FROM AccountRollUp

INSERT INTO	AccountRollUp 
SELECT DISTINCT
		TIPFIRST,	TIPMID,		TIPLAST,	TIPNUMBER,	TYPE,		SSN,		DDANUM,	SAVNUM,	BANK,		CLIENT,
		ACCTNUM,	OLDCC,		NA1,		LASTNAME,	PERIOD,		NA2,		NA3,	NA4,	NA5,		NA6,
		ADDR1,		ADDR2,		ADDR3,		CITYSTATE,	ZIP,		STATUS,		PHONE1,	PHONE2,	TRANCODE,	POINTS,
		SigvsPin
FROM		debitcardin
ORDER BY	acctnum

DELETE FROM	debitcardin2

INSERT INTO	debitcardin2
	(
		TIPFIRST,	TIPMID,		TIPLAST,	TIPNUMBER,	TYPE,		SSN,		DDANUM,	SAVNUM,	BANK,		CLIENT,
		ACCTNUM,	OLDCC,		NA1,		LASTNAME,	PERIOD,		NA2,		NA3,	NA4,	NA5,		NA6,
		ADDR1,		ADDR2,		ADDR3,		CITYSTATE,	ZIP,		STATUS,		PHONE1,	PHONE2,	NUMPURCH,	PURCH,
		NUMRET,		AMTRET,		TRANCODE,	POINTS,		SigvsPin
	)
SELECT
		TIPFIRST,	TIPMID,		TIPLAST,	TIPNUMBER,	TYPE,		SSN,		DDANUM,	SAVNUM,	BANK,		CLIENT,
		a.ACCTNUM,	OLDCC,		NA1,		LASTNAME,	PERIOD,		NA2,		NA3,	NA4,	NA5,		NA6,
		ADDR1,		ADDR2,		ADDR3,		CITYSTATE,	ZIP,		[STATUS],	PHONE1,	PHONE2,	b.NUMPURCH,	b.amtPURCH,
		b.NUMcr,	b.AMTcr,	TRANCODE,	POINTS,		a.SigvsPin	        	
FROM	AccountRollUp a JOIN DebitRollUp b
	ON	a.acctnum=b.acctnum and a.SigvsPin=b.SigvsPin
/********************************************************************************/
/*  End logic to roll up multiple records into one by NA2						*/
/********************************************************************************/

/********************************************************************************/
/*  	Set Joint flag with "S" if NA2 is blank otherwise make it "J"           */
/********************************************************************************/
UPDATE	debitcardin2
SET		joint = 'J'
WHERE	NA2 is not null and substring(na2,1,1) not like ' '

UPDATE	debitcardin2
SET		joint='S'
WHERE	NA2 is null or substring(na2,1,1) like ' '



GO


