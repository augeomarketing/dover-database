USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGenerateTIPNumbersEmployee]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGenerateTIPNumbersEmployee]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGenerateTIPNumbersEmployee]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGenerateTIPNumbersEmployee]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to employ last tip used instead of max tip
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2008   */
/* REVISION: 2 */
/* SCAN: SEB0012*/
/* CHANGE:  added logic to add tip numbers generated to temp table and then check temp table on next pass thru to catch    */
/*          multiple records for a new person when that person has not yet been set up */
CREATE PROCEDURE [dbo].[spFBOPGenerateTIPNumbersEmployee] @tipfirst char(3), @Enddate char(10), @BankIn char(3)
AS 

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)
declare @Tipnumber char(15), @Banknbr char(3), @Name char (40), @SSN char(9), @LastName char(40), @accnt char(16)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/****************       Start SEB001    ******************/
--set @SQLSelect=''SELECT @newnum = max(TIPNUMBER)
--		from '' + QuoteName(@DBName) + N''.dbo.affiliat 
--		where left(tipnumber,3)=@TipFirst ''
--Exec sp_executesql @SQLSelect, N''@newnum bigint output, @Tipfirst char(3)'', @newnum=@newnum output, @TipFirst=@Tipfirst

--if @newnum is null
--	begin
--	set @newnum=@TipFirst + ''000000000000''
--	end
--set @newnum = @newnum + 1

/***********************************/
/*  START SEB002                   */
/***********************************/
CREATE TABLE #CHKSSN (
			CHKTIP CHAR(15),
			CHKSSN char(9) 
			)
/***********************************/
/* END SEB002                      */
/***********************************/


declare @LastTipUsed char(15)

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @Newnum = cast(@LastTipUsed as bigint) + 1  
/*********  End SEB001  *************************/

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber, banknbr, [officer name], ssn, lastname
from EmployeeIncentivebonus
where banknbr=@BankIn and tipnumber is null
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @Banknbr, @Name, @SSN, @LastName
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	  /***********************************/
	  /* STRAT SEB002                    */
	  /***********************************/
	  IF EXISTS (SELECT * FROM #CHKSSN WHERE CHKSSN=@SSN)
	  BEGIN
		set @newnum = @newnum - 1 
		GOTO SAMEREC
	  END
	  /***********************************/
	  /* END SEB002                      */
	  /***********************************/
	 	
		set @NewTip = Cast(@Newnum as char)
		set @NewTip = replicate(''0'',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip   /* Added 11/29/2006 */
		
		set @SQLInsert=''		
				INSERT INTO '' + QuoteName(@DBName) + N''.dbo.customer 
				(	TIPNumber
					, Runavailable
					, Runbalance
					, Runredeemed
					, TIPFirst
					, TIPLast
					, AcctName1
					, AcctName2	
					, AcctName3
					, AcctName4
					, AcctName5
					, AcctName6
					, Address1
					, Address2
					, Address4
					, City
					, State
					, Zipcode
					, Status
					, StatusDescription
					, HomePhone
					, WorkPhone
					, DateAdded
					, Lastname
					, Misc1
					, Misc2
					, Misc3
					, SegmentCode
				)
				Values ( 
					@NewTip
					, ''''0''''
					, ''''0''''
					, ''''0''''
					, LEFT(@NewTip,3)
					, right(@NewTip,12)
					, @Name
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, ''''A''''
					, '''' ''''
					, '''' ''''
					, '''' ''''
					, cast(@Enddate as datetime)
					, @LastName
					, @SSN
					, '''' ''''
					, '''' ''''
					, '''' '''') '' 
				exec sp_executesql @SQLInsert, N'' @NewTip nvarchar(15)
								, @Name nvarchar(40)
								, @LASTNAME nvarchar(40)
								, @Enddate nvarchar(10)
								, @SSN char(9)''
								, @NewTip = @NewTip
								, @Name = @Name
								, @LASTNAME = @LASTNAME
								, @Enddate = @Enddate 
								, @SSN = @SSN 

/***********************************/
/* START SEB002                    */
/***********************************/
		INSERT INTO #CHKSSN(CHKTIP, CHKSSN)
		VALUES (@NEWTIP,@SSN)
/***********************************/
/* END SEB002                      */
/***********************************/

set @accnt=left(@ssn,9) + '' ''+''123456''

		set @SQLInsert=''	
				INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat
				(
					ACCTID
					, TIPNUMBER
					, AcctType
					, DATEADDED
					, SecID
					, AcctStatus
					, AcctTypeDesc
					, LastName
					, YTDEarned
					, CustID
				)
				VALUES (
					@accnt
					, @NewTip
					, ''''Dummy''''
					, cast(@Enddate as datetime)
					, @SSN
					, ''''A''''
					, ''''Dummy ''''
					, @lastname
					, ''''0''''
					, '''' ''''
				) ''
				exec sp_executesql @SQLInsert, N'' @NewTip nvarchar(15)
								, @accnt char(16)
								, @LASTNAME nvarchar(40)
								, @Enddate nvarchar(10)
								, @SSN char(9)	''
								, @NewTip = @NewTip
								, @accnt = @accnt
								, @LASTNAME = @LASTNAME
								, @SSN = @SSN
								, @Enddate = @Enddate 
						
    SAMEREC:  /* SEB002 */
		update EmployeeIncentivebonus	
		set tipnumber = @Newtip 
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr into @Tipnumber, @Banknbr, @Name, @SSN, @LastName

	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

exec RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip  /*SEB001 */' 
END
GO
