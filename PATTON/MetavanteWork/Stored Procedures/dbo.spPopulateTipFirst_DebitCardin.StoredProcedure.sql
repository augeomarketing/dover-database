USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spPopulateTipFirst_DebitCardin]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPopulateTipFirst_DebitCardin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPopulateTipFirst_DebitCardin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPopulateTipFirst_DebitCardin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S.Blanchette
-- Create date: 6/11/2009
-- Description:	to populate tip first in debitcardin using lookup to table 
-- =============================================
CREATE PROCEDURE [dbo].[spPopulateTipFirst_DebitCardin]
	-- Add the parameters for the stored procedure here
	@TipPrefix char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	If @TipPrefix not in (select rtrim(TipFirst) from dbo.DebitBins)
	Begin
		Update dbo.DebitCardin
		set TipFirst = @TipPrefix
	End
	Else 
		update dbo.DebitCardin
		set TipFirst = (Select distinct TipFirst from dbo.DebitBins where rtrim(BIN) = dbo.DebitCardin.client and tipFirst=@TipPrefix)

	delete from dbo.DebitCardin
	where TipFirst is null

END
' 
END
GO
