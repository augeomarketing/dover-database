USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kPrepDemogImportRawMidMonth]    Script Date: 01/19/2011 15:37:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kPrepDemogImportRawMidMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kPrepDemogImportRawMidMonth]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kPrepDemogImportRawMidMonth]    Script Date: 01/19/2011 15:37:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_b2kPrepDemogImportRawMidMonth]
AS
	TRUNCATE TABLE b2kDemogImportRawMidMonth
	TRUNCATE TABLE b2kDemogMidMonth
	
	INSERT INTO b2kDemogMidMonth
	(
		dim_b2kdemog_accounttype
		, dim_b2kdemog_addressline1
		, dim_b2kdemog_addressline2
		, dim_b2kdemog_addressline3
		, dim_b2kdemog_addressline4
		, dim_b2kdemog_agent
		, dim_b2kdemog_agentinstitutionid
		, dim_b2kdemog_associationid
		, dim_b2kdemog_bank
		, dim_b2kdemog_billcode
		, dim_b2kdemog_binplan
		, dim_b2kdemog_cardnumber
		, dim_b2kdemog_cardtype
		, dim_b2kdemog_city
		, dim_b2kdemog_corpid
		, dim_b2kdemog_country
		, dim_b2kdemog_created
		, dim_b2kdemog_emailaddress
		, dim_b2kdemog_filedate
		, dim_b2kdemog_homephonenumber
		, dim_b2kdemog_hostaccountstatus
		, dim_b2kdemog_hostacctstatusdate
		, dim_b2kdemog_importdate
		, dim_b2kdemog_lastmodified
		, dim_b2kdemog_newcardnumber
		, dim_b2kdemog_oldcardnumber
		, dim_b2kdemog_primaryfirstname
		, dim_b2kdemog_primarylastname
		, dim_b2kdemog_primarymiddleinitial
		, dim_b2kdemog_primarysocialsecuritynumber
		, dim_b2kdemog_productline
		, dim_b2kdemog_relationshipaccount
		, dim_b2kdemog_rnstatus
		, dim_b2kdemog_scenrollmentdate
		, dim_b2kdemog_scparticipationflag
		, dim_b2kdemog_scprogramstatus
		, dim_b2kdemog_scprogramstatuschangedate
		, dim_b2kdemog_secondaryfirstname
		, dim_b2kdemog_secondarylastname
		, dim_b2kdemog_secondarymiddleinitial
		, dim_b2kdemog_state
		, dim_b2kdemog_subproducttype
		, dim_b2kdemog_workphonenumber
		, dim_b2kdemog_zipcode
	)
	SELECT
		dim_b2kdemog_accounttype
		, dim_b2kdemog_addressline1
		, dim_b2kdemog_addressline2
		, dim_b2kdemog_addressline3
		, dim_b2kdemog_addressline4
		, dim_b2kdemog_agent
		, dim_b2kdemog_agentinstitutionid
		, dim_b2kdemog_associationid
		, dim_b2kdemog_bank
		, dim_b2kdemog_billcode
		, dim_b2kdemog_binplan
		, dim_b2kdemog_cardnumber
		, dim_b2kdemog_cardtype
		, dim_b2kdemog_city
		, dim_b2kdemog_corpid
		, dim_b2kdemog_country
		, dim_b2kdemog_created
		, dim_b2kdemog_emailaddress
		, dim_b2kdemog_filedate
		, dim_b2kdemog_homephonenumber
		, dim_b2kdemog_hostaccountstatus
		, dim_b2kdemog_hostacctstatusdate
		, dim_b2kdemog_importdate
		, dim_b2kdemog_lastmodified
		, dim_b2kdemog_newcardnumber
		, dim_b2kdemog_oldcardnumber
		, dim_b2kdemog_primaryfirstname
		, dim_b2kdemog_primarylastname
		, dim_b2kdemog_primarymiddleinitial
		, dim_b2kdemog_primarysocialsecuritynumber
		, dim_b2kdemog_productline
		, dim_b2kdemog_relationshipaccount
		, dim_b2kdemog_rnstatus
		, dim_b2kdemog_scenrollmentdate
		, dim_b2kdemog_scparticipationflag
		, dim_b2kdemog_scprogramstatus
		, dim_b2kdemog_scprogramstatuschangedate
		, dim_b2kdemog_secondaryfirstname
		, dim_b2kdemog_secondarylastname
		, dim_b2kdemog_secondarymiddleinitial
		, dim_b2kdemog_state
		, dim_b2kdemog_subproducttype
		, dim_b2kdemog_workphonenumber
		, dim_b2kdemog_zipcode
	FROM b2kDemog

GO

