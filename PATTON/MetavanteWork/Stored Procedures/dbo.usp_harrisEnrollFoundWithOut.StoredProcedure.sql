USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_harrisEnrollFoundWithOut]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_harrisEnrollFoundWithOut]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_harrisEnrollFoundWithOut]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_harrisEnrollFoundWithOut]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_harrisEnrollFoundWithOut] 
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@tipnumber varchar(15),
	@address1 varchar(50),
	@address2 varchar(50),
	@citystatezip varchar(100),
	@email varchar(200),
	@match varchar(1),
	@bodyout varchar(4096) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	 If @match = ''1''
	  Begin
	   set @bodyout= (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(dim_harrisemail_enrolled, ''<<< name >>>'',@name), ''<<< tipnumber >>>'',@tipnumber), ''<<< address1 >>>'',@address1), ''<<< address2 >>>'',@address2), ''<<< csz >>>'',@citystatezip), ''<<< email >>>'',@email), '''','''')
	   FROM metavantework.dbo.harrisemail)
	  End
	 Else
	  Begin
	   set @bodyout= (SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(dim_harrisemail_nomatch, ''<<< name >>>'',@name), ''<<< tipnumber >>>'',@tipnumber), ''<<< address1 >>>'',@address1), ''<<< address2 >>>'',@address2), ''<<< csz >>>'',@citystatezip), ''<<< email >>>'',@email), '''','''')
	   FROM metavantework.dbo.harrisemail)
	  End  
	

END
' 
END
GO
