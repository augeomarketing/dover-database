USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerGet2pt]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGet2pt]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerGet2pt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGet2pt]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerGet2pt]
	-- Add the parameters for the stored procedure here
	@Errorcount int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @error int
	
	Truncate table dbo.Kroeger2pt
	
	insert into dbo.Kroeger2pt	(LoyaltyNumber, Amount, Sign, DateIn)
	select substring(col001,3,16), cast(substring(Col001,20,14) as float) , substring(Col001,34,1), substring(Col001,35,7) 
	from dbo.KroegerGenericIn
	where left(col001,2)=''02''
	
	set @Errorcount=(select substring(col001,15,10) from dbo.KroegerGenericIn where left(col001,2)=''09'') - 2 - (select count(*) from dbo.Kroeger2pt)
	
END
' 
END
GO
