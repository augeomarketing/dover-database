USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPLoadCardsinWithHELOCCards]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadCardsinWithHELOCCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPLoadCardsinWithHELOCCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPLoadCardsinWithHELOCCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPLoadCardsinWithHELOCCards]
as

insert into FBOPCardsin(tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, Cardtype)
select tipfirst, tipmid, joint, ssn, ddanum, acctnum, oldcc, na1, na2, na3, na4, na5, na6, addr1, addr2, addr3, citystate, zip, status, phone1, phone2, numpurch, purch, numret, amtret, period, ''H'' from HELOCCardin2
order by ssn asc, ddanum asc' 
END
GO
