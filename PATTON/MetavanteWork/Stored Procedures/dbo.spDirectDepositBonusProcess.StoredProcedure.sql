USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spDirectDepositBonusProcess]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDirectDepositBonusProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDirectDepositBonusProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDirectDepositBonusProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spDirectDepositBonusProcess] @TipFirst char(3), @DateIn varchar(10)
AS

declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName nchar(100)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/

/*********************************************************/
/* By S. Blanchette                                      */
/* Date 6/08                                             */
/* SCAN SEB001                                           */
/* Reason  Add new trancode for Business                  */
/*********************************************************/

delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, left([Card #],16), Points
from DirectDepositBonus
where tipnumber like @TipFirst+''%''

update transumbonus
set ratio=''1'', trancode=''FD'', description=''Direct Deposit Bonus'', histdate=@datein, numdebit=''1'', overage=''0''

/****************/
/* START SEB001 */
/****************/
update transumbonus 
set trancode=''F7''
where left(acctno,10) in (''4521039500'', ''4521038500'', ''4521032001'', ''4521033501'')
/****************/
/* END SEB001 */
/****************/

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '''' '''', ratio, overage
			from transumbonus''
Exec sp_executesql @SQLInsert  
 
set @SQLUpdate=''update '' + QuoteName(@DBName) + N''.dbo.customer
	set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)
	where exists(select tipnumber from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)''
Exec sp_executesql @SQLupdate' 
END
GO
