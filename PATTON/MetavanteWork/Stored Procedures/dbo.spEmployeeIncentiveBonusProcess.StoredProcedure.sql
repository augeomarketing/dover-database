USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spEmployeeIncentiveBonusProcess]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEmployeeIncentiveBonusProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spEmployeeIncentiveBonusProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEmployeeIncentiveBonusProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spEmployeeIncentiveBonusProcess] @TipFirst char(3), @DateIn varchar(10)
AS

declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName nchar(100)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, trancode, amtdebit)
select tipnumber, typecard, SumofPoints
from EmployeeIncentiveBonus
where tipnumber like @TipFirst+''%''

update transumbonus
set ratio=''1'', histdate=@datein, numdebit=''1'', overage=''0''

update transumbonus
set trancode=''FE'', description=''Employee Incentive Debit''
where trancode<>''C '' or trancode is null

update transumbonus
set trancode=''FF'',description=''Employee Incentive Credit''
where trancode=''C '' 

set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '''' '''', ratio, overage
			from transumbonus''
Exec sp_executesql @SQLInsert  
 
set @SQLUpdate=''update '' + QuoteName(@DBName) + N''.dbo.customer
	set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)
	where exists(select tipnumber from transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)''
Exec sp_executesql @SQLupdate' 
END
GO
