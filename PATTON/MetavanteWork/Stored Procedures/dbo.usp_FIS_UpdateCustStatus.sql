USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_UpdateCustStatus]    Script Date: 01/26/2012 14:10:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_UpdateCustStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_UpdateCustStatus]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_UpdateCustStatus]    Script Date: 01/26/2012 14:10:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_FIS_UpdateCustStatus] 
	@TipFirst varchar(3)

AS 

DECLARE	@SQL nvarchar(max)

SET	@SQL = N'
	Update	c 
	SET		status = ''C'', 
			statusdescription = ''Closed[C]''
	FROM	[<<DBNAME>>].dbo.CUSTOMER c
			LEFT OUTER JOIN
			(SELECT	TIPNUMBER FROM [<<DBNAME>>].dbo.AFFILIAT WHERE acctstatus = ''A'') a
			ON  c.tipnumber = a.tipnumber
	WHERE	a.tipnumber is null
		and c.status not in (''X'')
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL

SET	@SQL = N'
	UPDATE	c 
	SET		status = ''A'',  
			statusdescription = ''Active[A]'' 
	FROM	[<<DBNAME>>].dbo.CUSTOMER c
			JOIN
			(SELECT	TIPNUMBER FROM [<<DBNAME>>].dbo.AFFILIAT WHERE acctstatus = ''A'') a
			ON  c.tipnumber = a.tipnumber
		and c.status not in (''X'')
	'
SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
EXEC	sp_executesql @SQL
GO


