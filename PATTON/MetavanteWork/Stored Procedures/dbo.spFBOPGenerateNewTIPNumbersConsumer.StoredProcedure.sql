USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPGenerateNewTIPNumbersConsumer]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGenerateNewTIPNumbersConsumer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPGenerateNewTIPNumbersConsumer]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPGenerateNewTIPNumbersConsumer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to employ last tip used instead of max tip
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 3/2008   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* Changed logic to check for ssn all 0s */
CREATE PROCEDURE [dbo].[spFBOPGenerateNewTIPNumbersConsumer] @tipfirst char(3)
AS 

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

/****************       Start SEB001    ******************/
--set @SQLSelect=''SELECT @newnum = max(TIPNUMBER)
--		from '' + QuoteName(@DBName) + N''.dbo.affiliat 
--		where left(tipnumber,3)=@TipFirst ''
--Exec sp_executesql @SQLSelect, N''@newnum bigint output, @Tipfirst char(3)'', @newnum=@newnum output, @TipFirst=@Tipfirst

--if @newnum is null
--	begin
--	set @newnum=@TipFirst + ''000000000000''
--	end
--set @newnum = @newnum + 1

declare @LastTipUsed char(15)

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @Newnum = cast(@LastTipUsed as bigint) + 1  
/*********  End SEB001  *************************/

update cardsin
set na2='' ''
where na2 is null

update cardsin
set na3='' ''
where na3 is null

update cardsin
set na4='' ''
where na4 is null

update cardsin
set na5='' ''
where na5 is null

update cardsin
set na6='' ''
where na6 is null

delete from cardsin2

insert into cardsin2 
select * from cardsin
order by tipfirst, ssn


drop table wrktab2FBOP 

select distinct ssn, tipnumber
into wrktab2FBOP
from cardsin2
where tipfirst=@tipfirst and tipnumber is null and ssn is not null and ssn <> ''000000000'' /* SEB002 */

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from wrktab2FBOP
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @NewTip = Cast(@Newnum as char)
		set @NewTip = replicate(''0'',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip   /* Added 11/29/2006 */
		
		update wrktab2FBOP	
		set tipnumber = @Newtip 
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update cardsin2
set tipnumber=(select tipnumber from wrktab2FBOP where ssn=cardsin2.ssn )
where ssn is not null and ssn <> ''000000000'' and tipnumber is null /* SEB002 */

--update cardsin2
--set tipnumber=b.tipnumber 
--from cardsin2 a, wrktab2FBOP b
--where a.ssn is not null and a.tipnumber is null and b.ssn=a.ssn 

/*****************************************************************************/
/* added SEB002 to assign tip to recrods with all 0s for ssn                 */
/*                                                                           */
/*****************************************************************************/
set @newnum = @newnum - 1

drop table wrktab3FBOP 

select distinct acctnum, tipnumber
into wrktab3FBOP
from cardsin2
where tipfirst=@tipfirst and tipnumber is null and acctnum is not null 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr1 cursor
for select tipnumber
from wrktab3FBOP
for update

/*                                                                            */
open tip_crsr1
/*                                                                            */
fetch tip_crsr1 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error1
	set @newnum = @newnum + 1
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @NewTip = Cast(@Newnum as char)
		set @NewTip = replicate(''0'',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip   /* Added 11/29/2006 */
		
		update wrktab3FBOP	
		set tipnumber = @Newtip 
		where current of tip_crsr1
		set @newnum = @newnum + 1		
		goto Next_Record1
Next_Record1:
		fetch tip_crsr1
	end

Fetch_Error1:
close  tip_crsr1
deallocate  tip_crsr1

update cardsin2
set tipnumber=(select tipnumber from wrktab3FBOP where acctnum=cardsin2.acctnum )
where acctnum is not null and tipnumber is null /* SEB002 */

delete from cardsin

insert into cardsin 
select * from cardsin2
order by tipfirst, tipnumber, ssn

exec RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip  /*SEB001 */' 
END
GO
