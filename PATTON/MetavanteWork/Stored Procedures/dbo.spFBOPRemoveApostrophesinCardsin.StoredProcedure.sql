USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPRemoveApostrophesinCardsin]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPRemoveApostrophesinCardsin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPRemoveApostrophesinCardsin]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPRemoveApostrophesinCardsin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPRemoveApostrophesinCardsin] AS
update FBOPcardsin
set NA1=replace(na1,char(39), '' ''),na2=replace(na2,char(39), '' ''), NA3=replace(NA3,char(39), '' ''),NA4=replace(NA4,char(39), '' ''), NA5=replace(NA5,char(39), '' ''),NA6=replace(NA6,char(39), '' ''), lastname=replace(lastname,char(39), '' ''), addr1=replace(addr1,char(39), '' ''), addr2=replace(addr2,char(39), '' ''), addr3=replace(addr3,char(39), '' ''), citystate=replace(citystate,char(39), '' '')' 
END
GO
