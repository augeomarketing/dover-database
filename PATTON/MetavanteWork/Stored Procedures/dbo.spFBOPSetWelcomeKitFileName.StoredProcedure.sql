USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPSetWelcomeKitFileName]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPSetWelcomeKitFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPSetWelcomeKitFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPSetWelcomeKitFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(60), @currentdate nchar(8), @workmonthda nchar(5), @workmonth nchar(2), @workda nchar(2), @workyear nchar(4), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonthda=left(@endingdate,5)
set @workmonth=left(@workmonthda,2)
set @workda=right(@workmonthda,2)
set @workyear=right(@endingdate,4)

set @currentdate=@workyear + @workmonth + @workda 

set @filename=''WFBOP'' + @currentdate + ''.xls''
 
set @newname=''O:\5xx\Output\WelcomeKits\FBOPWelcome.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\WelcomeKits\'' + @filename' 
END
GO
