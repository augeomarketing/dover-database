USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[sp500EmployeeBonusProcess]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp500EmployeeBonusProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp500EmployeeBonusProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp500EmployeeBonusProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp500EmployeeBonusProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (acctno, amtdebit)
select [card #], [Total Pts#]	
from [EmployeeBonus]

update transumbonus
set ratio=''1'', trancode=''BE'', description=''Employee Bonus'', histdate=@datein, numdebit=''1'', overage=''0'', tipnumber=(select tipnumber from [500AmericanTrust].dbo.affiliat where acctid = transumbonus.acctno)

delete from transumbonus
where tipnumber is null

insert into [500AmericanTrust].dbo.history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '' '', ratio, overage
from transumbonus

update [500AmericanTrust].dbo.customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=[500AmericanTrust].dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=[500AmericanTrust].dbo.customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=[500AmericanTrust].dbo.customer.tipnumber)' 
END
GO
