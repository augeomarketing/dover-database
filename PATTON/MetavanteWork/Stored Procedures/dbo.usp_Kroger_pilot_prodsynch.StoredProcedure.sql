USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_Kroger_pilot_prodsynch]    Script Date: 08/23/2016 08:52:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Kroger_pilot_prodsynch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Kroger_pilot_prodsynch]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_Kroger_pilot_prodsynch]    Script Date: 08/23/2016 08:52:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Kroger_pilot_prodsynch]

AS

BEGIN TRAN
--SYNCH PILOT DATA: AFFILIAT
DELETE t FROM [52JKroegerPersonalFinance].dbo.AFFILIAT t JOIN [5KJ].dbo.AFFILIAT s on t.TIPNUMBER = REPLACE(s.tipnumber, 'K', '2')
INSERT INTO [52JKroegerPersonalFinance].dbo.AFFILIAT
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
SELECT	ACCTID, REPLACE(tipnumber, 'K', '2'), AcctType, DATEADDED, isnull(SECID,'000000000'), AcctStatus, AcctTypeDesc, LastName, YTDEarned, RIGHT(ACCTID, 11)
FROM	[5KJ].dbo.AFFILIAT
WHERE AcctType = 'DEBIT'

DELETE t FROM [52R].dbo.AFFILIAT t JOIN [5KR].dbo.AFFILIAT s on t.TIPNUMBER = REPLACE(s.tipnumber, 'K', '2')
INSERT INTO [52R].dbo.AFFILIAT
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
SELECT	ACCTID, REPLACE(tipnumber, 'K', '2'), AcctType, DATEADDED, isnull(SECID,'000000000'), AcctStatus, AcctTypeDesc, LastName, YTDEarned, RIGHT(ACCTID, 11)
FROM	[5KR].dbo.AFFILIAT
WHERE AcctType = 'DEBIT'

DELETE t FROM [52W].dbo.AFFILIAT t JOIN [5KW].dbo.AFFILIAT s on t.TIPNUMBER = REPLACE(s.tipnumber, 'K', '2')
INSERT INTO [52W].dbo.AFFILIAT
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
SELECT	ACCTID, REPLACE(tipnumber, 'K', '2'), AcctType, DATEADDED, isnull(SECID,'000000000'), AcctStatus, AcctTypeDesc, LastName, YTDEarned, RIGHT(ACCTID, 11)
FROM	[5KW].dbo.AFFILIAT
WHERE AcctType = 'DEBIT'


--SYNCH PILOT DATA: CUSTOMER
DELETE t FROM [52JKroegerPersonalFinance].dbo.CUSTOMER t JOIN [5KJ].dbo.CUSTOMER s on t.TIPNUMBER = REPLACE(s.tipnumber, 'K', '2')
INSERT INTO [52JKroegerPersonalFinance].dbo.CUSTOMER
	(
	TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, [State], ZipCode,
	 StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1,
	 Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	 ) 
SELECT	REPLACE(TIPNUMBER, 'K', '2'), RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, 
		REPLACE(TIPFIRST, 'K', '2'), TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4,
		City, [State], ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES,
		BonusFlag, ISNULL(Misc1, '000000000'), Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
FROM	[5KJ].dbo.CUSTOMER

DELETE t FROM [52R].dbo.CUSTOMER t JOIN [5KR].dbo.CUSTOMER s on t.TIPNUMBER = REPLACE(s.tipnumber, 'K', '2')
INSERT INTO [52R].dbo.CUSTOMER
	(
	TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, [State], ZipCode,
	 StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1,
	 Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	 ) 
SELECT	REPLACE(TIPNUMBER, 'K', '2'), RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, 
		REPLACE(TIPFIRST, 'K', '2'), TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4,
		City, [State], ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES,
		BonusFlag, ISNULL(Misc1, '000000000'), Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
FROM	[5KR].dbo.CUSTOMER

DELETE t FROM [52W].dbo.CUSTOMER t JOIN [5KW].dbo.CUSTOMER s on t.TIPNUMBER = REPLACE(s.tipnumber, 'K', '2')
INSERT INTO [52W].dbo.CUSTOMER
	(
	TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, TIPFIRST, TIPLAST, 
	 ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, [State], ZipCode,
	 StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1,
	 Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	 ) 
SELECT	REPLACE(TIPNUMBER, 'K', '2'), RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, [STATUS], DATEADDED, LASTNAME, 
		REPLACE(TIPFIRST, 'K', '2'), TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4,
		City, [State], ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES,
		BonusFlag, ISNULL(Misc1, '000000000'), Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
FROM	[5KW].dbo.CUSTOMER


--SYNCH PILOT DATA: HISTORY
INSERT INTO [52JKroegerPersonalFinance].dbo.HISTORY
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT 	REPLACE(h.TIPNUMBER, 'K', '2'), ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
FROM	[5KJ].dbo.HISTORY h join [52JKroegerPersonalFinance].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER
WHERE	h.SECID <> 'PROD'
UPDATE	h
SET		SECID = 'PROD'
FROM	[5KJ].dbo.HISTORY h join [52JKroegerPersonalFinance].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER
WHERE	h.SECID <> 'PROD' 
DELETE h FROM [5KJ].dbo.HISTORY h join [52JKroegerPersonalFinance].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER WHERE h.SECID = 'PROD'
INSERT INTO [5KJ].dbo.HISTORY
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT 	REPLACE(h.TIPNUMBER, '52J', '5KJ'), ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, 'PROD', Ratio, Overage
FROM	[52JKroegerPersonalFinance].dbo.HISTORY h join [5KJ].dbo.CUSTOMER c on h.tipnumber = REPLACE(c.TIPNUMBER, 'K', '2') 

INSERT INTO [52R].dbo.HISTORY
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT 	REPLACE(h.TIPNUMBER, 'K', '2'), ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
FROM	[5KR].dbo.HISTORY h join [52R].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER
WHERE	h.SECID <> 'PROD'
UPDATE	h
SET		SECID = 'PROD'
FROM	[5KR].dbo.HISTORY h join [52R].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER
WHERE	h.SECID <> 'PROD' 
DELETE h FROM [5KR].dbo.HISTORY h join [52R].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER WHERE h.SECID = 'PROD'
INSERT INTO [5KR].dbo.HISTORY
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT 	REPLACE(h.TIPNUMBER, '52R', '5KR'), ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, 'PROD', Ratio, Overage
FROM	[52R].dbo.HISTORY h join [5KR].dbo.CUSTOMER c on h.tipnumber = REPLACE(c.TIPNUMBER, 'K', '2')

INSERT INTO [52W].dbo.HISTORY
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT 	REPLACE(h.TIPNUMBER, 'K', '2'), ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
FROM	[5KW].dbo.HISTORY h join [52W].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER
WHERE	h.SECID <> 'PROD'
UPDATE	h
SET		SECID = 'PROD'
FROM	[5KW].dbo.HISTORY h join [52W].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER
WHERE	h.SECID <> 'PROD' 
DELETE h FROM [5KW].dbo.HISTORY h join [52W].dbo.CUSTOMER c on REPLACE(h.tipnumber, 'K', '2') = c.TIPNUMBER WHERE h.SECID = 'PROD'
INSERT INTO [5KW].dbo.HISTORY
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
SELECT 	REPLACE(h.TIPNUMBER, '52W', '5KW'), ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, 'PROD', Ratio, Overage
FROM	[52W].dbo.HISTORY h join [5KW].dbo.CUSTOMER c on h.tipnumber = REPLACE(c.TIPNUMBER, 'K', '2') 

EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory '52J'
EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory '52R'
EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory '52W'
EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory '5KJ'
EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory '5KR'
EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory '5KW'

COMMIT TRAN


GO


