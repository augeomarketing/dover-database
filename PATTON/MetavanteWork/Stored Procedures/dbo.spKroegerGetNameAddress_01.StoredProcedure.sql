USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spKroegerGetNameAddress_01]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGetNameAddress_01]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spKroegerGetNameAddress_01]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spKroegerGetNameAddress_01]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 12/09
-- Description:	Get Name address records
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerGetNameAddress_01]
	-- Add the parameters for the stored procedure here
	@Errorcount int output	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Truncate table dbo.KroegerNameAddr
	
	insert into dbo.KroegerNameAddr	(NoMail, MR, MRS, MS, LoyaltyNumber, LastName, FirstName, MiddleName, Addr1, Addr2, City, State, Zip, HomePhone, OldLoyaltyNumber, CardNumber)
	select 
		substring(col001,3,1),		--NO-MAIL-FLAG
		substring(col001,4,1),		--TITLE-MR  PIC X(01) VALUE ''0''.
		substring(col001,5,1),		--TITLE-MRS PIC X(01) VALUE ''0''.
		substring(col001,6,1),		--TITLE-MS  PIC X(01) VALUE ''0''.
		substring(col001,7,11),		--LOYALTY-NBR PIC X(11) VALUE SPACES.
		substring(col001,18,18),	--NAME-LAST               PIC X(18).
		substring(col001,36,12),	--NAME-FIRST              PIC X(12).
		substring(col001,48,1),		--NAME-MIDDLE-INIT        PIC X(01).
		substring(col001,49,40),	--ADDR-1                  PIC X(40).
		substring(col001,89,40),	--ADDR-2                  PIC X(40).
		substring(col001,129,17),	--ADDR-CITY               PIC X(17).
		substring(col001,146,2),	--ADDR-STATE              PIC X(02).
		substring(col001,148,9),	--ADDR-ZIP                PIC X(09).
		substring(col001,157,10),	--HOME-PHONE              PIC X(10).
		substring(col001,167,16),	--OLD-LOYALTY-NBR         PIC X(16).
		substring(col001,185,16)    -- Card Number
	from dbo.KroegerNameAddrIn
	where left(col001,2)=''02''
	
	set @Errorcount=(select substring(col001,15,10) from dbo.KroegerNameAddrIn where left(col001,2)=''09'') - 2 - (select count(*) from dbo.KroegerNameAddr)
	
END 
' 
END
GO
