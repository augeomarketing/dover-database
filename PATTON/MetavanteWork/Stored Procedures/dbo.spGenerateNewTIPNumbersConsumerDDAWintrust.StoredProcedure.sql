USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDAWintrust]    Script Date: 05/08/2012 10:14:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersConsumerDDAWintrust]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDAWintrust]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersConsumerDDAWintrust]    Script Date: 05/08/2012 10:14:15 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[spGenerateNewTIPNumbersConsumerDDAWintrust] @tipfirst varchar(3)
AS 

EXEC [dbo].[usp_FIS_TIPassignment] @tipfirst

GO