USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcessBonusInNew]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessBonusInNew]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcessBonusInNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcessBonusInNew]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 12/09
-- Description:	Generic Process bonus
-- =============================================
CREATE PROCEDURE [dbo].[spBonusFileProcessBonusInNew]
	-- Add the parameters for the stored procedure here
	@datein char(10),
	@a_TipPrefix char(3)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	declare @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName nchar(100)

	/* Get DBName */
	Begin			
		set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
		where DBNumber=@a_TipPrefix)
	End		

	/******************************************************************************/
	/*                                                                            */
	/*    THIS IS TO process the Bonus input file Step 2                          */
	/*    This actually adds records to the History table and updates the balance */
	/*                                                                            */
	/******************************************************************************/
	delete from metavantework.dbo.transumbonus

	insert into metavantework.dbo.transumbonus (tipnumber, acctno, amtdebit)
	select tipnumber, [Card Number], [10000 Pts]	
	from metavantework.dbo.[BonusIn]
	where left(tipnumber,3)=@a_TipPrefix

	update metavantework.dbo.transumbonus
	set ratio=''1'', trancode=''BI'', description=''Bonus Points Increase'', histdate=@datein, numdebit=''1'', overage=''0''

	set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.history select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, '''' '''', ratio, overage
				from metavantework.dbo.transumbonus 
				where tipnumber in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.customer) ''
	Exec sp_executesql @SQLInsert  
	 
	set @SQLUpdate=''update '' + QuoteName(@DBName) + N''.dbo.customer
		set runavailable=runavailable + (select sum(amtdebit) from metavantework.dbo.transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from metavantework.dbo.transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)
		where exists(select tipnumber from metavantework.dbo.transumbonus where tipnumber='' + QuoteName(@DBName) + N''.dbo.customer.tipnumber)''
	Exec sp_executesql @SQLupdate

END
' 
END
GO
