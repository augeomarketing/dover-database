USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spAllowForHotCardedCardsKroeger]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAllowForHotCardedCardsKroeger]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spAllowForHotCardedCardsKroeger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spAllowForHotCardedCardsKroeger]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAllowForHotCardedCardsKroeger] 
	-- Add the parameters for the stored procedure here
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
declare @DBName varchar(50), @SQLSet nvarchar(2000), @SQLIf nvarchar(2000), @SQLInsert nvarchar(2000), @SQLUpdate nvarchar(2000)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

if exists(select * from dbo.sysobjects where xtype=''u'' and name = ''Hottemp'') 
	BEGIN
		drop table dbo.Hottemp 
	END 


set @SQLInsert=''select distinct tipnumber, oldcc, ssn, lastname, ddanum, period, cardtype
				into dbo.Hottemp
				from dbo.cardsin
				where Cardtype=''''D'''' 
						and status = ''''C'''' 
						and tipnumber is not null 
						and left(tipnumber,1)<>'''' '''' 
						and oldcc is not null and left(oldcc,1)<>'''' ''''
						and oldcc not in (select acctid from '' + QuoteName(@DBName) + N''.dbo.affiliat) ''
Exec sp_executesql @SQLInsert

set @SQLInsert = ''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, SecID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
					select oldcc, tipnumber, cardtype, period, ssn, ''''A'''', '''''''', lastname, ''''0'''', ddanum
					from dbo.Hottemp ''
Exec sp_executesql @SQLInsert


set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.affiliat
					set AcctType=''''Credit''''
					where AcctType=''''C'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.affiliat
					set AcctType=''''Debit''''
					where AcctType=''''D'''' ''
Exec sp_executesql @SQLUpdate

set @SQLUpdate = ''update '' + QuoteName(@DBName) + N''.dbo.affiliat
					set AcctTypeDesc = (select Accttypedesc 
											from '' + QuoteName(@DBName) + N''.dbo.accttype
        								where 	accttype='' + QuoteName(@DBName) + N''.dbo.affiliat.accttype) 
					where len(AcctTypeDesc) = ''''0'''' ''
Exec sp_executesql @SQLUpdate

END
' 
END
GO
