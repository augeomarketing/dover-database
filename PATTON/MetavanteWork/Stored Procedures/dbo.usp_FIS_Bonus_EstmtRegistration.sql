USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_EstmtRegistration]    Script Date: 01/24/2012 15:14:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_FIS_Bonus_EstmtRegistration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_FIS_Bonus_EstmtRegistration]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_FIS_Bonus_EstmtRegistration]    Script Date: 01/24/2012 15:14:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_FIS_Bonus_EstmtRegistration] 
	@startDate varchar(10), @EndDate varchar(10), @tipfirst varchar(3), @Trancode varchar(2)

AS 

/****************************************************************************/
/*   Procedure to generate the Activation bonus based on first use          */
/****************************************************************************/
DECLARE	@DBName varchar(50), 
		@SQL nvarchar(max), 
		@SQLInsert nvarchar(1000), 
		@SQLIF nvarchar(1000), 
		@SQLUpdate nvarchar(1000), 
		@BonusPoints numeric(9), 
		@Tipnumber char(15), 
		@Trandate varchar(10), 
		@Acctid char(16), 
		@ProcessFlag char(1), 
		@Description char(40)

set @Trandate=@EndDate

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  [E-StatementBonus] from DBProcessInfo where DBNumber=@TipFirst)
set @Description =(SELECT  Description from rewardsnow.dbo.Trantype where Trancode = @Trancode )

IF	@BonusPoints > 0 
	BEGIN
		TRUNCATE TABLE estmt_registration
		
		SET	@SQL = N'
			INSERT INTO [EStmt_Registration ] 
						(tipnumber)
			SELECT		tipnumber
			FROM		[RN1].[<<DBNAME2>>].dbo.[1security]
			WHERE		password is not null 
				and		regdate > = ''07/01/2009'' 
				and		emailstatement = ''Y''
				and		left(tipnumber,3)= ''<<TIPFIRST>>''
				and		tipnumber in (SELECT tipnumber FROM [<<DBNAME>>].dbo.customer)
				and		tipnumber not in (SELECT TIPNUMBER FROM [<<DBNAME>>].dbo.OneTimeBonuses WHERE trancode = ''<<TRANCODE>>'') 
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME2>>', (Select DBNameNEXL from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<TIPFIRST>>', @tipfirst)							
		SET		@SQL =	REPLACE(@SQL, '<<TRANCODE>>', @Trancode)							
		EXEC	sp_executesql @SQL

		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.history
						(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
			SELECT		TIPnumber, ''<<ENDDATE>>'', ''<<TRANCODE>>'', ''1'', <<BONUSPTS>>, ''1'', ''<<DESCRIPTION>>'', ''0''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber in (SELECT tipnumber FROM [EStmt_Registration ])
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<DESCRIPTION>>', (SELECT  Description from Trantype where Trancode = @Trancode))
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)
		SET		@SQL =	REPLACE(@SQL, '<<TRANCODE>>', @Trancode)							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)
		EXEC	sp_executesql @SQL  

		SET @sql = N'
			UPDATE	[<<DBNAME>>].dbo.Customer 
			SET		RunAvailable = RunAvailable + ''<<BONUSPTS>>'', 
					RunBalance = RunBalance + ''<<BONUSPTS>>''  
			WHERE	tipnumber in (SELECT tipnumber FROM [EStmt_Registration ])
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<BONUSPTS>>', @BonusPoints)							
		EXEC	sp_executesql @SQL

		SET	@SQL = '
			INSERT INTO	[<<DBNAME>>].dbo.OneTimeBonuses
						(TipNumber,TranCode, DateAwarded)
			SELECT		TIPnumber, ''<<TRANCODE>>'', ''<<ENDDATE>>''
			FROM		[<<DBNAME>>].dbo.Customer
			WHERE		tipnumber in (SELECT tipnumber FROM [EStmt_Registration ])
			'
		SET		@SQL =	REPLACE(@SQL, '<<DBNAME>>', (Select DBNamePatton from RewardsNOW.dbo.dbprocessinfo where DBNumber = @TipFirst))							
		SET		@SQL =	REPLACE(@SQL, '<<ENDDATE>>', @EndDate)
		SET		@SQL =	REPLACE(@SQL, '<<TRANCODE>>', @Trancode)							
		EXEC	sp_executesql @SQL
		
	END
GO


