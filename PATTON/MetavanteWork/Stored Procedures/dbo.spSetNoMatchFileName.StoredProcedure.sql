USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spSetNoMatchFileName]    Script Date: 03/19/2014 10:01:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetNoMatchFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetNoMatchFileName]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spSetNoMatchFileName]    Script Date: 03/19/2014 10:01:01 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/******************************************************************************/
/******************************************************************************/
/*                   SQL TO GENERATE NO MATCH REPORT file name                */
/*                                                                            */
/* BY:  S.BLANCHETTE                                                          */
/* DATE: 5/2007                                                               */
/* REVISION: 0                                                                */
/* Dynamically sets file name for No Match report                             */
/******************************************************************************/
/******************************************************************************/

CREATE PROCEDURE [dbo].[spSetNoMatchFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

DECLARE	@Filename		char(50)
	,	@currentdate	nchar(4)
	,	@workmonth		nchar(2)
	,	@workyear		nchar(2)
	,	@endingDate		char(10)

SET	@endingDate			=	(Select MonthEndingdate From autoprocessdetail Where TipFirst = @TipPrefix)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
SET	@workmonth			=	left(@endingdate,2)
SET	@workyear			=	right(@endingdate,2)
SET	@currentdate		=	@workmonth + @workyear
SET	@filename			=	'RewardsNOWNomatch.' + @TipPrefix + '_' + @currentdate + '.xls'

SET	@newname			=	'O:\5xx\Output\Nomatch\NoMatch.bat ' + @filename
SET	@ConnectionString	=	'O:\5xx\Output\NoMatch\' + @filename

GO
