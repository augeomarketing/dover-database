USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisUpdateCustomerStatus]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisUpdateCustomerStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisUpdateCustomerStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisUpdateCustomerStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisUpdateCustomerStatus] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Declare @DBName varchar(100), @SQLUpdate nvarchar(1000)


	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   	-- Get Data base name for this Harris FI
	set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	 --Update customer status to reflect that customer is enrolled.
	set @sqlupdate=''Update '' + QuoteName(@DBName) + N''.dbo.Customer
					set status=''''X''''
					where tipnumber not in (select dim_selfenroll_tipnumber from selfenroll) ''	
	Exec sp_executesql @sqlupdate  


END' 
END
GO
