USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spHarrisFoundEmail]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisFoundEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHarrisFoundEmail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHarrisFoundEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S. Blanchette
-- Create date: 12/09
-- Description:	Send Emails for found enrolle
-- =============================================
CREATE PROCEDURE [dbo].[spHarrisFoundEmail]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @name1 varchar(50),
			@tipnumber varchar(15),
			@address1 varchar(50),
			@address2 varchar(50),
			@citystatezip varchar(100),
			@email varchar(200),
			@match varchar(1), 
			@Bodyout varchar(4096),
			@Subject varchar(200), 
			@Body varchar(4096), 
			@To varchar(200), 
			@From varchar(200)

	set @Subject = ''Welcome to Harris Everyday Benefits!''
	set @From = ''rewards@rewardsnow.com (Harris Everyday Benefits)''

	-- Insert statements for procedure here
	declare CUSTIN_crsr cursor
	for select Name1, Tipnumber, Address1, Address2, CityStateZip, dim_selfenrollerror_email, match 
	from [RN1].Metavante.dbo.sendemail
	/*                                                                            */
	open CUSTIN_crsr
	fetch CUSTIN_crsr into @Name1, @Tipnumber, @Address1, @Address2, @CityStateZip, @email, @match 
	/******************************************************************************/	
	/*                                                                            */
	/* MAIN PROCESSING                                                            */
	/*                                                                            */
	--if @@FETCH_STATUS = 1
	--	goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
	begin

		exec [dbo].[usp_harrisEnrollFoundWithOut] @Name1, @Tipnumber, @Address1, @Address2, @CityStateZip, @email, @match, @bodyout output
		set @Body = @bodyout
		set @To = @email
		insert into maintenance.dbo.perlemail (dim_perlemail_subject, 
					dim_perlemail_body, 
					dim_perlemail_to, 
					dim_perlemail_from
					) 
		values (@Subject, 	
			@Body, 
			@To,
			@From 
			)

	goto Next_Record

Next_Record:
	fetch CUSTIN_crsr into @Name1, @Tipnumber, @Address1, @Address2, @CityStateZip, @email, @match 

end

Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr

END
' 
END
GO
