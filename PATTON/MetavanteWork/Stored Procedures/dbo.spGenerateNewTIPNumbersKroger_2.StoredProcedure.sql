USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbersKroger_2]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersKroger_2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbersKroger_2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateNewTIPNumbersKroger_2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbersKroger_2] 
	-- Add the parameters for the stored procedure here
	@tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)
/* SB003 */declare @tiplast varchar(12)

declare @LastTipUsed char(15), @tipnumber varchar(15) /* SEB003 */

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

/*****************************************************/
/*  SEB003            */
/*                    */
set @tipnumber = @LastTipUsed
Set @tiplast = Right(@tipnumber, 12)
Set @tiplast = @tiplast + 1

Set @tipnumber = rtrim(@tipfirst) + @tiplast
set @tipnumber=left(ltrim(@tipnumber), 3) + REPLICATE (''0'', (15- len(@tipnumber) )) + right(rtrim(@tipnumber), (len(@tipnumber)-3))


/*                    */
/**************************************************/
/*  End SEB003 */
--set @Newnum = cast(@LastTipUsed as bigint) + 1  
/*********  End SEB001  *************************/

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */

declare tip_crsr cursor
for select tipnumber
from [dbo].[KroegerNameAddr]
where tipnumber is null
for update


/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @NewTip = @tipnumber
				
		update [dbo].[KroegerNameAddr]
		set tipnumber = @Newtip 
		where current of tip_crsr
		
		Set @tiplast = Right(@tipnumber, 12)
		Set @tiplast = @tiplast + 1

		Set @tipnumber = rtrim(@tipfirst) + @tiplast
		set @tipnumber=left(ltrim(@tipnumber), 3) + REPLICATE (''0'', (15- len(@tipnumber) )) + right(rtrim(@tipnumber), (len(@tipnumber)-3))
		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

exec RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip  

END
' 
END
GO
