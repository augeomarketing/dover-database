USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spHandleOptOutsDebitCards]    Script Date: 06/05/2012 07:38:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHandleOptOutsDebitCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHandleOptOutsDebitCards]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[spHandleOptOutsDebitCards]    Script Date: 06/05/2012 07:38:36 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[spHandleOptOutsDebitCards]   --@TipPrefix char(3)  <--ORIGINAL CODE
	@Tipfirst varchar(3)

AS

EXEC usp_HandleOptOuts_v2 @Tipfirst

GO

------------ORIGINAL CODE --------------------

--/**********************************************************************/
--/*  BY S.Blanchette                                                   */
--/*  Date 9/2008                                                       */
--/*  SCAN SEB001                                                       */
--/*  Reason  To add logic to check DDA for opt out.                    */
--/**********************************************************************/ 
------- Remove CreditCardIn for Opt Out people  -----
--delete from  debitcardin
--where exists(select * from optoutcontrol where acctnumber= debitcardin.acctnum)

--/****************************/
--/* START SEB001             */
--/****************************/
--delete from  debitcardin
--where exists(select * from DDAOptOutControl where DDANUMBER= debitcardin.DDANUM and left(tipnumber,3)=@TipPrefix )
--GO



