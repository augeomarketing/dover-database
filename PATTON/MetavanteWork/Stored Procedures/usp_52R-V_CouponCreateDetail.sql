USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_52R-V_CouponCreateDetail]    Script Date: 05/31/2013 09:14:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_52R-V_CouponCreateDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_52R-V_CouponCreateDetail]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_52R-V_CouponCreateDetail]    Script Date: 05/31/2013 09:14:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_52R-V_CouponCreateDetail]

AS

BEGIN
	SET NOCOUNT ON;

	TRUNCATE TABLE	dbo.Kroeger_Coupon_Request_Detail
	 
	INSERT INTO	Kroeger_Coupon_Request_Detail (RecordType, LoyaltyNumber, CardHolderName, PointsRedeemed, TransID)
	SELECT		'D', b.misc2, b.ACCTNAME1, (points), transid
	FROM		OnlineHistoryWork.dbo.OnlHistory a JOIN [52R].dbo.customer b ON a.tipnumber = b.tipnumber
	WHERE		left(a.tipnumber, 3) = '52R' and a.KroegerCouponFlag is null and a.trancode = 'RK'
	
	INSERT INTO	Kroeger_Coupon_Request_Detail (RecordType, LoyaltyNumber, CardHolderName, PointsRedeemed, TransID)
	SELECT		'D', b.misc2, b.ACCTNAME1, (points), transid
	FROM		OnlineHistoryWork.dbo.OnlHistory a JOIN [52V].dbo.customer b ON a.tipnumber = b.tipnumber
	WHERE		left(a.tipnumber, 3) = '52V' and a.KroegerCouponFlag is null and a.trancode = 'RK'	
	
	UPDATE		OnlineHistoryWork.dbo.OnlHistory
	SET			KroegerCouponFlag = getdate()
	WHERE		left(tipnumber, 3) in ('52R','52V') and KroegerCouponFlag is null and trancode = 'RK'
	
END


GO


