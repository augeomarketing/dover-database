USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteDebitProcessLakeside]    Script Date: 07/23/2010 14:56:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcessLakeside]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteDebitProcessLakeside]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteDebitProcessLakeside]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2010
-- Description:	Debit process for Lakeside Bank
-- =============================================
-- S Blanchette
-- 7/2010
-- Remove comma from name switch logic
-- SEB001
--
-- S Blanchette
-- 7/2010
-- Remove comma from City State field
-- SEB002
--
CREATE PROCEDURE [dbo].[spMetavanteDebitProcessLakeside]  
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
								      
/*           Convert Trancode                                                     */
update DebitCardInlakeside
set DebitCardInlakeside.[Transaction Code]=TrancodeXref.trancodeout 
from DebitCardInlakeside, TrancodeXref 
where DebitCardInlakeside.[Transaction Code]=TrancodeXref.trancodeIn 

update DebitCardInlakeside	
set numpurch=''1'', purch=[Total Dollar Cents]
where [Transaction Code]=''67''

update DebitCardInlakeside	
set numret=''1'', amtret=[Total Dollar Cents]
where [Transaction Code]=''37''

update DebitCardInlakeside
set status=''A''
where [External Status]=''A''

update DebitCardInlakeside
set status=''C''
where [External Status]=''B''

update DebitCardInlakeside
set cardtype=''D''

update DebitCardInLakeside
set TIPMID=''0000000''

/* rearrange name 1 from  Last, First to First Last */
/* SEB001 update DebitCardInlakeside
set [Full Name]=rtrim(substring([Full Name],charindex('','',[Full Name])+2,len(rtrim([Full Name])))) + '' '' + substring([Full Name],1,charindex('','',[Full Name])-1)
where substring([Full Name],1,1) not like '' '' and [Full Name] is not null and [Full Name] LIKE ''%,%'' */

update DebitCardInlakeside
set [Full Name]=rtrim(substring([Full Name],charindex('' '',[Full Name])+1,len(rtrim([Full Name])))) + '' '' + substring([Full Name],1,charindex('' '',[Full Name])-1)
where substring([Full Name],1,1) not like '' '' and [Full Name] is not null /* SEB001 */

update DebitCardInlakeside
set [Full Name]=ltrim([Full Name])

/* rearrange name 2 from  Last, First to First Last */
/* SEB001 update DebitCardInlakeside
set [Secondary Name]=rtrim(substring([Secondary Name],charindex('','',[Secondary Name])+2,len(rtrim([Secondary Name])))) + '' '' + substring([Secondary Name],1,charindex('','',[Secondary Name])-1)
where substring([Secondary Name],1,1) not like '' '' and [Secondary Name] is not null and [Secondary Name] LIKE ''%,%'' */

update DebitCardInlakeside
set [Secondary Name]=rtrim(substring([Secondary Name],charindex('' '',[Secondary Name])+1,len(rtrim([Secondary Name])))) + '' '' + substring([Secondary Name],1,charindex('' '',[Secondary Name])-1)
where substring([Secondary Name],1,1) not like '' '' and [Secondary Name] is not null /* SEB001 */

update DebitCardInlakeside
set [Secondary Name]=ltrim([Secondary Name])

update DebitcardInLakeside
set [City / State]=replace([City / State],char(44), '''') /* SEB002 */

END
' 
END
GO
