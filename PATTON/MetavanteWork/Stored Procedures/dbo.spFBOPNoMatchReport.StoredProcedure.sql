USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPNoMatchReport]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPNoMatchReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPNoMatchReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPNoMatchReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2009
-- Description:	to be able to process FBOP seperately from other Metavante
-- =============================================
CREATE PROCEDURE [dbo].[spFBOPNoMatchReport] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(15), 
	@EndDateParm char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @errmsg varchar(50), @currentend numeric(9)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

truncate table FBOPwrknomatch1

set @SQLInsert=N''insert into FBOPwrknomatch1
			select left(misc1,9) as ssn
			from '' + QuoteName(@DBName) + N''.dbo.customer
			where dateadded=@Dateadded and misc1 is not null and left(misc1,9)<>''''000000000'''' and left(misc1,9)<>''''         '''' and right(misc1,1)<>''''0'''' ''
Exec sp_executesql @SQLInsert, N''@Dateadded nchar(10)'', @Dateadded = @EndDateParm

truncate table FBOPwrknomatch2

set @SQLInsert=N''insert into FBOPwrknomatch2
			select left(misc1,9) as ssn
			from '' + QuoteName(@DBName) + N''.dbo.customer
			where exists(select * from FBOPwrknomatch1 where ssn = left('' + QuoteName(@DBName) + N''.dbo.customer.misc1,9)) ''
Exec sp_executesql @SQLInsert

truncate table FBOPwrknomatch3

insert into FBOPwrknomatch3
select ssn
from FBOPwrknomatch2
group by ssn having count(*)>1

truncate table FBOPwrknomatch4

set @SQLInsert=N''insert into FBOPwrknomatch4
			select a.tipnumber, b.acctid, acctname1, acctname2, acctname3, address1, address2, address4, left(misc1,9) as ssn
			from '' + QuoteName(@DBName) + N''.dbo.customer a join '' + QuoteName(@DBName) + N''.dbo.affiliat b on a.tipnumber=b.tipnumber
			where exists(select * from FBOPwrknomatch3 where ssn = left(misc1,9)) ''
Exec sp_executesql @SQLInsert
END
' 
END
GO
