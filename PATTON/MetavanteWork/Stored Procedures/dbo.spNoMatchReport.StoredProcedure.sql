USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNoMatchReport]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNoMatchReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNoMatchReport]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNoMatchReport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/******************************************************************************/
/*                   SQL TO GENERATE NO MATCH REPORTS                         */
/*                                                                            */
/* BY:  S.BLANCHETTE                                                          */
/* DATE: 5/2007                                                               */
/* REVISION: 0                                                                */
/* Gets SSN from Customer table where Dateadded is equal to parameter passed  */
/* in.  Next it gets all records from Customer table that have the same SSN   */
/* as what was found for the date passed in.  Next file is created for SSN    */
/* that have more than two entries in customer table.                         */           
/******************************************************************************/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spNoMatchReport] @TipFirst char(15), @EndDateParm char(10)
AS

declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @errmsg varchar(50), @currentend numeric(9)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

truncate table wrknomatch1

set @SQLInsert=N''insert into wrknomatch1
			select left(misc1,9) as ssn
			from '' + QuoteName(@DBName) + N''.dbo.customer
			where dateadded=@Dateadded and misc1 is not null and left(misc1,9)<>''''000000000'''' and left(misc1,9)<>''''         '''' and right(misc1,1)<>''''0'''' ''
Exec sp_executesql @SQLInsert, N''@Dateadded nchar(10)'', @Dateadded = @EndDateParm

truncate table wrknomatch2

set @SQLInsert=N''insert into wrknomatch2
			select left(misc1,9) as ssn
			from '' + QuoteName(@DBName) + N''.dbo.customer
			where exists(select * from wrknomatch1 where ssn = left('' + QuoteName(@DBName) + N''.dbo.customer.misc1,9)) ''
Exec sp_executesql @SQLInsert

truncate table wrknomatch3

insert into wrknomatch3
select ssn
from wrknomatch2
group by ssn having count(*)>1

truncate table wrknomatch4

set @SQLInsert=N''insert into wrknomatch4
			select a.tipnumber, b.acctid, acctname1, acctname2, acctname3, address1, address2, address4, left(misc1,9) as ssn
			from '' + QuoteName(@DBName) + N''.dbo.customer a join '' + QuoteName(@DBName) + N''.dbo.affiliat b on a.tipnumber=b.tipnumber
			where exists(select * from wrknomatch3 where ssn = left(misc1,9)) ''
Exec sp_executesql @SQLInsert' 
END
GO
