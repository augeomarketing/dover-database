USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateAutoProcessDetail]    Script Date: 01/19/2011 15:38:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateAutoProcessDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateAutoProcessDetail]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateAutoProcessDetail]    Script Date: 01/19/2011 15:38:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_UpdateAutoProcessDetail]
	@beginDate DATETIME
	, @endDate DATETIME
	, @packageStep VARCHAR(2) --WK = WelcomeKit
	, @packageID INT --Package to run
	, @tipfirst VARCHAR(3)
AS
BEGIN
	DECLARE @beginDateString CHAR(10)
	DECLARE @endDateString CHAR(10)
	
	SET @beginDateString = CONVERT(CHAR(10), @beginDate, 101)
	SET @endDateString = CONVERT(CHAR(10), @endDate, 101)
	SET @packageStep = UPPER(@packageStep)
	
	IF @packageStep = 'WK'
	BEGIN
		UPDATE autoprocessdetail 
		SET MonthBeginingDate = @beginDateString
			, MonthEndingdate = @endDateString
			, WK_id = @packageID
		WHERE
			TipFirst = @tipfirst		
	END	
END


GO

