USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spMetavanteCombineAffiliatTables1]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteCombineAffiliatTables1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMetavanteCombineAffiliatTables1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMetavanteCombineAffiliatTables1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spMetavanteCombineAffiliatTables1]
as

delete from metavantework.dbo.affiliat

insert into  metavantework.dbo.affiliat
select * from [500AmericanTrust].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [501IllinoisNational].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [502LaytonBank].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [503Marin].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [504HudsonConsumer].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [505HudsonCommercial].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [506RabobankConsumer].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [507RabobankCommercial].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [508WoodtrustConsumer].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [509WoodtrustCommercial].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [510MarinCommercial].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [511NEBATConsumer].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [512NEBATCommercial].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [513LaytonCommercial].dbo.affiliat
where dateadded < ''06/01/2006''

insert into metavantework.dbo.affiliat
select * from [515LandmarkConsumer].dbo.affiliat
where dateadded < ''06/01/2006''' 
END
GO
