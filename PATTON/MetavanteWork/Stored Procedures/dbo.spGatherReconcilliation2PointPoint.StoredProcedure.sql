USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGatherReconcilliation2PointPoint]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation2PointPoint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGatherReconcilliation2PointPoint]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGatherReconcilliation2PointPoint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGatherReconcilliation2PointPoint]
	-- Add the parameters for the stored procedure here
@DateIn char(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	update [52JKroegerPersonalFinance].dbo.KroegerReconcilliationData
	set ValueOutPoint2 = ValueOutPoint2 + (select sum(points*ratio) from [52JKroegerPersonalFinance].dbo.history where trancode in (''G2'',''G7'') and histdate = @DateIn )
	where ProcessDate = @DateIn 
	
END
' 
END
GO
