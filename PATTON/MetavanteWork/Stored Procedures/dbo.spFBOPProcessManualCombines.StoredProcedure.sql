USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spFBOPProcessManualCombines]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPProcessManualCombines]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFBOPProcessManualCombines]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFBOPProcessManualCombines]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spFBOPProcessManualCombines] @Tipfirst char(3) 
AS 

declare @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40), @DDANUM char(13), @SSN char(9), @Cardtype char(1), @accttype char(20), @accttypedesc char(50), @OLDCC nchar(16), @period nchar(14)
declare @DBName varchar(50), @SQLupdate nvarchar(2000), @SQLIf nvarchar(2000)
set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlupdate=N''Truncate table '' + QuoteName(@DBName) + N''.dbo.COMB_IN''
Exec sp_executesql @SQLUPDATE

set @sqlupdate=N''insert into '' + QuoteName(@DBName) + N''.dbo.comb_in (TIP_PRI, TIP_SEC)
			select Newtipnumber, oldtipnumber
			from FBOPManualCombine
			where oldtipnumber<>newtipnumber and left(newtipnumber,3)=@Tipfirst ''
Exec sp_executesql @SQLUPDATE, N''@Tipfirst char(3)'', @Tipfirst = @Tipfirst 

set @sqlupdate=N''delete from '' + QuoteName(@DBName) + N''.dbo.COMB_IN
			where TIP_PRI not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.affiliat) ''
Exec sp_executesql @SQLUPDATE

set @sqlupdate=N''delete from '' + QuoteName(@DBName) + N''.dbo.COMB_IN
			where TIP_SEC not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.affiliat) ''
Exec sp_executesql @SQLUPDATE' 
END
GO
