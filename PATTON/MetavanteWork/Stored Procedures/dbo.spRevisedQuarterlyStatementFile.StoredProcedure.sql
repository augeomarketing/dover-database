USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spRevisedQuarterlyStatementFile]    Script Date: 09/30/2010 11:28:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedQuarterlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRevisedQuarterlyStatementFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRevisedQuarterlyStatementFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 10/2010
-- Description:	Comment out section SEB001
-- =============================================
CREATE PROCEDURE [dbo].[spRevisedQuarterlyStatementFile]  
	-- Add the parameters for the stored procedure here
				@StartDateParm char(10), 
				@EndDateParm char(10), 
				@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

	declare @DBName varchar(50), 
			@SQLUpdate nvarchar(1000),  
			@MonthBucket char(10), 
			@SQLTruncate nvarchar(1000), 
			@SQLInsert nvarchar(1000), 
			@monthbegin char(2), 
			@SQLSelect nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
	set @MonthBucket=''MonthBeg'' + @monthbegin

	/*******************************************************************************/
	/*******************************************************************************/
	/*                                                                             */
	/*          ISSUES WITH ADJUSTMENTS                                            */
	/*                                                                             */
	/*******************************************************************************/
	/*******************************************************************************/
	/* Load the statement file from the customer table  */
	Truncate Table dbo.Revised_Quarterly_Statement 


	if exists(select name from sysobjects where name=''wrkhist'')
	begin 
		truncate table dbo.wrkhist
	end

	set @SQLSelect=''insert into wrkhist
			select tipnumber, trancode, sum(points) as points from '' + QuoteName(@DBName) + N''.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode ''
	Exec sp_executesql @SQLSelect, N''@StartDate DateTime, @EndDate DateTime'', @StartDate=@StartDate, @EndDate=@EndDate 

	set @SQLInsert=''INSERT INTO dbo.Revised_Quarterly_Statement(tip, acctname1, acctname2, address1, address2, city, state, zip)
        				select tipnumber, acctname1, acctname2, address1, address2, rtrim(city), rtrim(state), left(zipcode,5)
					from '' + QuoteName(@DBName) + N''.dbo.customer where status=''''A'''' order by tipnumber asc''
	Exec sp_executesql @SQLInsert


	/* Load the statmement file with CREDIT purchases   63       */

	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set CRPurch=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode=''''63'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT purchases   67       */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set DBPurch=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode=''''67'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with HELOC purchases   67       */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set HELPurch=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode=''''6H'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Business purchases   6B       */
	/* SEB003  */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set BUSPurch=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode=''''6B'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with bonuses            */
	/* SEB002  */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set CRBonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and trancode in (''''FJ'''', ''''FG'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and trancode in (''''FJ'''', ''''FG'''')) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with debit bonuses            */

/* SEB001	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set DBBonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and trancode in(''''FC'''', ''''FD'''', ''''FI'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and trancode in(''''FC'''', ''''FD'''', ''''FI'''' )) ''
		Exec sp_executesql @SQLUpdate  */
		

	/* Load the statmement file with HELOC bonuses            */
	/* SEB002 */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set HELBonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode = ''''FH'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode = ''''FH'''' )) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with business bonuses            */
	/* SEB003   */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set BUSBonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and trancode in(''''F5'''', ''''F6'''', ''''F7'''', ''''F8'''')) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and trancode in(''''F5'''', ''''F6'''', ''''F7'''', ''''F8'''')) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Employee Incentive CR            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set EmpCRBonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode in (''''FF''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode in (''''FF'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Employee Incentive DB            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set EmpDBBonus=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode in (''''FE''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode in (''''FE'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Points2U Plus bonus           */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set P2UPlusBonus =
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode in (''''FA''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode in (''''FA'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Generic bonuses            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set BONUS=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode like ''''B%'''' and trancode<>''''BX'''') ) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode like ''''B%'''' and trancode<>''''BX'''') ) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Generic bonuses            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set BONUS= BONUS - 
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode in (''''BX''''))) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode in (''''BX'''' ))) ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with plus adjustments    */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AdjustAdd=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''IE'''' '' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AdjustAdd = AdjustAdd +
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''TR'''' '' 	
		Exec sp_executesql @SQLUpdate 
		
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AdjustAdd = AdjustAdd +
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''TP'''' '' 	
		Exec sp_executesql @SQLUpdate 

/* Load the statmement file with Direct Debosit Bonuses             */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set DirDepBonus = DirDepBonus + 
			isnull((select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode = ''''FD'''')),0) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode = ''''FD'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set DirDepBonus = 0 
			where DirDepBonus is null'' 	
		Exec sp_executesql @SQLUpdate 	

/* Load the statmement file with Pre Authorized Withdrawals Bonuses             */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set WthdrwlBonus = WthdrwlBonus +
			isnull((select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode = ''''G5'''')),0) 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and (trancode = ''''G5'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set WthdrwlBonus = 0 
			where WthdrwlBonus is null'' 	
		Exec sp_executesql @SQLUpdate 	

/* Load the statmement file with FI E-statement selection Bonuses             */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set EstmtFIBonus  = EstmtFIBonus + 
			isnull((select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode = ''''G6'''')),0) 	
			where exists(select * from wrkhist where tipnumber = dbo.Revised_Quarterly_Statement.tip and (trancode = ''''G6'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set EstmtFIBonus  = 0 
			where EstmtFIBonus is null'' 	
		Exec sp_executesql @SQLUpdate 	

/* Load the statmement file with Avg Deposit Bonuses             */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AvgDepositBonus= AvgDepositBonus +
			isnull((select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode = ''''G4'''')),0) 	
			where exists(select * from wrkhist where tipnumber= dbo.Revised_Quarterly_Statement.tip and (trancode = ''''G4'''')) ''
		Exec sp_executesql @SQLUpdate 
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AvgDepositBonus= 0 
			where AvgDepositBonus is null'' 	
		Exec sp_executesql @SQLUpdate 	


	/* Load the statmement file with total point increases */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set TotalAdd= CRPurch + DBPurch + HELPurch + BUSPurch + CRBonus + DBBonus + HELBonus + BUSBonus + EmpCRBonus + EmpDBBonus + P2UPlusBonus + BONUS + AdjustAdd + DirDepBonus + WthdrwlBonus + EstmtFIBonus + AvgDepositBonus ''
		Exec sp_executesql @SQLUpdate

	/* Load the statmement file with redemptions          */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set Redeem=
			(select sum(points) from wrkhist 
			where tipnumber= dbo.Revised_Quarterly_Statement.tip and trancode like ''''R%'''') 	
			where exists(select * from wrkhist where tipnumber=dbo.Revised_Quarterly_Statement.tip and trancode like ''''R%'''') ''
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set Redeem=Redeem +
			b.points 
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''IR'''' '' 	
		Exec sp_executesql @SQLUpdate 

	
	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set Redeem=Redeem -
			b.points 
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''DR'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with CREDIT returns            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set ReturnCR=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''33'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with DEBIT returns            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set ReturnDB=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''37'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with HELOC returns            */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set ReturnHEL=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''3H'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with Business returns            */
	/* SEB003   */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set ReturnBUS=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''3B'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with minus adjustments    */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AdjustSub=
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''DE'''' '' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AdjustSub=AdjustSub +
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''FR'''' '' 	
		Exec sp_executesql @SQLUpdate 

	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set AdjustSub=AdjustSub +
			b.points
			from dbo.Revised_Quarterly_Statement a, wrkhist b 
			where a.tip = b.tipnumber and b.trancode = ''''XP'''' '' 	
		Exec sp_executesql @SQLUpdate 

	/* Load the statmement file with total point decreases */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set TotalSub=Redeem + ReturnCR + ReturnDB + ReturnHEL + ReturnBUS + AdjustSub ''
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with the Beginning balance for the Month */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set PNTBEG = b.'' + Quotename(@MonthBucket) + N'' 
			from dbo.Revised_Quarterly_Statement a, '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table b
			where a.tip = b.tipnumber ''
	exec sp_executesql @SQLUpdate

	/* Load the statmement file with ending points */
	set @SQLUpdate=N''update dbo.Revised_Quarterly_Statement set PNTEND= PNTBEG + TotalAdd - TotalSub ''
	exec sp_executesql @SQLUpdate

	

END
' 
END
GO
