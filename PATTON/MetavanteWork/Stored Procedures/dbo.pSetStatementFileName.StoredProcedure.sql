USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[pSetStatementFileName]    Script Date: 02/18/2010 09:12:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetStatementFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetStatementFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetStatementFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSetStatementFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

/*********************************************************/
/* Date: 7/08                                            */
/* By: S. Blanchette                                     */
/* SCAN: SEB001                                          */
/* REASON: to put record count as part of name           */
/*                                                       */
/*********************************************************/
/*********************************************************/
/* Date: 7/08                                            */
/* By: S. Blanchette                                     */
/* SCAN: SEB002                                          */
/* REASON: to put full date    as part of name           */
/*                                                       */
/*********************************************************/

declare @Filename char(50), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
/*****************/
/* Start SEB001  */
/*****************/
declare @DBName varchar(100), @NumRecs numeric, @SQLSelect nvarchar(1000)
set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

set @SQLSelect=''set @NumRecs=(select count(*) from '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File) ''
Exec sp_executesql @SQLSelect, N''@NumRecs numeric output'', @NumRecs=@NumRecs output 
/*****************/
/* END SEB001  */
/*****************/

/*****************/
/* Start SEB001  */
/*****************/

-- set @endingDate=(select top 1 datein from DateforAudit) 
--Section to put together the current date.  Handle problem with date returning a one position month and day
--
-- set @workmonth=left(@endingdate,2) 
-- set @workyear=right(@endingdate,2) 
-- set @currentdate=@workmonth + @workyear 

set @currentdate=(select top 1 datein from DateforAudit) 
set @currentdate=REPLACE(@currentdate,''/'',''_'') 

/*****************/
/* END SEB001  */
/*****************/

/* SEB001 set @filename=''W'' + @TipPrefix + @currentdate + ''.xls'' */
/* SEB001 */set @filename=''S'' + @TipPrefix + ''_'' + @currentdate + ''-'' + rtrim(cast(@NumRecs as char(10))) + ''.xls''
 
set @newname=''O:\5xx\Output\Statements\Statement.bat '' + @filename
set @ConnectionString=''O:\5xx\Output\Statements\'' + @filename' 
END
GO
