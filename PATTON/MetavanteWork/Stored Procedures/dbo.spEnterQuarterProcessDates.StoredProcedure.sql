USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spEnterQuarterProcessDates]    Script Date: 02/18/2010 09:12:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEnterQuarterProcessDates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spEnterQuarterProcessDates]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEnterQuarterProcessDates]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spEnterQuarterProcessDates] @BeginDate nchar(10), @EndDate nchar(10)
AS

declare @monthbegin nchar(2)

set @monthbegin=left(@begindate,2)

if right(@monthbegin,1)=''/''
	begin
		set @begindate=''0'' + right(rtrim(@begindate),9)
	end

set @monthbegin=left(@EndDate,2)

if right(@monthbegin,1)=''/''
	begin
		set @EndDate=''0'' + right(rtrim(@EndDate),9)
	end

update ProcessingDates
set QuarterBeginDate=@begindate, QuarterEndDate=@EndDate' 
END
GO
