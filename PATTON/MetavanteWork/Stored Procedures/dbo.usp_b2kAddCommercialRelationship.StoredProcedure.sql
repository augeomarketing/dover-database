USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kAddCommercialRelationship]    Script Date: 05/03/2012 18:08:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kAddCommercialRelationship]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kAddCommercialRelationship]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kAddCommercialRelationship]    Script Date: 05/03/2012 18:08:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_b2kAddCommercialRelationship]
AS

	UPDATE CCI
	SET OLDCC = DMG.dim_b2kdemog_relationshipaccount
	FROM CREDITCARDIN CCI
	inner join b2kDemog DMG
	ON CCI.ACCTNUM = DMG.dim_b2kdemog_cardnumber
		and CCI.ACCTNUM <> DMG.dim_b2kdemog_relationshipaccount
	inner join CRTipFirst_Table TFT
		on DMG.dim_b2kdemog_binplan = TFT.Bin
		and DMG.dim_b2kdemog_productline = TFT.ProductLine
		and DMG.dim_b2kdemog_subproducttype = TFT.SubProductType
		and TFT.HasCommercial IN (1,2)
	where 1=1
		and Rewardsnow.dbo.fnCheckLuhn10(dim_b2kdemog_relationshipaccount) = 1
		and dim_b2kdemog_relationshipaccount not like '0%'
		and dim_b2kdemog_relationshipaccount not like ' %'
		and CCI.OLDCC IS NULL

GO

