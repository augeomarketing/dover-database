USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_HandleOptOuts_v2]    Script Date: 01/27/2012 14:42:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_HandleOptOuts_v2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_HandleOptOuts_v2]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_HandleOptOuts_v2]    Script Date: 01/27/2012 14:42:49 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



Create Procedure [dbo].[usp_HandleOptOuts_v2]
	@Tipfirst varchar(3)

AS

BEGIN
	SET NOCOUNT ON;
--------------------------------------------------------------------------------------------------------------------------------------
/* Remove CreditCardIn for Opt Out people  */
--------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM CreditCardIn WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = CreditCardIn.acctnum)
DELETE FROM CreditCardIn WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = CreditCardIn.OLDCC)
--------------------------------------------------------------------------------------------------------------------------------------
/* Remove DebitCardIn for Opt Out people  */
--------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM debitcardin WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = debitcardin.acctnum)
DELETE FROM debitcardin WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = debitcardin.OLDCC)
DELETE FROM debitcardin WHERE exists (SELECT * FROM DDAOptOutControl WHERE DDANUMBER = debitcardin.DDANUM and left(tipnumber,3) = @Tipfirst )
--------------------------------------------------------------------------------------------------------------------------------------
/* Remove DebitCardInKroger for Opt Out people  */
--------------------------------------------------------------------------------------------------------------------------------------
--DELETE FROM debitcardinKroger WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = debitcardinKroger.[Primary Acct Number])
--DELETE FROM debitcardinKroger WHERE exists (SELECT * FROM DDAOptOutControl WHERE DDANUMBER = debitcardinKroger.[DDA Acct Number] and left(tipnumber,3) = @Tipfirst)
--------------------------------------------------------------------------------------------------------------------------------------
/* Remove DebitCardInLakeside for Opt Out people  */
--------------------------------------------------------------------------------------------------------------------------------------
--DELETE FROM debitcardinLakeside WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = debitcardinLakeside.[Primary Acct Number])
--DELETE FROM debitcardinLakeside WHERE exists (SELECT * FROM DDAOptOutControl WHERE DDANUMBER = debitcardinLakeside.[DDA Acct Number] and left(tipnumber,3) = @Tipfirst)
--------------------------------------------------------------------------------------------------------------------------------------
/* Remove HELOCCardIn for Opt Out people  */
--------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM  HELOCcardin WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = HELOCcardin.acctnum)
DELETE FROM  HELOCcardin WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = HELOCcardin.OLDCC)
--------------------------------------------------------------------------------------------------------------------------------------
/* Remove from CardsIn for Opt Out people  */
--------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM cardsin WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = dbo.Cardsin.ACCTNUM)
DELETE FROM cardsin WHERE exists (SELECT * FROM optoutcontrol WHERE acctnumber = dbo.Cardsin.OLDCC)

END

GO
