USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kPopulateCreditCardInMidMonth]    Script Date: 01/19/2011 15:37:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_b2kPopulateCreditCardInMidMonth]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_b2kPopulateCreditCardInMidMonth]
GO

USE [MetavanteWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_b2kPopulateCreditCardInMidMonth]    Script Date: 01/19/2011 15:37:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_b2kPopulateCreditCardInMidMonth]
as
begin
	DELETE CCI 
	FROM CREDITCARDIN CCI
	INNER JOIN vw_b2kCreditCardHolding CCH
		ON CCI.ACCTNUM = CCH.acctnum
		 
	insert into CREDITCARDIN
	(
		SSN
		, BANK
		, AGENT
		, ACCTNUM
		, OLDCC
		, NA1
		, NA2
		, ADDR1
		, ADDR2
		, ADDR3
		, CITYSTATE
		, ZIP
		, STATUS
		, PHONE1
		, PHONE2
		, NUMPURCH
		, PURCH
		, NUMRET
		, AMTRET
		, PERIOD
		, LASTNAME
	)
	SELECT
		ssn
		, bank
		, agent
		, acctnum
		, oldcc
		, na1
		, na2
		, addr1
		, addr2
		, addr3
		, citystate
		, zip
		, status
		, phone1
		, phone2
		, numpurch
		, purch
		, numret
		, amtret
		, period
		, lastname
	FROM
		vw_b2kCreditCardHoldingMidMonth
end


GO

