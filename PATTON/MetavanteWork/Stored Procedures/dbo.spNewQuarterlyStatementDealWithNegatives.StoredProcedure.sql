USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewQuarterlyStatementDealWithNegatives]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyStatementDealWithNegatives]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewQuarterlyStatementDealWithNegatives]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewQuarterlyStatementDealWithNegatives]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spNewQuarterlyStatementDealWithNegatives] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
insert into [New_Quarterly_Statement_Detail_Negatives]
select *
from [New_Quarterly_Statement_Detail]
where	[PointsBegin] < 0 or
		[PointsEnd] < 0 or	
		[PointsPurchasedCR] < 0 or
		[PointsPurchasedDB] < 0 or 
		[PointsPurchasedHE] < 0 or 
		[PointsPurchasedBUS] < 0 or 
		[PointsBonusCR] < 0 or 
		[PointsBonusDB] < 0 or 
		[PointsBonusHE] < 0 or 
		[PointsBonusBUS] < 0 or 
		[PointsBonusEmpCR] < 0 or 
		[PointsBonusEmpDB] < 0 or 
		[PointsBonus] < 0 or 
		[PointsAdded] < 0 or 
		[PointsIncreased] < 0 or 
		[PointsRedeemed] < 0 or 
		[PointsReturnedCR] < 0 or 
		[PointsReturnedDB] < 0 or 
		[PointsReturnedHE] < 0 or 
		[PointsReturnedBUS] < 0 or 
		[PointsSubtracted] < 0 or 
		[PointsDecreased] < 0 or 
		[PNTDEBIT] < 0 or 
		[PNTMORT] < 0 or 
		[PNTHOME] < 0 or 
		[PointsToExpire] < 0 or 
		[PointsBonusP2UPlus] < 0 
	
delete from [New_Quarterly_Statement_Detail]
where	[PointsBegin] < 0 or
		[PointsEnd] < 0 or	
		[PointsPurchasedCR] < 0 or
		[PointsPurchasedDB] < 0 or 
		[PointsPurchasedHE] < 0 or 
		[PointsPurchasedBUS] < 0 or 
		[PointsBonusCR] < 0 or 
		[PointsBonusDB] < 0 or 
		[PointsBonusHE] < 0 or 
		[PointsBonusBUS] < 0 or 
		[PointsBonusEmpCR] < 0 or 
		[PointsBonusEmpDB] < 0 or 
		[PointsBonus] < 0 or 
		[PointsAdded] < 0 or 
		[PointsIncreased] < 0 or 
		[PointsRedeemed] < 0 or 
		[PointsReturnedCR] < 0 or 
		[PointsReturnedDB] < 0 or 
		[PointsReturnedHE] < 0 or 
		[PointsReturnedBUS] < 0 or 
		[PointsSubtracted] < 0 or 
		[PointsDecreased] < 0 or 
		[PNTDEBIT] < 0 or 
		[PNTMORT] < 0 or 
		[PNTHOME] < 0 or 
		[PointsToExpire] < 0 or 
		[PointsBonusP2UPlus] < 0  
END
' 
END
GO
