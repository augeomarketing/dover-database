USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbersLakeside]    Script Date: 07/12/2010 16:25:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbersLakeside]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetExistingTipNumbersLakeside]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetExistingTipNumbersLakeside]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2010
-- Description:	Get existing tips
-- =============================================
CREATE PROCEDURE [dbo].[spGetExistingTipNumbersLakeside]
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 /* Revision                                   */
	/* By Sarah Blanchette                         */
	/* Date 11/24/2007                             */
	/* SCAN  SEB001                                */
	/* Remove name check on auto combine per Doug at FBOP   */

	/* By Sarah Blanchette                         */
	/* Date 2/2009                             */
	/* SCAN  SEB002                                */
	/* Change statement to get top 1 because transactions are getting 2 different tips */
	declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)

	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	/*  Get tipnumber based on account number      */
	set @SQLUpdate=''update cardsin 
			set tipnumber=b.tipnumber
			from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
			where a.tipnumber is null and a.acctnum=b.acctid ''
	Exec sp_executesql @SQLUpdate

	/*  Get tipnumber based on old account number  */
	set @SQLUpdate=''update cardsin 
			set tipnumber=b.tipnumber
			from cardsin a, '' + QuoteName(@DBName) + N''.dbo.affiliat b
			where a.tipnumber is null and a.oldcc=b.acctid ''
	Exec sp_executesql @SQLUpdate

	/*  Get tipnumber based on ddanumber as long as joint codes are the same      */
	drop table [wrktab3]

	select distinct ddanum, tipnumber 
	into [wrktab3]
	from cardsin
	where ddanum is not null and tipnumber is not null and not exists(select * from CorporatePrefixes where TipPrefix=TipFirst)

	update cardsin
	set tipnumber=[wrktab3].tipnumber
	from cardsin, [wrktab3]
	where cardsin.ddanum is not null and cardsin.ddanum not like ''0000000000%'' and cardsin.tipnumber is null and cardsin.ddanum=[wrktab3].ddanum 

	/*  Get tipnumber based on ssn na1 na2 joint are the same      */
	update cardsin
	set na2='' ''
	where na2 is null or Len(na2)<''1''  /* 09/15/2009 */

	delete from cardsin
	where tipnumber is null and status<>''A''

END
' 
END
GO
