USE [MetavanteWork]
GO
/****** Object:  StoredProcedure [dbo].[spNewSetStatementFileName]    Script Date: 02/18/2010 09:13:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewSetStatementFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spNewSetStatementFileName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spNewSetStatementFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spNewSetStatementFileName] 
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
/*********************************************************/
/* Date: 7/08                                            */
/* By: S. Blanchette                                     */
/* SCAN: SEB002                                          */
/* REASON: to put full date    as part of name           */
/*                                                       */
/*********************************************************/

declare @Filename char(100), @currentdate nchar(10), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
/*****************/
/* Start SEB001  */
/*****************/
declare @DBName varchar(100), @NumRecs numeric, @SQLSelect nvarchar(1000)
set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipPrefix)

/*****************/
/* END SEB001  */
/*****************/

/*****************/
/* Start SEB001  */
/*****************/

-- set @endingDate=(select top 1 datein from DateforAudit) 
--Section to put together the current date.  Handle problem with date returning a one position month and day
--
-- set @workmonth=left(@endingdate,2) 
-- set @workyear=right(@endingdate,2) 
-- set @currentdate=@workmonth + @workyear 

set @currentdate=(select top 1 datein from DateforAudit) 
--set @currentdate=REPLACE(@currentdate,''/'','''') 
set @currentdate=right(@currentdate,4) + left(@currentdate,2) + substring(@currentdate,4,2)

/*****************/
/* END SEB001  */
/*****************/

/* SEB001 set @filename=''W'' + @TipPrefix + @currentdate + ''.xls'' */
/* SEB001 */set @filename=''REWARDS-STMTS-AD.'' + @TipPrefix + ''.'' + left(@currentdate,8) + REPLACE((SELECT CONVERT(VARCHAR(10), GETDATE(), 108) AS [CurrentTime]),'':'','''') + ''.txt''
 
set @newname=''O:\5xx\Output\Statements\NewStatement.bat '' + @filename
--set @ConnectionString=''O:\5xx\Output\Statements\'' + @filename
END
' 
END
GO
