USE MetavanteWork
GO


CREATE TRIGGER crtipfirsttable_audit
ON MetavanteWork.dbo.CRTipFirst_Table
AFTER INSERT, UPDATE, DELETE
AS
	--DETERMINE ACTION TYPE
	declare @type VARCHAR(1)
	declare @inscount int
	declare @delcount int
	
	select @inscount = count(*) from inserted
	select @delcount = count(*) from deleted
	
	if @delcount > 0 and @inscount > 0
	begin
		insert into RewardsNow.dbo.robCRTipFirstTableAudit (Bank, Agent, TipFirst, BankName, Bin, ProductLine, SubProductType, DateModified, ActionTaken, LoginID)
		select Bank, Agent, TipFirst, BankName, Bin, ProductLine, SubProductType, getdate(), 'C', system_user from deleted
	end
	
	if @delcount > 0 and @inscount = 0
	begin
		insert into RewardsNow.dbo.robCRTipFirstTableAudit (Bank, Agent, TipFirst, BankName, Bin, ProductLine, SubProductType, DateModified, ActionTaken, LoginID)
		select Bank, Agent, TipFirst, BankName, Bin, ProductLine, SubProductType, getdate(), 'D', system_user from deleted
	end
	
	if @delcount = 0 and @inscount > 0
	begin
		insert into RewardsNow.dbo.robCRTipFirstTableAudit (Bank, Agent, TipFirst, BankName, Bin, ProductLine, SubProductType, DateModified, ActionTaken, LoginID)
		select Bank, Agent, TipFirst, BankName, Bin, ProductLine, SubProductType, getdate(), 'A', system_user from inserted
	end
GO

