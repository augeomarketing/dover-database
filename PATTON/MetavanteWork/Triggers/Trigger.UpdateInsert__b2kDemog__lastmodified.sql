USE MetavanteWork
GO

IF OBJECT_ID(N'UpdateInsert__b2kDemog__lastmodified') IS NOT NULL
	DROP TRIGGER UpdateInsert__b2kDemog__lastmodified
GO

CREATE TRIGGER UpdateInsert__b2kDemog__lastmodified
ON b2kDemog

FOR  INSERT, UPDATE
AS

SET NOCOUNT ON
SET XACT_ABORT ON 
SET ARITHABORT ON 

DECLARE	
	@Now smalldatetime

SELECT	@Now = cast(GetDate() as smalldatetime)
UPDATE	U
SET
	dim_b2kDemog_lastmodified = @Now
FROM	inserted AS I
INNER JOIN b2kDemog AS U
	ON U.sid_b2kDemog_id = I.sid_b2kDemog_id
GO
