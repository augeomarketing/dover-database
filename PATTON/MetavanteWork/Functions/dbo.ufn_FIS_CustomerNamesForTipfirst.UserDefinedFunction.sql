USE [MetavanteWork]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_FIS_CustomerNamesForTipfirst]    Script Date: 02/14/2013 09:23:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_FIS_CustomerNamesForTipfirst]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_FIS_CustomerNamesForTipfirst]
GO

USE [MetavanteWork]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_FIS_CustomerNamesForTipfirst]    Script Date: 02/14/2013 09:23:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
CREATE FUNCTION [dbo].[ufn_FIS_CustomerNamesForTipfirst] ()  
RETURNS @namelist TABLE  
 (  
  tipnumber varchar(15)  
  , acctname1 varchar(100)  
  , acctname2 varchar(100)  
  , acctname3 varchar(100)  
  , acctname4 varchar(100)  
  , acctname5 varchar(100)  
  , acctname6 varchar(100)  
  , acctname7 varchar(100)  
  , acctname8 varchar(100)  
  , acctname9 varchar(100)  
  , acctname10 varchar(100)  
  , acctname11 varchar(100)  
  , acctname12 varchar(100)  
  , acctname13 varchar(100)  
  , acctname14 varchar(100)  
  , acctname15 varchar(100)  
  , acctname16 varchar(100)  
  , acctname17 varchar(100)  
  , acctname18 varchar(100)  
  , acctname19 varchar(100)  
  , acctname20 varchar(100)  
 )  
AS  
BEGIN  




  
 DECLARE @namesort TABLE  
 (  
  sid_namesort_id BIGINT IDENTITY(1,1)  
  , tipnumber VARCHAR(15)  
  , name1 VARCHAR(40)  
  , name2 VARCHAR(40)  
  , name3 VARCHAR(40)  
  , name4 VARCHAR(40)  
  , name5 VARCHAR(40)  
  , name6 VARCHAR(40)  
 )  

--INSERT BUSINESS DATA FIRST (IF ANY)
  
 INSERT INTO @namesort  
 (  
  tipnumber, name1, name2, name3, name4, name5, name6
 )  
 select distinct
	tipnumber
	, isnull(na1, '')
	, isnull(na2, '')
	, isnull(na3, '')
	, isnull(na4, '')
	, isnull(na5, '')
	, isnull(na6, '')
 from cardsin_view_by_tipnumber ci
 inner join b2kdemog dm
	on ci.acctnum = dm.dim_b2kdemog_relationshipaccount
 where ci.[status] <> 'C'
 order by ci.tipnumber

-- INSERT REMAINING DATA

 INSERT INTO @namesort  
 (  
  tipnumber, name1, name2, name3, name4, name5, name6
 )  
 select distinct
	tipnumber
	, isnull(na1, '')
	, isnull(na2, '')
	, isnull(na3, '')
	, isnull(na4, '')
	, isnull(na5, '')
	, isnull(na6, '')
 from cardsin_view_by_tipnumber ci
 left outer join b2kdemog dm
	on ci.acctnum = dm.dim_b2kdemog_relationshipaccount
 where dm.dim_b2kdemog_relationshipaccount IS NULL
   and ci.[status] <> 'C'
 order by ci.tipnumber

--INSERT WHERE ALL CARDS ON TIP ARE CLOSED

 INSERT INTO @namesort  
 (  
  tipnumber, name1, name2, name3, name4, name5, name6
 )  
 select distinct
	ci.tipnumber
	, isnull(na1, '')
	, isnull(na2, '')
	, isnull(na3, '')
	, isnull(na4, '')
	, isnull(na5, '')
	, isnull(na6, '')
 from cardsin_view_by_tipnumber ci
 inner join
 (
	select cc.tipnumber 
	from
	(
		SELECT TIPNUMBER, COUNT(*) 
		AS ALLCARDS 
		FROM cardsin_view_by_tipnumber 
		GROUP BY TIPNUMBER
	) AC
	INNER JOIN 
	(
		SELECT TIPNUMBER, COUNT(*) AS CLOSEDCARDS 
		FROM cardsin_view_by_tipnumber 
		WHERE STATUS = 'C' 
		GROUP BY TIPNUMBER
	) CC
	ON AC.TIPNUMBER = CC.TIPNUMBER
	AND AC.ALLCARDS = CC.CLOSEDCARDS
 ) ccds
 ON ccds.tipnumber = ci.tipnumber

  
 DECLARE @names TABLE  
 (  
  sid_names_id INT IDENTITY(1,1) PRIMARY KEY  
  , dim_names_tipnumber varchar(15)  
  , dim_names_name VARCHAR(max)  
 );  
  
 DECLARE @names2 TABLE  
 (  
  sid_names_id INT PRIMARY KEY  
  , dim_names_tipnumber varchar(15)  
  , dim_names_name VARCHAR(max)  
  , dim_names_rank VARCHAR(100)  
 );  
  
 INSERT INTO @names (dim_names_tipnumber, dim_names_name)  
 SELECT tipnumber, listname  
 FROM  
  @namesort n  
 UNPIVOT  
 (  
  listname FOR fieldlist in ([Name1], [Name2], [Name3], [Name4], [Name5], [Name6])  
 ) as unpvt  
 WHERE listname <> ''
 ORDER BY sid_namesort_id  
  
 --DEDUPE NAMES  
  
 INSERT INTO @names2(sid_names_id, dim_names_tipnumber, dim_names_name, dim_names_rank)  
 SELECT nms.sid_names_id, nms.dim_names_tipnumber, nms.dim_names_name  
   , 'ACCTNAME' + CAST(RANK() OVER (PARTITION BY nms.dim_names_tipnumber ORDER BY (nms.sid_names_id)) AS VARCHAR)   
 FROM @names nms  
 INNER JOIN  
 (  
  SELECT MIN(sid_names_id) sid_names_id, dim_names_tipnumber, dim_names_name  
  FROM @names  
  GROUP BY dim_names_tipnumber, dim_names_name  
 ) unq  
 ON nms.sid_names_id = unq.sid_names_id  
 ORDER BY nms.sid_names_id;  
  
 INSERT INTO @namelist  
 (  
  TIPNUMBER  
  , acctname1  
  , acctname2  
  , acctname3  
  , acctname4  
  , acctname5  
  , acctname6  
  , acctname7  
  , acctname8  
  , acctname9  
  , acctname10  
  , acctname11  
  , acctname12  
  , acctname13  
  , acctname14  
  , acctname15  
  , acctname16  
  , acctname17  
  , acctname18  
  , acctname19  
  , acctname20  
 )  
 SELECT dim_names_tipnumber  
  , [acctname1] AS acctname1  
  , [acctname2] AS acctname2  
  , [acctname3] AS acctname3  
  , [acctname4] AS acctname4  
  , [acctname5] AS acctname5  
  , [acctname6] AS acctname6  
  , [acctname7] AS acctname7  
  , [acctname8] AS acctname8  
  , [acctname9] AS acctname9  
  , [acctname10] AS acctname10  
  , [acctname11] AS acctname11  
  , [acctname12] AS acctname12  
  , [acctname13] AS acctname13  
  , [acctname14] AS acctname14  
  , [acctname15] AS acctname15  
  , [acctname16] AS acctname16  
  , [acctname17] AS acctname17  
  , [acctname18] AS acctname18  
  , [acctname19] AS acctname19  
  , [acctname20] AS acctname20  
 FROM  
 (SELECT dim_names_tipnumber, dim_names_name, dim_names_rank FROM @names2) nms  
 PIVOT (MIN (dim_names_name) FOR dim_names_rank IN 
	(
		[acctname1], [acctname2], [acctname3], [acctname4], [acctname5]
		, [acctname6], [acctname7], [acctname8], [acctname9], [acctname10]
		, [acctname11], [acctname12], [acctname13], [acctname14], [acctname15]
		, [acctname16], [acctname17], [acctname18], [acctname19], [acctname20]
	)  
 ) AS PVT;  
   
 RETURN;  
END  


GO


