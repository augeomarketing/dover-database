USE [MetavanteWork]
GO
/****** Object:  UserDefinedFunction [dbo].[fnLinkCheckingNames]    Script Date: 02/18/2010 09:13:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnLinkCheckingNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnLinkCheckingNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnLinkCheckingNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
CREATE function [dbo].[fnLinkCheckingNames] (@DDA nvarchar(25))

RETURNS @LinkedNames table(DDA nvarchar(25) primary key, Name1 nvarchar(35), SSN nvarchar(9), Name2 nvarchar(35), 
						Name3 nvarchar(35), Name4 nvarchar(35), Name5 nvarchar(35),
						Name6 nvarchar(35) )

as
--
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2009   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to omit closed records from being part of name generation

BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(35), SSN nvarchar(9))

	-- Get distinct names from NAME1 column for rows that match the @DDA parm
	insert into @names
	select distinct NA1, ssn
	from dbo.debitcardin
	where ddanum = @DDA 
		and status<>''C'' --SEB001

	-- Get distinct names from NAME2 column for rows that match the @DDA parm
	insert into @names
	select distinct NA2, ssn
	from dbo.debitcardin
	where ddanum = @DDA and
		NA2 not in (select AcctName from @Names)
		and status<>''C'' --SEB001

	-- Get distinct names from NAME2 column for rows that match the @DDA parm
	insert into @names
	select distinct NA3, ssn
	from dbo.debitcardin
	where ddanum = @DDA and
		NA3 not in (select AcctName from @Names)
		and status<>''C'' --SEB001

	-- Get distinct names from NAME2 column for rows that match the @DDA parm
	insert into @names
	select distinct NA4, ssn
	from dbo.debitcardin
	where ddanum = @DDA and
		NA4 not in (select AcctName from @Names)
		and status<>''C'' --SEB001

	-- Get distinct names from NAME2 column for rows that match the @DDA parm
	insert into @names
	select distinct NA5, ssn
	from dbo.debitcardin
	where ddanum = @DDA and
		NA5 not in (select AcctName from @Names)
		and status<>''C'' --SEB001

	-- Get distinct names from NAME2 column for rows that match the @DDA parm
	insert into @names
	select distinct NA6, ssn
	from dbo.debitcardin
	where ddanum = @DDA and
		NA6 not in (select AcctName from @Names)
		and status<>''C'' --SEB001

	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
	select	@DDA as DDA, (select AcctName from @names where id=1) as Name1, (Select SSN from @names where id=1) as SSN,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END' 
END
GO
