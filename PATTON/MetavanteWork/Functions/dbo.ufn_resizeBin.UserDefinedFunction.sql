USE [MetavanteWork]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_resizeBin]    Script Date: 01/06/2011 08:52:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_resizeBin]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_resizeBin]
GO

USE [MetavanteWork]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_resizeBin]    Script Date: 01/06/2011 08:52:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_resizeBin] (@tipfirst varchar(3), @oldbin varchar(9))
RETURNS VARCHAR(9)
AS
BEGIN
      DECLARE @out VARCHAR(9)
      SET @out = @oldbin
      
      DECLARE @binmap TABLE (
            tipfirst varchar(3)
            , bin varchar(9)
      )
      
      INSERT INTO @binmap (tipfirst, bin)
      SELECT tipfirst, bin FROM MetavanteWork.dbo.BinMap
      
      DECLARE @binmatch INT
      DECLARE @mbinsize INT
      DECLARE @checklen INT
      DECLARE @mmatch INT
      
      SET @mbinsize = ISNULL((SELECT max(len(bin)) from @binmap WHERE tipfirst = @tipfirst), 0)
      IF @mbinsize > 0
      BEGIN
            SET @oldbin = rewardsnow.dbo.ufn_Trim(@oldbin)
            
            SET @checklen = @mbinsize
            WHILE @checklen > 0
            BEGIN
                  IF (SELECT COUNT(*) FROM @binmap WHERE bin = LEFT(@oldbin, @checklen)) > 0
                        BEGIN
                              SET @out = LEFT(@oldbin, @checklen)
                              SET @checklen = 1                         
                        END
                        --SET @maxmatch = @checklen
            
                  SET @checklen = @checklen - 1
            END
      END
      
      RETURN @out
END

GO


