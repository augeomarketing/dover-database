USE [MetavanteWork];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[BonusProgram] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusProgram]([sid_BonusProgram_ID], [dim_BonusProgram_Description], [dim_BonusProgramEffectiveDate], [dim_BonusProgramExpirationDate], [sid_BonusProgramType_ID], [sid_TranType_TranCode], [dim_BonusProgram_DateAdded], [dim_BonusProgram_LastUpdated])
SELECT 1, N'Double Points', '20101001 00:00:00.000', '20101231 00:00:00.000', N'C', N'BI', '20101117 11:04:44.597', '20101117 11:04:44.597'
COMMIT;
RAISERROR (N'[dbo].[BonusProgram]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[BonusProgram] OFF;
