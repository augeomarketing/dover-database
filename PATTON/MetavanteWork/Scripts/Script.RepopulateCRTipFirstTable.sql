USE [MetavanteWork];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[CRTipFirst_Table]([Bank], [Agent], [Tipfirst], [BankName], [Bin], [ProductLine], [SubProductType])
SELECT N'1379', N' 261', N'500', N'American Trust                                    ', N'447044', N'VGL', N'261' UNION ALL
SELECT N'1379', N' 263', N'500', N'American Trust                                    ', N'436134', N'VGL', N'263' UNION ALL
SELECT N'3319', N'2001', N'501', N'Illinois National                                 ', NULL, NULL, NULL UNION ALL
SELECT N'3319', N'2002', N'501', N'Illinois National                                 ', NULL, NULL, NULL UNION ALL
SELECT N'3319', N'2111', N'501', N'Illinois National                                 ', NULL, NULL, NULL UNION ALL
SELECT N'3319', N'2112', N'501', N'Illinois National                                 ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N' 385', N'502', N'Layton State Consumer                             ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N' 385', N'502', N'Layton State Consumer                             ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N'6385', N'502', N'Layton State Consumer                             ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 385', N'513', N'Layton State Commercial                           ', NULL, NULL, NULL UNION ALL
SELECT N'  70', N' 568', N'503', N'Bank of Marin Consumer                            ', NULL, NULL, NULL UNION ALL
SELECT N' 270', N' 568', N'503', N'Bank of Marin Consumer                            ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N'6568', N'503', N'Bank of Marin Consumer                            ', NULL, NULL, NULL UNION ALL
SELECT N'3257', N'4100', N'506', N'Rabobank Consumer                                 ', N'441556', N'VGL', N'410' UNION ALL
SELECT N'1382', N'4568', N'503', N'Bank of Marin Consumer                            ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N' 580', N'504', N'Hudson Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'1743', N' 580', N'504', N'Hudson Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N' 580', N'504', N'Hudson Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 580', N'504', N'Hudson Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 581', N'504', N'Hudson Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'3257', N'5100', N'506', N'Rabobank Consumer                                 ', N'441553', N'VGL', N'510' UNION ALL
SELECT N'3257', N'6100', N'506', N'Rabobank Consumer                                 ', N'441557', N'VGL', N'610' UNION ALL
SELECT N'3257', N'7300', N'506', N'Rabobank Consumer                                 ', N'441557', N'VGL', N'730' UNION ALL
SELECT N'3257', N'5200', N'506', N'Rabobank Consumer                                 ', N'441557', N'VGL', N'510' UNION ALL
SELECT N'3258', N'8100', N'506', N'Rabobank Consumer                                 ', N'441557', N'VPL', N'810' UNION ALL
SELECT N'3258', N'9100', N'506', N'Rabobank Consumer                                 ', N'441557', N'VPL', N'910' UNION ALL
SELECT N'3258', N'9300', N'506', N'Rabobank Consumer                                 ', N'441557', N'VPL', N'930' UNION ALL
SELECT N'5694', N'1500', N'507', N'Rabobank Commercial                               ', N'480824', N'VCM', N'150' UNION ALL
SELECT N'5694', N'2500', N'507', N'Rabobank Commercial                               ', N'480824', N'VCM', N'250' UNION ALL
SELECT N'5694', N'3500', N'507', N'Rabobank Commercial                               ', N'480824', N'VCM', N'350' UNION ALL
SELECT N'5694', N'4500', N'507', N'Rabobank Commercial                               ', N'480824', N'VCM', N'450' UNION ALL
SELECT N' 270', N' 380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N' 270', N'5380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N' 270', N'6380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N' 380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N'5380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N'6380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N' 380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N'5380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N'6380', N'508', N'Woodtrust Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 380', N'509', N'Woodtrust Commercial                              ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N' 117', N'511', N'Nebat Consumer                                    ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 117', N'512', N'Nebat Commercial                                  ', NULL, NULL, NULL UNION ALL
SELECT N'1323', N' 117', N'512', N'Nebat Commercial                                  ', NULL, NULL, NULL UNION ALL
SELECT N'3402', N'2201', N'520', N'S&T Commercial                                    ', NULL, NULL, NULL UNION ALL
SELECT N'3402', N'2202', N'520', N'S&T Commercial                                    ', NULL, NULL, NULL UNION ALL
SELECT N'2566', N' 683', N'523', N'Helm Bank Consumer                                ', NULL, NULL, NULL UNION ALL
SELECT N'6193', N'   1', N'525', N'Union Bank Consumer                               ', N'485258', N'VCL', N'001' UNION ALL
SELECT N'6193', N'   2', N'525', N'Union Bank Consumer                               ', N'485258', N'VPL', N'002' UNION ALL
SELECT N'6193', N'1001', N'525', N'Union Bank Consumer                               ', N'485258', NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[CRTipFirst_Table]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[CRTipFirst_Table]([Bank], [Agent], [Tipfirst], [BankName], [Bin], [ProductLine], [SubProductType])
SELECT N'6193', N'1002', N'525', N'Union Bank Consumer                               ', N'485258', NULL, NULL UNION ALL
SELECT N'5867', N'   3', N'526', N'Union Bank Commercial                             ', N'486551', N'VCC', N'003' UNION ALL
SELECT N'5867', N'1003', N'526', N'Union Bank Commercial                             ', N'486551', NULL, NULL UNION ALL
SELECT N'1559', N' 386', N'528', N'Park Bank Milwaukee Commercial                    ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 387', N'528', N'Park Bank Milwaukee Commercial                    ', NULL, NULL, NULL UNION ALL
SELECT N'4929', N'   3', N'529', N'Everbank Consumer                                 ', N'486524', N'VPL', N'003' UNION ALL
SELECT N'4929', N'   5', N'529', N'Everbank Consumer                                 ', N'486524', N'VPL', N'003' UNION ALL
SELECT N'4929', N'   7', N'529', N'Everbank Consumer                                 ', N'486524', N'VPL', N'003' UNION ALL
SELECT N'4929', N'   9', N'529', N'Everbank Consumer                                 ', N'486524', N'VPL', N'003' UNION ALL
SELECT N'4924', N'1982', N'537', N'California Business Bank Consumer                 ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N' 178', N'539', N'Byron Bank Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N' 178', N'539', N'Byron Bank Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'5694', N'4100', N'507', N'Rabobank Commercial                               ', N'480824', N'VCM', N'410' UNION ALL
SELECT N'5492', N'1001', N'545', N'Washington Trust Consumer                         ', N'403603', N'VPL', N'101' UNION ALL
SELECT N'5492', N'1003', N'545', N'Washington Trust Consumer                         ', N'403603', N'VPL', N'103' UNION ALL
SELECT N'1375', N'  57', N'569', N'Foundations Consumer                              ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N'  57', N'570', N'Foundations Commercial                            ', NULL, NULL, NULL UNION ALL
SELECT N'1375', N'  99', N'531', N'Lake Mills Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'1382', N'  99', N'531', N'Lake Mills Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'5494', N' 301', N'519', N'STBankHELOC                                       ', NULL, NULL, NULL UNION ALL
SELECT N'5494', N' 302', N'519', N'STBankHELOC                                       ', NULL, NULL, NULL UNION ALL
SELECT N'5494', N' 303', N'519', N'STBankHELOC                                       ', NULL, NULL, NULL UNION ALL
SELECT N'5494', N' 304', N'519', N'STBankHELOC                                       ', NULL, NULL, NULL UNION ALL
SELECT N'0101', N'PN4 ', N'557', N'FBOP Park Bank                                    ', NULL, NULL, NULL UNION ALL
SELECT N'5494', N' 305', N'519', N'STBankHELOC                                       ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N' 271', N'598', N'First Western Consumer                            ', NULL, NULL, NULL UNION ALL
SELECT N'1323', N' 271', N'599', N'First Western Commercial                          ', NULL, NULL, NULL UNION ALL
SELECT N'5948', N'6001', N'546', N'Washington Trust Commercial                       ', N'447017', N'VCC', N'501' UNION ALL
SELECT N'1559', N' 379', N'509', N'Woodtrust Commercial                              ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N' 301', N'527', N'Monona Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N'1301', N'527', N'Monona Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N'2301', N'527', N'Monona Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'5947', N'   1', N'529', N'Everbank Consumer                                 ', N'435201', NULL, NULL UNION ALL
SELECT N'5947', N'   2', N'529', N'Everbank Consumer                                 ', N'486524', NULL, NULL UNION ALL
SELECT N'5947', N'   3', N'529', N'Everbank Consumer                                 ', N'486524', NULL, NULL UNION ALL
SELECT N'5947', N'   4', N'529', N'Everbank Consumer                                 ', N'486524', N'VCM', N'004' UNION ALL
SELECT N'5947', N'   5', N'529', N'Everbank Consumer                                 ', N'486524', NULL, NULL UNION ALL
SELECT N'5947', N'   6', N'529', N'Everbank Consumer                                 ', N'486524', NULL, NULL UNION ALL
SELECT N'5947', N'   7', N'529', N'Everbank Consumer                                 ', N'486524', N'VCM', N'004' UNION ALL
SELECT N'5947', N'   8', N'529', N'Everbank Consumer                                 ', N'486524', NULL, NULL UNION ALL
SELECT N'5947', N'   9', N'529', N'Everbank Consumer                                 ', N'486524', N'VCM', N'009' UNION ALL
SELECT N'1559', N'1301', N'593', N'Monona Commercial                                 ', NULL, NULL, NULL UNION ALL
SELECT N'5867', N'   5', N'526', N'Union Bank Commercial                             ', N'486551', N'VCC', N'003' UNION ALL
SELECT N'4923', N' 301', N'500', N'American Trust Business                           ', N'436134', N'VBS', N'301' UNION ALL
SELECT N'7252', N'1000', N'596', N'Royal CU Consumer                                 ', NULL, NULL, NULL UNION ALL
SELECT N'7252', N'2000', N'596', N'Royal CU Consumer                                 ', NULL, NULL, NULL UNION ALL
SELECT N'7252', N'3000', N'596', N'Royal CU Consumer                                 ', NULL, NULL, NULL UNION ALL
SELECT N'7252', N'4000', N'596', N'Royal CU Consumer                                 ', NULL, NULL, NULL UNION ALL
SELECT N'7252', N'5000', N'596', N'Royal CU Consumer                                 ', NULL, NULL, NULL UNION ALL
SELECT N'7253', N'1000', N'597', N'Royal CU Commercial                               ', NULL, NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[CRTipFirst_Table]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[CRTipFirst_Table]([Bank], [Agent], [Tipfirst], [BankName], [Bin], [ProductLine], [SubProductType])
SELECT N'7253', N'2000', N'597', N'Royal CU Commercial                               ', NULL, NULL, NULL UNION ALL
SELECT N'7253', N'3000', N'597', N'Royal CU Commercial                               ', NULL, NULL, NULL UNION ALL
SELECT N'7253', N'4000', N'597', N'Royal CU Commercial                               ', NULL, NULL, NULL UNION ALL
SELECT N'7253', N'5000', N'597', N'Royal CU Commercial                               ', NULL, NULL, NULL UNION ALL
SELECT N'1323', N' 787', N'599', N'First Western Business                            ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N' 787', N'598', N'First Western Consumer                            ', NULL, NULL, NULL UNION ALL
SELECT N'  70', N'  90', N'50F', N'Community Central                                 ', NULL, NULL, NULL UNION ALL
SELECT N' 270', N'  90', N'50F', N'Comunity Central                                  ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N'  90', N'50F', N'Community Central                                 ', NULL, NULL, NULL UNION ALL
SELECT N'4082', N' 700', N'50U', N'Des Moines Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'0101', N'PN5 ', N'557', N'FBOP Park Bank                                    ', NULL, NULL, NULL UNION ALL
SELECT N'0101', N'PN6 ', N'557', N'FBOP Park Bank                                    ', NULL, NULL, NULL UNION ALL
SELECT N'0102', N'SN4 ', N'561', N'FBOP San Diego National                           ', NULL, NULL, NULL UNION ALL
SELECT N'0102', N'SN5 ', N'561', N'FBOP San Diego National                           ', NULL, NULL, NULL UNION ALL
SELECT N'0102', N'SN6 ', N'561', N'FBOP San Diego National                           ', NULL, NULL, NULL UNION ALL
SELECT N'0103', N'CA4 ', N'559', N'FBOP California National                          ', NULL, NULL, NULL UNION ALL
SELECT N'0103', N'CA5 ', N'559', N'FBOP California National                          ', NULL, NULL, NULL UNION ALL
SELECT N'0103', N'CA6 ', N'559', N'FBOP California National                          ', NULL, NULL, NULL UNION ALL
SELECT N'0104', N'PC4 ', N'563', N'FBOP Pacific National                             ', NULL, NULL, NULL UNION ALL
SELECT N'0104', N'PC5 ', N'563', N'FBOP Pacific National                             ', NULL, NULL, NULL UNION ALL
SELECT N'0104', N'PC6 ', N'563', N'FBOP Pacific National                             ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 301', N'593', N'Monona Consumer                                   ', NULL, NULL, NULL UNION ALL
SELECT N'2567', N' 687', N'51C', N'Helm Bank Business                                ', NULL, NULL, NULL UNION ALL
SELECT N'4082', N'1187', N'50W', N'Cedar Rapids Consumer                             ', NULL, NULL, NULL UNION ALL
SELECT N'4082', N'1345', N'50U', N'Des Moines Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'4083', N' 700', N'50V', N'Des Moines Business                               ', NULL, NULL, NULL UNION ALL
SELECT N'4083', N'1187', N'50X', N'Cedar Rapids Business                             ', NULL, NULL, NULL UNION ALL
SELECT N'4083', N'1345', N'50V', N'Des Moines Business                               ', NULL, NULL, NULL UNION ALL
SELECT N'3508', N'3200', N'546', N'Washington Trust Business                         ', N'486525', N'VBS', N'320' UNION ALL
SELECT N'3508', N'3201', N'546', N'Washington Trust Business                         ', N'447017', N'VBS', N'321' UNION ALL
SELECT N'7034', N'   1', N'575', N'Lewiston Consumer                                 ', N'437782', N'VCL', N'001' UNION ALL
SELECT N'7034', N'   2', N'575', N'Lewiston Consumer                                 ', N'437782', N'VGL', N'002' UNION ALL
SELECT N'5949', N'   1', N'576', N'Lewiston Business                                 ', N'486526', N'VCC', N'001' UNION ALL
SELECT N'5949', N'   2', N'576', N'Lewiston Business                                 ', N'486526', NULL, NULL UNION ALL
SELECT N'5492', N'1004', N'545', N'Washington Trust Consumer                         ', N'403603', N'VPL', N'104' UNION ALL
SELECT N'7572', N'2000', N'515', N'Landmark                                          ', N'401109', N'VGL', N'200' UNION ALL
SELECT N'7573', N'1000', N'515', N'Landmark                                          ', N'468701', N'VPL', N'100' UNION ALL
SELECT N'7573', N'5000', N'515', N'Landmark                                          ', N'468701', N'VPL', N'500' UNION ALL
SELECT N'7604', N'1003', N'52B', N'First Niagara Consumer                            ', N'543118', N'MCL', N'003' UNION ALL
SELECT N'7605', N'1000', N'52B', N'First Niagara Consumer                            ', N'543216', N'MCP', N'000' UNION ALL
SELECT N'7606', N'1000', N'52C', N'First Niagara Business                            ', N'547974', N'MBS', N'000' UNION ALL
SELECT N'1746', N' 790', N'52N', N'Central Bank&Trust                                ', N'438835', N'VPL', N'790' UNION ALL
SELECT N'1746', N' 966', N'52N', N'Central Ban&Trust                                 ', N'438835', N'VPL', N'966' UNION ALL
SELECT N'1746', N' 851', N'52N', N'Central Ban&Trust                                 ', N'438835', N'VPL', N'851' UNION ALL
SELECT N'1746', N' 886', N'52N', N'Central Ban&Trust                                 ', N'438835', N'VPL', N'886' UNION ALL
SELECT N'4924', N' 635', N'52L', N'Smart Bank Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'4924', N'2635', N'52L', N'Smart Bank Consumer                               ', NULL, NULL, NULL UNION ALL
SELECT N'1559', N' 635', N'52M', N'Smart Bank Business                               ', NULL, NULL, NULL UNION ALL
SELECT N'3257', N'6100', N'506', N'Rabobank Consumer                                 ', N'441556', N'VGL', N'610' UNION ALL
SELECT N'3257', N'7300', N'506', N'Rabobank Consumer                                 ', N'441556', N'VGL', N'730'
COMMIT;
RAISERROR (N'[dbo].[CRTipFirst_Table]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[CRTipFirst_Table]([Bank], [Agent], [Tipfirst], [BankName], [Bin], [ProductLine], [SubProductType])
SELECT N'3257', N'5200', N'506', N'Rabobank Consumer                                 ', N'441556', N'VGL', N'510' UNION ALL
SELECT N'3258', N'8100', N'506', N'Rabobank Consumer                                 ', N'441556', N'VPL', N'810' UNION ALL
SELECT N'3258', N'9100', N'506', N'Rabobank Consumer                                 ', N'441556', N'VPL', N'910' UNION ALL
SELECT N'3258', N'9300', N'506', N'Rabobank Consumer                                 ', N'441556', N'VPL', N'930' UNION ALL
SELECT N'3257', N'4100', N'506', N'Rabobank Consumer                                 ', N'441557', N'VGL', N'410' UNION ALL
SELECT N'4929', N'   3', N'529', N'Everbank Consumer                                 ', N'435201', N'VPL', N'003' UNION ALL
SELECT N'4929', N'   5', N'529', N'Everbank Consumer                                 ', N'435201', N'VPL', N'003' UNION ALL
SELECT N'4929', N'   7', N'529', N'Everbank Consumer                                 ', N'435201', N'VPL', N'003' UNION ALL
SELECT N'4929', N'   9', N'529', N'Everbank Consumer                                 ', N'435201', N'VPL', N'003' UNION ALL
SELECT N'5947', N'   4', N'529', N'Everbank Consumer                                 ', N'435201', N'VCM', N'004' UNION ALL
SELECT N'5947', N'   7', N'529', N'Everbank Consumer                                 ', N'435201', N'VCM', N'004' UNION ALL
SELECT N'5947', N'   9', N'529', N'Everbank Consumer                                 ', N'435201', N'VCM', N'009' UNION ALL
SELECT N'1379', N' 261', N'500', N'American Trust                                    ', N'436134', N'VGL', N'261' UNION ALL
SELECT N'1379', N' 263', N'500', N'American Trust                                    ', N'447044', N'VGL', N'263' UNION ALL
SELECT N'4923', N' 301', N'500', N'American Trust                                    ', N'447044', N'VBS', N'301' UNION ALL
SELECT N'7605', N'1000', N'52B', N'First Niagara Consumer                            ', N'543118', N'MCP', N'000' UNION ALL
SELECT N'7604', N'1003', N'52B', N'First Niagara Consumer                            ', N'543216', N'MCL', N'003' UNION ALL
SELECT N'7573', N'1000', N'515', N'Landmark                                          ', N'401109', N'VPL', N'100' UNION ALL
SELECT N'7573', N'5000', N'515', N'Landmark                                          ', N'401109', N'VPL', N'500' UNION ALL
SELECT N'7572', N'2000', N'515', N'Landmark                                          ', N'468701', N'VGL', N'200' UNION ALL
SELECT N'3508', N'3200', N'546', N'Washington Trust Business                         ', N'447017', N'VBS', N'320' UNION ALL
SELECT N'5948', N'6001', N'546', N'Washington Trust Business                         ', N'486525', N'VCC', N'501' UNION ALL
SELECT N'3508', N'3201', N'546', N'Washington Trust Business                         ', N'486525', N'VBS', N'321'
COMMIT;
RAISERROR (N'[dbo].[CRTipFirst_Table]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

