USE [MetavanteWork];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[b2kImportStatusMapping]([dim_b2kimportstatusmapping_scprogramstatus], [dim_b2kimportstatusmapping_scparticipationflag], [dim_b2kimportstatusmapping_rnstatus], [dim_b2kimportstatusmapping_created], [dim_b2kimportstatusmapping_lastmodified])
SELECT N'A', N'Y', N'A', '20101108 16:09:28.687', '20101108 16:09:28.687' UNION ALL
SELECT N'S', N'Y', N'A', '20101108 16:09:28.687', '20101108 16:09:28.687' UNION ALL
SELECT N'A', N'N', N'C', '20101108 16:09:28.687', '20101108 16:09:28.687' UNION ALL
SELECT N'S', N'N', N'C', '20101108 16:09:28.687', '20101108 16:09:28.687' UNION ALL
SELECT N'I', N'Y', N'I', '20101108 16:09:28.687', '20101108 16:09:28.687' UNION ALL
SELECT N'I', N'N', N'C', '20101108 16:09:28.687', '20101108 16:09:28.687'
COMMIT;
GO

