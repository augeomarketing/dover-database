USE MetavanteWork
GO

ALTER TABLE crtipfirst_table ADD Bin VARCHAR(9)
GO

UPDATE CRTipFirst_Table
SET Bin = 
	CASE 
		WHEN (Bank IN ('5492') AND Agent IN('1001', '1003', '1004')) THEN '403603'
		WHEN (Bank IN ('3508') AND Agent IN('3200')) THEN '486525'
		WHEN (Bank IN ('3508') AND Agent IN('3201', '6001')) THEN '447017'
		WHEN (Bank IN ('3257') AND Agent IN('4100')) THEN '441556'
		WHEN (Bank IN ('3257', '3258') AND Agent IN('6100', '7300', '5200')) THEN '441557'
		WHEN (Bank IN ('5964') AND Agent IN ('4100','1500','2500','3500','4500')) THEN '480824'
		WHEN (Bank IN ('7604') AND Agent IN ('1003')) THEN '543118'
		WHEN (Bank IN ('7605') AND Agent IN ('1000')) THEN '543216'
		WHEN (Bank IN ('7606') AND Agent IN ('1000')) THEN '547974'
		WHEN (Bank IN ('7034') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('1', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('2', 'L', 4, ' '))) THEN '437782'
		WHEN (Bank IN ('5949') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('1', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('2', 'L', 4, ' '))) THEN '486526'
		WHEN (bank IN ('1746') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('966', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('851', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('886', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('790', 'L', 4, ' '))) THEN '438835'
		WHEN (bank IN ('7572') AND agent IN ('2000')) THEN '401109'
		WHEN (BANK IN ('7573') AND agent IN ('1000', '5000')) THEN '468701'
		WHEN (Bank IN ('5947') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('1', 'L', 4, ' '))) THEN '435201'
		WHEN (bank IN ('5947') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('2', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('3', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('4', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('5', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('6', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('7', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('8', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('9', 'L', 4, ' '))) THEN '486524'
		WHEN (bank IN ('4929') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('3', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('5', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('7', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('9', 'L', 4, ' '))) THEN '486524'
		WHEN (bank IN ('1379') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('261', 'L', 4, ' '))) THEN '447044'
		WHEN (bank IN ('1379', '4923') AND RewardsNow.dbo.ufn_pad(agent, 'L', 4, ' ') IN (RewardsNow.dbo.ufn_pad('263', 'L', 4, ' '), RewardsNow.dbo.ufn_pad('301', 'L', 4, ' '))) THEN '436134'
		WHEN (bank IN ('6193') AND rewardsnow.dbo.ufn_pad(agent, 'L', 4, ' ') IN ('1001', '1002', rewardsnow.dbo.ufn_pad('1', 'L', 4, ' '), rewardsnow.dbo.ufn_pad('2', 'L', 4, ' '))) THEN '485258'
		WHEN (bank IN ('5867') AND rewardsnow.dbo.ufn_pad(agent, 'L', 4, ' ') IN ('1003', rewardsnow.dbo.ufn_pad('3','L', 4, ' '), rewardsnow.dbo.ufn_pad('5', 'L', 4, ' '))) THEN '486551'
	ELSE NULL
	END
GO

	
	