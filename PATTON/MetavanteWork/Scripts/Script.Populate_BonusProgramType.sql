USE [MetavanteWork];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusProgramType]([sid_BonusProgramType_ID], [dim_BonusProgramType_Name], [dim_BonusProgramType_DateAdded], [dim_BonusProgramType_LastUpdated])
SELECT N'C', N'Calculated Bonus Point Amount', '20101117 11:01:44.157', '20101117 11:01:44.157' UNION ALL
SELECT N'F', N'Fixed Bonus Point Amount', '20101117 11:01:50.067', '20101117 11:01:50.067'
COMMIT;
RAISERROR (N'[dbo].[BonusProgramType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

