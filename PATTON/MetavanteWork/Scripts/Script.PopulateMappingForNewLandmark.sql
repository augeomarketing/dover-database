--PopulateMappingForNewLandmark

use metavantework
go

delete from CRTipFirst_Table
where Bank = '7572'
	and Agent = '2000'
	and Tipfirst = '515'
	and BankName = 'Landmark'
	and Bin = '468701'
	and ProductLine = 'VPL'
	and SubProductType in ('200', '300')

insert into CRTipFirst_Table(Bank, Agent, Tipfirst, BankName, Bin, ProductLine, SubProductType)
select '7572', '2000', '515', 'Landmark', '468701', 'VPL', '200'
union all select '7572', '2000', '515', 'Landmark', '468701', 'VPL', '300'

select * from CRTipFirst_Table where Tipfirst = '515'


