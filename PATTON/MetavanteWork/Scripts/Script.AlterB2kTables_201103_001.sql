USE MetavanteWork
GO

--Add TipFirst Column
ALTER TABLE b2kDemog ADD dim_b2kdemog_tipfirst VARCHAR(3)
GO
--Move data from bank column to tipfirst column
UPDATE b2kDemog
SET dim_b2kdemog_tipfirst = dim_b2kdemog_bank
WHERE RewardsNow.dbo.ufn_CharIsNullOrEmpty(dim_b2kDemog_Bank) = 0
GO

--RUN: /PATTON/MetavanteWork/Views/dbo.vw_b2kDemogImport_AllRawRecords.View
--RUN: /PATTON/MetavanteWork/Views/dbo.vw_b2kDemogImport_AllLatestRecords.View

--Populate Old Card Numbers From History
UPDATE dmg
SET dim_b2kdemog_oldcardnumber = dmgAll.dim_b2kdemogimportraw_cardnumber
FROM b2kDemog dmg
INNER JOIN vw_b2kDemogImport_AllRawRecords dmgAll
	ON dmg.dim_b2kdemog_cardnumber = dmgAll.dim_b2kdemogimportraw_newcardnumber
WHERE
	RewardsNow.dbo.ufn_CharIsNullOrEmpty(dmgAll.dim_b2kdemogimportraw_newcardnumber) = 0
GO

INSERT INTO b2kProcessingParameters (dim_b2kprocessingparameters_parametername, dim_b2kprocessingparameters_parametervalue)
SELECT 'demogsource', '0'
UNION SELECT 'demogsource', '1'
GO

--RUN: /PATTON/MetavanteWork/Views/dbo.vw_B2KBinMapping.View
--RUN: /PATTON/MetavanteWork/Views/dbo.vw_b2kCreditCardHolding.View
--RUN: /PATTON/MetavanteWork/Views/dbo.vw_b2kCreditCardHoldingMidMonth.View
--RUN: /PATTON/MetavanteWork/Stored Procedures/dbo.usp_b2kProcessDemographics.StoredProcedure
--RUN: /PATTON/MetavanteWork/Stored Procedures/dbo.usp_b2kProcessDemographicsMidMonth.StoredProcedure


