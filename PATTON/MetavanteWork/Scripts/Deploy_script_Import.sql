USE [MetavanteWork]

ALTER TABLE dbo.packagecodes
ADD	DI_HH_id int default (0)

update packagecodes
set DI_HH_id = 0

update packagecodes
set DI_HH_id = 1
where TipFirst in ('50E','510','518','51D','51E','51M','51P','51Q','51R','51T','51U','51W','51X','51Y','51Z','522','52E','52F','52K',
'52P','52S','533','542','547','549','553','554','560','565','566','567','568','573','577','579','580','582','585','590','591','595')

update packagecodes
set DI_HH_id = 2
where TipFirst in ('50U','50W','511','519','523','529','52B','52C','52L','52M','52T','530','537','539','558','571')

update packagecodes
set DI_HH_id = 3
where TipFirst in ('500','502','504','506','508','515','517','525','527','531','545','569','575','593')

insert into DebitBins (TipFirst, BIN) Select '500','4308108'
insert into DebitBins (TipFirst, BIN) Select '500','452107'
insert into DebitBins (TipFirst, BIN) Select '502','448024'
insert into DebitBins (TipFirst, BIN) Select '504','415818'
insert into DebitBins (TipFirst, BIN) Select '504','4158183'
insert into DebitBins (TipFirst, BIN) Select '504','404975'
insert into DebitBins (TipFirst, BIN) Select '504','4049753'
insert into DebitBins (TipFirst, BIN) Select '506','468471'
insert into DebitBins (TipFirst, BIN) Select '506','468472'
insert into DebitBins (TipFirst, BIN) Select '506','490200'
insert into DebitBins (TipFirst, BIN) Select '508','44701038'
insert into DebitBins (TipFirst, BIN) Select '508','490042'
insert into DebitBins (TipFirst, BIN) Select '509','474363'
insert into DebitBins (TipFirst, BIN) Select '50E','546157933'
insert into DebitBins (TipFirst, BIN) Select '510','551079'
insert into DebitBins (TipFirst, BIN) Select '515','4806362'
insert into DebitBins (TipFirst, BIN) Select '517','405612'
insert into DebitBins (TipFirst, BIN) Select '518','44941611'
insert into DebitBins (TipFirst, BIN) Select '51D','435817'
insert into DebitBins (TipFirst, BIN) Select '51E','432869010'
insert into DebitBins (TipFirst, BIN) Select '51E','444777010'
--insert into DebitBins (TipFirst, BIN) Select '51T','455901'
insert into DebitBins (TipFirst, BIN) Select '51U','441731'
insert into DebitBins (TipFirst, BIN) Select '51U','465357'
insert into DebitBins (TipFirst, BIN) Select '51U','44173105'
insert into DebitBins (TipFirst, BIN) Select '51U','44173199'
insert into DebitBins (TipFirst, BIN) Select '51U','44173109'
--insert into DebitBins (TipFirst, BIN) Select '51W','474385'
insert into DebitBins (TipFirst, BIN) Select '51X','410882'
insert into DebitBins (TipFirst, BIN) Select '51Y','410878'
insert into DebitBins (TipFirst, BIN) Select '51Z','410880'
insert into DebitBins (TipFirst, BIN) Select '522','429492'
insert into DebitBins (TipFirst, BIN) Select '525','440182'
insert into DebitBins (TipFirst, BIN) Select '525','447452'
insert into DebitBins (TipFirst, BIN) Select '526','442212'
insert into DebitBins (TipFirst, BIN) Select '527','418186'
insert into DebitBins (TipFirst, BIN) Select '527','447076'
insert into DebitBins (TipFirst, BIN) Select '528','468463'
insert into DebitBins (TipFirst, BIN) Select '52A','403655'
insert into DebitBins (TipFirst, BIN) Select '52H','438633'
--insert into DebitBins (TipFirst, BIN) Select '52J','4359702'
insert into DebitBins (TipFirst, BIN) Select '52K','430516999'
--insert into DebitBins (TipFirst, BIN) Select '52R','4359703'
insert into DebitBins (TipFirst, BIN) Select '52S','473192'
--insert into DebitBins (TipFirst, BIN) Select '52U','400540001'
--insert into DebitBins (TipFirst, BIN) Select '52V','400540002'
--insert into DebitBins (TipFirst, BIN) Select '52W','4359705'
insert into DebitBins (TipFirst, BIN) Select '530','433043'
insert into DebitBins (TipFirst, BIN) Select '531','449019'
insert into DebitBins (TipFirst, BIN) Select '531','465364'
insert into DebitBins (TipFirst, BIN) Select '532','416766'
insert into DebitBins (TipFirst, BIN) Select '533','472986'
insert into DebitBins (TipFirst, BIN) Select '542','415869'
insert into DebitBins (TipFirst, BIN) Select '542','478853'
insert into DebitBins (TipFirst, BIN) Select '544','480298'
insert into DebitBins (TipFirst, BIN) Select '545','47313791'
insert into DebitBins (TipFirst, BIN) Select '545','47313799'
insert into DebitBins (TipFirst, BIN) Select '545','446473'
insert into DebitBins (TipFirst, BIN) Select '546','4842279'
insert into DebitBins (TipFirst, BIN) Select '547','448737%'
insert into DebitBins (TipFirst, BIN) Select '549','433055'
insert into DebitBins (TipFirst, BIN) Select '549','489567'
insert into DebitBins (TipFirst, BIN) Select '553','544205'
insert into DebitBins (TipFirst, BIN) Select '556','411113'
insert into DebitBins (TipFirst, BIN) Select '558','459119'
insert into DebitBins (TipFirst, BIN) Select '560','459112'
insert into DebitBins (TipFirst, BIN) Select '565','468292001'
insert into DebitBins (TipFirst, BIN) Select '565','468292003'
insert into DebitBins (TipFirst, BIN) Select '565','468292004'
insert into DebitBins (TipFirst, BIN) Select '565','468292005'
insert into DebitBins (TipFirst, BIN) Select '565','468292007'
insert into DebitBins (TipFirst, BIN) Select '566','468292002'
insert into DebitBins (TipFirst, BIN) Select '567','449033'
insert into DebitBins (TipFirst, BIN) Select '568','468292006'
insert into DebitBins (TipFirst, BIN) Select '569','435250'
insert into DebitBins (TipFirst, BIN) Select '570','411116'
insert into DebitBins (TipFirst, BIN) Select '573','511829'
insert into DebitBins (TipFirst, BIN) Select '575','442678'
insert into DebitBins (TipFirst, BIN) Select '575','470054'
insert into DebitBins (TipFirst, BIN) Select '576','489611'
insert into DebitBins (TipFirst, BIN) Select '577','4490749'
insert into DebitBins (TipFirst, BIN) Select '578','449040'
insert into DebitBins (TipFirst, BIN) Select '579','417911'
insert into DebitBins (TipFirst, BIN) Select '580','434829'
insert into DebitBins (TipFirst, BIN) Select '582','408058'
insert into DebitBins (TipFirst, BIN) Select '582','414148'
insert into DebitBins (TipFirst, BIN) Select '585','418271'
insert into DebitBins (TipFirst, BIN) Select '590','44941670'
insert into DebitBins (TipFirst, BIN) Select '591','511984'
insert into DebitBins (TipFirst, BIN) Select '593','418289'
insert into DebitBins (TipFirst, BIN) Select '595','510901'
insert into DebitBins (TipFirst, BIN) Select '595','549931'

