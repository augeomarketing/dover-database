USE MetavanteWork
GO

ALTER TABLE CRTipFirst_Table
ADD ProductLine VARCHAR(3)
GO

ALTER TABLE CRTipFirst_Table
ADD SubProductType VARCHAR(3)
GO

UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '261' WHERE Bank = '1379' AND Agent = ' 261' AND tipfirst = '500'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '263' WHERE Bank = '1379' AND Agent = ' 263' AND tipfirst = '500'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '410' WHERE Bank = '3257' AND Agent = '4100' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '510' WHERE Bank = '3257' AND Agent = '5100' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '610' WHERE Bank = '3257' AND Agent = '6100' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '730' WHERE Bank = '3257' AND Agent = '7300' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '510' WHERE Bank = '3257' AND Agent = '5200' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '810' WHERE Bank = '3258' AND Agent = '8100' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '910' WHERE Bank = '3258' AND Agent = '9100' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '930' WHERE Bank = '3258' AND Agent = '9300' AND tipfirst = '506'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '150' WHERE Bank = '5694' AND Agent = '1500' AND tipfirst = '507'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '250' WHERE Bank = '5694' AND Agent = '2500' AND tipfirst = '507'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '350' WHERE Bank = '5694' AND Agent = '3500' AND tipfirst = '507'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '450' WHERE Bank = '5694' AND Agent = '4500' AND tipfirst = '507'
UPDATE CRTipFirst_Table SET Productline = 'VCL', SubProductType = '001' WHERE Bank = '6193' AND Agent = '   1' AND tipfirst = '525'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '002' WHERE Bank = '6193' AND Agent = '   2' AND tipfirst = '525'
UPDATE CRTipFirst_Table SET Productline = 'VCC', SubProductType = '003' WHERE Bank = '5867' AND Agent = '   3' AND tipfirst = '526'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '003' WHERE Bank = '4929' AND Agent = '   3' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '003' WHERE Bank = '4929' AND Agent = '   5' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '003' WHERE Bank = '4929' AND Agent = '   7' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '003' WHERE Bank = '4929' AND Agent = '   9' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '410' WHERE Bank = '5694' AND Agent = '4100' AND tipfirst = '507'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '101' WHERE Bank = '5492' AND Agent = '1001' AND tipfirst = '545'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '103' WHERE Bank = '5492' AND Agent = '1003' AND tipfirst = '545'
UPDATE CRTipFirst_Table SET Productline = 'VCC', SubProductType = '501' WHERE Bank = '5948' AND Agent = '6001' AND tipfirst = '546'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '004' WHERE Bank = '5947' AND Agent = '   4' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '004' WHERE Bank = '5947' AND Agent = '   7' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VCM', SubProductType = '009' WHERE Bank = '5947' AND Agent = '   9' AND tipfirst = '529'
UPDATE CRTipFirst_Table SET Productline = 'VCC', SubProductType = '003' WHERE Bank = '5867' AND Agent = '   5' AND tipfirst = '526'
UPDATE CRTipFirst_Table SET Productline = 'VBS', SubProductType = '301' WHERE Bank = '4923' AND Agent = ' 301' AND tipfirst = '500'
UPDATE CRTipFirst_Table SET Productline = 'VBS', SubProductType = '320' WHERE Bank = '3508' AND Agent = '3200' AND tipfirst = '546'
UPDATE CRTipFirst_Table SET Productline = 'VBS', SubProductType = '321' WHERE Bank = '3508' AND Agent = '3201' AND tipfirst = '546'
UPDATE CRTipFirst_Table SET Productline = 'VCL', SubProductType = '001' WHERE Bank = '7034' AND Agent = '   1' AND tipfirst = '575'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '002' WHERE Bank = '7034' AND Agent = '   2' AND tipfirst = '575'
UPDATE CRTipFirst_Table SET Productline = 'VCC', SubProductType = '001' WHERE Bank = '5949' AND Agent = '   1' AND tipfirst = '576'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '104' WHERE Bank = '5492' AND Agent = '1004' AND tipfirst = '545'
UPDATE CRTipFirst_Table SET Productline = 'VGL', SubProductType = '200' WHERE Bank = '7572' AND Agent = '2000' AND tipfirst = '515'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '100' WHERE Bank = '7573' AND Agent = '1000' AND tipfirst = '515'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '500' WHERE Bank = '7573' AND Agent = '5000' AND tipfirst = '515'
UPDATE CRTipFirst_Table SET Productline = 'MCL', SubProductType = '003' WHERE Bank = '7604' AND Agent = '1003' AND tipfirst = '52B'
UPDATE CRTipFirst_Table SET Productline = 'MCP', SubProductType = '000' WHERE Bank = '7605' AND Agent = '1000' AND tipfirst = '52B'
UPDATE CRTipFirst_Table SET Productline = 'MBS', SubProductType = '000' WHERE Bank = '7606' AND Agent = '1000' AND tipfirst = '52C'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '790' WHERE Bank = '1746' AND Agent = ' 790' AND tipfirst = '52N'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '966' WHERE Bank = '1746' AND Agent = ' 966' AND tipfirst = '52N'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '851' WHERE Bank = '1746' AND Agent = ' 851' AND tipfirst = '52N'
UPDATE CRTipFirst_Table SET Productline = 'VPL', SubProductType = '886' WHERE Bank = '1746' AND Agent = ' 886' AND tipfirst = '52N'
