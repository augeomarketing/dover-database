USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DebitRollUp]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DebitRollUp_sigvspin]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitRollUp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DebitRollUp_sigvspin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitRollUp] DROP CONSTRAINT [DF_DebitRollUp_sigvspin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitRollUp]') AND type in (N'U'))
DROP TABLE [dbo].[DebitRollUp]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitRollUp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DebitRollUp](
	[acctnum] [nvarchar](25) NULL,
	[numpurch] [int] NULL,
	[amtpurch] [float] NULL,
	[numcr] [int] NULL,
	[amtcr] [float] NULL,
	[sigvspin] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DebitRollUp]') AND name = N'IX_DebitRollUp')
CREATE NONCLUSTERED INDEX [IX_DebitRollUp] ON [dbo].[DebitRollUp] 
(
	[acctnum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DebitRollUp]') AND name = N'IX_DebitRollUp_1')
CREATE NONCLUSTERED INDEX [IX_DebitRollUp_1] ON [dbo].[DebitRollUp] 
(
	[acctnum] ASC,
	[sigvspin] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DebitRollUp_sigvspin]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitRollUp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DebitRollUp_sigvspin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitRollUp] ADD  CONSTRAINT [DF_DebitRollUp_sigvspin]  DEFAULT (' ') FOR [sigvspin]
END


End
GO
