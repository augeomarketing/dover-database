USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__b2kDemog__dim_b2__03A6B153]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[b2kDemog] DROP CONSTRAINT [DF__b2kDemog__dim_b2__03A6B153]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kDemog]    Script Date: 11/15/2010 13:30:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[b2kDemog]') AND type in (N'U'))
DROP TABLE [dbo].[b2kDemog]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kDemog]    Script Date: 11/15/2010 13:30:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[b2kDemog](
	[sid_b2kdemog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_b2kdemog_relationshipaccount] [varchar](19) NULL,
	[dim_b2kdemog_cardnumber] [varchar](19) NOT NULL,
	[dim_b2kdemog_cardtype] [varchar](3) NULL,
	[dim_b2kdemog_accounttype] [varchar](3) NULL,
	[dim_b2kdemog_primaryfirstname] [varchar](35) NULL,
	[dim_b2kdemog_primarymiddleinitial] [varchar](1) NULL,
	[dim_b2kdemog_primarylastname] [varchar](35) NULL,
	[dim_b2kdemog_secondaryfirstname] [varchar](35) NULL,
	[dim_b2kdemog_secondarymiddleinitial] [varchar](1) NULL,
	[dim_b2kdemog_secondarylastname] [varchar](35) NULL,
	[dim_b2kdemog_addressline1] [varchar](40) NULL,
	[dim_b2kdemog_addressline2] [varchar](40) NULL,
	[dim_b2kdemog_addressline3] [varchar](40) NULL,
	[dim_b2kdemog_addressline4] [varchar](40) NULL,
	[dim_b2kdemog_city] [varchar](30) NULL,
	[dim_b2kdemog_state] [varchar](3) NULL,
	[dim_b2kdemog_country] [varchar](4) NULL,
	[dim_b2kdemog_zipcode] [varchar](11) NULL,
	[dim_b2kdemog_homephonenumber] [varchar](20) NULL,
	[dim_b2kdemog_workphonenumber] [varchar](20) NULL,
	[dim_b2kdemog_emailaddress] [varchar](60) NULL,
	[dim_b2kdemog_scprogramstatus] [varchar](1) NULL,
	[dim_b2kdemog_scprogramstatuschangedate] [varchar](8) NULL,
	[dim_b2kdemog_hostaccountstatus] [varchar](2) NULL,
	[dim_b2kdemog_hostacctstatusdate] [varchar](8) NULL,
	[dim_b2kdemog_scparticipationflag] [varchar](1) NULL,
	[dim_b2kdemog_primarysocialsecuritynumber] [varchar](13) NULL,
	[dim_b2kdemog_newcardnumber] [varchar](19) NULL,
	[dim_b2kdemog_associationid] [varchar](2) NULL,
	[dim_b2kdemog_corpid] [varchar](9) NULL,
	[dim_b2kdemog_agentinstitutionid] [varchar](6) NULL,
	[dim_b2kdemog_billcode] [varchar](8) NULL,
	[dim_b2kdemog_binplan] [varchar](9) NULL,
	[dim_b2kdemog_scenrollmentdate] [varchar](8) NULL,
	[dim_b2kdemog_rnstatus] [varchar](1) NULL,
	[dim_b2kdemog_created] [smalldatetime] NOT NULL,
	[dim_b2kdemog_lastmodified] [smalldatetime] NULL,
	[dim_b2kdemog_importdate] [smalldatetime] NULL,
	[dim_b2kdemog_filedate] [varchar](8) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_b2kdemog_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[dim_b2kdemog_cardnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[b2kDemog] ADD  DEFAULT (getdate()) FOR [dim_b2kdemog_created]
GO


