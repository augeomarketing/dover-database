USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Pointconv]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pointconv]') AND type in (N'U'))
DROP TABLE [dbo].[Pointconv]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pointconv]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Pointconv](
	[52E] [nvarchar](255) NULL,
	[F2] [nvarchar](255) NULL,
	[F3] [nvarchar](255) NULL,
	[STELLA J  MARKS] [nvarchar](255) NULL,
	[4224810000016604] [nvarchar](255) NULL,
	[  27826] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
