USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TSYS1PurgedTips]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TSYS1PurgedTips]') AND type in (N'U'))
DROP TABLE [dbo].[TSYS1PurgedTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TSYS1PurgedTips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TSYS1PurgedTips](
	[tipnumber] [char](15) NULL,
	[acctname1] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
