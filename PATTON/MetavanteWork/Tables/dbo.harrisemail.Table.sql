USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[harrisemail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_harrisemail_dim_harrisemail_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[harrisemail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_harrisemail_dim_harrisemail_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[harrisemail] DROP CONSTRAINT [DF_harrisemail_dim_harrisemail_created]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_harrisemail_dim_harrisemail_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[harrisemail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_harrisemail_dim_harrisemail_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[harrisemail] DROP CONSTRAINT [DF_harrisemail_dim_harrisemail_lastmodified]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_harrisemail_dim_harrisemail_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[harrisemail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_harrisemail_dim_harrisemail_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[harrisemail] DROP CONSTRAINT [DF_harrisemail_dim_harrisemail_active]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[harrisemail]') AND type in (N'U'))
DROP TABLE [dbo].[harrisemail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[harrisemail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[harrisemail](
	[sid_harrisemail_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_harrisemail_nomatch] [varchar](4096) NOT NULL,
	[dim_harrisemail_enrolled] [varchar](4096) NOT NULL,
	[dim_harrisemail_created] [datetime] NOT NULL,
	[dim_harrisemail_lastmodified] [datetime] NOT NULL,
	[dim_harrisemail_active] [int] NOT NULL,
 CONSTRAINT [PK_harrisemail] PRIMARY KEY CLUSTERED 
(
	[sid_harrisemail_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_harrisemail_dim_harrisemail_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[harrisemail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_harrisemail_dim_harrisemail_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[harrisemail] ADD  CONSTRAINT [DF_harrisemail_dim_harrisemail_created]  DEFAULT (getdate()) FOR [dim_harrisemail_created]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_harrisemail_dim_harrisemail_lastmodified]') AND parent_object_id = OBJECT_ID(N'[dbo].[harrisemail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_harrisemail_dim_harrisemail_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[harrisemail] ADD  CONSTRAINT [DF_harrisemail_dim_harrisemail_lastmodified]  DEFAULT (getdate()) FOR [dim_harrisemail_lastmodified]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_harrisemail_dim_harrisemail_active]') AND parent_object_id = OBJECT_ID(N'[dbo].[harrisemail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_harrisemail_dim_harrisemail_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[harrisemail] ADD  CONSTRAINT [DF_harrisemail_dim_harrisemail_active]  DEFAULT (1) FOR [dim_harrisemail_active]
END


End
GO
