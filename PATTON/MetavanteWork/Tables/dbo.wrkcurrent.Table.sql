USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrkcurrent]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkcurrent]') AND type in (N'U'))
DROP TABLE [dbo].[wrkcurrent]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkcurrent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkcurrent](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
