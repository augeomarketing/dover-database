USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[PrintedWelcomeKit]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrintedWelcomeKit]') AND type in (N'U'))
DROP TABLE [dbo].[PrintedWelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PrintedWelcomeKit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PrintedWelcomeKit](
	[TIPNUMBER] [nvarchar](255) NULL,
	[ACCTNAME1] [nvarchar](255) NULL,
	[ACCTNAME2] [nvarchar](255) NULL,
	[ACCTNAME3] [nvarchar](255) NULL,
	[ACCTNAME4] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
