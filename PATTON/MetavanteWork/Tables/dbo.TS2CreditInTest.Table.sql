USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TS2CreditInTest]    Script Date: 01/11/2010 17:14:18 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TS2CreditInTest_Col001]') AND parent_object_id = OBJECT_ID(N'[dbo].[TS2CreditInTest]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TS2CreditInTest_Col001]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TS2CreditInTest] DROP CONSTRAINT [DF_TS2CreditInTest_Col001]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TS2CreditInTest]') AND type in (N'U'))
DROP TABLE [dbo].[TS2CreditInTest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TS2CreditInTest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TS2CreditInTest](
	[Col001] [char](1) NULL,
	[ProviderID] [char](11) NULL,
	[ClientCD] [char](6) NULL,
	[Acctnum] [char](21) NULL,
	[Name1] [char](36) NULL,
	[CoAppName] [char](36) NULL,
	[STCClose] [char](5) NULL,
	[STFFraud] [char](5) NULL,
	[STCCRRVK] [char](3) NULL,
	[STCClosed] [char](6) NULL,
	[STCSecure] [char](6) NULL,
	[STCCRRVKED] [char](7) NULL,
	[STCXFER] [char](4) NULL,
	[AcctnumXFER] [char](21) NULL,
	[Address1] [char](40) NULL,
	[Address2] [char](40) NULL,
	[City] [char](19) NULL,
	[State] [char](5) NULL,
	[Zip] [char](13) NULL,
	[HomePhone] [char](20) NULL,
	[WorkPhone] [char](20) NULL,
	[SSN] [char](12) NULL,
	[Total] [numeric](18, 2) NULL,
	[DBCRInd] [char](1) NULL,
	[Status] [char](1) NULL,
	[OverDueFlag] [numeric](12, 2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_TS2CreditInTest_Col001]') AND parent_object_id = OBJECT_ID(N'[dbo].[TS2CreditInTest]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TS2CreditInTest_Col001]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TS2CreditInTest] ADD  CONSTRAINT [DF_TS2CreditInTest_Col001]  DEFAULT (0) FOR [Col001]
END


End
GO
