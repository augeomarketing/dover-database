USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[fbop_w]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fbop_w]') AND type in (N'U'))
DROP TABLE [dbo].[fbop_w]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fbop_w]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fbop_w](
	[tipfirst] [nvarchar](3) NULL,
	[acctnum] [nvarchar](25) NULL,
	[status] [nvarchar](1) NULL,
	[cardtype] [varchar](6) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
