USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[LEWISTON]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LEWISTON]') AND type in (N'U'))
DROP TABLE [dbo].[LEWISTON]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LEWISTON]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LEWISTON](
	[ACCTNUM] [nvarchar](255) NULL,
	[TIPNUMBER] [nvarchar](255) NULL,
	[DEBIT] [float] NULL,
	[CREDIT] [float] NULL
) ON [PRIMARY]
END
GO
