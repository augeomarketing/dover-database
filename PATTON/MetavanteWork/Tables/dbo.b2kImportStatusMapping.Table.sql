USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__b2kImport__dim_b__058EF9C5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[b2kImportStatusMapping] DROP CONSTRAINT [DF__b2kImport__dim_b__058EF9C5]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__b2kImport__dim_b__06831DFE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[b2kImportStatusMapping] DROP CONSTRAINT [DF__b2kImport__dim_b__06831DFE]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kImportStatusMapping]    Script Date: 11/15/2010 15:52:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[b2kImportStatusMapping]') AND type in (N'U'))
DROP TABLE [dbo].[b2kImportStatusMapping]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kImportStatusMapping]    Script Date: 11/15/2010 15:52:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[b2kImportStatusMapping](
	[sid_b2kimportstatusmapping_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_b2kimportstatusmapping_scprogramstatus] [varchar](1) NOT NULL,
	[dim_b2kimportstatusmapping_scparticipationflag] [varchar](1) NOT NULL,
	[dim_b2kimportstatusmapping_rnstatus] [varchar](1) NOT NULL,
	[dim_b2kimportstatusmapping_created] [datetime] NOT NULL,
	[dim_b2kimportstatusmapping_lastmodified] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[b2kImportStatusMapping] ADD  DEFAULT (getdate()) FOR [dim_b2kimportstatusmapping_created]
GO

ALTER TABLE [dbo].[b2kImportStatusMapping] ADD  DEFAULT (getdate()) FOR [dim_b2kimportstatusmapping_lastmodified]
GO


