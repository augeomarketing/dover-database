USE MetavanteWork
GO

IF OBJECT_ID(N'b2kProcessingParameters') IS NOT NULL
	DROP TABLE b2kProcessingParameters
GO

CREATE TABLE b2kProcessingParameters
(
	sid_b2kprocessingparameters_id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, dim_b2kprocessingparameters_parametername VARCHAR(20) not null
	, dim_b2kprocessingparameters_parametervalue VARCHAR(255) not null
)
/*
INSERT b2kProcessingParameters 
	(
		dim_b2kprocessingparameters_parametername
		, dim_b2kprocessingparameters_parametervalue
	)
SELECT 'usebankagent', '1'
*/
GO
	