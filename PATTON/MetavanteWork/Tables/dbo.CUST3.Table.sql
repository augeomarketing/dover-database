USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[CUST3]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUST3]') AND type in (N'U'))
DROP TABLE [dbo].[CUST3]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUST3]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUST3](
	[BANK] [nvarchar](4) NULL,
	[AGENT] [nvarchar](4) NULL,
	[SSN] [nvarchar](9) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[OLDCC] [nvarchar](16) NULL,
	[NA1] [nvarchar](25) NULL,
	[NA2] [nvarchar](25) NULL,
	[STATUS1] [nvarchar](2) NULL,
	[STATUS] [nvarchar](1) NULL,
	[OLDCCPRE] [nvarchar](8) NULL,
	[OLDCCPOST] [nvarchar](10) NULL,
	[ADDR1] [nvarchar](36) NULL,
	[ADDR2] [nvarchar](36) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[CITY] [nvarchar](25) NULL,
	[STATE] [nvarchar](2) NULL,
	[ZIP] [nvarchar](10) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[NUMPURCH] [numeric](10, 0) NULL,
	[PURCH] [numeric](12, 0) NULL,
	[NUMRET] [numeric](10, 0) NULL,
	[AMTRET] [numeric](12, 0) NULL,
	[PERIOD] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
