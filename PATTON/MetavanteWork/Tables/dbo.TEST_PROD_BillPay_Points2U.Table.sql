USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TEST_PROD_BillPay_Points2U]    Script Date: 01/11/2010 17:14:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TEST_PROD_BillPay_Points2U]') AND type in (N'U'))
DROP TABLE [dbo].[TEST_PROD_BillPay_Points2U]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TEST_PROD_BillPay_Points2U]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TEST_PROD_BillPay_Points2U](
	[Subject ID] [char](12) NULL,
	[PR**] [char](11) NULL,
	[Brand Id] [char](14) NULL,
	[Brand Name] [char](50) NULL,
	[Member Id     Most Freq Aba *Most Freq Memb*Creation Date] [char](66) NULL,
	[Active Inactive] [char](48) NULL,
	[Member Type] [char](48) NULL,
	[Service Level Code] [char](48) NULL,
	[Std First Name                          *Std Last Name] [char](81) NULL,
	[Email Address] [char](255) NULL,
	[Std Address Line 1                      Std Address Line 2] [char](80) NULL,
	[Std City] [char](40) NULL,
	[S*Std *Std*First Payee Acct Crea*First Payment Schedul*Last Payee Account Cr*Last Payment Completi*] [char](99) NULL,
	[Ten] [char](3) NULL,
	[ure] [char](3) NULL,
	[ In *Paym] [char](9) NULL,
	[nts Com*] [char](8) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
