USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[CUST535]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUST535]') AND type in (N'U'))
DROP TABLE [dbo].[CUST535]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUST535]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUST535](
	[Col001] [char](16) NULL,
	[Col002] [char](9) NULL,
	[Col003] [char](240) NULL,
	[Col004] [char](1) NULL,
	[Col005] [char](15) NULL,
	[Col006] [char](120) NULL,
	[Col007] [char](40) NULL,
	[Col008] [char](15) NULL,
	[Col009] [char](60) NULL,
	[Col010] [char](106) NULL,
	[Col011] [char](60) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
