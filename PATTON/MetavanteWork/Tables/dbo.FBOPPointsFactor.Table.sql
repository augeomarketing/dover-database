USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPPointsFactor]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPPoint__Revis__4FD50324]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPPoint__Revis__4FD50324]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] DROP CONSTRAINT [DF__FBOPPoint__Revis__4FD50324]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPPoint__Start__50C9275D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPPoint__Start__50C9275D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] DROP CONSTRAINT [DF__FBOPPoint__Start__50C9275D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPPoint__Endda__51BD4B96]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPPoint__Endda__51BD4B96]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] DROP CONSTRAINT [DF__FBOPPoint__Endda__51BD4B96]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPPointsFactor_BusinessFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPPointsFactor_BusinessFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] DROP CONSTRAINT [DF_FBOPPointsFactor_BusinessFlag]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPPointsFactor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPPointsFactor](
	[DBNumber] [nchar](3) NOT NULL,
	[BIN] [nvarchar](10) NOT NULL,
	[Revision] [smallint] NOT NULL,
	[Startdate] [datetime] NOT NULL,
	[Enddate] [datetime] NOT NULL,
	[Credit] [numeric](5, 2) NOT NULL,
	[SigFactor] [numeric](5, 2) NOT NULL,
	[PinFactor] [numeric](5, 2) NOT NULL,
	[BusinessFlag] [char](1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPPoint__Revis__4FD50324]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPPoint__Revis__4FD50324]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] ADD  DEFAULT (0) FOR [Revision]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPPoint__Start__50C9275D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPPoint__Start__50C9275D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] ADD  DEFAULT (1 / 1 / 1900) FOR [Startdate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPPoint__Endda__51BD4B96]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPPoint__Endda__51BD4B96]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] ADD  DEFAULT (1 / 1 / 1900) FOR [Enddate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPPointsFactor_BusinessFlag]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPPointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPPointsFactor_BusinessFlag]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPPointsFactor] ADD  CONSTRAINT [DF_FBOPPointsFactor_BusinessFlag]  DEFAULT ('N') FOR [BusinessFlag]
END


End
GO
