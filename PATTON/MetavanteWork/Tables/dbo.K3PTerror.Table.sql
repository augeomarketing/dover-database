USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[K3PTerror]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[K3PTerror]') AND type in (N'U'))
DROP TABLE [dbo].[K3PTerror]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[K3PTerror]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[K3PTerror](
	[LoyaltyNumber] [varchar](24) NULL,
	[Amount] [float] NULL,
	[Sign] [char](1) NULL,
	[DateIn] [varchar](7) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
