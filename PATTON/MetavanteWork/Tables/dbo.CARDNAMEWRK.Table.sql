USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[CARDNAMEWRK]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CARDNAMEWRK]') AND type in (N'U'))
DROP TABLE [dbo].[CARDNAMEWRK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CARDNAMEWRK]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CARDNAMEWRK](
	[TIPNUMBER] [nchar](15) NULL,
	[NA1] [nvarchar](40) NULL,
	[na2] [char](40) NULL,
	[na3] [char](40) NULL,
	[na4] [char](40) NULL,
	[na5] [char](40) NULL,
	[na6] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
