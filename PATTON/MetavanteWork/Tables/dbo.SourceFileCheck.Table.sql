USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[SourceFileCheck]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_SourceFileCheck_present]') AND parent_object_id = OBJECT_ID(N'[dbo].[SourceFileCheck]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SourceFileCheck_present]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SourceFileCheck] DROP CONSTRAINT [DF_SourceFileCheck_present]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SourceFileCheck]') AND type in (N'U'))
DROP TABLE [dbo].[SourceFileCheck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SourceFileCheck]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SourceFileCheck](
	[TipPrefix] [char](3) NULL,
	[DebitFileName] [varchar](100) NULL,
	[CreditFileName] [varchar](100) NULL,
	[FIName] [varchar](100) NULL,
	[present] [char](1) NULL,
	[date] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_SourceFileCheck_present]') AND parent_object_id = OBJECT_ID(N'[dbo].[SourceFileCheck]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SourceFileCheck_present]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SourceFileCheck] ADD  CONSTRAINT [DF_SourceFileCheck_present]  DEFAULT ('N') FOR [present]
END


End
GO
