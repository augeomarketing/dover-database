USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Participantcounts]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Participantcounts]') AND type in (N'U'))
DROP TABLE [dbo].[Participantcounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Participantcounts]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Participantcounts](
	[dbname] [char](100) NULL,
	[creditcount] [numeric](18, 0) NULL,
	[debitcount] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
