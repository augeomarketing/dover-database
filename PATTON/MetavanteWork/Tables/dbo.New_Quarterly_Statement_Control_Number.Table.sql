USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[New_Quarterly_Statement_Control_Number]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Control_Number]') AND type in (N'U'))
DROP TABLE [dbo].[New_Quarterly_Statement_Control_Number]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Control_Number]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[New_Quarterly_Statement_Control_Number](
	[StatementFileNumber] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
