USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroger_Daily_Card_Address_Detail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Address_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Kroger_Daily_Card_Address_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Address_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroger_Daily_Card_Address_Detail](
	[RecordID] [char](2) NULL,
	[RecordSequence] [char](10) NULL,
	[SegmentID] [char](12) NULL,
	[SegmentVersion] [char](2) NULL,
	[CardNumber] [char](19) NULL,
	[LoyaltyNumber] [char](21) NULL,
	[ReferenceNumber] [char](19) NULL,
	[PrimaryDDA] [char](11) NULL,
	[PrimarySavings] [char](11) NULL,
	[Address1] [char](40) NULL,
	[Address2] [char](40) NULL,
	[Address3] [char](40) NULL,
	[City] [char](20) NULL,
	[State] [char](2) NULL,
	[Zip] [char](9) NULL,
	[CountryCode] [char](3) NULL,
	[CountryName] [char](40) NULL,
	[Filler] [char](199) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
