USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Valyou_Points_Test_file]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Valyou_Points_Test_file]') AND type in (N'U'))
DROP TABLE [dbo].[Valyou_Points_Test_file]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Valyou_Points_Test_file]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Valyou_Points_Test_file](
	[TXN ID] [varchar](255) NULL,
	[EFD Card Number] [varchar](255) NULL,
	[Acct Number] [varchar](255) NULL,
	[Points] [varchar](255) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
