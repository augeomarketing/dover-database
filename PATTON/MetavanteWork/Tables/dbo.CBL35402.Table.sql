USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[CBL35402]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CBL35402]') AND type in (N'U'))
DROP TABLE [dbo].[CBL35402]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CBL35402]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CBL35402](
	[Col001] [char](11) NULL,
	[Col002] [char](17) NULL,
	[Col003] [char](61) NULL,
	[Col004] [char](52) NULL,
	[Col005] [char](21) NULL,
	[Col006] [char](58) NULL,
	[Col007] [char](26) NULL,
	[Col008] [char](68) NULL,
	[Col009] [char](186) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
