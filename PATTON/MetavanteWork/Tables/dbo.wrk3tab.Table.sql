USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrk3tab]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrk3tab]') AND type in (N'U'))
DROP TABLE [dbo].[wrk3tab]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrk3tab]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrk3tab](
	[tipnumber] [nvarchar](15) NULL,
	[citystate] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
