USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[HarrisTest]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisTest]') AND type in (N'U'))
DROP TABLE [dbo].[HarrisTest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisTest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HarrisTest](
	[Account #] [nvarchar](255) NULL,
	[Last Name] [nvarchar](255) NULL,
	[Zip Code] [nvarchar](255) NULL,
	[Last 4] [nvarchar](255) NULL,
	[Last 6] [nvarchar](255) NULL,
	[Combined 4] [nvarchar](255) NULL,
	[Combined 6] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
