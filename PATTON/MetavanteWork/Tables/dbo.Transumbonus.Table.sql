USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Transumbonus]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transumbonus]') AND type in (N'U'))
DROP TABLE [dbo].[Transumbonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transumbonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transumbonus](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[NUMCREDIT] [nchar](6) NULL,
	[AMTCREDIT] [decimal](7, 0) NULL,
	[NUMDEBIT] [nchar](6) NULL,
	[AMTDEBIT] [decimal](7, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [decimal](5, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
