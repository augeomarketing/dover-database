USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[AuditSummaryEXP]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuditSummaryEXP_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummaryEXP]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuditSummaryEXP_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummaryEXP] DROP CONSTRAINT [DF_AuditSummaryEXP_PointsToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditSummaryEXP]') AND type in (N'U'))
DROP TABLE [dbo].[AuditSummaryEXP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditSummaryEXP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuditSummaryEXP](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[PointsToExpire] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuditSummaryEXP_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummaryEXP]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuditSummaryEXP_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummaryEXP] ADD  CONSTRAINT [DF_AuditSummaryEXP_PointsToExpire]  DEFAULT ('0') FOR [PointsToExpire]
END


End
GO
