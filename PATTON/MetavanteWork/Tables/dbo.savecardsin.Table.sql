USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[savecardsin]    Script Date: 01/11/2010 17:14:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[savecardsin]') AND type in (N'U'))
DROP TABLE [dbo].[savecardsin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[savecardsin]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[savecardsin](
	[TIPFIRST] [nvarchar](3) NULL,
	[TIPMID] [nvarchar](7) NULL,
	[TIPLAST] [nvarchar](5) NULL,
	[TIPNUMBER] [nchar](15) NULL,
	[JOINT] [nvarchar](1) NULL,
	[SSN] [nvarchar](9) NULL,
	[BANK] [nvarchar](4) NULL,
	[AGENT] [nvarchar](4) NULL,
	[DDANUM] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[OLDCC] [nvarchar](16) NULL,
	[NA1] [nvarchar](40) NULL,
	[NA2] [nvarchar](40) NULL,
	[NA3] [nvarchar](40) NULL,
	[NA4] [nvarchar](40) NULL,
	[NA5] [nvarchar](40) NULL,
	[NA6] [nvarchar](40) NULL,
	[ADDR1] [nvarchar](40) NULL,
	[ADDR2] [nvarchar](40) NULL,
	[ADDR3] [nvarchar](40) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[ZIP] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[NUMPURCH] [nvarchar](9) NULL,
	[PURCH] [float] NULL,
	[NUMRET] [nvarchar](9) NULL,
	[AMTRET] [float] NULL,
	[PERIOD] [nvarchar](14) NULL,
	[LASTNAME] [nvarchar](30) NULL,
	[Cardtype] [char](1) NULL,
	[SigvsPin] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
