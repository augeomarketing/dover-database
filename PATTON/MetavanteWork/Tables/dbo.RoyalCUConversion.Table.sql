USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[RoyalCUConversion]    Script Date: 01/11/2010 17:14:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoyalCUConversion]') AND type in (N'U'))
DROP TABLE [dbo].[RoyalCUConversion]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RoyalCUConversion]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RoyalCUConversion](
	[Account Number] [nvarchar](255) NULL,
	[Converting Points] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
