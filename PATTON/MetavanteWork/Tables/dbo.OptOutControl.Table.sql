USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[OptOutControl]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OptOutControl]') AND type in (N'U'))
DROP TABLE [dbo].[OptOutControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OptOutControl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OptOutControl](
	[Tipnumber] [char](15) NOT NULL,
	[AcctNumber] [char](25) NOT NULL,
	[Name] [char](40) NULL,
 CONSTRAINT [PK_OptOutControl] PRIMARY KEY CLUSTERED 
(
	[AcctNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OptOutControl]') AND name = N'IX_OptOutControl')
CREATE NONCLUSTERED INDEX [IX_OptOutControl] ON [dbo].[OptOutControl] 
(
	[AcctNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
