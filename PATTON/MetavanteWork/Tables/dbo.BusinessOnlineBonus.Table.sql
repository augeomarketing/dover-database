USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[BusinessOnlineBonus]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessOnlineBonus]') AND type in (N'U'))
DROP TABLE [dbo].[BusinessOnlineBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BusinessOnlineBonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BusinessOnlineBonus](
	[Bank] [nvarchar](3) NULL,
	[Acct #] [nvarchar](20) NULL,
	[Tax ID] [nvarchar](9) NULL,
	[Card #] [nvarchar](16) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
