USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Hottemp]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Hottemp]') AND type in (N'U'))
DROP TABLE [dbo].[Hottemp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Hottemp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Hottemp](
	[tipnumber] [nchar](15) NULL,
	[oldcc] [nvarchar](16) NULL,
	[ssn] [nvarchar](9) NULL,
	[lastname] [nvarchar](30) NULL,
	[ddanum] [nvarchar](11) NULL,
	[period] [nvarchar](14) NULL,
	[cardtype] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
