USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[WelcomeKit_Work]    Script Date: 12/16/2010 10:52:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WelcomeKit_Work]') AND type in (N'U'))
DROP TABLE [dbo].[WelcomeKit_Work]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[WelcomeKit_Work]    Script Date: 12/16/2010 10:52:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WelcomeKit_Work](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[DDA] [varchar](15) NULL
) ON [PRIMARY]

GO


