USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[ddachkwrk]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ddachkwrk]') AND type in (N'U'))
DROP TABLE [dbo].[ddachkwrk]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ddachkwrk]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ddachkwrk](
	[ddanum] [char](11) NULL,
	[namein] [char](40) NULL,
	[seq] [bigint] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
