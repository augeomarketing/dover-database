USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DebitBins]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitBins]') AND type in (N'U'))
DROP TABLE [dbo].[DebitBins]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitBins]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DebitBins](
	[TipFirst] [char](15) NULL,
	[BIN] [varchar](9) NOT NULL,
 CONSTRAINT [PK_DebitBins] PRIMARY KEY CLUSTERED 
(
	[BIN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
