USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[HarrisPilotwrk1]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisPilotwrk1]') AND type in (N'U'))
DROP TABLE [dbo].[HarrisPilotwrk1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisPilotwrk1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HarrisPilotwrk1](
	[acctnum] [nvarchar](25) NOT NULL,
	[na1] [nvarchar](40) NULL,
	[addr1] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
