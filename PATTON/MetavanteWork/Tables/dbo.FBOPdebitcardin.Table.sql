USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPdebitcardin]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_NUMPURCH]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_NUMPURCH]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] DROP CONSTRAINT [DF_FBOPdebitcardin_NUMPURCH]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_PURCH]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_PURCH]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] DROP CONSTRAINT [DF_FBOPdebitcardin_PURCH]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_NUMRET]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_NUMRET]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] DROP CONSTRAINT [DF_FBOPdebitcardin_NUMRET]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_AMTRET]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_AMTRET]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] DROP CONSTRAINT [DF_FBOPdebitcardin_AMTRET]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_SigvsPin]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_SigvsPin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] DROP CONSTRAINT [DF_FBOPdebitcardin_SigvsPin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPdebitcardin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPdebitcardin](
	[MATCH1] [nvarchar](1) NULL,
	[MATCH2] [nvarchar](1) NULL,
	[MATCH3] [nvarchar](1) NULL,
	[RUN] [nvarchar](1) NULL,
	[TIPFIRST] [nvarchar](3) NULL,
	[TIPMID] [nvarchar](7) NULL,
	[TIPLAST] [nvarchar](5) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[TYPE] [nvarchar](2) NULL,
	[SSN] [nvarchar](9) NULL,
	[DDANUM] [nvarchar](11) NULL,
	[SAVNUM] [nvarchar](11) NULL,
	[BANK] [nvarchar](4) NULL,
	[CLIENT] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](25) NOT NULL,
	[OLDCC] [nvarchar](16) NULL,
	[NA1] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](30) NULL,
	[PERIOD] [nvarchar](14) NULL,
	[NA2] [nvarchar](40) NULL,
	[NA3] [nvarchar](40) NULL,
	[NA4] [nvarchar](40) NULL,
	[NA5] [nvarchar](40) NULL,
	[NA6] [nvarchar](40) NULL,
	[ADDR1] [nvarchar](40) NULL,
	[ADDR2] [nvarchar](40) NULL,
	[ADDR3] [nvarchar](40) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[ZIP] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[NUMPURCH] [nvarchar](9) NULL,
	[PURCH] [float] NULL,
	[NUMRET] [nvarchar](9) NULL,
	[AMTRET] [float] NULL,
	[RECORDER] [float] NULL,
	[TRANCODE] [char](3) NULL,
	[POINTS] [char](9) NULL,
	[Addr1Ext] [char](4) NULL,
	[Addr2Ext] [char](4) NULL,
	[SigvsPin] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_NUMPURCH]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_NUMPURCH]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] ADD  CONSTRAINT [DF_FBOPdebitcardin_NUMPURCH]  DEFAULT (0) FOR [NUMPURCH]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_PURCH]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_PURCH]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] ADD  CONSTRAINT [DF_FBOPdebitcardin_PURCH]  DEFAULT (0) FOR [PURCH]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_NUMRET]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_NUMRET]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] ADD  CONSTRAINT [DF_FBOPdebitcardin_NUMRET]  DEFAULT (0) FOR [NUMRET]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_AMTRET]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_AMTRET]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] ADD  CONSTRAINT [DF_FBOPdebitcardin_AMTRET]  DEFAULT (0) FOR [AMTRET]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPdebitcardin_SigvsPin]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPdebitcardin]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPdebitcardin_SigvsPin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPdebitcardin] ADD  CONSTRAINT [DF_FBOPdebitcardin_SigvsPin]  DEFAULT (' ') FOR [SigvsPin]
END


End
GO
