USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroger_Daily_Batch_Errors]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Batch_Errors]') AND type in (N'U'))
DROP TABLE [dbo].[Kroger_Daily_Batch_Errors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Batch_Errors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroger_Daily_Batch_Errors](
	[Errorcount] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
