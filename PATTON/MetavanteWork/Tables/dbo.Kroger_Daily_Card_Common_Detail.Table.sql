USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroger_Daily_Card_Common_Detail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Common_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Kroger_Daily_Card_Common_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Common_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroger_Daily_Card_Common_Detail](
	[RecordID] [char](2) NULL,
	[RecordSequence] [char](10) NULL,
	[SegmentID] [char](12) NULL,
	[SegmentVersion] [char](2) NULL,
	[CardNumber] [char](19) NULL,
	[LoyaltyNumber] [char](21) NULL,
	[ReferenceNumber] [char](19) NULL,
	[PrimaryDDA] [char](11) NULL,
	[PrimarySavings] [char](11) NULL,
	[BIN] [char](11) NULL,
	[CardType] [char](2) NULL,
	[CardSubType] [char](2) NULL,
	[CardStatus] [char](1) NULL,
	[CardStatusReason] [char](1) NULL,
	[CardExpirationDate] [char](8) NULL,
	[OriginalIssueDate] [char](8) NULL,
	[LastReissueDate] [char](8) NULL,
	[DateLastUsed] [char](8) NULL,
	[DateFirstUsed] [char](8) NULL,
	[ReplacedCard] [char](19) NULL,
	[ReplacementCard] [char](19) NULL,
	[RewardPlan] [char](3) NULL,
	[RewardID] [char](9) NULL,
	[LoyaltyNumberII] [char](24) NULL,
	[RewardsEnrollmentDate] [char](8) NULL,
	[CardClosedDate] [char](8) NULL,
	[CardClosedTime] [char](4) NULL,
	[CardAssociationID] [char](6) NULL,
	[Filler] [char](236) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
