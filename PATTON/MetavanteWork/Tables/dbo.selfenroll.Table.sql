USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[selfenroll]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenroll]') AND type in (N'U'))
DROP TABLE [dbo].[selfenroll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenroll]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenroll](
	[sid_selfenroll_id] [int] NOT NULL,
	[dim_selfenroll_tipnumber] [varchar](16) NOT NULL,
	[dim_selfenroll_firstname] [varchar](255) NOT NULL,
	[dim_selfenroll_lastname] [varchar](255) NOT NULL,
	[dim_selfenroll_address1] [varchar](255) NOT NULL,
	[dim_selfenroll_address2] [varchar](255) NOT NULL,
	[dim_selfenroll_city] [varchar](255) NOT NULL,
	[dim_selfenroll_state] [varchar](255) NOT NULL,
	[dim_selfenroll_zipcode] [varchar](255) NOT NULL,
	[dim_selfenroll_ssnlast4] [char](10) NOT NULL,
	[dim_selfenroll_cardlastsix] [char](6) NOT NULL,
	[dim_selfenroll_email] [varchar](255) NOT NULL,
	[dim_selfenroll_created] [datetime] NOT NULL,
	[dim_selfenroll_lastmodified] [datetime] NOT NULL,
	[dim_selfenroll_active] [int] NOT NULL,
	[dim_selfenroll_sent_to_Metavante] [datetime] NULL,
	[dim_selfenroll_DDA_number] [varchar](20) NULL,
 CONSTRAINT [PK_selfenroll] PRIMARY KEY CLUSTERED 
(
	[sid_selfenroll_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[selfenroll]') AND name = N'ix_SelfEnroll_TipNumber')
CREATE NONCLUSTERED INDEX [ix_SelfEnroll_TipNumber] ON [dbo].[selfenroll] 
(
	[dim_selfenroll_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
