USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__b2kDemogI__dim_b__45A97EDA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[b2kDemogImportRaw] DROP CONSTRAINT [DF__b2kDemogI__dim_b__45A97EDA]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kDemogImportRaw]    Script Date: 11/15/2010 15:50:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[b2kDemogImportRaw]') AND type in (N'U'))
DROP TABLE [dbo].[b2kDemogImportRaw]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kDemogImportRaw]    Script Date: 11/15/2010 15:50:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[b2kDemogImportRaw](
	[sid_b2kdemogimportraw_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_b2kdemogimportraw_importdate] [datetime] NOT NULL,
	[dim_b2kdemogimportraw_filedate] [varchar](8) NOT NULL,
	[dim_b2kdemogimportraw_recordtype] [varchar](1) NULL,
	[dim_b2kdemogimportraw_relationshipaccount] [varchar](19) NULL,
	[dim_b2kdemogimportraw_cardnumber] [varchar](19) NULL,
	[dim_b2kdemogimportraw_primarycardnumber] [varchar](19) NULL,
	[dim_b2kdemogimportraw_cardtype] [varchar](3) NULL,
	[dim_b2kdemogimportraw_accounttype] [varchar](3) NULL,
	[dim_b2kdemogimportraw_producttype] [varchar](3) NULL,
	[dim_b2kdemogimportraw_productline] [varchar](3) NULL,
	[dim_b2kdemogimportraw_subproducttype] [varchar](3) NULL,
	[dim_b2kdemogimportraw_programid] [varchar](6) NULL,
	[dim_b2kdemogimportraw_primarynameprefix] [varchar](10) NULL,
	[dim_b2kdemogimportraw_primaryfirstname] [varchar](35) NULL,
	[dim_b2kdemogimportraw_primarymiddleinitial] [varchar](1) NULL,
	[dim_b2kdemogimportraw_primarylastname] [varchar](35) NULL,
	[dim_b2kdemogimportraw_primarynamesuffix] [varchar](10) NULL,
	[dim_b2kdemogimportraw_secondarynameprefix] [varchar](10) NULL,
	[dim_b2kdemogimportraw_secondaryfirstname] [varchar](35) NULL,
	[dim_b2kdemogimportraw_secondarymiddleinitial] [varchar](1) NULL,
	[dim_b2kdemogimportraw_secondarylastname] [varchar](35) NULL,
	[dim_b2kdemogimportraw_secondarynamesuffix] [varchar](10) NULL,
	[dim_b2kdemogimportraw_addressline1] [varchar](40) NULL,
	[dim_b2kdemogimportraw_addressline2] [varchar](40) NULL,
	[dim_b2kdemogimportraw_addressline3] [varchar](40) NULL,
	[dim_b2kdemogimportraw_addressline4] [varchar](40) NULL,
	[dim_b2kdemogimportraw_city] [varchar](30) NULL,
	[dim_b2kdemogimportraw_state] [varchar](3) NULL,
	[dim_b2kdemogimportraw_country] [varchar](4) NULL,
	[dim_b2kdemogimportraw_zipcode] [varchar](11) NULL,
	[dim_b2kdemogimportraw_homephonenumber] [varchar](20) NULL,
	[dim_b2kdemogimportraw_workphonenumber] [varchar](20) NULL,
	[dim_b2kdemogimportraw_workphoneextension] [varchar](6) NULL,
	[dim_b2kdemogimportraw_emailaddress] [varchar](60) NULL,
	[dim_b2kdemogimportraw_scprogramstatus] [varchar](1) NULL,
	[dim_b2kdemogimportraw_scprogramstatuschangedate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_hostaccountstatus] [varchar](2) NULL,
	[dim_b2kdemogimportraw_hostacctstatusdate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_scparticipationflag] [varchar](1) NULL,
	[dim_b2kdemogimportraw_primarysocialsecuritynumber] [varchar](13) NULL,
	[dim_b2kdemogimportraw_secondarysocialsecuritynumber] [varchar](13) NULL,
	[dim_b2kdemogimportraw_primarygender] [varchar](1) NULL,
	[dim_b2kdemogimportraw_secondarygender] [varchar](1) NULL,
	[dim_b2kdemogimportraw_birthdate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_accountopendate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_languagepreference] [varchar](3) NULL,
	[dim_b2kdemogimportraw_effectivedate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_purchaserate] [varchar](6) NULL,
	[dim_b2kdemogimportraw_newcardnumber] [varchar](19) NULL,
	[dim_b2kdemogimportraw_associationid] [varchar](2) NULL,
	[dim_b2kdemogimportraw_corpid] [varchar](9) NULL,
	[dim_b2kdemogimportraw_agentinstitutionid] [varchar](6) NULL,
	[dim_b2kdemogimportraw_billcode] [varchar](8) NULL,
	[dim_b2kdemogimportraw_binplan] [varchar](9) NULL,
	[dim_b2kdemogimportraw_scenrollmentdate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_laststatementdate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_transferdate] [varchar](8) NULL,
	[dim_b2kdemogimportraw_filler1] [varchar](74) NULL,
	[dim_b2kdemogimportraw_primaryhouseholdcardnumber] [varchar](19) NULL,
	[dim_b2kdemogimportraw_filler2] [varchar](8) NULL,
	[dim_b2kdemogimportraw_householdcardtype] [varchar](1) NULL,
	[dim_b2kdemogimportraw_institutionid] [varchar](9) NULL,
	[dim_b2kdemogimportraw_vipindicator] [varchar](1) NULL,
	[dim_b2kdemogimportraw_filler3] [varchar](55) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[b2kDemogImportRaw] ADD  DEFAULT (getdate()) FOR [dim_b2kdemogimportraw_importdate]
GO


