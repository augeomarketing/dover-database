USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[bonuserror]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_bonuserror_people]') AND parent_object_id = OBJECT_ID(N'[dbo].[bonuserror]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_bonuserror_people]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[bonuserror] DROP CONSTRAINT [DF_bonuserror_people]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonuserror]') AND type in (N'U'))
DROP TABLE [dbo].[bonuserror]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[bonuserror]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[bonuserror](
	[DBName] [varchar](150) NULL,
	[Distinct Tips] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_bonuserror_people]') AND parent_object_id = OBJECT_ID(N'[dbo].[bonuserror]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_bonuserror_people]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[bonuserror] ADD  CONSTRAINT [DF_bonuserror_people]  DEFAULT (0) FOR [Distinct Tips]
END


End
GO
