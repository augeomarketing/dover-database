USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[PointAdjustment]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointAdjustment]') AND type in (N'U'))
DROP TABLE [dbo].[PointAdjustment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointAdjustment]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PointAdjustment](
	[Ports with a Debit Card] [float] NULL,
	[F2] [float] NULL,
	[F3] [nvarchar](255) NULL,
	[F4] [float] NULL,
	[Tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
