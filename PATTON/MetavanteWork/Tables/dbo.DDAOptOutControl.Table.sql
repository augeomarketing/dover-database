USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DDAOptOutControl]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DDAOptOutControl]') AND type in (N'U'))
DROP TABLE [dbo].[DDAOptOutControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DDAOptOutControl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DDAOptOutControl](
	[Tipnumber] [nchar](15) NOT NULL,
	[DDANumber] [nchar](16) NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DDAOptOutControl]') AND name = N'IX_DDAOptOutControl')
CREATE NONCLUSTERED INDEX [IX_DDAOptOutControl] ON [dbo].[DDAOptOutControl] 
(
	[Tipnumber] ASC,
	[DDANumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
