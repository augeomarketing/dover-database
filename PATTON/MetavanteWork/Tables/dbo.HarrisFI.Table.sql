USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[HarrisFI]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisFI]') AND type in (N'U'))
DROP TABLE [dbo].[HarrisFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisFI]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HarrisFI](
	[TipFirst] [char](3) NOT NULL,
	[FI_Name] [varchar](100) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
