USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[dupwrk1]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dupwrk1]') AND type in (N'U'))
DROP TABLE [dbo].[dupwrk1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dupwrk1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[dupwrk1](
	[ddanum] [nvarchar](11) NULL,
	[tipnumber] [nvarchar](15) NULL,
	[tipnumber2] [char](15) NULL,
	[count1] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
