USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[New_Quarterly_Statement_Detail_Out]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail_Out]') AND type in (N'U'))
DROP TABLE [dbo].[New_Quarterly_Statement_Detail_Out]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail_Out]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[New_Quarterly_Statement_Detail_Out](
	[RecordType] [char](1) NULL,
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [char](9) NULL,
	[PointsEnd] [char](9) NULL,
	[PointsPurchasedCR] [char](9) NULL,
	[PointsPurchasedDB] [char](9) NULL,
	[PointsPurchasedHE] [char](9) NULL,
	[PointsPurchasedBUS] [char](9) NULL,
	[PointsBonusCR] [char](9) NULL,
	[PointsBonusDB] [char](9) NULL,
	[PointsBonusHE] [char](9) NULL,
	[PointsBonusBUS] [char](9) NULL,
	[PointsBonusEmpCR] [char](9) NULL,
	[PointsBonusEmpDB] [char](9) NULL,
	[PointsBonus] [char](9) NULL,
	[PointsAdded] [char](9) NULL,
	[PointsIncreased] [char](9) NULL,
	[PointsRedeemed] [char](9) NULL,
	[PointsReturnedCR] [char](9) NULL,
	[PointsReturnedDB] [char](9) NULL,
	[PointsReturnedHE] [char](9) NULL,
	[PointsReturnedBUS] [char](9) NULL,
	[PointsSubtracted] [char](9) NULL,
	[PointsDecreased] [char](9) NULL,
	[PNTDEBIT] [char](9) NULL,
	[PNTMORT] [char](9) NULL,
	[PNTHOME] [char](9) NULL,
	[PointsToExpire] [char](9) NULL,
	[PointsBonusP2UPlus] [char](9) NULL,
	[Zip] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
