USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[WelcomeKitAll]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WelcomeKitAll]') AND type in (N'U'))
DROP TABLE [dbo].[WelcomeKitAll]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WelcomeKitAll]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WelcomeKitAll](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [char](15) NULL,
	[ZipFour] [char](4) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
