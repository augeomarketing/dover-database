USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[KroegerGenericIn]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerGenericIn]') AND type in (N'U'))
DROP TABLE [dbo].[KroegerGenericIn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerGenericIn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KroegerGenericIn](
	[Col001] [char](250) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
