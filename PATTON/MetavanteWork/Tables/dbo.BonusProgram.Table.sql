USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BonusProgram_BonusProgramType]') AND parent_object_id = OBJECT_ID(N'[dbo].[BonusProgram]'))
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [FK_BonusProgram_BonusProgramType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgramExpirationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 12/02/2010 13:24:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BonusProgram]') AND type in (N'U'))
DROP TABLE [dbo].[BonusProgram]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 12/02/2010 13:24:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BonusProgram](
	[sid_BonusProgram_ID] [int] IDENTITY(1,1) NOT NULL,
	[dim_BonusProgram_Description] [varchar](255) NOT NULL,
	[dim_BonusProgramEffectiveDate] [datetime] NOT NULL,
	[dim_BonusProgramExpirationDate] [datetime] NOT NULL,
	[sid_BonusProgramType_ID] [varchar](1) NOT NULL,
	[sid_TranType_TranCode] [varchar](2) NOT NULL,
	[dim_BonusProgram_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgram_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgram_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgram_BonusProgramType] FOREIGN KEY([sid_BonusProgramType_ID])
REFERENCES [dbo].[BonusProgramType] ([sid_BonusProgramType_ID])
GO

ALTER TABLE [dbo].[BonusProgram] CHECK CONSTRAINT [FK_BonusProgram_BonusProgramType]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]  DEFAULT ('12/31/9999 23:59:59') FOR [dim_BonusProgramExpirationDate]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgram_DateAdded]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgram_LastUpdated]
GO


