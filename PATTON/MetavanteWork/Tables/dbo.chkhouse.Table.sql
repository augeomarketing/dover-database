USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[chkhouse]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkhouse]') AND type in (N'U'))
DROP TABLE [dbo].[chkhouse]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[chkhouse]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[chkhouse](
	[ddanum] [nvarchar](11) NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
