USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[dupwrkO]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dupwrkO]') AND type in (N'U'))
DROP TABLE [dbo].[dupwrkO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dupwrkO]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[dupwrkO](
	[ddanum] [nvarchar](11) NULL,
	[tipnumber] [nvarchar](15) NULL,
	[begbalpri] [int] NULL,
	[tipnumber2] [char](15) NULL,
	[begbalsec] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
