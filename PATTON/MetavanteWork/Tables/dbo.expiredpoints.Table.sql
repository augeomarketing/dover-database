USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[expiredpoints]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[expiredpoints]') AND type in (N'U'))
DROP TABLE [dbo].[expiredpoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[expiredpoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[expiredpoints](
	[tipnumber] [varchar](15) NOT NULL,
	[addpoints] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateOfExpire] [nvarchar](12) NULL,
 CONSTRAINT [PK_expiredpoints] PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
