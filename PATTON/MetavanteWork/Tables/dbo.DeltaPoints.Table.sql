USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DeltaPoints]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DeltaPoints_NewPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeltaPoints]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DeltaPoints_NewPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DeltaPoints] DROP CONSTRAINT [DF_DeltaPoints_NewPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DeltaPoints_OldPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeltaPoints]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DeltaPoints_OldPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DeltaPoints] DROP CONSTRAINT [DF_DeltaPoints_OldPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DeltaPoints_DeltaPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeltaPoints]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DeltaPoints_DeltaPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DeltaPoints] DROP CONSTRAINT [DF_DeltaPoints_DeltaPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeltaPoints]') AND type in (N'U'))
DROP TABLE [dbo].[DeltaPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeltaPoints]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeltaPoints](
	[Tipnumber] [char](15) NULL,
	[Trancode] [char](2) NULL,
	[NewPoints] [numeric](18, 0) NULL,
	[OldPoints] [numeric](18, 0) NULL,
	[DeltaPoints] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DeltaPoints_NewPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeltaPoints]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DeltaPoints_NewPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DeltaPoints] ADD  CONSTRAINT [DF_DeltaPoints_NewPoints]  DEFAULT (0) FOR [NewPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DeltaPoints_OldPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeltaPoints]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DeltaPoints_OldPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DeltaPoints] ADD  CONSTRAINT [DF_DeltaPoints_OldPoints]  DEFAULT (0) FOR [OldPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DeltaPoints_DeltaPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[DeltaPoints]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DeltaPoints_DeltaPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DeltaPoints] ADD  CONSTRAINT [DF_DeltaPoints_DeltaPoints]  DEFAULT (0) FOR [DeltaPoints]
END


End
GO
