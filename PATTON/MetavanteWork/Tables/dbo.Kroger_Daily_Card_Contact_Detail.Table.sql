USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroger_Daily_Card_Contact_Detail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Contact_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Kroger_Daily_Card_Contact_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Contact_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroger_Daily_Card_Contact_Detail](
	[RecordID] [char](2) NULL,
	[RecordSequence] [char](10) NULL,
	[SegmentID] [char](12) NULL,
	[SegmentVersion] [char](2) NULL,
	[CardNumber] [char](19) NULL,
	[LoyaltyNumber] [char](21) NULL,
	[ReferenceNumber] [char](19) NULL,
	[PrimaryDDA] [char](11) NULL,
	[PrimarySavings] [char](11) NULL,
	[PrimaryCardholderFirstName] [char](10) NULL,
	[PrimaryCardholderLastName] [char](15) NULL,
	[SecondaryCardholderName] [char](26) NULL,
	[HomePhoneNumber] [char](10) NULL,
	[BusinessPhoneNumber] [char](10) NULL,
	[PrimarySSN] [char](9) NULL,
	[PrimaryDOB] [char](8) NULL,
	[PrimaryEmail] [char](40) NULL,
	[AuthorizedRedeemer1] [char](25) NULL,
	[AuthorizedRedeemer2] [char](25) NULL,
	[PrimaryCardName] [char](26) NULL,
	[Filler] [char](189) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
