USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[AuditSummary]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0B6ACA3E]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0B6ACA3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__0B6ACA3E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0C5EEE77]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0C5EEE77]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__0C5EEE77]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0D5312B0]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0D5312B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__0D5312B0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0E4736E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0E4736E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__0E4736E9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0F3B5B22]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0F3B5B22]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__0F3B5B22]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__102F7F5B]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__102F7F5B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__102F7F5B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__1123A394]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__1123A394]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__1123A394]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__1217C7CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__1217C7CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__1217C7CD]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__130BEC06]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__130BEC06]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__130BEC06]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__1400103F]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__1400103F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__1400103F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__14F43478]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__14F43478]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__14F43478]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__15E858B1]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__15E858B1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__15E858B1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__16DC7CEA]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__16DC7CEA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] DROP CONSTRAINT [DF__AuditSumm__Point__16DC7CEA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditSummary]') AND type in (N'U'))
DROP TABLE [dbo].[AuditSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuditSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuditSummary](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0B6ACA3E]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0B6ACA3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0C5EEE77]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0C5EEE77]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0D5312B0]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0D5312B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0E4736E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0E4736E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__0F3B5B22]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__0F3B5B22]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__102F7F5B]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__102F7F5B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__1123A394]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__1123A394]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__1217C7CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__1217C7CD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__130BEC06]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__130BEC06]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__1400103F]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__1400103F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__14F43478]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__14F43478]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__15E858B1]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__15E858B1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AuditSumm__Point__16DC7CEA]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuditSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AuditSumm__Point__16DC7CEA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuditSummary] ADD  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
