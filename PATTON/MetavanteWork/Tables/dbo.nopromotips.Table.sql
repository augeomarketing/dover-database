USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[nopromotips]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nopromotips]') AND type in (N'U'))
DROP TABLE [dbo].[nopromotips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nopromotips]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[nopromotips](
	[tipnumber] [char](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[nopromotips]') AND name = N'ix_nopromotips_tipnumber')
CREATE NONCLUSTERED INDEX [ix_nopromotips_tipnumber] ON [dbo].[nopromotips] 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
