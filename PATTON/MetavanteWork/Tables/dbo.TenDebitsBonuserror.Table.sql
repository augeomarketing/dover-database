USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TenDebitsBonuserror]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TenDebitsBonuserror]') AND type in (N'U'))
DROP TABLE [dbo].[TenDebitsBonuserror]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TenDebitsBonuserror]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TenDebitsBonuserror](
	[Bank] [float] NULL,
	[Acct #] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Card #] [nvarchar](255) NULL,
	[Tax ID] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
