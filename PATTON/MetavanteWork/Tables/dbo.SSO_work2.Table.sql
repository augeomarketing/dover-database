USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[SSO_work2]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SSO_work2]') AND type in (N'U'))
DROP TABLE [dbo].[SSO_work2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SSO_work2]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SSO_work2](
	[RNI_Number] [char](15) NULL,
	[Deleted_DateTime] [char](16) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SSO_work2]') AND name = N'ix_sso_work2_RNI_Number')
CREATE NONCLUSTERED INDEX [ix_sso_work2_RNI_Number] ON [dbo].[SSO_work2] 
(
	[RNI_Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
