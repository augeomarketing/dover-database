USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[EmployeeIncentiveBonusSAVE]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeIncentiveBonusSAVE]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeIncentiveBonusSAVE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeIncentiveBonusSAVE]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeIncentiveBonusSAVE](
	[BankNbr] [nvarchar](255) NULL,
	[Officer Name] [nvarchar](255) NULL,
	[SumOfPoints] [float] NULL,
	[SSN] [nvarchar](255) NULL,
	[Tipnumber] [char](15) NULL,
	[LastName] [char](40) NULL,
	[TypeCard] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
