USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[New_Quarterly_Statement_Detail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBegin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsEnd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedHE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusHE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusEmpCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusEmpCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusEmpCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusEmpDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusEmpDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusEmpDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsIncreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsRedeemed]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedHE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsSubtracted]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsDecreased]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PNTDEBIT]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PNTDEBIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PNTDEBIT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PNTMORT]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PNTMORT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PNTMORT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PNTHOME]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PNTHOME]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PNTHOME]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusP2UPlus]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusP2UPlus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] DROP CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusP2UPlus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[New_Quarterly_Statement_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[New_Quarterly_Statement_Detail](
	[RecordType] [char](1) NULL,
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](9, 0) NULL,
	[PointsEnd] [numeric](9, 0) NULL,
	[PointsPurchasedCR] [numeric](9, 0) NULL,
	[PointsPurchasedDB] [numeric](9, 0) NULL,
	[PointsPurchasedHE] [numeric](9, 0) NULL,
	[PointsPurchasedBUS] [numeric](9, 0) NULL,
	[PointsBonusCR] [numeric](9, 0) NULL,
	[PointsBonusDB] [numeric](9, 0) NULL,
	[PointsBonusHE] [numeric](9, 0) NULL,
	[PointsBonusBUS] [numeric](9, 0) NULL,
	[PointsBonusEmpCR] [numeric](9, 0) NULL,
	[PointsBonusEmpDB] [numeric](9, 0) NULL,
	[PointsBonus] [numeric](9, 0) NULL,
	[PointsAdded] [numeric](9, 0) NULL,
	[PointsIncreased] [numeric](9, 0) NULL,
	[PointsRedeemed] [numeric](9, 0) NULL,
	[PointsReturnedCR] [numeric](9, 0) NULL,
	[PointsReturnedDB] [numeric](9, 0) NULL,
	[PointsReturnedHE] [numeric](9, 0) NULL,
	[PointsReturnedBUS] [numeric](9, 0) NULL,
	[PointsSubtracted] [numeric](9, 0) NULL,
	[PointsDecreased] [numeric](9, 0) NULL,
	[PNTDEBIT] [numeric](9, 0) NULL,
	[PNTMORT] [numeric](9, 0) NULL,
	[PNTHOME] [numeric](9, 0) NULL,
	[PointsToExpire] [numeric](9, 0) NULL,
	[PointsBonusP2UPlus] [numeric](9, 0) NULL,
	[Zip] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBegin]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsEnd]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedCR]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedHE]  DEFAULT (0) FOR [PointsPurchasedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsPurchasedBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsPurchasedBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsPurchasedBUS]  DEFAULT (0) FOR [PointsPurchasedBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusCR]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusDB]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusHE]  DEFAULT (0) FOR [PointsBonusHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusBUS]  DEFAULT (0) FOR [PointsBonusBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusEmpCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusEmpCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusEmpCR]  DEFAULT (0) FOR [PointsBonusEmpCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusEmpDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusEmpDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusEmpDB]  DEFAULT (0) FOR [PointsBonusEmpDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonus]  DEFAULT (0) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsIncreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedCR]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedHE]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedHE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedHE]  DEFAULT (0) FOR [PointsReturnedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsReturnedBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsReturnedBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsReturnedBUS]  DEFAULT (0) FOR [PointsReturnedBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsSubtracted]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsDecreased]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PNTDEBIT]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PNTDEBIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PNTDEBIT]  DEFAULT (0) FOR [PNTDEBIT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PNTMORT]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PNTMORT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PNTMORT]  DEFAULT (0) FOR [PNTMORT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PNTHOME]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PNTHOME]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PNTHOME]  DEFAULT (0) FOR [PNTHOME]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsToExpire]  DEFAULT (0) FOR [PointsToExpire]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_New_Quarterly_Statement_Detail_PointsBonusP2UPlus]') AND parent_object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_New_Quarterly_Statement_Detail_PointsBonusP2UPlus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[New_Quarterly_Statement_Detail] ADD  CONSTRAINT [DF_New_Quarterly_Statement_Detail_PointsBonusP2UPlus]  DEFAULT (0) FOR [PointsBonusP2UPlus]
END


End
GO
