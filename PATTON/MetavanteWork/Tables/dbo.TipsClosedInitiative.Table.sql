USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TipsClosedInitiative]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipsClosedInitiative]') AND type in (N'U'))
DROP TABLE [dbo].[TipsClosedInitiative]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TipsClosedInitiative]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TipsClosedInitiative](
	[tipnumber] [varchar](15) NULL,
	[acctname1] [varchar](40) NULL,
	[runavailable] [decimal](10, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
