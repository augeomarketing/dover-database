USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[selfenrollsend]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollsend]') AND type in (N'U'))
DROP TABLE [dbo].[selfenrollsend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[selfenrollsend]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[selfenrollsend](
	[sid_selfenroll_id] [int] NULL,
	[dim_selfenroll_tipnumber] [varchar](16) NOT NULL,
	[dim_selfenroll_firstname] [varchar](255) NOT NULL,
	[dim_selfenroll_lastname] [varchar](255) NOT NULL,
	[dim_selfenroll_address1] [varchar](255) NOT NULL,
	[dim_selfenroll_address2] [varchar](255) NOT NULL,
	[dim_selfenroll_city] [varchar](255) NOT NULL,
	[dim_selfenroll_state] [varchar](255) NOT NULL,
	[dim_selfenroll_zipcode] [varchar](255) NOT NULL,
	[dim_selfenroll_ssnlast4] [char](10) NOT NULL,
	[dim_selfenroll_cardlastsix] [char](6) NOT NULL,
	[dim_selfenroll_email] [varchar](255) NOT NULL,
	[dim_selfenroll_created] [datetime] NOT NULL,
	[dim_selfenroll_dda_number] [varchar](20) NULL,
	[dim_affiliat_acctid] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
