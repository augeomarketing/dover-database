USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[557tipstatus]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[557tipstatus]') AND type in (N'U'))
DROP TABLE [dbo].[557tipstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[557tipstatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[557tipstatus](
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
