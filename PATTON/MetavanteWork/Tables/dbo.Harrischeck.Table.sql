USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Harrischeck]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Harrischeck]') AND type in (N'U'))
DROP TABLE [dbo].[Harrischeck]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Harrischeck]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Harrischeck](
	[acct_num] [nvarchar](25) NULL
) ON [PRIMARY]
END
GO
