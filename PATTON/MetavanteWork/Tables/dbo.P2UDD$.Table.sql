USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[P2UDD$]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[P2UDD$]') AND type in (N'U'))
DROP TABLE [dbo].[P2UDD$]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[P2UDD$]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[P2UDD$](
	[DDA] [char](10) NULL,
	[Tri-County Bank ] [nvarchar](255) NULL,
	[F3] [float] NULL,
	[F4] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL,
	[F6] [nvarchar](255) NULL,
	[F7] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[F9] [nvarchar](255) NULL,
	[OCT] [nvarchar](255) NULL,
	[Black] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
