USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[MetavanteCardProblems]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MetavanteCardProblems]') AND type in (N'U'))
DROP TABLE [dbo].[MetavanteCardProblems]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MetavanteCardProblems]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MetavanteCardProblems](
	[tipfirst] [nvarchar](3) NULL,
	[acctnum] [nvarchar](25) NULL
) ON [PRIMARY]
END
GO
