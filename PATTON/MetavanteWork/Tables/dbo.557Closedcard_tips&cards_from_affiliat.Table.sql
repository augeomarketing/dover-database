USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[557Closedcard_tips&cards_from_affiliat]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[557Closedcard_tips&cards_from_affiliat]') AND type in (N'U'))
DROP TABLE [dbo].[557Closedcard_tips&cards_from_affiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[557Closedcard_tips&cards_from_affiliat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[557Closedcard_tips&cards_from_affiliat](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[inclosed] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
