USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[MagnaFix]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MagnaFix]') AND type in (N'U'))
DROP TABLE [dbo].[MagnaFix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MagnaFix]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MagnaFix](
	[Dr/ATM Card Holder Number] [nvarchar](16) NULL,
	[Dr/ATM Card Holder Name 1] [nvarchar](40) NULL
) ON [PRIMARY]
END
GO
