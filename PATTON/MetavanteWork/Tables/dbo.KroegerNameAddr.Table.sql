USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[KroegerNameAddr]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerNameAddr]') AND type in (N'U'))
DROP TABLE [dbo].[KroegerNameAddr]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerNameAddr]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KroegerNameAddr](
	[NoMail] [char](1) NULL,
	[MR] [char](1) NULL,
	[MRS] [char](1) NULL,
	[MS] [char](1) NULL,
	[LoyaltyNumber] [varchar](24) NULL,
	[LastName] [varchar](18) NULL,
	[FirstName] [varchar](12) NULL,
	[MiddleName] [varchar](1) NULL,
	[Addr1] [varchar](40) NULL,
	[Addr2] [varchar](40) NULL,
	[City] [varchar](17) NULL,
	[State] [char](2) NULL,
	[Zip] [char](9) NULL,
	[HomePhone] [varchar](10) NULL,
	[OldLoyaltyNumber] [varchar](24) NULL,
	[CardNumber] [char](16) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
