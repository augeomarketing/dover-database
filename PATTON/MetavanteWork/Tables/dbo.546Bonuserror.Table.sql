USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[546Bonuserror]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[546Bonuserror]') AND type in (N'U'))
DROP TABLE [dbo].[546Bonuserror]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[546Bonuserror]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[546Bonuserror](
	[Tipnumber] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
END
GO
