USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[packagecodes]    Script Date: 02/18/2010 09:13:05 ******/
/*
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[packagecodes]') AND type in (N'U'))
DROP TABLE [dbo].[packagecodes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[packagecodes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[packagecodes](
	[TipFirst] [char](3) NULL,
	[DI_id] [int] NULL,
	[CA_id] [int] NULL,
	[PW_id] [int] NULL,
	[UB_id] [int] NULL,
	[WK_id] [int] NULL,
	[NM_id] [int] NULL,
	[QS_id] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
*/

ALTER TABLE dbo.packagecodes
ADD	DI_HH_id int default (0)