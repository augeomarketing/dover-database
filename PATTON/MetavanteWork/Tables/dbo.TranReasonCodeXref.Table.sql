USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_tranreasoncodexref_trantype]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranReasonCodeXref]'))
ALTER TABLE [dbo].[TranReasonCodeXref] DROP CONSTRAINT [fk_tranreasoncodexref_trantype]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TranReaso__dim_t__18A1CE39]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranReasonCodeXref] DROP CONSTRAINT [DF__TranReaso__dim_t__18A1CE39]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TranReaso__dim_t__1995F272]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TranReasonCodeXref] DROP CONSTRAINT [DF__TranReaso__dim_t__1995F272]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[TranReasonCodeXref]    Script Date: 11/15/2010 16:06:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranReasonCodeXref]') AND type in (N'U'))
DROP TABLE [dbo].[TranReasonCodeXref]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[TranReasonCodeXref]    Script Date: 11/15/2010 16:06:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TranReasonCodeXref](
	[sid_tranreasoncodexref_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_tranreasoncodexref_trancodein] [varchar](10) NOT NULL,
	[dim_tranreasoncodexref_reasoncodein] [varchar](10) NOT NULL,
	[sid_tranreasoncodexref_trancode] [nvarchar](2) NULL,
	[dim_tranreasoncodexref_created] [datetime] NOT NULL,
	[dim_tranreasoncodexref_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [pk_tranreasoncodexref] PRIMARY KEY CLUSTERED 
(
	[dim_tranreasoncodexref_trancodein] ASC,
	[dim_tranreasoncodexref_reasoncodein] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TranReasonCodeXref]  WITH CHECK ADD  CONSTRAINT [fk_tranreasoncodexref_trantype] FOREIGN KEY([sid_tranreasoncodexref_trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[TranReasonCodeXref] CHECK CONSTRAINT [fk_tranreasoncodexref_trantype]
GO

ALTER TABLE [dbo].[TranReasonCodeXref] ADD  DEFAULT (getdate()) FOR [dim_tranreasoncodexref_created]
GO

ALTER TABLE [dbo].[TranReasonCodeXref] ADD  DEFAULT (getdate()) FOR [dim_tranreasoncodexref_lastmodified]
GO


