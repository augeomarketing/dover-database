USE MetavanteWork
GO

IF OBJECT_ID(N'KrogerBonusPreProcessing') IS NOT NULL
	DROP TABLE KrogerBonusPreProcessing
GO

CREATE TABLE KrogerBonusPreProcessing
(
	dim_krogerbonuspreprocessing_id INT IDENTITY(1,1) PRIMARY KEY
	, dim_krogerbonuspreprocessing_source VARCHAR(255)
	, dim_krogerbonuspreprocessing_row INT
	, dim_krogerbonuspreprocessing_histdate DATE
	, sid_dbprocessinfo_dbnumber VARCHAR(3)
	, dim_krogerbonuspreprocessing_tipnumber VARCHAR(15)
	, dim_krogerbonuspreprocessing_portfolionumber VARCHAR(20)
	, dim_krogerbonuspreprocessing_membernumber VARCHAR(20)
	, dim_krogerbonuspreprocessing_tinssn VARCHAR(20)
	, dim_krogerbonuspreprocessing_producttype INT
	, dim_krogerbonuspreprocessing_cardnumber VARCHAR(16)
	, dim_krogerbonuspreprocessing_transactiondate DATETIME
	, dim_krogerbonuspreprocessing_transfercardnumber VARCHAR(16)
	, dim_krogerbonuspreprocessing_transactioncode INTEGER
	, dim_krogerbonuspreprocessing_ddanumber VARCHAR(20)
	, dim_krogerbonuspreprocessing_transactionamount INTEGER
	, dim_krogerbonuspreprocessing_transactioncount INTEGER
	, dim_krogerbonuspreprocessing_transactiondescription VARCHAR(50)
	, dim_krogerbonuspreprocessing_currencycode VARCHAR(3)
	, dim_krogerbonuspreprocessing_merchantid VARCHAR(50)
	, dim_krogerbonuspreprocessing_transactionid VARCHAR(50)
	, dim_krogerbonuspreprocessing_authorizationcode VARCHAR(6)
	
)

GO

