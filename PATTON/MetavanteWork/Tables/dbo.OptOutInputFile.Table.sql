USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[OptOutInputFile]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OptOutInputFile]') AND type in (N'U'))
DROP TABLE [dbo].[OptOutInputFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OptOutInputFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OptOutInputFile](
	[Tipnumber] [nvarchar](255) NULL,
	[Acctname1] [nvarchar](255) NULL,
	[DDA] [nvarchar](255) NULL,
	[Acctnum] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
