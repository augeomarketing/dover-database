USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[getdbname]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getdbname]') AND type in (N'U'))
DROP TABLE [dbo].[getdbname]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getdbname]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[getdbname](
	[dbnamepatton] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
