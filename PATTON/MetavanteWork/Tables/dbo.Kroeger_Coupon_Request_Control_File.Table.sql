USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroeger_Coupon_Request_Control_File]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Control_File]') AND type in (N'U'))
DROP TABLE [dbo].[Kroeger_Coupon_Request_Control_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Control_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroeger_Coupon_Request_Control_File](
	[StatementFileNumber] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
