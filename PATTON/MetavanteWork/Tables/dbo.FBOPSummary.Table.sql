USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPSummary]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__02A9D05D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__02A9D05D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__02A9D05D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__039DF496]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__039DF496]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__039DF496]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__049218CF]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__049218CF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__049218CF]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__05863D08]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__05863D08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__05863D08]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__067A6141]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__067A6141]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__067A6141]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsPurchasedbus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsPurchasedbus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF_FBOPSummary_PointsPurchasedbus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__076E857A]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__076E857A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__076E857A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0862A9B3]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0862A9B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0862A9B3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0956CDEC]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0956CDEC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0956CDEC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsBonusbus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsBonusbus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF_FBOPSummary_PointsBonusbus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsBonusEmpCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsBonusEmpCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF_FBOPSummary_PointsBonusEmpCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsBonusEmpDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsBonusEmpDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF_FBOPSummary_PointsBonusEmpDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0A4AF225]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0A4AF225]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0A4AF225]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0B3F165E]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0B3F165E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0B3F165E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0C333A97]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0C333A97]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0C333A97]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0D275ED0]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0D275ED0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0D275ED0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0E1B8309]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0E1B8309]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0E1B8309]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0F0FA742]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0F0FA742]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__0F0FA742]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__1003CB7B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__1003CB7B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__1003CB7B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsReturnedbus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsReturnedbus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF_FBOPSummary_PointsReturnedbus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__10F7EFB4]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__10F7EFB4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__10F7EFB4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__11EC13ED]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__11EC13ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__Point__11EC13ED]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__PNTDE__12E03826]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__PNTDE__12E03826]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__PNTDE__12E03826]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__PNTMO__13D45C5F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__PNTMO__13D45C5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__PNTMO__13D45C5F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__PNTHO__14C88098]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__PNTHO__14C88098]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF__FBOPSumma__PNTHO__14C88098]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] DROP CONSTRAINT [DF_FBOPSummary_PointsToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPSummary]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPSummary](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedbus] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusbus] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedbus] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
	[PointsToExpire] [numeric](18, 0) NULL,
 CONSTRAINT [PK_FBOPStatement] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__02A9D05D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__02A9D05D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__02A9D05D]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__039DF496]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__039DF496]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__039DF496]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__049218CF]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__049218CF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__049218CF]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__05863D08]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__05863D08]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__05863D08]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__067A6141]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__067A6141]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__067A6141]  DEFAULT (0) FOR [PointsPurchasedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsPurchasedbus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsPurchasedbus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF_FBOPSummary_PointsPurchasedbus]  DEFAULT (0) FOR [PointsPurchasedbus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__076E857A]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__076E857A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__076E857A]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0862A9B3]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0862A9B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0862A9B3]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0956CDEC]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0956CDEC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0956CDEC]  DEFAULT (0) FOR [PointsBonusHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsBonusbus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsBonusbus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF_FBOPSummary_PointsBonusbus]  DEFAULT (0) FOR [PointsBonusbus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsBonusEmpCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsBonusEmpCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF_FBOPSummary_PointsBonusEmpCR]  DEFAULT (0) FOR [PointsBonusEmpCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsBonusEmpDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsBonusEmpDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF_FBOPSummary_PointsBonusEmpDB]  DEFAULT (0) FOR [PointsBonusEmpDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0A4AF225]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0A4AF225]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0A4AF225]  DEFAULT (0) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0B3F165E]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0B3F165E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0B3F165E]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0C333A97]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0C333A97]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0C333A97]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0D275ED0]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0D275ED0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0D275ED0]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0E1B8309]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0E1B8309]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0E1B8309]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__0F0FA742]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__0F0FA742]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__0F0FA742]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__1003CB7B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__1003CB7B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__1003CB7B]  DEFAULT (0) FOR [PointsReturnedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsReturnedbus]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsReturnedbus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF_FBOPSummary_PointsReturnedbus]  DEFAULT (0) FOR [PointsReturnedbus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__10F7EFB4]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__10F7EFB4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__10F7EFB4]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__Point__11EC13ED]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__Point__11EC13ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__Point__11EC13ED]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__PNTDE__12E03826]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__PNTDE__12E03826]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__PNTDE__12E03826]  DEFAULT (0) FOR [PNTDEBIT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__PNTMO__13D45C5F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__PNTMO__13D45C5F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__PNTMO__13D45C5F]  DEFAULT (0) FOR [PNTMORT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPSumma__PNTHO__14C88098]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPSumma__PNTHO__14C88098]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF__FBOPSumma__PNTHO__14C88098]  DEFAULT (0) FOR [PNTHOME]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPSummary_PointsToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPSummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPSummary_PointsToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPSummary] ADD  CONSTRAINT [DF_FBOPSummary_PointsToExpire]  DEFAULT (0) FOR [PointsToExpire]
END


End
GO
