USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroeger_Coupon_Request_Trailer]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Kroeger_Coupon_Request_Trailer_TotalRecords]') AND parent_object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Trailer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Kroeger_Coupon_Request_Trailer_TotalRecords]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Kroeger_Coupon_Request_Trailer] DROP CONSTRAINT [DF_Kroeger_Coupon_Request_Trailer_TotalRecords]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Kroeger_Coupon_Request_Trailer_TotalPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Trailer]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Kroeger_Coupon_Request_Trailer_TotalPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Kroeger_Coupon_Request_Trailer] DROP CONSTRAINT [DF_Kroeger_Coupon_Request_Trailer_TotalPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Trailer]') AND type in (N'U'))
DROP TABLE [dbo].[Kroeger_Coupon_Request_Trailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Trailer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroeger_Coupon_Request_Trailer](
	[RecordType] [char](1) NULL,
	[TotalRecords] [int] NULL,
	[TotalPoints] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Kroeger_Coupon_Request_Trailer_TotalRecords]') AND parent_object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Trailer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Kroeger_Coupon_Request_Trailer_TotalRecords]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Kroeger_Coupon_Request_Trailer] ADD  CONSTRAINT [DF_Kroeger_Coupon_Request_Trailer_TotalRecords]  DEFAULT (0) FOR [TotalRecords]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Kroeger_Coupon_Request_Trailer_TotalPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Trailer]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Kroeger_Coupon_Request_Trailer_TotalPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Kroeger_Coupon_Request_Trailer] ADD  CONSTRAINT [DF_Kroeger_Coupon_Request_Trailer_TotalPoints]  DEFAULT (0) FOR [TotalPoints]
END


End
GO
