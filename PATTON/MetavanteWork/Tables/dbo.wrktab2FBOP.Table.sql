USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrktab2FBOP]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab2FBOP]') AND type in (N'U'))
DROP TABLE [dbo].[wrktab2FBOP]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab2FBOP]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktab2FBOP](
	[ssn] [nvarchar](9) NULL,
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
