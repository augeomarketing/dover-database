USE [MetavanteWork]
GO
/****** Object:  View [dbo].[FBOPCardsin_View_by_Tipnumber]    Script Date: 01/11/2010 17:14:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCardsin_View_by_Tipnumber]'))
DROP VIEW [dbo].[FBOPCardsin_View_by_Tipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCardsin_View_by_Tipnumber]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[FBOPCardsin_View_by_Tipnumber]
AS
SELECT     TOP 100 PERCENT dbo.FBOPCARDSIN.*, TIPNUMBER AS Expr1, SSN AS Expr2, DDANUM AS Expr3
FROM         dbo.FBOPCARDSIN
ORDER BY TIPNUMBER, SSN, DDANUM

'
GO
