USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroger_Daily_Batch_Trailer]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Batch_Trailer]') AND type in (N'U'))
DROP TABLE [dbo].[Kroger_Daily_Batch_Trailer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Batch_Trailer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroger_Daily_Batch_Trailer](
	[RecordID] [char](2) NULL,
	[RecordSequence] [char](10) NULL,
	[SegmentID] [char](12) NULL,
	[SegmentVersion] [char](2) NULL,
	[BatchID] [char](20) NULL,
	[ClientID] [char](5) NULL,
	[ClientName] [char](35) NULL,
	[BatchRecordCount] [numeric](18, 0) NULL,
	[ProcessorCode] [char](3) NULL,
	[ProcessorName] [char](20) NULL,
	[BatchDebitTransactionCount] [numeric](18, 0) NULL,
	[BatchDebitTransactionAmount] [numeric](18, 2) NULL,
	[BatchCreditTransactionCount] [numeric](18, 0) NULL,
	[BatchCreditTransactionAmount] [numeric](18, 2) NULL,
	[Filler] [char](301) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
