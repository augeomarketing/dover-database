USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPAccountRollUp]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPAccountRollUp_sigvspin]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPAccountRollUp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPAccountRollUp_sigvspin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPAccountRollUp] DROP CONSTRAINT [DF_FBOPAccountRollUp_sigvspin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPAccountRollUp]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPAccountRollUp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPAccountRollUp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPAccountRollUp](
	[TIPFIRST] [nvarchar](3) NULL,
	[TIPMID] [nvarchar](7) NULL,
	[TIPLAST] [nvarchar](5) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[TYPE] [nvarchar](2) NULL,
	[SSN] [nvarchar](9) NULL,
	[DDANUM] [nvarchar](11) NULL,
	[SAVNUM] [nvarchar](11) NULL,
	[BANK] [nvarchar](4) NULL,
	[CLIENT] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[OLDCC] [nvarchar](16) NULL,
	[NA1] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](30) NULL,
	[PERIOD] [nvarchar](14) NULL,
	[NA2] [nvarchar](40) NULL,
	[NA3] [nvarchar](40) NULL,
	[NA4] [nvarchar](40) NULL,
	[NA5] [nvarchar](40) NULL,
	[NA6] [nvarchar](40) NULL,
	[ADDR1] [nvarchar](40) NULL,
	[ADDR2] [nvarchar](40) NULL,
	[ADDR3] [nvarchar](40) NULL,
	[CITYSTATE] [nvarchar](40) NULL,
	[ZIP] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[TRANCODE] [char](3) NULL,
	[POINTS] [char](9) NULL,
	[sigvspin] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPAccountRollUp_sigvspin]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPAccountRollUp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPAccountRollUp_sigvspin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPAccountRollUp] ADD  CONSTRAINT [DF_FBOPAccountRollUp_sigvspin]  DEFAULT (' ') FOR [sigvspin]
END


End
GO
