USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPWork]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPWork]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPWork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPWork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPWork](
	[bank] [varchar](255) NULL,
	[account] [varchar](255) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
