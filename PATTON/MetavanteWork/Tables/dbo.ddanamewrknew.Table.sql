USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[ddanamewrknew]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ddanamewrknew]') AND type in (N'U'))
DROP TABLE [dbo].[ddanamewrknew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ddanamewrknew]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ddanamewrknew](
	[tipnumber] [varchar](15) NOT NULL,
	[name1] [varchar](40) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
