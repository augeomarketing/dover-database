USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrknomatch3]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrknomatch3]') AND type in (N'U'))
DROP TABLE [dbo].[wrknomatch3]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrknomatch3]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrknomatch3](
	[ssn] [varchar](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
