USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TRANS12]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANS12]') AND type in (N'U'))
DROP TABLE [dbo].[TRANS12]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANS12]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TRANS12](
	[TIPNUMBER] [nvarchar](15) NULL,
	[PERIOD] [nvarchar](10) NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANAMOUNT] [float] NULL,
	[POINTS] [float] NULL,
	[TRANTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[LASTACT_DT] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
