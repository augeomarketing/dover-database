USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Harrisselfenrollsend]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Harrisselfenrollsend_Record_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Harrisselfenrollsend]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Harrisselfenrollsend_Record_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Harrisselfenrollsend] DROP CONSTRAINT [DF_Harrisselfenrollsend_Record_Id]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Harrisselfenrollsend_Reward_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Harrisselfenrollsend]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Harrisselfenrollsend_Reward_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Harrisselfenrollsend] DROP CONSTRAINT [DF_Harrisselfenrollsend_Reward_Id]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Harrisselfenrollsend]') AND type in (N'U'))
DROP TABLE [dbo].[Harrisselfenrollsend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Harrisselfenrollsend]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Harrisselfenrollsend](
	[Record_Id] [char](4) NOT NULL,
	[Institution_Id] [char](5) NOT NULL,
	[Cardholder_Account_Number] [char](19) NOT NULL,
	[Update_Date] [char](8) NOT NULL,
	[Update_Time] [char](6) NOT NULL,
	[Prefix] [char](11) NOT NULL,
	[Reward_Plan] [char](3) NOT NULL,
	[Reward_Id] [char](9) NOT NULL,
	[Member_Id] [char](24) NULL,
	[Entollment_Date] [char](8) NOT NULL,
	[Authorized_Redeemer_1] [char](25) NULL,
	[Authorized_Redeemer_2] [char](25) NULL,
	[Filler] [char](945) NULL,
	[sid_selfenroll_id] [int] NULL,
	[dim_selfenroll_tipnumber] [varchar](16) NOT NULL,
	[dim_selfenroll_firstname] [varchar](255) NOT NULL,
	[dim_selfenroll_lastname] [varchar](255) NOT NULL,
	[dim_selfenroll_address1] [varchar](255) NOT NULL,
	[dim_selfenroll_address2] [varchar](255) NOT NULL,
	[dim_selfenroll_city] [varchar](255) NOT NULL,
	[dim_selfenroll_state] [varchar](255) NOT NULL,
	[dim_selfenroll_zipcode] [varchar](255) NOT NULL,
	[dim_selfenroll_ssnlast4] [char](10) NOT NULL,
	[dim_selfenroll_cardlastsix] [char](6) NOT NULL,
	[dim_selfenroll_email] [varchar](255) NOT NULL,
	[dim_selfenroll_created] [datetime] NOT NULL,
	[dim_selfenroll_dda_number] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Harrisselfenrollsend_Record_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Harrisselfenrollsend]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Harrisselfenrollsend_Record_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Harrisselfenrollsend] ADD  CONSTRAINT [DF_Harrisselfenrollsend_Record_Id]  DEFAULT ('0320') FOR [Record_Id]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Harrisselfenrollsend_Reward_Id]') AND parent_object_id = OBJECT_ID(N'[dbo].[Harrisselfenrollsend]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Harrisselfenrollsend_Reward_Id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Harrisselfenrollsend] ADD  CONSTRAINT [DF_Harrisselfenrollsend_Reward_Id]  DEFAULT ('0001') FOR [Reward_Id]
END


End
GO
