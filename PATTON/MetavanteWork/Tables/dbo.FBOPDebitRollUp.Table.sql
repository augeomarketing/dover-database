USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPDebitRollUp]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPDebitRollUp_sigvspin]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPDebitRollUp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPDebitRollUp_sigvspin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPDebitRollUp] DROP CONSTRAINT [DF_FBOPDebitRollUp_sigvspin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPDebitRollUp]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPDebitRollUp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPDebitRollUp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPDebitRollUp](
	[acctnum] [nvarchar](25) NULL,
	[numpurch] [int] NULL,
	[amtpurch] [float] NULL,
	[numcr] [int] NULL,
	[amtcr] [float] NULL,
	[sigvspin] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_FBOPDebitRollUp_sigvspin]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPDebitRollUp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_FBOPDebitRollUp_sigvspin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPDebitRollUp] ADD  CONSTRAINT [DF_FBOPDebitRollUp_sigvspin]  DEFAULT (' ') FOR [sigvspin]
END


End
GO
