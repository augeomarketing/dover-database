USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[MissingSpend]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MissingSpend]') AND type in (N'U'))
DROP TABLE [dbo].[MissingSpend]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MissingSpend]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MissingSpend](
	[Bank] [float] NULL,
	[NewAcct] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[OldAcct] [nvarchar](255) NULL,
	[Spend] [float] NULL,
	[Credit / Debit flag] [nvarchar](255) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
