USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Revised_Quarterly_Statement]    Script Date: 09/30/2010 11:28:01 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_CRPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_CRPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_CRPurch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_DBPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_DBPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_DBPurch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_HELPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_HELPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_HELPurch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_BUSPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_BUSPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_BUSPurch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_CRBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_CRBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_CRBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_DBBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_DBBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_DBBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_HELBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_HELBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_HELBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_BUSBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_BUSBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_BUSBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_BONUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_BONUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_EmpCRBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_EmpCRBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_EmpCRBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_EmpDBBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_EmpDBBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_EmpDBBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_AdjustAdd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_AdjustAdd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_AdjustAdd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_TotalAdd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_TotalAdd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_TotalAdd]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnDB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnHEL]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnHEL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnHEL]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnBUS]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_Redeem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_Redeem]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_AdjustSUB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_AdjustSUB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_AdjustSUB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_TotalSUB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_TotalSUB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_TotalSUB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTBEG]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTBEG]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_PNTBEG]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTEND]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTEND]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_PNTEND]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ExpiringPts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ExpiringPts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_ExpiringPts]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_P2UPlusBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_P2UPlusBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_P2UPlusBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_DirDepBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_DirDepBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_DirDepBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_WithdrwlBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_WithdrwlBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_WithdrwlBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_EstmtFIBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_EstmtFIBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_EstmtFIBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_AvgDepositBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_AvgDepositBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_AvgDepositBonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTDEBIT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTDEBIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_PNTDEBIT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTMORT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTMORT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_PNTMORT]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTHOME]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTHOME]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] DROP CONSTRAINT [DF_Revised_Quarterly_Statement_PNTHOME]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]') AND type in (N'U'))
DROP TABLE [dbo].[Revised_Quarterly_Statement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Revised_Quarterly_Statement](
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](5) NULL,
	[Tip] [nchar](15) NOT NULL,
	[CRPurch] [numeric](9, 0) NULL,
	[DBPurch] [numeric](9, 0) NULL,
	[HELPurch] [numeric](9, 0) NULL,
	[BUSPurch] [numeric](9, 0) NULL,
	[CRBonus] [numeric](9, 0) NULL,
	[DBBonus] [numeric](9, 0) NULL,
	[HELBonus] [numeric](9, 0) NULL,
	[BUSBonus] [numeric](9, 0) NULL,
	[BONUS] [numeric](9, 0) NULL,
	[EmpCRBonus] [numeric](9, 0) NULL,
	[EmpDBBonus] [numeric](9, 0) NULL,
	[AdjustAdd] [numeric](9, 0) NULL,
	[TotalAdd] [numeric](9, 0) NULL,
	[ReturnCR] [numeric](9, 0) NULL,
	[ReturnDB] [numeric](9, 0) NULL,
	[ReturnHEL] [numeric](9, 0) NULL,
	[ReturnBUS] [numeric](9, 0) NULL,
	[Redeem] [numeric](9, 0) NULL,
	[AdjustSUB] [numeric](9, 0) NULL,
	[TotalSUB] [numeric](9, 0) NULL,
	[PNTBEG] [numeric](9, 0) NULL,
	[PNTEND] [numeric](9, 0) NULL,
	[ExpiringPts] [numeric](9, 0) NULL,
	[P2UPlusBonus] [numeric](9, 0) NULL,
	[DirDepBonus] [numeric](9, 0) NULL,
	[WthdrwlBonus] [numeric](9, 0) NULL,
	[EstmtFIBonus] [numeric](9, 0) NULL,
	[AvgDepositBonus] [numeric](9, 0) NULL,
	[PNTDEBIT] [numeric](9, 0) NULL,
	[PNTMORT] [numeric](9, 0) NULL,
	[PNTHOME] [numeric](9, 0) NULL,
 CONSTRAINT [PK_Revised_Quarterly_Statement] PRIMARY KEY CLUSTERED 
(
	[Tip] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_CRPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_CRPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_CRPurch]  DEFAULT ((0)) FOR [CRPurch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_DBPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_DBPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_DBPurch]  DEFAULT ((0)) FOR [DBPurch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_HELPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_HELPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_HELPurch]  DEFAULT ((0)) FOR [HELPurch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_BUSPurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_BUSPurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_BUSPurch]  DEFAULT ((0)) FOR [BUSPurch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_CRBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_CRBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_CRBonus]  DEFAULT ((0)) FOR [CRBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_DBBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_DBBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_DBBonus]  DEFAULT ((0)) FOR [DBBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_HELBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_HELBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_HELBonus]  DEFAULT ((0)) FOR [HELBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_BUSBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_BUSBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_BUSBonus]  DEFAULT ((0)) FOR [BUSBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_BONUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_BONUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_BONUS]  DEFAULT ((0)) FOR [BONUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_EmpCRBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_EmpCRBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_EmpCRBonus]  DEFAULT ((0)) FOR [EmpCRBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_EmpDBBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_EmpDBBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_EmpDBBonus]  DEFAULT ((0)) FOR [EmpDBBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_AdjustAdd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_AdjustAdd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_AdjustAdd]  DEFAULT ((0)) FOR [AdjustAdd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_TotalAdd]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_TotalAdd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_TotalAdd]  DEFAULT ((0)) FOR [TotalAdd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnCR]  DEFAULT ((0)) FOR [ReturnCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnDB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnDB]  DEFAULT ((0)) FOR [ReturnDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnHEL]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnHEL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnHEL]  DEFAULT ((0)) FOR [ReturnHEL]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ReturnBUS]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ReturnBUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_ReturnBUS]  DEFAULT ((0)) FOR [ReturnBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_Redeem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_Redeem]  DEFAULT ((0)) FOR [Redeem]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_AdjustSUB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_AdjustSUB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_AdjustSUB]  DEFAULT ((0)) FOR [AdjustSUB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_TotalSUB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_TotalSUB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_TotalSUB]  DEFAULT ((0)) FOR [TotalSUB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTBEG]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTBEG]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_PNTBEG]  DEFAULT ((0)) FOR [PNTBEG]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTEND]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTEND]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_PNTEND]  DEFAULT ((0)) FOR [PNTEND]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_ExpiringPts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_ExpiringPts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_ExpiringPts]  DEFAULT ((0)) FOR [ExpiringPts]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_P2UPlusBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_P2UPlusBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_P2UPlusBonus]  DEFAULT ((0)) FOR [P2UPlusBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_DirDepBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_DirDepBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_DirDepBonus]  DEFAULT ((0)) FOR [DirDepBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_WithdrwlBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_WithdrwlBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_WithdrwlBonus]  DEFAULT ((0)) FOR [WthdrwlBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_EstmtFIBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_EstmtFIBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_EstmtFIBonus]  DEFAULT ((0)) FOR [EstmtFIBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_AvgDepositBonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_AvgDepositBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_AvgDepositBonus]  DEFAULT ((0)) FOR [AvgDepositBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTDEBIT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTDEBIT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_PNTDEBIT]  DEFAULT ((0)) FOR [PNTDEBIT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTMORT]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTMORT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_PNTMORT]  DEFAULT ((0)) FOR [PNTMORT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Revised_Quarterly_Statement_PNTHOME]') AND parent_object_id = OBJECT_ID(N'[dbo].[Revised_Quarterly_Statement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Revised_Quarterly_Statement_PNTHOME]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Revised_Quarterly_Statement] ADD  CONSTRAINT [DF_Revised_Quarterly_Statement_PNTHOME]  DEFAULT ((0)) FOR [PNTHOME]
END


End
GO
