USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[EStmt_Registration ]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Registration ]') AND type in (N'U'))
DROP TABLE [dbo].[EStmt_Registration ]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Registration ]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EStmt_Registration ](
	[tipnumber] [char](20) NOT NULL,
	[Awarded] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
