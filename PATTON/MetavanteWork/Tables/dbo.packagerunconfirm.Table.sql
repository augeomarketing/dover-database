USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[packagerunconfirm]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[packagerunconfirm]') AND type in (N'U'))
DROP TABLE [dbo].[packagerunconfirm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[packagerunconfirm]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[packagerunconfirm](
	[TipFirst] [varchar](3) NULL,
	[type] [varchar](10) NULL,
	[DataMonth] [varchar](10) NULL,
	[Rundate] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
