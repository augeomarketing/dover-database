USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Niagaratest]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Niagaratest]') AND type in (N'U'))
DROP TABLE [dbo].[Niagaratest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Niagaratest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Niagaratest](
	[SSN] [nvarchar](255) NULL,
	[Primary Account Name] [nvarchar](255) NULL,
	[CardNumber] [nvarchar](255) NULL,
	[Points] [nvarchar](255) NULL,
	[F10] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
