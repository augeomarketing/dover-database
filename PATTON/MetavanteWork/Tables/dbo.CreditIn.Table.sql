USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[CreditIn]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreditIn]') AND type in (N'U'))
DROP TABLE [dbo].[CreditIn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreditIn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CreditIn](
	[Col001] [char](1) NULL,
	[BK_NUM] [char](6) NULL,
	[AGT_BK_NUM] [char](6) NULL,
	[ACCT_NUM] [char](16) NULL,
	[NAME1] [char](25) NULL,
	[NAME2] [char](25) NULL,
	[CR_RATING] [char](6) NULL,
	[TRANS_ACCT_NUM] [char](18) NULL,
	[ADDR1] [char](36) NULL,
	[ADDR2] [char](36) NULL,
	[CITY] [char](25) NULL,
	[STATE] [char](5) NULL,
	[ZIP] [char](5) NULL,
	[HOME_PHONE] [char](13) NULL,
	[WORK_PHONE] [char](13) NULL,
	[SSN] [char](12) NULL,
	[AMT] [float] NULL,
	[DEB_CRED_STAT] [char](4) NULL,
	[DDA] [char](18) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
