USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrktab4]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab4]') AND type in (N'U'))
DROP TABLE [dbo].[wrktab4]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab4]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktab4](
	[ACCTNUM] [nvarchar](25) NULL,
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
