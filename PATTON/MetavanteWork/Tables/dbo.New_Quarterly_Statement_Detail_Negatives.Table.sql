USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[New_Quarterly_Statement_Detail_Negatives]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail_Negatives]') AND type in (N'U'))
DROP TABLE [dbo].[New_Quarterly_Statement_Detail_Negatives]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Detail_Negatives]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[New_Quarterly_Statement_Detail_Negatives](
	[RecordType] [char](1) NULL,
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [decimal](9, 0) NULL,
	[PointsEnd] [decimal](9, 0) NULL,
	[PointsPurchasedCR] [decimal](9, 0) NULL,
	[PointsPurchasedDB] [decimal](9, 0) NULL,
	[PointsPurchasedHE] [decimal](9, 0) NULL,
	[PointsPurchasedBUS] [decimal](9, 0) NULL,
	[PointsBonusCR] [decimal](9, 0) NULL,
	[PointsBonusDB] [decimal](9, 0) NULL,
	[PointsBonusHE] [decimal](9, 0) NULL,
	[PointsBonusBUS] [decimal](9, 0) NULL,
	[PointsBonusEmpCR] [decimal](9, 0) NULL,
	[PointsBonusEmpDB] [decimal](9, 0) NULL,
	[PointsBonus] [decimal](9, 0) NULL,
	[PointsAdded] [decimal](9, 0) NULL,
	[PointsIncreased] [decimal](9, 0) NULL,
	[PointsRedeemed] [decimal](9, 0) NULL,
	[PointsReturnedCR] [decimal](9, 0) NULL,
	[PointsReturnedDB] [decimal](9, 0) NULL,
	[PointsReturnedHE] [decimal](9, 0) NULL,
	[PointsReturnedBUS] [decimal](9, 0) NULL,
	[PointsSubtracted] [decimal](9, 0) NULL,
	[PointsDecreased] [decimal](9, 0) NULL,
	[PNTDEBIT] [decimal](9, 0) NULL,
	[PNTMORT] [decimal](9, 0) NULL,
	[PNTHOME] [decimal](9, 0) NULL,
	[PointsToExpire] [decimal](9, 0) NULL,
	[PointsBonusP2UPlus] [decimal](9, 0) NULL,
	[Zip] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
