USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[New_Quarterly_Statement_Header]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Header]') AND type in (N'U'))
DROP TABLE [dbo].[New_Quarterly_Statement_Header]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[New_Quarterly_Statement_Header]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[New_Quarterly_Statement_Header](
	[RecordID] [char](1) NULL,
	[FileName] [char](16) NULL,
	[BatchNumber] [char](10) NULL,
	[CreationDate] [char](8) NULL,
	[CreationTime] [char](6) NULL,
	[SiteName] [char](5) NULL,
	[StatementCycleIndicator] [char](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
