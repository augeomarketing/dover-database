USE [MetavanteWork]
GO
/****** Object:  View [dbo].[Cardsin_view]    Script Date: 01/11/2010 17:14:25 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Cardsin_view]'))
DROP VIEW [dbo].[Cardsin_view]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Cardsin_view]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[Cardsin_view] AS 
			SELECT tipnumber, ssn, ddanum, na1, na2, na3, na4, na5, na6
			FROM cardsin
			group by tipnumber, ssn, ddanum, na1, na2, na3, na4, na5, na6 
			'
GO
