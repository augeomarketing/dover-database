USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_EndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_EndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_EndingPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Increases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Increases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Increases]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Decreases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Decreases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_Decreases]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_AdjustedEndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_AdjustedEndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND name = N'ix_Current_Month_Activity_tipnumber')
CREATE NONCLUSTERED INDEX [ix_Current_Month_Activity_tipnumber] ON [dbo].[Current_Month_Activity] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_EndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_EndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT (0) FOR [EndingPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Increases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Increases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT (0) FOR [Increases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_Decreases]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_Decreases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT (0) FOR [Decreases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Current_Month_Activity_AdjustedEndingPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_AdjustedEndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT (0) FOR [AdjustedEndingPoints]
END


End
GO
