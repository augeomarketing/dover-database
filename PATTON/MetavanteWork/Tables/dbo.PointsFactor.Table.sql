USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[PointsFactor]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__PointsFac__Revis__79412D0B]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PointsFac__Revis__79412D0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] DROP CONSTRAINT [DF__PointsFac__Revis__79412D0B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__PointsFac__Start__7A355144]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PointsFac__Start__7A355144]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] DROP CONSTRAINT [DF__PointsFac__Start__7A355144]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__PointsFac__Endda__7B29757D]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PointsFac__Endda__7B29757D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] DROP CONSTRAINT [DF__PointsFac__Endda__7B29757D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsFactor]') AND type in (N'U'))
DROP TABLE [dbo].[PointsFactor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsFactor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PointsFactor](
	[DBNumber] [nchar](3) NOT NULL,
	[BIN] [nvarchar](9) NOT NULL,
	[Revision] [smallint] NOT NULL,
	[Startdate] [datetime] NOT NULL,
	[Enddate] [datetime] NOT NULL,
	[Credit] [numeric](5, 2) NOT NULL,
	[SigFactor] [numeric](5, 2) NOT NULL,
	[PinFactor] [numeric](5, 2) NOT NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__PointsFac__Revis__79412D0B]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PointsFac__Revis__79412D0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] ADD  DEFAULT (0) FOR [Revision]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__PointsFac__Start__7A355144]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PointsFac__Start__7A355144]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] ADD  DEFAULT (1 / 1 / 1900) FOR [Startdate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__PointsFac__Endda__7B29757D]') AND parent_object_id = OBJECT_ID(N'[dbo].[PointsFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__PointsFac__Endda__7B29757D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PointsFactor] ADD  DEFAULT (1 / 1 / 1900) FOR [Enddate]
END


End
GO
