USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[debitcardinKroger]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Socia__52ACDD8F]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Socia__52ACDD8F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Socia__52ACDD8F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Point__53A101C8]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Point__53A101C8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Point__53A101C8]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Home __54952601]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Home __54952601]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Home __54952601]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Busin__55894A3A]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Busin__55894A3A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Busin__55894A3A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Prima__567D6E73]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Prima__567D6E73]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Prima__567D6E73]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Secon__577192AC]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Secon__577192AC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Secon__577192AC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Enrol__5865B6E5]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Enrol__5865B6E5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Enrol__5865B6E5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Expir__5959DB1E]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Expir__5959DB1E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Expir__5959DB1E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Origi__5A4DFF57]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Origi__5A4DFF57]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Origi__5A4DFF57]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Last __5B422390]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Last __5B422390]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Last __5B422390]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Date __5C3647C9]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Date __5C3647C9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Date __5C3647C9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__DDA A__5D2A6C02]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__DDA A__5D2A6C02]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__DDA A__5D2A6C02]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__SAV A__5E1E903B]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__SAV A__5E1E903B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__SAV A__5E1E903B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Total__5F12B474]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Total__5F12B474]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF__debitcard__Total__5F12B474]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_Numpurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_Numpurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF_debitcardinKroger_Numpurch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF_debitcardinKroger_purch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_Numret]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_Numret]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF_debitcardinKroger_Numret]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_Amtret]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_Amtret]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] DROP CONSTRAINT [DF_debitcardinKroger_Amtret]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]') AND type in (N'U'))
DROP TABLE [dbo].[debitcardinKroger]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[debitcardinKroger](
	[Client Number] [nvarchar](11) NULL,
	[Primary Acct Number] [nvarchar](17) NULL,
	[Social Security Number] [nvarchar](9) NULL,
	[Full Name] [nvarchar](26) NULL,
	[Secondary Name] [nvarchar](26) NULL,
	[Address Line 1] [nvarchar](26) NULL,
	[Address Line 2] [nvarchar](26) NULL,
	[City / State] [nvarchar](21) NULL,
	[Zip Code] [nvarchar](5) NULL,
	[Points Sign] [nvarchar](1) NULL,
	[Points] [numeric](6, 0) NULL,
	[Home Phone] [nvarchar](10) NULL,
	[Business Phone] [nvarchar](10) NULL,
	[Transaction Code] [nvarchar](3) NULL,
	[External Status] [nvarchar](1) NULL,
	[New Change Flag] [nvarchar](1) NULL,
	[Code Name] [nvarchar](8) NULL,
	[Primary Birthday] [nvarchar](6) NULL,
	[Secondary Birthday] [nvarchar](6) NULL,
	[Maintenance Flag] [nvarchar](1) NULL,
	[Enrollment Switch] [nvarchar](1) NULL,
	[Enrollment Points] [numeric](6, 0) NULL,
	[Wrk Full Primary Acct Num] [nvarchar](19) NULL,
	[Expiration Date] [nvarchar](8) NULL,
	[Original Issue Date] [nvarchar](8) NULL,
	[Last Reissue Date] [nvarchar](8) NULL,
	[Date Last Use] [nvarchar](8) NULL,
	[Fin Inst Number] [nvarchar](5) NULL,
	[Card Status] [nvarchar](1) NULL,
	[DDA Acct Number] [numeric](11, 0) NULL,
	[SAV Acct Number] [numeric](11, 0) NULL,
	[Address Line 1 - Last 4] [nvarchar](4) NULL,
	[Address Line 2 - Last 4] [nvarchar](4) NULL,
	[Total Dollar Cents] [numeric](9, 0) NULL,
	[Cross reference card numer] [nvarchar](19) NULL,
	[Signature or PIN] [nvarchar](1) NULL,
	[Reward Plan] [nvarchar](3) NULL,
	[Reward ID] [nvarchar](9) NULL,
	[Enrollment Date] [nvarchar](8) NULL,
	[Rewards Member ID] [nvarchar](24) NULL,
	[Filler] [nvarchar](113) NULL,
	[Recnum] [int] IDENTITY(1,1) NOT NULL,
	[tipfirst] [char](3) NULL,
	[Numpurch] [nvarchar](9) NULL,
	[purch] [float] NULL,
	[Numret] [nvarchar](9) NULL,
	[Amtret] [float] NULL,
	[Status] [char](1) NULL,
	[Tipmid] [nvarchar](7) NULL,
	[Tiplast] [nvarchar](5) NULL,
	[Tipnumber] [nchar](15) NULL,
	[Period] [nvarchar](14) NULL,
	[Lastname] [nvarchar](30) NULL,
	[Cardtype] [char](1) NULL,
 CONSTRAINT [PK_debitcardinKroger_1] PRIMARY KEY CLUSTERED 
(
	[Recnum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Socia__52ACDD8F]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Socia__52ACDD8F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Socia__52ACDD8F]  DEFAULT (0) FOR [Social Security Number]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Point__53A101C8]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Point__53A101C8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Point__53A101C8]  DEFAULT (0) FOR [Points]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Home __54952601]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Home __54952601]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Home __54952601]  DEFAULT (0) FOR [Home Phone]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Busin__55894A3A]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Busin__55894A3A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Busin__55894A3A]  DEFAULT (0) FOR [Business Phone]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Prima__567D6E73]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Prima__567D6E73]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Prima__567D6E73]  DEFAULT (0) FOR [Primary Birthday]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Secon__577192AC]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Secon__577192AC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Secon__577192AC]  DEFAULT (0) FOR [Secondary Birthday]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Enrol__5865B6E5]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Enrol__5865B6E5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Enrol__5865B6E5]  DEFAULT (0) FOR [Enrollment Points]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Expir__5959DB1E]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Expir__5959DB1E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Expir__5959DB1E]  DEFAULT (0) FOR [Expiration Date]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Origi__5A4DFF57]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Origi__5A4DFF57]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Origi__5A4DFF57]  DEFAULT (0) FOR [Original Issue Date]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Last __5B422390]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Last __5B422390]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Last __5B422390]  DEFAULT (0) FOR [Last Reissue Date]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Date __5C3647C9]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Date __5C3647C9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Date __5C3647C9]  DEFAULT (0) FOR [Date Last Use]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__DDA A__5D2A6C02]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__DDA A__5D2A6C02]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__DDA A__5D2A6C02]  DEFAULT (0) FOR [DDA Acct Number]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__SAV A__5E1E903B]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__SAV A__5E1E903B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__SAV A__5E1E903B]  DEFAULT (0) FOR [SAV Acct Number]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__debitcard__Total__5F12B474]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__debitcard__Total__5F12B474]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF__debitcard__Total__5F12B474]  DEFAULT (0) FOR [Total Dollar Cents]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_Numpurch]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_Numpurch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF_debitcardinKroger_Numpurch]  DEFAULT (0) FOR [Numpurch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF_debitcardinKroger_purch]  DEFAULT (0) FOR [purch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_Numret]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_Numret]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF_debitcardinKroger_Numret]  DEFAULT (0) FOR [Numret]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_debitcardinKroger_Amtret]') AND parent_object_id = OBJECT_ID(N'[dbo].[debitcardinKroger]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_debitcardinKroger_Amtret]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[debitcardinKroger] ADD  CONSTRAINT [DF_debitcardinKroger_Amtret]  DEFAULT (0) FOR [Amtret]
END


End
GO
