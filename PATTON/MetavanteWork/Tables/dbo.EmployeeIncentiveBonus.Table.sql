USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[EmployeeIncentiveBonus]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeIncentiveBonus]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeIncentiveBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeIncentiveBonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeIncentiveBonus](
	[BankNbr] [nvarchar](255) NULL,
	[Officer Name] [nvarchar](255) NULL,
	[SumOfPoints] [float] NULL,
	[SSN] [nvarchar](255) NULL,
	[Tipnumber] [char](15) NULL,
	[LastName] [char](40) NULL,
	[TypeCard] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
