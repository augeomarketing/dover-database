USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroger_Daily_Card_Transaction_Detail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Transaction_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Kroger_Daily_Card_Transaction_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroger_Daily_Card_Transaction_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroger_Daily_Card_Transaction_Detail](
	[RecordID] [char](2) NULL,
	[RecordSequence] [char](10) NULL,
	[SegmentID] [char](12) NULL,
	[SegmentVersion] [char](2) NULL,
	[CardNumber] [char](19) NULL,
	[LoyaltyNumber] [char](21) NULL,
	[ReferenceNumber] [char](19) NULL,
	[PrimaryDDA] [char](11) NULL,
	[PrimarySavings] [char](11) NULL,
	[TransactionCode] [char](4) NULL,
	[DebitCreditIndicator] [char](1) NULL,
	[Pinned/NonPinnedIndicator] [char](1) NULL,
	[TransactionAmount] [numeric](9, 2) NULL,
	[CashBackAmount] [numeric](9, 2) NULL,
	[CurrencyCode] [numeric](3, 0) NULL,
	[TerminalReference] [char](12) NULL,
	[AuthorizationCode] [char](8) NULL,
	[TransactionDate] [char](8) NULL,
	[TransactionTime] [char](6) NULL,
	[PostingDate] [char](8) NULL,
	[TransactionType] [char](5) NULL,
	[Filler] [char](319) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
