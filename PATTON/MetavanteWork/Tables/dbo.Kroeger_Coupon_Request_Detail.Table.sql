USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroeger_Coupon_Request_Detail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Kroeger_Coupon_Detail_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Detail]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Kroeger_Coupon_Detail_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Kroeger_Coupon_Request_Detail] DROP CONSTRAINT [DF_Kroeger_Coupon_Detail_PointsRedeemed]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Detail]') AND type in (N'U'))
DROP TABLE [dbo].[Kroeger_Coupon_Request_Detail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroeger_Coupon_Request_Detail](
	[RecordType] [char](1) NULL,
	[LoyaltyNumber] [char](15) NULL,
	[CardHolderName] [char](40) NULL,
	[PointsRedeemed] [char](9) NULL,
	[TransID] [char](36) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Kroeger_Coupon_Detail_PointsRedeemed]') AND parent_object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Detail]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Kroeger_Coupon_Detail_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Kroeger_Coupon_Request_Detail] ADD  CONSTRAINT [DF_Kroeger_Coupon_Detail_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
