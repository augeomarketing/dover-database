USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TrancodeXref]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrancodeXref]') AND type in (N'U'))
DROP TABLE [dbo].[TrancodeXref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TrancodeXref]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TrancodeXref](
	[TrancodeIn] [char](3) NOT NULL,
	[TrancodeOut] [char](2) NOT NULL,
 CONSTRAINT [PK_TrancodeXref] PRIMARY KEY CLUSTERED 
(
	[TrancodeIn] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrancodeXref]') AND name = N'IX_TrancodeXref_TranCodeIn_TranCodeOut')
CREATE NONCLUSTERED INDEX [IX_TrancodeXref_TranCodeIn_TranCodeOut] ON [dbo].[TrancodeXref] 
(
	[TrancodeIn] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
