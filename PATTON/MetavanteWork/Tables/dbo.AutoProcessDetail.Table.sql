USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[autoprocessdetail]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[autoprocessdetail]') AND type in (N'U'))
DROP TABLE [dbo].[autoprocessdetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[autoprocessdetail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[autoprocessdetail](
	[TipFirst] [char](3) NULL,
	[DBName1] [varchar](100) NULL,
	[DBName2] [varchar](100) NULL,
	[SourceFileNameString] [varchar](100) NULL,
	[BackupNamestring] [varchar](100) NULL,
	[MonthBeginingDate] [char](10) NULL,
	[MonthEndingdate] [char](10) NULL,
	[RNClientCode] [char](100) NULL,
	[DI_id] [int] NULL,
	[CA_id] [int] NULL,
	[PW_id] [int] NULL,
	[UB_id] [int] NULL,
	[WK_id] [int] NULL,
	[NM_id] [int] NULL,
	[QS_id] [int] NULL,
	[Debit/Credit] [char](1) NULL,
	[MissingNameOut] [varchar](200) NULL,
	[QuarterBeginingDate] [char](10) NULL,
	[QuarterEndingdate] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
