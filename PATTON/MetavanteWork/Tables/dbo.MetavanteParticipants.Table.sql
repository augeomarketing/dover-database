USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[MetavanteParticipants]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MetavanteParticipants]') AND type in (N'U'))
DROP TABLE [dbo].[MetavanteParticipants]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MetavanteParticipants]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MetavanteParticipants](
	[TipFirst] [char](5) NULL,
	[CreditCardCount] [int] NULL,
	[DebitCardCount] [int] NULL,
	[TotalCount] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
