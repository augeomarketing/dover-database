USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__b2kTranIm__dim_b__4F32E914]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[b2kTranImportRaw] DROP CONSTRAINT [DF__b2kTranIm__dim_b__4F32E914]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kTranImportRaw]    Script Date: 11/29/2010 10:57:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[b2kTranImportRaw]') AND type in (N'U'))
DROP TABLE [dbo].[b2kTranImportRaw]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[b2kTranImportRaw]    Script Date: 11/29/2010 10:57:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SET ANSI_PADDING ON
--GO

CREATE TABLE [dbo].[b2kTranImportRaw](
	[sid_b2ktranimportraw_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_b2ktranimportraw_importdate] [datetime] NOT NULL,
	[dim_b2ktranimportraw_filedate] [varchar](8) NOT NULL,
	[dim_b2ktranimportraw_corpid] [varchar](9) NULL,
	[dim_b2ktranimportraw_agentinstitutionid] [varchar](6) NULL,
	[dim_b2ktranimportraw_accountnumber] [varchar](19) NULL,
	[dim_b2ktranimportraw_trandate] [varchar](8) NULL,
	[dim_b2ktranimportraw_transactionpostdate] [varchar](8) NULL,
	[dim_b2ktranimportraw_transactionsign] [varchar](1) NULL,
	[dim_b2ktranimportraw_transactionamount] [varchar](13) NULL,
	[dim_b2ktranimportraw_prescoredearnings] [varchar](13) NULL,
	[dim_b2ktranimportraw_merchantname] [varchar](40) NULL,
	[dim_b2ktranimportraw_merchantcity] [varchar](30) NULL,
	[dim_b2ktranimportraw_merchantstate] [varchar](3) NULL,
	[dim_b2ktranimportraw_merchantcountry] [varchar](4) NULL,
	[dim_b2ktranimportraw_mccsiccode] [varchar](4) NULL,
	[dim_b2ktranimportraw_industrycode] [varchar](5) NULL,
	[dim_b2ktranimportraw_transactioncode] [varchar](2) NULL,
	[dim_b2ktranimportraw_reasoncode] [varchar](2) NULL,
	[dim_b2ktranimportraw_transactioncurrencyidentifier] [varchar](3) NULL,
	[dim_b2ktranimportraw_pricingplanid] [varchar](6) NULL,
	[dim_b2ktranimportraw_transactiontype] [varchar](1) NULL,
	[dim_b2ktranimportraw_referencenumber] [varchar](23) NULL,
	[dim_b2ktranimportraw_eciindicator] [varchar](20) NULL,
	[dim_b2ktranimportraw_vendorindicator] [varchar](1) NULL,
	[dim_b2ktranimportraw_filler1] [varchar](8) NULL,
	[dim_b2ktranimportraw_merchantid] [varchar](15) NULL,
	[dim_b2ktranimportraw_filler2] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF

ALTER TABLE [dbo].[b2kTranImportRaw] ADD  DEFAULT (getdate()) FOR [dim_b2ktranimportraw_importdate]
GO


