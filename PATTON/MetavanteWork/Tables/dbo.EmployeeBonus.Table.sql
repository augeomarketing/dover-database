USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[EmployeeBonus]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeBonus]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeeBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeBonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmployeeBonus](
	[card #] [nvarchar](255) NULL,
	[name] [nvarchar](255) NULL,
	[Bonus Pts] [float] NULL,
	[Referal Pts] [nvarchar](255) NULL,
	[Bal/Trans Bonus] [nvarchar](255) NULL,
	[Total Pts#] [float] NULL
) ON [PRIMARY]
END
GO
