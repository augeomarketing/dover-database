USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPMissing]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPMissing]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPMissing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPMissing]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPMissing](
	[Bank] [float] NULL,
	[New Accout Number] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Old Acct Number] [nvarchar](255) NULL,
	[Spend] [float] NULL,
	[Credit / Debit flag] [nvarchar](255) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
