USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[weirdstatus]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[weirdstatus]') AND type in (N'U'))
DROP TABLE [dbo].[weirdstatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[weirdstatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[weirdstatus](
	[tipnumber] [char](15) NULL,
	[histdate] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
