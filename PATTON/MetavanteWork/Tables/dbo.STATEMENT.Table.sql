USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[STATEMENT]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STATEMENT]') AND type in (N'U'))
DROP TABLE [dbo].[STATEMENT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[STATEMENT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[STATEMENT](
	[TRAVNUM] [nvarchar](255) NULL,
	[ACCTNAME1] [nvarchar](255) NULL,
	[ACCTNAME2] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[LASTLINE] [nvarchar](255) NULL,
	[STDATE] [nvarchar](255) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTDECRS] [float] NULL,
	[ZIP] [nvarchar](255) NULL,
	[ACCT_NUM] [nvarchar](255) NULL,
	[LASTFOUR] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
