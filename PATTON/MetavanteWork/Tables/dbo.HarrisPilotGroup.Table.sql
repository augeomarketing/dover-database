USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[HarrisPilotGroup]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisPilotGroup]') AND type in (N'U'))
DROP TABLE [dbo].[HarrisPilotGroup]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HarrisPilotGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HarrisPilotGroup](
	[Last ] [nvarchar](255) NULL,
	[First] [nvarchar](255) NULL,
	[Middle Initial] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[state ] [nvarchar](255) NULL,
	[zip] [float] NULL,
	[last six of card] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
