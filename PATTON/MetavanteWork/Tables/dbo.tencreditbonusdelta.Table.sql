USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[tencreditbonusdelta]    Script Date: 01/11/2010 17:14:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tencreditbonusdelta]') AND type in (N'U'))
DROP TABLE [dbo].[tencreditbonusdelta]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tencreditbonusdelta]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tencreditbonusdelta](
	[tipnumber] [nchar](15) NULL,
	[pointsposted] [float] NULL,
	[pointsshouldhavebeenposted] [float] NULL
) ON [PRIMARY]
END
GO
