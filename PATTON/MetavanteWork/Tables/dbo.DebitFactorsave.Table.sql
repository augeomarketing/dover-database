USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DebitFactorsave]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitFactorsave]') AND type in (N'U'))
DROP TABLE [dbo].[DebitFactorsave]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitFactorsave]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DebitFactorsave](
	[DBNumber] [nchar](3) NOT NULL,
	[Revision] [smallint] NOT NULL,
	[Startdate] [datetime] NOT NULL,
	[Enddate] [datetime] NOT NULL,
	[DebitFactor] [decimal](18, 2) NOT NULL,
	[SigFactor] [decimal](18, 0) NOT NULL,
	[PinFactor] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
END
GO
