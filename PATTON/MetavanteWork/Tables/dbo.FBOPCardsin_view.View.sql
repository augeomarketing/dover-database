USE [MetavanteWork]
GO
/****** Object:  View [dbo].[FBOPCardsin_view]    Script Date: 01/11/2010 17:14:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCardsin_view]'))
DROP VIEW [dbo].[FBOPCardsin_view]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[FBOPCardsin_view]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[FBOPCardsin_view] AS 
			SELECT tipnumber, ssn, ddanum, na1, na2, na3, na4, na5, na6
			FROM FBOPcardsin
			group by tipnumber, ssn, ddanum, na1, na2, na3, na4, na5, na6 
			
'
GO
