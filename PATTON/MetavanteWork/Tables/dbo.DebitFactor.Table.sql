USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DebitFactor]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DebitFact__Revis__384F51F2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DebitFact__Revis__384F51F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] DROP CONSTRAINT [DF__DebitFact__Revis__384F51F2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DebitFact__Start__3943762B]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DebitFact__Start__3943762B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] DROP CONSTRAINT [DF__DebitFact__Start__3943762B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DebitFact__Endda__3A379A64]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DebitFact__Endda__3A379A64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] DROP CONSTRAINT [DF__DebitFact__Endda__3A379A64]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DebitFactor_SigFactor]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DebitFactor_SigFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] DROP CONSTRAINT [DF_DebitFactor_SigFactor]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DebitFactor_PinFactor]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DebitFactor_PinFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] DROP CONSTRAINT [DF_DebitFactor_PinFactor]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitFactor]') AND type in (N'U'))
DROP TABLE [dbo].[DebitFactor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DebitFactor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DebitFactor](
	[DBNumber] [nchar](3) NOT NULL,
	[Revision] [smallint] NOT NULL,
	[Startdate] [datetime] NOT NULL,
	[Enddate] [datetime] NOT NULL,
	[SigFactor] [numeric](18, 0) NOT NULL,
	[PinFactor] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DebitFact__Revis__384F51F2]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DebitFact__Revis__384F51F2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] ADD  CONSTRAINT [DF__DebitFact__Revis__384F51F2]  DEFAULT (0) FOR [Revision]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DebitFact__Start__3943762B]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DebitFact__Start__3943762B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] ADD  CONSTRAINT [DF__DebitFact__Start__3943762B]  DEFAULT (1 / 1 / 1900) FOR [Startdate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DebitFact__Endda__3A379A64]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DebitFact__Endda__3A379A64]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] ADD  CONSTRAINT [DF__DebitFact__Endda__3A379A64]  DEFAULT (1 / 1 / 1900) FOR [Enddate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DebitFactor_SigFactor]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DebitFactor_SigFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] ADD  CONSTRAINT [DF_DebitFactor_SigFactor]  DEFAULT (2) FOR [SigFactor]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DebitFactor_PinFactor]') AND parent_object_id = OBJECT_ID(N'[dbo].[DebitFactor]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DebitFactor_PinFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DebitFactor] ADD  CONSTRAINT [DF_DebitFactor_PinFactor]  DEFAULT (2) FOR [PinFactor]
END


End
GO
