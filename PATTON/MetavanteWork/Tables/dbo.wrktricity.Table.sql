USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrktricity]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktricity]') AND type in (N'U'))
DROP TABLE [dbo].[wrktricity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktricity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktricity](
	[dda] [char](10) NULL,
	[points] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
