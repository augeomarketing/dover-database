USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[packagename]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[packagename]') AND type in (N'U'))
DROP TABLE [dbo].[packagename]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[packagename]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[packagename](
	[type] [varchar](50) NULL,
	[id] [int] NULL,
	[path] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
