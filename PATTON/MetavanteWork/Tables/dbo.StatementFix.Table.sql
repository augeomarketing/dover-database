USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[StatementFix]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatementFix]') AND type in (N'U'))
DROP TABLE [dbo].[StatementFix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StatementFix]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StatementFix](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[address4] [varchar](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
