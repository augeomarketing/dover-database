USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[ESTMTBONUS]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ESTMTBONUS]') AND type in (N'U'))
DROP TABLE [dbo].[ESTMTBONUS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ESTMTBONUS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ESTMTBONUS](
	[Tax ID Number] [float] NULL,
	[First Name] [nvarchar](255) NULL,
	[Last Name] [nvarchar](255) NULL,
	[Points] [float] NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
