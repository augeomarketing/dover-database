USE MetavanteWork
GO

CREATE TABLE b2kTranImportStatus
(
	sid_b2ktranimportstatus_id bigint identity(0,1) primary key
	, dim_b2ktranimportstatus_name VARCHAR(50)
	, dim_b2ktranimportstatus_desc VARCHAR(MAX)
	, dim_b2ktranimportstatus_dateadded datetime not null default(getdate())
	, dim_b2ktranimportstatus_lastmodified datetime
	, dim_b2ktranimportstatus_lastmodifiedby varchar(255)
)
GO

CREATE TRIGGER b2kTranImportStatus_InsertUpdate_AddLastModifiedInfo
ON b2kTranImportStatus
AFTER INSERT, UPDATE
AS

SET NOCOUNT ON
SET XACT_ABORT ON
SET ARITHABORT ON

DECLARE             
	@Now datetime
	, @User varchar(255)

SELECT  
	@Now = GetDate()
	, @User = SYSTEM_USER

UPDATE U
SET 
	dim_b2kTranImportStatus_dateadded = COALESCE(I.dim_b2kTranImportStatus_dateadded, @Now)     -- Set only IF NULL
	, dim_b2kTranImportStatus_lastmodified = I.dim_b2kTranImportStatus_lastmodified
	, dim_b2kTranImportStatus_lastmodifiedby = @User

FROM   inserted AS I

INNER JOIN b2kTranImportStatus AS U
	ON U.sid_b2kTranImportStatus_id = I.sid_b2kTranImportStatus_id

GO

SET IDENTITY_INSERT b2kTranImportStatus ON
	INSERT INTO b2kTranImportStatus (sid_b2ktranimportstatus_id, dim_b2ktranimportstatus_name, dim_b2ktranimportstatus_desc)
	SELECT 0, 'NEW', 'Newly imported records'
	UNION ALL SELECT 1, 'PROCESSED', 'Records that have been processed into user accounts'
	UNION ALL SELECT 2, 'NODEMOG', 'Records that do not have a corresponding record in b2kDemog'
	UNION ALL SELECT 3, 'NOPROGRAM', 'Records that are associated with any users that are not in any program'
	UNION ALL SELECT 4, 'PRIORTRANS', 'Records that originally did not have a b2kDemog association, but now do'
SET IDENTITY_INSERT b2kTranImportStatus OFF
GO

