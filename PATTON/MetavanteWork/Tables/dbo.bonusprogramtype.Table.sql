USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramType_dim_BonusProgramType_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramType_dim_BonusProgramType_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BonusProgramType]    Script Date: 12/02/2010 13:25:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BonusProgramType]') AND type in (N'U'))
DROP TABLE [dbo].[BonusProgramType]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BonusProgramType]    Script Date: 12/02/2010 13:25:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BonusProgramType](
	[sid_BonusProgramType_ID] [varchar](1) NOT NULL,
	[dim_BonusProgramType_Name] [varchar](255) NOT NULL,
	[dim_BonusProgramType_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgramType_LastUpdated] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgramType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgramType_DateAdded]
GO

ALTER TABLE [dbo].[BonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgramType_LastUpdated]
GO


