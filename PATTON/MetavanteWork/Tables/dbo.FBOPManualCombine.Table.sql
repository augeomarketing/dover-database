USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[FBOPManualCombine]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPManualCombine]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPManualCombine]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPManualCombine]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPManualCombine](
	[Bank] [varchar](255) NULL,
	[Account] [varchar](255) NULL,
	[CardNumber] [varchar](255) NULL,
	[Name1] [varchar](255) NULL,
	[Name2] [varchar](255) NULL,
	[CardTaxID] [varchar](255) NULL,
	[AccountTaxID] [varchar](255) NULL,
	[ShortName] [varchar](255) NULL,
	[Status] [varchar](255) NULL,
	[OldTipnumber] [char](15) NULL,
	[NewTipnumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
