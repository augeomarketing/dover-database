USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Extract_file_minus_Type_10]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Extract_file_minus_Type_10]') AND type in (N'U'))
DROP TABLE [dbo].[Extract_file_minus_Type_10]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Extract_file_minus_Type_10]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Extract_file_minus_Type_10](
	[Col001] [char](28) NULL,
	[Col002] [char](24) NULL,
	[Col003] [char](37) NULL,
	[Col004] [char](52) NULL,
	[Col005] [char](21) NULL,
	[Col006] [char](45) NULL,
	[Col007] [char](20) NULL,
	[Col008] [char](274) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
