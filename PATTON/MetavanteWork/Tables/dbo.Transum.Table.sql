USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Transum]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Transum_NUMPURCH]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transum_NUMPURCH]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF_Transum_NUMPURCH]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Transum__AMTPURC__797309D9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Transum__AMTPURC__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__AMTPURC__797309D9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Transum_NUMCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transum_NUMCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF_Transum_NUMCR]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Transum__AMTCR__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Transum__AMTCR__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__AMTCR__7A672E12]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Transum_ratio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transum_ratio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF_Transum_ratio]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Transum__overage__7B5B524B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Transum__overage__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__overage__7B5B524B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transum]') AND type in (N'U'))
DROP TABLE [dbo].[Transum]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transum]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transum](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [nchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[NUMPURCH] [nchar](6) NULL,
	[AMTPURCH] [numeric](9, 0) NULL,
	[NUMCR] [nchar](6) NULL,
	[AMTCR] [numeric](9, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [numeric](5, 0) NULL,
	[cardtype] [char](1) NULL,
	[SigvsPin] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Transum_NUMPURCH]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transum_NUMPURCH]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF_Transum_NUMPURCH]  DEFAULT (0) FOR [NUMPURCH]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Transum__AMTPURC__797309D9]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Transum__AMTPURC__797309D9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF__Transum__AMTPURC__797309D9]  DEFAULT (0) FOR [AMTPURCH]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Transum_NUMCR]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transum_NUMCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF_Transum_NUMCR]  DEFAULT (0) FOR [NUMCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Transum__AMTCR__7A672E12]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Transum__AMTCR__7A672E12]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF__Transum__AMTCR__7A672E12]  DEFAULT (0) FOR [AMTCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Transum_ratio]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Transum_ratio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF_Transum_ratio]  DEFAULT (0) FOR [ratio]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Transum__overage__7B5B524B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Transum]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Transum__overage__7B5B524B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF__Transum__overage__7B5B524B]  DEFAULT (0) FOR [overage]
END


End
GO
