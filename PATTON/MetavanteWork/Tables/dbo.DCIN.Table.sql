USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DCIN]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DCIN]') AND type in (N'U'))
DROP TABLE [dbo].[DCIN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DCIN]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DCIN](
	[CLIENT] [char](11) NULL,
	[ACCTNUM] [char](17) NULL,
	[SSN] [char](9) NULL,
	[NA1] [char](26) NULL,
	[NA2] [char](26) NULL,
	[ADDR1] [char](26) NULL,
	[ADDR2] [char](26) NULL,
	[CITYSTATE] [char](21) NULL,
	[ZIP] [char](5) NULL,
	[ZIGN] [char](1) NULL,
	[POINTS] [char](6) NULL,
	[PHONE1] [char](10) NULL,
	[PHONE2] [char](10) NULL,
	[TRANCODE] [char](3) NULL,
	[STATUS] [char](1) NULL,
	[NEWFLAG] [char](1) NULL,
	[CODENAME] [char](8) NULL,
	[BIRTH1] [char](6) NULL,
	[BIRTH2] [char](6) NULL,
	[MAINTFLAG] [char](1) NULL,
	[ENROLLSWTC] [char](1) NULL,
	[ENROLLPNTS] [char](6) NULL,
	[ACCT2] [char](19) NULL,
	[EXPIRE] [char](8) NULL,
	[ORIGDATE] [char](8) NULL,
	[LSTREISSDT] [char](8) NULL,
	[DATELSTUSE] [char](8) NULL,
	[BANK] [char](5) NULL,
	[CARDSTATUS] [char](1) NULL,
	[DDANUM] [char](11) NULL,
	[SAVNUM] [char](11) NULL,
	[FILL1] [char](194) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
