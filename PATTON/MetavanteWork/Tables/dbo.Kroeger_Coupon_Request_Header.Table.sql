USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Kroeger_Coupon_Request_Header]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Header]') AND type in (N'U'))
DROP TABLE [dbo].[Kroeger_Coupon_Request_Header]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Kroeger_Coupon_Request_Header]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Kroeger_Coupon_Request_Header](
	[RecordType] [char](1) NULL,
	[DateCreated] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
