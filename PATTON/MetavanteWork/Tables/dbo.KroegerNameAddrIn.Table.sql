USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[KroegerNameAddrIn]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerNameAddrIn]') AND type in (N'U'))
DROP TABLE [dbo].[KroegerNameAddrIn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KroegerNameAddrIn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[KroegerNameAddrIn](
	[Col001] [varchar](255) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
