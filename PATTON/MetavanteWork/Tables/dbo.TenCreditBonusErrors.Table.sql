USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TenCreditBonusErrors]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TenCreditBonusErrors]') AND type in (N'U'))
DROP TABLE [dbo].[TenCreditBonusErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TenCreditBonusErrors]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TenCreditBonusErrors](
	[AcctNumber] [nchar](16) NULL,
	[Trans] [nchar](10) NULL,
	[Points] [float] NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
