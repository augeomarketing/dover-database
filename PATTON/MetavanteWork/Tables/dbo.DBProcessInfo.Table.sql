USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[DBProcessInfo]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__Activ__51851410]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__Activ__51851410]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] DROP CONSTRAINT [DF__DBProcess__Activ__51851410]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__First__52793849]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__First__52793849]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] DROP CONSTRAINT [DF__DBProcess__First__52793849]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__Onlin__536D5C82]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__Onlin__536D5C82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] DROP CONSTRAINT [DF__DBProcess__Onlin__536D5C82]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__E-Sta__546180BB]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__E-Sta__546180BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] DROP CONSTRAINT [DF__DBProcess__E-Sta__546180BB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DBProcessInfo_DebitFactor]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DBProcessInfo_DebitFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] DROP CONSTRAINT [DF_DBProcessInfo_DebitFactor]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]') AND type in (N'U'))
DROP TABLE [dbo].[DBProcessInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DBProcessInfo](
	[DBName] [varchar](50) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL,
	[ActivationBonus] [numeric](18, 0) NOT NULL,
	[FirstUseBonus] [numeric](18, 0) NOT NULL,
	[OnlineRegistrationBonus] [numeric](18, 0) NOT NULL,
	[E-StatementBonus] [numeric](18, 0) NOT NULL,
	[DebitFactor] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]') AND name = N'IX_DBProcessinfo-1')
CREATE NONCLUSTERED INDEX [IX_DBProcessinfo-1] ON [dbo].[DBProcessInfo] 
(
	[DBNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__Activ__51851410]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__Activ__51851410]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF__DBProcess__Activ__51851410]  DEFAULT (0) FOR [ActivationBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__First__52793849]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__First__52793849]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF__DBProcess__First__52793849]  DEFAULT (0) FOR [FirstUseBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__Onlin__536D5C82]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__Onlin__536D5C82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF__DBProcess__Onlin__536D5C82]  DEFAULT (0) FOR [OnlineRegistrationBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__DBProcess__E-Sta__546180BB]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__DBProcess__E-Sta__546180BB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF__DBProcess__E-Sta__546180BB]  DEFAULT (0) FOR [E-StatementBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DBProcessInfo_DebitFactor]') AND parent_object_id = OBJECT_ID(N'[dbo].[DBProcessInfo]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DBProcessInfo_DebitFactor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF_DBProcessInfo_DebitFactor]  DEFAULT (2) FOR [DebitFactor]
END


End
GO
