USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[affinityuserlist]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinityuserlist]') AND type in (N'U'))
DROP TABLE [dbo].[affinityuserlist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinityuserlist]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[affinityuserlist](
	[sid_affinityuserlist_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_affinityuserlist_RNNumber] [varchar](16) NOT NULL,
	[dim_affinityuserlist_Runavailable] [int] NOT NULL,
	[dim_affinityuserlist_RunBalance] [int] NOT NULL,
	[dim_affinityuserlist_RunRedeemed] [int] NOT NULL,
	[dim_affinityuserlist_status] [varchar](2) NOT NULL,
	[dim_affinityuserlist_dateadded] [datetime] NOT NULL,
	[dim_affinityuserlist_lastname] [varchar](50) NOT NULL,
	[dim_affinityuserlist_Acctname1] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Acctname2] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Acctname3] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Acctname4] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Address1] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Address2] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Address3] [varchar](250) NOT NULL,
	[dim_affinityuserlist_Address4] [varchar](250) NOT NULL,
	[dim_affinityuserlist_City] [varchar](50) NOT NULL,
	[dim_affinityuserlist_State] [varchar](50) NOT NULL,
	[dim_affinityuserlist_zip] [varchar](10) NOT NULL,
	[dim_affinityuserlist_acctid] [varchar](50) NOT NULL,
 CONSTRAINT [PK_affinityuserlist] PRIMARY KEY CLUSTERED 
(
	[sid_affinityuserlist_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
