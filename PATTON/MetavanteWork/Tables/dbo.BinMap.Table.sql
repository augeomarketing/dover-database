USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BinMap]    Script Date: 01/06/2011 08:49:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BinMap]') AND type in (N'U'))
DROP TABLE [dbo].[BinMap]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BinMap]    Script Date: 01/06/2011 08:49:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BinMap](
	[tipfirst] [varchar](3) NOT NULL,
	[bin] [varchar](9) NOT NULL,
	CONSTRAINT pk_BinMap PRIMARY KEY(tipfirst, bin)
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


