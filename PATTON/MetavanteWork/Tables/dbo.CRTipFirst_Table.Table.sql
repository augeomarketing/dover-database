USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[CRTipFirst_Table]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRTipFirst_Table]') AND type in (N'U'))
DROP TABLE [dbo].[CRTipFirst_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CRTipFirst_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CRTipFirst_Table](
	[Bank] [char](4) NOT NULL,
	[Agent] [char](4) NOT NULL,
	[Tipfirst] [char](3) NOT NULL,
	[BankName] [char](50) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
