USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[Metavante504]    Script Date: 01/11/2010 17:14:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Metavante504]') AND type in (N'U'))
DROP TABLE [dbo].[Metavante504]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Metavante504]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Metavante504](
	[Col001] [char](1) NULL,
	[Col002] [char](15) NULL,
	[Col003] [char](80) NULL,
	[Col004] [char](120) NULL,
	[Col005] [char](80) NULL,
	[Col006] [char](252) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
