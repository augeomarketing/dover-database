USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[wrktab1n]    Script Date: 02/18/2010 09:13:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab1n]') AND type in (N'U'))
DROP TABLE [dbo].[wrktab1n]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrktab1n]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrktab1n](
	[ssn] [nvarchar](9) NULL,
	[na1] [nvarchar](40) NULL,
	[na2] [nvarchar](40) NULL,
	[joint] [nvarchar](1) NULL,
	[tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
