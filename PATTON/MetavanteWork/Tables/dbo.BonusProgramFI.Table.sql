USE [MetavanteWork]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BonusProgramFI_BonusProgram]') AND parent_object_id = OBJECT_ID(N'[dbo].[BonusProgramFI]'))
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [FK_BonusProgramFI_BonusProgram]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgramFI] DROP CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]
END

GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BonusProgramFI]    Script Date: 12/02/2010 13:24:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BonusProgramFI]') AND type in (N'U'))
DROP TABLE [dbo].[BonusProgramFI]
GO

USE [MetavanteWork]
GO

/****** Object:  Table [dbo].[BonusProgramFI]    Script Date: 12/02/2010 13:24:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BonusProgramFI](
	[sid_BonusProgramFI_Bin] [varchar](9) NOT NULL,
	[sid_BonusProgram_ID] [int] NOT NULL,
	[dim_BonusProgramFI_Effectivedate] [datetime] NOT NULL,
	[dim_BonusProgramFI_ExpirationDate] [datetime] NOT NULL,
	[dim_BonusProgramFI_BonusPoints] [int] NOT NULL,
	[dim_BonusProgramFI_PointMultiplier] [int] NOT NULL,
	[dim_BonusProgramFI_DateAdded] [datetime] NOT NULL,
	[dim_BonusProgramFI_LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BonusProgramFI] PRIMARY KEY CLUSTERED 
(
	[sid_BonusProgramFI_Bin] ASC,
	[sid_BonusProgram_ID] ASC,
	[dim_BonusProgramFI_Effectivedate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BonusProgramFI]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgramFI_BonusProgram] FOREIGN KEY([sid_BonusProgram_ID])
REFERENCES [dbo].[BonusProgram] ([sid_BonusProgram_ID])
GO

ALTER TABLE [dbo].[BonusProgramFI] CHECK CONSTRAINT [FK_BonusProgramFI_BonusProgram]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_BonusPoints]  DEFAULT ((0)) FOR [dim_BonusProgramFI_BonusPoints]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_PointMultiplier]  DEFAULT ((1)) FOR [dim_BonusProgramFI_PointMultiplier]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgramFI_DateAdded]
GO

ALTER TABLE [dbo].[BonusProgramFI] ADD  CONSTRAINT [DF_BonusProgramFI_dim_BonusProgramFI_LastUpdated]  DEFAULT (getdate()) FOR [dim_BonusProgramFI_LastUpdated]
GO


