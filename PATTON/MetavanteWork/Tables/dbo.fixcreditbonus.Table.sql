USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[fixcreditbonus]    Script Date: 01/11/2010 17:14:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixcreditbonus]') AND type in (N'U'))
DROP TABLE [dbo].[fixcreditbonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixcreditbonus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixcreditbonus](
	[tipnumber] [varchar](15) NOT NULL,
	[pointsshouldhavebeenposted] [numeric](18, 0) NULL,
	[pointswereposted] [numeric](18, 0) NULL,
	[pointstoadjust] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
