USE [MetavanteWork]
GO
/****** Object:  Table [dbo].[TenCreditBonuspostedfile]    Script Date: 01/11/2010 17:14:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TenCreditBonuspostedfile]') AND type in (N'U'))
DROP TABLE [dbo].[TenCreditBonuspostedfile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TenCreditBonuspostedfile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TenCreditBonuspostedfile](
	[AcctNumber] [nchar](16) NULL,
	[Trans] [nchar](10) NULL,
	[Points] [nchar](15) NULL,
	[tipnumber] [nchar](15) NULL,
	[PointstoPost] [float] NULL
) ON [PRIMARY]
END
GO
