USE [591UBMichigan]
GO
/****** Object:  Table [dbo].[wrkdda1]    Script Date: 09/25/2009 14:36:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkdda1](
	[custid] [char](13) NULL,
	[tippri] [char](15) NULL,
	[tipsec] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
