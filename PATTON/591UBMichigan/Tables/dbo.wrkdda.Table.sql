USE [591UBMichigan]
GO
/****** Object:  Table [dbo].[wrkdda]    Script Date: 09/25/2009 14:36:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkdda](
	[custid] [char](13) NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
