USE	[591UBMichigan]

GO

CREATE PROCEDURE usp_591_DemographicLoad

AS

DECLARE 
		@date date, 
		@FileDate varchar(5),
		@FileNameBusiness varchar(max),
		@FileNameConsumer varchar(max) 

SELECT	@date = Convert(date, MonthEndingdate) FROM MetavanteWork.dbo.autoprocessdetail WHERE TipFirst = '591'
SET		@FileDate = LEFT(DATENAME(m,@date),3) + RIGHT(DATENAME(yy,@date),2)

SET		@FileNameBusiness = '\\patton\ops\591\input\Business591_' + @FileDate + '.csv'
SET		@FileNameConsumer = '\\patton\ops\591\input\Consumer50e_' + @FileDate  + '.csv'


EXECUTE	Rewardsnow.dbo.usp_fileloadScheduleFile '591', 1, @date, 'Delimited', ',', @FileNameBusiness 
EXECUTE	Rewardsnow.dbo.usp_fileloadScheduleFile '591', 1, @date, 'Delimited', ',', @FileNameConsumer 
