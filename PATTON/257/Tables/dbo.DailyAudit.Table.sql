USE [257]
GO
/****** Object:  Table [dbo].[DailyAudit]    Script Date: 05/01/2013 09:36:07 ******/
ALTER TABLE [dbo].[DailyAudit] DROP CONSTRAINT [DF__DailyAudi__Point__0B91BA14]
GO
ALTER TABLE [dbo].[DailyAudit] DROP CONSTRAINT [DF__DailyAudi__Bonus__0C85DE4D]
GO
DROP TABLE [dbo].[DailyAudit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyAudit](
	[AccountNumber] [varchar](10) NOT NULL,
	[LoanNumber] [varchar](2) NOT NULL,
	[TransactionDate] [date] NOT NULL,
	[PointsEarned] [int] NOT NULL,
	[BonusPoints] [int] NOT NULL,
 CONSTRAINT [PK_DailyAudit] PRIMARY KEY CLUSTERED 
(
	[AccountNumber] ASC,
	[LoanNumber] ASC,
	[TransactionDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DailyAudit] ADD  CONSTRAINT [DF__DailyAudi__Point__0B91BA14]  DEFAULT ((0)) FOR [PointsEarned]
GO
ALTER TABLE [dbo].[DailyAudit] ADD  CONSTRAINT [DF__DailyAudi__Bonus__0C85DE4D]  DEFAULT ((0)) FOR [BonusPoints]
GO
