USE [257]
GO
/****** Object:  Table [dbo].[TfrPoints]    Script Date: 05/01/2013 09:36:07 ******/
ALTER TABLE [dbo].[TfrPoints] DROP CONSTRAINT [DF_TfrPoints_source]
GO
ALTER TABLE [dbo].[TfrPoints] DROP CONSTRAINT [DF_TfrPoints_dbnumber]
GO
ALTER TABLE [dbo].[TfrPoints] DROP CONSTRAINT [DF_TfrPoints_filetype_id]
GO
DROP TABLE [dbo].[TfrPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TfrPoints](
	[source] [varchar](255) NOT NULL,
	[sourcerow] [int] IDENTITY(1,1) NOT NULL,
	[dbnumber] [varchar](50) NOT NULL,
	[filetype_id] [int] NOT NULL,
	[Field01] [varchar](max) NULL,
	[Field02] [varchar](max) NULL,
	[Field03] [varchar](max) NULL,
	[Field04] [varchar](max) NULL,
	[Field05] [varchar](max) NULL,
	[Field06] [varchar](max) NULL,
	[Field07] [varchar](max) NULL,
	[Field08] [varchar](max) NULL,
	[Field09] [varchar](max) NULL,
	[Field10] [varchar](max) NULL,
	[Field11] [varchar](max) NULL,
	[Field12] [varchar](max) NULL,
	[Field13] [varchar](max) NULL,
	[Field14] [varchar](max) NULL,
	[Field15] [varchar](max) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TfrPoints] ADD  CONSTRAINT [DF_TfrPoints_source]  DEFAULT ('257_BONUS_20120927_01a.txt') FOR [source]
GO
ALTER TABLE [dbo].[TfrPoints] ADD  CONSTRAINT [DF_TfrPoints_dbnumber]  DEFAULT ((257)) FOR [dbnumber]
GO
ALTER TABLE [dbo].[TfrPoints] ADD  CONSTRAINT [DF_TfrPoints_filetype_id]  DEFAULT ((8)) FOR [filetype_id]
GO
