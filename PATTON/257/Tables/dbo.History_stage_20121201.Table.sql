USE [257]
GO
/****** Object:  Table [dbo].[History_stage_20121201]    Script Date: 05/01/2013 09:36:07 ******/
DROP TABLE [dbo].[History_stage_20121201]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History_stage_20121201](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NOT NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NOT NULL,
	[Overage] [bigint] NOT NULL
) ON [PRIMARY]
GO
