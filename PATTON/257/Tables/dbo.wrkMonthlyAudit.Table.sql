USE [257]
GO
/****** Object:  Table [dbo].[wrkMonthlyAudit]    Script Date: 05/01/2013 09:36:07 ******/
ALTER TABLE [dbo].[wrkMonthlyAudit] DROP CONSTRAINT [DF__wrkMonthl__Point__2645B050]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] DROP CONSTRAINT [DF__wrkMonthl__Point__2739D489]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] DROP CONSTRAINT [DF__wrkMonthl__Bonus__282DF8C2]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] DROP CONSTRAINT [DF__wrkMonthl__Point__29221CFB]
GO
DROP TABLE [dbo].[wrkMonthlyAudit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkMonthlyAudit](
	[tipnumber] [varchar](15) NULL,
	[AccountNumber] [varchar](10) NULL,
	[LoanID] [varchar](2) NULL,
	[PointsDate] [date] NULL,
	[PointsRedeemed] [int] NULL,
	[PointsEarned] [int] NULL,
	[BonusPoints] [int] NULL,
	[PointsAvailable] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] ADD  DEFAULT ((0)) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] ADD  DEFAULT ((0)) FOR [PointsEarned]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] ADD  DEFAULT ((0)) FOR [BonusPoints]
GO
ALTER TABLE [dbo].[wrkMonthlyAudit] ADD  DEFAULT ((0)) FOR [PointsAvailable]
GO
