USE [257]
GO

/****** Object:  StoredProcedure [dbo].[usp_GenerateMonthlyAuditExtract]    Script Date: 08/01/2014 08:24:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GenerateMonthlyAuditExtract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GenerateMonthlyAuditExtract]
GO

USE [257]
GO

/****** Object:  StoredProcedure [dbo].[usp_GenerateMonthlyAuditExtract]    Script Date: 08/01/2014 08:24:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_GenerateMonthlyAuditExtract]
	@monthstartdate				date,
	@monthenddate				date
	
AS

set nocount ON;


--  select @monthstartdate, @monthenddatetime

truncate table dbo.wrkMonthlyAudit

insert into wrkMonthlyAudit
(tipnumber, accountnumber, loanid, pointsdate, PointsAvailable, PointsEarned, PointsRedeemed, BonusPoints)
select tipnumber, left(acctid, len(acctid)-2), right(acctid,2) , @monthenddate, 0, 0, 0, 0
from dbo.affiliat
where accttype = 'ACCOUNTNUMBER' and acctstatus = 'A'


--POINTS EARNED BY ACCOUNT NUMBER
update tmp
	set pointsearned = pts
--		pointsavailable = pts
from wrkMonthlyAudit tmp join 

(
	select left(acctid, len(acctid)-2) accountnumber, right(acctid,2) loanid, sum(points * ratio) pts
    from dbo.history hst
    inner join 
    (
		select distinct sid_trantype_trancode from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('INCREASE_PURCHASE') 
		union 
		select distinct sid_trantype_trancode from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('DECREASE_RETURN')
	) tc
    on hst.TRANCODE = tc.sid_trantype_trancode
    
    where 
		convert(date, histdate) between @monthstartdate and @monthenddate
    group by left(acctid, len(acctid)-2), right(acctid,2)
) h
on tmp.accountnumber = h.accountnumber
and tmp.loanid = h.loanid

--POINTS AVAILABLE BY TIPNUMBER

UPDATE tmp
SET PointsAvailable = ISNULL(hst.PTS, 0)
FROM wrkMonthlyAudit tmp
LEFT OUTER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS * RATIO) PTS
	FROM HISTORY hst
	WHERE CONVERT(date, HISTDATE) <= @monthenddate
	GROUP BY TIPNUMBER
) hst
ON tmp.tipnumber = hst.TIPNUMBER

--BONUS POINTS BY TIPNUMBER	

update tmp
	set bonuspoints = h.pts
from wrkMonthlyAudit tmp 
join 
(
	select tipnumber, sum(points * ratio) pts
    from dbo.history hst
    inner join
    (
		select DISTINCT sid_trantype_trancode from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('INCREASE_BONUS')
		union
		select DISTINCT sid_trantype_trancode from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('DECREASE_REVERSAL_BONUS')
		union
		select DISTINCT sid_trantype_trancode  from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('INCREASE_TRANSFER')
		union
		select DISTINCT sid_trantype_trancode  from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('DECREASE_TRANSFER')
		union
		select DISTINCT sid_trantype_trancode  from RewardsNow.dbo.ufn_GetTrancodesForGroupByName('INCREASE_ADJUSTMENT_IE')
    ) tc
    on hst.TRANCODE = tc.sid_trantype_trancode
    where convert(date, histdate) between @monthstartdate and @monthenddate
    group by tipnumber
) h
on tmp.tipnumber = h.tipnumber

--REDEMPTIONS BY TIPNUMBER


update tmp
	set pointsredeemed = h.pts
from wrkMonthlyAudit tmp 
join 
(
	select 
		tipnumber
		, SUM(POINTS * RATIO) * -1 as pts
	from dbo.history hst
	inner join
	(
		select distinct sid_trantype_trancode FROM RewardsNow.dbo.ufn_GetTrancodesForGroupByName('DECREASE_REDEMPTION')
		union
		select distinct sid_trantype_trancode FROM RewardsNow.dbo.ufn_GetTrancodesForGroupByName('REDEMPTION_ADJUSTMENT')
		
	
	) tc
	on hst.TRANCODE = tc.sid_trantype_trancode
	where convert(date, histdate) between @monthstartdate and @monthenddate
	group by tipnumber
) h
on tmp.tipnumber = h.tipnumber


--update wrkMonthlyAudit
--	set pointsavailable = pointsavailable - pointsredeemed


--update tmp
--	set pointsavailable = h.pts
--from wrkMonthlyAudit tmp join (select tipnumber, sum(points * ratio) pts
--						from dbo.history
--						where histdate <= @monthenddatetime
--						group by tipnumber
--						) h
--	on tmp.tipnumber = h.tipnumber



--select *
--from wrkMonthlyAudit


/*  TEST HARNESS  */

--  exec [dbo].[usp_GenerateMonthlyAuditExtract] '02/01/2013', '02/28/2013'


GO


