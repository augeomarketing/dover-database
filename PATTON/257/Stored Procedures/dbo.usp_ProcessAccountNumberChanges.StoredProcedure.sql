USE [257]
GO
/****** Object:  StoredProcedure [dbo].[usp_ProcessAccountNumberChanges]    Script Date: 05/01/2013 09:34:47 ******/
DROP PROCEDURE [dbo].[usp_ProcessAccountNumberChanges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_ProcessAccountNumberChanges]
	@tipfirst			varchar(50),
	@debug				int = 0

AS


if object_id('tempdb..#newaccts') is not null
	drop table #newaccts

create table #newaccts
(sid_rnicustomer_id			bigint,
 newaccountnumber			varchar(20),
 oldaccountnumber			varchar(20),
 tipnumber					varchar(15) )

-- pull out any rows where a new account number is being generated for a customer
insert into #newaccts
(newaccountnumber, oldaccountnumber)
select distinct MooUAccountNumber, oldaccountnumber + portfolionumber
from rewardsnow.dbo.vw_257_ACCT_SOURCE_226
where len(oldaccountnumber) > 3

-- update affiliat setting OLDACCOUNTNUMBER to status of "C"
update aff
	set acctstatus = 'C'
from dbo.affiliat aff join #newaccts tmp
	on aff.acctid = tmp.oldaccountnumber

-- Update history changing the accountid to the new accountid.  The FI does NOT keep a record of the old accountid.  it is
-- actually purged from their system for ever and ever amen.  Doing this update allows the monthly audit file to tie out to the FI's
-- banking system.
update his
	set acctid = moouaccountnumber
from dbo.history his join (select moouaccountnumber, oldaccountnumber + portfolionumber as oldaccountnumber from  rewardsnow.dbo.vw_257_ACCT_SOURCE_226_archive vw where len(oldaccountnumber) > 3) vw
	on his.acctid = vw.oldaccountnumber
	
	
	
-- Retrieve the rnicustomerid for the row that has the NEW account number
update tmp
	set sid_rnicustomer_id = rnic.sid_rnicustomer_id
from #newaccts tmp join rewardsnow.dbo.rnicustomer rnic
	on tmp.newaccountnumber = rnic.dim_rnicustomer_member

-- Get tip# from the affiliat of the OLD account#
update tmp
	set tipnumber = aff.tipnumber
from #newaccts tmp join [257].dbo.affiliat aff
	on tmp.oldaccountnumber = aff.acctid
where aff.accttype = 'ACCOUNTNUMBER'


-- For rows in the temp table that DO NOT HAVE TIP#s - delete them.  The old account# does not exist in our system
delete from #newaccts where tipnumber is null


-- For rows that have the SAME tip# for the old and new account numbers - delete them.  they are already combined
delete tmp
from #newaccts tmp join [257].dbo.affiliat af1
	on tmp.oldaccountnumber = af1.acctid
join [257].dbo.affiliat af2
	on tmp.newaccountnumber = af2.acctid
where af1.accttype = 'ACCOUNTNUMBER' and af2.accttype = 'ACCOUNTNUMBER'
and af1.tipnumber = af2.tipnumber

--  select * from #newaccts



-- add the RNICUSTOMERID row into the affiliat table
insert into [257].dbo.affiliat
(acctid, tipnumber, accttype, dateadded, acctstatus, accttypedesc, custid)
select sid_rnicustomer_id, tipnumber, 'RNICUSTOMERID', getdate(), 'A', 'RNI Customer ID', newaccountnumber
from #newaccts tmp left outer join (select acctid from [257].dbo.affiliat aff where accttype = 'RNICUSTOMERID') aff
	on tmp.sid_rnicustomer_id = aff.acctid
where aff.acctid is null
GO
