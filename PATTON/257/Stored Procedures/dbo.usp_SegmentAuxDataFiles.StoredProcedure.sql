USE [257]
GO

/****** Object:  StoredProcedure [dbo].[usp_SegmentAuxDataFiles]    Script Date: 02/12/2014 10:40:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SegmentAuxDataFiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SegmentAuxDataFiles]
GO

USE [257]
GO

/****** Object:  StoredProcedure [dbo].[usp_SegmentAuxDataFiles]    Script Date: 02/12/2014 10:40:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_SegmentAuxDataFiles]
AS

--CUSTOM SEGMENTATION FOR BONUS FILE
UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_BNS_SOURCE_1 bs
ON ri.sid_rnirawimport_id = bs.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member LIKE bs.MemberNumber + '[0-9][0-9]'
	AND bs.PrimaryIndicatorID = rnic.dim_RNICustomer_PrimaryId
	AND rnic.sid_dbprocessinfo_dbnumber = '257'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1

UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_BNS_SOURCE_1 bs
ON ri.sid_rnirawimport_id = bs.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member LIKE bs.MemberNumber + '[0-9][0-9]'
	AND bs.PrimaryIndicatorID = rnic.dim_RNICustomer_PrimaryId
	AND rnic.sid_dbprocessinfo_dbnumber = '266'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1


UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_BNS_SOURCE_1 bs
ON ri.sid_rnirawimport_id = bs.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member LIKE bs.MemberNumber + '[0-9][0-9]'
	AND rnic.sid_dbprocessinfo_dbnumber = '257'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1

UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_BNS_SOURCE_1 bs
ON ri.sid_rnirawimport_id = bs.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member LIKE bs.MemberNumber + '[0-9][0-9]'
	AND rnic.sid_dbprocessinfo_dbnumber = '266'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1


--CUSTOM SEGMENTATION FOR CARD FILE
UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_CARD_SOURCE_1 crd
ON ri.sid_rnirawimport_id = crd.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member like crd.MemberNumber + '[0-9][0-9]'
	AND crd.PrimaryIndicatorID = rnic.dim_RNICustomer_PrimaryId
	AND rnic.sid_dbprocessinfo_dbnumber = '257'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1

UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_CARD_SOURCE_1 crd
ON ri.sid_rnirawimport_id = crd.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member like crd.MemberNumber + '[0-9][0-9]'
	AND crd.PrimaryIndicatorID = rnic.dim_RNICustomer_PrimaryId
	AND rnic.sid_dbprocessinfo_dbnumber = '266'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1

UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_CARD_SOURCE_1 crd
ON ri.sid_rnirawimport_id = crd.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member like crd.MemberNumber + '[0-9][0-9]'
	AND rnic.sid_dbprocessinfo_dbnumber = '257'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1

UPDATE ri
SET sid_dbprocessinfo_dbnumber = rnic.dim_RNICustomer_TipPrefix
FROM RewardsNow.dbo.RNIRawImport ri
INNER JOIN RewardsNow.dbo.vw_$MU_CARD_SOURCE_1 crd
ON ri.sid_rnirawimport_id = crd.sid_rnirawimport_id
INNER JOIN RewardsNow.dbo.RNICustomer rnic
ON rnic.dim_RNICustomer_Member like crd.MemberNumber + '[0-9][0-9]'
	AND rnic.sid_dbprocessinfo_dbnumber = '266'
	AND CONVERT(INT, rnic.dim_RNICustomer_CustomerCode) = 1



GO


