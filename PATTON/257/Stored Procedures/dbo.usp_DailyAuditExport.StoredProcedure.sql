USE [257]
GO
/****** Object:  StoredProcedure [dbo].[usp_DailyAuditExport]    Script Date: 05/01/2013 09:34:47 ******/
DROP PROCEDURE [dbo].[usp_DailyAuditExport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[usp_DailyAuditExport]
		@transactiondate			date = null
AS

-- SEB 11/2013 add code H 

if @transactiondate is null 
	set @transactiondate = getdate()


-- If data exists for the date, delete it and start anew
delete from dbo.dailyaudit where TransactionDate = @transactiondate

insert into dbo.dailyaudit
(AccountNumber, LoanNumber, TransactionDate, PointsEarned, BonusPoints)
select left(stg.acctid,10) AccountNumber, right(stg.acctid,2) LoanNumber, @transactiondate, 
	isnull(pe.points, 0) PointsEarned, isnull(bns.points,0) BonusPoints
from [257].dbo.affiliat_stage stg

left outer join (select acctid, sum(points * ratio) points from dbo.history_stage where histdate = @transactiondate and isnumeric(trancode) = 1 group by acctid) pe
	on stg.acctid = pe.acctid

left outer join (select acctid, sum(points * ratio) points from dbo.history_stage where histdate = @transactiondate and (trancode like 'B%' or trancode like 'F%' or trancode like 'G%' or trancode like 'H%') group by acctid) bns
	on stg.acctid = bns.acctid
where stg.accttype = 'ACCOUNTNUMBER'

order by stg.tipnumber


/* Test harness

exec dbo.usp_dailyauditexport '10/01/2012'


*/
GO
