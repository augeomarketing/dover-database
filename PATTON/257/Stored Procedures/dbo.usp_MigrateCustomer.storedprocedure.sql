USE [257]
GO

CREATE PROCEDURE usp_MigrateCustomer
	@sid_customer_tipnumber_old VARCHAR(15) 
AS
BEGIN
	DECLARE
		@sid_customer_tipnumber_new VARCHAR(15)
		, @lasttipused VARCHAR(15)
	
	
	-- GET MEMBER NUMBER
	declare @member VARCHAR(255)

	select @member = dim_rnicustomer_member 
	FROM RewardsNow.dbo.RNICustomer 
	where sid_dbprocessinfo_dbnumber = '257' 
		and dim_RNICustomer_RNIId = @sid_customer_tipnumber_old

	-- GET NEW TIPNUMBER

	select @sid_customer_tipnumber_new = dim_rnicustomer_rniid 
	from RewardsNow.dbo.RNICustomer 
	where sid_dbprocessinfo_dbnumber = '266' 
		and dim_RNICustomer_Member = @member

	-- CLOSE RNICUSTOMER DATA FOR OLD NUMBER FROM [257]
	
	update Rewardsnow.dbo.RNICustomer
	set dim_RNICustomer_CustomerCode = '99'
	where sid_dbprocessinfo_dbnumber = '257'
		and dim_RNICustomer_RNIId = @sid_customer_tipnumber_old

	-- SUM/INSERT HISTORY 
	DECLARE @points INT
	
	select @points = SUM(POINTS * RATIO) 
	from [257].dbo.HISTORY
	where TIPNUMBER = @sid_customer_tipnumber_old

	insert into [266].dbo.HISTORY (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	values (@sid_customer_tipnumber_new, NULL, CONVERT(DATE, GETDATE()), 'TP', 1, @points, 'Transfer From ' + @sid_customer_tipnumber_old, 'NEW', 1, 0)
	
	-- Purge Old Account
	UPDATE [257].dbo.CUSTOMER
	SET STATUS = '0'
		, StatusDescription = '[0] - Purge (xfer)'
	WHERE TIPNUMBER = @sid_customer_tipnumber_old
	
	EXEC Rewardsnow.dbo.usp_RNIPurgeCustomers_ByCountDownStatus '257'
	
	-- RECALC BALANCE
	
	/*
	
	Update [266].dbo.CUSTOMER
	set runbalance = isnull((Select sum(points*ratio) from [266].dbo.HISTORY where (left(trancode, 1) <> 'R' and trancode not in ('DR','IR')) and history.tipnumber = customer.tipnumber),0),
		runredeemed = isnull((Select sum(points*ratio)*-1 from [266].dbo.HISTORY where (left(trancode, 1) = 'R' or trancode in ('DR','IR')) and history.tipnumber = customer.tipnumber),0),
		runavailable = isnull((Select sum(points*ratio) from [266].dbo.HISTORY where history.tipnumber = customer.tipnumber),0)
	where TIPNUMBER = @sid_customer_tipnumber_new
	
	*/


END
	
	
	