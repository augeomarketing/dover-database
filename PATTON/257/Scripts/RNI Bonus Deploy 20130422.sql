USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusLoadSource] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusLoadSource]([sid_rnibonusloadsource_id], [sid_dbprocessinfo_dbnumber], [dim_rnibonusloadsource_sourcename], [dim_rnibonusloadsource_fileload], [sid_rnibonusprocesstype_id], [sid_rnibonustransactionamounttype_code])
SELECT 1, N'257', N'vw_257_BNS_SOURCE_226', 1, 1, N'P'
COMMIT;
RAISERROR (N'[dbo].[RNIBonusLoadSource]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusLoadSource] OFF;

USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusLoadColumn] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusLoadColumn]([sid_rnibonusloadcolumn_id], [sid_rnibonusloadsource_id], [dim_rnibonusloadcolumn_sourcecolumn], [dim_rnibonusloadcolumn_targetcolumn], [dim_rnibonusloadcolumn_required], [sid_rnicustomerloadsource_id], [dim_rnicustomerloadcolumn_targetcolumn], [dim_rnibonusloadcolumn_xrefcolumn])
SELECT 1, 1, N'PortfolioNumber', N'dim_rnibonus_portfolio', 1, NULL, NULL, NULL UNION ALL
SELECT 2, 1, N'MooUAccountNumber', N'dim_rnibonus_member', 1, 17, N'MooUAccountNumber', NULL UNION ALL
SELECT 3, 1, N'PrimaryIndicatorID', N'dim_rnibonus_primaryid', 1, NULL, NULL, NULL UNION ALL
SELECT 4, 1, N'ProcessingCode', N'dim_rnibonus_processingcode', 1, NULL, NULL, N'dim_rnitrantypexref_code01' UNION ALL
SELECT 5, 1, N'CardNumber', N'dim_rnibonus_cardnumber', 0, NULL, NULL, NULL UNION ALL
SELECT 6, 1, N'TransactionAmount', N'dim_rnibonus_transactionamount', 1, NULL, NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[RNIBonusLoadColumn]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusLoadColumn] OFF;

USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIRawImportDataDefinition] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIRawImportDataDefinition]([sid_rnirawimportdatadefinition_id], [sid_rnirawimportdatadefinitiontype_id], [sid_rniimportfiletype_id], [sid_dbprocessinfo_dbnumber], [dim_rnirawimportdatadefinition_outputfield], [dim_rnirawimportdatadefinition_outputdatatype], [dim_rnirawimportdatadefinition_sourcefield], [dim_rnirawimportdatadefinition_startindex], [dim_rnirawimportdatadefinition_length], [dim_rnirawimportdatadefinition_multipart], [dim_rnirawimportdatadefinition_multipartpart], [dim_rnirawimportdatadefinition_multipartlength], [dim_rnirawimportdatadefinition_dateadded], [dim_rnirawimportdatadefinition_lastmodified], [dim_rnirawimportdatadefinition_lastmodifiedby], [dim_rnirawimportdatadefinition_version], [dim_rnirawimportdatadefinition_applyfunction])
SELECT 2702, 1, 5, N'257', N'PortfolioNumber', N'VARCHAR', N'FIELD01', 1, 20, 0, 1, 1, '20130419 15:11:21.690', NULL, NULL, 226, NULL UNION ALL
SELECT 2703, 1, 5, N'257', N'MemberNumber', N'VARCHAR', N'FIELD02', 1, 20, 0, 1, 1, '20130419 15:11:42.087', NULL, NULL, 226, NULL UNION ALL
SELECT 2704, 1, 5, N'257', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD03', 1, 20, 0, 1, 1, '20130419 15:12:04.657', NULL, NULL, 226, NULL UNION ALL
SELECT 2705, 1, 5, N'257', N'MooUAccountNumber', N'VARCHAR', N'Field02', 1, 10, 1, 1, 2, '20130419 15:12:59.317', NULL, NULL, 226, NULL UNION ALL
SELECT 2706, 1, 5, N'257', N'MooUAccountNumber', N'VARCHAR', N'Field01', 1, 2, 1, 2, 2, '20130419 15:13:35.600', NULL, NULL, 226, NULL UNION ALL
SELECT 2707, 1, 5, N'257', N'ProcessingCode', N'VARCHAR', N'FIELD05', 1, 3, 0, 1, 1, '20130419 15:15:06.097', NULL, NULL, 226, NULL UNION ALL
SELECT 2708, 1, 5, N'257', N'CardNumber', N'VARCHAR', N'FIELD06', 1, 16, 0, 1, 1, '20130419 15:15:30.333', NULL, NULL, 226, NULL UNION ALL
SELECT 2710, 1, 5, N'257', N'TransactionAmount', N'VARCHAR', N'FIELD07', 1, 9, 0, 1, 1, '20130419 15:37:38.007', NULL, NULL, 226, NULL
COMMIT;
RAISERROR (N'[dbo].[RNIRawImportDataDefinition]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIRawImportDataDefinition] OFF;

USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProcessType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusProcessType]([sid_rnibonusprocesstype_id], [dim_rnibonusprocesstype_name], [dim_rnibonusprocesstype_active], [dim_rnibonusprocesstype_dateadded], [dim_rnibonusprocesstype_lastmodified])
SELECT 1, N'Load bonuses to history_stage', 1, '20130418 12:18:26.517', NULL
COMMIT;
RAISERROR (N'[dbo].[RNIBonusProcessType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProcessType] OFF;

USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusTransactionAmountType]([sid_rnibonustransactionamounttype_code], [dim_rnibonustransactionamounttype_name], [dim_rnibonustransactionamounttype_dateadded], [dim_rnibonustransactionamounttype_lastmodified])
SELECT N'D', N'Transaction Amount is Dollars', '20130419 17:59:39.720', NULL UNION ALL
SELECT N'P', N'Transaction Amount is Points', '20130419 17:59:30.900', NULL
COMMIT;
RAISERROR (N'[dbo].[RNIBonusTransactionAmountType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

