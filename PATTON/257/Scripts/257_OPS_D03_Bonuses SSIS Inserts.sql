USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'257_OPS_D03_ProcessBonuses', N'\\WEB3\257MSUFCU\TO_RN\', N'\Package.Variables[User::FTPFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'257_OPS_D03_ProcessBonuses', N'5', N'\Package.Variables[User::FileImportType].Properties[Value]', N'Int32' UNION ALL
SELECT N'257_OPS_D03_ProcessBonuses', N'009', N'\Package.Variables[User::BonusProcessingCode].Properties[Value]', N'String' UNION ALL
SELECT N'257_OPS_D03_ProcessBonuses', N'257_BONUS*.txt', N'\Package.Variables[User::BonusFileSpec].Properties[Value]', N'String' UNION ALL
SELECT N'257_OPS_D03_ProcessBonuses', N'\\236722-SQLCLUS2\OPS\257\Input\', N'\Package.Variables[User::BonusFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'257_OPS_D03_ProcessBonuses', N'"', N'\Package.Connections[BonusFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'257_OPS_D03_ProcessBonuses', N'P', N'\Package.Variables[User::TransactionAmountType].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

