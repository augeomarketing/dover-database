USE [257]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 05/01/2013 09:38:09 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from dbo.customer
GO
