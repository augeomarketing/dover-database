USE [257]
GO
/****** Object:  View [dbo].[vwAffiliatdeleted]    Script Date: 05/01/2013 09:38:09 ******/
DROP VIEW [dbo].[vwAffiliatdeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliatdeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID, datedeleted
			  from dbo.affiliatdeleted
GO
