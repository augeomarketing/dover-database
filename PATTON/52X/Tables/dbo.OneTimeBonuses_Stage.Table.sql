USE [52X]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
