USE [52X]
GO
/****** Object:  Table [dbo].[fixbonus]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fixbonus](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
