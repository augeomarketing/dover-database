USE [52X]
GO
/****** Object:  Table [dbo].[wrkbonus]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkbonus](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[trancode] [varchar](2) NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
