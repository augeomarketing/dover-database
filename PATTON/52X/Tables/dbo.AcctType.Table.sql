USE [52X]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AcctType] ADD  CONSTRAINT [DF_AcctType_Acctmultiplier]  DEFAULT ((1)) FOR [Acctmultiplier]
GO
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Car', N'Car Loan', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'CD', N'CD', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Check', N'Checking', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Credit', N'CREDIT CARD', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Credit/Debit', N'Credit/Debit Card', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Debit', N'Debit Card', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Loan', N'Loan Card', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Other', N'Other', CAST(1.00 AS Numeric(18, 2)))
INSERT [dbo].[AcctType] ([AcctType], [AcctTypeDesc], [Acctmultiplier]) VALUES (N'Savings', N'Savings Card', CAST(1.00 AS Numeric(18, 2)))
