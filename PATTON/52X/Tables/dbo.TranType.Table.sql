USE [52X]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [varchar](2) NOT NULL,
	[Description] [varchar](40) NULL,
	[IncDec] [varchar](1) NOT NULL,
	[CntAmtFxd] [varchar](1) NOT NULL,
	[Points] [int] NOT NULL,
	[Ratio] [int] NOT NULL,
	[TypeCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Points]  DEFAULT ((1)) FOR [Points]
GO
ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Ratio]  DEFAULT ((1)) FOR [Ratio]
GO
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'33', N'Credit Card Return', N'D', N'A', 1, -1, N'7')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'37', N'Debit Card Return', N'D', N'A', 1, -1, N'7')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'63', N'Credit Card Purchase', N'I', N'A', 1, 1, N'7')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'67', N'Debit Card Purchase', N'I', N'A', 1, 1, N'7')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'BA', N'Activation Bonus', N'I', N'A', 500, 1, N'5')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'BE', N'Bonus for Employee', N'I', N'A', 1, 1, N'7')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'BS', N'Bonus Online Signup ', N'I', N'A', 500, 1, N'5')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'DE', N'Adjustment (Decrease Earned)', N'D', N'F', 1, -1, N'1')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'DR', N'Adjustment (Decrease Redeemed)', N'I', N'F', 1, 1, N'3')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'IE', N'Adjustment (Increase Earned)', N'I', N'F', 1, 1, N'1')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'IR', N'Adjustment (Increase Redeemed)', N'D', N'F', 1, -1, N'3')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'NW', N'Bonus for First Trans Activity', N'I', N'A', 500, 1, N'5')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RB', N'Redeem CASH BACK', N'D', N'F', 1, 1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RC', N'Redeem for Gift Certificate', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RG', N'Redeem for Charity (Giving)', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RM', N'Redeem for Merchendise', N'D', N'F', 1, 1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RP', N'Redeem for Points', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RQ', N'Redeem for Brochure', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RS', N'Redeem for Down loadable Tunes', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RT', N'Redeem for Travel', N'D', N'F', 1, 1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RU', N'Redeem for QTRF Travel Certificate ', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'RV', N'Redeem for Online Travel', N'D', N'F', 1, -1, N'9')
INSERT [dbo].[TranType] ([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode]) VALUES (N'TT', N'Transfer from another TipNumber', N'I', N'F', 1, 1, N'1')
