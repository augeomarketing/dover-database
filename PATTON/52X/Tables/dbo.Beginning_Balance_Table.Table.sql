USE [52X]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT ((0)) FOR [MonthBeg1]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT ((0)) FOR [MonthBeg2]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT ((0)) FOR [MonthBeg3]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT ((0)) FOR [MonthBeg4]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT ((0)) FOR [MonthBeg5]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT ((0)) FOR [MonthBeg6]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT ((0)) FOR [MonthBeg7]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT ((0)) FOR [MonthBeg8]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT ((0)) FOR [MonthBeg9]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT ((0)) FOR [MonthBeg10]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT ((0)) FOR [MonthBeg11]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT ((0)) FOR [MonthBeg12]
GO
