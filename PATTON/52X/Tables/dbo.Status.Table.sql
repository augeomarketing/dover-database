USE [52X]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 10/25/2012 09:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[Status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'A', N'Active[A]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'C', N'Closed[C]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'D', N'Deleted[D]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'I', N'Inactive[I]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'K', N'Bankrupt[K]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'L', N'Lost/Stolen[L]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'O', N'OverLimit[O]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'P', N'Pending[P]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'S', N'Suspended[S]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'U', N'Fraud[U]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'X', N'PastDue[X]')
INSERT [dbo].[Status] ([Status], [StatusDescription]) VALUES (N'Y', N'FinanceChargeFrozen[Y]')
