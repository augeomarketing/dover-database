USE [master]
GO

/****** Object:  Database [52X]    Script Date: 10/25/2012 09:26:29 ******/
CREATE DATABASE [52X] ON  PRIMARY 
( NAME = N'52X_Data', FILENAME = N'C:\SQLData_RN\52X.mdf' , SIZE = 410944KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'52X_Log', FILENAME = N'C:\SQLData_RN\52X_1.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [52X] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [52X].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO

ALTER DATABASE [52X] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [52X] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [52X] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [52X] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [52X] SET ARITHABORT OFF 
GO

ALTER DATABASE [52X] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [52X] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [52X] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [52X] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [52X] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [52X] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [52X] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [52X] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [52X] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [52X] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [52X] SET  DISABLE_BROKER 
GO

ALTER DATABASE [52X] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [52X] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [52X] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [52X] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [52X] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [52X] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [52X] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [52X] SET  READ_WRITE 
GO

ALTER DATABASE [52X] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [52X] SET  MULTI_USER 
GO

ALTER DATABASE [52X] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO

ALTER DATABASE [52X] SET DB_CHAINING OFF 
GO

