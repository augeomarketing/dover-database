SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] @TipFirst char(15)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedcr numeric(9), @pointsbonuscr numeric(9), @pointspurchasedDB numeric(9), @pointsbonusdb numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedcr numeric(9), @pointsreturneddb numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) 

declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @SQLCursor nvarchar(2000), @SQLIF nvarchar(2000), @ErrorFlag char(1)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.Quarterly_Audit_ErrorFile '
Exec sp_executesql @SQLTruncate

/*                                                                            */
/* Setup Cursor for processing                                                */
set @sqlcursor=N'declare Tip_crsr cursor 
	for select Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased
	from ' + QuoteName(@DBName) + N' .dbo.Quarterly_Statement_File '
exec sp_executesql @SQLcursor

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, @pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, @pointsreturneddb, @pointsdecreased

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
	set @errmsg=NULL
	set @currentend='0'
	set @ErrorFlag='N'		
		
	set @sqlif=N'if @pointsend<>(select AdjustedEndingPoints from ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity where tipnumber=@tipnumber)
		Begin 
			set @errmsg=''Ending Balances do not match''
	
			set @currentend=(select AdjustedEndingPoints 
			from ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity
        		where 	tipnumber=@tipnumber) 
		
			INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Audit_ErrorFile
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, @pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, @pointsreturneddb, @pointsdecreased, @errmsg, @currentend) 
		End '

exec sp_executesql @SQLIf, N'@Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedcr numeric(9), 
				@pointsbonuscr numeric(9), @pointspurchasedDB numeric(9), @pointsbonusdb numeric(9), @pointsadded numeric(9),
 				@pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedcr numeric(9), @pointsreturneddb numeric(9), 
				@pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) ', 
				@Tipnumber=@Tipnumber, @pointsbegin =@pointsbegin, @pointsend = @pointsend, @pointspurchasedcr = @pointspurchasedcr, 
				@pointsbonuscr = @pointsbonuscr, @pointspurchasedDB = @pointspurchasedDB, @pointsbonusdb =@pointsbonusdb, 
				@pointsadded =@pointsadded, @pointsincreased = @pointsincreased, @pointsredeemed =@pointsredeemed, 
				@pointsreturnedcr = @pointsreturnedcr, @pointsreturneddb = @pointsreturneddb, @pointssubtracted = @pointssubtracted, 
				@pointsdecreased = @pointsdecreased, @errmsg = @errmsg, @currentend = @currentend

		goto Next_Record
		
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, @pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, @pointsreturneddb, @pointsdecreased
		
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
