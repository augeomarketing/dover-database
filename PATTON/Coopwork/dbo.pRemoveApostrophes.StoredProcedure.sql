SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pRemoveApostrophes] @TipFirst char(3)
AS

declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.DemographicIn set [Address #1]=replace([Address #1],char(39), '' ''), [Address #2]=replace([Address #2],char(39), '' ''), CITY=replace(City,char(39), '' ''),First=replace(First,char(39), '' ''), Last=replace(Last,char(39), '' '') '
		exec sp_executesql @SQLUpdate
GO
