SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[errors](
	[Pan] [char](16) NULL,
	[LastSix] [char](6) NULL,
	[First] [char](40) NULL,
	[Last] [char](40) NULL,
	[Address #1] [char](40) NULL,
	[City] [char](40) NULL,
	[ST] [char](10) NULL,
	[Error Message] [char](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
