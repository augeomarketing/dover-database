use COOPWORK
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

if object_id('pValidateDemographicIn') is not null
    drop PROCEDURE [dbo].[pValidateDemographicIn]
GO

CREATE PROCEDURE [dbo].[pValidateDemographicIn]
AS

truncate table dbo.errors
Truncate table dbo.DemographicIn_deleted


/*  PAN ERROR  */
insert into errors
(Pan, LastSix, First, Last, [Address #1], City, ST, [Error Message] )
select pan, right(pan,6), first, last, [address #1], city, st, 'PAN ERROR' 
from dbo.DemographicIn di left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 1 = sid_errorcheckoverridetype_id -- Bypass pan errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (Pan is null or left(pan,2)='  ')

Insert into DemographicIn_Deleted
Select di.*
from dbo.DemographicIn di left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 1 = sid_errorcheckoverridetype_id -- Bypass pan errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (Pan is null or left(pan,2)='  ')


delete di
from dbo.DemographicIn di left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 1 = sid_errorcheckoverridetype_id -- Bypass pan errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (Pan is null or left(pan,2)='  ')


/*  SSN ERROR -- for all BUT 605 */
insert into errors
(Pan, LastSix, First, Last, [Address #1], City, ST, [Error Message] )
select pan, right(pan,6), first, last, [address #1], city, st, 'SSN ERROR' 
from DemographicIn di left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 2 = sid_errorcheckoverridetype_id -- Bypass SSN errors
where ecpo.sid_errorcheckoverridetype_id is null
and  (SSN is null or left(SSN,2)='  ' or len(rtrim(ssn))<'9' or rtrim(ssn)='000000000' or rtrim(ssn)='111111111') --and left(pan,6)<>'463688' 

Insert into DemographicIn_Deleted
Select di.*
from DemographicIn di left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 2 = sid_errorcheckoverridetype_id -- Bypass SSN errors
where ecpo.sid_errorcheckoverridetype_id is null
and  (SSN is null or left(SSN,2)='  ' or len(rtrim(ssn))<'9' or rtrim(ssn)='000000000' or rtrim(ssn)='111111111') --and left(pan,6)<>'463688' 


delete di
from DemographicIn di left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 2 = sid_errorcheckoverridetype_id -- Bypass SSN errors
where ecpo.sid_errorcheckoverridetype_id is null
and  (SSN is null or left(SSN,2)='  ' or len(rtrim(ssn))<'9' or rtrim(ssn)='000000000' or rtrim(ssn)='111111111') --and left(pan,6)<>'463688' 


/*  ADDRESS 1 ERROR  */
insert into errors
(Pan, LastSix, First, Last, [Address #1], City, ST, [Error Message] )
select pan,right(pan,6), first, last, [address #1], city, st, 'ADDRESS 1 ERROR' 
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 3 = sid_errorcheckoverridetype_id -- Bypass Address1 errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   ([address #1] is null or left([address #1],2)='  ' )

Insert into DemographicIn_Deleted
Select di.*
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 3 = sid_errorcheckoverridetype_id -- Bypass Address1 errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   ([address #1] is null or left([address #1],2)='  ' )

delete di
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 3 = sid_errorcheckoverridetype_id -- Bypass Address1 errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   ([address #1] is null or left([address #1],2)='  ' )


/*  CITY ERROR  */
insert into errors
(Pan, LastSix, First, Last, [Address #1], City, ST, [Error Message] )
select pan,right(pan,6), first, last, [address #1], city, st, 'CITY ERROR' 
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 4 = sid_errorcheckoverridetype_id -- Bypass City errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (city is null or left(city,2)='  ' )

Insert into DemographicIn_Deleted
Select di.*
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 4 = sid_errorcheckoverridetype_id -- Bypass City errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (city is null or left(city,2)='  ' )

delete di
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 4 = sid_errorcheckoverridetype_id -- Bypass City errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (city is null or left(city,2)='  ' )


/*  STATE ERROR  */
insert into errors
(Pan, LastSix, First, Last, [Address #1], City, ST, [Error Message] )
select pan,right(pan,6), first, last, [address #1], city, st, 'STATE ERROR' 
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 5 = sid_errorcheckoverridetype_id -- Bypass State errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (st is null or left(st,1)=' ')

Insert into DemographicIn_Deleted
Select di.*
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 5 = sid_errorcheckoverridetype_id -- Bypass State errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (st is null or left(st,1)=' ')

delete di
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 5 = sid_errorcheckoverridetype_id -- Bypass State errors
where ecpo.sid_errorcheckoverridetype_id is null
AND   (st is null or left(st,1)=' ')


/*  NAME ERROR  */
insert into errors
(Pan, LastSix, First, Last, [Address #1], City, ST, [Error Message] )
select pan,right(pan,6), first, last, [address #1], city, st, 'NAME ERROR' 
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 6 = sid_errorcheckoverridetype_id -- Bypass Name errors
where ecpo.sid_errorcheckoverridetype_id is null
AND ([First] is null or left([First],2)='  ' or [Last] is null or left([Last],2)='  ')

Insert into DemographicIn_Deleted
Select di.*
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 6 = sid_errorcheckoverridetype_id -- Bypass Name errors
where ecpo.sid_errorcheckoverridetype_id is null
AND ([First] is null or left([First],2)='  ' or [Last] is null or left([Last],2)='  ')

delete di
from DemographicIn DI left outer join dbo.ErrorCheckPanOverride ecpo
    on left(di.pan,6) = ecpo.dim_errorcheckpanoverride_bin
    and 6 = sid_errorcheckoverridetype_id -- Bypass Name errors
where ecpo.sid_errorcheckoverridetype_id is null
AND ([First] is null or left([First],2)='  ' or [Last] is null or left([Last],2)='  ')


--CPM SSN AUDIT
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[zzzCPMDemographicInAudit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[zzzCPMDemographicInAudit]

select DemographicIn.*
into zzzCPMDemographicInAudit
from DemographicIn
where TipFirst='625'
-----END CPM SSN AUDIT



/*  WRITE OFF AND THEN DELETE FROM INPUT FILE  */
--Truncate table DemographicIn_deleted

--Insert into DemographicIn_Deleted
--Select *  from DemographicIn
--where ((Pan is null or left(pan,2)='  ' or SSN is null or left(SSN,2)='  ' or len(rtrim(ssn))<'9' or rtrim(ssn)='000000000' or rtrim(ssn)='111111111') and left(pan,6)<>'463688' )
--	or [Inst ID] is null or left([Inst ID],2)='  ' or [address #1] is null or left([address #1],2)='  ' 
--	or city is null or left(city,2)='  ' or st is null or left(st,1)=' ' or First is null or left(First,2)='  ' 
--	or Last is null or left(Last,2)='  '

--delete from DemographicIn
--where ((Pan is null or left(pan,2)='  ' or SSN is null or left(SSN,2)='  ' or len(rtrim(ssn))<'9' or rtrim(ssn)='000000000' or rtrim(ssn)='111111111') and left(pan,6)<>'463688' )
--	or [Inst ID] is null or left([Inst ID],2)='  ' or [address #1] is null or left([address #1],2)='  ' 
--	or city is null or left(city,2)='  ' or st is null or left(st,1)=' ' or First is null or left(First,2)='  ' 
--	or Last is null or left(Last,2)='  '
GO


/* Test Harness


exec [dbo].[pValidateDemographicIn]




*/