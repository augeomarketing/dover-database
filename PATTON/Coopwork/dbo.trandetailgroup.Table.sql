SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[trandetailgroup](
	[trandate] [char](10) NULL,
	[PAN] [nvarchar](19) NULL,
	[MSGTYPE] [char](4) NULL,
	[PROCESSINGCODE] [char](6) NULL,
	[amounttran] [decimal](38, 0) NULL,
	[SIC] [nchar](10) NULL,
	[NETID] [nchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
