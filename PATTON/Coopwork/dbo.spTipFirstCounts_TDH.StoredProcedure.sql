SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTipFirstCounts_TDH]
@CallTip varchar(3)=null,
@CallLocation varchar(20) =null,
@StartDate char(10)

 AS
set nocount on

Declare @BIN varchar(10),@TipFirst varchar(3), @BinLen int, @NumRecs int
declare @Rundate DateTime
Set @RunDate=GetDate()

declare csr cursor
for select TipFirst,rtrim(BIN)
from TipFirstReference
where Isactive=1
order by tipfirst
/*                                                                            */
print 'TransDetailHold records by tip with Trandate>=' + @StartDate
open csr
fetch csr into @TipFirst, @BIN

if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin
	
	Select @NumRecs= count(*) from TransDetailHold where  trandate >= @StartDate  and PAN like @BIN +'%' 
	
	print @TipFirst + '-' + @BIN + '-' + convert(varchar(50),@NumRecs)


insert into TipFirstCounts_TDH values (@TipFirst, @Bin, @NumRecs, @RunDate, @CallTip, @CallLocation)

fetch csr into @TipFirst, @BIN
end
Fetch_Error:
close  csr
deallocate  csr

select * from TipFirstCounts_TDH where @Rundate=(select max(rundate) from TipFirstCounts_TDH )
GO
