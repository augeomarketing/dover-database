SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGenerateEStatementBonus] @EndDate varchar(10), @tipfirst char(3)
AS 

/*************************************************************/
/*                                                           */
/*   Procedure to generate the E-Statement Bonus             */
/*                                                           */
/*                                                           */
/*************************************************************/

declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10)

set @Trandate=@EndDate

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  [E-StatementBonus] from DBProcessInfo
				where DBNumber=@TipFirst)
	
If @BonusPoints>0 and @BonusPoints is not null
Begin
	/*                                                                            */
	/* Setup Cursor for processing                                                */
	set @sqlcursor=N'declare Tip_crsr cursor 
		for select tipnumber
		from ' + QuoteName(@DBName) + N' .dbo.OnetimeBonuses 
		where trancode=''BT'' and dateawarded is null '
	exec sp_executesql @SQLcursor
	
	/*                                                                            */
	open Tip_crsr
	/*                                                                            */
	fetch Tip_crsr into @Tipnumber 
	
	/*                                                                            */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
	begin	
		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
				where tipnumber = @Tipnumber'
		exec sp_executesql @SQLUpdate, N'@Tipnumber nchar(15), @BonusPoints numeric(9)', @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints
	
		if exists(select tipnumber from customer where tipnumber=@tipnumber)
		Begin
			set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
	        		Values(@Tipnumber, @Trandate, ''BT'', ''1'', @BonusPoints, ''1'', ''E-Statement Bonus'', ''0'')'
			Exec sp_executesql @SQLInsert, N'@Trandate nchar(10), @Tipnumber nchar(15), @BonusPoints numeric(9)', @Trandate= @Trandate, @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints
		
			set @SQLUpdate='update ' + QuoteName(@DBName) + N'.dbo.OneTimeBonuses
					set DateAwarded=@Trandate
					where current of Tip_crsr '
			Exec sp_executesql @SQLUpdate, N'@Trandate nchar(10)', @Trandate=@Trandate 
		End
	
		goto Next_Record
	Next_Record:
		fetch tip_crsr into @tipnumber
	end
	
	Fetch_Error:
	close  tip_crsr
	deallocate  tip_crsr

End
GO
