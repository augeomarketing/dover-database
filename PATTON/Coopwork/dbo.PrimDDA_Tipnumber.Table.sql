SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrimDDA_Tipnumber](
	[Prim DDA] [nchar](16) NULL,
	[TipNumber] [nchar](15) NULL
) ON [PRIMARY]
GO
