SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_1]
AS 
Declare @Pan char(19), @msgtype char(4), @ProcessingCode char(6), @AmountTRAN decimal(12,0), @SIC char(4), @NETID char(3), @Trandate char(10), @Work1 char(4), @TERMID char(8), @ACCEPTORID char(15)
/*                                                                            */


delete from transdetailin
where sic='6011'
delete from transdetailin
where processingcode not in ('000000', '002000', '200020', '200040', '500000', '500020', '500010') 
delete from transdetailin
where left(msgtype,2) not in ('02', '04') 

declare @s nvarchar(1000)
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING TRANSDETAILIN                               */
/*                                                                            */
declare TRANSIN_crsr cursor
for select   Trandate, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, TERMID, ACCEPTORID
from TRANSDETAILIN
--where Pan='4238570016908715'
/*                                                                            */
open TRANSIN_crsr
fetch TRANSIN_crsr into @Trandate, @MSGTYPE, @PAN, @PROCESSINGCODE, @AMOUNTTRAN, @SIC, @NETID, @TERMID, @ACCEPTORID
 
declare  @Counter int, @RowsUpdated int ,@Remainder int, @StartTime datetime, @CurrTime datetime, @Elapsed int, @InsertCount int, @UpdateCount int
select @StartTime=GetDate()
set @RowsUpdated =0
set @Counter=0
set @InsertCount=0
set @UpdateCount=0
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin


-- Move incoming daily transcations to the monthly hold file
--do an updated first and if no matching recs  are available for update, do the insert

		begin


			-- Update CUSTOMER TABLE
			update TransDetailHold
			set AmountTran=AmountTran + @AmountTran, NumberOfTrans=NumberOfTrans + '1'
			where   pan=@pan and TRANDATE=@TRANDATE and MSGTYPE=@MSGTYPE and ProcessingCode=@ProcessingCode and SIC=@SIC and NETID=@NETID AND TERMID=@TERMID AND ACCEPTORID=@ACCEPTORID/* Rev 1 */
			Select @RowsUpdated=@@rowcount
			
			--set @UpdateCount=@UpdateCount+1
			
		end

		if @RowsUpdated=0
		begin

			-- Add to TransactionDetailHold TABLE

			insert into TransDetailHold(TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, NUMBEROFTRANS, TERMID, ACCEPTORID)				
			VALUES(@TRANDATE, @MSGTYPE, @PAN, @PROCESSINGCODE, @AMOUNTTRAN, @SIC, @NETID, '1', @TERMID, @ACCEPTORID)
	
			--set @InsertCount=@InsertCount+1

		end
	goto Next_Record
Next_Record:
	begin
--	set @Counter=@Counter+1
--	set @RowsUpdated =0
--	select @CurrTime=GetDate()
--	select @Remainder=@Counter % 5
--	if @Remainder=0 
--		set @Elapsed=datediff(ss,@StartTime,@CurrTime )
--		print convert(varchar(20),@Counter) + '-' + convert(varchar(20),@Elapsed) 
		
	fetch TRANSIN_crsr into @Trandate, @MSGTYPE, @PAN, @PROCESSINGCODE, @AMOUNTTRAN, @SIC, @NETID, @TERMID, @ACCEPTORID	end

end
Fetch_Error:
close  TRANSIN_crsr
deallocate  TRANSIN_crsr
print ' Inserts:' + convert(varchar(20),@InsertCount)
print ' Updates:' + convert(varchar(20),@UpdateCount)
GO
