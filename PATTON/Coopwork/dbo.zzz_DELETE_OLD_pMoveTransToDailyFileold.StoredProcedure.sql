SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzz_DELETE_OLD_pMoveTransToDailyFileold] 
AS 
-- 
-- Moves Data from the daily input source file to a daily work file
--    combining the two parts of a transaction into one record.
--
-- REVISION 1
-- BY  S. BLANCHETTE
-- DATE:  5/23/2007
-- SCAN:  SEB001
-- REASON:  TO HANDLE TWO NEW FIELDS FOR MERCHANT REWARDS
--
Declare @Col001 char(80), @Pan char(19), @msgtype char(4), @ProcessCode char(6), @Amount decimal(12,0), @SIC char(4), @NETID char(3), @Trandate char(10), @Work1 char(4), @TermID char(8), @AcceptorID char(15)
Truncate Table TransDetailIn
declare Tran_crsr cursor
for select col001 
from prc39
order by rec 
/*                                                                            */
open Tran_crsr
fetch Tran_crsr into @Col001
set @work1=year(getdate())
set @Trandate=left(@col001,2) +'/'+ SUBSTRING (@Col001, 3, 2)+'/'+@work1
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
fetch Tran_crsr into @Col001
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	If left(@Col001,2)='01' and SUBSTRING (@Col001, 10, 4)<>'0520' and SUBSTRING (@Col001, 10, 4)<>'0522'
	Begin
		set @Pan=SUBSTRING (@Col001, 14, 19)
		set @MSGtype=SUBSTRING (@Col001, 10, 4)
		set @ProcessCode=SUBSTRING (@Col001, 33, 6)
		Set @Amount=SUBSTRING (@Col001, 39, 12)
		goto Next_Record
	End
	Else
		If left(@Col001,2)='02' and SUBSTRING (@Col001, 10, 4)<>'0520' and SUBSTRING (@Col001, 10, 4)<>'0522'
		Begin
			set @SIC=SUBSTRING (@Col001, 10, 4)
			set @NETID=SUBSTRING (@Col001, 71, 3)
			insert into TransDetailIn (Trandate, MSGTYPE,PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID)
			values(@Trandate, @MSGTYPE, @Pan, @ProcessCode, @Amount, @SIC, @NETID)
			goto Next_Record
		End
				
	Else
Next_Record:
		fetch Tran_crsr into @Col001
end
Fetch_Error:
close  Tran_crsr
deallocate  TRAN_crsr
GO
