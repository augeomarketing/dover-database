SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TRANSDETAILIN](
	[Trandate] [char](10) NULL,
	[MSGTYPE] [char](4) NULL,
	[PAN] [nvarchar](19) NULL,
	[PROCESSINGCODE] [char](6) NULL,
	[AMOUNTTRAN] [decimal](12, 0) NULL,
	[SIC] [nchar](10) NULL,
	[NETID] [nchar](10) NULL,
	[TermID] [nchar](8) NULL,
	[AcceptorID] [nchar](15) NULL,
	[OrigDataElem] [nvarchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
