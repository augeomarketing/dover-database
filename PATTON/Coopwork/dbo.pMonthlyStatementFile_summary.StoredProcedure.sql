SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pMonthlyStatementFile_summary] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3)

AS 

/*******************************************************************************/
/* Inserts summary record into summary table  */
/*******************************************************************************/


Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = @StartDateParm 
set @Enddate = @EndDateParm

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket='MonthBeg' + @monthbegin

Print @DBName


set @SQLSelect='insert into  ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_Summary
	Select 
	@Enddate,
	Count(*) as NumAccounts,
	sum(isnull(PointsBegin,0)) as PointsBegin,
	sum(isnull(PointsEnd,0)) as PointEnd,
	sum(PointsPurchasedCR) as PointsPurchasedCR,
	sum(PointsBonusCR) as PointsBonusCR,
	sum(PointsAdded) as PointsAdded,
	sum(PointsPurchasedDB)as PointsPurchasedDB,
	sum(PointsBonusDB)as PointsBonusDB,
	sum(PointsIncreased) as PointsIncreased,	
	sum(PointsRedeemed) as PointsRedeemed,
	sum(PointsReturnedCR) as PointsReturnedCR,
	sum(PointsSubtracted) as PointsSubtracted,
	sum(PointsReturnedDB) as PointsReturnedDB,
	sum(PointsDecreased) as PointsDecreased
	from  ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File'


		
Exec sp_executesql @SQLSelect, N'@EndDate DateTime', @EndDate=@EndDate
GO
