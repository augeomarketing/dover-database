SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzz_DELETE_OLD_spMoveTransToMonthlyHoldOld042307] 
AS 

Declare @Pan char(19), @msgtype char(4), @ProcessingCode char(6), @AmountTRAN decimal(12,0), @SIC char(4), @NETID char(3), @Trandate char(10), @Work1 char(4)

/*                                                                            */
/*  REMOVE INVALID CODES FROM TRANSDETAILIN                                   */
/*                                                                            */
/*    Revision 1                                                              */
/*    By S. Blanchette                                                        */
/*    Fix problem with missing codes do to not using all parameters in check  */
/*    to see if exists                                                        */

delete from transdetailin
where sic='6011'

delete from transdetailin
where processingcode not in ('000000', '002000', '200020', '200040', '500000', '500020', '500010') 

delete from transdetailin
where left(msgtype,2) not in ('02', '04') 

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING TRANSDETAILIN                               */
/*                                                                            */
declare TRANSIN_crsr cursor
for select Trandate, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID
from TRANSDETAILIN
/*                                                                            */
open TRANSIN_crsr
fetch TRANSIN_crsr into @Trandate, @MSGTYPE, @PAN, @PROCESSINGCODE, @AMOUNTTRAN, @SIC, @NETID
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

--
-- Move incoming daily transcations to the monthly hold file
--
	if (not exists (select pan from TransDetailHold where TRANDATE=@TRANDATE and MSGTYPE=@MSGTYPE and pan=@pan and ProcessingCode=@ProcessingCode and SIC=@SIC and NETID=@NETID) and @pan is not null and left(@pan,1)<>' ') /* Rev 1 */
		begin
			-- Add to TransactionDetailHold TABLE
			insert into TransDetailHold(TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, NUMBEROFTRANS)				
			VALUES(@TRANDATE, @MSGTYPE, @PAN, @PROCESSINGCODE, @AMOUNTTRAN, @SIC, @NETID, '1')
		end
	else
		begin
			-- Update CUSTOMER TABLE
			update TransDetailHold
			set AmountTran=AmountTran + @AmountTran, NumberOfTrans=NumberOfTrans + '1'
			where TRANDATE=@TRANDATE and MSGTYPE=@MSGTYPE and pan=@pan and ProcessingCode=@ProcessingCode and SIC=@SIC and NETID=@NETID /* Rev 1 */
		end

	goto Next_Record
Next_Record:
	fetch TRANSIN_crsr into @TRANDATE, @MSGTYPE, @PAN, @PROCESSINGCODE, @AMOUNTTRAN, @SIC, @NETID

end

Fetch_Error:
close  TRANSIN_crsr
deallocate  TRANSIN_crsr
GO
