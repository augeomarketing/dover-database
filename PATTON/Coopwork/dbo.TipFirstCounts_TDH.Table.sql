SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipFirstCounts_TDH](
	[TipFirst] [varchar](3) NULL,
	[Bin] [varchar](10) NULL,
	[RecCount] [int] NULL,
	[RunDate] [datetime] NULL,
	[CallTip] [varchar](3) NULL,
	[CallLocation] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
