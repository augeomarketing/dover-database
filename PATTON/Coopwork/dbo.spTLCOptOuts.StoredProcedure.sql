SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/***********************************************************************************/
/*                   SQL TO SCRUB CUSTOMERS FOR COOP FI AND PLACE IN OPT OUT FILE */
/*                                                                           */
/* BY:  S.BLANCHETTE                                                         */
/* DATE: 5/2007                                                              */
/* REVISION: 0                                                               */
/*                                                                           */
/* This IS TO SCRUB CUSTOMER AND AFFILIAT FILESreplaces any special characters with blanks                          */


CREATE PROCEDURE [dbo].[spTLCOptOuts]
As


declare @tipnumber nchar(15)

declare Tran_crsr cursor
for select tipnumber 
from tlcopt
order by TIPNUMBER
/*                                                                            */

OPEN Tran_crsr 
fetch Tran_crsr into @Tipnumber

if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

	exec spOptOutControlEntry @Tipnumber 

Next_Record:
	fetch Tran_crsr into @Tipnumber

end

Fetch_Error:
close  Tran_crsr
deallocate  TRAN_crsr
GO
