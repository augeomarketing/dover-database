SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetTipFirst_TransDetailHold_Grouped]  
AS

--update the TipFirst field for TransDetailHold
update TransDetailHold_Grouped
set tipFirst=b.TipFirst
from TransDetailHold_Grouped a
join TipFirstReference b
on Left(a.PAN,len(rtrim(b.bin)))=rtrim(b.bin)
WHERE a.TipFirst is null
GO
