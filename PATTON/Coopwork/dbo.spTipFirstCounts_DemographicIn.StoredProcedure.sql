SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTipFirstCounts_DemographicIn]
@CallTip varchar(3)=null,
@CallLocation varchar(20) =null,
@Note varchar(100)

 AS
set nocount on

Declare @BIN varchar(10),@TipFirst varchar(3), @BinLen int, @NumRecs int
declare @Rundate DateTime
Set @RunDate=GetDate()

declare csr cursor
for select TipFirst,rtrim(BIN)
from TipFirstReference
where Isactive=1
order by tipfirst
/*                                                                            */
print 'DemographicIN records by tip'
open csr
fetch csr into @TipFirst, @BIN

if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin
	
	Select @NumRecs= count(*) from DemographicIn where PAN like @BIN +'%'
	
	print @TipFirst + '-' + @BIN + '-' + convert(varchar(50),@NumRecs)


insert into TipFirstCounts values (@TipFirst, @Bin, @NumRecs, @RunDate, @CallTip, @CallLocation,@Note)



fetch csr into @TipFirst, @BIN
end
Fetch_Error:
close  csr
deallocate  csr

declare @MaxRunDate datetime
select @MaxRunDate =max(rundate) from TipFirstCounts
select * from TipFirstCounts where Rundate=@MaxRunDate
GO
