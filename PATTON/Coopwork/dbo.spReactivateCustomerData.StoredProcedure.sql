SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spReactivateCustomerData] @TipNumber char(15), @DBNAME char(100)
AS 

Declare @SQLInsert nvarchar(2000),@SQLDelete nvarchar(2000) 

set @sqlInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Customer (TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
       	SELECT Top 1 TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, ''A'', ''Active'', HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew 
	FROM ' + QuoteName(@DBName) + N'.dbo.Customerdeleted where tipnumber=@tipNumber
	Order by Tipnumber asc, Datedeleted desc '
Exec sp_executesql @sqlInsert, N'@TIPNUMBER nchar(15)', @TIPNUMBER=@TIPNUMBER

set @sqlInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Affiliat (TIPNumber, accttype, dateadded, AcctTypeDesc, AcctStatus, SecID,  acctid, custid)
       	SELECT TIPNumber, accttype, dateadded, AcctTypeDesc, AcctStatus, SecID,  acctid, custid
	FROM ' + QuoteName(@DBName) + N'.dbo.affiliatdeleted where tipnumber=@TipNumber and dateadded is not null and acctid is not null '
Exec sp_executesql @sqlInsert, N'@TIPNUMBER nchar(15)', @TIPNUMBER=@TIPNUMBER

set @sqlInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage 
	FROM ' + QuoteName(@DBName) + N'.dbo.Historydeleted where tipnumber=@TipNumber '
Exec sp_executesql @sqlInsert, N'@TIPNUMBER nchar(15)', @TIPNUMBER=@TIPNUMBER

set @sqlDelete='delete from ' + QuoteName(@DBName) + N'.dbo.affiliatdeleted where tipnumber=@TipNumber'
Exec sp_executesql @sqlDelete, N'@TIPNUMBER nchar(15)', @TIPNUMBER=@TIPNUMBER

set @sqlDelete='delete from ' + QuoteName(@DBName) + N'.dbo.historydeleted where tipnumber=@TipNumber'
Exec sp_executesql @sqlDelete, N'@TIPNUMBER nchar(15)', @TIPNUMBER=@TIPNUMBER

set @sqlDelete='delete from ' + QuoteName(@DBName) + N'.dbo.customerdeleted where tipnumber=@TipNumber'
Exec sp_executesql @sqlDelete, N'@TIPNUMBER nchar(15)', @TIPNUMBER=@TIPNUMBER
GO
