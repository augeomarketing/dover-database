USE [COOPWork]
GO

/****** Object:  View [dbo].[vw_cc605DemogImport]    Script Date: 11/30/2010 16:25:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_cc605DemogImport]'))
DROP VIEW [dbo].[vw_cc605DemogImport]
GO

USE [COOPWork]
GO

/****** Object:  View [dbo].[vw_cc605DemogImport]    Script Date: 11/30/2010 16:25:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_cc605DemogImport]
AS
SELECT 
	dim_cc605demogimport_filedate
	, dim_cc605demogimport_memberid
	, dim_cc605demogimport_accountnumber
	, dim_cc605demogimport_relationshipaccount
	, dim_cc605demogimport_deleteindicator
	, dim_cc605demogimport_fullname
	, dim_cc605demogimport_primaryssn
	, dim_cc605demogimport_secondaryssn
FROM
	cc605DemogImport

GO


