SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STATEMENT](
	[TRAVNUM] [nvarchar](255) NULL,
	[ACCTNAME1] [nvarchar](255) NULL,
	[ACCTNAME2] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[LASTLINE] [nvarchar](255) NULL,
	[STDATE] [nvarchar](255) NULL,
	[PNTBEG] [float] NULL,
	[PNTEND] [float] NULL,
	[PNTPRCHSCR] [float] NULL,
	[PNTBONUSCR] [float] NULL,
	[PNTADD] [float] NULL,
	[PNTPRCHSDB] [float] NULL,
	[PNTBONUSDB] [float] NULL,
	[PNTINCRS] [float] NULL,
	[PNTREDEM] [float] NULL,
	[PNTRETRNCR] [float] NULL,
	[PNTSUBTR] [float] NULL,
	[PNTRETRNDB] [float] NULL,
	[PNTDECRS] [float] NULL,
	[ZIP] [nvarchar](255) NULL,
	[ACCT_NUM] [nvarchar](255) NULL,
	[LASTFOUR] [nvarchar](255) NULL
) ON [PRIMARY]
GO
