SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spHandleOptOuts]
as


/* delete OptOutControl records for cardnumbers in the OptIns table */
-- delete optoutcontrol where acctnumber in (select Acctnumber from OptIns)
delete ooc
from COOPWORK.dbo.optoutcontrol ooc join dbo.optins oi
	on ooc.acctnumber = oi.acctnumber



/* Remove Demographicin records for Opt Out people  */
--delete from Demographicin
--where exists(select * from optoutcontrol where acctnumber=Demographicin.pan)
delete di
from dbo.DemographicIn di join dbo.optoutcontrol ooc
	on di.pan = ooc.acctnumber


/* Remove transaction records for Opt Out people  */
--delete from transdetailhold
--where exists(select * from optoutcontrol where acctnumber=transdetailhold.pan)
delete tdh
from dbo.transdetailhold tdh join dbo.optoutcontrol ooc
	on tdh.pan = ooc.acctnumber
GO
