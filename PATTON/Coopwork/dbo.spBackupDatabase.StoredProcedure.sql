SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spBackupDatabase] @DBName varchar(100)
as

declare @spath varchar(100), @sBackupPath varchar(100), @cleardbs varchar(100)
declare @backupname varchar(100)

/*	set @sPath = 'd:\SQLData\BACKUPS\'
	set @sBackupPath = @sPath + @DBName
*/

set @backupname = @dbname + '.bak'

backup database @DBName to disk = @backupname with noinit
GO
