SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyAuditValidation] @TipFirst char(15)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

declare @DBName varchar(50), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000), @errmsg varchar(50)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--set @DBName='zz617OmniCCUConsumer'


set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.Monthly_Audit_ErrorFile '
Exec sp_executesql @SQLTruncate

set @errmsg='Ending Balances do not match'

set @SQLInsert = N'INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Monthly_Audit_ErrorFile
	select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
	from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity b
	where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints '
Exec sp_executesql @SQLInsert, N'@errmsg varchar(50)', @errmsg = @errmsg
GO
