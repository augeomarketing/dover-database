SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[zsp_DELETE_617_HandleOptiNS]
as




/* Remove Demographicin records for All but Opt In  people  */
delete from Demographicin
where substring(Pan,1,9)='544330865'
and Pan NOT in(select CardNumOut from ZtMP_delete_217OptInData)



/* Remove transaction records for All but Opt In people  */
--delete from coopwork.dbo. transdetailhold
--where substring(Pan,1,9)='544330865'
--and Pan NOT in(select CardNumOut from ZtMP_delete_217OptInData)
GO
