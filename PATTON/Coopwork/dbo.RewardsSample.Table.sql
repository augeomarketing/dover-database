SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RewardsSample](
	[Name1] [varchar](255) NULL,
	[Name2] [varchar](255) NULL,
	[Addr1] [varchar](255) NULL,
	[Addr2] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[State] [varchar](255) NULL,
	[Zip] [varchar](255) NULL,
	[HomePhone] [varchar](255) NULL,
	[WorkPhone] [varchar](255) NULL,
	[status] [varchar](255) NULL,
	[cardno] [varchar](255) NULL,
	[AccountNumber] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
