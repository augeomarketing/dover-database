SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zNotes](
	[Note] [varchar](500) NULL,
	[Notedate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[zNotes] ADD  CONSTRAINT [DF_zNotes_Notedate]  DEFAULT (getdate()) FOR [Notedate]
GO
