SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGetDemographicin] @TipFirst char(3)
as

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.Demographicin'
Exec sp_executesql @SQLTruncate

--insert into  Demographicin
--select * from COOPWork.dbo.Demographicin
--where TipFirst=@TipFirst

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Demographicin select Pan, [Inst ID], CS, [Prim DDA], [1st DDA], [2nd DDA], [3rd DDA], [4th DDA], [5th DDA], [Address #1], [Address #2], [City ], ST, Zip, ltrim([First]), ltrim([Last]), MI, [First2], [Last2], MI2, [First3], [Last3], MI3, [First4], [Last4], MI4, SSN, [Home Phone], [Work Phone], TipFirst, TipNumber, NA1, NA2, NA3, NA4, NA5, NA6, LastName from Demographicin where tipfirst =@Tipfirst'

Exec sp_executesql @SQLInsert, N'@Tipfirst char(3)',@tipfirst=@tipfirst
GO
