SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipFirstReference](
	[BIN] [char](9) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[TypeCard] [char](1) NULL,
	[IsActive] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[TipFirstReference] ADD  CONSTRAINT [DF_TipFirstReference_IsActive]  DEFAULT (1) FOR [IsActive]
GO
