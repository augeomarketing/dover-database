SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pLoadCUSTINssn] @dateadded char(10), @tipFirst char(3)
AS 
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO LOAD CUSTIN TABLE                                            */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*                                                                            */
/* Revision:                                           */
/* By S. Blanchette                               */
/* Date 7/19/2007                               */
/* Scan SEB001                                   */
/* Reason:  add logic to put DDANUM into MISC1    */
/*                                                                            */
/******************************************************************************/
 

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--set @DBName='zz617OmniCCUConsumer'

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.custin'
Exec sp_executesql @SQLTruncate

--LOADS THE SSN into Misc1

-- Add to CUSTIN TABLE
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.custin(ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, HomePhone, WorkPhone, MISC1, DateAdded ) 
									select PAN, NA1, NA2, NA3, NA4, NA5, NA6, ''A'', TIPNUMBER, [Address #1], [Address #2], (rtrim([City ]) + '' '' + rtrim(st) + '' '' + rtrim(zip)), [City ], ST, left(ltrim(rtrim(ZIP)),5), LASTNAME, [Home Phone], [Work Phone], [SSN], @DateAdded 
from ' + QuoteName(@DBName) + N'.DBO.Demographicin order by tipnumber'

Exec sp_executesql @SQLInsert, N'@DATEADDED nchar(10)',	@Dateadded= @Dateadded
GO
