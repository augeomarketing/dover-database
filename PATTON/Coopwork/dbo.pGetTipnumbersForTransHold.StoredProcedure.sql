SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetTipnumbersForTransHold] 
AS 

Declare @SQLInsert nvarchar(1000), @DBName varchar(50)

TRUNCATE TABLE affiliat

DECLARE cDBName CURSOR FOR
	SELECT  rtrim(DBName) from DBProcessInfo
	OPEN cDBName 
	FETCH NEXT FROM cDBName INTO @DBName

WHILE (@@FETCH_STATUS=0)
	BEGIN
		set @SQLInsert='insert into Affiliat select acctid, tipnumber
			from ' + QuoteName(@DBName) + N'.dbo.Affiliat'
		exec sp_executesql @SQLInsert 

		FETCH NEXT FROM cDBName INTO @DBName
	END

Fetch_Error:
CLOSE cDBName
DEALLOCATE cDBName

update TransDetailHold
set TransDetailHold.tipnumber=affiliat.tipnumber
from TransDetailHold, Affiliat
where TransDetailHold.pan=affiliat.acctid
GO
