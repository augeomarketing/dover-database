SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spWelcomeKitCNTY] @EndDate varchar(10), @TipFirst nchar(3)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.WelcomekitCNTY '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.WelcomekitCNTY SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode, Misc2 FROM ' + QuoteName(@DBName) + N' .dbo.customer WHERE (DATEADDED = @EndDate AND STATUS <> ''c'') '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate
GO
