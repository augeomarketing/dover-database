SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGenerateNewAccountBonuses] @Tipfirst char(3), @DateIn varchar(10)
AS

declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10)

set @Trandate=@Datein

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  ActivationBonus from DBProcessInfo
				where DBNumber=@TipFirst)

if @BonusPoints>0 and @BonusPoints is not null
Begin
	
/*                                                                            */
/* Setup Cursor for processing                                                */
set @sqlcursor=N'declare Customer_crsr cursor 
for select tipnumber
from ' + QuoteName(@DBName) + N' .dbo.Customer where dateadded=@Trandate'
exec sp_executesql @SQLcursor, N'@Trandate nchar(10)',	@Trandate= @Trandate

/*                                                                            */
open Customer_crsr
/*                                                                            */
fetch Customer_crsr into @Tipnumber 

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
		where tipnumber = @Tipnumber'
		exec sp_executesql @SQLUpdate, N'@Tipnumber nchar(15), @BonusPoints numeric(9)', @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints

		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        	Values(@Tipnumber, @Trandate, ''BA'', ''1'', @BonusPoints, ''1'', ''Activation Bonus'', ''0'')'
		Exec sp_executesql @SQLInsert, N'@Trandate nchar(10), @Tipnumber nchar(15), @BonusPoints numeric(9)', @Trandate= @Trandate, @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints
 
Next_Record:
	fetch Customer_crsr into @Tipnumber
end

Fetch_Error:
close Customer_crsr
deallocate Customer_crsr

End
GO
