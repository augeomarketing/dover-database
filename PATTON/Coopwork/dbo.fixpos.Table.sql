SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixpos](
	[TipFirst] [char](3) NULL,
	[msgtype] [char](2) NULL,
	[MonthStart] [char](10) NULL,
	[Number] [int] NULL,
	[Points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
