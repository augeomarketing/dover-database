SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_NEW]
AS 
Declare @Pan char(19), @msgtype char(4), @ProcessingCode char(6), @AmountTRAN decimal(12,0), @SIC char(4), @NETID char(3), @Trandate char(10), @Work1 char(4), @TERMID char(8), @ACCEPTORID char(15)
/*                                                                            */


delete from transdetailin	where sic='6011' 
delete from transdetailin	where  processingcode not in ('000000', '002000', '200020', '200040', '500000', '500020', '500010') 
delete from transdetailin	where  left(msgtype,2) not in ('02','04')   

                                                                    
insert Into TransDetailHold (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID,  NUMBEROFTRANS, TERMID, ACCEPTORID,OrigDataElem )
	 select   Trandate, MSGTYPE, PAN, PROCESSINGCODE, SUM(AMOUNTTRAN) as AmountTran, SIC, NETID, Count(AMOUNTTRAN),TERMID, ACCEPTORID, OrigDataElem
	from TRANSDETAILIN
	Group By PAN, TranDate,MsgType,ProcessingCode,SIC,NETID, TERMID, ACCEPTORID, OrigDataElem
GO
