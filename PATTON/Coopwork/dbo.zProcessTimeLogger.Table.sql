SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zProcessTimeLogger](
	[ProcessName] [varchar](50) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[ElapsedMinutes] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
