USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[pLoadCUSTINssndda642]    Script Date: 07/30/2010 10:21:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pLoadCUSTINssndda642]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pLoadCUSTINssndda642]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pLoadCUSTINssndda642]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
	/******************************************************************************/
	/*                                                                            */
	/*    THIS IS TO LOAD CUSTIN TABLE                                            */
	/*                                                                            */
	/******************************************************************************/
	/******************************************************************************/
	/*                                                                            */
	/* Revision:                                           */
	/* By S. Blanchette                               */
	/* Date 7/19/2007                               */
	/* Scan SEB001                                   */
	/* Reason:  add logic to put DDANUM into MISC1    */
	/*                                                                            */
	/******************************************************************************/
	/******************************************************************************/
	/*                                                                            */
	/* Revision:                                           */
	/* By S. Blanchette                               */
	/* Date 7/2010                               */
	/* Scan SEB002                                   */
	/* Reason:  add logic to populate segment code    */
	/*                                                                            */
	/******************************************************************************/
CREATE PROCEDURE [dbo].[pLoadCUSTINssndda642] 
	-- Add the parameters for the stored procedure here
	@dateadded char(10), 
	@tipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLUpdate nvarchar(1000)

	 

	set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)
	--set @DBName=''zz617OmniCCUConsumer''

	set @SQLTruncate=''Truncate Table '' + QuoteName(@DBName) + N''.dbo.custin''
	Exec sp_executesql @SQLTruncate

	--LOADS THE SSN into Misc1

	-- Add to CUSTIN TABLE
	set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.custin(ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, HomePhone, WorkPhone, MISC1, MISC2,DateAdded, segcode ) 
										select PAN, NA1, NA2, NA3, NA4, NA5, NA6, ''''A'''', TIPNUMBER, [Address #1], [Address #2], (rtrim([City ]) + '''' '''' + rtrim(st) + '''' '''' + rtrim(zip)), [City ], ST, left(ltrim(rtrim(ZIP)),5), LASTNAME, [Home Phone], [Work Phone], [SSN], [PRIM DDA], @DateAdded, [CS] 
	from '' + QuoteName(@DBName) + N''.DBO.Demographicin order by tipnumber''

	Exec sp_executesql @SQLInsert, N''@DATEADDED nchar(10)'',	@Dateadded= @Dateadded

	set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.custin set segcode = ''''D'''' where segcode=''''0'''' ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.custin set segcode = ''''C'''' where segcode is null ''
	Exec sp_executesql @SQLUpdate

END
' 
END
GO
