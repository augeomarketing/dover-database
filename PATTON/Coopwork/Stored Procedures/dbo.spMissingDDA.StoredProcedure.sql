USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spMissingDDA]    Script Date: 07/23/2012 15:05:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMissingDDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMissingDDA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMissingDDA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6/2012
-- Description:	Strip out records with missing DDAs
-- =============================================
CREATE PROCEDURE [dbo].[spMissingDDA] 
	-- Add the parameters for the stored procedure here
	@DBNumber char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @DBName varchar(50), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max)
	
	set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@DBNumber)
				
	set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N''.dbo.DemographicIn_MissingDDA ''
	exec sp_executesql @SQLTruncate


	set @sqlInsert= N''insert into '' + QuoteName(@DBName) + N''.dbo.DemographicIn_MissingDDA
					SELECT Pan, [Inst ID], CS, [Prim DDA], [1st DDA], [2nd DDA], [3rd DDA], [4th DDA], [5th DDA], [Address #1], [Address #2], [City ], ST, Zip, [First], [Last], MI, [First2], [Last2], MI2, [First3], [Last3], MI3, [First4], [Last4], MI4, SSN, [Home Phone], [Work Phone], DI.TipFirst, TipNumber, NA1, NA2, NA3, NA4, NA5, NA6, LastName
					FROM '' + QuoteName(@DBName) + N''.dbo.DemographicIn di
					join dbo.TipFirstReference tf on di.pan like rtrim(tf.bin) + ''''%''''
					where tf.tipfirst  = '''''' + @DBNumber + N'''''' and tf.TypeCard = ''''D'''' and IsActive = 1 
					AND 
					(
					(
					LEN(RTRIM(LTRIM([Prim DDA]))) = 0
					AND
					LEN(RTRIM(LTRIM([1st DDA]))) = 0 
					AND
					LEN(RTRIM(LTRIM([2nd DDA]))) = 0 
					AND
					LEN(RTRIM(LTRIM([3rd DDA]))) = 0 
					AND
					LEN(RTRIM(LTRIM([4th DDA]))) = 0 
					AND
					LEN(RTRIM(LTRIM([5th DDA]))) = 0 
					) 
					   OR (LEN(RTRIM(LTRIM([Prim DDA]))) = 0
					   OR [Prim DDA] is null  )  
					)
					ORDER BY LEN(RTRIM(LTRIM([Last]))) ''
	exec sp_executesql @sqlInsert

--------delete the bad recs from demographicIn
	set @sqlDelete= N''Delete From '' + QuoteName(@DBName) + N''.dbo.DemographicIn where pan in (Select pan from '' + QuoteName(@DBName) + N''.dbo.DemographicIn_missingDDA) ''
	exec sp_executesql @sqlDelete

END
' 
END
GO
