USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[pRemoveApostrophes]    Script Date: 01/07/2011 13:14:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pRemoveApostrophes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pRemoveApostrophes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pRemoveApostrophes]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pRemoveApostrophes] @TipFirst char(3)
AS

declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlupdate=N''Update '' + QuoteName(@DBName) + N'' .dbo.DemographicIn set [Address #1]=replace([Address #1],char(39), ''''''''), [Address #2]=replace([Address #2],char(39), ''''''''), CITY=replace(City,char(39), ''''''''),First=replace(First,char(39), ''''''''), Last=replace(Last,char(39), '''''''') ''
		exec sp_executesql @SQLUpdate
		
' 
END
GO
