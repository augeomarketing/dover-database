USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_cc605ProcessDemographics]    Script Date: 07/26/2013 13:56:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cc605ProcessDemographics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_cc605ProcessDemographics]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cc605ProcessDemographics]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_cc605ProcessDemographics]
AS

-- S Blanchette 7/2013 Added the update section.
-- S Blanchette 2/2015 Remove the update section and
--                     Truncate table cc605Demog before bringing in import data since we get a full file every month.
--						Closed cards are staying in the cc605Demog file and causing address problems.
-- S Blanchette 2/2015 Added BIN exclusion

-- Update
/* update cc605Demog
set dim_cc605demog_address1 = imp.dim_cc605demogimport_address1
	, dim_cc605demog_address2 = imp.dim_cc605demogimport_address2
	, dim_cc605demog_city = imp.dim_cc605demogimport_city
	, dim_cc605demog_first = imp.dim_cc605demogimport_first
	, dim_cc605demog_homephone =RIGHT(RewardsNow.dbo.ufn_RemoveAlpha(imp.dim_cc605demogimport_homephone), 10) 
	, dim_cc605demog_last = imp.dim_cc605demogimport_last
	, dim_cc605demog_ssn = RewardsNow.dbo.ufn_RemoveAlpha(imp.dim_cc605demogimport_ssn)
	, dim_cc605demog_st = imp.dim_cc605demogimport_st
	, dim_cc605demog_zip = RewardsNow.dbo.ufn_RemoveAlpha(imp.dim_cc605demogimport_zip)
FROM
	cc605DemogImport imp
LEFT OUTER JOIN
	cc605Demog dmg
	ON imp.dim_cc605demogimport_pan = dmg.dim_cc605demog_pan
WHERE
	dmg.dim_cc605demog_pan IS not NULL
*/

Truncate table cc605Demog

--ADDS

INSERT INTO cc605Demog
(
	dim_cc605demog_address1
	, dim_cc605demog_address2
	, dim_cc605demog_city
	, dim_cc605demog_first
	, dim_cc605demog_homephone
	, dim_cc605demog_last
	, dim_cc605demog_pan
	, dim_cc605demog_ssn
	, dim_cc605demog_st
	, dim_cc605demog_zip
	, dim_cc605demog_tipfirst
	, dim_cc605demog_cs
)
SELECT
	imp.dim_cc605demogimport_address1
	, imp.dim_cc605demogimport_address2
	, imp.dim_cc605demogimport_city
	, imp.dim_cc605demogimport_first
	, RIGHT(RewardsNow.dbo.ufn_RemoveAlpha(imp.dim_cc605demogimport_homephone), 10)
	, imp.dim_cc605demogimport_last
	, imp.dim_cc605demogimport_pan
	, RewardsNow.dbo.ufn_RemoveAlpha(imp.dim_cc605demogimport_ssn)
	, imp.dim_cc605demogimport_st
	, RewardsNow.dbo.ufn_RemoveAlpha(imp.dim_cc605demogimport_zip)
	, ''605''
	, ''C''
FROM
	cc605DemogImport imp
LEFT OUTER JOIN
	cc605Demog dmg
	ON imp.dim_cc605demogimport_pan = dmg.dim_cc605demog_pan
WHERE
	left(imp.dim_cc605demogimport_pan,6) !=''543031''
	and dmg.dim_cc605demog_pan IS NULL
	
	
--parse names as well as can be expected...

--INSERT RECORDS INTO DemographicsIn
INSERT INTO DemographicIn
(
	[Pan]
	, [Inst ID]
	, [CS]
	, [Prim DDA]
	, [1st DDA]
	, [2nd DDA]
	, [3rd DDA]
	, [4th DDA]
	, [5th DDA]
	, [Address #1]
	, [Address #2]
	, [City ]
	, [ST]
	, [Zip]
	, [First]
	, [Last]
	, [MI]
	, [First2]
	, [Last2]
	, [MI2]
	, [First3]
	, [Last3]
	, [MI3]
	, [First4]
	, [Last4]
	, [MI4]
	, [SSN]
	, [Home Phone]
	, [Work Phone]
	, [TipFirst]
	, [TipNumber]
	, [NA1]
	, [NA2]
	, [NA3]
	, [NA4]
	, [NA5]
	, [NA6]
	, [LastName]
)
SELECT
	[dim_cc605demog_pan]
	, [dim_cc605demog_instid]
	, [dim_cc605demog_cs]
	, [dim_cc605demog_primdda]
	, [dim_cc605demog_1stdda]
	, [dim_cc605demog_2nddda]
	, [dim_cc605demog_3rddda]
	, [dim_cc605demog_4thdda]
	, [dim_cc605demog_5thdda]
	, [dim_cc605demog_address1]
	, [dim_cc605demog_address2]
	, [dim_cc605demog_city]
	, [dim_cc605demog_st]
	, [dim_cc605demog_zip]
	, [dim_cc605demog_first]
	, [dim_cc605demog_last]
	, [dim_cc605demog_mi]
	, [dim_cc605demog_first2]
	, [dim_cc605demog_last2]
	, [dim_cc605demog_mi2]
	, [dim_cc605demog_first3]
	, [dim_cc605demog_last3]
	, [dim_cc605demog_mi3]
	, [dim_cc605demog_first4]
	, [dim_cc605demog_last4]
	, [dim_cc605demog_mi4]
	, [dim_cc605demog_ssn]
	, [dim_cc605demog_homephone]
	, [dim_cc605demog_workphone]
	, [dim_cc605demog_tipfirst]
	, [dim_cc605demog_tipnumber]
	, [dim_cc605demog_na1]
	, [dim_cc605demog_na2]
	, [dim_cc605demog_na3]
	, [dim_cc605demog_na4]
	, [dim_cc605demog_na5]
	, [dim_cc605demog_na6]
	, [dim_cc605demog_lastname]
FROM cc605Demog dmg
LEFT OUTER JOIN DemographicIn din
ON dmg.dim_cc605demog_pan = din.[Pan]
WHERE din.[Pan] IS NULL


' 
END
GO
