use [COOPWORK]
go

IF OBJECT_ID(N'usp_cc605PrepProcessingForMonth') IS NOT NULL
	DROP PROCEDURE usp_cc605PrepProcessingForMonth
GO

CREATE PROCEDURE usp_cc605PrepProcessingForMonth
	@enddate DATE = NULL
AS

SET @enddate = ISNULL(Rewardsnow.dbo.ufn_GetLastOfMonth(@enddate), Rewardsnow.dbo.ufn_GetLastOfPrevMonth(GETDATE()))

UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = '\\patton\ops\6CO\Input\605\FTP\Postfile_Rollup_' + CONVERT(VARCHAR(4), @enddate, 12) + '.txt'
WHERE PackagePath IN
(
	'\Package.Connections[Postfile_Rollup].Properties[ConnectionString]'
	, '\Package.Variables[User::TransactionRollupFile].Properties[Value]'
	, '\Package.Variables[User::TransactionFile].Properties[Value]'
)
AND ConfigurationFilter = '605'

UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = '\\patton\ops\6CO\Input\605\FTP\AFCUDemog_' + CONVERT(VARCHAR(10), @enddate, 112) + '.csv'
WHERE PackagePath IN 
(
	'\Package.Connections[Demographics].Properties[ConnectionString]'
	, '\Package.Variables[User::DemographicFile].Properties[Value]'
)
AND ConfigurationFilter = '605'


UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = CONVERT(varchar(10), @enddate, 101)
WHERE PackagePath IN
(
	'\Package.Variables[User::EndDateString].Properties[Value]'
	, '\Package.Variables[User::EndDate].Properties[Value]'
)
AND ConfigurationFilter = '605'

UPDATE Rewardsnow.dbo.[SSIS Configurations]
SET ConfiguredValue = CONVERT(varchar(10), Rewardsnow.dbo.ufn_GetFirstOfMonth(@enddate), 101)
WHERE PackagePath IN
(
	'\Package.Variables[User::StartDateString].Properties[Value]'
	, '\Package.Variables[User::StartDate].Properties[Value]'
)
AND ConfigurationFilter = '605'

