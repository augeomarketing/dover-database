USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spClearClosedCards]    Script Date: 04/27/2012 09:52:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spClearClosedCards]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spClearClosedCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spClearClosedCards]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spClearClosedCards] 
	-- Add the parameters for the stored procedure here
	@TipFirst CHAR(3),
	@Enddate char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @dbname char(50), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @SQLSelectInto nvarchar(1000)
	declare @datedeleted datetime
	set @datedeleted=@Enddate

	set @DBName=(SELECT rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
	where DBNumber=@TipFirst)

--	if OBJECT_ID(N''[tempdb].[dbo].[#tmpClosedCards]'') IS  NOT NULL

/* */

	set @SQLSelectInto = N''SELECT TIPNumber, acctid
						into dbo.[##tmpClosedCards]
						FROM '' + QuoteName(@DBName) + N''.dbo.affiliat 
						where AcctStatus=''''C'''' and TIPNUMBER in (select TIPNUMBER from '' + QuoteName(@DBName) + N'' .dbo.AFFILIAT where AcctStatus=''''A'''') ''
	exec sp_executesql @SQLSelectInto

/* */

	set @sqlinsert=N''INSERT INTO '' + QuoteName(@DBName) + N'' .dbo.AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
       		SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, (rtrim(AcctTypeDesc)+''''-CLOSED''''), lastname, ytdearned, custid, @datedeleted
			FROM '' + QuoteName(@DBName) + N''.dbo.affiliat 
			where Acctid in (select Acctid from dbo.[##tmpClosedCards]) ''
	exec sp_executesql @SQLInsert, N''@datedeleted datetime'', @datedeleted=@datedeleted

/* */

	set @SQLDelete = N''delete from '' + QuoteName(@DBName) + N'' .dbo.AFFILIAT
	where Acctid in (select Acctid from dbo.[##tmpClosedCards]) ''
	exec sp_executesql @SQLDelete

/* */

	--set @sqlinsert=N''INSERT INTO '' + QuoteName(@DBName) + N'' .dbo.Account_Reference_Deleted (TIPNumber, acctnumber, TipFirst, datedeleted)
 --      		SELECT TIPNumber, acctnumber, TipFirst, @datedeleted
	--		FROM '' + QuoteName(@DBName) + N''.dbo.Account_Reference 
	--		where acctnumber in (select Acctid from dbo.[##tmpClosedCards]) ''
	--exec sp_executesql @SQLInsert, N''@datedeleted datetime'', @datedeleted=@datedeleted
/* */

	-- Insert statements for procedure here
	set @SQLDelete = N''delete from '' + QuoteName(@DBName) + N'' .dbo.Account_Reference
	where acctnumber in (select Acctid from dbo.[##tmpClosedCards]) ''
	exec sp_executesql @SQLDelete

END

' 
END
GO
