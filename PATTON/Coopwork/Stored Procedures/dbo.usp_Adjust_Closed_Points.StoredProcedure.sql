USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[usp_Adjust_Closed_Points]    Script Date: 12/06/2010 11:28:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Adjust_Closed_Points]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Adjust_Closed_Points]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Adjust_Closed_Points]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 12/2010
-- Description:	Process Closed card file to give back closing points
-- =============================================
CREATE PROCEDURE [dbo].[usp_Adjust_Closed_Points] 
	-- Add the parameters for the stored procedure here
	@TipFirst char (3),
	@Trandate char(10) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLInsert=N''insert into '' + QuoteName(@DBName) + N'' .dbo.History
				select distinct newtip, '''' '''', @trandate, ''''IE'''', ''''1'''', pointsclosed, ''''Increase Earned'''', '''' '''', ''''1'''', ''''0''''
				from '' + QuoteName(@DBName) + N'' .dbo.ClosedCardProblemWork where newtip is not null''
exec sp_executesql @SQLInsert, N''@Trandate nchar(10)'',	@Trandate= @Trandate

set @SQLUpdate=N''update '' + QuoteName(@DBName) + N'' .dbo.customer
				set runavailable = runavailable + pointsclosed,
					runbalance = runbalance + pointsclosed
				from '' + QuoteName(@DBName) + N'' .dbo.customer join '' + QuoteName(@DBName) + N'' .dbo.ClosedCardProblemWork 
				on tipnumber = newtip
				where newtip is not null''
exec sp_executesql @SQLUpdate	
					
END
' 
END
GO
