USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerAndAffiliat_stage]    Script Date: 10/10/2012 15:05:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerAndAffiliat_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerAndAffiliat_stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage] @TipFirst char(3), @EndDate DateTime 
AS 

declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20)
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CUSTOMER DATA                                         */
/*                                                                            */
/******************************************************************************/
/* Update all of the Customer_Stage records to a status of ''C'' .... As new records are inserted they have a status of A from the Custin table, 
As records are udated, they already have  a status of A in Custin too 
Anything remaining means that this is the first month that they HAVEN''T appeared, their status is already C and they will be added 
to the Customer_Closed table with MonthEndToDelete set to Two months beyond the EndDate passed in
*/
/*************************************/
/* S Blanchette                      */
/* 11/2010                           */
/* SEB001                            */
/* delete from customer closed if    */
/* tip gets back to a status of ''''A''''  */
/*************************************/
 
--Get the dateToDelete (x months out based on Client.ClosedMonths)
declare @tmpDate datetime,@NumMonths smallint, @DateClosed datetime, @DateToDelete datetime
declare @DBName varchar(50), @sqlUpdate nvarchar(max), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max), @sqlDeclare nvarchar(max), @sqlif nvarchar(max)

set @NumMonths =(SELECT ClosedMonths from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst) 

set @DBName = (SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
				
select @tmpDate=dateadd(d,1,@Enddate)
select @tmpDate=dateadd(m,@NumMonths,@tmpDate)
select @DateToDelete=dateadd(d,-1,@tmpDate)   --usually 2 months beyond the Close date
set @DateClosed=@EndDate

set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.customer_stage set status=''''C'''' ''
	exec sp_executesql @sqlUpdate

set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Affiliat_stage set acctstatus=''''C'''' ''
	exec sp_executesql @sqlUpdate
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/* 
                                                                           */
set @sqlDeclare = N''declare CUSTIN_crsr cursor
					for select ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3
					from  '' + QuoteName(@DBName) + N''.dbo.CUSTIN
					order by tipnumber, status desc ''
	exec sp_executesql @sqlDeclare

/*                                                                            */
open CUSTIN_crsr
fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	set @sqlif = N''if (not exists (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.customer_stage where tipnumber='' + @tipnumber + N'') and '' + @tipnumber + N''is not null and left('' + @tipnumber + N'',1)<>'''' '''')
		begin
			-- Add to CUSTOMER TABLE
			INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Customer_stage (TIPNumber, Runavailable, Runbalance, Runredeemed, Status, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode,  HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3)
					values(@tipnumber, ''''0'''', ''''0'''', ''''0'''', @Status, LEFT(@tipnumber,3), right(@tipnumber,12), @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @ADDRESS1, @ADDRESS2, @ADDRESS4, rtrim(@City), rtrim(@state), rtrim(@zip), @HomePhone, @WorkPhone, cast(@DateAdded as datetime), @LastName, @Misc1, @Misc2, @Misc3) 
		end
		else
			begin
				-- Update CUSTOMER TABLE
				update '' + QuoteName(@DBName) + N''.dbo.customer_stage
						set Status=@Status, AcctName1=@NAMEACCT1, AcctName2=@NAMEACCT2, AcctName3=@NAMEACCT3, AcctName4=@NAMEACCT4, AcctName5=@NAMEACCT5, AcctName6=@NAMEACCT6, Address1=@Address1, Address2=@Address2, Address4=@Address4, City=rtrim(@City), State=rtrim(@STATE), Zipcode=rtrim(@zip), HomePhone=@HomePhone, WorkPhone=@WorkPhone, lastname=@lastname, misc1=@misc1, misc2=@misc2, misc3=@misc3
						where tipnumber=@tipnumber
			end ''
	exec sp_executesql @sqlif, N''@NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6 nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20)'',
								@NAMEACCT1 = @NAMEACCT1 , @NAMEACCT2=@NAMEACCT2, @NAMEACCT3=@NAMEACCT3, @NAMEACCT4=@NAMEACCT4, @NAMEACCT5=@NAMEACCT5, @NAMEACCT6=@NAMEACCT6, @STATUS=@STATUS, @TIPNUMBER=@TIPNUMBER, @ADDRESS1=@ADDRESS1, @ADDRESS2=@ADDRESS2, @ADDRESS4=@ADDRESS4, @CITY=@CITY,  @STATE= @STATE, @ZIP=@ZIP, @LASTNAME=@LASTNAME, @HOMEPHONE=@HOMEPHONE, @WORKPHONE=@WORKPHONE, @DATEADDED=@DATEADDED, @MISC1=@MISC1, @MISC2=@MISC2, @MISC3=@MISC3

	--load affilitat_stage
	set @sqlif = N''if (not exists (select acctid from '' + QuoteName(@DBName) + N''.dbo.affiliat_stage where acctid= '''''' + @ACCT_NUM + N'''''') and '''''' + @ACCT_NUM + N'''''' is not null and left('''''' + @ACCT_NUM + N'''''',1)<>'''' '''')
		begin
			-- Add to AFFILIAT TABLE
			INSERT INTO '' + QuoteName(@DBName) + N''.dbo.affiliat_stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned) 
			values(@ACCT_NUM, @tipnumber, ''''Debit'''', cast(@DateAdded as datetime), ''''A'''', ''''Debit Card'''', @lastname, ''''0'''') 
		end
	else
		begin
			-- Update AFFILIAT TABLE
			update '' + QuoteName(@DBName) + N''.dbo.AFFILIAT_stage	
					set lastname=@lastname, 
					acctstatus=@status 
					where acctid=@acct_num
		End ''
	exec sp_executesql @sqlif, N''@ACCT_NUM nvarchar(25), @TIPNUMBER nvarchar(15), @LASTNAME nvarchar(40),  @DATEADDED nvarchar(10), @STATUS nvarchar(1)'',
							@ACCT_NUM=@ACCT_NUM, @TIPNUMBER=@TIPNUMBER, @LASTNAME=@LASTNAME, @DATEADDED=@DATEADDED, @STATUS=@STATUS

goto Next_Record
Next_Record:
		fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
end
Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr

--Any Customer_Stage records that STILL have a status=''C'' get added to the Customer_Closed table if they don''t already exist in it
set @sqlInsert = N''INSERT into '' + QuoteName(@DBName) + N''.dbo.Customer_Closed 
	select Tipnumber, @DateClosed, @DateToDelete from '' + QuoteName(@DBName) + N''.dbo.Customer_stage where Status=''''C'''' AND tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.Customer_closed) ''
	exec sp_executesql @sqlInsert, N''@DateClosed datetime, @DateToDelete datetime'', @DateClosed=@DateClosed, @DateToDelete=@DateToDelete

--see if any of the closed customers have transactions for > DateToDelete...if so , bump their DateToDelete value up by a month
set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer_Closed set DateToDelete=dateadd(d,-1,dateadd(m,1,dateadd(d,1,DateToDelete))) where tipnumber in (select Tipnumber from '' + QuoteName(@DBName) + N''.dbo.History_stage where histdate >= @DateToDelete) ''
	exec sp_executesql @sqlUpdate, N''@DateToDelete datetime'', @DateToDelete=@DateToDelete

set @sqlDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.Customer_Closed
	where TipNumber in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.CUSTOMER_Stage where STATUS=''''A'''') ''
	exec sp_executesql @sqlDelete

--Update the statusDescription field
set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer_Stage set StatusDescription = rs.StatusDescription from '' + QuoteName(@DBName) + N''.dbo.Customer_Stage cs join Rewardsnow.dbo.Status rs on cs.status=rs.status ''
	exec sp_executesql @sqlUpdate

--Fix * in address2 field
/* Fix asterics in address 2  */
set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer_Stage 
					set Address2= (CASE WHEN Address2 LIKE ''''%[0-9a-Z]%'''' THEN Address2 
									ELSE REPLACE(Address2, ''''*'''', '''''''') 
									END ) '' --SEB002 
	exec sp_executesql @sqlUpdate

' 
END
GO
