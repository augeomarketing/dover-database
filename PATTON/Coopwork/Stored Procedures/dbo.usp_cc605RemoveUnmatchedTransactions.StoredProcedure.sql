USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605RemoveUnmatchedTransactions]    Script Date: 11/30/2010 16:24:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cc605RemoveUnmatchedTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_cc605RemoveUnmatchedTransactions]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605RemoveUnmatchedTransactions]    Script Date: 11/30/2010 16:24:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_cc605RemoveUnmatchedTransactions]
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO cc605CreditTransactionRawUnmatched
	(
		RecordType
		,CorporationNumber
		,AccountNumber
		,CardCode
		,PostingDate
		,TieBreaker
		,TransactionCode
		,ReasonCode
		,CardType
		,PostFlag
		,PostDate
		,PurchaseDate
		,LocalTransactionAmount
		,LocalTransactionAmountSign
		,OriginalTransactionAmount
		,OriginalTransactionAmountSign
		,OriginalCurrencyCode
		,OriginalCurrencyExponent
		,DestinationTransactionAmount
		,DestinationTransactionAmountSign
		,AdditionalData1
		,AdditionalData2
	)
	SELECT
		RecordType
		,CorporationNumber
		,AccountNumber
		,CardCode
		,PostingDate
		,TieBreaker
		,TransactionCode
		,ReasonCode
		,CardType
		,PostFlag
		,PostDate
		,PurchaseDate
		,LocalTransactionAmount
		,LocalTransactionAmountSign
		,OriginalTransactionAmount
		,OriginalTransactionAmountSign
		,OriginalCurrencyCode
		,OriginalCurrencyExponent
		,DestinationTransactionAmount
		,DestinationTransactionAmountSign
		,AdditionalData1
		,AdditionalData2
	FROM
		cc605CreditTransactionRaw trn
	LEFT OUTER JOIN cc605Demog dmg
		ON trn.AccountNumber = dmg.dim_cc605demog_pan
	WHERE
		dmg.dim_cc605demog_pan IS NULL

	DELETE trn
	FROM
		cc605CreditTransactionRaw trn
	LEFT OUTER JOIN cc605Demog dmg
		ON trn.AccountNumber = dmg.dim_cc605demog_pan
	WHERE
		dmg.dim_cc605demog_pan IS NULL
	
END

GO


