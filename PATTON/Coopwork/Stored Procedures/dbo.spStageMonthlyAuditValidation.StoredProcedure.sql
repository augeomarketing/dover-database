USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spStageMonthlyAuditValidation]    Script Date: 07/23/2012 15:05:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyAuditValidation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStageMonthlyAuditValidation] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		-- Insert statements for procedure here
	declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),@pointspurchasedDB numeric(9), @pointsbonusDB numeric(9), @pointsbonusCR numeric(9), @pointsadded numeric(9),
	 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9), @pointsreturnedDB numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9)

	declare @DBName varchar(100), @sqlUpdate nvarchar(max), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max), @sqlDeclare nvarchar(max), @sqlif nvarchar(max)

	set @DBName = (SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)
	
	set @sqlTruncate = N''truncate table '' + QuoteName(@DBName) + N''.dbo.Monthly_Audit_ErrorFile ''
	exec sp_executesql @sqlTruncate

	/*                                                                            */
	/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
	/*                                                                            */
	set @sqlDeclare = N''declare tip_crsr cursor
						for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonusDB, PointsBonusCR,pointsadded, pointsincreased, pointsredeemed, 
						pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased 
						from '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File ''
	exec sp_executesql @sqlDeclare
	/*                                                                            */
	open tip_crsr
	/*                                                                            */
	fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonusDB, @pointsbonusCR,@pointsadded, @pointsincreased, @pointsredeemed, 
	@pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased
	/******************************************************************************/	
	/*                                                                            */
	/* MAIN PROCESSING  VERIFICATION                                              */
	/*                                                                            */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
		begin	
		set @errmsg=NULL
		set @currentend=''0''
		set @sqlif = N''if @pointsend<>(select AdjustedEndingPoints from '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity where tipnumber=@tipnumber)
					begin
						set @errmsg=''''Ending Balances do not match''''
						set @currentend=(select AdjustedEndingPoints from '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity where tipnumber=@tipnumber)		
						INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Monthly_Audit_ErrorFile
       							values(  @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @PointsBonusCR,  @pointsadded,  @pointspurchasedDB, @pointsbonusDB, 
								@pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointssubtracted, @pointsreturnedDB, 
										 @pointsdecreased, @errmsg, @currentend )
					end ''
		exec sp_executesql @sqlif, N''@Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),@pointspurchasedDB numeric(9), @pointsbonusDB numeric(9), @pointsbonusCR numeric(9), @pointsadded numeric(9),
									 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9), @pointsreturnedDB numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9)'',
									@Tipnumber = @Tipnumber, @pointsbegin = @pointsbegin, @pointsend = @pointsend, @pointspurchasedCR = @pointspurchasedCR, @PointsBonusCR = @PointsBonusCR,  @pointsadded = @pointsadded,  @pointspurchasedDB = @pointspurchasedDB, @pointsbonusDB = @pointsbonusDB, 
								@pointsincreased = @pointsincreased, @pointsredeemed = @pointsredeemed, @pointsreturnedCR = @pointsreturnedCR, @pointssubtracted = @pointssubtracted, @pointsreturnedDB = @pointsreturnedDB, 
										 @pointsdecreased = @pointsdecreased, @errmsg = @errmsg, @currentend = @currentend
		
	goto Next_Record
	Next_Record:
			fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonusDB, @pointsbonusCR,@pointsadded, @pointsincreased, @pointsredeemed, 
			@pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased
					 
		end
	Fetch_Error:
	close  tip_crsr
	deallocate  tip_crsr

END
' 
END
GO
