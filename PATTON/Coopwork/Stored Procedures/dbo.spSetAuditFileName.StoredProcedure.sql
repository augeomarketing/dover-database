USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spSetAuditFileName]    Script Date: 01/18/2011 10:28:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetAuditFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetAuditFileName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetAuditFileName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 1/ 2011
-- Description:	To handle Vesdia and Access
-- =============================================
CREATE PROCEDURE [dbo].[spSetAuditFileName] 
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output, 
	@ConnectionString nchar(100) output 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	declare @Filename char(60), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(4), @endingDate char(10)

	set @endingDate=(select top 1 datein from DateforAudit)
	-- Section to put together the current date.  Handle problem with date returning a one position month and day

	--
	set @workmonth=left(@endingdate,2)
	set @workyear=right(@endingdate,2)

	set @currentdate=@workmonth + @workyear

	set @filename=''M'' + @TipPrefix + @currentdate + ''.xls''
	if  @TipPrefix=''609''
		Begin
			set @newname=''O:\6CO\Output\AuditFiles\AuditTCM.bat '' + @filename
			set @ConnectionString=''O:\6CO\Output\AuditFiles\'' + @filename
		End
	else
		Begin
			set @newname=''O:\6CO\Output\AuditFiles\AuditM.bat '' + @filename
			set @ConnectionString=''O:\6CO\Output\AuditFiles\'' + @filename
		END

END
' 
END
GO
