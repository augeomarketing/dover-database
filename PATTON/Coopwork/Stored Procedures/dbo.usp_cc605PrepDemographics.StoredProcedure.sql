USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605PrepDemographics]    Script Date: 12/13/2010 15:55:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cc605PrepDemographics]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_cc605PrepDemographics]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605PrepDemographics]    Script Date: 12/13/2010 15:55:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_cc605PrepDemographics]
as
insert into cc605demogimporthistory
(
	sid_cc605demogimport_id
	, dim_cc605demogimport_address1
	, dim_cc605demogimport_address2
	, dim_cc605demogimport_city 
	, dim_cc605demogimport_first
	, dim_cc605demogimport_homephone
	, dim_cc605demogimport_last
	, dim_cc605demogimport_pan
	, dim_cc605demogimport_ssn
	, dim_cc605demogimport_st
	, dim_cc605demogimport_zip
)
select 
	sid_cc605demogimport_id
	, dim_cc605demogimport_address1
	, dim_cc605demogimport_address2
	, dim_cc605demogimport_city 
	, dim_cc605demogimport_first
	, dim_cc605demogimport_homephone
	, dim_cc605demogimport_last
	, dim_cc605demogimport_pan
	, dim_cc605demogimport_ssn
	, dim_cc605demogimport_st
	, dim_cc605demogimport_zip
from
	cc605demogimport
	
delete from cc605demogimport

GO


