USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spGetMissingDDA]    Script Date: 08/22/2012 09:23:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetMissingDDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetMissingDDA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetMissingDDA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 8/2012
-- Description:	Find Debit card records that are missing DDA
-- =============================================
CREATE PROCEDURE [dbo].[spGetMissingDDA] @TipFirst char(3)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @DBName varchar(50), @sqlUpdate nvarchar(max), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max), @sqlDeclare nvarchar(max), @sqlif nvarchar(max)

	set @DBName = (SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
	
    -- Insert statements for procedure here
	set @sqlTruncate=N''truncate table '' + QuoteName(@DBName) + N''.dbo.DemographicIn_MissingDDA ''
	exec sp_executesql @sqlTruncate

	set @sqlInsert=N''insert into '' + QuoteName(@DBName) + N''.dbo.DemographicIn_MissingDDA
		SELECT     right(rtrim(Pan),4) as Pan, [Inst ID] as [INSID], CS, [Prim DDA], [1st DDA], [2nd DDA], [3rd DDA], [4th DDA], [5th DDA], [Address #1], [Address #2], [City ], ST, Zip, [First], [Last], MI, [First2], [Last2], MI2, [First3], [Last3], MI3, [First4], [Last4], MI4, SSN, [Home Phone], [Work Phone], DI.TipFirst, TipNumber, NA1, NA2, NA3, NA4, NA5, NA6, LastName
		FROM '' + QuoteName(@DBName) + N''.dbo.DemographicIn DI join dbo.TipFirstReference tfr on rtrim(pan)like rtrim(tfr.bin)+''''%''''
		WHERE  tfr.TypeCard = ''''D''''
		AND 
		(
		(
		(LEN(RTRIM(LTRIM([Prim DDA]))) = 0 or [Prim DDA] is null)
		AND
		(LEN(RTRIM(LTRIM([1st DDA]))) = 0 or [1st DDA] is null)
		AND
		(LEN(RTRIM(LTRIM([2nd DDA]))) = 0 or [2nd DDA] is null)
		AND
		(LEN(RTRIM(LTRIM([3rd DDA]))) = 0 or [3rd DDA] is null)
		AND
		(LEN(RTRIM(LTRIM([4th DDA]))) = 0 or [4th DDA] is null)
		AND
		(LEN(RTRIM(LTRIM([5th DDA]))) = 0 or [5th DDA] is null)
		) 
		  or (LEN(RTRIM(LTRIM([Prim DDA]))) = 0 or [Prim DDA] is null )  
		)
		ORDER BY LEN(RTRIM(LTRIM([Last]))) ''
	exec sp_executesql @sqlinsert

	--------delete the bad recs from demographicIn
	set @sqlDelete=N''delete from '' + QuoteName(@DBName) + N''.dbo.DemographicIn
		where Pan in (
						SELECT Pan
						FROM '' + QuoteName(@DBName) + N''.dbo.DemographicIn DI join dbo.TipFirstReference tfr on rtrim(pan)like rtrim(tfr.bin)+''''%''''
						WHERE  tfr.TypeCard = ''''D''''
						AND 
						(
						(
						(LEN(RTRIM(LTRIM([Prim DDA]))) = 0 or [Prim DDA] is null)
						AND
						(LEN(RTRIM(LTRIM([1st DDA]))) = 0 or [1st DDA] is null)
						AND
						(LEN(RTRIM(LTRIM([2nd DDA]))) = 0 or [2nd DDA] is null)
						AND
						(LEN(RTRIM(LTRIM([3rd DDA]))) = 0 or [3rd DDA] is null)
						AND
						(LEN(RTRIM(LTRIM([4th DDA]))) = 0 or [4th DDA] is null)
						AND
						(LEN(RTRIM(LTRIM([5th DDA]))) = 0 or [5th DDA] is null)
						) 
						  or (LEN(RTRIM(LTRIM([Prim DDA]))) = 0 or [Prim DDA] is null )  
						)
					) ''
	exec sp_executesql @sqldelete

END

' 
END
GO
