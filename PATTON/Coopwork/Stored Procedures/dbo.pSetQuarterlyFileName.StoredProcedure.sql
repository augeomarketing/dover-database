USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[pSetQuarterlyFileName]    Script Date: 04/22/2015 15:33:21 ******/
DROP PROCEDURE [dbo].[pSetQuarterlyFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pSetQuarterlyFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

--SEB 4/2015 added case to handle 213

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='Q' + @TipPrefix + @currentdate + '.xls'
 
set @newname= Case @TipPrefix
				when '213' then 'O:\6CO\Output\QuarterlyFiles\QTR213.bat ' + @filename
				else 'O:\6CO\Output\QuarterlyFiles\QTR.bat ' + @filename
				end

set @ConnectionString='O:\6CO\Output\QuarterlyFiles\' + @filename
GO
