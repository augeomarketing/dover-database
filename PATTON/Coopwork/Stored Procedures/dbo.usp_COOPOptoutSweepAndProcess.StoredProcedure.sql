USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_COOPOptoutSweepAndProcess]    Script Date: 07/31/2013 11:23:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_COOPOptoutSweepAndProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_COOPOptoutSweepAndProcess]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_COOPOptoutSweepAndProcess]    Script Date: 07/31/2013 11:23:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		S Blanchette .
-- Create date: 3/2012
-- Description:	To get opt out request from LRC and Process
-- =============================================
CREATE PROCEDURE [dbo].[usp_COOPOptoutSweepAndProcess] 
	-- Add the parameters for the stored procedure here
	
AS

Declare @TipFirst varchar(3)
 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    /* Step through DBProcessInfo to get COOP TipFirst         */
    DECLARE cDBNumber CURSOR FOR
	SELECT  distinct rtrim(DBNumber) from RewardsNow.dbo.DBProcessInfo
	where dbnumber like '6%' and dbnumber not in ('600', '6EB', '651', '653') and sid_FiProdStatus_statuscode in ('P', 'V') and DBAvailable = 'Y'
	OPEN cDBNumber 
	FETCH NEXT FROM cDBNumber INTO @TipFirst

	if @@FETCH_STATUS = 1
		goto Fetch_Error_Number
	/*                                                                            */
	WHILE (@@FETCH_STATUS=0)
		BEGIN
		
			declare @Tipnumber char(15)
			
			exec rewardsnow.dbo.usp_LRCOptOutUpdate @TipFirst, 'A'
			/*                                                                            */
			/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
			/*                                                                            */
			declare tip_crsr cursor
			for select tipnumber
			from rewardsnow.dbo.optoutTracking
			where tipprefix=@tipfirst and optoutposted is null
			/*                                                                            */
			open tip_crsr
			/*                                                                            */
			fetch tip_crsr into @Tipnumber
			/******************************************************************************/	
			/*                                                                            */
			/* MAIN PROCESSING  VERIFICATION                                              */
			/*                                                                            */
			if @@FETCH_STATUS = 1
				goto Fetch_Error
			/*                                                                            */
			while @@FETCH_STATUS = 0
				begin	
					exec COOPwork.dbo.spOptOutControlEntry @Tipnumber
					
					update RewardsNow.dbo.OPTOUTTracking
					set optoutposted = GETDATE()
					where TIPNUMBER = @Tipnumber
					
					goto Next_Record

				Next_Record:
				fetch tip_crsr into @Tipnumber
				end

				Fetch_Error:
				close  tip_crsr
				deallocate  tip_crsr


		goto Next_Record_Number

		Next_Record_Number:
			FETCH FROM cDBNumber INTO @TipFirst
		end

		Fetch_Error_Number:
		close  cDBNumber
		deallocate  cDBNumber

END


GO


