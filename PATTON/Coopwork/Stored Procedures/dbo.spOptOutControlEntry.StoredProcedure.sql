USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spOptOutControlEntry]    Script Date: 01/23/2013 14:33:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutControlEntry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spOptOutControlEntry]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutControlEntry]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spOptOutControlEntry] @Tipnumber varchar(15)
As

Declare @Name varchar(40), @Nameout char(40), @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLSelect nvarchar(1000)

/* Get DBName    */
/*RWL 4/5/10 Fully qualified to Rewardsnow.dbo,.dbprocessinfo*/
set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=left(@TipNumber,3))
/*End change 4/5/10*/

/* Get Customer Name */
set @SQLSelect=N''set @Nameout=(select acctname1 from '' + QuoteName(@DBNAME) + N''.dbo.Customer where tipnumber=@Tipnumber)''
	exec sp_executesql @SQLSelect, N''@Nameout char(40) output, @Tipnumber char(15)'', @tipnumber=@tipnumber, @Nameout=@Name output

/* Get Institution ID */
/* set @InstitutionID=(select InstitutionID from TipFirstReference where Tipfirst=left(@Tipnumber,3))  Removed 1/2/07 SEB  Institution no longer valid */

/* Populate the Opt out table */
set @SQLInsert=N''Insert into OptOutControl (Tipnumber, Acctnumber, Name)
select Tipnumber, Acctid, @Name from '' + QuoteName(@DBNAME) + N''.dbo.Affiliat where tipnumber=@Tipnumber''
	exec sp_executesql @SQLInsert, N''@Name varchar(40), @Tipnumber varchar(15)'',  @Name=@name ,@tipnumber=@tipnumber

/* Perform delete functions on tables */
--print @DBName

exec spOptOutManuallyDeleteCustomer @Tipnumber, @DBName
' 
END
GO
