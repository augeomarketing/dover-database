USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605ProcessTransactions]    Script Date: 11/30/2010 16:24:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cc605ProcessTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_cc605ProcessTransactions]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605ProcessTransactions]    Script Date: 11/30/2010 16:24:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_cc605ProcessTransactions]
AS
	INSERT INTO TRANSDETAILHOLD
	(
		trandate
		, msgtype
		, pan
		, processingcode
		, sic
		, netid
		, amounttran
		, tipnumber
		, numberoftrans
	)
	SELECT
	--	* 
		convert(varchar(10), convert(datetime, PostDate, 101), 101)	as trandate
		, '0000' as msgtype
		, AccountNumber as pan
		, TransactionCode + ReasonCode as processingcode
		, SUBSTRING(AdditionalData1, 49,4) as sic
		, 'CCC' as netid
		, CAST(LocalTransactionAmount AS INT) as amounttran
		, null as tipnumber
		, 1 as numberoftrans
	FROM cc605CreditTransactionRaw
	where TransactionCode in ('005', '025')
	and ReasonCode in ('00')

GO


