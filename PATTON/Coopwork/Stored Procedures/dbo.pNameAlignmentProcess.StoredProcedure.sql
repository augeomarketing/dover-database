USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[pNameAlignmentProcess]    Script Date: 01/12/2011 10:17:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pNameAlignmentProcess]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pNameAlignmentProcess]
GO

USE [COOPWork]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[pNameAlignmentProcess] @TipFirst char(3)
AS  

SET NOCOUNT ON

DECLARE @DBName varchar(50)
DECLARE @SQLUpdate nvarchar(1000)
DECLARE @SQLSelect nvarchar(1000)
DECLARE @WrkTable AS TABLE
(
	MyID INT NOT NULL IDENTITY (1,1)
    , TipNumber NCHAR(15)
    , FullName VARCHAR(40)
    , MyRank VARCHAR(10)
)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo where DBNumber=@TipFirst)

IF OBJECT_ID('tempdb.dbo.#wrktab2') IS NOT NULL
	DROP TABLE #wrktab2

CREATE TABLE #wrktab2
(
	Tipnumber VARCHAR(15)
	, First VARCHAR(40)
	, Last VARCHAR(40)
	, MI VARCHAR(40)
)

set @sqlselect=
REPLACE(
'
	INSERT INTO #wrktab2 (Tipnumber, First, Last, MI) 
	select distinct tipnumber, ISNULL(First, '''') AS FIRST, ISNULL(Last, '''') AS LAST, ISNULL(MI, '''') AS MI from [<DBNAMEPATTON>].dbo.DemographicIn
	union select distinct tipnumber, ISNULL(First2, '''') AS FIRST, ISNULL(Last2, '''') AS LAST, ISNULL(MI2, '''') AS MI from [<DBNAMEPATTON>].dbo.DemographicIn
	union select distinct tipnumber, ISNULL(First3, '''') AS FIRST, ISNULL(Last3, '''') AS LAST, ISNULL(MI3, '''') AS MI from [<DBNAMEPATTON>].dbo.DemographicIn
	union select distinct tipnumber, ISNULL(First4, '''') AS FIRST, ISNULL(Last4, '''') AS LAST, ISNULL(MI4, '''') AS MI from [<DBNAMEPATTON>].dbo.DemographicIn
	order by First, Last, MI
'
, '<DBNAMEPATTON>', @dbname)

exec sp_executesql @SQLSelect

DELETE FROM #wrktab2 WHERE LTRIM(RTRIM(ISNULL(First, '') + ISNULL(Last, '') + ISNULL(MI, ''))) = ''
                                
                                
--Repair instances where First Name and Last Name are the same
update #wrktab2
set [First] = ''
where [First] = [Last]


--Populate table variable to get unique identifier (and also so we don�t have to process the name later)
INSERT INTO @WrkTable(TipNumber, FullName)
SELECT
	TIPNUMBER
    , CASE WHEN LTRIM(RTRIM(MI)) = '' THEN LEFT(LTRIM(RTRIM(FIRST)) + ' ' + LTRIM(RTRIM(LAST)), 40)
		ELSE LEFT(LTRIM(RTRIM(FIRST)) + ' ' + LTRIM(RTRIM(MI)) + ' ' +  LTRIM(RTRIM(LAST)), 40)
		END
FROM 
	#wrktab2
                
DROP TABLE #wrktab2

--Deduplicate names

DELETE @WRKTABLE FROM @WRKTABLE D
LEFT OUTER JOIN
	(
		SELECT TIPNUMBER, FULLNAME, MIN(MYID) AS MYID
		FROM @WRKTABLE
		GROUP BY TIPNUMBER, FULLNAME
	) U
ON
	D.TIPNUMBER = U.TIPNUMBER
	AND D.FULLNAME = U.FULLNAME
	AND D.MYID = U.MYID
WHERE
	U.MYID IS NULL

--Add position/ranking of name within group (tip number) so we can later pivot it and make it a wide record

SELECT MYID, 'NA' + CAST(RANK() OVER (PARTITION BY TipNumber ORDER BY (FullName)) AS VARCHAR) AS MYRANK
INTO #WrkTable
FROM @WrkTable

UPDATE vwt
SET MyRank = twt.MyRank
FROM @WrkTable vwt
INNER JOIN #WrkTable twt
	ON vwt.MyID = twt.MyID
	
DROP TABLE #WrkTable

--UPDATE @WrkTable
--SET MyRank = 'NA' + CAST(RANK() OVER (PARTITION BY TipNumber ORDER BY (FullName)) AS VARCHAR)

DELETE FROM cardname

-- Pivot data from selection to create a wide table 
--�Hard� table used again. See above comment�

INSERT INTO cardname(TIPNUMBER, NA1, NA2, NA3, NA4, NA5, NA6)
SELECT TipNumber, [NA1] AS NA1, [NA2] AS NA2, [NA3] AS NA3, [NA4] AS NA4, [NA5] AS NA5, [NA6] AS NA6
FROM
(SELECT TipNumber, FullName, MyRank FROM @WrkTable) PS
PIVOT (MIN (FullName) FOR MYRANK IN ([NA1], [NA2], [NA3], [NA4], [NA5], [NA6])
) AS PVT

SET @SQLUpdate = N'UPDATE [<dbname>].dbo.DemographicIn SET NA1 = C.NA1, NA2 = C.NA2, NA3 = C.NA3, NA4 = C.NA4, NA5 = C.NA5, NA6 = C.NA6 FROM [<dbname>].dbo.DemographicIn S INNER JOIN cardname C ON S.tipnumber = C.tipnumber'
SET @SQLUpdate = REPLACE(@SQLUpdate, '<dbname>', @DBName) 

exec sp_executesql @SQLUpdate

GO


--USE [COOPWork]
--GO

--/****** Object:  StoredProcedure [dbo].[pNameAlignmentProcess]    Script Date: 01/12/2011 10:17:42 ******/
--SET ANSI_NULLS OFF
--GO

--SET QUOTED_IDENTIFIER OFF
--GO

--CREATE Procedure [dbo].[pNameAlignmentProcess] @TipFirst char(3)
--as  

--declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLSelect nvarchar(1000)

--set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
--				where DBNumber=@TipFirst)
--/******************************************************/
--/* Section to get all names on same record            */
--/******************************************************/
--declare @NA1 nvarchar(40), @NA2 nvarchar(40), @NA3 nvarchar(40), @NA4 nvarchar(40), @NA5 nvarchar(40), @NA6 nvarchar(40), @tipnumber nchar(15), @First char(40), @last char(40), @MI char(1)
--declare @TotalName char(40)

----added this if statement to prevent blowups-09/08/08
--if exists (select * from sysobjects where [name] = 'wrktab2' and xtype = 'U')
--drop table dbo.wrktab2



--set @sqlselect=N'select distinct Tipnumber, First, Last, Mi into dbo.wrktab2 from ' + QuoteName(@DBName) + N' .dbo.DemographicIn order by tipnumber, last, first'
--		exec sp_executesql @SQLSelect

--delete from cardname

--update wrktab2
--set MI='' 
--where MI is null 

--insert into cardname (Tipnumber)
--select distinct Tipnumber
--from wrktab2
--where not exists (select * from cardname where tipnumber=wrktab2.tipnumber)

--update cardname
--set na1=case 
--		when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--		when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--		end
--from cardname, wrktab2
--where cardname.tipnumber=wrktab2.tipnumber and na1 is null

--update cardname
--set na2=case 
--		when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--		when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--		end
--from cardname, wrktab2
--where cardname.tipnumber=wrktab2.tipnumber and na2 is null and na1<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end

--update cardname
--set na3=case 
--		when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--		when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--		end
--from cardname, wrktab2
--where cardname.tipnumber=wrktab2.tipnumber and na3 is null and na1<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na2<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end

--update cardname
--set na4=case 
--		when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--		when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--		end
--from cardname, wrktab2
--where cardname.tipnumber=wrktab2.tipnumber and na4 is null and na1<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na2<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na3<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end

--update cardname
--set na5=case 
--		when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--		when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--		end
--from cardname, wrktab2
--where cardname.tipnumber=wrktab2.tipnumber and na5 is null and na1<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na2<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na3<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na4<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end

--update cardname
--set na6=case 
--		when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--		when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--		end
--from cardname, wrktab2
--where cardname.tipnumber=wrktab2.tipnumber and na6 is null and na1<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na2<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na3<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na4<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end
--							   and na5<>case 
--								when mi = ' ' then (rtrim(first) + ' ' + rtrim(Last))
--								when mi <> ' ' then (rtrim(first) + ' ' + rtrim(MI) + ' ' + rtrim(Last))
--								end


--/******************************************************/
--/* Section to remove duplicate names on same record   */
--/******************************************************/

--update Cardname
--set na2=null
--where na2=na1

--update Cardname
--set na3=null
--where na3=na1 or na3=na2

--update Cardname
--set na4=null
--where na4=na1 or na4=na2 or na4=na3

--update Cardname
--set na5=null
--where na5=na1 or na5=na2 or na5=na3 or na5=na4

--update Cardname
--set na6=null
--where na6=na1 or na6=na2 or na6=na3 or na6=na4 or na6=na5

--/******************************************************************************/
--/* Section to move names to the beginning of the name fields on same record   */
--/******************************************************************************/
--declare @count numeric(1,0)
--set @count=0

--while @count<5
--begin

--	update Cardname
--	set na1=na2, na2=na3, na3=na4, na4=na5, na5=na6, na6=null
--	where na1 is null or substring(na1,1,1) like ' '

--	update Cardname
--	set na2=na3, na3=na4, na4=na5, na5=na6, na6=null
--	where na2 is null or substring(na2,1,1) like ' '

--	update Cardname
--	set na3=na4, na4=na5, na5=na6, na6=null
--	where na3 is null or substring(na3,1,1) like ' '

--	update Cardname
--	set na4=na5, na5=na6, na6=null
--	where na4 is null or substring(na4,1,1) like ' '

--	update Cardname
--	set na5=na6, na6=null
--	where na5 is null or substring(na5,1,1) like ' '

--	set @count= @count + 1
--end

--/********************************************************************************/
--/*                                                      	                */
--/*                              	                             	        */
--/*  	Update names to the DemographicIn Table                                       */
--/*                                                       		  	*/
--/*                                                        			*/
--/********************************************************************************/
--set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.DemographicIn set ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA1=Cardname.NA1, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA2=Cardname.NA2, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA3=Cardname.NA3, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA4=Cardname.NA4, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA5=Cardname.NA5, ' + QuoteName(@DBName) + N' .dbo.DemographicIn.NA6=Cardname.NA6 from ' + QuoteName(@DBName) + N' .dbo.DemographicIn, Cardname where ' + QuoteName(@DBName) + N' .dbo.DemographicIn.tipnumber=Cardname.tipnumber'                 
--		exec sp_executesql @SQLUpdate
--GO


