USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605ClearCreditTransactions]    Script Date: 11/30/2010 16:22:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_cc605ClearCreditTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_cc605ClearCreditTransactions]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[usp_cc605ClearCreditTransactions]    Script Date: 11/30/2010 16:22:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_cc605ClearCreditTransactions]
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO cc605CreditTransactionRawHistory
	(
		RecordType
		,CorporationNumber
		,AccountNumber
		,CardCode
		,PostingDate
		,TieBreaker
		,TransactionCode
		,ReasonCode
		,CardType
		,PostFlag
		,PostDate
		,PurchaseDate
		,LocalTransactionAmount
		,LocalTransactionAmountSign
		,OriginalTransactionAmount
		,OriginalTransactionAmountSign
		,OriginalCurrencyCode
		,OriginalCurrencyExponent
		,DestinationTransactionAmount
		,DestinationTransactionAmountSign
		,AdditionalData1
		,AdditionalData2
	)
	SELECT
		RecordType
		,CorporationNumber
		,AccountNumber
		,CardCode
		,PostingDate
		,TieBreaker
		,TransactionCode
		,ReasonCode
		,CardType
		,PostFlag
		,PostDate
		,PurchaseDate
		,LocalTransactionAmount
		,LocalTransactionAmountSign
		,OriginalTransactionAmount
		,OriginalTransactionAmountSign
		,OriginalCurrencyCode
		,OriginalCurrencyExponent
		,DestinationTransactionAmount
		,DestinationTransactionAmountSign
		,AdditionalData1
		,AdditionalData2
	FROM
		cc605CreditTransactionRaw
		
	DELETE FROM cc605CreditTransactionRaw
	

    -- Insert statements for procedure here
END

GO


