USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spOptOutManuallyDeleteCustomer]    Script Date: 03/07/2012 15:51:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutManuallyDeleteCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spOptOutManuallyDeleteCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spOptOutManuallyDeleteCustomer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/***********************************/
/* S Blanchette 3/2012             */
/* added element names to insert on*/
/* Customer deleted                */
/* SEB001                          */
/***********************************/

CREATE Procedure [dbo].[spOptOutManuallyDeleteCustomer] @Tipnumber nvarchar(15), @DBName nvarchar(50)
as

Declare @SQLInsert nvarchar(2000), @SQLDelete nvarchar(1000), @SQLUpdate nvarchar(1000), @SQLIf nvarchar(1000),
		@TipFirst nvarchar(3), @DBName_RN1 nvarchar(50)
		
	--get the RN1 Database name
	
	set @TipFirst=left(@TipNumber,3)
	select @DBName_RN1 = ltrim(rtrim(DBNameNEXL))
	from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @TipFirst	

/* Set Customer Status*/
set @SQLUpdate=N''update ''+ QuoteName(@DBNAME) + N''.dbo.Customer set status=''''O'''' where tipnumber=@Tipnumber''
	exec sp_executesql @SQLUpdate, N''@Tipnumber char(15)'', @tipnumber=@tipnumber

/* SEB001 */ /* Insert into CustomerDeleted */
set @SQLInsert=N''Insert into ''+ QuoteName(@DBNAME) + N''.dbo.Customerdeleted (TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1 ,Address2, Address3, Address4 ,City, State, Zipcode, LastName,Status, StatusDescription ,HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt ,RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode ,Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted) 
		SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, getdate() 
		FROM ''+ QuoteName(@DBNAME) + N''.dbo.Customer where tipnumber=@Tipnumber''
	exec sp_executesql @SQLInsert, N''@Tipnumber char(15)'', @tipnumber=@tipnumber 

/* Insert into AffiliatDeleted */
set @SQLInsert=N''Insert into ''+ QuoteName(@DBNAME) + N''.dbo.Affiliatdeleted (TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, Datedeleted)
    SELECT TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, getdate() 
	FROM ''+ QuoteName(@DBNAME) + N''.dbo.affiliat where tipnumber=@Tipnumber''
	exec sp_executesql @SQLInsert, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

/* Insert into HistoryDeleted */
set @SQLInsert=N''Insert into ''+ QuoteName(@DBNAME) + N''.dbo.Historydeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
    SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, getdate() 
	FROM ''+ QuoteName(@DBNAME) + N''.dbo.History where tipnumber=@Tipnumber''
	exec sp_executesql @SQLInsert, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

	set @SQLIf=N''if exists(select * from '' + QuoteName(@DBName) + N''.dbo.sysobjects where xtype=''''u'''' and name = ''''Account_Reference_Deleted'''')
	Begin
		INSERT INTO  '' + QuoteName(@DBName) + N''.dbo.Account_Reference_Deleted (Tipnumber,Acctnumber,Tipfirst) select @Tipnumber,Acctnumber,left(@Tipnumber,3) as Tipfirst from   '' + QuoteName(@DBName) + N''.dbo.Account_Reference WHERE TIPNUMBER=@Tipnumber  
		DELETE '' + QuoteName(@DBName) + N''.dbo.Account_Reference WHERE TIPNUMBER=@Tipnumber
		
	End ''
	print @SQLIf
	exec sp_executesql @SQLIf, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

--PATTON SIDE
/* Delete from Affiliat */
set @SQLDelete=N''delete from ''+ QuoteName(@DBNAME) + N''.dbo.Affiliat 
		where  tipnumber=@Tipnumber''
	exec sp_executesql @SQLDelete, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

/* Delete from History */
set @SQLDelete=N''delete from ''+ QuoteName(@DBNAME) + N'' .dbo.History 
		where  tipnumber=@Tipnumber''
	exec sp_executesql @SQLDelete, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

/* Delete from Customer */
set @SQLDelete=N''delete from ''+ QuoteName(@DBNAME) + N''.dbo.Customer 
		where  tipnumber=@Tipnumber''
	exec sp_executesql @SQLDelete, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

--WEB SIDE
/* Delete from Affiliat */
set @SQLDelete=N''delete from [RN1].''+ QuoteName(@DBName_RN1) + N''.dbo.[1Security] 
		where  tipnumber=@Tipnumber''
	exec sp_executesql @SQLDelete, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

/* Delete from History */
set @SQLDelete=N''delete from [RN1].''+ QuoteName(@DBName_RN1) + N'' .dbo.Account 
		where  tipnumber=@Tipnumber''
	exec sp_executesql @SQLDelete, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

/* Delete from Customer */
set @SQLDelete=N''delete from [RN1]''+ QuoteName(@DBName_RN1) + N''.dbo.Customer 
		where  tipnumber=@Tipnumber''
	exec sp_executesql @SQLDelete, N''@Tipnumber varchar(15)'', @Tipnumber =@Tipnumber

' 
END
GO
