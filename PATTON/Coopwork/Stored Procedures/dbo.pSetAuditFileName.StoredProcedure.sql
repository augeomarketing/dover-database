USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[pSetAuditFileName]    Script Date: 02/08/2012 14:22:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetAuditFileName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetAuditFileName]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[pSetAuditFileName]    Script Date: 02/08/2012 14:22:54 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[pSetAuditFileName]  @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(60), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(4), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)
-- Section to put together the current date.  Handle problem with date returning a one position month and day

--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='M' + @TipPrefix + @currentdate + '.xls'
if  @TipPrefix='609'
	Begin
		set @newname='O:\6CO\Output\AuditFiles\AuditTC.bat ' + @filename
		set @ConnectionString='O:\6CO\Output\AuditFiles\' + @filename
	End
else
	Begin
		set @newname='O:\6CO\Output\AuditFiles\Audit.bat ' + @filename
		set @ConnectionString='O:\6CO\Output\AuditFiles\' + @filename
	END
GO


