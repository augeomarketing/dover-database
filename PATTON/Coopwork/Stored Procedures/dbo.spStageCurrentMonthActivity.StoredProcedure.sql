USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spStageCurrentMonthActivity]    Script Date: 07/23/2012 15:05:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageCurrentMonthActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageCurrentMonthActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageCurrentMonthActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2012
-- Description:	Determine current activity points
-- =============================================
CREATE PROCEDURE [dbo].[spStageCurrentMonthActivity] 
	-- Add the parameters for the stored procedure here
	
	@TipFirst char(3),
	@EndDateParm varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	Declare @EndDate DateTime 						--RDT 10/09/2006 
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )		--RDT 10/09/2006 

	declare @DBName varchar(100), @sqlUpdate nvarchar(max), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max), @sqlDeclare nvarchar(max), @sqlif nvarchar(max)

	set @DBName = (SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)
	
	set @sqlTruncate = N''truncate table '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity ''
	exec sp_executesql @sqlTruncate

	set @sqlInsert = N''INSERT into '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)	
						select tipnumber, RunAvailable,0 ,0 ,0 from '' + QuoteName(@DBName) + N''.dbo.Customer_Stage ''
	exec sp_executesql @sqlInsert
							
	/* Load the current activity table with increases for the current month         */
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity
						set increases=(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history_Stage where histdate>@enddate and ratio=''''1''''
											and '' + QuoteName(@DBName) + N''.dbo.History_Stage.Tipnumber = '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity.Tipnumber)
						where exists (select * from '' + QuoteName(@DBName) + N''.dbo.history_Stage where histdate>@enddate and ratio=''''1''''
											and '' + QuoteName(@DBName) + N''.dbo.History_Stage.Tipnumber = '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity.Tipnumber) ''
	exec sp_executesql @sqlUpdate, N''@EndDate DateTime'', @EndDate = @EndDate

	/* Load the current activity table with decreases for the current month         */
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity
						set decreases=(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history_Stage where histdate>@enddate and ratio=''''-1''''
											and '' + QuoteName(@DBName) + N''.dbo.History_Stage.Tipnumber = '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity.Tipnumber)
						where exists (select * from '' + QuoteName(@DBName) + N''.dbo.history_Stage where histdate>@enddate and ratio=''''-1''''
											and '' + QuoteName(@DBName) + N''.dbo.History_Stage.Tipnumber = '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity.Tipnumber) ''
	exec sp_executesql @sqlUpdate, N''@EndDate DateTime'', @EndDate = @EndDate


	/* Load the calculate the adjusted ending balance        */
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Current_Month_Activity
						set adjustedendingpoints=endingpoints - increases + decreases ''
	exec sp_executesql @sqlUpdate
		
END
' 
END
GO
