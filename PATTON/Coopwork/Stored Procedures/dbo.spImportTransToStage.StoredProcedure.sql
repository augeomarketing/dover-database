USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 09/18/2012 09:12:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransToStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/

/*********************************/
/* S Blanchette                  */
/* 11/2012                       */
/* Added were clause to insert   */
/* into history_stage            */
/*********************************/

/*  **************************************  */
CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst char(3)
AS 
Declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
Declare @DBName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get Database name                                      */
set @DBName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst ) 

/***************************** HISTORY_STAGE *****************************/
----  Insert TransStandard into History_stage 
 set @SQLStmt = N'' insert into '' + QuoteName(@DBName) + N''.dbo.history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select TFNO, Acct_num, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, ''''NEW'''', 0 From '' + QuoteName(@DBName) + N''.dbo.TransStandard where TranAmt <> 0 and TranAmt is not null ''
exec sp_executesql @SQLStmt

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	set @SQLStmt = N''update '' + QuoteName(@DBName) + N''.dbo.History_Stage
		set Overage = (H.Points * H.Ratio) - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM '' + QuoteName(@DBName) + N''.dbo.History_Stage H JOIN '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage A on H.Tipnumber = A.Tipnumber 
		and  A.AcctID = H.AcctID
		where A.YTDEarned + H.Points > @MaxPointsPerYear ''
	exec sp_executesql @SQLStmt, N''@MaxPointsPerYear decimal(9)'', @MaxPointsPerYear=@MaxPointsPerYear
End

/***************************** AFFILIAT_STAGE -ROLLUP BY CARD (AcctID)*****************************/
-- Update Affiliat_stage YTDEarned 
set @SQLStmt = N''SELECT A.ACCTID, A.Tipnumber, SUM(H.POINTS*H.Ratio) AS AcctIDPoints
				into '' + QuoteName(@DBName) + N''.dbo.AP
				FROM '' + QuoteName(@DBName) + N''.dbo.AFFILIAT_stage A INNER JOIN
                      '' + QuoteName(@DBName) + N''.dbo.HISTORY_stage H ON A.ACCTID = H.ACCTID AND A.TIPNUMBER = H.TIPNUMBER
				where H.SecID=''''NEW''''
				GROUP BY A.ACCTID, A.Tipnumber ''
exec sp_executesql @SQLStmt
				
---Join on the temp table and update 
set @SQLStmt = N''Update '' + QuoteName(@DBName) + N''.dbo.Affiliat_stage
				set YTDEarned=YTDEarned +  AP.AcctIDPoints
				FROM '' + QuoteName(@DBName) + N''.dbo.AFFILIAT_stage A INNER JOIN
                      '' + QuoteName(@DBName) + N''.dbo.AP AP ON A.ACCTID = AP.ACCTID AND A.TIPNUMBER = AP.TIPNUMBER ''
exec sp_executesql @SQLStmt

set @SQLStmt = N''Drop table '' + QuoteName(@DBName) + N''.dbo.AP ''
exec sp_executesql @SQLStmt
---?????? why 

set @SQLStmt = N''Update '' + QuoteName(@DBName) + N''.dbo.History_Stage 
					Set Points = Points - Overage where Points > Overage ''
exec sp_executesql @SQLStmt
	
/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
set @SQLStmt = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer_Stage Set RunAvaliableNew  = 0 where RunAvaliableNew  is null ''
exec sp_executesql @SQLStmt

set @SQLStmt = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer_Stage Set RunAvailable  = 0 where RunAvailable is null ''
exec sp_executesql @SQLStmt


set @SQLStmt = N''if exists (select * from dbo.sysobjects where id = object_id(N''''[dbo].[vw_histpoints_'' + @DBName + N'']'''') and OBJECTPROPERTY(id, N''''IsView'''') = 1)
drop view dbo.[vw_histpoints_'' + @DBName + N''] ''
exec sp_executesql @SQLStmt

/* Create View */
set @SQLStmt = ''Create view dbo.vw_histpoints_'' + @DBName + N'' as select tipnumber, sum(points*ratio) as points from '' + QuoteName(@DBName) + N''.dbo.history_stage where secid = ''''NEW''''group by tipnumber''
exec sp_executesql @SQLStmt

set @SQLStmt = N''Update '' + QuoteName(@DBName) + N''.dbo.customer_stage 
					Set RunAvailable  = RunAvailable + v.Points 
					From '' + QuoteName(@DBName) + N''.dbo.Customer_Stage C join dbo.vw_histpoints_'' + @DBName + N'' V on C.Tipnumber = V.Tipnumber ''
exec sp_executesql @SQLStmt

set @SQLStmt = N''Update '' + QuoteName(@DBName) + N''.dbo.customer_stage 
					Set RunAvaliableNew  = v.Points 
					From '' + QuoteName(@DBName) + N''.dbo.Customer_Stage C join dbo.vw_histpoints_'' + @DBName + N'' V on C.Tipnumber = V.Tipnumber ''
exec sp_executesql @SQLStmt

set @SQLStmt = N''drop view dbo.[vw_histpoints_'' + @DBName + N''] ''
exec sp_executesql @SQLStmt


' 
END
GO
