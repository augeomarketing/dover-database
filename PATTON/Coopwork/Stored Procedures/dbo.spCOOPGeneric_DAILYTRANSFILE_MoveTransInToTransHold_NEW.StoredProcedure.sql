USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_NEW]    Script Date: 01/26/2011 09:22:24 ******/
/*************************************************/
/* S Blanchette                                  */
/* 1/2011                                        */
/* SEB002                                        */
/* Modify to contain both Debit and Credit codes */
/*************************************************/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_NEW]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_NEW]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_NEW]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spCOOPGeneric_DAILYTRANSFILE_MoveTransInToTransHold_NEW]
AS 
Declare @Pan char(19), @msgtype char(4), @ProcessingCode char(6), @AmountTRAN decimal(12,0), @SIC char(4), @NETID char(3), @Trandate char(10), @Work1 char(4), @TERMID char(8), @ACCEPTORID char(15)
/*                                                                            */


delete from transdetailin	where sic=''6011'' 
/* SEB001 delete from transdetailin	where  processingcode not in (''000000'', ''002000'', ''200020'', ''200040'', ''500000'', ''500020'', ''500010'') */ 
/* SEB002 SEB001  delete from transdetailin	where  processingcode not in (''003000'', ''200030'', ''200000'', ''090000'', ''093000'') */
/* SEB002 */ delete from transdetailin	where  processingcode not in (''000000'', ''002000'', ''200020'', ''200040'', ''500000'', ''500020'', ''500010'', ''003000'', ''200030'', ''200000'', ''090000'', ''093000'')
delete from transdetailin	where  left(msgtype,2) not in (''02'',''04'')   

                                                                    
insert Into TransDetailHold (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID,  NUMBEROFTRANS, TERMID, ACCEPTORID,OrigDataElem )
	 select   Trandate, MSGTYPE, PAN, PROCESSINGCODE, SUM(AMOUNTTRAN) as AmountTran, SIC, NETID, Count(AMOUNTTRAN),TERMID, ACCEPTORID, OrigDataElem
	from TRANSDETAILIN
	Group By PAN, TranDate,MsgType,ProcessingCode,SIC,NETID, TERMID, ACCEPTORID, OrigDataElem' 
END
GO
