USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spSetStatementFileNameExpM]    Script Date: 01/18/2011 11:45:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetStatementFileNameExpM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetStatementFileNameExpM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetStatementFileNameExpM]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spSetStatementFileNameExpM] 
	-- Add the parameters for the stored procedure here
	@TipPrefix char (3), 
	@newname nchar(100)  output, 
	@ConnectionString nchar(100) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

	set @endingDate=(select top 1 datein from DateforAudit)

	-- Section to put together the current date.  Handle problem with date returning a one position month and day
	--
	set @workmonth=left(@endingdate,2)
	set @workyear=right(@endingdate,2)

	set @currentdate=@workmonth + @workyear

	set @filename=''S'' + @TipPrefix + @currentdate + ''.xls''
	 
	set @newname=''O:\6CO\Output\Statements\StatementExpM.bat '' + @filename
	set @ConnectionString=''O:\6CO\Output\Statements\'' + @filename

END
' 
END
GO
