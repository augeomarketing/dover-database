USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFileM]    Script Date: 05/16/2011 13:36:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFileM]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatementFileM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFileM]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--
-- S Blanchette 5/2012
-- SEB002
-- Fix calculation for Bonus reversals.  Currently adding points instead of subtracting.

-- S Blanchette 8/2012
-- Added PP and TP to points added

CREATE PROCEDURE [dbo].[spQuarterlyStatementFileM]  
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10), 
	@EndDateParm char(10), 
	@Tipfirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )    --RDT 10/09/2006

	declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)


	set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

	set @monthbegin=CONVERT( int , left(@StartDateParm,2))
	set @MonthBucket=''MonthBeg'' + @monthbegin

	/*******************************************************************************/
	/*******************************************************************************/
	/*                                                                             */
	/*          ISSUES WITH ADJUSTMENTS                                            */
	/*                                                                             */
	/*******************************************************************************/
	/*******************************************************************************/
	/* Load the statement file from the customer table  */
	set @sqlTruncate= N''Truncate Table '' + QuoteName(@DBName) + N'' .dbo.Quarterly_Statement_File ''
	exec sp_executesql @SQLTruncate

	/*set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip, acctnum, lastfour)
        		select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), zipcode, 
			from '' + QuoteName(@DBName) + N''.dbo.customer '' */
	set @SQLInsert=''INSERT INTO '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        		select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '''' '''' + rtrim(state) + '''' '''' + zipcode), zipcode 
			from '' + QuoteName(@DBName) + N''.dbo.customer ''
	Exec sp_executesql @SQLInsert
	 
	set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set acctnum=(select top 1 rtrim(acctid) from '' + QuoteName(@DBName) + N''.dbo.affiliat where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and acctstatus=''''A'''' ) ''
	Exec sp_executesql @SQLUpdate

	set @SQLUpdate=''Update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set lastfour=right(rtrim(acctnum),4) ''
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with CREDIT purchases   37       */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointspurchasedcr=
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''63'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''63'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with DEBIT purchases   67       */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointspurchaseddb=
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''67'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''67'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate


	/* Load the statmement file with bonuses            */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonuscr=
			(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode = ''''FJ'''')) 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode = ''''FJ'''')) ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* SEB001 */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonusDB= 
				(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and ((trancode like ''''B%'''' and trancode <> ''''BX'''')or trancode= ''''NW'''' or (trancode like ''''F%'''' and trancode not in (''''FJ'''', ''''F0'''', ''''G0'''', ''''F9'''', ''''G9'''') ))) 
				where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and ((trancode like ''''B%'''' and trancode <> ''''BX'''')or trancode= ''''NW'''' or (trancode like ''''F%'''' and trancode not in (''''FJ'''', ''''F0'''', ''''G0'''', ''''F9'''', ''''G9'''') ))) ''
		Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* SEB001 */
	/* SEB002 set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonusDB= pointsbonusDB - */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonusDB= pointsbonusDB + /* SEB002 */
				(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in (''''BX'''') ) 
				where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in (''''BX'''') ) ''
		Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* SEB001 add bonus for Vesdia and Access  */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonusMN= 
				(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in (''''F0'''', ''''G0'''') ) 
				where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in (''''F0'''', ''''G0'''') ) ''
		Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* SEB001 add bonus for Vesdia and Access  */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbonusMN= pointsbonusMN -
				(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in (''''F9'''', ''''G9'''') ) 
				where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in (''''F9'''', ''''G9'''') ) ''
		Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate


	/* Load the statmement file with plus adjustments    */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsadded=
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in(''''IE'''', ''''TP'''', ''''PP'''')) 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in(''''IE'''', ''''TP'''', ''''PP'''')) ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with total point increases */
	/* SEB001 */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded + pointsbonusMN''
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with redemptions          */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsredeemed=
			(select sum(points*ratio*-1) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''''R%'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''''R%'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsredeemed=pointsredeemed -
			(select sum(points*ratio) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''''DR'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''''DR'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with CREDIT returns            */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsreturnedcr=
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''33'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''33'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with DEBIT returns            */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsreturneddb=
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''37'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''37'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with minus adjustments    */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointssubtracted=
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''DE'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''DE'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with minus adjustments    */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointssubtracted= pointssubtracted +
			(select sum(points) from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''XP'''') 
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.history where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''''XP'''') ''
	Exec sp_executesql @SQLUpdate, N''@StartDate DateTime, @EndDate DateTime'', @StartDate= @StartDate, @EndDate=@EndDate

	/* Load the statmement file with total point decreases */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted ''
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with the Beginning balance for the Month */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsbegin=
			(select '' + Quotename(@MonthBucket) + N''from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table where tipnumber= '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber)
			where exists(select * from '' + QuoteName(@DBName) + N''.dbo.Beginning_Balance_Table where tipnumber='' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File.tipnumber)''
	exec sp_executesql @SQLUpdate

	/* Load the statmement file with ending points */
	set @SQLUpdate=N''update '' + QuoteName(@DBName) + N''.dbo.Quarterly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased ''
	exec sp_executesql @SQLUpdate


END
' 
END
GO
