USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFile_summary_M]    Script Date: 06/06/2013 09:54:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile_summary_M]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyStatementFile_summary_M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementFile_summary_M]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spMonthlyStatementFile_summary_M] 
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10), 
	@EndDateParm char(10), 
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 /*******************************************************************************/
/* Inserts summary record into summary table  */
/*******************************************************************************/


Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = @StartDateParm 
set @Enddate = @EndDateParm

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket=''MonthBeg'' + @monthbegin

set @SQLSelect='' delete  '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_Summary where EndDate=@EndDate''
Exec sp_executesql @SQLSelect, N''@EndDate DateTime'', @EndDate=@EndDate

set @SQLSelect=''insert into  '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_Summary
	Select 
	Count(*) as NumAccounts,
	sum(isnull(PointsBegin,0)) as PointsBegin,
	sum(isnull(PointsEnd,0)) as PointEnd,
	sum(isnull(PointsPurchasedCR,0)) as PointsPurchasedCR,
	sum(isnull(PointsBonusCR,0))as PointsBonusCR,
	sum(isnull(PointsAdded,0)) as PointsAdded,
	sum(isnull(PointsPurchasedDB,0))as PointsPurchasedDB,
	sum(isnull(PointsBonusDB,0))as PointsBonusDB,
	sum(isnull(PointsIncreased,0)) as PointsIncreased,	
	sum(isnull(PointsRedeemed,0)) as PointsRedeemed,
	sum(isnull(PointsReturnedCR,0)) as PointsReturnedCR,
	sum(isnull(PointsReturnedDB,0)) as PointsReturnedDB,
	sum(isnull(PointsSubtracted,0)) as PointsSubtracted,
	sum(isnull(PointsDecreased,0)) as PointsDecreased,
	@EndDate as Enddate,
	isnull(sum(PointsBonusMN),0) as PointsBonusMN 
	from  '' + QuoteName(@DBName) + N''.dbo.Monthly_Statement_File''
		
Exec sp_executesql @SQLSelect, N''@EndDate DateTime'', @EndDate = @EndDate
END

' 
END
GO
