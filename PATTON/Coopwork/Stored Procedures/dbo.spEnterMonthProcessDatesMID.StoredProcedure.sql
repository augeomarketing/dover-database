USE [COOPWork]
GO
/****** Object:  StoredProcedure [dbo].[spEnterMonthProcessDatesMID]    Script Date: 09/06/2011 08:45:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEnterMonthProcessDatesMID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spEnterMonthProcessDatesMID]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEnterMonthProcessDatesMID]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[spEnterMonthProcessDatesMID] @BeginDate nchar(10), @EndDate nchar(10)
AS

declare @monthbegin nchar(2)

set @monthbegin=left(@begindate,2)

if right(@monthbegin,1)=''/''
	begin
		set @begindate=''0'' + right(rtrim(@begindate),9)
	end

set @monthbegin=left(@EndDate,2)

if right(@monthbegin,1)=''/''
	begin
		set @EndDate=''0'' + right(rtrim(@EndDate),9)
	end

if not exists (select * from dbo.ProcessingDates where RecType=''MID'') 
Begin
		insert into dbo.ProcessingDates (RecType)
		values(''MID'')  
end

update dbo.ProcessingDates
set DataMonthBeginDate=@begindate, DataMonthEndDate=@EndDate
where RecType=''MID''


' 
END
GO
