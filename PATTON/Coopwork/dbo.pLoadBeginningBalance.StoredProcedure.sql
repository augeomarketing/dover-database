SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pLoadBeginningBalance] @MonthBeginDate nchar(10), @tipFirst char(3)
AS

Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @DBName varchar(50)

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

--set @monthfix=CONVERT( int , left(@MonthBeginDate,2)) + '1'
set @MonthBegin=CONVERT( int , left(@MonthBeginDate,2)) + '1'

--set @MonthBegin=@Monthfix

/* if CONVERT( int , @MonthBegin)<'10' 
	begin
	set @monthbegin='0' + left(@monthbegin,1)
	end	
else  */
	if CONVERT( int , @MonthBegin)='13' 
		begin
		set @monthbegin='1'
		end	

set @MonthBucket='MonthBeg' + @monthbegin

set @SQLUpdate=N'update '+ Quotename(@DBName) + N'.dbo.Beginning_Balance_Table set '+ Quotename(@MonthBucket) + N'= 
		(select pointsend from '+ Quotename(@DBName) + N'.dbo.Monthly_Statement_File where tipnumber= '+ Quotename(@DBName) + N'.dbo.Beginning_Balance_Table.tipnumber) 
		where exists(select * from '+ Quotename(@DBName) + N'.dbo.Monthly_Statement_File where tipnumber='+ Quotename(@DBName) + N'.dbo.Beginning_Balance_Table.tipnumber)'

set @SQLInsert=N'insert into '+ Quotename(@DBName) + N'.dbo.Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') 
		select tipnumber, pointsend from '+ Quotename(@DBName) + N'.dbo.Monthly_Statement_File 
		where not exists(select * from '+ Quotename(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber='+ Quotename(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert
GO
