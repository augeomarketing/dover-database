SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IDBRewardsInput](
	[ACN] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[CardNum] [varchar](255) NULL,
	[CardHolder] [varchar](255) NULL,
	[Add1] [varchar](255) NULL,
	[Add2] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[St] [varchar](255) NULL,
	[Zip] [varchar](255) NULL,
	[Country] [varchar](255) NULL,
	[ForFlg] [varchar](255) NULL,
	[Activated] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
