USE [COOPWork]
GO
--Increase size of phone number import field to accommodate non-alphanumeric characters (stripped later)
ALTER TABLE cc605demogimport ALTER COLUMN dim_cc605demogimport_homephone VARCHAR(20)
GO
--Increase size of phone number import field to accommodate non-alphanumeric characters (stripped later)
ALTER TABLE cc605demogimporthistory ALTER COLUMN dim_cc605demogimport_homephone VARCHAR(20)
GO


USE [605ActorsFCUConsumer]
GO
--Add Index to Customer 
CREATE INDEX ix_CustIN_AcctNum_Includes ON dbo.CustIn (Acct_num)
INCLUDE (TIPNUMBER, SEGCODE, DATEADDED, LASTNAME, STATUS)
GO

CREATE INDEX ix_CustIN_TipNumber_Includes ON dbo.CustIn(TipNumber)
INCLUDE (NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, ADDRESS1, ADDRESS2, ADDRESS4, City, state, zip, STATUS, HomePhone, WorkPhone, DateAdded, LastName, Misc1, Misc2, Misc3)
GO

