USE [COOPWork];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[ErrorCheckPanOverride] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[ErrorCheckPanOverride]([sid_errorcheckpanoverride_id], [dim_errorcheckpanoverride_bin], [sid_errorcheckoverridetype_id], [dim_errorcheckpanoverride_created], [dim_errorcheckpanoverride_lastmodified])
SELECT 1, N'463688', 2, '20111110 15:16:43.547', '20111110 15:16:43.547' UNION ALL
SELECT 2, N'583901', 2, '20111110 15:17:47.110', '20111110 15:17:47.110' UNION ALL
SELECT 3, N'551342', 2, '20111110 15:17:56.540', '20111110 15:17:56.540'
COMMIT;
RAISERROR (N'[dbo].[ErrorCheckPanOverride]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[ErrorCheckPanOverride] OFF;

