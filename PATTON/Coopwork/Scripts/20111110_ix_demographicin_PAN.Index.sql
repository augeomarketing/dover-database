USE [COOPWork]
GO

/****** Object:  Index [IX_DemographicIn_Pan]    Script Date: 11/10/2011 16:10:19 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DemographicIn]') AND name = N'IX_DemographicIn_Pan')
DROP INDEX [IX_DemographicIn_Pan] ON [dbo].[DemographicIn] WITH ( ONLINE = OFF )
GO

USE [COOPWork]
GO

/****** Object:  Index [IX_DemographicIn_Pan]    Script Date: 11/10/2011 16:10:19 ******/
CREATE NONCLUSTERED INDEX [IX_DemographicIn_Pan] ON [dbo].[DemographicIn] 
(
	[Pan] ASC
)
INCLUDE ( [Address #1],
[City ],
[ST],
[Zip],
[First],
[SSN],
[Last]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

