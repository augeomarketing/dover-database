USE [COOPWork];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[ErrorCheckOverrideType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[ErrorCheckOverrideType]([sid_errorcheckoverridetype_id], [dim_errorcheckoverridetype_description], [dim_errorcheckoverridetype_created], [dim_errorcheckoverridetype_lastmodified])
SELECT 1, N'Bypass PAN Validation', '20111110 15:14:00.300', '20111110 15:14:00.300' UNION ALL
SELECT 2, N'Bypass SSN Validation', '20111110 15:14:16.257', '20111110 15:14:16.257' UNION ALL
SELECT 3, N'Bypass Address1 Validation', '20111110 15:14:31.157', '20111110 15:14:31.157' UNION ALL
SELECT 4, N'Bypass City Validation', '20111110 15:14:44.653', '20111110 15:14:44.653' UNION ALL
SELECT 5, N'Bypass State Validation', '20111110 15:14:55.000', '20111110 15:14:55.000' UNION ALL
SELECT 6, N'Bypass Name Validation', '20111110 15:15:00.147', '20111110 15:15:00.147'
COMMIT;
RAISERROR (N'[dbo].[ErrorCheckOverrideType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[ErrorCheckOverrideType] OFF;

