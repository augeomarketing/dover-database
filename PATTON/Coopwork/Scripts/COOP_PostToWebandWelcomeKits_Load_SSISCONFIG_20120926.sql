USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'COOP_PostToWeb', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{17B7A28C-C735-49CF-AAFC-B6FA36D19746}236722-SQLCLUS2\RN.RewardsNow1;Auto Translate=False;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'COOP_PostToWeb', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{0F98E1BC-A712-42C2-BC66-C555B01B6BE7}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'COOP_PostToWeb', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{98651CA4-2F2D-4BFC-8D35-13D303CD6744}236722-SQLCLUS2\RN.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'COOP_WelcomeKits', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-WelcomeKits-{0C9C4B7A-03BB-444E-83C8-2EFEB4A67DBA}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'COOP_WelcomeKits', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-WelcomeKits-{1CA68226-F61D-410D-97B4-AE191563C912}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'COOP_WelcomeKits', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-WelcomeKits-{F2237D7D-CB4D-428B-A1E6-D81220BF2783}236722-SQLCLUS2\RN.COOPWork;Auto Translate=False;', N'\Package.Connections[COOPWork].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

