SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pDemographicin_ClearAndBackup] @DataDate varchar(10)
as


--truncate table DemographicIn_Previous 

insert into DemographicIn_Previous (Pan, [Inst ID], CS, [Prim DDA], [1st DDA], [2nd DDA], [3rd DDA], [4th DDA], [5th DDA], [Address #1], [Address #2], City , ST, Zip, [First], [Last], MI, First2, Last2, MI2, First3, Last3, MI3, First4, Last4, MI4, SSN, [Home Phone], [Work Phone], TipFirst, TipNumber, NA1, NA2, NA3, NA4, NA5, NA6, LastName, DataDate)
select DemographicIn.*, @DataDate from DemographicIn

Truncate Table DemographicIn
truncate table DemographicIn_Deleted
GO
