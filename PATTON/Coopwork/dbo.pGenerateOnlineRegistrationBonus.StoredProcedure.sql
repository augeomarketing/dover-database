SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGenerateOnlineRegistrationBonus] @EndDate varchar(10), @tipfirst char(3)
AS 

/*************************************************************/
/*                                                           */
/*   Procedure to generate the Online sign up bonus          */
/*                                                           */
/*                                                           */
/*************************************************************/

declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10)

set @Trandate=@EndDate

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  OnlineRegistrationBonus from DBProcessInfo
				where DBNumber=@TipFirst)

if @BonusPoints>0 and @BonusPoints is not null
Begin
	
/*                                                                            */
/* Setup Cursor for processing                                                */
set @sqlcursor=N'declare Tip_crsr cursor 
for select tipnumber
from ' + QuoteName(@DBName) + N' .dbo.Signon_Bonus_Table where tipnumber=@tipnumber'
exec sp_executesql @SQLcursor, N'@Tipnumber nchar(15)',	@Tipnumber= @Tipnumber

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber 

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
		where tipnumber = @Tipnumber'
		exec sp_executesql @SQLUpdate, N'@Tipnumber nchar(15), @BonusPoints numeric(9)', @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints

		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        	Values(@Tipnumber, @Trandate, ''BS'', ''1'', @BonusPoints, ''1'', ''Online Registration Bonus'', ''0'')'
		Exec sp_executesql @SQLInsert, N'@Trandate nchar(10), @Tipnumber nchar(15), @BonusPoints numeric(9)', @Trandate= @Trandate, @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints
 
		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

End
GO
