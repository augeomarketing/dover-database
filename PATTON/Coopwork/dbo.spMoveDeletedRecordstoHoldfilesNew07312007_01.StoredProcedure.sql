SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spMoveDeletedRecordstoHoldfilesNew07312007_01] @TipFirst char(3), @Enddate char(10)
as

/*******************************************************************************/
/* SEB001 8/07 Added logic to delete records from Tip_DDA_Reference */
/*******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 10/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* Add logic to remove customers from to be deleted file if they have history beyond the end of month.  */

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateToDelete char(6), @dateclosed datetime, @newmonth nchar(2), @newyear nchar(4)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--set @DBName='zz617OmniCCUConsumer'

declare @datedeleted datetime
set @datedeleted=@Enddate
--set @datedeleted=getdate()

/********************************************************************/
/*  Code to build work table with tipnumbers that are to be deleted */
/********************************************************************/
set @newmonth= cast(datepart(month, @datedeleted) as char(2)) 
set @newyear= cast(datepart(yyyy,@datedeleted) as char(4)) 

if CONVERT( int , @newmonth)<'10' 
	begin
	set @newmonth='0' + left(@newmonth,1)
	end	

set @DateToDelete = @newmonth + @newyear

--Drop Table #ClosedCustomers
CREATE TABLE #ClosedCustomers (Tipnumber nchar(15))

set @sqlinsert=N'INSERT INTO [#ClosedCustomers] (Tipnumber)
       	SELECT TIPNumber FROM ' + QuoteName(@DBName) + N'.dbo.Customer_Closed where DateToDelete = @DateToDelete'
exec sp_executesql @SQLInsert, N'@DateToDelete nchar(6)', @DateToDelete = @DateToDelete
/********************************************************************/
/*  End Code to build work table with tipnumbers that to be deleted */
/********************************************************************/

/**********************************************************/
/*  Start add code  SEB001                                */
/**********************************************************/
--delete customers to be closed that have any transaction after the EndDate
set @SQLDelete = N'delete from [#ClosedCustomers]
			where tipnumber in (select tipnumber from ' + QuoteName(@DBName) + N'.dbo.history where histdate>@enddate) '
exec sp_executesql @SQLDelete, N'@enddate nchar(10)', @enddate = @enddate

/**********************************************************/
/*  End add code  SEB001                                */
/**********************************************************/

set @sqlinsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.CustomerDeleted 
       	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @datedeleted 
	FROM ' + QuoteName(@DBName) + N'.dbo.Customer where status=''C'' and Tipnumber in (Select Tipnumber from [#ClosedCustomers])'
exec sp_executesql @SQLInsert, N'@datedeleted datetime', @datedeleted=@datedeleted
/* */
/* */
set @sqlinsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
	FROM ' + QuoteName(@DBName) + N'.dbo.affiliat 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLInsert, N'@datedeleted datetime', @datedeleted=@datedeleted
/* */
/* */
set @sqlinsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, @datedeleted
	FROM ' + QuoteName(@DBName) + N'.dbo.history
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLInsert, N'@datedeleted datetime', @datedeleted=@datedeleted
/* */
/* */
set @sqlinsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.Account_Reference_Deleted (TIPNumber, acctnumber, tipfirst, datedeleted)
       	SELECT TIPNumber, acctnumber, tipfirst, @Datedeleted
	FROM ' + QuoteName(@DBName) + N'.dbo.Account_Reference 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLInsert, N'@datedeleted datetime', @datedeleted=@datedeleted
/* */
/* */
set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.affiliat 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLDelete
/* */
/* */
set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.history 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLDelete
/* */
/* */
set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.customer 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLDelete
/* */
/* */
set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.[Account_Reference]
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLDelete
/* */
/* */
set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.[Beginning_Balance_Table]
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLDelete

/* Begin SEB001            */
If @TipFirst in ('609','634','650')
	Begin
		set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.[Tip_DDA_Reference]
			where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
		exec sp_executesql @SQLDelete
	End
/* End SEB001             */

set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.[Customer_Closed]
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) '
exec sp_executesql @SQLDelete
GO
