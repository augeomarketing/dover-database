SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pMonthlyStatementFilesav] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3) /* Added SEB 1/9/2007 */
/* CREATE PROCEDURE pMonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10), @Tipfirst char(3) SEB 1/09/2007 */
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/
Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Monthly_Statement_File '
exec sp_executesql @SQLTruncate

set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
        	select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
		from ' + QuoteName(@DBName) + N'.dbo.customer '
Exec sp_executesql @SQLInsert
 

/* Load the statmement file with purchases          */
--update Monthly_Statement_File
--set pointspurchased=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='67')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='67')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchaseddb=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''67'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''67'') '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with bonuses            */
--update Monthly_Statement_File
--set pointsbonus=(select sum(points*ratio) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like'B%')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'B%')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusdb=
		(select sum(points*ratio) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''B%'' or trancode=''NW'')) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''B%'' or trancode=''NW'')) '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with plus adjustments    */
--update Monthly_Statement_File
--set pointsadded=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''IE'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''IE'') '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with total point increases */
--update Monthly_Statement_File
--set pointsincreased=pointspurchaseddb + pointsbonusdb + pointsadded
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsincreased=pointspurchaseddb + pointsbonusdb + pointsadded '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
--update Monthly_Statement_File
--set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=
		(select sum(points*ratio*-1) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'') '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file decreased redeemed         */
--update Monthly_Statement_File
--set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=pointsredeemed -
		(select sum(points*ratio) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''DR'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''DR'') '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with returns            */
--update Monthly_Statement_File
--set pointsreturned=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='37')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='37')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturneddb=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''37'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''37'') '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with minus adjustments    */
--update Monthly_Statement_File
--set pointssubtracted=(select sum(points) from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='DE')
--where exists(select * from history where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='DE')
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointssubtracted=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''DE'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''DE'') '
Exec sp_executesql @SQLUpdate, N'@StartDate varchar(10), @EndDate varchar(10)', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with total point decreases */
--update Monthly_Statement_File
--set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturneddb + pointssubtracted '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
/*update Monthly_Statement_File
set pointsbegin=(select BeginningPoints from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber) */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbegin=
		(select ' + Quotename(@MonthBucket) + N'from ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber)
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
--update Monthly_Statement_File
--set pointsend=pointsbegin + pointsincreased - pointsdecreased
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased '
exec sp_executesql @SQLUpdate
GO
