SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCheckExistanceOfTipnumber] @DBNAME char(100)
AS 

declare @TipNumber char(15), @SQLIf nvarchar(2000)

drop table chktip

set @SQLIF='select tipnumber
		into chktip
		from demographicin
		where tipnumber is not null and not exists( SELECT * FROM ' + Quotename(@DBNAME) + N'.dbo.customer where tipnumber=demographicin.Tipnumber) '
exec sp_executesql @SQLIF

declare cardsin_crsr cursor
for select tipnumber
from chktip
order by tipnumber

open cardsin_crsr
/*                                                                            */
fetch cardsin_crsr into @tipnumber
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
/*         */
		exec spreactivatecustomerdata @Tipnumber, @DBNAME			
				
	fetch cardsin_crsr into @tipnumber
END

Fetch_Error:
close cardsin_crsr
deallocate cardsin_crsr
GO
