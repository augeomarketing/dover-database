USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[spMoveDeletedRecordstoHoldfilesNew07312007]    Script Date: 07/16/2014 10:21:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMoveDeletedRecordstoHoldfilesNew07312007]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMoveDeletedRecordstoHoldfilesNew07312007]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[spMoveDeletedRecordstoHoldfilesNew07312007]    Script Date: 07/16/2014 10:21:45 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spMoveDeletedRecordstoHoldfilesNew07312007] @TipFirst CHAR(3), @Enddate DATE

AS

/*******************************************************************************/
/* SEB001 8/07 Added logic to delete records from Tip_DDA_Reference */
/*******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 10/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* Add logic to remove customers from to be deleted file if they have history beyond the end of month.  */

DECLARE	@DBName			VARCHAR(50)		= (select rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo where DBNumber = @TipFirst)
	,	@purgeincrement INT				= (select ClosedMonths from  rewardsnow.dbo.DBProcessInfo where DBNumber = @TipFirst)
	,	@SQLDelete		NVARCHAR(MAX)
	,	@SQLInsert		NVARCHAR(MAX)
	,	@SQLIf			NVARCHAR(MAX)
	,	@SQLIfDelete	NVARCHAR(MAX)

/********************************************************************/
/*  Code to build work table with tipnumbers that are to be deleted */
/********************************************************************/
--Drop Table #ClosedCustomers
CREATE TABLE #ClosedCustomers (Tipnumber nchar(15))

SET		@sqlinsert	=	N'
			INSERT INTO [#ClosedCustomers] (Tipnumber)
       		SELECT TIPNumber FROM [<<DBNAME>>].dbo.Customer_Closed WHERE CAST(DATEADD(MONTH,<<PURGEINCREMENT>>,dateclosed) AS DATE) <= ''<<ENDDATE>>''
       					'
SET		@SQLInsert = REPLACE(@sqlinsert,'<<DBNAME>>',@DBName)
SET		@SQLInsert = REPLACE(@sqlinsert,'<<PURGEINCREMENT>>',@purgeincrement)
SET		@SQLInsert = REPLACE(@sqlinsert,'<<ENDDATE>>',@Enddate)

EXEC	sp_executesql @SQLInsert--, N'@DateToDelete nchar(6)', @DateToDelete = @DateToDelete
/********************************************************************/
/*  End Code to build work table with tipnumbers that to be deleted */
/********************************************************************/

/**********************************************************/
/*  Start add code  SEB001                                */
/**********************************************************/
--delete customers to be closed that have any transaction after the EndDate
SET		@SQLDelete	=	N'
				DELETE FROM [#ClosedCustomers]
				WHERE tipnumber IN (select tipnumber from [<<DBNAME>>].dbo.history where CAST(histdate as DATE) > ''<<ENDDATE>>'')
						'
SET		@SQLDelete = REPLACE(@SQLDelete,'<<DBNAME>>',@DBName)
SET		@SQLDelete = REPLACE(@SQLDelete,'<<ENDDATE>>',@Enddate)

EXEC	sp_executesql @SQLDelete

/**********************************************************/
/*  End add code  SEB001                                */
/**********************************************************/

SET	@sqlinsert	=	N'
		INSERT INTO [<<DBNAME>>].dbo.CustomerDeleted 
			(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4,
			 City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate,
			 NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5,
			 RunBalanceNew, RunAvaliableNew, datedeleted)
       	SELECT	TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4,
       			City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate,
       			NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5,
       			RunBalanceNew, RunAvaliableNew, ''<<ENDDATE>>'' 
		FROM	[<<DBNAME>>].dbo.Customer 
		WHERE	status=''C'' and Tipnumber in (Select Tipnumber from [#ClosedCustomers])
					'
SET		@SQLInsert = REPLACE(@sqlinsert,'<<DBNAME>>',@DBName)
SET		@SQLInsert = REPLACE(@sqlinsert,'<<ENDDATE>>',@Enddate)

EXEC	sp_executesql @SQLInsert
/* */
/* */
SET	@sqlinsert	=	N'
		INSERT INTO [<<DBNAME>>].dbo.AffiliatDeleted 
			(TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
       	SELECT	TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, ''<<ENDDATE>>''
		FROM	[<<DBNAME>>].dbo.affiliat 
		WHERE	Tipnumber in (Select Tipnumber from [#ClosedCustomers])
					'
SET		@SQLInsert = REPLACE(@sqlinsert,'<<DBNAME>>',@DBName)
SET		@SQLInsert = REPLACE(@sqlinsert,'<<ENDDATE>>',@Enddate)
	
EXEC	sp_executesql @SQLInsert
/* */
/* */
SET	@sqlinsert	=	N'
		INSERT INTO [<<DBNAME>>].dbo.historyDeleted 
			(TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT	TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, ''<<ENDDATE>>''
		FROM	[<<DBNAME>>].dbo.history
		WHERE	Tipnumber in (Select Tipnumber from [#ClosedCustomers])
					'
SET		@SQLInsert = REPLACE(@sqlinsert,'<<DBNAME>>',@DBName)
SET		@SQLInsert = REPLACE(@sqlinsert,'<<ENDDATE>>',@Enddate)

EXEC	sp_executesql @SQLInsert
/* */

/* CONDITIONAL ACCOUNT_REFERENCE*/
SET		@SQLIf	=	N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
				BEGIN
				INSERT INTO [<<DBNAME>>].dbo.Account_Reference_Deleted (TIPNumber, acctnumber, tipfirst, datedeleted)
       			SELECT TIPNumber, acctnumber, tipfirst, ''<<ENDDATE>>''
				FROM [<<DBNAME>>].dbo.Account_Reference 
				WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 
				End
					'
SET		@SQLIf = REPLACE(@SQLIf,'<<DBNAME>>',@DBName)
SET		@SQLIf = REPLACE(@SQLIf,'<<ENDDATE>>',@Enddate)

EXEC	sp_executesql @SQLIf
/* */

SET		@sqlDelete	=	N'
			DELETE FROM [<<DBNAME>>].dbo.affiliat WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers])
						'
SET		@sqlDelete = REPLACE(@sqlDelete,'<<DBNAME>>',@DBName)

EXEC	sp_executesql @SQLDelete
/* */
/* */
SET		@sqlDelete	=	N'
			DELETE FROM [<<DBNAME>>].dbo.history WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers])
						'
SET		@sqlDelete = REPLACE(@sqlDelete,'<<DBNAME>>',@DBName)

EXEC	sp_executesql @SQLDelete
/* */
/* */
SET		@sqlDelete	=	N'
			DELETE FROM [<<DBNAME>>].dbo.customer WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers])
						'
SET		@sqlDelete = REPLACE(@sqlDelete,'<<DBNAME>>',@DBName)

EXEC	sp_executesql @SQLDelete
/* */
/* deletes IF account_reference exists*/
SET		@SQLIfDelete	=	N'
			IF EXISTS (select * from [<<DBNAME>>].dbo.sysobjects where xtype=''u'' and name = ''Account_Reference'')
				BEGIN
				DELETE FROM [<<DBNAME>>].dbo.[Account_Reference] WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 
				END
							'
SET		@SQLIfDelete = REPLACE(@SQLIfDelete,'<<DBNAME>>',@DBName)

EXEC	sp_executesql @SQLIfDelete
/* */
/* */
SET		@sqlDelete=N'
			DELETE FROM [<<DBNAME>>].dbo.[Beginning_Balance_Table] WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers])
					'
SET		@sqlDelete = REPLACE(@sqlDelete,'<<DBNAME>>',@DBName)

EXEC	sp_executesql @SQLDelete
/* Begin SEB001            */
IF	@TipFirst in ('609','634','650')
	BEGIN
	SET	@sqlDelete	=	N'
			DELETE FROM [<<DBNAME>>].dbo.[Tip_DDA_Reference] WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers])
						'
	SET		@sqlDelete = REPLACE(@sqlDelete,'<<DBNAME>>',@DBName)
					
	EXEC	sp_executesql @SQLDelete
	END
/* End SEB001             */
SET		@sqlDelete	=	N'
			DELETE FROM [<<DBNAME>>].dbo.[Customer_Closed] WHERE Tipnumber in (Select Tipnumber from [#ClosedCustomers])
						'
SET		@sqlDelete = REPLACE(@sqlDelete,'<<DBNAME>>',@DBName)

EXEC	sp_executesql @SQLDelete

GO


