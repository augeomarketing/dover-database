USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[spPurgeTransHold]    Script Date: 06/02/2010 13:00:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeTransHold]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeTransHold]
GO

USE [COOPWork]
GO

/****** Object:  StoredProcedure [dbo].[spPurgeTransHold]    Script Date: 06/02/2010 13:00:15 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spPurgeTransHold] @Startdate char(10), @Enddate char(10)
AS 
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 0 */
/* SCAN: SEB */
/* Script to extract records to purge from the transdetailhold table  */

--no longer purging this table
--Truncate table TransPurge
/*
insert into TransPurge (tdhID, TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID, OrigDataElem)
select tdhID, TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID, OrigDataElem
from Transdetailhold
where trandate>=@startdate and trandate<=@enddate
*/

delete from Transdetailhold
where convert(datetime,trandate)>=@startdate and convert(datetime,trandate)<=@enddate

GO


