SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spBackupDatabaseInit] @DBName varchar(100)
as

declare @spath varchar(100), @sBackupPath varchar(100), @cleardbs varchar(100)
set @sPath = 'd:\SQLData\BACKUPS\'

set @sBackupPath = @sPath + @DBName

backup database @DBName to disk = @DBName with init
GO
