SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzz_DELETE_pSetTipFirstSarah] 
AS
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 8/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to be able to handle 9 digit bins  */

/* Get tipfirst based on Institution ID  */

/******************       START SEB001            ***************************/
--update DemographicIn
--set DemographicIn.tipFirst=TipFirstReference.TipFirst
--from DemographicIn, TipFirstReference
--where left(demographicin.pan,6)=TipFirstReference.BIN

update DemographicInSarah
set tipFirst=b.TipFirst
from DemographicInSarah a, TipFirstReference b
where a.pan like (rtrim(b.bin) + '%')
/******************       END SEB001            ***************************/
GO
