USE [COOPWork]
GO
/****** Object:  Table [dbo].[ProcessingDates]    Script Date: 09/02/2011 15:26:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessingDates]') AND type in (N'U'))
DROP TABLE [dbo].[ProcessingDates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProcessingDates]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProcessingDates](
	[DataMonthBeginDate] [nchar](10) NULL,
	[DataMonthEndDate] [nchar](10) NULL,
	[ProcessMonthBeginDate] [nchar](10) NULL,
	[ProcessMonthEndDate] [nchar](10) NULL,
	[QuarterBeginDate] [nchar](10) NULL,
	[QuarterEndDate] [nchar](10) NULL,
	[RecType] [varchar](3) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
