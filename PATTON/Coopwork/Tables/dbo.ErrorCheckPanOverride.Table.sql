USE [COOPWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ErrorChec__dim_e__3D7EE6BA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ErrorCheckPanOverride] DROP CONSTRAINT [DF__ErrorChec__dim_e__3D7EE6BA]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ErrorChec__dim_e__3E730AF3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ErrorCheckPanOverride] DROP CONSTRAINT [DF__ErrorChec__dim_e__3E730AF3]
END

GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[ErrorCheckPanOverride]    Script Date: 11/10/2011 15:13:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ErrorCheckPanOverride]') AND type in (N'U'))
DROP TABLE [dbo].[ErrorCheckPanOverride]
GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[ErrorCheckPanOverride]    Script Date: 11/10/2011 15:13:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ErrorCheckPanOverride](
	[sid_errorcheckpanoverride_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_errorcheckpanoverride_bin] [varchar](6) NOT NULL,
	[sid_errorcheckoverridetype_id] [bigint] NOT NULL,
	[dim_errorcheckpanoverride_created] [datetime] NOT NULL,
	[dim_errorcheckpanoverride_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK__ErrorChe__60E430CA3B969E48] PRIMARY KEY CLUSTERED 
(
	[sid_errorcheckpanoverride_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_ErrorCheckPanOverride_bin_overridetype] UNIQUE NONCLUSTERED 
(
	[dim_errorcheckpanoverride_bin] ASC,
	[sid_errorcheckoverridetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ErrorCheckPanOverride] ADD  CONSTRAINT [DF__ErrorChec__dim_e__3D7EE6BA]  DEFAULT (getdate()) FOR [dim_errorcheckpanoverride_created]
GO

ALTER TABLE [dbo].[ErrorCheckPanOverride] ADD  CONSTRAINT [DF__ErrorChec__dim_e__3E730AF3]  DEFAULT (getdate()) FOR [dim_errorcheckpanoverride_lastmodified]
GO

