USE [COOPWork]
GO
/****** Object:  Table [dbo].[HouseHoldException]    Script Date: 09/14/2012 11:25:57 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HouseHoldException_dim_HouseHoldException_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[HouseHoldException]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HouseHoldException_dim_HouseHoldException_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HouseHoldException] DROP CONSTRAINT [DF_HouseHoldException_dim_HouseHoldException_DateAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HouseHoldException]') AND type in (N'U'))
DROP TABLE [dbo].[HouseHoldException]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HouseHoldException]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HouseHoldException](
	[sid_HouseHoldException_TipFirst] [varchar](3) NOT NULL,
	[sid_HouseHoldException_ExceptionType] [varchar](50) NOT NULL,
	[dim_HouseHoldException_ExceptionValue] [varchar](50) NOT NULL,
	[dim_HouseHoldException_active] [int] NOT NULL,
	[dim_HouseHoldException_DateAdded] [date] NOT NULL,
 CONSTRAINT [PK_HouseHoldException] PRIMARY KEY CLUSTERED 
(
	[sid_HouseHoldException_TipFirst] ASC,
	[sid_HouseHoldException_ExceptionType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_HouseHoldException_dim_HouseHoldException_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[HouseHoldException]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_HouseHoldException_dim_HouseHoldException_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[HouseHoldException] ADD  CONSTRAINT [DF_HouseHoldException_dim_HouseHoldException_DateAdded]  DEFAULT (getdate()) FOR [dim_HouseHoldException_DateAdded]
END


End
GO
