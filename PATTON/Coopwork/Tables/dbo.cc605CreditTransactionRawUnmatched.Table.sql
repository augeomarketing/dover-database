USE [COOPWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__cc605Cred__DateA__6C6EDFCD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[cc605CreditTransactionRawUnmatched] DROP CONSTRAINT [DF__cc605Cred__DateA__6C6EDFCD]
END

GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605CreditTransactionRawUnmatched]    Script Date: 11/30/2010 16:27:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cc605CreditTransactionRawUnmatched]') AND type in (N'U'))
DROP TABLE [dbo].[cc605CreditTransactionRawUnmatched]
GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605CreditTransactionRawUnmatched]    Script Date: 11/30/2010 16:27:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cc605CreditTransactionRawUnmatched](
	[RecordType] [varchar](2) NULL,
	[CorporationNumber] [varchar](6) NULL,
	[AccountNumber] [varchar](16) NULL,
	[CardCode] [varchar](1) NULL,
	[PostingDate] [varchar](6) NULL,
	[TieBreaker] [varchar](4) NULL,
	[TransactionCode] [varchar](3) NULL,
	[ReasonCode] [varchar](2) NULL,
	[CardType] [varchar](1) NULL,
	[PostFlag] [varchar](1) NULL,
	[PostDate] [varchar](6) NULL,
	[PurchaseDate] [varchar](6) NULL,
	[LocalTransactionAmount] [varchar](13) NULL,
	[LocalTransactionAmountSign] [varchar](1) NULL,
	[OriginalTransactionAmount] [varchar](12) NULL,
	[OriginalTransactionAmountSign] [varchar](1) NULL,
	[OriginalCurrencyCode] [varchar](3) NULL,
	[OriginalCurrencyExponent] [varchar](1) NULL,
	[DestinationTransactionAmount] [varchar](12) NULL,
	[DestinationTransactionAmountSign] [varchar](1) NULL,
	[AdditionalData1] [varchar](255) NULL,
	[AdditionalData2] [varchar](255) NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[cc605CreditTransactionRawUnmatched] ADD  DEFAULT (getdate()) FOR [DateAdded]
GO


