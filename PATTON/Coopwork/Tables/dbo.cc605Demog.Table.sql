USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605Demog]    Script Date: 11/30/2010 16:28:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cc605Demog]') AND type in (N'U'))
DROP TABLE [dbo].[cc605Demog]
GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605Demog]    Script Date: 11/30/2010 16:28:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cc605Demog](
	[dim_cc605demog_pan] [varchar](16) NULL,
	[dim_cc605demog_instid] [varchar](10) NULL,
	[dim_cc605demog_cs] [varchar](5) NULL,
	[dim_cc605demog_primdda] [varchar](16) NULL,
	[dim_cc605demog_1stdda] [varchar](16) NULL,
	[dim_cc605demog_2nddda] [varchar](16) NULL,
	[dim_cc605demog_3rddda] [varchar](16) NULL,
	[dim_cc605demog_4thdda] [varchar](16) NULL,
	[dim_cc605demog_5thdda] [varchar](16) NULL,
	[dim_cc605demog_address1] [varchar](40) NULL,
	[dim_cc605demog_address2] [varchar](40) NULL,
	[dim_cc605demog_city] [varchar](40) NULL,
	[dim_cc605demog_st] [varchar](2) NULL,
	[dim_cc605demog_zip] [varchar](9) NULL,
	[dim_cc605demog_first] [varchar](40) NULL,
	[dim_cc605demog_last] [varchar](40) NULL,
	[dim_cc605demog_mi] [varchar](1) NULL,
	[dim_cc605demog_first2] [varchar](40) NULL,
	[dim_cc605demog_last2] [varchar](40) NULL,
	[dim_cc605demog_mi2] [varchar](1) NULL,
	[dim_cc605demog_first3] [varchar](40) NULL,
	[dim_cc605demog_last3] [varchar](40) NULL,
	[dim_cc605demog_mi3] [varchar](1) NULL,
	[dim_cc605demog_first4] [varchar](40) NULL,
	[dim_cc605demog_last4] [varchar](40) NULL,
	[dim_cc605demog_mi4] [varchar](1) NULL,
	[dim_cc605demog_ssn] [varchar](16) NULL,
	[dim_cc605demog_homephone] [varchar](10) NULL,
	[dim_cc605demog_workphone] [varchar](10) NULL,
	[dim_cc605demog_tipfirst] [varchar](3) NULL,
	[dim_cc605demog_tipnumber] [varchar](15) NULL,
	[dim_cc605demog_na1] [varchar](40) NULL,
	[dim_cc605demog_na2] [varchar](40) NULL,
	[dim_cc605demog_na3] [varchar](40) NULL,
	[dim_cc605demog_na4] [varchar](40) NULL,
	[dim_cc605demog_na5] [varchar](40) NULL,
	[dim_cc605demog_na6] [varchar](40) NULL,
	[dim_cc605demog_lastname] [varchar](40) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


