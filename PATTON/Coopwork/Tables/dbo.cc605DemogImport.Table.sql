USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605demogimport]    Script Date: 12/13/2010 15:52:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cc605demogimport]') AND type in (N'U'))
DROP TABLE [dbo].[cc605demogimport]
GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605demogimport]    Script Date: 12/13/2010 15:52:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cc605demogimport](
	[sid_cc605demogimport_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_cc605demogimport_address1] [varchar](40) NULL,
	[dim_cc605demogimport_address2] [varchar](40) NULL,
	[dim_cc605demogimport_city] [varchar](40) NULL,
	[dim_cc605demogimport_first] [varchar](40) NULL,
	[dim_cc605demogimport_homephone] [varchar](10) NULL,
	[dim_cc605demogimport_last] [varchar](40) NULL,
	[dim_cc605demogimport_pan] [varchar](16) NULL,
	[dim_cc605demogimport_ssn] [varchar](16) NULL,
	[dim_cc605demogimport_st] [varchar](2) NULL,
	[dim_cc605demogimport_zip] [varchar](11) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


