USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605CreditTransactionRaw]    Script Date: 11/30/2010 16:26:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[cc605CreditTransactionRaw]') AND type in (N'U'))
DROP TABLE [dbo].[cc605CreditTransactionRaw]
GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[cc605CreditTransactionRaw]    Script Date: 11/30/2010 16:26:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[cc605CreditTransactionRaw](
	[RecordType] [varchar](2) NULL,
	[CorporationNumber] [varchar](6) NULL,
	[AccountNumber] [varchar](16) NULL,
	[CardCode] [varchar](1) NULL,
	[PostingDate] [varchar](6) NULL,
	[TieBreaker] [varchar](4) NULL,
	[TransactionCode] [varchar](3) NULL,
	[ReasonCode] [varchar](2) NULL,
	[CardType] [varchar](1) NULL,
	[PostFlag] [varchar](1) NULL,
	[PostDate] [varchar](6) NULL,
	[PurchaseDate] [varchar](6) NULL,
	[LocalTransactionAmount] [varchar](13) NULL,
	[LocalTransactionAmountSign] [varchar](1) NULL,
	[OriginalTransactionAmount] [varchar](12) NULL,
	[OriginalTransactionAmountSign] [varchar](1) NULL,
	[OriginalCurrencyCode] [varchar](3) NULL,
	[OriginalCurrencyExponent] [varchar](1) NULL,
	[DestinationTransactionAmount] [varchar](12) NULL,
	[DestinationTransactionAmountSign] [varchar](1) NULL,
	[AdditionalData1] [varchar](255) NULL,
	[AdditionalData2] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


