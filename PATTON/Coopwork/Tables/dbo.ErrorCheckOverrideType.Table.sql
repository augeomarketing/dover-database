USE [COOPWork]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__errorchec__dim_e__4337C010]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ErrorCheckOverrideType] DROP CONSTRAINT [DF__errorchec__dim_e__4337C010]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__errorchec__dim_e__442BE449]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ErrorCheckOverrideType] DROP CONSTRAINT [DF__errorchec__dim_e__442BE449]
END

GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[ErrorCheckOverrideType]    Script Date: 11/10/2011 15:10:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ErrorCheckOverrideType]') AND type in (N'U'))
DROP TABLE [dbo].[ErrorCheckOverrideType]
GO

USE [COOPWork]
GO

/****** Object:  Table [dbo].[ErrorCheckOverrideType]    Script Date: 11/10/2011 15:10:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ErrorCheckOverrideType](
	[sid_errorcheckoverridetype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_errorcheckoverridetype_description] [varchar](255) NOT NULL,
	[dim_errorcheckoverridetype_created] [datetime] NOT NULL,
	[dim_errorcheckoverridetype_lastmodified] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_errorcheckoverridetype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ErrorCheckOverrideType] ADD  DEFAULT (getdate()) FOR [dim_errorcheckoverridetype_created]
GO

ALTER TABLE [dbo].[ErrorCheckOverrideType] ADD  DEFAULT (getdate()) FOR [dim_errorcheckoverridetype_lastmodified]
GO


