SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spCOOPGeneric_zTimeLogger] @StartOrStop varchar(50), @ProcessName varchar(50)
AS 

Declare @StartTime datetime, @EndTime datetime, @SQLInsert nvarchar(500), @SQLUpdate nvarchar(500), @ElapsedMinutes int
set @StartTime=Getdate()
set @EndTime=Getdate()

if @StartOrStop='START'
Begin
	set @SQLInsert=' INSERT INTO zProcessTimeLogger (StartTime,ProcessName) values (@StartTime,@ProcessName )'
	print @SQLInsert
	exec sp_executesql @SQLInsert, N'@StartTime  datetime,@ProcessName varchar(50)',@StartTime=@StartTime, @ProcessName=@ProcessName
End

if @StartOrStop='STOP'
Begin

	declare @SQLSelect nvarchar(500)
	set @SQLSelect='SELECT @StartTime=StartTime from zProcessTimeLogger where EndTime is NULL and ProcessName=@ProcessName'
	exec sp_executesql @SQLSelect, N'@StartTime  datetime output,@ProcessName varchar(50)',@StartTime=@StartTime output, @ProcessName=@ProcessName


	set @ElapsedMinutes= datediff(mi,@StartTime, @EndTime)
	set @SQLUpdate=' UPDATE zProcessTimeLogger SET EndTime=@EndTime, ElapsedMinutes=@ElapsedMinutes  WHERE ProcessName=@ProcessName and EndTime is null'

	exec sp_executesql @SQLUpdate, N'@EndTime  datetime,@ProcessName varchar(50), @ElapsedMinutes int',@EndTime=@EndTime, @ProcessName=@ProcessName, @ElapsedMinutes=@ElapsedMinutes
End
GO
