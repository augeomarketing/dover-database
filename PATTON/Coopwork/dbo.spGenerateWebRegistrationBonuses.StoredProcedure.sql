SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGenerateWebRegistrationBonuses] @startDate varchar(10), @EndDate varchar(10), @tipfirst char(3), @Trancode char(2)
AS 

/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLIF nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1), @Description char(40)

set @Trandate=@EndDate

set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  OnlineRegistrationBonus from DBProcessInfo
				where DBNumber=@TipFirst)
set @Description =(SELECT  Description from Trantype
				where Trancode = @Trancode )

if @BonusPoints>0 
Begin
	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select tipnumber
from Web_Registration 
where left(tipnumber,3)=@Tipfirst 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag='N'		
		set @sqlif=N'if not exists(select * from ' + QuoteName(@DBName) + N' .dbo.OneTimeBonuses where tipnumber=@tipnumber and trancode = @Trancode) Begin set @ProcessFlag=''Y'' End '  
		exec sp_executesql @SQLIf, N'@Tipnumber nchar(15), @ProcessFlag char(1) output, @Trancode char(2)', @Tipnumber= @Tipnumber, @ProcessFlag=@ProcessFlag output, @Trancode = @Trancode 


		if @ProcessFlag='Y'
		Begin
 
		set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.Customer set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
		where tipnumber = @Tipnumber'
		exec sp_executesql @SQLUpdate, N'@Tipnumber nchar(15), @BonusPoints numeric(9)', @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints

		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        	Values(@Tipnumber, @Trandate, @Trancode, ''1'', @BonusPoints, ''1'', @Description, ''0'')'
		Exec sp_executesql @SQLInsert, N'@Trandate nchar(10), @Tipnumber nchar(15), @BonusPoints numeric(9), @Trancode char(2), @Description char(40)', @Trandate= @Trandate, @Tipnumber= @Tipnumber, @BonusPoints=@BonusPoints,  @Trancode = @Trancode, @Description = @Description  
 
		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.OneTimeBonuses(TipNumber,TranCode, DateAwarded)
        	Values(@Tipnumber, @Trancode, @Trandate)'
		Exec sp_executesql @SQLInsert, N'@Trandate nchar(10), @Tipnumber nchar(15), @Trancode char(2)', @Trandate= @Trandate, @Tipnumber= @Tipnumber, @Trancode = @Trancode 

		End

		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

End
GO
