SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBProcessInfo](
	[DBName] [varchar](50) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL,
	[ActivationBonus] [numeric](18, 0) NOT NULL,
	[FirstUseBonus] [numeric](18, 0) NOT NULL,
	[OnlineRegistrationBonus] [numeric](18, 0) NOT NULL,
	[E-StatementBonus] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF_DBProcessInfo_ActivationBonus]  DEFAULT (0) FOR [ActivationBonus]
GO
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF_DBProcessInfo_FirstUseBonus]  DEFAULT (0) FOR [FirstUseBonus]
GO
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF_DBProcessInfo_OnlineRegistrationBonus]  DEFAULT (0) FOR [OnlineRegistrationBonus]
GO
ALTER TABLE [dbo].[DBProcessInfo] ADD  CONSTRAINT [DF_DBProcessInfo_E-StatementBonus]  DEFAULT (0) FOR [E-StatementBonus]
GO
