SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pDealWithClosedCards] @TipFirst char(3), @Enddate char(10)
AS 
/************************************************************************************/
/*                                                                                  */
/*  Acctid in Affiliat table is actually the last six of cardno                     */
/*  Secid in Affiliat table is actually the Acctno (DDA#) for Spirit Bank           */
/*  Custid in Affiliat table is actually the CIS# for Spirit Bank                   */
/*                                                                                  */
/************************************************************************************/

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

declare @datedeleted datetime
set @datedeleted = @Enddate
--set @datedeleted=getdate()

--update affiliat	
--set acctstatus = 'C'
--where not exists(select * from rnfile where cardno=affiliat.acctid)
--meaning, If we don't see a card in Custin, se it to closed
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.affiliat set acctstatus = ''C'' where not exists(select * from ' + QuoteName(@DBName) + N' .dbo.custin where ' + QuoteName(@DBName) + N' .dbo.custin.acct_num= ' + QuoteName(@DBName) + N' .dbo.affiliat.acctid) '
exec sp_executesql @SQLUpdate

--update customer
--set status='C'
--where status='A' and not exists(select * from affiliat where tipnumber=customer.tipnumber and acctstatus='A')
--meaning, set active Tipnumbers to closed if it has no active affiliat records
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set status = ''C'' where status=''A'' and not exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat where ' + QuoteName(@DBName) + N' .dbo.affiliat.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat.acctstatus=''A'') '
exec sp_executesql @SQLUpdate

/* added 7/2007 */
--Set any closed accounts to active if they have any active affiliat recs
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer set status = ''A'' where status=''C'' and exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat where ' + QuoteName(@DBName) + N' .dbo.affiliat.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat.acctstatus=''A'') '
exec sp_executesql @SQLUpdate

--INSERT INTO AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
  --     	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
	--FROM affiliat where acctstatus='C'

--set @sqlinsert=N'INSERT INTO ' + QuoteName(@DBName) + N' .dbo.AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
--       	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
--	FROM ' + QuoteName(@DBName) + N'.dbo.affiliat where acctstatus=''C'''
--exec sp_executesql @SQLInsert, N'@datedeleted datetime', @datedeleted=@datedeleted

--delete from affiliat
--where acctstatus='C'
--set @sqlDelete=N'Delete from ' + QuoteName(@DBName) + N' .dbo.affiliat where acctstatus = ''C'' '
--exec sp_executesql @SQLDelete
GO
