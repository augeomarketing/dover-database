SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pCurrentMonthActivityLoadSav] @EndDateParm varchar(10), @TipFirst char(3)
--CREATE PROCEDURE pCurrentMonthActivityLoad @EndDate varchar(10), @TipFirst char(3), @Segment char(1)
AS

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2)

/*
RDT 10/09/2006
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
*/

Declare @EndDate DateTime                         --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Current_Month_Activity '
exec sp_executesql @SQLTruncate



--insert into Current_Month_Activity (Tipnumber, EndingPoints)
--select tipnumber, runavailable 
--from Customer
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity(tipnumber, EndingPoints)
        	select tipnumber, runavailable
		from ' + QuoteName(@DBName) + N'.dbo.customer '
Exec sp_executesql @SQLInsert

/* Load the current activity table with increases for the current month         */
--update Current_Month_Activity
--set increases=(select sum(points) from history where histdate>@enddate and ratio='1' and history.tipnumber=Current_Month_Activity.tipnumber)
--where exists(select * from history where histdate>@enddate and ratio='1' and history.tipnumber=Current_Month_Activity.tipnumber)
 set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set increases=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where histdate>@enddate and ratio=''1'' and ' + QuoteName(@DBName) + N'.dbo.history.tipnumber=' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity.tipnumber) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where histdate>@enddate and ratio=''1'' and ' + QuoteName(@DBName) + N'.dbo.history.tipnumber=' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity.tipnumber) ' 
/*set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set increases=
		sum(b.points)
		from 
		' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity a, ' + QuoteName(@DBName) + N'.dbo.history b
		where b.histdate>@enddate and b.ratio=''1'' and b.tipnumber=a.tipnumber '	 */	
Exec sp_executesql @SQLUpdate, N'@EndDate datetime', @EndDate=@EndDate

/* Load the current activity table with decreases for the current month         */
--update Current_Month_Activity
--set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1' and history.tipnumber=Current_Month_Activity.tipnumber)
--where exists(select * from history where histdate>@enddate and ratio='-1' and history.tipnumber=Current_Month_Activity.tipnumber)
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set decreases=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where histdate>@enddate and ratio=''-1'' and ' + QuoteName(@DBName) + N'.dbo.history.tipnumber=' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity.tipnumber) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where histdate>@enddate and ratio=''-1'' and ' + QuoteName(@DBName) + N'.dbo.history.tipnumber=' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity.tipnumber) '  
/* set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set decreases=
		sum(b.points)
		from 
		' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity a, ' + QuoteName(@DBName) + N'.dbo.history b
		where b.histdate>@enddate and b.ratio=''-1'' and b.tipnumber=a.tipnumber '	*/	
Exec sp_executesql @SQLUpdate, N'@EndDate datetime', @EndDate=@EndDate

/* Load the calculate the adjusted ending balance        */
--update Current_Month_Activity
--set adjustedendingpoints=endingpoints - increases + decreases
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Current_Month_Activity set adjustedendingpoints=endingpoints - increases + decreases '
Exec sp_executesql @SQLUpdate
GO
