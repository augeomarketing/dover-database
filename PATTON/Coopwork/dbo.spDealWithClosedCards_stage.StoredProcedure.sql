SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDealWithClosedCards_stage] @TipFirst char(3), @Enddate char(10)
AS 
/************************************************************************************/
/*                                                                                  */
/*  Acctid in Affiliat table is actually the last six of cardno                     */
/*  Secid in Affiliat table is actually the Acctno (DDA#) for Spirit Bank           */
/*  Custid in Affiliat table is actually the CIS# for Spirit Bank                   */
/*                                                                                  */
/************************************************************************************/

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
--set @DBName='zz617OmniCCUConsumer'

declare @datedeleted datetime
set @datedeleted = @Enddate

--update the AFFILIAT_stage table setting to CLOSED where FI.CustIN.Acct_Num does not exist in the affiliat table
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.affiliat_stage set acctstatus = ''C'' where not exists(select * from ' + QuoteName(@DBName) + N' .dbo.custin where ' + QuoteName(@DBName) + N' .dbo.custin.acct_num= ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.acctid) '
exec sp_executesql @SQLUpdate

--Update the CUSTOMER_stage  table setting to CLOSED those active accounts whose tipnumber is NOT in the affiliat table and whose affiliat.AcctStatus is 'A'
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer_stage set status = ''C'' where status=''A'' and not exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat_stage where ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer_stage.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.acctstatus=''A'') '
exec sp_executesql @SQLUpdate

/* added 7/2007 */
-- Update the CUSTOMER_stage  table setting to ACTIVE those closed accounts whose tipnumber IS in the Affiliat table and whose affiliat.acctstatus = 'A'
set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.customer_stage set status = ''A'' where status=''C'' and exists(select * from ' + QuoteName(@DBName) + N' .dbo.affiliat_stage where ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.tipnumber= ' + QuoteName(@DBName) + N' .dbo.customer_stage.tipnumber and ' + QuoteName(@DBName) + N' .dbo.affiliat_stage.acctstatus=''A'') '
exec sp_executesql @SQLUpdate
GO
