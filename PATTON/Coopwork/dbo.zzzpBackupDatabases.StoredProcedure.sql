SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[zzzpBackupDatabases] @Tipfirst char(3)
as

declare @spath varchar(100), @sDBName varchar(100), @sBackupPath varchar(100), @cleardbs varchar(100)
set @sPath = 'd:\SQLData\BACKUPS\'

DECLARE cur_databases CURSOR FOR
	select name from master.dbo.sysdatabases where name not in ('tempdb') and name like @Tipfirst
	OPEN cur_databases 
	fetch next  from cur_databases into @sDBName
	
WHILE (@@FETCH_STATUS=0)
	BEGIN
-- Clear out backups in this folder
-- Uncomment if you like (clears out databases that have since been removed)
-- However this may defeat the purpose of this script, since the creation dates will have changed on all files
-- set @cleardbs = 'del "' + @sPath + '*.bak"'
-- exec master..xp_cmdshell @cleardbs
	   set @sBackupPath = @sPath + @sDBName
   	   backup database @sDBName to disk = @sBackupPath with init
   	   fetch next  from cur_databases into @sDBName
	END

close cur_databases
deallocate cur_databases
GO
