SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MerchantInfo](
	[TipID] [nchar](4) NULL,
	[TERMID] [nchar](8) NULL,
	[ACCEPTORID] [nchar](15) NULL,
	[MerchantName] [nvarchar](100) NULL
) ON [PRIMARY]
GO
