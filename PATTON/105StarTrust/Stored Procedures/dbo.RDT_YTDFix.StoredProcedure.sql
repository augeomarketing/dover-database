USE [105StarTrust]
GO
/****** Object:  StoredProcedure [dbo].[RDT_YTDFix]    Script Date: 10/01/2009 15:42:11 ******/
DROP PROCEDURE [dbo].[RDT_YTDFix]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*** RDT 

add overage to history where histdate > 2008/01/01
set overage to 0 
recalc customer avaail, & balance
Recalc Affiliat YTDEarned set to total transactions in 2008 
rerun process from PUSH TO Web.

**/
CREATE procedure [dbo].[RDT_YTDFix] as 

-- Add overages to 2008 history records 
update History set points = points + overage where histdate > '2008-01-01'  

-- recalculate customer runAvailable from ALL of history
Declare @TipAvail Table ( Tip Char(15), avail int) 

Insert into @TipAvail 	Select Tipnumber, sum(Points*Ratio) from History Group by Tipnumber 

Update Customer  
	set Runavailable = t.Avail
	from Customer C, @TipAvail t where C.tipnumber = t.tip 

-- Recalc customer runbalance 
Update Customer  set Runbalance  = Runavailable + RunRedeemed

-- zero out YTDearned 
Update Affiliat  set YTDEarned = 0

-- Add YTDEarned from 2008 history EARNINGS records 
Update Affiliat  
	set YTDEarned = YTDEarned + ( h.points * ratio )
	from history h join Affiliat c on h.acctid = c.acctid
	where histdate > '2008-01-01'  and Trancode in  ('63', '67')

Update Affiliat  
	set YTDEarned = YTDEarned + ( h.points * ratio )
	from history h join Affiliat c on h.acctid = c.acctid
	where histdate > '2008-01-01'  and Trancode in  ('33', '37')

-- Clear the Overage column for 2008 History
Update History  	set Overage = 0 where histdate > '2008-01-01'  

--
GO
