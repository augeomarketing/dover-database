SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixomni](
	[tipnumber] [char](20) NOT NULL,
	[lastname] [varchar](50) NOT NULL,
	[lastsix] [char](6) NOT NULL,
	[cardnumber] [char](16) NULL,
	[dda] [char](25) NULL,
	[ssn] [char](9) NULL,
	[demoname] [char](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
