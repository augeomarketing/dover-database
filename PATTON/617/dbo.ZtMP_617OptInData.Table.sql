SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZtMP_617OptInData](
	[CardNumOut] [nvarchar](16) NULL,
	[AccountOut] [nvarchar](255) NULL,
	[BIN] [nvarchar](9) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
