USE [617OmniCCUConsumer]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File_V1]    Script Date: 11/16/2012 14:55:49 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Begin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Begin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Begin]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_End]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_End]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_End]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Add]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Subtract]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Net]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Net]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Net]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Bonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Bonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Bonus]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Redeem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Redeem]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_ToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_ToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_ToExpire]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_CRD_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_CRD_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_CRD_Purch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_CRD_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_CRD_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_CRD_Return]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_DBT_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_DBT_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_DBT_Purch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_DBT_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_DBT_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_DBT_Return]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_ADJ_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_ADJ_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_ADJ_Add]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_ADJ_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_ADJ_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_ADJ_Subtract]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_PointsBonusMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_PointsBonusMN]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_PP_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_PP_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] DROP CONSTRAINT [DF_Quarterly_Statement_File_V1_PP_Purch]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File_V1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File_V1](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[TOT_Begin] [numeric](18, 0) NULL,
	[TOT_End] [numeric](18, 0) NULL,
	[TOT_Add] [numeric](18, 0) NULL,
	[TOT_Subtract] [numeric](18, 0) NULL,
	[TOT_Net] [numeric](18, 0) NULL,
	[TOT_Bonus] [numeric](18, 0) NULL,
	[TOT_Redeem] [numeric](18, 0) NULL,
	[TOT_ToExpire] [numeric](18, 0) NULL,
	[CRD_Purch] [numeric](18, 0) NULL,
	[CRD_Return] [numeric](18, 0) NULL,
	[DBT_Purch] [numeric](18, 0) NULL,
	[DBT_Return] [numeric](18, 0) NULL,
	[ADJ_Add] [numeric](18, 0) NULL,
	[ADJ_Subtract] [numeric](18, 0) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL,
	[PP_Purch] [numeric](18, 0) NULL,
	[AcctNumber] [varchar](25) NULL,
 CONSTRAINT [PK_Tipnumber] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Begin]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Begin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Begin]  DEFAULT ((0)) FOR [TOT_Begin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_End]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_End]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_End]  DEFAULT ((0)) FOR [TOT_End]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Add]  DEFAULT ((0)) FOR [TOT_Add]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Subtract]  DEFAULT ((0)) FOR [TOT_Subtract]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Net]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Net]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Net]  DEFAULT ((0)) FOR [TOT_Net]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Bonus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Bonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Bonus]  DEFAULT ((0)) FOR [TOT_Bonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_Redeem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_Redeem]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_Redeem]  DEFAULT ((0)) FOR [TOT_Redeem]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_TOT_ToExpire]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_TOT_ToExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_TOT_ToExpire]  DEFAULT ((0)) FOR [TOT_ToExpire]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_CRD_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_CRD_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_CRD_Purch]  DEFAULT ((0)) FOR [CRD_Purch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_CRD_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_CRD_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_CRD_Return]  DEFAULT ((0)) FOR [CRD_Return]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_DBT_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_DBT_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_DBT_Purch]  DEFAULT ((0)) FOR [DBT_Purch]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_DBT_Return]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_DBT_Return]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_DBT_Return]  DEFAULT ((0)) FOR [DBT_Return]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_ADJ_Add]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_ADJ_Add]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_ADJ_Add]  DEFAULT ((0)) FOR [ADJ_Add]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_ADJ_Subtract]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_ADJ_Subtract]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_ADJ_Subtract]  DEFAULT ((0)) FOR [ADJ_Subtract]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_PointsBonusMN]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_V1_PP_Purch]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File_V1]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_V1_PP_Purch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File_V1] ADD  CONSTRAINT [DF_Quarterly_Statement_File_V1_PP_Purch]  DEFAULT ((0)) FOR [PP_Purch]
END


End
GO
