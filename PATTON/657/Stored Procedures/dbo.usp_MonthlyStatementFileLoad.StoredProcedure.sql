USE [657]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 05/12/2014 13:35:20 ******/
DROP PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]  
       @tipfirst               varchar(3),
        @StartDateParm          datetime, 
        @EndDateParm            datetime,
        @debug                  int = 0
AS
 
-- Changed to load PointsBegin using calc instead of lookup in Beginning Balance Table.

-- SEB 11/2013 Added codes H0 H9
-- SEB 5/2014 Added codes 35, 65 in debit area

Declare     @MonthBegin         char(2)
Declare     @StartDate          DateTime
Declare     @EndDate            DateTime

declare     @dbname             nvarchar(50)
declare     @sql                nvarchar(max)

declare		@Trancode			varchar(2)
declare		@Ratio				int
declare		@tcdesc				varchar(40)
declare @nbrrows int = 0
declare @ctr    int = 1

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 


set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

set @MonthBegin = month(@StartDate)

if @debug = 1
BEGIN
    print @dbname
    print @Startdate 
    print @Enddate
    print @monthbegin
END


/* Load the statement file from the customer table  */

set @sql = 'delete from ' + @dbname + '.dbo.Monthly_Statement_File'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql

set @sql = 'insert into ' + @dbname + '.dbo.Monthly_Statement_File
            (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
            select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
            from ' + @dbname + '.dbo.customer_Stage'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
    
set @sql = 'update ' + @dbname + '.dbo.Monthly_Statement_File
			set Acctnum = RIGHT(RTRIM(af.acctid),4)
			from ' + @dbname + '.dbo.Monthly_Statement_File msf inner join ' + @dbname + '.dbo.affiliat_Stage af
					on msf.tipnumber = af.tipnumber
			where af.accttype=''DEFAULT'' '    
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
 
/* Load the statmement file with CREDIT purchases          */
set @sql = 'update ' + @dbname + '.dbo.Monthly_Statement_File 
	            set	pointspurchasedCR = isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''63''), 0),
		            pointsreturnedCR =	isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''33''), 0),
		            pointspurchasedDB =	isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''67''), 0)+
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''65''), 0),
		            pointsreturnedDB =	isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''37''), 0)+
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''35''), 0),

		            pointsadded =		isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''IE''), 0) +
						            isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''DR''), 0) +
						            isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''GB''), 0) +						            
						            isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''TP''), 0),

		            pointssubtracted =	isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''DE''), 0) +
						            isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''EP''), 0) +
						            isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''XP''), 0),

		           PurchasedPoints =	isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''PP''), 0),
					
				   PointsBonusMER = 	isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''F0''), 0) +
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''G0''), 0) -
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''H0''), 0) -
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''F9''), 0) -
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''G9''), 0) -
										isnull(' + @dbname + '.dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''H9''), 0)   '
    exec sp_executesql @sql, N'@startdate datetime, @enddate datetime', @startdate = @startdate, @EndDate = @EndDate
/* Load the statmement file with bonuses            */
--
set @sql = N'update ' + @dbname + '.dbo.Monthly_Statement_File
	set pointsbonusDB=(select sum(points*ratio) 
				    from ' + @dbname + '.dbo.History_Stage 
				    where tipnumber=' + @dbname + '.dbo.Monthly_Statement_File.tipnumber and 
						  histdate>=@startdate and histdate<=@enddate and ((trancode like ''B%'' or trancode like ''F%'') and trancode not in (''F0'', ''G0'', ''H0'', ''F9'', ''G9'', ''H9'')) )
	where exists(select * from ' + @dbname + '.dbo.History_Stage where tipnumber=' + @dbname + '.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and ((trancode like ''B%'' or trancode like ''F%'') and trancode not in (''F0'', ''G0'', ''H0'', ''F9'', ''G9'', ''H9''))) '
if @debug = 1
    print @sql
else
    exec sp_executesql @sql, N'@startdate datetime, @enddate datetime', @startdate = @startdate, @EndDate = @EndDate


/* Load the statmement file with total point increases */
set @sql = N'update ' + @dbname + '.dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonusDB + pointsadded + pointsbonusMER + PurchasedPoints '
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
    
/* Load the statmement file with redemptions          */
set @sql = N'update ' + @dbname + '.dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) from ' + @dbname + '.dbo.History_Stage where tipnumber= ' + @dbname + '.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')
where exists(select * from ' + @dbname + '.dbo.History_Stage where tipnumber= ' + @dbname + '.dbo.Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'') '
if @debug = 1
    print @sql
else
    exec sp_executesql @sql, N'@startdate datetime, @enddate datetime', @startdate = @startdate, @EndDate = @EndDate
    

/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
set @sql = N'update ' + @dbname + '.dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted '
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
    


/* Load the statmement file with the Beginning balance for the Month */
set @sql = N'update ' + @dbname + '.dbo.Monthly_Statement_File 
set pointsbegin = (select isnull(SUM(points * ratio),0)
					from ' + @dbname + '.dbo.history 
					where Tipnumber = ' + @dbname + '.dbo.Monthly_Statement_File.TIPNUMBER
					and histdate < @Startdate) '
if @debug = 1
    print @sql
else
    exec sp_executesql @sql, N'@startdate datetime', @startdate = @startdate
    

/* Load the statmement file with beginning points */
set @sql = N'update ' + @dbname + '.dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased '
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
GO
