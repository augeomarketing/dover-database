USE [Deliverables]
GO
/****** Object:  Table [dbo].[contact]    Script Date: 01/12/2010 14:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[sid_contact_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_contact_email] [nvarchar](500) NULL,
	[dim_contact_lastmodified] [datetime] NULL,
	[dim_contact_created] [datetime] NULL,
	[dim_contact_active] [int] NULL,
	[dim_contact_deliv] [nvarchar](50) NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[sid_contact_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
