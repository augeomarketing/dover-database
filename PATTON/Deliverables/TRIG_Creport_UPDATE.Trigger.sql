USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_Creport_UPDATE]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_Creport_UPDATE] ON [dbo].[Creport] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_Creport_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_Creport_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_Creport_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE Creport SET dim_Creport_lastmodified = getdate() WHERE sid_Creport_id = @sid_Creport_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_Creport_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
