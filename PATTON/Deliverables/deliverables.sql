SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Creport]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Creport](
	[sid_creport_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_creport_creport] [ntext] NOT NULL,
	[dim_creport_path] [ntext] NOT NULL,
	[dim_creport_created] [datetime] NOT NULL CONSTRAINT [DF_Creport_dim_creport_created]  DEFAULT (getdate()),
	[dim_creport_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_Creport_dim_creport_lastmodified]  DEFAULT (getdate()),
	[dim_creport_Active] [int] NOT NULL CONSTRAINT [DF_Creport_dim_creport_Active]  DEFAULT (1)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_Creport_UPDATE] ON [dbo].[Creport] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_Creport_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_Creport_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_Creport_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE Creport SET dim_Creport_lastmodified = getdate() WHERE sid_Creport_id = @sid_Creport_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_Creport_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fiinfo]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[fiinfo](
	[Sid_fiinfo_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_fiinfo_tip] [nchar](5) NOT NULL,
	[dim_fiinfo_dbname] [nvarchar](50) NOT NULL,
	[dim_fiinfo_shrtdbname] [nvarchar](50) NOT NULL,
	[dim_fiinfo_netpath] [text] NULL,
	[sid_processor_id] [int] NULL CONSTRAINT [DF_fiinfo_sid_processor_id]  DEFAULT (13),
	[sid_contact_id] [int] NULL CONSTRAINT [DF_fiinfo_sid_contact_id]  DEFAULT (97),
	[dim_fiinfo_delivtype] [nvarchar](50) NULL,
	[dim_fiinfo_created] [datetime] NOT NULL CONSTRAINT [DF_fiinfo_dim_fiinfo_created]  DEFAULT (getdate()),
	[dim_fiinfo_active] [int] NOT NULL CONSTRAINT [DF_fiinfo_dim_fiinfo_active]  DEFAULT (1),
	[dim_fiinfo_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_fiinfo_dim_fiinfo_lastmodified]  DEFAULT (getdate()),
 CONSTRAINT [PK_fiinfo] PRIMARY KEY CLUSTERED 
(
	[Sid_fiinfo_id] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_fiinfo_UPDATE] ON [dbo].[fiinfo] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_fiinfo_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_fiinfo_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_fiinfo_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE fiinfo SET dim_fiinfo_lastmodified = getdate() WHERE sid_fiinfo_id = @sid_fiinfo_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_fiinfo_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Processor]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Processor](
	[sid_Processor_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_processor_fname] [nvarchar](50) NOT NULL,
	[dim_processor_lname] [nvarchar](50) NOT NULL,
	[dim_processor_ext] [int] NOT NULL,
	[dim_processor_dept] [nvarchar](50) NOT NULL,
	[dim_processor_supervisor] [int] NULL,
	[dim_processor_active] [int] NOT NULL CONSTRAINT [DF_Processor_dim_processor_active]  DEFAULT (1),
	[dim_processor_created] [datetime] NOT NULL CONSTRAINT [DF_Processor_dim_processor_created]  DEFAULT (getdate()),
	[dim_processor_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_Processor_dim_processor_lastmodified]  DEFAULT (getdate()),
 CONSTRAINT [PK_Processor] PRIMARY KEY CLUSTERED 
(
	[sid_Processor_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_processor_UPDATE] ON [dbo].[Processor] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_processor_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_processor_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_processor_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE processor SET dim_processor_lastmodified = getdate() WHERE sid_processor_id = @sid_processor_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_processor_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[eventtype]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[eventtype](
	[sid_eventtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_eventtype_type] [nvarchar](500) NOT NULL,
	[dim_eventtype_active] [int] NOT NULL CONSTRAINT [DF_eventtype_dim_eventtype_active]  DEFAULT (1),
	[dim_eventtype_created] [datetime] NOT NULL CONSTRAINT [DF_eventtype_dim_eventtype_created]  DEFAULT (getdate()),
	[dim_eventtype_Lastmodified] [datetime] NOT NULL CONSTRAINT [DF_eventtype_dim_eventtype_Lastmodified]  DEFAULT (getdate()),
 CONSTRAINT [PK_eventtype] PRIMARY KEY CLUSTERED 
(
	[sid_eventtype_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_eventtype_UPDATE] ON [dbo].[eventtype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_eventtype_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_eventtype_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_eventtype_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE eventtype SET dim_eventtype_lastmodified = getdate() WHERE sid_eventtype_id = @sid_eventtype_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_eventtype_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tracker]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tracker](
	[sid_tracker_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_fiinfo_id] [int] NULL,
	[dim_tracker_dateshown] [datetime] NULL,
	[sid_eventtype_id] [int] NULL,
	[sid_contact_id] [nchar](10) NULL,
	[dim_tracker_contacttarget] [nvarchar](500) NULL,
	[dim_tracker_filepath] [nvarchar](500) NULL,
	[dim_tracker_Notes] [nvarchar](1000) NULL,
	[dim_tracker_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_tracker_dim_tracker_lastmodified]  DEFAULT (getdate()),
	[dim_tracker_created] [datetime] NOT NULL CONSTRAINT [DF_tracker_dim_tracker_created]  DEFAULT (getdate()),
	[dim_tracker_active] [int] NOT NULL CONSTRAINT [DF_tracker_dim_tracker_active]  DEFAULT (1),
 CONSTRAINT [PK_tracker] PRIMARY KEY CLUSTERED 
(
	[sid_tracker_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_Tracker_UPDATE] ON [dbo].[tracker] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_Tracker_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_Tracker_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_Tracker_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE Tracker SET dim_Tracker_lastmodified = getdate() WHERE sid_Tracker_id = @sid_Tracker_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_Tracker_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Job]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Job](
	[sid_job_id] [int] IDENTITY(518,1) NOT NULL,
	[sid_creport_id] [int] NULL,
	[sid_fiinfo_id] [char](3) NULL,
	[dim_Job_format] [text] NULL,
	[dim_job_fileformat] [nvarchar](50) NULL,
	[dim_job_upload] [nvarchar](50) NULL,
	[dim_job_lastmodified] [datetime] NOT NULL CONSTRAINT [DF_Job_dim_job_lastmodified]  DEFAULT (getdate()),
	[dim_job_created] [datetime] NULL CONSTRAINT [DF_Job_dim_job_created]  DEFAULT (getdate()),
	[dim_job_active] [int] NOT NULL CONSTRAINT [DF_Job_dim_job_active]  DEFAULT (1),
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[sid_job_id] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_job_UPDATE] ON [dbo].[Job] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_job_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_job_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_job_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE job SET dim_job_lastmodified = getdate() WHERE sid_job_id = @sid_job_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_job_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[liab]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[liab](
	[sid_liab_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_fiinfo_id] [char](3) NULL,
	[dim_liab_format] [text] NULL,
	[dim_liab_fileformat] [nvarchar](50) NULL,
	[dim_liab_upload] [nvarchar](50) NULL,
	[dim_liab_lastmodified] [datetime] NULL CONSTRAINT [DF_liab_dim_liab_lastmodified]  DEFAULT (getdate()),
	[dim_liab_created] [datetime] NULL CONSTRAINT [DF_liab_dim_liab_created]  DEFAULT (getdate()),
	[dim_liab_active] [int] NULL CONSTRAINT [DF_liab_dim_liab_active]  DEFAULT (1),
 CONSTRAINT [PK_liab] PRIMARY KEY CLUSTERED 
(
	[sid_liab_id] ASC
) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_liab_UPDATE] ON [dbo].[liab] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_liab_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_liab_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_liab_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE liab SET dim_liab_lastmodified = getdate() WHERE sid_liab_id = @sid_liab_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_liab_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[contact]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[contact](
	[sid_contact_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_contact_email] [nvarchar](500) NULL,
	[dim_contact_lastmodified] [datetime] NULL CONSTRAINT [DF_Contactfix_dim_contact_lastmodified]  DEFAULT (getdate()),
	[dim_contact_created] [datetime] NULL CONSTRAINT [DF_Contactfix_dim_contact_created]  DEFAULT (getdate()),
	[dim_contact_active] [int] NULL CONSTRAINT [DF_Contactfix_dim_contact_active]  DEFAULT (1),
	[dim_contact_deliv] [nvarchar](250) NULL,
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[sid_contact_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [TRIG_contactfix_UPDATE] ON [dbo].[contact] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_contact_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_contact_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_contact_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE contact SET dim_contact_lastmodified = getdate() WHERE sid_contact_id = @sid_contact_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_contact_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vw_liab]') AND OBJECTPROPERTY(id, N'IsView') = 1)
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vw_liab]
AS
SELECT     TOP 100 PERCENT liab_1.sid_liab_id, dbo.Processor.sid_Processor_id, dbo.Processor.dim_processor_fname, dbo.fiinfo.Sid_fiinfo_id, 
                      dbo.fiinfo.dim_fiinfo_tip, dbo.fiinfo.dim_fiinfo_dbname, dbo.fiinfo.Sid_fiinfo_id AS Expr1, dbo.fiinfo.dim_fiinfo_shrtdbname, liab_1.dim_liab_fileformat, 
                      liab_1.dim_liab_format, liab_1.dim_liab_upload, dbo.Contact.dim_contact_email
FROM         dbo.fiinfo INNER JOIN
                      dbo.Processor ON dbo.fiinfo.sid_processor_id = dbo.Processor.sid_Processor_id INNER JOIN
                      dbo.liab AS liab_1 ON dbo.fiinfo.Sid_fiinfo_id = liab_1.sid_fiinfo_id INNER JOIN
                      dbo.Contact ON dbo.fiinfo.sid_contact_id = dbo.Contact.sid_contact_id
WHERE     (liab_1.dim_liab_active = 1)
ORDER BY dbo.fiinfo.dim_fiinfo_tip
' 
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "fiinfo"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 61
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Processor"
            Begin Extent = 
               Top = 6
               Left = 228
               Bottom = 61
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "liab_1"
            Begin Extent = 
               Top = 6
               Left = 418
               Bottom = 61
               Right = 570
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contact"
            Begin Extent = 
               Top = 6
               Left = 608
               Bottom = 121
               Right = 810
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_liab'

GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_liab'

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vw_tracker]') AND OBJECTPROPERTY(id, N'IsView') = 1)
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vw_tracker]
AS
SELECT     dbo.fiinfo.dim_fiinfo_tip, RewardsNOW.dbo.dbprocessinfo.DBNameNEXL, RewardsNOW.dbo.dbprocessinfo.ClientCode, 
                      dbo.tracker.dim_tracker_dateshown, dbo.fiinfo.dim_fiinfo_delivtype, dbo.tracker.dim_tracker_contacttarget, dbo.tracker.dim_tracker_filepath, 
                      dbo.tracker.dim_tracker_Notes
FROM         dbo.tracker INNER JOIN
                      dbo.fiinfo ON dbo.fiinfo.Sid_fiinfo_id = dbo.tracker.sid_fiinfo_id INNER JOIN
                      RewardsNOW.dbo.dbprocessinfo ON RewardsNOW.dbo.dbprocessinfo.DBNumber = dbo.fiinfo.dim_fiinfo_tip
' 
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -288
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tracker"
            Begin Extent = 
               Top = 6
               Left = 508
               Bottom = 121
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "fiinfo"
            Begin Extent = 
               Top = 6
               Left = 280
               Bottom = 121
               Right = 470
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "dbprocessinfo (RewardsNOW.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3000
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_tracker'

GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_tracker'

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vw_Job]') AND OBJECTPROPERTY(id, N'IsView') = 1)
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[vw_Job]
AS
SELECT     TOP 100 PERCENT Job_1.sid_job_id, dbo.Processor.sid_Processor_id, dbo.Processor.dim_processor_fname, dbo.fiinfo.Sid_fiinfo_id, 
                      dbo.fiinfo.dim_fiinfo_tip, dbo.fiinfo.dim_fiinfo_dbname, dbo.fiinfo.Sid_fiinfo_id AS Expr1, dbo.fiinfo.dim_fiinfo_shrtdbname, Job_1.dim_job_fileformat, 
                      Job_1.dim_Job_format, Job_1.dim_job_upload, dbo.contact.dim_contact_email, dbo.contact.sid_contact_id
FROM         dbo.fiinfo INNER JOIN
                      dbo.Processor ON dbo.fiinfo.sid_processor_id = dbo.Processor.sid_Processor_id INNER JOIN
                      dbo.Job AS Job_1 ON dbo.fiinfo.Sid_fiinfo_id = Job_1.sid_fiinfo_id INNER JOIN
                      dbo.contact ON dbo.fiinfo.sid_contact_id = dbo.contact.sid_contact_id
WHERE     (Job_1.dim_job_active = 1)
ORDER BY dbo.fiinfo.dim_fiinfo_tip
' 
GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "fiinfo"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Processor"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Job_1"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 121
               Right = 447
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "contact"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 361
               Right = 240
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
  ' ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_Job'

GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' End
End
' ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_Job'

GO
EXEC dbo.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 ,@level0type=N'USER', @level0name=N'dbo', @level1type=N'VIEW', @level1name=N'vw_Job'

