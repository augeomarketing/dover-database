USE [Deliverables]
GO
/****** Object:  Table [dbo].[Job]    Script Date: 01/12/2010 14:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Job](
	[sid_job_id] [int] IDENTITY(518,1) NOT NULL,
	[sid_creport_id] [int] NULL,
	[sid_fiinfo_id] [char](3) NULL,
	[dim_Job_format] [text] NULL,
	[dim_job_fileformat] [nvarchar](50) NULL,
	[dim_job_upload] [nvarchar](50) NULL,
	[dim_job_lastmodified] [datetime] NOT NULL,
	[dim_job_created] [datetime] NULL,
	[dim_job_active] [int] NOT NULL,
 CONSTRAINT [PK_Job] PRIMARY KEY CLUSTERED 
(
	[sid_job_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
