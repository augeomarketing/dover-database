USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_job_UPDATE]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_job_UPDATE] ON [dbo].[Job] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_job_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_job_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_job_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE job SET dim_job_lastmodified = getdate() WHERE sid_job_id = @sid_job_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_job_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
