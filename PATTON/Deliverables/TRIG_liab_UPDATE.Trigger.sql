USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_liab_UPDATE]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_liab_UPDATE] ON [dbo].[liab] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_liab_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_liab_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_liab_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE liab SET dim_liab_lastmodified = getdate() WHERE sid_liab_id = @sid_liab_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_liab_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
