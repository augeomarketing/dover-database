USE [Deliverables]
GO
/****** Object:  Table [dbo].[Processor]    Script Date: 01/12/2010 14:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Processor](
	[sid_Processor_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_processor_fname] [nvarchar](50) NOT NULL,
	[dim_processor_lname] [nvarchar](50) NOT NULL,
	[dim_processor_ext] [int] NOT NULL,
	[dim_processor_dept] [nvarchar](50) NOT NULL,
	[dim_processor_supervisor] [int] NULL,
	[dim_processor_active] [int] NOT NULL,
	[dim_processor_created] [datetime] NOT NULL,
	[dim_processor_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_Processor] PRIMARY KEY CLUSTERED 
(
	[sid_Processor_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
