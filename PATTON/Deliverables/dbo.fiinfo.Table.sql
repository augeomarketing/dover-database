USE [Deliverables]
GO
/****** Object:  Table [dbo].[fiinfo]    Script Date: 01/12/2010 14:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fiinfo](
	[Sid_fiinfo_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_fiinfo_tip] [nchar](5) NOT NULL,
	[dim_fiinfo_dbname] [nvarchar](50) NOT NULL,
	[dim_fiinfo_shrtdbname] [nvarchar](50) NOT NULL,
	[dim_fiinfo_netpath] [text] NULL,
	[sid_processor_id] [int] NULL,
	[sid_contact_id] [int] NULL,
	[dim_fiinfo_delivtype] [nvarchar](50) NULL,
	[dim_fiinfo_created] [datetime] NOT NULL,
	[dim_fiinfo_active] [int] NOT NULL,
	[dim_fiinfo_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_fiinfo] PRIMARY KEY CLUSTERED 
(
	[Sid_fiinfo_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
