USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_eventtype_UPDATE]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_eventtype_UPDATE] ON [dbo].[eventtype] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_eventtype_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_eventtype_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_eventtype_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE eventtype SET dim_eventtype_lastmodified = getdate() WHERE sid_eventtype_id = @sid_eventtype_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_eventtype_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
