use deliverables
GO

if object_id('vw_job') is not null
	drop view dbo.vw_job
GO

CREATE VIEW [dbo].[vw_Job]
AS
SELECT TOP 100 PERCENT j.sid_job_id, p.sid_Processor_id, p.dim_processor_fname, fi.Sid_fiinfo_id, 
		fi.dim_fiinfo_tip, fi.dim_fiinfo_dbname, fi.Sid_fiinfo_id AS Expr1, 
		fi.dim_fiinfo_shrtdbname, j.dim_job_fileformat, j.dim_Job_format, j.dim_job_upload, 
		c.dim_contact_email, c.sid_contact_id
FROM dbo.fiinfo fi join dbo.Processor P 
	ON fi.sid_processor_id = p.sid_Processor_id 
JOIN dbo.Job AS J
	ON fi.Sid_fiinfo_id = j.sid_fiinfo_id 
JOIN dbo.contact c 
	ON fi.sid_contact_id = c.sid_contact_id
join rewardsnow.dbo.dbprocessinfo dbpi
	on fi.dim_fiinfo_tip = dbpi.dbnumber
	
WHERE     (j.dim_job_active = 1)
ORDER BY fi.dim_fiinfo_tip

GO
