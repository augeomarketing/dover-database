USE [Deliverables]
GO
/****** Object:  Table [dbo].[Creport]    Script Date: 01/12/2010 14:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Creport](
	[sid_creport_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_creport_creport] [ntext] NOT NULL,
	[dim_creport_path] [ntext] NOT NULL,
	[dim_creport_created] [datetime] NOT NULL,
	[dim_creport_lastmodified] [datetime] NOT NULL,
	[dim_creport_Active] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
