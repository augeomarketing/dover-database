USE [Deliverables]
GO
/****** Object:  Table [dbo].[liab]    Script Date: 01/12/2010 14:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[liab](
	[sid_liab_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_fiinfo_id] [char](3) NULL,
	[dim_liab_format] [text] NULL,
	[dim_liab_fileformat] [nvarchar](50) NULL,
	[dim_liab_upload] [nvarchar](50) NULL,
	[dim_liab_lastmodified] [datetime] NULL,
	[dim_liab_created] [datetime] NULL,
	[dim_liab_active] [int] NULL,
 CONSTRAINT [PK_liab] PRIMARY KEY CLUSTERED 
(
	[sid_liab_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
