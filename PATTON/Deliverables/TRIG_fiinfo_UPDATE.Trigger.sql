USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_fiinfo_UPDATE]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_fiinfo_UPDATE] ON [dbo].[fiinfo] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_fiinfo_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_fiinfo_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_fiinfo_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE fiinfo SET dim_fiinfo_lastmodified = getdate() WHERE sid_fiinfo_id = @sid_fiinfo_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_fiinfo_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
