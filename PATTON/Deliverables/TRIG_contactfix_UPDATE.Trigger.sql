USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_contactfix_UPDATE]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_contactfix_UPDATE] ON [dbo].[contact] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_contact_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_contact_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_contact_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE contact SET dim_contact_lastmodified = getdate() WHERE sid_contact_id = @sid_contact_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_contact_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
