USE [Deliverables]
GO
/****** Object:  Table [dbo].[eventtype]    Script Date: 01/12/2010 14:03:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eventtype](
	[sid_eventtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_eventtype_type] [nvarchar](500) NOT NULL,
	[dim_eventtype_active] [int] NOT NULL,
	[dim_eventtype_created] [datetime] NOT NULL,
	[dim_eventtype_Lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_eventtype] PRIMARY KEY CLUSTERED 
(
	[sid_eventtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
