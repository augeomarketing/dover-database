USE [Deliverables]
GO
/****** Object:  View [dbo].[vw_liab]    Script Date: 01/12/2010 14:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_liab]
AS
SELECT     TOP 100 PERCENT liab_1.sid_liab_id, dbo.Processor.sid_Processor_id, dbo.Processor.dim_processor_fname, dbo.fiinfo.Sid_fiinfo_id, 
                      dbo.fiinfo.dim_fiinfo_tip, dbo.fiinfo.dim_fiinfo_dbname, dbo.fiinfo.Sid_fiinfo_id AS Expr1, dbo.fiinfo.dim_fiinfo_shrtdbname, liab_1.dim_liab_fileformat, 
                      liab_1.dim_liab_format, liab_1.dim_liab_upload, dbo.Contact.dim_contact_email
FROM         dbo.fiinfo INNER JOIN
                      dbo.Processor ON dbo.fiinfo.sid_processor_id = dbo.Processor.sid_Processor_id INNER JOIN
                      dbo.liab AS liab_1 ON dbo.fiinfo.Sid_fiinfo_id = liab_1.sid_fiinfo_id INNER JOIN
                      dbo.Contact ON dbo.fiinfo.sid_contact_id = dbo.Contact.sid_contact_id
WHERE     (liab_1.dim_liab_active = 1)
ORDER BY dbo.fiinfo.dim_fiinfo_tip
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "fiinfo"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 61
               Right = 190
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Processor"
            Begin Extent = 
               Top = 6
               Left = 228
               Bottom = 61
               Right = 380
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "liab_1"
            Begin Extent = 
               Top = 6
               Left = 418
               Bottom = 61
               Right = 570
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Contact"
            Begin Extent = 
               Top = 6
               Left = 608
               Bottom = 121
               Right = 810
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_liab'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_liab'
GO
