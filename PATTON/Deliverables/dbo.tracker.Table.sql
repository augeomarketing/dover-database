USE [Deliverables]
GO
/****** Object:  Table [dbo].[tracker]    Script Date: 01/12/2010 14:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tracker](
	[sid_tracker_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_fiinfo_id] [int] NULL,
	[dim_tracker_dateshown] [datetime] NULL,
	[sid_eventtype_id] [int] NULL,
	[sid_contact_id] [nchar](10) NULL,
	[dim_tracker_contacttarget] [nvarchar](500) NULL,
	[dim_tracker_filepath] [nvarchar](500) NULL,
	[dim_tracker_Notes] [nvarchar](1000) NULL,
	[dim_tracker_lastmodified] [datetime] NOT NULL,
	[dim_tracker_created] [datetime] NOT NULL,
	[dim_tracker_active] [int] NOT NULL,
 CONSTRAINT [PK_tracker] PRIMARY KEY CLUSTERED 
(
	[sid_tracker_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
