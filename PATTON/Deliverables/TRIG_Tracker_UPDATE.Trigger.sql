USE [Deliverables]
GO
/****** Object:  Trigger [TRIG_Tracker_UPDATE]    Script Date: 01/12/2010 14:03:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_Tracker_UPDATE] ON [dbo].[tracker] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
      DECLARE @sid_Tracker_id INT 
      DECLARE UPD_QUERY INSENSITIVE CURSOR FOR 
           SELECT DELETED.sid_Tracker_id 
                FROM DELETED 
  
      OPEN UPD_QUERY 
             FETCH NEXT FROM UPD_QUERY INTO @sid_Tracker_id 
             WHILE (@@FETCH_STATUS = 0) 
      BEGIN 
  
              UPDATE Tracker SET dim_Tracker_lastmodified = getdate() WHERE sid_Tracker_id = @sid_Tracker_id 
       
              FETCH NEXT FROM UPD_QUERY INTO @sid_Tracker_id 
      END 
      CLOSE UPD_QUERY 
      DEALLOCATE UPD_QUERY 
 END
GO
