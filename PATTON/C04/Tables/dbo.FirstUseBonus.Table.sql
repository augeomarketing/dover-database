USE [C04]
GO
/****** Object:  Table [dbo].[FirstUseBonus]    Script Date: 02/24/2016 11:00:20 ******/
DROP TABLE [dbo].[FirstUseBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FirstUseBonus](
	[Member] [varchar](50) NULL,
	[BeginDate] [date] NULL,
	[EndDate] [date] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
