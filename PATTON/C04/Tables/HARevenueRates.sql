USE [C04]
GO

IF	OBJECT_ID('dbo.HARevenueRates', 'U') IS NOT NULL 
	DROP TABLE dbo.HARevenueRates; 
  
CREATE TABLE dbo.HARevenueRates
( Description	VARCHAR(50)
 ,MerchantID	VARCHAR(50)
 ,CentsPerMile	DECIMAL(6,5)
)

INSERT	INTO dbo.HARevenueRates
SELECT	DISTINCT Description, MerchantID, 1.64
FROM	HATransaction_OutLog
WHERE	Description like '76%'
OR		Description like 'Hele%'

INSERT	INTO dbo.HARevenueRates
SELECT	DISTINCT Description, MerchantID, 1.7
FROM	HATransaction_OutLog
WHERE	Description like '7-Eleven%'

----select * from dbo.HARevenueRates

----SELECT * INTO	dbo.wrkProgramPerformanceRpt_BKP_ASIM
----FROM			dbo.wrkProgramPerformanceRpt

----SELECT * INTO	dbo.wrkMonthYearOverYearRpt_BKP_ASIM
----FROM			dbo.wrkMonthYearOverYearRpt

----SELECT * INTO	dbo.wrkYearOverYearRpt_BKP_ASIM
----FROM			dbo.wrkYearOverYearRpt


------exec dbo.usp_Load_wrkProgramPerformanceRpt


----select COUNT(*) from dbo.wrkProgramPerformanceRpt_BKP_ASIM
----select COUNT(*) from dbo.wrkProgramPerformanceRpt

----select COUNT(*) from dbo.wrkMonthYearOverYearRpt_BKP_ASIM
----select COUNT(*) from dbo.wrkMonthYearOverYearRpt

----select COUNT(*) from dbo.wrkYearOverYearRpt_BKP_ASIM
----select COUNT(*) from dbo.wrkYearOverYearRpt



----select * from dbo.wrkProgramPerformanceRpt_BKP_ASIM
----select * from dbo.wrkProgramPerformanceRpt

----select * from dbo.wrkMonthYearOverYearRpt_BKP_ASIM
----select * from dbo.wrkMonthYearOverYearRpt

----select * from dbo.wrkYearOverYearRpt_BKP_ASIM
----select * from dbo.wrkYearOverYearRpt



------select * from dbo.wrkProgramPerformanceRpt_BKP_ASIM
------select * from dbo.wrkProgramPerformanceRpt

----_BKP_ASIM

----SELECT * INTO dbo.wrkPartnerPerformanceRpt_BKP_ASIM
----FROM dbo.wrkPartnerPerformanceRpt

----SELECT * INTO dbo.wrkPartnerPerformanceRpt_Condensed_BKP_ASIM
----FROM dbo.wrkPartnerPerformanceRpt_Condensed


----EXEC dbo.usp_Load_wrkPartnerPerformanceRpt


----SELECT * FROM dbo.wrkPartnerPerformanceRpt_BKP_ASIM
----where AccrualFileMonth=5 and AccrualFileYear=2018
---- order by 1, 2, 3

----SELECT * FROM dbo.wrkPartnerPerformanceRpt
----where AccrualFileMonth=5 and AccrualFileYear=2018
---- order by 1, 2, 3

----select * from dbo.HARevenueRates
----where MerchantID in(
---- '650000006813953'
----,'650000007077194'
----,'650000008185160'
----)

----SELECT * FROM dbo.wrkPartnerPerformanceRpt_Condensed_BKP_ASIM
----SELECT * FROM dbo.wrkPartnerPerformanceRpt_Condensed

----SELECT * INTO	dbo.wrkOE_ProgramPerformanceRpt_BKP_ASIM
----FROM			dbo.wrkOE_ProgramPerformanceRpt

----SELECT * INTO	dbo.wrkOE_MonthYearOverYearRpt_BKP_ASIM
----FROM			dbo.wrkOE_MonthYearOverYearRpt

----SELECT * INTO	dbo.wrkOE_YearOverYearRpt_BKP_ASIM
----FROM			dbo.wrkOE_YearOverYearRpt


----exec dbo.usp_Load_wrkOE_ProgramPerformanceRpt

----select * from dbo.wrkOE_ProgramPerformanceRpt_BKP_ASIM
----select * from dbo.wrkOE_ProgramPerformanceRpt

----select * from dbo.wrkOE_MonthYearOverYearRpt_BKP_ASIM
----select * from dbo.wrkOE_MonthYearOverYearRpt

----dbo.wrkOE_YearOverYearRpt_BKP_ASIM


----select * 
----  from information_schema.routines 
---- where routine_type = 'PROCEDURE'
---- and specific_name like '%usp_load%'
 
 
 
---- SELECT * INTO	dbo.wrkOE_PartnerPerformanceRpt_BKP_ASIM
---- FROM			dbo.wrkOE_PartnerPerformanceRpt
 
---- SELECT * INTO	dbo.wrkOE_PartnerPerformanceRpt_Condensed_BKP_ASIM
---- FROM			dbo.wrkOE_PartnerPerformanceRpt_Condensed
 
 
----exec dbo.usp_Load_wrkOEPartnerPerformanceRpt



---- SELECT * FROM dbo.wrkOE_PartnerPerformanceRpt_BKP_ASIM	ORDER BY 1
---- SELECT * FROM dbo.wrkOE_PartnerPerformanceRpt			 ORDER BY 1
 
---- SELECT * FROM dbo.wrkOE_PartnerPerformanceRpt_Condensed_BKP_ASIM	ORDER BY 1
---- SELECT * FROM dbo.wrkOE_PartnerPerformanceRpt_Condensed	ORDER BY 1
 
 
---- select * from INFORMATION_SCHEMA.ROUTINES
---- where SPECIFIC_NAME like 'usp_Load%'
---- order by SPECIFIC_NAME
 
 
----exec usp_Load_wrkPartnerPerformanceRpt
----exec usp_Load_wrkProgramPerformanceRpt
--exec usp_Load_wrkReconciliationRpt
-- @BeginDate	= '6/01/2018'
--,@EndDate	= '6/30/2018'


--select	PartnerCode, Src, AwardAmount, AwardPoints, SignMultiplier, miles
--FROM	[C04].[dbo].[HATransaction_OutLog]	AS TF
--WHERE	TF.PartnerCode	=	'AIMAC'

--	    SELECT
--			PartnerCode,
--			[Description],
--			RptOrder
--		FROM
--			C04.[dbo].HAPartnerCodes		
--		WHERE
--			RptOrder IS NOT NULL
--			and PartnerCode <> 'AIMHP'
--			and PartnerCode='AIMCS'
--		Order by RptOrder

----select * from dbo.wrkPartnerPerformanceRpt

----select * from dbo.wrkProgramPerformanceRpt

----select * from dbo.wrkReconciliationRpt 

 

--SELECT Filter, [Type], [Description]
--FROM MerchantCondensed  
--WHERE [Description] is not null 
--order by 1


--SELECT Filter, [Type], [Description]
--FROM MerchantCondensed  
--WHERE Description like '7-Eleven%'
--order by 1

--SELECT Filter, [Type], [Description]
--FROM MerchantCondensed  
--WHERE	Description like '76%'
--OR		Description like 'Hele%'
--order by 1

--SELECT * FROM DBO.wrkPartnerPerformanceRpt_Condensed
--where MerchantName like '%7-Eleven%'-+

--or MerchantName like '76%'
--or MerchantName like '%Hele%'
--order by 1


--	SELECT * FROM dbo.wrkReconciliationRpt
--	WHERE PartnerCode='AIMCS'
--	ORDER BY RptSort


--exec dbo.usp_Load_wrkOEPartnerPerformanceRpt
--exec dbo.usp_Load_wrkOE_ProgramPerformanceRpt
--exec dbo.usp_Load_wrkPartnerPerformanceRpt
--exec dbo.usp_Load_wrkProgramPerformanceRpt

--exec usp_Load_wrkReconciliationRpt
-- @BeginDate = '05/01/2018'
--,@EndDate = '05/31/2018'


--select * from dbo.wrkPartnerPerformanceRpt_Condensed
--order by 1,3,2

--select * from dbo.wrkPartnerPerformanceRpt
--order by 1,3,2


--select * from dbo.wrkProgramPerformanceRpt
--order by 1,2,3


--select * from dbo.wrkReconciliationRpt
--order by RptSort



