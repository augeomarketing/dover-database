USE [C04]
GO
/****** Object:  Table [dbo].[Bonus_Award_Statistics]    Script Date: 09/15/2016 11:10:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bonus_Award_Statistics](
	[MonthEndDate] [date] NOT NULL,
	[Count_Awarded] [int] NULL,
	[Points_Awarded] [int] NULL,
	[Count_Eligible] [int] NULL
) ON [PRIMARY]
GO
