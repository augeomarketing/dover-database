USE [C04]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HATransaction_OutLog]') AND type in (N'U'))
DROP TABLE [dbo].[HATransaction_OutLog]
GO

/****** Object:  Table [dbo].[HATransaction_OutLog]    Script Date: 12/21/2015 12:51:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HATransaction_OutLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OutDate] [varchar](16) NOT NULL,
	[FTPNo] [varchar](20) NULL,
	[Lastname] [varchar](35) NULL,
	[Firstname] [varchar](35) NULL,
	[ActivityDate] [datetime] NULL,
	[Miles] [int] NULL,
	[ReferenceID] [bigint] NULL,
	[PartnerCode] [varchar](5) NULL,
	[Description] [varchar](50) NULL,
	[MerchantID] [varchar](50) NULL,
	[Tipnumber] [varchar](15) NULL,
	[FsixLfour] [varchar](10) NULL,
	[Src] [varchar](2) NULL,
	[FixedWidthRec] [varchar](185) NULL,
	[SignMultiplier] [int] NOT NULL,
 CONSTRAINT [PK_HATransaction_OutLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[HATransaction_OutLog]  WITH CHECK ADD  CONSTRAINT [chk_SignMultiplier] CHECK  (([SignMultiplier]=(1) OR [SignMultiplier]=(-1)))
GO

ALTER TABLE [dbo].[HATransaction_OutLog] CHECK CONSTRAINT [chk_SignMultiplier]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'value from dim_RNITransaction_Member' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransaction_OutLog', @level2type=N'COLUMN',@level2name=N'FTPNo'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data from dim_RNITransaction_TransactionDate - YYYYMMDD format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransaction_OutLog', @level2type=N'COLUMN',@level2name=N'ActivityDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'value from dim_RNITransaction_PointsAwarded' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransaction_OutLog', @level2type=N'COLUMN',@level2name=N'Miles'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'value from sid_RNITransaction_ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransaction_OutLog', @level2type=N'COLUMN',@level2name=N'ReferenceID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data from Merchant Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransaction_OutLog', @level2type=N'COLUMN',@level2name=N'Description'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'identifier to indicate if the transactions are from Zavee (ZA) or Prospero(PR)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransaction_OutLog', @level2type=N'COLUMN',@level2name=N'Src'
GO



