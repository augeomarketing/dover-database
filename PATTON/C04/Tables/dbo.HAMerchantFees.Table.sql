USE [C04]
GO

/****** Object:  Table [dbo].[HAMerchantFees]    Script Date: 12/21/2015 13:02:06 ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HAMerchantFees]') AND type in (N'U'))
DROP TABLE [dbo].[HAMerchantFees]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HAMerchantFees](
	[MerchantFeeId] [int] IDENTITY(1,1) NOT NULL,
	[MerchantId] [varchar](50) NOT NULL,
	[MerchantFee] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_HAMerchantFees] PRIMARY KEY CLUSTERED 
(
	[MerchantFeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



