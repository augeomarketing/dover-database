USE [C04]
GO

/****** Object:  Table [dbo].[HATransactionResult_IN]    Script Date: 1/8/2016 2:46:23 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HATransactionResult_IN]') AND type in (N'U'))
DROP TABLE [dbo].[HATransactionResult_IN]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HATransactionResult_IN](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_HAFileHistory_ID] [bigint] NULL,
	[RowNum] [int] NULL,
	[HATransactionResult_IN_raw] [varchar](300) NULL,
	[FTPNo] [varchar](20) NULL,
	[Lastname] [varchar](35) NULL,
	[Firstname] [varchar](35) NULL,
	[ActivityDate] [datetime] NULL,
	[Sign] [varchar](1) NULL,
	[Miles] [int] NULL,
	[ReferenceID] [bigint] NULL,
	[PartnerCode] [varchar](5) NULL,
	[Description] [varchar](50) NULL,
	[RejectStatus] [varchar](1) NULL,
	[RejectDescription] [varchar](50) NULL,
 CONSTRAINT [PK_HATransactionResult_IN] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'value from dim_RNITransaction_Member' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransactionResult_IN', @level2type=N'COLUMN',@level2name=N'FTPNo'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data from dim_RNITransaction_TransactionDate - YYYYMMDD format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransactionResult_IN', @level2type=N'COLUMN',@level2name=N'ActivityDate'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'value from dim_RNITransaction_PointsAwarded' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransactionResult_IN', @level2type=N'COLUMN',@level2name=N'Miles'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'value from sid_RNITransaction_ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransactionResult_IN', @level2type=N'COLUMN',@level2name=N'ReferenceID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data from Merchant Name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'HATransactionResult_IN', @level2type=N'COLUMN',@level2name=N'Description'
GO



