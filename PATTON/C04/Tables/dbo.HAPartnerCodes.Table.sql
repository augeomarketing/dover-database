USE [C04]
GO

/****** Object:  Table [dbo].[HAPartnerCodes]    Script Date: 1/8/2016 2:38:35 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HAPartnerCodes]') AND type in (N'U'))
DROP TABLE [dbo].[HAPartnerCodes]
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HAPartnerCodes](
	[PartnerCodeId] [int] IDENTITY(1,1) NOT NULL,
	[PartnerCode] [varchar](5) NOT NULL,
	[IsDirect] [bit] NULL,
	[IsOpenEnrolled] [bit] NOT NULL,
	[IsOnlineMall] [bit] NOT NULL,
	[IsHABranded] [bit] NOT NULL,
	[IsPlugin] [bit] NOT NULL,
 CONSTRAINT [PK_HAPartnerCodes] PRIMARY KEY CLUSTERED 
(
	[PartnerCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



