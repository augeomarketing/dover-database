USE [C04]
GO
/****** Object:  StoredProcedure [dbo].[usp_Generate_Monthly_Bonus_Statistics]    Script Date: 09/15/2016 11:11:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Generate_Monthly_Bonus_Statistics]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Begdate date, @Enddate date, @Current date

set @Current = GETDATE()
set @Begdate = RewardsNow.dbo.ufn_GetFirstOfPrevMonth (@Current)
set @Enddate = RewardsNow.dbo.ufn_GetLastOfPrevMonth (@Current)

if @Enddate not in (select MonthEndDate from Bonus_Award_Statistics)
Begin
	insert into dbo.Bonus_Award_Statistics
	(MonthEndDate, Count_Awarded, Points_Awarded, Count_Eligible)
	select @Enddate, COUNT(*) Count_Awarded, SUM(points) Points_Awarded
		, COUNT(*) + (select COUNT(*) from FirstUseBonus where BeginDate = @Begdate) Count_Eligible
	from HISTORY hs
	where TRANCODE='BI'
	and HISTDATE >= @Begdate
	and HISTDATE<= @Enddate
	and SECID is null
End

END
GO
