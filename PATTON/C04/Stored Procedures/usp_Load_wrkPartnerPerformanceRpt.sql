USE [C04]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_wrkPartnerPerformanceRpt]    Script Date: 06/07/2018 11:00:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bill Spack
-- Create date: 12/8/2016
-- Description:	Load data for PartnerPerformanceRpt
-- exec usp_Load_wrkPartnerPerformanceRpt
-- =============================================
-- Change History
-- =============================================
-- PR   Date        Author		Description 
--		--------	-------		------------------------------------
-- 1    06/07/2018  Asim		Added  left join to new table dbo.HARevenueRates which keep track of how we calculate the REVENUE.
--								Please check comments inline below to see specIFic lines of code changed.
-- =============================================
-- =============================================
IF			 OBJECT_ID('dbo.usp_Load_wrkPartnerPerformanceRpt','p') is not null
	DROP	PROCEDURE	dbo.usp_Load_wrkPartnerPerformanceRpt
GO

CREATE		PROCEDURE	dbo.usp_Load_wrkPartnerPerformanceRpt
	
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ProcessMonth	INT = Month(GetDate())
	DECLARE @ProcessYear	INT = Year(GetDate())
	
	-- Adjust for January
	IF @ProcessMonth = 1
		BEGIN
			SET @ProcessMonth = 12
			SET @ProcessYear = @ProcessYear - 1
		END
	ELSE
		BEGIN
		-- The sproc runs on the first of the month so subtract 1 to get previous month
			SET @ProcessMonth = Month(GETDATE()) - 1
			SET @ProcessYear = Year(GetDate())			
		END	
	
	-- SET OptDate
	DECLARE @OptDate char(7)
	DECLARE @month varchar(2) 
	
	-- Make sure Month is 2 characters
	IF LEN(CAST(@ProcessMonth as varchar(2))) = 1
		SET @month = '0' + CAST(@ProcessMonth as varchar(2))	
	ELSE 
		SET @month = CAST(@ProcessMonth as varchar(2))	
			
	SET @OptDate = cast(@ProcessYear as varchar(4)) + @month + '%'
	
	PRINT 'HERE'
	PRINT '@OptDate:' + @OptDate
	PRINT 'THERE'
	-- delete last years data
	DELETE	FROM dbo.wrkPartnerPerformanceRpt
	WHERE	AccrualFileMonth = @ProcessMonth
		
	-- Get Merchants
	DECLARE @Merchant varchar(250)
	DECLARE @OriginalMerchant varchar(250)
	DECLARE @Mid varchar(50)
	
	DECLARE @tblMerch table
	( Merchant varchar(100),
	  MID varchar(50)
	)
	  
	-- Load Merchants into @tblMerch
	INSERT iNTO @tblMerch (Merchant,MID)
	    SELECT DISTINCT Description, MerchantID FROM dbo.HATransaction_OutLog			
		
	-- Load wrkPartnerPerformanceRpt
	INSERT INTO dbo.wrkPartnerPerformanceRpt
		SELECT
		M.Merchant												AS MerchantName,		
		@ProcessMonth											AS AccrualFileMonth,	
		@ProcessYear											AS AccrualFileYear,
		ISNULL(SUM(ISNULL(OL.Miles,0) * OL.SignMultiplier),0)	AS TotalMiles,	
		COUNT(*)												AS TotalTransactions,
		COUNT(distinct OL.Tipnumber)							AS TotalUniqueMemberTransactions,		
		
		ISNULL(SUM((CASE	WHEN OL.Src = 'PR' OR (OL.PartnerCode in ('ACIVC', 'ACIAX', 'ACIMC', 'ACDVC', 'ACDAX', 'ACDMC') 
								AND OL.Src NOT IN ('MO', 'ML','RN'))	THEN (OL.AwardPoints / 100)
							WHEN OL.Src IN ('MO', 'ML','RN','AF')		THEN OL.AwardAmount
							/* Change made on 6/7/2018 by Asim --  STARTS HERE */
							--ELSE OL.AwardAmount * 2 END) *
							ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2)
					END)
							/* Change made on 6/7/2018 by Asim --  ENDS HERE */
					*
					CASE	WHEN OL.SignMultiplier = -1 AND ISNULL(OL.TranAmt,0) <= 0  THEN  1
							ELSE OL.SignMultiplier
					END),0)
																AS TotalRevenues,							 

		'Direct'												AS ProgramName,
		M.MID													AS MerchantID
		
		FROM			dbo.HATransaction_OutLog	AS OL
		INNER JOIN		@tblMerch					AS M	ON M.Merchant		= OL.Description AND M.MID = OL.MerchantID
		LEFT OUTER JOIN	dbo.HATransactionResult_IN	AS HI	ON HI.ReferenceID	= OL.ReferenceID 
		/* new code added by Asim - 6/5/2018 */
		LEFT JOIN		dbo.HARevenueRates			AS RR	ON RR.Description	= OL.Description AND RR.MerchantID = OL.MerchantID
		/* new code added by Asim - 6/5/2018 */
		WHERE			OutDate LIKE @OptDate
		AND				OL.PartnerCode <> 'AIMCS'
		AND				OL.PartnerCode <> 'AIMHP'
		AND				OL.PartnerCode <> 'AIMAC'
		AND				HI.RejectStatus = 0
		GROUP BY		Merchant,MID
		
-- SET ProgramName	based on SRC			
	UPDATE		P 
	SET			P.ProgramName = 'Indirect-PR'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src = 'PR'

	UPDATE		P 
	SET			P.ProgramName = 'Indirect-MO'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src = 'MO'

	UPDATE		P 
	SET			P.ProgramName = 'Indirect-RN'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src = 'RN'

	UPDATE		P 
	SET			P.ProgramName = 'Indirect-ML'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src = 'ML'

	UPDATE		P 
	SET			P.ProgramName = 'Indirect'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName and OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src <> 'MO'
	AND			OL.Src <> 'PR'
	AND			OL.Src <> 'LT'
	AND			OL.Src <> 'RN'
	AND			OL.Src <> 'ML'		
		
	UPDATE		P 
	SET			P.ProgramName = 'Indirect-PR'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src = 'LT'
	AND			OL.Description in ('ATHLETA', 'BANANA REPUBLIC', 'GAP', 'Gap Inc', 'OLD NAVY', 'Whole Foods Market, Inc.')
				
	UPDATE		P 
	SET			P.ProgramName = 'Online Mall'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode Not LIKE 'ACI%'
	AND			OL.PartnerCode not LIKE 'ACD%'
	AND			OL.PartnerCode <> 'AIMCS'
	AND			OL.PartnerCode <> 'AIMHP'
	AND			OL.PartnerCode <> 'AIMAC'

------------------------------------
-- added 5/3/18 for affinity
	UPDATE		P	 
	SET			P.ProgramName = 'Indirect-AF'
	FROM		dbo.wrkPartnerPerformanceRpt P
	INNER JOIN	dbo.HATransaction_OutLog OL ON OL.Description = P.MerchantName AND OL.MerchantID = P.MerchantID
	WHERE		OL.PartnerCode LIKE 'ACI%'
	AND			OL.Src = 'AF'
-----------------------------------
		
	-- Special Case Logic
	-- This section is used for fixing the output in the report
	
	UPDATE	dbo.wrkPartnerPerformanceRpt
	SET		MerchantName = LEFT(MerchantName,charindex('(',MerchantName)-1)
	WHERE	MerchantName LIKE '%(dup%'
	
	UPDATE	dbo.wrkPartnerPerformanceRpt
	SET		MerchantName = LEFT(MerchantName,charindex('(',MerchantName)-1)
	WHERE	MerchantName LIKE '%(mil%'
	AND		MerchantName <> 'LOVING HUT (MILPITAS)'
	
	UPDATE	dbo.wrkPartnerPerformanceRpt
	SET		MerchantName = 'Greens & Vines, a Subsidiary of Licious Dishes'
	WHERE	MerchantName LIKE 'Greens & Vines, a Subsidiary of%'
		
	UPDATE	dbo.wrkPartnerPerformanceRpt
	SET		ProgramName = 'Direct'
	WHERE	MerchantName LIKE 'KUALOA RANCH%'
	
	DELETE	dbo.wrkPartnerPerformanceRpt_Condensed

	INSERT INTO		dbo.wrkPartnerPerformanceRpt_Condensed
	SELECT * FROM	dbo.wrkPartnerPerformanceRpt
		
	-- Call sproc that condenses the merchant names
	EXEC usp_CondenseMerchantName 'wrkPartnerPerformanceRpt_Condensed'
END
