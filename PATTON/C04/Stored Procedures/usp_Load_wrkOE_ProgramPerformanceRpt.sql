USE [C04]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_wrkOE_ProgramPerformanceRpt]    Script Date: 06/08/2018 11:56:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bill Spack
-- Create date: 7/21/16
-- Description:	Retrieves monthly Open Enrollment data for the HA OE Program Performance Report
-- EXEC usp_Load_wrkOE_ProgramPerformanceRpt
-- =============================================
-- Change History
-- =============================================
-- PR   DATE        Author		Description 
--		--------	-------		------------------------------------
-- 1    06/08/2018  Asim		Added  left join to new table dbo.HARevenueRates which keep track of how we calculate the REVENUE.
--								Please check comments inline below to see specIFic lines of code changed.
-- =============================================
-- =============================================
IF			 object_id('dbo.usp_Load_wrkOE_ProgramPerformanceRpt','p') is not null
	DROP	PROCEDURE	dbo.usp_Load_wrkOE_ProgramPerformanceRpt
GO

CREATE		PROCEDURE	dbo.usp_Load_wrkOE_ProgramPerformanceRpt
	
AS
BEGIN
	SET NOCOUNT ON
	
	-- Get current date
	DECLARE @CurrentDate	DATE	= getdate()
	DECLARE @ProcessMonth	INT		= Month(@CurrentDate)
	DECLARE @ProcessYear	INT		= Year(@CurrentDate)
	DECLARE @sdate			VARCHAR(20)
	DECLARE @edate			VARCHAR(20)
	DECLARE @day			VARCHAR(2)
	DECLARE @month			VARCHAR(2)
	DECLARE @BegMonth		VARCHAR(20)
	DECLARE @ENDMonth		VARCHAR(20)
	DECLARE @wrkMonth		INT		= Month(@CurrentDate) - 1
	
	-- SET DATE range IF new year
	IF @ProcessMonth = 1
		BEGIN
			SET @ProcessMonth = 12
			SET @wrkMonth = 12
			SET @ProcessYear = @ProcessYear - 1
		END
----- debugging		
	PRINT '@CurrentDate:'	+ cast(@CurrentDATE		AS VARCHAR)			
	PRINT '@ProcessMonth:'	+ cast(@ProcessMonth	AS VARCHAR)	
	PRINT '@ProcessYear:'	+ cast(@ProcessYear		AS VARCHAR)
----- debugging	
	-- Make sure Day AND Month are 2 characters
	SET @day = CAST(Day(DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, (CAST(@ProcessMonth AS VARCHAR(2)))+'/01/'+ CAST(@ProcessYear AS VARCHAR(4)))+ 1, 0)))as VARCHAR(2))
	IF LEN(@day) = 1
		SET @day = '0' + @day
	
	SET @month = @ProcessMonth	
	IF LEN(CAST(@ProcessMonth AS VARCHAR(2))) = 1
		SET @month = '0' + CAST(@ProcessMonth AS VARCHAR(2))
		
	-- AppEND Time values to start AND END dates
	SET @sdate		= CAST(@ProcessYear AS VARCHAR(4)) + @month + '01000000'
	SET @edate		= CAST(@ProcessYear AS VARCHAR(4)) + @month + @day  + '595959'
	SET @BegMonth	= @month + '/01/' + cast(@ProcessYear AS VARCHAR(4)) 
	SET @ENDMonth	= @month + '/' + @day + '/' + cast(@ProcessYear AS VARCHAR(4))
	
	-- Truncate table
	TRUNCATE TABLE dbo.wrkOE_ProgramPerformanceRpt
	
    -- Loop through dates AND pull data FROM HATransaction_OutLog
    --WHILE (@sdate <> '20151201000000') remmed out 20180202
    WHILE (@sdate <> '20161201000000')
    BEGIN
		INSERT INTO dbo.wrkOE_ProgramPerformanceRpt
		SELECT 'Direct'								AS ProgramName,
		@ProcessYear								AS AccrualFileYear,
		@ProcessMonth								AS AccrualFileMonth,
		
		(SELECT COUNT(*) 
		FROM		dbo.HATransaction_OutLog		AS OL
		INNER JOIN	dbo.HATransactionResult_IN		AS HI ON HI.ReferenceID = OL.ReferenceID 
		WHERE	OutDATE >= @sdate
		AND		OutDATE <= @edate
		AND		HI.RejectStatus = 0
		AND		OL.PartnerCode like 'ACD%')			AS TotalTransactions,			
		
		0											AS TotalTransactionPercentChange,
		
		ISNULL(SUM(	CASE WHEN OL.SignMultiplier = -1	and OL.TranAmt < 0  THEN ISNULL(OL.TranAmt * 1,0)
						ELSE ISNULL((OL.TranAmt * OL.SignMultiplier),0)
					END),0)		
													AS TotalSales,
		
		0											AS TotalSalePercentChange,
		ISNULL(SUM(OL.Miles * OL.SignMultiplier),0)	AS TotalMiles,
		0											AS TotalMilePercentChange,
		
		(SELECT COUNT(*)
		FROM		dbo.HATransaction_OutLog		AS OL
		INNER JOIN 	dbo.HATransactionResult_IN		AS HI ON HI.ReferenceID = OL.ReferenceID 
		WHERE	OutDATE >= @sdate
		AND		OutDATE <= @edate
		AND		HI.RejectStatus = 0
		AND		(OL.PartnerCode <> 'ACDHD' AND OL.PartnerCode <> 'ACDHC' AND OL.PartnerCode like 'ACD%'))
													AS TotalOETransactions
		
		,0											AS TotalOETransactionPercentChange			
		,COUNT(distinct OL.Tipnumber) AS TotalUniqueMemberTransactions
		,0											AS TotalUniqueMemberTransactionPercentChange		
		
		,ISNULL(SUM((	CASE	WHEN OL.Src = 'PR' OR (OL.PartnerCode IN ('ACDVC', 'ACDAX', 'ACDMC') 
									AND OL.Src NOT IN ('MO', 'ML', 'RN'))	THEN (OL.AwardPoints / 100)
								WHEN OL.Src IN ('MO', 'ML','RN')			THEN OL.AwardAmount
								/* Change made on 6/4/2018 by Asim --  STARTS HERE */
								--ELSE OL.AwardAmount * 2
								ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2 )
								/* Change made on 6/4/2018 by Asim --  ENDS HERE */
						END)
						*
						CASE	WHEN OL.SignMultiplier = -1 AND ISNULL(OL.TranAmt,0) <= 0  THEN  1
								ELSE OL.SignMultiplier
						END),0) 							
													AS TotalRevenues
		
		,0											AS TotalRevenuePercentChange
		,0											AS TotalUniqueMemberRegCard
		,0											AS TotalOETransactions
		,@BegMonth									AS MonthStartDate
		,@ENDMonth									AS MonthENDDate		
		
		FROM			dbo.HATransaction_OutLog	AS OL
		INNER JOIN		dbo.HATransactionResult_IN	AS HI	ON HI.ReferenceID	= OL.ReferenceID 
		LEFT OUTER JOIN dbo.HAMerchantFees			AS MF	ON MF.MerchantId	= OL.MerchantID
		/* new code added by Asim - 6/5/2018 */
		LEFT JOIN		dbo.HARevenueRates			AS M	ON M.Description	= OL.Description AND M.MerchantID = OL.MerchantID
		/* new code added by Asim - 6/5/2018 */
		WHERE	OutDATE BETWEEN @sdate AND @edate
		AND		HI.RejectStatus = 0 
		AND		OL.PartnerCode  IN ('ACDVC', 'ACDAX', 'ACDMC')
		
		INSERT INTO dbo.wrkOE_ProgramPerformanceRpt
		SELECT 'Indirect'							AS ProgramName,
		@ProcessYear								AS AccrualFileYear,
		@ProcessMonth								AS AccrualFileMonth,	
		
		(SELECT COUNT(*) 
		FROM		dbo.HATransaction_OutLog		AS OL
		INNER JOIN	dbo.HATransactionResult_IN		AS HI ON HI.ReferenceID = OL.ReferenceID 
		WHERE	OutDATE >= @sdate
		AND		OutDATE <= @edate
		AND		HI.RejectStatus = 0
		AND		OL.PartnerCode like 'ACI%')			AS TotalTransactions,	
	
		0											AS TotalTransactionPercentChange,
		
		ISNULL(SUM(CASE WHEN OL.SignMultiplier = -1	AND OL.TranAmt < 0  THEN ISNULL(OL.TranAmt * 1,0)
							 ELSE ISNULL((OL.TranAmt * OL.SignMultiplier),0)
							 END),0)		
													AS TotalSales,
		
		0											AS TotalSalePercentChange,
		ISNULL(SUM(OL.Miles * OL.SignMultiplier),0)	AS TotalMiles,
		0											AS TotalMilePercentChange,	
		
		(SELECT COUNT(*)
		FROM		dbo.HATransaction_OutLog		AS OL
		INNER JOIN	dbo.HATransactionResult_IN		AS HI ON HI.ReferenceID = OL.ReferenceID 
		WHERE	OutDATE >= @sdate
		AND		OutDATE <= @edate
		AND		HI.RejectStatus = 0
		AND		OL.PartnerCode IN ('ACIVC', 'ACIAX', 'ACIMC'))
													AS TotalOETransactions
		
		,0											AS TotalOETransactionPercentChange					
		,COUNT(distinct OL.Tipnumber)				AS TotalUniqueMemberTransactions		
		,0											AS TotalUniqueMemberTransactionPercentChange
		
		,ISNULL(SUM((	CASE	WHEN OL.Src = 'PR' OR (OL.PartnerCode IN ('ACIVC', 'ACIAX', 'ACIMC') 
									AND OL.Src NOT IN ('MO', 'ML','RN'))  THEN (OL.AwardPoints / 100)
								WHEN OL.Src IN ('MO', 'ML','RN')  THEN OL.AwardAmount
								/* Change made on 6/4/2018 by Asim --  STARTS HERE */
								--ELSE OL.AwardAmount * 2 END) *
								ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2 )
								/* Change made on 6/4/2018 by Asim --  ENDS HERE */
						END)
						*
						CASE	WHEN OL.SignMultiplier = -1 AND ISNULL(OL.TranAmt,0) <= 0  THEN  1
								ELSE OL.SignMultiplier
						END),0) 						
													AS TotalRevenues			 
		,0											AS TotalRevenuePercentChange		
		,CASE	WHEN (SELECT COUNT([ReportDate]) FROM dbo.wrkHAWeeklyTrEND WHERE ReportDate BETWEEN @BegMonth AND @ENDMonth) > 0
					THEN (SELECT [UniqueMembers] FROM dbo.wrkHAWeeklyTrEND WHERE ReportDate = (	SELECT MAX(ReportDate) FROM dbo.wrkHAWeeklyTrEND
																								WHERE ReportDate BETWEEN @BegMonth AND @ENDMonth))
				ELSE 0
		 END										AS TotalUniqueMemberRegCard

		,0											AS TotalUMRPercent		
		
		,@BegMonth									AS MonthStartDate
		,@ENDMonth									AS MonthENDDate
		
		FROM		dbo.HATransaction_OutLog		AS OL
		INNER JOIN	dbo.HATransactionResult_IN		AS HI	ON HI.ReferenceID	= OL.ReferenceID
		/* new code added by Asim - 6/5/2018 */
		LEFT JOIN	dbo.HARevenueRates				AS M	ON M.Description	= OL.Description AND M.MerchantID = OL.MerchantID
		/* new code added by Asim - 6/5/2018 */
		WHERE	OutDATE BETWEEN @sdate AND @edate
		AND		HI.RejectStatus = 0
		AND		OL.PartnerCode	IN ('ACIVC', 'ACIAX', 'ACIMC')
	
		-- ReSET Loop counter
		IF @ProcessMonth = 1
			BEGIN
				SET @ProcessMonth = 12
				SET @ProcessYear = @ProcessYear - 1
			END
		ELSE
			SET @ProcessMonth = @ProcessMonth - 1	
			
		-- Make get last DATE of the next month  
		SET @day = CAST(Day(DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, (CAST(@ProcessMonth AS VARCHAR(2)))+'/01/'+ CAST(@ProcessYear AS VARCHAR(4)))+ 1, 0)))as VARCHAR(2))
		
		--make sure Month is 2 characters			
		IF LEN(CAST(@ProcessMonth AS VARCHAR(2))) = 1
			SET @month = '0' + CAST(@ProcessMonth AS VARCHAR(2))
		ELSE
			SET @month = CAST(@ProcessMonth AS VARCHAR(2))
			
		-- AppEND Time values to start AND END dates
		SET @sdate = CAST(@ProcessYear AS VARCHAR(4)) + @month + '01000000'
		SET @edate = CAST(@ProcessYear AS VARCHAR(4)) + @month + @day  + '595959'	
		SET @BegMonth = @month + '/01/' + cast(@ProcessYear AS VARCHAR(4)) 
		SET @ENDMonth = @month + '/' + @day + '/' + cast(@ProcessYear AS VARCHAR(4)) 
	END

	
	-- Calculate Change Percentages
	EXEC dbo.usp_CalculateTotalPercentageChange_Rpt_OE_ProgramPerformance
	
	-- Load wrkMonthYearOverYearRpt
	EXEC dbo.usp_Load_wrkOE_MonthYearOverYearRpt @wrkMonth
	EXEC dbo.usp_Load_wrkOE_YearOverYearRpt @wrkMonth
END
