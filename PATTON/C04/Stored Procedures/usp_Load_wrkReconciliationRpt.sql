USE [C04]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_wrkReconciliationRpt]    Script Date: 06/05/2018 15:12:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bill Spack
-- Create date: 7/7/2016
-- Description:	HA Reconciliation Report
-- =============================================
-- Change History
-- =============================================
-- PR   Date        Author		Description 
--		--------	-------		------------------------------------
-- 1    06/04/2018  Asim		Added  left join to new table dbo.HARevenueRates which keep track of how we calculate the Rewards and Tax.
--								Please check comments inline below to see specific lines of code changed.
-- 2	06/12/2018  Asim		added logic to make Miles, Rewards, Fee and Tax 0 for PartnerCode='AIMAC'
--								6/12/2018 by Asim----
--								UPDATE	dbo.wrkReconciliationRpt 
--								SET		Descript	='Holiday Promotion'
--								,		Miles		=0
--								,		Rewards		=0
--								,		Fee			=0
--								,		Tax			=0
--								WHERE PartnerCode='AIMAC'
--								added on 	6/12/2018 by Asim----
-- =============================================
-- =============================================

IF			 object_id('dbo.usp_Load_wrkReconciliationRpt','p') is not null
	DROP	PROCEDURE	dbo.usp_Load_wrkReconciliationRpt
GO

CREATE		PROCEDURE	dbo.usp_Load_wrkReconciliationRpt
	@BeginDate date,
	@EndDate date
AS
BEGIN
	SET NOCOUNT ON
	
    -- Clear data in wrkReconciliationRpt
	truncate table wrkReconciliationRpt
	
	DECLARE @PartnerCode varchar(20)
	DECLARE @Desc varchar(250)
	DECLARE @Sort int	

	-- Get Distinct Partners
	DECLARE csr CURSOR For
	    SELECT
			PartnerCode,
			[Description],
			RptOrder
		FROM
			C04.[dbo].HAPartnerCodes		
		WHERE
			RptOrder IS NOT NULL
			and PartnerCode <> 'AIMHP'
		Order by RptOrder
		
	OPEN csr
	FETCH NEXT FROM csr INTO @PartnerCode, @Desc, @Sort
 
	-- Loop through each MID and add transaction counts
	WHILE @@FETCH_STATUS = 0
		BEGIN
			Insert Into wrkReconciliationRpt
			SELECT
				@PartnerCode AS PartnerCode,
				@Desc,
				ISNULL(SUM(TF.Miles * TF.SignMultiplier),0) AS Miles,
				
				/* Change made on 6/4/2018 by Asim --  STARTS HERE */
				ISNULL(SUM((CASE	WHEN	TF.Src = 'PR' OR (TF.PartnerCode IN ('ACIVC', 'ACIAX', 'ACIMC', 'ACDVC', 'ACDAX', 'ACDMC') 
										AND	TF.Src NOT IN ('MO', 'ML', 'RN'))	THEN (TF.AwardPoints / 100)
									WHEN TF.Src in ('MO', 'ML', 'RN')			THEN TF.AwardAmount
									WHEN TF.PartnerCode	= 'AIMCS'				THEN TF.Miles * 0.02 -- coded for AIMCS	 Customer service courtesy, Asim added this line on 6/12/2018
									ELSE TF.AwardAmount * ISNULL(CentsPerMile, 2 ) --default is 2 cents otherwise its based on the table HARevenueRates
							END)
							* 
							CASE	WHEN	TF.SignMultiplier = -1 AND TF.TranAmt < 0  THEN  1
									ELSE	TF.SignMultiplier
							END),0)  
			       AS Rewards,			
				/* Change made on 6/4/2018 by Asim --  ENDS HERE */
				ISNULL(SUM(ISNULL(MF.MerchantFee, 0.00) * TF.TranAmt),0.00) AS Fee,
				/* Change made on 6/4/2018 by Asim --  STARTS HERE */
				ISNULL(SUM(	CASE	WHEN PC.IsDirect = 0 OR PC.IsDirect IS NULL THEN 0.00 
									ELSE TF.AwardAmount * ISNULL(CentsPerMile, 2 )
							END * TF.SignMultiplier  * 0.018 ) ,0.00) AS Tax, --CHANGED TAX RATE FROM 7.5 TO 1.8
				/* Change made on 6/4/2018 by Asim --  ENDS HERE */
				@Sort		
			FROM
				[C04].[dbo].[HATransaction_OutLog]	AS TF
			INNER JOIN
				[C04].[dbo].[HAPartnerCodes]		AS PC	ON PC.PartnerCode = TF.PartnerCode
			LEFT OUTER JOIN 
				[C04].[dbo].[HAMerchantFees]		AS MF	ON TF.MerchantID = MF.MerchantId
			/* new code added by Asim - 6/4/2018 */
			LEFT JOIN dbo.HARevenueRates			AS M	ON M.Description = TF.Description AND M.MerchantID = TF.MerchantID
			/* new code added by Asim - 6/4/2018 */
			INNER join 
				[C04].[dbo].HATransactionResult_IN	AS HI	ON HI.ReferenceID = TF.ReferenceID 
			WHERE	CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) >= @BeginDate
				AND	CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @EndDate
				AND HI.RejectStatus = 0
				AND TF.PartnerCode = @PartnerCode
				
			FETCH NEXT FROM csr INTO  @PartnerCode, @Desc, @Sort
		END
	 
	CLOSE csr
	DEALLOCATE csr	

-- Special case for AIMHP	
	INSERT INTO dbo.wrkReconciliationRpt
		SELECT
			'AIMHP' AS PartnerCode,
			'HA Promotion'  as [Descript],
			SUM(TF.Miles * TF.SignMultiplier) AS Miles,		
			0.00 AS Rewards,
			0.00 AS Fee,
			0.00 AS Tax,
			140 as RptSort 
		FROM
			[C04].[dbo].[HATransaction_OutLog] AS TF
			left outer join 
				[C04].[dbo].HATransactionResult_IN HI ON HI.ReferenceID = TF.ReferenceID 			
		WHERE
			TF.PartnerCode = 'AIMHP' AND
			CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) >= @BeginDate AND
			CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @EndDate AND 
			HI.RejectStatus = 0
	
	UPDATE	wrkReconciliationRpt
	SET		Miles = (
						SELECT SUM(TF.Miles * TF.SignMultiplier)
						FROM
							[C04].[dbo].[HATransaction_OutLog] AS TF
							left outer join 
								[C04].[dbo].HATransactionResult_IN HI ON HI.ReferenceID = TF.ReferenceID 
						WHERE
							TF.PartnerCode = 'AIMHP' AND
							CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) >= @BeginDate AND
							CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @EndDate AND 
							HI.RejectStatus = 0
					)
	WHERE PartnerCode = 'AIMHP'
	
	-- Move blank Partner ID's to AIMIA
	DECLARE @Miles int
	DECLARE @Rewards money
	DECLARE @Fee money
	DECLARE @Tax money
	
	SELECT
		@Miles		= ISNULL(SUM(TF.Miles * TF.SignMultiplier),0),
		/* Change made on 6/4/2018 by Asim --  STARTS HERE */
		@Rewards	= ISNULL(SUM((	CASE	WHEN TF.Src = 'PR' OR (TF.PartnerCode in ('ACIVC', 'ACIAX', 'ACIMC', 'ACDVC', 'ACDAX', 'ACDMC')
												AND TF.Src <> 'MO')		THEN (TF.AwardPoints / 100)
											WHEN TF.Src = 'MO'			THEN  TF.AwardAmount
											ELSE TF.AwardAmount * ISNULL(CentsPerMile, 2 ) --default is 2 cents otherwise its based on the table HARevenueRates
									END) * TF.SignMultiplier),0) ,
		@Fee		= ISNULL(SUM(ISNULL(MF.MerchantFee, 0.00) * TF.TranAmt),0.00),
		@Tax		= ISNULL(SUM(	CASE	WHEN PC.IsDirect = 0 OR PC.IsDirect IS NULL
												THEN 0.00								  --default is 2 cents otherwise its based on the table HARevenueRates
											ELSE	(TF.AwardAmount * ISNULL(CentsPerMile, 2 )) * TF.SignMultiplier
									END) * 0.018,0.00)  --CHANGED TAX RATE FROM 7.5 TO 1.8
		/* Change made on 6/4/2018 by Asim --  ENDS HERE */
	FROM
		[C04].[dbo].[HATransaction_OutLog] AS TF
	LEFT OUTER JOIN
		[C04].[dbo].[HAPartnerCodes] AS PC ON PC.PartnerCode = (CASE WHEN TF.PartnerCode = '' THEN 'AIMIA' ELSE TF.PartnerCode END)
	LEFT OUTER JOIN 
		[C04].[dbo].[HAMerchantFees] AS MF ON TF.MerchantID = MF.MerchantId
	LEFT OUTER join 
				[C04].[dbo].HATransactionResult_IN HI ON HI.ReferenceID = TF.ReferenceID
	/*new code added by Asim - 6/4/2018 */
	LEFT JOIN dbo.HARevenueRates M on M.Description = TF.Description and M.MerchantID = TF.MerchantID
	/*new code added by Asim - 6/4/2018 */

	WHERE	CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) >= @BeginDate
	AND		CONVERT(DATE, SUBSTRING(OutDate, 1, 4) + '-' + SUBSTRING(OutDate, 5, 2) + '-' + SUBSTRING(OutDate, 7, 2)) <= @EndDate
	AND		HI.RejectStatus	= 0
	AND		Src				<> 'BO'
	AND		TF.PartnerCode	= ''
	
	UPDATE	dbo.wrkReconciliationRpt
	SET		Miles = Miles + @Miles,
		Rewards = Rewards + @Rewards,
		Fee = Fee + @Fee,
		Tax = Tax + @Tax
	WHERE PartnerCode = 'AIMIA' 
		
	--added on 	7/25/17----
	--UPDATE	dbo.wrkReconciliationRpt 
	--SET		Rewards=0 
	----,Miles=0 
	--WHERE PartnerCode='AIMCS'
	----------------------
	--added on 	6/12/2018 by Asim----
	UPDATE	dbo.wrkReconciliationRpt 
	SET		Descript	='Holiday Promotion'
	,		Miles		=0
	,		Rewards		=0
	,		Fee			=0
	,		Tax			=0
	WHERE PartnerCode='AIMAC'
	--added on 	6/12/2018 by Asim----

	SELECT * FROM dbo.wrkReconciliationRpt
	ORDER BY RptSort
END
