USE [C04]
GO

/****** Object:  StoredProcedure [dbo].[spHATransaction_OUT]    Script Date: 12/21/2015 12:54:58 ******/
SET ANSI_NULLS ON
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spHATransaction_OUT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spHATransaction_OUT]
GO

SET QUOTED_IDENTIFIER ON
GO


/*CHANGE LOG

*/

/*
	--sample call 
	declare @OutDate varchar(16)
	exec spHATransaction_OUT @OutDate output
	print '@OutDate:' + @OutDate

select * from ZaveeTransactions
select * from AffinityMatchedTransactionFeed
select * from ProsperoTransactions	

select * from HATransaction_OutLog
select * from HAHeaderFooter
truncate table HATransaction_OutLog
truncate table HAHeaderFooter

*/

CREATE PROCEDURE [dbo].[spHATransaction_OUT]
   @OutDate varchar(16) output

AS
BEGIN
 --set the outdate that will be used to to identify this batch going out. format is yyyymmddhhmmss
	set @OutDate = REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, getdate(), 112), 126), '-', ''), 'T', ''), ':', '')
	declare @DBNumber varchar(3)='C04'
	declare @MasterPartnerCode varchar(5)='AIMIA'
/* the select is based on usp_GetZaveeTransaction */ 
/*Select the CLO/Zavee Transactions (and prospero and affinity with unions */

insert into [C04].dbo.HATransaction_OutLog
	(OutDate, FTPNo, Lastname, Firstname, ActivityDate, SignMultiplier, Miles, ReferenceID, PartnerCode, Description, MerchantID, Tipnumber, FsixLfour, src, FixedWidthRec)

--===BEGIN BONUS C04.HISTORY Transactions===========================================				
SELECT   --distinct  -- need distinct because the testing data has tip multiple times in RNICustomer 
	@OutDate as OutDate
	,HIST.ACCTID as Member	
	,RewardsNow.dbo.ufn_ParseName (CUST.ACCTNAME1,'L') as LastName		
	,RewardsNow.dbo.ufn_ParseName (CUST.ACCTNAME1,'F') as FirstName	
	,HIST.HISTDATE as TransactionDate
	,1 		
	,HIST.POINTS as AwardPoints
	,HIST.sid_history_id as ReferenceID
	,'AIMHP' as PartnerCode   --had coded per John's email 11/13
	,'New Cardholder Partner Bonus' as MerchantName
	,'      ' as MerchantID
	,HIST.TIPNUMBER as Tipnumber
	,'          ' as FsixLfour
	,'BO'
	,NULL
	 
FROM [C04].dbo.[History] HIST  with (nolock) 
	 join [C04].dbo.Customer CUST  with (nolock) on HIST.Tipnumber = CUST.TIPNUMBER
	 where 1=1
		AND	isnull(HIST.ACCTID,'') <>''
		AND HIST.HISTDATE<= GETDATE()
		AND HIST.POINTS>0
		AND HIST.sid_history_id not in (select ReferenceID from [C04].dbo.HATransaction_OutLog)
--===END BONUS C04.HISTORY Transactions===========================================				
UNION ALL
--===BEGIN AZIGO Transactions===========================================		
SELECT   distinct  -- need distinct because the testing data has tip multiple times in RNICustomer 
	@OutDate as OutDate
	,RNIC.dim_RNICustomer_Member as Member	
	,RewardsNow.dbo.ufn_ParseName (RNIC.dim_RNICustomer_Name1,'L') as LastName		
	,RewardsNow.dbo.ufn_ParseName (RNIC.dim_RNICustomer_Name1,'F') as FirstName	
	,AZ.dim_AzigoTransactions_TransactionDate as TransactionDate
	,(CASE WHEN dim_AzigoTransactions_TransactionAmount >= 0.00 THEN 1 ELSE -1 END)
	,AZ.dim_AzigoTransactions_Points as AwardPoints
	,AZ.sid_AzigoTransactions_Identity as ReferenceID
	--,'XXXX' as PartnerCode
	,AZ.dim_AzigoTransactions_PartnerCode as PartnerCode
	,left(AZ.dim_AzigoTransactions_MerchantName,50) as MerchantName
	,'      ' as MerchantID
	,AZ.dim_AzigoTransactions_Tipnumber as Tipnumber
	,'          ' as FsixLfour
	,'AZ'
	,NULL
	 
FROM [RewardsNow].dbo.[AzigoTransactions] AZ  with (nolock) 
	 join [RewardsNow].dbo.RNICustomer RNIC  with (nolock) on AZ.dim_AzigoTransactions_Tipnumber = RNIC.dim_RNICustomer_RNIId
	 where 1=1
		AND  isnull(RNIC.dim_RNICustomer_Member,'') <>''
		and AZ.dim_AzigoTransactions_Points >0
		and AZ.dim_AzigoTransactions_TransactionDate <= GETDATE()
		AND AZ.sid_AzigoTransactions_Identity not in (select ReferenceID from [C04].dbo.HATransaction_OutLog)
		AND AZ.dim_AzigoTransactions_FinancialInstituionID = 'C04'	
--===End AZIGO Transactions===========================================		
UNION ALL
--===Begin Prospero Transactions===========================================
SELECT   
	@OutDate as OutDate
	,TA.dim_RNITransaction_Member as Member
--,CUST.ACCTNAME1	as 	Lastname	
	,''	as LastName		
	,''	as FirstName		
	,TA.dim_RNITransaction_TransactionDate as TransactionDate
	,CASE WHEN PROST.dim_ProsperoTransactions_TransactionCode='C' THEN -1 ELSE 1 END
	,CAST(PROST.dim_ProsperoTransactions_AwardAmount AS INT) as AwardPoints
	,TA.sid_RNITransaction_ID as ReferenceID
	,[c04].dbo.ufn_GetPartnerCode(@DBNumber,'PR', TA.sid_RNITransaction_ID,TA.dim_RNITransaction_MerchantID) as PartnerCode
	,left(TA.dim_rnitransaction_merchantname,50) as MerchantName
	,TA.dim_RNITransaction_MerchantID as MerchantID
	,PROST.dim_ProsperoTransactions_TipNumber 
	,LEFT(dim_RNITransaction_CardNumber,6) + right(rtrim(dim_RNITransaction_CardNumber),4) as FsixLfour
	,'PR'
	,NULL
	 
FROM [RewardsNow].dbo.[RNITransactionArchive] TA  with (nolock) 
	 join [RewardsNow].dbo.ProsperoTransactions PROST  with (nolock) on PROST.sid_RNITransaction_ID=TA.sid_RNITransaction_ID
	 LEFT OUTER join [RewardsNow].dbo.ProsperoMerchant PM  with (nolock) on PM.dim_ProsperoMerchant_MerchantId=TA.dim_RNITransaction_MerchantID
	 
	 where 1=1
		AND  TA.dim_RNITransaction_Member is  not null

		 and TA.dim_RNITransaction_TransactionDate <= GETDATE()
		 --AND PROST.dim_ProsperoTransactions_AwardPoints >0
		--AND PROST.dim_ProsperoTransactions_TransactionCode='D' -- this means Positive only
		AND TA.sid_RNITransaction_ID not in (select ReferenceID from [C04].dbo.HATransaction_OutLog)
		AND PROST.dim_ProsperoTransactions_TipFirst='C04'					--added 10/29/15 to exclude Hawaiian
----===END Prospero Transactions===========================================
UNION ALL
----===Begin ZaveeTransactions===========================================

select   
@OutDate 	
,TA.dim_RNITransaction_Member as Member
--,CUST.ACCTNAME1	as 	Lastname	
,''	as LastName		
,''	as FirstName		
,TA.dim_RNITransaction_TransactionDate as TransactionDate
,(CASE WHEN dim_ZaveeTransactions_TransactionAmount >= 0.00 THEN 1 ELSE -1 END)
,ZTRAN.dim_ZaveeTransactions_AwardPoints as AwardPoints
,TA.sid_RNITransaction_ID as ReferenceID
,[c04].dbo.ufn_GetPartnerCode(@DBNumber,'ZA', TA.sid_RNITransaction_ID,ZTran.dim_ZaveeTransactions_MerchantId) as PartnerCode
,left(ZTRAN.dim_ZaveeTransactions_MerchantName,50) as MerchantName
,ZTRAN.dim_ZaveeTransactions_MerchantId as MerchantID
,ZTRAN.dim_ZaveeTransactions_MemberID as Tipnumber
,LEFT(dim_RNITransaction_CardNumber,6) + right(rtrim(dim_RNITransaction_CardNumber),4) as FsixLfour
,'ZA'
,NULL

 FROM [RewardsNow].dbo.[RNITransactionArchive] TA  with (nolock)  
			 join [RewardsNow].dbo.ZaveeMerchant zm   with (nolock) on zm.dim_ZaveeMerchant_MerchantId = TA.dim_RNITransaction_MerchantID
			 join [RewardsNow].dbo.ZaveeTransactions ZTRAN  with (nolock) on ZTRAN.sid_RNITransaction_ID=TA.sid_RNITransaction_ID
			 where 1=1
				 
				  and  TA.dim_RNITransaction_Member is  not null

				  --and zm.dim_ZaveeMerchant_EffectiveDate <=  TA.dim_RNITransaction_TransactionDate
					and (
					 zm.dim_ZaveeMerchant_Status = 'Active'
					 or (zm.dim_ZaveeMerchant_Status = 'Suspended'  and  TA.dim_RNITransaction_TransactionDate <= zm.dim_ZaveeMerchant_SuspendedDate)
					 )
				 --and TA.dim_RNITransaction_TransactionDate <= GETDATE()
				 AND ZTRAN.dim_ZaveeTransactions_AwardPoints >0
				 AND TA.sid_RNITransaction_ID not in (select ReferenceID from [C04].dbo.HATransaction_OutLog)
				 AND ZTRAN.dim_ZaveeTransactions_FinancialInstituionID = 'C04' -- ONLY  Hawaiian Airlines
				 --AND   AFF.AcctType='MEMBER'
			 
--===END ZaveeTransactions=========GOOD================================




--===============================================================================
--===============================================================================
--create the output updating the  HATransaction_OutLog.FixedWidthRec field
--and using this field for the actual output 
--=========================================================
-- calc the total miles and number of recs in the batch
--declare @TotalMiles int, @TotalRecs int
--select @TotalMiles=isnull(SUM(Miles),0) from HATransaction_OutLog where OutDate=@OutDate
--select @TotalRecs=COUNT(*) from HATransaction_OutLog where OutDate=@OutDate
declare @TotalMiles int, @TotalRecs int, @AddedMiles int, @SubtractedMiles int
select @AddedMiles = isnull(SUM(Miles),0) from HATransaction_OutLog where OutDate=@OutDate and SignMultiplier = 1
select @SubtractedMiles = isnull(SUM(Miles),0) from HATransaction_OutLog where OutDate=@OutDate and SignMultiplier = -1
select @TotalMiles = @AddedMiles - @SubtractedMiles
select @TotalRecs=COUNT(*) from HATransaction_OutLog where OutDate=@OutDate
--PRINT CAST(@AddedMiles AS VARCHAR(10)) + ' - ' + CAST(@SubtractedMiles AS VARCHAR(10)) + ' = ' + CAST(@TotalMiles AS VARCHAR(10))

-------------------------

--populate the field that is used to pull the data 
update HATransaction_OutLog 
set FixedWidthRec=

 '1 '
+ 'HA ' 
+  RewardsNow.dbo.ufn_Pad(ltrim(rtrim(FTPNo)),'R',20,' ')   
+  RewardsNow.dbo.ufn_Pad(Lastname,'R',35,' ')   
+  RewardsNow.dbo.ufn_Pad(Firstname,'R',35,' ')
+ CONVERT(varchar(8),ActivityDate,112)  
+ (CASE WHEN SignMultiplier = 1 THEN '+' ELSE '-' END)
+  RewardsNow.dbo.ufn_Pad(Miles,'L',10,'0')  
+  RewardsNow.dbo.ufn_Pad(ReferenceID,'R',15,' ')
+  RewardsNow.dbo.ufn_Pad('','R',1,' ')
+ RewardsNow.dbo.ufn_Pad(PartnerCode,'R',5,' ')
+  RewardsNow.dbo.ufn_Pad(isnull([Description],''),'R',50,' ')
from [C04].dbo.HATransaction_OutLog with (nolock)
where OutDate=@OutDate


--add the header and footer records to the table HAHeaderFooter
Declare @Header varchar(185), @Footer varchar(185)

select @Header = '0' + CONVERT(varchar(8),getdate(),112) + @MasterPartnerCode +  RewardsNow.dbo.ufn_Pad('','L',171,' ')
select @Footer =  '2' + RewardsNow.dbo.ufn_Pad(@TotalMiles,'L',9,'0') + RewardsNow.dbo.ufn_Pad(@TotalRecs,'L',5,'0') +  RewardsNow.dbo.ufn_Pad('','L',171,' ')
insert into HAHeaderFooter (OutDate, HAHeader, HAFooter)
					select @OutDate, @Header,  @Footer
----
----header
SELECT @Header as FixedWidthRec
--SELECT '0' + CONVERT(varchar(8),getdate(),112) + @MasterPartnerCode +  RewardsNow.dbo.ufn_Pad('','L',171,' ')
union ALL
--detail
select FixedWidthRec from HATransaction_OutLog where OutDate=@OutDate
union all
---footer
SELECT @Footer as FixedWidthRec
--select '2' + RewardsNow.dbo.ufn_Pad(@TotalMiles,'L',9,'0') + RewardsNow.dbo.ufn_Pad(@TotalRecs,'L',5,'0') +  RewardsNow.dbo.ufn_Pad('','L',171,' ')
				
END

	
