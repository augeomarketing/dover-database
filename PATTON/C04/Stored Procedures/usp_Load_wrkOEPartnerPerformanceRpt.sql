USE [C04]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_wrkOEPartnerPerformanceRpt]    Script Date: 06/11/2018 08:27:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bill Spack
-- Create date: 7/28/2016
-- Description:	Load data for wrkOEPartnerPerformanceRpt
-- =============================================
-- Change History
-- =============================================
-- PR   Date        Author		Description 
--		--------	-------		------------------------------------
-- 1    06/11/2018  Asim		Added  left join to new table dbo.HARevenueRates which keep track of how we calculate the REVENUE.
--								Please check comments inline below to see specIFic lines of code changed.
-- =============================================
-- =============================================
IF			 object_id('dbo.usp_Load_wrkOEPartnerPerformanceRpt','p') is not null
	DROP	PROCEDURE	dbo.usp_Load_wrkOEPartnerPerformanceRpt
GO

CREATE	PROCEDURE		dbo.usp_Load_wrkOEPartnerPerformanceRpt
	
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @ProcessMonth int = Month(GetDate())
	DECLARE @ProcessYear int = Year(GetDate())
	
	-- Adjust for January
	IF @ProcessMonth = 1
		BEGIN
			SET @ProcessMonth = 12
			SET @ProcessYear = @ProcessYear - 1
		END
	ELSE
		BEGIN
		-- The sproc runs on the first of the month so subtract 1 to get previous month
			SET @ProcessMonth = Month(GETDATE()) - 1
			SET @ProcessYear = Year(GetDate())			
		END	
	
	
	-- SET OptDate
	DECLARE @OptDate char(7)
	DECLARE @month varchar(2) 
	
	-- Make sure Month is 2 characters
	IF LEN(CAST(@ProcessMonth AS varchar(2))) = 1
		SET @month = '0' + CAST(@ProcessMonth AS varchar(2))
	ELSE 
		SET @month = CAST(@ProcessMonth AS varchar(2))	
	
	SET @OptDate = CAST(@ProcessYear AS varchar(4)) + @month + '%'
	
	
	-- Truncate table
	TRUNCATE TABLE dbo.wrkOE_PartnerPerformanceRpt
	
	-- Get Distinct Merchants
	DECLARE @Merchant varchar(250)
	DECLARE @OriginalMerchant varchar(250)
	DECLARE @Mid varchar(50)
	
	DECLARE @tblMerch table
	( Merchant varchar(100),
	  MID varchar(50)
	)	  
	
	INSERT INTO @tblMerch (Merchant,MID)
	SELECT DISTINCT Description, MerchantID 
	FROM HATransaction_OutLog	
	WHERE PartnerCode in ('ACIVC', 'ACIAX', 'ACIMC', 'ACDVC', 'ACDAX', 'ACDMC')
	
	-- Loop through dates AND pull data FROM HATransaction_OutLog
	DECLARE @cnt int 
	SET @cnt = 11
	WHILE (@cnt <> 0)  
	BEGIN		
		INSERT INTO dbo.wrkOE_PartnerPerformanceRpt
		SELECT M.Merchant										AS MerchantName,		
		@ProcessMonth											AS AccrualFileMonth,	
		@ProcessYear											AS AccrualFileYear,
		ISNULL(SUM(ISNULL(OL.Miles,0) * OL.SignMultiplier),0)	AS TotalMiles,	
		COUNT(*)												AS TotalTransactions,
		COUNT(distinct OL.Tipnumber)							AS TotalUniqueMemberTransactions,
		
		ISNULL(SUM((CASE	WHEN OL.Src = 'PR' OR (OL.PartnerCode in ('ACIVC', 'ACIAX', 'ACIMC', 'ACDVC', 'ACDAX', 'ACDMC') 
								AND OL.Src NOT in ('MO', 'ML', 'RN'))  THEN (OL.AwardPoints / 100)
							WHEN OL.Src in ('MO', 'ML','RN')  THEN OL.AwardAmount						 
					/* Change made on 6/11/2018 by Asim --  STARTS HERE */
							--ELSE OL.AwardAmount * 2 END) * 1),0)  AS TotalRevenues,
							ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2 )
					END) * 1),0)								AS TotalRevenues,
					/* Change made on 6/11/2018 by Asim --  ENDS HERE */
		'Direct'												AS ProgramName	,
		M.MID													AS MerchantID		
		
		FROM		dbo.HATransaction_OutLog	AS OL
		INNER JOIN	dbo.HATransactionResult_IN	AS HI	ON HI.ReferenceID = OL.ReferenceID 
		INNER JOIN	@tblMerch					AS M	ON M.Merchant		= OL.Description AND M.MID			= OL.MerchantID
		/* new code added by Asim - 6/5/2018 */
		LEFT JOIN	dbo.HARevenueRates			AS RR	ON RR.Description	= OL.Description AND RR.MerchantID	= OL.MerchantID
		/* new code added by Asim - 6/5/2018 */
		WHERE		OutDate like  @OptDate
		AND			OL.PartnerCode  IN ('ACIVC', 'ACIAX', 'ACIMC', 'ACDVC', 'ACDAX', 'ACDMC')
		AND			HI.RejectStatus = 0
		GROUP BY	Merchant,MID
		
		IF @ProcessMonth = 1
			BEGIN
				SET @ProcessMonth = 12
				SET @ProcessYear = @ProcessYear -1 
			END
		ELSE
			SET @ProcessMonth = @ProcessMonth - 1	
			
		-- Make sure Month is 2 characters
		IF LEN(CAST(@ProcessMonth AS varchar(2))) = 1
			SET @month = '0' + cast(@ProcessMonth AS varchar(2))
		ELSE
			SET @month = cast(@ProcessMonth AS varchar(2))	
		
		SET @OptDate = cast(@ProcessYear AS varchar(4)) + @month + '%'
		SET @cnt = @cnt - 1	
	
	END		
	
	-- Add  Partner type
	UPDATE P 
	SET P.ProgramName = 
					CASE 
						WHEN LEFT(OL.PartnerCode,3) = 'ACD' THEN 'Direct'
						WHEN LEFT(OL.PartnerCode,3) = 'ACI'
							AND P.MerchantName in ('ATHLETA', 'BANANA REPUBLIC', 'GAP', 'Gap Inc', 'OLD NAVY', 'Whole Foods Market, Inc.') THEN 'Indirect-PR'
						--WHEN LEFT(OL.PartnerCode,3) = 'ACI' AND  OL.Src = 'MO' then 'Indirect-MO' 
						ELSE 'Indirect-ML'
						--WHEN LEFT(OL.PartnerCode,3) = 'ACI' AND  OL.Src = 'RN' then 'Indirect-RN' 
					END
	FROM		dbo.wrkOE_PartnerPerformanceRpt AS P
	INNER JOIN	dbo.HATransaction_OutLog		AS OL ON OL.Description = P.MerchantName 
	AND			OL.MerchantID = P.MerchantID
	
	
	-- Load wrkPartnerPerformanceRpt_Condensed table
	DELETE			dbo.wrkOE_PartnerPerformanceRpt_Condensed

	INSERT INTO		dbo.wrkOE_PartnerPerformanceRpt_Condensed
	SELECT * FROM	dbo.wrkOE_PartnerPerformanceRpt
		
	-- Call sproc that condenses the merchant names
	EXEC usp_CondenseMerchantName 'wrkOE_PartnerPerformanceRpt_Condensed'
END
