USE [C04]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_wrkProgramPerformanceRpt]    Script Date: 06/06/2018 12:28:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bill Spack
-- Create date: 6/23/16
-- Description:	Rtrieves monthly data for the HA Program Performance Report
-- =============================================
/*
Mod log
02/02/18  --changed used by the loop to stop creating records
*/
-- Change History
-- =============================================
-- PR   Date        Author		Description 
--		--------	-------		------------------------------------
-- 1    06/06/2018  Asim		Added  left join to new table dbo.HARevenueRates which keep track of how we calculate the REVENUE.
--								Please check comments inline below to see specIFic lines of code changed.
-- =============================================
-- =============================================
IF			 object_id('dbo.usp_Load_wrkProgramPerformanceRpt','p') is not null
	DROP	PROCEDURE	dbo.usp_Load_wrkProgramPerformanceRpt
GO

CREATE	PROCEDURE		dbo.usp_Load_wrkProgramPerformanceRpt
	
AS
BEGIN
	SET NOCOUNT ON
	
	-- Get current date
	DECLARE @CurrentDate	date	= getdate()
	DECLARE @ProcessMonth	int		= Month(@CurrentDate)
	DECLARE @ProcessYear	int		= Year(@CurrentDate)
	DECLARE @sdate			varchar(20)
	DECLARE @edate			varchar(20)
	DECLARE @day			varchar(2)
	DECLARE @month			varchar(2)
	DECLARE @BegMonth		varchar(20)
	DECLARE @EndMonth		varchar(20)
	DECLARE @wrkMonth		int		= Month(@CurrentDate) - 1
	
	-- SET date range IF new year
	IF @ProcessMonth = 1
		BEGIN
			SET @ProcessMonth = 12
			SET @wrkMonth = 12
			SET @ProcessYear = @ProcessYear - 1
		END
		
	 	
	-- Make sure Day AND Month are 2 characters
	SET @day = CAST(Day(DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, (CAST(@ProcessMonth AS varchar(2)))+'/01/'+ CAST(@ProcessYear AS varchar(4)))+ 1, 0)))AS varchar(2))
	IF LEN(@day) = 1
		SET @day = '0' + @day
		
	SET @month = @ProcessMonth	
	IF LEN(CAST(@ProcessMonth AS varchar(2))) = 1
		SET @month = '0' + CAST(@ProcessMonth AS varchar(2))
		
	-- Append Time values to start AND end dates
	SET @sdate		= CAST(@ProcessYear AS varchar(4)) + @month + '01000000'
	SET @edate		= CAST(@ProcessYear AS varchar(4)) + @month + @day  + '595959'
	SET @BegMonth	= @month + '/01/' + cast(@ProcessYear AS varchar(4)) 
	SET @EndMonth	= @month + '/' + @day + '/' + cast(@ProcessYear AS varchar(4))
	
	-- Truncate table
	TRUNCATE TABLE dbo.wrkProgramPerformanceRpt
	
    -- Loop through dates AND pull data FROM HATransaction_OutLog
    --WHILE (@sdate <> '20151201000000')
    WHILE (@sdate <> '20161201000000')  --modIFied 02/02/18 this loops counting down AND wAS including 2016
    BEGIN
    -- BEGIN DIRECT
		INSERT INTO dbo.wrkProgramPerformanceRpt
		SELECT 'Direct' AS ProgramName,
			@ProcessYear AS AccrualFileYear,
			@ProcessMonth AS AccrualFileMonth,		
			COUNT(*) AS TotalTransactions,
			0 AS TotalTransactionPercentChange,
			
			ISNULL(SUM(	CASE	WHEN OL.SignMultiplier = -1	AND OL.TranAmt < 0  THEN ISNULL(OL.TranAmt * 1,0)
								ELSE ISNULL((OL.TranAmt * OL.SignMultiplier),0)
						END),0)		
			AS TotalSales,
			 	
			0 AS TotalSalePercentChange,
			ISNULL(SUM(OL.Miles * OL.SignMultiplier),0) AS TotalMiles,
			0 AS TotalMilePercentChange,
			
			(SELECT COUNT(*) 
			FROM HATransaction_OutLog OL
				inner join [C04].[dbo].HATransactionResult_IN tr on tr.ReferenceID = OL.ReferenceID
			WHERE
					OutDate >= @sdate AND
					OutDate <= @edate AND
					OL.PartnerCode = 'ACDHC' AND
					tr.RejectStatus = 0)
			AS TotalHACCTransactions,
			
			0 AS TotalHACCTransactionPercentChange,
			
			(SELECT COUNT(*)
			FROM HATransaction_OutLog OL
				inner join [C04].[dbo].HATransactionResult_IN tr on tr.ReferenceID = OL.ReferenceID
			WHERE
					OutDate >= @sdate AND
					OutDate <= @edate AND
					tr.RejectStatus = 0 AND
					OL.PartnerCode = 'ACDHD')
		    AS TotalHADBTransactions,
			
			0 AS TotalHADBTransactionPercentChange,
			
			(SELECT COUNT(*)
			FROM HATransaction_OutLog OL
				inner join [C04].[dbo].HATransactionResult_IN tr on tr.ReferenceID = OL.ReferenceID
			WHERE
					OutDate >= @sdate AND
					OutDate <= @edate AND
					tr.RejectStatus = 0 AND
					(OL.PartnerCode <> 'ACDHD' AND OL.PartnerCode <> 'ACDHC' AND OL.PartnerCode like 'ACD%'))
			 AS TotalOETransactions
			
			,0 AS TotalOETransactionPercentChange			
			,COUNT(distinct OL.Tipnumber) AS TotalUniqueMemberTransactions
			,0 AS TotalUniqueMemberTransactionPercentChange				
			
			,ISNULL(SUM((	CASE	WHEN OL.Src = 'PR' OR (OL.PartnerCode in ('ACDVC', 'ACDAX', 'ACDMC') 
										AND OL.Src not in ('MO', 'ML','RN'))	THEN (OL.AwardPoints / 100)
									WHEN OL.Src in ('MO', 'ML','RN')			THEN OL.AwardAmount
									/* Change made on 6/4/2018 by Asim --  STARTS HERE */
									--ELSE OL.AwardAmount * 2
									ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2 )
									/* Change made on 6/4/2018 by Asim --  ENDS HERE */
							END)
							*
							CASE	WHEN OL.SignMultiplier = -1 AND ISNULL(OL.TranAmt,0) <= 0  THEN  1
									ELSE OL.SignMultiplier
							END),0) 
			AS TotalRevenues
			
			,0 AS TotalRevenuePercentChange
			,@BegMonth AS MonthStartDate
			,@EndMonth AS MonthEndDate
			FROM HATransaction_OutLog OL
			LEFT OUTER JOIN dbo.HAMerchantFees			AS MF	ON MF.MerchantId	= OL.MerchantID
			INNER JOIN		dbo.HATransactionResult_IN	AS TR	ON tr.ReferenceID	= OL.ReferenceID
			/* new code added by Asim - 6/5/2018 */
			LEFT JOIN		dbo.HARevenueRates			AS M	ON M.Description	= OL.Description AND M.MerchantID = OL.MerchantID
			/* new code added by Asim - 6/5/2018 */
			WHERE OutDate between @sdate AND @edate
			AND TR.RejectStatus = 0
			AND OL.PartnerCode  LIKE 'ACD%'

-- End Direct


-- BEGIN Indirect		
		INSERT INTO dbo.wrkProgramPerformanceRpt
			SELECT 'Indirect' AS ProgramName,
			@ProcessYear AS AccrualFileYear,
			@ProcessMonth AS AccrualFileMonth,		
			COUNT(*) AS TotalTransactions,
			0 AS TotalTransactionPercentChange,
			
			ISNULL(SUM(	CASE	WHEN OL.SignMultiplier = -1	AND OL.TranAmt < 0  THEN ISNULL(OL.TranAmt * 1,0)
								ELSE ISNULL((OL.TranAmt * OL.SignMultiplier),0)
						END),0)		
			AS TotalSales,
			
			0 AS TotalSalePercentChange,
			ISNULL(SUM(OL.Miles * OL.SignMultiplier),0) AS TotalMiles,
			0 AS TotalMilePercentChange,	
			
			(SELECT COUNT(*) 
			FROM dbo.HATransaction_OutLog OL
			INNER JOIN dbo.HATransactionResult_IN tr ON tr.ReferenceID = OL.ReferenceID
			WHERE	OutDate >= @sdate
			AND		OutDate <= @edate
			AND		tr.RejectStatus = 0
			AND		OL.PartnerCode = 'ACIHC') AS TotalHACCTransactions,
			
			0 AS TotalHACCTransactionPercentChange,	
			
			(SELECT COUNT(*) 
			FROM dbo.HATransaction_OutLog OL
			INNER JOIN dbo.HATransactionResult_IN tr ON tr.ReferenceID = OL.ReferenceID
			WHERE	OutDate >= @sdate
			AND		OutDate <= @edate
			AND		tr.RejectStatus = 0
			AND		OL.PartnerCode = 'ACIHD') AS TotalHADBTransactions,

			0 AS TotalHADBTransactionPercentChange,
			
			(SELECT COUNT(*)
			FROM dbo.HATransaction_OutLog OL
			INNER JOIN dbo.HATransactionResult_IN tr ON tr.ReferenceID = OL.ReferenceID
			WHERE	OutDate >= @sdate
			AND		OutDate <= @edate
			AND		tr.RejectStatus = 0
			AND		(OL.PartnerCode <> 'ACIHD' AND OL.PartnerCode <> 'ACIHC' AND OL.PartnerCode LIKE 'ACI%')) 
			AS TotalOETransactions
			
			,0 AS TotalOETransactionPercentChange					
			,COUNT(distinct OL.Tipnumber) AS TotalUniqueMemberTransactions		
			,0 AS TotalUniqueMemberTransactionPercentChange
			
			,ISNULL(SUM((	CASE	WHEN OL.Src = 'PR' OR (OL.PartnerCode in ('ACIVC', 'ACIAX', 'ACIMC') 
										AND OL.Src not in ('MO', 'ML','RN'))	THEN (OL.AwardPoints / 100)
									WHEN OL.Src in ('MO', 'ML','RN')			THEN OL.AwardAmount
									/* Change made on 6/4/2018 by Asim --  STARTS HERE */
									--ELSE OL.AwardAmount * 2 END)  *
									ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2 )
							END)	/* Change made on 6/4/2018 by Asim --  ENDS HERE */
							*
							CASE	WHEN OL.SignMultiplier = -1 AND ISNULL(OL.TranAmt,0) <= 0  THEN  1
									ELSE OL.SignMultiplier
							END),0)   
			AS TotalRevenues
			
			,0 AS TotalRevenuePercentChange
			,@BegMonth AS MonthStartDate,
			@EndMonth AS MonthEndDate
			FROM			dbo.HATransaction_OutLog OL
			INNER JOIN		dbo.HATransactionResult_IN	AS tr	ON tr.ReferenceID = OL.ReferenceID
			/* new code added by Asim - 6/5/2018 */
			LEFT JOIN		dbo.HARevenueRates			AS M	ON M.Description = OL.Description AND M.MerchantID = OL.MerchantID
			/* new code added by Asim - 6/5/2018 */
			WHERE OutDate between @sdate AND @edate
			AND tr.RejectStatus = 0
			AND OL.PartnerCode like 'ACI%'

-- END Indirect


-- BEGIN Online Mall		
		INSERT INTO dbo.wrkProgramPerformanceRpt
			SELECT 'Online Mall'							AS ProgramName, 
			@ProcessYear									AS AccrualFileYear,
			@ProcessMonth									AS AccrualFileMonth,		
			COUNT(*)										AS TotalTransactions,
			0												AS TotalTransactionPercentChange,
			
			ISNULL(Sum(CASE WHEN OL.SignMultiplier = -1	AND OL.TranAmt < 0  THEN ISNULL(OL.TranAmt * 1,0)
							ELSE ISNULL((OL.TranAmt * OL.SignMultiplier),0)
							END),0)		
			AS TotalSales
			
			,0												AS TotalSalePercentChange
			,ISNULL(SUM(OL.Miles * OL.SignMultiplier),0)	AS TotalMiles
			,0 AS TotalMilePercentChange
			,0 AS TotalHACCTransactions
			,0 AS TotalHACCTransactionPercentChange
			,0 AS TotalHADBTransactions
			,0 AS TotalHADBTransactionPercentChange
			,0 AS TotalOETransactions
			,0 AS TotalOETransactionPercentChange
			,COUNT(distinct OL.Tipnumber)					AS TotalUniqueMemberTransactions
			,0												AS TotalUniqueMemberTransactionPercentChange
			
			,ISNULL(SUM((CASE	WHEN OL.Src = 'PR'					THEN OL.AwardPoints / 100
								WHEN OL.Src in ('MO', 'ML','RN')	THEN OL.AwardAmount
								/* Change made on 6/4/2018 by Asim --  STARTS HERE */
								--ELSE OL.AwardAmount * 2 END) 
								ELSE OL.AwardAmount * ISNULL(CentsPerMile, 2 )
						END)/* Change made on 6/4/2018 by Asim --  ENDS HERE */
						* 
						CASE	WHEN OL.SignMultiplier = -1 AND OL.TranAmt < 0  THEN  1
								ELSE OL.SignMultiplier
						END),0)  
															AS TotalRevenues
															
			,0												AS TotalRevenuePercentChange
			,@BegMonth										AS MonthStartDate
			,@EndMonth										AS MonthEndDate	
			FROM		dbo.HATransaction_OutLog OL
			INNER JOIN	dbo.HATransactionResult_IN	AS TR	ON tr.ReferenceID = OL.ReferenceID
			/* new code added by Asim - 6/5/2018 */
			LEFT JOIN	dbo.HARevenueRates			AS M	ON M.Description = OL.Description AND M.MerchantID = OL.MerchantID
			/* new code added by Asim - 6/5/2018 */
			WHERE OL.OutDate between @sdate AND @edate
			AND TR.RejectStatus = 0
			AND OL.PartnerCode NOT LIKE 'ACD%'
			AND OL.PartnerCode NOT LIKE 'ACI%'
			AND OL.PartnerCode <> 'AIMCS'
			AND OL.PartnerCode <> 'AIMHP'
			AND OL.PartnerCode <> 'AIMAC'
-- END Online Mall

			
		-- ReSET Loop counter
		IF @ProcessMonth = 1
			BEGIN
				SET @ProcessMonth = 12
				SET @ProcessYear = @ProcessYear - 1
			END
		ELSE
			SET @ProcessMonth = @ProcessMonth - 1	
			
		-- Make get last date of the next month  
		SET @day = CAST(Day(DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, (CAST(@ProcessMonth AS varchar(2)))+'/01/'+ CAST(@ProcessYear AS varchar(4)))+ 1, 0)))AS varchar(2))
		
		--make sure Month is 2 characters			
		IF LEN(CAST(@ProcessMonth AS varchar(2))) = 1
			SET @month = '0' + CAST(@ProcessMonth AS varchar(2))
		ELSE
			SET @month = CAST(@ProcessMonth AS varchar(2))
			
		-- Append Time values to start AND end dates
		SET @sdate = CAST(@ProcessYear AS varchar(4)) + @month + '01000000'
		SET @edate = CAST(@ProcessYear AS varchar(4)) + @month + @day  + '595959'	
		SET @BegMonth = @month + '/01/' + cast(@ProcessYear AS varchar(4)) 
		SET @EndMonth = @month + '/' + @day + '/' + cast(@ProcessYear AS varchar(4)) 
	END
	
	-- Calculate Change Percentages
	EXEC dbo.usp_CalculateTotalPercentageChange_RptProgramPerformance
	
	-- Load wrkMonthYearOverYearRpt
	EXEC dbo.usp_Load_wrkMonthYearOverYearRpt @wrkMonth
	EXEC dbo.usp_Load_wrkYearOverYearRpt @wrkMonth

END
