USE [585AdvantageHeronLake]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 09/25/2009 14:33:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
