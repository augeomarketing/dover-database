Use [000Dow]

SELECT 
LEFT(Tipnumber, 3) AS inst,
clientname as 'FI Name',
datename(mm, LEFT(GETDATE() - DAY(GETDATE()), 12))+' '+ datename(yy, LEFT(GETDATE() - DAY(GETDATE()), 12)) AS 'Report Date', 
COUNT(LEFT(Tipnumber, 3)) AS 'Participating Customers', 
(select count(Tipnumber) from Monthly_statement_file a where Pointsend >= 750 and left(Monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as 'Eligible Customer Count',
(select count(Tipnumber) from Monthly_statement_file a where Pointsend < 750 and left(Monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as 'Ineligible Customer Count',
cast(SUM(Pointsend) as int) AS 'Points Outstanding',
cast((select sum((floor(Pointsend/750))*750) from Monthly_statement_file a where Pointsend >= 750 and left(Monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as int) as 'Points Available to Redeem',
cast((select avg((floor(Pointsend/750))*750) from Monthly_statement_file a where Pointsend >= 750 and left(Monthly_statement_file.Tipnumber,3) = left(a.Tipnumber,3) Group by left(Tipnumber,3)) as int) as 'Average Points per Eligible Customer',
cast(avg(Pointsend) as int) as 'Average Points per Part. Customer',
cast(SUM(PointsPurchased) as int) AS 'Points Added for Purchases', 
cast(SUM(PointsBonus) as int) AS 'Points Added for Bonus',
cast(sum(PointsAdded) as int) as 'Points Added/ Misc', 
cast(SUM(PointsIncreased) as int) AS 'Total Points Added', 
cast(SUM(PointsRedeemed) as int) AS 'Points Subtracted for Redeemed', 
cast(SUM(PointsReturned) as int) AS 'Points Subtracted for Returns', 
cast(sum(PointsSubtracted) as int) as 'Points Subtracted/ Misc', 
cast(SUM(PointsDecreased) as int) AS 'Total Points Subtracted'
FROM Monthly_statement_file, login.dbo.client
where login.dbo.client.tipfirst = left(Tipnumber,3)
GROUP BY LEFT(Tipnumber, 3), clientname
ORDER BY LEFT(Tipnumber, 3)