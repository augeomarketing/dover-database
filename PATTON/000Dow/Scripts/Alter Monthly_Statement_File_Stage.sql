/*
   Tuesday, July 12, 201111:16:37 AM
   User: 
   Server: doolittle\rn
   Database: 000Dow
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Monthly_Statement_File_Stage
	(
	Tipnumber varchar(50) NOT NULL,
	Acctname1 varchar(40) NULL,
	Acctname2 varchar(40) NULL,
	Address1 varchar(40) NULL,
	Address2 varchar(40) NULL,
	Address3 varchar(40) NULL,
	CityStateZip varchar(50) NULL,
	stdate char(30) NULL,
	PointsBegin int NOT NULL,
	PointsEnd int NOT NULL,
	PointsPurchased int NOT NULL,
	PointsBonus int NOT NULL,
	PointsAdded int NOT NULL,
	PointsIncreased int NOT NULL,
	PointsRedeemed int NOT NULL,
	PointsReturned int NOT NULL,
	PointsSubtracted int NOT NULL,
	PointsDecreased int NOT NULL,
	Zip char(5) NULL,
	Acctnum char(25) NULL,
	LastFour char(4) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsBegin DEFAULT 0 FOR PointsBegin
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsAdded DEFAULT 0 FOR PointsAdded
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsIncreased DEFAULT 0 FOR PointsIncreased
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsRedeemed DEFAULT 0 FOR PointsRedeemed
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsReturned DEFAULT 0 FOR PointsReturned
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsSubtracted DEFAULT 0 FOR PointsSubtracted
GO
ALTER TABLE dbo.Tmp_Monthly_Statement_File_Stage ADD CONSTRAINT
	DF_Monthly_Statement_File_Stage_PointsDecreased DEFAULT 0 FOR PointsDecreased
GO
IF EXISTS(SELECT * FROM dbo.Monthly_Statement_File_Stage)
	 EXEC('INSERT INTO dbo.Tmp_Monthly_Statement_File_Stage (Tipnumber, Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, stdate, PointsBegin, PointsEnd, PointsPurchased, PointsBonus, PointsAdded, PointsIncreased, PointsRedeemed, PointsReturned, PointsSubtracted, PointsDecreased, Zip, Acctnum, LastFour)
		SELECT CONVERT(varchar(50), Tipnumber), Acctname1, Acctname2, Address1, Address2, Address3, CityStateZip, stdate, CONVERT(int, PointsBegin), CONVERT(int, PointsEnd), CONVERT(int, PointsPurchased), CONVERT(int, PointsBonus), CONVERT(int, PointsAdded), CONVERT(int, PointsIncreased), CONVERT(int, PointsRedeemed), CONVERT(int, PointsReturned), CONVERT(int, PointsSubtracted), CONVERT(int, PointsDecreased), Zip, Acctnum, LastFour FROM dbo.Monthly_Statement_File_Stage WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Monthly_Statement_File_Stage
GO
EXECUTE sp_rename N'dbo.Tmp_Monthly_Statement_File_Stage', N'Monthly_Statement_File_Stage', 'OBJECT' 
GO
ALTER TABLE dbo.Monthly_Statement_File_Stage ADD CONSTRAINT
	PK_Monthly_Statement_File_Stage PRIMARY KEY CLUSTERED 
	(
	Tipnumber
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
