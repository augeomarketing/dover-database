USE [000Dow]
GO
/****** Object:  Table [dbo].[wrkstatement]    Script Date: 08/30/2016 15:30:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkstatement](
	[tipnumber] [varchar](15) NOT NULL,
	[ccdacct] [varchar](16) NOT NULL,
	[enddate] [varchar](8) NULL,
	[endbalance] [varchar](11) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
