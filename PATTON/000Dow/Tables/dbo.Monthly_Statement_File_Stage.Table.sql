USE [000Dow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsBegin]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsIncreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsRedeemed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsReturned]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsSubtracted]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_Stage_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File_Stage] DROP CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsDecreased]
END

GO

USE [000Dow]
GO

/****** Object:  Table [dbo].[Monthly_Statement_File_Stage]    Script Date: 07/12/2011 11:17:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File_Stage]
GO

USE [000Dow]
GO

/****** Object:  Table [dbo].[Monthly_Statement_File_Stage]    Script Date: 07/12/2011 11:17:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Monthly_Statement_File_Stage](
	[Tipnumber] [varchar](50) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [int] NOT NULL,
	[PointsEnd] [int] NOT NULL,
	[PointsPurchased] [int] NOT NULL,
	[PointsBonus] [int] NOT NULL,
	[PointsAdded] [int] NOT NULL,
	[PointsIncreased] [int] NOT NULL,
	[PointsRedeemed] [int] NOT NULL,
	[PointsReturned] [int] NOT NULL,
	[PointsSubtracted] [int] NOT NULL,
	[PointsDecreased] [int] NOT NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
 CONSTRAINT [PK_Monthly_Statement_File_Stage] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsReturned]  DEFAULT ((0)) FOR [PointsReturned]
GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Monthly_Statement_File_Stage] ADD  CONSTRAINT [DF_Monthly_Statement_File_Stage_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

