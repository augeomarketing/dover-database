USE [000Dow]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 01/11/2010 16:22:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 99 ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6754599E]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6754599E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6754599E]  DEFAULT (0) FOR [MonthBeg1]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__68487DD7]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__68487DD7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__68487DD7]  DEFAULT (0) FOR [MonthBeg2]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__693CA210]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__693CA210]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__693CA210]  DEFAULT (0) FOR [MonthBeg3]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6A30C649]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6A30C649]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6A30C649]  DEFAULT (0) FOR [MonthBeg4]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6B24EA82]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6B24EA82]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6B24EA82]  DEFAULT (0) FOR [MonthBeg5]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6C190EBB]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6C190EBB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6C190EBB]  DEFAULT (0) FOR [MonthBeg6]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6D0D32F4]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6D0D32F4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6D0D32F4]  DEFAULT (0) FOR [MonthBeg7]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6E01572D]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6E01572D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6E01572D]  DEFAULT (0) FOR [MonthBeg8]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6EF57B66]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6EF57B66]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6EF57B66]  DEFAULT (0) FOR [MonthBeg9]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__6FE99F9F]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__6FE99F9F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__6FE99F9F]  DEFAULT (0) FOR [MonthBeg10]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__70DDC3D8]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__70DDC3D8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__70DDC3D8]  DEFAULT (0) FOR [MonthBeg11]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__71D1E811]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__71D1E811]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__71D1E811]  DEFAULT (0) FOR [MonthBeg12]
END


END
GO
