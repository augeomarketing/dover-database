USE [000Dow]
GO
/****** Object:  Table [dbo].[DOW_CUST_IN]    Script Date: 01/11/2010 16:22:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DOW_CUST_IN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[DOW_CUST_IN](
	[ACCTID] [nvarchar](16) NULL,
	[ACCTNAME1] [varchar](35) NULL,
	[ACCTNAME2] [varchar](35) NULL,
	[FILLER1] [varchar](5) NULL,
	[E_STATUS] [nvarchar](1) NULL,
	[I_STATUS] [nvarchar](1) NULL,
	[FILLER2] [varchar](45) NULL,
	[OLDCCNUM] [nvarchar](16) NULL,
	[FILLER3] [nvarchar](1) NULL,
	[TRAVNO] [nvarchar](10) NULL,
	[ADDR1] [nvarchar](32) NULL,
	[ADDR2] [nvarchar](32) NULL,
	[CITY] [varchar](32) NULL,
	[STATE] [varchar](32) NULL,
	[ZIP] [nvarchar](5) NULL,
	[HMPHONE] [nvarchar](10) NULL,
	[WKPHONE] [nvarchar](10) NULL,
	[FILLER4] [varchar](35) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
