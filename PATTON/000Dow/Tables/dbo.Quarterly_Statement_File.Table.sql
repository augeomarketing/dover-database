USE [000Dow]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 01/11/2010 16:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchased] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturned] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[CISNO] [nchar](8) NULL,
 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 99 ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__1AD3FDA4]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1AD3FDA4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1AD3FDA4]  DEFAULT (0) FOR [PointsBegin]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__1BC821DD]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1BC821DD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1BC821DD]  DEFAULT (0) FOR [PointsEnd]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__1CBC4616]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1CBC4616]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1CBC4616]  DEFAULT (0) FOR [PointsPurchased]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__1DB06A4F]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1DB06A4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1DB06A4F]  DEFAULT (0) FOR [PointsBonus]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__1EA48E88]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1EA48E88]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1EA48E88]  DEFAULT (0) FOR [PointsAdded]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__1F98B2C1]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__1F98B2C1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1F98B2C1]  DEFAULT (0) FOR [PointsIncreased]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__208CD6FA]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__208CD6FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__208CD6FA]  DEFAULT (0) FOR [PointsRedeemed]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2180FB33]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2180FB33]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2180FB33]  DEFAULT (0) FOR [PointsReturned]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__22751F6C]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__22751F6C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__22751F6C]  DEFAULT (0) FOR [PointsSubtracted]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__236943A5]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__236943A5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__236943A5]  DEFAULT (0) FOR [PointsDecreased]
END


END
GO
