USE [000Dow]
GO
/****** Object:  Table [dbo].[ACCTBP]    Script Date: 01/11/2010 16:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ACCTBP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[ACCTBP](
	[TIPNUMBER] [nvarchar](15) NULL,
	[TRANDATE] [smalldatetime] NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANCOUNT] [smallint] NULL,
	[TRANAMT] [float] NULL,
	[CARDTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[LASTACTIVE] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
