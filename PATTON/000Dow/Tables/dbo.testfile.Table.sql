USE [000Dow]
GO
/****** Object:  Table [dbo].[testfile]    Script Date: 01/11/2010 16:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[testfile]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[testfile](
	[tipnumber] [varchar](15) NOT NULL,
	[totpts] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
