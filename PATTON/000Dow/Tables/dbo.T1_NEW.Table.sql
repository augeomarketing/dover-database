USE [000Dow]
GO
/****** Object:  Table [dbo].[T1_NEW]    Script Date: 01/11/2010 16:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[T1_NEW]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[T1_NEW](
	[TIPNUMBER] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCTID] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANCOUNT] [float] NULL,
	[TRANAMT] [float] NULL,
	[CARDTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[DTLASTACT] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
