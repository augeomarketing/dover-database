USE [000Dow]
GO
/****** Object:  Table [dbo].[IMPORT]    Script Date: 01/11/2010 16:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[IMPORT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[IMPORT](
	[CORP_ID] [char](4) NULL,
	[CORP_ID_EXT] [char](2) NULL,
	[ACCTID] [char](16) NULL,
	[REJ_CODE] [char](1) NULL,
	[CARD_CODE] [char](1) NULL,
	[TRAN_CODE] [char](2) NULL,
	[REASON_CODE] [char](2) NULL,
	[VISA_REIMB] [char](1) NULL,
	[MC_MBA_IRD] [char](2) NULL,
	[MICRO_REF_NO] [char](23) NULL,
	[SIC] [char](4) NULL,
	[MERCHANT_NAME] [char](25) NULL,
	[MERCHANT_CITY] [char](13) NULL,
	[MERCHANT_STATE] [char](3) NULL,
	[MERCHANT_COUNTRY] [char](3) NULL,
	[AUTH_NUM] [char](6) NULL,
	[TRAN_AMT] [char](15) NULL,
	[TRAN_DT] [char](8) NULL,
	[LEFTOVER] [char](169) NULL,
	[RN_TRANCODE] [char](2) NULL,
	[RN_POINTS] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
