USE [000Dow]
GO
/****** Object:  Table [dbo].[Reg]    Script Date: 01/11/2010 16:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Reg]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
END
GO
