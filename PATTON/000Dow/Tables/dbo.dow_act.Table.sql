USE [000Dow]
GO
/****** Object:  Table [dbo].[dow_act]    Script Date: 01/11/2010 16:22:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[dow_act]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[dow_act](
	[CuNo] [nvarchar](255) NULL,
	[FmDate] [nvarchar](255) NULL,
	[AcctID] [nvarchar](255) NULL,
	[RecType] [nvarchar](255) NULL,
	[DetailType] [nvarchar](255) NULL,
	[PostDate] [nvarchar](255) NULL,
	[TranDate] [nvarchar](255) NULL,
	[Descr] [nvarchar](255) NULL,
	[Amount] [float] NULL,
	[Points] [bigint] NULL
) ON [PRIMARY]
END
GO
