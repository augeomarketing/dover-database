USE [000Dow]
GO
/****** Object:  Table [dbo].[FIX]    Script Date: 01/11/2010 16:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FIX]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[FIX](
	[Tipnumber] [nvarchar](15) NULL,
	[DATE1] [nvarchar](8) NULL,
	[ACCTNO] [nvarchar](16) NULL,
	[TRANCD] [nvarchar](3) NULL,
	[DISCRIPT] [nvarchar](50) NULL,
	[TRANAMT] [float] NULL,
	[TRUNCAMT] [float] NULL
) ON [PRIMARY]
END
GO
