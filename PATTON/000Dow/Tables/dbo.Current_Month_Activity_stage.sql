USE [000Dow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_stage_EndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_stage] DROP CONSTRAINT [DF_Current_Month_Activity_stage_EndingPoints]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_stage_Increases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_stage] DROP CONSTRAINT [DF_Current_Month_Activity_stage_Increases]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_stage_Decreases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_stage] DROP CONSTRAINT [DF_Current_Month_Activity_stage_Decreases]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Current_Month_Activity_stage_AdjustedEndingPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity_stage] DROP CONSTRAINT [DF_Current_Month_Activity_stage_AdjustedEndingPoints]
END

GO

USE [000Dow]
GO

/****** Object:  Table [dbo].[Current_Month_Activity_stage]    Script Date: 03/22/2010 10:45:31 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Current_Month_Activity_stage]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[Current_Month_Activity_stage]
GO

USE [000Dow]
GO

/****** Object:  Table [dbo].[Current_Month_Activity_stage]    Script Date: 03/22/2010 10:45:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Current_Month_Activity_stage](
	[Tipnumber] [nchar](15) NOT NULL,
	[EndingPoints] [int] NOT NULL,
	[Increases] [int] NOT NULL,
	[Decreases] [int] NOT NULL,
	[AdjustedEndingPoints] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Current_Month_Activity_stage] ADD  CONSTRAINT [DF_Current_Month_Activity_stage_EndingPoints]  DEFAULT ((0)) FOR [EndingPoints]
GO

ALTER TABLE [dbo].[Current_Month_Activity_stage] ADD  CONSTRAINT [DF_Current_Month_Activity_stage_Increases]  DEFAULT ((0)) FOR [Increases]
GO

ALTER TABLE [dbo].[Current_Month_Activity_stage] ADD  CONSTRAINT [DF_Current_Month_Activity_stage_Decreases]  DEFAULT ((0)) FOR [Decreases]
GO

ALTER TABLE [dbo].[Current_Month_Activity_stage] ADD  CONSTRAINT [DF_Current_Month_Activity_stage_AdjustedEndingPoints]  DEFAULT ((0)) FOR [AdjustedEndingPoints]
GO


