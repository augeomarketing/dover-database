USE [000Dow]
GO
/****** Object:  Table [dbo].[tnb_act]    Script Date: 01/11/2010 16:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tnb_act]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tnb_act](
	[CUNum] [char](4) NULL,
	[Trandate] [varchar](8) NULL,
	[AcctID] [char](16) NULL,
	[Filler1] [char](14) NULL,
	[TranDesc] [char](25) NULL,
	[TranCity] [char](13) NULL,
	[TranState] [char](2) NULL,
	[TranCountry] [char](3) NULL,
	[Amount] [varchar](8) NULL,
	[Histdate] [smalldatetime] NULL,
	[Ratio] [varchar](2) NULL,
	[Trancode] [varchar](3) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tnb_act_Ratio]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tnb_act_Ratio]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tnb_act] ADD  CONSTRAINT [DF_tnb_act_Ratio]  DEFAULT (1) FOR [Ratio]
END


END
GO
IF Not EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_tnb_act_Trancode]') AND type = 'D')
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tnb_act_Trancode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tnb_act] ADD  CONSTRAINT [DF_tnb_act_Trancode]  DEFAULT (63) FOR [Trancode]
END


END
GO
