USE [000Dow]
GO
/****** Object:  Table [dbo].[AFFILIAT_STAGE]    Script Date: 01/11/2010 16:22:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AFFILIAT_STAGE]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[AFFILIAT_STAGE](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[AffiliatFlag] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
