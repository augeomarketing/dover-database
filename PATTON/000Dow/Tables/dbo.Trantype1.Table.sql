USE [000Dow]
GO
/****** Object:  Table [dbo].[Trantype1]    Script Date: 01/11/2010 16:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Trantype1]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Trantype1](
	[TRANCODE] [nvarchar](2) NULL,
	[TRANDESC] [nvarchar](40) NULL,
	[INcDEC] [nvarchar](1) NULL,
	[CNTAMTFXD] [nvarchar](1) NULL,
	[POINTS] [float] NULL,
	[Ratio] [float] NULL,
	[TYPECODE] [nvarchar](1) NULL
) ON [PRIMARY]
END
GO
