USE [000Dow]
GO
/****** Object:  Table [dbo].[FDIS_IN_BJQ]    Script Date: 01/11/2010 16:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FDIS_IN_BJQ]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[FDIS_IN_BJQ](
	[CorpID] [char](6) NOT NULL,
	[AcctID] [char](16) NOT NULL,
	[Codes1] [char](2) NULL,
	[Trancode] [char](2) NULL,
	[ReasonCode] [char](2) NULL,
	[Filler1] [char](30) NULL,
	[MerchName] [char](25) NULL,
	[MerchCity] [char](13) NULL,
	[MerchState] [char](3) NULL,
	[MerchCountry] [char](3) NULL,
	[AuthNum] [char](6) NULL,
	[Amount] [varchar](15) NULL,
	[TranDate] [char](8) NULL,
	[Points] [bigint] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
