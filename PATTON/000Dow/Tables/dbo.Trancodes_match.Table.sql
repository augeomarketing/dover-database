USE [000Dow]
GO
/****** Object:  Table [dbo].[Trancodes_match]    Script Date: 01/11/2010 16:22:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Trancodes_match]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Trancodes_match](
	[Dow_trancode] [varchar](3) NOT NULL,
	[RN_trancode] [varchar](3) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
