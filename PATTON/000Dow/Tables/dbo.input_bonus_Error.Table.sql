USE [000Dow]
GO
/****** Object:  Table [dbo].[input_bonus_Error]    Script Date: 01/11/2010 16:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[input_bonus_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[input_bonus_Error](
	[MemberNum] [char](16) NOT NULL,
	[Points] [numeric](18, 0) NULL,
	[Reason] [char](15) NULL,
	[TipNumber] [char](15) NULL,
	[Trancode] [char](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
