USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spUpdates]    Script Date: 01/20/2012 11:15:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdates]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdates]
GO

USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spUpdates]    Script Date: 01/20/2012 11:15:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdates] 
	
AS
BEGIN
	SET NOCOUNT ON;
	
	update [000Dow].dbo.CUSTOMER
	set lastname = RIGHT(rtrim(replace(replace(replace(replace(replace(ACCTNAME1,' JR', ''), ' SR', ''), ' II', ''), ' III', ''), ' IV','')), len(rtrim(replace(replace(replace(replace(replace(ACCTNAME1,' JR', ''), ' SR', ''), ' II', ''), ' III', ''), ' IV',''))) - [000Dow].dbo.RAT(' ', rtrim(replace(replace(replace(replace(replace(ACCTNAME1,' JR', ''), ' SR', ''), ' II', ''), ' III', ''), ' IV','')), 1))

	update [000Dow].dbo.AFFILIAT
	set LastName = (select LastName from CUSTOMER where TIPNUMBER = AFFILIAT.TIPNUMBER)
	where LastName is null or LastName = ''

	update [000Dow].dbo.CUSTOMER
	set STATUS = 'C'
	where TIPNUMBER not in (select TIPNUMBER from [000Dow].dbo.DOW_CUST_IN)

	Declare tipnumber cursor
	for select distinct rtrim(tipnumber) as tipnumber from [000Dow].dbo.customer where status = 'C'

	Declare @tipnumber varchar(15)
	Declare @sqlcmd nvarchar(4000)
	declare @ldlm smalldatetime = getdate() - day(getdate())

	open tipnumber
	Fetch next from tipnumber into @tipnumber
	while @@Fetch_Status = 0
		Begin
		  set @sqlcmd =
			'exec rewardsnow.dbo.usp_purgeSingleRNAcct ' + quotename(@tipnumber, '''') + ', ' + quotename(convert(nvarchar(20), @ldlm, 101), '''') + ', '' '', ''ssmith@rewardsnow.com'', ''purges'', 0, 0'
		  EXECUTE sp_executesql @SqlCmd
		  --print @sqlcmd
		  --print '---'
		  fetch Next from tipnumber into @tipnumber
		end
	Close tipnumber
	Deallocate tipnumber
	
END

GO

--exec [000Dow].dbo.spupdates
