USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[usp_NewCardDoublePointBonus]    Script Date: 06/19/2015 14:50:05 ******/
DROP PROCEDURE [dbo].[usp_NewCardDoublePointBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 5/2015
-- Description:	Award Double Points On purchases for new cards since conversion within 
--				90 days of being added
-- =============================================
CREATE PROCEDURE [dbo].[usp_NewCardDoublePointBonus]
	@TipFirst varchar(3)
	,@MonthEndDate date
	,@Debug int = 0  
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
DECLARE @sqlInsert NVARCHAR(MAX) =   
	REPLACE(
	'  
	insert into HISTORY_STAGE
	(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select hs.TIPNUMBER, null,	''<MONTHENDDATE>'', ''BI'', ''1'', hs.POINTS, ''Double Points Bonus'', ''NEW'',	''1'',	''0''
		from HISTORY_STAGE hs
		join CUSTOMER_STAGE cs
		on hs.TIPNUMBER = cs.TIPNUMBER
		where left(ACCTID,8) =''49077330''
		and TRANCODE =''63''
		and SECID=''NEW''
		and DATEDIFF(month, cs.dateadded, ''<MONTHENDDATE>'')<4 '
		, '<MONTHENDDATE>', @monthenddate)
		
if @debug = 0  
 EXEC sp_executesql @sqlInsert  
else   
 print @sqlInsert
   		
END
GO
