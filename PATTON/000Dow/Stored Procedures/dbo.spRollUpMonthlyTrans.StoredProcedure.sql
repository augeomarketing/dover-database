USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[spRollUpMonthlyTrans]    Script Date: 01/11/2010 16:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spRollUpMonthlyTrans]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'















/******************************************************************************/
/*    This will Roll up the Credit Transactions for OOODOW                    */
/* */
/*  - Update dbo.FDIS_ROLLUP                                                  */
/* BY:  B.QUINN  */
/* DATE: 07/2009   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spRollUpMonthlyTrans]   @runstart nvarchar(10), @RUNEND NVARCHAR (10) AS    

/* Test Data */
--declare @runstart nvarchar(10)
--DECLARE @RUNEND NVARCHAR (10)
--set  @runstart = ''09/01/2009'' 
--set  @RUNEND = ''09/30/2009'' 

DECLARE @MONTHSTART nvarCHAR(6)
DECLARE @MONTHEND nvarCHAR(6)
declare @stday char(2)
DECLARE @STMO CHAR(2)
DECLARE @STYR CHAR(2)
declare @endMO char(2)
declare @endday char(2)
declare @endYR char(2)
DECLARE @CorpID char (6) 
DECLARE @AcctID char (16) 
DECLARE @Codes1 char (2) 
DECLARE @Trancode char (2) 
DECLARE @ReasonCode char (2) 
DECLARE @Filler1 char (30) 
DECLARE @MerchName char (25) 
DECLARE @MerchCity char (13) 
DECLARE @MerchState char (3) 
DECLARE @MerchCountry char (3) 
DECLARE @AuthNum char (6) 
DECLARE @Amount varchar (15) 
DECLARE @Amount_N int 
DECLARE @TranDate char (8) 
DECLARE @Points bigint 
DECLARE @Tipnumber varchar (15)  
declare @dtot1 int
declare @dtot2 int
declare @TOTPUR INT
DECLARE @TOTRET INT

--PRINT @runstart
SET @STYR = RIGHT(@runstart,2)
--PRINT @STYR
SET @STMO = RIGHT(@runstart,10)
--PRINT @STMO
SET @STDAY = RIGHT(@runstart,7)
--PRINT @STDAY
--PRINT @runEND
SET @ENDYR = RIGHT(@RUNEND,2)
--PRINT @ENDYR
SET @ENDMO = RIGHT(@RUNEND,10)
--PRINT @ENDMO
SET @ENDDAY = RIGHT(@RUNEND,7)
--PRINT @ENDDAY
SET @MONTHSTART = (@STYR + @STMO + @STDAY)
--PRINT ''@MONTHSTART''
--PRINT @MONTHSTART
SET @MONTHEND = (@ENDYR + @ENDMO + @ENDDAY)
--PRINT ''@MONTHEND''
--PRINT @MONTHEND 

truncate table FDIS_ROLLUP

/*   - declare @ CURSOR AND OPEN TABLES  */
declare FDIS_CRSR  cursor 
for Select *  
From FDIS_IN
ORDER BY TRANCODE,ACCTID

Open FDIS_CRSR 


Fetch FDIS_CRSR  
into 
  @CorpID ,@AcctID,@Codes1,@Trancode,@ReasonCode,@Filler1,@MerchName,
  @MerchCity,@MerchState,@MerchCountry,@AuthNum,@Amount,@TranDate,@Points,@Tipnumber 
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN


if @Tipnumber is null
GOTO FETCH_NEXT

if @TranCode not in (''33'',''63'')
GOTO FETCH_NEXT


set @Amount_N = ''0''


if @Amount is null
set @Amount = ''0''

set @Amount_N = round(@Amount,0)


	/*  - Check For daily Record       */
 	if not exists (select acctid from dbo.FDIS_ROLLUP
                    where acctid = @acctid and trancode = @Trancode)	
		begin
		Insert into FDIS_ROLLUP
                (
 		 CorpID ,AcctID,Codes1,Trancode,ReasonCode,Filler1,MerchName,
  		 MerchCity,MerchState,MerchCountry,AuthNum,Amount,TranDate,Points,Tipnumber 
                )
	        values
 		(
 		 @CorpID ,@AcctID,@Codes1,@Trancode,@ReasonCode,@Filler1,@MerchName,
  		 @MerchCity,@MerchState,@MerchCountry,@AuthNum,@Amount_N,@TranDate,@Points,@Tipnumber 
		)
	 	end

	else
		begin
		update FDIS_ROLLUP
		set	 
		Amount = Amount + @Amount_N,
		Points = Points + @Points
		WHERE
		   ACCTID = @ACCTID and 
		   Trancode = @Trancode
		END

		
 



 
        

FETCH_NEXT:



	
	Fetch FDIS_CRSR   
	into 
	  @CorpID ,@AcctID,@Codes1,@Trancode,@ReasonCode,@Filler1,@MerchName,
  	  @MerchCity,@MerchState,@MerchCountry,@AuthNum,@Amount,@TranDate,@Points,@Tipnumber


END /*while */



 

GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:




close  FDIS_CRSR 
deallocate  FDIS_CRSR 





















' 
END
GO
