USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyStatementFileStage]    Script Date: 05/17/2010 09:56:25 ******/
if object_id('spMonthlyStatementFileStage') is not null
    DROP PROCEDURE [dbo].[spMonthlyStatementFileStage]
GO



CREATE PROCEDURE [dbo].[spMonthlyStatementFileStage] @StartDate varchar(10), @EndDate varchar(20) AS 

--declare @StartDate nvarchar(20), @EndDate nvarchar(20)
--set @StartDate = '2009-09-01'
--set @EndDate = '2009-09-30'

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000), @SQLDynamic nvarchar(1000)

set @MonthBegin = month(Convert(datetime, @StartDate) )
set @EndDate = @EndDate + ' 23:59:59'
print 'start'
print @StartDate
print 'end'
print @EndDate

/* Load the statement file from the customer table  */
truncate table Monthly_Statement_File_stage


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_history_TranCode]') and OBJECTPROPERTY(id, 
N'IsView') = 1)
drop view [dbo].[view_history_TranCode]

/* Create View */
set @SQLDynamic = 'create view view_history_TranCode as 
Select tipnumber, trancode, sum(points) as TranCodePoints 
from history_Stage 
where histdate between '''+@StartDate+''' and '''+ @EndDate +''' group by tipnumber, trancode'
exec sp_executesql @SQLDynamic

insert into Monthly_Statement_File_stage (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
select tipnumber, acctname1, acctname2, address1, address2, address3, address4, zipcode
from Customer_Stage


--update Monthly_Statement_File_stage
--set
--PointsBegin = '0',Pointsend = '0',PointsPurchased = '0',PointsBonus = '0',PointsAdded = '0',
--PointsIncreased = '0',PointsRedeemed = '0',PointsReturned = '0',PointsSubtracted = '0',
--PointsDecreased = '0'
--PointsToExpire = '0',DateOfExpiration = ' ',pointsexpired = '0'

/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File_stage 
set pointspurchased
= view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = '63' 

/* Load the statmement file CREDIT with returns            */
update Monthly_Statement_File_stage 
set pointsreturned
= view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode ='33'

/* Load the statmement file with bonuses            */
--update Monthly_Statement_File_stage 
--set pointsbonus
--= view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode like 'B%'

--update Monthly_Statement_File_stage 
--set pointsbonus = pointsbonus +
--view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'F0'

--update Monthly_Statement_File_stage 
--set pointsbonus = pointsbonus +
--view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'G0'

update msf
    set pointsbonus = vw.points
from dbo.Monthly_Statement_File_stage msf join (select tipnumber, sum(TranCodePoints) points
                                                from dbo.view_history_trancode
                                                where ((trancode like 'b%' or trancode like 'f%' or trancode like 'g%')
                                                or trancode in ('PI', 'PT', 'PP'))
                                                and trancode not in ('G9', 'F9')
                                                group by tipnumber) vw
    on msf.tipnumber = vw.tipnumber
  
/* Load the statmement file with plus adjustments */
update Monthly_Statement_File_stage 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
( view_history_TranCode.trancode ='IE'  )

update Monthly_Statement_File_stage 
set pointsadded 
= view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
( view_history_TranCode.trancode ='=2'  )


/* Add  DECREASED REDEEMED to PointsAdded */
update Monthly_Statement_File_stage 
set  pointsadded=pointsadded + 
 view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'DR'


/* Load the statmement file with total point increases */
update Monthly_Statement_File_stage
set pointsincreased= pointspurchased + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
--update Monthly_Statement_File_stage 
--set pointsredeemed =
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RM'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RT'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RU'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RV'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RC'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RS'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RP'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RG'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RB'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RD'

--update Monthly_Statement_File_stage 
--set pointsredeemed = pointsredeemed + 
--view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
--where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
--view_history_TranCode.trancode = 'RR'


update msf
    set pointsredeemed = vw.points
from dbo.Monthly_Statement_File_stage msf join (select tipnumber, sum(TranCodePoints) points
                                                from dbo.view_history_trancode
                                                where trancode like 'r%' 
                                                group by tipnumber) vw
    on msf.tipnumber = vw.tipnumber
    

/* Load the statmement file with Increases to Subtractions          */
update Monthly_Statement_File_stage 
set pointssubtracted
=  pointssubtracted + view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'IR' 

update Monthly_Statement_File_stage 
set pointssubtracted
=  pointssubtracted + view_history_TranCode.TranCodePoints  from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = '$-' 

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'DE'

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'XP'

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'G9'

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'F9'

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'PD'

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'PR'

update Monthly_Statement_File_stage 
set pointssubtracted = pointssubtracted +
view_history_TranCode.TranCodePoints from Monthly_Statement_File_stage, view_history_TranCode
where  view_history_TranCode.tipnumber = Monthly_Statement_File_stage.tipnumber and 
view_history_TranCode.trancode = 'PS'

/* Load the statmement file with total point decreases */
update Monthly_Statement_File_stage
set pointsdecreased = pointsredeemed+ pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File_stage
set pointsbegin = (select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where 
tipnumber=Monthly_Statement_File_stage.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File_stage.tipnumber)'

exec sp_executesql @SQLUpdate

/* Load the statmement file with beginning points */
update Monthly_Statement_File_stage
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Drop the view */
drop view view_history_TranCode
--select * from view_history_TranCode order by tipnumber 

GO


