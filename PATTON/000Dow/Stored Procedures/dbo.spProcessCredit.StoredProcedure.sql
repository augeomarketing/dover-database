USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[spProcessCredit]    Script Date: 01/11/2010 16:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spProcessCredit]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    This will Process the Monthly DebitTrans For DOW Bank                   */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessCredit] @Rundate datetime AS    

/* Test Data */
--declare @Rundate datetime
--set  @Rundate = ''09/30/2009'' 
declare @POSTDATE nvarchar(10) 
DECLARE @CorpID char (6) 
DECLARE @AcctID char (16) 
DECLARE @Codes1 char (2) 
DECLARE @Trancode char (2) 
DECLARE @ReasonCode char (2) 
DECLARE @Filler1 char (30) 
DECLARE @MerchName char (25) 
DECLARE @MerchCity char (13) 
DECLARE @MerchState char (3) 
DECLARE @MerchCountry char (3) 
DECLARE @AuthNum char (6) 
DECLARE @Amount varchar (15) 
DECLARE @TranDate char (8) 
DECLARE @Points bigint 
DECLARE @Tipnumber varchar (15)  
Declare @TRANDESC char(40)
Declare @LASTNAME char(40)
Declare @AcctType char(1)
Declare @DDA char(20)

Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @OVERAGE int

Declare @YTDEarned int

Declare @MAXPOINTSPERYEAR numeric(10)

declare @Result  int

declare @SECID NVARCHAR(10)
Declare @afTranAmt numeric(10)
Declare @afTranCode char(2)
Declare @afAcctType char(20)
Declare @afRatio  numeric
Declare @afAccTid varchar(25)
Declare @afTipNumber varchar(15)
Declare @afLastname char(50)
Declare @afDateAdded datetime
Declare @afSecId varchar(10)
Declare @afAcctStatus Varchar(1)
Declare @afAcctTypeDecs varchar(50)
Declare @afFound nvarchar(1)

Declare @RATIO nvarchar(2)
Declare @Rundate2 nvarchar(11)
set @Rundate2 = @rundate
set @Rundate  = @rundate2



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare FDIS_IN_CRSR  cursor 
for Select *
From dbo.FDIS_ROLLUP 
order by tipnumber asc

Open FDIS_IN_CRSR 
/*                  */



Fetch FDIS_IN_CRSR  
into 
  @CorpID ,@AcctID,@Codes1,@Trancode,@ReasonCode,@Filler1,@MerchName,
  @MerchCity,@MerchState,@MerchCountry,@AuthNum,@Amount,@TranDate,@Points,@Tipnumber

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = ''000DOW''
	SET @DateAdded = @Rundate 	
	SET @YTDEarned = 0





/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN


if @tipnumber is null
or @tipnumber = '' ''
goto fetch_next


	SET @RunAvailableNew = ''0''

	set @afFound = '' ''
	SET @YTDEarned = ''0''
	set @OVERAGE = ''0'' 
	SET @afFound = '' ''

	Select
	   @lastname = lastname,
	   @afAcctStatus = STATUS
	from
	   Customer_stage
	where
	   @tipnumber = tipnumber

--print ''@Tipnumber''
--print @tipnumber
--print ''@AcctID''
--print @AcctID

--print ''@Trancode''
--print @Trancode

--print ''@Amount''
--print @Amount

--print ''@Points''
--print @Points





	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat_Stage.AcctID from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.AcctID = @AcctID)	
	   begin
		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@Rundate,
 		@afAcctStatus,
		@YTDEarned,
 		'' '',
		''Credit'',
		''Credit Card''
		)
	   end
                                   
 	select 
	   @YTDEarned = YTDEarned
	From
	   Affiliat_Stage
	Where
	   acctid = @acctid 

     
/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

	select
	    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
	From
	    client


	IF @RunAvailableNew is NULL
           SET @RunAvailableNew = ''0''
	IF @YTDEarned is null
	   SET @YTDEarned = ''0''  




SET @Result = 0
SET @OVERAGE = 0


if @TRANCODE = ''63''
begin
  if  (@Points + @YTDEarned) > @MAXPOINTSPERYEAR
    Begin
      set @OVERAGE = (@Points + @YTDEarned) - @MAXPOINTSPERYEAR 
      set @YTDEarned = (@YTDEarned + @Points) - @OVERAGE
      set @RunAvailableNew = @RunAvailableNew + @Points - @OVERAGE
      set @Points = @Points - @OVERAGE
    End		 
  else
    Begin
      set @RunAvailableNew = @RunAvailableNew + @Points
      set @YTDEarned = @YTDEarned + @Points 		      		      
    End  
end


   


IF @TRANCODE = ''33''
   BEGIN                                           
     set @RunAvailableNew = @RunAvailableNew - @Points
   END  




/*  UPDATE THE CUSTOMER RECORD WITH THE debit TRANSACTION DATA          */


Update Customer_Stage
Set 
RunAvaliableNew = @RunAvailablenew  + RunAvaliableNew
Where @TipNumber = Customer_Stage.TIPNUMBER

--print ''@Tipnumber''
--print @tipnumber
--print ''@AcctID''
--print @AcctID

--print ''@Trancode''
--print @Trancode

--print ''@Amount''
--print @Amount

--print ''@Points''
--print @Points


		
IF @TRANCODE = ''63''
   BEGIN 
    Select 
	@TRANDESC = Description
	From [PATTON\RN].[RewardsNOW].[dbo].TranType
	where
	TranCode = ''63'' 
	   print ''tran desc''
	   print  @TRANDESC
	   Insert into HISTORY_Stage
	   (
	      TIPNUMBER
	      ,ACCTID
	      ,HISTDATE
	      ,TRANCODE
	      ,TranCount
	      ,POINTS
	      ,Description
	      ,SECID
	      ,Ratio
	      ,OVERAGE
	   )
	   Values
	   (
	    @TIPNUMBER
           ,@acctid
           ,@RunDate
           ,''63''
           ,''1''
           ,@POINTS
	   ,@TRANDESC	            
           ,''NEW''
	   ,''1''
           ,@OVERAGE
            )
    END	 
           
--print ''********* acctid rundate points transdesc overage''
--print @acctid
--print @RunDate
--print @POINTS
--print @TRANDESC
--print @OVERAGE

/* Post Returns    */


IF @TRANCODE = ''33''  
  Begin	
  Select 
      @TRANDESC = Description
  From [PATTON\RN].[RewardsNOW].[dbo].TranType
  where
   TranCode = ''33''
   print ''tran desc''
   print  @TRANDESC   
   Insert into HISTORY_Stage
   (
      TIPNUMBER
      ,ACCTID
      ,HISTDATE
      ,TRANCODE
      ,TranCount
      ,POINTS
      ,Description
      ,SECID
      ,Ratio
      ,OVERAGE
   )
   Values
   (
    @TIPNUMBER
    ,@acctid
    ,@RunDate
    ,''33''
    ,''1''
    ,@POINTS
    ,@TRANDESC	            
    ,''NEW''
    ,''-1''
    ,''0''
    )
 END	     



set @POINTS = ''0''

if @YTDEarned is null
   set @YTDEarned = 0

Select
 @lastname = lastname
from Customer_stage
where
 @tipnumber = tipnumber


Update AFFILIAT_stage
Set 
     YTDEarned = @YTDEarned 
    ,AcctStatus = @afAcctStatus
    ,Lastname = @lastname 
    ,custid = '' ''		
Where
     TIPNUMBER = @TIPNUMBER
and   ACCTID  = @Acctid           


       

FETCH_NEXT:

	set @TRANCODE = '' ''
	set @TRANDESC = '' ''

	
	Fetch FDIS_IN_CRSR   
	into 
	  @CorpID ,@AcctID,@Codes1,@Trancode,@ReasonCode,@Filler1,@MerchName,
 	  @MerchCity,@MerchState,@MerchCountry,@AuthNum,@Amount,@TranDate,@Points,@Tipnumber
END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:
close  FDIS_IN_CRSR 
deallocate  FDIS_IN_CRSR
' 
END
GO
