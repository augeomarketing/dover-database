USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateDOWINPUTS]    Script Date: 04/07/2010 10:18:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spUpdateDOWINPUTS]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spUpdateDOWINPUTS]
GO

USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateDOWINPUTS]    Script Date: 04/07/2010 10:18:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/******************************************************************************/
/*    This will Create the Customer Records for DOW Bank                  */
/* */
/*  - Update FDIS_IN      */
/* BY:  B.QUINN  */
/* DATE: 2/2010   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spUpdateDOWINPUTS] AS    

DECLARE @RUNDATE DATETIME
-- NO Declares Modify Input Table

--  UPDATE FDIS_IN FIELDS

delete from FDIS_in 
where Trancode not in ('05','06','25','26')

DELETE FROM FDIS_in 
WHERE AcctID NOT LIKE '4820%' 

UPDATE FDIS_in 
--SET Points = ROUND AMOUNT(CAST AS NUMERIC),-2)/100
set points = round((amount+50)/100,0)

UPDATE FDIS_in
SET 
FDIS_IN.Tipnumber = AFF.TIPNUMBER
FROM dbo.AFFILIAT_STAGE AS AFF
INNER JOIN dbo.FDIS_in AS FDI
ON FDI.AcctID = AFF.ACCTID
WHERE AFF.ACCTID IN (SELECT FDI.AcctID FROM FDIS_IN)
AND FDI.Tipnumber IS NULL
OR FDI.Tipnumber = ' '

UPDATE FDIS_in
SET 
FDIS_IN.Tipnumber = DCI.TIPNUMBER
FROM dbo.DOW_CUST_IN AS DCI
INNER JOIN dbo.FDIS_in AS FDI
ON FDI.AcctID = DCI.ACCTID
WHERE DCI.ACCTID IN (SELECT FDI.AcctID FROM FDIS_IN)
AND FDI.Tipnumber IS NULL
OR FDI.Tipnumber = ' '

UPDATE FDIS_IN SET Trancode = '63'
WHERE Trancode = '05'

UPDATE FDIS_IN SET Trancode = '33'
WHERE Trancode IN ('06','25','26')

-- UPDATE DOW_CUST_IN FIELDS

DELETE FROM DOW_CUST_IN
WHERE E_STATUS IN ('L','U','Z')

UPDATE DOW_CUST_IN
SET Tipnumber = ('00000' + TRAVNO)
WHERE Tipnumber IS NULL
OR Tipnumber = ' '

UPDATE DOW_CUST_IN
SET E_STATUS = ' '
WHERE E_STATUS = 'N' 

UPDATE DOW_CUST_IN
SET E_STATUS = I_STATUS
WHERE E_STATUS = ' ' 

UPDATE DOW_CUST_IN
SET E_STATUS = 'O'
WHERE E_STATUS = 'X' 

UPDATE DOW_CUST_IN
SET E_STATUS = 'C'
WHERE E_STATUS IN ('B','E','F')

UPDATE DOW_CUST_IN
SET E_STATUS = '*'
WHERE E_STATUS = 'A' 

UPDATE DOW_CUST_IN
SET E_STATUS = 'Y'
WHERE E_STATUS = 'I' 

SELECT * FROM FDIS_in

GO


