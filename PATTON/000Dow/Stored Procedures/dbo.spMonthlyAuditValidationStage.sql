USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyAuditValidationStage]    Script Date: 05/17/2010 09:55:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyAuditValidationStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyAuditValidationStage]
GO

USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyAuditValidationStage]    Script Date: 05/17/2010 09:55:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[spMonthlyAuditValidationStage] AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

delete from Monthly_Audit_ErrorFile 
                                                                          


	insert into Monthly_Audit_ErrorFile 
	(
 	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchased
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturned
	, pointssubtracted
	, pointsdecreased
 ) 
	select
	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchased
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturned
	, pointssubtracted
	, pointsdecreased
	from
	 Monthly_Statement_File_Stage
	 where pointsend <>
	 (select AdjustedEndingPoints from CURRENT_MONTH_ACTIVITY_Stage 	  
	  where Monthly_Statement_File_Stage.Tipnumber = CURRENT_MONTH_ACTIVITY_Stage.TIPNUMBER)
 









GO


