USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[usp_MthStmtFile_Extract]    Script Date: 08/30/2016 15:31:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Create statement file that must be exported to the FI
-- =============================================
CREATE PROCEDURE [dbo].[usp_MthStmtFile_Extract]
	-- Add the parameters for the stored procedure here
	 @MonthEndingDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @StartdateOut varchar(8), @EnddateOut varchar(8)
	
    -- Insert statements for procedure here
	set @EnddateOut = convert(varchar(8),@MonthEndingDate,112)

	truncate table wrkstatement

	insert into wrkstatement (tipnumber, ccdacct, enddate)
	select tipnumber, acctid, @EnddateOut
	from AFFILIAT_Stage
	where AcctType='PORTFOLIO'
		and AcctStatus='A'
	order by TIPNUMBER

	update wrkstatement
	set endbalance = msf.PointsEnd
	from wrkstatement wks join Monthly_Statement_File msf on wks.tipnumber=msf.Tipnumber

END
GO
