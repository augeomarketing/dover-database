USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivityLoadstage]    Script Date: 05/17/2010 09:57:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCurrentMonthActivityLoadstage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCurrentMonthActivityLoadstage]
GO

USE [000Dow]
GO

/****** Object:  StoredProcedure [dbo].[spCurrentMonthActivityLoadstage]    Script Date: 05/17/2010 09:57:22 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spCurrentMonthActivityLoadstage] @EndDate varchar(20)
AS

delete from Current_Month_Activity_stage 

insert into Current_Month_Activity_stage (Tipnumber, EndingPoints)
select tipnumber, (RunAvaliableNew + runavailable) 
from Customer_stage

set @EndDate = @EndDate + ' 23:59:59'


/* Load the current activity table with increases for the current month         */
update Current_Month_Activity_stage
set increases=(select sum(points) from history_stage where histdate>@enddate and ratio='1'
 and History_stage.Tipnumber = Current_Month_Activity_stage.Tipnumber )
where exists(select * from history_stage where histdate>@enddate and ratio='1'
 and History_stage.Tipnumber = Current_Month_Activity_stage.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity_stage
set decreases=(select sum(points) from history_stage where histdate>@enddate and ratio='-1'
 and History_stage.Tipnumber = Current_Month_Activity_stage.Tipnumber )
where exists(select * from history_stage where histdate>@enddate and ratio='-1'
 and History_stage.Tipnumber = Current_Month_Activity_stage.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity_stage
set adjustedendingpoints=endingpoints - increases + decreases

GO


