USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyAuditValidation]    Script Date: 01/11/2010 16:22:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spQuarterlyAuditValidation]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spQuarterlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchased numeric(9), @pointsbonus numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturned numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) 

delete from Quarterly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Quarterly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchased, pointsbonus, pointsadded, pointsincreased, pointsredeemed, pointsreturned, pointssubtracted, pointsdecreased 
from Quarterly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend=''0''
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg=''Ending Balances do not match''
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Quarterly_Audit_ErrorFile
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased, @errmsg, @currentend)
		goto Next_Record
		end
		
goto Next_Record

Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchased, @pointsbonus, @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturned, @pointssubtracted, @pointsdecreased

	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr' 
END
GO
