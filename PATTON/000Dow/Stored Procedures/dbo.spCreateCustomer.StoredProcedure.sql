USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[spCreateCustomer]    Script Date: 01/11/2010 16:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spCreateCustomer]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    This will Create the Customer Records for DOW Bank                  */
/* */
/*  - Update CUSTOMER_stage      */
/* BY:  B.QUINN  */
/* DATE: 8/2009   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCreateCustomer] @MonthEndDate DATETIME AS    

/* Test Data */
--declare @MonthEndDate DATETIME  
--set  @MonthEndDate = ''09/30/2009''

Declare @custnum nvarchar(9)
Declare @LASTNAME char(40)


Declare @address3 char(40)
Declare @homephone char(10)
Declare @workphone char(10)

Declare @Zipplus4 char(5)
Declare @STATUS char(1)
Declare @BusFLAG char(1)
Declare @EMPFLAG char(1)
Declare @postdate nvarchar(10)
Declare @ActiveCust nvarchar(1)
Declare @INSTID char(10)
Declare @DateAdded DATETIME
Declare @RunAvailable INT
Declare @RunBalance   INT
Declare @RunRedeemed   INT
Declare @SSLAST4 char(4)
Declare @Social char(9)
Declare @cityState nvarchar(40)
Declare @RunBalanceNew char(8)
Declare @RunAvailableNew char(8)
declare @Address4 nvarchar(40)
Declare @rundate nvarchar(10)
Declare @STATUSDESCRIPTION char(40)
declare	@ACCTID nvarchar (16) 
declare	@ACCTNAME1 varchar (35) 
declare	@ACCTNAME2 varchar (35) 
declare	@FILLER1 varchar (5) 
declare	@E_STATUS varchar (1) 
declare	@I_STATUS varchar (1) 
declare	@FILLER2 varchar (45) 
declare	@OLDCCNUM nvarchar (16) 
declare	@FILLER3 nvarchar (1) 
declare	@TRAVNO nvarchar (10) 
declare	@ADDR1 nvarchar (32) 
declare	@ADDR2 nvarchar (32) 
declare	@CITY varchar (32) 
declare	@STATE varchar (32) 
declare	@ZIP nvarchar (5) 
declare	@HMPHONE nvarchar (10) 
declare	@WKPHONE nvarchar (10) 
declare	@FILLER4 varchar (35) 
declare	@Tipnumber varchar (15)



truncate table Accountdeleteinput

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare Cust_IN_CRSR  cursor 
for Select *
From Dow_Cust_In

Open Cust_IN_CRSR 
/*                  */



Fetch Cust_IN_CRSR  
into 
	@ACCTID,@ACCTNAME1,@ACCTNAME2,@FILLER1,@E_STATUS,@I_STATUS,@FILLER2,@OLDCCNUM,@FILLER3,@TRAVNO,
	@ADDR1,@ADDR2,@CITY,@STATE,@ZIP,@HMPHONE,@WKPHONE,@FILLER4,@Tipnumber 

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


	SET @RunDate = @MonthEndDate
	SET @DateAdded = @MonthEndDate 	
	SET @RunBalanceNew = 0		
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0





/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN


SET @RUNBALANCE = 0		
SET @RunAvailable = 0		
SET @RunRedeemed = 0
set @CityState = (rtrim(@City)+'' ''+LEFT(@State,2)+'' ''+rtrim(@Zip))
set @Address3 = (rtrim(@City)+'' ''+LEFT(@State,2)+'' ''+rtrim(@Zip))
set @Address4 = (rtrim(@City)+'' ''+LEFT(@State,2)+'' ''+rtrim(@Zip))
	
	
	
-- UPDATE INPUT STATUS BASED ON DOW BUSINESS LOGIC
-- IF ACCOUNT IS CLOSED DUE TO THE INPUT STATUS INSERT THE ACCOUNT ID''S INTO THE ACCOUNTDELETEINPUT TABLE

	
if @E_STATUS IN (''B'',''C'',''E'',''F'',''I'',''L'',''N'',''U'',''X'',''Z'')
Begin
set @E_STATUS = ''C''
insert  into accountdeleteinput
(acctid, dda)
values
 ( @acctid,@Tipnumber)
end
	
if @E_STATUS IN ('' '',''N'')
Begin
set @E_STATUS = ''A''
end


	select @STATUSDESCRIPTION = statusDescription
	from status
	where 
	status = @E_STATUS


/*  				- Create CUSTOMER  */

	if @ACCTNAME1 is null
	   set @ACCTNAME1 = '' '' 
	if @ACCTNAME2 is null
	   set @ACCTNAME2 = '' ''

 
--PRINT ''@ACCTNAME1''
--PRINT @ACCTNAME1	
--PRINT ''@TipNumber''
--PRINT @TipNumber			  



 	if exists (select dbo.Customer_Stage.TIPNUMBER from dbo.Customer_Stage 
                    where dbo.Customer_Stage.TIPNUMBER = @TipNumber)	
        Begin	
 		Update Customer_Stage  
		Set 
	        ACCTNAME1 = @ACCTNAME1  
 	       ,ACCTNAME2 = @ACCTNAME2
 	       ,ACCTNAME3 = '' ''
 		   ,lastname = @lastname 
 	       ,ADDRESS1 = @Addr1 
 	       ,ADDRESS2 = @Addr2
 		   ,ADDRESS3 = @Address3
 		   ,ADDRESS4 = @Address4
 	       ,CITY = rtrim(@City)  
  	       ,STATE = LEFT(@State,2)  
 	       ,ZIPCODE = rtrim(@Zip)
 		   ,STATUS = @E_STATUS  
 	           ,HOMEPHONE = @HMPHONE      
		Where @TipNumber = Customer_Stage.TIPNUMBER 
	    END		
 	else
	    Begin
		Insert into CUSTOMER_Stage  
	      (	
		LASTNAME  	
		,ACCTNAME1  	
		,ACCTNAME2 
		,ACCTNAME3	
		,ADDRESS1 
		,ADDRESS2  	
	    ,ADDRESS3
	    ,ADDRESS4     
		,CITY 		
		,STATE		
		,ZIPCODE 
		,HOMEPHONE
	 	,MISC1 		
		,MISC2		
		,MISC3 
		,MISC4
		,MISC5    		
		,DATEADDED
		,RUNAVAILABLE
	 	,RUNBALANCE 
		,RUNREDEEMED
		,TIPNUMBER
		,STATUS
		,TIPFIRST
		,TIPLAST
		,StatusDescription
		,laststmtdate
		,nextstmtdate
		,acctname4
		,acctname5
		,acctname6
		,businessflag
		,employeeflag
		,segmentcode
		,combostmt
		,rewardsonline
		,notes
		,runavaliablenew
	      )		
		values
		 (
		@Lastname, @ACCTNAME1,@ACCTNAME2,'' '',
 		@Addr1, @Addr2,@address3, @address4,	
 		rtrim(@City), rtrim(@State), rtrim(@Zip), @homephone,
		'' '', '' '', '' '', '' '', @social,
--		 MISC1 thru MISC4 Social is put ito Misc5 for use in building Affiliat 
 		@DateAdded, @RunAvailable,
 		@RunBalance, @RunRedeemed,
		@TIPNUMBER, @E_STATUS,
		left(@TipNumber,3), right(@TipNumber,12),
		@STATUSDESCRIPTION,
--		 laststmtdate thru runbalancenew are initialized here to eliminate nuls from the customer table 
		'' '', '' '', '' '', '' '', '' '', @BusFlag, @EMPFLAG, '' '', '' '', '' '', '' '',  ''0''	
	         )

 	        if not exists (select dbo.beginning_balance_table.TIPNUMBER from dbo.beginning_balance_table 
                    where dbo.beginning_balance_table.TIPNUMBER = @TipNumber)
		  begin
		  insert into beginning_balance_table
		  ( tipnumber,
		    Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
		    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 
		  )
		  values
		  (@TIPNUMBER, ''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'',''0'')
		  end

             END  
    	
 	if not exists (select AcctID from Affiliat_Stage 
                    where AcctID = @AcctID)	
	   begin
		Insert into AFFILIAT_Stage
        (
	    ACCTID,
	    TipNumber,
	    LastName,                      
        DateAdded,
        AcctStatus,
	    YTDEarned,
	    CustID,
	    AcctType,
	    AcctTypeDesc)
	    values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@DateAdded,
 		''A'',
		''0'',
 		'' '',
		''Credit'',
		''Credit Card'')
	   end



        

FETCH_NEXT:


	
	Fetch Cust_IN_CRSR   
	into 
	@ACCTID,@ACCTNAME1,@ACCTNAME2,@FILLER1,@E_STATUS,@I_STATUS,@FILLER2,@OLDCCNUM,@FILLER3,@TRAVNO,
	@ADDR1,@ADDR2,@CITY,@STATE,@ZIP,@HMPHONE,@WKPHONE,@FILLER4,@Tipnumber   


END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:
close  Cust_IN_CRSR 
deallocate  Cust_IN_CRSR
' 
END
GO
