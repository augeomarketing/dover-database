USE [000Dow]
GO
/****** Object:  StoredProcedure [dbo].[spPointsPurge_Stage]    Script Date: 01/11/2010 16:22:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spPointsPurge_Stage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spPointsPurge_Stage] @DateInput varchar(10) AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/* Modified 11/2007 BJQ now Points to the Stage Files  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
/*  **************************************  */
--declare @DateInput varchar(10)
--set @DateInput = ''2009-09-30''
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat_stage set AcctStatus = ''A'' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer_stage set Status = ''A'' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat_stage	
	set Acctstatus = ''9''
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat_stage.acctid = AccountDeleteInput.acctid )
	

	Delete from affiliat_stage where acctstatus = ''9''
	
	/*  **************************************  */
	-- Find Customers without accounts and mark for delete 
	update Customer_stage
	set Status=''9''
	where tipnumber not in 
		(select  distinct(tipnumber)  from affiliat_stage )
	or status = ''C''
	

	-- Delete History
	Delete from history_stage where tipnumber in (select tipnumber from customer_stage where status = ''9'')
	
	/*  **************************************  */
	-- Delete Customer
	Delete from customer_stage where status = ''9''
	/* Return 0 */
End
Else
	Print '' Bad date''
	/* Return 99 */
' 
END
GO
