USE [514CitizensBankEffinghamConsumer]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 09/24/2009 09:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__19DFD96B]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1AD3FDA4]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1BC821DD]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1CBC4616]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1DB06A4F]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1EA48E88]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__1F98B2C1]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__208CD6FA]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__2180FB33]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__22751F6C]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__236943A5]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__245D67DE]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF__Monthly_S__Point__25518C17]  DEFAULT (0) FOR [PointsDecreased]
GO
