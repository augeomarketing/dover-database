/******************************************************************************************/
/**                                                                                      **/
/**  Generate the RBPTpostingperiod table                                                    **/
/**                                                                                      **/
/******************************************************************************************/

declare @year			int
declare @month			int
declare @tipfirst		varchar(3)

declare @PPStartDate	datetime
declare @PPEndDate		datetime

set @year = 2003

set @TipFirst = '519'

delete from dbo.RBPTpostingperiod
where sid_tipfirst = @tipfirst

while @year <= 2015
BEGIN

	set @month = 01
	
	while @month <= 12
	BEGIN

		set @PPStartDate = cast(cast(@year as varchar(4)) + '/' +
						cast(@month as varchar(2)) + '/' + '01' as datetime)

		-- calculate last day of month.  Add 1 month to start date, then back off one day to get last day of month
		set @PPEndDate = dateadd(month, 1, @PPStartDate)
		set @PPEndDate = dateadd(day, -1, @PPEndDate)

		-- Now add in 23 hrs, 59 mins and 59 secs
		set @PPEndDate = dateadd(hh, 23, dateadd(mi, 59, dateadd(ss, 59, @PPEndDate)))

		insert into dbo.RBPTpostingperiod
		(sid_tipfirst, dim_RBPTpostingperiod_StartDate, dim_RBPTpostingperiod_EndDate, dim_RBPTpostingperiod_Month, dim_RBPTpostingperiod_Year)
		values(@TipFirst, @PPStartDate, @PPEndDate, @month, @Year)

		set @month = @month + 1
	END

	set @year = @year + 1
END	


select *
from RBPTpostingperiod
where sid_tipfirst = @TipFirst 


