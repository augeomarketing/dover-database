--
-- Define account type (point earnings) groupings

--** VERY IMPORTANT
-- Any new point earning transaction codes MUST be set up BEFORE they go into the FI's history table.
-- Failure to do so means you have to go recalc the RBPT data.  See the Build RBPT Ledger.sql script in SVN


select * from dbo.rbptaccounttype

select * from dbo.rbptaccounttypetrancode
where sid_tipfirst in ('002', '003')
order by sid_tipfirst


--------------------------------------------------------
--
-- Define redemption type groupings

-- ** VERY IMPORTANT #2
-- Any new redemption transaction codes MUST be set up BEFORE they go into the FI's history table.
-- Failure to do so means you have to go recalc the RBPT data.  See the Build RBPT Ledger.sql script in SVN

select *
from dbo.rbptproducttype

select *
from dbo.rbptproducttypetrancode
where dim_rbptproducttypetrancode_tipfirst in ('002', '003')
order by dim_rbptproducttypetrancode_tipfirst

-----------------------------------------------------------
--
-- Define cost per point - on a tipfirst & redemption trancode level

select *
from dbo.rbptcostperpoint
where sid_tipfirst in ('002', '003')
order by sid_tipfirst

-------------------------------------------------------------
