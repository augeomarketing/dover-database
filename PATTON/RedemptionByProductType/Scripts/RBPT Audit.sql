--
-- The 2 queries below will confirm point earnings
select sid_tipnumber, sum(dim_rbptledger_pointsearned) pts
from rewardsnow.dbo.rbptledger
where left(sid_tipnumber,3) = '713'
group by sid_tipnumber
order by sid_tipnumber


select tipnumber, sum(points * ratio) pts
from dbo.history
where trancode not like 'r%'
group by tipnumber
order by tipnumber


--
-- The 2 queries below will confirm the redemptions
select year(histdate) RedeemYear, month(histdate) RedeemMonth, trancode, sum(points) PtsRedeemed
from dbo.history
where trancode like 'r%'
group by  year(histdate), month(histdate), trancode
order by  year(histdate), month(histdate), trancode

select year(dim_rbptreport_dateadded) RedeemYear, month(dim_rbptreport_dateadded) RedeemMonth, sid_trantype_trancode, sum(dim_rbptreport_points) PtsRedeemed
from rewardsnow.dbo.rbptreport
where dim_rbptreport_tipfirst = '713'
group by year(dim_rbptreport_dateadded), month(dim_rbptreport_dateadded), sid_trantype_trancode
order by year(dim_rbptreport_dateadded), month(dim_rbptreport_dateadded), sid_trantype_trancode




