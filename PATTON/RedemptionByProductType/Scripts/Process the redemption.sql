declare @tipnumber					varchar(15)
declare @RedeemPoints				int
declare @enddate					datetime
declare @TransID					uniqueidentifier
declare @TranCode					varchar(2)
declare @histdate					datetime

declare @CostPerPoint				decimal(18,4)


declare @pointszeroedout				int
declare @pointbalance				int
declare @pointstoreduce				int
declare @pointremainder				int
declare @nbrAccountTypes				int
declare @AccountTypePoints			int
declare @AccountTypeCtr				int
declare @AccountTypeId				int
declare @Remainder					int
declare @RemainderAdjustment			int
declare @ctr						int


set @tipnumber = '209000000000002'
set @RedeemPoints = 3000
set @TransId = newid()
set @TranCode = 'Rt'
set @histdate = getdate()

set @CostPerPoint = (select dim_RBPTCostPerPoint_Cost
				 from dbo.RBPTCostPerPoint
				 where sid_TipFirst = left(@tipnumber,3)
				 and sid_trantype_trancode = @trancode
				 and @histdate between dim_RBPTCostPerPoint_EffectiveDate and dim_RBPTCostPerPoint_ExpirationDate)

if @costperpoint is null 
	set @costperpoint = 0.01

--select 'Cost Per Point:', @CostPerPoint

--
-- Find the highest date that will be used in the ledger to handle the redemption
-- Any/all dates prior to this date will have their balances zeroed out
-- 
set @EndDate = (select top 1 dim_RBPTLedger_Histdate
			 from (select dim_RBPTLedger_HistDate, sum(dim_RBPTLedger_PointsRemaining) as pointsremaining,	
						(select sum(dim_rbptledger_pointsremaining) 
						 from dbo.rbptledger l2 
						 where l2.sid_tipnumber = @tipnumber and 
							l2.dim_RBPTLedger_HistDate <= l1.dim_RBPTLedger_HistDate) as runningtotal
				  from dbo.rbptledger l1			
				  where sid_tipnumber = @tipnumber
				   group by dim_RBPTLedger_HistDate) tbl
			 where runningtotal >= @redeempoints
			 order by dim_rbptledger_histdate)
--select 'End Date: ', @enddate
--
-- get # points that will be zeroed out.  This value, subtracted from the redemption
-- will be what gets allocated across the account types for the transactions = @enddate
--
set @pointszeroedout = (select sum(dim_rbptledger_pointsremaining)
				    from dbo.rbptledger
				    where sid_tipnumber = @tipnumber
				    and dim_rbptledger_histdate < @enddate)


--**************
--**
	BEGIN TRAN
--**
--**************

select *
from dbo.rbptledger where sid_tipnumber = @tipnumber order by dim_RBPTLedger_Histdate

--
-- zero out points remaining for tip, for hist dates < @end date
--


-- IMPORTANT!!!  Make sure the RBPTReport table is updated BEFORE points are zeroed out!!!
--
insert into dbo.RBPTReport
(dim_RBPTReport_TipFirst, sid_OnlHistory_TransID, sid_AccountType_Id, 
 dim_RBPTReport_PricePerPoint, dim_RBPTReport_Points, sid_TranType_TranCode)
select max(left(@TipNumber,3)), @TransId, sid_AccountType_Id, max(@costperpoint), sum(dim_RBPTLedger_PointsRemaining), max(@TranCode)
from dbo.RBPTLedger
where sid_TipNumber = @TipNumber
and dim_RBPTLedger_HistDate < @EndDate
Group By sid_AccountType_Id


update dbo.rbptledger
	set dim_rbptledger_pointsremaining = 0
where sid_tipnumber = @tipnumber
and dim_rbptledger_histdate < @enddate

--
-- Create Temp Table
--
drop table #accttypes
create table #AcctTypes (sid int identity(1, 1) primary key, dim_AcctTypes_Priority int, sid_RBPTAccountType_Id int)

create index ix_tmpAcctTypes on #AcctTypes(dim_AcctTypes_Priority, sid_RBPTAccountType_Id)

insert into #AcctTypes (dim_AcctTypes_Priority, sid_RBPTAccountType_Id)
select distinct dim_RBPTPriority_Priority, sid_AccountType_Id
from dbo.RBPTLedger lgr join dbo.RBPTPriority pty
	on left(lgr.sid_tipnumber,3) = pty.sid_tipfirst
	and lgr.sid_AccountType_Id = pty.sid_RBPTAccountType_Id
where lgr.sid_tipnumber = @tipnumber
and lgr.dim_RBPTLedger_HistDate = @EndDate
and lgr.dim_RBPTLedger_PointsRemaining > 0
order by dim_RBPTPriority_Priority 

set @NbrAccountTypes = @@ROWCount
set @AccountTypeCtr = @NbrAccountTypes

--select * from #AcctTypes


set @PointBalance = @RedeemPoints - @PointsZeroedOut

set @Remainder = @PointBalance % @AccountTypeCtr


set @PointsToReduce = @PointBalance / @NbrAccountTypes

--select 'Nbr Acct Types: ', @nbraccounttypes, 'Account Type Ctr: ', @accounttypectr, @PointBalance PointBalance, 
--		@RedeemPoints RedeemPoints, @Pointszeroedout PointsZeroedOut

set @ctr = 1

while @ctr <= @NbrAccountTypes
BEGIN

	set @AccountTypeId = (select sid_RBPTAccountType_ID from #AcctTypes where sid = @ctr)

	-- get points remaining for acct type
	select @AccountTypePoints = dim_RBPTLedger_PointsRemaining
	from dbo.RBPTLedger
	where sid_tipnumber = @TipNumber
	and sid_AccountType_Id = @AccountTypeId
	and dim_RBPTLedger_HistDate = @EndDate

	if @Remainder > 0
	BEGIN
		set @RemainderAdjustment = 1
		set @Remainder = @Remainder - 1
	END
	else
	BEGIN
		set @RemainderAdjustment = 0
	END

	-- back out pointstoreduce update table
	if @PointsToReduce > @AccountTypePoints
	BEGIN

		update dbo.RBPTLedger
			set dim_RBPTLedger_PointsRemaining = @AccountTypePoints
		where sid_tipnumber = @TipNumber
		and sid_AccountType_Id = @AccountTypeId
		and dim_RBPTLedger_HistDate = @EndDate


		insert into dbo.RBPTReport
		(dim_RBPTReport_TipFirst, sid_OnlHistory_TransID, sid_AccountType_Id, 
		 dim_RBPTReport_PricePerPoint, dim_RBPTReport_Points, sid_TranType_TranCode)
	     values( left(@TipNumber,3), @TransID, @AccountTypeId, @CostPerPoint,
				@AccountTypePoints, @TranCode)


		set @PointBalance = @PointBalance - @AccountTypePoints

		set @AccountTypeCtr = case 
							when @AccountTypeCtr - 1 < 1 then 1
							else @AccountTypeCtr - 1
						  end

		set @PointsToReduce = @PointBalance / @AccountTypeCtr

	END

	else
	BEGIN
		update dbo.RBPTLedger
			set dim_RBPTLedger_PointsRemaining = dim_RBPTLedger_PointsRemaining -  (@PointsToReduce + @RemainderAdjustment)
		where sid_tipnumber = @TipNumber
		and sid_AccountType_Id = @AccountTypeId
		and dim_RBPTLedger_HistDate = @EndDate

		insert into dbo.RBPTReport
		(dim_RBPTReport_TipFirst, sid_OnlHistory_TransID, sid_AccountType_Id, 
		 dim_RBPTReport_PricePerPoint, dim_RBPTReport_Points, sid_TranType_TranCode)
	     values( left(@TipNumber,3), @TransID, @AccountTypeId, @CostPerPoint,
				@PointsToReduce + @RemainderAdjustment, @TranCode)

	END

	set @ctr = @ctr + 1
END



select *
from dbo.rbptledger where sid_tipnumber = @tipnumber order by dim_RBPTLedger_Histdate



--**************
--**
	commit  TRAN
--**
--**************
