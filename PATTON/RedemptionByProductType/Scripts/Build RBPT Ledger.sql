use rewardsnow

/*
Steps to run:
1)  Run these 2 deletes.  BE CAREFUL OF THE TIP FIRST!!!!
    delete from rewardsnow.dbo.rbptledger where left(sid_tipnumber,3) = 'xxx'
    delete from rewardsnow.dbo.RBPTReport where dim_rbptreport_tipfirst = 'xxx'

2)  Change the Tip First at the top of this script, and run the script.

3)  Execute the stored proc below.  This processes all redemptions that the RBPTReport table does not know about
    exec dbo.spRBPTNightlyRedemptionProcess 


NOTE:  tips will not be included in this report until they have a redemption


4) Audit

5) Run reports


*/



--
-- First, make sure entries exist for all non-redemption trancodes in the accounttypetrancode table in rewardsnow db
-- 

declare @tip			varchar(3)
declare @dbname		    varchar(50)
declare @sql			nvarchar(4000)
declare @MissingTCodes	int

set @tip = '713'   --<<<< CHANGE THIS TO THE TIPFIRST OF THE FI TO LOAD

set @MissingTCodes = 0

set @dbname = (select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tip)

set @dbname = quotename( @dbname )


-- Put this index on the FI's history table BEFORE running
set @SQL = 'if not exists(select 1 from ' + @dbname + '.dbo.sysindexes where [name] = ''ix_history_trancode_ratio_points_tipnumber_histdate'')
CREATE NONCLUSTERED INDEX [ix_history_trancode_ratio_points_tipnumber_histdate] ON ' + @dbname + '.[dbo].[HISTORY] 
(
	[TRANCODE] ASC,
	[Ratio] ASC,
	[POINTS] ASC,
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
) ON [PRIMARY]'

exec sp_executesql @sql

--
--set @sql = 'select @MissingTCodes = count(*)
--		from ' + @dbname + '.dbo.history his left outer join rewardsnow.dbo.RBPTaccounttypetrancode att
--			on his.trancode = att.trancode
--		where att.trancode is null
--		and his.trancode not like ''r%'''
--
----   fix this - need to add in definition for missingtcodes variable
--exec sp_executesql @sql, N'@MissingTCodes int output', @MissingTCodes = @MissingTCodes output


if @MissingTCodes = 0
BEGIN

	set @sql = 
			'insert into rewardsnow.dbo.RBPTLedger
			(sid_TipNumber, sid_AccountType_Id, dim_RBPTLedger_HistDate, dim_RBPTLedger_PointsEarned, dim_RBPTLedger_PointsRemaining)
			select tipnumber, sid_RBPTAccountType_Id, histdate, sum(points * ratio), sum(points * ratio)
			from ' + @dbname + '.dbo.History his with (nolock) join rewardsnow.dbo.RBPTaccounttypetrancode act with (nolock)
				on his.trancode = act.sid_trantype_Trancode
				and left(his.tipnumber,3) = act.sid_tipfirst
			group by tipnumber, sid_RBPTAccountType_Id, histdate'

	exec sp_executesql @sql
END

else

BEGIN
	set @sql = 'select distinct trancode
		from ' + @dbname + '.dbo.history his left outer join rewardsnow.dbo.RBPTaccounttypetrancode att
			on his.trancode = att.sid_trantype_trancode
		where att.sid_trantype_trancode is null
		and his.trancode not like ''r%'''

	exec sp_executesql @sql

END


