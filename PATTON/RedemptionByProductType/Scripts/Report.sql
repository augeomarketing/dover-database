
set nocount on

declare @TIPFirst			varchar(3)
declare @startdate			datetime
declare @enddate			datetime

declare @SQL				nvarchar(4000)
declare @GUID_Report		uniqueidentifier

declare @nbraccttypes	int
declare @rowctr		int
declare @accttype		int

set @TIPFirst = '209'
set @startdate = '01/01/2008'
set @enddate = '12/31/2008'

set @GUID_Report = newid()


drop table #Report
create table #Report
	(sid_RBPTProductType_id			int primary key,

	 sid_RBPTAccountType_Id_01		int,
	 dim_Report_AccountType01_Points	bigint,
	 dim_Report_AccountType01_Dollars	money,

	 sid_RBPTAccountType_Id_02		int,
	 dim_Report_AccountType02_Points	bigint,
	 dim_Report_AccountType02_Dollars	money,

	 sid_RBPTAccountType_Id_03		int,
	 dim_Report_AccountType03_Points	bigint,
	 dim_Report_AccountType03_Dollars	money,

	 sid_RBPTAccountType_Id_04		int,
	 dim_Report_AccountType04_Points	bigint,
	 dim_Report_AccountType04_Dollars	money,

	 sid_RBPTAccountType_Id_05		int,
	 dim_Report_AccountType05_Points	bigint,
	 dim_Report_AccountType05_Dollars	money,

	 sid_RBPTAccountType_Id_06		int,
	 dim_Report_AccountType06_Points	bigint,
	 dim_Report_AccountType06_Dollars	money,

	 sid_RBPTAccountType_Id_07		int,
	 dim_Report_AccountType07_Points	bigint,
	 dim_Report_AccountType07_Dollars	money,

	 sid_RBPTAccountType_Id_08		int,
	 dim_Report_AccountType08_Points	bigint,
	 dim_Report_AccountType08_Dollars	money,

	 sid_RBPTAccountType_Id_09		int,
	 dim_Report_AccountType09_Points	bigint,
	 dim_Report_AccountType09_Dollars	money,

	 sid_RBPTAccountType_Id_10		int,
	 dim_Report_AccountType10_Points	bigint,
	 dim_Report_AccountType10_Dollars	money)


insert into #report
(sid_RBPTProductType_id)
select distinct rptt.sid_RBPTProductType_Id
from dbo.RBPTReport rep join dbo.RBPTProductTypeTranCode rptt
	on rep.sid_TranType_TranCode = rptt.sid_TranType_TranCode
where rep.dim_RBPTReport_TipFirst = @TipFirst

create table #accttypes
	(sid		int identity(1,1) primary key,
	 sid_accttype int)

insert into #accttypes (sid_accttype)
select distinct sid_AccountType_Id
from dbo.rbptreport
where dim_RBPTReport_TipFirst = @tipfirst
and dim_RBPTReport_DateAdded between @startdate and @enddate

set @nbraccttypes = @@rowcount

set @rowctr = 1

set @accttype = (select sid_accttype from #accttypes where sid = @rowctr)
while @accttype is not null
BEGIN

	set @sql = '
	update #report
		set sid_RBPTAccountType_Id_' + right('00' + @rowctr, 2) + ' = ' + @accttype + ',
			dim_Report_AccountType' + right('00' + @rowctr, 2) + '_Points = sum(points),
			dim_'

-- #Report needs to be permanent table.  use a guid to distinguish unique run
-- change the update to do individual select sum() statements for the points & dollar columns


	print @sql
	exec sp_executesql @sql

	set @rowctr = @rowctr + 1
	set @accttype = (select sid_accttype from #accttypes where sid = @rowctr)
END

-- now select from the report table built above
select *
from #report
-- delete from #report where report_guid = @GUID_Report
