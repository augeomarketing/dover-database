
--
-- Build out the transaction code to RBPT account type mappings for the FI

select distinct trancode
from dbo.history
order by trancode

select *
from rewardsnow.dbo.rbptaccounttype

select *
from rewardsnow.dbo.rbptaccounttypetrancode
where sid_tipfirst in ('219' ,'713')
order by sid_tipfirst


insert into rewardsnow.dbo.RBPTAccountTypeTranCode
(sid_TipFirst, sid_RBPTAccountType_Id, sid_TranType_TranCode)
select '219', 5, trancode
from rewardsnow.dbo.trantype
where trancode not in ('be', 'bi', 'f0', 'f9', 'g0', 'g9')  -- already loaded these by hand
and 
(trancode like 'b%' or trancode like 'f%' or trancode like 'g%')


select *
from rewardsnow.dbo.RBPTAccountTypeTranCode
where sid_tipfirst = '219'

------------------------------------------------------------------------------------------------------------------------
--
-- Put in the cost per point for each redemption transaction code  (default is $0.01 per point)
insert into rewardsnow.dbo.RBPTCostPerPoint
(sid_TipFirst, sid_TranType_TranCode, dim_RBPTCostPerPoint_EffectiveDate, dim_RBPTCostPerPoint_Cost,dim_RBPTCostPerPoint_DateEntered,
 dim_RBPTCostPerPoint_ExpirationDate,dim_RBPTCostPerPoint_DateLastModified)
--select '219', trancode, '01/01/2000', 0.01, '12/31/9999', getdate(), getdate()  --dirish 3/19/2012  this is in the wrong order
 select '219', trancode, '01/01/2000', 0.01,  getdate(),  '12/31/9999',getdate()
from rewardsnow.dbo.trantype
where trancode like 'r%'


------------------------------------------------------------------------------------------------------------------------

--
-- Load up the RBPT Priority Table
-- this is a priority of which account types are burned first.
-- Typically - burn the other and bonus types first, followed by the credit/debit/???




insert into rewardsnow.dbo.RBPTPriority
(sid_TipFirst,sid_RBPTAccountType_Id,dim_RBPTPriority_Priority)
values
('105',5,1)

insert into rewardsnow.dbo.RBPTPriority
(sid_TipFirst,sid_RBPTAccountType_Id,dim_RBPTPriority_Priority)
values
('105',4,2)

insert into rewardsnow.dbo.RBPTPriority
(sid_TipFirst,sid_RBPTAccountType_Id,dim_RBPTPriority_Priority)
Values ('105',2,3)





------------------------------------------------------------------------------------------------------------------------

--
-- Load up the transaction code product type mappings

select * from rewardsnow.dbo.RBPTProductType

select * from rewardsnow.dbo.trantype where trancode like 'r%'



NOTE:  tips will not be included in this report until they have a redemption



