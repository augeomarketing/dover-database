

declare @RedemptionDate		datetime

-- Get current date, then back off one day.  this is to process prior day's redemptions
set @RedemptionDate = dateadd(dd, -1, getdate())

-- Drop off the time portion of the date.
set @RedemptionDate =  convert(datetime,convert(char(10), @RedemptionDate,101))

exec dbo.spRBPTNightlyRedemptionProcess @RedemptionDate