


exec dbo.spRBPTNightlyRedemptionProcess








select sum(dim_RBPTReport_Points)
from rbptreport
where dim_RBPTReport_TipFirst = '003'

select sum(points)
from [asbcorp].dbo.history
where trancode like 'r%'

78,802,627
89,627,117


drop table #rbpt
drop table #hist

create table #rbpt
	(Tipnumber		varchar(15) primary key,
	 rbpt_points		int)

insert into #rbpt
select tipnumber, sum(dim_RBPTReport_Points) RBPT_Points
from rewardsnow.dbo.rbptreport rbpt join fullfillment.dbo.main onl 
	on rbpt.sid_OnlHistory_TransID = onl.transid
where left(tipnumber,3) = '003'
group by tipnumber
order by tipnumber

create table #hist
	(tipnumber	varchar(15) primary key,
	 hist_points	int)

insert into #hist
select tipnumber, sum(points)
from [asbcorp].dbo.history
where trancode like 'R%'
group by tipnumber


select *
from #hist h left outer join #rbpt r
	on h.tipnumber = r.tipnumber
where r.tipnumber is null


select *
from #hist h left outer join #rbpt r
	on h.tipnumber = r.tipnumber
where h.hist_points != r.rbpt_points
and abs(h.hist_points - r.rbpt_points) > 2


select *
from [asbcorp].dbo.history
where tipnumber = '003000000001222'
and trancode like 'r%'

select *
from fullfillment.dbo.main
where tipnumber = '003000000001222'

select sum(points * ratio)
from [asbcorp].dbo.history
where tipnumber = '003000000001222' and trancode like 'r%'

select sum(points * ratio)
from [asbcorp].dbo.history
where tipnumber = '003000000001222' and trancode not like 'r%'

select *
from [asbcorp].dbo.customer
where tipnumber = '003000000001222'



select *
from rbptreport



select sid_onlhistory_transid, sum(dim_rbptreport_points)
from rbptreport
where sid_OnlHistory_TransID in
(
'00336BD2-A49A-4F32-A7F6-8221E6F57957',
'C3B50FB0-321D-4A2E-8856-8FDAB9195538',
'E80D4BF5-6A60-4595-AAA5-4BA4566D487C',
'FBBFB29B-AE29-4784-8AE7-F152B2ACBB56',
'45389996-84F8-44E2-B6BB-2E76C397525D',
'807AD3DE-49CD-4D8F-BB22-DB40AF0AAED2',
'04A56E6F-F61A-47B1-8A1F-9246C9C8F5BD',
'1D4480E3-0D0E-4232-869F-D865300D4ACA',
'6D598A66-19A9-4D02-9C2F-737F16F75829',
'F7181EE0-65CA-475E-BBDF-2DAAF6953759',
'D9BF4034-1C71-4714-8659-EF2F34C149B4',
'FED22E04-91C0-4FFE-B4B0-1881ED2604B4',
'2B0DD320-A273-477B-AD87-ADFC73958E57',
'9F04BAF7-7BE2-4FDD-8E8E-BEAB167C4580',
'2941ADC8-02B6-4F74-8E87-F6F017C552DE',
'8CB6B2AC-9641-4319-BC86-B94F517980FE',
'A7166640-5EFA-44EC-9CD5-6667C1598B0E',
'CC0B8F97-927D-4668-BA24-F9A679557672',
'DF74F6BE-05A3-49B4-A74D-A740C5F0A7AA',
'EF6E0976-23E9-421C-B15D-C6C14800879E',
'0E91911D-A37C-4D8B-8496-E1204832B746',
'8B5AEDB1-AD29-474D-9595-A399ACDCE447',
'AE462664-7642-495B-BAA9-85E06BFB263A',
'BF7E51CC-448F-474A-99F5-B8455B046067'
)
group by sid_onlhistory_transid
order by sid_onlhistory_transid


select *
from fullfillment.dbo.main
where tipnumber = '003000000001043'



exec rewardsnow.dbo.spRBPTProcessRedemption
				'003000000001043',
				'RC',
				'2007-04-17 22:17:00',
				'00336BD2-A49A-4F32-A7F6-8221E6F57957    ',
				40000





truncate table rewardsnow.dbo.rbptreport
update rbptledger
	set dim_RBPTLedger_PointsRemaining = dim_RBPTLedger_PointsEarned
where dim_RBPTLedger_PointsRemaining != dim_RBPTLedger_PointsEarned



select *
from rbptreport
where sid_OnlHistory_TransID = '00336BD2-A49A-4F32-A7F6-8221E6F57957    '


select *
from rbptledger
where sid_tipnumber = '003000000001043'
order by dim_rbptledger_histdate


--------------------------
	exec rewardsnow.dbo.spRBPTProcessRedemption
					@TipNumber,
					@TranCode,
					@HistDate,
					@TransId,
					@Points

declare @guid uniqueidentifier

set @guid = newid()

exec rewardsnow.dbo.spRBPTProcessRedemption 
	'003000000001001', 
	'RM', 
	'2006-12-06 14:05:00.000', 
	@guid, 
	35000

/*
problem #1 - fullfillment & onlinehistory don't go back in time far enough.
--can pull data from history and fudge the guid



*/




select sum(dim_RBPTReport_Points)
from dbo.RBPTReport
where dim_RBPTReport_DateAdded > '12/31/2008'

select sum(points * ratio)
from asbcorp.dbo.history
where histdate > '12/31/2008'
and trancode like 'r%'
