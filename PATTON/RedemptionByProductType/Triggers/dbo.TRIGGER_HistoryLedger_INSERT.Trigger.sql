IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TRIGGER_HistoryLedger_INSERT]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
	DROP TRIGGER [dbo].[TRIGGER_HistoryLedger_INSERT]
GO


CREATE TRIGGER [dbo].[TRIGGER_HistoryLedger_INSERT]
   ON  [dbo].[HISTORY]
   AFTER INSERT
AS 

BEGIN
	-- Should there be an existing row in the RBPT Ledger, update the points earned & points remaining
	update LGR
		set dim_RBPTLedger_PointsEarned = dim_RBPTLedger_PointsEarned + ins.points,
			dim_RBPTLedger_PointsRemaining = dim_RBPTLedger_PointsRemaining + ins.points
	from rewardsnow.dbo.RBPTLedger LGR join (SELECT ins.tipnumber, attc.sid_rbptaccounttype_id, ins.histdate, SUM(points * ratio) Points
									FROM inserted ins 
										INNER JOIN RewardsNOW.dbo.RBPTAccountTypeTranCode attc
											ON ins.trancode = attc.sid_trantype_trancode
											and left(ins.tipnumber,3) = attc.sid_tipfirst  -- added this

									left outer join rewardsnow.dbo.RBPTLedger lgr
										on lgr.sid_tipnumber = ins.tipnumber
										and lgr.sid_accounttype_id = attc.sid_rbptaccounttype_id
										and lgr.dim_RBPTLedger_histdate = ins.histdate
									where trancode not like 'R%' and trancode not in ('XP') and lgr.sid_tipnumber is null
									GROUP BY ins.tipnumber, attc.sid_rbptaccounttype_id, ins.histdate) ins
		on lgr.sid_tipnumber = ins.tipnumber
		and lgr.sid_accounttype_id = ins.sid_rbptaccounttype_id
		and lgr.dim_RBPTLedger_HistDate = ins.histdate


	-- Insert into ledger if rows don't already exist in the ledger (see left outer join).  It is possible to have >1 
	-- transaction for the same tip, account type, and date on the same day.  for example multiple DE or IE
	INSERT INTO RewardsNOW.dbo.RBPTLedger 
		(sid_tipnumber, sid_accounttype_id, dim_RBPTLedger_histdate, dim_RBPTLedger_PointsEarned, dim_RBPTLedger_PointsRemaining)
	SELECT ins.tipnumber, attc.sid_rbptaccounttype_id, ins.histdate, SUM(points * ratio), sum(points * ratio)
		FROM inserted ins 
			INNER JOIN RewardsNOW.dbo.RBPTAccountTypeTranCode attc
				ON ins.trancode = attc.sid_trantype_trancode
				and left(ins.tipnumber,3) = attc.sid_tipfirst -- added this

		left outer join rewardsnow.dbo.RBPTLedger lgr
			on lgr.sid_tipnumber = ins.tipnumber
			and lgr.sid_accounttype_id = attc.sid_rbptaccounttype_id
			and lgr.dim_RBPTLedger_histdate = ins.histdate
		where trancode not like 'R%' and trancode not in ('XP') and lgr.sid_tipnumber is null
		GROUP BY ins.tipnumber, attc.sid_rbptaccounttype_id, ins.histdate

	
END
