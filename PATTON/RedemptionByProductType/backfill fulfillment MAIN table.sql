
begin tran

insert into dbo.main
(TipNumber, TipFirst, Source, Name1, Name2, TransID, HistDate, TranCode, TranDesc, Points, CatalogQty,
 sName, RedStatus, RedReqFulDate, InvoiceDate, Notes, cashvalue)
select h.tipnumber, left(h.tipnumber,3), 'RBPT_ONLY', acctname1, acctname2, newid(), histdate, trancode, description, points, 1 catalogqty,
		acctname1 sname, 5 redstatus, histdate redreqfuldate, histdate invoicedate, 'Txn for backfilling RBPT.  DO NOT USE FOR ACCT RESEARCH!' notes, 0 cashvalue
from [asbcorp].dbo.history h join [asbcorp].dbo.customer c
	on h.tipnumber = c.tipnumber
where histdate < '01/01/2007'
and trancode like 'r%'

commit tran


