use rewardsnow
GO

if exists(select 1 from dbo.sysobjects where name = 'spRBPTGenerateReport' and xtype = 'P')
	drop procedure dbo.spRBPTGenerateReport
go

create procedure dbo.spRBPTGenerateReport
	@TipFirst			nvarchar(3),
	@StartDate		datetime,
	@EndDate			DateTime

AS


set nocount on

--declare @TIPFirst			nvarchar(3)
--declare @startdate			datetime
--declare @enddate			datetime

declare @SQL				nvarchar(4000)
declare @GUID_Report		uniqueidentifier

declare @nbraccttypes	int
declare @rowctr		int
declare @accttype		int
declare @counter		int

--set @TIPFirst = '209'
--set @startdate = '01/01/2008'
--set @enddate = '12/31/2008'

--
-- Generate report run time GUID - so each report execution doesn't step on each other
--
set @GUID_Report = newid()

set @enddate = dateadd(dd, 1, @enddate)
set @enddate = dateadd(ms, -3, @enddate)

-- drop table dbo.wrkRBPTReport
-- If work table does not exist, create it
if not exists(select 1 from dbo.sysobjects where name = 'wrkRBPTReport' and xtype = 'U')
	create table dbo.wrkRBPTReport
		(sid_wrkRBPTReport_id			int identity(1,1) primary key,
		 sid_wrkRBPTReport_RunTimeGUID	uniqueidentifier,

		 dim_RBPTReport_TipFirst			varchar(3),
		 sid_RBPTProductType_id			int,

		 sid_RBPTAccountType_Id01		int,
		 dim_Report_AccountType01_Points	bigint,
		 dim_Report_AccountType01_Dollars	money,

		 sid_RBPTAccountType_Id02		int,
		 dim_Report_AccountType02_Points	bigint,
		 dim_Report_AccountType02_Dollars	money,

		 sid_RBPTAccountType_Id03		int,
		 dim_Report_AccountType03_Points	bigint,
		 dim_Report_AccountType03_Dollars	money,

		 sid_RBPTAccountType_Id04		int,
		 dim_Report_AccountType04_Points	bigint,
		 dim_Report_AccountType04_Dollars	money,

		 sid_RBPTAccountType_Id05		int,
		 dim_Report_AccountType05_Points	bigint,
		 dim_Report_AccountType05_Dollars	money,

		 sid_RBPTAccountType_Id06		int,
		 dim_Report_AccountType06_Points	bigint,
		 dim_Report_AccountType06_Dollars	money,

		 sid_RBPTAccountType_Id07		int,
		 dim_Report_AccountType07_Points	bigint,
		 dim_Report_AccountType07_Dollars	money,

		 sid_RBPTAccountType_Id08		int,
		 dim_Report_AccountType08_Points	bigint,
		 dim_Report_AccountType08_Dollars	money,

		 sid_RBPTAccountType_Id09		int,
		 dim_Report_AccountType09_Points	bigint,
		 dim_Report_AccountType09_Dollars	money,

		 sid_RBPTAccountType_Id10		int,
		 dim_Report_AccountType10_Points	bigint,
		 dim_Report_AccountType10_Dollars	money)

--
-- add in rows by product type
--
insert into dbo.wrkRBPTReport
(sid_RBPTProductType_id, sid_wrkRBPTReport_RunTimeGUID, dim_RBPTReport_TipFirst)
select distinct rptt.sid_RBPTProductType_Id, @GUID_Report, @TIPFirst
from dbo.RBPTReport rep join dbo.RBPTProductTypeTranCode rptt
	on rep.sid_TranType_TranCode = rptt.sid_TranType_TranCode
where rep.dim_RBPTReport_TipFirst = @TipFirst

--
-- create temp table of unique account types for this customer for this date range.
--drop table #accttypes
--
create table #accttypes
	(sid		int identity(1,1) primary key,
	 sid_accttype int)


--
-- populate temp table with the account types used by the FI within the date range specified
--
insert into #accttypes (sid_accttype)
select distinct sid_AccountType_Id
from dbo.rbptreport
where dim_RBPTReport_TipFirst = @tipfirst
and dim_RBPTReport_DateAdded between @startdate and @enddate

--
-- Get # rows inserted
--
set @nbraccttypes = @@rowcount

--
-- Loop for each account type used, updating totals in wrkRBPTReport table
--
set @rowctr = 1

set @accttype = (select sid_accttype from #accttypes where sid = @rowctr)

while @accttype is not null
BEGIN

	set @SQL = '
	update dbo.wrkRBPTReport
		set sid_RBPTAccountType_Id' + right('00' + cast(@rowctr as varchar(2)), 2) + ' = ' + cast(@accttype as varchar(15)) + ',
			dim_Report_AccountType' + right('00' + cast(@rowctr as varchar(2)), 2) + '_Points = isnull( (select sum(dim_RBPTReport_Points) from dbo.RBPTReport  rpt join dbo.RBPTProductTypeTranCode rtc
																							on rpt.sid_trantype_trancode = rtc.sid_TranType_TranCode
																							and wrkRbptReport.sid_rbptProductType_id = rtc.sid_RBPTProductType_Id
																							and ' + char(39) + @TipFirst + char(39) + '  = rtc.dim_RBPTProductTypeTranCode_TipFirst
																								where dim_RBPTReport_TipFirst = ' + char(39) + @TipFirst + char(39) + ' and
																								 sid_AccountType_ID = ' + cast(@accttype as varchar(15)) + ' and
																								 dim_RBPTReport_DateAdded between @StartDate and @EndDate), 0), 

			dim_Report_AccountType' + right('00' + cast(@rowctr as varchar(2)), 2) + '_Dollars = isnull((select sum(dim_RBPTReport_Points * dim_RBPTReport_PricePerPoint) from dbo.RBPTReport  rpt join dbo.RBPTProductTypeTranCode rtc
																							on rpt.sid_trantype_trancode = rtc.sid_TranType_TranCode
																							and wrkRbptReport.sid_rbptProductType_id = rtc.sid_RBPTProductType_Id
																							and ' + char(39) + @TipFirst + char(39) + '  = rtc.dim_RBPTProductTypeTranCode_TipFirst
																					where dim_RBPTReport_TipFirst = ' + char(39) + @TipFirst + char(39) + ' and
																					 sid_AccountType_ID = ' + cast(@accttype as varchar(15)) + ' and
																					 dim_RBPTReport_DateAdded between @StartDate and @EndDate), 0)
	where dim_RBPTReport_TipFirst = ' + char(39) + @TIPFirst + char(39) + ' and 
	sid_wrkRBPTReport_RunTimeGUID = ' + char(39) + cast(@GUID_Report as varchar(50)) + char(39)
 
	print @sql
	exec sp_executesql @sql, N'@StartDate datetime, @EndDate datetime', @StartDate = @StartDate, @EndDate = @EndDate

	set @rowctr = @rowctr + 1
	set @accttype = (select sid_accttype from #accttypes where sid = @rowctr)
END

--
-- Add a total column to the next unused column in the table
--
set @counter = 1
set @SQL = '
	update dbo.wrkRBPTReport
		set sid_RBPTAccountType_ID' + right('00' + cast(@rowctr as varchar(2)), 2) + ' = isnull( (select sid_RBPTAccountType_Id
																			from dbo.RBPTAccountType
																			where dim_RBPTAccountType_Name = ''Report Totals''), 0), 
			dim_Report_AccountType' + right('00' + cast(@rowctr as varchar(2)), 2) + '_Points = '
while @counter < @rowctr
BEGIN
	if @counter = 1
		set @SQL = @SQL + 'dim_Report_AccountType' + right('00' + cast(@counter as varchar(2)), 2) + '_Points'
	else
		set @SQL = @SQL + ' + dim_Report_AccountType' + right('00' + cast(@counter as varchar(2)), 2) + '_Points'

	set @counter = @counter + 1
END

set @SQL = @SQL + ',
				dim_Report_AccountType'  + right('00' + cast(@rowctr as varchar(2)), 2) + '_Dollars = '
set @counter = 1
while @counter < @rowctr
BEGIN
	if @counter = 1
		set @SQL = @SQL + 'dim_Report_AccountType' + right('00' + cast(@counter as varchar(2)), 2) + '_Dollars'
	else
		set @SQL = @SQL + ' + dim_Report_AccountType' + right('00' + cast(@counter as varchar(2)), 2) + '_Dollars'

	set @counter = @counter + 1
END


print @sql
exec sp_executesql @SQL

--
-- now select from the report table built above
--
select sid_wrkRBPTReport_RunTimeGUID, wrk.sid_RBPTProductType_id, pt.dim_RBPTProductType_name, 
		sid_RBPTAccountType_Id01, at01.dim_RBPTAccountType_Name AccountTypeName01, dim_Report_AccountType01_Points, dim_Report_AccountType01_Dollars,
		sid_RBPTAccountType_Id02, at02.dim_RBPTAccountType_Name AccountTypeName02, dim_Report_AccountType02_Points, dim_Report_AccountType02_Dollars,
		sid_RBPTAccountType_Id03, at03.dim_RBPTAccountType_Name AccountTypeName03, dim_Report_AccountType03_Points, dim_Report_AccountType03_Dollars,
		sid_RBPTAccountType_Id04, at04.dim_RBPTAccountType_Name AccountTypeName04, dim_Report_AccountType04_Points, dim_Report_AccountType04_Dollars,
		sid_RBPTAccountType_Id05, at05.dim_RBPTAccountType_Name AccountTypeName05, dim_Report_AccountType05_Points, dim_Report_AccountType05_Dollars,
		sid_RBPTAccountType_Id06, at06.dim_RBPTAccountType_Name AccountTypeName06, dim_Report_AccountType06_Points, dim_Report_AccountType06_Dollars,
		sid_RBPTAccountType_Id07, at07.dim_RBPTAccountType_Name AccountTypeName07, dim_Report_AccountType07_Points, dim_Report_AccountType07_Dollars,
		sid_RBPTAccountType_Id08, at08.dim_RBPTAccountType_Name AccountTypeName08, dim_Report_AccountType08_Points, dim_Report_AccountType08_Dollars,
		sid_RBPTAccountType_Id09, at09.dim_RBPTAccountType_Name AccountTypeName09, dim_Report_AccountType09_Points, dim_Report_AccountType09_Dollars,
		sid_RBPTAccountType_Id10, at10.dim_RBPTAccountType_Name AccountTypeName10, dim_Report_AccountType10_Points, dim_Report_AccountType10_Dollars

from dbo.wrkRBPTReport wrk join dbo.RBPTProductType pt
	on wrk.sid_RBPTProductType_id = pt.sid_RBPTProductType_id

left outer join dbo.RBPTAccountType at01
	on wrk.sid_RBPTAccountType_Id01 = at01.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at02
	on wrk.sid_RBPTAccountType_Id02 = at02.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at03
	on wrk.sid_RBPTAccountType_Id03 = at03.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at04
	on wrk.sid_RBPTAccountType_Id04 = at04.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at05
	on wrk.sid_RBPTAccountType_Id05 = at05.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at06
	on wrk.sid_RBPTAccountType_Id06 = at06.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at07
	on wrk.sid_RBPTAccountType_Id07 = at07.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at08
	on wrk.sid_RBPTAccountType_Id08 = at08.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at09
	on wrk.sid_RBPTAccountType_Id09 = at09.sid_RBPTAccountType_Id

left outer join dbo.RBPTAccountType at10
	on wrk.sid_RBPTAccountType_Id10 = at10.sid_RBPTAccountType_Id
where sid_wrkRBPTReport_RunTimeGUID = @GUID_Report

--delete from dbo.wrkRBPTReport where sid_wrkRBPTReport_RunTimeGUID = @GUID_Report



/*

exec   dbo.spRBPTGenerateReport
	@TipFirst			= '209',
	@StartDate		= '01/01/2008',
	@EndDate			= '12/31/2008'


*/