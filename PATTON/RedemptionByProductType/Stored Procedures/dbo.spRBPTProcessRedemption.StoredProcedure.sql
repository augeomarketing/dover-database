use rewardsnow
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[spRBPTProcessRedemption]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[spRBPTProcessRedemption]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[spRBPTProcessRedemption]
	@TipNumber			varchar(15),
	@TranCode			varchar(2),	
	@HistDate			datetime,
	@TransId			uniqueidentifier,	
	@RedeemPoints		int

as


declare @enddate					datetime
declare @CostPerPoint				decimal(18,4)
declare @pointszeroedout			int
declare @pointbalance				int
declare @pointstoreduce				int
declare @pointremainder				int
declare @nbrAccountTypes			int
declare @AccountTypePoints			int
declare @AccountTypeCtr				int
declare @AccountTypeId				int
declare @Remainder					int
declare @RemainderAdjustment		int
declare @ctr						int
declare @leftover					int

				

--set @tipnumber = '002000000063456'
--set @RedeemPoints =   166  --54298
--set @TransId = '0BF12068-ADD7-465D-B016-5246B2EE281E'
--set @TranCode = 'RC'
--set @histdate = '2007-09-15 14:00:00'

--update dbo.rbptledger
--	set dim_RBPTLedger_PointsRemaining = dim_RBPTLedger_PointsEarned
--where sid_tipnumber = @tipnumber


--select * from rbptledger	 where sid_tipnumber = @tipnumber order by dim_rbptledger_histdate


set @CostPerPoint = isnull((select dim_RBPTCostPerPoint_Cost
				 from dbo.RBPTCostPerPoint
				 where sid_TipFirst = left(@tipnumber,3)
				 and sid_trantype_trancode = @trancode
				 and @histdate between dim_RBPTCostPerPoint_EffectiveDate and dim_RBPTCostPerPoint_ExpirationDate), 0.00)

--select @CostPerPoint CostPerPoint

if @costperpoint is null 
	set @costperpoint = 0.01
--select 'Cost Per Point:', @CostPerPoint
--
-- Find the highest date that will be used in the ledger to handle the redemption
-- Any/all dates prior to this date will have their balances zeroed out
-- 
set @EndDate = (select top 1 dim_RBPTLedger_Histdate
			 from (select dim_RBPTLedger_HistDate, sum(dim_RBPTLedger_PointsRemaining) as pointsremaining,	
						(select sum(dim_rbptledger_pointsremaining) 
						 from dbo.rbptledger l2 
						 where l2.sid_tipnumber = @tipnumber and 
							l2.dim_RBPTLedger_HistDate <= l1.dim_RBPTLedger_HistDate) as runningtotal
				  from dbo.rbptledger l1			
				  where sid_tipnumber = @tipnumber
				   group by dim_RBPTLedger_HistDate) tbl
			 where runningtotal >= @redeempoints
			 order by dim_rbptledger_histdate)

--select 'End Date: ', @enddate

--
-- get # points that will be zeroed out.  This value, subtracted from the redemption
-- will be what gets allocated across the account types for the transactions = @enddate
--
set @pointszeroedout = isnull((select sum(dim_rbptledger_pointsremaining)
				    from dbo.rbptledger
				    where sid_tipnumber = @tipnumber
				    and dim_rbptledger_histdate < @enddate), 0)
--select @PointsZeroedOut PointsZeroedOut

--**************
--**
	BEGIN TRAN
--**
--**************

--
-- zero out points remaining for tip, for hist dates < @end date
--
-- IMPORTANT!!!  Make sure the RBPTReport table is updated BEFORE points are zeroed out!!!
--

insert into dbo.RBPTReport
(dim_RBPTReport_TipFirst, sid_OnlHistory_TransID, sid_AccountType_Id, dim_RBPTReport_PricePerPoint, dim_RBPTReport_Points, sid_TranType_TranCode, dim_rbptreport_dateadded)
select max(left(@TipNumber,3)), @TransId, sid_AccountType_Id, max(@costperpoint), sum(dim_RBPTLedger_PointsRemaining), max(@TranCode), max(@histdate)
from dbo.RBPTLedger
where sid_TipNumber = @TipNumber
and dim_RBPTLedger_HistDate < @EndDate
GROUP BY sid_AccountType_Id


update dbo.rbptledger
	set dim_rbptledger_pointsremaining = 0
where sid_tipnumber = @tipnumber
and dim_rbptledger_histdate < @enddate

--
-- Create Temp Table
--
create table #AcctTypes (sid int identity(1, 1) primary key, dim_AcctTypes_Priority int, sid_RBPTAccountType_Id int, dim_RBPTLedger_PointsRemaining int)

create index ix_tmpAcctTypes on #AcctTypes(dim_RBPTLedger_PointsRemaining, dim_AcctTypes_Priority, sid_RBPTAccountType_Id)

insert into #AcctTypes (dim_AcctTypes_Priority, sid_RBPTAccountType_Id, dim_RBPTLedger_PointsRemaining)
select dim_RBPTPriority_Priority, sid_AccountType_Id, SUM(lgr.dim_RBPTLedger_PointsRemaining)
from dbo.RBPTLedger lgr join dbo.RBPTPriority pty
	on left(lgr.sid_tipnumber,3) = pty.sid_tipfirst
	and lgr.sid_AccountType_Id = pty.sid_RBPTAccountType_Id
where lgr.sid_tipnumber = @tipnumber
and lgr.dim_RBPTLedger_HistDate = @EndDate
and lgr.dim_RBPTLedger_PointsRemaining > 0
group by dim_RBPTPriority_Priority, sid_AccountType_Id
order by SUM(lgr.dim_RBPTLedger_PointsRemaining), dim_RBPTPriority_Priority

-- 
-- Changing order to give smallest to largest to be sure the points can be alocatted appropriately.  
-- ie: if acct type A doesn't have enough points to cover 1/2 of redemption, the leftover needs to apply to
--			acct type B

set @NbrAccountTypes = @@ROWCount
set @AccountTypeCtr = @NbrAccountTypes

--
-- if the # account types = 0, then this tip first has not been configured correctly.
-- to continue would cause errors and/or unpredictable results - so just rollback, and kick out.
if @NbrAccountTypes = 0
	goto ERROR



set @PointBalance = @RedeemPoints - @PointsZeroedOut

set @Remainder = @PointBalance % @AccountTypeCtr
set @PointsToReduce = @PointBalance / @NbrAccountTypes

--select 'Nbr Acct Types: ', @nbraccounttypes, 'Account Type Ctr: ', @accounttypectr, @PointBalance PointBalance, 
--		@RedeemPoints RedeemPoints, @Pointszeroedout PointsZeroedOut



set @ctr = 1
set @leftover = 0

while @ctr <= @NbrAccountTypes
BEGIN

	set @AccountTypeId = (select sid_RBPTAccountType_ID from #AcctTypes where sid = @ctr)

	-- get points remaining for acct type
	select @AccountTypePoints = dim_RBPTLedger_PointsRemaining
	from dbo.RBPTLedger
	where sid_tipnumber = @TipNumber
	and sid_AccountType_Id = @AccountTypeId
	and dim_RBPTLedger_HistDate = @EndDate

--select @ctr COUNTER, @AccountTypePoints AccountTypePoints, @LeftOver LeftOver, @remainder Remainder

	-- since ordered smallest to biggest, if the redeemable points is more than the points earned, the carry over
	-- needs to be applied to the points (and new remainders determined).
	-- Assumes all redemptions came from people1 with greater than or equal to the points for redemption

	if @leftover <> 0
	begin
		--set @AccountTypePoints = @AccountTypePoints - @leftover

		set @AccountTypeCtr = case 
						when (@NbrAccountTypes - @ctr) < 1 then 1
						else (@NbrAccountTypes - @ctr)
					  end

		set @Remainder = @LeftOver % @AccountTypeCtr
		set @PointsToReduce = @LeftOver / @AccountTypeCtr
	end

	if @Remainder > 0
	BEGIN
		set @RemainderAdjustment = 1
		set @Remainder = @Remainder - 1
	END
	else
	BEGIN
		set @RemainderAdjustment = 0
	END

--select @pointstoreduce Points2Reduce, @AccountTypePoints AcctTypePoints


	-- back out pointstoreduce update table
--	if @PointsToReduce > @AccountTypePoints  4/14/09
	if @PointsToReduce > @AccountTypePoints
	BEGIN

		update dbo.RBPTLedger
			set dim_RBPTLedger_PointsRemaining = 0
			where sid_tipnumber = @TipNumber
				and sid_AccountType_Id = @AccountTypeId
				and dim_RBPTLedger_HistDate = @EndDate


		insert into dbo.RBPTReport
		(dim_RBPTReport_TipFirst, sid_OnlHistory_TransID, sid_AccountType_Id, 
		 dim_RBPTReport_PricePerPoint, dim_RBPTReport_Points, sid_TranType_TranCode, dim_rbptReport_dateAdded)
	     values( left(@TipNumber,3), @TransID, @AccountTypeId, @CostPerPoint,
				@AccountTypePoints, @TranCode, @histdate)  

		set @leftover = (@PointBalance - @AccountTypePoints) --+ @RemainderAdjustment
--select @LeftOver as SloppySeconds, @PointBalance PointBalance

--		set @PointBalance = @PointBalance - @leftover
		set @PointBalance =  @leftover
--select @pointbalance as PtBal, @AccountTypePoints AccountTypePoints

--		set @AccountTypeCtr = case 
--							when @AccountTypeCtr - 1 < 1 then 1
--							else @AccountTypeCtr - 1
--						  end
--		set @PointsToReduce = @PointBalance / @AccountTypeCtr
	END

	else
	BEGIN

--select @pointbalance as PtBal_Round2, @PointstoReduce Round2_Pts2Reduce, @RemainderAdjustment RemainderAdj

		update dbo.RBPTLedger
			set dim_RBPTLedger_PointsRemaining = dim_RBPTLedger_PointsRemaining - (@PointsToReduce + @RemainderAdjustment) -- @PointBalance + @RemainderAdjustment
		where sid_tipnumber = @TipNumber
		and sid_AccountType_Id = @AccountTypeId
		and dim_RBPTLedger_HistDate = @EndDate


		insert into dbo.RBPTReport
		(dim_RBPTReport_TipFirst, sid_OnlHistory_TransID, sid_AccountType_Id, 
		 dim_RBPTReport_PricePerPoint, dim_RBPTReport_Points, sid_TranType_TranCode, dim_rbptreport_dateadded)
	     values( left(@TipNumber,3), @TransID, @AccountTypeId, @CostPerPoint,
				--@PointsToReduce + @RemainderAdjustment, @TranCode)
				  @PointstoReduce + @RemainderAdjustment, @TranCode, @histdate)		

		set @leftover = 0
	END

	set @ctr = @ctr + 1
END


/*
select top 10 * from rbptreport  
where dim_rbptreport_tipfirst = '360'  
and sid_onlhistory_transid = 'F5F6F2F1-9834-40AB-A48F-478A12691760'
  
select * from rbptledger where sid_tipnumber = '360000000214332'  
order by dim_rbptledger_histdate  
  
select * from fullfillment.dbo.main where transid = 'F5F6F2F1-9834-40AB-A48F-478A12691760'

*/

/*

select *
from dbo.rbptreport where dim_rbptreport_tipfirst = left(@Tipnumber,3)
and sid_onlhistory_transid = @transid

--select sum(dim_rbptledger_pointsremaining)
--from dbo.rbptledger
--where sid_tipnumber = '002000000010319'
--and dim_rbptledger_histdate < '2006-06-30'  -- 54798





select *
from rbptledger where sid_tipnumber = @tipnumber
order by dim_rbptledger_histdate



*/



--rollback tran
COMMIT TRAN

goto OK_To_End

ERROR:
rollback tran

OK_To_End:

