use REWARDSNOW
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTPriority_RBPTPriority]') AND type = 'F')
ALTER TABLE [dbo].[RBPTPriority] DROP CONSTRAINT [FK_RBPTPriority_RBPTPriority]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTPriority_TranType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTPriority] DROP CONSTRAINT [FK_RBPTPriority_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTPriority]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RBPTPriority]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[RBPTPriority](
	[sid_TipFirst] [varchar](3) NOT NULL,
	[sid_RBPTAccountType_Id] [int] NOT NULL,
	[dim_RBPTPriority_Priority] [int] NOT NULL,
	[dim_RBPTPriority_DateAdded] [datetime] NOT NULL CONSTRAINT [DF_RBPTPriority_dim_RBPTPriority_DateAdded]  DEFAULT (getdate()),
	[dim_RBPTPriority_DateLastModified] [datetime] NOT NULL CONSTRAINT [DF_RBPTPriority_dim_RBPTPriority_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTPriority_1] PRIMARY KEY CLUSTERED 
(
	[sid_TipFirst] ASC,
	[sid_RBPTAccountType_Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_RBPTPriority_UPDATE] ON [dbo].[RBPTPriority] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTPriority_DateLastModified = getdate()
	FROM dbo.RBPTPriority c JOIN deleted del
		ON c.sid_tipfirst = del.sid_tipfirst
			AND c.sid_RBPTAccountType_Id = del.sid_RBPTAccountType_Id
 END



GO
ALTER TABLE [dbo].[RBPTPriority]  WITH CHECK ADD  CONSTRAINT [FK_RBPTPriority_RBPTAccountType] FOREIGN KEY([sid_RBPTAccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTPriority] CHECK CONSTRAINT [FK_RBPTPriority_RBPTAccountType]