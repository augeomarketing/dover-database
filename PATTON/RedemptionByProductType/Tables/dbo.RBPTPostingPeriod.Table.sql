use rewardsnow
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTPostingPeriod]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[RBPTPostingPeriod]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[RBPTPostingPeriod](
	[sid_RBPTPostingPeriod_Id] [int] IDENTITY(1,1) NOT NULL,
	[sid_TipFirst] [varchar](3) NOT NULL,
	[dim_RBPTPostingPeriod_StartDate] [datetime] NOT NULL,
	[dim_RBPTPostingPeriod_EndDate] [datetime] NOT NULL,
	[dim_RBPTPostingPeriod_Month] [int] NOT NULL,
	[dim_RBPTPostingPeriod_Year] [int] NOT NULL,
	[dim_RBPTPostingPeriod_DateEntered] [datetime] NOT NULL CONSTRAINT [DF_PostingPeriod_DateEntered]  DEFAULT (getdate()),
	[dim_RBPTPostingPeriod_DateLastModified] [datetime] NULL CONSTRAINT [DF_RBPTPostingPeriod_dim_RBPTPostingPeriod_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTPostingPeriod] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTPostingPeriod_Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF