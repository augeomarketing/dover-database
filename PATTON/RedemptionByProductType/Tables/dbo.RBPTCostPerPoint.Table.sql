IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTCostPerPoint_TranType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTCostPerPoint] DROP CONSTRAINT [FK_RBPTCostPerPoint_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTCostPerPoint]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RBPTCostPerPoint]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTCostPerPoint](
	[sid_TipFirst] [varchar](3) NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTCostPerPoint_EffectiveDate] [datetime] NOT NULL,
	[dim_RBPTCostPerPoint_Cost] [decimal](18, 4) NOT NULL,
	[dim_RBPTCostPerPoint_ExpirationDate] [datetime] NOT NULL CONSTRAINT [DF_CostPerPoint_CostPerPointExpirationDate]  DEFAULT ('12/31/9999'),
	[dim_RBPTCostPerPoint_DateEntered] [datetime] NOT NULL,
	[dim_RBPTCostPerPoint_DateLastModified] [datetime] NOT NULL,
 CONSTRAINT [PK_RBPTCostPerPoint] PRIMARY KEY CLUSTERED 
(
	[sid_TipFirst] ASC,
	[sid_TranType_TranCode] ASC,
	[dim_RBPTCostPerPoint_EffectiveDate] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[tru_RBPTCostPerPoint]
   ON  [dbo].[RBPTCostPerPoint]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update cpp
		set dim_RBPTCostPerPoint_DateLastModified = getdate()
	from inserted ins join dbo.RBPTcostperpoint cpp
		on ins.sid_tipfirst = cpp.sid_tipfirst
		and ins.sid_TranType_trancode = cpp.sid_TranType_trancode
		and ins.dim_RBPTcostperpoint_effectivedate = cpp.dim_RBPTcostperpoint_effectivedate
END

GO
ALTER TABLE [dbo].[RBPTCostPerPoint]  WITH CHECK ADD  CONSTRAINT [FK_RBPTCostPerPoint_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTCostPerPoint] CHECK CONSTRAINT [FK_RBPTCostPerPoint_TranType]


