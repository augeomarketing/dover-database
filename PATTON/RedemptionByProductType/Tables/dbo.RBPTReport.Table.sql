use RewardsNOW
Go

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTReport_AccountType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTReport] DROP CONSTRAINT [FK_RBPTReport_AccountType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTReport_TranType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTReport] DROP CONSTRAINT [FK_RBPTReport_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTReport]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RBPTReport]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[RBPTReport](
	[sid_RBPTReport_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_RBPTReport_TipFirst] [varchar](3) NOT NULL,
	[sid_OnlHistory_TransID] [uniqueidentifier] NOT NULL,
	[sid_AccountType_Id] [int] NOT NULL,
	[dim_RBPTReport_PricePerPoint] [money] NOT NULL,
	[dim_RBPTReport_Points] [int] NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTReport_DateAdded] [datetime] NOT NULL CONSTRAINT [DF_RBPTReport_dim_RBPTReport_DateAdded]  DEFAULT (getdate()),
	[dim_RBPTReport_DateLastModified] [datetime] NOT NULL CONSTRAINT [DF_RBPTReport_dim_RBPTReport_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTReport] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTReport_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

CREATE NONCLUSTERED INDEX [IX_RBPTReport_TipFirst_PricePerPoint_Points_Trancode_AccountTypeId_TransId] ON [dbo].[RBPTReport] 
(
	[dim_RBPTReport_TipFirst] ASC,
	[dim_RBPTReport_PricePerPoint] ASC,
	[dim_RBPTReport_Points] ASC,
	[sid_TranType_TranCode] ASC,
	[sid_AccountType_Id] ASC,
	[sid_OnlHistory_TransID] ASC
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RBPTReport]  WITH NOCHECK ADD  CONSTRAINT [FK_RBPTReport_AccountType] FOREIGN KEY([sid_AccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTReport] CHECK CONSTRAINT [FK_RBPTReport_AccountType]
GO
ALTER TABLE [dbo].[RBPTReport]  WITH NOCHECK ADD  CONSTRAINT [FK_RBPTReport_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTReport] CHECK CONSTRAINT [FK_RBPTReport_TranType]