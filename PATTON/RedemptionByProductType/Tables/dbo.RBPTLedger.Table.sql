use rewardsnow
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTLedger]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[RBPTLedger]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTLedger](
	[sid_TipNumber] [varchar](15) NOT NULL,
	[sid_AccountType_Id] [int] NOT NULL,
	[dim_RBPTLedger_HistDate] [datetime] NOT NULL,
	[dim_RBPTLedger_PointsEarned] [int] NOT NULL DEFAULT (0),
	[dim_RBPTLedger_PointsRemaining] [int] NULL DEFAULT (0),
	[dim_RBPTLedger_DateAdded] [datetime] NULL CONSTRAINT [DF_RBPTLedger_dim_RBPTLedger_DateAdded]  DEFAULT (getdate()),
	[dim_RBPTLedger_DateLastModified] [datetime] NULL CONSTRAINT [DF_RBPTLedger_dim_RBPTLedger_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTLedger] PRIMARY KEY CLUSTERED 
(
	[sid_TipNumber] ASC,
	[sid_AccountType_Id] ASC,
	[dim_RBPTLedger_HistDate] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

CREATE NONCLUSTERED INDEX [ix_RBPTLedger_PointsRemaining_TipNumber] ON [dbo].[RBPTLedger] 
(
	[dim_RBPTLedger_PointsRemaining] ASC,
	[sid_TipNumber] ASC
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ix_RBPTLedger_sid_TipNumber_dim_RBPTLedger_PointsRemaining] ON [dbo].[RBPTLedger] 
(
	[sid_TipNumber] ASC,
	[dim_RBPTLedger_PointsRemaining] ASC
) ON [PRIMARY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[tru_RBPTLedger]
   ON  [dbo].[RBPTLedger]
   AFTER update
AS 
BEGIN
	SET NOCOUNT ON;

	update l
		set dim_RBPTLedger_DateLastModified  = getdate()
	from inserted ins join dbo.RBPTLedger l
		on ins.sid_TipNumber = l.sid_TipNumber
		and ins.sid_AccountType_Id = l.sid_AccountType_Id
		and ins.dim_RBPTLedger_HistDate = l.dim_RBPTLedger_HistDate
END

GO
ALTER TABLE [dbo].[RBPTLedger]  WITH CHECK ADD  CONSTRAINT [FK_RBPTLedger_RBPTAccountType] FOREIGN KEY([sid_AccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTLedger] CHECK CONSTRAINT [FK_RBPTLedger_RBPTAccountType]