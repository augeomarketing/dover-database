use RewardsNOW
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTProductTypeTranCode_RBPTProductTypeTranCode]') AND type = 'F')
ALTER TABLE [dbo].[RBPTProductTypeTranCode] DROP CONSTRAINT [FK_RBPTProductTypeTranCode_RBPTProductTypeTranCode]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTProductTypeTranCode_TranType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTProductTypeTranCode] DROP CONSTRAINT [FK_RBPTProductTypeTranCode_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTProductTypeTranCode]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RBPTProductTypeTranCode]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[RBPTProductTypeTranCode](
	[dim_RBPTProductTypeTranCode_TipFirst] [varchar](3) NOT NULL,
	[sid_RBPTProductType_Id] [int] NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTProductTypeTranCode_DateAdded] [datetime] NOT NULL CONSTRAINT [DF_RBPTProductTypeTranCode_dim_RBPTProductTypeTranCode_DateAdded]  DEFAULT (getdate()),
	[dim_RBPTProductTypeTranCode_DateLastModified] [datetime] NOT NULL CONSTRAINT [DF_RBPTProductTypeTranCode_dim_RBPTProductTypeTranCode_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTProductTypeTranCode] PRIMARY KEY CLUSTERED 
(
	[dim_RBPTProductTypeTranCode_TipFirst] ASC,
	[sid_RBPTProductType_Id] ASC,
	[sid_TranType_TranCode] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode]  WITH NOCHECK ADD  CONSTRAINT [FK_RBPTProductTypeTranCode_RBPTProductTypeTranCode] FOREIGN KEY([sid_RBPTProductType_Id])
REFERENCES [dbo].[RBPTProductType] ([sid_RBPTProductType_id])
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode] CHECK CONSTRAINT [FK_RBPTProductTypeTranCode_RBPTProductTypeTranCode]
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode]  WITH CHECK ADD  CONSTRAINT [FK_RBPTProductTypeTranCode_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTProductTypeTranCode] CHECK CONSTRAINT [FK_RBPTProductTypeTranCode_TranType]

