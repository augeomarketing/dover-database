use rewardsnow
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTAccountType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE [dbo].[RBPTAccountType]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RBPTAccountType](
	[sid_RBPTAccountType_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_RBPTAccountType_Name] [varchar](255) NOT NULL,
	[dim_RBPTAccountType_DateEntered] [datetime] NOT NULL CONSTRAINT [DF_AccountType_DateEntered]  DEFAULT (getdate()),
	[dim_RBPTAccountType_DateLastModified] [datetime] NULL CONSTRAINT [DF_AccountType_dim_AccountType_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTAccountType] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTAccountType_Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF