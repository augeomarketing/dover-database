use REwardsNOW
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTProductType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RBPTProductType]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBPTProductType](
	[sid_RBPTProductType_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_RBPTProductType_name] [varchar](255) NOT NULL,
	[dim_RBPTProductType_DateAdded] [datetime] NOT NULL CONSTRAINT [DF_RBPTProductType_dim_RBPTProductType_DateAdded]  DEFAULT (getdate()),
	[dim_RBPTProductType_DateLastModified] [datetime] NOT NULL CONSTRAINT [DF_RBPTProductType_dim_RBPTProductType_DateLastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_RBPTProductType] PRIMARY KEY CLUSTERED 
(
	[sid_RBPTProductType_id] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

CREATE NONCLUSTERED INDEX [IX_RBPTProductType_ProductTypeID_Name] ON [dbo].[RBPTProductType] 
(
	[sid_RBPTProductType_id] ASC,
	[dim_RBPTProductType_name] ASC
) ON [PRIMARY]