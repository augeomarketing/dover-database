use rewardsnow
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTAccountTypeTranCode_RBPTAccountType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] DROP CONSTRAINT [FK_RBPTAccountTypeTranCode_RBPTAccountType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_RBPTAccountTypeTranCode_TranType]') AND type = 'F')
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] DROP CONSTRAINT [FK_RBPTAccountTypeTranCode_TranType]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RBPTAccountTypeTranCode]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[RBPTAccountTypeTranCode]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RBPTAccountTypeTranCode](
	[sid_TipFirst] [varchar](3) NOT NULL,
	[sid_RBPTAccountType_Id] [int] NOT NULL,
	[sid_TranType_TranCode] [nvarchar](2) NOT NULL,
	[dim_RBPTAccountTypeTranCode_DateEntered] [datetime] NOT NULL CONSTRAINT [DF_AccountTypeTranCode_DateEntered]  DEFAULT (getdate()),
	[dim_RBPTAccountTypeTranCode_DatelastModified] [datetime] NOT NULL CONSTRAINT [DF_AccountTypeTranCode_DatelastModified]  DEFAULT (getdate()),
 CONSTRAINT [PK_AccountTypeTranCode_1] PRIMARY KEY CLUSTERED 
(
	[sid_TipFirst] ASC,
	[sid_RBPTAccountType_Id] ASC,
	[sid_TranType_TranCode] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

CREATE NONCLUSTERED INDEX [ix_accounttypetrancode_trancode_accounttypeid] ON [dbo].[RBPTAccountTypeTranCode] 
(
	[sid_TranType_TranCode] ASC,
	[sid_RBPTAccountType_Id] ASC
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode]  WITH CHECK ADD  CONSTRAINT [FK_RBPTAccountTypeTranCode_RBPTAccountType] FOREIGN KEY([sid_RBPTAccountType_Id])
REFERENCES [dbo].[RBPTAccountType] ([sid_RBPTAccountType_Id])
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] CHECK CONSTRAINT [FK_RBPTAccountTypeTranCode_RBPTAccountType]
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode]  WITH CHECK ADD  CONSTRAINT [FK_RBPTAccountTypeTranCode_TranType] FOREIGN KEY([sid_TranType_TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[RBPTAccountTypeTranCode] CHECK CONSTRAINT [FK_RBPTAccountTypeTranCode_TranType]