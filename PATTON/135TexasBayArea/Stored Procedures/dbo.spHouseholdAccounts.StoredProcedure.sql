USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spHouseholdAccounts]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spHouseholdAccounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spHouseholdAccounts]
as

set nocount on

declare @dda			varchar(25)
declare @assocnbr		varchar(3)

--drop table #householdtips
create table #HouseHoldTips
	(assocnbr		varchar(3) primary key,
	 CombineNames	varchar(1) not null,
	 DDAMatch		varchar(1) not null)

insert into #HouseHoldTips
select assocnum assocnbr, combinenames, ddamatch
from newtnb.dbo.assoc
where 'Y' in (combinenames, ddamatch)
and tipfirst = '135'


declare csrDDA cursor for
	select distinct CheckingNbr, cus.AssocNbr
	from dbo.impCustomer cus join #householdtips hht
		on cus.assocnbr = hht.assocnbr
	and checkingnbr not in ('', '0000000000000000')
	
open csrDDA

fetch next from csrDDA into @dda, @assocnbr

while @@FETCH_STATUS = 0
BEGIN

	update imp
		set	PrimaryNm =  tmp.name1,
			SecondaryNm = tmp.name2,
			AcctName3 = tmp.name3,
			AcctName4 = tmp.name4,
			AcctName5 = tmp.name5,
			AcctName6 = tmp.name6,
			PrimarySSN = tmp.PrimarySSN
	from [dbo].[fnLinkCheckingNames] (@dda, @assocnbr) tmp join dbo.impCustomer imp
		on tmp.checking = imp.CheckingNbr
		and tmp.assocnbr = imp.assocnbr

	fetch next from csrDDA into @dda, @assocnbr
END

close csrDDA

deallocate csrDDA






--
--create table #primary
--	(sid				bigint identity(1, 1) primary key,
--	 primarycustomerdda varchar(25),
--	 CustomerAddress	varchar(40),
--	 CustomerCity		varchar(50),
--	 CustomerState		varchar(2) null,
--	 CustomerZip		varchar(15) null,
--	 CustomerHomePhone	varchar(10) null,
--	 CustomerWorkPhone	varchar(10) null)
--
--create index ix_tmp_primaryAddress_001 on #primary
--	(primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone)
--
--create index ix_impCustomer_PrimaryFlag_AddressData_001 on dbo.impCustomer
--(primaryflag, primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone)
--
--
--insert into #primary
--(primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone)
--select primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone
--from dbo.impcustomer
--where primaryflag = 'Y'
--
--update cus
--	set	CustomerAddress	= tmp.CustomerAddress,
--		CustomerCity		= tmp.CustomerCity,
--		CustomerState		= tmp.CustomerState,
--		CustomerZip		= tmp.CustomerZip,
--		CustomerHomePhone	= tmp.CustomerHomePhone,
--		CustomerWorkPhone	= tmp.CustomerWorkPhone
--from #primary tmp join dbo.impCustomer cus
--	on tmp.primarycustomerdda = cus.primarycustomerdda
--
--drop index dbo.impCustomer.ix_impCustomer_PrimaryFlag_AddressData_001
GO
