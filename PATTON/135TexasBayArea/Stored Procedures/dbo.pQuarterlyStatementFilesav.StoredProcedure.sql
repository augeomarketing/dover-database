USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[pQuarterlyStatementFilesav]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[pQuarterlyStatementFilesav]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pQuarterlyStatementFilesav] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3), @Segment char(1)
--CREATE PROCEDURE pMonthlyStatementFile @StartDate varchar(10), @EndDate varchar(10), @Tipfirst char(3), @Segment char(1)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Quarterly_Statement_File '
exec sp_executesql @SQLTruncate

if exists(select name from sysobjects where name='wrkhist')
begin 
	drop table wrkhist
end


set @SQLSelect='select tipnumber, trancode, sum(points) as points into wrkhist from ' + QuoteName(@DBName) + N'.dbo.history where histdate>=@Startdate and histdate <=@Enddate group by tipnumber, trancode '
Exec sp_executesql @SQLSelect, N'@StartDate DateTime, @EndDate DateTime', @StartDate=@StartDate, @EndDate=@EndDate 


/*set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip, acctnum, lastfour)
        	select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), zipcode, 
		from ' + QuoteName(@DBName) + N'.dbo.customer ' */
if (@segment='C' or @segment='D')
	begin
		set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        		select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), left(zipcode,5) 
			from ' + QuoteName(@DBName) + N'.dbo.customer where segmentcode=@Segment '
		Exec sp_executesql @SQLInsert, N'@Segment char(1)', @Segment=@Segment
	end
else
	if (@segment<>'C' and @segment<>'D')
		Begin	
			set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        			select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), left(zipcode,5) 
				from ' + QuoteName(@DBName) + N'.dbo.customer '
			Exec sp_executesql @SQLInsert
		End
 
set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set acctnum=(select top 1 rtrim(acctid) from ' + QuoteName(@DBName) + N'.dbo.affiliat where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber) '
Exec sp_executesql @SQLUpdate

set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set lastfour=right(rtrim(acctnum),4) '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   37       */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointspurchasedcr=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode=''63'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode=''63'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointspurchaseddb=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode=''67'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode=''67'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with bonuses            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsbonuscr=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and (trancode like ''B%'' or trancode= ''NW'')) 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and (trancode like ''B%'' or trancode= ''NW'')) '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsadded=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode in(''IE'', ''TR'')) 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode in(''IE'', ''TR'')) '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point increases */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded '
	Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsredeemed=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode like ''R%'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode like ''R%'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsredeemed=pointsredeemed-
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''DR'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''DR'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsreturnedcr=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''33'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''33'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsreturneddb=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''37'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''37'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with minus adjustments    */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointssubtracted=
		(select sum(points) from wrkhist 
		where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''DE'') 	
		where exists(select * from wrkhist where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and trancode = ''DE'') '
	Exec sp_executesql @SQLUpdate 

/* Load the statmement file with total point decreases */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsbegin=
		(select ' + Quotename(@MonthBucket) + N'from ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber)
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased '
exec sp_executesql @SQLUpdate
GO
