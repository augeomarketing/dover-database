USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[pQuarterlyAuditValidation]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[pQuarterlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pQuarterlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @errmsg varchar(50) 

set @errmsg='Ending Balances do not match'

delete from Quarterly_Audit_ErrorFile

INSERT INTO Quarterly_Audit_ErrorFile
select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
from Quarterly_Statement_File a, 	Current_Month_Activity b
where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints
GO
