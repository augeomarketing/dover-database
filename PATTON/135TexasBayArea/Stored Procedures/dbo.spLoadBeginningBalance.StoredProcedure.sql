USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spLoadBeginningBalance]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spLoadBeginningBalance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadBeginningBalance] @MonthBeginDate datetime

AS 

/*Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)  chg 6/15/2006   */
Declare @MonthBucket char(10), @MonthBegin char(2), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)

set @MonthBegin = month(@MonthBeginDate)  + 1

if CONVERT( int , @MonthBegin)='13' 
	begin
	set @monthbegin='1'
end	

set @MonthBucket='MonthBeg' + @monthbegin

set @SQLUpdate=N'
			update bbt
				set ' + quotename(@MonthBucket) + N' = msf.pointsend
			from dbo.beginning_balance_table bbt join dbo.monthly_statement_file msf
				on bbt.tipnumber = msf.tipnumber'

set @SQLInsert=N'
				insert into dbo.Beginning_Balance_Table
				(Tipnumber, ' + Quotename(@MonthBucket) + ')
				select msf.tipnumber, msf.pointsend
				from dbo.monthly_statement_file msf left outer join dbo.Beginning_Balance_Table bbt
					on msf.tipnumber = bbt.tipnumber
				where bbt.tipnumber is null'


exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert
GO
