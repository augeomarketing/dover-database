USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spOneTimeBonuses]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spOneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[spOneTimeBonuses]
	(@MonthEndDate			datetime)

AS


------------------------------------------------------------------------------------------
-- E Statement Bonuses
------------------------------------------------------------------------------------------

create table #EStatements
	(Tipnumber varchar(15) primary key)

--
-- Get list of all customers who signed up for EStatements
--
insert into #EStatements
select tipnumber
from rn1.newtnb.dbo.[1Security]
where isnull(email, '') != ''
and upper(EmailStatement) = 'Y'
and left(tipnumber,3) = '135'

--
-- Remove from temp file FI's not issuing EStatement Bonuses
--
delete tmp
from #EStatements tmp left outer join newtnb.dbo.BonusProgramFI fi
	on left(tmp.tipnumber,3) = fi.sid_BonusProgramFI_TipFirst	
	and @MonthEndDate between fi.dim_BonusProgramFI_Effectivedate and fi.dim_BonusProgramFI_ExpirationDate

join newtnb.dbo.BonusProgram bp
	on fi.sid_BonusProgram_ID = bp.sid_BonusProgram_ID
	and 'BE' = bp.sid_TranType_TranCode

where fi.sid_BonusProgramFI_TipFirst is null 

--
-- Remove from this temp file those who already have been
-- bonused for e-statements
--
Delete tmp
from #EStatements tmp join newtnb.dbo.OneTimeBonuses otb
	on tmp.tipnumber = otb.tipnumber
	and 'BE' = otb.trancode


--
-- Now we have a clean temp file of customers to get E-Statements
--
insert into dbo.OneTimeBonuses_Stage
(TipNumber, Trancode, AcctID, DateAwarded)
select distinct tmp.tipnumber, 'BE', null , @MonthEndDate
from #EStatements tmp join dbo.customer_stage cus
	on tmp.tipnumber = cus.tipnumber

--
-- Add to TransStandard
--
insert into dbo.TransStandard
( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt], ratio ) 
select tmp.TipNumber, @MonthEndDate trandate, '' as acctnum, 'BE' as trancode, 1 as trannum, dim_BonusProgramFI_BonusPoints , @MonthEndDate,
		tt.ratio
from #Estatements tmp join newtnb.dbo.BonusProgramFI fi
	on left(tmp.tipnumber,3) = fi.sid_BonusProgramFI_TipFirst
	and @MonthEndDate between fi.dim_BonusProgramFI_Effectivedate and fi.dim_BonusProgramFI_ExpirationDate

join newtnb.dbo.BonusProgram bp
	on fi.sid_BonusProgram_ID = bp.sid_BonusProgram_ID
	and 'BE' = bp.sid_TranType_TranCode

join dbo.trantype tt
	on bp.sid_TranType_TranCode = tt.trancode


------------------------------------------------------------------------------------------
-- New Account Bonuses
------------------------------------------------------------------------------------------
-- drop table #newacctbonuses
--declare @MonthEndDate datetime
--set @MonthEndDate = '2009-04-30'

create table #NewAcctBonuses
	(Tipnumber		varchar(15) not null,
	 AcctId			varchar(16) not null,
	 TranCode			varchar(2) not null,
	 HistDate			datetime not null,
	 Points			int not null)

insert into #NewAcctBonuses
select distinct his.tip, '' as AcctID, 'BN',  @MonthEndDate, fi.dim_BonusProgramFI_BonusPoints
from dbo.transstandard his join dbo.affiliat_stage aff
	on his.tip = aff.tipnumber

join newtnb.dbo.BonusProgramFI fi
	on left(his.tip,3) = fi.sid_BonusProgramFI_TipFirst
	and @MonthEndDate between fi.dim_BonusProgramFI_Effectivedate and fi.dim_BonusProgramFI_ExpirationDate

join newtnb.dbo.BonusProgram bp
	on fi.sid_BonusProgram_ID = bp.sid_BonusProgram_ID
	and bp.sid_TranType_TranCode = 'BN'

left outer join dbo.OneTimeBonuses otb
	on otb.tipnumber = his.tip

where otb.tipnumber is null and 
	'Y' = 	case
				when left(his.tip,3) = '135' and aff.accttype like 'CREDIT%' then 'Y'
				when left(his.tip,3) != '135' then 'Y'
				else 'N'
			end  
order by his.tip



insert into dbo.OneTimeBonuses_Stage
(TipNumber, Trancode, AcctID, DateAwarded)
select tipnumber, trancode, acctid, histdate
from #NewAcctBonuses


--
-- Add to TransStandard
--
insert into dbo.TransStandard
( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt], ratio ) 
select tmp.TipNumber, @MonthEndDate trandate, '' as acctnum, 'BN' as trancode, 1 as trannum, dim_BonusProgramFI_BonusPoints, @MonthEndDate, tt.ratio
from #NewAcctBonuses tmp join newtnb.dbo.BonusProgramFI fi
	on left(tmp.tipnumber,3) = fi.sid_BonusProgramFI_TipFirst
	and @MonthEndDate between fi.dim_BonusProgramFI_Effectivedate and fi.dim_BonusProgramFI_ExpirationDate

join newtnb.dbo.BonusProgram bp
	on fi.sid_BonusProgram_ID = bp.sid_BonusProgram_ID
	and 'BN' = bp.sid_TranType_TranCode

join dbo.trantype tt
	on bp.sid_TranType_TranCode = tt.trancode


/*
------------------------------------------------------------------------------------------
-- DOUBLE POINTS - based on processing month
------------------------------------------------------------------------------------------
if object_id('tempdb.dbo.#tipfirsts') is not null
    drop table #tipfirsts

-- Build temp table of tip firsts that are doing one time double points
create table #TipFirsts
	(TipFirst		varchar(3)	primary key,
	 TranCode		varchar(2),
	 Multiplier		int)


-- NOTE:  the point multiplier calculation.  On double points bonuses, we only want to add a bonus award for the points 
--        earned for the month.  Pts Earned + bonus of Pts earned = double points
--                               Pts Earned + 2 x Pts earned = Triple Points
-- When the entry is put into the Bonus Program FI table, put in 2 - as in double points, 3 for triple

insert into #TipFirsts
select sid_BonusProgramFI_TipFirst, bp.sid_TranType_TranCode,
									case 
										when fi.dim_BonusProgramFI_PointMultiplier - 1 <= 0 then 1
										else fi.dim_BonusProgramFI_PointMultiplier - 1
									end
from dbo.BonusProgramFI fi join dbo.bonusprogram bp
	on fi.sid_bonusprogram_id = bp.sid_bonusprogram_id
where bp.sid_trantype_trancode = 'BF'
and @MonthEndDate between fi.dim_BonusProgramFI_Effectivedate and fi.dim_BonusProgramFI_ExpirationDate


-- Create temp table of tipnumber and points earned for the month.
-- ONLY TIPS THAT WERE ADDED THIS MONTH!
if object_id('tempdb.dbo.#tips') is not null
    drop table #tips

create table #Tips
	(TipNumber		varchar(15) primary key,
	 MTDPoints		int)

insert into #tips
select tip, sum( cast(tranamt as int))
from dbo.transstandard ts join #tipfirsts tf
	on left(ts.tip,3) = tf.TipFirst
join dbo.customer_stage cus
	on ts.tip = cus.tipnumber
where cus.dateadded = @MonthEndDate and ts.trancode not like 'BF' 
group by tip


Update tmp
	set MTDPoints = MTDPoints * Multiplier
from #TipFirsts tf join #Tips tmp
	on tf.tipfirst = left(tmp.tipnumber,3)

--
-- Now add the bonus transactions into TransStandard
--
insert into dbo.transstandard
(TIP, TranDate, AcctNum, TranCode, TranNum, TranAmt, Ratio, CrdActvlDt)
select tmp.tipNumber, @MonthEndDate, '' as AcctNum, bp.sid_TranType_TranCode, 1, MTDPoints, 1, @MonthEndDate
from #TipFirsts tf join #Tips tmp
	on tf.tipfirst = left(tmp.TipNumber,3)
join dbo.BonusProgramFI bpFI
    on bpfi.sid_BonusProgramFI_TipFirst = tf.tipfirst
    and @MonthEndDate >= dim_BonusProgramFI_Effectivedate and @MonthEndDate <= dim_BonusProgramFI_ExpirationDate

join dbo.bonusprogram bp
    on bp.sid_BonusProgram_ID = bpfi.sid_BonusProgram_ID


-----------------------------------------------------------------------------------------------------------------------------
--
-- Double Points - based on actual transaction date
--
-----------------------------------------------------------------------------------------------------------------------------
if object_id('tempdb.dbo.#tipfirsts_bm') is not null
    drop table #tipfirsts_bm

if object_id('tempdb.dbo.#tips_bm') is not null
    drop table #tips_bm

--declare @monthenddate datetime = '09/14/2010'

-- Build temp table of tip firsts that are doing one time double points
create table #TipFirsts_bm
	(TipFirst		    varchar(3)	primary key,
	 TranCode		    varchar(2),
	 Multiplier	    int,
	 effectivedate	    datetime,
      expirationdate    datetime
      )


-- NOTE:  the point multiplier calculation.  On double points bonuses, we only want to add a bonus award for the points 
--        earned for the month.  Pts Earned + bonus of Pts earned = double points
--                               Pts Earned + 2 x Pts earned = Triple Points
-- When the entry is put into the Bonus Program FI table, put in 2 - as in double points, 3 for triple

insert into #TipFirsts_bm
select sid_BonusProgramFI_TipFirst, bp.sid_TranType_TranCode,
									case 
										when fi.dim_BonusProgramFI_PointMultiplier - 1 <= 0 then 1
										else fi.dim_BonusProgramFI_PointMultiplier - 1
									end, fi.dim_BonusProgramFI_Effectivedate, fi.dim_BonusProgramFI_ExpirationDate
from dbo.BonusProgramFI fi join dbo.bonusprogram bp
	on fi.sid_bonusprogram_id = bp.sid_bonusprogram_id

-- Txn Code BM = Monthly Bonus
-- check if the min transactiondate is within the bonus program effective date & expiration date  OR
-- the max transactiondate is  within the bonus program effective date & expiration date
-- Do this because end of December txns could still come in with January and even February data
where bp.sid_trantype_trancode = 'BM'  --and  ( (imptxn.mintxndate >= fi.dim_BonusProgramFI_Effectivedate ) or (imptxn.maxtxndate <= fi.dim_BonusProgramFI_ExpirationDate)  )


create table #Tips_bm
	(TipNumber		varchar(15) primary key,
	 MTDPoints		int)

--
-- This is set up to only double point earnings (purchases), not returns
insert into #tips_bm
select tipnumber, sum(points * multiplier)
from dbo.imptransaction imp join #tipfirsts_bm tf
    on left(imp.tipnumber,3) = tf.tipfirst
where   rntrancode like '6%'
    and imp.transactiondt >= effectivedate and imp.transactiondt <= expirationdate
group by tipnumber





--
-- Now add the bonus transactions into TransStandard
--
insert into dbo.transstandard
(TIP, TranDate, AcctNum, TranCode, TranNum, TranAmt, Ratio, CrdActvlDt)
select t.tipnumber, @monthenddate, '' as AcctNum, tf.trancode, 1, t.mtdpoints, tt.ratio, @monthenddate
from #tips_bm t join #tipfirsts_bm tf
    on left(t.tipnumber,3) = tf.tipfirst
join rewardsnow.dbo.trantype tt
    on tf.trancode = tt.trancode



*/
GO
