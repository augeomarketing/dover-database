USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spStageMonthlyAuditValidation]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spStageMonthlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStageMonthlyAuditValidation] 
    @errorrows	 int OUTPUT

AS


/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
delete from dbo.monthly_audit_errorfile


insert into dbo.monthly_audit_errorfile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Errormsg, Currentend)

select msf.Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, 
	   pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased,'Ending Balances do not match',
	   cma.adjustedendingpoints
		  
from dbo.Monthly_Statement_File msf join dbo.current_month_activity cma
    on msf.tipnumber = cma.tipnumber
where msf.pointsend != cma.adjustedendingpoints

set @errorrows = @@rowcount
GO
