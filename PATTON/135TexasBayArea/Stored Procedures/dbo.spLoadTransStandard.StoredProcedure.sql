USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded datetime AS


-- Clear TransStandard 
Truncate table dbo.TransStandard 

-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  cus.[TipNumber], @DateAdded, txn.[CCAcctNbr], [RNTranCode], count(*) , sum([Points]), @DateAdded
from dbo.impTransaction txn join dbo.customer_stage cus
	on txn.tipnumber = cus.tipnumber
where Status in ('A', 'S') -- active or suspended only
group by cus.[TipNumber], txn.[CCAcctNbr], [RNTranCode]


-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update dbo.TransStandard 
	set	TranType = left(R.Description, 20),
		Ratio = R.Ratio 
from RewardsNow.dbo.Trantype R join dbo.TransStandard T 
on R.TranCode = T.Trancode



--
-- For FIs configured to zero points for delinquent accounts,
-- Add in a "DE" to zero out their points
-- The FI's current month transactions have already been zeroed out in the calc points sproc

/*
create table #TipsToZeroOut
(TipNumber		    varchar(15) primary key)

--
-- Get list of Tips to zero out
insert into #TipSToZeroOut
    (TipNumber)
select distinct tipnumber
from dbo.assocdelinquent ad join dbo.impcustomer cus
   on ad.assocnum = cus.assocnbr
where isnumeric(isnull(cus.delinquentcycles, 0)) = 1
   and cus.delinquentcycles >= ad.delinquentcycles
   
--
-- create a "DE" decrease earned transaction to 
insert into dbo.TransStandard
	( TIP, TranDate, AcctNum, TranCode, Ratio, TranType, TranNum, TranAmt, CrdActvlDt ) 
select stg.tipnumber, @dateadded, '' as ccacctnbr, 'DE' as Trancode, -1 as Ratio, 'Zero out Pts - Dlqnt' as TranType,
	   1 as TranNum, stg.runavailable, @dateadded
from #tipstozeroout tmp join dbo.customer_stage stg
    on tmp.tipnumber = stg.tipnumber

*/
GO
