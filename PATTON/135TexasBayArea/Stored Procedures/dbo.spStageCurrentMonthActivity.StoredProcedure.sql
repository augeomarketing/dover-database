USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spStageCurrentMonthActivity]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spStageCurrentMonthActivity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spStageCurrentMonthActivity] @EndDate datetime
AS

/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
-- PHB 4/18/2008 Changed to work w/ LOC FCU
*/
--
--Declare @EndDate DateTime 						--RDT 10/09/2006 
--set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 

set @enddate = dateadd(dd, 1, @enddate)

truncate table dbo.Current_Month_Activity

insert into dbo.Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer_Stage


/* Load the current activity table with increases for the current month         */
update dbo.Current_Month_Activity
	set increases= isnull( (select sum(points) 
							from dbo.history_Stage hs 
							where hs.histdate >= @enddate and ratio='1'
							and hs.Tipnumber = Current_Month_Activity.Tipnumber ), 0)

/* Load the current activity table with decreases for the current month         */
update dbo.Current_Month_Activity
	set decreases=	isnull( (select sum(points) 
							 from dbo.history_Stage hs 
							 where hs.histdate >= @enddate and ratio= '-1'
							 and hs.Tipnumber = Current_Month_Activity.Tipnumber ), 0)


--
--create table #points 
--(id		bigint identity(1,1) primary key,
-- tipnumber varchar(15),
-- ratio	int,
-- points	bigint default(0)
--)
--
--create index ix__points on #points (tipnumber, ratio, points)
--
--insert into #points
--select tipnumber, ratio, sum(points)
--from history_stage
--where ratio in (1, -1) and secid = 'NEW'
--group by tipnumber, ratio
--
--
--update cma
--	set increases = isnull(points, 0)
--from current_month_activity cma left outer join #points tmp
--	on cma.tipnumber = tmp.tipnumber
--where ratio = '1'
--
--
--
--update cma
--	set decreases = isnull(points, 0)
--from current_month_activity cma left outer join #points tmp
--	on cma.tipnumber = tmp.tipnumber
--where ratio = '-1'
--


/* Load the calculate the adjusted ending balance        */
update dbo.Current_Month_Activity
	set adjustedendingpoints=endingpoints - increases + decreases
GO
