USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[TMP_ProgramAnal]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[TMP_ProgramAnal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rich
-- Create date: 
-- Description:	OneShot for Duane
-- =============================================
CREATE PROCEDURE [dbo].[TMP_ProgramAnal] 
	-- Add the parameters for the stored procedure here
	@p1 int = 0, 
	@p2 int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT @p1, @p2
Truncate table Temphist 

Insert into Temphist 
( hMonth , HYear, Trancode  , Points )  
select 
	Month(histdate), 
	Year(histdate) , 
	trancode, 
	sum(points*ratio)  
	from history
	Group by history.histdate,  trancode
	union 
	select Month(histdate), Year(histdate) , trancode, sum(points*ratio) 
	from historydeleted
	Group by historydeleted.histdate,  trancode

Update Temphist
Set TranDesc = Trantype.Description
from TranType 
where Trantype.Trancode = TempHist.Trancode
--
--select HYear , hMonth , TranDesc, sum(points) from Temphist 
--group by HYear , hMonth , TranDesc
--order by HYear , hMonth , TranDesc

END
GO
