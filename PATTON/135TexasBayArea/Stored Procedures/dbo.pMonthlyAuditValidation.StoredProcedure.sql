USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[pMonthlyAuditValidation]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[pMonthlyAuditValidation]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @errmsg varchar(50) 

set @errmsg='Ending Balances do not match'

delete from Monthly_Audit_ErrorFile

INSERT INTO Monthly_Audit_ErrorFile
select a.Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased, @errmsg, b.AdjustedEndingPoints
from Monthly_Statement_File a, 	Current_Month_Activity b
where a.tipnumber=b.tipnumber and a.pointsend<> b.AdjustedEndingPoints
GO
