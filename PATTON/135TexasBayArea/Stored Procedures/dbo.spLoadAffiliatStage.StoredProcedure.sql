USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  ******************************************/
/* Date:  4/1/07						*/
/* Author:  Rich T						*/
/*  ******************************************/

/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*
	4/15/2008 PHB:  Copied & modified for use w/ 218LOCFCU
*/
/*********************************************/


CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd datetime  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, AffiliatFlag)
	select distinct t.CCAcctNbr, t.TipNumber, act.accttypedesc,  @MonthEnd, 
			case
				when impc.rewardsnowsts is null then ltrim(rtrim(c.status))
				else impc.rewardsnowsts
			end, 
			case
				when act.accttypedesc = 'CREDIT' then 'CREDIT CARD'
				when act.accttypedesc = 'DEBIT' then 'DEBIT CARD'
				else 'CARD'
			end accttype, isnull(c.LastName, 'NOT FOUND'), 0, null

	from dbo.customer_stage c join dbo.impTransaction t 
		on c.Tipnumber = t.Tipnumber
		
	left outer join dbo.impcustomer impc
		on impc.ccacctnbr = t.ccacctnbr

	join dbo.AcctType act
		on act.AcctType = t.RNtrancode

	left outer join dbo.Affiliat_Stage aff
	on t.CCAcctNbr = aff.AcctId
--	and t.tipnumber = aff.tipnumber
--	and act.accttype = aff.accttype

	where aff.AcctId is null 



--
-- Add in new credit cards, in case we received a new card# from a lost/stolen, yet didn't have 
-- any transactions
--
insert into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, AffiliatFlag)
select distinct c.ccacctnbr, c.tipnumber, aff.AcctType, @MonthEnd, c.rewardsnowsts, aff.AcctTypeDesc, 
		isnull(c.LastName, 'NOT FOUND'), 0, null

from dbo.impcustomer c join dbo.affiliat aff
	on c.oldccacctnbr = aff.acctid

join dbo.customer_stage cstg
	on c.tipnumber = cstg.tipnumber

left outer join dbo.affiliat_stage aff2
	on c.ccacctnbr = aff2.acctid

where c.oldccacctnbr not in ('0000000000000000', '') and aff2.acctid is null


insert into dbo.affiliat_stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, AffiliatFlag)
select distinct txn.ccacctnbr, txn.tipnumber, act.accttypedesc,  @MonthEnd, 
			case
				when impc.rewardsnowsts is null then ltrim(rtrim(c.status))
				else impc.rewardsnowsts
			end,
			case
				when act.accttypedesc = 'CREDIT' then 'CREDIT CARD'
				when act.accttypedesc = 'DEBIT' then 'DEBIT CARD'
				else 'CARD'
			end accttype,
		isnull(c.LastName, 'NOT FOUND'), 0, null
from dbo.imptransaction txn join dbo.customer_stage c
	on txn.tipnumber = c.tipnumber
	
join dbo.accttype act
	on txn.rntrancode = act.accttype
	
left outer join dbo.impcustomer impc
	on impc.ccacctnbr = txn.ccacctnbr
	
left outer join dbo.affiliat_stage aff
	on aff.acctid = txn.ccacctnbr
where aff.tipnumber is null


--- 
-- Add in affiliat rows for customers that do not have transactions.
-- Tips have been assigned, and there is a card number in the impcustomer table - so add them
-- for if/when txns do come in for them 
---

Insert Into dbo.Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, AffiliatFlag)
select distinct imp.ccacctnbr, imp.tipnumber, 'CREDIT' as AcctType, @MonthEnd, 
			case
				when imp.rewardsnowsts is null then ltrim(rtrim(stg.status))
				else imp.rewardsnowsts
			end, '' as accttypedesc, 
	isnull(stg.lastname, 'NOTFOUND'), 0, null

from dbo.impcustomer imp left outer join dbo.affiliat_stage aff	
	on imp.ccacctnbr = aff.acctid

join dbo.customer_stage stg
	on imp.tipnumber = stg.tipnumber

where aff.tipnumber is null



--
-- For Lost/Stolen cards - those card#s found in OLDCCACCTNBR, mark them as "C" in the affiliat table
--
update aff
	set acctstatus = 'C'
from dbo.impcustomer imp join dbo.affiliat_stage aff
	on imp.oldccacctnbr = aff.acctid
--where acctstatus = 'A'


--
-- mark the card closed if there is NOT a "old ccacctnbr" specfied
--
update aff
	set acctstatus = 'C'
from dbo.impcustomer imp join dbo.affiliat_stage aff
	on imp.ccacctnbr = aff.acctid
where RewardsNowSts = 'C' and ( len(OldCCAcctNbr) = 0 OR oldccacctnbr like '0000000000%')


--
-- Mark card closed if rewardsnowsts is closed, and affiliat hasn't been marked closed
--
update aff
    set acctstatus = 'C'
from dbo.impcustomer imp join dbo.affiliat_stage aff
    on imp.ccacctnbr = aff.acctid
where rewardsnowsts = 'C'
and aff.acctstatus != 'C'
GO
