USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spCalculatePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************	*/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* 4/15/08 PHB - Modified to work with		*/
/*				LOC FCU				*/
/*  **************************************	*/
/*  Calculates points in input_transaction	*/
/*  **************************************	*/
CREATE PROCEDURE [dbo].[spCalculatePoints]  
		@MonthEndDate			datetime


AS   

	Update imp 
		set Points = round(imp.TransactionAmt * F.pointFactor , 0)
	from dbo.impTransaction imp join dbo.Trancode_factor F 
	on imp.RNTrancode = f.Trancode


--
-- Some FI's use point multipliers (1pt for $2 for ex.) based on System & Prin values
-- Adjust point values for rows that meet these criteria only
--
	update imp
		set Points = round(imp.TransactionAmt * sp.pointfactor, 0)
	from dbo.imptransaction imp join newtnb.dbo.SystemPrin_Factor sp
		on imp.system = sp.system
		and imp.prin = sp.prin


	
--
-- Using TNB's provided processing status rules, points do NOT get
-- get calculated on these statuses
--
	update imptransaction	
		set points = 0
	where accountsts in ('B', 'C', 'E', 'F', 'I', 'U', 'Z')


--
-- Zero points out for card holders who are delinquent.  This is configured in the AssocDelinquent table
--
/*
    create table #TipsToZeroOut
    (TipNumber		    varchar(15) primary key)

    -- Get list of Tips to zero out
    insert into #TipSToZeroOut
    (TipNumber)
    select distinct tipnumber
    from dbo.assocdelinquent ad join dbo.impcustomer cus
	   on ad.assocnum = cus.assocnbr
    where isnumeric(isnull(cus.delinquentcycles, 0)) = 1
	   and cus.delinquentcycles >= ad.delinquentcycles
	   
    --
    -- Zero out points
    update txn
	   set points = 0
    from #tipstozeroout ttz join dbo.imptransaction txn
	   on ttz.tipnumber = txn.tipnumber

*/
GO
