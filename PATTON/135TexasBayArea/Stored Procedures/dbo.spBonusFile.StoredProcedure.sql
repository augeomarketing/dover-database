USE [135TexasBayArea]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFile]    Script Date: 08/01/2012 10:10:09 ******/
DROP PROCEDURE [dbo].[spBonusFile]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This Stored Procedure imports Bonus into PointsNow Tables */

/*	Read a bonus file and import points
 */
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @CardType Char(6), 
-- @BonusAmt int, 
-- @TranType 
/******************************************************************************/
CREATE PROCEDURE [dbo].[spBonusFile] AS

Declare @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @MemberNum	char(16)
Declare @Points 	numeric (9)
Declare @Reason	char(15)
Declare @DateAdded	datetime
Declare @TranCode	char(2)
Declare @BonusDesc 	char(40)

-- Remove records with Null Points from Input_bonus
Delete from Input_Bonus where Points is null 

-- Load Tipnumbers to Input_bonus where Misc2 = membernum
UPDATE Input_Bonus
	SET 	TIPNUMBER = C.TIPNUMBER
	FROM 	Affiliat C Join Input_Bonus I on C.acctid = I.Membernum

-- Copy Missing Members to input_Bonus_error
Truncate Table Input_Bonus_Error 
Insert into Input_Bonus_Error select * from input_bonus where tipnumber is null

-- Delete Missing Members from input_Bonus
Delete from input_bonus where tipnumber is null

-- Cursor thru the input table
 Declare Input_Crsr Cursor for  Select * from Input_Bonus 

Open Input_Crsr 
Fetch Input_Crsr into  @MemberNum, @Points, @Reason, @Tipnumber, @Trancode, @DateAdded

If @@Fetch_Status = 1 GoTo Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN

	Set @BonusDesc = (select Description from TranType where Trancode = @Trancode) 

	UPDATE Customer 
		set RunAvailable = RunAvailable + @Points, RunBalance=RunBalance + @Points  
		where tipnumber = @Tipnumber	

	INSERT INTO history(TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
		Values(@Tipnumber, @MemberNum, convert(char(10), @DateAdded,101), @TranCode, '1', @Points, '1', @BonusDesc, '0')
 
	Next_Record:
		Fetch Input_Crsr into @MemberNum, @Points, @Reason, @Tipnumber, @Trancode, @DateAdded 
END


Fetch_Error:
close  Input_Crsr
deallocate  Input_Crsr
GO
