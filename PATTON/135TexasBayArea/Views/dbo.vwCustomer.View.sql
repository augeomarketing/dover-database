USE [135TexasBayArea]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 08/01/2012 10:10:14 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [135TexasBayArea].dbo.customer
GO
