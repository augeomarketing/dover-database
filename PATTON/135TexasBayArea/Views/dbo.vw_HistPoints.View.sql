USE [135TexasBayArea]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 07/30/2012 13:39:29 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_histpoints]'))
DROP VIEW [dbo].[vw_histpoints]
GO


/****** Object:  View [dbo].[vw_histpoints]    Script Date: 07/30/2012 13:39:30 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

	Create view [dbo].[vw_histpoints] as 
		select tipnumber, sum(points * ratio) as points 
	from dbo.history_stage 
	where secid = 'NEW' group by tipnumber

GO


