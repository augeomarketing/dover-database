USE [135TexasBayArea]
GO
/****** Object:  View [dbo].[vwHistorydeleted]    Script Date: 08/01/2012 10:10:14 ******/
DROP VIEW [dbo].[vwHistorydeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistorydeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
			 from [135TexasBayArea].dbo.historydeleted
GO
