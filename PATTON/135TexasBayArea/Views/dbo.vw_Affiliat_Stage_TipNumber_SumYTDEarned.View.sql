USE [135TexasBayArea]
GO

/****** Object:  View [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]    Script Date: 07/30/2012 13:39:22 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]'))
DROP VIEW [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]
GO


/****** Object:  View [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]    Script Date: 07/30/2012 13:39:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[vw_Affiliat_Stage_TipNumber_SumYTDEarned]

as


SELECT     TIPNUMBER, ACCTID, SUM(YTDEarned) AS YTDEarned
FROM         dbo.Affiliat_Stage
GROUP BY TIPNUMBER, ACCTID

GO


