USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impcustomer_201205]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[impcustomer_201205]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impcustomer_201205](
	[sid_impCustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[AssocNbr] [varchar](3) NULL,
	[CheckingNbr] [varchar](16) NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[TipPrefix] [varchar](3) NULL,
	[TipNumber] [varchar](15) NULL,
	[AddressLine6] [varchar](40) NULL,
	[RewardsNowSts] [varchar](1) NULL,
	[LastName] [varchar](40) NULL,
	[Misc3] [varchar](7) NULL,
	[DelinquentCycles] [varchar](2) NULL,
	[DelinquentDays] [varchar](3) NULL,
	[GroupID] [varchar](13) NULL,
	[RelationshipControl] [varchar](1) NULL,
	[DependentStrategy] [varchar](4) NULL
) ON [PRIMARY]
GO
