USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[NewImport]    Script Date: 02/25/2014 13:02:26 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Numbe__1E6F845E]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Numbe__1E6F845E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] DROP CONSTRAINT [DF__NewImport__Numbe__1E6F845E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Amoun__1F63A897]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Amoun__1F63A897]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] DROP CONSTRAINT [DF__NewImport__Amoun__1F63A897]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Numbe__2057CCD0]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Numbe__2057CCD0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] DROP CONSTRAINT [DF__NewImport__Numbe__2057CCD0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Amoun__214BF109]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Amoun__214BF109]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] DROP CONSTRAINT [DF__NewImport__Amoun__214BF109]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewImport]') AND type in (N'U'))
DROP TABLE [dbo].[NewImport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewImport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NewImport](
	[sid_Import_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[NumberPurchases] [varchar](2) NULL,
	[AmountPurchases] [int] NULL,
	[NumberReturns] [varchar](2) NULL,
	[AmountReturns] [int] NULL,
 CONSTRAINT [PK__NewImport] PRIMARY KEY CLUSTERED 
(
	[sid_Import_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Numbe__1E6F845E]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Numbe__1E6F845E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] ADD  DEFAULT ((0)) FOR [NumberPurchases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Amoun__1F63A897]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Amoun__1F63A897]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] ADD  DEFAULT ((0)) FOR [AmountPurchases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Numbe__2057CCD0]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Numbe__2057CCD0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] ADD  DEFAULT ((0)) FOR [NumberReturns]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewImport__Amoun__214BF109]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewImport]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewImport__Amoun__214BF109]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewImport] ADD  DEFAULT ((0)) FOR [AmountReturns]
END


End
GO
