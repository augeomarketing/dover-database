USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [varchar](2) NOT NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NOT NULL
) ON [PRIMARY]
GO
