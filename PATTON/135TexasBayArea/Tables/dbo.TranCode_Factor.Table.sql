USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[TranCode_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL,
 CONSTRAINT [PK_TranCode_Factor] PRIMARY KEY CLUSTERED 
(
	[Trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
