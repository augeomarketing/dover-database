USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[HISTORYTIP] DROP CONSTRAINT [DF__HISTORYTI__Overa__07020F21]
GO
DROP TABLE [dbo].[HISTORYTIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HISTORYTIP] ADD  DEFAULT (0) FOR [Overage]
GO
