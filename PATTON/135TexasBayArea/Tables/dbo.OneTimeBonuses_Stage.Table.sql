USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[OneTimeBonuses_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NOT NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NOT NULL
) ON [PRIMARY]
GO
