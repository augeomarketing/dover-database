USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[NewChecking]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[NewChecking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewChecking](
	[Checking Account Number ] [varchar](53) NULL,
	[Member Name] [nvarchar](255) NULL,
	[Product ] [nvarchar](255) NULL,
	[Debit PREFIX] [varchar](53) NULL,
	[CARDNBR] [varchar](53) NULL,
	[Acctid] [char](20) NULL
) ON [PRIMARY]
GO
