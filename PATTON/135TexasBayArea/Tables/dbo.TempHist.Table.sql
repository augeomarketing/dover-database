USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[TempHist]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[TempHist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TempHist](
	[HMonth] [int] NULL,
	[hYear] [int] NULL,
	[Trancode] [char](2) NULL,
	[Trandesc] [char](40) NULL,
	[points] [int] NULL
) ON [PRIMARY]
GO
