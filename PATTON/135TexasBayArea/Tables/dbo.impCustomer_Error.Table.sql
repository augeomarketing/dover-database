USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impCustomer_Error]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[impCustomer_Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impCustomer_Error](
	[sid_impCustomer_Error_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[AssocNbr] [varchar](3) NULL,
	[CheckingNbr] [varchar](16) NULL,
	[TipPrefix] [varchar](3) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK__impCustomer_Error] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_Error_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
