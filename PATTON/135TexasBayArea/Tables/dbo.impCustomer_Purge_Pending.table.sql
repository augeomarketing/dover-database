USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impCustomer_Purge_pending]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[impCustomer_Purge_pending] DROP CONSTRAINT [DF_impCustomer_Purge_pending_DateAdded]
GO
DROP TABLE [dbo].[impCustomer_Purge_pending]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impCustomer_Purge_pending](
	[TipNumber] [varchar](15) NOT NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[PrimaryNm] [varchar](35) NOT NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impCustomer_Purge_pending] ADD  CONSTRAINT [DF_impCustomer_Purge_pending_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
