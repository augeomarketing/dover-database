USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[History_Stage_Deleted]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[History_Stage_Deleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History_Stage_Deleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
