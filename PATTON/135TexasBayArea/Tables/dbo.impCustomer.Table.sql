USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impCustomer]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[impCustomer] DROP CONSTRAINT [DF_impCustomer_DelinquentCycles]
GO
ALTER TABLE [dbo].[impCustomer] DROP CONSTRAINT [DF_impCustomer_DelinquentDays]
GO
DROP TABLE [dbo].[impCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impCustomer](
	[sid_impCustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[ExternalSts] [varchar](1) NULL,
	[InternalSts] [varchar](1) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
	[AssocNbr] [varchar](3) NULL,
	[CheckingNbr] [varchar](16) NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[TipPrefix] [varchar](3) NULL,
	[TipNumber] [varchar](15) NULL,
	[AddressLine6] [varchar](40) NULL,
	[RewardsNowSts] [varchar](1) NULL,
	[LastName] [varchar](40) NULL,
	[Misc3] [varchar](7) NULL,
	[DelinquentCycles] [varchar](2) NULL,
	[DelinquentDays] [varchar](3) NULL,
	[GroupID] [varchar](13) NULL,
	[RelationshipControl] [varchar](1) NULL,
	[DependentStrategy] [varchar](4) NULL,
 CONSTRAINT [PK__impCustomer] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impCustomer] ADD  CONSTRAINT [DF_impCustomer_DelinquentCycles]  DEFAULT ((0)) FOR [DelinquentCycles]
GO
ALTER TABLE [dbo].[impCustomer] ADD  CONSTRAINT [DF_impCustomer_DelinquentDays]  DEFAULT ((0)) FOR [DelinquentDays]
GO
