USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [varchar](15) NOT NULL,
	[PointsBegin] [int] NULL,
	[PointsEnd] [int] NULL,
	[PointsPurchasedCR] [int] NULL,
	[PointsPurchasedDB] [int] NULL,
	[PointsBonus] [int] NULL,
	[PointsAdded] [int] NULL,
	[PointsIncreased] [int] NULL,
	[PointsRedeemed] [int] NULL,
	[PointsReturnedCR] [int] NULL,
	[PointsReturnedDB] [int] NULL,
	[PointsSubtracted] [int] NULL,
	[PointsDecreased] [int] NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [int] NULL,
 CONSTRAINT [PK_Monthly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
