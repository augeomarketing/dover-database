USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Emails]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Emails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emails](
	[TipNumber] [char](20) NOT NULL,
	[Email] [varchar](50) NULL
) ON [PRIMARY]
GO
