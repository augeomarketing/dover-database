USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impDeletedCards]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[impDeletedCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impDeletedCards](
	[sid_impdeletedcards_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ccacctnbr] [varchar](16) NOT NULL,
	[primarynm] [varchar](35) NOT NULL,
	[primaryssn] [varchar](9) NOT NULL,
	[assocnbr] [varchar](3) NOT NULL,
	[checkingnbr] [varchar](16) NOT NULL,
	[creditunionnbr] [varchar](4) NOT NULL,
 CONSTRAINT [PK_impDeletedCards] PRIMARY KEY CLUSTERED 
(
	[sid_impdeletedcards_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
