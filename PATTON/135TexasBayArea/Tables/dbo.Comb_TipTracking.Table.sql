USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Comb_TipTracking]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Comb_TipTracking]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comb_TipTracking](
	[NewTIP] [char](15) NOT NULL,
	[OldTip] [char](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[OldTipPoints] [int] NULL,
	[OldTipRedeemed] [int] NULL,
	[OldTipRank] [char](1) NULL
) ON [PRIMARY]
GO
