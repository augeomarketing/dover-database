USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Affiliat_Stage_Deleted]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Affiliat_Stage_Deleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Affiliat_Stage_Deleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL,
	[DateDeleted] [datetime] NULL,
	[AffiliatFlag] [varchar](2) NULL
) ON [PRIMARY]
GO
