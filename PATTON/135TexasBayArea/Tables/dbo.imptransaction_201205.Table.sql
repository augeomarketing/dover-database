USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[imptransaction_201205]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[imptransaction_201205]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[imptransaction_201205](
	[sid_impTransaction_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NULL,
	[System] [varchar](4) NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [money] NOT NULL,
	[AccountSts] [varchar](1) NOT NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[TipNumber] [varchar](15) NULL,
	[Points] [bigint] NULL,
	[RNTranCode] [varchar](2) NULL,
	[AcctNbr] [varchar](15) NULL,
	[Misc3] [varchar](7) NULL,
	[MerchantSICCode] [varchar](4) NULL
) ON [PRIMARY]
GO
