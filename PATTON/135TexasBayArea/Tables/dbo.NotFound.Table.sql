USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[NotFound]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[NotFound]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotFound](
	[NAME] [nvarchar](255) NULL,
	[OldAGR] [nvarchar](255) NULL,
	[OldCCNum] [nvarchar](255) NULL,
	[NewAGR] [nvarchar](255) NULL,
	[Acctid] [nvarchar](255) NULL,
	[Expire] [smalldatetime] NULL,
	[Linked1] [float] NULL,
	[Link2] [float] NULL,
	[ADDRESS] [nvarchar](255) NULL,
	[CSZ] [nvarchar](255) NULL
) ON [PRIMARY]
GO
