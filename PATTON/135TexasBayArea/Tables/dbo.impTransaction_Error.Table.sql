USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impTransaction_Error]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[impTransaction_Error] DROP CONSTRAINT [DF_impTransaction_Error_TransactionAmt]
GO
ALTER TABLE [dbo].[impTransaction_Error] DROP CONSTRAINT [DF_impTransaction_Error_Points]
GO
DROP TABLE [dbo].[impTransaction_Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impTransaction_Error](
	[sid_impTransaction_Error_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NULL,
	[System] [varchar](4) NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [varchar](10) NULL,
	[AccountSts] [varchar](1) NULL,
	[TipNumber] [varchar](15) NULL,
	[Points] [bigint] NULL,
	[AcctNbr] [varchar](25) NULL,
 CONSTRAINT [PK__impTransaction_Error] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_Error_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impTransaction_Error] ADD  CONSTRAINT [DF_impTransaction_Error_TransactionAmt]  DEFAULT ((0.00)) FOR [TransactionAmt]
GO
ALTER TABLE [dbo].[impTransaction_Error] ADD  CONSTRAINT [DF_impTransaction_Error_Points]  DEFAULT ((0)) FOR [Points]
GO
