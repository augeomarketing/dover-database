USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[NewimpTransaction]    Script Date: 02/24/2014 16:10:04 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewimpTra__Trans__1B9317B3]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewimpTransaction]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewimpTra__Trans__1B9317B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewimpTransaction] DROP CONSTRAINT [DF__NewimpTra__Trans__1B9317B3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewimpTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[NewimpTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewimpTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NewimpTransaction](
	[sid_impTransaction_id] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionCount] [int] NULL,
	[TransactionAmt] [money] NOT NULL,
	[RNTranCode] [varchar](2) NULL,
 CONSTRAINT [PK_NewimpTransaction] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__NewimpTra__Trans__1B9317B3]') AND parent_object_id = OBJECT_ID(N'[dbo].[NewimpTransaction]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NewimpTra__Trans__1B9317B3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NewimpTransaction] ADD  DEFAULT ((0)) FOR [TransactionCount]
END


End
GO
