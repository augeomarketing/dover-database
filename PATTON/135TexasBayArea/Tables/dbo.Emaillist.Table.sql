USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Emaillist]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Emaillist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emaillist](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[Email] [varchar](50) NULL
) ON [PRIMARY]
GO
