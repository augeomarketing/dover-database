USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[AffiliatDeleted] DROP CONSTRAINT [DF__AffiliatD__YTDEa__7A9C383C]
GO
DROP TABLE [dbo].[AffiliatDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AffiliatDeleted] ADD  DEFAULT (0) FOR [YTDEarned]
GO
