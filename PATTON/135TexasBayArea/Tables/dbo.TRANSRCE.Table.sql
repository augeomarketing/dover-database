USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[TRANSRCE]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[TRANSRCE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRANSRCE](
	[ASSNUM] [nvarchar](3) NULL,
	[SYSTEM] [nvarchar](4) NULL,
	[PRIN] [nvarchar](4) NULL,
	[DATE1] [smalldatetime] NULL,
	[ACCTNO] [nvarchar](16) NULL,
	[TRANCODE] [nvarchar](3) NULL,
	[DISCRIPT] [nvarchar](50) NULL,
	[TRANAMT] [nvarchar](10) NULL,
	[STATUS] [nvarchar](1) NULL,
	[CUNUM] [nvarchar](4) NULL
) ON [PRIMARY]
GO
