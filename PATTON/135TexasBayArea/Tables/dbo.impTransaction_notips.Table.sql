USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impTransaction_notips]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[impTransaction_notips] DROP CONSTRAINT [DF_impTransactionnt_TransactionAmt]
GO
ALTER TABLE [dbo].[impTransaction_notips] DROP CONSTRAINT [DF__impTransa_nt_Point__164452B1]
GO
DROP TABLE [dbo].[impTransaction_notips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impTransaction_notips](
	[sid_impTransaction_notips_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NULL,
	[System] [varchar](4) NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [money] NOT NULL,
	[AccountSts] [varchar](1) NOT NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[TipNumber] [varchar](15) NULL,
	[Points] [bigint] NULL,
	[RNTranCode] [varchar](2) NULL,
	[AcctNbr] [varchar](15) NULL,
	[Misc3] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_notips_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impTransaction_notips] ADD  CONSTRAINT [DF_impTransactionnt_TransactionAmt]  DEFAULT ((0.00)) FOR [TransactionAmt]
GO
ALTER TABLE [dbo].[impTransaction_notips] ADD  CONSTRAINT [DF__impTransa_nt_Point__164452B1]  DEFAULT ((0)) FOR [Points]
GO
