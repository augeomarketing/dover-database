USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[wrkhist]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[wrkhist]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkhist](
	[tipnumber] [varchar](15) NOT NULL,
	[trancode] [varchar](2) NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
