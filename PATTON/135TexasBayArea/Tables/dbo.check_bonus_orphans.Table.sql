USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[check_bonus_orphans]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[check_bonus_orphans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[check_bonus_orphans](
	[MemberNum] [char](16) NOT NULL,
	[Points] [numeric](18, 0) NULL,
	[Reason] [char](15) NULL,
	[TipNumber] [char](15) NULL,
	[Trancode] [char](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
