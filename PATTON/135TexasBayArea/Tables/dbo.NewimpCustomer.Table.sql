USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[NewimpCustomer]    Script Date: 02/24/2014 16:10:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewimpCustomer]') AND type in (N'U'))
DROP TABLE [dbo].[NewimpCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NewimpCustomer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NewimpCustomer](
	[sid_impCustomer_id] [bigint] IDENTITY(1,1) NOT NULL,
	[CCAcctNbr] [varchar](16) NULL,
	[PrimaryNm] [varchar](40) NULL,
	[SecondaryNm] [varchar](40) NULL,
	[OldCCAcctNbr] [varchar](16) NULL,
	[AddressLine1] [varchar](40) NULL,
	[AddressLine2] [varchar](40) NULL,
	[City_State] [varchar](40) NULL,
	[StateCd] [varchar](2) NULL,
	[Zip] [varchar](15) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[PrimarySSN] [varchar](9) NULL,
 CONSTRAINT [PK__NewimpCustomer] PRIMARY KEY CLUSTERED 
(
	[sid_impCustomer_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
