USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Rollover]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Rollover]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rollover](
	[NAME] [nvarchar](255) NULL,
	[OldAGR] [nvarchar](255) NULL,
	[OldCCNum] [nvarchar](255) NOT NULL,
	[NewAGR] [nvarchar](255) NULL,
	[Acctid] [nvarchar](255) NULL,
	[Expire] [nvarchar](255) NULL,
	[Linked1] [nvarchar](255) NULL,
	[Link2] [nvarchar](255) NULL,
	[ADDRESS] [nvarchar](255) NULL,
	[CSZ] [nvarchar](255) NULL,
 CONSTRAINT [PK_Rollover] PRIMARY KEY CLUSTERED 
(
	[OldCCNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
