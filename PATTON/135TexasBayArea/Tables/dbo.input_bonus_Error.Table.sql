USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[input_bonus_Error]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[input_bonus_Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[input_bonus_Error](
	[MemberNum] [char](16) NOT NULL,
	[Points] [numeric](18, 0) NULL,
	[Reason] [char](15) NULL,
	[TipNumber] [char](15) NULL,
	[Trancode] [char](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
