USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__498EEC8D]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__4A8310C6]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__4B7734FF]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__4C6B5938]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__4D5F7D71]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__4E53A1AA]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__4F47C5E3]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__503BEA1C]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__51300E55]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__5224328E]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__531856C7]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__540C7B00]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Point__55009F39]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] DROP CONSTRAINT [DF__Quarterly__Curre__55F4C372]
GO
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__498EEC8D]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__4A8310C6]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__4B7734FF]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__4C6B5938]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__4D5F7D71]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__4E53A1AA]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__4F47C5E3]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__503BEA1C]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__51300E55]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__5224328E]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__531856C7]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__540C7B00]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__55009F39]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Curre__55F4C372]  DEFAULT (0) FOR [Currentend]
GO
