USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Addyfix]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Addyfix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addyfix](
	[ASSNUM] [nvarchar](3) NULL,
	[TFPRE] [nvarchar](3) NULL,
	[TFNUM] [nvarchar](7) NULL,
	[TIPNUMBER] [nvarchar](15) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[NAME1] [nvarchar](35) NULL,
	[NAME2] [nvarchar](35) NULL,
	[ESTATUS] [nvarchar](1) NULL,
	[ISTATUS] [nvarchar](1) NULL,
	[OLDCCNUM] [nvarchar](16) NULL,
	[ADDRESS1] [nvarchar](32) NULL,
	[ADDRESS2] [nvarchar](32) NULL,
	[ADDRESS6] [nvarchar](32) NULL,
	[CITY] [nvarchar](32) NULL,
	[STATE] [nvarchar](32) NULL,
	[ZIP] [nvarchar](5) NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[SSNUM] [nvarchar](9) NULL,
	[CYCLEMERCH] [nvarchar](12) NULL
) ON [PRIMARY]
GO
