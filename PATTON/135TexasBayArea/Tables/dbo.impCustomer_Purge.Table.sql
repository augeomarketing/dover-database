USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[impCustomer_Purge]    Script Date: 08/01/2012 10:10:12 ******/
ALTER TABLE [dbo].[impCustomer_Purge] DROP CONSTRAINT [DF_impCustomer_Purge_DateAdded]
GO
DROP TABLE [dbo].[impCustomer_Purge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impCustomer_Purge](
	[TipNumber] [varchar](15) NOT NULL,
	[CCAcctNbr] [varchar](25) NOT NULL,
	[PrimaryNm] [varchar](50) NOT NULL,
	[DateAdded] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impCustomer_Purge] ADD  CONSTRAINT [DF_impCustomer_Purge_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
