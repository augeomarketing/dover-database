USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[ACCTBP]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[ACCTBP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCTBP](
	[TIPNUMBER] [nvarchar](15) NULL,
	[TRANDATE] [smalldatetime] NULL,
	[ACCTNUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANCOUNT] [smallint] NULL,
	[TRANAMT] [float] NULL,
	[CARDTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[LASTACTIVE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
