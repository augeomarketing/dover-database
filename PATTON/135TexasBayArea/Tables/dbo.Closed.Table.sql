USE [135TexasBayArea]
GO
/****** Object:  Table [dbo].[Closed]    Script Date: 08/01/2012 10:10:12 ******/
DROP TABLE [dbo].[Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Closed](
	[NAME] [nvarchar](255) NULL,
	[OldAGR] [float] NULL,
	[OldCCNum] [nvarchar](255) NULL,
	[NewAGR] [float] NULL,
	[Acctid] [varchar](16) NULL,
	[Expire] [smalldatetime] NULL,
	[Linked1] [float] NULL,
	[Link2] [float] NULL,
	[ADDRESS] [nvarchar](255) NULL,
	[CSZ] [nvarchar](255) NULL
) ON [PRIMARY]
GO
