USE [135TexasBayArea]
GO
/****** Object:  UserDefinedFunction [dbo].[fnLinkCheckingNames]    Script Date: 08/01/2012 10:10:13 ******/
DROP FUNCTION [dbo].[fnLinkCheckingNames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnLinkCheckingNames] (@Checking nvarchar(25), @assocnbr nvarchar(3))

RETURNS @LinkedNames table(assocnbr nvarchar(3), Checking nvarchar(25), Name1 nvarchar(35), PrimarySSN nvarchar(9), Name2 nvarchar(35), 
						Name3 nvarchar(35), Name4 nvarchar(35), Name5 nvarchar(35),
						Name6 nvarchar(35), primary key([checking], [assocnbr]) )

as

BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(35), PrimarySSN nvarchar(9))

	insert into @names
	select distinct PrimaryNm, PrimarySSN
	from dbo.impcustomer
	where CheckingNbr = @Checking
	and assocnbr = @assocnbr
--	and isnull(primaryssn, '') != ''
--
--	-- Get distinct names from NAME1 column for rows that match the @Checking parm
--	insert into @names
--	select distinct SecondaryNm, PrimarySSN
--	from dbo.impCustomer
--	where CheckingNbr = @Checking and
--		SecondaryNm not in (select AcctName from @Names)



	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
	select	@assocnbr as AssocNbr, @checking as Checking, (select AcctName from @names where id=1) as Name1, (Select PrimarySSN from @names where id=1) as PrimarySSN,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END
GO
