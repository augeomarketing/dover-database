USE [271]
GO
/****** Object:  StoredProcedure [dbo].[usp_MthStmtFile_Extract]    Script Date: 09/18/2014 09:47:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MthStmtFile_Extract]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MthStmtFile_Extract]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MthStmtFile_Extract]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Create statement file that must be exported to the FI
-- =============================================
CREATE PROCEDURE [dbo].[usp_MthStmtFile_Extract]
	-- Add the parameters for the stored procedure here
	@MonthBeginningDate datetime
	, @MonthEndingDate datetime

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @StartdateOut varchar(8), @EnddateOut varchar(8)
	
    -- Insert statements for procedure here
	set @StartdateOut = convert(varchar(8),@MonthBeginningDate,112)
	set @EnddateOut = convert(varchar(8),@MonthEndingDate,112)

	truncate table wrkstatement

	insert into wrkstatement (tipnumber, ccdacct, startdate, enddate)
	select tipnumber, acctid, @StartdateOut, @EnddateOut
	from AFFILIAT_Stage
	where AcctType=''MEMBER''
		and AcctStatus=''A''
	order by TIPNUMBER

	update wrkstatement
	set begbalance = abs(msf.PointsBegin)
			, ptsearned = abs((msf.PointsPurchasedCR + msf.PointsPurchasedDB - msf.PointsReturnedCR - msf.PointsReturnedDB))
			, ptsadjust = abs((msf.PointsAdded + msf.PurchasedPoints + msf.PointsBonusCR + msf.PointsBonusDB + msf.PointsBonusMER - msf.PointsSubtracted ))
			, ptsredeem = abs(msf.PointsRedeemed)
			, endbalance = abs(msf.PointsEnd)
	from wrkstatement wks join Monthly_Statement_File msf on wks.tipnumber=msf.Tipnumber

	update wrkstatement
	set begbsign = case when msf.PointsBegin<0 then ''-'' else ''+'' end
		, ptsesign = case when (msf.PointsPurchasedCR + msf.PointsPurchasedDB - msf.PointsReturnedCR - msf.PointsReturnedDB)<0 then ''-'' else ''+'' end
		, ptsasign = case when (msf.PointsAdded + msf.PurchasedPoints + msf.PointsBonusCR + msf.PointsBonusDB + msf.PointsBonusMER - msf.PointsSubtracted )<0 then ''-'' else ''+'' end
		, ptsrsign = case when msf.PointsRedeemed <0 then ''+'' else ''-'' end
		, endbsign = case when msf.PointsEnd<0 then ''-'' else ''+'' end
	from wrkstatement wks join Monthly_Statement_File msf on wks.tipnumber=msf.Tipnumber

END
' 
END
GO
