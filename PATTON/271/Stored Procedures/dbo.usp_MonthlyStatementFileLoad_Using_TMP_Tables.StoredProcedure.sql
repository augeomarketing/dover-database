USE [271]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad_Using_TMP_Tables]    Script Date: 11/28/2016 14:31:33 ******/
DROP PROCEDURE [dbo].[usp_MonthlyStatementFileLoad_Using_TMP_Tables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad_Using_TMP_Tables]  
       @tipfirst               varchar(3),
        @StartDateParm          datetime, 
        @EndDateParm            datetime
AS
--declare        @tipfirst               varchar(3),
--        @StartDateParm          datetime, 
--        @EndDateParm            datetime
--set @tipfirst='641'
--set @StartDateParm='12/01/2010'
--set @EndDateParm='12/31/2010'

Declare     @MonthBegin         char(2)
Declare     @StartDate          DateTime
Declare     @EndDate            DateTime

declare     @dbname             nvarchar(50)
declare     @sql                nvarchar(max)

declare		@Trancode			varchar(2)
declare		@Ratio				int
declare		@tcdesc				varchar(40)
declare		@nbrrows			int = 0
declare		@ctr				int = 1
declare		@SQLDynamic			nvarchar(max)

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 


set @dbname = (select dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)
--print @dbname
set @MonthBegin = month(@StartDate)

declare @TMP_Trancode varchar(max)='Trancode_' + convert(varchar(36),NEWID())
declare @TMP_Redemptions varchar(max)='Redemption_' + convert(varchar(36),NEWID())
declare @TMP_BONUS varchar(max)='Bonus_' + convert(varchar(36),NEWID())
declare @TMP_BEGIN varchar(max)='Begin_' + convert(varchar(36),NEWID())


/* Create temp of all trancodes */

set @SQLDynamic = N'Select tipnumber, trancode, sum(points) as TranCodePoints 
			into [##' + @TMP_Trancode + ']
			from ' + QUOTENAME(@dbname) + '.dbo.HISTORY
			where histdate between '''+convert( char(23), @StartDate,21 ) +''' and '''+ convert(char(23),@EndDate,21) +''' group by tipnumber, trancode
		'
exec sp_executesql @SQLDynamic
--print 'ok'
/* Create temp for Bonus */
set @SQLDynamic = N'Select tipnumber, sum(TranCodePoints) as BonusPoints 
		into [##' + @TMP_BONUS + ']
		from [##' + @TMP_Trancode + ']
		where trancode like ''B%'' or (trancode like ''F%'' and trancode not in (''F0'',''F9'',''G0'',''G9'',''H0'',''H9'')) group by tipnumber
 '
exec sp_executesql @SQLDynamic
--print 'ok'
/* Create temp for Redemptions */

set @SQLDynamic = N'
		Select tipnumber, sum(TranCodePoints) as RedemptionPoints 
		into [##' + @TMP_Redemptions + ']
		from [##' + @TMP_Trancode + '] 
		where trancode like ''R%'' or trancode=''IR''group by tipnumber
	'
exec sp_executesql @SQLDynamic


/* Load the statement file from the customer table  */
set @SQLDynamic = N'
		delete from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File
			 '
exec sp_executesql @SQLDynamic

set @SQLDynamic =N'
			insert into ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File
            (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
            select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
            from ' + QUOTENAME(@dbname) + '.dbo.customer'
exec sp_executesql @SQLDynamic
    
--set @SQLDynamic ='update ' + QUOTENAME(@dbname) + 'dbo.Monthly_Statement_File
--			set Acctnum = RIGHT(RTRIM(af.acctid),4)
--			from dbo.Monthly_Statement_File msf inner join dbo.affiliat af
--					on msf.tipnumber = af.tipnumber
--			where af.accttype=''DEFAULT'' '    
--exec sp_executesql @SQLDynamic

/* Load the statmement file with CREDIT purchases          */
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointspurchasedCR 
				= isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in (''61'') '
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointspurchasedCR 
				= pointspurchasedCR + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in (''63'') '
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointspurchasedCR 
				= pointspurchasedCR + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in (''32'') '
	exec sp_executesql @SQLDynamic

/* Load the statmement file with DEBIT purchases          */
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointspurchasedDB = isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + ']  tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in (''65'') '
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointspurchasedDB = pointspurchasedDB + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + ']  tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in (''67'') '
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointspurchasedDB =  pointspurchasedDB + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + ']  tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in (''36'') '
	exec sp_executesql @SQLDynamic

/* Load the statmement file with Purchased Points */
set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
			set PurchasedPoints 
			= isnull(tmp.TranCodePoints,0) 
			from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
			where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
			 tmp.trancode =''PP''
			 '
exec sp_executesql @SQLDynamic

/* Load the statmement file with plus adjustments */
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsadded 
					= isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in (''IE'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsadded 
					= pointsadded + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in (''DR'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsadded 
					= pointsadded + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in (''GP'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsadded 
					= pointsadded + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in (''TP'')
					'
	exec sp_executesql @SQLDynamic

/* Load the statmement file with Merchant Bonus */
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set PointsBonusMER 
					= isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in(''F0'')
				'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set PointsBonusMER 
					= PointsBonusMER + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in(''G0'')
				'
	exec sp_executesql @SQLDynamic
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set PointsBonusMER 
					= PointsBonusMER + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					 tmp.trancode in(''H0'')
				'
	exec sp_executesql @SQLDynamic

/* Load the statmement file with bonuses            */
set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointsbonusDB 
				= isnull(tmp.BonusPoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_BONUS + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber 
				'
exec sp_executesql @SQLDynamic

/* Load the statmement file with total point increases */
set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonusDB + pointsadded + pointsbonusMER + PurchasedPoints' 
exec sp_executesql @SQLDynamic


/* Load the statmement file CREDIT with returns            */
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsreturnedCR
					= isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					tmp.trancode in (''33'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsreturnedCR
					= pointsreturnedCR + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					tmp.trancode in (''31'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsreturnedCR
					= pointsreturnedCR + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					tmp.trancode in (''62'')
					'
	exec sp_executesql @SQLDynamic
				
/* Load the statmement file DEBIT with returns            */
	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsreturnedDB
					= isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					tmp.trancode in (''37'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsreturnedDB
					= pointsreturnedDB + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					tmp.trancode in (''35'')
					'
	exec sp_executesql @SQLDynamic

	set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
					set pointsreturnedDB
					= pointsreturnedDB + isnull(tmp.TranCodePoints,0) 
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
					where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
					tmp.trancode in (''66'')
					'
	exec sp_executesql @SQLDynamic
 
/* Load the statmement file with minus adjustments    */
set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointssubtracted
				= isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in(''DE'')
				'
 exec sp_executesql @SQLDynamic

set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointssubtracted
				= pointssubtracted + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in(''EP'')
				'
 exec sp_executesql @SQLDynamic

set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointssubtracted
				= pointssubtracted + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in(''XP'')
				'
 exec sp_executesql @SQLDynamic

set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointssubtracted
				= pointssubtracted + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in(''F9'')
				'
 exec sp_executesql @SQLDynamic

set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointssubtracted
				= pointssubtracted + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in(''H9'')
				'
 exec sp_executesql @SQLDynamic

set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointssubtracted
				= pointssubtracted + isnull(tmp.TranCodePoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Trancode + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber and 
				tmp.trancode in(''G9'')
				'
 exec sp_executesql @SQLDynamic
   
/* Load the statmement file with redemptions          */
set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointsredeemed 
				= isnull(tmp.RedemptionPoints,0) 
				from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File, [##' + @TMP_Redemptions + '] tmp
				where  tmp.tipnumber = Monthly_Statement_File.tipnumber 
				'
 exec sp_executesql @SQLDynamic
				
/* Load the statmement file with total point decreases */
set @SQLDynamic =N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File 
				set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
				'  
exec sp_executesql @SQLDynamic

/* Create View for Beginning Balance */
set @SQLDynamic = N'Select tipnumber, sum(points*ratio) as BeginPoints
					into [##' + @TMP_BEGIN + ']
					from ' + QUOTENAME(@dbname) + '.dbo.history 
					where histdate < '''+ convert( char(23), @StartDate,21 )  + ''' group by tipnumber'
exec sp_executesql @SQLDynamic

/* Load the statmement file with the Beginning balance for the Month */
set @SQLDynamic = N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File
					set PointsBegin = isnull(tmp.BeginPoints,0)
					from ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File msf join [##' + @TMP_BEGIN + '] tmp on msf.Tipnumber=tmp.TipNumber
				'
exec sp_executesql @SQLDynamic
				
/* Load the statmement file with beginning points */
set @SQLDynamic = N'update ' + QUOTENAME(@dbname) + '.dbo.Monthly_Statement_File
				set pointsend=pointsbegin + pointsincreased - pointsdecreased 
				'
exec sp_executesql @SQLDynamic

exec ('DROP TABLE [##' + @TMP_Trancode + ']')
exec ('DROP TABLE [##' + @TMP_Redemptions + ']')
exec ('DROP TABLE [##' + @TMP_BONUS + ']')
exec ('DROP TABLE [##' + @TMP_BEGIN + ']')
GO
