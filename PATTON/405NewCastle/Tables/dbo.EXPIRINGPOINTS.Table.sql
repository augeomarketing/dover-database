USE [405NewCastle]
GO
/****** Object:  Table [dbo].[EXPIRINGPOINTS]    Script Date: 06/01/2010 16:48:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EXPIRINGPOINTS](
	[tipnumber] [varchar](15) NOT NULL,
	[CredAddPoints] [float] NULL,
	[DebAddpoints] [float] NULL,
	[CredRetPoints] [float] NULL,
	[DebRetPoints] [float] NULL,
	[CredAddPointsNext] [float] NULL,
	[DebAddPointsNext] [float] NULL,
	[CredRetPointsNext] [float] NULL,
	[DebRetPointsNext] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateofExpire] [nvarchar](25) NULL,
	[PointsToExpireNext] [float] NULL,
	[AddPoints] [float] NULL,
	[ADDPOINTSNEXT] [float] NULL,
	[PointsOther] [float] NULL,
	[PointsOthernext] [float] NULL,
	[EXPTODATE] [float] NULL,
	[ExpiredRefunded] [float] NULL,
	[dbnameonnexl] [nvarchar](50) NULL,
	[CredaddpointsPLUS3MO] [float] NULL,
	[DEbaddpointsPLUS3MO] [float] NULL,
	[CredRetpointsPLUS3MO] [float] NULL,
	[DEbRetpointsPLUS3MO] [float] NULL,
	[POINTSTOEXPIREPLUS3MO] [float] NULL,
	[ADDPOINTSPLUS3MO] [float] NULL,
	[PointsOtherPLUS3MO] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_CredAddPoints]  DEFAULT ((0)) FOR [CredAddPoints]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DebAddpoints]  DEFAULT ((0)) FOR [DebAddpoints]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_CredRetPoints]  DEFAULT ((0)) FOR [CredRetPoints]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DebRetPoints]  DEFAULT ((0)) FOR [DebRetPoints]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_CredAddPointsNext]  DEFAULT ((0)) FOR [CredAddPointsNext]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DebAddPointsNext]  DEFAULT ((0)) FOR [DebAddPointsNext]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_CredRetPointsNext]  DEFAULT ((0)) FOR [CredRetPointsNext]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DebRetPointsNext]  DEFAULT ((0)) FOR [DebRetPointsNext]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_REDPOINTS]  DEFAULT ((0)) FOR [REDPOINTS]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_POINTSTOEXPIRE]  DEFAULT ((0)) FOR [POINTSTOEXPIRE]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_PREVEXPIRED]  DEFAULT ((0)) FOR [PREVEXPIRED]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DateofExpire]  DEFAULT ((0)) FOR [DateofExpire]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_PointsToExpireNext]  DEFAULT ((0)) FOR [PointsToExpireNext]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_AddPoints]  DEFAULT ((0)) FOR [AddPoints]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_ADDPOINTSNEXT]  DEFAULT ((0)) FOR [ADDPOINTSNEXT]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_PointsOther]  DEFAULT ((0)) FOR [PointsOther]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_PointsOthernext]  DEFAULT ((0)) FOR [PointsOthernext]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_ExpiredRefunded]  DEFAULT ((0)) FOR [ExpiredRefunded]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_CredaddpointsPLUS3MO]  DEFAULT ((0)) FOR [CredaddpointsPLUS3MO]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DEbaddpointsPLUS3MO]  DEFAULT ((0)) FOR [DEbaddpointsPLUS3MO]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_CredRetpointsPLUS3MO]  DEFAULT ((0)) FOR [CredRetpointsPLUS3MO]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_DEbRetpointsPLUS3MO]  DEFAULT ((0)) FOR [DEbRetpointsPLUS3MO]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_POINTSTOEXPIREPLUS3MO]  DEFAULT ((0)) FOR [POINTSTOEXPIREPLUS3MO]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_ADDPOINTSPLUS3MO]  DEFAULT ((0)) FOR [ADDPOINTSPLUS3MO]
GO
ALTER TABLE [dbo].[EXPIRINGPOINTS] ADD  CONSTRAINT [DF_EXPIRINGPOINTS_PointsOtherPLUS3MO]  DEFAULT ((0)) FOR [PointsOtherPLUS3MO]
GO
