USE [405NewCastle]
GO
/****** Object:  Table [dbo].[GenTip]    Script Date: 06/01/2010 16:48:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[custid] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
