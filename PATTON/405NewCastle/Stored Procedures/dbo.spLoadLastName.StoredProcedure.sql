USE [405NewCastle]
GO
/****** Object:  StoredProcedure [dbo].[spLoadLastName]    Script Date: 06/01/2010 16:47:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dan Foster>
-- Create date: <12.15.2009>
-- Description:	<Load Last Name to Input_Customer>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadLastName]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Insert newlastname (acctid,tipnumber, acctname1)
	select acctid,tipnumber, acctname1 from roll_customer
		
	update newlastname set reversedname = reverse(rtrim(ltrim(acctname1)))
		
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastspace >= 0
	
	update newlastname set reversedname = ltrim(reverse(left(acctname1, len(acctname1) -3)))
	where lastname in ('JR','SR','II','IV','ND')
	
	update newlastname set reversedname = ltrim(reverse(left(acctname1, len(acctname1) -4)))
	where lastname in ('III')
	 
	update newlastname set lastspace = CHARINDEX(' ', reversedname) - 1 
	where lastspace <= 3
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastname in ('JR','SR','II','IV','ND') and lastspace > 0
	
	update newlastname set lastname = acctname1 where lastname IN ('INC','CO','CTR',
		   'LTD','CT','CH','INC','LLC','LLP','ASSOC')
	
	update newlastname set lastname = reverse(reversedname) where lastspace <= 1
	
	/*  Load lastname into Input_Customer Table */
	
	update roll_customer set lastname = n.lastname 
	from newlastname n join roll_customer c on c.acctid = n.acctid

END
GO
