USE [405NewCastle]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionSummary]    Script Date: 06/01/2010 16:47:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spTransactionSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table TransactionSummary

insert TransactionSummary (dateadded)
values (@enddate)

update  TransactionSummary set purchases = (select SUM(CONVERT(int, points)) FROM transactions WHERE trancode = '63'  and dateadded = @enddate)

update   TransactionSummary set returned =(select SUM(CONVERT(int,points)) FROM transactions WHERE trancode = '33' and dateadded =  @enddate)
GO
