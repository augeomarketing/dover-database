USE [405NewCastle]
GO
/****** Object:  StoredProcedure [dbo].[sp405GenTIPNumbersStage]    Script Date: 06/01/2010 16:47:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp405GenTIPNumbersStage]
AS 


update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.acctid = b.acctid 

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 405, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 405000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 405, @newnum

update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
