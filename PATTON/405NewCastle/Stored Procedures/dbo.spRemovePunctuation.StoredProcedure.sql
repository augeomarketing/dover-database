USE [405NewCastle]
GO
/****** Object:  StoredProcedure [dbo].[spRemovePunctuation]    Script Date: 06/01/2010 16:47:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRemovePunctuation]
 AS

update customerwork
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), addr1=replace(addr1,char(39), ' '),
addr4=replace(addr4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update customerwork
set acctname1=replace(acctname1,char(140), ' '),acctname2=replace(acctname2,char(140), ' '), addr1=replace(addr1,char(140), ' '),
addr4=replace(addr4,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')

update customerwork
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), addr1=replace(addr1,char(44), ' '),
addr4=replace(addr4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update customerwork
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), addr1=replace(addr1,char(46), ' '),
addr4=replace(addr4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')
GO
