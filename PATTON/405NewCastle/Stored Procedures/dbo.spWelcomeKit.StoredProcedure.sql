USE [405NewCastle]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 06/01/2010 16:47:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10)
AS 

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)
--declare @EndDate varchar(10)
--set @EndDate = '12/31/2009'

set @DBName= '405NewCastle'  --(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5) FROM ' + QuoteName(@DBName) + N' .dbo.customer WHERE (DATEADDED = @EndDate AND STATUS <> ''C'') '
exec sp_executesql @SQLInsert, N'@Enddate char(10)',@Enddate=@Enddate
GO
