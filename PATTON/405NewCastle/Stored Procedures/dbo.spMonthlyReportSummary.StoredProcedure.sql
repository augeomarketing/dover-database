USE [405NewCastle]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyReportSummary]    Script Date: 06/01/2010 16:47:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spMonthlyReportSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table MonthlySummary

insert MonthlySummary (dateadded)
values (@enddate)

update  MonthlySummary set purchases = (select SUM(CONVERT(int, pointspurchased)) FROM monthly_statement_file)

update   MonthlySummary set returned =(select SUM(CONVERT(int, pointsreturned)) FROM monthly_statement_file)

update MonthlySummary set bonuses = (select sum(convert(int,pointsbonus)) from  monthly_statement_file)

update BegBalanceCheck set newbegbalance = (select sum(convert(int,pointsbegin)) from monthly_statement_file)
GO
