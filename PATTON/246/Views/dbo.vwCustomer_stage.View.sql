USE [246]
GO
/****** Object:  View [dbo].[vwCustomer_stage]    Script Date: 01/27/2012 10:43:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomer_stage]
			 as
			 Select *
			  from [219DeanBank].dbo.Customer_stage
'
GO
