USE [246]
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 01/27/2012 10:43:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomerdeleted]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [219DeanBank].dbo.Customerdeleted
'
GO
