USE [246]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 01/27/2012 10:43:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomer]
			 as
			 select *
			 from [219DeanBank].dbo.customer
'
GO
