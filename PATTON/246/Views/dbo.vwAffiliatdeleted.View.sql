USE [246]
GO
/****** Object:  View [dbo].[vwAffiliatdeleted]    Script Date: 01/27/2012 10:43:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAffiliatdeleted]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwAffiliatdeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID, datedeleted
			  from [219DeanBank].dbo.affiliatdeleted
'
GO
