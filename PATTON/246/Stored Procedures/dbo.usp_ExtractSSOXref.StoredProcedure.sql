use [246]
GO

if object_id('usp_ExtractSSOXref') is not null
	drop procedure dbo.usp_ExtractSSOXref
GO

create procedure dbo.usp_ExtractSSOXref
		@tipfirst		varchar(3)

AS

	declare @dbname varchar(50)
	declare @pointsupdateddt date 
	declare @sql	nvarchar(max)

	select	@pointsupdateddt = isnull(pointsupdated, datejoined),
			@dbname = quotename(dbnamepatton)
	from rewardsnow.dbo.dbprocessinfo 
	where dbnumber = @tipfirst


	set @sql = '
				select aff.tipnumber, runavailable, acctid, @pointsupdateddt, acctname1
				from ' + @dbname + '.dbo.affiliat aff join ' + @dbname + '.dbo.customer cus
					on aff.tipnumber = cus.tipnumber
				where aff.accttype = ''CREDIT''
				order by tipnumber'
				
	exec sp_executesql @sql, N'@pointsupdateddt date', @pointsupdateddt = @pointsupdateddt

/*  test harness

exec dbo.usp_ExtractSSOXref '246'

*/