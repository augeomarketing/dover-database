USE [246]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 04/30/2012 15:35:51 ******/
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
