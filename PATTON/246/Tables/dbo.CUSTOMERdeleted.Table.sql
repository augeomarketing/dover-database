USE [246]
GO
/****** Object:  Table [dbo].[CUSTOMERdeleted]    Script Date: 04/30/2012 15:35:51 ******/
DROP TABLE [dbo].[CUSTOMERdeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMERdeleted](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CUSTOMERdeleted] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
