USE [246]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 04/30/2012 15:35:51 ******/
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [FK_TransStandard_TranType]
GO
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [DF_TransStandard_TranNum]
GO
ALTER TABLE [dbo].[TransStandard] DROP CONSTRAINT [DF_TransStandard_TranAmt]
GO
DROP TABLE [dbo].[TransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransStandard](
	[sid_transstandard_id] [bigint] IDENTITY(1,1) NOT NULL,
	[TIPNumber] [varchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NOT NULL,
	[TranNum] [int] NOT NULL,
	[TranAmt] [decimal](18, 0) NOT NULL,
	[TranType] [varchar](40) NOT NULL,
	[Ratio] [int] NOT NULL,
	[CrdActvlDt] [date] NULL,
 CONSTRAINT [PK_TransStandard_1] PRIMARY KEY CLUSTERED 
(
	[sid_transstandard_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_CUSTOMER_Stage] FOREIGN KEY([TIPNumber])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO
ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_TranType]
GO
ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranNum]  DEFAULT ((1)) FOR [TranNum]
GO
ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranAmt]  DEFAULT ((0)) FOR [TranAmt]
GO
