USE [252]
GO

/****** Object:  StoredProcedure [dbo].[usp_GenerateMonthlyAuditFile]    Script Date: 05/15/2013 12:36:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GenerateMonthlyAuditFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GenerateMonthlyAuditFile]
GO

USE [252]
GO

/****** Object:  StoredProcedure [dbo].[usp_GenerateMonthlyAuditFile]    Script Date: 05/15/2013 12:36:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
CREATE PROCEDURE [dbo].[usp_GenerateMonthlyAuditFile]    
 @endDate DATE    
AS    
BEGIN    
 DELETE FROM MonthlyStatementFile    
    
 DECLARE @startDate DATE     
    
 SET @startDate = Rewardsnow.dbo.ufn_GetFirstOfMonth(@endDate)    
    
 INSERT INTO MonthlyStatementFile    
 (    
  TIPNUMBER, ACCTNAME1, ACCTNAME2, ADDRESS1, ADDRESS2, ADDRESS3    
  , CityStateZip, TOT_BEGIN, TOT_END, TOT_ADD, TOT_SUBTRACT    
  , TOT_NET, TOT_BONUS, TOT_REDEEM, TOT_ToExpire, CRD_PURCH    
  , CRD_RETURN, DBT_PURCH, DBT_RETURN, ADJ_ADD, ADJ_SUBTRACT    
  , POINTSBONUSMN, STATMENTDATE, ACCTID, CARDNUM    
 )    
 SELECT     
  CST.TIPNUMBER    
  , COALESCE (CSTSTG.ACCTNAME1, CSTPRD.ACCTNAME1, '') AS ACCTNAME1    
  , COALESCE (CSTSTG.ACCTNAME2, CSTPRD.ACCTNAME2, '') AS ACCTNAME2    
  , COALESCE (CSTSTG.ADDRESS1, CSTPRD.ADDRESS1, '') AS ADDRESS1    
  , COALESCE (CSTSTG.ADDRESS2, CSTPRD.ADDRESS2, '') AS ADDRESS2    
  , COALESCE (CSTSTG.ADDRESS3, CSTPRD.ADDRESS3, '') AS ADDRESS3    
  , LTRIM(RTRIM(COALESCE(CSTSTG.City, CSTPRD.City, '')))     
   + ' ' + LTRIM(RTRIM(COALESCE(CSTSTG.State, CSTPRD.State, '')))     
   + ' ' + LTRIM(RTRIM(COALESCE(CSTSTG.ZipCode, CSTPRD.ZIPCODE))) AS CityStateZip    
  , ISNULL(BB.Balance, 0) AS TOT_BEGIN    
  , (ISNULL(BB.Balance, 0) + ISNULL(TOT_ADD, 0)) - ABS(ISNULL(TOT_SUBTRACT, 0)) AS TOT_END    
  , ISNULL(TOT_ADD, 0) AS TOT_ADD    
  , ABS(ISNULL(TOT_SUBTRACT, 0)) AS TOT_SUBTRACT    
  , ISNULL(TOT_ADD, 0) - ABS(ISNULL(TOT_SUBTRACT, 0)) AS TOT_NET    
  , ISNULL(TOT_BONUS, 0) AS TOT_BONUS    
  , ABS(ISNULL(TOT_REDEEM, 0)) AS TOT_REDEEM    
  , ISNULL(TOT_ToExpire, 0) AS TOT_ToExpire    
  , ISNULL(CRD_PURCH, 0) AS CRD_PURCH    
  , ABS(ISNULL(CRD_RETURN, 0)) AS CRD_RETURN    
  , ISNULL(DBT_PURCH, 0) AS DBT_PURCH    
  , ABS(ISNULL(DBT_RETURN, 0)) AS DBT_RETURN    
  , ISNULL(ADJ_ADD, 0) AS ADJ_ADD    
  , ABS(ISNULL(ADJ_SUBTRACT, 0)) AS ADJ_SUBTRACT    
  , ISNULL(POINTSBONUSMN, 0) AS POINTSBONUSMN    
  , @endDate AS STATMENTDATE    
  , ACCT.acctid AS ACCTID    
  , cl.CardList AS CARDNUM    
      
 FROM    
    
 (SELECT TIPNUMBER FROM CUSTOMER    
  UNION SELECT TIPNUMBER FROM CUSTOMER_STAGE) CST    
      
 LEFT OUTER JOIN CUSTOMER CSTPRD    
 ON CST.TIPNUMBER = CSTPRD.TIPNUMBER    
    
 LEFT OUTER JOIN CUSTOMER_Stage CSTSTG    
 ON CST.TIPNUMBER = CSTSTG.TIPNUMBER    
      
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) TOT_ADD    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'POINTINCREASE'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) TOTADD    
 ON CST.TIPNUMBER = TOTADD.TIPNUMBER    
    
 LEFT OUTER JOIN     
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) TOT_SUBTRACT    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'POINTDECREASE'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) TOTSUBT    
 ON CST.TIPNUMBER = TOTSUBT.TIPNUMBER    
 LEFT OUTER JOIN     
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) TOT_BONUS    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name IN ('INCREASE_BONUS_NOSF', 'INCREASE_2PT', 'INCREASE_3PT')
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) TOTBNS    
 ON CST.TIPNUMBER = TOTBNS.TIPNUMBER    
 LEFT OUTER JOIN     
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) POINTSBONUSMN    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'INCREASE_BONUS_SHOPPINGFLING'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) TOTSF    
 ON CST.TIPNUMBER = TOTSF.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) TOT_REDEEM    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    where dim_rnitrancodegroup_name = 'DECREASE_REDEMPTION'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) TOTREDM    
 ON CST.TIPNUMBER = TOTREDM.TIPNUMBER    
 LEFT OUTER JOIN     
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) CRD_PURCH    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'INCREASE_PURCHASE_CREDIT'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) CRDPURCH    
 ON CST.TIPNUMBER = CRDPURCH.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) CRD_RETURN    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'DECREASE_RETURN_CREDIT'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) CRDRET    
 ON CST.TIPNUMBER = CRDRET.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) DBT_PURCH    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'INCREASE_PURCHASE_DEBIT'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) DBTPURCH    
 ON CST.TIPNUMBER = DBTPURCH.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) DBT_RETURN    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'DECREASE_RETURN_DEBIT'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) DBTRET    
 ON CST.TIPNUMBER = DBTRET.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) ADJ_ADD    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'INCREASE_ADJUSTMENT'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) ADJADD    
 ON CST.TIPNUMBER = ADJADD.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT TIPNUMBER, SUM(HST.Points * HST.RATIO) ADJ_SUBTRACT    
  FROM HISTORY_Stage HST    
  INNER JOIN Rewardsnow.dbo.vw_RNITrancodeMap TCM    
  ON HST.TRANCODE = TCM.sid_trantype_trancode    
  where dim_rnitrancodegroup_name = 'DECREASE_ADJUSTMENT'    
  AND CONVERT(DATE, HST.HISTDATE) BETWEEN @startDate AND @endDate    
  GROUP BY TIPNUMBER    
 ) ADJSUBT    
 ON CST.TIPNUMBER = ADJSUBT.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  select prm.dim_rnicustomer_rniid AS TIPNUMBER, rnic.dim_RNICustomer_Member as acctid     
  from Rewardsnow.dbo.ufn_RNICustomerPrimarySIDsForTip('252') prm    
  inner join rewardsnow.dbo.rnicustomer rnic    
  on rnic.sid_rnicustomer_id = prm.sid_rnicustomer_id    
 ) ACCT    
 ON CST.TIPNUMBER = ACCT.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT sid_ExpiringPointsProjection_Tipnumber as TIPNUMBER, dim_ExpiringPointsProjection_PointsToExpireThisPeriod as TOT_ToExpire     
  FROM Rewardsnow.dbo.ExpiringPointsProjection     
  WHERE sid_ExpiringPointsProjection_Tipnumber LIKE '252%'    
 ) TOTEXP    
 ON CST.TIPNUMBER = TOTEXP.TIPNUMBER    
 LEFT OUTER JOIN    
 (    
  SELECT tipnumber, SUM(points * ratio) Balance  
  FROM HISTORY  
  WHERE convert(date, HISTDATE) < @startDate  
  GROUP BY TIPNUMBER   
 ) BB    
 ON CST.TIPNUMBER = BB.tipnumber    
 LEFT OUTER JOIN RewardsNow.dbo.ufn_RNICustomerGetActiveCardListForTipfirst('252') cl
 ON CST.TIPNUMBER = cl.dim_rnicustomer_rniid
    
 INNER JOIN Rewardsnow.dbo.ufn_RNICustomerPrimarySIDsForTip('252') prim    
 ON CST.TIPNUMBER = prim.dim_rnicustomer_rniid    
     
 INNER JOIN Rewardsnow.dbo.RNICustomer rnic    
 ON prim.sid_rnicustomer_id = rnic.sid_RNICustomer_ID    
    
 ORDER BY TIPNUMBER    
END    

GO


