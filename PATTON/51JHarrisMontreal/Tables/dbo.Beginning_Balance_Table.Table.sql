USE [51JHarrisMontreal]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 01/12/2010 09:14:46 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__30F848ED]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__30F848ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__30F848ED]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__31EC6D26]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__31EC6D26]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__31EC6D26]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__32E0915F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__32E0915F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__32E0915F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__33D4B598]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__33D4B598]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__33D4B598]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__34C8D9D1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__34C8D9D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__34C8D9D1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__35BCFE0A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__35BCFE0A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__35BCFE0A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__36B12243]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__36B12243]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__36B12243]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__37A5467C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__37A5467C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__37A5467C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__38996AB5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__38996AB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__38996AB5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__398D8EEE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__398D8EEE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__398D8EEE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__3A81B327]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__3A81B327]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__3A81B327]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__3B75D760]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__3B75D760]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__3B75D760]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__30F848ED]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__30F848ED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__30F848ED]  DEFAULT (0) FOR [MonthBeg1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__31EC6D26]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__31EC6D26]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__31EC6D26]  DEFAULT (0) FOR [MonthBeg2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__32E0915F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__32E0915F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__32E0915F]  DEFAULT (0) FOR [MonthBeg3]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__33D4B598]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__33D4B598]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__33D4B598]  DEFAULT (0) FOR [MonthBeg4]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__34C8D9D1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__34C8D9D1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__34C8D9D1]  DEFAULT (0) FOR [MonthBeg5]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__35BCFE0A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__35BCFE0A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__35BCFE0A]  DEFAULT (0) FOR [MonthBeg6]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__36B12243]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__36B12243]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__36B12243]  DEFAULT (0) FOR [MonthBeg7]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__37A5467C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__37A5467C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__37A5467C]  DEFAULT (0) FOR [MonthBeg8]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__38996AB5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__38996AB5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__38996AB5]  DEFAULT (0) FOR [MonthBeg9]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__398D8EEE]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__398D8EEE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__398D8EEE]  DEFAULT (0) FOR [MonthBeg10]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__3A81B327]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__3A81B327]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__3A81B327]  DEFAULT (0) FOR [MonthBeg11]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Beginning__Month__3B75D760]') AND parent_object_id = OBJECT_ID(N'[dbo].[Beginning_Balance_Table]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Beginning__Month__3B75D760]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF__Beginning__Month__3B75D760]  DEFAULT (0) FOR [MonthBeg12]
END


End
GO
