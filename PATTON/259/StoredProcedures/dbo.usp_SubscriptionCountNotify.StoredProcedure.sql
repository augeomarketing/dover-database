USE [259]
GO

IF OBJECT_ID(N'usp_SubscriptionCountNotify') IS NOT NULL
	DROP PROCEDURE usp_SubscriptionCountNotify
GO

CREATE PROCEDURE usp_SubscriptionCountNotify
AS

DECLARE @body VARCHAR(MAX)
 
SET @body = '<table border ="1"><th>YEAR-WEEK</th><th>COUNT</th>'

SELECT TOP 26 @body = @body + '<tr><td align="center">' 
	+ yearweek
	+ '</td><td align="center">' 
	+ CONVERT(VARCHAR(10), subcount)
	+ '</td></tr>'
FROM 
	( 
		 SELECT CONVERT(VARCHAR(4), DATEPART(YEAR, dim_subscriptioncustomer_startdate)) + '-' + RIGHT('00' + CONVERT(VARCHAR(2), DATEPART(WEEK, dim_subscriptioncustomer_startdate)), 2) yearweek, COUNT(*) subcount
		 FROM RN1.rewardsnow.dbo.subscriptioncustomer
		 WHERE sid_subscriptioncustomer_tipnumber LIKE '259%'
			 AND sid_subscriptionstatus_id = 1
		 GROUP BY 
			CONVERT(VARCHAR(4), DATEPART(YEAR, dim_subscriptioncustomer_startdate)) + '-' + RIGHT('00' + CONVERT(VARCHAR(2), DATEPART(WEEK, dim_subscriptioncustomer_startdate)), 2)
	) ywk
ORDER BY
	yearweek DESC

SET @body = @body + '</table>'

INSERT INTO Maintenance.dbo.perlemail(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from, dim_perlemail_bcc, dim_perlemail_attachment)
SELECT 'Purchasing Power Sponsored Subscriptions', @body, 'dcotrupi@rewardsnow.com; kcaldwell@rewardsnow.com; cheit@rewardsnow.com', 'cheit@rewardsnow.com', 0, NULL


GO