USE [259]
GO

IF OBJECT_ID(N'usp_PurchasingPowerUpdateSponsoredSubscriptions') IS NOT NULL
	DROP PROCEDURE usp_PurchasingPowerUpdateSponsoredSubscriptions
GO

CREATE PROCEDURE usp_PurchasingPowerUpdateSponsoredSubscriptions
AS

DECLARE @myid INT = 1
DECLARE @maxid INT
DECLARE @sqlUpdateTemplate VARCHAR(MAX)
DECLARE @sqlRegisterTemplate VARCHAR(MAX)
DECLARE @sqlUpdate NVARCHAR(MAX)
DECLARE @sqlRegister NVARCHAR(MAX)

DECLARE @subscriptionAdd TABLE
(
	myid INT IDENTITY(1,1) PRIMARY KEY
	, tipnumber varchar(15)
	, startdate varchar(10)
	, canceldate varchar(10) 
)

insert into @subscriptionAdd (tipnumber, startdate, canceldate)
select 
	rnic.dim_rnicustomer_rniid as tipnumber 
	, CONVERT(VARCHAR(10), GETDATE(), 101)
	, CONVERT(VARCHAR(10), DATEADD(YEAR, 1, GETDATE()), 101)
from Rewardsnow.dbo.RNICustomer rnic
left outer join 
(
	select sid_subscriptioncustomer_tipnumber tipnumber
	from rn1.rewardsnow.dbo.subscriptioncustomer
	where sid_subscriptioncustomer_tipnumber like '259%'
		and sid_subscriptiontype_id = 1
		and sid_subscriptionstatus_id = 1
) sc
	on rnic.dim_RNICustomer_RNIId = sc.tipnumber
where sid_dbprocessinfo_dbnumber = '259'
	and dim_RNICustomer_CustomerType = 1
	and sc.tipnumber is null

SET @sqlUpdateTemplate = 'EXEC rn1.rewardsnow.dbo.usp_webUpdateSubscriptionCustomer ''<TIPNUMBER>'', ''<STARTDATE>'', ''YY'', ''<CANCELDATE>'', 1, 1, ''''' 
SET @sqlRegisterTemplate = 'EXEC rewardsnow.dbo.usp_CouponCentsRegistrationByTipnumber ''<TIPNUMBER>'''


SELECT @maxid = MAX(myid) FROM @subscriptionAdd

WHILE @myid <= @maxid
BEGIN
	SELECT @sqlUpdate = REPLACE(REPLACE(REPLACE(@sqlUpdateTemplate, '<TIPNUMBER>', tipnumber), '<STARTDATE>', startdate), '<CANCELDATE>', canceldate)
		, @sqlRegister = REPLACE(@sqlRegisterTemplate, '<TIPNUMBER>', tipnumber)
	FROM
		@subscriptionAdd
	WHERE myid = @myid

/*
	PRINT 'UPDATE ----------------------------------------------'
	PRINT @sqlUpdate

	PRINT 'REGISTER --------------------------------------------'
	PRINT @sqlRegister
*/

	EXEC sp_executesql @sqlUpdate
	EXEC sp_executesql @sqlRegister	

	SET @myid = @myid + 1
END
GO