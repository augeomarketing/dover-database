USE [245]
GO

Declare 
	@Tip3 char(3), 
	@DBNamePatton varchar(50),
	@DBNameNEXL varchar(50), 
	@DBAvailable char(1), 
	@ClientCode varchar(50) , 
	@ClientName varchar(256) , 
	@ProgramName varchar(256),
	@PointExpirationYears int,
	@MinRedeemNeeded int,
	@MaxPointsPerYear int ,
	@LastTip char(15),
	@PointsExpireFrequencyCd char(2),
	@dim_fiinfo_delivtype varchar(50),
	@dim_liab_fileformat varchar(max),  
	@dim_liab_upload nvarchar(50)
	

select * from RewardsNow.dbo.dbprocessinfo where DBNumber = '245'

Set @Tip3 = '245'				-- Tip Prefix
Set @DBNamePatton = '245'		-- This SHOULD be the Tip Number 
Set @DBNameNEXL = 'RedWage'		-- Database name on RN1 
Set @DBAvailable = 'N'		-- Set to N 
Set @ClientCode = 'SmartWage'		-- 
Set @ClientName = 'SmartWage'		-- Used on reports
Set @ProgramName = 'RedStone Discounts & Rewards'		-- ie: Extra Credit Rewards
Set @PointExpirationYears = 0	-- confirm against program form
Set @MinRedeemNeeded = 1500		-- confirm against program form
Set @MaxPointsPerYear = 0		-- confirm against program form
Set @LastTip = @Tip3+'000000000001'
Set @PointsExpireFrequencyCd = 'YE' -- ME = Month End , YE = Year End

declare @contactId INT = 0
declare @processor INT = 13					-- put sid_processor_id if known, 13 if unknown

declare @emailAddress VARCHAR(1024) = 'cheit@rewardsnow.com' -- contact person at FI
declare @delivType VARCHAR(255) = 'Email'	-- put FTP path if available
declare @tipfirst varchar(3) = '245'		-- Tip First of FI
declare @copyFrom varchar(3) = '243'		-- choose tipfirst to copy from
---- ENTER FI DATABASE NAME BELOW ---- 

/*
DROP TABLE AcctType
DROP TABLE Status
DROP TABLE PointsExpireFrequency
DROP TABLE TranType
DROP TABLE TransStandard
DROP TABLE TranCode_Factor
DROP TABLE EStmtTips
*/

TRUNCATE TABLE  Monthly_Statement_File
TRUNCATE TABLE  Monthly_Audit_ErrorFile
TRUNCATE TABLE  Quarterly_Statement_File
TRUNCATE TABLE  Quarterly_Audit_ErrorFile

TRUNCATE TABLE  Current_Month_Activity
TRUNCATE TABLE  Beginning_Balance_Table
TRUNCATE TABLE  Client

TRUNCATE TABLE  OneTimeBonuses_Stage
TRUNCATE TABLE  OneTimeBonuses

TRUNCATE TABLE  CUSTOMERdeleted
TRUNCATE TABLE  CUSTOMER_Stage
TRUNCATE TABLE  CUSTOMER


TRUNCATE TABLE  HistoryDeleted
TRUNCATE TABLE  HISTORY_Stage
TRUNCATE TABLE  HISTORY

TRUNCATE TABLE  AFFILIAT_Stage
TRUNCATE TABLE  AFFILIAT
TRUNCATE TABLE  AffiliatDeleted


BEGIN TRANSACTION
	INSERT INTO [dbo].[Client]
	([ClientCode], [ClientName], [Description], [TipFirst], [Address1], [Address2], [Address3], [Address4], [City], [State], [Zipcode], [Phone1], [Phone2], [ContactPerson1], [ContactPerson2], [ContactEmail1], [ContactEmail2], [DateJoined], [RNProgramName], [TermsConditions], [PointsUpdatedDT], [MinRedeemNeeded], [TravelFlag], [MerchandiseFlag], [TravelIncMinPoints], [MerchandiseBonusMinPoints], [MaxPointsPerYear], [PointExpirationYears], [ClientID], [Pass], [ServerName], [DbName], [UserName], [Password], [PointsExpire], [LastTipNumberUsed], [PointsExpireFrequencyCd], [ClosedMonths], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [logo], [landing], [termspage], [faqpage], [earnpage], [Business], [StatementDefault], [StmtNum], [CustomerServicePhone])
	SELECT
	 @ClientCode, @ClientName, @ClientName, @Tip3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, GETDATE(), N'RewardsNOW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @PointExpirationYears, @Tip3, N'Test', NULL, NULL, NULL, NULL, NULL, @LastTip, @PointsExpireFrequencyCd, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
COMMIT
	RAISERROR (N'[dbo].[Client]: Insert Batch: 1 of 1.....Done!', 10, 1) WITH NOWAIT;
	
--use deliverables

select @contactId = sid_contact_id FROM Deliverables.dbo.contact where dim_contact_email = @emailAddress
if @contactId = 0 OR @contactId is null
begin
	insert into Deliverables.dbo.contact (dim_contact_email)
	Values (@emailAddress)
	SET @contactId = SCOPE_IDENTITY()
end

declare @fiId INT = 0
select top 1 @fiid = sid_fiinfo_id 
FROM deliverables.dbo.fiinfo 
where dim_fiinfo_tip = @tipfirst

IF @fiId = 0 OR @fiId is null
BEGIN
  INSERT INTO Deliverables.dbo.fiinfo (dim_fiinfo_tip, dim_fiinfo_dbname, dim_fiinfo_shrtdbname, dim_fiinfo_netpath, sid_processor_id, sid_contact_id, dim_fiinfo_delivtype)
  SELECT @tipfirst, DBNamePatton, DBNameNEXL, 'c:\reports\bin', @processor, @contactId, @delivType
	  FROM Rewardsnow.dbo.dbprocessinfo
	  where DBNumber = @tipfirst
	  
  set @fiId = SCOPE_IDENTITY()
END

INSERT INTO Deliverables.dbo.liab (sid_fiinfo_id, dim_liab_format, dim_liab_fileformat, dim_liab_upload)
	SELECT @fiId, REPLACE(l1.dim_liab_format, @copyfrom, @tipfirst), l1.dim_liab_fileformat, @delivType
		from Deliverables.dbo.liab l1
		WHERE l1.sid_fiinfo_id = (select sid_fiinfo_id from Deliverables.dbo.fiinfo where dim_fiinfo_tip = @copyFrom)
		AND (SELECT COUNT(*) from Deliverables.dbo.liab 
				WHERE sid_fiinfo_id = @fiId 
					AND dim_liab_format = REPLACE(l1.dim_liab_format, @copyfrom, @tipfirst)) = 0

INSERT INTO Deliverables.dbo.job (sid_fiinfo_id, dim_Job_format, dim_job_fileformat, dim_job_upload)
	SELECT @fiId, REPLACE(l1.dim_job_format, @copyfrom, @tipfirst), l1.dim_job_fileformat, @delivType
		from Deliverables.dbo.job l1
		WHERE l1.sid_fiinfo_id = (select sid_fiinfo_id from Deliverables.dbo.fiinfo where dim_fiinfo_tip = @copyFrom)
		AND (SELECT COUNT(*) from Deliverables.dbo.job 
				WHERE sid_fiinfo_id = @fiId 
					AND dim_job_format = REPLACE(l1.dim_job_format, @copyfrom, @tipfirst)) = 0
*/					
					
INSERT INTO RewardsNow.dbo.RNIRawImportDataDefinition(
	sid_rnirawimportdatadefinitiontype_id
	, sid_rniimportfiletype_id
	, sid_dbprocessinfo_dbnumber
	, dim_rnirawimportdatadefinition_outputfield
	, dim_rnirawimportdatadefinition_outputdatatype
	, dim_rnirawimportdatadefinition_sourcefield
	, dim_rnirawimportdatadefinition_startindex
	, dim_rnirawimportdatadefinition_length
	, dim_rnirawimportdatadefinition_multipart
	, dim_rnirawimportdatadefinition_multipartpart
	, dim_rnirawimportdatadefinition_multipartlength
	, dim_rnirawimportdatadefinition_version
	, dim_rnirawimportdatadefinition_applyfunction
)
select
	sid_rnirawimportdatadefinitiontype_id
	, sid_rniimportfiletype_id
	, @tip3
	, dim_rnirawimportdatadefinition_outputfield
	, dim_rnirawimportdatadefinition_outputdatatype
	, dim_rnirawimportdatadefinition_sourcefield
	, dim_rnirawimportdatadefinition_startindex
	, dim_rnirawimportdatadefinition_length
	, dim_rnirawimportdatadefinition_multipart
	, dim_rnirawimportdatadefinition_multipartpart
	, dim_rnirawimportdatadefinition_multipartlength
	, dim_rnirawimportdatadefinition_version
	, dim_rnirawimportdatadefinition_applyfunction
from
	RewardsNow.dbo.RNIRawImportDataDefinition
where
	sid_dbprocessinfo_dbnumber = @copyFrom
	

insert into RewardsNow.dbo.RNICustomerLoadSource (sid_dbprocessinfo_dbnumber, dim_rnicustomerloadsource_sourcename, dim_rnicustomerloadsource_fileload)
select @Tip3, REPLACE(dim_rnicustomerloadsource_sourcename, @copyFrom, @Tip3), dim_rnicustomerloadsource_fileload
from RewardsNow.dbo.RNICustomerLoadSource where sid_dbprocessinfo_dbnumber = @copyFrom

declare @loadsourcefrom int = 
(
	select sid_rnicustomerloadsource_id 
	from RewardsNow.dbo.RNICustomerLoadSource 
	where dim_rnicustomerloadsource_sourcename = 'vw_243_ACCT_SOURCE_225'
)

declare @loadsource int = 
(
	select sid_rnicustomerloadsource_id 
	from RewardsNow.dbo.RNICustomerLoadSource 
	where dim_rnicustomerloadsource_sourcename = replace('vw_243_ACCT_SOURCE_225', '243', @Tip3)
)

INSERT INTO RewardsNow.dbo.RNICustomerLoadColumn
(
	sid_rnicustomerloadsource_id
		, dim_rnicustomerloadcolumn_sourcecolumn
		, dim_rnicustomerloadcolumn_targetcolumn
		, dim_rnicustomerloadcolumn_fidbcustcolumn
		, dim_rnicustomerloadcolumn_keyflag
		, dim_rnicustomerloadcolumn_loadtoaffiliat
		, dim_rnicustomerloadcolumn_affiliataccttype
		, dim_rnicustomerloadcolumn_primaryorder
		, dim_rnicustomerloadcolumn_primarydescending
		, dim_rnicustomerloadcolumn_required
)
select
		@loadsource
		, dim_rnicustomerloadcolumn_sourcecolumn
		, dim_rnicustomerloadcolumn_targetcolumn
		, dim_rnicustomerloadcolumn_fidbcustcolumn
		, dim_rnicustomerloadcolumn_keyflag
		, dim_rnicustomerloadcolumn_loadtoaffiliat
		, dim_rnicustomerloadcolumn_affiliataccttype
		, dim_rnicustomerloadcolumn_primaryorder
		, dim_rnicustomerloadcolumn_primarydescending
		, dim_rnicustomerloadcolumn_required
from
	RewardsNow.dbo.RNICustomerLoadColumn
where
	sid_rnicustomerloadsource_id = @loadsourcefrom
	

select * from RewardsNow.dbo.RNICustomerLoadColumn where sid_rnicustomerloadsource_id = 1
