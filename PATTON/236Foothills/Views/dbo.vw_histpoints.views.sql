USE [236]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 10/11/2010 16:56:22 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_histpoints]'))
DROP VIEW [dbo].[vw_histpoints]
GO

USE [236]
GO

/****** Object:  View [dbo].[vw_histpoints]    Script Date: 10/11/2010 16:56:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[vw_histpoints] 

as 

select tipnumber, sum(points * ratio) as points 
from dbo.history_stage 
where secid = 'NEW' 
group by tipnumber


GO


