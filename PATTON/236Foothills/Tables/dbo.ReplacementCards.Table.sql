USE [236]
GO
drop table [dbo].[ReplacementCards]
go
/****** Object:  Table [dbo].[ReplacementCards]    Script Date: 09/30/2010 09:30:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReplacementCards](
	[ACCTID] [varchar](25) NOT NULL,
	[Last6Affiliat] [varchar](6) NULL,
	[CreditCardNumber] [varchar](16) NULL,
	[CustomerId] [varchar](25) NOT NULL,
	[source] [varchar](11) NOT NULL
) ON [PRIMARY]

GO


