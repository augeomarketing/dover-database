USE [236]
GO

/****** Object:  Table [dbo].[Customer_MissingNameAddress]    Script Date: 09/15/2010 09:13:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer_MissingNameAddress](
	[CustomerId] [varchar](25) NOT NULL,
	[Name1] [varchar](40) NULL,
	[LastName1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[LastName2] [varchar](40) NULL,
	[Name3] [varchar](40) NULL,
	[Name4] [varchar](40) NULL,
	[Name5] [varchar](40) NULL,
	[Name6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](20) NULL,
	[StateCode] [varchar](2) NULL,
	[ZipCode] [varchar](5) NULL,
	[Zip4] [varchar](4) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[StatusCode] [varchar](2) NULL,
	[Last4SSN] [varchar](4) NULL,
	[BusinessFlag] [varchar](1) NULL,
	[EmployeeFlag] [varchar](1) NULL,
	[InstitutionNumber] [varchar](10) NULL,
	[CreditCardNumber] [varchar](16) NULL,
	[PrimaryFlag] [varchar](1) NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_Customer_MissingNameAddressId] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

