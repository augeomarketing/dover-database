USE [236]
GO

/****** Object:  Table [dbo].[wrkTipsAndCards]    Script Date: 01/07/2011 08:29:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkTipsAndCards]') AND type in (N'U'))
DROP TABLE [dbo].[wrkTipsAndCards]
GO

USE [236]
GO

/****** Object:  Table [dbo].[wrkTipsAndCards]    Script Date: 01/07/2011 08:29:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[wrkTipsAndCards](
	[TipNumber] [varchar](15) NULL,
	[Card1] [varchar](16) NULL,
	[Card2] [varchar](16) NULL,
	[Card3] [varchar](16) NULL,
	[Card4] [varchar](16) NULL,
	[Card5] [varchar](16) NULL,
	[Card6] [varchar](16) NULL,
	[Card7] [varchar](16) NULL,
	[Card8] [varchar](16) NULL,
	[Card9] [varchar](16) NULL,
	[Card10] [varchar](16) NULL
) ON [PRIMARY]

GO


