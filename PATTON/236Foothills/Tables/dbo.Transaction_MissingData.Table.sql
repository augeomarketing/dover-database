USE [236]
GO
DROP TABLE [dbo].[Transaction_MissingData]
GO
/****** Object:  Table [dbo].[Transaction_MissingData]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction_MissingData](
	[CustomerId] [varchar](20) NOT NULL,
	[AccountNumber] [varchar](20) NULL,
	[TransferAccount] [varchar](20) NULL,
	[DateLastActivity] [varchar](12) NULL,
	[TransactionCode] [varchar](3) NULL,
	[TransactionAmount] [numeric](13,2) NULL,
	[transactionCount] [numeric](3, 0) NULL,
	[DateAdded]  [datetime] NULL,
) ON [PRIMARY]
GO
