USE [236]
GO

/****** Object:  Table [dbo].[impcustomer_Purge_pending]    Script Date: 12/30/2010 12:04:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impcustomer_Purge_pending]') AND type in (N'U'))
DROP TABLE [dbo].[impcustomer_Purge_pending]
GO

USE [236]
GO

/****** Object:  Table [dbo].[impcustomer_Purge_pending]    Script Date: 12/30/2010 12:04:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impcustomer_Purge_pending](
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL
) ON [PRIMARY]

GO


