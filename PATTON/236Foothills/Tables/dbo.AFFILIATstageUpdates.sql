USE [236]
GO

/****** Object:  Table [dbo].[AFFILIATStageUpdates]    Script Date: 10/21/2010 14:31:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIATStageUpdates]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIATStageUpdates]
GO

USE [236]
GO

/****** Object:  Table [dbo].[AFFILIATStageUpdates]    Script Date: 10/21/2010 14:31:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AFFILIATStageUpdates](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIATStageUpdates] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[TIPNUMBER] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


