USE [236]
GO

DROP TABLE [dbo].[NamePrs]
GO
/****** Object:  Table [dbo].[NamePrs]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NamePrs](
	[PERSNBR] [varchar](25) NULL,
	[AcctNm] [varchar](50) NULL,
	[FIRSTNM] [varchar](50) NULL,
	[MIDDLENM] [varchar](50) NULL,
	[LASTNM] [varchar](50) NULL
) ON [PRIMARY]
GO
