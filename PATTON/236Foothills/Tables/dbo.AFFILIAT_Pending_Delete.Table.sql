USE [236]
GO
DROP TABLE [dbo].[AFFILIAT_Pending_Delete]
GO
/****** Object:  Table [dbo].[AFFILIAT_Pending_Delete]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFILIAT_Pending_Delete](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [bigint] NOT NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT_Pending_Delete] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AFFILIAT_Pending_Delete]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Pending_Delete_AcctType] FOREIGN KEY([AcctType])
REFERENCES [dbo].[AcctType] ([AcctType])
GO
ALTER TABLE [dbo].[AFFILIAT_Pending_Delete] CHECK CONSTRAINT [FK_AFFILIAT_Pending_Delete_AcctType]
GO
ALTER TABLE [dbo].[AFFILIAT_Pending_Delete]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Pending_Delete_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO
ALTER TABLE [dbo].[AFFILIAT_Pending_Delete] CHECK CONSTRAINT [FK_AFFILIAT_Pending_Delete_CUSTOMER_Stage]
GO
ALTER TABLE [dbo].[AFFILIAT_Pending_Delete] ADD  CONSTRAINT [DF_AFFILIAT_Pending_Delete_YTDEarned]  DEFAULT ((0)) FOR [YTDEarned]
GO
