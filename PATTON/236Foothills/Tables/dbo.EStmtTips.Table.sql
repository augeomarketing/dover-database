USE [236]
GO
DROP TABLE [dbo].[EStmtTips]
GO
/****** Object:  Table [dbo].[EStmtTips]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EStmtTips](
	[sid_estmttips_tipnumber] [varchar](15) NOT NULL,
 CONSTRAINT [PK_EStmtTips] PRIMARY KEY CLUSTERED 
(
	[sid_estmttips_tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
