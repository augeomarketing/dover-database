USE [236]
GO
drop table  [dbo].[CUSTOMER_Pending_Delete]
go
/****** Object:  Table [dbo].[CUSTOMER_Pending_Delete]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER_Pending_Delete](
	[TIPNUMBER] [varchar](15) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[CUSTID] [varchar](13) NULL,
	[RECENTACTIVITY] [varchar] (1) NULL,
	[DATEDELETED] [datetime] NULL

) ON [PRIMARY]
GO
