USE [236]
GO
DROP TABLE [dbo].[AFFILIAT_Rejects_No_CC]
GO
/****** Object:  Table [dbo].[AFFILIAT_Rejects_No_CC]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFILIAT_Rejects_No_CC](
	[ACCTID] [varchar](25) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DATEADDED] [datetime] NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL
) ON [PRIMARY]
GO
