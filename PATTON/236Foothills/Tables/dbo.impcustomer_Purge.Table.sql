USE [236]
GO

/****** Object:  Table [dbo].[impcustomer_Purge]    Script Date: 12/30/2010 12:04:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impcustomer_Purge]') AND type in (N'U'))
DROP TABLE [dbo].[impcustomer_Purge]
GO

USE [236]
GO

/****** Object:  Table [dbo].[impcustomer_Purge]    Script Date: 12/30/2010 12:04:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impcustomer_Purge](
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL
) ON [PRIMARY]

GO


