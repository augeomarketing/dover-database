USE [236]
GO
DROP TABLE [dbo].[AcctType]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 09/09/2010 10:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AcctType] ADD  CONSTRAINT [DF_AcctType_Acctmultiplier]  DEFAULT ((1)) FOR [Acctmultiplier]
GO
