USE [236]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_BonusProgram_BonusType]') AND parent_object_id = OBJECT_ID(N'[dbo].[BonusProgram]'))
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [FK_BonusProgram_BonusType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_ExpirationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_ExpirationDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_PointMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_PointMultiplier]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]
END

GO

USE [236]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 11/02/2010 15:45:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BonusProgram]') AND type in (N'U'))
DROP TABLE [dbo].[BonusProgram]
GO

USE [236]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 11/02/2010 15:45:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BonusProgram](
	[sid_BonusType_Id] [int] NOT NULL,
	[dim_BonusProgram_EffectiveDate] [datetime] NOT NULL,
	[dim_BonusProgram_ExpirationDate] [datetime] NOT NULL,
	[dim_BonusProgram_PointMultiplier] [decimal](18, 2) NULL,
	[dim_BonusProgram_DateAdded] [datetime] NULL,
	[dim_BonusProgram_LastUpdated] [datetime] NULL,
	[dim_BonusProgram_BonusPoints] [bigint] NULL,
 CONSTRAINT [PK_BonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC,
	[dim_BonusProgram_EffectiveDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BonusProgram]  WITH NOCHECK ADD  CONSTRAINT [FK_BonusProgram_BonusType] FOREIGN KEY([sid_BonusType_Id])
REFERENCES [dbo].[BonusType] ([sid_BonusType_Id])
GO

ALTER TABLE [dbo].[BonusProgram] CHECK CONSTRAINT [FK_BonusProgram_BonusType]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_ExpirationDate]  DEFAULT ('12/31/9999') FOR [dim_BonusProgram_ExpirationDate]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_PointMultiplier]  DEFAULT ((1)) FOR [dim_BonusProgram_PointMultiplier]
GO

ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgram_DateAdded]
GO


