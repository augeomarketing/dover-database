USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\TransactionReject.xls', N'\Package.Connections[TransactionReject.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\patton\ops\236\Output\Audit\transactionReject.txt', N'\Package.Connections[TransactionReject].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\Templates\Transaction_Template.xls', N'\Package.Connections[Transaction_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\patton\ops\236\Input\SBDRewards.txt', N'\Package.Connections[SBDRewards.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\patton\ops\236\Output\Audit\Templates\MSFSummary_Template.xls', N'\Package.Connections[MSFSummary_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236', N'Data Source=patton\rn;Initial Catalog=236;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-236_OPS_M04_PostToWeb-{F6B029F7-EC22-4A1C-BFA9-D0E5F16B6995}patton\rn.236;', N'\Package.Connections[236Foothill].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=O:\236\Output\Audit\MonthlyStatement.xls;Extended Properties="EXCEL 12.0;HDR=YES";', N'\Package.Connections[Excel Monthly Statement].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\CustomerReject.xls', N'\Package.Connections[CustomerReject.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\Templates\Customer_Template.xls', N'\Package.Connections[Customer_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\patton\ops\236\Output\Audit\CustomerRejectFile.txt', N'\Package.Connections[Customer Reject File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\patton\ops\236\Input\Transaction236.txt', N'\Package.Connections[236 Foothill Transaction File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_OPS_M03_WelcomeKit', N'O:\236\Output\Templates\WelcomeKitTemplate.xls', N'\Package.Connections[WelcomeKitTemplate.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_OPS_M03_WelcomeKit', N'O:\236\Output\Welcome Kits\WelcomeKit.xls', N'\Package.Connections[WelcomeKit.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_OPS_M03_WelcomeKit', N'\\ftp\ike\Production Files\WelcomeKit', N'\Package.Variables[User::ProdWKPath].Properties[Value]', N'String' UNION ALL
SELECT N'236_OPS_M03_WelcomeKit', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\236\Output\Welcome Kits\WelcomeKit.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Welcome Kit XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\patton\ops\236\Input\Customer236.txt', N'\Package.Connections[236 Foothill Customer File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\236\Output\Audit\MSFSummary.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Excel Connection MSF Summary].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\236\Output\Audit\TransactionReject.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Excel Connection Manager Transaction Reject].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=O:\236\Output\Audit\CustomerReject.xls;Extended Properties="EXCEL 12.0;HDR=YES";', N'\Package.Connections[Excel Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_SUB_M04_EStatement', N'Data Source=patton\web;Initial Catalog=Foothill;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-236_SUB_M04_EStatements-{53AB6FD1-EE61-4CF5-829E-52E95ACA64D0}patton\web.Foothill;', N'\Package.Connections[patton\web.Foothill].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\MSFSummary.xls', N'\Package.Connections[MSFSummary.xls File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\Templates\MonthlyStatement_Template.xls', N'\Package.Connections[MonthlyStatement_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\MonthlyStatement.xls', N'\Package.Connections[MonthlyStatement XLS File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_OPS_M04_PostToWeb', N'Data Source=patton\web;Initial Catalog=Foothill;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-236_OPS_M04_PostToWeb-{4E9B9016-65A4-4591-B8E4-5AD3142EB008}patton\web;', N'\Package.Connections[RN1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_OPS_M04_PostToWeb', N'C:\RewardsNOW SVN Repository\Database\SSIS\236Foothill\Integration Services Project1\236_SUB_M04_EStatements.dtsx', N'\Package.Connections[236_SUB_M04_EStatements.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-236_M01_ImportToStage-{8301DF43-3541-45B8-BA11-D3A25E1396D7}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236', N'236', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'236', N'10/01/2010 00:00:00', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'236', N'10/31/2010 00:00:00', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

