USE [RewardsNow]
GO
INSERT INTO [RewardsNow].[dbo].[dbprocessinfo]
           ([DBNumber]
           ,[DBNamePatton]
           ,[DBNameNEXL]
           ,[DBAvailable]
           ,[ClientCode]
           ,[ClientName]
           ,[ProgramName]
           ,[DBLocationPatton]
           ,[DBLocationNexl]
           ,[PointExpirationYears]
           ,[DateJoined]
           ,[MinRedeemNeeded]
           ,[MaxPointsPerYear]
           ,[LastTipNumberUsed]
           ,[PointsExpireFrequencyCd]
           ,[ClosedMonths]
           ,[WelcomeKitGroupName]
           ,[GenerateWelcomeKit]
           ,[sid_FiProdStatus_statuscode]
           ,[IsStageModel]
           ,[ExtractGiftCards]
           ,[ExtractCashBack]
           ,[RNProgramName]
           ,[PointsUpdated]
           ,[TravelMinimum]
           ,[TravelBottom]
           ,[CashBackMinimum]
           ,[Merch]
           ,[AirFee]
           ,[Logo]
           ,[Landing]
           ,[TermsPage]
           ,[FaqPage]
           ,[EarnPage]
           ,[Business]
           ,[StatementDefault]
           ,[StatementNum]
           ,[CustomerServicePhone]
           ,[ContactPerson1]
           ,[ContactPerson2]
           ,[ContactPhone1]
           ,[ContactPhone2]
           ,[ContactEmail1]
           ,[ContactEmail2]
           ,[Address1]
           ,[Address2]
           ,[Address3]
           ,[Address4]
           ,[City]
           ,[State]
           ,[ZipCode]
           ,[ServerName]
           ,[TransferHistToRn1]
           ,[CalcDailyExpire]
           ,[hasonlinebooking])
     VALUES
           ('236'
           ,'236'
           ,'Foothill'
           ,'Y'
           ,'236Foothill'
           ,'Foothill Federal Credit Union'
           ,'Foothill Rewards'
           ,'236'
           ,'Foothill'
           ,3
           ,'2010-10-01 00:00:00.000'
           ,750
           ,NULL
           ,236000000000001
           ,'ME'
           ,NULL
           ,NULL
           ,'Y'
           ,'Y'
           ,1
           ,'N'
           ,'N'
           ,NULL
           ,NULL
           ,5000
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'CINDY COYLE'
           ,NULL
           ,'626-445-0950 Ext 6223'
           ,NULL
           ,'ccoyle@foothillcu.org'
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'N'
           ,'N'
            0)
GO



