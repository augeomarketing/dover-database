USE [236]
GO
/****** create table client  ******/
INSERT INTO [236].[dbo].[Client]
           ([ClientCode]
           ,[ClientName]
           ,[Description]
           ,[TipFirst]
        
           ,[DateJoined]
           ,[RNProgramName]
          
           ,[MinRedeemNeeded]
           ,[TravelFlag]
           ,[MerchandiseFlag]
           ,[TravelIncMinPoints]
           ,[MerchandiseBonusMinPoints]
          
           ,[PointExpirationYears]
           ,[ClientID]
           ,[Pass]
           ,[ServerName]
           ,[DbName]
           
           ,[PointsExpire]
           ,[PointsExpireFrequencyCd])
     VALUES
           ('236Foothill',
           'Foothill', 
           'Foothill Federal Credit Union', 
           '236', 
           '2010-10-10 00:00:00.000',
           'Foothill Rewards',
          
           750, 
           '1', 
           '1',
           5000, 
           0, 
        
           3, 
           '236', 
           'foothills', 
           'PATTON\RN', 
           '236',
          
           '3', 
           'ME')
GO

 

 


/****  populate [PointsExpireFrequency]   ***/
 

INSERT INTO [236].[dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd], [PointsExpireFrequencyNm]) VALUES ('ME', 'Month End')
INSERT INTO [236].[dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd], [PointsExpireFrequencyNm]) VALUES ('MM', 'Mid Month')
INSERT INTO [236].[dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd], [PointsExpireFrequencyNm]) VALUES ('YE', 'Year End')

go

/****  populate [STATUS]   ***/

INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('A', 'Active[A]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('C', 'Closed[C]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('D', 'Deleted[D]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('I', 'Inactive[I]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('K', 'Bankrupt[K]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('L', 'Lost/Stolen[L]')

INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('O', 'OverLimit[O]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('P', 'Pending[P]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('S', 'Suspended[S]')


INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('U', 'Fraud[U]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('X', 'PastDue[X]')
INSERT INTO [236].[dbo].[Status] ([Status],[StatusDescription]) VALUES ('Y', 'FinanceChargeFrozen[Y]')


/****  populate [dbprocessinfo]   ***/



INSERT INTO [RewardsNow].[dbo].[dbprocessinfo]
           ([DBNumber]
           ,[DBNamePatton]
           ,[DBNameNEXL]
           ,[DBAvailable]
           ,[ClientCode]
           ,[ClientName]
           ,[ProgramName]
           ,[DBLocationPatton]
           ,[DBLocationNexl]
           ,[PointExpirationYears]
           ,[DateJoined]
           ,[MinRedeemNeeded]
           ,[TravelMinimum]
            
           ,[PointsExpireFrequencyCd]
           
           
           ,[GenerateWelcomeKit]
           ,[sid_FiProdStatus_statuscode]
           ,[IsStageModel]
           ,[ExtractGiftCards]
           ,[ExtractCashBack]
           
            )
     VALUES
           ('236'
           ,'236'
           ,'Foothill'
           ,'Y'
           ,'Foothill'
           ,'Foothill Federal Credit Union'
           ,'Foothill Rewards'
           ,'236'
           ,'Foothill'
           ,3
           ,'2010-10-01 00:00:00.000'
           ,750
           ,5000  
            
           ,'ME'
            
           
           ,'N'
           ,'V'
           ,0
           ,'N'
           ,'N'
           
           )
GO

