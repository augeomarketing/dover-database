 
USE 236
GO
ALTER TABLE dbo.wrkTipsAndCards ALTER COLUMN TipNumber varchar(15) NOT NULL;
go
ALTER TABLE dbo.wrkTipsAndCards WITH NOCHECK 
ADD CONSTRAINT PK_wrkTipsAndCards_TipNumber PRIMARY KEY CLUSTERED (TipNumber);
GO


