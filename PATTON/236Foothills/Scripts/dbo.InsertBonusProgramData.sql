USE [236];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusProgram]([sid_BonusType_Id], [dim_BonusProgram_EffectiveDate], [dim_BonusProgram_ExpirationDate], [dim_BonusProgram_PointMultiplier], [dim_BonusProgram_DateAdded], [dim_BonusProgram_LastUpdated], [dim_BonusProgram_BonusPoints])
SELECT 1, '20101126 00:00:00.000', '20101231 23:59:59.997', 1.00, '20101001 00:00:00.000', NULL, NULL UNION ALL
SELECT 2, '20101001 00:00:00.000', '99991231 00:00:00.000', 0.00, '20101001 00:00:00.000', NULL, 2000
COMMIT;
RAISERROR (N'[dbo].[BonusProgram]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

