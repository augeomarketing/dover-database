********LOADED ACCT_TYPE AND TRAN_TYPE USING SQL EXPORT WIZARD IN MGT STUDIO
USE [236]
GO


/********************** create tables  **********************/
DROP TABLE  [236].[dbo].[AcctType]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_AcctType] PRIMARY KEY CLUSTERED 
(
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

 
GO

ALTER TABLE [dbo].[AcctType] ADD  CONSTRAINT [DF_AcctType_Acctmultiplier]  DEFAULT ((1)) FOR [Acctmultiplier]
GO
 

 SET ANSI_PADDING off
GO



/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 08/25/2010 16:02:21 ******/
DROP TABLE  [236].[dbo].[AFFILIAT]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
 
 

ALTER TABLE [dbo].[AFFILIAT]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_AcctType] FOREIGN KEY([AcctType])
REFERENCES [dbo].[AcctType] ([AcctType])
GO

ALTER TABLE [dbo].[AFFILIAT] CHECK CONSTRAINT [FK_AFFILIAT_AcctType]
GO
 

 SET ANSI_PADDING off
GO


/****** Object:  Table [dbo].[AFFILIAT_Stage]    Script Date: 08/25/2010 16:03:09 ******/
DROP TABLE  [236].[dbo].[AFFILIAT_Stage]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFFILIAT_Stage](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [bigint] NOT NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT_Stage] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

 

ALTER TABLE [dbo].[AFFILIAT_Stage]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Stage_AcctType] FOREIGN KEY([AcctType])
REFERENCES [dbo].[AcctType] ([AcctType])
GO

ALTER TABLE [dbo].[AFFILIAT_Stage] CHECK CONSTRAINT [FK_AFFILIAT_Stage_AcctType]
GO

ALTER TABLE [dbo].[AFFILIAT_Stage]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO

ALTER TABLE [dbo].[AFFILIAT_Stage] CHECK CONSTRAINT [FK_AFFILIAT_Stage_CUSTOMER_Stage]
GO

ALTER TABLE [dbo].[AFFILIAT_Stage] ADD  CONSTRAINT [DF_AFFILIAT_Stage_YTDEarned]  DEFAULT ((0)) FOR [YTDEarned]
GO

  SET ANSI_PADDING off
GO

/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 08/25/2010 16:03:35 ******/

DROP TABLE  [236].[dbo].[AffiliatDeleted]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
 
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 08/25/2010 16:03:57 ******/
 
DROP TABLE  [236].[dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT ((0)) FOR [MonthBeg1]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT ((0)) FOR [MonthBeg2]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT ((0)) FOR [MonthBeg3]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT ((0)) FOR [MonthBeg4]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT ((0)) FOR [MonthBeg5]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT ((0)) FOR [MonthBeg6]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT ((0)) FOR [MonthBeg7]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT ((0)) FOR [MonthBeg8]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT ((0)) FOR [MonthBeg9]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT ((0)) FOR [MonthBeg10]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT ((0)) FOR [MonthBeg11]
GO

ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT ((0)) FOR [MonthBeg12]
GO

  SET ANSI_PADDING off
GO



/****** Object:  Table [dbo].[Client]    Script Date: 08/25/2010 16:05:24 ******/


DROP TABLE  [236].[dbo].[Client]    
GO
 
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [decimal](18, 0) NULL,
	[MerchandiseBonusMinPoints] [decimal](18, 0) NULL,
	[MaxPointsPerYear] [decimal](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Pass] [varchar](30) NOT NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipNumberUsed] [char](15) NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[PointsUpdated] [datetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [bit] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[logo] [varchar](50) NULL,
	[landing] [varchar](255) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StmtNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

 
 
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd])
GO

ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]
GO

 SET ANSI_PADDING off
GO
 
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/25/2010 16:06:16 ******/



DROP TABLE  [236].[dbo].[Current_Month_Activity]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [varchar](15) NOT NULL,
	[EndingPoints] [int] NOT NULL,
	[Increases] [int] NOT NULL,
	[Decreases] [int] NOT NULL,
	[AdjustedEndingPoints] [int] NOT NULL,
 CONSTRAINT [PK_Current_Month_Activity] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

 

ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT ((0)) FOR [EndingPoints]
GO

ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT ((0)) FOR [Increases]
GO

ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT ((0)) FOR [Decreases]
GO

ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT ((0)) FOR [AdjustedEndingPoints]
GO
 SET ANSI_PADDING off
GO
 
/****** Object:  Table [dbo].[CUSTOMER]   Script Date: 08/25/2010 16:06:35 ******/


DROP TABLE  [236].[dbo].[CUSTOMER]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

 

ALTER TABLE [dbo].[CUSTOMER]  WITH CHECK ADD  CONSTRAINT [FK_CUSTOMER_Status] FOREIGN KEY([STATUS])
REFERENCES [dbo].[Status] ([Status])
GO

ALTER TABLE [dbo].[CUSTOMER] CHECK CONSTRAINT [FK_CUSTOMER_Status]
GO

  SET ANSI_PADDING off
GO


/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 08/25/2010 16:06:54 ******/


DROP TABLE  [236].[dbo].[CUSTOMER_Stage]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CUSTOMER_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER_stage] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

 

/****** Object:  Table [dbo].[CUSTOMERdeleted]    Script Date: 08/25/2010 16:07:09 ******/


DROP TABLE  [236].[dbo].[CUSTOMERdeleted]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CUSTOMERdeleted](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_CUSTOMERdeleted] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
 

 
/****** Object:  Table [dbo].[HISTORY]    Script Date: 08/25/2010 16:07:55 ******/



DROP TABLE  [236].[dbo].[HISTORY]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[sid_history_id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HISTORY] PRIMARY KEY CLUSTERED 
(
	[sid_history_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

 SET ANSI_PADDING off
GO
GO

ALTER TABLE [dbo].[HISTORY]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_TranType] FOREIGN KEY([TRANCODE])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[HISTORY] CHECK CONSTRAINT [FK_HISTORY_TranType]
GO
 SET ANSI_PADDING off
GO



 /****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 08/31/2010 08:37:02 ******/



DROP TABLE  [236].[dbo].[HISTORY_Stage]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [bigint] NOT NULL,
	[HistoryStageId] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HISTORY_Stage] PRIMARY KEY CLUSTERED 
(
	[HistoryStageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

 

ALTER TABLE [dbo].[HISTORY_Stage]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO

ALTER TABLE [dbo].[HISTORY_Stage] CHECK CONSTRAINT [FK_HISTORY_Stage_CUSTOMER_Stage]
GO

ALTER TABLE [dbo].[HISTORY_Stage]  WITH CHECK ADD  CONSTRAINT [FK_HISTORY_Stage_TranType] FOREIGN KEY([TRANCODE])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[HISTORY_Stage] CHECK CONSTRAINT [FK_HISTORY_Stage_TranType]
GO

ALTER TABLE [dbo].[HISTORY_Stage] ADD  CONSTRAINT [DF_HISTORY_Stage_Overage]  DEFAULT ((0)) FOR [Overage]
GO

 SET ANSI_PADDING off
GO

 
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 08/31/2010 08:50:04 ******/



DROP TABLE  [236].[dbo].[HistoryDeleted]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[DateDeleted] [datetime] NULL,
	[HistoryDeletedId] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HistoryDeleted] PRIMARY KEY CLUSTERED 
(
	[HistoryDeletedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO




/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 08/25/2010 16:09:13 ******/


DROP TABLE  [236].[dbo].[Monthly_Audit_ErrorFile]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Monthly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

 
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 08/25/2010 16:09:29 ******/


DROP TABLE  [236].[dbo].[Monthly_Statement_File]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NOT NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NOT NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[acctid] [varchar](16) NULL,
	[cardseg] [varchar](10) NULL,
	[status] [varchar](1) NULL,
	[lastfour] [varchar](4) NULL,
	[pointfloor] [varchar](11) NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

 
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_pointfloor]  DEFAULT ((0)) FOR [pointfloor]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
GO
 SET ANSI_PADDING off
GO

/****** Object:  Table [dbo].[NamePrs]    Script Date: 08/25/2010 16:09:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[NamePrs](
	[PERSNBR] [varchar](25) NULL,
	[AcctNm] [varchar](50) NULL,
	[FIRSTNM] [varchar](50) NULL,
	[MIDDLENM] [varchar](50) NULL,
	[LASTNM] [varchar](50) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

 
   
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 08/25/2010 16:10:47 ******/

DROP TABLE  [236].[dbo].[Quarterly_Audit_ErrorFile]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBillPay] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NOT NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
	[PointsTransferred] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Quarterly_Audit_ErrorFile] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

 
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBillPay]  DEFAULT ((0)) FOR [PointsBillPay]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_Currentend]  DEFAULT ((0)) FOR [Currentend]
GO
 SET ANSI_PADDING off
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
GO

ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF_Quarterly_Audit_ErrorFile_PointsTransferred]  DEFAULT ((0)) FOR [PointsTransferred]
GO
 


/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 08/25/2010 16:11:04 ******/

DROP TABLE  [236].[dbo].[Quarterly_Statement_File]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NOT NULL,
	[PointsEnd] [decimal](18, 0) NOT NULL,
	[PointsPurchasedCR] [decimal](18, 0) NOT NULL,
	[PointsPurchasedDB] [decimal](18, 0) NOT NULL,
	[PointsBillPay] [decimal](18, 0) NOT NULL,
	[PointsBonus] [decimal](18, 0) NOT NULL,
	[PointsAdded] [decimal](18, 0) NOT NULL,
	[PointsIncreased] [decimal](18, 0) NOT NULL,
	[PointsRedeemed] [decimal](18, 0) NOT NULL,
	[PointsReturnedCR] [decimal](18, 0) NOT NULL,
	[PointsReturnedDB] [decimal](18, 0) NOT NULL,
	[PointsSubtracted] [decimal](18, 0) NOT NULL,
	[PointsDecreased] [decimal](18, 0) NOT NULL,
	[acctid] [char](16) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL,
	[PointsExpire] [decimal](18, 0) NOT NULL,
	[PointsTransferred] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

 
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBillPay]  DEFAULT ((0)) FOR [PointsBillPay]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsTransferred]  DEFAULT ((0)) FOR [PointsTransferred]
GO
 SET ANSI_PADDING off
GO
 


/****** Object:  Table [dbo].[Status]    Script Date: 08/25/2010 16:11:19 ******/

DROP TABLE  [236].[dbo].[Status]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[Status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 08/25/2010 16:11:31 ******/


DROP TABLE  [236].[dbo].[TranCode_Factor]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [varchar](2) NOT NULL,
	[PointFactor] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_TranCode_Factor] PRIMARY KEY CLUSTERED 
(
	[Trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO
 
ALTER TABLE [dbo].[TranCode_Factor]  WITH CHECK ADD  CONSTRAINT [FK_TranCode_Factor_TranType] FOREIGN KEY([Trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[TranCode_Factor] CHECK CONSTRAINT [FK_TranCode_Factor_TranType]
GO

 SET ANSI_PADDING off
GO
 
/****** Object:  Table [dbo].[TransStandard]    Script Date: 09/01/2010 10:41:52 ******/



DROP TABLE  [236].[dbo].[TransStandard]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransStandard](
	[TransStandardID] [bigint] IDENTITY(1,1) NOT NULL,
	[TIPNumber] [varchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctID] [varchar](25) NULL,
	[TranCode] [varchar](2) NOT NULL,
	[TranNum] [int] NOT NULL,
	[TranAmt] [decimal](18, 0) NOT NULL,
	[TranType] [varchar](40) NOT NULL,
	[Ratio] [int] NOT NULL,
	[CrdActvlDt] [date] NULL,
 CONSTRAINT [PK_TransStandard_1] PRIMARY KEY CLUSTERED 
(
	[TransStandardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

 
ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_CUSTOMER_Stage] FOREIGN KEY([TIPNumber])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO

ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_CUSTOMER_Stage]
GO


ALTER TABLE [dbo].[TransStandard]  WITH CHECK ADD  CONSTRAINT [FK_TransStandard_TranCode] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[TransStandard] CHECK CONSTRAINT [FK_TransStandard_TranCode]
GO



ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranNum]  DEFAULT ((1)) FOR [TranNum]
GO

ALTER TABLE [dbo].[TransStandard] ADD  CONSTRAINT [DF_TransStandard_TranAmt]  DEFAULT ((0)) FOR [TranAmt]
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[TranType]    Script Date: 08/25/2010 16:12:05 ******/


DROP TABLE  [236].[dbo].[TranType]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TranType](
	[TranCode] [varchar](2) NOT NULL,
	[Description] [varchar](40) NULL,
	[IncDec] [varchar](1) NOT NULL,
	[CntAmtFxd] [varchar](1) NOT NULL,
	[Points] [int] NOT NULL,
	[Ratio] [int] NOT NULL,
	[TypeCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY CLUSTERED 
(
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

 
GO

ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Points]  DEFAULT ((1)) FOR [Points]
GO

ALTER TABLE [dbo].[TranType] ADD  CONSTRAINT [DF_TranType_Ratio]  DEFAULT ((1)) FOR [Ratio]
GO
 
 SET ANSI_PADDING off
GO
/****** Object:  Table [dbo].[web_Account]    Script Date: 08/25/2010 16:12:18 ******/



DROP TABLE  [236].[dbo].[web_Account]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[web_Account](
	[TipNumber] [varchar](15) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [varchar](20) NOT NULL,
	[SSNLast4] [varchar](4) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

 
/****** Object:  Table [dbo].[web_Customer]    Script Date: 08/25/2010 16:12:29 ******/


DROP TABLE  [236].[dbo].[web_Customer]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[web_Customer](
	[TipNumber] [varchar](15) NOT NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[TipLast] [varchar](12) NOT NULL,
	[Name1] [varchar](50) NOT NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Address1] [varchar](50) NOT NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[CityStateZip] [varchar](50) NULL,
	[ZipCode] [varchar](10) NULL,
	[EarnedBalance] [int] NULL,
	[Redeemed] [int] NULL,
	[AvailableBal] [int] NULL,
	[Status] [varchar](2) NULL,
	[Segment] [varchar](2) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](5) NULL,
PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

 
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 08/25/2010 16:12:43 ******/


DROP TABLE  [236].[dbo].[WelcomeKit]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WelcomeKit](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
 CONSTRAINT [PK_WelcomeKit] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

  
 

/****** Object:  Table [dbo].[CustomerImport]    Script Date: 08/26/2010 10:16:06 ******/



DROP TABLE  [236].[dbo].[CustomerImport]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CustomerImport](
	[CustomerId] [varchar](25) NOT NULL,
	[Name1] [varchar](40) NULL,
	[LastName1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[LastName2] [varchar](40) NULL,
	[Name3] [varchar](40) NULL,
	[Name4] [varchar](40) NULL,
	[Name5] [varchar](40) NULL,
	[Name6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](20) NULL,
	[StateCode] [varchar](2) NULL,
	[ZipCode] [varchar](5) NULL,
	[Zip4] [varchar](4) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[StatusCode] [varchar](2) NULL,
	[Last4SSN] [varchar](4) NULL,
	[BusinessFlag] [varchar](1) NULL,
	[EmployeeFlag] [varchar](1) NULL,
	[InstitutionNumber] [varchar](10) NULL,
	[CreditCardNumber] [varchar](16) NULL,
	[PrimaryFlag] [varchar](1) NULL,
 CONSTRAINT [PK_CustomerId] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


 
/****** Object:  Table [dbo].[TransactionImport]    Script Date: 08/26/2010 10:17:22 ******/



DROP TABLE  [236].[dbo].[TransactionImport]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionImport](
	[CustomerId] [varchar](20) NOT NULL,
	[AccountNumber] [varchar](20) NULL,
	[TransferAccount] [varchar](20) NULL,
	[DateLastActivity] [varchar](12) NULL,
	[TransactionCode] [varchar](3) NULL,
	[TransactionAmount] [numeric](15, 0) NULL,
	[transactionCount] [numeric](3, 0) NULL,
 CONSTRAINT [PK_TransCustomerId] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[CustomerImportScrubbed]   Script Date: 08/26/2010 15:54:50 ******/



DROP TABLE  [236].[dbo].[CustomerImportScrubbed]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CustomerImportScrubbed](
	[CustomerId] [varchar](25) NOT NULL,
	[Name1] [varchar](40) NULL,
	[LastName1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[LastName2] [varchar](40) NULL,
	[Name3] [varchar](40) NULL,
	[Name4] [varchar](40) NULL,
	[Name5] [varchar](40) NULL,
	[Name6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](20) NULL,
	[StateCode] [varchar](2) NULL,
	[ZipCode] [varchar](5) NULL,
	[Zip4] [varchar](4) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[StatusCode] [varchar](2) NULL,
	[Last4SSN] [varchar](4) NULL,
	[BusinessFlag] [varchar](1) NULL,
	[EmployeeFlag] [varchar](1) NULL,
	[InstitutionNumber] [varchar](10) NULL,
	[CreditCardNumber] [varchar](16) NULL,
	[PrimaryFlag] [varchar](1) NULL
 CONSTRAINT [PK_CustomerImportScrubbedId] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[TransactionImportScrubbed]   Script Date: 08/26/2010 15:56:32 ******/


DROP TABLE  [236].[dbo].[TransactionImportScrubbed]
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransactionImportScrubbed](
	[CustomerId] [varchar](20) NOT NULL,
	[AccountNumber] [varchar](20) NULL,
	[TransferAccount] [varchar](20) NULL,
	[DateLastActivity] [varchar](12) NULL,
	[TransactionCode] [varchar](3) NULL,
	[TransactionAmount] [numeric](15, 0) NULL,
	[transactionCount] [numeric](3, 0) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[CUSTOMER_Pending_Delete]    Script Date: 08/31/2010 09:18:58 ******/



DROP TABLE  [236].[dbo].[CUSTOMER_Pending_Delete]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CUSTOMER_Pending_Delete](
	[TIPNUMBER] [varchar](15) NULL,
	[DATEADDED] [datetime]   NULL,
	[LASTNAME] [varchar](40) NULL,
	[ACCTNAME1] [varchar](40) NULL
)
 	 
  


/****** Object:  Table [dbo].[HISTORY_Pending_Delete]    Script Date: 08/31/2010 08:34:49 ******/



DROP TABLE  [236].[dbo].[HISTORY_Pending_Delete]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HISTORY_Pending_Delete](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [int] NULL,
	[Overage] [bigint] NOT NULL,
	[HistoryStageId] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HISTORY_Pending_Delete] PRIMARY KEY CLUSTERED 
(
	[HistoryStageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

go
 SET ANSI_PADDING off
GO


/****** Object:  Table [dbo].[AFFILIAT_Pending_Delete]    Script Date: 08/31/2010 08:40:27 ******/


DROP TABLE  [236].[dbo].[AFFILIAT_Pending_Delete]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFFILIAT_Pending_Delete](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [bigint] NOT NULL,
	[CustID] [varchar](13) NULL,
 CONSTRAINT [PK_AFFILIAT_Pending_Delete] PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC,
	[AcctType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

 

ALTER TABLE [dbo].[AFFILIAT_Pending_Delete]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Pending_Delete_AcctType] FOREIGN KEY([AcctType])
REFERENCES [dbo].[AcctType] ([AcctType])
GO

ALTER TABLE [dbo].[AFFILIAT_Pending_Delete] CHECK CONSTRAINT [FK_AFFILIAT_Pending_Delete_AcctType]
GO

ALTER TABLE [dbo].[AFFILIAT_Pending_Delete]  WITH CHECK ADD  CONSTRAINT [FK_AFFILIAT_Pending_Delete_CUSTOMER_Stage] FOREIGN KEY([TIPNUMBER])
REFERENCES [dbo].[CUSTOMER_Stage] ([TIPNUMBER])
GO

ALTER TABLE [dbo].[AFFILIAT_Pending_Delete] CHECK CONSTRAINT [FK_AFFILIAT_Pending_Delete_CUSTOMER_Stage]
GO

ALTER TABLE [dbo].[AFFILIAT_Pending_Delete] ADD  CONSTRAINT [DF_AFFILIAT_Pending_Delete_YTDEarned]  DEFAULT ((0)) FOR [YTDEarned]
GO

SET ANSI_PADDING OFF
GO
USE [236]
GO

/****** Object:  Table [dbo].[AFFILIAT_Rejects_No_CC]  Script Date: 09/01/2010 15:49:55 ******/


DROP TABLE  [236].[dbo].[AFFILIAT_Rejects_No_CC]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFFILIAT_Rejects_No_CC](
	[ACCTID] [varchar](25)   NULL,
	[TIPNUMBER] [varchar](15)   NULL,
	[AcctType] [varchar](20)   NULL,
	[DATEADDED] [datetime]   NULL,
	[SECID] [varchar](10)  ,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL )

/****** Object:  Table [dbo].[AFFILIAT_Rejects_NO_Match_TIP]  Script Date: 09/01/2010 15:49:55 ******/


DROP TABLE  [236].[dbo].[AFFILIAT_Rejects_NO_Match_TIP]  
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AFFILIAT_Rejects_NO_Match_TIP]  (
	[ACCTID] [varchar](25)   NULL,
	[TIPNUMBER] [varchar](15)   NULL,
	[AcctType] [varchar](20)   NULL,
	[DATEADDED] [datetime]   NULL,
	[SECID] [varchar](10)  ,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [int] NULL,
	[CustID] [varchar](13) NULL )

 
/****** Object:  Table [dbo].[Customer_MissingNameAddress]    Script Date: 09/07/2010 11:47:56 ******/


DROP TABLE  [236].[dbo].[Customer_MissingNameAddress]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Customer_MissingNameAddress](
	[CustomerId] [varchar](25) NOT NULL,
	[Name1] [varchar](40) NULL,
	[LastName1] [varchar](40) NULL,
	[Name2] [varchar](40) NULL,
	[LastName2] [varchar](40) NULL,
	[Name3] [varchar](40) NULL,
	[Name4] [varchar](40) NULL,
	[Name5] [varchar](40) NULL,
	[Name6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](20) NULL,
	[StateCode] [varchar](2) NULL,
	[ZipCode] [varchar](5) NULL,
	[Zip4] [varchar](4) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[StatusCode] [varchar](2) NULL,
	[Last4SSN] [varchar](4) NULL,
	[BusinessFlag] [varchar](1) NULL,
	[EmployeeFlag] [varchar](1) NULL,
	[InstitutionNumber] [varchar](10) NULL,
	[CreditCardNumber] [varchar](16) NULL,
	[PrimaryFlag] [varchar](1) NULL,
 CONSTRAINT [PK_Customer_MissingNameAddressId] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
 

/****** Object:  Table [dbo].[Transaction_MissingData]    Script Date: 09/07/2010 12:46:53 ******/


DROP TABLE  [236].[dbo].[Transaction_MissingData]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Transaction_MissingData](
	[CustomerId] [varchar](20) NOT NULL,
	[AccountNumber] [varchar](20) NULL,
	[TransferAccount] [varchar](20) NULL,
	[DateLastActivity] [varchar](12) NULL,
	[TransactionCode] [varchar](3) NULL,
	[TransactionAmount] [numeric](15, 0) NULL,
	[transactionCount] [numeric](3, 0) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
 

/****** Object:  Table [dbo].[CUSTOMERStageUpdates]    Script Date: 09/07/2010 15:32:12 ******/



DROP TABLE  [236].[dbo].[CUSTOMERStageUpdates]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CUSTOMERStageUpdates](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NOT NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMERStageUpdates] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** create table [PointsExpireFrequency]      ******/



DROP TABLE  [236].[dbo].[PointsExpireFrequency]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

