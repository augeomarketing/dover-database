USE [236];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[BonusType] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusType]([sid_BonusType_Id], [dim_BonusType_TranCode], [dim_BonusType_Description], [dim_BonusType_DateAdded], [dim_BonusType_LastUpdated])
SELECT 1, N'BI', N'Double Points', '20101001 00:00:00.000', NULL UNION ALL
SELECT 2, N'BR', N'Registering On-line', '20101025 09:19:24.683', NULL
COMMIT;
RAISERROR (N'[dbo].[BonusType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[BonusType] OFF;
