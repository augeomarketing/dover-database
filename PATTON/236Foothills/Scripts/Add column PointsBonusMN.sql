alter table [236].dbo.Monthly_Statement_File
 add   PointsBonusMN  numeric (18,0) null
 
 go
 
 
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
GO

alter table [236].dbo.Monthly_Audit_ErrorFile
 add   PointsBonusMN  numeric (18,0) null
 
 go
 
 