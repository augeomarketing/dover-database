




USE [236];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[BonusType] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusType]([sid_BonusType_Id], [dim_BonusType_TranCode], [dim_BonusType_Description], [dim_BonusType_DateAdded], [dim_BonusType_LastUpdated])
SELECT 1, N'BI', N'Double Points', '20101001 00:00:00.000', NULL
COMMIT;
RAISERROR (N'[dbo].[BonusType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[BonusType] OFF;






USE [236];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusProgram]([sid_BonusType_Id], [dim_BonusProgram_EffectiveDate], [dim_BonusProgram_ExpirationDate], [dim_BonusProgram_PointMultiplier], [dim_BonusProgram_DateAdded], [dim_BonusProgram_LastUpdated])
SELECT 1, '20101126 00:00:00.000', '20101231 23:59:59.997', 1.00, '20101001 00:00:0.000', NULL
COMMIT;
RAISERROR (N'[dbo].[BonusProgram]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

