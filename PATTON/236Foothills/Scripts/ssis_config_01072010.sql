USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete  [dbo].[SSIS Configurations] where [ConfigurationFilter] = '236_M01_ImportToStage'
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\TransactionReject.xls', N'\Package.Connections[TransactionReject.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\doolittle\ops\236\Output\Audit\transactionReject.txt', N'\Package.Connections[TransactionReject].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\Templates\Transaction_Template.xls', N'\Package.Connections[Transaction_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\doolittle\ops\236\Input\SBDRewards.txt', N'\Package.Connections[SBDRewards.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\Doolittle\ops\236\Output\Audit\Templates\MSFSummary_Template.xls', N'\Package.Connections[MSFSummary_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\236\Output\Audit\TransactionReject.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Excel Connection Manager Transaction Reject].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\Doolittle\ops\236\Output\Audit\MonthlyStatement.xls;Extended Properties="Excel 8.0;HDR=YES";', N'\Package.Connections[Excel Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.ACE.OLEDB.12.0;Data Source=O:\236\Output\Audit\CustomerReject.xls;Extended Properties="EXCEL 12.0;HDR=YES";', N'\Package.Connections[Excel Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Data Source=doolittle\rn;Initial Catalog=236;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-236_OPS_M04_PostToWeb-{F6B029F7-EC22-4A1C-BFA9-D0E5F16B6995}doolittle\rn.236;', N'\Package.Connections[236Foothill].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\Templates\MonthlyStatement_Template.xls', N'\Package.Connections[MonthlyStatement_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\MonthlyStatement.xls', N'\Package.Connections[MonthlyStatement XLS File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\CustomerReject.xls', N'\Package.Connections[CustomerReject.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\Templates\Customer_Template.xls', N'\Package.Connections[Customer_Template.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\Doolittle\ops\236\Output\Audit\CustomerRejectFile.txt', N'\Package.Connections[Customer Reject File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\Doolittle\ops\236\Input\Transaction236.txt', N'\Package.Connections[236 Foothill Transaction File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'O:\236\Output\Audit\MSFSummary.xls', N'\Package.Connections[MSFSummary.xls File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=O:\236\Output\Audit\MSFSummary.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Excel Connection MSF Summary].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'236_M01_ImportToStage', N'\\Doolittle\ops\236\Input\Customer236.txt', N'\Package.Connections[236 Foothill Customer File].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

