USE [236]
GO
DROP function [dbo].[fnTotalPointsFromHistoryForTranCode]
GO
/****** Object:  UserDefinedFunction [dbo].[fnTotalPointsFromHistoryForTranCode]    Script Date: 09/09/2010 10:25:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[fnTotalPointsFromHistoryForTranCode] (@TipNumber nvarchar(16), @StartDate datetime, @EndDate datetime, @TranCode nvarchar(2))

returns int

AS

BEGIN
declare @points	int

	set @points = 0

	set @points = (select sum(points) 
					from dbo.History_Stage
					where tipnumber=@TipNumber
					and histdate between @StartDate and @EndDate
					and trancode=@TranCode)

	return isnull(@points, 0)

END
GO
