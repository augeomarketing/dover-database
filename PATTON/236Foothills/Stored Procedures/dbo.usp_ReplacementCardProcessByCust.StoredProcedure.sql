USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_ReplacementCardProcessByCust]    Script Date: 02/23/2011 16:17:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ReplacementCardProcessByCust]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ReplacementCardProcessByCust]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_ReplacementCardProcessByCust]    Script Date: 02/23/2011 16:17:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_ReplacementCardProcessByCust]  
@TipFirst varchar(3)

AS


  
BEGIN TRY
	BEGIN TRANSACTION  --- START
	--PRINT 'START'
	 
--Note:  Foothill send us the last 6 digits of the card#. so when this file comes in,
--we determine if there is a replacement incase a matching transaction file does not 
-- come in. there is another similar procedure that will handle replacement from the transaction file.
   truncate table ReplacementCards
   --make list of replacement cards that have come in with this months file
   insert into [236].dbo.ReplacementCards   
   select  aff.ACCTID,SUBSTRING(aff.acctid,11,6) AS Last6Affiliat,cis.CreditCardNumber,cis.CustomerId,'Customer'  
   from AFFILIAT_Stage aff  
   join CustomerImportScrubbed cis on aff.CustID = cis.CustomerId
   and SUBSTRING(aff.acctid,11,6) <> cis.CreditCardNumber
   and SUBSTRING(aff.tipnumber,1,3) =  @TipFirst
   and AcctStatus =   'A'
 
   
 --  PRINT 'COMPLETED INSERT INTO REPLACEMENTCARDS'
   
--**create a cursor to loop thru customer records to udpate affiliat.  
declare @CustID			varchar(25)
declare @NewLastSixCC			varchar(25)

declare csrReplacement cursor FAST_FORWARD for
	select cis.customerId, cis.CreditCardNumber from dbo.CustomerImportScrubbed cis INNER join ReplacementCards rc
	on cis.customerid = rc.customerId
	where rc.source = 'Customer'

open csrReplacement

fetch next from csrReplacement into @CustID,@NewLastSixCC

while @@FETCH_STATUS = 0
BEGIN


----cancel card that is being replaced
--		update  AFFILIAT_Stage  
--		set  AcctStatus = 'C'
--		where  CustID = @CustID
--		and AcctTypeDesc = 'Credit'
--		and AcctStatus =  'A'
--	    and SUBSTRING(tipnumber,1,3) = @TipFirst
	    

--	--===>  new affiliat has not been created by ssis, it doesn't create affiliat
-- if there is no matching transaction  
--	--now insert new row with new card #

	 if object_id('tempdb..#tmpAffiliat') is not null
	   drop table #tmpAffiliat
	  
	   
-- Create temp table
     create table #tmpAffiliat
    ( acctid		  varchar(25),
     TIPNUMBER        varchar(15),
     AcctType		  varchar(20),
     DateAdded		  datetime,
    SECID			  varchar(10),
    AcctStatus		  varchar(1),
     AcctTypeDesc	  varchar(50),
    LastName          varchar(40),
    YTDEarned		  bigint,
    CustomerId		  varchar(13) )
 


	 --pull row of values into tmp table...to be used to create new row in affiliat_stage 
    insert into #tmpAffiliat
	select    (@NewLastSixCC) as acctid, TIPNUMBER,AcctType,GETDATE() as DateAdded
	,SECID,'A' as AcctStatus,'Active' as AcctTypeDesc,LastName,YTDEarned,CustID as CustomerId
	from dbo.AFFILIAT_Stage 
	where CustID = @CustID 
    and AcctType = 'Credit'
	and SUBSTRING(tipnumber,1,3) = @TipFirst
	

	
	--now create affilat stage with row from above
	 insert    dbo.AFFILIAT_Stage   (ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,LastName,YTDEarned,CustID)
	 select acctid,TIPNUMBER,AcctType, DateAdded,SECID,AcctStatus,AcctTypeDesc,LastName,YTDEarned,CustomerId
	 from  #tmpAffiliat    
 

 
fetch next from csrReplacement into  @CustID,@NewLastSixCC
END


close csrReplacement

deallocate csrReplacement


-- If we reach here, success!
   COMMIT TRANSACTION
PRINT 'NOW WE HAVE COMMITTED'

END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error


close csrReplacement

deallocate csrReplacement


  IF @@TRANCOUNT > 0
     ROLLBACK TRANSACTION
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH


GO


