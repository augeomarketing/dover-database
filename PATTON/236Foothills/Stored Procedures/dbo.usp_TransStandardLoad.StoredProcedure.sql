USE [236]
GO
DROP PROCEDURE [dbo].[usp_TransStandardLoad]
GO
/****** Object:  StoredProcedure [dbo].[usp_TransStandardLoad]    Script Date: 09/20/2010 09:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_TransStandardLoad] @DateAdded datetime, @BillPayPointMultiplier int
AS


BEGIN TRY
	BEGIN TRANSACTION  --- START
	
	
	
-- Clear TransStandard 
Truncate table dbo.TransStandard 


-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	(TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt  ) 

select dim_impcustomer_tipnumber, @DateAdded, dim_impcustomer_acctnum, '67', 1, dim_impcustomer_points,
    tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.trantype Tt 
    on '67' = tt.trancode
where dim_impcustomer_pointssign = '+'

union all

select dim_impcustomer_tipnumber, @DateAdded, dim_impcustomer_acctnum, '37', 1, dim_impcustomer_points ,
    tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.trantype Tt 
    on '37' = tt.trancode
where dim_impcustomer_pointssign = '-'


 


-- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH
