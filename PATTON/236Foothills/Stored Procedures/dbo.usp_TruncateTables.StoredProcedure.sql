USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_TruncateTables]    Script Date: 01/26/2011 13:40:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TruncateTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TruncateTables]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_TruncateTables]    Script Date: 01/26/2011 13:40:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_TruncateTables]

as


--modification
--dirish 1/26/2011  add TRUNCATE TABLE.dbo.wrkTipsAndCards


BEGIN TRY
	BEGIN TRANSACTION  --- START
	
	
truncate table dbo.CustomerImport
truncate table dbo.TransactionImport

truncate table dbo.AFFILIAT_Stage
truncate table dbo.CustomerImportScrubbed
truncate table dbo.TransactionImportScrubbed
truncate table dbo.TransStandard

delete dbo.CUSTOMER_Stage

truncate table dbo.HISTORY_Stage
TRUNCATE TABLE dbo.CUSTOMERStageUpdates
TRUNCATE TABLE dbo.Customer_MissingNameAddress
TRUNCATE TABLE dbo.AFFILIAT_Rejects_No_CC
TRUNCATE TABLE dbo.AFFILIAT_Rejects_NO_Match_TIP
TRUNCATE TABLE dbo.Transaction_MissingData
TRUNCATE TABLE dbo.Transaction_NoMatchingTIP
TRUNCATE TABLE dbo.AFFILIATStageUpdates
TRUNCATE TABLE.dbo.wrkTipsAndCards

 -- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO


