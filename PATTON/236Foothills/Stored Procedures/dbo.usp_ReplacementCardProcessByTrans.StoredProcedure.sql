USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_ReplacementCardProcessByTrans]    Script Date: 02/23/2011 16:53:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ReplacementCardProcessByTrans]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ReplacementCardProcessByTrans]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_ReplacementCardProcessByTrans]    Script Date: 02/23/2011 16:53:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 create PROCEDURE [dbo].[usp_ReplacementCardProcessByTrans]  
@TipFirst varchar(3)

AS
  
BEGIN TRY
	BEGIN TRANSACTION  --- START
	--PRINT 'START'
	
 	declare @bankNumber varchar(10) 
 	--set @bankNumber = '5449650100'
 	 
-- 
   --make list of replacement cards that have come in with this months file
   insert into [236].dbo.ReplacementCards   
   select  aff.ACCTID,SUBSTRING(aff.acctid,11,6) AS Last6Affiliat,tis.AccountNumber,tis.CustomerId,'Transaction'  
   from AFFILIAT_Stage aff  
   join TransactionImportScrubbed tis on aff.CustID = tis.CustomerId
   and aff.acctid <> tis.AccountNumber
   and SUBSTRING(aff.tipnumber,1,3) =  @TipFirst
   and AcctTypeDesc = 'Credit'
   and AcctStatus =   'A'
   
    --
   
   
 --  PRINT 'COMPLETED INSERT INTO REPLACEMENTCARDS'
   
--**create a cursor to loop thru customer records to udpate affiliat.  
declare @CustID			varchar(25)
declare @CC			varchar(25)

declare csrReplacement cursor FAST_FORWARD for
	select tis.customerId, tis.AccountNumber from dbo.TransactionImportScrubbed tis INNER join ReplacementCards rc
	on tis.customerid = rc.customerId
	where rc.source = 'Transaction'

open csrReplacement

fetch next from csrReplacement into @CustID,@CC

while @@FETCH_STATUS = 0
BEGIN


--cancel card that is being replaced
		update  AFFILIAT_Stage  
		set  AcctStatus = 'C'
		where  CustID = @CustID
		and AcctTypeDesc = 'Credit'
		and AcctStatus =  'A'
	    and SUBSTRING(tipnumber,1,3) = @TipFirst
	    
  
 
fetch next from csrReplacement into  @CustID,@CC
END


close csrReplacement

deallocate csrReplacement


-- If we reach here, success!
   COMMIT TRANSACTION
PRINT 'NOW WE HAVE COMMITTED'

END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK TRANSACTION
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO


