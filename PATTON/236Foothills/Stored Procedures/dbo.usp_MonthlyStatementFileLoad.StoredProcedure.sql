USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 01/26/2011 14:17:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyStatementFileLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 01/26/2011 14:17:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]  @StartDateParm datetime, @EndDateParm datetime

AS 

--modifications 
--dirish 1/26/2011 - include all bonuses trancode especially Merchant funding F0,F9,G0,G9
----------------------------------------------------------------------------------------------------------------
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	 
Declare @EndDate DateTime 	 

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')  
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' ) 

--print @Startdate 
--print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )



BEGIN TRY
	BEGIN TRANSACTION  --- START
	
	
/* Load the statement file from the customer table  */
delete from dbo.Monthly_Statement_File

--monthly statement should  have distinct tipnumbers  
insert into dbo.Monthly_Statement_File 
(tipnumber, acctname1, acctname2, address1, address2, address3,
 citystatezip, status, cardseg,lastfour)
select c.tipnumber, c.acctname1, c.acctname2, c.address1, c.address2, c.address3,
 (rtrim(c.city) + ' ' + rtrim(c.state) + ' ' + c.zipcode) , c.Status,cardtype = 
 case   
 when  Misc3 IS NULL  then ''
 else 'credit'
 end
,c.Misc2
from dbo.customer_Stage c 
 
  

/* Load the statmement file with CREDIT purchases          */
/* upon initial implementation, Foothills is only passing type 33 and 63...but i'm leaving coding becaue function is pulling
in activity from history.  Web activity is updated into the history table which is where these other codes ie,dr,tp etc.. come from
*/
update dbo.Monthly_Statement_File 
	set	pointspurchasedCR = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '63'), 0),
		pointsreturnedCR =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '33'), 0),
		pointspurchasedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '67'), 0),
		pointsreturnedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, '37'), 0),

		pointsadded =		isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'IE'), 0) +
						isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'DR'), 0) +
						isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'TP'), 0),
		--dirish 1/26/2011 remove below stmt					
		--				 + isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'BI'), 0),

		pointssubtracted =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'DE'), 0) +
						isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'EP'), 0),

		PointsExpire =		isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, 'XP'), 0)
	--dirish 1/26/2011 remove below stmt	
 	--PointsBonus = isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, 'BR'), 0) 
 


/* Load the statmement file with bonuses */		
----dirish 1/26/2011 add points bonus to include all bonuses except merchant funding
    update Monthly_Statement_File
	set pointsbonus=(select sum(points*ratio) 
				     from dbo.History_Stage 
				     where tipnumber=Monthly_Statement_File.tipnumber 
				     and histdate>=@startdate and histdate<=@enddate
					 and (trancode like 'B%' or trancode like 'F%' or trancode like 'G%')
					 and (trancode <>  'F0' and trancode <>  'F9'  and trancode <>  'G0' and trancode <>  'G9') )
     where exists(select * from History_Stage 
     where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate 
     and (trancode like 'B%' or trancode like 'F%'  or trancode like 'G%' ))


/* Load the statmement file with Merchant Funding bonuses */		
----dirish 1/26/2011 populate new column pointsBonusMN (F0,F9,G0,G9)
 update Monthly_Statement_File
	set PointsBonusMN =(select sum(points*ratio) 
				        from dbo.History_Stage 
				        where tipnumber=Monthly_Statement_File.tipnumber and 
						      histdate>=@startdate and histdate<=@enddate 
						      and (trancode = 'F0' or trancode = 'F9' or trancode ='G0' or trancode =  'G9') )
     where exists(select * from History_Stage 
     where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate 
     and (trancode = 'F0' or trancode = 'F9' or trancode ='G0' or trancode =  'G9'))
     
     

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update dbo.Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate

 
----dirish 1/26/2011 include pointsBonusMN (F0,F9,G0,G9)
/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR  + pointsbonus + pointsadded + PointsBonusMN

/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with total point decreases */
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR  + pointssubtracted + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update dbo.Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from dbo.Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased

-- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO


