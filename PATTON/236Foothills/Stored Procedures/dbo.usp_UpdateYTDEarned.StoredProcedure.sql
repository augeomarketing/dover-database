USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateYTDEarned]    Script Date: 01/06/2011 15:15:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateYTDEarned]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateYTDEarned]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateYTDEarned]    Script Date: 01/06/2011 15:15:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_UpdateYTDEarned]
    @TipFirst		    varchar(3)

as

  
--modifications
-- d.irish 1/6/2011, modify to accomodate mulitple cards per tip
BEGIN TRY
	BEGIN TRANSACTION  --- START

-- Create temp table
create table #YTDEarned
    ( TIPNUMBER		  varchar(15) ,
     acctid varchar(16),
     sumYTDEARNED INT)
--  63 is Credit Purchase
--  33 Credit Return

--LOAD TMP TABLE WITH YTDEARNED
insert into #YTDEarned
(TIPNUMBER,acctid,sumYTDEARNED)
select TIPNUMBER,acctid,SUM(Points* ratio)   
FROM dbo.HISTORY_STAGE    where LEFT(tipnumber,3) = @TipFirst
GROUP BY TIPNUMBER,acctid
ORDER BY TIPNUMBER,acctid
 
 
  
 --UPDATE AFFILIAT_STAGE
Update A
set YTDEarned  = A.YTDEarned  + Y.sumYTDEARNED
FROM #YTDEarned Y JOIN dbo.AFFILIAT_Stage A  
on Y.Tipnumber = A.Tipnumber
and Y.acctid = A.ACCTID
where LEFT(Y.tipnumber,3) = @TipFirst

 
 
 -- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO


