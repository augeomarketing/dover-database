USE [236]
GO
DROP  procedure [dbo].[usp_AssignTipNumbers]
go
/****** Object:  StoredProcedure [dbo].[usp_AssignTipNumbers]    Script Date: 09/23/2010 09:05:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  procedure [dbo].[usp_AssignTipNumbers]
	   @TipFirst		    varchar(3)

As


declare @lasttipused		varchar(15)
declare @TipInt			    int
declare @Customer			varchar(25)
declare @tipnumber          varchar(15)

set nocount on


BEGIN TRY
	BEGIN TRANSACTION  --- START
--
-- Check to see if customer already is in system.....
--
-- Update CustomerImportScrubbed with Tip#s from affiliat with matching card #s
    update cis
    set TipNumber = aff.tipnumber
    from dbo.CustomerImportScrubbed cis join dbo.affiliat_stage aff
     on cis.customerid = aff.custid
    where aff.TIPNUMBER is not null

--
    update cis
    set TipNumber = cus.tipnumber
    from dbo.CustomerImportScrubbed cis join dbo.customer_stage cus
     on cis.customerid = cus.misc1
    where cis.TipNumber is null




 
------------------------------------------------------------------------------------------
--
-- Now add in new cards
------------------------------------------------------------------------------------------

--
-- get last tip# used
exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirst, @lasttipused OUTPUT
set @TipInt = cast(  right(@lasttipused, 12) as int)

print 'lastTipUsed  = ' + @lasttipused
 

----
---- Drop temp table should it exist (should only exist during testing/dev)
if object_id('tempdb..#tmpCust') is not null
    drop table #tmpCust
 

---- Create temp tables
create table #tmpCust
    (customerid		  varchar(25) primary key,
     processed		  varchar(1))

 
 
-- Get list of distinct customers without tipnumber
insert into #tmpCust
(customerid) 
select distinct  CustomerId
from  dbo.CustomerImportScrubbed
where  TipNumber is null  
order by CustomerId 

 
 
  
-- Loop 
declare csrNewCustomers cursor FAST_FORWARD for
  select distinct  CustomerId
from  dbo.CustomerImportScrubbed
where  TipNumber is null  
order by CustomerId 


open csrNewCustomers
fetch next from csrNewCustomers into @Customer
 
while  @@FETCH_STATUS = 0  

BEGIN
    set @tipint += 1
    
    update cs
    set  tipnumber = @tipfirst + right('000000000000' + cast(@tipint as varchar(12)), 12)
    from #tmpCust t join CustomerImportScrubbed cs
    on t.CustomerId = cs.CustomerId
    and cs.TipNumber is null
    and t.CustomerId = @Customer
    
  
   
     --print 'cust = '  + @Customer
     --print 'tip = ' +  convert(char(12),@tipint)
 
   fetch next from csrNewCustomers into @Customer
END

close csrNewCustomers

deallocate csrNewCustomers
   
  
     
-- Now update last tip number used

set @lasttipused = @tipfirst + right('000000000000' + cast(@tipint as varchar(12)), 12)
EXEC RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @lasttipused

 
 -- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

