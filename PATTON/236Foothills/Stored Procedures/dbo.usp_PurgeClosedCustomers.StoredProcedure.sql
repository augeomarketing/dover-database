USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_PurgeClosedCustomers]    Script Date: 06/06/2011 09:04:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PurgeClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PurgeClosedCustomers]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_PurgeClosedCustomers]    Script Date: 06/06/2011 09:04:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--*********************************************************************************************************
--           MODIFICATIONS
--** take from 236.[dbo].[usp_PurgeClosedCustomers and merge with this 232 version
--12/30/2010

--d.irish 6/3/2011	ADD left outer join to include those records that need to be deleted & don't have history

CREATE PROCEDURE [dbo].[usp_PurgeClosedCustomers]  
	@Production_Flag	char(1), 
	@DateDeleted		datetime,
	@TipFirst			varchar(3) = null

AS

Declare @SQLDynamic nvarchar(4000)
declare @SQL		nvarchar(4000)
Declare @DBName	varchar(50)

Declare @Tipnumber 	char(15)

-- executed from 236_M01_ImportToStage.dtsx
----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------

--========================================================================================================
--note: we don;'t want to delete from stageing or regular (affiliat,customer etc..)
-- table as the 'importTostage' step since, sometimes this has to be ru-run.
--  deletes can happen in the postToProduction step since customer has given the "ok" to process
--========================================================================================================
Begin

	truncate table dbo.impcustomer_Purge

    if object_id('tempdb..#oot') is not null
	   drop table #oot

    if object_id('tempdb..#optoutcards') is not null
	   drop table #optoutcards



--items in dbo.impCustomer_Purge_Pending have been there since last month
-- or more & couldn't be purged during last month because of current history etc...
-- so lets try to purge this month
--  dbo.impCustomer_Purge table is used to hold possible purges or deletes for this month
	insert into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1
	from dbo.impCustomer_Purge_Pending ipp 
	

	truncate table dbo.impCustomer_Purge_Pending
	

 
  --===================================   
	-- Add in replacement cards for tips that are in the OptOut table.
	-- These are cases where FI is still sending us the customers even
	-- though they opted-out.  If the cards are not added into the optout table,
	-- the account will be deleted, and then re-added back in - causing monthly audit errors.
	 
	
	-- create temp table of tip# and lastname
	create table #oot
		(tipnumber		varchar(15) primary key,
		 lastname			varchar(50))

	insert into #oot
	select distinct tipnumber, oot.lastname
	from rewardsnow.dbo.optouttracking oot
	where left(tipnumber,3) = @tipfirst


	-- create temp table of card numbers in opt out table
	create table #optoutcards
		(acctid		varchar(25))
		
	insert into #optoutcards
	select distinct acctid
	from rewardsnow.dbo.optouttracking oot
     where left(tipnumber,3) = @tipfirst


	-- Add in the replacement cards
	insert into rewardsnow.dbo.optouttracking
	(TipPrefix, TIPNUMBER, ACCTID, FIRSTNAME, LASTNAME, OPTOUTDATE, OPTOUTSOURCE, OPTOUTPOSTED)

	select left(imp.tipnumber,3) tipprefix, imp.tipnumber,CreditCardNumber, 
			 name1, oot.lastname, @DateDeleted optoutdate, 'PROCESSING' optoutsource, @DateDeleted
	from dbo.CustomerImportScrubbed imp join #oot oot  		on imp.tipnumber = oot.tipnumber
    join dbo.TransactionImportScrubbed tis on tis.AccountNumber = oot.tipnumber
	left outer join #optoutcards ooc  	   on tis.AccountNumber = ooc.acctid
	where ooc.acctid is null
 --===================================
	
	-- Get all the opt outs to delete immediately 
	Insert Into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	Select TipNumber, AcctId, FirstName
	from rewardsnow.dbo.OptOutTracking oot 
     where left(oot.tipnumber,3) = @tipfirst and
		  oot.tipnumber not in (select distinct tipnumber 
							from dbo.history 
							where histdate > @DateDeleted and trancode not in ('RQ'))
	
	--
	-- For accounts with activity after the monthend date, put them into purge_pending
	-- then remove them from purge. The accounts in purge_pending will attempt to be
	-- purged next month.
	--
	insert into dbo.impcustomer_purge_pending
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select distinct pg.dim_impcustomer_tipnumber, pg.dim_impcustomer_acctnum, pg.dim_impcustomer_name1
	from dbo.impcustomer_purge pg
	where pg.dim_impcustomer_tipnumber in (select tipnumber from dbo.history_stage his 
					   where histdate > @DateDeleted
					   and trancode != 'RQ')

	delete pg
	from dbo.impcustomer_purge pg join dbo.impcustomer_purge_pending pp
		on pg.dim_impcustomer_tipnumber = pp.dim_impcustomer_tipnumber


	--
	-- Begin removing purged tips from import tables and staging tables
	--

	delete cus
	from dbo.CustomerImportScrubbed cus join dbo.impCustomer_Purge prg
		on cus.tipnumber = prg.dim_impcustomer_tipnumber

	delete his
	from dbo.history_stage his join dbo.impCustomer_Purge dlt
		on his.tipnumber = dlt.dim_impcustomer_tipnumber


	delete aff
	from dbo.affiliat_stage aff join dbo.impCustomer_Purge dlt
		on aff.tipnumber = dlt.dim_impcustomer_tipnumber


	delete cus
	from dbo.customer_stage cus join dbo.impCustomer_Purge dlt
		on cus.tipnumber = dlt.dim_impcustomer_tipnumber


End

--executed from  236_M02_Post2Production.dtsx
----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin


	set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

	-- copy any impCustomer_Purge_pending into impCustomer_Purge
	 
	Insert into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_name1
	from dbo.impCustomer_Purge_Pending
	where left(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Clear impCustomer_Purge_Pending 
	delete from dbo.impCustomer_Purge_Pending where left(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_Name1)
	select distinct imp.dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.dim_impcustomer_tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	'RQ' != his.trancode
		where left(imp.dim_impcustomer_tipnumber,3) = @TipFirst
		
		

--  load  dbo.impCustomer_Purge staging table of all customers in CUSTOMER_Pending_Delete table for 3+ months

--d.irish 6/3/2011			
	INSERT INTO   dbo.impCustomer_Purge
        (dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
     SELECT  distinct pen.tipnumber,aff.ACCTID,pen.acctname1 
     from dbo.CUSTOMER_Pending_Delete pen 
     join dbo.AFFILIAT aff on pen.tipnumber = aff.TIPNUMBER
     left outer join  dbo.history his on pen.tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	'RQ' != his.trancode
	 where left(pen.tipnumber,3) = @tipfirst  
     and datediff(month,pen.DATEADDED,@DateDeleted)  >=  3       
     
     
     
	if exists(select 1 from dbo.sysobjects where name = 'wrkPurgeTips' and xtype = 'U')
		drop table dbo.wrkPurgeTips
	
	create table dbo.wrkPurgeTips (TipNumber varchar(15) primary key)

	insert into dbo.wrkPurgeTips
	select distinct dim_impcustomer_tipnumber from dbo.impcustomer_purge


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	Delete imp
		from dbo.wrkPurgeTips imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @DateDeleted
			and	his.trancode != 'RQ'
		where left(imp.tipnumber,3) = @TipFirst


	-- Insert customer to customerdeleted
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from dbo.Customer c join dbo.wrkPurgeTips prg 
	   on c.tipnumber = prg.tipnumber
	where left(c.tipnumber,3) = @tipfirst 


	-- Insert affiliat to affiliatdeleted 
	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
	from dbo.Affiliat aff join dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber
	where left(aff.tipnumber,3) = @tipfirst 

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from dbo.History H join  dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber
	where left(h.tipnumber,3) = @tipfirst 

	-- Delete records from History 
	Delete h
	from dbo.History h join  dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join  dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber

	-- Delete from customer 
	Delete c
	from dbo.Customer c join  dbo.wrkPurgeTips prg
		on c.tipnumber = prg.tipnumber

	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = 'C'
	from dbo.customer c join dbo.impCustomer_Purge_Pending prg
		on c.tipnumber = prg.dim_impcustomer_tipnumber
		
 -- Delete from CUSTOMER_Pending_Delete 
	Delete c
	from dbo.CUSTOMER_Pending_Delete c join  dbo.wrkPurgeTips prg
		on c.tipnumber = prg.tipnumber
		



End




GO


