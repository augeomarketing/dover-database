USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 01/06/2011 16:34:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_WebCustomerAccountLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_WebCustomerAccountLoad]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 01/06/2011 16:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_WebCustomerAccountLoad]

AS



BEGIN TRY
	BEGIN TRANSACTION  --- START
	
	
delete from dbo.web_account
delete from dbo.web_customer


insert into dbo.web_customer
	(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
	 EarnedBalance, Redeemed, AvailableBal, Status, city, state)
select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
		address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
from dbo.customer
Where Status in ('A','S')
 




insert into dbo.web_account (TipNumber, LastName, LastSix, SSNLast4)
--select aff.tipnumber,  aff.lastname, cus.Misc4  , cus.Misc2 
select aff.tipnumber,  aff.lastname,  RIGHT(aff.ACCTID,6) , cus.Misc2 
from dbo.affiliat aff join dbo.customer cus
	on aff.tipnumber = cus.tipnumber
 Where Status in ('A','S')
  order by aff.TIPNUMBER
  
  


-- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH

GO


