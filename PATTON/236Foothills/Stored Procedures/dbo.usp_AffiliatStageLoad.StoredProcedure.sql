USE [236]
GO
/****** Object:  StoredProcedure [dbo].[usp_AffiliatStageLoad_test]    Script Date: 05/03/2012 09:00:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[usp_AffiliatStageLoad]   @MonthEnd datetime  AS 
 
 declare @tip				      varchar(15)
 declare @newAcctID				  varchar(16)
 declare @oldAcctID				  varchar(6)
 declare @LastSix  				  varchar(6)
 /* modifications
 d.irish 12/22/2010 modify line  on insert of new affiliats since there could be more than 1 card for a customer
 dirish 12/22/2011 prevent full length pan from being deleted
 dirish 5/3/12 foothills may send partial pan with no previous full pan...or they
 could send us a partial pan AFTER they send us full pan...modify this proc to account for partial pan after the fact
 */
  
--print 'first update existing customers'
/* Update Existing Customers         */
Update stg
	set LastName = cis.LastName1
	--   , CustID = cis.CustomerId 
	from dbo.AFFILIAT_Stage stg join dbo.CustomerImportScrubbed   cis
	on (stg.TIPNUMBER = cis.TIPNUMBER  )
	join dbo.TransactionImportScrubbed  tis
	on	(stg.CustID = tis.CustomerId  )
	 

/************ Insert New Accounts into Affiliat Stage  ***********/
--so , when there is only a customer record in file (no matching transaction) an affiliat
-- record will be creating using the lastSix from the card in the customer file.But if an
--existing transaction record exists, create affililat with full pan
--print ' update AFFILIAT_Stage'
 insert  [dbo].[AFFILIAT_Stage]
      ([ACCTID],[TIPNUMBER],[AcctType],[DATEADDED],[SECID]
      ,[AcctStatus],[AcctTypeDesc],[LastName],[YTDEarned],[CustID])
   select DISTINCT COALESCE(tis.AccountNumber,imp.CreditCardNumber) as AcctID, imp.TipNumber, 'Credit',@monthend, 
'NEW','A', 'Active', imp.lastname1, 0, imp.CustomerId
from  [236].dbo.CustomerImportScrubbed imp
  left outer join [236].dbo.TransactionImportScrubbed tis on tis.CustomerId = imp.CustomerId
-- d.irish 12/22/2010 comment out line below since there could be more than 1 card for a customer
--where imp.CustomerId not in (select cs.CustID from [236].dbo.AFFILIAT_Stage cs)

-- d.irish 2/24/2011 
--where tis.AccountNumber not in (select cs.ACCTID from [236].dbo.AFFILIAT_Stage cs)
  where COALESCE(tis.AccountNumber,imp.CreditCardNumber) not in (select cs.ACCTID from [236].dbo.AFFILIAT_Stage cs) 
  
  --dirish 5/3/12 begin
  --use this instead of cursor
  --delete partial pan only if full pan for tip already exists
  -- allow partial pan that comes in in customer record with no transaction
  --recrod and therefore no full card(pan).
 DELETE   s1
from [dbo].[AFFILIAT_Stage] s1
join [dbo].[AFFILIAT_Stage] s2 on s1.ACCTID = s2.ACCTID and s1.TIPNUMBER = s2.TIPNUMBER
where   RIGHT(s1.acctid,6) =  s2.acctid 
and len(s2.ACCTID) = 6
and s2.ACCTID not in ( select ACCTID from dbo.AFFILIAT)
 and s1.TIPNUMBER in (select TIPNUMBER from AFFILIAT)
 
 -- --dirish 5/3/12 end
  
 --dirish 5/3/12 begin   get rid of cursor, also AFFILIAT_Pending_Delete not being used anywhere
----/************ fix duplicate Accounts in Affiliat Stage  ***********/
------now find out if we have  2 affiliats, one with full card and one with partial card  
----declare cursorTIP cursor FAST_FORWARD for
----	select  TIPNUMBER  FROM  [dbo].[AFFILIAT_Stage]
----     group by TIPNUMBER
----     having COUNT(*) > 1
 
----open cursorTIP
----fetch next from cursorTIP into @TIP

----while @@FETCH_STATUS = 0
----BEGIN 
------since there can be more than 1 card per tip, we want to find the partial acctid (last6) that matches
------ new new record with full acctid

------dirish 12/22/2011 add line below
----set @oldAcctID = ''
----set @newAcctID = ''
 
----  print 'tip = ' +     @tip  
---- print 'find old and new acct ids'
----     select @oldAcctID =   (select   RIGHT(acctid,6)  FROM  [dbo].[AFFILIAT_Stage]   
----     where TIPNUMBER = @tip  group by RIGHT(acctid,6)  having COUNT(*) > 1  )


----     select @newAcctID  = (select acctid from  [dbo].[AFFILIAT_Stage]  where TIPNUMBER = @tip
----      and RIGHT(acctid,6) =  @oldAcctID and LEN(ACCTID) > 10 )
       
----       -- delete affiliat_stage with partial PAN
----		delete [236].[dbo].AFFILIAT_Stage
----		where tipnumber = @tip
----		and ACCTID = @oldAcctID
----		and ACCTID = RIGHT(@newacctid,6)
----		--dirish 12/22/2011 add line below
----		and LEN(ACCTID) <= 6
		 
----  --insert new replacement record with full pan
----		INSERT INTO [236].[dbo].[AFFILIAT_Pending_Delete]
----           (ACCTID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus
----           ,AcctTypeDesc,LastName,YTDEarned,CustID)
----		SELECT @newAcctID,TIPNUMBER,AcctType,DATEADDED,SECID,AcctStatus
----           ,AcctTypeDesc,LastName,YTDEarned,CustID
----		FROM [236].[dbo].[AFFILIAT_Pending_Delete]
----		WHERE TIPNUMBER = @TIP  
----		AND ACCTID = @oldAcctID
			
---- --delete old  record ( with partial pan)
----		DELETE [236].[dbo].[AFFILIAT_Pending_Delete]
----		WHERE TIPNUMBER = @tip
----		and ACCTID = @oldAcctID
----		and ACCTID = RIGHT(@newacctid,6)
	
----	fetch next from cursorTIP into @TIP
----END

------print 'done'
----close cursorTIP

----deallocate cursorTIP  
--dirish 5/3/12 end

