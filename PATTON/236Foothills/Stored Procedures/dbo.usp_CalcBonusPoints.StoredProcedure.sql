USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_CalcBonusPoints]    Script Date: 01/14/2011 12:06:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CalcBonusPoints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CalcBonusPoints]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_CalcBonusPoints]    Script Date: 01/14/2011 12:06:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_CalcBonusPoints]    @DateAdded datetime
AS
 
  -- modifications  -----
  -- d.irish 12/5/2010 - allow for bonus points to be alloted to the correct card if there is more than 1
  -- d.irish 1/14/2010 - add date check for on-line registration bonus points ...they will end 1/31/2011
--
-- Now add in bonuses
--
 --NORMALLY EARNED POINTS + THIS CALCULATION WILL RESULTS IN DOUBLE BONUS POINTS
--  

Insert dbo.TransStandard 
(TIPNumber, TranDate, AcctID, TranCode, TranNum,TranAmt, points, TranType, Ratio, CrdActvlDt  ) 

select af.TIPNUMBER, tis.DateLastActivity, af.ACCTID, bt.dim_BonusType_TranCode, 
		1, tis.TransactionAmount,
		cast(ROUND((tis.TransactionAmount * bp.dim_BonusProgram_PointMultiplier),0) as int) as bonusPoints,
    tt.Description, tt.ratio, tis.DateLastActivity
from dbo.AFFILIAT_Stage af join dbo.trantype tt 
    on 'BI' = tt.trancode
    join dbo.TransactionImportScrubbed tis on tis.CustomerId = af.CustID
   			and tis.AccountNumber = af.ACCTID   --12/5/2010 modification

join dbo.bonustype bt 	on bt.dim_BonusType_TranCode = tt.trancode	
join dbo.bonusprogram bp 	on bt.sid_BonusType_Id = bp.sid_BonusType_Id
where rtrim(ltrim(tis.TransactionCode)) = 'P'   -- only calculate bonus points on purchases
and tis.DateLastActivity >=   bp.dim_BonusProgram_EffectiveDate  
and tis.DateLastActivity <= bp.dim_BonusProgram_ExpirationDate
 
 
 --- add bonus points for on-line registrations 
 
  
    BEGIN

	   create table #registrations
	   (tipnumber		   varchar(15) primary key,
	    bonustype          varchar(2),
	    regdate			    datetime)

	   -- Get on-line registrations  
	   insert into #registrations
	   select tipnumber,'BR' as bonustype,regdate
	   from [RN1].[Foothill].[dbo].[1security]
	   where RegDate is not null
	   and password is not null
	   
	   -- Remove tips from the temp table if they have already received the registration bonus
	   delete tmp
	   from #registrations tmp join dbo.history h
		  on tmp.tipnumber = h.tipnumber
	   where trancode = 'BR'
	  
 

	   -- Now add the bonus transactions to transstandard
	   Insert dbo.TransStandard 
	    (TIPNumber, TranDate, AcctID, TranCode,
	     TranNum,TranAmt,Points,  TranType, Ratio  ) 
	   select r.tipnumber, r.regdate, stg.ACCTID  , bt.dim_BonusType_TranCode, 
	   1,0, bp.dim_BonusProgram_BonusPoints, bt.dim_BonusType_Description, 1
	   from #registrations r
	   join dbo.Bonustype bt on  bt.dim_BonusType_TranCode = r.bonustype
	   join dbo.BonusProgram bp on  bp.sid_BonusType_Id = bt.sid_BonusType_Id
	   join dbo.AFFILIAT_Stage stg on (stg.TIPNUMBER = r.tipnumber
	         and stg.AcctType = 'Credit')
	   where --d.irish 1/14/2011 add date check
		  @DateAdded >=   bp.dim_BonusProgram_EffectiveDate  
		and @DateAdded <= bp.dim_BonusProgram_ExpirationDate

    END

 

GO


