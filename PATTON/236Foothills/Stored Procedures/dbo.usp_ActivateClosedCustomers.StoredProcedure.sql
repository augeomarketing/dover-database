USE [236]
GO
/****** Object:  StoredProcedure [dbo].[usp_ActivateClosedCustomers]    Script Date: 10/05/2010 11:40:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_ActivateClosedCustomers]    
 @TipFirst		    varchar(3)
 
AS

/* If a customer was previously closed & closed for less than 90 days, it;ll
be in this table.  so if has since been activated IN THIS RUN
, remove it from this table .
this will re-activate any status otherthan closed (active or pending)   */
  
  
  DELETE  FROM CUSTOMER_Pending_Delete
  FROM CUSTOMER_Pending_Delete   CPD
  INNER JOIN  dbo.CustomerImportScrubbed CIS
  ON CPD.CUSTID = CIS.CustomerId
   where  ltrim(RTRIM(CIS.StatusCode)) <> 'C'  
   and SUBSTRING(CIS.TipNumber,1,3) = @TipFirst
  
  