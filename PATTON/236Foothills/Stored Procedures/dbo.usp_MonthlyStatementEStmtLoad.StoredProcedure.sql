USE [236]
GO
/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementEStmtLoad]    Script Date: 09/30/2010 09:21:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[usp_MonthlyStatementEStmtLoad]
    @EndDate		    datetime

AS


select 
    msf.Tipnumber as Tip, 
    Acctname1, 
    PointsBegin as PntBeg, 
    PointsEnd as PntEnd,
    PointsPurchasedCR + PointsPurchasedDB as PntPrchs, 
    PointsBonus as PntBonus, 
    PointsAdded as PntAdd,
    PointsIncreased as PntIncrs, 
    PointsRedeemed as PntRedem,
    PointsReturnedCR + PointsReturnedDB as PntRetrn,
    PointsSubtracted as PntSubtr, 
    PointsDecreased as PntDecrs
from dbo.Monthly_Statement_File msf 


/*

exec usp_MonthlyStatementEstmtLoad '02/28/2010'

*/

