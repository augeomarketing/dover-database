USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_WelcomeKitLoad]    Script Date: 12/17/2010 10:45:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_WelcomeKitLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_WelcomeKitLoad]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_WelcomeKitLoad]    Script Date: 12/17/2010 10:45:00 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO




CREATE PROCEDURE [dbo].[usp_WelcomeKitLoad] 
	   @EndDate datetime


AS 
declare @kitscreated    int



BEGIN TRY
	BEGIN TRANSACTION  --- START

	
Truncate Table dbo.Welcomekit 

insert into dbo.Welcomekit 
SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
		ADDRESS2, ADDRESS3, City, State, ZipCode 
FROM dbo.customer 
WHERE (Year(DATEADDED) = Year(@EndDate)
	AND Month(DATEADDED) = Month(@EndDate)
	AND Upper(STATUS) <> 'C') 


 select COUNT(*) from dbo.Welcomekit 



-- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH
 

GO


