USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_CalcCustomerPoints]    Script Date: 10/11/2010 16:54:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CalcCustomerPoints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CalcCustomerPoints]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_CalcCustomerPoints]    Script Date: 10/11/2010 16:54:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[usp_CalcCustomerPoints]
    @TipFirst		    varchar(3)

as



/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update dbo.Customer_Stage 
	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
		RunAvailable		= isnull(RunAvailable, 0)
		where TIPFIRST = @TipFirst


Update C
	Set	RunAvailable  = RunAvailable + v.Points,
		RunAvaliableNew = v.Points 
From dbo.Customer_Stage C join dbo.vw_histpoints V 
	on C.Tipnumber = V.Tipnumber
    where TIPFIRST = @TipFirst

GO


