USE [236]
GO
/****** Object:  StoredProcedure [dbo].[usp_CustomerStageLoad]    Script Date: 11/08/2010 09:41:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_CustomerStageLoad] @EndDate DateTime AS

set nocount on

declare @tipnumber		varchar(15)

declare @sql			nvarchar(4000)
declare @tip			nvarchar(15)
declare @ctr			int = 0
declare @name			varchar(40) = ''



/* Update Existing Customers                                         */
Update cstg
	set	lastname		= ltrim(rtrim(imp.lastname1)),
		acctname1		= ltrim(rtrim(imp.Name1)),
		acctname2		= ltrim(rtrim(imp.Name2)),
		address1		= ltrim(rtrim(imp.Address1)),
		address2		= ltrim(rtrim(imp.Address2)),
		address4		= ltrim(rtrim(imp.City)) + ltrim(rtrim(imp.statecode)) + ltrim(rtrim(imp.zipcode)), 
		city			= ltrim(rtrim(imp.City)),
		state		= ltrim(rtrim(imp.StateCode)),
		zipcode		= ltrim(rtrim(imp.zipcode)),
		homephone		= ltrim(rtrim(imp.homephone)),
		workphone		= ltrim(rtrim(imp.WorkPhone)),
		misc1		=  LTRIM(RTRIM(imp.customerid)),
		misc2		=  LTRIM(RTRIM(imp.Last4SSN)),
		misc3		=  '',
		misc4		=  LTRIM(RTRIM(imp.CreditCardNumber)),
		status		=  LTRIM(rtrim(imp.statuscode)),
		businessflag = ltrim(rtrim(imp.businessflag)),
		employeeflag = ltrim(rtrim(imp.employeeflag))

from dbo.Customer_Stage cstg join dbo.CustomerImportScrubbed imp
on	cstg.tipnumber = imp.tipnumber
 
 

/* Add New Customers                                                      */

declare csrNewCustomers cursor FAST_FORWARD for
	select distinct imp.tipnumber
	from dbo.CustomerImportScrubbed imp left outer join dbo.Customer_Stage stg
		on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null


open csrNewCustomers

fetch next from csrNewCustomers into @tipnumber

while @@FETCH_STATUS = 0
BEGIN

	Insert into Customer_Stage
		(tipnumber, tipfirst, tiplast, lastname,
		 acctname1, acctname2, address1, address2, address4,
		 city, 
		 state, 
		  zipcode, homephone, workphone,
		 misc1, misc2,misc3,Misc4,
		 status,EmployeeFlag,BusinessFlag, dateadded, 
		 runavailable, runbalance, runredeemed, runavaliableNew)

	select top 1 imp.TIPNUMBER tipnumber, left(imp.TIPNUMBER,3) tipfirst, right(ltrim(rtrim(imp.TIPNUMBER)),12) tiplast, left(ltrim(rtrim(imp.LastName1)),40) lastname,
		  ltrim(rtrim(imp.Name1)) acctname1, ltrim(rtrim(imp.Name2)) acctname2,
		  imp.address1, imp.address2,
		   ltrim(rtrim(imp.City)) + ltrim(rtrim(imp.statecode)) + ltrim(rtrim(imp.zipcode)),
		   ltrim(rtrim(imp.City)),
			ltrim(rtrim(imp.statecode)),
		  ltrim(rtrim(imp.zipcode)), ltrim(rtrim(imp.homephone)), ltrim(rtrim(imp.workphone)), 
		   LTRIM(RTRIM(imp.customerid)), LTRIM(RTRIM(imp.Last4SSN)), '',LTRIM(RTRIM(imp.CreditCardNumber)),
		   --'A', ltrim(rtrim(imp.employeeflag)), ltrim(rtrim(imp.businessflag)), @EndDate,
		   LTRIM(rtrim(imp.statuscode)), ltrim(rtrim(imp.employeeflag)), ltrim(rtrim(imp.businessflag)), @EndDate,
		    0, 0, 0, 0

	from dbo.CustomerImportScrubbed imp left outer join dbo.Customer_Stage stg
	on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null and imp.tipnumber = @tipnumber 

	fetch next from csrNewCustomers into @tipnumber
END

close csrNewCustomers

deallocate csrNewCustomers


