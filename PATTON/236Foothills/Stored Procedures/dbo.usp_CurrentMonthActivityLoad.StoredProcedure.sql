USE [236]
GO
DROP PROCEDURE [dbo].[usp_CurrentMonthActivityLoad]
GO
/****** Object:  StoredProcedure [dbo].[usp_CurrentMonthActivityLoad]    Script Date: 09/23/2010 09:05:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[usp_CurrentMonthActivityLoad] @EndDateParm datetime
AS


---THIS PROC BACKS OUT ANY ACTIVITY THAT OCCURRED FROM MONTH-END TO DATE THIS PROC IS RUN




BEGIN TRY
	BEGIN TRANSACTION  --- START
	
	
Declare @EndDate DateTime

set @Enddate = dateadd(d, 1, @enddateparm)
set @enddate = dateadd(s, -1, @enddate)

delete from dbo.Current_Month_Activity


insert into dbo.Current_Month_Activity 
(Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 
from dbo.Customer_Stage
--where status = 'A'


/* Load the current activity table with increases for the current month         */
update dbo.Current_Month_Activity
	set increases=(select sum(points) 
				from dbo.history_Stage 
				where histdate>@enddate and ratio='1'
				and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

where exists(select * from history_Stage where histdate>@enddate and ratio='1'
			and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )


/* Load the current activity table with decreases for the current month         */
update dbo.Current_Month_Activity
	set decreases=(select sum(points) 
				from dbo.history_Stage 
				where histdate>@enddate and ratio='-1'
				 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

where exists(select * from history_Stage where histdate>@enddate and ratio='-1'
			and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )



/* Load the calculate the adjusted ending balance        */
update dbo.Current_Month_Activity
	set adjustedendingpoints=endingpoints - increases + decreases


 -- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH
