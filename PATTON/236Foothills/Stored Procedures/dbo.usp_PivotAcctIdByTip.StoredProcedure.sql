USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_PivotAcctIdByTip]    Script Date: 01/07/2011 09:41:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PivotAcctIdByTip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PivotAcctIdByTip]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_PivotAcctIdByTip]    Script Date: 01/07/2011 09:41:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





create procedure [dbo].[usp_PivotAcctIdByTip]
    
AS



    declare @sql		    nvarchar(max) = ''
    declare @columnnames    varchar(max) = ''

    declare @ctr		    int = 1
    declare @maxcardsontip  int

    set nocount on
    
       truncate table  dbo.wrkTipsAndCards

        set @maxcardsontip = 10

        -- Build string of column names for the pivot for clause
        -- string will look something like:
        -- '[Card_1], [Card_2],....'
        --
        while @ctr <= @maxcardsontip
        BEGIN
	       set @columnnames = @columnnames + '[Card_' + cast(@ctr as varchar) + ']' + ', '
	       set @ctr += 1
        END

        -- Now get rid of trailing ', ' from the end of the string
        set @columnnames = left(@columnnames, datalength(@columnnames)-2)

        print @columnnames

 
 set @sql   =  'insert   dbo.wrkTipsAndCards (tipnumber,card1,card2,card3,card4,card5,card6,card7,card8,card9,card10)
                select *  
			     from (select tipnumber, acctid, ''Card_'' + cast(row_number()  over(partition by tipnumber order by tipnumber) as varchar) rownbr 
				      from dbo.affiliat_stage
				      group by tipnumber, acctid) aff
			     pivot
			     ( max(acctid)
				    for rownbr in (<columnnames>)
			     ) as pvttable'
			     
	 

        -- Now replace the '<columnnames>' with the @columnnames string built in the while loop
        set @sql = replace(@sql, '<columnnames>', @columnnames)

       print @sql

         exec sp_executesql @sql
 



GO


