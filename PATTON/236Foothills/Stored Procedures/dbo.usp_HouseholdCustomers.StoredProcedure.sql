USE [236]
GO
DROP  PROCEDURE [dbo].[usp_HouseholdCustomers]
GO
/****** Object:  StoredProcedure [dbo].[usp_HouseholdCustomers]    Script Date: 09/20/2010 09:40:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_HouseholdCustomers]  AS

set nocount on

declare @tipnumber		varchar(15)

--declare @sql			nvarchar(4000)
declare @tip			nvarchar(15)
declare @ctr			int = 0
declare @name			varchar(40) = ''




BEGIN TRY
	BEGIN TRANSACTION  --- START
--**********************************************************************************************
--**
--**  household names
--**
--**********************************************************************************************
print 'create temp tables'

create table #names
	(tipnumber			varchar(15) primary key,
	 Name1				varchar(40),
	 Name2				varchar(40),
	 Name3				varchar(40),
	 Name4				varchar(40),
	 Name5				varchar(40),
	 Name6				varchar(40))

create table #tipnames
	(Name				varchar(40))

create table #tipnames2
	(Name				varchar(40))


declare csr cursor fast_forward for
	select distinct  TipNumber
	from dbo.CustomerImportScrubbed
    where StatusCode <> 'C'
    and TipNumber is not null
    
    
open csr
print 'start cursor'
fetch next from csr into @tip

while @@FETCH_STATUS = 0
BEGIN
print 'found records'
	truncate table #tipnames
	truncate table #tipnames2
	
	--
	-- Get all the distinct names for the tip
	insert into #tipnames
	select distinct Name1
	from  dbo.CustomerImportScrubbed
	where   TipNumber = @tip
	
	insert into #tipnames
	select distinct Name2
	from dbo.CustomerImportScrubbed
	where TipNumber = @tip

	-- Get distinct names into second work table
	insert into #tipnames2
	select distinct name
	from #tipnames
	
	
	insert into #names 
	(tipnumber)
	values(@tip)
	
	set @ctr = 0
	while @ctr <= 6 
	BEGIN
		set @ctr = @ctr + 1
		
		select @name = name
		from #tipnames2
		

		if @ctr = 1  update #names set name1 = @name where tipnumber = @tip
		if @ctr = 2  update #names set name2 = @name where tipnumber = @tip
		if @ctr = 3  update #names set name3 = @name where tipnumber = @tip
		if @ctr = 4  update #names set name4 = @name where tipnumber = @tip
		if @ctr = 5  update #names set name5 = @name where tipnumber = @tip
		if @ctr = 6  update #names set name6 = @name where tipnumber = @tip
		
		delete from #tipnames2 where name = @name
	END
	
	fetch next from csr into @tip
END

close csr
deallocate  csr
print 'done with cursor, now update customer_stage'

update stg
	set acctname1 = name1,
		acctname2 = name2,
		acctname3 = name3,
		acctname4 = name4,
		acctname5 = name5,
		acctname6 = name6
from #names tmp join dbo.customer_stage stg
	on tmp.tipnumber = stg.tipnumber

print 'end of householding'


 -- If we reach here, success!
   COMMIT


END TRY
--
--IF ANY ABOVE SQL CAUSES ERROR..WE DON'T WANT TO COMMIT ANYTHING...SO GO TO ERROR 'CATCH'
BEGIN CATCH
-- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK
-- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)

END CATCH
