USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyAuditValidation]    Script Date: 01/10/2011 12:04:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MonthlyAuditValidation]
GO

USE [236]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyAuditValidation]    Script Date: 01/10/2011 12:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_MonthlyAuditValidation]
    @errorrows	 int OUTPUT

AS


/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
delete from dbo.monthly_audit_errorfile


insert into dbo.monthly_audit_errorfile
(Tipnumber, PointsBegin, PointsEnd, PointsPurchasedCR, PointsPurchasedDB, PointsBonus, PointsAdded, 
 PointsIncreased, PointsRedeemed, PointsReturnedCR, PointsReturnedDB, PointsSubtracted, PointsDecreased, Errormsg, Currentend)

select msf.Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, 
	   pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased,'Ending Balances do not match',
	   cma.adjustedendingpoints
		  
from dbo.Monthly_Statement_File msf join dbo.current_month_activity cma
    on msf.tipnumber = cma.tipnumber
where msf.pointsend != cma.adjustedendingpoints
--set @errorrows = @@rowcount
 
 
 select   @errorrows  = COUNT(*)   from dbo.monthly_audit_errorfile
 print 'errors  = '  +     CONVERT(CHAR(6), @errorrows)

 

GO


