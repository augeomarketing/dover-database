USE [50UDesMoinesConsumer]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 09/24/2009 10:48:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
GO
