USE [594HarrisConsumer]
GO
/****** Object:  Table [dbo].[fixaccount]    Script Date: 01/12/2010 09:22:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixaccount]') AND type in (N'U'))
DROP TABLE [dbo].[fixaccount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixaccount]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[fixaccount](
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[fixaccount] ADD [patlastsix] [varchar](6) NULL
ALTER TABLE [dbo].[fixaccount] ADD [patlastfour] [varchar](4) NULL
END
GO
SET ANSI_PADDING OFF
GO
