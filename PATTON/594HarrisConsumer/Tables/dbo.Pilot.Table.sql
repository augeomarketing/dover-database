USE [594HarrisConsumer]
GO
/****** Object:  Table [dbo].[Pilot]    Script Date: 01/12/2010 09:22:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pilot]') AND type in (N'U'))
DROP TABLE [dbo].[Pilot]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Pilot]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Pilot](
	[Last ] [nvarchar](255) NULL,
	[First] [nvarchar](255) NULL,
	[Middle Initial] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[state ] [nvarchar](255) NULL,
	[zip] [float] NULL,
	[last six of card] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
