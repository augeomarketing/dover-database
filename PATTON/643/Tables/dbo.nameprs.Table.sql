USE [643]
GO
/****** Object:  Table [dbo].[nameprs]    Script Date: 01/03/2011 14:31:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nameprs]') AND type in (N'U'))
DROP TABLE [dbo].[nameprs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[nameprs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[nameprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[CSNA1] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[ACCTNO] [nvarchar](16) NULL,
	[CARDNO] [nvarchar](16) NULL
) ON [PRIMARY]
END
GO
