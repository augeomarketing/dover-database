USE [643]
GO
/****** Object:  Table [dbo].[StmtExp]    Script Date: 01/03/2011 14:31:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StmtExp]') AND type in (N'U'))
DROP TABLE [dbo].[StmtExp]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StmtExp]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [decimal](10, 0) NULL,
	[ENDBAL] [decimal](10, 0) NULL,
	[CCPURCHASE] [decimal](10, 0) NULL,
	[BONUS] [decimal](10, 0) NULL,
	[PNTADD] [decimal](10, 0) NULL,
	[PNTINCRS] [decimal](10, 0) NULL,
	[REDEEMED] [decimal](10, 0) NULL,
	[PNTRETRN] [decimal](10, 0) NULL,
	[PNTSUBTR] [decimal](10, 0) NULL,
	[PNTDECRS] [decimal](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
