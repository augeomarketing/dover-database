USE [643]
GO
/****** Object:  Table [dbo].[EStmt_Work]    Script Date: 01/03/2011 14:31:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Work]') AND type in (N'U'))
DROP TABLE [dbo].[EStmt_Work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EStmt_Work]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EStmt_Work](
	[Tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
