USE [643]
GO
/****** Object:  Table [dbo].[Account_Reference_Stage]    Script Date: 01/05/2011 10:40:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference_Stage](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
 CONSTRAINT [PK_Account_Reference_Stage] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC,
	[acctnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
