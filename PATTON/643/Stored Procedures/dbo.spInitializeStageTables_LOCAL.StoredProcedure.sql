USE [643]
GO
/****** Object:  StoredProcedure [dbo].[spInitializeStageTables_LOCAL]    Script Date: 01/03/2011 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInitializeStageTables_LOCAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInitializeStageTables_LOCAL]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInitializeStageTables_LOCAL]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/*  **************************************  */
/* FOR USE WHEN DEBUGGING ON LOCAL MACHINE ONLY*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[spInitializeStageTables_LOCAL] @TipFirst char(4), @MonthEnd char(20),  @spErrMsgr varchar(80) Output AS 
Declare  @DD char(2), @MM char(2), @YYYY char(4), @MonthBeg DateTime
Declare @dbName varchar(25)
Declare @SQLCmnd nvarchar(1000)
Declare @Row Int
set @DD = ''01''
set @MM = Month(@MonthEnd)
set @YYYY = Year(@MonthEnd)
set @MonthBeg = convert(datetime, @MM+''/''+@DD+''/''+@YYYY+'' 00:00:00:000'' )	
Declare @MonthBegChar char(20)
set @MonthBegChar = Convert(char(20), @MonthBeg, 120)
print @MonthBegChar 
set @spErrMsgr = ''no''
-- Clear the Stage tables
set @SQLCmnd = ''Truncate table Customer_Stage'' 
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Truncate table Affiliat_Stage''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Truncate table  History_Stage''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Truncate table  OneTimeBonuses_Stage''
Exec sp_executeSql @SQLCmnd 
-- Load the stage tables
set @SQLCmnd = ''Insert into  Customer_Stage Select * from  Customer''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Update  Customer_Stage Set  RunAvaliableNew = 0 ''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Insert into  Affiliat_Stage Select * from  Affiliat''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Insert into  History_stage Select * from History where Histdate >= '''''' + @MonthBegChar +''''''''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Update History_stage set SecId = ''''OLD'''' ''
Exec sp_executeSql @SQLCmnd 
set @SQLCmnd = ''Insert into OnetimeBonuses_Stage Select * from OnetimeBonuses''
Exec sp_executeSql @SQLCmnd
' 
END
GO
