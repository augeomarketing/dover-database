USE [643]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 11/04/2013 14:33:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyStatement]
GO

USE [643]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 11/04/2013 14:33:05 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
*/
/****************************/
/* 1/2012                   */
/* SEB001                   */
/* Add shopping fling codes */
/* Add PP code              */
/****************************/
/****************************/
/* 2/2012                   */
/* SEB002                   */
/* Add factor and calc for  */
/* dollars purch            */
/****************************/
/*******************************************************************************/
/* SEB005 6/12 Add code for Bonus Reversal BX  */
/*******************************************************************************/

/*******************************************************************************/
CREATE PROCEDURE [dbo].[spStageMonthlyStatement]  @StartDateParm char(10), @EndDateParm char(10)
AS 
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)
Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 
set @MonthBegin = month(Convert(datetime, @StartDate) )
/* Load the statement file from the customer table  */
delete from Monthly_Statement_File
insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer_Stage

/******************/
/* START SEB002   */   
/******************/
select tipnumber, MAX(acctid) as acctid
into #tmp 
from AFFILIAT_Stage
where acctstatus = 'A' 
group by TIPNUMBER

alter table #tmp
add factor numeric (4,2) 

update #tmp
set factor=1.00

update #tmp
set factor=isnull(r.factor,1.00)
from #tmp t join Relationship r on t.acctid=r.Acct

update Monthly_Statement_File
set factor = t.factor
from Monthly_Statement_File m join #tmp t on m.Tipnumber=t.tipnumber

update Monthly_Statement_File
set factor = 1.00
where factor is null or factor = 0.00

/******************/
/* END SEB002     */   
/******************/


/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File
set pointspurchasedCR =(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='63' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
and histdate>=@startdate and histdate<=@enddate and trancode='63'  )

/* Load the statmement file  with CREDIT returns            */
update Monthly_Statement_File
set pointsreturnedCR=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='33' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='33' )

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File
set pointspurchasedDB=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='67' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='67' )

/* Load the statmement file with DEBIT  returns            */
update Monthly_Statement_File
set pointsreturnedDB=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='37' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='37' )

/* Load the statmement file with DEBITbonuses            */
update Monthly_Statement_File
set pointsbonusDB=(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and ((trancode like 'B%' and trancode <> 'BX')or trancode= 'NW' or (trancode like 'F%' and trancode<>'FJ' and trancode<>'F0' and trancode<>'F9' ))  ) --SEB001
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and ((trancode like 'B%' and trancode <> 'BX')or trancode= 'NW' or (trancode like 'F%' and trancode<>'FJ' and trancode<>'F0' and trancode<>'F9' )))   --SEB001

/***********************/
/* START SEB005        */
/***********************/
update Monthly_Statement_File
set pointsbonusDB=pointsbonusDB + (select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode = 'BX' ) --SEB001
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode = 'BX' )   --SEB001
/***********************/
/* END SEB005        */
/***********************/

/* Load the statmement file with CREDIT bonuses            */
update Monthly_Statement_File
set pointsbonusCR=(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='FJ'  )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='FJ'  )

/****************/
/* Start SEB001 */
/****************/

/* Load the statmement file with Shopping Fling Bonuses */
update Monthly_Statement_File
set pointsbonusMN=(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ('F0', 'G0', 'H0')  )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ('F0', 'G0', 'H0') )

update Monthly_Statement_File
set pointsbonusMN=pointsbonusMN + (select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ('F9', 'G9', 'H9')  )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in ('F9', 'G9', 'H9') )

/****************/
/* End SEB001   */
/****************/

/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in('IE', 'PP') ) --SEB001
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode in('IE', 'PP') )  --SEB001

/* Add  DECREASED REDEEMED to adjustments     */
update Monthly_Statement_File
set pointsadded=pointsadded + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DR' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DR' )

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonusDB +  pointsbonusCR + pointsadded + pointsbonusMN

/* Load the statmement file with redemptions          */
UPDATE	Monthly_Statement_File
SET		pointsredeemed = (	select	sum(points*ratio*-1) 
							from	History_Stage 
							where	tipnumber = Monthly_Statement_File.tipnumber
								and	histdate >= @startdate and histdate <= @enddate 
								and (trancode like 'R%' or TRANCODE = 'IR')
						 )
WHERE exists (	select	* 
				from	History_Stage 
				where	tipnumber = Monthly_Statement_File.tipnumber 
					and	histdate >= @startdate and histdate <= @enddate 
					and (trancode like 'R%' or TRANCODE = 'IR')
			 )

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DE' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='DE' )

/* Add EP to  minus adjustments    */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='EP' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='EP' )

-- RDT 8/28/2007  Added expired Points 
/* Add expired Points */
update Monthly_Statement_File
set PointsExpire = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='XP' )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
 and histdate>=@startdate and histdate<=@enddate and trancode='XP' )

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire
/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate

/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the monthly statement table */
Update Monthly_Statement_file 
set acctid = a.acctid from affiliat_Stage a where Monthly_Statement_file.tipnumber = a.tipnumber

/******************/
/* START SEB002   */   
/******************/

update Monthly_Statement_file
set PurchDollars = round((pointspurchasedCR + pointspurchasedDB - pointsreturnedCR - pointsreturnedDB) / factor, 2 )

/******************/
/* END SEB002     */   
/******************/

GO


