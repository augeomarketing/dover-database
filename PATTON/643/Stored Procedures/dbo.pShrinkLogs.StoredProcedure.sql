USE [643]
GO
/****** Object:  StoredProcedure [dbo].[pShrinkLogs]    Script Date: 01/03/2011 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pShrinkLogs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pShrinkLogs]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pShrinkLogs]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[pShrinkLogs] @Tipfirst char(3)
as
declare @sDBName varchar(100), @logname varchar(100)
DECLARE cur_databases CURSOR FOR
	select name from master.dbo.sysdatabases where name not in (''tempdb'') and name like @Tipfirst 
	OPEN cur_databases 
	fetch next  from cur_databases into @sDBName
	
WHILE (@@FETCH_STATUS=0)
	BEGIN
	
		BACKUP LOG @sDBName WITH TRUNCATE_ONLY
		set @logname= ''['' + @sDBName + '']'' + ''_log''
		set @logname=  ''['' + @sDBName + '']'' + ''_log''
		exec (''use '' +  ''['' + @sDBName + '']'')
		EXEC (''DBCC SHRINKFILE  ( '' +  ''['' + @sDBName + '']'' + '', 500, TRUNCATEONLY)'')
		fetch next  from cur_databases into  @sDBName
	END
close cur_databases
deallocate cur_databases
' 
END
GO
