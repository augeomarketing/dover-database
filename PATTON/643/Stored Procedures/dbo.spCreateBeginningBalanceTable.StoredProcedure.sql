USE [643]
GO
/****** Object:  StoredProcedure [dbo].[spCreateBeginningBalanceTable]    Script Date: 01/03/2011 14:28:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateBeginningBalanceTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateBeginningBalanceTable]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateBeginningBalanceTable]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    This will Create a new Beginning_balance_table                          */
/*    using the tipnumber extracted from the customer table                   */
/*                                                                            */
/* BY:  B.QUINN                                                               */
/* DATE: 5/2007                                                               */
/* REVISION: 0                                                                */
/*                                                                            */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spCreateBeginningBalanceTable]  AS 
  
Declare @TipNumber char(15)
select tipnumber into custwktab1 from customer_stage
 
 
 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From custwktab1 
Open custfix_crsr  
/*                  */
Fetch custfix_crsr  
into  @tipnumber 
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
		
insert into beginning_balance_table   
 (tipnumber  
 ,monthbeg1   
 ,monthbeg2   
 ,monthbeg3  
 ,monthbeg4  
 ,monthbeg5  
 ,monthbeg6  
 ,monthbeg7  
 ,monthbeg8   
 ,monthbeg9   
 ,monthbeg10   
 ,monthbeg11   
 ,monthbeg12)   
values
   (@tipnumber
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0''
  ,''0'')
 
Fetch custfix_crsr  
into  @tipnumber 
	
END /*while */
drop table custwktab1 
GoTo EndPROC
Fetch_Error:
Print ''Fetch Error''
EndPROC:
close  custfix_crsr
deallocate  custfix_crsr
' 
END
GO
