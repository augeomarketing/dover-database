--insert into AcctType
--	SELECT AcctType, accttypedesc FROM [52JKroegerPersonalFinance].dbo.AcctType

--insert into Status
--	SELECT Status, Statusdescription FROM [52JKroegerPersonalFinance].dbo.Status

--insert into Reconcilliationout
--	SELECT field1 FROM [52JKroegerPersonalFinance].dbo.Reconcilliationout

--insert into Client
--	SELECT	'KPFA' as ClientCode, 'Kroger Personal Finance Accela' as ClientName, 'Kroger Personal Finance Accela' as Clientdescription,
--			'52U' as tipfirst, Address1, Address2, Address3, Address4, City, State, ZipCode, Phone1, Phone2, ContactPerson1, ContactPerson2,
--			ContactEmail1, ContactEmail2, '2011-08-01 00:00:00.000' as DateJoined, RNProgramName, TermsConditions, PointsUpdatedDT,
--			MinRedeemNeeded, TravelFlag, MerchandiseFlag, TravelIncMinPoints, MerchandiseBonusMinPoints, MaxPointsPerYear, PointExpirationYears,
--			'52U' as Clientid, Pass, ServerName, '52U' as dbname, UserName, Password, PointsExpire, '52U000000000000' as lasttipnumberused,
--			PointsExpireFrequencyCd, ClosedMonths
--	FROM	[52JKroegerPersonalFinance].dbo.Client

--insert into MetavanteWork.dbo.autoprocessdetail
--	SELECT	'52U' as tipfirst, '52U' as dbname1, '52U' as dbname2, NULL as sourcefilenamestring,
--			'T:\ProcessingBackups\52U.bak' as backupnamestring, '08/01/2011' as monthbeginingdate, 
--			'08/31/2011' as monthendingdate, 'KPFA' as rnclientcode, '0' as di_d, '0' as ca_id, '0' as pw_id,
--			'0' as ub_id, '0' as wk_id, '0' as qs_id, 'D' as [Debit/Credit], NULL as missingnameout, 
--			NULL as quarterbeginingdate, NULL as quarterendingdate

--insert into MetavanteWork.dbo.packagecodes
--	SELECT	'52U',0,0,10,1,0,0,0

--insert into MetavanteWork.dbo.DebitBins
--	Select '52U','400540001'
	
--insert into RewardsNow.dbo.dbprocessinfo
--	SELECT	'52U' as dbnumber, '52U' as dbnamepatton, dbnamenexl, 'N' as dbavailable, 
--			'KPFA' as ClientCode, 'Kroger Personal Finance Accela' as ClientName, programname,
--			dblocationpatton, dblocationnexl, pointexpirationyears, '2011-08-01 00:00:00.000' as DateJoined,
--			minredeemneeded, maxpointsperyear, '52U000000000000' as lasttipnumberused, pointsexpirefrequencycd,
--			closedmonths, welcomekitgroupname, generatewelcomekit, 'P' as sid_fiprodstatus_statuscode, IsStageModel,
--			ExtractGiftCards, ExtractCashBack, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum,
--			Merch, AirFee, Logo, Landing, TermsPage, FaqPage, EarnPage, Business, StatementDefault, StatementNum, 
--			CustomerServicePhone, ContactPerson1, ContactPerson2, ContactPhone1, ContactPhone2, ContactEmail1, ContactEmail2,
--			Address1, Address2, Address3, Address4, City, State, ZipCode, ServerName, TransferHistToRn1, CalcDailyExpire,
--			hasonlinebooking, VesdiaParticipant, AccessDevParticipant, ExpPointsDisplayPeriodOffset, ExpPointsDisplay			
--	FROM	rewardsnow.dbo.dbprocessinfo
--	WHERE	dbnumber = '52J'		

--insert into RewardsNow.dbo.RptCtlClients
--	SELECT	'Kroger Personal Finance Accela' as RNName, '52U' as ClientNum, 'Kroger Personal Finance Accela' as FormalName,
--			'52U' as ClientDBName, ClientDBLocation, OnlClientDBName, OnlClientDBLocation
--	FROM	RewardsNow.dbo.RptCtlClients
--	WHERE	ClientNum = '52J'
	
----------------------------------------------------------------------------------------------------------

--insert into AcctType
--	SELECT AcctType, accttypedesc FROM [52R].dbo.AcctType

--insert into Status
--	SELECT Status, Statusdescription FROM [52R] .dbo.Status

--insert into Reconcilliationout
--	SELECT field1 FROM [52R].dbo.Reconcilliationout

--insert into Client
--	SELECT	'RLPA' as ClientCode, 'Ralph''s Loyalty Program Accela' as ClientName, 'Ralph''s Loyalty Program Accela' as Clientdescription,
--			'52V' as tipfirst, Address1, Address2, Address3, Address4, City, State, ZipCode, Phone1, Phone2, ContactPerson1, ContactPerson2,
--			ContactEmail1, ContactEmail2, '2011-08-01 00:00:00.000' as DateJoined, RNProgramName, TermsConditions, PointsUpdatedDT,
--			MinRedeemNeeded, TravelFlag, MerchandiseFlag, TravelIncMinPoints, MerchandiseBonusMinPoints, MaxPointsPerYear, PointExpirationYears,
--			'52V' as Clientid, Pass, ServerName, '52V' as dbname, UserName, Password, PointsExpire, '52V000000000000' as lasttipnumberused,
--			PointsExpireFrequencyCd, ClosedMonths
--	FROM	[52R].dbo.Client

--insert into MetavanteWork.dbo.autoprocessdetail
--	SELECT	'52V' as tipfirst, '52V' as dbname1, '52V' as dbname2, NULL as sourcefilenamestring,
--			'T:\ProcessingBackups\52V.bak' as backupnamestring, '08/01/2011' as monthbeginingdate, 
--			'08/31/2011' as monthendingdate, 'RLPA' as rnclientcode, '0' as di_d, '0' as ca_id, '0' as pw_id,
--			'0' as ub_id, '0' as wk_id, '0' as qs_id, 'D' as [Debit/Credit], NULL as missingnameout, 
--			NULL as quarterbeginingdate, NULL as quarterendingdate

--insert into MetavanteWork.dbo.packagecodes
--	SELECT	'52V',0,0,10,1,0,0,0

--insert into MetavanteWork.dbo.DebitBins
--	Select '52V','400540002'	
	
--insert into RewardsNow.dbo.dbprocessinfo
--	SELECT	'52V' as dbnumber, '52V' as dbnamepatton, dbnamenexl, 'N' as dbavailable, 
--			'RLPA' as ClientCode, 'Ralph''s Loyalty Program Accela' as ClientName, programname,
--			dblocationpatton, dblocationnexl, pointexpirationyears, '2011-08-01 00:00:00.000' as DateJoined,
--			minredeemneeded, maxpointsperyear, '52V000000000000' as lasttipnumberused, pointsexpirefrequencycd,
--			closedmonths, welcomekitgroupname, generatewelcomekit, 'P' as sid_fiprodstatus_statuscode, IsStageModel,
--			ExtractGiftCards, ExtractCashBack, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum,
--			Merch, AirFee, Logo, Landing, TermsPage, FaqPage, EarnPage, Business, StatementDefault, StatementNum, 
--			CustomerServicePhone, ContactPerson1, ContactPerson2, ContactPhone1, ContactPhone2, ContactEmail1, ContactEmail2,
--			Address1, Address2, Address3, Address4, City, State, ZipCode, ServerName, TransferHistToRn1, CalcDailyExpire,
--			hasonlinebooking, VesdiaParticipant, AccessDevParticipant, ExpPointsDisplayPeriodOffset, ExpPointsDisplay			
--	FROM	rewardsnow.dbo.dbprocessinfo
--	WHERE	dbnumber = '52R'		

--insert into RewardsNow.dbo.RptCtlClients
--	SELECT	'Ralphs Accela' as RNName, '52V' as ClientNum, 'Ralph''s Loyalty Program Accela' as FormalName,
--			'52V' as ClientDBName, ClientDBLocation, OnlClientDBName, OnlClientDBLocation
--	FROM	RewardsNow.dbo.RptCtlClients
--	WHERE	ClientNum = '52R'

	