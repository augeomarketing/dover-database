SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spCREDIT_FixNames_RemoveApostrophes] 
AS

--parse first and last names out into their own fields
update DemographicIn_CREDIT
set 
FirstName= rtrim(ltrim(substring(rtrim(ltrim(substring(AcctName,charindex(',',AcctName)+1,len(AcctName) ))),1,charindex(' ',rtrim(ltrim(substring(AcctName,charindex(',',AcctName)+1,len(AcctName) ))))))),
LastName=rtrim(ltrim(substring(AcctName,1,charindex(',',AcctName)-1)))

--replace apostrophes with nothing
Update DemographicIn_CREDIT set 
[Address1]=replace([Address1],char(39), ''), 
[Address2]=replace([Address2],char(39), ''), 
CITY=replace(City,char(39), ''),
Firstname=replace(FirstName,char(39), ''), 
LastName=replace(LastName,char(39), '') 

--Update the value of RNAcctName
update DemographicIn_CREDIT
set RNAcctName=Firstname + ' ' + Lastname
GO
