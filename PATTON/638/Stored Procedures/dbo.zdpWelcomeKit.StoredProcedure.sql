SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zdpWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 
----------------------------------
--THIS IS TO ONLY BE USED FOR KITS GOING OUT A MONTH OR MORE AFTER POINTS START ACCRUING
----------------------------------
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

--set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, left(ZipCode,5) FROM  customer WHERE (DATEADDED <= @EndDate AND STATUS <> ''c'') '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate
GO
