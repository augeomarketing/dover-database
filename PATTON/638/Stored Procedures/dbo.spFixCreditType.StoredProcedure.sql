USE [638LimeStone]
GO
/****** Object:  StoredProcedure [dbo].[spFixCreditType]    Script Date: 06/01/2011 10:14:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFixCreditType]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spFixCreditType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spFixCreditType]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 6-2011
-- Description:	Set accttype and description for Credit cards.
-- =============================================
CREATE PROCEDURE [dbo].[spFixCreditType] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update dbo.AFFILIAT_Stage
	set AcctType=''Credit'', AcctTypeDesc=''Credit Card''
	where ACCTID in (select PAN from dbo.DemographicIN_credit)
	
END
' 
END
GO
