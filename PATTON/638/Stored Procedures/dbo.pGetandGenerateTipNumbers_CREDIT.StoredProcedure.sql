USE [638LimeStone]
GO
/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbers_CREDIT]    Script Date: 03/11/2011 15:57:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_CREDIT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetandGenerateTipNumbers_CREDIT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_CREDIT]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbers_CREDIT]
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS  for the CREDIT cards from the DELTA file that DON''T are NEW accounts (don''t have a value in the replacement card field and are not being closed (''c'' in the CloseFlag field)                   */
/*                                                                            */
/******************************************************************************/

/****************************/
/* S Blanchette             */
/* 3/2011                   */
/* SEB003                   */
/* Add dda to house holding */
/****************************/

-- Changed logic to employ last tip used instead of max tip
declare  @PAN nchar(16),  @SSN nchar(16), @DDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @process int, @worktip nchar(15)



/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN_CREDIT                               */
                                                                      
declare DEMO_crsr cursor
for select Tipnumber, Pan, SSN, ltrim(rtrim(DDA)) /* SEB003 */
from DemographicIn_CREDIT /* SEB001 */
for update
/*                                                                            */
open DEMO_crsr
 
                                                                         
set @newtipnumber=''0''

/*****************/
/* START SEB003  */		
/*****************/
--fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN
fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN, @DDA
/*****************/
/* END   SEB003  */		
/*****************/
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
set @worktip=''0''
		
	-- Check if PAN already assigned to a tip number.

/*****************/
/* START SEB003  */		
/*****************/

	--if exists(select tipnumber from account_reference where acctnumber=@PAN)
	--	Begin
	--		set @Worktip=(select tipnumber from account_reference where acctnumber=@PAN)
	--	END
	--else -- Check if SSN is already assigned to a tip number
	--	begin
	--		set @Worktip=(select tipnumber from account_reference where acctnumber=@SSN)
	--	end

	if exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			set @Worktip=(select tipnumber from account_reference where acctnumber=@PAN)
		END
		-- Check if SSN is already assigned to a tip number
	else	if exists(select tipnumber from account_reference where acctnumber=@SSN)
				begin
					set @Worktip=(select tipnumber from account_reference where acctnumber=@SSN)
				end
			else
				begin
					set @Worktip=(select tipnumber from account_reference where acctnumber=@DDA)
				end
/*****************/
/* END   SEB003  */		
/*****************/
	
	
	If @workTip=''0'' or @worktip is null
	Begin

		declare @LastTipUsed char(15)
		
		exec rewardsnow.dbo.spGetLastTipNumberUsed ''638'', @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @newtipnumber = cast(@LastTipUsed as bigint) + 1  
	/*********  End SEB002  *************************/
		set @UpdateTip=@newtipnumber
		
		--if @PAN is not null and left(@PAN,1)<> '' '' and not exists(select tipnumber from account_reference where acctnumber=@PAN)
		Begin
			insert into Account_Reference (Tipnumber, acctnumber, TipFirst)
			values(@UpdateTip, @PAN, left(@UpdateTip,3))

			insert into Account_Reference (Tipnumber, acctnumber, TipFirst)
			values(@UpdateTip, @SSN, left(@UpdateTip,3))			
		
/*****************/
/* START SEB003  */		
/*****************/
			insert into Account_Reference (Tipnumber, acctnumber, TipFirst)
			values(@UpdateTip, @DDA, left(@UpdateTip,3))			
/*****************/
/* END   SEB003  */		
/*****************/
			End

		
		exec RewardsNOW.dbo.spPutLastTipNumberUsed ''638'', @Newtipnumber  /*SEB002 */
	End
	Else
	If @workTip<>''0'' and @worktip is not null
		Begin
			set @updateTip=@worktip
		End	
	update DemographicIn_CREDIT	/* SEB001 */
	set tipnumber = @UpdateTip 
	where current of demo_crsr 
	goto Next_Record
Next_Record:
/*****************/
/* START SEB003  */		
/*****************/
		--fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN
		fetch DEMO_crsr into  @Tipnumber, @PAN, @SSN, @DDA
/*****************/
/* END   SEB003  */		
/*****************/
		
end
Fetch_Error:
close  DEMO_crsr
deallocate  DEMO_crsr


' 
END
GO
