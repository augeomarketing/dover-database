SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zdspGetLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output
AS 


SELECT @LastTipUsed = LastTipNumberUsed 
		from client
GO
