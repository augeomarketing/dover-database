USE [638LimeStone]
GO
/****** Object:  StoredProcedure [dbo].[sp638GetDDAforTip]    Script Date: 02/18/2011 10:51:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp638GetDDAforTip]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp638GetDDAforTip]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp638GetDDAforTip]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp638GetDDAforTip] AS

/************************************************/
/* S Blanchette                                 */
/* 2/2011                                       */
/* SEB001                                       */
/* Limit capture of only 11 positions for MISC2 */
/************************************************/
--clear the work table
truncate table tmpTipDDA

--insert all of the distinct Tip/DDA combinations so that this table can be joined to 
--insert into tmpTipDDA select  distinct Tipnumber,[Prim DDA] as dda  from DemographicIN     ---ORIGINAL. Fails to update if there is no demographicInRecord so changed to select below
/* SEB001 insert into tmpTipDDA select  distinct Tipnumber, Misc2 as dda  from Customer_stage */
/* SEB001 */ insert into tmpTipDDA select  distinct Tipnumber, rtrim(left(Misc2,11)) as dda  from Customer_stage

--update Monthly_Statement_File DDA field
 UPDATE msf set msf.DDA =t.DDA 
FROM  tmpTipDDA t
join Monthly_Statement_File msf on
msf.Tipnumber = t.Tipnumber' 
END
GO
