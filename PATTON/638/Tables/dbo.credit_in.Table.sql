SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[credit_in](
	[TipNumber] [nvarchar](15) NULL,
	[PAN] [varchar](16) NULL,
	[AcctName] [varchar](40) NULL,
	[RNAcctName] [varchar](40) NULL,
	[SSN] [varchar](9) NULL,
	[Address1] [varchar](25) NULL,
	[Address2] [varchar](25) NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](3) NULL,
	[Zip] [varchar](5) NULL,
	[HomePhone] [varchar](10) NULL,
	[FirstName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
