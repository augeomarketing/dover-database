USE [638LimeStone]
GO
/****** Object:  Table [dbo].[DemographicIN_credit_work]    Script Date: 06/03/2011 10:54:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DemographicIN_credit_work]') AND type in (N'U'))
DROP TABLE [dbo].[DemographicIN_credit_work]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DemographicIN_credit_work]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DemographicIN_credit_work](
	[TipNumber] [nvarchar](15) NULL,
	[PAN] [varchar](16) NULL,
	[AcctName] [varchar](40) NULL,
	[RNAcctName] [varchar](40) NULL,
	[SSN] [varchar](9) NULL,
	[Address1] [varchar](25) NULL,
	[Address2] [varchar](25) NULL,
	[City] [varchar](25) NULL,
	[State] [varchar](3) NULL,
	[Zip] [varchar](5) NULL,
	[HomePhone] [varchar](10) NULL,
	[FirstName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[DDA] [varchar](16) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
