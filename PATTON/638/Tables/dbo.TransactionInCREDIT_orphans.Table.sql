USE [638LimeStone]
GO
/****** Object:  Table [dbo].[TransactionInCREDIT_orphans]    Script Date: 12/21/2010 15:16:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT_orphans]') AND type in (N'U'))
DROP TABLE [dbo].[TransactionInCREDIT_orphans]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
--
--
-- Modified S Blanchette
-- 12/2010
-- expanded RECTYPE from 1 to 3 positions to handle trancode

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionInCREDIT_orphans]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransactionInCREDIT_orphans](
	[RecType] [varchar](3) NULL,
	[CardNumber] [varchar](16) NULL,
	[TransSign] [varchar](1) NULL,
	[TransAmt] [varchar](13) NULL,
	[PurchaseDate] [varchar](10) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
