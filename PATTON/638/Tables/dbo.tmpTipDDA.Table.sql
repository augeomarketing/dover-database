SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmpTipDDA](
	[Tipnumber] [nchar](15) NULL,
	[dda] [nchar](16) NULL
) ON [PRIMARY]
GO
