USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[sp229CGenerateTIPNumbersStage]    Script Date: 05/03/2010 12:33:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp229CGenerateTIPNumbersStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp229CGenerateTIPNumbersStage]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[sp229CGenerateTIPNumbersStage]    Script Date: 05/03/2010 12:33:29 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp229CGenerateTIPNumbersStage]
AS 

update roll_customer set Tipnumber = b.tipnumber
from roll_customer a join affiliat_stage b on a.custid = b.custid
where a.TipNumber is null or a.TipNumber = ' '

update roll_customer set Tipnumber = b.tipnumber
from roll_customer a join affiliat_stage b on a.cardnumber = b.acctid
where a.TipNumber is null or a.TipNumber = ' '

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '229', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 229000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '229', @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a join gentip b on a.custid = b.custid
where a.tipnumber is null or a.tipnumber = ' '
GO
