USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrubSIG]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrubSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrubSIG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrubSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE  PROCEDURE [dbo].[spInputScrubSIG] AS

/************************************************************************************************/
/* Changes: 1) add zipcode                                                                      */
/************************************************************************************************/

Truncate Table  Input_CustomerSIG_error
Truncate Table Input_Transactions_errorSIG

update Input_CustomerSIG
set [acctname1]=replace([acctname1],char(39), '' ''),  [acctname2]=replace([acctname2],char(39), '' ''), address1=replace(address1,char(39), '' ''),
address2=replace(address2,char(39), '' ''),   city=replace(city,char(39), '' ''),  lastname=replace(lastname,char(39), '' '')

update Input_CustomerSIG
set [acctname1]=replace([acctname1],char(140), '' ''), [acctname2]=replace([acctname2],char(140), '' ''), address1=replace(address1,char(140), '' ''),
address2=replace(address2,char(140), '' ''),   city=replace(city,char(140), '' ''),  lastname=replace(lastname,char(140), '' '')
  
update Input_CustomerSIG
set [acctname1]=replace([acctname1],char(44), '' ''), [acctname2]=replace([acctname2],char(44), '' ''), address1=replace(address1,char(44), '' ''),
address2=replace(address2,char(44), '' ''),   city=replace(city,char(44), '' ''),  lastname=replace(lastname,char(44), '' '')
  
update Input_CustomerSIG
set [acctname1]=replace([acctname1],char(46), '' ''), [acctname2]=replace([acctname2],char(46), '' ''), address1=replace(address1,char(46), '' ''),
address2=replace(address2,char(46), '' ''),   city=replace(city,char(46), '' ''),  lastname=replace(lastname,char(46), '' '')

update Input_CustomerSIG
set [acctname1]=replace([acctname1],char(34), '' ''), [acctname2]=replace([acctname2],char(34), '' ''), address1=replace(address1,char(34), '' ''),
address2=replace(address2,char(34), '' ''),   city=replace(city,char(34), '' ''),  lastname=replace(lastname,char(34), '' '')

update Input_CustomerSIG
set [acctname1]=replace([acctname1],char(35), '' ''), [acctname2]=replace([acctname2],char(35), '' ''), address1=replace(address1,char(35), '' ''),
address2=replace(address2,char(35), '' ''),   city=replace(city,char(35), '' ''),  lastname=replace(lastname,char(35), '' '')

update Input_CustomerSIG set zipcode = left(zipcode,5) + ''-'' + right(zipcode,4)
where len(zipcode) > 5

-- change (1) added zipcode 
UPDATE    Input_CustomerSIG
SET  Address4 = rtrim(City) + '' '' + rtrim(State) + '' '' + left(rtrim(zipcode),5)

--------------- Input Customer table

/********************************************************************/
/* Remove Input_CustomerSIG records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_CustomerSIG_error 
	select * from Input_CustomerSIG 
	where (custid is null or custid = '' '') or  
	      (cardnumber is null or cardnumber = '' '') or
	      (acctname1 is null or acctname1 = '' '')

delete from Input_CustomerSIG 
where (custid is null or custid = '' '') or  
           (cardnumber is null or cardnumber = '' '') or
           (acctname1 is null or acctname1 = '' '')

update Input_CustomerSIG set homephone =  Left(Input_CustomerSIG.homephone,3) + substring(Input_CustomerSIG.homephone,5,3) + right(Input_CustomerSIG.homephone,4)
--update Input_CustomerSIG set workphone  = Left(Input_CustomerSIG.workphone,3) + substring(Input_CustomerSIG.workphone,5,3) + right(Input_CustomerSIG.workphone,4)

--UPDATE  Input_CustomerSIG SET misc2 = CustID

--------------- Input Transaction table

Insert into Input_Transactions_errorSIG 
	select * from Input_TransactionsSIG 
	where 	(cardnumber is null or cardnumber = '' '') or 
		(purchase is null and [returns] is null and bonus is null) or 
		(purchase = ''0'' and [returns] = ''0'' and bonus = ''0'') 
	       
Delete from Input_TransactionsSIG
where 	(cardnumber is null or cardnumber = '' '') or 
	(purchase is null and [returns] is null and bonus is null)  or 
	(purchase = ''0'' and [returns] = ''0'' and bonus = ''0'')


' 
END
GO
