/****** Object:  StoredProcedure [dbo].[spLoadCustomerStageDebit]    Script Date: 07/23/2009 14:47:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStageDebit] @EndDate DateTime AS

/* Move Address2 to address1 if address1 is null */
Update input_customer_debit
Set 
Address4 = Address2, 
Address2 = null 
where address4 is null



/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(input_customer_debit.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(input_customer_debit.acctNAME1),40 )
,ACCTNAME2 	= left(rtrim(input_customer_debit.acctNAME2),40 )
,ADDRESS1 	= left(rtrim( input_customer_debit.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( input_customer_debit.ADDRESS2),40)
,ADDRESS4  	= left(rtrim( input_customer_debit.ADDRESS4),40)
,CITY 		= input_customer_debit.CITY
,STATE		= left(input_customer_debit.STATE,2)
,ZIPCODE 	= rtrim(left(input_customer_debit.ZIPCode,5) + '-' + right(input_customer_debit.zipcode,4))
,HOMEPHONE 	= Ltrim(input_customer_debit.homephone)
,workPHONE 	= Ltrim(input_customer_debit.workphone)
,STATUS	= input_customer_debit.STATUS
,statusdescription = input_customer_debit.statusdescription
From input_customer_debit
Where input_customer_debit.TIPNUMBER = Customer_Stage.TIPNUMBER 


/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ACCTNAME2, ADDRESS1,  ADDRESS2, ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE, DATEADDED, STATUS, statusdescription,
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),12), left(rtrim(LASTNAME),40),
	left(rtrim(Input_Customer_debit.acctNAME1),40) , 
	left(rtrim(Input_Customer_debit.acctNAME2),40) , 
	Left(rtrim(ADDRESS1),40), 
  	Left(rtrim(ADDRESS2),40), 
	Left(rtrim(ADDRESS4),40), 
	CITY, left(STATE,2), rtrim(left(ZIPCode,5) + '-' + right(zipcode,4)),
	Ltrim(Input_Customer_debit.homephone) ,
	 @enddate, STATUS,  statusdescription
	,0, 0, 0, 0
from  Input_Customer_Debit 
	where Input_Customer_Debit.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set STATUS = 'A', statusdescription =  'Active [A]' 
Where STATUS IS NULL
GO
