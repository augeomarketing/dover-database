USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spRollAccountNamesSIG]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollAccountNamesSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRollAccountNamesSIG]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollAccountNamesSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spRollAccountNamesSIG]
AS

UPDATE input_customerSIG SET acctname1 = b.acctname1
FROM input_customerSIG a, roll_customerSIG b
WHERE     a.tipnumber = b.tipnumber and b.misc7 = ''Z''

UPDATE input_customerSIG SET acctname1 = b.acctname1
FROM input_customerSIG a, roll_customerSIG b
WHERE     a.tipnumber = b.tipnumber and (a.acctname1 is null or a.acctname1 = '' '')

UPDATE    input_customerSIG
SET              custid = b.custid, cardnumber = b.cardnumber, lastname = b.lastname, address1 = b.address1, address2 = b.address2, 
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2, acctname3 = b.acctname3
FROM         input_customerSIG a, roll_customerSIG b
WHERE     a.tipnumber = b.tipnumber

update input_customerSIG  set acctname2 =  b.acctname1 
from input_customerSIG a, roll_customerSIG b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
(a.acctname2 is null or a.acctname2 = '' '') 

update input_customerSIG  set acctname3 =  b.acctname1 
from input_customerSIG a, roll_customerSIG b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and (a.acctname3 is null or a.acctname3 = '' '')

update input_customerSIG  set acctname4 =  b.acctname1 
from input_customerSIG a, roll_customerSIG b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
(a.acctname4 is null or a.acctname4 = '' '')

update input_customerSIG  set acctname5 =  b.acctname1 
from input_customerSIG a, roll_customerSIG b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and (a.acctname5 is null or a.acctname5 = '' '')

update input_customerSIG  set acctname6 =  b.acctname1 
from input_customerSIG a, roll_customerSIG b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1) and 
rtrim(b.acctname1) != rtrim(a.acctname2) and rtrim(b.acctname1) != rtrim(a.acctname3) and 
rtrim(b.acctname1) != rtrim(a.acctname4) and rtrim(b.acctname1) != rtrim(a.acctname5) and
(a.acctname6 is null or a.acctname6 = '' '')

UPDATE    input_customerSIG
SET              lastname = SUBSTRING(acctname1, 1, CHARINDEX('','', acctname1) - 1)
WHERE     SUBSTRING(acctname1, 1, 1) NOT LIKE '' '' AND acctname1 IS NOT NULL AND acctname1 LIKE ''%,%''

UPDATE    input_customerSIG
SET              lastname = acctname1 
WHERE   len(lastname) <= 3

UPDATE    input_customerSIG
SET              ACCTNAME1 = RTRIM(SUBSTRING(ACCTNAME1, CHARINDEX('','', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + '' '' + SUBSTRING(ACCTNAME1, 1, 
                      CHARINDEX('','', ACCTNAME1) - 1)
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE '' '') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE ''%,%'')

UPDATE    input_customerSIG
SET              ACCTNAME2 = RTRIM(SUBSTRING(ACCTNAME2, CHARINDEX('','', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + '' '' + SUBSTRING(ACCTNAME2, 1, 
                      CHARINDEX('','', ACCTNAME2) - 1)
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE '' '') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE ''%,%'')

UPDATE    input_customerSIG
SET              ACCTNAME3 = RTRIM(SUBSTRING(ACCTNAME3, CHARINDEX('','', ACCTNAME3) + 1, LEN(RTRIM(ACCTNAME3)))) + '' '' + SUBSTRING(ACCTNAME3, 1, 
                      CHARINDEX('','', ACCTNAME3) - 1)
WHERE     (SUBSTRING(ACCTNAME3, 1, 1) NOT LIKE '' '') AND (ACCTNAME3 IS NOT NULL) AND (ACCTNAME3 LIKE ''%,%'')

UPDATE    input_customerSIG
SET              ACCTNAME4 = RTRIM(SUBSTRING(ACCTNAME4, CHARINDEX('','', ACCTNAME4) + 1, LEN(RTRIM(ACCTNAME4)))) + '' '' + SUBSTRING(ACCTNAME4, 1, 
                      CHARINDEX('','', ACCTNAME4) - 1)
WHERE     (SUBSTRING(ACCTNAME4, 1, 1) NOT LIKE '' '') AND (ACCTNAME4 IS NOT NULL) AND (ACCTNAME4 LIKE ''%,%'')

UPDATE    input_customerSIG
SET              ACCTNAME4 = RTRIM(SUBSTRING(ACCTNAME5, CHARINDEX('','', ACCTNAME5) + 1, LEN(RTRIM(ACCTNAME5)))) + '' '' + SUBSTRING(ACCTNAME5, 1, 
                      CHARINDEX('','', ACCTNAME5) - 1)
WHERE     (SUBSTRING(ACCTNAME5, 1, 1) NOT LIKE '' '') AND (ACCTNAME5 IS NOT NULL) AND (ACCTNAME5 LIKE ''%,%'')

UPDATE    input_customerSIG
SET              ACCTNAME6 = RTRIM(SUBSTRING(ACCTNAME6, CHARINDEX('','', ACCTNAME6) + 1, LEN(RTRIM(ACCTNAME6)))) + '' '' + SUBSTRING(ACCTNAME6, 1, 
                      CHARINDEX('','', ACCTNAME6) - 1)
WHERE     (SUBSTRING(ACCTNAME6, 1, 1) NOT LIKE '' '') AND (ACCTNAME6 IS NOT NULL) AND (ACCTNAME6 LIKE ''%,%'')

UPDATE    input_customerSIG
SET              lastname = RTRIM(SUBSTRING(lastname, CHARINDEX('','', lastname) + 1, LEN(RTRIM(lastname)))) + '' '' + SUBSTRING(lastname, 1, 
                      CHARINDEX('','', lastname) - 1)
WHERE     (SUBSTRING(lastname, 1, 1) NOT LIKE '' '') AND (lastname IS NOT NULL) AND (lastname LIKE ''%,%'')

' 
END
GO
