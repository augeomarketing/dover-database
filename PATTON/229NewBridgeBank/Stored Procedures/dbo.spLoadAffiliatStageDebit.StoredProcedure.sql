USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStageDebit]    Script Date: 12/09/2009 13:10:48 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

ALTER PROCEDURE [dbo].[spLoadAffiliatStageDebit]   @MonthEnd char(20)  AS 

/************ Insert New Accounts into Affiliat Stage  ***********/

Insert Into Affiliat_Stage (AcctID, Tipnumber)
	select distinct(c.acctid), c.TipNumber
	from roll_debitcard c where (c.acctid is not null) and c.acctid not in ( Select acctid from Affiliat_Stage)

update affiliat_stage set AcctType = 'Debit', DateAdded = '11/30/2009', secid = right(c.acctid,4),
AcctStatus = c.Status, AcctTypeDesc = 'Debit Card', lastname = c.lastname, custid = c.custid 
from affiliat_stage a, roll_debitcard c
where a.tipnumber = c.tipnumber and (a.dateadded is null or a.dateadded = ' ')

update affiliat_stage set  lastname = c.lastname
from affiliat_stage a, customer_stage c
where a.tipnumber = c.tipnumber and (a.lastname is null or a.lastname = ' ')

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypedesc = T.AcctTypedesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
	
GO