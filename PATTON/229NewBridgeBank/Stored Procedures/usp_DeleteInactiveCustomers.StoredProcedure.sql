USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spCombineTips]    Script Date: 12/14/2011 11:54:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_DeleteInactiveCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_DeleteInactiveCustomers]
GO

/******************************************************************************/	

 CREATE PROCEDURE [dbo].[usp_DeleteInactiveCustomers]  @ProcessingMonth Date  , @InactiveMonths int
AS    

--set  @ProcessingMonth = '2011/11/30'
--set @InactiveMonths = -12

Declare @CutOffChar varchar(12) 
Declare @CutOffDate Date  
set @CutOffChar = Convert( varchar(2), Month( DATEADD ( m,@InactiveMonths ,@ProcessingMonth) ) ) + '/01/'  +  Convert( varchar(4), Year ( DATEADD ( m,@InactiveMonths ,@ProcessingMonth) ) ) 
set @CutOffDate = Convert ( date ,@CutOffChar) 
print @CutOffDate 

---- put tipnumbers with Current History SPEND into a table 
	Select  c.TIPNUMBER  into #CurrentHistory
	From CUSTOMER c 
	Where c.TIPNUMBER  in 
		( 
		select distinct TIPNUMBER from HISTORY 
			where HISTDATE >= @CutOffDate
			and 	 TRANCODE in (  '63','67','33','37') 
			Union 
		select distinct TIPNUMBER from HISTORY_Stage 
			where HISTDATE >= @CutOffDate
			and 	 TRANCODE in (  '63','67','33','37') 
		)

---- Set tipnumbers that do not have CurrentHistory to 'T'
Update Customer
set Status = 'T' , StatusDescription = 'Purged due to inactivity since '+convert( varchar( 15) , @CutOffDate) 
Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 

--/******** PURGE INACTIVE ********/

Insert into AffiliatDeleted 
	(ACCTID, TIPNUMBER, AcctTYPE, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
	select ACCTID, TIPNUMBER, AcctTYPE, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, @ProcessingMonth
	from AFFILIAT 
		Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 

Insert into HistoryDeleted 
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, datedeleted)
	select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, @ProcessingMonth
	from HISTORY 
		Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 

Insert into CustomerDeleted 
	( TIPNumber, TIPFirst,    TIPLast,    AcctName1, AcctName2,      AcctName3,   AcctName4,    AcctName5,     AcctName6, Address1,      
	Address2,    Address3,     Address4, City, State, Zipcode, LastName,      Status,     StatusDescription, HomePhone, WorkPhone,         RunBalance,    
	RunRedeemed,  RunAvailable,  LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt,     RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, 
	Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, datedeleted,BonusFlag ) 
	select 
	TIPNUMBER, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, 
	ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, LASTNAME, STATUS, StatusDescription, HOMEPHONE, WORKPHONE, RUNBALANCE, 
	RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DATEADDED, NOTES, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, 
	Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew,  @ProcessingMonth,BonusFlag
	from CUSTOMER 
		Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 


-- delete from affiliat 
delete from AFFILIAT 	Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 

-- delete from history
delete 	from HISTORY 		Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 

-- delete from customer
delete from CUSTOMER 	Where TIPNUMBER NOT in ( select TIPNUMBER from #CurrentHistory ) 


