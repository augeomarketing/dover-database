USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 08/10/2010 10:43:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 08/10/2010 10:43:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
        
/* BY:  D Foster  */
/* DATE: 09/2009   */
/* REVISION: 0 */
/* pending purge procedure for FI's Using Points Now                          */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */

declare @tipnumber nvarchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

UPDATE CS SET [STATUS] = 'A', StatusDescription = 'Active[A]'
from CUSTOMER CS join affiliat AFS on cs.tipnumber = afs.tipnumber
WHERE  CS.[STATUS] != 'A' AND 
(EXISTS(SELECT 1 FROM affiliat WHERE tipnumber = cs.tipnumber AND (acctstatus = 'A' OR acctstatus IS NULL)))

UPDATE CS SET [STATUS] = 'I', StatusDescription = 'InActive[I]'
from CUSTOMER CS join affiliat AFS on cs.tipnumber = afs.tipnumber
WHERE  CS.[STATUS] = 'A' AND 
(NOT EXISTS(SELECT 1 FROM affiliat WHERE tipnumber = cs.tipnumber AND (acctstatus = 'A' OR acctstatus IS NULL)))

delete dbo.Pending_Purge_Accts 
where(EXISTS(SELECT 1 FROM customer WHERE tipnumber = Pending_Purge_Accts.tipnumber AND [Status] = 'A' ))

insert dbo.Pending_Purge_Accts (acctname,tipnumber,CycleNumber)
select acctname1,tipnumber,'1' from CUSTOMER where [Status] != 'A' and
TIPNUMBER not in (select TIPNUMBER from dbo.Pending_Purge_Accts)

update CUSTOMER set [STATUS] = 'P', StatusDescription = 'Pending Purge'
from customer c join Pending_Purge_Accts p on c.TIPNUMBER = p.tipnumber
where P.CycleNumber >= '3'  

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge(tipnumber)
select tipnumber from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

update customer set status = 'C', statusdescription = 'Closed'
from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
where @purgedate >= (select max(histdate) from history
where c.tipnumber  = tipnumber)

delete from PendingPurge
    where tipnumber in (select tipnumber from customer where status = 'C')
    
delete from Pending_Purge_Accts
    where tipnumber in (select tipnumber from customer where status = 'C')



GO


