USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoad_MonthlySummary]    Script Date: 10/08/2009 16:23:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 8/20/2009
-- Description:	Load Monthly Summary Table
-- =============================================
ALTER PROCEDURE [dbo].[spLoad_MonthlySummary] @Process_date varchar(10) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert  monthlysummary (dateadded,customer_count,cr_points_added,cr_points_returned,points_added,points_subtracted,bonus_points,points_redeemed,db_points_added,
	db_points_returned)
	SELECT @Process_Date as dateadded,COUNT(Tipnumber) AS Customer_Count, SUM(CRPointsPurchased) AS cr_points_added, SUM(CRPointsReturned) AS cr_points_returned,
	SUM(PointsAdded) AS Points_Added,SUM(PointsSubtracted) AS Points_Subtracted,SUM(PointsBonus)AS Bonus_Points,  SUM(PointsRedeemed) AS Points_Redeemed,
	SUM(DBPointsPurchased) AS db_points_added,SUM(DBPointsReturned) AS db_points_returned 
	FROM Monthly_Statement_File

	update monthlysummary set IN_DB_Purchases = (select Total_Input_DBPurchase from debit_summary where rowid = (select max(rowid) from debit_summary)),
	IN_DB_Returns = (select Total_Input_DBReturn from debit_summary where rowid = (select max(rowid) from debit_summary)),
	Err_DB_Points_Added = (select Total_DBPurchase_Err from debit_summary where rowid = (select max(rowid) from debit_summary)),
	Err_DB_Points_Returned = (select Total_DBReturn_Err from debit_summary where rowid = (select max(rowid) from debit_summary)) 
END
