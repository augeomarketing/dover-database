USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spDebitTransactionScrub]    Script Date: 07/27/2010 16:57:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDebitTransactionScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDebitTransactionScrub]
GO

/****** Object:  StoredProcedure [dbo].[spDebitTransactionScrub]    Script Date: 07/27/2010 16:57:51 ******/
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

/********** REVISION HISTORY  **********
RDT 4/13/2011 Replaced update statement with a "more better" one. 
							The first one was taking to bloody long. 
RDT 9/13/2011 - Changed point earning ratio from $2 = 1 point to $3 = 1 point.	

*/

CREATE PROCEDURE [dbo].[spDebitTransactionScrub]  @monthend varchar(10)
 AS

truncate table input_dbtransactions_error

Update input_dbtransactions 
set in_dbtranamt =  dbtranamt,  in_crtranamt = crtranamt 

/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_dbtransactions 
set dbtranamt = round(dbtranamt,0), crtranamt = round(crtranamt,0)

Update input_dbtransactions 
-- RDT 9/13/2011  set dbtranamt = round(dbtranamt/2,0), crtranamt = round(crtranamt/2,0)
								set dbtranamt = round(dbtranamt/3,0), crtranamt = round(crtranamt/3,0)  


--RDT 4/13/2011 Modifications START
-- UPDATE    input_dbtransactions
-- SET              tipnumber = b.tipnumber
-- FROM         input_dbtransactions a, affiliat_stage b
-- WHERE     (a.acctid = b.acctid) OR  ((a.custid = b.custid) and a.custid != '000000000')

Update Input_DBTransactions 
set Tipnumber = b.TIPNUMBER 
FROM         input_dbtransactions a join affiliat_stage b on a.AcctID = b.ACCTID 
where a.Tipnumber  is null 

Update Input_DBTransactions 
set Tipnumber = b.TIPNUMBER 
FROM         input_dbtransactions a join affiliat_stage b on a.custid = b.custid
where a.custid != '000000000' and a.Tipnumber is null 
--RDT 4/13/2011 Modifications END 

--------------- Input Transaction table

Insert into Input_DBTransactions_error 
	select * from Input_DBTransactions 
	where (acctid is null or acctid = ' ') or
	      (tipnumber is null or tipnumber = ' ') 

---  Step 2 Load Debit_Summary
	      
update debit_summary set Total_DBPurchase_Err = (select sum(in_dbtranamt) from Input_DBTransactions_Error),
Total_DBReturn_Err = (select sum(in_crtranamt) from Input_DBTransactions_Error)
where rowid = (select max(rowid) from debit_summary)
	       
Delete from Input_dbTransactions
where (acctid is null or acctid = ' ') or
      (tipnumber is null or tipnumber = ' ') or
      ((dbtranamt is null  or dbtranamt =  0) and (crtranamt is null or crtranamt  = 0))

-- 1.add insert distinct to eliminate duplicate acctids

insert into Affiliat_Stage (AcctID, Tipnumber)
select distinct(acctid),tipnumber from Input_DBTransactions 
where acctid not in ( Select acctid from Affiliat_Stage)

-- 2.update loads data into fields inserted into step above

update Affiliat_Stage set AcctType = 'Debit', DateAdded=@monthend, secid=right(c.acctid,4),
AcctStatus='A', AcctTypeDesc='Debit Card', LastName=' ', YTDEarned=0, custid=c.custid
from input_dbtransactions c join Affiliat_Stage afs on c.AcctID = afs.ACCTID
where afs.DATEADDED is null or DATEADDED = ' '

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypedesc = T.AcctTypedesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

GO


