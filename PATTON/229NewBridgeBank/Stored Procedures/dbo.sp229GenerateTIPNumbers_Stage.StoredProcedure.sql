USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[sp229GenerateTIPNumbers_Stage]    Script Date: 05/03/2010 12:39:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp229GenerateTIPNumbers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp229GenerateTIPNumbers_Stage]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[sp229GenerateTIPNumbers_Stage]    Script Date: 05/03/2010 12:39:09 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[sp229GenerateTIPNumbers_Stage]
AS 

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a join affiliat_stage b on a.cr3 = b.acctid
where (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a join affiliat_stage b on a.companyid = b.custid
where (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a join affiliat_stage b on a.cardnumber = b.acctid
where (a.tipnumber is null or a.tipnumber = ' ')


DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber, tipnumber	
from roll_commercial where (tipnumber is null or tipnumber = ' ') and misc8 = 'N'

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '229', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 229000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '229', @newnum

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a join gentip b on a.cardnumber = b.acctid
where (a.tipnumber is null or a.tipnumber = ' ')

/* Assigning Tipnumbers combining on CompanyIdentifier */

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct companyid as custid, tipnumber	
from roll_commercial where tipnumber is null and misc8 = 'Y'

	/*    Create new tip          */
 
exec rewardsnow.dbo.spGetLastTipNumberUsed '229', @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 229000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '229', @newnum
update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a join gentip b on a.companyid = b.custid
where (a.tipnumber is null or a.tipnumber = ' ')
GO
