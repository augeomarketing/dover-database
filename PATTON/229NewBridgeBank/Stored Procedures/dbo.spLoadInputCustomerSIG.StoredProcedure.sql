USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadInputCustomerSIG]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadInputCustomerSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadInputCustomerSIG]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadInputCustomerSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spLoadInputCustomerSIG]
AS
BEGIN

	truncate table input_customerSIG

	INSERT 
	INTO Input_CustomerSIG
						  (tipnumber)
	SELECT 
	DISTINCT tipnumber
	FROM         Roll_CustomerSIG

	UPDATE    input_customerSIG
	SET	custid = b.custid
		, cardnumber = b.cardnumber
		, acctname1 = b.acctname1
		, lastname = b.lastname
		, address1 = b.address1
		, address2 = b.address2
		, city = b.city
		, state = b.state
		, zipcode = b.zipcode
		, homephone = b.homephone
		, statuscode = b.statuscode
		, systembankid = b.systembankid
		, principlebankid = b.principlebankid
		, agentbankid = b.agentbankid
		, acctname2 = b.acctname2
		, acctname3 = b.acctname3
	FROM input_customerSIG a, roll_customerSIG b
	WHERE     a.tipnumber = b.tipnumber
END
' 
END
GO
