/****** Object:  StoredProcedure [dbo].[spScrub_Customer_Debit]    Script Date: 07/23/2009 14:47:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spScrub_Customer_Debit]
AS

update roll_debitcard set acctname1 = ltrim(rtrim(firstname)) + ' ' + ltrim(rtrim(lastname))

UPDATE roll_debitcard SET Address4 = ltrim(rtrim(City) + '  ' + State + ' ' + zipcode)

update roll_debitcard set acctid = rtrim(ltrim(acctid)), custid = rtrim(ltrim(custid))
GO
