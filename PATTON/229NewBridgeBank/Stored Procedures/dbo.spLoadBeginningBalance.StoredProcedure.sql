/****** Object:  StoredProcedure [dbo].[spLoadBeginningBalance]    Script Date: 07/23/2009 14:47:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadBeginningBalance] @MonthBeginDate nchar(10)
AS 

/*Declare @MonthBucket char(10), @MonthBegin char(2), @Monthfix int, @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)  chg 6/15/2006   */
Declare @MonthBucket char(10), @MonthBegin char(2), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000)

/* set @monthfix=CONVERT( int , left(@MonthBeginDate,2)) + '1' chg 6/15/2006   */

set @MonthBegin=CONVERT( int , left(@MonthBeginDate,2)) + '1'

/* if CONVERT( int , @MonthBegin)<'10' 
	begin
	set @monthbegin='0' + left(@monthbegin,1)
	end	
else chg 6/15/2006   */
   if CONVERT( int , @MonthBegin)='13' 
	begin
	set @monthbegin='1'
	end	

set @MonthBucket='MonthBeg' + @monthbegin

set @SQLUpdate=N'update Beginning_Balance_Table set '+ Quotename(@MonthBucket) + N'= (select pointsend from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber) where exists(select * from Monthly_Statement_File where tipnumber=Beginning_Balance_Table.tipnumber)'

set @SQLInsert=N'insert into Beginning_Balance_Table (Tipnumber, ' + Quotename(@MonthBucket) + ') select tipnumber, pointsend from Monthly_Statement_File where not exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate
exec sp_executesql @SQLInsert
GO
