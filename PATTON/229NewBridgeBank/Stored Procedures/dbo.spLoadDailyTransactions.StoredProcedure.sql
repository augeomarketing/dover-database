USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spLoadDailyTransactions]    Script Date: 09/28/2010 08:35:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadDailyTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadDailyTransactions]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spLoadDailyTransactions]    Script Date: 09/28/2010 08:35:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Dan Foster
-- Create date: 9/13/2009
-- Description:	Load Daily transactions
-- =============================================
CREATE PROCEDURE [dbo].[spLoadDailyTransactions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    update hold_DBTransactions set dim_hold_dbTransactions_DBTranAmt = dim_hold_dbTransactions_DBTranAmt/100,dim_hold_dbTransactions_CRTranAmt = dim_hold_dbTransactions_CRTranAmt/100
    
    update IDBT set DBTranCnt = idbt.DBTranCnt + hdbt.dim_hold_dbTransactions_dbtrancnt, DBTranAmt = idbt.DBTranAmt + hdbt.dim_hold_dbTransactions_dbtranAmt,
    CRTranCnt = idbt.CRTranCnt + hdbt.dim_hold_dbTransactions_CRtrancnt, CRTranAmt = idbt.CRTranAmt + hdbt.dim_hold_dbTransactions_CRtranAmt
    from Input_DBTransactions IDBT join hold_DBTransactions hdbt on IDBT.AcctID = hdbt.dim_hold_dbTransactions_acctid
	
	insert Input_DBTransactions (CustID, AcctID, DBTrancode, DBTranCnt, DBTranAmt, CRTranCode, CRTranCnt, CRTranAmt, Tipnumber, IN_DBTranAmt, IN_CRTranAmt)
	select dim_hold_dbTransactions_CustID, dim_hold_dbTransactions_AcctID, dim_hold_dbTransactions_DBTrancode, dim_hold_dbTransactions_DBTranCnt,dim_hold_dbTransactions_DBTranAmt, dim_hold_dbTransactions_CRTranCode, dim_hold_dbTransactions_CRTranCnt,
	dim_hold_dbTransactions_CRTranAmt, dim_hold_dbTransactions_Tipnumber, dim_hold_dbTransactions_IN_DBTranAmt, dim_hold_dbTransactions_CRTranAmt from hold_DBTransactions
	where dim_hold_dbTransactions_AcctID not in( select acctid from dbo.Input_DBTransactions)
	
END


GO


