/****** Object:  StoredProcedure [dbo].[spTransactionSummary]    Script Date: 07/23/2009 14:47:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spTransactionSummary]  @enddateparm varchar(10)
AS

declare @enddate datetime

set @enddate = convert(datetime,@enddateparm)

truncate table TransactionSummary

insert TransactionSummary (dateadded)
values (@enddate)

update  TransactionSummary set purchases = (select SUM(CONVERT(dec, points)) FROM transactions WHERE trancode = '63'  and dateadded = @enddate)

update   TransactionSummary set returned =(select SUM(CONVERT(dec, points)) FROM transactions WHERE trancode = '33' and dateadded =  @enddate)

update   TransactionSummary set bonuses =(select SUM(CONVERT(dec, points)) FROM transactions WHERE trancode = 'BO' and dateadded =  @enddate)
GO
