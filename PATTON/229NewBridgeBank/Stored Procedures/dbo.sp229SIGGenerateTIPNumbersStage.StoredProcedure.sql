USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[sp229SIGGenerateTIPNumbersStage]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp229SIGGenerateTIPNumbersStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp229SIGGenerateTIPNumbersStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp229SIGGenerateTIPNumbersStage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[sp229SIGGenerateTIPNumbersStage]
AS 

update roll_customerSIG set Tipnumber = b.tipnumber
from roll_customerSIG a join affiliat_stage b on a.custid = b.custid
where a.TipNumber is null or a.TipNumber = '' ''

update roll_customerSIG set Tipnumber = b.tipnumber
from roll_customerSIG a join affiliat_stage b on a.cardnumber = b.acctid
where a.TipNumber is null or a.TipNumber = '' ''

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customerSIG

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed ''229'', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 229000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = '' ''

exec RewardsNOW.dbo.spPutLastTipNumberUsed ''229'', @newnum

update roll_customerSIG 
set Tipnumber = b.tipnumber
from roll_customerSIG a join gentip b on a.custid = b.custid
where a.tipnumber is null or a.tipnumber = '' ''

' 
END
GO
