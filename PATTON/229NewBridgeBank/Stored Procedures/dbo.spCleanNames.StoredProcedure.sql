/****** Object:  StoredProcedure [dbo].[spCleanNames]    Script Date: 07/23/2009 14:47:02 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spCleanNames]
AS

UPDATE    CUSTOMERWORK SET  lastname = SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
WHERE     SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE    CUSTOMERWORK SET ACCTNAME1 = RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1,
LEN(RTRIM(ACCTNAME1)))) + ' ' + SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1)
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE    CUSTOMERWORK SET ACCTNAME2 = RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1,
LEN(RTRIM(ACCTNAME2)))) + ' ' + SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1)
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

update customerwork
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), address1=replace(address1,char(39), ' '), address2=replace(address2,char(39), ' '),
address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update customerwork
set acctname1=replace(acctname1,char(96), ' '),acctname2=replace(acctname2,char(96), ' '), address1=replace(address1,char(96), ' '), address2=replace(address2,char(96), ' '),
address4=replace(address4,char(96), ' '),   city=replace(city,char(96), ' '),  lastname=replace(lastname,char(96), ' ')

update customerwork
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), address1=replace(address1,char(44), ' '), address2=replace(address2,char(44), ' '),
address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update customerwork
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), address1=replace(address1,char(46), ' '), address2=replace(address2,char(46), ' '),
address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')
 
update customerwork
set acctname1=replace(acctname1,char(37), 'C/O'),acctname2=replace(acctname2,char(37), 'C/O'), address1=replace(address1,char(37), 'C/O'), address2=replace(address2,char(37), 'C/O'),
address4=replace(address4,char(37), 'C/O')

UPDATE   CustomerWORK SET lastname = ACCTNAME1 
WHERE     (lastname = 'LLC') OR (lastname = 'BankCard') OR (lastname = 'INC') OR (lastname = 'INC.') OR (lastname = 'LTD') OR (lastname = 'Payable')
GO
