/****** Object:  StoredProcedure [dbo].[spDebitTransSummary]    Script Date: 07/23/2009 14:47:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDebitTransSummary]
 AS

update debit_summary set total_debit_processed = (select sum(dbtranamt) from input_dbtransactions),
total_credit_processed = (select sum(crtranamt) from input_dbtransactions) 
where rowid = (select max(rowid) from debit_summary)

update debit_summary set total_debit_error = (select sum(dbtranamt) from input_dbtransactions_error),
total_credit_error = (select sum(crtranamt) from input_dbtransactions_error) 
where rowid = (select max(rowid) from debit_summary)
GO
