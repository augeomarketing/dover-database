SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 8/20/2009
-- Description:	Step 1  to load Debit Summary
-- =============================================
CREATE PROCEDURE debit_summary_step1 @processed_date varchar(10) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table debit_summary

	Insert debit_summary (processed_date,Total_Input_DBPurchase,Total_Input_DBReturn)
	
	SELECT  @processed_date,SUM(dbtranamt) AS Total_Input_DBPurchase, SUM(crtranamt) AS Total_Input_DBReturn
	FROM Input_dbTransactions

END
GO
