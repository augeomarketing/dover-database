USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spSpecial_Onetime_Bonus]    Script Date: 06/08/2010 13:32:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSpecial_Onetime_Bonus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSpecial_Onetime_Bonus]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spSpecial_Onetime_Bonus]    Script Date: 06/08/2010 13:32:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:		Dan Foster
-- Create date: March 24, 2010
-- Description:	Handle Special Bonus Request
-- Change 1:  Changed trancode to 67 from 64  DRF 5/13/2010
-- Change 2:  Changed code from "not exists" to "exists" and 
--		      trancode from "BF" to "is null" also date ck 
--			  added  DRF 5/13/2010
-- Change 3:  Change cursor to be acctid instead of tipnumber
-- Change 4:  Removed Cursor processing
-- ==========================================================
CREATE PROCEDURE [dbo].[spSpecial_Onetime_Bonus]  @dateadded varchar(10), @Points varchar(6)
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--declare @dateadded varchar(10), @Points varchar(6)
	--set @dateadded = '05/31/2010'
	--set @Points='5'
	
	update TransStandard set AcctNum = (RTRIM(ltrim(acctnum))) 
	update OneTimeBonuses set AcctID = (RTRIM(ltrim(acctid)))  

	update OTB set Trancode = 'BC', DateAwarded = @dateadded                                                                      
	from OneTimeBonuses_Stage OTB join TransStandard TSS on OTB.AcctID = TSS.AcctNum and TSS.TranCode = '67'
	where OTB.DateAwarded is null and (@dateadded > '04/30/2010' and @dateadded < '07/01/2010') 

	INSERT INTO transstandard (Tip,tranDate,acctnum,TranCode,Trannum,tranamt,Ratio,trantype)
	  select Tipnumber, @dateadded, acctid,'BC', '1', @Points, '1', 'Bonus for MC ReIssue' from OneTimeBonuses_Stage
	  where DateAwarded = @dateadded and Trancode = 'BC'
	
END
GO


