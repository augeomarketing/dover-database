/****** Object:  StoredProcedure [dbo].[spRollAccountNames]    Script Date: 07/23/2009 14:47:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRollAccountNames]  AS

update input_customer_debit  set acctname2 =  b.acctname1 
from input_customer_debit a, roll_debitcard b 
where a.tipnumber = b.tipnumber and  rtrim(b.acctname1) != rtrim(a.acctname1)and 
(a.acctname2 is null or a.acctname2 = ' ') and a.acctname1 != b.acctname1
GO
