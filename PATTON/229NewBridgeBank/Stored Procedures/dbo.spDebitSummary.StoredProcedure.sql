/****** Object:  StoredProcedure [dbo].[spDebitSummary]    Script Date: 07/23/2009 14:47:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDebitSummary]     @processed_date varchar(10)
 AS

insert debit_summary (processed_date,total_input_custinfo)
select @processed_date, count(*) from input_customer_debit  

update debit_summary set total_input_transinfo = (select count(*) from input_dbtransactions)  
where rowid = (select max(rowid) from debit_summary)

update debit_summary set total_input_debit = (select sum(dbtranamt) from input_dbtransactions),
total_input_credit = (select sum(crtranamt) from input_dbtransactions) 
where rowid = (select max(rowid) from debit_summary)
GO
