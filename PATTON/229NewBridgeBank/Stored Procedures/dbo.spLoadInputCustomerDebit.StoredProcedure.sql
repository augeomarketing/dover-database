USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadInputCustomerDebit]    Script Date: 08/17/2009 11:17:55 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spLoadInputCustomerDebit]
AS
truncate table roll_debitcard_error
truncate table input_customer_Debit

insert into roll_debitcard_error
select * from roll_debitcard where tipnumber is null

INSERT INTO Input_Customer_debit  (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_DebitCard where tipnumber is not null

UPDATE    input_customer_debit
SET              custid = b.custid, acctid = b.acctid, acctname1 = b.acctname1, firstname = b.firstname, lastname = b.lastname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, workphone =  b.workphone, status = b.status, last4 = b.last4
FROM         input_customer_debit a, roll_debitcard b
WHERE     a.tipnumber = b.tipnumber

update input_customer_debit set statusdescription = 'Active[A]' , status = 'A'

UPDATE    Input_Customer_debit SET dateadded =  (SELECT dateadded FROM CUSTOMER_Stage
WHERE  customer_stage.tipnumber = input_customer_debit.tipnumber)