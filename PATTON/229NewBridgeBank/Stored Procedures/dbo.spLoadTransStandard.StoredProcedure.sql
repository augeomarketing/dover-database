
Use [229NewBridgeBank] 
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO

/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 07/23/2009 14:47:04 ******/
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  D.Foster  */
/* DATE: 5/2007   */
/* REVISION: 0 */
-- RDT: 10/11/2011 Removed Bonus points for First Time Use ( JIRA NEWBRIDGE-34 )

/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10), @BonusPoints varchar(4)
AS

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1)

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
--Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], 'BI', '1', convert(char(15), [bonus])  from Input_Transactions 
where (bonus <> '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '63', '1', convert(char(15), [purchase])  from Input_Transactions
where (purchase > '0')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], '33','1', convert(char(15), [returns])  from Input_Transactions
where ([returns] > '0')


-- RDT: 10/11/2011 Removed Bonus points for First Time Use -- START 
/******************************************************************************/
-- Load the TransStandard table with  FTUB Customers
/******************************************************************************/
--------set @Trandate=@dateadded
--------set @bonuspoints='5000'
--------/* Setup Cursor for processing                                                */
--------declare Tip_crsr cursor 
--------for select distinct tip
--------from transstandard 
--------where  trancode in ('63', '33') 
--------/*                                                                            */
--------open Tip_crsr
--------/*                                                                            */
--------fetch Tip_crsr into @Tipnumber
--------/*                                                                            */
--------if @@FETCH_STATUS = 1
--------	goto Fetch_Error
--------/*                                                                            */
--------while @@FETCH_STATUS = 0
--------begin	
--------		set @ProcessFlag='N'		
--------		if not exists(select * from OneTimeBonuses_stage where tipnumber=@tipnumber AND trancode='BF')
--------		Begin 
--------			set @ProcessFlag='Y' 
--------		End 	  
--------		if @ProcessFlag='Y'
--------		Begin
--------			set @acctid = (select acctnum from transstandard 
--------					where rowid  = (select max(rowid) from transstandard where tip = @tipnumber and (acctnum is not null or acctnum <> ' ')))
--------			--Update Customer_stage 
--------			--set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
--------			--where tipnumber = @Tipnumber
--------			INSERT INTO transstandard (Tip,tranDate,acctnum,TranCode,Trannum,tranamt,Ratio,trantype)
--------        		Values(@Tipnumber, @Trandate, @acctid,'BF', '1', @BonusPoints, '1', 'Bonus for First Trans Activity') 
--------			INSERT INTO OneTimeBonuses_stage (TipNumber,TranCode, DateAwarded)
--------        		Values(@Tipnumber, 'BF', @Trandate)
--------		End
--------		goto Next_Record
--------Next_Record:
--------		fetch tip_crsr into @tipnumber
--------End
-- RDT: 10/11/2011 Removed Bonus points for First Time Use -- END

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

--------Fetch_Error:
--------close  tip_crsr
--------deallocate  tip_crsr
GO
