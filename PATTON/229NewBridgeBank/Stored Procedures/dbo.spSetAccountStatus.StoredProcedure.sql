USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spSetAccountStatus]    Script Date: 12/06/2011 08:25:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetAccountStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetAccountStatus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetAccountStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spSetAccountStatus]
 AS

update AFFILIAT_Stage set AcctStatus = ''A'' where AcctStatus is null

update CS set [STATUS] = ''A'', StatusDescription = ''Active[A]''
from CUSTOMER_Stage CS join affiliat_stage AFS on cs.tipnumber = afs.tipnumber
WHERE (cs.[STATUS] != ''A'') AND afs.acctstatus = ''A''

UPDATE AFS SET AcctStatus = ''C''
from affiliat_stage AFS join input_delete_accounts IDA on AFS.TIPNUMBER = IDA.TipNumber
WHERE afs.acctid = ida.cardnumber

UPDATE AFS SET AcctStatus = ''C''
from AFFILIAT_Stage AFS join input_delete_debit IDD on AFS.TIPNUMBER = IDD.TipNumber
WHERE   AFS.ACCTID = idd.CardNumber

UPDATE AFS SET AcctStatus = ''C''
from affiliat_stage AFS join input_delete_accountsSIG IDS on AFS.TIPNUMBER = IDS.TipNumber 
WHERE   AFS.ACCTID = ids.CardNumber
                          
UPDATE CS SET [STATUS] = ''P'', StatusDescription = ''Pending Purge''
from CUSTOMER_Stage CS join affiliat_stage AFS on cs.tipnumber = afs.tipnumber
WHERE  CS.[STATUS] = ''A'' AND (NOT EXISTS(SELECT 1 FROM affiliat_stage WHERE tipnumber = cs.tipnumber AND (acctstatus = ''A'' OR acctstatus IS NULL)))


' 
END
GO
