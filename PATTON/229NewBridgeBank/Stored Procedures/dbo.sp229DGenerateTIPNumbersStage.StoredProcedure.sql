USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[sp229DGenerateTIPNumbersStage]    Script Date: 05/03/2010 12:35:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp229DGenerateTIPNumbersStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp229DGenerateTIPNumbersStage]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[sp229DGenerateTIPNumbersStage]    Script Date: 05/03/2010 12:35:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp229DGenerateTIPNumbersStage]
AS 

update roll_DebitCard
set custid = D.custid
from roll_debitcard r join Input_DBTransactions D on (r.acctid = d.acctid)
where r.custid = '000000000'

update roll_DebitCard
set Tipnumber = a.tipnumber
from roll_debitcard r join affiliat_stage a on (r.custid = a.custid)
where (r.custid != '000000000') and (r.Tipnumber is null or r.Tipnumber = ' ')

update roll_debitcard
set Tipnumber = a.tipnumber
from roll_debitcard r join affiliat_stage a on (r.acctid = a.acctid)
where (r.Tipnumber is null or r.Tipnumber = ' ')

delete Roll_DebitCard
where AcctID not in(select AcctID from Input_DBTransactions) and Tipnumber is null

DELETE FROM GenTip

insert into gentip (acctid)
select   distinct acctid	
from roll_debitcard

update gentip set tipnumber = b.tipnumber
from gentip a, roll_debitcard b
where a.acctid = b.acctid

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '229', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 229000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '229', @newnum

update roll_debitcard
set Tipnumber = b.tipnumber
from roll_debitcard a join gentip b on a.acctid = b.acctid
where a.tipnumber is null or a.tipnumber = ' '