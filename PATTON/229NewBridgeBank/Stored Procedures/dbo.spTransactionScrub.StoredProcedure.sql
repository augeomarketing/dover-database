USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spTransactionScrub]    Script Date: 08/17/2009 14:53:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER PROCEDURE [dbo].[spTransactionScrub]
 AS

update input_transactions set custid = b.custid
from input_transactions a, roll_commercial b
where a.tipnumber = b.tipnumber and a.custid is null  


--------------- Input Transaction table

Insert into Input_Transactions_error 
	select * from Input_Transactions 
	where (cardnumber is null or cardnumber = ' ') or
	      (tipnumber is null or tipnumber = ' ') or
	      (purchase is null  and [returns] is null and bonus is null)
	       
Delete from Input_Transactions
where (cardnumber is null or cardnumber = ' ') or
      (tipnumber is null or tipnumber = ' ') or
      (purchase is null  or (purchase =  0 and [Returns] = 0 and Bonus = 0))


/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transactions 
set  purchase = round(purchase,0),  [Returns] =  round([returns],0), bonus = round(Bonus,0)
