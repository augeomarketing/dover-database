/****** Object:  StoredProcedure [dbo].[spDBLoadTransStandard]    Script Date: 07/23/2009 14:47:02 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spDBLoadTransStandard] @DateAdded char(10)
AS

declare  @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1)

/******************************************************************************/
-- Load the TransStandard table with rows from Input_DBTransaction
/******************************************************************************/

--Truncate table TransStandard

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [acctid], '67',dbtrancnt, convert(varchar(15), dbtranamt) from Input_dbTransactions
where dbtranamt >'0'

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [acctid], '37',crtrancnt, convert(varchar(15), crtranamt)  from Input_DBTransactions
where crtranamt > '0'

Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

--Fetch_Error:
--close  tip_crsr
--deallocate  tip_crsr
GO
