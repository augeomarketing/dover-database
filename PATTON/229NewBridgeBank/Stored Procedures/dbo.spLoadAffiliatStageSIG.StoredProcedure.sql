USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStageSIG]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStageSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStageSIG]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStageSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	input_customerSIG  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

Create PROCEDURE [dbo].[spLoadAffiliatStageSIG]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, ''Credit'', @monthend, right(c.custid,4),  c.Statuscode, ''Credit Card'', c.LastName, 0, c.companyid
	from roll_CustomerSIG c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cr3, c.TipNumber, ''Credit'', @monthend, right(c.custid,4),  c.Statuscode, ''Credit Card'', c.LastName, 0, c.companyid
	from roll_CustomerSIG c where c.cr3 > ''0'' and c.cr3 not in ( Select acctid from Affiliat_Stage)

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

update affiliat_stage
set lastname = b.lastname 
from input_customerSIG b, affiliat_stage a
where   b.tipnumber = a.tipnumber

update affiliat_stage
set acctstatus = b.statuscode
from input_customerSIG b, affiliat_stage a
where   b.tipnumber = a.tipnumber

' 
END
GO
