USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spSIGDateAdded]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSIGDateAdded]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSIGDateAdded]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSIGDateAdded]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spSIGDateAdded]  
	@MonthEndingDate varchar(10)
AS
BEGIN

	UPDATE    Input_CustomerSIG
	SET       dateadded =cus.dateadded
	from input_customersig ic join customer cus on ic.tipnumber = cus.TIPNUMBER  
	
	UPDATE   input_CUSTOMERSIG
	SET  dateadded = CONVERT(datetime, @MonthEndingDate)
	WHERE dateadded  IS NULL or dateadded = '' ''
	
END	
' 
END
GO
