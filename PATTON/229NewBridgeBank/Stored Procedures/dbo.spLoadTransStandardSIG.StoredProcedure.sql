USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandardSIG]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandardSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandardSIG]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandardSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandardSIG] @DateAdded char(10) AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
--Truncate table TransStandard 

UPDATE  input_transactionsSIG SET tipnumber = b.tipnumber
FROM input_transactionsSIG a, affiliat_stage b
WHERE a.cardnumber = b.acctid

Insert into Input_Transactions_ErrorSIG 
	select * from input_transactionsSIG 
	where 	(cardnumber is null or cardnumber = '' '') or 
		(tipnumber is null or tipnumber = '' '')
	       
Delete from input_transactionsSIG
where 	(cardnumber is null or cardnumber = '' '') or
	(tipnumber is null or tipnumber = '' '') or
	(purchase is null and [returns] is null and bonus is null)  or 
	(purchase = ''0'' and [returns] = ''0'' and bonus = ''0'')

/******************************************************************************/
-- Load the TransStandard table with rows from input_transactionSIG
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], ''BI'', ''1'', convert(char(15), [bonus])  from input_transactionsSIG 
where (bonus > ''0'')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], ''63'', ''1'', convert(char(15), [purchase])  from input_transactionsSIG
where (purchase > ''0'')

Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @dateadded, [cardnumber], ''33'',''1'', convert(char(15), [returns])  from input_transactionsSIG
where ([returns] > ''0'')

/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode

' 
END
GO
