USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spInitializeTransHold]    Script Date: 01/11/2011 14:22:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInitializeTransHold]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInitializeTransHold]
GO

USE [229NewBridgeBank]
GO

/****** Object:  StoredProcedure [dbo].[spInitializeTransHold]    Script Date: 01/11/2011 14:22:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =================================================
-- Author:		Dan Foster
-- Create date: 12/17/2010
-- Description:	Initialize Transaction Hold Tables
-- =================================================
CREATE PROCEDURE [dbo].[spInitializeTransHold]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    truncate table dbo.Hold_DBTransactions
    truncate table dbo.Input_DBTransactions
    
END

GO


