USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spSIGName1andAddress4]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSIGName1andAddress4]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSIGName1andAddress4]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSIGName1andAddress4]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spSIGName1andAddress4]  AS
BEGIN

	UPDATE    input_customerSIG
	SET       lastname = SUBSTRING(acctname1,1, CHARINDEX('','', acctname1) - 1)
	WHERE     SUBSTRING(acctname1,1, 1) NOT LIKE '' '' AND acctname1 IS NOT NULL AND acctname1 LIKE ''%,%''
	
	UPDATE    input_CUSTOMERSIG
	SET       ACCTNAME1 = RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX('','', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + '' '' + SUBSTRING(ACCTNAME1, 1,CHARINDEX('','',ACCTNAME1) - 1)
	WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE '' '') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE ''%,%'')

END	
' 
END
GO
