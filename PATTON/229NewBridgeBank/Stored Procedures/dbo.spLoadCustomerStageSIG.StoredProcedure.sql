USE [229NewBridgeBank]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStageSIG]    Script Date: 12/01/2011 09:05:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStageSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerStageSIG]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerStageSIG]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStageSIG] @EndDate DateTime AS

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_CustomerSIG.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_CustomerSIG.acctNAME1),40 )
,ACCTNAME2 	= left(rtrim(Input_CustomerSIG.acctNAME2),40 )
,ACCTNAME3 	= left(rtrim(Input_CustomerSIG.acctNAME3),40 )
,ACCTNAME4 	= left(rtrim(Input_CustomerSIG.acctNAME4),40 )
,ACCTNAME5 	= left(rtrim(Input_CustomerSIG.acctNAME5),40 )
,ACCTNAME6 	= left(rtrim(Input_CustomerSIG.acctNAME6),40 )
,ADDRESS1 	= left(rtrim( Input_CustomerSIG.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( Input_CustomerSIG.ADDRESS2),40)
,ADDRESS4  	= left(rtrim( Input_CustomerSIG.ADDRESS4),40)
,CITY 		= Input_CustomerSIG.CITY
,STATE		= left(Input_CustomerSIG.STATE,2)
,ZIPCODE 	= ltrim(Input_CustomerSIG.ZIPCODE)
,HOMEPHONE 	= Ltrim(Input_CustomerSIG.homephone)
,STATUS	= Input_CustomerSIG.STATUSCODE
,statusdescription = Input_CustomerSIG.statusdescription
From Input_CustomerSIG
Where Input_CustomerSIG.TIPNUMBER = Customer_Stage.TIPNUMBER 


/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6,
	ADDRESS1,  ADDRESS2, address3, ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE, DATEADDED, STATUS, statusdescription,
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), left(rtrim(LASTNAME),40),
	left(rtrim(Input_CustomerSIG.acctNAME1),40) , 
	left(rtrim(Input_CustomerSIG.acctNAME2),40) , 
	left(rtrim(Input_CustomerSIG.acctNAME3),40) , 
	left(rtrim(Input_CustomerSIG.acctNAME4),40) , 
	left(rtrim(Input_CustomerSIG.acctNAME5),40) , 
	left(rtrim(Input_CustomerSIG.acctNAME6),40) , 
	Left(rtrim(ADDRESS1),40), 
  	Left(rtrim(ADDRESS2),40), 
	Left(rtrim(ADDRESS2),40), 
	Left(rtrim(ADDRESS4),40), 
	CITY, left(STATE,2), rtrim(ZIPCODE),
	Ltrim(Input_CustomerSIG.homephone) , 
	 @enddate, STATUSCODE,  statusdescription
	,0, 0, 0, 0
from  Input_CustomerSIG 
	where Input_CustomerSIG.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set STATUS = ''A'', statusdescription =  ''Active [A]'' 
Where STATUS IS NULL 

/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null

' 
END
GO
