/****** Object:  Table [dbo].[Input_Customer_Debit_Error]    Script Date: 07/23/2009 14:48:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer_Debit_Error](
	[Tipnumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[CustID] [varchar](25) NULL,
	[FirstName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](40) NULL,
	[Zipcode] [varchar](10) NULL,
	[HomePhone] [varchar](10) NULL,
	[WorkPhone] [varchar](10) NULL,
	[Status] [varchar](2) NULL,
	[Last4] [varchar](4) NULL,
	[BusinessFlag] [varchar](1) NULL,
	[EmployeeFlag] [varchar](1) NULL,
	[InstitutionNumber] [varchar](10) NULL,
	[AcctName1] [varchar](255) NULL,
	[AcctName2] [varchar](255) NULL,
	[DateAdded] [datetime] NULL,
	[StatusDescription] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
