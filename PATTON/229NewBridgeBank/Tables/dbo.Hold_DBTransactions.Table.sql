USE [229NewBridgeBank]
GO

/****** Object:  Table [dbo].[Hold_DBTransactions]    Script Date: 09/30/2010 11:16:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Hold_DBTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[Hold_DBTransactions]
GO

USE [229NewBridgeBank]
GO

/****** Object:  Table [dbo].[Hold_DBTransactions]    Script Date: 09/30/2010 11:16:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Hold_DBTransactions](
	[sid_hold_dbtransactions_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_hold_dbtransactions_CustID] [varchar](20) NOT NULL,
	[dim_hold_dbtransactions_AcctID] [varchar](25) NOT NULL,
	[dim_hold_dbtransactions_DBTrancode] [varchar](2) NULL,
	[dim_hold_dbtransactions_DBTranCnt] [bigint] NOT NULL,
	[dim_hold_dbtransactions_DBTranAmt] [decimal](18, 2) NOT NULL,
	[dim_hold_dbtransactions_CRTranCode] [varchar](2) NULL,
	[dim_hold_dbtransactions_CRTranCnt] [bigint] NOT NULL,
	[dim_hold_dbtransactions_CRTranAmt] [decimal](19, 2) NOT NULL,
	[dim_hold_dbtransactions_Tipnumber] [varchar](15) NULL,
	[dim_hold_dbtransactions_IN_DBTranAmt] [decimal](18, 0) NULL,
	[dim_hold_dbtransactions_IN_CRTranAmt] [decimal](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_hold_dbtransactions_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Hold_DBTransactions] ADD  DEFAULT ((0)) FOR [dim_hold_dbtransactions_DBTrancode]
GO

ALTER TABLE [dbo].[Hold_DBTransactions] ADD  DEFAULT ((0)) FOR [dim_hold_dbtransactions_DBTranAmt]
GO

ALTER TABLE [dbo].[Hold_DBTransactions] ADD  DEFAULT ((0)) FOR [dim_hold_dbtransactions_CRTranCode]
GO

ALTER TABLE [dbo].[Hold_DBTransactions] ADD  DEFAULT ((0)) FOR [dim_hold_dbtransactions_CRTranAmt]
GO

ALTER TABLE [dbo].[Hold_DBTransactions] ADD  DEFAULT ((0)) FOR [dim_hold_dbtransactions_IN_DBTranAmt]
GO

ALTER TABLE [dbo].[Hold_DBTransactions] ADD  DEFAULT ((0)) FOR [dim_hold_dbtransactions_IN_CRTranAmt]
GO

SET ANSI_PADDING ON
GO


