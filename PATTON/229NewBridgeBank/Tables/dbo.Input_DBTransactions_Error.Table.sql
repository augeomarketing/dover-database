/****** Object:  Table [dbo].[Input_DBTransactions_Error]    Script Date: 07/23/2009 14:48:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_DBTransactions_Error](
	[CustID] [varchar](20) NULL,
	[AcctID] [varchar](25) NULL,
	[DBTrancode] [varchar](2) NULL,
	[DBTranCnt] [varchar](4) NULL,
	[DBTranAmt] [money] NULL,
	[CRTranCode] [varchar](2) NULL,
	[CRTranCnt] [varchar](4) NULL,
	[CRTranAmt] [money] NULL,
	[Tipnumber] [varchar](15) NULL,
	[IN_DBTranAmt] [decimal](18, 0) NULL,
	[IN_CRTranAmt] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
