/****** Object:  Table [dbo].[GenTip]    Script Date: 07/23/2009 14:48:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[custid] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
