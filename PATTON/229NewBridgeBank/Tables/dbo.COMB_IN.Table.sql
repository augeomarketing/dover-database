/****** Object:  Table [dbo].[COMB_IN]    Script Date: 07/23/2009 14:48:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COMB_IN](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[LAST6_PRI] [varchar](6) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[LAST6_SEC] [varchar](6) NULL,
	[POINT_SEC] [float] NULL,
	[POINT_TOT] [float] NULL,
	[TANDATE] [varchar](8) NULL,
	[ERRMSG] [varchar](80) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
