/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 07/23/2009 14:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [varchar](15) NOT NULL,
	[Trancode] [varchar](2) NULL,
	[AcctID] [varchar](25) NULL,
	[CustID] [varchar](9) NULL,
	[DateAwarded] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
