/****** Object:  Table [dbo].[Input_Delete_Debit]    Script Date: 07/23/2009 14:48:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Delete_Debit](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CustID] [nvarchar](255) NULL,
	[AcctName1] [nvarchar](255) NULL,
	[AcctName2] [nvarchar](50) NULL,
	[StatusCode] [nvarchar](255) NULL,
	[CardNumber] [nvarchar](255) NULL,
	[TipNumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
