/****** Object:  Table [dbo].[TransactionSummary]    Script Date: 07/23/2009 14:49:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionSummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[Purchases] [decimal](18, 0) NULL,
	[Returned] [decimal](18, 0) NULL,
	[Bonuses] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
