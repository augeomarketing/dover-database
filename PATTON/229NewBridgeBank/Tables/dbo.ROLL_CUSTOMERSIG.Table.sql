USE [229NewBridgeBank]
GO
/****** Object:  Table [dbo].[ROLL_CUSTOMERSIG]    Script Date: 12/01/2011 09:00:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ROLL_CUSTOMERSIG]') AND type in (N'U'))
DROP TABLE [dbo].[ROLL_CUSTOMERSIG]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ROLL_CUSTOMERSIG]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ROLL_CUSTOMERSIG](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[CardNumber] [varchar](25) NULL,
	[CompanyName] [varchar](50) NULL,
	[CompanyID] [varchar](10) NULL,
	[SystemBankID] [varchar](4) NULL,
	[PrincipleBankID] [varchar](4) NULL,
	[AgentBankID] [varchar](4) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[acctname2] [varchar](40) NULL,
	[acctname3] [varchar](40) NULL,
	[acctname4] [varchar](40) NULL,
	[acctname5] [varchar](40) NULL,
	[acctname6] [varchar](40) NULL,
	[StatusCode] [char](1) NULL,
	[TIPNUMBER] [varchar](15) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[address3] [varchar](40) NULL,
	[address4] [varchar](40) NULL,
	[ZipCode] [varchar](15) NULL,
	[lastname] [char](40) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[fld4] [varchar](10) NULL,
	[open1] [char](106) NULL,
	[misc1] [varchar](20) NULL,
	[misc2] [varchar](20) NULL,
	[misc3] [varchar](20) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[BonusFlag] [char](1) NULL,
	[LastChangeDate] [datetime] NULL,
	[CustID] [varchar](9) NULL,
	[DateAdded] [datetime] NULL,
	[Misc7] [varchar](5) NULL,
	[Misc8] [varchar](5) NULL,
	[CR3] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
