/****** Object:  Table [dbo].[Debit_Summary]    Script Date: 07/23/2009 14:48:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Debit_Summary](
	[Rowid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Processed_Date] [datetime] NULL,
	[Total_Input_CustInfo] [numeric](18, 0) NULL,
	[Total_Input_TransInfo] [numeric](18, 0) NULL,
	[Total_Input_Debit] [decimal](18, 0) NULL,
	[Total_Input_Credit] [decimal](18, 0) NULL,
	[Total_Debit_Processed] [decimal](18, 0) NULL,
	[Total_Credit_Processed] [decimal](18, 0) NULL,
	[Total_Debit_Error] [decimal](18, 0) NULL,
	[Total_Credit_Error] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
