/****** Object:  Table [dbo].[QCReferenceTable]    Script Date: 07/23/2009 14:49:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCReferenceTable](
	[QCFlag] [varchar](2) NULL,
	[ROWCNT] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
