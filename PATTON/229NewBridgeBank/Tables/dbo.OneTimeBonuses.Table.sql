/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 07/23/2009 14:49:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [varchar](50) NOT NULL,
	[Trancode] [varchar](50) NULL,
	[AcctID] [varchar](50) NULL,
	[CustID] [varchar](9) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
