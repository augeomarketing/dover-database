/****** Object:  Table [dbo].[worktable]    Script Date: 07/23/2009 14:49:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[worktable](
	[Acctid] [varchar](25) NULL,
	[rowid] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
