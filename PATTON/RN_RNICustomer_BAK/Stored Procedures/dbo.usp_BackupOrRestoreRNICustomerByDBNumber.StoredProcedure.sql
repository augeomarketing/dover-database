USE [RN_RNICustomer_BAK]
GO

/****** Object:  StoredProcedure [dbo].[usp_BackupOrRestoreRNICustomerByDBNumber]    Script Date: 06/02/2015 16:30:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_BackupOrRestoreRNICustomerByDBNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_BackupOrRestoreRNICustomerByDBNumber]
GO

USE [RN_RNICustomer_BAK]
GO

/****** Object:  StoredProcedure [dbo].[usp_BackupOrRestoreRNICustomerByDBNumber]    Script Date: 06/02/2015 16:30:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_BackupOrRestoreRNICustomerByDBNumber]
	@DBNumber varchar(3),
	@Action varchar(1) ,--B=backup, R=Restore
	@errMsg varchar(500) output
/* SAMPLE CALL
	declare @DBNumber varchar(3)='712', @Action varchar(1)='B', @errMsg varchar(500) 
	exec usp_BackupOrRestoreRNICustomerByDBNumber @DBNumber , @Action, @errMsg output
	print @errMsg
	
	select * from RewardsNow.dbo.RNICustomer where dim_RNICustomer_TipPrefix = '712'
	select * from RN_RNICustomer_BAK.dbo.RNICustomer where dim_RNICustomer_TipPrefix = '712'
	
	update RewardsNow.dbo.RNICustomer set dim
*/	
AS



BEGIN TRY
	BEGIN TRANSACTION;
			--to back up Production RNICustomer records to RN_RNICustomer_BAK.dbo.RNICustomer
			if @Action='B'
			BEGIN 

				DELETE from RN_RNICustomer_BAK.dbo.RNICustomer where dim_RNICustomer_TipPrefix=@DBNumber

				insert into RN_RNICustomer_BAK.dbo.RNICustomer
				(dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_DateAdded, dim_RNICustomer_LastModified, sid_RNICustomer_ID, dim_RNICustomer_GUID,  dim_rnicustomer_periodsclosed)
				select 
					dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_DateAdded, dim_RNICustomer_LastModified, sid_RNICustomer_ID, dim_RNICustomer_GUID, dim_rnicustomer_periodsclosed
					from rewardsnow.dbo.RNICustomer  WHERE sid_dbprocessinfo_dbnumber=@DBNumber
			END
			
			
			-- RESTORE backed up records to RewardsNow.dbo.RNICustomer
			if @Action='R'
			BEGIN
				declare @NumCustRecs int=0,@NumBakRecs int=0;
				select @NumCustRecs=COUNT(*) from RewardsNow.dbo.RNICustomer where sid_dbprocessinfo_dbnumber=@DBNumber
				select @NumBakRecs=COUNT(*) from RN_RNICustomer_BAK.dbo.RNICustomer where sid_dbprocessinfo_dbnumber=@DBNumber
		
				--if there are NO backup records to restore OR the numbers differ dramatically. Raiserror jumps to the catch block
				if (@NumBakRecs=0 OR (abs(@NumCustRecs-@NumBakRecs)>@NumCustRecs/2) )
					begin 
						
						RAISERROR ('Confirm that a backup data is accurate',
						11,1)
					end 
			
			
				--DELETE the production records for this DBNumber
				DELETE from Rewardsnow.dbo.RNICustomer where sid_dbprocessinfo_dbnumber=@DBNumber


				--Restore the RewardsNow.dbo.RNICustomer records for this DBNumber
				set IDENTITY_INSERT RewardsNow.dbo.RNICustomer ON
				insert into RewardsNow.dbo.RNICustomer
				(dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_DateAdded, dim_RNICustomer_LastModified, sid_RNICustomer_ID, dim_RNICustomer_GUID,  dim_rnicustomer_periodsclosed)
				select 
					dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_DateAdded, dim_RNICustomer_LastModified, sid_RNICustomer_ID, dim_RNICustomer_GUID, dim_rnicustomer_periodsclosed
					from RN_RNICustomer_BAK.dbo.RNICustomer  WHERE sid_dbprocessinfo_dbnumber=@DBNumber
				set IDENTITY_INSERT RewardsNow.dbo.RNICustomer  OFF

			END


END TRY


BEGIN CATCH
	if @@TRANCOUNT> 0
		ROLLBACK TRAN
		set @errMsg = 'ERROR in procedure ' +  OBJECT_NAME(@@PROCID) + ':' + ERROR_MESSAGE()
		RETURN

END CATCH	

if @@TRANCOUNT> 0
	COMMIT TRAN

GO


