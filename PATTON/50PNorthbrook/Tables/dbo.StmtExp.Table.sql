USE [50PNorthbrook]
GO
/****** Object:  Table [dbo].[StmtExp]    Script Date: 09/24/2009 10:44:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [numeric](10, 0) NULL,
	[ENDBAL] [numeric](10, 0) NULL,
	[CCPURCHASE] [numeric](10, 0) NULL,
	[BONUS] [numeric](10, 0) NULL,
	[PNTADD] [numeric](10, 0) NULL,
	[PNTINCRS] [numeric](10, 0) NULL,
	[REDEEMED] [numeric](10, 0) NULL,
	[PNTRETRN] [numeric](10, 0) NULL,
	[PNTSUBTR] [numeric](10, 0) NULL,
	[PNTDECRS] [numeric](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
