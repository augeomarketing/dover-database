USE [655]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 03/18/2014 14:59:55 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4222D4EF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4222D4EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4222D4EF]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4316F928]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4316F928]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4316F928]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__440B1D61]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__440B1D61]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__440B1D61]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__44FF419A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__44FF419A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__44FF419A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__45F365D3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__45F365D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__45F365D3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__46E78A0C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__46E78A0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__46E78A0C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__47DBAE45]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__47DBAE45]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__47DBAE45]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__48CFD27E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__48CFD27E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__48CFD27E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__49C3F6B7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__49C3F6B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__49C3F6B7]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4AB81AF0]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4AB81AF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4AB81AF0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4BAC3F29]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4BAC3F29]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4BAC3F29]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4CA06362]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4CA06362]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4CA06362]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4D94879B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4D94879B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Point__4D94879B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Curre__4E88ABD4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Curre__4E88ABD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] DROP CONSTRAINT [DF__Monthly_A__Curre__4E88ABD4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_ErrorFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4222D4EF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4222D4EF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__4222D4EF]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4316F928]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4316F928]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__4316F928]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__440B1D61]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__440B1D61]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__440B1D61]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__44FF419A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__44FF419A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__44FF419A]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__45F365D3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__45F365D3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__45F365D3]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__46E78A0C]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__46E78A0C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__46E78A0C]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__47DBAE45]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__47DBAE45]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__47DBAE45]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__48CFD27E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__48CFD27E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__48CFD27E]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__49C3F6B7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__49C3F6B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__49C3F6B7]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4AB81AF0]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4AB81AF0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__4AB81AF0]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4BAC3F29]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4BAC3F29]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__4BAC3F29]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4CA06362]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4CA06362]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__4CA06362]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Point__4D94879B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Point__4D94879B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Point__4D94879B]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Monthly_A__Curre__4E88ABD4]') AND parent_object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_ErrorFile]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_A__Curre__4E88ABD4]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Monthly_A__Curre__4E88ABD4]  DEFAULT (0) FOR [Currentend]
END


End
GO
