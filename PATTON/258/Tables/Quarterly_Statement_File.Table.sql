USE [258]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 11/12/2013 13:36:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

USE [258]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 11/12/2013 13:36:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NOT NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NOT NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NOT NULL,
	[PointsBegin] [decimal](18, 0) default 0 ,
	[PointsEnd] [decimal](18, 0) default 0 ,
	[PointsPurchasedCR] [decimal](18, 0) default 0 ,
	[PointsPurchasedDB] [decimal](18, 0) default 0 ,
	[PointsBonus] [decimal](18, 0) default 0 ,
	[PointsAdded] [decimal](18, 0) default 0 ,
	[PointsIncreased] [decimal](18, 0) default 0 ,
	[PointsRedeemed] [decimal](18, 0) default 0 ,
	[PointsReturnedCR] [decimal](18, 0) default 0 ,
	[PointsReturnedDB] [decimal](18, 0) default 0 ,
	[PointsSubtracted] [decimal](18, 0) default 0 ,
	[PointsDecreased] [decimal](18, 0) default 0 ,
	[PointsExpire] [decimal](18, 0) default 0 ,
	[PointsBonusMN] [decimal](18, 0) default 0 ,
	[PurchasePoints] [decimal](18, 0) default 0 
	
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


