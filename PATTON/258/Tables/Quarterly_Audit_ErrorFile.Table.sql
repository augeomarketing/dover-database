USE [258]
GO

/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 11/12/2013 14:35:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Audit_ErrorFile]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Audit_ErrorFile]
GO

USE [258]
GO

/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 11/12/2013 14:35:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [varchar](15) NOT NULL,
	[PointsBegin] [decimal](18, 0) default 0 ,
	[PointsEnd] [decimal](18, 0) default 0 ,
	[PointsPurchasedCR] [decimal](18, 0) default 0 ,
	[PointsPurchasedDB] [decimal](18, 0) default 0 ,
	[PointsBonus] [decimal](18, 0) default 0 ,
	[PointsAdded] [decimal](18, 0) default 0 ,
	[PointsIncreased] [decimal](18, 0) default 0 ,
	[PointsRedeemed] [decimal](18, 0) default 0 ,
	[PointsReturnedCR] [decimal](18, 0) default 0 ,
	[PointsReturnedDB] [decimal](18, 0) default 0 ,
	[PointsSubtracted] [decimal](18, 0) default 0 ,
	[PointsDecreased] [decimal](18, 0) default 0 ,
	[PointsExpire] [decimal](18, 0) default 0 ,
	[PointsBonusMN] [decimal](18, 0) default 0 ,
	[PurchasePoints] [decimal](18, 0) default 0 ,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) default 0 
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


