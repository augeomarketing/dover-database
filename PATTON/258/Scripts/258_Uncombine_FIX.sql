
--============================
--backup sid filtered RNIcust 
----------------DONE
----------------use RewardsNow
----------------select c.*
----------------into z258RNICustomerBAK 
----------------from RNICustomer c where dim_RNICustomer_TipPrefix='258'
--========================================================
--backup the FI database
--------------DONE
--------------exec dbo.spBackupDBLiteSpeed_Tip_Descr '258','258PRE_Uncombine_BAK'
--============================
--select * from [258].dbo.ztmp258_FixList
--select * from [258].dbo.ztmp258_FixList where NeedsTip=1
--============================

	--loop through recs needing new tip
	--for each
	--	get newtip, cardnumber, tip into vars,
	--	update work table with new tip
	--next
--------DONE		
--------declare @ID int, @Tip varchar(15), @ssn varchar(9), @Cardnumber varchar(16), @Name1 varchar(50), @NewTip varchar(15)
--------declare c cursor FAST_FORWARD for  
--------select ID, TIP, ssn , Cardnumber, Name1 from [258].dbo.ztmp258_FixList where NeedsTip=1	

---------- loop through tips needing new tip
--------open c
--------fetch next from c into @ID , @Tip, @ssn , @Cardnumber, @Name1
--------while @@FETCH_STATUS = 0
--------Begin
--------		--	get newtip, cardnumber, tip into vars,
--------		exec RewardsNow.dbo.usp_GetAndPutNextTipNumber '258', @NewTip output
		
--------		--	update work table with new tip	
--------		update [258].dbo.ztmp258_FixList
--------			set NewTip=@NewTip 
--------			where  ID=@ID
	
	
	
--------	fetch next from c into @ID , @Tip, @ssn , @Cardnumber, @Name1
--------end
--------close c
--------deallocate c

--=================================================================
--update the RNICust tipnumber where=tip and cardnumber to NewTip 
/*  22 recs
select rnc.*
from
RNICustomer rnc join [258].dbo.ztmp258_FixList w on 
	rnc.dim_RNICustomer_RNIId=w.tip
and rnc.dim_RNICustomer_CardNumber=w.CardNumber
and w.needstip=1
*/
update rnc
set rnc.dim_RNICustomer_RNIId =w.NewTip
from
RNICustomer rnc join [258].dbo.ztmp258_FixList w on 
	rnc.dim_RNICustomer_RNIId=w.tip
and rnc.dim_RNICustomer_CardNumber=w.CardNumber
and w.needstip=1




--=================================================================
--delete from afiliate for cards getting new tip
/* 27 recs
select w.Tip,w.cardnumber, a.* from [258].dbo.AFFILIAT a  join [258].dbo.ztmp258_FixList w 
	on a.ACCTID=w.cardnumber
	where w.NeedsTip=1
*/	
delete a from [258].dbo.AFFILIAT a  join [258].dbo.ztmp258_FixList w 
	on a.ACCTID=w.cardnumber
	where w.NeedsTip=1

--=================================================================
--run	dbo.usp_RNICustomerInsertAffiliat '258', '04/30/2014'
exec RewardsNow.dbo.usp_RNICustomerInsertAffiliat '258', '04/30/2014'

--=================================================================
--update the history set tipnumber =Newtip from  worktable joined on card
/* 20 recs
select h.* 
from [258].dbo.HISTORY h join [258].dbo.ztmp258_FixList z 
	on h.acctid=z.CardNumber
	where z.needsTip=1
*/
update h
set h.tipnumber =z.NewTip
from [258].dbo.HISTORY h join [258].dbo.ztmp258_FixList z 
	on h.acctid=z.CardNumber
	where z.needsTip=1
 
