USE [254]
GO
/****** Object:  StoredProcedure [dbo].[usp_QuarterlyStatement_std_M]    Script Date: 11/22/2013 15:54:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QuarterlyStatement_std_M]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_QuarterlyStatement_std_M]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_QuarterlyStatement_std_M]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[usp_QuarterlyStatement_std_M]
	-- Add the parameters for the stored procedure here
	@StartDateParm datetime, 
	@EndDateParm datetime, 
	@TipFirst nvarchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

Declare @StartDate DATE
Declare @EndDate DATE
declare @DBName varchar(50), @SQLUpdate nvarchar(2000), @SQLInsert nvarchar(2000), @SQLTruncate nvarchar(2000)

set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)


set @Startdate = convert(DATE, @StartDateParm)
set @Enddate = convert(DATE, @EndDateParm)	


/* Truncate the FI.Quarterly_Statement_File  */
Truncate Table Quarterly_Statement_File

/* Load the statement file from the customer table  */

insert into Quarterly_Statement_File 
(
	tipnumber
	, acctname1
	, acctname2
	, address1
	, address2
	, address3
	, citystatezip
	, TOT_Begin
	, TOT_End
	, TOT_Add
	, TOT_Subtract
	, TOT_Net
	, TOT_Bonus
	, TOT_Redeem
	, TOT_ToExpire
	, CRD_Purch
	, CRD_Return
	, DBT_Purch
	, DBT_Return
	, ADJ_Add
	, ADJ_Subtract
	, PointsBonusMN
)
select 
	cst.tipnumber
	, acctname1
	, acctname2
	, address1
	, address2
	, address3
	, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode) AS CityStateZip
	, 0 AS TOT_Begin
	, 0 AS TOT_End
	, 0 AS TOT_Add
	, 0 AS TOT_Subtract
	, 0 AS TOT_Net
	, 0 AS TOT_Bonus
	, 0 AS TOT_Redeem
	, 0 AS TOT_ToExpire
	, 0 AS CRD_Purch
	, 0 AS CRD_Return
	, 0 AS DBT_Purch
	, 0 AS DBT_Return
	, 0 AS ADJ_Add
	, 0 AS ADJ_Subtract
	, 0 AS PointsBonusMN	
from customer cst
inner join RN1.KEMBA.dbo.[1Security] os
on cst.TIPNUMBER = os.tipnumber
where os.EmailStatement = ''N''





SET @StartDate = CONVERT(DATE, @startdate)

/*
update QSF
SET CRD_PURCH = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN #tmpHistory TH
	ON QSF.Tipnumber = TH.TIPNUMBER
WHERE TH.TRANCODE = 
*/

SELECT TIPNUMBER, TRANCODE, SUM(POINTS * RATIO) POINTS 
INTO #tmpHistory
FROM HISTORY
WHERE CONVERT(DATE, HISTDATE) BETWEEN @startdate and @EndDate
GROUP BY TIPNUMBER, TRANCODE

---------------------------------------------------------------------------------
/* Load the statmement file with DEBIT purchases          */

update QSF
SET DBT_PURCH = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN #tmpHistory TH
	ON QSF.Tipnumber = TH.TIPNUMBER
WHERE TH.TRANCODE = ''64''

select * from Quarterly_Statement_File

---------------------------------------------------------------------------------
/* Load the statmement file with DEBIT RETURNS          */

update QSF
SET DBT_RETURN = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN #tmpHistory TH
	ON QSF.Tipnumber = TH.TIPNUMBER
WHERE TH.TRANCODE = ''34''

---------------------------------------------------------------------------------
-- Load the statmement file with plus adjustments   

update QSF
SET ADJ_Add = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS) POINTS
	FROM #tmpHistory 
	WHERE TRANCODE IN (''IE'', ''PP'')	
	GROUP BY TIPNUMBER
) TH
	ON QSF.Tipnumber = TH.TIPNUMBER


--------------------------------------------------------------------------------------------------
--Load the statmement file with minus adjustments    

update QSF
SET CRD_PURCH = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN #tmpHistory TH
	ON QSF.Tipnumber = TH.TIPNUMBER
WHERE TH.TRANCODE = ''DE''


/* Load the statmement file with bonuses            */
update QSF
SET TOT_Bonus = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS) POINTS
	FROM #tmpHistory 
	WHERE TRANCODE LIKE ''[BFG]%''
		AND TRANCODE NOT LIKE ''[FGH][09]''
	GROUP BY TIPNUMBER
) TH
	ON QSF.Tipnumber = TH.TIPNUMBER


/* Load the statmement file with Merchant bonuses            */
update QSF
SET PointsBonusMN = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS) POINTS
	FROM #tmpHistory 
	WHERE TRANCODE LIKE ''[FGH][09]''
	GROUP BY TIPNUMBER
) TH
	ON QSF.Tipnumber = TH.TIPNUMBER

----------------------------------------------------------------------------------------------------
--Load the statmement file with total point increases 
update   dbo.Quarterly_Statement_File
set TOT_Add= CRD_Purch + DBT_Purch +  ADJ_Add  + TOT_Bonus + PointsBonusMN 


-- Load the statmement file with redemptions          

update Quarterly_Statement_File
set TOT_Redeem=(select sum(points*ratio*-1) from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')
where exists(select * from History where tipnumber=Quarterly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like ''R%'')


update QSF
SET TOT_Redeem = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS) POINTS
	FROM #tmpHistory 
	WHERE TRANCODE LIKE ''R%''
	GROUP BY TIPNUMBER
) TH
	ON QSF.Tipnumber = TH.TIPNUMBER


-- Load the statmement file with Decrease redeemed          

update QSF
SET TOT_Redeem = TOT_Redeem - ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS) POINTS
	FROM #tmpHistory 
	WHERE TRANCODE LIKE ''DR''
	GROUP BY TIPNUMBER
) TH
	ON QSF.Tipnumber = TH.TIPNUMBER


---------------------------------------------------------------------------------------------------
-- Add expired Points 

update QSF
SET TOT_ToExpire = ABS(POINTS)
FROM Quarterly_Statement_File QSF
INNER JOIN #tmpHistory TH
	ON QSF.Tipnumber = TH.TIPNUMBER
WHERE TH.TRANCODE = ''XP''

-- Load the statmement file with total point decreases 
update Quarterly_Statement_File
set TOT_Subtract=TOT_Redeem + CRD_Return + DBT_Return + ADJ_Subtract + TOT_ToExpire

----------------------------------------------------------------
-- Load the statmement file with the Beginning balance for the Period 

Update QSF 
SET TOT_Begin = HST.POINTS
FROM Quarterly_Statement_File QSF 
INNER JOIN 
(
	SELECT TIPNUMBER, SUM(POINTS * RATIO) POINTS
	FROM HISTORY
	WHERE CONVERT(date, histdate) < @StartDate
	GROUP BY TIPNUMBER
) HST
ON QSF.Tipnumber = HST.TIPNUMBER

-------------------------------------------------------------------

--Load the statmement file with ENDING points 
update  Quarterly_Statement_File
set TOT_End=TOT_Begin + TOT_Add - TOT_Subtract 

--Load the statmement file with Tot_NET
update  Quarterly_Statement_File
set TOT_NET= TOT_Add - TOT_Subtract 

DROP TABLE #tmpHistory

END


' 
END
GO
