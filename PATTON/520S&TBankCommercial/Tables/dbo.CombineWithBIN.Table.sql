USE [520S&TBankCommercial]
GO
/****** Object:  Table [dbo].[CombineWithBIN]    Script Date: 09/25/2009 10:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CombineWithBIN](
	[PRIMARY] [char](16) NULL,
	[PrimaryTip] [char](15) NULL,
	[SECONDARY] [char](16) NULL,
	[SecondaryTip] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
