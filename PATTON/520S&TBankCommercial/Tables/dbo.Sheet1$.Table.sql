USE [520S&TBankCommercial]
GO
/****** Object:  Table [dbo].[Sheet1$]    Script Date: 09/25/2009 10:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sheet1$](
	[F1] [nvarchar](255) NULL,
	[OldTip] [char](15) NULL,
	[F2] [nvarchar](255) NULL,
	[F3] [nvarchar](255) NULL,
	[newtip] [char](15) NULL,
	[F4] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL,
	[reccnt] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
