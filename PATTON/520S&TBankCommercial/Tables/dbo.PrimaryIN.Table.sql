USE [520S&TBankCommercial]
GO
/****** Object:  Table [dbo].[PrimaryIN]    Script Date: 09/25/2009 10:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PrimaryIN](
	[Company Name] [nvarchar](255) NULL,
	[Cardholder Name] [nvarchar](255) NULL,
	[Primary Account Number] [float] NULL,
	[Tipnumber] [nvarchar](255) NULL,
	[Acctname1] [nvarchar](255) NULL,
	[Acctname2] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[Address3] [nvarchar](255) NULL,
	[CityStateZip] [nvarchar](255) NULL,
	[stdate] [nvarchar](255) NULL
) ON [PRIMARY]
GO
