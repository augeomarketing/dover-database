USE [520S&TBankCommercial]
GO
/****** Object:  Table [dbo].[S&TFIX]    Script Date: 09/25/2009 10:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[S&TFIX](
	[Tipnumber] [nvarchar](255) NULL,
	[Acctnum] [nvarchar](255) NULL,
	[PointsReturnedCR] [float] NULL,
	[F14] [nvarchar](255) NULL
) ON [PRIMARY]
GO
