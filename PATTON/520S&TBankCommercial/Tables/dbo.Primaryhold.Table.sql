USE [520S&TBankCommercial]
GO
/****** Object:  Table [dbo].[Primaryhold]    Script Date: 09/25/2009 10:30:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Primaryhold](
	[acctid] [nchar](25) NULL,
	[acctname1] [nchar](40) NULL,
	[acctname2] [nchar](40) NULL,
	[lastname] [nchar](40) NULL,
	[address1] [nchar](40) NULL,
	[address2] [nchar](40) NULL,
	[address3] [nchar](40) NULL,
	[address4] [nchar](40) NULL,
	[TIPNUMBER] [nchar](15) NULL,
	[CITY] [nchar](40) NULL,
	[STATE] [nchar](2) NULL,
	[ZIPCODE] [nchar](15) NULL
) ON [PRIMARY]
GO
