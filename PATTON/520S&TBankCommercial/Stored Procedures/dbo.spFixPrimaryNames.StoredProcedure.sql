USE [520S&TBankCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spFixPrimaryNames]    Script Date: 09/25/2009 10:30:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spFixPrimaryNames]
as

update customer
set acctname1=b.acctname1, acctname2=b.acctname2, acctname3=null, acctname4=null, acctname5=null, acctname6=null, lastname=b.lastname, address1=b.address1, address2=b.address2, address3=b.address3, address4=b.address4, city=b.city, state=b.state, zipcode=b.zipcode
from customer a, Primaryhold b
where a.tipnumber=b.tipnumber

update affiliat
set lastname=b.lastname
from affiliat a, Primaryhold b
where a.tipnumber=b.tipnumber
GO
