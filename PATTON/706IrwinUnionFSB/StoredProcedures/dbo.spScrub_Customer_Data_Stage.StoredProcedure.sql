USE [706IrwinUnionFSB]
GO
/****** Object:  StoredProcedure [dbo].[spScrub_Customer_Data_Stage]    Script Date: 11/16/2010 10:29:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spScrub_Customer_Data_Stage]
AS

/*****************************************************/
/*    Set Customer Last Names                        */
/*****************************************************/   

UPDATE roll_consumer SET lstname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1,1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE   roll_consumer SET lstname = ACCTNAME1 
WHERE     (lstname = 'LLC') OR (lstname = 'BankCard') OR (lstname = 'INC') OR (lstname = 'INC.') OR (lstname = 'LTD') OR (lstname = 'Payable') or (lstname = ' ') or (lstname is null) 

UPDATE roll_consumer SET lstname = ACCTNAME1
WHERE len(lstname) = 2

UPDATE roll_commercial SET lastname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1,1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE   roll_commercial SET lastname = ACCTNAME1 
WHERE     (lastname = 'LLC') OR (lastname = 'BankCard') OR (lastname = 'INC') OR (lastname = 'INC.') OR (lastname = 'LTD') OR (lastname = 'Payable') or (lastname = ' ') or (lastname is null) 

UPDATE roll_commercial SET lastname = ACCTNAME1
WHERE len(lastname) = 2

UPDATE roll_commercial_rollup SET lastname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1,1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE   roll_commercial_rollup SET lastname = ACCTNAME1 
WHERE     (lastname = 'LLC') OR (lastname = 'BankCard') OR (lastname = 'INC') OR (lastname = 'INC.') OR (lastname = 'LTD') OR (lastname = 'Payable') or (lastname = ' ') or (lastname is null) 

UPDATE roll_commercial_rollup SET lastname = ACCTNAME1
WHERE len(lastname) = 2

/*****************************************************/
/*    Set Roll_Consumer fields for Display           */
/*****************************************************/   

UPDATE roll_consumer SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE roll_consumer SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

UPDATE roll_consumer SET Address4 = ltrim(rtrim(City + '  ' + State + ' ' + left(zipcode,5)  + '-' + right(zipcode,4)))

UPDATE roll_consumer SET externalstatuscode = 'A'

update roll_consumer set segmentcode = 'CN'

/*****************************************************/
/*  Scrub Commercial Customer Information            */
/*****************************************************/   

UPDATE roll_commercial SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE roll_commercial SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

UPDATE roll_commercial SET Address4 = ltrim(rtrim(City + '  ' + State +  ' ' + left(zipcode,5)  + '-' + right(zipcode,4)))

UPDATE roll_commercial SET statuscode = 'A'

update roll_commercial set segmentcode = 'CM'

/*****************************************************/
/*  Scrub Commercial Customer Rollups Information    */
/*****************************************************/   

UPDATE roll_commercial_rollup SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1,CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' 
+ SUBSTRING(ACCTNAME1, 1, CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE roll_commercial_rollup SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2,CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' 
+ SUBSTRING(ACCTNAME2, 1, CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

UPDATE roll_commercial_rollup SET Address4 = ltrim(rtrim(City + '  ' + State +  ' ' + left(zipcode,5)  + '-' + right(zipcode,4)))

UPDATE roll_commercial_rollup SET statuscode = 'A'

update roll_commercial_rollup set segmentcode = 'CM'

exec spRemovePunctuation
GO
