USE [706IrwinUnionFSB]
GO
/****** Object:  StoredProcedure [dbo].[sp706CGenerateTIPNumbers_Stage]    Script Date: 11/16/2010 10:29:20 ******/
/* 
Comments:
	RDT 2011/05/17
		Added delete code for sproc.
		Removed code to get MAX(Tipnumber). 
*/

/****** Object:  StoredProcedure [dbo].[sp706CGenerateTIPNumbers_Stage]    Script Date: 05/17/2011 11:26:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp706CGenerateTIPNumbers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp706CGenerateTIPNumbers_Stage]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp706CGenerateTIPNumbers_Stage]
AS 

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.cr3 = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/*
update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.companyid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
*/

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')
DELETE FROM GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber, tipnumber
from roll_commercial

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '706', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

-- RDT 2011/05/17 SELECT @newnum = max(TIPNUMBER) from affiliat_stage

if @newnum is null or @newnum = 0
	begin
	set @newnum= 706000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '706', @newnum

update roll_commercial 
set Tipnumber = b.tipnumber
from roll_commercial a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')
GO
