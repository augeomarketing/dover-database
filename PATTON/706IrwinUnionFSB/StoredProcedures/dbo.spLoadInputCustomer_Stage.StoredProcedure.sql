USE [706IrwinUnionFSB]
GO
/****** Object:  StoredProcedure [dbo].[spLoadInputCustomer_Stage]    Script Date: 11/16/2010 10:29:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadInputCustomer_Stage] @startdate as varchar(10)
AS

Truncate Table Input_Customer

INSERT INTO Input_Customer (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_consumer

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lstname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.externalstatuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_consumer b
WHERE     a.tipnumber = b.tipnumber

INSERT INTO Input_Customer (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Commercial
where tipnumber not in(select tipnumber from input_customer)

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lastname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial b
WHERE     a.tipnumber = b.tipnumber


INSERT INTO Input_Customer (tipnumber)
SELECT DISTINCT tipnumber FROM Roll_Commercial_rollup
where tipnumber not in(select tipnumber from input_customer)


UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lastname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial_rollup b
WHERE    misc7 = 'Z' and a.tipnumber = b.tipnumber

UPDATE    input_customer
SET              custid = b.custid, cardnumber = b.cardnumber, acctname1 = b.acctname1, lastname = b.lastname, address1 = b.address1, address2 = b.address2, address4 = b.address4,
                      city = b.city, state = b.state, zipcode = b.zipcode, homephone = b.homephone, statuscode = b.statuscode, systembankid = b.systembankid, 
                      principlebankid = b.principlebankid, agentbankid = b.agentbankid, acctname2 = b.acctname2
FROM         input_customer a, roll_commercial_rollup b
WHERE    a.principlebankid is null and a.tipnumber = b.tipnumber


update input_customer set statusdescription = 'Active[A]' where statuscode = 'A'

UPDATE input_customer SET dateadded = (SELECT dateadded FROM CUSTOMER_stage
WHERE customer_stage.tipnumber = input_customer.tipnumber)

UPDATE input_customer SET DateAdded = CONVERT(datetime, @startdate)
WHERE (DateAdded IS NULL)
GO
