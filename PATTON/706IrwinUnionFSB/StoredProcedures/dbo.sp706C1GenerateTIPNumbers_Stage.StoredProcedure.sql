USE [706IrwinUnionFSB]
GO
/****** Object:  StoredProcedure [dbo].[sp706C1GenerateTIPNumbers_Stage]    Script Date: 11/16/2010 10:29:20 ******/
/* 
Comments:
	RDT 2011/05/17
		Added delete code for sproc.
		Removed code to get MAX(Tipnumber). 
*/

/****** Object:  StoredProcedure [dbo].[sp706CGenerateTIPNumbers_Stage]    Script Date: 05/17/2011 11:26:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp706C1GenerateTIPNumbers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp706C1GenerateTIPNumbers_Stage]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp706C1GenerateTIPNumbers_Stage]
AS 

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,affiliat_stage b
where a.cr3 = b.acctid

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,affiliat_stage b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,affiliat_stage b
where a.misc8 = b.secid and (a.tipnumber is null or a.tipnumber = ' ')


--DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select distinct misc8 as custid, tipnumber
from roll_commercial_rollup where tipnumber is null or tipnumber = ' '

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed '706', @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

-- RDT 2011/05/17SELECT @newnum = max(TIPNUMBER) from affiliat_stage

if @newnum is null or @newnum = 0
	begin
	set @newnum= 706000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed '706', @newnum


update roll_commercial_rollup 
set Tipnumber = b.tipnumber
from roll_commercial_rollup a,gentip b
where a.misc8 = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
