USE [706IrwinUnionFSB]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/16/2010 10:29:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend,  right(custid,4),  c.ExternalStatuscode, 'Credit Card', c.LstName, 0, c.custid
	from roll_Consumer c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage (AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, ytdearned, custid)
SELECT CR3 AS acctid, TipNumber, 'Credit', @monthend, right(custid,4), externalstatuscode,'Credit Card', lstname, ytdearned = '0',custid 
FROM Roll_Consumer WHERE ((CR3 IS NOT NULL) AND (CR3 <> '0')) and cr3 not in (select acctid from affiliat_stage)

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend,  right(custid,4),  c.Statuscode, 'Credit Card', c.LastName, 0, c.custid
	from roll_Commercial c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage (AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, ytdearned, custid)
SELECT CR3 AS acctid, TipNumber, 'Credit', @monthend, right(custid,4), statuscode,'Credit Card', lastname, ytdearned = '0',custid 
FROM Roll_Commercial WHERE ((CR3 IS NOT NULL) AND (CR3 <> '0')) and cr3 not in (select acctid from affiliat_stage)

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend,  right(custid,4),  c.Statuscode, 'Credit Card', c.LastName, 0, c.custid
	from roll_Commercial_rollup c where c.cardnumber not in ( Select acctid from Affiliat_Stage)

Insert Into Affiliat_Stage (AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, ytdearned, custid)
SELECT CR3 AS acctid, TipNumber, 'Credit', @monthend, right(custid,4), statuscode,'Credit Card', lastname, ytdearned = '0',custid 
FROM Roll_Commercial_Rollup WHERE ((CR3 IS NOT NULL) AND (CR3 <> '0')) and cr3 not in (select acctid from affiliat_stage)

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypedesc = T.AcctTypedesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
GO
