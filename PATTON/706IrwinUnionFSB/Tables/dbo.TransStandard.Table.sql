USE [706IrwinUnionFSB]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 11/16/2010 10:30:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[TIP] [nvarchar](15) NULL,
	[TranDate] [nvarchar](10) NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](50) NULL,
	[Ratio] [nvarchar](4) NULL
) ON [PRIMARY]
GO
