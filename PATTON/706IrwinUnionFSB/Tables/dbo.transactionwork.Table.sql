USE [706IrwinUnionFSB]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 11/16/2010 10:30:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](25) NOT NULL,
	[SystemBankIdentifier] [varchar](4) NOT NULL,
	[PrincipleBankIdentifier] [varchar](3) NOT NULL,
	[AgentBankIdentifier] [varchar](4) NOT NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 2) NOT NULL,
	[Returns] [decimal](18, 2) NOT NULL,
	[Bonus] [decimal](18, 2) NOT NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
