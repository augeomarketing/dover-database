USE [706IrwinUnionFSB]
GO
/****** Object:  Table [dbo].[New Table]    Script Date: 11/16/2010 10:30:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[New Table](
	[CardNumber] [varchar](25) NULL,
	[SystemBankID] [varchar](4) NULL,
	[PrincipleBankID] [varchar](4) NULL,
	[AgentBankID] [varchar](4) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[InternalStatusCode] [char](1) NULL,
	[ExternalStatusCode] [char](1) NULL,
	[CustID] [varchar](9) NULL,
	[AcctID2] [char](40) NULL,
	[LastChangeDate] [datetime] NULL,
	[LstName] [char](40) NULL,
	[Address4] [varchar](40) NULL,
	[Misc7] [char](8) NULL,
	[Misc8] [char](8) NULL,
	[CR3] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
