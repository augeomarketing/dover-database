USE [706IrwinUnionFSB]
GO
/****** Object:  Table [dbo].[FTUB_Work_Table]    Script Date: 11/16/2010 10:30:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FTUB_Work_Table](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Tipnumber] [varchar](15) NULL,
	[Acctid] [varchar](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
