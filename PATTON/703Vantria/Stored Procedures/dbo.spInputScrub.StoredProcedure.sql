USE [703Vantria]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 11/19/2010 10:19:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrub]
GO

USE [703Vantria]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 11/19/2010 10:19:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spInputScrub] @enddate varchar(10)AS

/****************************************************************************/
/*Changes: 1) update address4 processing for foreign addresses              */
/****************************************************************************/

Truncate Table  roll_Customer_error
Truncate Table Input_Transaction_error

UPDATE roll_customer SET lastname = SUBSTRING(acctname1,1, CHARINDEX(',', acctname1) - 1)
WHERE SUBSTRING(acctname1, 1, 1) NOT LIKE ' ' AND acctname1 IS NOT NULL AND acctname1 LIKE '%,%'

UPDATE    roll_customer
SET ACCTNAME1 = ltrim(RTRIM(SUBSTRING(ACCTNAME1, CHARINDEX(',', ACCTNAME1) + 1, LEN(RTRIM(ACCTNAME1)))) + ' ' + SUBSTRING(ACCTNAME1, 1, 
                      CHARINDEX(',', ACCTNAME1) - 1))
WHERE     (SUBSTRING(ACCTNAME1, 1, 1) NOT LIKE ' ') AND (ACCTNAME1 IS NOT NULL) AND (ACCTNAME1 LIKE '%,%')

UPDATE    roll_customer
SET ACCTNAME2 = ltrim(RTRIM(SUBSTRING(ACCTNAME2, CHARINDEX(',', ACCTNAME2) + 1, LEN(RTRIM(ACCTNAME2)))) + ' ' + SUBSTRING(ACCTNAME2, 1, 
                      CHARINDEX(',', ACCTNAME2) - 1))
WHERE     (SUBSTRING(ACCTNAME2, 1, 1) NOT LIKE ' ') AND (ACCTNAME2 IS NOT NULL) AND (ACCTNAME2 LIKE '%,%')

UPDATE    roll_customer
SET ACCTNAME3 = ltrim(RTRIM(SUBSTRING(ACCTNAME3, CHARINDEX(',', ACCTNAME3) + 1, LEN(RTRIM(ACCTNAME3)))) + ' ' + SUBSTRING(ACCTNAME3, 1, 
                      CHARINDEX(',', ACCTNAME3) - 1))
WHERE     (SUBSTRING(ACCTNAME3, 1, 1) NOT LIKE ' ') AND (ACCTNAME3 IS NOT NULL) AND (ACCTNAME3 LIKE '%,%')

UPDATE    roll_customer SET dateadded =
    (SELECT dateadded FROM CUSTOMER_stage
     WHERE      customer_stage.tipnumber = roll_customer.tipnumber)

UPDATE   roll_customer SET dateadded = CONVERT(datetime, @enddate)
WHERE     (dateadded  IS NULL) or dateadded = ' '

UPDATE    roll_customer SET statuscode = 'A'

update roll_customer
set [acctname1]=replace([acctname1],char(39), ' '),  [acctname2]=replace([acctname2],char(39), ' '), address1=replace(address1,char(39), ' '),
address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update roll_customer
set [acctname1]=replace([acctname1],char(140), ' '), [acctname2]=replace([acctname2],char(140), ' '), address1=replace(address1,char(140), ' '),
address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update roll_customer
set [acctname1]=replace([acctname1],char(44), ' '), [acctname2]=replace([acctname2],char(44), ' '), address1=replace(address1,char(44), ' '),
address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update roll_customer
set [acctname1]=replace([acctname1],char(46), ' '), [acctname2]=replace([acctname2],char(46), ' '), address1=replace(address1,char(46), ' '),
address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update roll_customer
set [acctname1]=replace([acctname1],char(34), ' '), [acctname2]=replace([acctname2],char(34), ' '), address1=replace(address1,char(34), ' '),
address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update roll_customer
set [acctname1]=replace([acctname1],char(35), ' '), [acctname2]=replace([acctname2],char(35), ' '), address1=replace(address1,char(35), ' '),
address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')


-- CHANGE (1)
update roll_customer set address4 = city + ' ' + state + ' ' + left(zipcode,5) + '-' + right(zipcode,4)
where len(zipcode) > 5

update roll_customer set address4 = city + ' ' + state + ' ' + zipcode
where len(zipcode) = 5

update roll_customer set address4 = city + ' ' + state
where zipcode is null

--------------- Input Customer table

/********************************************************************/
/* Remove roll_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into roll_customer_error 
	select * from roll_customer 
	where (custid is null or custid = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (tipnumber is null or tipnumber = ' ')

delete from roll_customer 
where (custid is null or custid = ' ') or  
           (cardnumber is null or cardnumber = ' ') or
           (tipnumber is null or tipnumber = ' ')

--update roll_customer set homephone =  Left(roll_customer.homephone,3) + substring(roll_customer.homephone,5,3) + right(roll_customer.homephone,4)
--update roll_customer set workphone  = Left(roll_customer.workphone,3) + substring(roll_customer.workphone,5,3) + right(roll_customer.workphone,4)

--UPDATE  roll_customer SET misc2 = CustID

--------------- Input Transaction table

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where 	(cardnumber is null or cardnumber = ' ') or 
		(purchase is null and [returns] is null and bonus is null) or 
		(purchase = '0' and [returns] = '0' and bonus = '0') 
	       
Delete from Input_Transaction
where 	(cardnumber is null or cardnumber = ' ') or 
	(purchase is null and [returns] is null and bonus is null)  or 
	(purchase = '0' and [returns] = '0' and bonus = '0')


GO


