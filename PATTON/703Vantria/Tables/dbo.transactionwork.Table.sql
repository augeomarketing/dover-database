/****** Object:  Table [dbo].[transactionwork]    Script Date: 02/26/2009 09:26:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[CardNumber] [varchar](25) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](3) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 0) NULL CONSTRAINT [DF_transactionwork_Purchase]  DEFAULT (0),
	[Returns] [decimal](18, 0) NULL CONSTRAINT [DF_transactionwork_Returns]  DEFAULT (0),
	[Bonus] [decimal](18, 0) NULL CONSTRAINT [DF_transactionwork_Bonus]  DEFAULT (0),
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
