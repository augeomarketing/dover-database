/****** Object:  Table [dbo].[DeleteWork]    Script Date: 02/26/2009 09:26:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeleteWork](
	[TIPNUMBER] [varchar](15) NULL,
	[AcctID] [varchar](16) NULL,
	[CustID] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
