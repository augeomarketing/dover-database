/****** Object:  Table [dbo].[Bonus Table]    Script Date: 02/26/2009 09:25:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bonus Table](
	[TipNumber] [varchar](15) NULL,
	[CustID] [varchar](9) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
