/****** Object:  Table [dbo].[Input_transaction]    Script Date: 02/26/2009 11:47:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_transaction](
	[CardNumber] [varchar](25) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](3) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[CustID] [varchar](9) NULL,
	[Purchase] [decimal](18, 2) NULL,
	[Returns] [decimal](18, 2) NULL,
	[Bonus] [decimal](18, 2) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
