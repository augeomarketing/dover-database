USE [277]
GO
/****** Object:  Table [dbo].[impTransaction]    Script Date: 01/16/2015 08:59:14 ******/
ALTER TABLE [dbo].[impTransaction] DROP CONSTRAINT [DF_impTransaction_TransactionAmt]
GO
DROP TABLE [dbo].[impTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impTransaction](
	[sid_impTransaction_id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssocNbr] [varchar](3) NULL,
	[System] [varchar](4) NULL,
	[Prin] [varchar](4) NULL,
	[TransactionDt] [varchar](8) NULL,
	[CCAcctNbr] [varchar](16) NOT NULL,
	[TransactionCd] [varchar](3) NULL,
	[TransactionDesc] [varchar](50) NULL,
	[TransactionAmt] [money] NOT NULL,
	[AccountSts] [varchar](1) NOT NULL,
	[CreditUnionNbr] [varchar](4) NULL,
	[TipNumber] [varchar](15) NULL,
	[Points] [bigint] NULL,
	[RNTranCode] [varchar](2) NULL,
	[AcctNbr] [varchar](15) NULL,
	[Misc3] [varchar](7) NULL,
	[MerchantSICCode] [varchar](4) NULL,
 CONSTRAINT [PK_impTransaction] PRIMARY KEY CLUSTERED 
(
	[sid_impTransaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[impTransaction] ADD  CONSTRAINT [DF_impTransaction_TransactionAmt]  DEFAULT ((0.00)) FOR [TransactionAmt]
GO
