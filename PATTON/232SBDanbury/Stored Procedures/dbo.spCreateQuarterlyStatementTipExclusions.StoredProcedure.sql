USE [232SBDanbury]
GO

/****** Object:  StoredProcedure [dbo].[spCreateQuarterlyStatementTipExclusions]    Script Date: 08/15/2012 14:28:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateQuarterlyStatementTipExclusions]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[spCreateQuarterlyStatementTipExclusions]
GO
--
/****** Object:  StoredProcedure [dbo].[spCreateQuarterlyStatementTipExclusions]    Script Date: 08/15/2012 14:28:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[spCreateQuarterlyStatementTipExclusions]

As


create table #ExcludeZeroBalances
	(TipFirst			varchar(3) primary key)
	
create table #ExcludeNoActivity
	(TipFirst			varchar(3) primary key)


truncate table dbo.quarterly_statement_exclusions

insert into #ExcludeZeroBalances
select distinct tipfirst
from dbo.statementrun
where excludezerobalances = 'Y'

--SEB 5/2014 New Points to expire logic
EXEC RewardsNow.dbo.usp_ExpirePoints '232', 1

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select distinct qsfa.tipnumber
from dbo.Quarterly_Statement_File qsfa join #excludezerobalances tmp
	on left(qsfa.tipnumber,3) = tmp.tipfirst
left outer join rewardsnow.dbo.RNIExpirationProjection xp --SEB 5/2014
    on qsfa.tipnumber = xp.tipnumber
where pointsbegin = 0 and pointsend = 0 and isnull(xp.points_expiring, 0) = 0 --SEB 5/2014



insert into #ExcludeNoActivity
select distinct tipfirst
from dbo.statementrun
where excludenoactivity = 'Y'

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select distinct qsfa.tipnumber
from dbo.Quarterly_Statement_File qsfa join #ExcludeNoActivity tmp
	on left(qsfa.tipnumber,3) = tmp.tipfirst

left outer join dbo.quarterly_statement_exclusions qse
	on qsfa.tipnumber = qse.tipnumber

left outer join rewardsnow.dbo.RNIExpirationProjection xp --SEB 5/2014
    on qsfa.tipnumber = xp.tipnumber

where qse.tipnumber is null
and qsfa.PointsIncreased = 0 and qsfa.PointsDecreased = 0 and qsfa.pointsexpire = 0 and isnull(xp.points_expiring, 0) = 0 --SEB 5/2014

---
-- Add in exclusions for E-Statements
---

insert into dbo.quarterly_statement_exclusions
(tipnumber)
select sid_estmttips_tipnumber
from dbo.EstmtTips et left outer join dbo.quarterly_statement_exclusions qse
    on et.sid_estmttips_tipnumber = qse.tipnumber
where qse.tipnumber is null




GO


