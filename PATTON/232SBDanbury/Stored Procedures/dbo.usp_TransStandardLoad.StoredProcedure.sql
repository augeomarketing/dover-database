USE [232SBDanbury]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TransStandardLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TransStandardLoad]
GO

CREATE PROCEDURE [dbo].[usp_TransStandardLoad] @DateAdded datetime, @BillPayPointMultiplier int
AS

-- Clear TransStandard 
Truncate table dbo.TransStandard 


-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	(TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt  ) 

select dim_impcustomer_tipnumber, @DateAdded, dim_impcustomer_acctnum, '67', 1, dim_impcustomer_points,
    tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.trantype Tt 
    on '67' = tt.trancode
join dbo.customer_stage stg
	on imp.dim_impcustomer_tipnumber = stg.tipnumber    
where dim_impcustomer_pointssign = '+' and dim_impcustomer_points != 0 and stg.status != 'X'

union all

select dim_impcustomer_tipnumber, @DateAdded, dim_impcustomer_acctnum, '37', 1, dim_impcustomer_points ,
    tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.trantype Tt 
    on '37' = tt.trancode
join dbo.customer_stage stg
	on imp.dim_impcustomer_tipnumber = stg.tipnumber    
where dim_impcustomer_pointssign = '-' and dim_impcustomer_points != 0 and stg.status != 'X'




--Now add in the bill pay

insert into dbo.transstandard
	(TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt  ) 

select dim_impbillpay_tipnumber, @dateadded, null, 'FC', 1, (bp.dim_impbillpay_onlinebillspaid * @BillPayPointMultiplier) ,
	   tt.description, tt.ratio, @dateadded
from dbo.impBillPay bp join dbo.trantype tt 
    on 'FC' = tt.trancode
join dbo.customer_stage stg
	on bp.dim_impbillpay_tipnumber = stg.tipnumber
where bp.dim_impbillpay_tipnumber is not null
and bp.dim_impbillpay_onlinebillspaid != 0
and stg.status != 'X'


--
-- Now add in bonuses
--

-- Double Points Bonus

Insert dbo.TransStandard 
(TIPNumber, TranDate, AcctID, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt  ) 

select dim_impcustomer_tipnumber, @DateAdded, dim_impcustomer_acctnum, bt.dim_bonustype_trancode, 
		1, dim_impcustomer_points * bp.dim_bonusprogram_pointmultiplier,
    tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.trantype Tt 
    on 'BI' = tt.trancode

join dbo.bonustype bt
	on bt.dim_bonustype_trancode = tt.trancode
	
join dbo.bonusprogram bp
	on bt.sid_bonustype_id = bp.sid_bonustype_id
	and @DateAdded between dim_bonusprogram_effectivedate and dim_bonusprogram_expirationdate
where dim_impcustomer_pointssign = '+'
and (dim_impcustomer_points * bp.dim_bonusprogram_pointmultiplier) <> 0

/*

declare @dateadded datetime = '08/31/2010'

select dim_impcustomer_tipnumber, @DateAdded, dim_impcustomer_acctnum, bt.dim_bonustype_trancode, 
		1, dim_impcustomer_points * bp.dim_bonusprogram_pointmultiplier,
    tt.Description, tt.ratio, @DateAdded
from dbo.impcustomer imp join dbo.trantype Tt 
    on 'BI' = tt.trancode

join dbo.bonustype bt
	on bt.dim_bonustype_trancode = tt.trancode
	
join dbo.bonusprogram bp
	on bt.sid_bonustype_id = bp.sid_bonustype_id
	and @DateAdded between dim_bonusprogram_effectivedate and dim_bonusprogram_expirationdate
where dim_impcustomer_pointssign = '+'
and (dim_impcustomer_points * bp.dim_bonusprogram_pointmultiplier) <> 0
order by dim_impcustomer_tipnumber



select dim_impcustomer_tipnumber, dim_impcustomer_points
from dbo.impcustomer
order by dim_impcustomer_tipnumber


*/
GO


