USE [232SBDanbury]
GO
/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 12/03/2012 13:39:55 ******/
DROP PROCEDURE [dbo].[usp_WebCustomerAccountLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_WebCustomerAccountLoad]

AS

delete from dbo.web_account
delete from dbo.web_customer


insert into dbo.web_customer
	(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
	 EarnedBalance, Redeemed, AvailableBal, Status, city, state)
select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
		address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
from dbo.customer	
where status in ( 'A', 'S')




insert into dbo.web_account (TipNumber, LastName, LastSix, SSNLast4)
select aff.tipnumber, isnull(aff.lastname, 'NotAssigned') , right(acctid,6), null
from dbo.affiliat aff join dbo.customer cus
	on aff.tipnumber = cus.tipnumber
where cus.status in( 'A', 'S')
GO
