USE [232SBDanbury]
GO
/****** Object:  StoredProcedure [dbo].[usp_AssignTipNumbers]    Script Date: 12/03/2012 13:39:54 ******/
DROP PROCEDURE [dbo].[usp_AssignTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_AssignTipNumbers]
	   @TipFirst		    varchar(3)

As
 -- SEB 05/2014 Added tipnumber check to a couple of the update statements

declare @lasttipused		  varchar(15)

declare @TipInt			  int
--declare @tipfirst			  varchar(3) = '232'

declare @dda				  varchar(10)



set nocount on

update dbo.impcustomer set dim_impcustomer_tipnumber = null


--
-- Check to see if customer already is in system.....
--


--
-- Update impCustomer with Tip#s from affiliat with matching card #s
update imp
    set dim_impcustomer_tipnumber = aff.tipnumber
from dbo.impCustomer imp join dbo.affiliat aff
    on imp.dim_impcustomer_acctNum = aff.acctid

--
-- Update impCustomer with Tip#s from affiliat with matching old card #s.  This will pick up the new card that is
-- a replacement
update imp
    set dim_impcustomer_tipnumber = aff.tipnumber
from dbo.impCustomer imp join dbo.affiliat aff
    on imp.dim_impcustomer_oldcc = aff.acctid
where imp.dim_impcustomer_tipnumber is null

--
-- Update impCustomer with tip#s from affiliat with matching DDA#s
update imp
    set dim_impcustomer_tipnumber = aff.tipnumber
from dbo.impCustomer imp join dbo.affiliat aff
    on imp.dim_impcustomer_dda = aff.custid
where dim_impcustomer_dda != '0000000000'
and dim_impcustomer_tipnumber is null


--
-- Update impCustomer with tip#s from affiliat with matching SSN
update imp
    set dim_impcustomer_tipnumber = cus.tipnumber
from dbo.impCustomer imp join dbo.customer_stage cus
    on imp.dim_impcustomer_ssn = cus.misc1
where dim_impcustomer_ssn != '000000000'
and dim_impcustomer_tipnumber is null


------------------------------------------------------------------------------------------
--
-- Now add in new cards
------------------------------------------------------------------------------------------

--
-- get last tip# used
exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirst, @lasttipused OUTPUT
set @TipInt = cast(  right(@lasttipused, 12) as int)


--
-- Drop temp tables should they exist (should only exist during testing/dev)
if object_id('tempdb..#dda') is not null
    drop table #dda

if object_id('tempdb..#dda2') is not null
    drop table #dda2

if object_id('tempdb..#dda3') is not null
    drop table #dda3

if object_id('tempdb..#ssn') is not null
    drop table #ssn

-- Create temp tables
create table #dda
    (dim_impcustomer_dda		  varchar(10) primary key,
     processed				  varchar(1))


create table #dda2
    (dim_impcustomer_dda		  varchar(10) primary key)


create table #ssn
    (dim_impcustomer_ssn		  varchar(9) primary key)


--
-- Get list of distinct DDA#s
insert into #dda
(dim_impcustomer_dda)
select distinct dim_impcustomer_dda
from dbo.impcustomer
where dim_impcustomer_tipnumber is null
and dim_impcustomer_dda != '0000000000'
and dim_impcustomer_ssn != '000000000'
and dim_impcustomer_status = 'A'
order by dim_impcustomer_dda


--
-- Loop through each DDA#
set @dda = (select top 1 dim_impcustomer_dda from #dda where processed is null)
while @dda is not null
BEGIN

    --print @dda
    truncate table #ssn
    truncate table #dda2

    -- get all SSNs for the specific DDA
    insert into #ssn
    select distinct dim_impcustomer_ssn
    from dbo.impcustomer
    where dim_impcustomer_dda = @dda
    and dim_impcustomer_ssn != '000000000'
    and dim_impcustomer_status = 'A'


    -- Get list of DDAs that the above SSNs are also tied to
    insert into #dda2
    select distinct imp.dim_impcustomer_dda
    from dbo.impcustomer imp join dbo.#ssn ssn
	   on imp.dim_impcustomer_ssn = ssn.dim_impcustomer_ssn
    where imp.dim_impcustomer_dda != '0000000000'
    and dim_impcustomer_status = 'A'

    set @tipint += 1

    update imp
	   set dim_impcustomer_tipnumber = @tipfirst + right('000000000000' + cast(@tipint as varchar(12)), 12)
    from #dda2 d join dbo.impcustomer imp
	   on d.dim_impcustomer_dda = imp.dim_impcustomer_dda
    where dim_impcustomer_status = 'A'
	and dim_impcustomer_tipnumber is null --SEB05/2014


    update d
	   set processed = 'Y'
    from #dda2 d2 join #dda D 
	   on d2.dim_impcustomer_dda = d.dim_impcustomer_dda

    set @dda = (select top 1 dim_impcustomer_dda from #dda where processed is null)
END
    

--
-- now group DDA#s, with zeros for SSN and give them tip#s

create table #dda3
    (dim_impcustomer_dda		  varchar(10) primary key,
     tipint				  int)

insert into #dda3
(dim_impcustomer_dda)
select distinct dim_impcustomer_dda
from dbo.impcustomer
where dim_impcustomer_tipnumber is null
and dim_impcustomer_dda != '0000000000'
and dim_impcustomer_status = 'A'


update #dda3
    set @tipint = tipint = @tipint + 1

update imp
    set dim_impcustomer_tipnumber =  @tipfirst + right('000000000000' + cast(tipint as varchar(12)), 12)
from #dda3 d join dbo.impcustomer imp
    on d.dim_impcustomer_dda = imp.dim_impcustomer_dda
where dim_impcustomer_tipnumber is null
and dim_impcustomer_status = 'A'


--
-- match up non zeroes ssns, assign tips

update imp
    set dim_impcustomer_tipnumber = ssn.dim_impcustomer_tipnumber
from dbo.impcustomer imp join (
						  select dim_impcustomer_ssn, dim_impcustomer_tipnumber
						  from dbo.impcustomer
						  where dim_impcustomer_tipnumber is not null and dim_impcustomer_ssn != '000000000') ssn
    on imp.dim_impcustomer_ssn = ssn.dim_impcustomer_ssn
where imp.dim_impcustomer_tipnumber is null
and imp.dim_impcustomer_ssn != '000000000'
and dim_impcustomer_status = 'A'



--
-- For all others, assign new tip numbers

create table #allnew
    (sid_impcustomer_id	   bigint primary key,
     tipint			   int)

insert into #allnew
(sid_impcustomer_id)
select sid_impcustomer_id
from dbo.impcustomer
where dim_impcustomer_tipnumber is null
and dim_impcustomer_status = 'A'


update #allnew
    set @tipint = tipint = @tipint + 1


update imp
    set dim_impcustomer_tipnumber =  @tipfirst + right('000000000000' + cast(tipint as varchar(12)), 12)
from dbo.impcustomer imp join #allnew tmp
    on imp.sid_impcustomer_id = tmp.sid_impcustomer_id
where dim_impcustomer_status = 'A'
and dim_impcustomer_tipnumber is null --SEB 05/2014


--
-- Now update last tip number used

set @lasttipused = @tipfirst + right('000000000000' + cast(@tipint as varchar(12)), 12)
EXEC RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @lasttipused



update bp
    set dim_impBillPay_tipnumber = imp.dim_impcustomer_tipnumber
from dbo.impBillPay bp join dbo.impCustomer imp
    on bp.dim_impBillPay_DDA = imp.dim_impCustomer_DDA
where bp.dim_impBillPay_tipnumber is null


update bp
    set dim_impBillPay_tipnumber = imp.dim_impcustomer_tipnumber
from dbo.impBillPay bp join dbo.impCustomer imp
    on bp.dim_impBillPay_ssn =  imp.dim_impCustomer_ssn
where bp.dim_impBillPay_ssn is null






/*

exec dbo.spAssignTipNumbers '232'

update dbo.impcustomer set dim_impcustomer_tipnumber = null


select * from rewardsnow.dbo.dbprocessinfo where dbnumber = '232'

select * from dbo.client


select distinct dim_impcustomer_tipnumber
from dbo.impcustomer

select count(*) from dbo.impcustomer


*/
GO
