USE [232SBDanbury]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerStageLoad]    Script Date: 01/15/2013 15:03:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_CustomerStageLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_CustomerStageLoad]
GO

USE [232SBDanbury]
GO

/****** Object:  StoredProcedure [dbo].[usp_CustomerStageLoad]    Script Date: 01/15/2013 15:03:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_CustomerStageLoad] @EndDate DateTime AS

set nocount on

declare @tipnumber		varchar(15)

declare @sql			nvarchar(4000)
declare @tip			nvarchar(15)
declare @ctr			int = 0
declare @name			varchar(40) = ''



/* Update Existing Customers                                         */
Update cstg
	set	lastname		= ltrim(rtrim(imp.dim_impcustomer_lastname)),
		acctname1		= ltrim(rtrim(imp.dim_impcustomer_name1)),
		acctname2		= ltrim(rtrim(imp.dim_impcustomer_name2)),
		address1		= ltrim(rtrim(imp.dim_impcustomer_address1)),
		address2		= ltrim(rtrim(imp.dim_impcustomer_address2)),
		address4		= left(ltrim(rtrim(imp.dim_impcustomer_citystate)) + ' ' + ltrim(rtrim(imp.dim_impcustomer_zipcode)), 40),
		city			= case 
					   when charindex(',',dim_impcustomer_citystate, 1) != 0 then left(dim_impcustomer_citystate, charindex(',',dim_impcustomer_citystate, 1)-1)
					   else dim_impcustomer_citystate
					  end,
		state		= case
					   when charindex(',', dim_impcustomer_citystate,1) != 0 then reverse(ltrim(rtrim( left( reverse(dim_impcustomer_citystate), charindex( ' ,', reverse(dim_impcustomer_citystate), 1)))))
					   else ''
					  end,
		zipcode		= ltrim(rtrim(dim_impcustomer_zipcode)),
		homephone		= ltrim(rtrim(imp.dim_impcustomer_phone1)),
		workphone		= ltrim(rtrim(imp.dim_impcustomer_phone2)),
		misc1		= ltrim(rtrim(imp.dim_impcustomer_ssn)),
		misc2		= right(ltrim(rtrim(imp.dim_impcustomer_acctnum)), 6)
		--,	status		= 'A'  -- JIRA: SBDANBURY17

from dbo.Customer_Stage cstg join dbo.impCustomer imp
    on	cstg.tipnumber = imp.dim_impcustomer_tipnumber
where dim_impcustomer_status = 'A'


--
-- JIRA: SBDANBURY-17
update stg
	set status = case
					when (sec.emailstatement = 'Y' and sec.username is not null) then 'A'
					else 'X'
				 END
from dbo.customer_stage stg left outer join rn1.sbdanbury.dbo.[1security] sec
	on stg.tipnumber = sec.tipnumber
where stg.dateadded >= '11/01/2012'


/* Add New Customers                                                      */

declare csrNewCustomers cursor FAST_FORWARD for
	select distinct imp.dim_impcustomer_tipnumber
	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.dim_impcustomer_tipnumber = stg.tipnumber
	where stg.tipnumber is null
     and dim_impcustomer_status = 'A'

open csrNewCustomers

fetch next from csrNewCustomers into @tipnumber

while @@FETCH_STATUS = 0
BEGIN

--
-- JIRA: SBDANBURY-17
-- New customers get loaded with a STATUS of "X".
-- They are not technically enrolled until they register online.
-- They will get a welcome kit however
--
	Insert into Customer_Stage
		(tipnumber, tipfirst, tiplast, lastname,
		 acctname1, acctname2, address1, address2, address4,
		 city, 
		 state, 
		  zipcode, homephone, workphone,
		 misc1, misc2, status, dateadded, 
		 runavailable, runbalance, runredeemed, runavaliableNew)

	select top 1 imp.dim_impcustomer_TIPNUMBER tipnumber, left(imp.dim_impcustomer_TIPNUMBER,3) tipfirst, right(ltrim(rtrim(imp.dim_impcustomer_TIPNUMBER)),12) tiplast, left(ltrim(rtrim(imp.dim_impcustomer_lastname)),40) lastname,
		  ltrim(rtrim(imp.dim_impcustomer_Name1)) acctname1, ltrim(rtrim(imp.dim_impcustomer_Name2)) acctname2,
		  dim_impcustomer_address1, dim_impcustomer_address2,
		  left(ltrim(rtrim(imp.dim_impcustomer_citystate)) + ' ' + ltrim(rtrim(imp.dim_impcustomer_zipcode)), 40),

		  case 
			  when charindex(',',dim_impcustomer_citystate, 1) != 0 then left(dim_impcustomer_citystate, charindex(',',dim_impcustomer_citystate, 1)-1)
			  else dim_impcustomer_citystate
		  end,
		  case
			   when charindex(',', dim_impcustomer_citystate,1) != 0 then reverse(ltrim(rtrim( left( reverse(dim_impcustomer_citystate), charindex( ' ,', reverse(dim_impcustomer_citystate), 1)))))
			   else ''
		  end,

		  ltrim(rtrim(imp.dim_impcustomer_zipcode)), ltrim(rtrim(imp.dim_impcustomer_phone1)), ltrim(rtrim(imp.dim_impcustomer_phone2)), ltrim(rtrim(imp.dim_impcustomer_ssn)),
		  right(ltrim(rtrim(imp.dim_impcustomer_acctnum)), 6), 'X' acctstatus, @EndDate, 0, 0, 0, 0

	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.dim_impcustomer_tipnumber = stg.tipnumber
		
	join (select dda from dbo.PriorityAccount group by dda) pa  -- JIRA: SBDANBURY-17
	    on imp.dim_impcustomer_dda = pa.dda

	where stg.tipnumber is null and imp.dim_impcustomer_tipnumber = @tipnumber 

	fetch next from csrNewCustomers into @tipnumber
END

close csrNewCustomers

deallocate csrNewCustomers

--**********************************************************************************************
--**
--** Now household names
--**
--**********************************************************************************************


create table #names
	(tipnumber			varchar(15) primary key,
	 Name1				varchar(40),
	 Name2				varchar(40),
	 Name3				varchar(40),
	 Name4				varchar(40),
	 Name5				varchar(40),
	 Name6				varchar(40))

create table #tipnames
	(Name				varchar(40))

create table #tipnames2
	(Name				varchar(40))


declare csr cursor fast_forward for
	select distinct dim_impcustomer_tipnumber
	from dbo.impCustomer

open csr

fetch next from csr into @tip

while @@FETCH_STATUS = 0
BEGIN

	truncate table #tipnames
	truncate table #tipnames2
	
	--
	-- Get all the distinct names for the tip
	insert into #tipnames
	select distinct dim_impcustomer_name1
	from dbo.impcustomer
	where dim_impcustomer_tipnumber = @tip
	
	insert into #tipnames
	select distinct dim_impcustomer_name2
	from dbo.impcustomer
	where dim_impcustomer_tipnumber = @tip

	-- Get distinct names into second work table
	insert into #tipnames2
	select distinct name
	from #tipnames
	
	
	insert into #names 
	(tipnumber)
	values(@tip)
	
	set @ctr = 0
	while @ctr <= 6 
	BEGIN
		set @ctr = @ctr + 1
		
		select @name = name
		from #tipnames2
		

		if @ctr = 1  update #names set name1 = @name where tipnumber = @tip
		if @ctr = 2  update #names set name2 = @name where tipnumber = @tip
		if @ctr = 3  update #names set name3 = @name where tipnumber = @tip
		if @ctr = 4  update #names set name4 = @name where tipnumber = @tip
		if @ctr = 5  update #names set name5 = @name where tipnumber = @tip
		if @ctr = 6  update #names set name6 = @name where tipnumber = @tip
		
		delete from #tipnames2 where name = @name
	END
	
	fetch next from csr into @tip
END

close csr
deallocate  csr


update stg
	set acctname1 = name1,
		acctname2 = name2,
		acctname3 = name3,
		acctname4 = name4,
		acctname5 = name5,
		acctname6 = name6
from #names tmp join dbo.customer_stage stg
	on tmp.tipnumber = stg.tipnumber



--**********************************************************************************************
-- End of Householding
--**********************************************************************************************



/* set Default status to A */
--Update dbo.Customer_Stage
--	Set STATUS = 'X' 
--Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status

GO


