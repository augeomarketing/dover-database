USE [232SBDanbury]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImportStageToProd]    Script Date: 12/03/2012 13:39:54 ******/
DROP PROCEDURE [dbo].[usp_ImportStageToProd]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ImportStageToProd] @TipFirst varchar(3)
AS 

Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
Update c
	Set 
	Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
	Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
	Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
	Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
	City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
	Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
	Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
	Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
From dbo.Customer_Stage S Join dbo.Customer C 
On S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update dbo.Customer_Stage 	
	Set RunAvailable = 0 

--	Insert New Customers from Customers_Stage
Insert into Customer 
	select stg.*
	from Customer_Stage stg left outer join dbo.Customer cus
	on stg.tipnumber = cus.tipnumber
	where cus.Tipnumber is null

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
Update C 
	set RunBalance = C.RunBalance + S.RunAvaliableNew,
	      RunAvailable = C.RunAvailable + S.RunAvaliableNew
From Customer_Stage S Join Customer C 
	On S.Tipnumber = C.Tipnumber



----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
Update a
	set YTDEarned = S.YTDEarned,
	    custid = s.custid 
from dbo.Affiliat_Stage S join dbo.Affiliat A 
	on S.acctid = A.acctid

--	Insert New Affiliat accounts
Insert into dbo.Affiliat 
	select stg.* 
	from dbo.Affiliat_Stage stg left outer join dbo.Affiliat aff
		on stg.acctid = aff.acctid
	where aff.Tipnumber is null



----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
Insert Into dbo.History 
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
from dbo.History_Stage 
where SecID = 'NEW'


----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
insert into dbo.OneTimeBonuses 
select stg.* 
from dbo.OneTimeBonuses_stage stg left outer join dbo.OneTimeBonuses otb
	on stg.TipNumber = otb.tipnumber
	and stg.trancode = otb.trancode
where otb.tipnumber is null

-- Truncate Stage Tables so's we don't double post.
delete from dbo.transstandard
delete from dbo.OneTimeBonuses_stage
delete from dbo.History_Stage
delete from dbo.Affiliat_Stage
delete from dbo.Customer_Stage
GO
