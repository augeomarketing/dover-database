use [232SBDanbury]
GO

if object_id('dbo.usp_BuildTipDDAPivot') is not null
	drop procedure dbo.usp_BuildTipDDAPivot
GO


create procedure dbo.usp_BuildTipDDAPivot

As


declare @sql		    nvarchar(max) = ''
declare @columnnames    varchar(max) = ''
declare @sql_tmptable	nvarchar(max) = 'if object_id(''dbo.zztipdda'') is not null drop table dbo.zztipdda; CREATE TABLE dbo.zztipdda (dim_zztipdda_tipnumber varchar(15),  '

declare @ctr		    int = 1
declare @maxDDAsontip  int

set nocount on

-- Get max # cards against a tip#

select top 1 @maxddasontip = count(distinct custid)
from dbo.affiliat
group by tipnumber
order by count(distinct custid) desc


if @maxDDAsontip is not null
BEGIN

    -- Build string of column names for the pivot for clause
    -- string will look something like:
    -- '[Card_1], [Card_2],....'
    --
    while @ctr <= @maxDDAsontip
    BEGIN
	   set @columnnames = @columnnames + '[DDA_' + cast(@ctr as varchar) + ']' + ', '
	   set @sql_tmptable = @sql_tmptable + '[DDA_' + cast(@ctr as varchar) + '] varchar(20), '
	   set @ctr += 1
    END

    -- Now get rid of trailing ', ' from the end of the string
    set @columnnames = left(@columnnames, datalength(@columnnames)-2)
	set @sql_tmptable = left(@sql_tmptable, len(@sql_tmptable)-1) + ')'
	

 --   print @columnnames
	--print @sql_tmptable

	exec sp_executesql @sql_tmptable
	
    -- Now do the select, pivoting the results
    set @sql = 'insert into zztipdda
				select *
				from (select tipnumber, custid, ''DDA_'' + cast(row_number()  over(partition by tipnumber order by tipnumber) as varchar) rownbr 
				  from dbo.affiliat
				  group by tipnumber, custid) aff
				pivot
				( max(custid)
				for rownbr in (<columnnames>)
				) as pvttable'

    -- Now replace the '<columnnames>' with the @columnnames string built in the while loop
    set @sql = replace(@sql, '<columnnames>', @columnnames)

    --print @sql
	exec sp_executesql @sql

END


/* Test harness

exec dbo.usp_BuildTipDDAPivot

select * from dbo.zztipdda

*/