use [232SBDanbury]
GO

if object_id('dbo.usp_BuildCustIDPivotTable') is not null
	drop procedure dbo.usp_BuildCustIDPivotTable
GO


create procedure dbo.usp_BuildCustIDPivotTable
AS

-- Clear work table
delete from dbo.wrkPivotCustID

insert into dbo.wrkPivotCustID
(tipnumber, cid_1, cid_2, cid_3, cid_4, cid_5)
select *  from (select tipnumber, custid, 'Card_' + cast(row_number()  over(partition by tipnumber order by tipnumber) as varchar) rownbr 
				  from dbo.affiliat_stage
				  group by tipnumber, custid) aff
			 pivot
			 ( max(custid)
				for rownbr in ([Card_1], [Card_2], [Card_3], [Card_4], [Card_5])
			 ) as pvttable
			 
			 
