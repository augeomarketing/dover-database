USE [232SBDanbury]
GO

/****** Object:  StoredProcedure [dbo].[usp_PurgeClosedCustomers]    Script Date: 05/16/2013 13:40:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_PurgeClosedCustomers]  
	@Production_Flag	char(1), 
	@DateDeleted		datetime,
	@TipFirst			varchar(3) = null

AS

Declare @SQLDynamic nvarchar(4000)
declare @SQL		nvarchar(4000)
Declare @DBName	varchar(50)

Declare @Tipnumber 	char(15)


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin

	truncate table dbo.impcustomer_Purge

    if object_id('tempdb..#oot') is not null
	   drop table #oot

    if object_id('tempdb..#optoutcards') is not null
	   drop table #optoutcards

    if object_id('tempdb..#all') is not null
	   drop table #all

    if object_id('tempdb..#closed') is not null
	   drop table #closed

    if object_id('tempdb..#purge_c_z') is not null
	   drop table #purge_c_z


    -- Get customers from pendingpurge that couldn't be purged last month.
    -- Put them back into the current purge process
	insert into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1
	from dbo.impCustomer_Purge_Pending ipp 
	
    -- Empty the pending table
	truncate table dbo.impCustomer_Purge_Pending
	


    --
    -- close cards in the affiliat_stage table with cards marked as closed (status = b)
    update aff
        set AcctStatus = 'C'
    from dbo.impcustomer imp join dbo.affiliat_stage aff
        on imp.dim_impcustomer_acctnum = aff.acctid
    where imp.dim_impcustomer_status = 'B'


    -- Now remove the deleted cards from the working input table
    delete from dbo.impcustomer
    where dim_impcustomer_Status = 'B'  -- remove closed accounts


    ---- Get rid of bill pay for customers not active
    --delete bp
    --from dbo.impBillPay bp left outer join (select dim_impcustomer_dda from dbo.impcustomer where dmi_impcustomer_status = 'A') imp
    --    on bp.dim_impbillpay_dda = imp.dim_impcustomer_dda
    --where imp.dim_impcustomer_dda is null


	
-- OPT OUT PROCESS....	
	-- create temp table of tip# and lastname
	create table #oot
		(tipnumber		varchar(15) primary key,
		 lastname			varchar(50))

	insert into #oot
	select distinct tipnumber, oot.lastname
	from rewardsnow.dbo.optouttracking oot
	where left(tipnumber,3) = @tipfirst


	-- create temp table of card numbers in opt out table
	create table #optoutcards
		(acctid		varchar(25))
		
	insert into #optoutcards
	select distinct acctid
	from rewardsnow.dbo.optouttracking oot
     where left(tipnumber,3) = @tipfirst


	-- Add in the replacement cards
	insert into rewardsnow.dbo.optouttracking
	(TipPrefix, TIPNUMBER, ACCTID, FIRSTNAME, LASTNAME, OPTOUTDATE, OPTOUTSOURCE, OPTOUTPOSTED)

	select left(imp.dim_impcustomer_tipnumber,3) tipprefix, imp.dim_impcustomer_tipnumber, dim_impcustomer_acctnum, 
			 dim_impcustomer_name1, oot.lastname, @DateDeleted optoutdate, 'PROCESSING' optoutsource, @DateDeleted
	from dbo.impcustomer imp join #oot oot
		on imp.dim_impcustomer_tipnumber = oot.tipnumber

	left outer join #optoutcards ooc
		on imp.dim_impcustomer_Acctnum = ooc.acctid
	where ooc.acctid is null

	
	-- Get all the opt outs
	Insert Into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	Select TipNumber, AcctId, FirstName
	from rewardsnow.dbo.OptOutTracking oot 
     where left(oot.tipnumber,3) = @tipfirst and
		  oot.tipnumber not in (select distinct tipnumber 
							from dbo.history 
							where histdate > @DateDeleted and trancode not in ('RQ'))
	
	
	-- Get count of all card#s associated to a tip using AFFILIAT_STAGE
	select tipnumber, count(*) cardcount
	into #all
	from dbo.affiliat_stage
	group by tipnumber


	-- Get count of all cards associated to a tip, that have a status C in at least one of their rows
	select tipnumber, count(*) cardcount
	into #closed
	from dbo.affiliat_stage
	where acctstatus in ('C')
	group by tipnumber

	select distinct a.tipnumber
	into #purge_c_z
	from #all a join #closed c
		on a.tipnumber = c.tipnumber
	where a.cardcount = c.cardcount


	-- For tips that have ALL the cards marked as closed, add them to the purge.
	insert into dbo.impcustomer_purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select aff.tipnumber, aff.acctid, aff.lastname
	from #purge_c_z cz join dbo.affiliat_stage aff
		on cz.tipnumber = aff.tipnumber
	left outer join dbo.impcustomer_purge pg
		on aff.acctid = pg.dim_impcustomer_acctnum
	where pg.dim_impcustomer_acctnum is null
	and aff.tipnumber not in (select imp.dim_impcustomer_tipnumber 
							  from dbo.impcustomer imp
							  where dim_impcustomer_status = 'A')


	--
	-- For accounts with activity after the monthend date, put them into purge_pending
	-- then remove them from purge
	--
	insert into dbo.impcustomer_purge_pending
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select distinct pg.dim_impcustomer_tipnumber, pg.dim_impcustomer_acctnum, pg.dim_impcustomer_name1
	from dbo.impcustomer_purge pg
	where pg.dim_impcustomer_tipnumber in (select tipnumber from dbo.history_stage his 
					   where histdate > @DateDeleted
					   and trancode != 'RQ')

	delete pg
	from dbo.impcustomer_purge pg join dbo.impcustomer_purge_pending pp
		on pg.dim_impcustomer_tipnumber = pp.dim_impcustomer_tipnumber


	--
	-- If an account converted to priorityplus which was previously deleted and recorded in the opt-out table, now remove them from opt-out.
	--
	delete oot
	from priorityaccount pa join rewardsnow.dbo.optouttracking oot
		on pa.pan = oot.acctid
	where tipprefix = @TipFirst and optoutsource = 'FI Business Rules'



	--
	-- Begin removing purged tips from import tables and staging tables
	--
	if object_id('tempdb..#optoutaccts') is not null drop table #optoutaccts

	create table #optoutaccts
	(acctid			varchar(16) primary key,
	 dda			varchar(20),
	 tipnumber		varchar(15))
	 
	 insert into #optoutaccts (acctid)
	 select distinct acctid
	 from rewardsnow.dbo.optouttracking
	 where tipnumber like '232%'

	update tmp
		set dda = dim_impcustomer_dda,
			tipnumber = dim_impcustomer_tipnumber
	from #optoutaccts tmp join dbo.impcustomer imp
		on tmp.acctid = imp.dim_impcustomer_acctNum

	
	delete cus
	from #optoutaccts tmp join dbo.impcustomer cus
		on tmp.tipnumber = cus.dim_impcustomer_tipnumber
	
	delete cus
	from #optoutaccts tmp join dbo.impcustomer cus
		on tmp.dda = cus.dim_impcustomer_dda

	delete bp
	from dbo.impBillPay bp join #optoutaccts tmp
		on bp.dim_impbillpay_tipnumber = tmp.tipnumber
	

	delete cus
	from dbo.impCustomer cus join dbo.impCustomer_Purge prg
		on cus.dim_impcustomer_tipnumber = prg.dim_impcustomer_tipnumber

	delete bp
	from dbo.impBillPay bp join dbo.impcustomer_purge prg
		on bp.dim_impBillPay_TipNumber = prg.dim_impcustomer_tipnumber

	delete his
	from dbo.history_stage his join dbo.impCustomer_Purge dlt
		on his.tipnumber = dlt.dim_impcustomer_tipnumber


	delete aff
	from dbo.affiliat_stage aff join dbo.impCustomer_Purge dlt
		on aff.tipnumber = dlt.dim_impcustomer_tipnumber


	delete cus
	from dbo.customer_stage cus join dbo.impCustomer_Purge dlt
		on cus.tipnumber = dlt.dim_impcustomer_tipnumber



End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin


	set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

	-- copy any impCustomer_Purge_pending into impCustomer_Purge
	 
	Insert into dbo.impCustomer_Purge
	(dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1)
	select dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_name1
	from dbo.impCustomer_Purge_Pending
	where left(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Clear impCustomer_Purge_Pending 
	delete from dbo.impCustomer_Purge_Pending where left(dim_impcustomer_tipnumber,3) = @TipFirst

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(dim_impcustomer_TipNumber, dim_impcustomer_acctNum, dim_impcustomer_Name1)
	select distinct imp.dim_impcustomer_TipNumber, dim_impcustomer_acctnum, dim_impcustomer_name1
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.dim_impcustomer_tipnumber = his.tipnumber
			and	@datedeleted > his.histdate 
			and	'RQ' != his.trancode
		where left(imp.dim_impcustomer_tipnumber,3) = @TipFirst


	if exists(select 1 from dbo.sysobjects where name = 'wrkPurgeTips' and xtype = 'U')
		drop table dbo.wrkPurgeTips
	
	create table dbo.wrkPurgeTips (TipNumber varchar(15) primary key)

	insert into dbo.wrkPurgeTips
	select distinct dim_impcustomer_tipnumber from dbo.impcustomer_purge


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	Delete imp
		from dbo.wrkPurgeTips imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @DateDeleted
			and	his.trancode != 'RQ'
		where left(imp.tipnumber,3) = @TipFirst


	-- Insert customer to customerdeleted
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			c.LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, c.City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from dbo.Customer c join dbo.wrkPurgeTips prg 
	   on c.tipnumber = prg.tipnumber
	where left(c.tipnumber,3) = @tipfirst 


	-- Insert affiliat to affiliatdeleted 
	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, aff.LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
	from dbo.Affiliat aff join dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber
	where left(aff.tipnumber,3) = @tipfirst 


	-- Insert into optouttracking
	insert into rewardsnow.dbo.optouttracking
	(TipPrefix, TIPNUMBER, ACCTID, FIRSTNAME, LASTNAME, OPTOUTDATE, OPTOUTSOURCE, OPTOUTPOSTED)
	select @tipfirst, aff.tipnumber, aff.acctid, cd.acctname1, cd.acctname1, cd.datedeleted, 'FI Business Rules', cd.datedeleted
	from dbo.affiliatdeleted aff join dbo.customerdeleted cd
		on aff.tipnumber = cd.tipnumber
	join dbo.wrkpurgetips prg
		on aff.tipnumber = prg.tipnumber
	where cd.datedeleted = @datedeleted


	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from dbo.History H join dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber
	where left(h.tipnumber,3) = @tipfirst 

	-- Delete records from History 
	Delete h
	from dbo.History h join dbo.wrkPurgeTips prg
		on h.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join dbo.wrkPurgeTips prg
		on aff.tipnumber = prg.tipnumber

	-- Delete from customer 
	Delete c
	from dbo.Customer c join dbo.wrkPurgeTips prg
		on c.tipnumber = prg.tipnumber

	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = 'C'
	from dbo.customer c join dbo.impCustomer_Purge_Pending prg
		on c.tipnumber = prg.dim_impcustomer_tipnumber


End




GO


