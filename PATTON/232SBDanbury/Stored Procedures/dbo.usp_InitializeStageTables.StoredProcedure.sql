USE [232SBDanbury]
GO
/****** Object:  StoredProcedure [dbo].[usp_InitializeStageTables]    Script Date: 12/03/2012 13:39:54 ******/
DROP PROCEDURE [dbo].[usp_InitializeStageTables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_InitializeStageTables]
    @StartDate		datetime

AS

delete from dbo.transstandard
delete from dbo.history_stage
delete from dbo.onetimebonuses_stage
delete from dbo.affiliat_stage
delete from dbo.customer_stage


insert into dbo.customer_stage
(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
select TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
from dbo.customer


insert into dbo.affiliat_stage
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
select ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
from dbo.affiliat

insert into onetimebonuses_stage
(TipNumber, Trancode, AcctID, DateAwarded)
select TipNumber, Trancode, AcctID, DateAwarded
from dbo.onetimebonuses

insert into dbo.history_stage
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, 'OLD', Ratio, Overage
from dbo.history
where histdate >= @startdate
GO
