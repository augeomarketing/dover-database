USE [232SBDanbury]
GO
/****** Object:  StoredProcedure [dbo].[usp_AffiliatStageLoad]    Script Date: 12/03/2012 13:39:54 ******/
DROP PROCEDURE [dbo].[usp_AffiliatStageLoad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AffiliatStageLoad]   @MonthEnd datetime  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into dbo.Affiliat_Stage
    (AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
select imp.dim_impcustomer_acctnum, dim_impcustomer_tipnumber, act.accttype, @monthend, 'A',
	   act.AcctTypeDesc, imp.dim_impcustomer_lastname, 0, imp.dim_impcustomer_DDA
from dbo.customer_stage c join dbo.impcustomer imp
    on c.tipnumber = imp.dim_impcustomer_tipnumber

join dbo.AcctType act
    on act.accttype = 'Debit'

left outer join dbo.affiliat_stage aff
    on imp.dim_impcustomer_acctnum = aff.acctid
where aff.acctid is null
GO
