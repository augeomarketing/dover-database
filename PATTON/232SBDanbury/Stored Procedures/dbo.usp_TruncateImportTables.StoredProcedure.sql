USE [232SBDanbury]
GO

/****** Object:  StoredProcedure [dbo].[usp_TruncateImportTables]    Script Date: 01/15/2013 10:37:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TruncateImportTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TruncateImportTables]
GO

USE [232SBDanbury]
GO

/****** Object:  StoredProcedure [dbo].[usp_TruncateImportTables]    Script Date: 01/15/2013 10:37:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[usp_TruncateImportTables]

as

truncate table dbo.impBillPay
truncate table dbo.impBillPay_Error
truncate table dbo.impSBDBillPay
truncate table dbo.impSBDanbury
truncate table dbo.impcustomer

truncate table dbo.nameprs

truncate table dbo.PriorityAccount

GO


