USE [232SBDanbury]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__wrkPivotC__cid_1__627A95E8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkPivotCustID] DROP CONSTRAINT [DF__wrkPivotC__cid_1__627A95E8]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__wrkPivotC__cid_2__636EBA21]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkPivotCustID] DROP CONSTRAINT [DF__wrkPivotC__cid_2__636EBA21]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__wrkPivotC__cid_3__6462DE5A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkPivotCustID] DROP CONSTRAINT [DF__wrkPivotC__cid_3__6462DE5A]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__wrkPivotC__cid_4__65570293]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkPivotCustID] DROP CONSTRAINT [DF__wrkPivotC__cid_4__65570293]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__wrkPivotC__cid_5__664B26CC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[wrkPivotCustID] DROP CONSTRAINT [DF__wrkPivotC__cid_5__664B26CC]
END

GO

USE [232SBDanbury]
GO

/****** Object:  Table [dbo].[wrkPivotCustID]    Script Date: 01/08/2013 09:23:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkPivotCustID]') AND type in (N'U'))
DROP TABLE [dbo].[wrkPivotCustID]
GO

USE [232SBDanbury]
GO

/****** Object:  Table [dbo].[wrkPivotCustID]    Script Date: 01/08/2013 09:23:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[wrkPivotCustID](
	[tipnumber] [varchar](15) NOT NULL,
	[cid_1] [varchar](25) NULL,
	[cid_2] [varchar](25) NULL,
	[cid_3] [varchar](25) NULL,
	[cid_4] [varchar](25) NULL,
	[cid_5] [varchar](25) NULL,
	[Card_1] [varchar](16) NULL,
	[Card_2] [varchar](16) NULL,
	[Card_3] [varchar](16) NULL,
	[Card_4] [varchar](16) NULL,
	[Card_5] [varchar](16) NULL,
	[Card_6] [varchar](16) NULL,
	[Card_7] [varchar](16) NULL,
	[Card_8] [varchar](16) NULL,
	[Card_9] [varchar](16) NULL,
	[Card_10] [varchar](16) NULL,
PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[wrkPivotCustID] ADD  DEFAULT ('') FOR [cid_1]
GO

ALTER TABLE [dbo].[wrkPivotCustID] ADD  DEFAULT ('') FOR [cid_2]
GO

ALTER TABLE [dbo].[wrkPivotCustID] ADD  DEFAULT ('') FOR [cid_3]
GO

ALTER TABLE [dbo].[wrkPivotCustID] ADD  DEFAULT ('') FOR [cid_4]
GO

ALTER TABLE [dbo].[wrkPivotCustID] ADD  DEFAULT ('') FOR [cid_5]
GO

