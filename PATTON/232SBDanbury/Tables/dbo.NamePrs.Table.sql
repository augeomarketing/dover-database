USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[NamePrs]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[NamePrs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NamePrs](
	[PERSNBR] [varchar](25) NULL,
	[AcctNm] [varchar](50) NULL,
	[FIRSTNM] [varchar](50) NULL,
	[MIDDLENM] [varchar](50) NULL,
	[LASTNM] [varchar](50) NULL
) ON [PRIMARY]
GO
