USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[impcustomer_Purge_pending]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[impcustomer_Purge_pending]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impcustomer_Purge_pending](
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL
) ON [PRIMARY]
GO
