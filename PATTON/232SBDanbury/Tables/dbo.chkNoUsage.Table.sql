USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[chkNoUsage]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[chkNoUsage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chkNoUsage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[DATEADDED] [datetime] NOT NULL
) ON [PRIMARY]
GO
