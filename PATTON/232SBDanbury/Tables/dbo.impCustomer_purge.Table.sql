USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[impcustomer_Purge]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[impcustomer_Purge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impcustomer_Purge](
	[dim_impcustomer_tipnumber] [varchar](15) NULL,
	[dim_impcustomer_name1] [varchar](26) NULL,
	[dim_impcustomer_acctNum] [varchar](17) NULL
) ON [PRIMARY]
GO
