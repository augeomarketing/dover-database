USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[impBillPay]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[impBillPay]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impBillPay](
	[sid_impBillPay_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_impBillPay_DDA] [varchar](25) NULL,
	[dim_impBillPay_SSN] [varchar](9) NULL,
	[dim_impBillPay_OnlineBillsPaid] [int] NULL,
	[dim_impBillPay_TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_impBillPay] PRIMARY KEY CLUSTERED 
(
	[sid_impBillPay_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
