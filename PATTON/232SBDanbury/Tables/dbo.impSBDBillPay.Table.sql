USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[impSBDBillPay]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[impSBDBillPay]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impSBDBillPay](
	[DDANbr] [varchar](25) NULL,
	[SSN] [varchar](9) NULL,
	[FIRSTNAME] [varchar](255) NULL,
	[LASTNAME] [varchar](255) NULL,
	[OnlineBillsPaid] [int] NULL,
	[sid] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_impSBDBillPay] PRIMARY KEY CLUSTERED 
(
	[sid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
