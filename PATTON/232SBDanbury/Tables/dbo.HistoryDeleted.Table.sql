USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[HistoryDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[DateDeleted] [datetime] NULL,
	[sid_historydeleted_id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_HistoryDeleted] PRIMARY KEY CLUSTERED 
(
	[sid_historydeleted_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
