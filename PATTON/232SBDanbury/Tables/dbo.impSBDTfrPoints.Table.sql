USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[impSBDTfrPoints]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[impSBDTfrPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impSBDTfrPoints](
	[ActNbr] [varchar](25) NULL,
	[TAXID] [varchar](9) NULL,
	[FIRSTNAMEUPPER] [varchar](50) NULL,
	[LASTNAMEUPPER] [varchar](50) NULL,
	[points] [bigint] NULL
) ON [PRIMARY]
GO
