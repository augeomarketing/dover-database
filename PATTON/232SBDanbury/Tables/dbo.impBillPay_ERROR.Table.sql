USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[impBillPay_ERROR]    Script Date: 12/03/2012 13:39:59 ******/
ALTER TABLE [dbo].[impBillPay_ERROR] DROP CONSTRAINT [DF_impBillPay_ERROR_dim_impBillPay_OnlineBillsPaid]
GO
DROP TABLE [dbo].[impBillPay_ERROR]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[impBillPay_ERROR](
	[sid_impBillPay_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_impBillPay_DDA] [varchar](25) NULL,
	[dim_impBillPay_SSN] [varchar](9) NULL,
	[dim_impBillPay_OnlineBillsPaid] [int] NULL,
 CONSTRAINT [PK_impBillPay_ERROR] PRIMARY KEY CLUSTERED 
(
	[sid_impBillPay_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[impBillPay_ERROR] ADD  CONSTRAINT [DF_impBillPay_ERROR_dim_impBillPay_OnlineBillsPaid]  DEFAULT ((0)) FOR [dim_impBillPay_OnlineBillsPaid]
GO
