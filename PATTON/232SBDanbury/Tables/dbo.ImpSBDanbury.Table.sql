USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[ImpSBDanbury]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[ImpSBDanbury]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImpSBDanbury](
	[Client] [varchar](11) NULL,
	[AcctNum] [varchar](17) NULL,
	[SSN] [varchar](9) NULL,
	[Name1] [varchar](26) NULL,
	[Name2] [varchar](26) NULL,
	[Address1] [varchar](26) NULL,
	[Address2] [varchar](26) NULL,
	[CityState] [varchar](21) NULL,
	[ZipCode] [varchar](5) NULL,
	[PointsSign] [varchar](1) NULL,
	[Points] [varchar](6) NULL,
	[Phone1] [varchar](10) NULL,
	[Phone2] [varchar](10) NULL,
	[TranCode] [varchar](3) NULL,
	[ExternalStatus] [varchar](1) NULL,
	[NewChangeFlag] [varchar](1) NULL,
	[CodeName] [varchar](8) NULL,
	[PrimaryBirthDate] [varchar](6) NULL,
	[SecondaryBirthDate] [varchar](6) NULL,
	[MaintenanceFlag] [varchar](1) NULL,
	[EnrollmentSwitch] [varchar](1) NULL,
	[EnrollmentPoints] [varchar](6) NULL,
	[FullPrimaryAccountID] [varchar](19) NULL,
	[ExpirationDate] [varchar](8) NULL,
	[OriginalIssueDate] [varchar](8) NULL,
	[LastReIssueDate] [varchar](8) NULL,
	[LastUsedDate] [varchar](8) NULL,
	[FinancialInstitutionID] [varchar](6) NULL,
	[CardStatus] [varchar](1) NULL,
	[DDA] [varchar](10) NULL,
	[SavNum] [varchar](11) NULL,
	[Addr1Ext] [varchar](4) NULL,
	[Addr2Ext] [varchar](4) NULL,
	[TotalSpend] [varchar](9) NULL,
	[OldCC] [varchar](16) NULL,
	[NU036] [varchar](161) NULL,
	[NU037] [varchar](2) NULL
) ON [PRIMARY]
GO
