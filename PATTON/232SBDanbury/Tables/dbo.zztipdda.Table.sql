USE [232SBDanbury]
GO
/****** Object:  Table [dbo].[zztipdda]    Script Date: 12/03/2012 13:39:59 ******/
DROP TABLE [dbo].[zztipdda]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zztipdda](
	[dim_zztipdda_tipnumber] [varchar](15) NULL,
	[DDA_1] [varchar](20) NULL,
	[DDA_2] [varchar](20) NULL,
	[DDA_3] [varchar](20) NULL,
	[DDA_4] [varchar](20) NULL,
	[DDA_5] [varchar](20) NULL
) ON [PRIMARY]
GO
