USE [232SBDanbury]
GO

/****** Object:  View [dbo].[vw_wrkPivotCustID_ConcatIDs]    Script Date: 01/04/2013 09:39:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_wrkPivotCustID_ConcatIDs]'))
DROP VIEW [dbo].[vw_wrkPivotCustID_ConcatIDs]
GO

USE [232SBDanbury]
GO

/****** Object:  View [dbo].[vw_wrkPivotCustID_ConcatIDs]    Script Date: 01/04/2013 09:39:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create view [dbo].[vw_wrkPivotCustID_ConcatIDs]
as

	select tipnumber,
			isnull(cid_1, '') + case
									when cid_2 is null then ''
									else ', '
								end +
			isnull(cid_2, '') + case
									when cid_3 is null then ''
									else ', '
								end +
			isnull(cid_3, '') + case
									when cid_4 is null then ''
									else ', '
								end +
			isnull(cid_4, '') + case
									when cid_5 is null then ''
									else ', '
								end +
			isnull(cid_5, '') as DDA_Numbers
	from dbo.wrkPivotCustID

GO

