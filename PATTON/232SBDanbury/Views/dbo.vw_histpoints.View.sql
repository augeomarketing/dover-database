USE [232SBDanbury]
GO
/****** Object:  View [dbo].[vw_histpoints]    Script Date: 03/30/2010 13:34:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vw_histpoints] 

as 

select tipnumber, sum(points * ratio) as points 
from dbo.history_stage 
where secid = 'NEW' 
group by tipnumber
GO
