USE [232SBDanbury]
GO
/****** Object:  View [dbo].[vw_BonusParticipantsPointMultiplier]    Script Date: 03/30/2010 13:34:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_BonusParticipantsPointMultiplier]
as

select dim_BonusParticipants_TipNumber, dim_BonusProgram_PointMultiplier, dim_BonusProgram_EffectiveDate, dim_BonusProgram_ExpirationDate
from dbo.BonusParticipants bp join dbo.BonusProgram pgm
	on bp.sid_BonusType_Id = pgm.sid_BonusType_Id
GO
