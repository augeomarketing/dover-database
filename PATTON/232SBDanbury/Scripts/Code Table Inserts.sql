﻿USE [232SBDanbury];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[AcctType]([AcctType], [AcctTypeDesc], [Acctmultiplier])
SELECT N'Car', N'Car Loan', 1.00 UNION ALL
SELECT N'CD', N'CD', 1.00 UNION ALL
SELECT N'Check', N'Checking', 1.00 UNION ALL
SELECT N'Credit', N'CREDIT CARD', 1.00 UNION ALL
SELECT N'Credit/Debit', N'Credit/Debit Card', 1.00 UNION ALL
SELECT N'Debit', N'Debit Card', 1.00 UNION ALL
SELECT N'Loan', N'Loan Card', 1.00 UNION ALL
SELECT N'Other', N'Other', 1.00 UNION ALL
SELECT N'Savings', N'Savings Card', 1.00
COMMIT;
RAISERROR (N'[dbo].[AcctType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[PointsExpireFrequency]([PointsExpireFrequencyCd], [PointsExpireFrequencyNm])
SELECT N'ME', N'Month End' UNION ALL
SELECT N'MM', N'Mid Month' UNION ALL
SELECT N'YE', N'Year End'
COMMIT;
RAISERROR (N'[dbo].[PointsExpireFrequency]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranType]([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode])
SELECT N'0A', N'Welcome Call Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0B', N'Employee Activation Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0C', N'Mortgage Cross Sell Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0D', N'Branch Balance Transfer Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0E', N'Activation Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0F', N'April Purchase Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0G', N'Monthly Purchase Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0H', N'Loyalty Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0I', N'Gift Card Issue Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0J', N'Summer Usage Bonus
 
 ', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'0K', N'Back Activity', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'31', N'Credit Preferred Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'32', N'Credit Return Reversal', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'33', N'Credit Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'34', N'Debit Signature Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'35', N'Debit PIN Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'36', N'Debit Return Reversal', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'37', N'Debit Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'38', N'Tiered Awards Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'39', N'Fixed Awards Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'3B', N'Return Business Debit', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'3H', N'HELOC Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'3M', N'Merchant Credit Return', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'61', N'Credit Preferred Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'62', N'Credit Purchase Reversal', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'63', N'Credit Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'64', N'Debit Signature Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'65', N'Debit Pin Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'66', N'Debit Purchase Reversal', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'67', N'Debit Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'68', N'Tiered Awards Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'69', N'Fixed Awards Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'6B', N'Purchase Business Debit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'6H', N'HELOC Purchase', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'6M', N'Merchant Credit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'B1', N'Credit Card E-Statement selection', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BA', N'Bonus Activation', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BC', N'Bonus Conversion', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BE', N'Bonus Email Statements', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BF', N'Bonus First Time Debit_Credit Card Use', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BG', N'Consumer Flex Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BH', N'Consumer Sig Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BI', N'Bonus Generic', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BJ', N'Business Flex Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BK', N'Business Sig Money Market', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BL', N'HELOC New', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BM', N'Bonus Monthly', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BN', N'Bonus New Account', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BP', N'Direct Deposit Monthly', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BR', N'Bonus On-Line Rewards Registration', N'I', N'A', 1, 1, N'7'
COMMIT;
RAISERROR (N'[dbo].[TranType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranType]([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode])
SELECT N'BS', N'Online Registration Bonus', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BT', N'Bonus Transfer', N'I', N'A', 1, 1, N'5' UNION ALL
SELECT N'BV', N'Investment', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'BX', N'Bonus Reversal', N'D', N'A', 1, -1, N'5' UNION ALL
SELECT N'DE', N'Decrease Earned', N'D', N'A', 1, -1, N'1' UNION ALL
SELECT N'DR', N'Decrease Redeem', N'I', N'A', 1, 1, N'3' UNION ALL
SELECT N'DZ', N'FI Entered  Decrease  Redemption', N'D', N'A', 1, 1, N'3' UNION ALL
SELECT N'F1', N'Life Insurance w/Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F2', N'Disability Insurance w/Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F3', N'Unemployment Insurance w/Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F4', N'Identity Theft Insurance', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F5', N'Bonus Business Online Banking', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F6', N'Bonus Business Bill Pay', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F7', N'Bonus Business Direct Deposit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'F8', N'Bonus Business Ten Debit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FA', N'Merchant Reward', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FB', N'Online Banking', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FC', N'Online Bill Pay', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FD', N'Direct Deposit Payroll', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FE', N'Employee Incentive Debit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FF', N'Employee Incentive Credit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FG', N'Ten Credit Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FH', N'HELOC Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FI', N'Debit Card w/New Checking Account', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FJ', N'New Credit Card', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FK', N'New Savings Account', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FL', N'New Money CD', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FM', N'Mortgage', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FN', N'Automobile Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FO', N'Kids Bank Deposits', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FP', N'Personal Loan', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FQ', N'Referral Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FR', N'Full Spectrum Reversal', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'FS', N'Savings Account Balance', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FT', N'Line of Credit', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FU', N'Preferred Customer Bonus', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FV', N'Business Debit Card', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FW', N'Business Online Enrollment', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FX', N'New Business Savings', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'FY', N'New Kids Savings', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G2', N'2 Point', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G3', N'3 Point', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G4', N'Avg. Deposit ', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G5', N'Pre-Authorized Withdrawals', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G6', N'E-Statement Selection', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'G7', N'2 Point Returns', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'G8', N'3 Point Returns', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'GX', N'Corporate Returns', N'D', N'A', 1, -1, N'7' UNION ALL
SELECT N'IE', N'Increase Earned', N'I', N'A', 1, 1, N'1' UNION ALL
SELECT N'II', N'Combine Increase', N'I', N'A', 1, 1, N'7'
COMMIT;
RAISERROR (N'[dbo].[TranType]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranType]([TranCode], [Description], [IncDec], [CntAmtFxd], [Points], [Ratio], [TypeCode])
SELECT N'IR', N'Increase Redeem', N'D', N'A', 1, -1, N'3' UNION ALL
SELECT N'RB', N'Redeem (Bucks) Cash Back', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RC', N'Redeem Cards', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RD', N'Redeem Downloaded Items', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RF', N'Redeem for Fee', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RG', N'Redeem Giving', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RI', N'Redeem Increase', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RK', N'Redeem Coupon', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RM', N'Redeem Merchandise', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RP', N'Redeem Points (Legacy Code)', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RQ', N'Promotional Brochure Request', N'I', N'A', 1, 1, N'9' UNION ALL
SELECT N'RS', N'Redeem Downloadable Tunes (Legacy)', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RT', N'Redeem Travel', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RU', N'Redeem for QTR Travel (Legacy)', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RV', N'Redeem for Online Travel', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'RZ', N'CLASS FI FULLFILLED REDEMPTION', N'D', N'A', 1, -1, N'9' UNION ALL
SELECT N'TD', N'Transfer Points Decrease', N'D', N'A', 1, -1, N'1' UNION ALL
SELECT N'TI', N'Transfer Points Increase', N'I', N'A', 1, 1, N'1' UNION ALL
SELECT N'TP', N'Transfer Standard', N'I', N'A', 1, 1, N'1' UNION ALL
SELECT N'XF', N'Expired Points Fix', N'I', N'A', 1, 1, N'7' UNION ALL
SELECT N'XP', N'Expired Points Standard', N'D', N'A', 1, -1, N'7'
COMMIT;
RAISERROR (N'[dbo].[TranType]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[Client]([ClientCode], [ClientName], [Description], [TipFirst], [Address1], [Address2], [Address3], [Address4], [City], [State], [Zipcode], [Phone1], [Phone2], [ContactPerson1], [ContactPerson2], [ContactEmail1], [ContactEmail2], [DateJoined], [RNProgramName], [TermsConditions], [PointsUpdatedDT], [MinRedeemNeeded], [TravelFlag], [MerchandiseFlag], [TravelIncMinPoints], [MerchandiseBonusMinPoints], [MaxPointsPerYear], [PointExpirationYears], [ClientID], [Pass], [ServerName], [DbName], [UserName], [Password], [PointsExpire], [LastTipNumberUsed], [PointsExpireFrequencyCd], [ClosedMonths], [PointsUpdated], [TravelMinimum], [TravelBottom], [CashBackMinimum], [Merch], [AirFee], [logo], [landing], [termspage], [faqpage], [earnpage], [Business], [StatementDefault], [StmtNum], [CustomerServicePhone])
SELECT N'Danbury', N'Savings Bank of Danbury', N'Savings Bank of Danbury', N'232', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20100301 00:00:00.000', N'RewardsNOW', NULL, NULL, NULL, N'1', N'1', NULL, NULL, 999999999, NULL, N'232', N'SBDanbury', N'236722-SQLCLUS2\RN', N'232SBDanbury', NULL, NULL, NULL, N'232000000151611', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[Client]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[TranCode_Factor]([Trancode], [PointFactor])
SELECT N'33', 0.50 UNION ALL
SELECT N'37', 0.50 UNION ALL
SELECT N'63', 0.50 UNION ALL
SELECT N'67', 0.50
COMMIT;
RAISERROR (N'[dbo].[TranCode_Factor]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

