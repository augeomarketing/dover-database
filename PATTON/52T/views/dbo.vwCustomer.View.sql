USE [52T]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 08/09/2011 10:17:11 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [52T].dbo.customer
GO
