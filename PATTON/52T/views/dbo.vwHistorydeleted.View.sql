USE [52T]
GO
/****** Object:  View [dbo].[vwHistorydeleted]    Script Date: 08/09/2011 10:17:11 ******/
DROP VIEW [dbo].[vwHistorydeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistorydeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
			 from [52T].dbo.historydeleted
GO
