USE [52T]
GO
/****** Object:  StoredProcedure [dbo].[pSetDailyFileName]    Script Date: 08/09/2011 10:12:34 ******/
DROP PROCEDURE [dbo].[pSetDailyFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetDailyFileName] @newname nchar(100)  output 
AS

declare @Filename char(50), @currentdate nchar(8), @workmonth nchar(2), @workday nchar(2), @workyear nchar(4)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=month(getdate())
set @workday=day(getdate())
set @workyear=year(getdate())


if len(@workmonth)='1'
	begin
	set @workmonth='0'+@workmonth	
	end

if len(@workday)='1'
	begin
	set @workday='0'+@workday	
	end

set @currentdate=@workmonth + '-' + @workday + '-' + right(@workyear,2)

set @filename=@currentdate + '.txt'

set @newname='\\236722-sqlclus2\Everbank\FileGen.bat ' + @filename
GO
