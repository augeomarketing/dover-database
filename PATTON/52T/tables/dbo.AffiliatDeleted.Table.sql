USE [52T]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 08/09/2011 10:10:45 ******/
ALTER TABLE [dbo].[AffiliatDeleted] DROP CONSTRAINT [DF__AffiliatD__YTDEa__5AEE82B9]
GO
DROP TABLE [dbo].[AffiliatDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[AffiliatDeleted] ADD  DEFAULT ((0)) FOR [YTDEarned]
GO
