USE [52T]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[Customer_Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
GO
