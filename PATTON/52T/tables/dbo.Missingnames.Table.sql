USE [52T]
GO
/****** Object:  Table [dbo].[Missingnames]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[Missingnames]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Missingnames](
	[ssn] [nvarchar](9) NULL,
	[bank] [nvarchar](4) NULL,
	[agent] [nvarchar](4) NULL,
	[ddanum] [nvarchar](11) NULL,
	[acctnum] [nvarchar](25) NULL,
	[addr1] [nvarchar](40) NULL,
	[citystate] [nvarchar](40) NULL,
	[zip] [nvarchar](10) NULL
) ON [PRIMARY]
GO
