USE [52T]
GO
/****** Object:  Table [dbo].[DailyFeed]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[DailyFeed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyFeed](
	[Cardnumber] [char](16) NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
