USE [52T]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 08/09/2011 10:10:45 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__4E88ABD4]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__4F7CD00D]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__5070F446]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__5165187F]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__52593CB8]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__534D60F1]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__5441852A]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__5535A963]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__5629CD9C]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__571DF1D5]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__5812160E]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] DROP CONSTRAINT [DF__Beginning__Month__59063A47]
GO
DROP TABLE [dbo].[Beginning_Balance_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg1]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg2]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg3]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg4]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg5]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg6]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg7]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg8]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg9]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg10]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg11]
GO
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT ((0)) FOR [MonthBeg12]
GO
