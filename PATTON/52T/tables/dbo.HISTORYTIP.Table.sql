USE [52T]
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 08/09/2011 10:10:46 ******/
ALTER TABLE [dbo].[HISTORYTIP] DROP CONSTRAINT [DF__HISTORYTI__Overa__47DBAE45]
GO
DROP TABLE [dbo].[HISTORYTIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[HISTORYTIP] ADD  DEFAULT ((0)) FOR [Overage]
GO
