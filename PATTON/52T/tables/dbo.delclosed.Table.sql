USE [52T]
GO
/****** Object:  Table [dbo].[delclosed]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[delclosed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[delclosed](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
