USE [52T]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 08/09/2011 10:10:46 ******/
ALTER TABLE [dbo].[HistoryDeleted] DROP CONSTRAINT [DF__HistoryDe__Overa__48CFD27E]
GO
DROP TABLE [dbo].[HistoryDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[HistoryDeleted] ADD  DEFAULT ((0)) FOR [Overage]
GO
