USE [52T]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/09/2011 10:10:45 ******/
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Endin__4AB81AF0]
GO
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Incre__4BAC3F29]
GO
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Decre__4CA06362]
GO
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Adjus__4D94879B]
GO
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT ((0)) FOR [EndingPoints]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT ((0)) FOR [Increases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT ((0)) FOR [Decreases]
GO
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT ((0)) FOR [AdjustedEndingPoints]
GO
