USE [52T]
GO
/****** Object:  Table [dbo].[DailyPoints]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[DailyPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyPoints](
	[tipnumber] [char](20) NOT NULL,
	[availablebal] [int] NULL,
	[AccountNumber] [varchar](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
