USE [52T]
GO
/****** Object:  Table [dbo].[Visaextras]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[Visaextras]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Visaextras](
	[F2] [nvarchar](20) NULL,
	[F4] [float] NULL,
	[acctid] [char](16) NULL,
	[points] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
