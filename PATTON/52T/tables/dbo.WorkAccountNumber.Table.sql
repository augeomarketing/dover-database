USE [52T]
GO
/****** Object:  Table [dbo].[WorkAccountNumber]    Script Date: 08/09/2011 10:10:46 ******/
DROP TABLE [dbo].[WorkAccountNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkAccountNumber](
	[AccountNumber] [char](16) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
