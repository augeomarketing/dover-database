USE [52T]
GO

insert into AcctType
	(AcctType, accttypedesc)
	SELECT	AcctType, accttypedesc FROM [529EverbankConsumer].dbo.AcctType

insert into Constants
	(Field1, Field2)
	SELECT Field1, Field2 FROM [529EverbankConsumer].dbo.Constants

insert into PointsExpireFrequency
	(PointsExpireFrequencyCd, PointsExpireFrequencyNm)
	SELECT PointsExpireFrequencyCd, PointsExpireFrequencyNm FROM [529EverbankConsumer].dbo.PointsExpireFrequency

insert into Status
	(Status, StatusDescription)
	SELECT Status, Statusdescription FROM [529EverbankConsumer].dbo.Status

insert into trantype
	(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
	SELECT TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode FROM [529EverbankConsumer].dbo.trantype

insert into Client
(
	ClientCode, ClientName, Description, tipfirst, Address1, Address2, Address3, Address4
	, City, State, ZipCode, Phone1, Phone2, ContactPerson1, ContactPerson2, ContactEmail1
	, ContactEmail2, DateJoined, RNProgramName, TermsConditions, PointsUpdatedDT, MinRedeemNeeded
	, TravelFlag, MerchandiseFlag, TravelIncMinPoints, MerchandiseBonusMinPoints, MaxPointsPerYear
	, PointExpirationYears, Clientid, Pass, ServerName, dbname, UserName, Password, PointsExpire
	, lasttipnumberused, PointsExpireFrequencyCd, ClosedMonths

)

	SELECT	'EverbankCash' as ClientCode, 'EverBank Cash Rewards' as ClientName, 'EverBank Cash Rewards' as Description,
			'52T' as tipfirst, Address1, Address2, Address3, Address4, City, State, ZipCode, Phone1, Phone2, ContactPerson1, ContactPerson2,
			ContactEmail1, ContactEmail2, '2011-08-01 00:00:00.000' as DateJoined, 'EverBank Cash Rewards' as RNProgramName, TermsConditions, 
			PointsUpdatedDT, MinRedeemNeeded, TravelFlag, MerchandiseFlag, TravelIncMinPoints, MerchandiseBonusMinPoints, MaxPointsPerYear, 
			PointExpirationYears, '52T' as Clientid, Pass, ServerName, '52T' as dbname, UserName, Password, PointsExpire, 
			'52T000000000000' as lasttipnumberused, PointsExpireFrequencyCd, ClosedMonths
	FROM	[529EverbankConsumer].dbo.Client

insert into MetavanteWork.dbo.autoprocessdetail
(
	tipfirst, dbname1, dbname2, sourcefilenamestring, backupnamestring, monthbeginingdate
	, monthendingdate, rnclientcode, di_id, ca_id, pw_id, ub_id, wk_id, qs_id, [Debit/Credit]
	, missingnameout, quarterbeginingdate, quarterendingdate
)
	SELECT	'52T' as tipfirst, '52T' as dbname1, '52T' as dbname2, NULL as sourcefilenamestring,
			'T:\ProcessingBackups\52T.bak' as backupnamestring, '08/01/2011' as monthbeginingdate, 
			'08/31/2011' as monthendingdate, 'EverbankCash' as rnclientcode, '0' as di_d, '0' as ca_id, '0' as pw_id,
			'0' as ub_id, '0' as wk_id, '0' as qs_id, 'C' as [Debit/Credit], 
			'O:\5xx\Output\Errorfiles\MissingNames\52TMissingNames.csv' as missingnameout, 
			NULL as quarterbeginingdate, NULL as quarterendingdate

insert into MetavanteWork.dbo.packagecodes
	SELECT	'52T',0,1,4,1,1,0,0

insert into MetavanteWork.dbo.DBProcessInfo (DBName, DBNumber, ActivationBonus, FirstUseBonus, OnlineRegistrationBonus, [E-StatementBonus], DebitFactor)
	SELECT	'52T', '52T', ActivationBonus, FirstUseBonus, OnlineRegistrationBonus, [E-StatementBonus] , DebitFactor
	FROM	MetavanteWork.dbo.DBProcessInfo
	WHERE	DBNumber = '529'

	UPDATE	RewardsNow.dbo.dbprocessinfo
	SET		DBAvailable = 'Y', lasttipnumberused = '52T000000000000', maxpointsperyear = '0', Closedmonths = '0', sid_fiprodstatus_statuscode = 'P',
			pointexpirationyears = '0', minredeemneeded = '2500', CalcDailyExpire = 'Y'
	WHERE	dbnumber = '52T'

insert into RewardsNow.dbo.RptCtlClients
	SELECT	'EverbankCash' as RNName, '52T' as ClientNum, 'EverBank Cash Rewards' as FormalName,
			'52T' as ClientDBName, ClientDBLocation, OnlClientDBName, OnlClientDBLocation
	FROM	RewardsNow.dbo.RptCtlClients
	WHERE	ClientNum = '529'
	