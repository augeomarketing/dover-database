USE [master]
GO
/****** Object:  Database [52T]    Script Date: 08/08/2011 09:18:09 ******/
--CREATE DATABASE [52T] ON  PRIMARY 
--( NAME = N'52T_Data', FILENAME = N'C:\SQLData_RN\52T.mdf' , SIZE = 21440KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
-- LOG ON 
--( NAME = N'52T_Log', FILENAME = N'C:\SQLData_RN\52T_log.ldf' , SIZE = 13632KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
--GO
ALTER DATABASE [52T] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [52T].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [52T] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [52T] SET ANSI_NULLS OFF
GO
ALTER DATABASE [52T] SET ANSI_PADDING OFF
GO
ALTER DATABASE [52T] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [52T] SET ARITHABORT OFF
GO
ALTER DATABASE [52T] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [52T] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [52T] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [52T] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [52T] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [52T] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [52T] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [52T] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [52T] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [52T] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [52T] SET  DISABLE_BROKER
GO
ALTER DATABASE [52T] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [52T] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [52T] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [52T] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [52T] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [52T] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [52T] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [52T] SET  READ_WRITE
GO
ALTER DATABASE [52T] SET RECOVERY SIMPLE
GO
ALTER DATABASE [52T] SET  MULTI_USER
GO
ALTER DATABASE [52T] SET PAGE_VERIFY TORN_PAGE_DETECTION
GO
ALTER DATABASE [52T] SET DB_CHAINING OFF
GO
USE [52T]
GO
/****** Object:  User [RNIColdFusion]    Script Date: 08/08/2011 09:18:09 ******/
CREATE USER [RNIColdFusion] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [REWARDSNOW\svc-ReportingService]    Script Date: 08/08/2011 09:18:09 ******/
CREATE USER [REWARDSNOW\svc-ReportingService] FOR LOGIN [REWARDSNOW\svc-ReportingService] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [rewardsnow\IT NonDevelopers]    Script Date: 08/08/2011 09:18:09 ******/
CREATE USER [rewardsnow\IT NonDevelopers] FOR LOGIN [REWARDSNOW\IT NonDevelopers]
GO
/****** Object:  User [rewardsnow\IT Developers]    Script Date: 08/08/2011 09:18:09 ******/
CREATE USER [rewardsnow\IT Developers] FOR LOGIN [REWARDSNOW\IT Developers]
GO
/****** Object:  Table [dbo].[orphanwork]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orphanwork](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[trancount] [int] NULL,
	[points] [numeric](18, 0) NULL,
	[description] [varchar](50) NULL,
	[secid] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [numeric](5, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Monthly_Statement_Filesav]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Statement_Filesav](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusMER] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[lastfour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
	[PointsToExpire] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Monthly_Audit_ErrorFile] ON [dbo].[Monthly_Audit_ErrorFile] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Missingnames]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Missingnames](
	[ssn] [nvarchar](9) NULL,
	[bank] [nvarchar](4) NULL,
	[agent] [nvarchar](4) NULL,
	[ddanum] [nvarchar](11) NULL,
	[acctnum] [nvarchar](25) NULL,
	[addr1] [nvarchar](40) NULL,
	[citystate] [nvarchar](40) NULL,
	[zip] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_history_trancode_ratio_points_tipnumber_histdate] ON [dbo].[HISTORY] 
(
	[TRANCODE] ASC,
	[Ratio] ASC,
	[POINTS] ASC,
	[TIPNUMBER] ASC,
	[HISTDATE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistExp]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistExp](
	[TIPNumber] [varchar](15) NOT NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Zipcode] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[TotEarned] [int] NULL,
	[RunRedemed] [int] NULL,
	[Available] [int] NULL,
	[LastStmtDT] [smalldatetime] NULL,
	[AcctID] [varchar](25) NULL,
	[DateAdded] [smalldatetime] NULL,
	[CardType] [varchar](20) NULL,
	[Descriptio] [nvarchar](40) NULL,
	[TranCode] [nvarchar](2) NULL,
	[Points] [numeric](18, 0) NULL,
	[HistDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EXPIRINGPOINTS]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EXPIRINGPOINTS](
	[tipnumber] [varchar](15) NOT NULL,
	[CredAddPoints] [float] NULL,
	[DebAddpoints] [float] NULL,
	[CredRetPoints] [float] NULL,
	[DebRetPoints] [float] NULL,
	[CredAddPointsNext] [float] NULL,
	[DebAddPointsNext] [float] NULL,
	[CredRetPointsNext] [float] NULL,
	[DebRetPointsNext] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateofExpire] [nvarchar](25) NULL,
	[PointsToExpireNext] [float] NULL,
	[AddPoints] [float] NULL,
	[ADDPOINTSNEXT] [float] NULL,
	[PointsOther] [float] NULL,
	[PointsOthernext] [float] NULL,
	[EXPTODATE] [float] NULL,
	[ExpiredRefunded] [float] NULL,
	[dbnameonnexl] [nvarchar](50) NULL,
	[CredaddpointsPLUS3MO] [float] NULL,
	[DEbaddpointsPLUS3MO] [float] NULL,
	[CredRetpointsPLUS3MO] [float] NULL,
	[DEbRetpointsPLUS3MO] [float] NULL,
	[POINTSTOEXPIREPLUS3MO] [float] NULL,
	[ADDPOINTSPLUS3MO] [float] NULL,
	[PointsOtherPLUS3MO] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[delclosed]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[delclosed](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DailyPoints]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyPoints](
	[tipnumber] [char](20) NOT NULL,
	[availablebal] [int] NULL,
	[AccountNumber] [varchar](16) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DailyFeed]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DailyFeed](
	[Cardnumber] [char](16) NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerDeleted]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerDeleted](
	[TIPNumber] [varchar](15) NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [numeric](10, 0) NULL,
	[RunRedeemed] [numeric](10, 0) NULL,
	[RunAvailable] [numeric](10, 0) NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[Notes] [text] NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BusinessFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[customer_fix]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customer_fix](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Constants]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Constants](
	[Field1] [char](10) NULL,
	[Field2] [char](6) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comb_TipTracking]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comb_TipTracking](
	[NewTIP] [char](15) NOT NULL,
	[OldTip] [char](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[OldTipPoints] [int] NULL,
	[OldTipRedeemed] [int] NULL,
	[OldTipRank] [char](1) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMB_IN]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMB_IN](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[LAST6_PRI] [varchar](6) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[LAST6_SEC] [varchar](6) NULL,
	[POINT_SEC] [float] NULL,
	[POINT_TOT] [float] NULL,
	[TANDATE] [varchar](8) NULL,
	[ERRMSG] [varchar](80) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMB_err]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMB_err](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[TRANDATE] [datetime] NULL,
	[ERRMSG] [varchar](80) NULL,
	[ERRMSG2] [varchar](80) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CertificateDeleted]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [numeric](18, 0) NULL,
	[RedeemedNow] [numeric](18, 0) NOT NULL,
	[RunAvailable] [numeric](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificate](
	[CertificateID] [int] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [int] NULL,
	[RedeemedNow] [int] NULL,
	[RunAvailable] [int] NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [int] NOT NULL,
	[CatalogCode] [varchar](50) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NOT NULL,
	[Special] [char](1) NOT NULL,
	[Business] [char](1) NOT NULL,
	[Television] [char](1) NOT NULL,
	[ClientAwardPoints] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AugEnd]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AugEnd](
	[TRAVNUM] [nvarchar](255) NULL,
	[PNTEND] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[affiliat_fix]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[affiliat_fix](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_AFFILIAT_106_389576426__K2_K1] ON [dbo].[AFFILIAT] 
(
	[TIPNUMBER] ASC,
	[ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 08/08/2011 09:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[spCombineBeginningBalance]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO update beginning_balance_table with COMBINES                          */
/*                                                                            */
/* BY:  R.Tremblay                                          */
/* DATE: 9/2006                                                               */
/* REVISION: 1                                                                */
/* */
/* 	CAUTION 	CAUTION 	CAUTION */
/* Run this ONCE because the Secondary TIP values are ADDED to the primary tip values. */
/* 	CAUTION 	CAUTION 	CAUTION */
/* */


CREATE   PROCEDURE [dbo].[spCombineBeginningBalance] @TipPrimary char(15), @TipSecondary char(15)
AS 

Declare @MonthBucket char(10), @Month int, @SQLUpdate nvarchar(1000)

Set @Month = 1

While @Month < 13 
Begin 
	set @MonthBucket='MonthBeg' + cast(@Month as char)

	set @SQLUpdate = N'update Beginning_Balance_Table set '+ Quotename(@MonthBucket) + 
	   N'= ( select sum(' + Quotename(@MonthBucket) +' ) from  beginning_balance_table 
		where tipnumber = ''' +@TipPrimary + ''' or tipnumber = '''  + @TipSecondary + ' '') where tipnumber = ''' + @TipPrimary + ''''
--print @SQLUpdate 
exec sp_executesql @SQLUpdate

	
	set @Month = @Month + 1 

End
GO
/****** Object:  Table [dbo].[Results]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [smalldatetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reg]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusMer] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Quarterly_Statement_File] ON [dbo].[Quarterly_Statement_File] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quarterly_Audit_ErrorFile]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Quarterly_Audit_ErrorFile] ON [dbo].[Quarterly_Audit_ErrorFile] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[pSetDailyFileName]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetDailyFileName] @newname nchar(100)  output 
AS

declare @Filename char(50), @currentdate nchar(8), @workmonth nchar(2), @workday nchar(2), @workyear nchar(4)

-- Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=month(getdate())
set @workday=day(getdate())
set @workyear=year(getdate())


if len(@workmonth)='1'
	begin
	set @workmonth='0'+@workmonth	
	end

if len(@workday)='1'
	begin
	set @workday='0'+@workday	
	end

set @currentdate=@workmonth + '-' + @workday + '-' + right(@workyear,2)

set @filename=@currentdate + '.txt'

set @newname='\\236722-sqlclus2\Everbank\FileGen.bat ' + @filename
GO
/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL,
 CONSTRAINT [PK_PointsExpireFrequency] PRIMARY KEY CLUSTERED 
(
	[PointsExpireFrequencyCd] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisaextrasWork2]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisaextrasWork2](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VisaExtrasWork1]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VisaExtrasWork1](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Visaextras]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visaextras](
	[F2] [nvarchar](20) NULL,
	[F4] [float] NULL,
	[acctid] [char](16) NULL,
	[points] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StmtExp]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [numeric](10, 0) NULL,
	[ENDBAL] [numeric](10, 0) NULL,
	[CCPURCHASE] [numeric](10, 0) NULL,
	[BONUS] [numeric](10, 0) NULL,
	[PNTADD] [numeric](10, 0) NULL,
	[PNTINCRS] [numeric](10, 0) NULL,
	[REDEEMED] [numeric](10, 0) NULL,
	[PNTRETRN] [numeric](10, 0) NULL,
	[PNTSUBTR] [numeric](10, 0) NULL,
	[PNTDECRS] [numeric](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkAccountNumber]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkAccountNumber](
	[AccountNumber] [char](16) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 08/08/2011 09:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WelcomeKit](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwHistorydeleted]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistorydeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
			 from [52T].dbo.historydeleted
GO
/****** Object:  View [dbo].[vwHistory]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistory]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from [52T].dbo.history
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [52T].dbo.Customerdeleted
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [52T].dbo.customer
GO
/****** Object:  View [dbo].[vwAffiliatdeleted]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliatdeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID, datedeleted
			  from [52T].dbo.affiliatdeleted
GO
/****** Object:  View [dbo].[vwAffiliat]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliat]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			 from [52T].dbo.affiliat
GO
/****** Object:  StoredProcedure [dbo].[spCombineTips]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*                   SQL TO PROCESS  COMBINES                          */
/*                                                                         */
/* BY:  R.Tremblay                                          */
/* DATE: 8/2006                                                               */
/* REVISION: 1                                                                */
/* This creates a new tip. Copies the pri customer to customerdeleted.
 Copies sec customer to customerdeleted. Changes  pri customer tip  to new tip
 Changes all pri affiliat records to new Tip. Changes all pri history records to new Tip.
 Changes all sec affiliat records to new Tip. Changes all sec history records to new Tip.
 Add secondary TIP values (runavailable, runbalance, etc) to NEW TIP 
 Deletes sec customer                                                           

 Added code to call spCombineBeginningBalance
 Added code to combine Bonus tale transactions 10/10/06

 Altered Table by adding OldTipPoints column to track the number of points transferred from the old tip
 Added code to update the points transferred 

 10/3/06 - Added OldTipRank = P = Primary S = Secondary

 Input file must be ordered by PRITIP to check for duplicates. 
 NEW Tables needed 
	COMB_IN
	COMB_err
THE FOLLOWING TABLE NEEDS TO BE CREATED ON PATTON AND RN1
	Comb_TipTracking
BRAEK LOGIC REPEATED AFTER WHILE STATEMENT TO PROCESS LAST RECORD AFTER EOF    BJQ 12/27/2006
WRITE TO COMB_ERR CHANGED TO ADD NAME1 AND NAME2 TO THE OUTPUT REC             BJQ 12/28/2006
                                                                                                 */
/******************************************************************************/	
/******************************************************************************/	
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
-- Changed logic to employ last tip used instead of max tip
/******************************************************************************/	

 CREATE PROCEDURE [dbo].[spCombineTips] @a_TipPrefix nchar(3) /* SEB001 */AS    

declare @a_TIPPRI char(15), @a_TIPSEC char(15), @a_TIPBREAK char(15), @a_errmsg char(80)
declare @a_namep char(40), @a_namep1 char(40), @a_names char(40), @a_names1 char(40), @a_addrp char(40), @a_addrs char(40)     
declare @a_RunAvailable int, @a_RunBalance int, @a_RunRedeemed int
declare @DateToday datetime
declare @NewTip as char(15), @NewTipValue bigint, @DeleteDescription as char(40)
declare @Old_TipPri nvarchar(15)
declare @newnum bigint, @DBName varchar(50) /* SEB001 */

SET @DateToday = GetDate()
SET @a_TIPBREAK = '0'
set @Old_TipPri = '0'
/******************************************************************************/	
/*  Delete null tips                                */
delete from comb_in where tip_pri is null
delete from comb_in where tip_sec is null

/******************************************************************************/	
/*  Delete tips  from Comb_IN if there is an error message               */
delete from comb_in where Len( RTrim( errmsg) ) > 0 

/******************************************************************************/	
/*  DECLARE CURSOR FOR PROCESSING COMB_IN TABLE                               */
declare combine_crsr cursor for 
	select TIP_PRI, TIP_SEC, ERRMSG
	from comb_in 
	order by TIP_PRI, TIP_SEC

open combine_crsr

fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg 

/******************************************************************************/	
/* MAIN PROCESSING                                                */
/******************************************************************************/	
if @@FETCH_STATUS = 1
	goto Fetch_Error

while @@FETCH_STATUS = 0
begin	

	IF @a_TIPPRI <> @Old_TipPri
	   set @NewTip = '0'

	/* Check for existance of  Primary and secondary tips. */ 
	IF @a_TIPPRI <> @Old_TipPri
	 begin
	  IF  not EXISTS ( SELECT tipnumber FROM Customer WHERE TIPnumber = @a_TIPPRI )	  
		begin
		Insert into comb_err (TIP_PRI, TIP_SEC, TRANDATE, errmsg, errmsg2)
		Values (@a_TIPPRI, @a_TIPSEC, @DateToday,  @a_errmsg, 'Primary Tip not in Customer Table' )
		goto Next_Record
		end	   
	end

	IF  NOT EXISTS ( SELECT tipnumber FROM Customer WHERE TIPnumber = @a_TIPSEC )
	begin
		Insert into comb_err (TIP_PRI, TIP_SEC, TRANDATE, errmsg, errmsg2)
		Values (@a_TIPPRI, @a_TIPSEC,  @DateToday,  @a_errmsg, 'Secondary Tip not in Customer Table.' )
		goto Next_Record
	end


	/* Check for new primary tip. If new then create a new tip else use the same "new" tip */
	IF @a_TIPPRI <> @a_TIPBREAK 
	Begin
		/*    Create new tip          */
		/*********  Begin SEB001  *************************/
		--set @NewTipValue = ( select cast(max(tipnumber)as bigint) + 1 from customer )
		declare @LastTipUsed char(15)

		exec rewardsnow.dbo.spGetLastTipNumberUsed @a_TipPrefix, @LastTipUsed output
		select @LastTipUsed as LastTipUsed
		
		set @NewTipValue = cast(@LastTipUsed as bigint) + 1  
		/*********  End SEB001  *************************/

		/*  Make sure the tipnumber is 15 character long by adding zeros to the begining of the Tip*/
		/* clients with tip prefix starting zeros will have max tips less than 15 characters */

/* 	Code replaced with Code Sarah recreated to pad with leading zeros where approiate  BJQ 12/2006 */
/*		set @NewTipValue = replicate('0',15 - Len( RTrim( @NewTipValue ) ) ) +  @NewTipValue */
/*		set @NewTip = Cast(@NewTipValue as char)                                              */ 

       		 set @NewTip = Cast(@NewTipValue as char)
           	 set @NewTip = replicate('0',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip 

		exec RewardsNOW.dbo.spPutLastTipNumberUsed @a_TipPrefix, @NewTip  /*SEB001 */

		/******************************************************************************/	
		/*  copy PRI customer to customerdeleted set description to new tipnumber     */
		/* IF the PRI tip is not repeated */
 
		set @DeleteDescription = 'Pri Combined to '  + @NewTip
		INSERT INTO CustomerDeleted
		(
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
		)
       		SELECT 
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	 	 
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
		FROM Customer where tipnumber = @a_TIPPRI

		/******************************************************************************/	
		/* Add Get PRI tip values from Customer  */
 
		select 	@a_RunAvailable =RunAvailable,  @a_RunRedeemed=RunRedeemed from customer where tipnumber = @a_TIPPRI

		/*  Insert record into Comb_TipTracking table  */
 
		Insert into Comb_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank) Values (@NewTip, @a_TIPPRI, @DateToday, @a_RunAvailable, @a_RunRedeemed,'P' )

	End --@a_TIPPRI <> @a_TIPBREAK 

	/******************************************************************************/	
	/*  copy SEC customer to customerdeleted set description to new tipnumber         */
	set @DeleteDescription = 'Sec Combined to '  + @NewTip
 
	INSERT INTO CustomerDeleted
	(
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 StatusDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	  
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	DateDeleted 
	)
       	SELECT 
		TIPNUMBER,  
		 RunAvailable ,	 RUNBALANCE  ,	 RunRedeemed ,	 LastStmtDate ,	 NextStmtDate ,	 STATUS  ,	 
		 DATEADDED  ,	 LASTNAME  ,	 TIPFIRST  ,	 TIPLAST  ,
		 ACCTNAME1 ,	 ACCTNAME2 ,	 ACCTNAME3 ,	 ACCTNAME4 ,	 ACCTNAME5 ,	 ACCTNAME6 ,
		 ADDRESS1 ,	 ADDRESS2 ,	 ADDRESS3 ,	 ADDRESS4 ,	 City  ,	 State ,	 ZipCode ,
		 @DeleteDescription  ,	 HOMEPHONE  ,	 WORKPHONE  ,	 BusinessFlag,  	 EmployeeFlag , 
		 SegmentCode  ,	  ComboStmt, RewardsOnline,  NOTES  ,	  
		 Misc1  ,	 Misc2  ,	 Misc3  ,	 Misc4  ,	 Misc5  ,
		 RunBalanceNew  ,	 RunAvaliableNew , 	@DateToday  
	FROM Customer where tipnumber = @a_TIPSEC

	/******************************************************************************/	
	/* Add Get SEC  tip values from Customer  */
 
	select 	@a_RunAvailable =RunAvailable,  @a_RunRedeemed=RunRedeemed from customer where tipnumber = @a_TIPSEC
 
	/******************************************************************************/	
	/*  Insert record into Comb_TipTracking table  */
 
	Insert into Comb_TipTracking (NewTip, OldTIP, TranDate, OldTipPoints, OldTipRedeemed, OldTipRank) Values (@NewTip, @a_TIPSEC, @DateToday, @a_RunAvailable, @a_RunRedeemed,'S' )
	/******************************************************************************/	
	/*   change  PRI tip in customer to new tip number  (this avoids an insert)      */
	IF @a_TIPPRI <> @a_TIPBREAK
	   begin
	   Update Customer set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI
	   SET @Old_TipPri = @a_TIPPRI
	   end 

	/* OLD_TIPPRI ADDED for check of next record in Fetch Statement. If The nhext Tipnumber to be combined */
        /* equals a_TIPPRI the next time thru thw while statement the program will not have to select for the */
	/* Customer Record because it has just been processed   BJQ 12/2006                */

	/******************************************************************************/	
	/*         Change all PRI affiliat records to new Tip              */
 
	IF @a_TIPPRI <> @a_TIPBREAK  Update AFFILIAT  set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
	
	/******************************************************************************/	
	/*         Change all SEC affiliat records to new Tip              */
 
	update AFFILIAT set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPSEC

	/******************************************************************************/	
	/*       Change all PRI history records to new Tip          */
 
	IF @a_TIPPRI <> @a_TIPBREAK  Update HISTORY Set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPPRI

	/******************************************************************************/	
	/*       Change all SEC history records to new Tip          */
 
	update HISTORY set TIPNUMBER= @NewTip where TIPNUMBER = @a_TIPSEC

	/******************************************************************************/	
	/* Get and Add SEC tip values to Customer NEW TIP */
 
	select 
		@a_RunAvailable =RunAvailable, 
		@a_RunBalance=RunBalance, 
		@a_RunRedeemed=RunRedeemed 
	from customer where tipnumber = @a_TIPSEC

 
	update customer	
	set RunAvailable = RunAvailable + @a_RunAvailable, 
	     RunBalance=RunBalance + @a_RunBalance, 
 	     RunRedeemed=RunRedeemed + @a_RunRedeemed  
	where tipnumber = @NEWTIP
	
	/******************************************************************************/	
	/*    change  PRI tip in BEGINNING_BALANCE_TABLE to new tip number  (this avoids an insert)      */
 
	IF @a_TIPPRI <> @a_TIPBREAK  Update Beginning_Balance_Table set TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI

	/******************************************************************************/	
	/* Add SEC tip values to Beginning_Balance_Table NEW TIP for each month */
 
	exec spCombineBeginningBalance @NewTip, @a_TIPSEC

	/******************************************************************************/	
	/* Delete SEC Beginning_Balance_Table                  */
 
	delete from Beginning_Balance_Table where tipnumber = @a_TIPSEC

	/******************************************************************************/	
	/* Delete SEC Customer                  */
 
	delete from CUSTOMER where tipnumber = @a_TIPSEC

	/******************************************************************************/	
	/*         Change all PRI & SEC BONUS records to new Tip              */
 
	If exists ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[OneTimeBonuses]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 )
	BEGIN
		Update OneTimeBonuses  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPPRI 
		Update OneTimeBonuses  set  TIPNUMBER = @NewTip where TIPNUMBER = @a_TIPSEC
	END

Next_Record:
 
	Set @a_TIPBREAK = @a_TIPPRI
	fetch combine_crsr into @a_TIPPRI, @a_TIPSEC, @a_errmsg
	
end --while @@FETCH_STATUS = 0




Fetch_Error:
close combine_crsr

deallocate combine_crsr
GO
/****** Object:  StoredProcedure [dbo].[pGenerateDailyFeed]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[pGenerateDailyFeed]
as

Truncate Table Dailyfeed

Truncate Table WorkAccountNumber

insert into workAccountNumber ( AccountNumber, Tipnumber)
select distinct Acctid, Tipnumber from affiliat
where acctstatus='A'

update dailypoints
set dailypoints.accountnumber=workaccountnumber.accountnumber
from dailypoints, workaccountnumber
where dailypoints.tipnumber=workaccountnumber.tipnumber

insert into dailyfeed (cardnumber, points)
select accountnumber, availablebal from dailypoints
where accountnumber>'0'
GO
/****** Object:  Table [dbo].[Client]    Script Date: 08/08/2011 09:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [numeric](18, 0) NULL,
	[MerchandiseBonusMinPoints] [numeric](18, 0) NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NOT NULL,
	[Pass] [varchar](30) NOT NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipNumberUsed] [char](15) NULL,
	[ClosedMonths] [int] NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[PointsUpdated] [datetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [bit] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[logo] [varchar](50) NULL,
	[landing] [varchar](255) NULL,
	[termspage] [varchar](50) NULL,
	[faqpage] [varchar](50) NULL,
	[earnpage] [varchar](50) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StmtNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Default [DF__Monthly_A__Point__4BAC3F29]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBegin]
GO
/****** Object:  Default [DF__Monthly_A__Point__4CA06362]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsEnd]
GO
/****** Object:  Default [DF__Monthly_A__Point__4D94879B]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsPurchasedCR]
GO
/****** Object:  Default [DF__Monthly_A__Point__4E88ABD4]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBonusCR]
GO
/****** Object:  Default [DF__Monthly_A__Point__4F7CD00D]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsAdded]
GO
/****** Object:  Default [DF__Monthly_A__Point__5070F446]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsPurchasedDB]
GO
/****** Object:  Default [DF__Monthly_A__Point__5165187F]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBonusDB]
GO
/****** Object:  Default [DF__Monthly_A__Point__52593CB8]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsIncreased]
GO
/****** Object:  Default [DF__Monthly_A__Point__534D60F1]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsRedeemed]
GO
/****** Object:  Default [DF__Monthly_A__Point__5441852A]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsReturnedCR]
GO
/****** Object:  Default [DF__Monthly_A__Point__5535A963]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsSubtracted]
GO
/****** Object:  Default [DF__Monthly_A__Point__5629CD9C]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsReturnedDB]
GO
/****** Object:  Default [DF__Monthly_A__Point__571DF1D5]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsDecreased]
GO
/****** Object:  Default [DF__Monthly_A__Curre__5812160E]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Monthly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [Currentend]
GO
/****** Object:  Default [DF__HISTORYTI__Overa__29572725]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[HISTORYTIP] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__HistoryDe__Overa__276EDEB3]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[HistoryDeleted] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__HISTORY__Overage__25869641]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[HISTORY] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__Current_M__Endin__46E78A0C]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [EndingPoints]
GO
/****** Object:  Default [DF__Current_M__Incre__47DBAE45]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [Increases]
GO
/****** Object:  Default [DF__Current_M__Decre__48CFD27E]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [Decreases]
GO
/****** Object:  Default [DF__Current_M__Adjus__49C3F6B7]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Current_Month_Activity] ADD  DEFAULT (0) FOR [AdjustedEndingPoints]
GO
/****** Object:  Default [DF__Beginning__Month__3A81B327]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg1]
GO
/****** Object:  Default [DF__Beginning__Month__3B75D760]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg2]
GO
/****** Object:  Default [DF__Beginning__Month__3C69FB99]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg3]
GO
/****** Object:  Default [DF__Beginning__Month__3D5E1FD2]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg4]
GO
/****** Object:  Default [DF__Beginning__Month__3E52440B]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg5]
GO
/****** Object:  Default [DF__Beginning__Month__3F466844]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg6]
GO
/****** Object:  Default [DF__Beginning__Month__403A8C7D]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg7]
GO
/****** Object:  Default [DF__Beginning__Month__412EB0B6]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg8]
GO
/****** Object:  Default [DF__Beginning__Month__4222D4EF]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg9]
GO
/****** Object:  Default [DF__Beginning__Month__4316F928]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg10]
GO
/****** Object:  Default [DF__Beginning__Month__440B1D61]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg11]
GO
/****** Object:  Default [DF__Beginning__Month__44FF419A]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[Beginning_Balance_Table] ADD  DEFAULT (0) FOR [MonthBeg12]
GO
/****** Object:  Default [DF__AugEnd__PNTEND__03F0984C]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[AugEnd] ADD  DEFAULT (0) FOR [PNTEND]
GO
/****** Object:  Default [DF__AffiliatD__YTDEa__1BFD2C07]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[AffiliatDeleted] ADD  DEFAULT (0) FOR [YTDEarned]
GO
/****** Object:  Default [DF__AFFILIAT__YTDEar__1920BF5C]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [YTDEarned]
GO
/****** Object:  Default [DF__AFFILIAT__CustID__1A14E395]    Script Date: 08/08/2011 09:18:12 ******/
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [CustID]
GO
/****** Object:  Default [DF__Quarterly__Point__31B762FC]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__31B762FC]  DEFAULT (0) FOR [PointsBegin]
GO
/****** Object:  Default [DF__Quarterly__Point__32AB8735]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__32AB8735]  DEFAULT (0) FOR [PointsEnd]
GO
/****** Object:  Default [DF__Quarterly__Point__339FAB6E]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__339FAB6E]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
/****** Object:  Default [DF__Quarterly__Point__3493CFA7]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3493CFA7]  DEFAULT (0) FOR [PointsBonusCR]
GO
/****** Object:  Default [DF__Quarterly__Point__3587F3E0]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3587F3E0]  DEFAULT (0) FOR [PointsAdded]
GO
/****** Object:  Default [DF__Quarterly__Point__367C1819]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__367C1819]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
/****** Object:  Default [DF__Quarterly__Point__37703C52]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__37703C52]  DEFAULT (0) FOR [PointsBonusDB]
GO
/****** Object:  Default [DF_Quarterly_Statement_File_PointsBonusMer]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMer]  DEFAULT (0) FOR [PointsBonusMer]
GO
/****** Object:  Default [DF__Quarterly__Point__3864608B]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3864608B]  DEFAULT (0) FOR [PointsIncreased]
GO
/****** Object:  Default [DF__Quarterly__Point__395884C4]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__395884C4]  DEFAULT (0) FOR [PointsRedeemed]
GO
/****** Object:  Default [DF__Quarterly__Point__3A4CA8FD]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3A4CA8FD]  DEFAULT (0) FOR [PointsReturnedCR]
GO
/****** Object:  Default [DF__Quarterly__Point__3B40CD36]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3B40CD36]  DEFAULT (0) FOR [PointsSubtracted]
GO
/****** Object:  Default [DF__Quarterly__Point__3C34F16F]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3C34F16F]  DEFAULT (0) FOR [PointsReturnedDB]
GO
/****** Object:  Default [DF__Quarterly__Point__3D2915A8]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__3D2915A8]  DEFAULT (0) FOR [PointsDecreased]
GO
/****** Object:  Default [DF__Quarterly__Point__6754599E]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBegin]
GO
/****** Object:  Default [DF__Quarterly__Point__68487DD7]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsEnd]
GO
/****** Object:  Default [DF__Quarterly__Point__693CA210]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsPurchasedCR]
GO
/****** Object:  Default [DF__Quarterly__Point__6A30C649]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBonusCR]
GO
/****** Object:  Default [DF__Quarterly__Point__6B24EA82]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsAdded]
GO
/****** Object:  Default [DF__Quarterly__Point__6C190EBB]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsPurchasedDB]
GO
/****** Object:  Default [DF__Quarterly__Point__6D0D32F4]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsBonusDB]
GO
/****** Object:  Default [DF__Quarterly__Point__6E01572D]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsIncreased]
GO
/****** Object:  Default [DF__Quarterly__Point__6EF57B66]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsRedeemed]
GO
/****** Object:  Default [DF__Quarterly__Point__6FE99F9F]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsReturnedCR]
GO
/****** Object:  Default [DF__Quarterly__Point__70DDC3D8]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsSubtracted]
GO
/****** Object:  Default [DF__Quarterly__Point__71D1E811]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsReturnedDB]
GO
/****** Object:  Default [DF__Quarterly__Point__72C60C4A]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [PointsDecreased]
GO
/****** Object:  Default [DF__Quarterly__Curre__73BA3083]    Script Date: 08/08/2011 09:18:14 ******/
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  DEFAULT (0) FOR [Currentend]
GO
/****** Object:  ForeignKey [FK_Client_PointsExpireFrequency]    Script Date: 08/08/2011 09:18:15 ******/
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_PointsExpireFrequency] FOREIGN KEY([PointsExpireFrequencyCd])
REFERENCES [dbo].[PointsExpireFrequency] ([PointsExpireFrequencyCd])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_PointsExpireFrequency]
GO
