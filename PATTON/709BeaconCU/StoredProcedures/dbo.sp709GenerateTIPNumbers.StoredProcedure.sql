USE [709BeaconCU]
GO
/****** Object:  StoredProcedure [dbo].[sp709GenerateTIPNumbers]    Script Date: 11/17/2010 10:59:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp709GenerateTIPNumbers]
AS 

update customerwork 
set Tipnumber = b.tipnumber
from customerwork a,affiliat b
where a.custid = b.custid

update customerwork 
set Tipnumber = b.tipnumber
from customerwork a,affiliat b
where a.cardnumber = b.acctid

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from customerwork

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 709, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 709000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 709, @newnum

update customerwork 
set Tipnumber = b.tipnumber
from customerwork a,gentip b
where a.custid = b.custid and a. tipnumber is null or a.tipnumber = ' '
GO
