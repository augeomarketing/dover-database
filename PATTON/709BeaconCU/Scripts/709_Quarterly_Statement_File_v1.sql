use [709BeaconCU]
go

/*
   Wednesday, October 30, 20133:32:55 PM
   User: 
   Server: doolittle\rn
   Database: 709BeaconCU
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Quarterly_Statement_File ADD
	ExpPts decimal(18, 0) NULL
GO
ALTER TABLE dbo.Quarterly_Statement_File ADD CONSTRAINT
	DF_Quarterly_Statement_File_ExpPts DEFAULT (0) FOR ExpPts
GO
ALTER TABLE dbo.Quarterly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
