USE [709BeaconCU]
GO
/****** Object:  Table [dbo].[lstnamework]    Script Date: 11/17/2010 11:00:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[lstnamework](
	[tipnumber] [varchar](15) NULL,
	[acctnbr] [varchar](15) NULL,
	[lastname] [char](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
