/*
   Tuesday, May 26, 20154:35:02 PM
   User: 
   Server: doolittle\rn
   Database: 205AltaVista
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Monthly_OptIN ADD
	Member_Number varchar(20) NULL
GO
ALTER TABLE dbo.Monthly_OptIN SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
