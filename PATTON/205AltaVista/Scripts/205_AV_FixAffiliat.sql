-- Script to match new coop data to existing member information
use [205AltaVista]
go

--BEGIN TRAN
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixaffiliat]') AND type in (N'U'))
DROP TABLE [dbo].[fixaffiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fixaffiliat]') AND type in (N'U'))
BEGIN
create table FixAffiliat
 (
 SSN varchar(9),
 Member varchar(30),
 LastName varchar (30),
 Pan varchar(16),
 Tipnumber varchar(15)
 ) ON [PRIMARY]
END
GO

 delete from FixAffiliat
 
---- for Prim DDA not empty
insert into FixAffiliat (SSN, Member, LastName, Pan)
select distinct ssn, isnull(SUBSTRING(LEFT(di.[Prim DDA],(len(di.[Prim DDA])-3)), PATINDEX('%[^0 ]%', LEFT(di.[prim dda],(len(di.[Prim DDA])-3)) + ' '), LEN(LEFT(di.[Prim DDA],(len(di.[prim dda])-3)))), '') as Member, Last, Pan
from COOPWork.dbo.DemographicIn di
WHERE LEFT(pan,6) in ('068484','038282','025077','030389') and [prim dda] is not null and [prim dda] != ''

---- for Prim DDA empty
insert into FixAffiliat (SSN, Member, LastName, Pan)
select distinct ssn, '' as Member, Last, Pan
from COOPWork.dbo.DemographicIn di
WHERE LEFT(pan,6) in ('068484','038282','025077','030389') and ([prim dda] is null or [prim dda] = '')


Update FixAffiliat set Tipnumber = '' /*   RDT set all tipnumbers = '' */

select distinct member, '               ' as tipnumber
into #tmp
from FixAffiliat
where Member<>''
group by Member

--- get tips based on Member
update #tmp
set tipnumber=isnull((select tipnumber from AFFILIAT where ACCTID=[#tmp].member),'')
where member <>''

update FixAffiliat 
set tipnumber= tp.tipnumber 
from FixAffiliat fa inner join [#tmp] tp on fa.member=tp.member

drop table #tmp
go

update fa
set Tipnumber=ff.tipnumber
from Fixaffiliat fa inner join fixaffiliat ff on fa.ssn=ff.ssn
where fa.Tipnumber='' and ff.Tipnumber<>''

 -- Get tipnumber by Acctname
select rtrim(first)+ ' ' + rtrim(Last) as name, di.Pan, '               ' as Tipnumber
into #tmpname
from COOPWork.dbo.DemographicIn di join FixAffiliat fa on di.Pan = fa.Pan 
	where fa.Tipnumber = '' 


update #tmpname
set tipnumber=cu.TIPNUMBER
from #tmpname tp join customer cu on cu.acctname1 = tp.name or cu.ACCTNAME2 = tp.name 

update FA
set fa.Tipnumber=tp.tipnumber
from FixAffiliat fa join #tmpname tp on fa.Pan=tp.pan
WHERE fa.Tipnumber  = '' /* rdt added where clause */

drop table #tmpname
go

-- update tipnumber for those with Member but did not get caught
select distinct member, tipnumber
into #tmp
from FixAffiliat
where tipnumber<>'' and member<>''

update FixAffiliat 
set tipnumber=tp.tipnumber
from FixAffiliat fa inner join [#tmp] tp on fa.Member=tp.member
where fa.tipnumber = '' and fa.member <>'' and tp.tipnumber<>''

drop table #tmp
go

-- make sure that all same ssn only have one tip
select ssn, min(tipnumber) as tipnumber
into #tmp
from FixAffiliat
where Tipnumber<>''
group by SSN


update FixAffiliat
set Tipnumber=tp.tipnumber
from FixAffiliat fa inner join #tmp tp on fa.SSN=tp.ssn

drop table #tmp
go

--insert ssn
INSERT INTO [205AltaVista].[dbo].[AFFILIAT]
           ([ACCTID]
           ,[TIPNUMBER]
           ,[AcctType]
           ,[DATEADDED]
           ,[SECID]
           ,[AcctStatus]
           ,[AcctTypeDesc]
           ,[LastName]
           ,[YTDEarned]
           ,[CustID])
select SSN, TIPNUMBER, 'SSN', GETDATE(), '', 'A', 'SSN RECORD', min(LastName), 0, ''
from FixAffiliat 
where Tipnumber is not null and Tipnumber != '' and SSN not in (select acctid from affiliat) 
group by SSN, Tipnumber

-- insert pan
INSERT INTO [205AltaVista].[dbo].[AFFILIAT]
           ([ACCTID]
           ,[TIPNUMBER]
           ,[AcctType]
           ,[DATEADDED]
           ,[SECID]
           ,[AcctStatus]
           ,[AcctTypeDesc]
           ,[LastName]
           ,[YTDEarned]
           ,[CustID])
select pan, TIPNUMBER, (select Accttype from coopwork.dbo.tipfirstreference where bin=left(fixaffiliat.pan,6)), GETDATE(), '', 'A', (select AccttypeDesc from coopwork.dbo.tipfirstreference where bin=left(fixaffiliat.pan,6)), LastName, 0, ''
from FixAffiliat 
where Tipnumber is not null and Tipnumber != '' and PAN not in (select acctid from affiliat) 
go

--rollback tran










