USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[sp205CurrentMonthActivityLoadStage]    Script Date: 01/11/2010 13:54:22 ******/
SET ANSI_NULLS OFF
GO
DROP PROCEDURE [dbo].[sp205CurrentMonthActivityLoadStage] 
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp205CurrentMonthActivityLoadStage] @EndDateParm varchar(10)
AS

/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 02/23/2010 - set nulls to zero

*/

Declare @EndDate DateTime 						--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 

-- RDT 02/23/2010 delete from Current_Month_Activity
Truncate Table Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints)
select tipnumber, (RunAvaliableNew + RunAvailable)
from Customer_stage



/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history_Stage where histdate>@enddate and ratio='1'
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history_Stage where histdate>@enddate and ratio='1'
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history_Stage where histdate>@enddate and ratio='-1'
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history_Stage where histdate>@enddate and ratio='-1'
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases

-- RDT 02/23/2010 -- START

update Current_Month_Activity
	set adjustedendingpoints = 0 
	where adjustedendingpoints is null 

update Current_Month_Activity
	set	endingpoints = 0 
	where endingpoints is null 

-- RDT 02/23/2010 -- END

GO
