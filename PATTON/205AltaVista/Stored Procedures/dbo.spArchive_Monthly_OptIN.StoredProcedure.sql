USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spArchive_Monthly_OptIN]    Script Date: 05/27/2015 08:21:08 ******/
DROP PROCEDURE [dbo].[spArchive_Monthly_OptIN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 7/2013
-- Description:	Archive Optin records
-- =============================================
CREATE PROCEDURE [dbo].[spArchive_Monthly_OptIN]
	@Enddate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into dbo.Monthly_OptIN_Archive ([Card_Number], [Date_Archived], Member_Number)
	select Card_Number, CAST(@EndDate as Date), Member_Number
	from dbo.Monthly_OptIn
	
	Delete From dbo.Monthly_OptIN
	
END
GO
