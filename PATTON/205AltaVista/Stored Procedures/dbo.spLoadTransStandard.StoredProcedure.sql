USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 01/11/2010 13:54:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard] 
GO
/******************************************************************************/
/*    This exports data from input_Transaction into a table 
      which is then exported to a text file for import into POINTSNOW */
/*  - Create one record for each Column       */
/*  - Lookup TranType             */
/*   -Lookup ratio */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* RDT 07/30/2010 - Removed hard codeing of point factor */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS
/* input */

Declare @MemberNum char(15)
Declare @AccountNum char(15)
Declare @PurchAmt money  
Declare @PurchCnt int
Declare @ReturnAmt money 
Declare @RetunCnt int
Declare @trandate char(10)
Declare @ProductType char(6)
Declare @Tipnumber char(15)
Declare @MemberAcct varchar(25)

/* Output */
Declare @TranAmt int
Declare @TranCode char(2)
Declare @TranCodeSuffix char(1)
Declare @CardType char(20)
Declare @Ratio  numeric

/* Misc */
Declare @Factor  float

/*   - Read input_custTran  */
Declare Input_CustTran_crsr cursor
for Select *
From Input_Transaction

Open Input_CustTran_crsr 

Fetch Input_CustTran_crsr  
into  	 @MemberNum, @AccountNum, @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @trandate, 
	@ProductType , @Tipnumber, @MemberAcct 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 
	If Upper (@ProductType) = 'CREDIT' 
	Begin
--		set @factor = 1 
		set @TranCodeSuffix = '3'
	End
	If Upper (@ProductType) = 'DEBIT' 
	Begin
--		set @factor = .5  
		set @TranCodeSuffix ='7'
	End
	/*  - Create one record for  PURCHASE AMTs     */
	If @PurchAmt > 0 
	  Begin
		-- Choose case for debit or credit
		Set  @TranAmt = cast(( @PurchAmt * @factor ) as int)
		Set  @TranCode = '6'+@TranCodeSuffix 
		Set  @CardType = @ProductType
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)
		Set  @factor = (Select PointFactor from TranCode_Factor where TranCode = @TranCode) -- RDT 07/30/2010 - 
		
		Insert into TransStandard ( 
		TIP ,TranDate  ,AcctNum,TranCode,TranNum,TranAmt,TranType, Ratio, CrdActvlDt)
		Values
		( @TipNumber,	@DateAdded,@MemberAcct, @TranCode, @PurchCnt, @TranAmt, @CardType, @Ratio, @DateAdded   )

	  End
	/*  - Create record for  RETURN AMOUNTs      */
	If @ReturnAmt > 0 
	  Begin
		Set  @TranAmt = cast( (@ReturnAmt * @factor) as int)
		Set  @TranCode = '3' + @TranCodeSuffix 
		Set  @CardType = @ProductType
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)
		Set  @factor = (Select PointFactor from TranCode_Factor where TranCode = @TranCode) -- RDT 07/30/2010 - 

		Insert into TransStandard ( 
		TIP ,TranDate  ,AcctNum,TranCode,TranNum,TranAmt,TranType, Ratio, CrdActvlDt)
		Values
		( @TipNumber,	@DateAdded,@MemberAcct, @TranCode, @PurchCnt, @TranAmt, @CardType, @Ratio, @DateAdded   )
	
	  End
	
	Fetch Input_CustTran_crsr  
	into  	 @MemberNum, @AccountNum, @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @trandate , @ProductType , @Tipnumber, @MemberAcct 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Input_CustTran_crsr
deallocate  Input_CustTran_crsr
GO
