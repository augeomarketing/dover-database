USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewMember]    Script Date: 01/11/2010 13:54:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This Stored Procedure awards Bonuses  to PointsNow Tables */
/*	for members with a date added = date parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType char(2)
-- @CardType char(6) * Not required 

/******************************************************************************/
CREATE PROCEDURE [dbo].[spBonusNewMember]  @DateAdded char(10),  @BonusAmt int, @TranCode Char(2) , @CardType Char(6) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AcctId	char(16)
Declare @BonusDesc char(40)

/* Create View  of all accounts with dateadded = @DateAdded and not in onetimebonuses for trancode */

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewMember]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewMember]

If len(@CardType ) > 1 
	Begin
		set @SQLDynamic = 'create view view_NewMember as 
		Select c.tipnumber from Customer c Join Affiliat a on c.tipnumber = a.tipnumber 
		where c.dateadded = '''+rtrim(convert( char(23), @DateAdded,21 )) +'''' + ' and Upper(a.accttype ) = '''+@CardType+''''
	End
Else
	Begin
		set @SQLDynamic = 'create view view_NewMember as 
		Select tipnumber from Customer 
		where dateadded = '''+rtrim(convert( char(23), @DateAdded,21 )) +''''
	End

print @SQLDynamic
exec sp_executesql @SQLDynamic


set @BonusDesc = (select description from trantype where trancode = @trancode )

-- Cursor thru the view 
Declare View_Crsr Cursor for 
Select tipnumber from View_NewMember

Open View_Crsr

Fetch View_Crsr into @Tipnumber

If @@Fetch_Status = 1 GoTo Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE Customer 
	set RunAvailable = RunAvailable + @BonusAmt, RunBalance=RunBalance + @BonusAmt  
	where tipnumber = @Tipnumber
	
	INSERT INTO history(TipNumber, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
		Values(@Tipnumber, convert(char(10), @DateAdded,101), @TranCode, '1', @BonusAmt, '1', @BonusDesc, '0')
 
	INSERT INTO OneTimeBonuses (Tipnumber, Trancode,  DateAwarded)
		Values (@Tipnumber, @Trancode,  @DateAdded)


	Next_Record:
		Fetch View_Crsr into @Tipnumber

END


Fetch_Error:
close  View_crsr
deallocate  View_crsr

-- Delete View
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewMember]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewMember]
GO
