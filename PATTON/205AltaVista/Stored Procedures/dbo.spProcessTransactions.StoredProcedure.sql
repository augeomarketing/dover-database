USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spProcessTransactions]    Script Date: 01/11/2010 13:54:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DebitTrans For ALTAVISTA          */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessTransactions] @POSTDATE  nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)
--set  @POSTDATE = '11/30/2009'

declare @TipNumber  nvarchar(15)  
declare	@TranDate nvarchar(10)  
declare @AcctId nvarchar(25)  
declare	@TranCode nvarchar(2)  
declare	@TranCount nvarchar(4)  
declare	@TranAmt int 
declare	@CardType nvarchar(20)  
declare @Ratio nvarchar(4)  
declare	@DateLastAct nvarchar(10) 
Declare @STATUS nvarchar(1)
Declare @STATUSDESCRIPTION nvarchar(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @PurOVERAGE int
Declare @PinPurOVERAGE numeric(5)
Declare @YTDEarned int
Declare @SSLAST4 nvarchar(13)
Declare @MAXPOINTSPERYEAR numeric(10)
Declare @afFound nvarchar(1)
declare @Transacctid nvarchar(20)
Declare @LASTNAME char(40)
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @DateAdded dateTIME
set @DateAdded = @POSTDATE
set @RunDate = @POSTDATE



/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare AV_CRSR  cursor 
for Select *
From output_Transaction 

Open AV_CRSR 
/*                  */



Fetch AV_CRSR  
into 
	 @TipNumber, @TranDate, @AcctId, @TranCode, @TranCount, @TranAmt, @CardType, @Ratio, @DateLastAct 

 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error







/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

Begin Transaction PostTrans;

if @Tipnumber is null
or @Tipnumber = ' '
goto Fetch_Next

if @Acctid is null
or @Acctid = ' '
goto Fetch_Next


if not exists (select dbo.Customer_Stage.TIPNUMBER from dbo.Customer_Stage 
                    where dbo.Customer_Stage.TIPNUMBER = @TipNumber)
goto Fetch_Next


/*  UPDATE THE CUSTOMER RECORD WITH THE TRANSACTION DATA          */


           
Update Customer_Stage
Set 
 RunAvaliableNew =   RunAvaliableNew + (@TranAmt * @Ratio)
Where @TipNumber = Customer_Stage.TIPNUMBER

           


    SET @RunAvailableNew = '0' 
	set @afFound = ' '
	SET @YTDEarned = '0'
	set @PurOVERAGE = '0' 
	SET @afFound = ' '
	set @lastname = ' '



	/*  - Check For Affiliat Record       */
 	if not exists (select dbo.Affiliat_Stage.Acctid from dbo.Affiliat_Stage 
                    where dbo.Affiliat_Stage.Acctid = @Acctid)	
	begin
		Insert into AFFILIAT_Stage
                (
	         ACCTID,
	         TipNumber,
	         LastName,                      
                 DateAdded,
                 AcctStatus,
	         YTDEarned,
	         CustID,
	         AcctType,
	         AcctTypeDesc
                )
	        values
 		(
		@acctid,
 		@TipNumber,
		@lastname,
 		@DateAdded,
 		'A',
		@YTDEarned,
 		'0',
		@CardType,
		@CardType
		)
	 
	end


                                   
 	select 
	     @YTDEarned = YTDEarned
	From
	   Affiliat_Stage
	Where
	   acctid = @acctid 

     
/* SELECT THE CLIENT RECORD TO DETERMINE THE MAXIMUM POINT AMOUNT                     */

--	select
--	    @MAXPOINTSPERYEAR = MAXPOINTSPERYEAR
--	From
--	    client
--	where clientcode = '205AltaVista'

	IF @RunAvailableNew is NULL
           SET @RunAvailableNew = '0'
	IF @YTDEarned is null
	   SET @YTDEarned = '0'                                                






/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */
        

  		   Select 
		      @TRANDESC = Description
	           From [RewardsNOW].[dbo].TranType
		   where
		   TranCode = @TRANCODE
		   print 'tran desc'
	           print  @TRANDESC


		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	            ,@acctid
	            ,@RunDate
	            ,@TRANCODE
	            ,@tranCount
	            ,@TranAmt
		    ,@TRANDESC	            
	            ,'NEW'
		    ,@Ratio
	            ,@PurOVERAGE
	            )
	        



	
                if @YTDEarned is null
	           set @YTDEarned = 0



		Update AFFILIAT_stage
		Set 
		     YTDEarned = @YTDEarned + (@TranAmt * @Ratio)
		Where
		     TIPNUMBER = @TIPNUMBER
		and   ACCTID  = @Acctid     
	

	 
 if not exists (select dbo.beginning_balance_table.TIPNUMBER from dbo.beginning_balance_table 
   where dbo.beginning_balance_table.TIPNUMBER = @TipNumber)
  begin
  insert into beginning_balance_table
  ( tipnumber,
    Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 
  )
  values
 (@TIPNUMBER, '0','0','0','0','0','0','0','0','0','0','0','0')
  end



 
        

FETCH_NEXT:


	Fetch AV_CRSR   
	into 
	 @TipNumber, @TranDate, @AcctId, @TranCode, @TranCount, @TranAmt, @CardType, @Ratio, @DateLastAct 

commit Transaction PostTrans;

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'
rollback Transaction PostTrans;

EndPROC:
close  AV_CRSR 
deallocate  AV_CRSR
GO
