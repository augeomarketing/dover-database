USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 01/11/2010 13:54:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTipNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTipNumbers]
GO
/*****************************************/
/* This updates the TIPNUMBER in the input_CustTran table   */
/* First It looks up the input_CustTran Old Account Number in the affiliat */
/* Second it  looks up the input_CustTran  Account Number in the affiliat */
/*****************************************/
/*Author: Rich Tremblay  */
/* Date 1/21/2007  */
/* Revised: 
	Customer.Misc2 is used to store Member Number 
	Client doesn't send a transaction for every Customer 
	so the affiliat table may not have a record for that customer

 5/19/07 Some transactions are sent without a customer records so get missing tipnumbers from customer file 
 RDT 11/20/2009 - Call Sproc to get lasttipnumber
 RDT 10/25/2010 - Get tipnumbers for Member numbers first THEN get tipnumbers for Old member numbers.
 
*/
/*****************************************/

CREATE PROCEDURE [dbo].[spLoadTipNumbers] @TipPrefix char(3) AS

Declare @NewTip bigint

/* Load Existing Tipnumbers to Input_Customer Table */

/* Update Tipnumber where OLD member number  = Customer.Misc2   */
-- RDT 10/25/2010 UPDATE Input_Customer
-- RDT 10/25/2010 	SET 	TIPNUMBER = C.TIPNUMBER
-- RDT 10/25/2010 	FROM 	Customer C Join Input_Customer  I on C.Misc2 = I.OLDMember
-- RDT 10/25/2010 		and I.TIPNUMBER  is NULL

/* Update Tipnumber where member number  = Customer.Misc2   */
UPDATE Input_Customer
	SET 	TIPNUMBER = C.TIPNUMBER
	FROM 	Customer C Join Input_Customer  I on C.Misc2 = I.Membernum
		and I.TIPNUMBER  is NULL

/* Update Tipnumber where OLD member number  = Customer.Misc2   */
UPDATE Input_Customer
	SET 	TIPNUMBER = C.TIPNUMBER
	FROM 	Customer C Join Input_Customer  I on C.Misc2 = I.OLDMember
		and I.TIPNUMBER  is NULL

--  RDT 5/22/07 replace since the member number is in the affiliat table 
/* Update Tipnumber where OLD member number  = characters left of '-' in Affiliat  */
/* UPDATE Input_Customer
	SET 	TIPNUMBER = Affiliat.TIPNUMBER
	FROM 	Affiliat, Input_Customer  
	WHERE left(Affiliat.acctid, charindex('-', acctid )-1 ) = Input_Customer.OLDMember and Upper( Affiliat.Accttype ) <> 'MEMBER'
		and Input_Customer.TIPNUMBER  is NULL
*/
/*Update Tipnumber where OLD member number  = acctid in Affiliat    */
UPDATE Input_Customer
	SET 	TIPNUMBER = Affiliat.TIPNUMBER
	FROM 	Affiliat, Input_Customer  
	WHERE Affiliat.acctid  = Input_Customer.OLDMember and Upper( Affiliat.Accttype ) =  'MEMBER'
		and Input_Customer.TIPNUMBER  is NULL

-- RDT 5/22/07 replace since the member number is in the affiliat table 
/* Update Tipnumber where Member number  = characters left of '-' in Affiliat  */
/* 
UPDATE Input_Customer
	SET 	TIPNUMBER = Affiliat.TIPNUMBER
	FROM 	Affiliat, Input_Customer  
	WHERE left(Affiliat.acctid, charindex('-', acctid )-1 ) = Input_Customer.MemberNum
		and Input_Customer.TIPNUMBER  is NULL

*/
/* Update Tipnumber where Member number  = acctid in Affiliat  */
UPDATE Input_Customer
	SET 	TIPNUMBER = Affiliat.TIPNUMBER
	FROM 	Affiliat, Input_Customer  
	WHERE Affiliat.acctid  = Input_Customer.MemberNum and Upper( Affiliat.Accttype ) =  'MEMBER'
		and Input_Customer.TIPNUMBER  is NULL

-- RDT 11/20/2009 -- set @NewTip = ( select LastTipNumberUsed  from Client )
exec rewardsnow.dbo.spGetLastTipNumberUsed @TipPrefix, @NewTip output

-- SELECT @NewTip = max(TIPNUMBER) from Affiliat
If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'

set @NewTip = @NewTip + 1
/*  DECLARE CURSOR FOR PROCESSING CustTranInput TABLE                         */

declare tip_crsr cursor
for select TipNumber
from Input_Customer
WHERE (TipNumber is null or len(rtrim(TipNumber)) = 0)
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		update Input_Customer
		set TIPNUMBER =  convert(char(15), @NewTip)
		where current of tip_crsr
		set @NewTip = @NewTip + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

-- Update LastTip
-- RDT 11/20/2009 -- Update Client set LastTipNumberUsed = @NewTip 
exec rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip 


/* Load Tipnumbers to Input_Transaction from Input_customer Table */
UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = Input_Customer.TIPNUMBER
FROM Input_Transaction, Input_Customer  WHERE Input_Transaction.MemberNum = Input_Customer.MemberNum
and Input_Transaction.TIPNUMBER  is NULL

/* Get tipnumbers from customer file */
UPDATE Input_Transaction
	SET 	TIPNUMBER = C.TIPNUMBER
	FROM 	Customer C Join Input_Transaction I on C.Misc2 = I.MemberNum
		and I.TIPNUMBER  is NULL
GO
