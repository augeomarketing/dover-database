USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeProd]    Script Date: 01/11/2010 13:54:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPurgeProd] @DateInput as datetime AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/* Modified 11/2007 BJQ now Points to the Stage Files  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
/*  **************************************  */
--declare @DateInput as datetime
--set @dateinput = '2009-03-31 00:00:00:000'
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat set AcctStatus = 'A' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer  set Status = 'A' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat 	
	set Acctstatus = '9'
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat .acctid = AccountDeleteInput.acctid )
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat  
	 WHERE  affiliat .acctstatus = '9'
	
	/*  **************************************  */
	Delete from affiliat  where acctstatus = '9'
	
	/*  **************************************  */
	-- Find Customers without accounts and mark for delete 
	update Customer 
	set Status='9'
	where tipnumber not in 
		(select  distinct(tipnumber)  from affiliat  )
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,' ',RunAvaliableNew, @DateDeleted 
	from customer  WHERE  status = '9'


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history  
	where tipnumber in 
	(select tipnumber from customer  where status = '9')
	/*  **************************************  */
	-- Delete History
	Delete from history  where tipnumber in (select tipnumber from customer  where status = '9')
	
	/*  **************************************  */
	-- Delete Customer
	Delete from customer  where status = '9'
	/* Return 0 */
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
