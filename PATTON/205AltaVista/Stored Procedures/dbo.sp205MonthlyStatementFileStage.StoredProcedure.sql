USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[sp205MonthlyStatementFileStage]    Script Date: 06/26/2013 14:36:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp205MonthlyStatementFileStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp205MonthlyStatementFileStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp205MonthlyStatementFileStage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[sp205MonthlyStatementFileStage] @StartDateParm char(10), @EndDateParm char(10)
AS 
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- 2/2008 added HELOC codes

-- RDT 1/24/2011 Added XF to increase 
-- RDT 1/24/2011 added Merchant Funded Codes (F% G%) to bonus
-- RDT 1/24/2011 Changed old Update (exists) to use Join (Yaaay!)
Jira: BNW-27
*/
-- S Blanchette  7/2013  Changed to feed Misc 1 (SSN) to SSN and Misc2 (Member) to Member on Statement audit file JIRA AVCU-20

/*******************************************************************************/
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)
Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 		--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + '' 00:00:00:001'')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )			--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
--RDT 02/23/2010 -- delete from Monthly_Statement_File
truncate table Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
--RDT 02/23/2010 -- from CUSTOMER
from CUSTOMER_Stage


/* Load the statmement file with CREDIT purchases          */
update msf
	set PointsPurchasedCR  = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and trancode = ''63''
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber


/* Load the statmement file  with CREDIT returns            */
update msf
	set PointsReturnedCR = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and trancode = ''33''
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber
     
/* Load the statmement file with DEBIT purchases          */
update msf
	set pointspurchasedDB = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and trancode = ''67''
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file with DEBIT  returns            */
update msf
	set pointsreturnedDB = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and trancode = ''37''
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber
     
/* Load the statmement file with HELOC purchases          */
update msf
	set pointspurchasedH = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and trancode = ''6H''
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file  with HELOC returns            */
update msf
	set pointsreturnedH = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and trancode = ''3H''
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file with bonuses            */
print ''bonus''
update msf
	set pointsbonus = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and (trancode like ''B%'' or trancode like ''F%'' or trancode like ''G%'')
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file with plus adjustments    */
update msf
	set pointsadded = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and (trancode = ''DR'' or trancode = ''IE'' or TRANCODE = ''XF'' )
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointspurchasedH

/* Load the statmement file with redemptions          */
update msf
	set pointsredeemed = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and (trancode like ''R%'')
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file with minus adjustments    */
/* Add EP to  minus adjustments    */
update msf
	set pointssubtracted = hs.points
	from dbo.monthly_statement_file msf  join 
	(select tipnumber, sum(points ) points 
		from dbo.history_stage
			where histdate >= @startdate and histdate <= @enddate
				and (trancode = ''EP'' or TRANCODE = ''DE'' or TRANCODE = ''XP'' )
		group by tipnumber) hs
     on msf.tipnumber = hs.tipnumber

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + pointsreturnedH

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N''update Monthly_Statement_File
set pointsbegin=(select monthbeg''+ @MonthBegin + N'' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)''

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the monthly statement table */
Update Monthly_Statement_file 
set SSN= Misc1, Member=Misc2 from Customer_Stage a where Monthly_Statement_file.tipnumber = a.tipnumber' 
END
GO
