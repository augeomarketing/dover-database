USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spBackupDatabase]    Script Date: 01/11/2010 13:54:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  backup database */
CREATE Procedure [dbo].[spBackupDatabase]
as

declare @spath varchar(100), @sDBNameLog varchar(100), @sDBName varchar(100), @sBackupPath varchar(100), @SQLexec nvarchar(100)

set @sPath = 'd:\SQLData\BACKUPS\'
set @sDBName = '205AltaVista'
set @sDBNameLog = '205AltaVista_log'

/*  Shrink log */
/* Cant get this to work yet. 
set @SQLexec = 'BACKUP LOG ' + @sDBName + ' WITH TRUNCATE_ONLY'
exec sp_executesql @SQLexec 
set @SQLexec = 'USE ' + @sDBName
exec sp_executesql @SQLexec 
set @SQLexec = 'DBCC SHRINKFILE ( ' + @sDBNameLog + ', 200, TRUNCATEONLY)'
exec sp_executesql @SQLexec 
*/

/*  backup database */
-- Clear out backups in this folder
-- Uncomment if you like (clears out databases that have since been removed)
-- However this may defeat the purpose of this script, since the creation dates will have changed on all files
-- set @cleardbs = 'del "' + @sPath + '*.bak"'
-- exec master..xp_cmdshell @cleardbs
Set @sBackupPath = @sPath + @sDBName
Backup database @sDBName to disk = @sBackupPath with init
GO
