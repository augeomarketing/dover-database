USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spLoadStageTables]    Script Date: 01/11/2010 13:54:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from customer affiliat and history to *_Stage tables.   */
/*  Tables:  
	Customer 	 - select 
	Affiliat		- select
--	History		- select
	Customer_Stage - Truncate, Insert 
--	Affiliat_Stage	- Truncate, Insert 
	History_Stage	- Truncate, Insert  */
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadStageTables] @MonthEnd char(20)  AS 

Declare  @DD char(2), @MM char(2), @YYYY char(4), @MonthBeg DateTime

set @DD = '01'
set @MM = Month(@MonthEnd)
set @YYYY = Year(@MonthEnd)
set @MonthBeg = convert(datetime, @MM+'/'+@DD+'/'+@YYYY+' 00:00:00:000' )	


Truncate table Customer_Stage
Truncate table Affiliat_Stage
Truncate table History_Stage

Insert into Customer_Stage Select * from Customer

Insert Into Affiliat_Stage Select * from Affiliat

Insert Into History_stage Select * from History where Histdate > @MonthBeg

Insert Into OnetimeBonusesStage Select * from OnetimeBonuses
GO
