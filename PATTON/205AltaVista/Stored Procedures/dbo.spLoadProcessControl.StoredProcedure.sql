USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spLoadProcessControl]    Script Date: 01/11/2010 13:54:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadProcessControl] @MonthEnd char(12) AS

Declare @MonthBegin char(12) 

Set @MonthBegin = Convert( varchar(4), year( @monthEnd ) ) +'/' + convert( Varchar(2), month( @monthEnd )) + '/01'

Insert into aux_ProcessControl 
( Rundate , MonthBegin, MonthEnd )
values
( GetDate() , @MonthBegin, @MonthEnd)
GO
