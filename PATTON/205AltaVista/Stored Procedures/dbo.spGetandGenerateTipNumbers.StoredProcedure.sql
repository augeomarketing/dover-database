USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spGetandGenerateTipNumbers]    Script Date: 06/26/2013 14:36:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetandGenerateTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spGetandGenerateTipNumbers] @ParmTipFirst nvarchar(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS                     */
/*                                                                            */
/******************************************************************************/
declare @LastTipUsed char(15), @newtipnumber bigint

-- Set Tipnumber to null
update Demographicin
set tipnumber = null

-- Get existing based on Pan
update DemographicIn
set TipNumber= af.TIPNUMBER
from DemographicIn di join AFFILIAT_Stage af on di.Pan=af.ACCTID
where di.TipNumber is null 

-- Get existing based on SSN
update DemographicIn
set TipNumber= af.TIPNUMBER
from DemographicIn di join AFFILIAT_Stage af on di.ssn=af.ACCTID
where di.TipNumber is null and af.AcctType=''SSN''

-- Generate New Tips House Hold by SSN
select distinct ssn, ''               '' as Tipnumber
into #tmpTable
from DemographicIn
where TipNumber is null 

If (select COUNT(*) from #tmpTable)> 0 
Begin
	exec rewardsnow.dbo.spGetLastTipNumberUsed @ParmTipFirst, @LastTipUsed output
	select @LastTipUsed as LastTipUsed

	set @newtipnumber = cast(@LastTipUsed as bigint) + 1 

	update #tmpTable
	set Tipnumber = @newtipnumber, @newtipnumber = @newtipnumber + 1

	exec RewardsNOW.dbo.spPutLastTipNumberUsed @ParmTipFirst, @newtipnumber  

	update DemographicIn
	set TipNumber= tp.TIPNUMBER
	from DemographicIn di join #tmpTable tp on di.ssn=tp.ssn
	where di.TipNumber is null 
End
' 
END
GO
