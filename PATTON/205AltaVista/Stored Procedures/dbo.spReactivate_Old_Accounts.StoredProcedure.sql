USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spReactivate_Old_Accounts]    Script Date: 06/26/2013 14:36:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spReactivate_Old_Accounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spReactivate_Old_Accounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spReactivate_Old_Accounts]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spReactivate_Old_Accounts]
	@DateParm varchar(10)
AS

Declare @CheckDate DateTime 	--RDT 10/09/2006 
set @CheckDate = convert(datetime, @DateParm + '' 00:00:00:001'')	--RDT 10/09/2006 



BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update AFFILIAT_Stage
	set AcctStatus = ''A''
	where AcctStatus = ''C'' 
		and TIPNUMBER not in (select TIPNUMBER from demographicin)
		and AcctType in (''Debit'',''Credit'', ''Member'') 
		and LEN(acctid)<16
		and (TIPNUMBER in (select TIPNUMBER from CUSTOMER where RunAvailable<>0 and RUNBALANCE<>0)
				or TIPNUMBER in (select TIPNUMBER from CUSTOMER where RunAvailable=0 and RUNBALANCE=0 and DATEADDED>=@CheckDate))

	update CUSTOMER_Stage
	set STATUS = ''A''
	where Status =''C'' 
		and tipnumber in (select tipnumber from AFFILIAT_Stage where AcctStatus=''A'')

	delete from customer_closed
	where tipnumber in (select tipnumber from CUSTOMER_Stage where Status=''A'')
		
END
' 
END
GO
