USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 06/26/2013 14:36:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeClosedCustomers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- This will delete all customers with a status of ''C''losed OR ''H''ot card.
--based on Customer_Closed.DateToDelete
/******************************************************************************/
/************************************************************/
/* S Blanchette                                             */
/* 12/2010                                                  */
/* Remove tips from account reference if not in customer    */
/* SEB001                                                   */
/*                                                          */
/************************************************************/

CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @EndDate char(10), @DBNumber char(3) 
AS

Declare @SQLDynamic nvarchar(max)
Declare @SQLTruncate nvarchar(max)
Declare @SQLInsert nvarchar(max)
Declare @SQLUpdate nvarchar(max)
Declare @SQLDelete nvarchar(max)
Declare @Tipnumber 	char(15)
Declare @MonthsToHold int
declare @EndDateDatetime char(10) 
declare @DatetoPurge datetime 
Declare @dbName varchar(100) 
/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @DBNumber )

---Not for why not using Pending Purge) 
---PUT NOTE about spLoadCustomerAndAffiliat upping CustClosed.DateTodelete by one month
----------- Stage Table Processing ----------
If @Production_Flag <> ''P''
----------- Stage Table Processing ----------
Begin
	
--delete unless the tip has had transactions affter the datedeleted OR it''s a request for brochure
	set @SQLDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.Customer_stage 	where TipNumber in (Select Tipnumber from '' + QuoteName(@DBName) + N''.dbo.customer_closed where DateToDelete <=@EndDate )
						AND tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.history where Histdate>=@EndDate AND Trancode<>''''RQ'''') '' 
	exec sp_executesql @SQLDelete, N''@EndDate char(10)'', @EndDate=@EndDate
								 	
	set @SQLDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage 	where TipNumber not in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.Customer_Stage) ''
	exec sp_executesql @SQLDelete

	set @SQLDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.History_stage 	where TipNumber not in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.Customer_Stage) ''
	exec sp_executesql @SQLDelete


End
----------- Production Table Processing ----------
If @Production_Flag = ''P''
----------- Production Table Processing ----------
Begin
	Begin Tran 
	-- flag all Undeleted Customers "A" that have an input_purge_pending record 
	--Update customer set status = ''A'' 
	--	where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)
	-- Insert customer to customerdeleted 
	set @SQLInsert = N''Insert Into '' + QuoteName(@DBName) + N''.dbo.CustomerDeleted            ([TIPNumber]
						   ,[TIPFirst],[TIPLast],[AcctName1],[AcctName2],[AcctName3],[AcctName4]
						   ,[AcctName5],[AcctName6],[Address1],[Address2],[Address3],[Address4]
						   ,[City],[State],[Zipcode],[LastName],[Status],[StatusDescription],[HomePhone]
						   ,[WorkPhone],[RunBalance],[RunRedeemed],[RunAvailable],[LastStmtDate]
						   ,[NextStmtDate],[DateAdded],[Notes],[ComboStmt],[RewardsOnline],[EmployeeFlag]
						   ,[BusinessFlag],[SegmentCode],[Misc1],[Misc2],[Misc3],[Misc4],[Misc5]
						   ,[RunBalanceNew],[RunAvaliableNew],[DateDeleted])
					Select  c.TIPNUMBER, c.TIPFIRST, c.TIPLAST, c.ACCTNAME1, c.ACCTNAME2, c.ACCTNAME3, c.ACCTNAME4
							, c.ACCTNAME5, c.ACCTNAME6, c.ADDRESS1, c.ADDRESS2, c.ADDRESS3, c.ADDRESS4
							, c.City, c.State, c.ZipCode, c.lastname, c.STATUS, c.StatusDescription, c.HOMEPHONE
							, c.WORKPHONE, c.RUNBALANCE, c.RunRedeemed, c.RunAvailable, c.LastStmtDate
							, c.NextStmtDate, c.DATEADDED, c.NOTES, c.ComboStmt, c.RewardsOnline, c.EmployeeFlag
							, c.BusinessFlag, c.SegmentCode, c.Misc1, c.Misc2, c.Misc3, c.Misc4, c.Misc5
							, c.RunBalanceNew, c.RunAvaliableNew , @EndDate as Datedeleted      
					From '' + QuoteName(@DBName) + N''.dbo.Customer c 
					Where Tipnumber  in (select Tipnumber from '' + QuoteName(@DBName) + N''.dbo.Customer_Closed  where DateToDelete<=@EndDate) 
							AND tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.history where Histdate>=@EndDate AND Trancode<>''''RQ'''') '' 
	exec sp_executesql @SQLInsert, N''@EndDate char(10)'', @EndDate=@EndDate

	-- Delete from customer 
	set @SQLDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.Customer
		Where Tipnumber  in (select Tipnumber from '' + QuoteName(@DBName) + N''.dbo.Customer_Closed  where DateToDelete<=@EndDate) 
				AND tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.history where Histdate>=@EndDate AND Trancode<>''''RQ'''') '' 
	exec sp_executesql @SQLDelete, N''@EndDate char(10)'', @EndDate=@EndDate

	-- Insert affiliat to affiliatdeleted 
	set @SQLInsert = N''Insert Into '' + QuoteName(@DBName) + N''.dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
		 LastName, YTDEarned, CustId, DateDeleted )
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
			   LastName, YTDEarned, CustId, @EndDate 
		From '' + QuoteName(@DBName) + N''.dbo.Affiliat  
		Where Tipnumber not in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.Customer) ''
	exec sp_executesql @SQLInsert, N''@EndDate char(10)'', @EndDate=@EndDate

	-- copy history to historyDeleted 
	set @SQLInsert = N''Insert Into '' + QuoteName(@DBName) + N''.dbo.HistoryDeleted 
		Select TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, @EndDate as DateDeleted 
		From '' + QuoteName(@DBName) + N''.dbo.History H 
		Where Tipnumber not in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.Customer) ''
	exec sp_executesql @SQLInsert, N''@EndDate char(10)'', @EndDate=@EndDate

	-- Delete records from History 
	set @SQLDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.History 
		Where Tipnumber not in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.Customer) ''
	exec sp_executesql @SQLDelete, N''@EndDate char(10)'', @EndDate=@EndDate

	-- Delete records from affiliat 
	set @SQLDelete = N''Delete from '' + QuoteName(@DBName) + N''.dbo.Affiliat   
		Where Tipnumber not in (select TipNumber from '' + QuoteName(@DBName) + N''.dbo.Customer) ''
	exec sp_executesql @SQLDelete, N''@EndDate char(10)'', @EndDate=@EndDate


	Commit Transaction 
End


' 
END
GO
