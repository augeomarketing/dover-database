USE [205AltaVista]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadOutputTransaction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadOutputTransaction]
GO

/******************************************************************************/
/*    This exports data from input_Transaction into a table 
      which is then exported to a text file for import into POINTSNOW */
/*  - Create one record for each Column       */
/*  - Lookup TranType             */
/*   -Lookup ratio */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* 02/28/2008 RDT	Added HELOC transactions */
-- RDT 10/20/2009 Double points for Oct 2009
-- RDT 10/22/2009 DO NOT double points for Debit or HELOC
-- RDT 11/20/2009 Remove Double Points for Credit
-- RDT 10/01/2011 Change Debit earning from $2 = 1 point to $3 = 1 point 
/******************************************************************************/
Create PROCEDURE [dbo].[spLoadOutputTransaction] @DateAdded char(10) AS
/* input */

Declare @MemberNum char(15)
Declare @AccountNum char(15)
Declare @PurchAmt money  
Declare @PurchCnt int
Declare @ReturnAmt money 
Declare @RetunCnt int
Declare @trandate char(10)
Declare @ProductType char(6)
Declare @Tipnumber char(15)
Declare @MemberAcct varchar(25)

/* Output */
Declare @TranAmt int
Declare @TranCode char(2)
Declare @TranCodeSuffix char(1)
Declare @CardType char(20)
Declare @Ratio  numeric

/* Misc */
Declare @Factor  float

/*   - Read input_custTran  */
Declare Input_CustTran_crsr cursor
for Select *
From Input_Transaction

Open Input_CustTran_crsr 

Fetch Input_CustTran_crsr  
into  	 @MemberNum, @AccountNum, @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @trandate , @ProductType , @Tipnumber, @MemberAcct 

IF @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 
	If Upper (@ProductType) = 'CREDIT' 
	Begin
	-- RDT 10/20/2009 set @factor = 1 
	-- RDT 11/20/2009 set @factor = 2
		set @factor = 1
		set @TranCodeSuffix = '3'
	End
	If Upper (@ProductType) = 'DEBIT' 
	Begin
		-- set @factor = .5 -- RDT 10/01/2011 
			set @factor = .33 -- RDT 10/01/2011 
		set @TranCodeSuffix ='7'
	End
	If Upper (@ProductType) = 'HELOC' 
	Begin
		-- RDT 10/20/2009 set @factor = 2
		-- RDT 10/22/2009 set @factor = 4
		set @factor = 2
		set @TranCodeSuffix ='H'
	End
	/*  - Create one record for  PURCHASE AMTs     */
	If @PurchAmt > 0 
	  Begin
		-- Choose case for debit or credit
		Set  @TranAmt = cast(( @PurchAmt * @factor ) as int)
		Set  @TranCode = '6'+@TranCodeSuffix 
		Set  @CardType = @ProductType
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)

		Insert into OutPut_Transaction ( 
		TipNumber ,TranDate  ,AcctId  ,TranCode  ,TranCount  ,TranAmt  ,	CardType , 	Ratio   )
		Values
		( @TipNumber ,	@DateAdded  ,	@MemberAcct , @TranCode  , @PurchCnt  ,@TranAmt ,@CardType , 	@Ratio   )

	  End
	/*  - Create record for  RETURN AMOUNTs      */
	If @ReturnAmt > 0 
	  Begin
		Set  @TranAmt = cast( (@ReturnAmt * @factor) as int)
		Set  @TranCode = '3' + @TranCodeSuffix 
		Set  @CardType = @ProductType
		Set  @Ratio = (Select  Ratio  from TranType where TranCode = @TranCode)

		Insert into OutPut_Transaction ( 
		TipNumber ,TranDate  ,AcctId  ,TranCode  ,TranCount  ,TranAmt  ,	CardType , 	Ratio   )
		Values
		( @TipNumber ,	@DateAdded  ,	@MemberAcct  , @TranCode  , @RetunCnt  ,@TranAmt ,@CardType , 	@Ratio   )
	
	  End
	
	Fetch Input_CustTran_crsr  
	into  	 @MemberNum, @AccountNum, @PurchAmt, @PurchCnt, @ReturnAmt, @RetunCnt, @trandate , @ProductType , @Tipnumber, @MemberAcct 

END /*while */

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Input_CustTran_crsr
deallocate  Input_CustTran_crsr
GO
