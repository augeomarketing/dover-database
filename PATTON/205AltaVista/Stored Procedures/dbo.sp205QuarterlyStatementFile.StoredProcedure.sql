USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[sp205QuarterlyStatementFile]    Script Date: 01/11/2010 13:54:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp205QuarterlyStatementFile] @StartDateParm char(10), @EndDateParm char(10)
AS 
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/
Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode)
from customer

/* Load the statmement file with CR purchases          */
update Quarterly_Statement_File
set pointspurchasedCR =(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='63')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='63')

/* Load the statmement file CR returns            */
update Quarterly_Statement_File
set pointsreturnedCR=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='33')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='33')

/* Load the statmement file with DB purchases          */
update Quarterly_Statement_File
set pointspurchasedDB=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='67')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='67')

/* Load the statmement file DB returns            */
update Quarterly_Statement_File
set pointsreturnedDB=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='37')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='37')

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like'B%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like 'B%')


/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='IE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
update Quarterly_Statement_File
set pointsadded=pointsadded + (select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DR')


/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded 


/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like 'R%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode like 'R%')

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='DE')

/* Add EP to  minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='EP')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate between @StartDate and @EndDate and trancode='EP')


/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Quarterly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased
GO
