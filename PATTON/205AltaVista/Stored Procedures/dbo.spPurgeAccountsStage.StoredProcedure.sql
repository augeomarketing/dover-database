USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeAccountsStage]    Script Date: 01/11/2010 13:54:25 ******/
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeAccountsStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeAccountsStage]
GO 


create PROCEDURE [dbo].[spPurgeAccountsStage] @DateInput as datetime AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
--
/* THIS DELETES CUSTOMERS BY MEMBER NUMBER NOT BY ACCOUNT */ 
--
/*  Delete from AccountDeleteInput */
/*  USE THE MEMBER NUMBER NOT THE ACCOUNT NUMBER */ 
/*  Delete Account */
/*  Delete History */
/*  Delete Customer*/
/*  USE THE MEMBER NUMBER NOT THE ACCOUNT NUMBER */ 
/* RDT 5/19/2010 
	Changed updates and deleted to reference stage tables instead of the production tables. */
/*  **************************************  */
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	update accountdeleteinput  
		set accountdeleteinput.tipnumber = aff.tipnumber
	from dbo.AFFILIAT_Stage as aff						-- RDT 5/19/2010 
		inner JOIN dbo.accountdeleteinput as adi
		on aff.acctid = adi.acctid

	update accountdeleteinput  
		set accountdeleteinput.tipnumber = aff.tipnumber
	from dbo.AFFILIAT_Stage as aff						-- RDT 5/19/2010 
		inner JOIN dbo.accountdeleteinput as adi
		on aff.custid = adi.dda
	
	/*  **************************************  */
--- RDT 05/19/2010 The following Insert was commented out for the stage process. 
/*	
	-- Copy deleted accounts to AffiliatedDeleted where custid = dda
	INSERT INTO AffiliatDeleted 
		(TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
		 YTDEarned,CustID,DateDeleted )
	SELECT 
		 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
		 YTDEarned,CustID,  @DateDeleted
	FROM Affiliat where  exists 
		( select acctid from AccountDeleteInput 
		  where  Affiliat.custid = AccountDeleteInput.dda )

*/	
	/*  **************************************  */

	--- RDT 05/19/2010 Delete from Affiliat where  exists 
	Delete from AFFILIAT_Stage  where  exists				--- RDT 05/19/2010 
		( select acctid from AccountDeleteInput 
		  where  AFFILIAT_Stage.custid = AccountDeleteInput.dda )
	
	/*  **************************************  */
--- RDT 05/19/2010 The following Insert was commented out for the stage process. 
/*	
	----Copy  Customers with Tipnumbers in affailiatDeleted to Customerdeleted 
	INSERT INTO [CustomerDeleted]
		(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
		DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
		ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
		WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
		Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
		TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
		DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
		ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
		WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
		Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @DateDeleted 
	from customer WHERE  	tipnumber  in  (select  distinct(tipnumber)  from AffiliatDeleted )
*/
	/*  **************************************  */
	-- Delete Customer with Tipnumbers in affailiatDeleted 
	/*	Delete from CUSTOMER
	where tipnumber  in (select  distinct(tipnumber)  from AffiliatDeleted )
	*/
--- RDT 05/19/2010 The following was added to replace the code above. 
	Delete from CUSTOMER_Stage 
		where Misc2 in ( select dda from AccountDeleteInput )


	/*  **************************************  */
	-- Copy History records with Tipnumbers in affailiatDeleted to HistoryDeleted 
--- RDT 05/19/2010 The following Insert was commented out for the stage process. 
/*	
	INSERT INTO HistoryDeleted
		([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
		[POINTS], [Description], [SECID], [Ratio], [DateDeleted])
	select 
		[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
		[POINTS], [Description], [SECID], [Ratio], @DateDeleted
	from history 
	where	tipnumber  in 	(select  distinct(tipnumber)  from AffiliatDeleted )
*/

	/*  **************************************  */
	--- RDT 05/19/2010 Delete from history 
	Delete from HISTORY_Stage 
	where tipnumber  Not in (select  TIPNUMBER from CUSTOMER_Stage )
	
	/* Return 0 */
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
