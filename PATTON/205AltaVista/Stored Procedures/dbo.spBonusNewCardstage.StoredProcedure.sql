USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewCardstage]    Script Date: 01/11/2010 13:54:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    This Stored Procedure awards Bonuses   */

/*	New Platinum Credit card: Points on 1st use
	New check (debit) card: points  on 1st use
 */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* Cloned by B. Quinn for use with stage tables */
-- Parms. 
-- @DateAdded char(10), 
-- @CardType Char(6), 
-- @BonusAmt int, 
-- @TranType 
-- IF HELOC check date First Use bonus is ONLY good from 2/1/08 thru 8/31/08
/******************************************************************************/
create PROCEDURE [dbo].[spBonusNewCardstage]  @DateAdded char(10), @CardType Char(6), @BonusAmt int, @TranCode Char(2) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AcctId	char(16)

If @TranCode = 'FH' and @DateAdded NOT Between '02/01/2008' and '08/31/2008' 
	Begin
		Return 1
	End


/* Create View  of all accounts with dateadded = @DateAdded and not in onetimebonuses for trancode */
Set @CardType = UPPER(@CardType)
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]

set @SQLDynamic = 'create view view_NewCard as 
Select tipnumber, acctid from affiliat 
where dateadded = '''+rtrim(convert( char(23), @DateAdded,21 )) +
''' and uPPER(AcctType) = '''+ @CardType + ''' and acctid not in 
(select acctid from onetimebonuses where trancode = '''+@TranCode+''' )'

print @SQLDynamic
exec sp_executesql @SQLDynamic

-- Cursor thru the view 
Declare View_Crsr Cursor for 
Select tipnumber, acctid from View_NewCard

Open View_Crsr

Fetch View_Crsr into @Tipnumber, @Acctid

If @@Fetch_Status = 1 GoTo Fetch_Error

WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE Customer_stage 
	set RunAvaliableNew = RunAvaliableNew + @BonusAmt
	--, RunBalance=RunBalance + @BonusAmt  
	where tipnumber = @Tipnumber
	
	INSERT INTO history_stage(TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
		Values(@Tipnumber, @Acctid, convert(char(10), @DateAdded,101), @TranCode, '1', @BonusAmt, '1', 'Bonus Activation', '0')
 
	INSERT INTO OneTimeBonuses_stage (Tipnumber, Trancode, Acctid, DateAwarded)
		Values (@Tipnumber, @Trancode, @Acctid, @DateAdded)


	Next_Record:
		Fetch View_Crsr into @Tipnumber, @Acctid
END


Fetch_Error:
close  View_crsr
deallocate  View_crsr

-- Delete View
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[view_NewCard]') and OBJECTPROPERTY(id, N'IsView') = 1) drop view [dbo].[view_NewCard]
GO
