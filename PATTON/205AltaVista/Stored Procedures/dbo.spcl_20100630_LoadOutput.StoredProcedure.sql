USE [205AltaVista]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spcl_20100630_LoadOutput]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spcl_20100630_LoadOutput]
GO

/******************************************************************************/
/* BY:  R.Tremblay  */
/* This procedure does the following:
	1. Find all history reocrds for members in ajustment table.
	2. Calculate the total earned points for the member 
	3. Create a DE transacation for total points.
	4. Add DE to Output_transaction. 
*/

/******************************************************************************/

Create PROCEDURE [dbo].[spcl_20100630_LoadOutput] AS

--	1. Find all history reocrds for members in ajustment table.
--	2. Calculate the total earned points for the member-account 
--	3. Create a DE transacation for total points.

Create table #temp_hist ( Tipnumber char(15) , histearned int, MemAcct char(16), CardType char(20), Trancode char(2) )

Insert into #temp_hist 
Select h.TIPNUMBER, SUM(points*ratio) as histearned, h.ACCTID, i.ProductType , 'DE'  from HISTORY h join input_transaction_20100630 i 
on h.ACCTID = i.MemberAcct
where h.HISTDATE > '2009/12/31 23:59:59'
	and h.TRANCODE in ( '63','67','33','37')
group by h.TIPNUMBER , h.ACCTID , i.ProductType

--	4. Add DE to Output_transaction. 
insert into output_Transaction 
(TipNumber, TranDate, AcctId, TranCode, TranCount, TranAmt, CardType, Ratio )
select Tipnumber, '2010/05/31', MemAcct, Trancode, 1, histearned,  CardType, -1 from #temp_hist 

drop table #temp_hist 
 