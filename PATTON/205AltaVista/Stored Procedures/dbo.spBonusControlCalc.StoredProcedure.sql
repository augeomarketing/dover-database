SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusControlCalc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusControlCalc] 
GO

-- =============================================
-- Author:		Rich T
-- Create date: 07/28/2010
-- Description:	Calculate Bonus for new cards
-- =============================================
CREATE PROCEDURE spBonusControlCalc @MonthDate Char(10) 
AS
BEGIN
-- read the BonusControl Table calculate 
-- retrieve cards that qualify for bonus
-- insert bonus transactions into the input_transaction table. 

Declare @MinDate		Char(10) 
Declare @IncrementType	Char(2)
Declare @IncrementValue	char(3)
Declare @Product		Char(6)
Declare @ProductTrancode Char(2)
Declare @BonusTrancode	Char(2)
Declare @BonusAmount	int
Declare @BonusFactor	Float
Declare @SQLcmd			nVarChar(2000)
Declare @RowsUpdated	int

Declare csrBonus Cursor Fast_Forward
	 For Select 
		dim_BonusControl_IncrementType, 
		dim_BonusControl_IncrementValue, 
		dim_BonusControl_Product, 
		dim_BonusControl_ProductTrancode, 
		dim_BonusControl_BonusTrancode, 
		dim_BonusControl_BonusAmount, 
		dim_BonusControl_BonusFactor

	from BonusControl where BonusControl.dim_BonusControl_EffectiveDate <= @MonthDate 


--- retrieve all records with Effective >= current date
Truncate Table tmpBonus 

Open csrBonus 

 
Fetch Next From csrBonus 
	Into @IncrementType, @IncrementValue, @Product, @ProductTrancode, 
		@BonusTrancode, @BonusAmount, @BonusFactor	
	
WHILE @@FETCH_STATUS = 0
BEGIN 

	--- Load the Temp table with accounts from Affiliat_Stage where the 
	--- Affiliat.dateAdded is less than or equal to the calculated_date from the bonus control table
	--- Calculated_date = DateAdded + dim_BonusControl_IncrementType (MM) + dim_BonusControl_IncrementValue (2)
	set @SQLcmd			  = 'Insert into tmpBonus (dim_tmpBonusAccount, dim_tmpBonusTipnumber, dim_tmpBonusProduct, dim_tmpBonusTranCode)'
	set @SQLcmd = @SQLcmd + 'select distinct  Acctid,  Tipnumber, '
	set @SQLcmd = @SQLcmd + ''''+ @Product+ ''''
	set @SQLcmd = @SQLcmd + ', dim_BonusControl_BonusTrancode from AFFILIAT_Stage '
	set @SQLcmd = @SQLcmd + 'a join BonusControl B on a.AcctType = b.dim_BonusControl_Product '
	set @SQLcmd = @SQLcmd + 'where DateDiff( '
	set @SQLcmd = @SQLcmd + @IncrementType
	set @SQLcmd = @SQLcmd +	', a.DATEADDED , '
	set @SQLcmd = @SQLcmd + ''''+ @MonthDate + ''''
	set @SQLcmd = @SQLcmd + ') <= '+ @IncrementValue

	execute sp_sqlexec @SQLcmd
	
	--- Get factor from BonusControl table based on Product 
	--- and Update the Temp table with points
	If @BonusAmount is not null 
		Begin
			Update tmpBonus 
			Set dim_tmpBonusPoints = @BonusAmount
				Where dim_tmpBonusProduct = @Product
				and dim_tmpBonusPoints is null 
		End 
	If @BonusFactor is Not Null 
		Begin 
			Update tmpBonus 
			Set dim_tmpBonusPoints = ( Select (PurchAmt*@BonusFactor) - (ReturnAmt  * @BonusFactor) ) 
			From tmpBonus B join Input_Transaction T on B.dim_tmpBonusAccount = t.MemberAcct
				Where dim_tmpBonusProduct = @Product

		End 	
	Fetch Next From csrBonus 
		Into @IncrementType, @IncrementValue, @Product, @ProductTrancode, 
			@BonusTrancode, @BonusAmount, @BonusFactor	
		
END

Close csrBonus 
Deallocate csrBonus 



Begin TRY
	Begin Transaction 

	UPDATE cs
	Set RunAvaliableNew = RunAvaliableNew + bns.bnsPoints
	From Customer_stage cs Join 
	(Select dim_tmpBonusTipnumber, isNull( SUM(  dim_tmpBonusPoints  ) , 0 ) bnsPoints 
		From tmpBonus 
		Group by dim_tmpBonusTipnumber ) bns
		on cs.TIPNUMBER = bns.dim_tmpBonusTipnumber 
							
	INSERT INTO history_stage
		( TipNumber,
			Acctid, 
			HistDate,
			TranCode,
			TranCount,
			Points,
			Ratio,
			Description, 
			SecID )
		 
		  Select 
				t.Tipnumber, 
				a.dim_tmpBonusAccount, 
				convert(char(10),@MonthDate,101), 
				dim_tmpBonusTrancode,  
				'1', 
				dim_tmpBonusPoints, 
				'1', 
				TranType.Description, 
				'NEW'  
			From Input_Transaction T 
			join dbo.tmpBonus a on t.MemberAcct = a.dim_tmpBonusAccount
			join TranType on Trancode = dim_tmpBonusTrancode 
			where dim_tmpBonusPoints <> 0 
			
			set @RowsUpdated= @@ROWCOUNT
			print @RowsUpdated 
			
	Commit Transaction 
End TRY


Begin Catch
		If @@TRANCOUNT > 0 
			Begin
				print 'rollback'
				Rollback Transaction 
				Return @RowsUpdated
			End 
		Else
			Return @RowsUpdated
		
End Catch


END
GO
