USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spLoadStatementExtraStage]    Script Date: 01/11/2010 13:54:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date: 5/07 */
/* Author: Rich T  */
/*  **************************************  */
/*  	Load Extra fields for client into Monthly_Statement_File 
	Customer.Misc2 -> AcctId 	
Parameter: Frequency M = monthly Q = Quarterly. 

*/
/*  **************************************  */

create PROCEDURE [dbo].[spLoadStatementExtraStage]  @Frequency char(1)
AS 

If Upper( @Frequency ) = 'M'
Begin
	Update Monthly_Statement_File_Stage 
	Set Acctid = c.Misc2 from Customer_Stage C Join Monthly_Statement_File_Stage M on C.Tipnumber = M.Tipnumber
End

If Upper( @Frequency ) = 'Q'
Begin
	Update Monthly_Statement_File_Stage 
	Set Acctid = c.Misc2 from Customer_Stage C Join Monthly_Statement_File_Stage M on C.Tipnumber = M.Tipnumber
End
GO
