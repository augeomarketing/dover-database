USE [205AltaVista]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spProcessOptInFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spProcessOptInFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spProcessOptInFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spProcessOptInFile]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Remove opt out records that are from the monthly file.
	delete from dbo.DemographicIn 
	from dbo.demographicin di left outer join Monthly_OptIN oi on di.Pan = oi.Card_Number
	where oi.Card_Number is null
	

END

' 
END
GO
