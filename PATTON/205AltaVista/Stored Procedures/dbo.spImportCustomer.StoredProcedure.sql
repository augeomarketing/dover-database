USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spImportCustomer]    Script Date: 01/11/2010 13:54:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
/* 5/2007 Added code to populate Misc2 with Member number */
/* 5/2007 Added code to insert a member record into the Affiliat Table for all customers. */
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spImportCustomer] @EndDate DateTime AS
/* Update Existing Customers                                            */
Update Customer
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= Ltrim(  left(rtrim(Input_Customer.FIRSTNAME) + ' ' + rtrim(Input_Customer.LASTNAME),40) )
,ADDRESS1 	= Input_Customer.ADDRESS1
,ADDRESS2  	= Input_Customer.ADDRESS2
,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIP)+' '+ ltrim(rtrim(Input_Customer.Zip4)) ),40)
,CITY 		= Input_Customer.CITY
,STATE		= left(Input_Customer.STATE,2)
,ZIPCODE 	= ltrim(Input_Customer.ZIP)+ltrim(Input_Customer.ZIP4)
,HOMEPHONE 	= left(Input_Customer.HOMEPHONE,10)
,WORKPHONE = left(Input_Customer.BUSINESSPHONE,10)
,MISC1		= Input_Customer.EMPLOYEEFLAG
,STATUS	= Input_Customer.STATUSCODE
,MISC2                = Input_Customer.MEMBERNUM
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer.TIPNUMBER 


/*Add New Customers                                                      */
	Insert into Customer
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ADDRESS1, ADDRESS2, ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE, WORKPHONE, MISC1, MISC2, DATEADDED, STATUS, 
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), left(rtrim(LASTNAME),40),
	LTrim( left(rtrim(Input_Customer.FIRSTNAME) + ' ' + rtrim(Input_Customer.LASTNAME),40) ) , 
	Left(rtrim(ADDRESS1),40), Left(rtrim(ADDRESS2),40), left(ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIP) +' '+ltrim(rtrim(Input_Customer.Zip4)) ),40),
	CITY, left(STATE,2), rtrim(ZIP),
	left(HOMEPHONE,10), left(BUSINESSPHONE,10), EMPLOYEEFLAG, MEMBERNUM, @EndDate, STATUSCODE
	,0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer)

/* set Default status to A */
Update Customer
Set STATUS = 'A' 
Where STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer 
Set StatusDescription = 
S.StatusDescription 
from status S join Customer C on S.Status = C.Status


/* 5/2007 code to insert a member record into the Affiliat Table for all customers. */
Insert into Affiliat 
(acctid, tipnumber, accttype, dateadded, secid, accttypedesc, lastname, ytdearned, custid) 
select misc2, tipnumber, 'MEMBER', dateadded, '205', 'Member Record', lastname, 0, misc2 from customer 
where tipnumber not in (select tipnumber from affiliat where accttype = 'MEMBER')


/*                                                                            */
GO
