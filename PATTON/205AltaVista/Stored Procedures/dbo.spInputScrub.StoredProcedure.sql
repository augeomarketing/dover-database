USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 01/11/2010 13:54:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrub] 
GO
/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
--------------- Input Customer table
--  Remove input_customer records with Firstname and lastname = null 
--  Remove transactions for deleted customers
--  Remove single quotes from last name
--  Replace null zip+4 with empty string 
--  Remove formatting from Business phone numbers 
--------------- Input Transaction table
--  Replace null with 0 on purhase and trancounts in transaction 
--  Round the amounts
--  Combine MemberNumber and Account number to new col.
-- RDT 08/07/2010 Credits are now being sent as negatives. Debits are sent as positives. 
/******************************************************************************/	

CREATE PROCEDURE [dbo].[spInputScrub] AS

--------------- Input Customer table
/* Remove input_customer records with Firstname and lastname = null */
Delete from Input_Customer_error
Delete from Input_Transaction_error

Insert into Input_Customer_error 
select * from Input_Customer where FirstName is null and LastName is Null

Delete from Input_Customer 
where FirstName is null and LastName is Null

Insert into Input_Transaction_error 
	select * from Input_Transaction where membernum not in (select membernum from input_Customer) 
Delete from Input_Transaction 
	where membernum not in (select membernum from input_Customer) 


/* Move Last name to firstname if Firstname is null and set lastname to null  */
Update Input_Customer 
set FirstName = LastName, LastName = Null 
where firstname is null  and lastname is not null  

/* Update the Lastname = the first word in the lastname column */
update input_Customer 
set LastName = reverse( SUBSTRING ( reverse (rtrim(FirstName)), 1, PATINDEX ( '% %' , Right(reverse(rtrim(FirstName)),len(rtrim(FirstName))-1 ) )) ) 
where LastName is null  


/* set lastname = firstname where lastname < 1  */
Update Input_Customer
set LastName = FirstName 
where len(lastname ) < 1 

/* Remove any spaces left of name */
update input_customer
set firstname = ltrim(firstname)
where left(firstname,1) = ' '

Update Input_Customer
set LastName = Replace(LastName, '''','') 
where PatIndex('%''%', lastname ) > 0


/*Replace null zip+4 with empty string */
Update Input_Customer
set Zip4 = '' where Zip4 is null

/*Remove formatting from Home phone numbers */
Update Input_Customer 
Set HomePhone =  replace(HomePhone,'(','') 
where HomePhone is not null 

Update Input_Customer 
Set HomePhone =  replace(HomePhone,')','') 
where HomePhone is not null 

Update Input_Customer 
Set HomePhone =  replace(HomePhone,'-','') 
where HomePhone is not null 

/*Remove formatting from Business phone numbers */
Update Input_Customer 
Set BusinessPhone =  replace(BusinessPhone,'(','') 
where BusinessPhone is not null 

Update Input_Customer 
Set BusinessPhone =  replace(BusinessPhone,')','') 
where BusinessPhone is not null 

Update Input_Customer 
Set BusinessPhone =  replace(BusinessPhone,'-','') 
where BusinessPhone is not null

--------------- Input Transaction table
/*Replace null with 0 on purhase and trancounts in transaction */
Update Input_transaction 
set Purchamt  = 0 
where Purchamt  is null

Update Input_transaction 
set Purchcnt  = 0 
where Purchcnt  is null

Update Input_transaction 
set Returnamt  = 0 
where Returnamt  is null

Update Input_transaction 
set RetunCnt = 0 
where RetunCnt is null

-- RDT 08/07/2010 
Update Input_Transaction
set Returnamt = Returnamt *-1
where Returnamt < 0 

/* Round the amounts */
Update input_transaction 
set  purchamt = round(purchamt,0)

/*  Combine MemberNumber and Account number to new col.*/
update  input_transaction 
set MemberAcct = rtrim(MemberNum)+'-'+rtrim(AccountNum)
GO
