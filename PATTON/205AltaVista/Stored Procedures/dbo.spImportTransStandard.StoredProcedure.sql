USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransStandard]    Script Date: 01/11/2010 13:54:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  **************************************  */
/* Date:  4/2/2007 */
/* Author:  R. Tremblay*/
/*  **************************************  */
/*  Description: Imports Transaction data from TransStandard table into PointsNow */
/*  Tables:  
	TransStandard - select
	[database].client - select
	[database].Affiliat	- Update, Insert
	[database].history - insert
*/
/*  Revisions: */
/*  **************************************  */


CREATE PROCEDURE [dbo].[spImportTransStandard] @TipFirst char(3), @StartDate char(10), @enddate char(10), @Errmsg nvarchar(100) output 
AS 
declare @TFNO  nvarchar(15), @Trandate nchar(10), @ACCT_NUM nvarchar(25), @TRANCODE nchar(2), @TRANNUM nchar(4), @TRANAMT nchar(15), @TRANTYPE nchar(20), @RATIO nchar(4), @CRDACTVLDT nchar(10)  
declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
declare @dbName char(50), @SqlCmd nvarchar(1000)

/******************************************************************************/
/* Get database from dbProcessInfo */
/******************************************************************************/
set @dbName = ( select dbNamePatton from [RewardsNOW].dbo.dbProcessInfo where dbNumber = @TipFirst) 
If @dbName is null 
Begin
	set @ErrMsg = 'No Record in Rewardsnow.dbo.dbProcessInfo for tip' + @TipFirst
	Return -1
	/* Client record not found in */
End
 
/******************************************************************************/
/*    Get Max points from Client */
/******************************************************************************/
set @SqlCmd = 'Select @MaxPointsPerYear =  MaxPointsPerYear from ' + QuoteName(@dbName) + N'.dbo.Client' 
exec sp_ExecuteSql @SqlCmd, N'@MaxPointsPerYear decimal (9) output ', @MaxPointsPerYear = @MaxPointsPerYear Output

If @MaxPointsPerYear is null set @MaxPointsPerYear = 0 


/******************************************************************************/
/*  Cursor for TransStandard  */
/******************************************************************************/
declare Tran_crsr cursor
for select Tip, TRANDATE, ACCTNUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT
from TransStandard
order by Tip
/*                                                                            */
open Tran_crsr
fetch Tran_crsr into @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
 
/******************************************************************************/	
/* MAIN PROCESSING                                                            */
/******************************************************************************/
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	/*********************************************************************/
	/*  Purchase transaction                                             */
	/*********************************************************************/
	if (@Trancode='67')
	Begin
		/*********************************************************************/
		/*  Check MAX YTD Earned                                             */
		/*********************************************************************/
		set @YTDEarned=(select YTDEarned from affiliat where acctid=@acct_num)
	
		if (@YTDEarned) >= @MAXPOINTSPERYEAR
	        Begin
	        	set @OVERAGE = @TRANAMT
	        	set @AmtToPost='0'
			
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, ' ', @RATIO, @OVERAGE)			
			
			Goto NextRecord
		End
	
		if (@YTDEarned + @Tranamt)<= @MAXPOINTSPERYEAR
	        Begin
	        	set @OVERAGE = '0'
	        	set @AmtToPost=@TRANAMT
			set @YTDEarned= @YTDEarned + @AmttoPost
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, ' ', @RATIO, @OVERAGE)			
			update customer
			set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
			where tipnumber=@TFNO			
			update affiliat
			set YTDEarned=@YTDEarned 
			where acctid=@acct_num		
			Goto NextRecord
		End
		if (@YTDEarned + @Tranamt)> @MAXPOINTSPERYEAR
	        Begin
	        	set @OVERAGE = (@YTDEarned + @Tranamt)- @MAXPOINTSPERYEAR
	        	set @AmtToPost=@TRANAMT - @OVERAGE
			set @YTDEarned= @YTDEarned + @AmttoPost
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, ' ', @RATIO, @OVERAGE)			
			update customer
			set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
			where tipnumber=@TFNO			
			update affiliat
			set YTDEarned=@YTDEarned 
			where acctid=@acct_num		
			
			Goto NextRecord
		End
	End
	/*********************************************************************/
	/*  Return transaction                                              */
	/*********************************************************************/
	if (@Trancode='37')
	Begin
	        	set @AmtToPost = @TRANAMT
			insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
			values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, ' ', @RATIO, @OVERAGE)			
			update customer
			set runavailable=runavailable - @AmtToPost, runbalance=runbalance - @AmtToPost
			where tipnumber=@TFNO			
			Goto NextRecord
	End
NextRecord:
	fetch Tran_crsr into @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
end
Fetch_Error:
close  Tran_crsr
deallocate  Tran_crsr
GO
