USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeAccounts]    Script Date: 01/11/2010 13:54:25 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPurgeAccounts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPurgeAccounts]
GO 
CREATE PROCEDURE [dbo].[spPurgeAccounts] @DateInput as datetime AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
--
/* THIS DELETES CUSTOMERS BY MEMBER NUMBER NOT BY ACCOUNT */ 
--
/*  Delete from AccountDeleteInput */
/*  USE THE MEMBER NUMBER NOT THE ACCOUNT NUMBER */ 
/*  Delete Account */
/*  Delete History */
/*  Delete Customer*/
/*  USE THE MEMBER NUMBER NOT THE ACCOUNT NUMBER */ 
-- RDT 05/19/2010 
/*  **************************************  */
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	update accountdeleteinput  
		set accountdeleteinput.tipnumber = aff.tipnumber
	from dbo.affiliat as aff
		inner JOIN dbo.accountdeleteinput as adi
		on aff.acctid = adi.acctid

	update accountdeleteinput  
		set accountdeleteinput.tipnumber = aff.tipnumber
	from dbo.affiliat as aff
		inner JOIN dbo.accountdeleteinput as adi
		on aff.custid = adi.dda
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted where custid = dda
	INSERT INTO AffiliatDeleted 
		(TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
		 YTDEarned,CustID,DateDeleted )
	SELECT 
		 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
		 YTDEarned,CustID,  @DateDeleted
	FROM Affiliat where  exists 
		( select acctid from AccountDeleteInput 
		  where  Affiliat.custid = AccountDeleteInput.dda )
	
	/*  **************************************  */
	Delete from Affiliat where  exists 
		( select acctid from AccountDeleteInput 
		  where  Affiliat.custid = AccountDeleteInput.dda )
	
	/*  **************************************  */
	----Copy  Customers with Tipnumbers in affailiatDeleted to Customerdeleted 
	INSERT INTO [CustomerDeleted]
		(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
		DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
		ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
		WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
		Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
		TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
		DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
		ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
		WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
		Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, @DateDeleted 
		from customer WHERE tipnumber  not in  (select  distinct(tipnumber)  from Affiliat )   -- RDT 05/19/2010 
	-- RDT 05/19/2010 from customer WHERE  	tipnumber  in  (select  distinct(tipnumber)  from AffiliatDeleted ) 
	
	/*  **************************************  */
	-- Delete Customer with Tipnumbers in affailiatDeleted 
	Delete from customer 
	WHERE tipnumber  not in  (select  distinct(tipnumber)  from Affiliat )   -- RDT 05/19/2010 
	-- RDT 05/19/2010 where tipnumber  in (select  distinct(tipnumber)  from AffiliatDeleted )
	
	/*  **************************************  */
	-- Copy History records with Tipnumbers in affailiatDeleted to HistoryDeleted 
	INSERT INTO HistoryDeleted
		([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
		[POINTS], [Description], [SECID], [Ratio], [DateDeleted])
	select 
		[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
		[POINTS], [Description], [SECID], [Ratio], @DateDeleted
	from history 
	WHERE tipnumber  not in  (select  distinct(tipnumber)  from CUSTOMER )   -- RDT 05/19/2010 
	-- RDT 05/19/2010 	where	tipnumber  in 	(select  distinct(tipnumber)  from AffiliatDeleted )

	/*  **************************************  */
	Delete from history 
	WHERE tipnumber  not in  (select  distinct(tipnumber)  from CUSTOMER )   -- RDT 05/19/2010 
	-- RDT 05/19/2010  where tipnumber  in 	(select  distinct(tipnumber)  from AffiliatDeleted )
	
	/* Return 0 */
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
