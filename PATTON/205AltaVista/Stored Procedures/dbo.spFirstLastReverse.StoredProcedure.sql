USE [205AltaVista]
GO
/****** Object:  StoredProcedure [dbo].[spFirstLastReverse]    Script Date: 01/11/2010 13:54:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* BY:  R.Tremblay  */

/******************************************************************************/
CREATE PROCEDURE [dbo].[spFirstLastReverse] AS

Update customer 
set 
acctname1 =  Ltrim(  left(rtrim(I.Lastname) + ' ' + rtrim(I.Firstname),40) ),
lastname = lTrim(left(rtrim(i.Firstname),40))
from input_Customer i join customer c on i.tipnumber = c.tipnumber 

Update Affiliat 
set lastname = c.lastname 
from affiliat a join customer c on a.tipnumber = c.tipnumber
GO
