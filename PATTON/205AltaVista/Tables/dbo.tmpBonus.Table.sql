USE [205AltaVista]
GO

/****** Object:  Table [dbo].[tmpBonus]    Script Date: 08/03/2010 15:37:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tmpBonus]') AND type in (N'U'))
DROP TABLE [dbo].[tmpBonus]
GO

USE [205AltaVista]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tmpBonus](
	[dim_tmpBonusAccount] [nchar](16) NULL,
	[dim_tmpBonusTipnumber] [nchar](15) NULL,
	[dim_tmpBonusPoints] [int] NULL,
	[dim_tmpBonusProduct] [char](7) NULL,
	[dim_tmpBonusTrancode] [char](2) NULL
	
) ON [PRIMARY]

GO


