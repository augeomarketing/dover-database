USE [205AltaVista]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_OptIn_Archive]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_OptIn_Archive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_OptIn_Archive]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_OptIn_Archive](
	[sid_Record_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Card_Number] [varchar](16) NOT NULL,
	[Date_Archived] [datetime] NOT NULL,
 CONSTRAINT [PK_Monthly_OptIn_Archive] PRIMARY KEY CLUSTERED 
(
	[sid_Record_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_OptIn_Archive]') AND name = N'IX_Monthly_OptIn_Archive')
CREATE NONCLUSTERED INDEX [IX_Monthly_OptIn_Archive] ON [dbo].[Monthly_OptIn_Archive] 
(
	[Card_Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
