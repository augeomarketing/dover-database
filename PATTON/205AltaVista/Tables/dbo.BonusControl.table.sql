USE [205AltaVista]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[BonusControl]    Script Date: 07/30/2010 15:19:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BonusControl]') AND type in (N'U'))
DROP TABLE [dbo].[BonusControl]
GO

/****** Object:  Table [dbo].[BonusControl]    Script Date: 07/30/2010 15:19:42 ******/


CREATE TABLE [dbo].[BonusControl](
	[dim_BonusControl_EffectiveDate] [date] NOT NULL,
	[dim_bonusControl_AddDate] [date] NULL,
	[dim_BonusControl_IncrementType] [char](10) NOT NULL,
	[dim_BonusControl_IncrementValue] [int] NOT NULL,
	[dim_BonusControl_Product] [char](6) NOT NULL,
	[dim_BonusControl_ProductTrancode] [char](2) NULL,
	[dim_BonusControl_BonusTrancode] [char](2) NULL,
	[dim_BonusControl_BonusAmount] [Int] NULL, 
	[dim_BonusControl_BonusFactor] [float] NULL, 
	
) ON [PRIMARY]

GO

/****** Object:  Index [IX_BonusControl]    Script Date: 07/30/2010 15:20:06 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[BonusControl]') AND name = N'IX_BonusControl')
DROP INDEX [IX_BonusControl] ON [dbo].[BonusControl] WITH ( ONLINE = OFF )
GO

USE [205AltaVista]
GO

/****** Object:  Index [IX_BonusControl]    Script Date: 07/30/2010 15:20:06 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_BonusControl] ON [dbo].[BonusControl] 
(
	[dim_BonusControl_EffectiveDate] ASC,
	[dim_BonusControl_Product] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


SET ANSI_PADDING OFF
GO


