/****** Object:  Table [dbo].[StageHISTORY]    Script Date: 05/19/2009 16:06:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [StageHISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
