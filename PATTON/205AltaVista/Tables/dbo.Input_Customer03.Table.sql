USE [205AltaVista]
GO
/****** Object:  Table [dbo].[Input_Customer03]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer03](
	[MemberNum] [char](15) NOT NULL,
	[FirstName] [char](50) NULL,
	[LastName] [char](50) NULL,
	[Address1] [char](50) NULL,
	[Address2] [char](50) NULL,
	[City] [char](50) NULL,
	[State] [char](10) NULL,
	[Zip] [char](5) NULL,
	[ZIP4] [char](4) NULL,
	[HomePhone] [char](13) NULL,
	[BusinessPhone] [char](13) NULL,
	[StatusCode] [char](1) NULL,
	[Last4] [char](4) NULL,
	[EmployeeFlag] [char](1) NULL,
	[OldMember] [char](15) NULL,
	[TipNumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
