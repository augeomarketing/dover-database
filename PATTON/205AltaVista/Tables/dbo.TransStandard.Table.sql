USE [205AltaVista]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 06/26/2013 14:32:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
DROP TABLE [dbo].[TransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransStandard]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransStandard](
	[TFNO] [nvarchar](15) NULL,
	[TranDate] [nvarchar](10) NULL,
	[Acct_Num] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](20) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
