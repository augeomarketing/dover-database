USE [205AltaVista]
GO
/****** Object:  Table [dbo].[Client_extra]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Client_extra](
	[DataName] [char](20) NOT NULL,
	[DataValue] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
