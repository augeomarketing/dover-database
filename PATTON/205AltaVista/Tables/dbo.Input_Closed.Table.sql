USE [205AltaVista]
GO
/****** Object:  Table [dbo].[Input_Closed]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Closed](
	[MemberNUm] [char](15) NULL,
	[FirstName] [char](50) NULL,
	[LastName] [char](50) NULL,
	[Status] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
