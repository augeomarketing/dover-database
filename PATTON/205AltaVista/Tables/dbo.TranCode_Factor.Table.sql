USE [205AltaVista] 
GO

/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 07/29/2010 16:29:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND type in (N'U'))
DROP TABLE [dbo].[TranCode_Factor]
GO

/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 07/29/2010 16:29:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
