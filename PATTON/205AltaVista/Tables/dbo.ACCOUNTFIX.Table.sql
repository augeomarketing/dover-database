USE [205AltaVista]
GO
/****** Object:  Table [dbo].[ACCOUNTFIX]    Script Date: 01/11/2010 13:54:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCOUNTFIX](
	[TIPNUMBER] [nvarchar](15) NULL,
	[RUNAVAILABLE] [numeric](18, 0) NULL,
	[HISTPOINTS] [numeric](18, 0) NULL,
	[DIFF] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
