USE [205AltaVista]
GO
/****** Object:  Table [dbo].[Input_Heloc]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Heloc](
	[MemberNum] [char](15) NULL,
	[AccountNum] [char](15) NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[ReturnAmt] [money] NULL,
	[ReturnCnt] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
