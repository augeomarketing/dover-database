USE [205AltaVista]
GO
/****** Object:  Table [dbo].[Input_Transaction_Error]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction_Error](
	[MemberNum] [char](15) NOT NULL,
	[AccountNum] [char](15) NOT NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[ReturnAmt] [money] NULL,
	[RetunCnt] [int] NULL,
	[trandate] [char](10) NULL,
	[ProductType] [char](6) NULL,
	[TipNUmber] [char](15) NULL,
	[MemberAcct] [varchar](25) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
