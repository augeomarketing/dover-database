USE [205AltaVista]
GO
/****** Object:  Table [dbo].[output_Transaction]    Script Date: 01/11/2010 13:54:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[output_Transaction](
	[TipNumber] [char](15) NOT NULL,
	[TranDate] [char](10) NOT NULL,
	[AcctId] [char](25) NOT NULL,
	[TranCode] [char](2) NOT NULL,
	[TranCount] [char](4) NOT NULL,
	[TranAmt] [int] NOT NULL,
	[CardType] [char](20) NOT NULL,
	[Ratio] [char](4) NOT NULL,
	[DateLastAct] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
