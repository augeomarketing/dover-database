USE [205AltaVista]
GO
/****** Object:  Table [dbo].[input_bonus_Error]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[input_bonus_Error](
	[MemberNum] [char](15) NOT NULL,
	[Points] [numeric](18, 0) NULL,
	[Reason] [char](50) NULL,
	[TipNumber] [char](15) NULL,
	[Trancode] [char](2) NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
