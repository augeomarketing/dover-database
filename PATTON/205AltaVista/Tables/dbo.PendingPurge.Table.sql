USE [205AltaVista]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 01/11/2010 13:54:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[ACCTID] [char](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
