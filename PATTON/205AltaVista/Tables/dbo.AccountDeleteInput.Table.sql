USE [205AltaVista]
GO
/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 01/11/2010 13:54:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
