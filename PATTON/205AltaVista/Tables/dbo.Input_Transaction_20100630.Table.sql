USE [205AltaVista]
GO

/****** Object:  Table [dbo].[Input_Transaction_20100630]    Script Date: 06/30/2010 09:26:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transaction_20100630]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transaction_20100630]
GO

USE [205AltaVista]
GO

/****** Object:  Table [dbo].[Input_Transaction_20100630]    Script Date: 06/30/2010 09:26:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_Transaction_20100630](
	[MemberNum] [char](15) NOT NULL,
	[AccountNum] [char](15) NOT NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[ReturnAmt] [money] NULL,
	[RetunCnt] [int] NULL,
	[trandate] [char](10) NULL,
	[ProductType] [char](6) NULL,
	[TipNUmber] [char](15) NULL,
	[MemberAcct] [varchar](25) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


