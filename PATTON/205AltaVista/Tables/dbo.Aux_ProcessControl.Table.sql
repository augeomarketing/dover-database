USE [205AltaVista]
GO
/****** Object:  Table [dbo].[Aux_ProcessControl]    Script Date: 01/11/2010 13:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Aux_ProcessControl](
	[RecordNumber] [int] IDENTITY(1,1) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[MonthBegin] [datetime] NOT NULL,
	[MonthEnd] [datetime] NOT NULL
) ON [PRIMARY]
GO
