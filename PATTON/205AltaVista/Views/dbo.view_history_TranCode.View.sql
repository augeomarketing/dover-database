USE [205AltaVista]
GO
/****** Object:  View [dbo].[view_history_TranCode]    Script Date: 01/11/2010 13:54:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[view_history_TranCode] as 
 select HISTORY.tipnumber, sum(points*ratio) as tranpoints 
   from history, Beginning_Balance_Table
   where histdate < '2007/03/01' 
	AND HISTORY.TIPNUMBER = BEGINNING_BALANCE_TABLE.TIPNUMBER 
	group by HISTORY.tipnumber
GO
