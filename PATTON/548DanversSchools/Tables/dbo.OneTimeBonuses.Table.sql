USE [548DanversSchools]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 09/25/2009 11:38:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
