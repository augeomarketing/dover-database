USE [548DanversSchools]
GO
/****** Object:  Table [dbo].[sweepfile]    Script Date: 09/25/2009 11:38:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sweepfile](
	[Tipnumber] [varchar](15) NOT NULL,
	[School] [varchar](15) NULL,
	[Dollars] [numeric](9, 2) NULL,
	[Acctname1] [varchar](40) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
