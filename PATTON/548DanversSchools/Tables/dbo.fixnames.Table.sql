USE [548DanversSchools]
GO
/****** Object:  Table [dbo].[fixnames]    Script Date: 09/25/2009 11:38:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixnames](
	[tipnumber] [varchar](15) NOT NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
