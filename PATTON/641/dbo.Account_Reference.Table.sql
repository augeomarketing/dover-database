SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_Reference](
	[Tipnumber] [varchar](15) NOT NULL,
	[acctnumber] [varchar](25) NOT NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Account_Reference] ADD  CONSTRAINT [DF_Account_Reference_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
