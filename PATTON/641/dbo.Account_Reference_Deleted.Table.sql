SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account_Reference_Deleted](
	[Tipnumber] [varchar](15) NOT NULL,
	[acctnumber] [varchar](50) NOT NULL,
	[TipFirst] [varchar](50) NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Account_Reference_Deleted] ADD  CONSTRAINT [DF_Account_Reference_Deleted_DateDeleted]  DEFAULT (getdate()) FOR [DateDeleted]
GO
