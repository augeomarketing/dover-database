SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[acctid] [char](16) NULL,
	[lastfour] [char](4) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[pointfloor] [char](11) NULL,
	[PointsExpire] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_pointfloor]  DEFAULT (0) FOR [pointfloor]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT (0) FOR [PointsExpire]
GO
