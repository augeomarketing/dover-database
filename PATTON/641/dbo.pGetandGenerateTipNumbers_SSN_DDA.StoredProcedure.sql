SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbers_SSN_DDA] @TipFirstParm varchar(3)
AS 

declare @LastTipUsed char(15)
declare  @PAN nchar(16), @SSN varchar(16), @PrimDDA nchar(16), @1stDDA nchar(16), @2ndDDA nchar(16), @3rdDDA nchar(16), @4thDDA nchar(16), @5thDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)

update demographicIn set pan=rtrim(ltrim(pan))
update demographicIn set [SSN]=null where len(rtrim(ltrim([SSN])))=0
update demographicIn set [Prim DDA]=null where len(rtrim(ltrim([Prim DDA])))=0
update demographicIn set [1st DDA]=null where len(rtrim(ltrim([1st DDA])))=0
update demographicIn set [2nd DDA]=null where len(rtrim(ltrim([2nd DDA])))=0
update demographicIn set [3rd DDA]=null where len(rtrim(ltrim([3rd DDA])))=0
update demographicIn set [4th DDA]=null where len(rtrim(ltrim([4th DDA])))=0
update demographicIn set [5th DDA]=null where len(rtrim(ltrim([5th DDA])))=0

-------------BEGIN update DemographicIn to Assign Tips to recs via SSN first, then DDA
--pass 1 (update by SSN)

update di
      set TipNumber = ar.tipnumber,
            tipfirst = @TipFirstParm
	from dbo.DemographicIn di join dbo.account_reference ar
      on di.[SSN] = ar.acctnumber
      where di.Tipnumber is null
      
     
--pass 2 (update by PrimDDA)      
update di
      set TipNumber = ar.tipnumber,
            tipfirst = @TipFirstParm
from dbo.DemographicIn di join dbo.account_reference ar
      on di.[Prim DDA] = ar.acctnumber      
      where di.Tipnumber is null
-----------END update DemographicIn to Assign Tips to recs via SSN first, then DDA      



---------==============================================
--create a temp table based on Distinct SSNs and DDAs for recs still w/o tipnumber
select distinct [SSN],[Prim DDA] as PrimDDA
into #tmpSSN_DDA
from DemographicIn 
where [Tipnumber] is null
--where [Prim DDA] not in (select acctnumber from Account_Reference)


declare csrSSN_DDA cursor FAST_FORWARD for
select SSN,PrimDDA from #tmpSSN_DDA
open csrSSN_DDA
------------------
      exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstParm, @LastTipUsed output
      select @LastTipUsed as LastTipUsed
	  set @newtipnumber=@LastTipUsed
	  set @newtipnumber = cast(@newtipnumber as bigint) + 1  
	  --set the value of the first new tipnumber
---------------     



fetch next from csrSSN_DDA
      into  @SSN, @PrimDDA

while @@FETCH_STATUS = 0
BEGIN
      -- Get new tip#

		
      -- if Neither SSN OR PrimDDA are in account_reference, get a new tip and add both
		if NOT exists(select acctnumber from account_reference where acctnumber=@SSN) AND NOT exists(select acctnumber from account_reference where acctnumber=@PrimDDA)
			begin
				set @newtipnumber = cast(@newtipnumber as bigint) + 1  
				set @UpdateTip=@newtipnumber
				--
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @PrimDDA, @TipFirstParm)					
			
			end
		--if the SSN isn't in AcctRef but DDA IS, Lookup tip associated with DDA  and add this in with the SSN
		if not exists(select acctnumber from account_reference where acctnumber=@SSN) and EXISTS(select acctnumber from account_reference where acctnumber=@PrimDDA)	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@PrimDDA
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @SSN, @TipFirstParm) 		
			End

		--if the DDA isn't in AcctRef but SSN IS, Lookup tip associated with SSN  and add this in with the DDA
		if not exists(select acctnumber from account_reference where acctnumber=@PrimDDA) and EXISTS(select acctnumber from account_reference where acctnumber=@SSN)	
			Begin
				Select @UpdateTip=Tipnumber from account_reference where acctnumber=@SSN
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst)
					values(@UpdateTip, @PrimDDA, @TipFirstParm) 		
			End			
			


                     
                     
      fetch next from csrSSN_DDA
      into  @SSN, @PrimDDA
END

close csrSSN_DDA
deallocate csrSSN_DDA

-- Update last Tip Number Used
exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstParm, @newtipnumber 
---===============================================

--re-update Demographic in to give tips to the NEW card numbers
update di
      set TipNumber = ar.tipnumber,
            tipfirst = @TipFirstParm
from dbo.DemographicIn di join dbo.account_reference ar
      on di.[Prim DDA] = ar.acctnumber
      where di.Tipnumber is null
-----------==================================================
GO
