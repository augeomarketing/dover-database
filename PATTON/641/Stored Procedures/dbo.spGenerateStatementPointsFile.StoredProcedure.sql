USE [641BillingsFCU]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateStatementPointsFile]    Script Date: 05/16/2012 13:48:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateStatementPointsFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGenerateStatementPointsFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGenerateStatementPointsFile]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateStatementPointsFile]
	-- Add the parameters for the stored procedure here
	@EndDate char(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @EndingDate date
	
	set @EndingDate=CONVERT(datetime, @enddate, 102)

    -- Insert statements for procedure here
	Truncate table StatementPoints

	insert into dbo.StatementPoints
	select tipnumber as RewardsNumber, RTRIM(acctname1) as Name, RTRIM(Misc1) as SSN, RTRIM(Misc2) as DDA, Runavailable as PointsAvailable, @EndingDate as AsOfDate
	from dbo.customer
	order by Tipnumber asc
END
' 
END
GO
