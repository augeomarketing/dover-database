USE [507RabobankCommercial]
GO
/****** Object:  Table [dbo].[fixer]    Script Date: 09/23/2009 17:10:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixer](
	[tipnumber] [varchar](15) NOT NULL,
	[fixpoints] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
