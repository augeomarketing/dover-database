USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_RN_FITables_FromBAK_DELETEDATA]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_FITables_FromBAK_DELETEDATA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Load_RN_FITables_FromBAK_DELETEDATA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_FITables_FromBAK_DELETEDATA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
/*
	this proc loads RNICustomer for a particular DBNumber
*/

-- =============================================
CREATE PROCEDURE [dbo].[usp_Load_RN_FITables_FromBAK_DELETEDATA]
	@DBNumber varchar(3)
	, @debug INT = 0 

/* SAMPLE CALL
	declare @DBNumber varchar(3)=''260''
	exec usp_Load_RN_FITables_FromBAK_DELETEDATA @DBNumber , 0
	
	select * from [260].dbo.Customer
	select * from [260].dbo.Customer_stage
	select * from [260].dbo.CustomerDeleted
	
	select * from [260].dbo.Affiliat
	select * from [260].dbo.Affiliat_stage
	select * from [260].dbo.AffiliatDeleted
	
	select * from [260].dbo.History
	select * from [260].dbo.History_stage
	select * from [260].dbo.HistoryDeleted	
		
	delete from RNICustomer
*/	
AS
BEGIN


	declare @DBNamePatton nvarchar(100)
	declare @HistTruncSQL nvarchar(2000),@HistTruncSQLs nvarchar(2000),@HistTruncSQLd nvarchar(2000)
	declare @AffTruncSQL nvarchar(2000),@AffTruncSQLs nvarchar(2000),@AffTruncSQLd nvarchar(2000)
	declare @CustTruncSQL nvarchar(2000),@CustTruncSQLs nvarchar(2000),@CustTruncSQLd nvarchar(2000)

	select @DBNamePatton=DBNamePatton FROM dbprocessinfo where DBNumber=@DBNumber


	/*******************************************************************************************************/
	/* BEGIN HISTORY TABLES*/

	
	set @HistTruncSQL=''TRUNCATE TABLE [''  + @DBNamePatton + ''].dbo.History''
	set @HistTruncSQLs=''DELETE FROM [''  + @DBNamePatton + ''].dbo.History_stage''
	set @HistTruncSQLd=''TRUNCATE TABLE [''  + @DBNamePatton + ''].dbo.Historydeleted''
	
	set @AffTruncSQL=''TRUNCATE TABLE [''  + @DBNamePatton + ''].dbo.Affiliat''
	set @AffTruncSQLs=''DELETE FROM [''  + @DBNamePatton + ''].dbo.Affiliat_stage''
	set @AffTruncSQLd=''TRUNCATE TABLE [''  + @DBNamePatton + ''].dbo.Affiliatdeleted''
	
	set @CustTruncSQL=''TRUNCATE TABLE [''  + @DBNamePatton + ''].dbo.Customer''
	set @CustTruncSQLs=''DELETE FROM [''  + @DBNamePatton + ''].dbo.Customer_stage''
	set @CustTruncSQLd=''TRUNCATE TABLE [''  + @DBNamePatton + ''].dbo.Customerdeleted''
	
	if @debug=1
	begin
		print @HistTruncSQL
		print @HistTruncSQLs
		print @HistTruncSQLd
		print @AffTruncSQL
		print @AffTruncSQLs
		print @AffTruncSQLd
		print @CustTruncSQL
		print @CustTruncSQLs
		print @CustTruncSQLd
		
	end
	else
		begin
			EXECUTE sp_executesql @HistTruncSQL
			EXECUTE sp_executesql @HistTruncSQLs
			EXECUTE sp_executesql @HistTruncSQLd
			EXECUTE sp_executesql @AffTruncSQL
			EXECUTE sp_executesql @AffTruncSQLs
			EXECUTE sp_executesql @AffTruncSQLd
			EXECUTE sp_executesql @CustTruncSQL
			EXECUTE sp_executesql @CustTruncSQLs
			EXECUTE sp_executesql @CustTruncSQLd
		end
	

END

' 
END
GO
