USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_RN_FITables_FromBAK_LOADDATA]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_FITables_FromBAK_LOADDATA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Load_RN_FITables_FromBAK_LOADDATA]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_FITables_FromBAK_LOADDATA]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
/*
	this proc loads RNICustomer for a particular DBNumber
*/

-- =============================================
CREATE PROCEDURE [dbo].[usp_Load_RN_FITables_FromBAK_LOADDATA]
	@DBNumber varchar(3)
	, @debug INT = 0 

/* SAMPLE CALL
	declare @DBNumber varchar(3)=''260''
	exec usp_Load_RN_FITables_FromBAK_LOADDATA @DBNumber , 1
	
	select * from [260].dbo.Customer
	select * from [260].dbo.Customer_stage
	select * from [260].dbo.CustomerDeleted
	
	select * from [260].dbo.Affiliat
	select * from [260].dbo.Affiliat_stage
	select * from [260].dbo.AffiliatDeleted
	
	select * from [260].dbo.History
	select * from [260].dbo.History_stage
	select * from [260].dbo.HistoryDeleted	
		
	delete from RNICustomer
*/	
AS
BEGIN


	declare @DBNamePatton nvarchar(100)
	declare @TruncSQL nvarchar(2000),@TruncSQLs nvarchar(2000),@TruncSQLd nvarchar(2000)
	declare @InsSQL nvarchar(2000),@InsSQLs nvarchar(2000),@InsSQLd nvarchar(2000)
	select @DBNamePatton=DBNamePatton FROM dbprocessinfo where DBNumber=@DBNumber

	--/* BEGIN CUSTOMER TABLES*/

	--- load the Customer table
	set @InsSQL=''insert into ['' + @DBNamePatton + ''].dbo.Customer
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	FROM [RN_CentralProcessingTables_BAK].dbo.Customer''
	------------------------------------------------------------
	---customer_stage
	set @InsSQLs=''insert into ['' + @DBNamePatton + ''].dbo.Customer_stage
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	FROM [RN_CentralProcessingTables_BAK].dbo.Customer_stage''

	--------------------------------------------------------------
	--CustomerDeleted
	set @InsSQLd=''insert into ['' + @DBNamePatton + ''].dbo.CustomerDeleted
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
	select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted
	FROM [RN_CentralProcessingTables_BAK].dbo.CustomerDeleted''

	
	if @debug=1
	begin
		print @InsSQL
		print @InsSQLs
		print @InsSQLd
	end
	else
		begin
			EXECUTE sp_executesql @InsSQL
			EXECUTE sp_executesql @InsSQLs
			EXECUTE sp_executesql @InsSQLd
		end	
	
	/* END CUSTOMER TABLES*/	


	
	--/*******************************************************************************************************/
	----/* BEGIN AFFILIAT TABLES*/

	
	set @InsSQL=''insert into ['' + @DBNamePatton + ''].dbo.Affiliat
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID )
	select 
		ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID 
	FROM [RN_CentralProcessingTables_BAK].dbo.Affiliat''

	-------------------------------------------------------------------
	set @InsSQLs=''insert into ['' + @DBNamePatton + ''].dbo.Affiliat_stage
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select 
		ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
	FROM [RN_CentralProcessingTables_BAK].dbo.Affiliat_stage''

	-------------------------------------------------------------------
	set @InsSQLd=''insert into ['' + @DBNamePatton + ''].dbo.AffiliatDeleted
		(TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
	select 
		TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted
	FROM [RN_CentralProcessingTables_BAK].dbo.AffiliatDeleted''

	---------------------------------------------------------------------	
	if @debug=1
	begin
		print @InsSQL
		print @InsSQLs
		print @InsSQLd
	end
	else
		begin
			EXECUTE sp_executesql @InsSQL
			EXECUTE sp_executesql @InsSQLs
			EXECUTE sp_executesql @InsSQLd
		end			
	
	/* END AFFILIAT TABLES*/

	/*******************************************************************************************************/
	/* BEGIN HISTORY TABLES*/

	set @InsSQL=''insert into ['' + @DBNamePatton + ''].dbo.History
		(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select 
		TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	FROM [RN_CentralProcessingTables_BAK].dbo.History''

	-------------------------------------------------------------------
	set @InsSQLs=''insert into ['' + @DBNamePatton + ''].dbo.History_stage
		(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select 
		TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	FROM [RN_CentralProcessingTables_BAK].dbo.History_stage''

	-------------------------------------------------------------------	
	set @InsSQLd=''insert into ['' + @DBNamePatton + ''].dbo.HistoryDeleted
		(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	select 
		TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted
	FROM [RN_CentralProcessingTables_BAK].dbo.HistoryDeleted''
	
	---------------------------------------------------------------------		
	if @debug=1
	begin
		print @InsSQL
		print @InsSQLs
		print @InsSQLd
	end
	else
		begin
			EXECUTE sp_executesql @InsSQL
			EXECUTE sp_executesql @InsSQLs
			EXECUTE sp_executesql @InsSQLd
		end				
		
	/* END HISTORY TABLES*/	
	--/*******************************************************************************************************/

END

' 
END
GO
