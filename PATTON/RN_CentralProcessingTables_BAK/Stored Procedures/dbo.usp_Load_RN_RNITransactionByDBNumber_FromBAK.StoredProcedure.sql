USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_RN_RNITransactionByDBNumber_FromBAK]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_RNITransactionByDBNumber_FromBAK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Load_RN_RNITransactionByDBNumber_FromBAK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_RNITransactionByDBNumber_FromBAK]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
/*
	this proc loads RNITransaction for a particular DBNumber
*/

-- =============================================
CREATE PROCEDURE [dbo].[usp_Load_RN_RNITransactionByDBNumber_FromBAK]
	@DBNumber varchar(3)

/* SAMPLE CALL
	declare @DBNumber varchar(3)=''229''
	exec usp_Load_RN_RNITransactionByDBNumber_FromBAK @DBNumber
	
	select * from Rewardsnow.dbo.RNITransaction where sid_dbprocessinfo_dbnumber=''229''
	
*/	
AS
BEGIN



DELETE from RewardsNow.dbo.RNITransaction where sid_dbprocessinfo_dbnumber=@DBNumber


set IDENTITY_INSERT RewardsNow.dbo.RNITransaction ON

insert into RewardsNow.dbo.RNITransaction
(sid_RNITransaction_ID, sid_rnirawimport_id, dim_RNITransaction_TipPrefix, dim_RNITransaction_Portfolio, dim_RNITransaction_Member, dim_RNITransaction_PrimaryId, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, dim_RNITransaction_CardNumber, dim_RNITransaction_TransactionDate, dim_RNITransaction_TransferCard, dim_RNITransaction_TransactionCode, dim_RNITransaction_DDANumber, dim_RNITransaction_TransactionAmount, dim_RNITransaction_TransactionCount, dim_RNITransaction_TransactionDescription, dim_RNITransaction_CurrencyCode, dim_RNITransaction_MerchantID, dim_RNITransaction_TransactionID, dim_RNITransaction_AuthorizationCode, dim_RNITransaction_TransactionProcessingCode, dim_RNITransaction_PointsAwarded, sid_trantype_trancode,  dim_RNITransaction_EffectiveDate, sid_localfi_history_id, sid_smstranstatus_id, dim_rnitransaction_merchantname, dim_rnitransaction_merchantaddress1, dim_rnitransaction_merchantaddress2, dim_rnitransaction_merchantaddress3, dim_rnitransaction_merchantcity, dim_rnitransaction_merchantstateregion, dim_rnitransaction_merchantecountrycode, dim_rnitransaction_merchantpostalcode, dim_rnitransaction_merchantcategorycode)
select 
	sid_RNITransaction_ID, sid_rnirawimport_id, dim_RNITransaction_TipPrefix, dim_RNITransaction_Portfolio, dim_RNITransaction_Member, dim_RNITransaction_PrimaryId, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, dim_RNITransaction_CardNumber, dim_RNITransaction_TransactionDate, dim_RNITransaction_TransferCard, dim_RNITransaction_TransactionCode, dim_RNITransaction_DDANumber, dim_RNITransaction_TransactionAmount, dim_RNITransaction_TransactionCount, dim_RNITransaction_TransactionDescription, dim_RNITransaction_CurrencyCode, dim_RNITransaction_MerchantID, dim_RNITransaction_TransactionID, dim_RNITransaction_AuthorizationCode, dim_RNITransaction_TransactionProcessingCode, dim_RNITransaction_PointsAwarded, sid_trantype_trancode, dim_RNITransaction_EffectiveDate, sid_localfi_history_id, sid_smstranstatus_id, dim_rnitransaction_merchantname, dim_rnitransaction_merchantaddress1, dim_rnitransaction_merchantaddress2, dim_rnitransaction_merchantaddress3, dim_rnitransaction_merchantcity, dim_rnitransaction_merchantstateregion, dim_rnitransaction_merchantecountrycode, dim_rnitransaction_merchantpostalcode, dim_rnitransaction_merchantcategorycode
	from RN_CentralProcessingTables_BAK.dbo.RNITransaction  WHERE sid_dbprocessinfo_dbnumber=@DBNumber
set IDENTITY_INSERT RewardsNow.dbo.RNITransaction  OFF

END
' 
END
GO
