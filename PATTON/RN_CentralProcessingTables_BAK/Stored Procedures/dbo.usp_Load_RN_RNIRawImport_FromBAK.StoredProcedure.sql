USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_RN_RNIRawImport_FromBAK]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_RNIRawImport_FromBAK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Load_RN_RNIRawImport_FromBAK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_RNIRawImport_FromBAK]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Bob L.>
/*
	This proc takes a DBNumber 
	pulls records from  RNIRawImport in the BAK database and Copies them to RewardsNow.dbo.RNIRawImport fpr the DBNumber
*/
-- =============================================
CREATE PROCEDURE [dbo].[usp_Load_RN_RNIRawImport_FromBAK]
	@DBNumber varchar(3)
--	,@dimRNIRawImport_ACCT_Source varchar(500)=''''
--	,@dimRNIRawImport_TRAN_Source varchar(500)=''''

/* SAMPLE CALL
	declare @DBNumber varchar(3)=''717''
	exec usp_LoadRNIRawImport_FromBAK @DBNumber   ----, ''\\Patton\ops\700_Series_Input\700_ACCT_20150205_1.txt'', ''\\Patton\ops\700_Series_Input\700_TRAN_20150205_1.txt''
	
	
	select * from RewardsNow.dbo.RNIRawImport where sid_dbprocessinfo_dbnumber=''717'' @DBNumber
	delete from RewardsNow.dbo.RNIRawImport  where sid_dbprocessinfo_dbnumber=''717''
*/	
AS
BEGIN


declare @NumRawRecs int
select @NumRawRecs =COUNT(*) from RNIRawImport where sid_dbprocessinfo_dbnumber=@DBNumber



-- if There are RNIRawImport records fo this 
if (@NumRawRecs>0)
	BEGIN
		
		
		DELETE from RewardsNow.dbo.RNIRawImport where sid_dbprocessinfo_dbnumber=@DBNumber
		
		set IDENTITY_INSERT RewardsNow.dbo.RNIRawImport ON
		
		insert into RewardsNow.dbo.RNIRawImport
			(sid_rnirawimport_id, sid_rniimportfiletype_id, dim_rnirawimport_source, dim_rnirawimport_sourcerow, sid_dbprocessinfo_dbnumber, dim_rnirawimport_field01, dim_rnirawimport_field02, dim_rnirawimport_field03, dim_rnirawimport_field04, dim_rnirawimport_field05, dim_rnirawimport_field06, dim_rnirawimport_field07, dim_rnirawimport_field08, dim_rnirawimport_field09, dim_rnirawimport_field10, dim_rnirawimport_field11, dim_rnirawimport_field12, dim_rnirawimport_field13, dim_rnirawimport_field14, dim_rnirawimport_field15, dim_rnirawimport_field16, dim_rnirawimport_field17, dim_rnirawimport_field18, dim_rnirawimport_field19, dim_rnirawimport_field20, dim_rnirawimport_field21, dim_rnirawimport_field22, dim_rnirawimport_field23, dim_rnirawimport_field24, dim_rnirawimport_field25, dim_rnirawimport_field26, dim_rnirawimport_field27, dim_rnirawimport_field28, dim_rnirawimport_field29, dim_rnirawimport_field30, dim_rnirawimport_field31, dim_rnirawimport_field32, dim_rnirawimport_field33, dim_rnirawimport_field34, dim_rnirawimport_field35, dim_rnirawimport_field36, dim_rnirawimport_field37, dim_rnirawimport_field38, dim_rnirawimport_field39, dim_rnirawimport_field40, dim_rnirawimport_field41, dim_rnirawimport_field42, dim_rnirawimport_field43, dim_rnirawimport_field44, dim_rnirawimport_field45, dim_rnirawimport_field46, dim_rnirawimport_field47, dim_rnirawimport_field48, dim_rnirawimport_field49, dim_rnirawimport_field50, dim_rnirawimport_field51, dim_rnirawimport_field52, dim_rnirawimport_field53, dim_rnirawimport_field54, dim_rnirawimport_field55, dim_rnirawimport_field56, dim_rnirawimport_field57, dim_rnirawimport_field58, dim_rnirawimport_field59, dim_rnirawimport_dateadded, dim_rnirawimport_lastmodified, dim_rnirawimport_lastmodifiedby, sid_rnirawimportstatus_id, dim_rnirawimport_processingenddate, RowHash)
		select 
			sid_rnirawimport_id, sid_rniimportfiletype_id, dim_rnirawimport_source, dim_rnirawimport_sourcerow, sid_dbprocessinfo_dbnumber, dim_rnirawimport_field01, dim_rnirawimport_field02, dim_rnirawimport_field03, dim_rnirawimport_field04, dim_rnirawimport_field05, dim_rnirawimport_field06, dim_rnirawimport_field07, dim_rnirawimport_field08, dim_rnirawimport_field09, dim_rnirawimport_field10, dim_rnirawimport_field11, dim_rnirawimport_field12, dim_rnirawimport_field13, dim_rnirawimport_field14, dim_rnirawimport_field15, dim_rnirawimport_field16, dim_rnirawimport_field17, dim_rnirawimport_field18, dim_rnirawimport_field19, dim_rnirawimport_field20, dim_rnirawimport_field21, dim_rnirawimport_field22, dim_rnirawimport_field23, dim_rnirawimport_field24, dim_rnirawimport_field25, dim_rnirawimport_field26, dim_rnirawimport_field27, dim_rnirawimport_field28, dim_rnirawimport_field29, dim_rnirawimport_field30, dim_rnirawimport_field31, dim_rnirawimport_field32, dim_rnirawimport_field33, dim_rnirawimport_field34, dim_rnirawimport_field35, dim_rnirawimport_field36, dim_rnirawimport_field37, dim_rnirawimport_field38, dim_rnirawimport_field39, dim_rnirawimport_field40, dim_rnirawimport_field41, dim_rnirawimport_field42, dim_rnirawimport_field43, dim_rnirawimport_field44, dim_rnirawimport_field45, dim_rnirawimport_field46, dim_rnirawimport_field47, dim_rnirawimport_field48, dim_rnirawimport_field49, dim_rnirawimport_field50, dim_rnirawimport_field51, dim_rnirawimport_field52, dim_rnirawimport_field53, dim_rnirawimport_field54, dim_rnirawimport_field55, dim_rnirawimport_field56, dim_rnirawimport_field57, dim_rnirawimport_field58, dim_rnirawimport_field59, dim_rnirawimport_dateadded, dim_rnirawimport_lastmodified, dim_rnirawimport_lastmodifiedby, sid_rnirawimportstatus_id, dim_rnirawimport_processingenddate, RowHash
			from RN_CentralProcessingTables_BAK.dbo.RNIRawImport  
			WHERE sid_dbprocessinfo_dbnumber=@DBNumber
			
		set IDENTITY_INSERT RewardsNow.dbo.RNIRawImport  OFF	
	END

	


END
' 
END
GO
