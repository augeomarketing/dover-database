USE [RN_CentralProcessingTables_BAK]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadRNITransactionByDBNumber]    Script Date: 03/20/2015 15:16:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadRNITransactionByDBNumber]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadRNITransactionByDBNumber]
GO

USE [RN_CentralProcessingTables_BAK]
GO

/****** Object:  StoredProcedure [dbo].[usp_LoadRNITransactionByDBNumber]    Script Date: 03/20/2015 15:16:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Bob L.>
/*
	This proc takes a DBNumber pull records from either RNITransaction
*/
-- =============================================
CREATE PROCEDURE [dbo].[usp_LoadRNITransactionByDBNumber]
	@DBNumber varchar(3),
	@TransGTEDate date --used to pulls only RNITransaction records with a TransactionDate >= to it's value. SSIS package passes in 1/1/1900 by default
/* SAMPLE CALL
	declare @DBNumber varchar(3)='717'
	--exec usp_LoadRNITransactionByDBNumber @DBNumber
	
	select * from RN_CentralProcessingTables_BAK.dbo.RNITransaction where DBNumber=@DBNumber
	delete from RN_CentralProcessingTables_BAK.dbo.RNITransaction
*/	
AS
BEGIN

	DELETE from RN_CentralProcessingTables_BAK.dbo.RNITransaction 


	

	--get all of the transaction records for the FI >= the date passed in as the parameter @TransGTEDate
	BEGIN
		set IDENTITY_INSERT RN_CentralProcessingTables_BAK.dbo.RNITransaction ON
		
		insert into RN_CentralProcessingTables_BAK.dbo.RNITransaction
			(sid_RNITransaction_ID, sid_rnirawimport_id, dim_RNITransaction_TipPrefix, dim_RNITransaction_Portfolio, dim_RNITransaction_Member, dim_RNITransaction_PrimaryId, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, dim_RNITransaction_CardNumber, dim_RNITransaction_TransactionDate, dim_RNITransaction_TransferCard, dim_RNITransaction_TransactionCode, dim_RNITransaction_DDANumber, dim_RNITransaction_TransactionAmount, dim_RNITransaction_TransactionCount, dim_RNITransaction_TransactionDescription, dim_RNITransaction_CurrencyCode, dim_RNITransaction_MerchantID, dim_RNITransaction_TransactionID, dim_RNITransaction_AuthorizationCode, dim_RNITransaction_TransactionProcessingCode, dim_RNITransaction_PointsAwarded, sid_trantype_trancode, dim_RNITransaction_EffectiveDate, sid_localfi_history_id, sid_smstranstatus_id, dim_rnitransaction_merchantname, dim_rnitransaction_merchantaddress1, dim_rnitransaction_merchantaddress2, dim_rnitransaction_merchantaddress3, dim_rnitransaction_merchantcity, dim_rnitransaction_merchantstateregion, dim_rnitransaction_merchantecountrycode, dim_rnitransaction_merchantpostalcode, dim_rnitransaction_merchantcategorycode)
		select 
			sid_RNITransaction_ID, sid_rnirawimport_id, dim_RNITransaction_TipPrefix, dim_RNITransaction_Portfolio, dim_RNITransaction_Member, dim_RNITransaction_PrimaryId, dim_RNITransaction_RNIId, dim_RNITransaction_ProcessingCode, dim_RNITransaction_CardNumber, dim_RNITransaction_TransactionDate, dim_RNITransaction_TransferCard, dim_RNITransaction_TransactionCode, dim_RNITransaction_DDANumber, dim_RNITransaction_TransactionAmount, dim_RNITransaction_TransactionCount, dim_RNITransaction_TransactionDescription, dim_RNITransaction_CurrencyCode, dim_RNITransaction_MerchantID, dim_RNITransaction_TransactionID, dim_RNITransaction_AuthorizationCode, dim_RNITransaction_TransactionProcessingCode, dim_RNITransaction_PointsAwarded, sid_trantype_trancode,  dim_RNITransaction_EffectiveDate, sid_localfi_history_id, sid_smstranstatus_id, dim_rnitransaction_merchantname, dim_rnitransaction_merchantaddress1, dim_rnitransaction_merchantaddress2, dim_rnitransaction_merchantaddress3, dim_rnitransaction_merchantcity, dim_rnitransaction_merchantstateregion, dim_rnitransaction_merchantecountrycode, dim_rnitransaction_merchantpostalcode, dim_rnitransaction_merchantcategorycode
			from rewardsnow.dbo.RNITransaction  
			WHERE sid_dbprocessinfo_dbnumber=@DBNumber
			and dim_RNITransaction_TransactionDate >= @TransGTEDate
			
		set IDENTITY_INSERT RN_CentralProcessingTables_BAK.dbo.RNITransaction  OFF	
	END


	

	

END





GO


