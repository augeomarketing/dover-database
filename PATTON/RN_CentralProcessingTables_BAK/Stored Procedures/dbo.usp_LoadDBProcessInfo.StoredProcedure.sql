USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadDBProcessInfo]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadDBProcessInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadDBProcessInfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadDBProcessInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_LoadDBProcessInfo] 
AS
BEGIN
/*
exec usp_LoadDBProcessInfo
select * from RN_CentralProcessingTables_BAK.dbo.dbProcessInfo
*/

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.dbProcessInfo
 
 INSERT INTO RN_CentralProcessingTables_BAK.dbo.dbProcessInfo
	(DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, DateJoined, MinRedeemNeeded, MaxPointsPerYear, LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName, GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, Logo, Landing, TermsPage, FaqPage, EarnPage, Business, StatementDefault, StatementNum, CustomerServicePhone, ServerName, TransferHistToRn1, CalcDailyExpire, hasonlinebooking, AccessDevParticipant, VesdiaParticipant, MinCatalogPointValue, MaxCatalogPointValue, ExpPointsDisplayPeriodOffset, ExpPointsDisplay, sid_editaddress_id, SSOExactMatch, ShoppingFlingExportFile, MDTAutoUpdateLiabilitySent, ParseFirstName, LocalMerchantParticipant, OnlineOffersParticipant, LocalMerchantChainsParticipant)
	SELECT DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, DateJoined, MinRedeemNeeded, MaxPointsPerYear, LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName, GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, Logo, Landing, TermsPage, FaqPage, EarnPage, Business, StatementDefault, StatementNum, CustomerServicePhone, ServerName, TransferHistToRn1, CalcDailyExpire, hasonlinebooking, AccessDevParticipant, VesdiaParticipant, MinCatalogPointValue, MaxCatalogPointValue, ExpPointsDisplayPeriodOffset, ExpPointsDisplay, sid_editaddress_id, SSOExactMatch, ShoppingFlingExportFile, MDTAutoUpdateLiabilitySent, ParseFirstName, LocalMerchantParticipant, OnlineOffersParticipant, LocalMerchantChainsParticipant
		FROM RewardsNow.dbo.dbprocessinfo


END
' 
END
GO
