USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_Load_RN_RNICustomerByDBNumber_FromBAK]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_RNICustomerByDBNumber_FromBAK]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Load_RN_RNICustomerByDBNumber_FromBAK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Load_RN_RNICustomerByDBNumber_FromBAK]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
/*
	this proc loads RNICustomer for a particular DBNumber
*/

-- =============================================
CREATE PROCEDURE [dbo].[usp_Load_RN_RNICustomerByDBNumber_FromBAK]
	@DBNumber varchar(3)

/* SAMPLE CALL
	declare @DBNumber varchar(3)=''717''
	exec usp_Load_RN_RNICustomerByDBNumber_FromBAK @DBNumber
	
	select * from Rewardsnow.dbo.RNICustomer where sid_dbprocessinfo_dbnumber=''717''
	delete from RNICustomer
*/	
AS
BEGIN



DELETE from RewardsNow.dbo.RNICustomer where sid_dbprocessinfo_dbnumber=@DBNumber


set IDENTITY_INSERT RewardsNow.dbo.RNICustomer ON

insert into RewardsNow.dbo.RNICustomer
(dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_DateAdded, dim_RNICustomer_LastModified, sid_RNICustomer_ID, dim_RNICustomer_GUID,  dim_rnicustomer_periodsclosed)
select 
	dim_RNICustomer_TipPrefix, dim_RNICustomer_Portfolio, dim_RNICustomer_Member, dim_RNICustomer_PrimaryId, dim_RNICustomer_RNIId, dim_RNICustomer_PrimaryIndicator, dim_RNICustomer_Name1, dim_RNICustomer_Name2, dim_RNICustomer_Name3, dim_RNICustomer_Name4, dim_RNICustomer_Address1, dim_RNICustomer_Address2, dim_RNICustomer_Address3, dim_RNICustomer_City, dim_RNICustomer_StateRegion, dim_RNICustomer_CountryCode, dim_RNICustomer_PostalCode, dim_RNICustomer_PriPhone, dim_RNICustomer_PriMobilPhone, dim_RNICustomer_CustomerCode, dim_RNICustomer_BusinessFlag, dim_RNICustomer_EmployeeFlag, dim_RNICustomer_InstitutionID, dim_RNICustomer_CardNumber, dim_RNICustomer_EmailAddress, dim_RNICustomer_CustomerType, dim_RNICustomer_DateAdded, dim_RNICustomer_LastModified, sid_RNICustomer_ID, dim_RNICustomer_GUID,  dim_rnicustomer_periodsclosed
	from RN_CentralProcessingTables_BAK.dbo.RNICustomer  WHERE sid_dbprocessinfo_dbnumber=@DBNumber
set IDENTITY_INSERT RewardsNow.dbo.RNICustomer  OFF

END
' 
END
GO
