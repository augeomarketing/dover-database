USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  StoredProcedure [dbo].[usp_LoadFITables]    Script Date: 03/19/2015 13:04:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadFITables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LoadFITables]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LoadFITables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
/*
	this proc loads RNICustomer for a particular DBNumber
*/

-- =============================================
CREATE PROCEDURE [dbo].[usp_LoadFITables]
	@DBNumber varchar(3)

/* SAMPLE CALL
	declare @DBNumber varchar(3)=''717''
	exec usp_LoadFITables @DBNumber
	
	select * from Customer
	select * from Customer_stage
	select * from CustomerDeleted
	
	select * from Affiliat
	select * from Affiliat_stage
	select * from AffiliatDeleted
	
	select * from History
	select * from History_stage
	select * from HistoryDeleted	
		
	delete from RNICustomer
*/	
AS
BEGIN


	declare @DBNamePatton nvarchar(100), @sql nvarchar(2000)
	select @DBNamePatton=DBNamePatton FROM dbprocessinfo where DBNumber=@DBNumber



	/* BEGIN CUSTOMER TABLES*/
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.Customer
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.CUSTOMER_Stage
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.CUSTOMERdeleted

	--- load the Customer table
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.Customer
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	FROM ['' + @DBNamePatton + ''].dbo.Customer''
	print @SQL
	EXECUTE sp_executesql @sql
	------------------------------------------------------------
	---customer_stage
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.Customer_stage
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew)
	select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
	FROM ['' + @DBNamePatton + ''].dbo.Customer_stage''
	print @SQL
	EXECUTE sp_executesql @sql
	--------------------------------------------------------------
	--CustomerDeleted
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.CustomerDeleted
		(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
	select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted
	FROM ['' + @DBNamePatton + ''].dbo.CustomerDeleted''
	print @SQL
	EXECUTE sp_executesql @sql
	/* END CUSTOMER TABLES*/	
	/*******************************************************************************************************/
	/* BEGIN AFFILIAT TABLES*/
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.Affiliat
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.Affiliat_Stage
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.Affiliatdeleted	
	
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.Affiliat
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID )
	select 
		ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID 
	FROM ['' + @DBNamePatton + ''].dbo.Affiliat''
	print @SQL
	EXECUTE sp_executesql @sql	
	-------------------------------------------------------------------
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.Affiliat_stage
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select 
		ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID
	FROM ['' + @DBNamePatton + ''].dbo.Affiliat_stage''
	print @SQL
	EXECUTE sp_executesql @sql	
	-------------------------------------------------------------------
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.AffiliatDeleted
		(TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted)
	select 
		TipNumber, AcctType, DateAdded, SecID, AcctID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID, DateDeleted
	FROM ['' + @DBNamePatton + ''].dbo.AffiliatDeleted''
	print @SQL
	EXECUTE sp_executesql @sql	
	-------------------------------------------------------------------	
		
	
	/* END AFFILIAT TABLES*/
	/*******************************************************************************************************/
	/* BEGIN HISTORY TABLES*/
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.History
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.History_Stage
	TRUNCATE TABLE RN_CentralProcessingTables_BAK.dbo.Historydeleted	

	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.History
		(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
	select 
		TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
	FROM ['' + @DBNamePatton + ''].dbo.History''
	print @SQL
	EXECUTE sp_executesql @sql	
	-------------------------------------------------------------------
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.History_stage
		(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, sid_rnitransaction_id)
	select 
		TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, sid_rnitransaction_id
	FROM ['' + @DBNamePatton + ''].dbo.History_stage''
	print @SQL
	EXECUTE sp_executesql @sql	
	-------------------------------------------------------------------	
	set @sql=''insert into RN_CentralProcessingTables_BAK.dbo.HistoryDeleted
		(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	select 
		TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted
	FROM ['' + @DBNamePatton + ''].dbo.HistoryDeleted''
	print @SQL
	EXECUTE sp_executesql @sql	
	-------------------------------------------------------------------		
		
		
	/* END HISTORY TABLES*/
END
' 
END
GO
