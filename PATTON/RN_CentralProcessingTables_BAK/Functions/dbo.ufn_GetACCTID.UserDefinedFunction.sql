USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetACCTID]    Script Date: 03/19/2015 13:04:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_GetACCTID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_GetACCTID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_GetACCTID]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[ufn_GetACCTID] (@accttype VARCHAR(20) )
RETURNS @acctid TABLE (
	tipnumber VARCHAR(15)
	, accttype VARCHAR(255)
	, minacctid VARCHAR(255)
	, maxacctid VARCHAR(255)
	, allacctid VARCHAR(max)
)
AS
BEGIN
DECLARE @WHERE1 VARCHAR(255)
	, @WHERE2 VARCHAR(255)


-- IF ACCTTYPE IN = ''ALLCARDS'' THEN GET DEBIT AND CREDIT OTHERWISE GET INDIVIDUAL TYPES

IF @accttype = ''ALLCARDS''
BEGIN
	SET @WHERE1 = ''%DEBIT%''
	SET @WHERE2 = ''%CREDIT%''
END
ELSE IF @accttype LIKE ''%CREDIT%''
BEGIN
	SET @WHERE1 = ''%CREDIT%''
	SET @WHERE2 = ''%CREDIT%''
END 
ELSE IF @accttype LIKE ''%DEBIT%''
BEGIN
	SET @WHERE1 = ''%DEBIT%''
	SET @WHERE2 = ''%DEBIT%''
END
ELSE
BEGIN
	SET @WHERE1 = @ACCTTYPE + ''%''
	SET @WHERE2 = @ACCTTYPE + ''%''
END

INSERT INTO @acctid (TIPNUMBER, accttype, MINACCTID, MAXACCTID, ALLACCTID)

SELECT CST.TIPNUMBER, @ACCTTYPE, MINMAX.MINACCTID, MINMAX.MAXACCTID, ALLACCTID
FROM CUSTOMER CST
LEFT OUTER JOIN
(
	SELECT tipnumber, MIN(acctid) AS minacctid, MAX(acctid) as maxacctid
	FROM AFFILIAT
	WHERE AcctType LIKE @WHERE1
		OR AcctType LIKE @WHERE2
	GROUP BY TIPNUMBER
) AS MINMAX
ON CST.TIPNUMBER = MINMAX.TIPNUMBER
LEFT OUTER JOIN
(
	SELECT TIPNUMBER
		, [1]
		+ CASE WHEN [2] <> '''' THEN ''|'' + [2] ELSE '''' END
		+ CASE WHEN [3] <> '''' THEN ''|'' + [3] ELSE '''' END
		+ CASE WHEN [4] <> '''' THEN ''|'' + [4] ELSE '''' END
		+ CASE WHEN [5] <> '''' THEN ''|'' + [5] ELSE '''' END
		+ CASE WHEN [6] <> '''' THEN ''|'' + [6] ELSE '''' END
		+ CASE WHEN [7] <> '''' THEN ''|'' + [7] ELSE '''' END
		+ CASE WHEN [8] <> '''' THEN ''|'' + [8] ELSE '''' END 
		+ CASE WHEN [9] <> '''' THEN ''|'' + [9] ELSE '''' END 
		+ CASE WHEN [10] <> '''' THEN ''|'' + [10] ELSE '''' END AS ALLACCTID
	FROM
	(
		SELECT TIPNUMBER
			, ISNULL([1], '''') AS [1]
			, ISNULL([2], '''') AS [2]
			, ISNULL([3], '''') AS [3]
			, ISNULL([4], '''') AS [4]
			, ISNULL([5], '''') AS [5]
			, ISNULL([6], '''') AS [6]
			, ISNULL([7], '''') AS [7]
			, ISNULL([8], '''') AS [8]
			, ISNULL([9], '''') AS [9]
			, ISNULL([10], '''') AS [10]
		FROM
		(
			SELECT tipnumber, ACCTID, RANK() OVER (PARTITION BY TipNumber ORDER BY (ACCTID)) AS MyRank 
			FROM AFFILIAT
			WHERE AcctType LIKE @WHERE1
				OR ACCTTYPE LIKE @WHERE2
		) PVTSRC
		PIVOT
		(
			MIN(ACCTID) FOR MyRank IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10])
		) AS PVT
	) T1
) AT
ON CST.TIPNUMBER = AT.TIPNUMBER

RETURN;

END
' 
END
GO
