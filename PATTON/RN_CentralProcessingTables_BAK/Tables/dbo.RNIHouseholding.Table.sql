USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIHouseholding]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIHouseholding]') AND type in (N'U'))
DROP TABLE [dbo].[RNIHouseholding]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIHouseholding]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIHouseholding](
	[sid_dbprocessinfo_dbnumber] [varchar](10) NOT NULL,
	[dim_rnihouseholding_precedence] [int] NOT NULL,
	[dim_rnihouseholding_field01] [varchar](50) NOT NULL,
	[dim_rnihouseholding_field02] [varchar](50) NULL,
	[dim_rnihouseholding_field03] [varchar](50) NULL,
	[dim_rnihouseholding_field04] [varchar](50) NULL,
 CONSTRAINT [pk_RNIHouseholding__dbnumber_precedence] PRIMARY KEY CLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_rnihouseholding_precedence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
