USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIRawImport]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__RNIRawImp__dim_r__2AA05119]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIRawImport]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__2AA05119]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImport] DROP CONSTRAINT [DF__RNIRawImp__dim_r__2AA05119]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__RNIRawImp__sid_r__2B947552]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIRawImport]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__sid_r__2B947552]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImport] DROP CONSTRAINT [DF__RNIRawImp__sid_r__2B947552]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImport]') AND type in (N'U'))
DROP TABLE [dbo].[RNIRawImport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIRawImport](
	[sid_rnirawimport_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rniimportfiletype_id] [bigint] NULL,
	[dim_rnirawimport_source] [varchar](255) NOT NULL,
	[dim_rnirawimport_sourcerow] [int] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NULL,
	[dim_rnirawimport_field01] [varchar](max) NULL,
	[dim_rnirawimport_field02] [varchar](max) NULL,
	[dim_rnirawimport_field03] [varchar](max) NULL,
	[dim_rnirawimport_field04] [varchar](max) NULL,
	[dim_rnirawimport_field05] [varchar](max) NULL,
	[dim_rnirawimport_field06] [varchar](max) NULL,
	[dim_rnirawimport_field07] [varchar](max) NULL,
	[dim_rnirawimport_field08] [varchar](max) NULL,
	[dim_rnirawimport_field09] [varchar](max) NULL,
	[dim_rnirawimport_field10] [varchar](max) NULL,
	[dim_rnirawimport_field11] [varchar](max) NULL,
	[dim_rnirawimport_field12] [varchar](max) NULL,
	[dim_rnirawimport_field13] [varchar](max) NULL,
	[dim_rnirawimport_field14] [varchar](max) NULL,
	[dim_rnirawimport_field15] [varchar](max) NULL,
	[dim_rnirawimport_field16] [varchar](max) NULL,
	[dim_rnirawimport_field17] [varchar](max) NULL,
	[dim_rnirawimport_field18] [varchar](max) NULL,
	[dim_rnirawimport_field19] [varchar](max) NULL,
	[dim_rnirawimport_field20] [varchar](max) NULL,
	[dim_rnirawimport_field21] [varchar](max) NULL,
	[dim_rnirawimport_field22] [varchar](max) NULL,
	[dim_rnirawimport_field23] [varchar](max) NULL,
	[dim_rnirawimport_field24] [varchar](max) NULL,
	[dim_rnirawimport_field25] [varchar](max) NULL,
	[dim_rnirawimport_field26] [varchar](max) NULL,
	[dim_rnirawimport_field27] [varchar](max) NULL,
	[dim_rnirawimport_field28] [varchar](max) NULL,
	[dim_rnirawimport_field29] [varchar](max) NULL,
	[dim_rnirawimport_field30] [varchar](max) NULL,
	[dim_rnirawimport_field31] [varchar](max) NULL,
	[dim_rnirawimport_field32] [varchar](max) NULL,
	[dim_rnirawimport_field33] [varchar](max) NULL,
	[dim_rnirawimport_field34] [varchar](max) NULL,
	[dim_rnirawimport_field35] [varchar](max) NULL,
	[dim_rnirawimport_field36] [varchar](max) NULL,
	[dim_rnirawimport_field37] [varchar](max) NULL,
	[dim_rnirawimport_field38] [varchar](max) NULL,
	[dim_rnirawimport_field39] [varchar](max) NULL,
	[dim_rnirawimport_field40] [varchar](max) NULL,
	[dim_rnirawimport_field41] [varchar](max) NULL,
	[dim_rnirawimport_field42] [varchar](max) NULL,
	[dim_rnirawimport_field43] [varchar](max) NULL,
	[dim_rnirawimport_field44] [varchar](max) NULL,
	[dim_rnirawimport_field45] [varchar](max) NULL,
	[dim_rnirawimport_field46] [varchar](max) NULL,
	[dim_rnirawimport_field47] [varchar](max) NULL,
	[dim_rnirawimport_field48] [varchar](max) NULL,
	[dim_rnirawimport_field49] [varchar](max) NULL,
	[dim_rnirawimport_field50] [varchar](max) NULL,
	[dim_rnirawimport_field51] [varchar](max) NULL,
	[dim_rnirawimport_field52] [varchar](max) NULL,
	[dim_rnirawimport_field53] [varchar](max) NULL,
	[dim_rnirawimport_field54] [varchar](max) NULL,
	[dim_rnirawimport_field55] [varchar](max) NULL,
	[dim_rnirawimport_field56] [varchar](max) NULL,
	[dim_rnirawimport_field57] [varchar](max) NULL,
	[dim_rnirawimport_field58] [varchar](max) NULL,
	[dim_rnirawimport_field59] [varchar](max) NULL,
	[dim_rnirawimport_dateadded] [datetime] NOT NULL,
	[dim_rnirawimport_lastmodified] [datetime] NULL,
	[dim_rnirawimport_lastmodifiedby] [varchar](50) NULL,
	[sid_rnirawimportstatus_id] [bigint] NOT NULL,
	[dim_rnirawimport_processingenddate] [date] NULL,
	[RowHash] [varbinary](8000) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimport_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__RNIRawImp__dim_r__2AA05119]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIRawImport]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__dim_r__2AA05119]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImport] ADD  DEFAULT (getdate()) FOR [dim_rnirawimport_dateadded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__RNIRawImp__sid_r__2B947552]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIRawImport]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIRawImp__sid_r__2B947552]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIRawImport] ADD  DEFAULT ((0)) FOR [sid_rnirawimportstatus_id]
END


End
GO
