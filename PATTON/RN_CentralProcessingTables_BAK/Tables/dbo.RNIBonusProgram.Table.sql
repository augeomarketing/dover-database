USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProgram]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgram]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgram]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgram]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProgram](
	[sid_rnibonusprogram_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_rnibonusprogram_description] [varchar](255) NOT NULL,
	[dim_rnibonusprogram_effectivedate] [date] NOT NULL,
	[dim_rnibonusprogram_expirationdate] [date] NOT NULL,
	[sid_rnibonusprogramtype_code] [varchar](1) NOT NULL,
	[sid_trantype_tranCode] [nvarchar](2) NOT NULL,
	[dim_rnibonusprogram_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogram_lastupdated] [datetime] NOT NULL,
	[dim_rnibonusprogram_maxawards] [tinyint] NOT NULL,
 CONSTRAINT [PK_RNIBonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogram_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
