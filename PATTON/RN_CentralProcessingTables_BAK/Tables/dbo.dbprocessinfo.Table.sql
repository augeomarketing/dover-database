USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[dbprocessinfo]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]') AND type in (N'U'))
DROP TABLE [dbo].[dbprocessinfo]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[dbprocessinfo](
	[DBNumber] [varchar](50) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[DBAvailable] [char](1) NOT NULL,
	[ClientCode] [varchar](50) NULL,
	[ClientName] [varchar](256) NULL,
	[ProgramName] [varchar](256) NULL,
	[DBLocationPatton] [varchar](50) NULL,
	[DBLocationNexl] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[DateJoined] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[LastTipNumberUsed] [varchar](15) NULL,
	[PointsExpireFrequencyCd] [varchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[WelcomeKitGroupName] [varchar](50) NULL,
	[GenerateWelcomeKit] [varchar](1) NOT NULL,
	[sid_FiProdStatus_statuscode] [varchar](1) NULL,
	[IsStageModel] [int] NULL,
	[ExtractGiftCards] [varchar](1) NOT NULL,
	[ExtractCashBack] [varchar](1) NOT NULL,
	[RNProgramName] [varchar](50) NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [int] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Landing] [varchar](255) NULL,
	[TermsPage] [varchar](50) NULL,
	[FaqPage] [varchar](20) NULL,
	[EarnPage] [varchar](20) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StatementNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL,
	[ServerName] [varchar](50) NULL,
	[TransferHistToRn1] [varchar](1) NOT NULL,
	[CalcDailyExpire] [varchar](1) NOT NULL,
	[hasonlinebooking] [int] NOT NULL,
	[AccessDevParticipant] [varchar](1) NOT NULL,
	[VesdiaParticipant] [varchar](1) NOT NULL,
	[MinCatalogPointValue] [int] NOT NULL,
	[MaxCatalogPointValue] [int] NOT NULL,
	[ExpPointsDisplayPeriodOffset] [int] NULL,
	[ExpPointsDisplay] [int] NULL,
	[sid_editaddress_id] [int] NOT NULL,
	[SSOExactMatch] [int] NOT NULL,
	[ShoppingFlingExportFile] [varchar](1) NOT NULL,
	[MDTAutoUpdateLiabilitySent] [varchar](1) NOT NULL,
	[ParseFirstName] [varchar](1) NOT NULL,
	[LocalMerchantParticipant] [varchar](1) NULL,
	[OnlineOffersParticipant] [varchar](1) NULL,
	[LocalMerchantChainsParticipant] [varchar](1) NULL,
 CONSTRAINT [PK_dbprocessinfo_1] PRIMARY KEY CLUSTERED 
(
	[DBNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
