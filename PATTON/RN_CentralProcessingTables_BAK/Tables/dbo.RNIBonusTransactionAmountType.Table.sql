USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusTransactionAmountType]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusTransactionAmountType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusTransactionAmountType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusTransactionAmountType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusTransactionAmountType](
	[sid_rnibonustransactionamounttype_code] [varchar](1) NOT NULL,
	[dim_rnibonustransactionamounttype_name] [varchar](255) NOT NULL,
	[dim_rnibonustransactionamounttype_dateadded] [datetime] NULL,
	[dim_rnibonustransactionamounttype_lastmodified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnibonustransactionamounttype_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
