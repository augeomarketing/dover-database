USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProgramType]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProgramType](
	[sid_rnibonusprogramtype_code] [varchar](1) NOT NULL,
	[dim_rnibonusprogramtype_description] [varchar](255) NOT NULL,
	[dim_rnibonusprogramtype_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramtype_lastupdated] [datetime] NOT NULL,
 CONSTRAINT [PK_RNIBonusProgramType] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramtype_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
