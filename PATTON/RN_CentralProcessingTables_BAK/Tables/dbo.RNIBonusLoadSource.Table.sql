USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusLoadSource]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadSource]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusLoadSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusLoadSource](
	[sid_rnibonusloadsource_id] [bigint] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_rnibonusloadsource_sourcename] [varchar](100) NOT NULL,
	[dim_rnibonusloadsource_fileload] [int] NOT NULL,
	[sid_rnibonusprocesstype_id] [bigint] NOT NULL,
	[sid_rnibonustransactionamounttype_code] [varchar](1) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
