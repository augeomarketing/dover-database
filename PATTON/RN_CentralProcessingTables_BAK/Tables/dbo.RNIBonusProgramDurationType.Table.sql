USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProgramDurationType]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramDurationType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramDurationType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramDurationType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProgramDurationType](
	[sid_rnibonusprogramdurationtype_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_rnibonusprogramdurationtype_description] [varchar](255) NOT NULL,
	[dim_rnibonusprogramdurationtype_shortcode] [varchar](3) NOT NULL,
	[dim_rnibonusprogramdurationtype_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramdurationtype_lastmodified] [datetime] NULL,
	[dim_rnibonusprogramdurationtype_lastupdatedby] [varchar](50) NULL,
 CONSTRAINT [PK__RNIBonus__EB98A14C467F0C66] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramdurationtype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
