USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIRawImportDataDefinitionWhereClause]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportDataDefinitionWhereClause]') AND type in (N'U'))
DROP TABLE [dbo].[RNIRawImportDataDefinitionWhereClause]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportDataDefinitionWhereClause]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIRawImportDataDefinitionWhereClause](
	[sid_rnirawimportdatadefinitionwhereclause_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_rnirawimportdatadefinitiontype_id] [int] NOT NULL,
	[sid_rniimportfiletype_id] [int] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](3) NOT NULL,
	[dim_rnirawimportdatadefinition_version] [int] NOT NULL,
	[dim_rnirawimportdatadefinitionwhereclause_whereclause] [varchar](max) NULL,
	[dim_rnirawimportdatadefinitionwhereclause_dateadded] [datetime] NOT NULL,
	[dim_rnirawimportdatadefinitionwhereclause_lastmodified] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimportdatadefinitionwhereclause_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
