USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNICustomerLoadSource]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadSource]') AND type in (N'U'))
DROP TABLE [dbo].[RNICustomerLoadSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNICustomerLoadSource](
	[sid_rnicustomerloadsource_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_rnicustomerloadsource_sourcename] [varchar](100) NOT NULL,
	[dim_rnicustomerloadsource_fileload] [int] NOT NULL,
	[dim_rnicustomerloadsource_dedupe] [int] NOT NULL,
 CONSTRAINT [PK__RNICusto__10CB059A0D1D3D2F] PRIMARY KEY CLUSTERED 
(
	[sid_rnicustomerloadsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [unq_rnicustomerloadsource__dbnumber__sourcename] UNIQUE NONCLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_rnicustomerloadsource_sourcename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
