USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNITransactionLoadColumn]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionLoadColumn]') AND type in (N'U'))
DROP TABLE [dbo].[RNITransactionLoadColumn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionLoadColumn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNITransactionLoadColumn](
	[sid_rnitransactionloadcolumn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnitransactionloadsource_id] [bigint] NOT NULL,
	[dim_rnitransactionloadcolumn_sourcecolumn] [varchar](100) NULL,
	[dim_rnitransactionloadcolumn_targetcolumn] [varchar](100) NULL,
	[dim_rnitransactionloadcolumn_fidbhistcolumn] [varchar](100) NULL,
	[dim_rnitransactionloadcolumn_required] [int] NULL,
	[dim_rnitransactionloadcolumn_xrefcolumn] [varchar](100) NULL,
	[sid_rnicustomerloadcolumn_id] [bigint] NULL,
	[dim_rnicustomerloadcolumn_targetcolumn] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnitransactionloadcolumn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
