USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProgramCriteria]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramCriteria]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramCriteria]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramCriteria]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProgramCriteria](
	[sid_rnibonusprogramcriteria_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnibonusprogramfi_id] [bigint] NULL,
	[dim_rnibonusprogramcriteria_code01] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code02] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code03] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code04] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code05] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code06] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code07] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code08] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code09] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_code10] [varchar](max) NULL,
	[dim_rnibonusprogramcriteria_effectivedate] [date] NULL,
	[dim_rnibonusprogramcriteria_expirationdate] [date] NULL,
	[dim_rnibonusprogramcriteria_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramcriteria_lastupdated] [datetime] NOT NULL,
 CONSTRAINT [PK_RNIBonusProgramCriteria] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramcriteria_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
