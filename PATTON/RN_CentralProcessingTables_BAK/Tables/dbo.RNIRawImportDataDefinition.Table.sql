USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIRawImportDataDefinition]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportDataDefinition]') AND type in (N'U'))
DROP TABLE [dbo].[RNIRawImportDataDefinition]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIRawImportDataDefinition]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIRawImportDataDefinition](
	[sid_rnirawimportdatadefinition_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnirawimportdatadefinitiontype_id] [bigint] NULL,
	[sid_rniimportfiletype_id] [bigint] NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_outputfield] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_outputdatatype] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_sourcefield] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_startindex] [int] NOT NULL,
	[dim_rnirawimportdatadefinition_length] [int] NOT NULL,
	[dim_rnirawimportdatadefinition_multipart] [bit] NOT NULL,
	[dim_rnirawimportdatadefinition_multipartpart] [tinyint] NOT NULL,
	[dim_rnirawimportdatadefinition_multipartlength] [tinyint] NOT NULL,
	[dim_rnirawimportdatadefinition_dateadded] [datetime] NOT NULL,
	[dim_rnirawimportdatadefinition_lastmodified] [datetime] NULL,
	[dim_rnirawimportdatadefinition_lastmodifiedby] [varchar](50) NULL,
	[dim_rnirawimportdatadefinition_version] [int] NOT NULL,
	[dim_rnirawimportdatadefinition_applyfunction] [varchar](100) NULL,
	[dim_rnirawimportdatadefinition_defaultValue] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnirawimportdatadefinition_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
