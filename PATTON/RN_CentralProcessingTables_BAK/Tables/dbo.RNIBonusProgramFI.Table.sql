USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProgramFI]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramFI]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramFI]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramFI]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProgramFI](
	[sid_rnibonusprogramfi_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_rnibonusprogram_id] [int] NOT NULL,
	[dim_rnibonusprogramfi_effectivedate] [date] NOT NULL,
	[dim_rnibonusprogramfi_expirationdate] [date] NOT NULL,
	[dim_rnibonusprogramfi_bonuspoints] [int] NOT NULL,
	[dim_rnibonusprogramfi_pointmultiplier] [numeric](5, 2) NOT NULL,
	[sid_rnibonusprogramdurationtype_id] [int] NOT NULL,
	[dim_rnibonusprogramfi_duration] [int] NOT NULL,
	[dim_rnibonusprogramfi_storedprocedure] [nvarchar](255) NULL,
	[dim_rnibonusprogramfi_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramfi_lastupdated] [datetime] NULL,
	[dim_rnibonusprogramfi_lastupdatedby] [varchar](50) NULL,
 CONSTRAINT [PK_RNIBonusProgramFI] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramfi_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
