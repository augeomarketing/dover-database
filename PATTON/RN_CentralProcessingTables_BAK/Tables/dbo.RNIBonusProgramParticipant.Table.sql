USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProgramParticipant]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramParticipant]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramParticipant]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramParticipant]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProgramParticipant](
	[sid_customer_tipnumber] [varchar](15) NOT NULL,
	[sid_rnibonusprogramfi_id] [int] NOT NULL,
	[dim_rnibonusprogramparticipant_isbonusawarded] [bit] NOT NULL,
	[dim_rnibonusprogramparticipant_dateawarded] [date] NULL,
	[dim_rnibonusprogramparticipant_remainingpointstoearn] [int] NOT NULL,
	[dim_rnibonusprogramparticipant_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramparticipant_lastupdated] [datetime] NULL,
	[dim_rnibonusprogramparticipant_lastupdatedby] [varchar](50) NULL,
	[sid_rnibonusprogramparticipant_id] [bigint] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramparticipant_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
