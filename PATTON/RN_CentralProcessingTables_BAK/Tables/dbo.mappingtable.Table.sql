USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[mappingtable]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingtable]') AND type in (N'U'))
DROP TABLE [dbo].[mappingtable]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingtable]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mappingtable](
	[sid_mappingtable_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingtable_tablename] [varchar](50) NOT NULL,
	[dim_mappingtable_active] [int] NOT NULL,
	[dim_mappingtable_created] [datetime] NOT NULL,
	[dim_mappingtable_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingtable] PRIMARY KEY CLUSTERED 
(
	[sid_mappingtable_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
