USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[rniTrantypeXref]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rniTrantypeXref]') AND type in (N'U'))
DROP TABLE [dbo].[rniTrantypeXref]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rniTrantypeXref]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[rniTrantypeXref](
	[sid_rniTrantypeXref_id] [int] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_trantype_trancode] [varchar](2) NULL,
	[dim_rnitrantypexref_code01] [varchar](128) NULL,
	[dim_rnitrantypexref_code02] [varchar](128) NULL,
	[dim_rnitrantypexref_code03] [varchar](128) NULL,
	[dim_rnitrantypexref_code04] [varchar](128) NULL,
	[dim_rnitrantypexref_code05] [varchar](128) NULL,
	[dim_rnitrantypexref_code06] [varchar](128) NULL,
	[dim_rnitrantypexref_code07] [varchar](128) NULL,
	[dim_rnitrantypexref_code08] [varchar](128) NULL,
	[dim_rnitrantypexref_code09] [varchar](128) NULL,
	[dim_rnitrantypexref_code10] [varchar](128) NULL,
	[dim_rnitrantypexref_active] [int] NOT NULL,
	[dim_rnitrantypexref_created] [datetime] NOT NULL,
	[dim_rnitrantypexref_lastmodified] [datetime] NOT NULL,
	[dim_rnitrantypexref_lastmodifiedby] [varchar](50) NULL,
	[dim_rnitrantypexref_factor] [numeric](6, 3) NULL,
	[dim_rnitrantypexref_fixedpoints] [int] NOT NULL,
	[dim_rnitrantypexref_conversiontype] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rniTrantypeXref_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
