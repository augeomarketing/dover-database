USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[mappingdefinition]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingdefinition]') AND type in (N'U'))
DROP TABLE [dbo].[mappingdefinition]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingdefinition]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mappingdefinition](
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_mappingtable_id] [bigint] NOT NULL,
	[sid_mappingdestination_id] [bigint] NOT NULL,
	[sid_mappingsource_id] [bigint] NOT NULL,
	[dim_mappingdefinition_active] [int] NOT NULL,
	[dim_mappingdefinition_created] [datetime] NOT NULL,
	[dim_mappingdefinition_lastmodified] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
