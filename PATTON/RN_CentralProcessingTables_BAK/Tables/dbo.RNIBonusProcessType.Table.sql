USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusProcessType]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProcessType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProcessType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProcessType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusProcessType](
	[sid_rnibonusprocesstype_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_rnibonusprocesstype_name] [varchar](255) NOT NULL,
	[dim_rnibonusprocesstype_active] [int] NOT NULL,
	[dim_rnibonusprocesstype_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprocesstype_lastmodified] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprocesstype_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
