USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[mappingdestination]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingdestination]') AND type in (N'U'))
DROP TABLE [dbo].[mappingdestination]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingdestination]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mappingdestination](
	[sid_mappingdestination_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingdestination_columnname] [varchar](50) NOT NULL,
	[dim_mappingdestination_active] [int] NOT NULL,
	[dim_mappingdestination_created] [datetime] NOT NULL,
	[dim_mappingdestination_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingdestination] PRIMARY KEY CLUSTERED 
(
	[sid_mappingdestination_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
