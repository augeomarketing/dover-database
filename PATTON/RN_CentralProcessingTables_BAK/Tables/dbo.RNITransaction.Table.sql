USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNITransaction]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransaction]') AND type in (N'U'))
DROP TABLE [dbo].[RNITransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNITransaction](
	[sid_RNITransaction_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnirawimport_id] [bigint] NOT NULL,
	[dim_RNITransaction_TipPrefix] [varchar](3) NOT NULL,
	[dim_RNITransaction_Portfolio] [varchar](255) NULL,
	[dim_RNITransaction_Member] [varchar](255) NULL,
	[dim_RNITransaction_PrimaryId] [varchar](255) NULL,
	[dim_RNITransaction_RNIId] [varchar](15) NULL,
	[dim_RNITransaction_ProcessingCode] [int] NOT NULL,
	[dim_RNITransaction_CardNumber] [varchar](16) NULL,
	[dim_RNITransaction_TransactionDate] [datetime] NULL,
	[dim_RNITransaction_TransferCard] [varchar](16) NULL,
	[dim_RNITransaction_TransactionCode] [int] NOT NULL,
	[dim_RNITransaction_DDANumber] [varchar](20) NULL,
	[dim_RNITransaction_TransactionAmount] [decimal](18, 2) NULL,
	[dim_RNITransaction_TransactionCount] [int] NULL,
	[dim_RNITransaction_TransactionDescription] [varchar](255) NULL,
	[dim_RNITransaction_CurrencyCode] [varchar](3) NULL,
	[dim_RNITransaction_MerchantID] [varchar](50) NULL,
	[dim_RNITransaction_TransactionID] [varchar](50) NULL,
	[dim_RNITransaction_AuthorizationCode] [varchar](6) NULL,
	[dim_RNITransaction_TransactionProcessingCode] [int] NULL,
	[dim_RNITransaction_PointsAwarded] [int] NULL,
	[sid_trantype_trancode] [nvarchar](2) NULL,
	[sid_dbprocessinfo_dbnumber]  AS ([dim_RNITransaction_TipPrefix]),
	[dim_RNITransaction_EffectiveDate] [date] NULL,
	[sid_localfi_history_id] [bigint] NULL,
	[sid_smstranstatus_id] [int] NOT NULL,
	[dim_rnitransaction_merchantname] [varchar](40) NULL,
	[dim_rnitransaction_merchantaddress1] [varchar](40) NULL,
	[dim_rnitransaction_merchantaddress2] [varchar](40) NULL,
	[dim_rnitransaction_merchantaddress3] [varchar](40) NULL,
	[dim_rnitransaction_merchantcity] [varchar](40) NULL,
	[dim_rnitransaction_merchantstateregion] [varchar](3) NULL,
	[dim_rnitransaction_merchantecountrycode] [varchar](3) NULL,
	[dim_rnitransaction_merchantpostalcode] [varchar](20) NULL,
	[dim_rnitransaction_merchantcategorycode] [varchar](4) NULL,
 CONSTRAINT [PK__RNITrans__BF55D9862395C2DC] PRIMARY KEY CLUSTERED 
(
	[sid_RNITransaction_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
