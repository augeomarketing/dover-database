USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[mappingsource]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingsource]') AND type in (N'U'))
DROP TABLE [dbo].[mappingsource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[mappingsource]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[mappingsource](
	[sid_mappingsource_id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_mappingsource_description] [varchar](max) NOT NULL,
	[sid_mappingtable_id] [bigint] NULL,
	[sid_mappingsourcetype_id] [bigint] NULL,
	[dim_mappingsource_source] [varchar](50) NULL,
	[dim_mappingsource_active] [int] NOT NULL,
	[dim_mappingsource_created] [datetime] NOT NULL,
	[dim_mappingsource_lastmodified] [datetime] NULL,
 CONSTRAINT [PK_mappingsource] PRIMARY KEY CLUSTERED 
(
	[sid_mappingsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
