USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNICustomerLoadColumn]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadColumn]') AND type in (N'U'))
DROP TABLE [dbo].[RNICustomerLoadColumn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomerLoadColumn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNICustomerLoadColumn](
	[sid_rnicustomerloadcolumn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnicustomerloadsource_id] [bigint] NOT NULL,
	[dim_rnicustomerloadcolumn_sourcecolumn] [varchar](100) NULL,
	[dim_rnicustomerloadcolumn_targetcolumn] [varchar](100) NULL,
	[dim_rnicustomerloadcolumn_fidbcustcolumn] [varchar](100) NULL,
	[dim_rnicustomerloadcolumn_keyflag] [tinyint] NOT NULL,
	[dim_rnicustomerloadcolumn_loadtoaffiliat] [tinyint] NOT NULL,
	[dim_rnicustomerloadcolumn_affiliataccttype] [varchar](100) NULL,
	[dim_rnicustomerloadcolumn_primaryorder] [tinyint] NOT NULL,
	[dim_rnicustomerloadcolumn_primarydescending] [tinyint] NOT NULL,
	[dim_rnicustomerloadcolumn_required] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnicustomerloadcolumn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
