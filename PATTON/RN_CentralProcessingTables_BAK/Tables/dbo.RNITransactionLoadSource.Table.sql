USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNITransactionLoadSource]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionLoadSource]') AND type in (N'U'))
DROP TABLE [dbo].[RNITransactionLoadSource]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNITransactionLoadSource]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNITransactionLoadSource](
	[sid_rnitransactionloadsource_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_rnitransactionloadsource_sourcename] [varchar](100) NOT NULL,
	[dim_rnitransactionloadsource_fileload] [int] NOT NULL,
 CONSTRAINT [PK__RNITrans__3F5C9C2230EFBDFA] PRIMARY KEY CLUSTERED 
(
	[sid_rnitransactionloadsource_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY],
 CONSTRAINT [unq_rnitransactionloadsource__dbnumber__sourcename] UNIQUE NONCLUSTERED 
(
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_rnitransactionloadsource_sourcename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
