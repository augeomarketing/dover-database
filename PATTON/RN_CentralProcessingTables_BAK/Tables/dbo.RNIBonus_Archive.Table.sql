USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonus_Archive]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonus_Archive]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonus_Archive]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonus_Archive]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonus_Archive](
	[sid_rnibonus_id] [bigint] NOT NULL,
	[sid_rnirawimport_id] [bigint] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_rnibonusprocesstype_id] [bigint] NOT NULL,
	[dim_rnibonus_portfolio] [varchar](255) NULL,
	[dim_rnibonus_member] [varchar](255) NULL,
	[dim_rnibonus_primaryid] [varchar](255) NULL,
	[dim_rnibonus_RNIID] [varchar](15) NULL,
	[dim_rnibonus_processingcode] [int] NOT NULL,
	[dim_rnibonus_cardnumber] [varchar](16) NOT NULL,
	[dim_rnibonus_transactionamount] [decimal](18, 2) NOT NULL,
	[dim_rnibonus_bonuspoints] [int] NOT NULL,
	[dim_rnibonus_processingenddate] [date] NOT NULL,
	[sid_trantype_trancode] [nvarchar](2) NOT NULL,
	[dim_rnibonusarchive_dateadded] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_rnibonus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
