USE [RN_CentralProcessingTables_BAK]
GO
/****** Object:  Table [dbo].[RNIBonusLoadColumn]    Script Date: 03/19/2015 13:04:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadColumn]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusLoadColumn]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusLoadColumn]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RNIBonusLoadColumn](
	[sid_rnibonusloadcolumn_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnibonusloadsource_id] [bigint] NOT NULL,
	[dim_rnibonusloadcolumn_sourcecolumn] [varchar](100) NOT NULL,
	[dim_rnibonusloadcolumn_targetcolumn] [varchar](100) NOT NULL,
	[dim_rnibonusloadcolumn_required] [int] NULL,
	[sid_rnicustomerloadsource_id] [bigint] NULL,
	[dim_rnicustomerloadcolumn_targetcolumn] [varchar](100) NULL,
	[dim_rnibonusloadcolumn_xrefcolumn] [varchar](100) NULL,
 CONSTRAINT [PK__RNIBonus__F726C64A1FD280F5] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusloadcolumn_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
