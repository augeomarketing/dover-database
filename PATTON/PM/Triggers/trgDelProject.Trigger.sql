USE [PM]
GO
/****** Object:  Trigger [trgDelProject]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[trgDelProject]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[trgDelProject]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[trgDelProject]') AND OBJECTPROPERTY(id, N'IsTrigger') = 1)
EXEC dbo.sp_executesql @statement = N'CREATE TRIGGER [dbo].[trgDelProject] ON [dbo].[tProjects] 
FOR DELETE 
AS
declare @ProjectID int
select @ProjectID=ProjectID from Deleted
delete tCustomerProjects where ProjectID=@ProjectID
'
GO
