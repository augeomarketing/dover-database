USE [PM]
GO
/****** Object:  Table [dbo].[tProjectTeams]    Script Date: 02/23/2010 10:47:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tProjectTeams_IsAdmin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tProjectTeams] DROP CONSTRAINT [DF_tProjectTeams_IsAdmin]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tProjectTeams]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tProjectTeams]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tProjectTeams]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tProjectTeams](
	[CustomerID] [int] NOT NULL,
	[ProjectID] [int] NOT NULL,
	[TeamMemberUserID] [int] NOT NULL,
	[IsProjectAdmin] [int] NOT NULL CONSTRAINT [DF_tProjectTeams_IsAdmin]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
