USE [PM]
GO
/****** Object:  Table [dbo].[tTaskUsers]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tTaskUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tTaskUsers](
	[CustomerID] [int] NOT NULL,
	[TaskID] [int] NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]
END
GO
