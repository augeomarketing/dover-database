USE [PM]
GO
/****** Object:  Table [dbo].[tUsers]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tUsers](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](40) NOT NULL,
	[UserFName] [varchar](25) NULL,
	[UserLName] [varchar](25) NULL,
	[UserPW] [varchar](10) NOT NULL,
	[SecLvl] [int] NOT NULL,
	[email] [varchar](50) NOT NULL,
	[LastUpdated] [datetime] NULL,
	[IsActive] [int] NOT NULL,
	[IsClient] [int] NOT NULL,
 CONSTRAINT [PK_tUsers] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
