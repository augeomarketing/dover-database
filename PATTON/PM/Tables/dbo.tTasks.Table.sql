USE [PM]
GO
/****** Object:  Table [dbo].[tTasks]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tTasks_IsTaskLeadMailed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tTasks] DROP CONSTRAINT [DF_tTasks_IsTaskLeadMailed]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTasks]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tTasks]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTasks]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tTasks](
	[TaskID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NULL,
	[CustomerID] [int] NULL,
	[TaskName] [varchar](100) NULL,
	[TaskLeadUserID] [int] NULL,
	[IsTaskLeadMailed] [int] NULL CONSTRAINT [DF_tTasks_IsTaskLeadMailed]  DEFAULT ((1)),
	[TaskTypeID] [int] NULL,
	[TaskStartDate] [datetime] NULL,
	[TaskEndDate] [datetime] NULL,
	[TaskStatusID] [int] NULL,
	[PctComplete] [varchar](3) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserID] [int] NULL,
	[RTLink] [varchar](100) NULL,
	[TicketUpdated] [datetime] NULL,
 CONSTRAINT [PK_tTasks] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
