USE [PM]
GO
/****** Object:  Table [dbo].[tEmailHistory]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tEmailHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tEmailHistory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tEmailHistory]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tEmailHistory](
	[MailID] [int] IDENTITY(1,1) NOT NULL,
	[MailDate] [datetime] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[SenderUserID] [int] NOT NULL,
	[MailType] [varchar](20) NOT NULL,
	[MailTypePKID] [int] NOT NULL,
	[MailTo] [varchar](1000) NOT NULL,
	[MsgSubj] [varchar](200) NULL,
	[MsgBody] [varchar](3000) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
