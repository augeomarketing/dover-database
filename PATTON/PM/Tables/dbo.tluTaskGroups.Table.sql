USE [PM]
GO
/****** Object:  Table [dbo].[tluTaskGroups]    Script Date: 02/23/2010 10:47:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tluTaskGroups]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tluTaskGroups]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tluTaskGroups]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tluTaskGroups](
	[TaskGroupID] [int] IDENTITY(1,1) NOT NULL,
	[TaskGroup] [varchar](30) NOT NULL,
	[CustomerID] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
