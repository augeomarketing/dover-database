USE [PM]
GO
/****** Object:  Table [dbo].[tCustomerProjects]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCustomerProjects]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tCustomerProjects]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCustomerProjects]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tCustomerProjects](
	[CPID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ProjectID] [int] NOT NULL,
 CONSTRAINT [PK_tCustomerProjects] PRIMARY KEY CLUSTERED 
(
	[CPID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM dbo.sysindexes WHERE id = OBJECT_ID(N'[dbo].[tCustomerProjects]') AND name = N'IX_tCustomerProjects')
CREATE UNIQUE NONCLUSTERED INDEX [IX_tCustomerProjects] ON [dbo].[tCustomerProjects] 
(
	[CPID] ASC
) ON [PRIMARY]
GO
