USE [PM]
GO
/****** Object:  Table [dbo].[tluDetailTypes]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tluDetailTypes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tluDetailTypes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tluDetailTypes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tluDetailTypes](
	[DetailTypeID] [int] IDENTITY(1,1) NOT NULL,
	[DetailType] [varchar](30) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[SortOrder] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
