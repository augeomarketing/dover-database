USE [PM]
GO
/****** Object:  Table [dbo].[tCustomerUsers]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCustomerUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tCustomerUsers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCustomerUsers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tCustomerUsers](
	[CUID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[IsCustomerAdmin] [int] NOT NULL,
	[SecLvl] [int] NULL,
 CONSTRAINT [PK_tCustomerUsers] PRIMARY KEY CLUSTERED 
(
	[CUID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
