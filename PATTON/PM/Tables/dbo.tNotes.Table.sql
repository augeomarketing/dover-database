USE [PM]
GO
/****** Object:  Table [dbo].[tNotes]    Script Date: 02/23/2010 10:47:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tNotes_NoteDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tNotes] DROP CONSTRAINT [DF_tNotes_NoteDate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tNotes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tNotes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tNotes]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tNotes](
	[NoteID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[FKID] [int] NOT NULL,
	[FKName] [varchar](25) NULL,
	[NoteDate] [datetime] NULL CONSTRAINT [DF_tNotes_NoteDate]  DEFAULT (getdate()),
	[Note] [varchar](2000) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
