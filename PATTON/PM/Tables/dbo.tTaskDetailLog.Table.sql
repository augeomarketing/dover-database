USE [PM]
GO
/****** Object:  Table [dbo].[tTaskDetailLog]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskDetailLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tTaskDetailLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskDetailLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tTaskDetailLog](
	[TaskDetailID] [int] NOT NULL,
	[TaskID] [int] NULL,
	[CustomerID] [int] NULL,
	[NoteDate] [datetime] NOT NULL,
	[TaskDetailNote] [varchar](2000) NOT NULL,
	[DetailStatus] [varchar](30) NULL,
	[DetailUserName] [varchar](40) NULL,
	[DetailQty] [float] NULL,
	[DetailType] [varchar](30) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserID] [int] NULL,
	[FormMethod] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
