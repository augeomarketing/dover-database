USE [PM]
GO
/****** Object:  Table [dbo].[tTaskDetail]    Script Date: 02/23/2010 10:47:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tTaskDetail_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tTaskDetail] DROP CONSTRAINT [DF_tTaskDetail_LastUpdated]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskDetail]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tTaskDetail]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskDetail]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tTaskDetail](
	[TaskDetailID] [int] IDENTITY(1,1) NOT NULL,
	[TaskID] [int] NULL,
	[CustomerID] [int] NULL,
	[NoteDate] [datetime] NOT NULL,
	[TaskDetailNote] [varchar](2000) NOT NULL,
	[DetailStatusID] [int] NULL,
	[DetailUserId] [int] NULL,
	[DetailQty] [float] NULL,
	[DetailTypeID] [int] NULL,
	[LastUpdated] [datetime] NULL CONSTRAINT [DF_tTaskDetail_LastUpdated]  DEFAULT (getdate()),
	[LastUserID] [int] NULL,
 CONSTRAINT [PK_tTaskDetail] PRIMARY KEY CLUSTERED 
(
	[TaskDetailID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
