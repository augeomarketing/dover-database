USE [PM]
GO
/****** Object:  Table [dbo].[tCalendarEvents]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCalendarEvents]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tCalendarEvents]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCalendarEvents]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tCalendarEvents](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[EventDateTime] [datetime] NULL,
	[EventTime] [datetime] NULL,
	[EventNote] [varchar](255) NULL,
	[CalType] [varchar](20) NULL,
	[CalTypePKID] [int] NULL,
	[LastEditedBy] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
