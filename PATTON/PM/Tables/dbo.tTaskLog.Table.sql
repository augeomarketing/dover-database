USE [PM]
GO
/****** Object:  Table [dbo].[tTaskLog]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tTaskLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tTaskLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tTaskLog](
	[TaskID] [int] NULL,
	[ProjectID] [int] NULL,
	[CustomerID] [int] NULL,
	[TaskName] [varchar](100) NULL,
	[TaskLeadUserName] [varchar](40) NULL,
	[IsTaskLeadMailed] [varchar](3) NULL,
	[TaskType] [varchar](30) NULL,
	[TaskStartDate] [datetime] NULL,
	[TaskEndDate] [datetime] NULL,
	[TaskStatus] [varchar](30) NULL,
	[PctComplete] [varchar](3) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserName] [varchar](40) NULL,
	[FormMethod] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
