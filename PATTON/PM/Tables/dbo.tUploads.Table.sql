USE [PM]
GO
/****** Object:  Table [dbo].[tUploads]    Script Date: 02/23/2010 10:47:28 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tUploads]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tUploads]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tUploads]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tUploads](
	[UploadID] [int] IDENTITY(1,1) NOT NULL,
	[KeyFieldID] [int] NULL,
	[CustomerGuid] [varchar](50) NOT NULL,
	[ParentLvl] [varchar](10) NOT NULL,
	[FileString] [varchar](50) NOT NULL,
	[DocDate] [datetime] NOT NULL,
	[FileSize] [int] NOT NULL,
	[FileDescr] [varchar](200) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
