USE [PM]
GO
/****** Object:  Table [dbo].[tDataReset]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tDataReset]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tDataReset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tDataReset]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tDataReset](
	[TableName] [varchar](50) NULL,
	[PKName] [varchar](50) NULL,
	[PKIDVal] [int] NULL,
	[FKName] [varchar](50) NULL,
	[FKIDVal] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
