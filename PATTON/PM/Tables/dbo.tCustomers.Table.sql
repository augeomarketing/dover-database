USE [PM]
GO
/****** Object:  Table [dbo].[tCustomers]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tCustomers_CustomerGUID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tCustomers] DROP CONSTRAINT [DF_tCustomers_CustomerGUID]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCustomers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tCustomers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tCustomers](
	[CustomerGUID] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_tCustomers_CustomerGUID]  DEFAULT (newid()),
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](50) NULL,
	[Company] [varchar](50) NULL,
	[AdminLName] [varchar](25) NULL,
	[AdminFName] [varchar](25) NULL,
	[AdminLogin] [varchar](10) NULL,
	[AdminPW] [varchar](10) NULL,
	[AdminEMail] [varchar](50) NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_tCustomers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
