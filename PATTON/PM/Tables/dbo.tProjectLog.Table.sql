USE [PM]
GO
/****** Object:  Table [dbo].[tProjectLog]    Script Date: 02/23/2010 10:47:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tProjectLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tProjectLog]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tProjectLog]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tProjectLog](
	[ProjectID] [int] NOT NULL,
	[CustomerID] [int] NULL,
	[ProjectName] [varchar](100) NOT NULL,
	[Priority] [int] NULL,
	[ProjMgrUserName] [varchar](40) NULL,
	[IsProjMgrMailed] [varchar](3) NULL,
	[ClientUserName] [varchar](40) NULL,
	[IsClientMailed] [varchar](3) NULL,
	[ProjStartDate] [datetime] NULL,
	[ProjEndDate] [datetime] NOT NULL,
	[ProjectStatus] [varchar](30) NULL,
	[ProjectType] [varchar](30) NULL,
	[ProjDescr] [varchar](1000) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserName] [varchar](40) NULL,
	[FormMethod] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
