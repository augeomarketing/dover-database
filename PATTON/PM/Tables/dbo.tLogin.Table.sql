USE [PM]
GO
/****** Object:  Table [dbo].[tLogin]    Script Date: 02/23/2010 10:47:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tLogin_Logdate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tLogin] DROP CONSTRAINT [DF_tLogin_Logdate]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tLogin]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tLogin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tLogin]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tLogin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](40) NULL,
	[Logdate] [datetime] NULL CONSTRAINT [DF_tLogin_Logdate]  DEFAULT (getdate()),
	[UserPW] [varchar](10) NULL,
	[UserLoc] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
