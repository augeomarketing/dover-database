USE [PM]
GO
/****** Object:  Table [dbo].[tProjects]    Script Date: 02/23/2010 10:47:27 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tProjects_ProjMgrEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tProjects] DROP CONSTRAINT [DF_tProjects_ProjMgrEmail]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_tProjects_ClientEmail]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[tProjects] DROP CONSTRAINT [DF_tProjects_ClientEmail]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tProjects]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[tProjects]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[tProjects]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[tProjects](
	[ProjectID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[ProjectName] [varchar](100) NOT NULL,
	[priority] [int] NULL,
	[ProjMgrUserID] [int] NULL,
	[IsProjMgrMailed] [int] NULL CONSTRAINT [DF_tProjects_ProjMgrEmail]  DEFAULT ((1)),
	[ClientID] [int] NULL,
	[IsClientMailed] [int] NULL CONSTRAINT [DF_tProjects_ClientEmail]  DEFAULT ((1)),
	[ProjStartDate] [datetime] NULL,
	[ProjEndDate] [datetime] NOT NULL,
	[ProjectStatusID] [int] NULL,
	[ProjectTypeID] [int] NULL,
	[ProjDescr] [varchar](1000) NULL,
	[LastUpdated] [datetime] NULL,
	[LastUserID] [int] NULL,
 CONSTRAINT [PK_tProjects] PRIMARY KEY CLUSTERED 
(
	[ProjectID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
