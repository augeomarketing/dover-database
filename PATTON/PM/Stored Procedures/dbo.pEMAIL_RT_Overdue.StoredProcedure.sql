USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pEMAIL_RT_Overdue]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_RT_Overdue]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pEMAIL_RT_Overdue]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_RT_Overdue]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pEMAIL_RT_Overdue]  
@TaskID int


AS

Declare 
@Ctr int,
@OwnerEmail varchar(1000) ,
@OwnerName varchar(1000),
@CCEmail varchar(200),
@TaskUserName varchar(50),
@TaskUserEmail varchar(50),
@RTLink varchar(100),
@TaskName varchar(100),
@TicketUpdated datetime

set @Ctr=0
Declare @Subj varchar(255), @Body varchar(2000)

---get the values for the Task and Owner
SELECT		@TaskName		=	tTasks.TaskName,
			@RTLink			=	tTasks.RTLink ,
			@TicketUpdated	=	tTasks.TicketUpdated,
			@OwnerName= tUsers.UserFName + '' '' + tUsers.UserLName , 
			@OwnerEmail = tUsers.email 
FROM         tTasks INNER JOIN
                      tUsers ON tTasks.TaskLeadUserID = tUsers.UserID
WHERE     (tTasks.TaskID = @TaskID) 


set @Subj=''Production/Serious ticket-'' +  @TaskName
set @Body =''This ticket has not been updated since ''-- + isnull(@TicketUpdated,'''')
set @Body = @Body + char(13) + char(13)
set @Body=@Body + ''Owner:'' + @OwnerName + CHAR(13)
set @Body=@Body + ''Ticket :'' + @TaskName+ CHAR(13)
set @Body=@Body + ''RT Link:'' + @RTLink 
---------------------------------------
--get the values for the reminder CCs
declare @Email varchar(50)



set @CCEmail=''''
set @Email=''''

DECLARE cTaskUsers CURSOR FOR
Select Email from tUsers where UserID in (select UserID from tTaskUsers where taskID=@TaskID)
  
OPEN cTaskUsers
FETCH NEXT FROM cTaskUsers INTO  @Email 
WHILE (@@FETCH_STATUS=0)
BEGIN

	
	if @ctr=0
		begin 
	 		Set @CCEmail= @Email 
		end
	else
		begin 
	 		Set @CCEmail=@CCEmail + '', '' + @Email 
		end	
	


set @ctr=@ctr+1
FETCH NEXT FROM cTaskUsers INTO @Email 

END
CLOSE cTaskUsers
DEALLOCATE cTaskUsers



print @OwnerEmail
print @CCEmail
print @Subj
print @Body

insert into Maintenance.dbo.perlemail( dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
values(@Subj, @Body , @OwnerEmail + '','' + @CCEmail, ''itops@rewardsnow.com'' )

--select * from Maintenance.dbo.perlemail
' 
END
GO
