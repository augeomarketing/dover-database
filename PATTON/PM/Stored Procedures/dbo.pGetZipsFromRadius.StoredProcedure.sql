USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pGetZipsFromRadius]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pGetZipsFromRadius]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pGetZipsFromRadius]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pGetZipsFromRadius]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pGetZipsFromRadius] @RadiusParm int, @ZipParm nvarchar(5)
AS
-----create the temp table Find_Center_ZIP
select ZIP_CODE, Latitude,Longitude into #Find_Center_Zip from zip_lat_long where ZIP_CODE = @ZipParm
--select * from #Find_Center_Zip where ZIP_CODE = @ZipParm

----------------------------------------------------------------------------------------------------------------

----create the Find_Distance table
SELECT ZIP_LAT_LONG.ZIP_CODE, 
ZIP_LAT_LONG.LATITUDE, 
ZIP_LAT_LONG.LONGITUDE,
 (ZIP_LAT_LONG.LATITUDE-#FIND_CENTER_ZIP.LATITUDE)*69.1 AS Distance_Lat

, 

(69.1*(ZIP_LAT_LONG.LONGITUDE-#FIND_CENTER_ZIP.LONGITUDE)*(Cos(#FIND_CENTER_ZIP.LATITUDE/57.3))) AS Distance_Long, 

POWER(
SQUARE( (ZIP_LAT_LONG.LATITUDE-#FIND_CENTER_ZIP.LATITUDE)*69.1) 
+
SQUARE((69.1*(ZIP_LAT_LONG.LONGITUDE-#FIND_CENTER_ZIP.LONGITUDE)*(Cos(#FIND_CENTER_ZIP.LATITUDE/57.3))))
,.5)

AS Distance

 into #Find_Distance from ZIP_LAT_LONG, #Find_Center_Zip
/*
select * from #Find_Distance  
where #Find_Distance.Distance<=@RadiusParm
ORDER BY ZIP_CODE
*/

SELECT 
#Find_Distance.Zip_Code, 
#Find_Distance.Distance, 
Zip_Lat_Long.CITY, 
Zip_Lat_Long.STATE,
Zip_Lat_Long.COUNTY
FROM #Find_Distance INNER JOIN Zip_Lat_Long ON #Find_Distance.Zip_Code = Zip_Lat_Long.Zip_Code
WHERE (((#Find_Distance.Distance)<=@RadiusParm))
ORDER BY #Find_Distance.Distance
' 
END
GO
