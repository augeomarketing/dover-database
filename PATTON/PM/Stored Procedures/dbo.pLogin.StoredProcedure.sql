USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pLogin]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pLogin]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pLogin]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pLogin]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pLogin]
@Email varchar(50) , 
@UserPW varchar(10) 

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION

SELECT tUsers.*, tCustomers.CustomerGUID, tCustomerUsers.IsCustomerAdmin AS IsCustomerAdmin, tCustomerUsers.CUID AS CUID, tCustomerUsers.CustomerID AS CustomerID 
				FROM tUsers INNER JOIN tCustomerUsers ON tUsers.UserID = tCustomerUsers.UserID	INNER JOIN tCustomers ON tCustomerUsers.CustomerID = tCustomers.CustomerID 
				 where tUsers.IsActive=1 and UserPW=@UserPW and Email=@Email
				
--	SELECT tUsers.*  FROM tUsers
--	where IsActive=1 and UserPW=@UserPW and Email=@Email


COMMIT TRANSACTION' 
END
GO
