USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pEMAIL_Generic]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_Generic]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pEMAIL_Generic]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_Generic]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pEMAIL_Generic] 
@FormMethod varchar(20) =  ''CREATE'',
@UserID int,  --the person sending this email
@CustomerID int ,   --the customer ID of the person sending this email  
@MailType varchar (20),  --comes in as ''USER'',''TASK'', ''PROJECT''
@MailTypePKID int ,    --the pPK value of the type (UserID for USER, TaskID for TASK and ProjectID for PROJECT
@MailTo varchar(1000)=null,
@MsgSubject varchar(200)=null,  -- the Subject of the Mail message
@MsgBody varchar(1000) =null,  --the body of the email message
@MailList varchar(1000) out
AS




 --@NumDays is the passed parameter

Declare 
@rc INT,  --the return code for XPSMTP

@MailFrom varchar(200),
--@MailTo varchar(1000),
@CCRecipients varchar(1000),

---below not used
@CCUserNames varchar(1000),
@BCCRecipients varchar(200),
@TaskName varchar (200),
@ProjectName varchar(200)



Select @MailFrom =Email from tUsers where UserID=@UserID  	--get the email addy of the person alling this proc
set @CCRecipients=@MailFrom  				--CC the person who sent it
/*
if @MailType=''USER'' 

	Begin
		SELECT @MailTo=Email from tUsers where USERID=@MailTypePKID	
	End
*/
---------------------------------------------------------------------------------------------
if @MailType=''TASK''  or @MailType=''PROJECT'' 

	Begin
		print @MailType
		print @MailTypePKID
		----------------------------------------------Calls sp that returns a comma separated list of the people to receive this email
		exec  pEMAIL_GetTargets  @MailTypePKID,  @MailType, @CCEmail= @MailList  out
		--set @MailList=@MailTo
		----------------------------------------------
	End
	if @MailType=''PROJECT''
		begin
			select @ProjectName=ProjectName from tProjects where ProjectID=@MailTypePKID
			set @MsgSubject =@MsgSubject  + '' - project: '' + upper(@ProjectName) 
		end
	if @MailType=''TASK''
		begin
			select @TaskName=TaskName from tTasks where TaskID=@MailTypePKID
			set @MsgSubject =@MsgSubject  + '' - task: '' + upper(@TaskName) 
		end




	if @FormMethod=''SEND''
	begin
		exec @rc = master.dbo.xp_smtp_sendmail
		        @FROM = @MailFrom ,
		        @TO = @MailTo,
		        @CC = @CCRecipients,
		        @BCC = @BCCRecipients,
		        @subject = @MsgSubject,
		        @message = @MsgBody,	
		        @server = N''localhost''
		select RC = @rc
			exec pEmail_Generic_log @UserID,  @CustomerID ,   @MailType ,  @MailTypePKID ,    @MailTo , @MsgSubject ,  @MsgBody 

	end


	print ''-----------------------------------------------------------------------------------'' 
	print   ''@From:'' +@MailFrom  + char(13)
	print   ''@To:'' + @MailTo  + char(13)
	print   ''@CC:'' + @CCRecipients + char(13)
	print   ''@BCC:'' + @BCCRecipients + char(13)
	print   ''@Subj:'' + @MsgSubject + char(13)
	print   ''@Msg:'' + @MsgBody  + char(13)

return
' 
END
GO
