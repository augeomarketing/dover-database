USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pEMAIL_RT_GetAllOverdue]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_RT_GetAllOverdue]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pEMAIL_RT_GetAllOverdue]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_RT_GetAllOverdue]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pEMAIL_RT_GetAllOverdue]  

AS

Declare 
@Ctr int,
@TaskID int, 
@ProdSeriousProjectID int

--this needs to be set correctly
select @ProdSeriousProjectID=ProjectID from tProjects 
where ProjectName=''Production Serious Tickets''


DECLARE cRTTasks CURSOR FOR
Select TaskID from tTasks 
	where ProjectID=@ProdSeriousProjectID
	AND ( 
			(DATEDIFF(day, GETDATE(), tTasks.TicketUpdated) < 1) 
		--or 
		--	tTasks.TicketUpdated is Null
		)
OPEN cRTTasks
FETCH NEXT FROM cRTTasks INTO  @TaskID 
WHILE (@@FETCH_STATUS=0)
BEGIN

	exec pEMAIL_RT_Overdue @TaskID

FETCH NEXT FROM cRTTasks INTO @TaskID

END
CLOSE cRTTasks
DEALLOCATE cRTTasks
' 
END
GO
