USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pTaskEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pTaskEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pTaskEditCreate]
@UserID int ,
@FormMethod varchar(10) ,
@TaskID int ,
@ProjectID int ,
@CustomerID int,
@TaskName varchar(100),
@TaskLeadUserID int , 
@IsTaskLeadMailed int , 
@TaskTypeID int , 
@PctComplete varchar(3) , 
@TaskStartDate datetime ,  
@TaskEndDate datetime ,
@RTLink varchar(100),
@TicketUpdated datetime,  
@TaskStatusID int , 
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION

-------------------------------------Copy the existing task to tTaskLog
exec pTaskLog @FormMethod, @TaskID

-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tTasks (     CustomerID,    ProjectID,     TaskName,     TaskTypeID,     PctComplete,     TaskLeadUserID ,      IsTaskLeadMailed,     TaskStatusID,    TaskStartDate,     TaskEndDate, RTLink, TicketUpdated, LastUpdated, LastUserID)
	 VALUES( @CustomerID, @ProjectID, @TaskName, @TaskTypeID,  @PctComplete, @TaskLeadUserID ,  @IsTaskLeadMailed, @TaskStatusID, @TaskStartDate, @TaskEndDate, @RTLink  , @TicketUpdated, GetDate(), @UserID)
	SET @TaskID = @@Identity 
	SET @NewPKVal = @TaskID
end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
	-- ProjectID, TaskName, TaskTypeID,  Assignee,  TaskStatus, TaskDate
		Update tTasks set 
		ProjectID	=	@ProjectID,
		TaskName	=	@TaskName , 
		TaskTypeID	=	@TaskTypeID,
		TaskLeadUserID=	@TaskLeadUserID ,
		IsTaskLeadMailed=	@IsTaskLeadMailed,
		TaskStatusID	=	@TaskStatusID,
		PctComplete	=	@PctComplete,
		TaskStartDate	=	@TaskStartDate,
		TaskEndDate	=	@TaskEndDate,
		RTLink 		=	@RTLink,
		TicketUpdated=	@TicketUpdated,
		LastUpdated	=	GetDate(),
		LastUserID	=	@UserID
		where TaskID	=	@TaskID
End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tTasks  where TaskID	=	@TaskID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tTasks'', ''TaskID'', @TaskID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
