USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pCalendarModifyEvent]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pCalendarModifyEvent]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pCalendarModifyEvent]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pCalendarModifyEvent]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pCalendarModifyEvent]
@FormMethod varchar(20),
@UserID int ,
@EventID int,
@CustomerID int,
@EventDateTime smalldatetime,
@EventTime datetime=null,
@EventNote varchar(255)=null ,
@CalType varchar(20),
@CalTypePKID int,

@ReturnString varchar(255) output,
@NewPKVal int output

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION


declare @ctr int, @tmpDate smalldatetime
set @ctr=0
set @tmpDate=@EventDateTime



if @FormMethod=''CREATE''
begin
	
	Insert Into tCalendarEvents (CustomerID, EventDateTime, EventTime, EventNote, CalType, CalTypePKID, LastEditedBy )
	VALUES (  @CustomerID, @EventDateTime,  @EventTime, @EventNote, @CalType, @CalTypePKID,  @UserID )
	SET @EventID = @@Identity 
	SET @NewPKVal = @EventID
end

if @FormMethod=''UPDATE''
begin
	
	UPDATE tCalendarEvents SET 
		CustomerID	=	@CustomerID, 
		EventDateTime	=	@EventDateTime, 
		EventTime	=	@EventTime,
		EventNote 	=	@EventNote, 
 		LastEditedBy 	=	@UserID
	WHERE EventID	=	@EventID
end





--------------------------------------------------------------------------
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
