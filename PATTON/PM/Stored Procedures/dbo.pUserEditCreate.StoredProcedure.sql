USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pUserEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pUserEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pUserEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pUserEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pUserEditCreate]
@FormMethod varchar(6) ,
@UserID int ,
--@UserName varchar(40) , 
@UserFName varchar(25) , 
@UserLName varchar(25) , 
@UserPW varchar(10) , 
@SecLvl int ,  
@Email varchar(50) ,
@IsActive int,
@IsClient int, 
@AdminUserID int,
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tUsers (UserName,  UserFName, UserLName,  UserPW,  SecLvl, Email, IsActive, IsClient )
	 VALUES(  @UserLName + '', '' + @UserFName,   @UserFName, @UserLName,  @UserPW,  @SecLvl, @Email,  @IsActive, @IsClient )
	SET @UserID = @@Identity 
	SET @NewPKVal = @UserID

	--Based  on the AdminUserID (the person creating the new user ), look up the CompanyID of this person and use this to insert a record in tCustomerUsers
	declare @CustomerID int
	select @CustomerID = CustomerID from tCustomerUsers where UserID=@AdminUserID
	-----------insert the record
	insert into tCustomerUsers (CustomerID, UserID, IsCustomerAdmin )
			values( @CustomerID, @UserID, 0 )
end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
Begin
	-- UserName,  UserLogin,  UserPW,  SecLvl, Email,  EmpID	
		Update tUsers set 
		UserName	=	@UserLName + '', '' + @UserFName, 
		UserFName	=	@UserFName , 
		UserLName	=	@UserLName , 
		UserPW	=	@UserPW,
		SecLvl	=	@SecLvl,
		Email	=	@Email,
		IsActive=	@IsActive,
		IsClient	=	@IsClient,
		LastUpdated=	GetDate() 
		where UserID	=	@UserID

		--update the matching fields from the customers record
		Update tCustomers set 
		AdminEmail=@Email, 
		AdminPW=@UserPW, 
		AdminFName=@UserFName,
		AdminLName=@UserLName,
		CustomerName=	@UserLName + '', '' + @UserFName
		where CustomerID=(Select customerID from tCustomerUsers where UserID=@UserID)
		-----
End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tCustomerUsers  where UserID	=	@UserID
		DELETE tUsers  where UserID	=	@UserID
	End
--------------------------------------------------------------------------
 exec pLogEvent @AdminUserID, ''tUsers'', ''UserID'', @UserID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
