USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pProjectLog]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pProjectLog]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pProjectLog]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pProjectLog]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pProjectLog]
@FormMethod varchar(10) ,
@ProjectID int
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
declare 

@OldProjectID int,
@CustomerID int,
@ProjectName varchar(100) ,
@Priority int, 
@ProjMgrUserID int ,
@IsProjMgrMailed Int ,
@ClientID int , 
@IsClientMailed Int ,
@ProjStartDate datetime,
@ProjEndDate datetime ,
@ProjectStatusID  int,
@ProjectTypeID int , 
@ProjDescr  varchar(1000),
@LastUpdated	datetime,
@LastUserID	int
-------------------------------------------
-------------------------------------Lookup the old values (do translations first)  and copy into the log table



if @FormMethod =''UPDATE'' or @FormMethod =''DELETE''
	Begin

		SELECT
		@OldProjectID	=	ProjectID,
		@CustomerID	=	CustomerID ,
		@ProjectName 	=	ProjectName ,
		@Priority		=Priority,		 
		@ProjMgrUserID=	ProjMgrUserID	,
		@IsProjMgrMailed =	IsProjMgrMailed,
		@ClientID	=	ClientID		,
		@IsClientMailed =	IsClientMailed	,
		@ProjEndDate	=	ProjEndDate	,
 		@ProjStartDate  =	ProjStartDate	,
		@ProjectStatusID=	ProjectStatusID	,
		@ProjectTypeID	=	ProjectTypeID	 ,
		@ProjDescr	=	ProjDescr	,
		@LastUpdated	=	LastUpdated	,
		@LastUserID	=	LastUserID
		FROM tProjects
		where ProjectID	=	@ProjectID
Declare 
@ProjMgrUserName varchar(40),
@ClientUserName varchar(40),

@IsProjMgrMailedYesNo varchar(3),
@IsClientMailedYesNo varchar(3),
@ProjectStatus varchar(30),
@ProjectType varchar(30),
@LastUserName varchar(40)


if @IsProjMgrMailed  = 0
	set @IsProjMgrMailedYesNo  = ''No''
else
	set @IsProjMgrMailedYesNo = ''Yes''

if @IsClientMailed  = 0
	set @IsClientMailedYesNo  = ''No''
else
	set @IsClientMailedYesNo  = ''Yes''

Select @ProjMgrUserName=UserName from tUsers where userID=@ProjMgrUserID
Select @ClientUserName=UserName from tUsers where userID=@ClientID
Select @LastUserName=UserName from tUsers where userID=@LastUserID

Select @ProjectStatus = Status from tluStatus where StatusID=@ProjectStatusID
Select @ProjectType = ProjectType from tluProjectTypes where ProjectTypeID=@ProjectTypeID


Insert into tProjectLog ( ProjectID,      CustomerID,     ProjectName,      Priority,  ProjMgrUserName,      IsProjMgrMailed ,                ClientUserName,     IsClientMailed ,                ProjEndDate,     ProjStartDate,     ProjectStatus,     ProjectType,   ProjDescr ,     LastUpdated, 	LastUserName ,  FormMethod )
			VALUES( @OldProjectID,		@CustomerID,    @ProjectName,					@Priority , @ProjMgrUserName,  @IsProjMgrMailedYesNo ,  @ClientUserName, @IsClientMailedYesNo ,  @ProjEndDate, @ProjStartDate, @ProjectStatus, @ProjectType, @ProjDescr , @LastUpdated,  @LastUserName ,  @FormMethod)
End
------------------------
--------------------------------------------------------------------------

IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
