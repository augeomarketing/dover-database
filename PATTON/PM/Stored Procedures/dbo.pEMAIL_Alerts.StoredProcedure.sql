USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pEMAIL_Alerts]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_Alerts]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pEMAIL_Alerts]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_Alerts]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pEMAIL_Alerts] @NumDays int
AS



 --@NumDays is the passed parameter

Declare 
@rc INT,  --the return code for XPSMTP

@TaskID int,
@TaskLeadFName varchar (200),
@TaskLeadLName varchar (200),
@TaskName varchar (200),
@TaskPctComplete int ,
@TaskEndDate datetime,
@ProjectName varchar(200),

@TaskLeadEmail varchar(200),
@MailFrom varchar(200),
@MsgSubject varchar(200),
@MsgBody varchar(1000),
@CCRecipients varchar(1000),
@CCUserNames varchar(1000),
@BCCRecipients varchar(200)



---------------------------------------------------------------------------------------------
	set @MailFrom=''mail@openbookcontractors.com''   --this can get the ProjectSuperviosors or someone elses email

	Set @TaskLeadEmail=''''
	set @MsgSubject=''''
	set @MsgBody=''''
	set @CCRecipients=''''
	set @BCCRecipients=''''

	set @TaskID = NULL
	set @TaskLeadFName = ''''
	set @TaskLeadLName = ''''
	set @TaskName = ''''
	set @TaskPctComplete = ''''
	set @TaskEndDate = null
	set @TaskLeadEmail = ''''
	set @ProjectName = ''''

--this gets the Task Lead and emails them a note telling them that the task is 
DECLARE cTaskLeads CURSOR FOR
	SELECT    tTasks.TaskID,  tUsers.UserFName as TaskLeadFName ,  tUsers.UserLName as TaskLeadLName, tTasks.TaskName, tTasks.PctComplete as TaskPctComplete, tTasks.TaskEndDate, tUsers.email as TaskLeadEmail , tProjects.ProjectName
	FROM         tUsers INNER JOIN tTasks ON tUsers.UserID = tTasks.TaskLeadUserID LEFT OUTER JOIN tProjects ON tTasks.ProjectID = tProjects.ProjectID
	WHERE (DATEDIFF(day,  getdate(), tTasks.TaskEndDate)=@NumDays)
	OR ( tTasks.TaskEndDate< GetDate()-1) 
OPEN cTaskLeads
FETCH NEXT FROM cTaskLeads INTO @TaskID,  @TaskLeadFName , @TaskLeadLName, @TaskName, @TaskPctComplete, @TaskEndDate, @TaskLeadEmail , @ProjectName
WHILE (@@FETCH_STATUS=0)
BEGIN

 	if ( @TaskEndDate< GetDate()-1) 
		set @MsgSubject = ''Task:'' + UPPER(@TaskName) + '' is overdue '' 
	else
		set @MsgSubject = ''Task:'' + UPPER(@TaskName) + '' is due within '' + convert(varchar(2),@NumDays) +'' days.''
	


	if (len(@ProjectName)>0)
		set @MsgBody= '' This task belongs to the project-'' + UPPER(@ProjectName) + char(10) + char(13) 
	set @MsgBody= char(10) + char(13)  + @MsgBody + ''Hi ''  + @TaskLeadFName + '' ''  + @TaskLeadLName + '',''  + char(10) + char(13) + ''Task-'' + UPPER(@TaskName) + '' is within '' + convert(varchar(2),@NumDays) +'' days of the estimated completion date.'' +char(13) +'' Percent complete currently stands at  '' + convert(varchar(3),@TaskPctComplete) + ''%.''

	
	exec  pEMAIL_GetTaskUsers  @TaskID, @NumDays, @CCEmail=@CCRecipients  out, @CCNames= @CCUserNames  out, @PMEmail=@BCCRecipients  out
	
	set @MsgBody= @MsgBody + char(13) + ''Users assigned to this task are: '' + @CCUserNames
--/*	

	exec @rc = master.dbo.xp_smtp_sendmail
	        @FROM = @MailFrom ,
	        @TO = @TaskLeadEmail,
	        @CC = @CCRecipients,
	        @BCC = @BCCRecipients,
	        @subject = @MsgSubject,
	        @message = @MsgBody,	
	        @server = N''localhost''
	select RC = @rc

--*/
	print ''-----------------------------------------------------------------------------------'' 
	print   ''@From:'' + @TaskLeadEmail  + char(13)
	print   ''@To:'' + @TaskLeadEmail  + char(13)
	print   ''@CC:'' + @CCRecipients + char(13)
	print   ''@BCC:'' + @BCCRecipients + char(13)
	print   ''@Subj:'' + @MsgSubject + char(13)
	print   ''@Msg:'' + @MsgBody  + char(13)
	

	
	
	
	

	Set @TaskLeadEmail=''''
	set @MsgSubject=''''
	set @MsgBody=''''
	set @CCRecipients=''''
	set @BCCRecipients=''''

 	set @TaskID = NULL
   	set @TaskLeadFName = ''''
	set @TaskLeadLName = ''''
	set @TaskName = ''''
	set @TaskPctComplete = ''''
	set @TaskEndDate = null
	set @TaskLeadEmail = ''''
	set @ProjectName = ''''

FETCH NEXT FROM cTaskLeads INTO @TaskID,  @TaskLeadFName , @TaskLeadLName, @TaskName, @TaskPctComplete, @TaskEndDate, @TaskLeadEmail , @ProjectName

END
CLOSE cTaskLeads
DEALLOCATE cTaskLeads
' 
END
GO
