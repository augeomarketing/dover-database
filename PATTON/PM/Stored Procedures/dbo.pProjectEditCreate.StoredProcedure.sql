USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pProjectEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pProjectEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pProjectEditCreate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pProjectEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pProjectEditCreate]
@UserID int ,
@FormMethod varchar(10) ,
@ProjectID int, 
@CustomerID int,
@ProjectName varchar(100) , 
@Priority int,
@ProjMgrUserID Int ,
@IsProjMgrMailed Int ,
@ClientID int , 
@IsClientMailed Int ,
@ProjStartDate datetime,
@ProjEndDate datetime ,
@ProjectStatusID  int,
@ProjectTypeID int , 
@ProjDescr  varchar(1000),
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Copy the existing task to tProjectLog
exec pProjectLog @FormMethod, @ProjectID

-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tProjects ( CustomerID,     ProjectName,  Priority,    ProjMgrUserID,      IsProjMgrMailed ,      ClientID,    IsClientMailed ,  ProjEndDate, ProjStartDate, ProjectStatusID, ProjectTypeID, ProjDescr, LastUpdated, LastUserID)
				VALUES( @CustomerID, @ProjectName,  @Priority,		@ProjMgrUserID,  @IsProjMgrMailed ,  @ClientID, @IsClientMailed ,  @ProjEndDate, @ProjStartDate, @ProjectStatusID, @ProjectTypeID, @ProjDescr, GetDate(), @UserID )
	SET @ProjectID = @@Identity 
	SET @NewPKVal = @ProjectID


	--Based  on the UserID (the person creating the new user ), look up the CompanyID of this person and use this to insert a record in tCustomerProjects
--	declare @CustomerID int
--	select @CustomerID = CustomerID from tCustomerUsers where UserID=@UserID
	-----------insert the record
	insert into tCustomerProjects (CustomerID, ProjectID )
			values( @CustomerID, @ProjectID )


---add the three project people
	insert Into tProjectTeams(CustomerID, ProjectID, TeamMemberUserID, IsProjectAdmin)
				values(@CustomerID, @ProjectID, @UserID, 1)

	insert Into tProjectTeams(CustomerID, ProjectID, TeamMemberUserID, IsProjectAdmin)
				values(@CustomerID, @ProjectID, @ProjMgrUserID, 1)

--	insert Into tProjectTeams(CustomerID, ProjectID, TeamMemberUserID, IsProjectAdmin)
--				values(@CustomerID, @ProjectID, @ClientID, 1)

--add the three project people

end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
	-- UserName,  UserLogin,  UserPW,  SecLvl, Email,  EmpID	
		Update tProjects set 
		ProjectName	=	@ProjectName , 
		Priority	=	@Priority,
		ProjMgrUserID	=	@ProjMgrUserID,
		IsProjMgrMailed	=	@IsProjMgrMailed ,
		ClientID		=	@ClientID,
		IsClientMailed	=	@IsClientMailed ,
		ProjEndDate	=	@ProjEndDate,
 		ProjStartDate	=	@ProjStartDate,
		ProjectStatusID	=	@ProjectStatusID,
		ProjectTypeID	=	@ProjectTypeID ,
		ProjDescr	=	@ProjDescr,
		LastUpdated	=	GetDate(),
		LastUserID	=	@UserID
		where ProjectID	=	@ProjectID
End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tProjects  where ProjectID	=	@ProjectID
		Delete tCustomerProjects where ProjectID=@ProjectID
		Delete tTaskUsers where TaskID in (Select TaskID from tTasks where ProjectID=@ProjectID) 
		Delete tTaskAlerts where TaskID in (Select TaskID from tTasks where ProjectID=@ProjectID) 
		Delete tTasks where ProjectID=@ProjectID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tProjects'', ''ProjectID'', @ProjectID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
