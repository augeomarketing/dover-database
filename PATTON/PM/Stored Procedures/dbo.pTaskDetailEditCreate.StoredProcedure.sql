USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pTaskDetailEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskDetailEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pTaskDetailEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskDetailEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pTaskDetailEditCreate]
@UserID int ,
@FormMethod varchar(10) ,
@TaskDetailID int ,
@TaskID int ,
@CustomerID int ,
@NoteDate datetime ,  
@TaskDetailNote varchar(2000),
@DetailStatusID int,
@DetailUserID int,
@DetailQty float,
@DetailTypeID int,
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION

-------------------------------------Copy the existing task to tTaskDetailLog
exec pTaskDetailLog @FormMethod, @TaskDetailID


-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tTaskDetail ( TaskID, CustomerID, NoteDate, TaskDetailNote, DetailStatusID, DetailUserID, DetailQty, DetailTypeID, LastUpdated , LastUserID )
	 VALUES( @TaskID, @CustomerID, @NoteDate, @TaskDetailNote, @DetailStatusID, @DetailUserID, @DetailQty, @DetailTypeID, GetDate() ,  @UserID)
	SET @TaskDetailID = @@Identity 
	SET @NewPKVal = @TaskDetailID
end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
		Update tTaskDetail set 
		CustomerID	=	@CustomerID , 
		NoteDate	=	@NoteDate , 
		TaskDetailNote	=	@TaskDetailNote,
		DetailStatusID	=	@DetailStatusID, 
		DetailUserID	=	@DetailUserID,
		DetailQty	=	@DetailQty,
		DetailTypeID	=	@DetailTypeID,
		LastUpdated	=	GetDate() , 
		LastUserID	=	@UserID
		where TaskDetailID	=	@TaskDetailID
End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tTaskDetailNotes  where TaskDetailID	=	@TaskDetailID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tTaskDetail'', ''TaskDetailID'', @TaskDetailID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
