USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pTaskDetailLog]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskDetailLog]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pTaskDetailLog]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskDetailLog]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pTaskDetailLog]
@FormMethod varchar(10) ,
@TaskDetailID int
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
declare 

@OldTaskDetailID int,
@TaskID int,
@CustomerID int,
@NoteDate datetime,
@TaskDetailNote varchar(100) , 
@DetailStatusID int ,
@DetailUserID Int ,
@DetailQty float, 
@DetailTypeID Int ,
@LastUpdated	datetime,
@LastUserID	int
-------------------------------------------
-------------------------------------Lookup the old values (do translations first)  and copy into the log table



if @FormMethod =''UPDATE'' or @FormMethod =''DELETE''
	Begin

		SELECT
		@OldTaskDetailID	=	TaskDetailID,
		@TaskID	=	TaskID ,
		@CustomerID	=	CustomerID ,
		@NoteDate	=	NoteDate ,
		@TaskDetailNote=	TaskDetailNote ,
		@DetailStatusID	=	DetailStatusID ,
		@DetailUserID	=	DetailUserID , 
		@DetailQty	=	DetailQty , 
		@DetailTypeID	=	DetailTypeID ,
		@LastUpdated	=	LastUpdated	,
		@LastUserID	=	LastUserID
		FROM tTaskDetail
		where TaskDetailID	=	@TaskDetailID
Declare 
@DetailUserName varchar(40),
@LastUserName varchar(40),
@DetailType varchar (30) ,
@DetailStatus varchar(30)




if @DetailStatusID  = 0
	set @DetailStatus   = ''Info''
else
	set @DetailStatus = ''Requires Attention!''


Select @DetailUserName=UserName from tUsers where userID=@DetailUserID
Select @LastUserName=UserName from tUsers where userID=@LastUserID


Select @DetailType = DetailType from tluDetailTypes where DetailTypeID=@DetailTypeID


Insert into tTaskDetailLog ( TaskDetailID ,      TaskID,   CustomerID,         NoteDate,    TaskDetailNote,     DetailStatus,     DetailUserName,    DetailQty,     DetailType, LastUpdated , LastUserID, FormMethod )
	 VALUES( @OldTaskDetailID, @TaskID, 	 @CustomerID , @NoteDate, @TaskDetailNote, @DetailStatus, @DetailUserName, @DetailQty, @DetailType, GetDate() ,  @LastUserID,  @FormMethod)
End
------------------------
--------------------------------------------------------------------------

IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
