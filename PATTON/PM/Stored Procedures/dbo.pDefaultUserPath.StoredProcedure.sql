USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pDefaultUserPath]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pDefaultUserPath]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pDefaultUserPath]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pDefaultUserPath]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pDefaultUserPath]

@UserID int ,
@CallingPage varchar(50),
@CustomerID int ,  
@SecLvl int,

@ReturnString varchar(255) output
AS 


--if this is the god account, go to the Admin page
if @SecLvl=100
	set @ReturnString  = ''/Admin/default.asp''
	return

if @SecLvl=50
	begin
		declare @IsNotSetup int
		select @IsNotSetup=count(*) from tluProjectTypes where ProjectType=''Type 1'' and CustomerID=@CustomerID
		if  @IsNotSetup =1
			set @ReturnString =''<A href="/forms/projecttypes.asp?FormMethod=CREATE">Setup Project Types</a>''
		else
			set @ReturnString =''/forms/ProjectEdit.asp''
	end
	

		
--is a Customer Admin?
--	n- Assigned to only one task
--		y-open detail in CREATE mode
--		n-. redirect to Overview
--			

--	y-	
/*
'' this procedure is called at login or after changing the pasword.
''If called from changing the password then-
	''is an admin 
	''Y- is it the first time they logged in?
		''Y-Set up requisites
		''N-Go to predetermined page
	''N- go to the predetermined page
*/



IF @@ERROR <>0 
	BEGIN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
' 
END
GO
