USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pTaskUsersEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskUsersEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pTaskUsersEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskUsersEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE  PROCEDURE [dbo].[pTaskUsersEditCreate] 
@FormMethod varchar(20), 
@UserID int = NULL, 
@CustomerID int = NULL, 
@TaskID int = NULL, 
@TaskUserID int = NULL,
@ReminderDays int = NULL
AS 


if @FormMethod= ''REMOVE''
BEGIN
	DELETE tTaskUsers WHERE UserID=@TaskUserID  AND TaskID = @TaskID 
	DELETE tTaskAlerts where TaskUserID=@TaskUserID  AND TaskID = @TaskID 
END

if @FormMethod= ''ADD''
BEGIN
	INSERT INTO tTaskUsers  ( UserID, TaskID, CustomerID)
			VALUES(@TaskUserID, @TaskID, @CustomerID)
end

if @FormMethod= ''-1DAYSADD'' OR @FormMethod= ''-1DAYSREMOVE'' OR  @FormMethod= ''0DAYSADD'' OR @FormMethod= ''0DAYSREMOVE'' OR @FormMethod= ''2DAYSADD'' OR @FormMethod= ''2DAYSREMOVE'' OR @FormMethod= ''10DAYSADD'' OR @FormMethod= ''10DAYSREMOVE''
BEGIN
	if @FormMethod = ''-1DAYSADD''
	begin	
		delete tTaskAlerts where ReminderDays=0 and TaskID=@TaskID AND TaskUserID=@TaskUserID
		insert into tTaskAlerts(TaskID, TaskUserID, ReminderDays )
			values( @TaskID, @TaskUserID, -1 )
	end

	if @FormMethod = ''-1DAYSREMOVE''
	begin	
		delete tTaskAlerts where ReminderDays=-1 and TaskID=@TaskID AND TaskUserID=@TaskUserID
	end



	if @FormMethod = ''0DAYSADD''
	begin	
		delete tTaskAlerts where ReminderDays=0 and TaskID=@TaskID AND TaskUserID=@TaskUserID
		insert into tTaskAlerts(TaskID, TaskUserID, ReminderDays )
			values( @TaskID, @TaskUserID, 0 )
	end

	if @FormMethod = ''0DAYSREMOVE''
	begin	
		delete tTaskAlerts where ReminderDays=0 and TaskID=@TaskID AND TaskUserID=@TaskUserID
	end



	if @FormMethod = ''2DAYSADD''
	begin	
		delete tTaskAlerts where ReminderDays=2 and TaskID=@TaskID AND TaskUserID=@TaskUserID
		insert into tTaskAlerts(TaskID, TaskUserID, ReminderDays )
			values( @TaskID, @TaskUserID, 2  )
	end

	if @FormMethod = ''2DAYSREMOVE''
	begin	
		delete tTaskAlerts where ReminderDays=2 and TaskID=@TaskID AND TaskUserID=@TaskUserID
	end


	if @FormMethod = ''10DAYSADD''
	begin	
		delete tTaskAlerts where ReminderDays=10 and TaskID=@TaskID AND TaskUserID=@TaskUserID
		insert into tTaskAlerts(TaskID, TaskUserID, ReminderDays )
			values( @TaskID, @TaskUserID, 10  )
	end


	if @FormMethod = ''10DAYSREMOVE''
	begin	
		delete tTaskAlerts where ReminderDays=10 and TaskID=@TaskID AND TaskUserID=@TaskUserID
	end

end
' 
END
GO
