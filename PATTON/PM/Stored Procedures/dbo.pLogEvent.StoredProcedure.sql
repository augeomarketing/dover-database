USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pLogEvent]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pLogEvent]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pLogEvent]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pLogEvent]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pLogEvent]
@UserID int ,
@TableName varchar(30) ,  
@PKName varchar(30),
@PKID int ,
@FormMethod varchar(20) 
AS 
declare 
@UserName varchar(40)
SELECT @UserName=UserName From tUsers WHERE UserID=@UserID
Insert INTO tLog ( Logdate, UserID, UserName, TableName, PKName, PKID, FormMethod )
	VALUES ( GETDATE(), @UserID, @UserName, @TableName, @PKName, @PKID, @FormMethod )
' 
END
GO
