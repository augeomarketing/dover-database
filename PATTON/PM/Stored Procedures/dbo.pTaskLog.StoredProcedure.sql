USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pTaskLog]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskLog]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pTaskLog]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pTaskLog]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pTaskLog]
@FormMethod varchar(10) ,
@TaskID int
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
declare 
@OldTaskID int,
@ProjectID int ,
@CustomerID int,
@TaskName varchar(100),
@TaskLeadUserID int , 
@IsTaskLeadMailed int , 
@TaskTypeID int , 
@PctComplete varchar(3) , 
@TaskStartDate datetime ,  
@TaskEndDate datetime ,  
@TaskStatusID int ,
@LastUpdated datetime,
@LastUserID int

-------------------------------------------
-------------------------------------Lookup the old values (do translations first)  and copy into the log table



if @FormMethod =''UPDATE'' or @FormMethod =''DELETE''
	Begin
	-- ProjectID, TaskName, TaskTypeID,  Assignee,  TaskStatus, TaskDate
		SELECT 
		@OldTaskID	=	TaskID,		
		@ProjectID	=	ProjectID	,
		@CustomerID	=	CustomerID	,
		@TaskName	=	TaskName	, 
		@TaskTypeID	=	TaskTypeID	,
		@TaskLeadUserID=	TaskLeadUserID ,
		@IsTaskLeadMailed=	IsTaskLeadMailed ,
		@TaskStatusID	=	TaskStatusID	,
		@PctComplete	=	PctComplete	,
		@TaskStartDate=	TaskStartDate	,
		@TaskEndDate	=	TaskEndDate	,
		@LastUpdated	=	LastUpdated	,
		@LastUserID	=	LastUserID	
		FROM tTasks 
		where TaskID	=	@TaskID

Declare 
@LastUserName varchar(40),
@LeadUserName varchar(40),
@TaskStatus varchar(30),
@TaskType varchar(30),
@IsTaskLeadMailedYesNo varchar(3)

if @IsTaskLeadMailed=0
	set @IsTaskLeadMailedYesNo =''No''
else
	set @IsTaskLeadMailedYesNo =''Yes''

Select @LastUserName=UserName from tUsers where userID=@LastuserID
Select @LeadUserName=UserName from tUsers where userID=@TaskLeadUserID
Select @TaskStatus = Status from tluStatus where StatusID=@TaskStatusID
Select @TaskType = TaskType from tluTaskTypes where TaskTypeID=@TaskTypeID


Insert into tTaskLog ( TaskID,        CustomerID,       ProjectID,   TaskName,         TaskType,     PctComplete,      TaskLeadUserName,      IsTaskLeadMailed ,   	        TaskStatus,    TaskStartDate,     TaskEndDate,    LastUpdated,      LastUserName,     FormMethod)
	 VALUES( @OldTaskID, @CustomerID, @ProjectID, @TaskName,     @TaskType, @PctComplete,  @LeadUserName,           @IsTaskLeadMailedYesNo , @TaskStatus, @TaskStartDate, @TaskEndDate, @LastUpdated,  @LastUserName, @FormMethod)
End
------------------------
--------------------------------------------------------------------------

IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
