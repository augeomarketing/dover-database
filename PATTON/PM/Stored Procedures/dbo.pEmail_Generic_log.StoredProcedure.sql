USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pEmail_Generic_log]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEmail_Generic_log]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pEmail_Generic_log]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEmail_Generic_log]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pEmail_Generic_log] 
@UserID int,  --the person sending this email
@CustomerID int ,   --the customer ID of the person sending this email  
@MailType varchar (20),  --comes in as ''USER'',''TASK'', ''PROJECT''
@MailTypePKID int ,    --the pPK value of the type (UserID for USER, TaskID for TASK and ProjectID for PROJECT
@MailTo varchar(1000)=null,
@MsgSubject varchar(200)=null,  -- the Subject of the Mail message
@MsgBody varchar(1000) =null  --the body of the email message

AS

insert into tEmailHistory
	(CustomerID, SenderUserID, MailType, MailTypePKID, MailTo, MsgSubj, MsgBody )
values( @CustomerID, @UserID, @MailType, @MailTypePKID, @MailTo, @MsgSubject, @MsgBody )
' 
END
GO
