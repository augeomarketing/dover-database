USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pDefaultLookupsCreate]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pDefaultLookupsCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pDefaultLookupsCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pDefaultLookupsCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pDefaultLookupsCreate]
@CustomerID int

AS 
SET XACT_ABORT ON
BEGIN TRANSACTION

--Create the default values for the lookup tables

--tluProjectTypes
--tluTaskGroups
--tluTaskTypes
--tluDetailTypes
---------------------
-----------tluProjectTypes
insert into tluProjectTypes (ProjectType, CustomerID) values(''Type 1'', @CustomerID)
insert into tluProjectTypes (ProjectType, CustomerID) values(''Type 2'', @CustomerID)
insert into tluProjectTypes (ProjectType, CustomerID) values(''Type 3'', @CustomerID)
	
-----------tluTaskGroups
insert into tluTaskGroups (TaskGroup ,  CustomerID) values(''Group 1'', @CustomerID)
insert into tluTaskGroups (TaskGroup ,  CustomerID) values(''Group 2'', @CustomerID)
insert into tluTaskGroups (TaskGroup ,  CustomerID) values(''Group 3'', @CustomerID)

-----------tluTaskTypes
insert into tluTaskTypes (TaskType ,  CustomerID) values(''Type 1'', @CustomerID)
insert into tluTaskTypes (TaskType ,  CustomerID) values(''Type 2'', @CustomerID)
insert into tluTaskTypes (TaskType ,  CustomerID) values(''Type 3'', @CustomerID)

-----------tluDetailTypes
Insert into tluDetailTypes (DetailType, CustomerID, SortOrder )  VALUES( ''Hours'', @CustomerID, 1 )
Insert into tluDetailTypes (DetailType, CustomerID, SortOrder )  VALUES( ''Dollars'', @CustomerID, 2)


IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN

	END
COMMIT TRANSACTION
' 
END
GO
