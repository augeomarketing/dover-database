USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pManageTaskGroups]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pManageTaskGroups]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pManageTaskGroups]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pManageTaskGroups]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pManageTaskGroups]
@FormMethod varchar(6) ,
@UserID int ,
@TaskGroupID int , 
@TaskGroup varchar(30) , 
@CustomerID int ,  
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
	begin
		Insert into tluTaskGroups ( TaskGroup, CustomerID )
		 	VALUES(   @TaskGroup, @CustomerID )
		SET @TaskGroupID = @@Identity 
		SET @NewPKVal = @TaskGroupID
	end
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
		Update tluTaskGroups set 
		TaskGroup		=	@TaskGroup 
		where TaskGroupID	=	@TaskGroupID
	End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tluTaskGroups  where TaskGroupID	=	@TaskGroupID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tluTaskGroups'',  ''TaskGroupID'', @TaskGroupID,  @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
