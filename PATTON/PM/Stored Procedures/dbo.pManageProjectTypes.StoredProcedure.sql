USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pManageProjectTypes]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pManageProjectTypes]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pManageProjectTypes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pManageProjectTypes]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pManageProjectTypes]
@FormMethod varchar(6) ,
@UserID int ,
@ProjectTypeID int , 
@ProjectType varchar(30) , 
@CustomerID int ,  
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
	begin
		Insert into tluProjectTypes ( ProjectType, CustomerID )
		 	VALUES(   @ProjectType, @CustomerID )
		SET @ProjectTypeID = @@Identity 
		SET @NewPKVal = @ProjectTypeID
	end
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
		Update tluProjectTypes set 
		ProjectType		=	@ProjectType, 
		CustomerID		=	@CustomerID
		where ProjectTypeID	=	@ProjectTypeID
	End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tluProjectTypes  where ProjectTypeID	=	@ProjectTypeID
	End
--------------------------------------------------------------------------
 exec pLogEvent  @UserID, ''tProjectTypes'', ''ProjectTypeID'', @ProjectTypeID,  @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
