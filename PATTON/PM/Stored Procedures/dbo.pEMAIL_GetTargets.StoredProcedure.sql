USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pEMAIL_GetTargets]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_GetTargets]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pEMAIL_GetTargets]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pEMAIL_GetTargets]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pEMAIL_GetTargets]  
@PKID int,
@TargetType varchar(20), 
@CCEmail varchar(1000) output

AS

--This Procedure takes 2 parameters, the primary key value and the Name of the level of email addresses that we want
--it returns a string of comma separated addresses

Declare 
@Ctr int,

@TaskUserEmail varchar(50)
set @Ctr=0


	
------------------------------If TASK
if @TargetType=''TASK''
begin
	DECLARE cTaskUsers CURSOR FOR
	
	SELECT      tUsers.email
	FROM         tTaskUsers INNER JOIN
	                      tUsers ON tTaskUsers.UserID = tUsers.UserID INNER JOIN
	                      tTasks ON tTaskUsers.TaskID = tTasks.TaskID
	WHERE     (tTasks.TaskID = @PKID)
	
	union 
	
	--the TaskLead
	SELECT  tUsers.email
	FROM         tTasks INNER JOIN
	                      tUsers ON tTasks.TaskLeadUserID = tUsers.UserID
	WHERE     (tTasks.TaskID = @PKID)
end
 
------------------------------If PROJECT
if @TargetType=''PROJECT''
begin
	DECLARE cTaskUsers CURSOR FOR
	
	--the ProjectManager
	SELECT DISTINCT tUsers.email
	FROM         tUsers INNER JOIN
	                      tProjects ON tUsers.UserID = tProjects.ProjMgrUserID
	WHERE     (tProjects.ProjectID = @PKID)
	
	UNION
	--the Client
	SELECT DISTINCT tUsers.email
	FROM         tUsers INNER JOIN
	                      tProjects ON tUsers.UserID = tProjects.ClientID
	WHERE     (tProjects.ProjectID = @PKID)
	
	
	--TASK LEVEL EMAIL
	UNION
	
	--the users from tTaskUsers
	SELECT      tUsers.email
	FROM         tTaskUsers INNER JOIN
	                      tUsers ON tTaskUsers.UserID = tUsers.UserID INNER JOIN
	                      tTasks ON tTaskUsers.TaskID = tTasks.TaskID
	WHERE     (tTasks.ProjectID = @PKID)
	
	
	union 
	
	--the TaskLead
	SELECT  tUsers.email
	FROM         tTasks INNER JOIN
	                      tUsers ON tTasks.TaskLeadUserID = tUsers.UserID
	WHERE     (tTasks.ProjectID = @PKID)

end


OPEN cTaskUsers
FETCH NEXT FROM cTaskUsers INTO  @TaskUserEmail 
WHILE (@@FETCH_STATUS=0)
BEGIN

	if @Ctr=0 
	begin -- if this is the first record in the cursor

	 	Set @CCEmail=@TaskUserEmail  
	end
	else		
	begin

	 	Set @CCEmail=@CCEMail + '', '' + @TaskUserEmail 
	end
	


	set @TaskUserEmail = ''''
set @ctr=@ctr+1
FETCH NEXT FROM cTaskUsers INTO  @TaskUserEmail 

END
CLOSE cTaskUsers
DEALLOCATE cTaskUsers
return
' 
END
GO
