USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pManageTaskTypes]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pManageTaskTypes]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pManageTaskTypes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pManageTaskTypes]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pManageTaskTypes]
@FormMethod varchar(6) ,
@UserID int ,
@TaskTypeID int , 
@TaskType varchar(30) , 
@TaskGroupID int,
@CustomerID int ,  
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
	begin
		Insert into tluTaskTypes ( TaskType, TaskGroupID, CustomerID )
		 	VALUES(   @TaskType,  @TaskGroupID, @CustomerID )
		SET @TaskTypeID = @@Identity 
		SET @NewPKVal = @TaskTypeID
	end
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
		Update tluTaskTypes set 
		TaskType		=	@TaskType, 
		TaskGroupID		=	@TaskGroupID,
		CustomerID		=	@CustomerID
		where TaskTypeID	=	@TaskTypeID
	End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tluTaskTypes  where TaskTypeID	=	@TaskTypeID
	End
--------------------------------------------------------------------------
 exec pLogEvent @TaskTypeID, ''tluTaskTypes'', ''TaskTypeID'', @UserID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
