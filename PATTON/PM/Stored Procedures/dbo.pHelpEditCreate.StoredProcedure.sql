USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pHelpEditCreate]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pHelpEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pHelpEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pHelpEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pHelpEditCreate]
@FormMethod varchar(6) ,
@UserID int ,
@HelpID int ,
@PageName varchar(50) , 
@ItemName varchar(50) , 
@HelpText varchar(2000) , 
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tHelp ( PageName, ItemName, HelpText)
	 VALUES( @PageName, @ItemName, @HelpText)
	SET @HelpID = @@Identity 
	SET @NewPKVal = @HelpID
end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
		Update tHelp set 
		PageName	=	@PageName,
		ItemName	=	@ItemName,
		HelpText	=	@HelpText
		where HelpID=	@HelpID
	 
End
------------------------
if @FormMethod =''DELETE''
Begin
	Delete tHelp where HelpID	=	@HelpID	
End
--------------------------------------------------------------------------
 	exec pLogEvent @UserID, ''tHelp'', ''HelpID'', @HelpID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
