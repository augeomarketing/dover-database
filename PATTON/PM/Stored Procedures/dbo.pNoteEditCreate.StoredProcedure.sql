USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pNoteEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pNoteEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pNoteEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pNoteEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pNoteEditCreate]
@UserID int ,
@FormMethod varchar(10) ,
@NoteID int ,
@FKID int ,
@FKName varchar(25) ,
@Note varchar(2000),
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tNotes ( UserID, FKID, FKName, Note)
	 VALUES(@UserID,  @FKID, @FKName,  @Note)
	SET @NoteID = @@Identity 
	SET @NewPKVal = @NoteID
end
-------------------------------------------
-------NO Update

------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tNotes  where NoteID	=	@NoteID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tNotes'', ''NoteID'', @NoteID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
