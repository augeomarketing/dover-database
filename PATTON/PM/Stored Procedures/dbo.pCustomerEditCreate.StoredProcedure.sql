USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pCustomerEditCreate]    Script Date: 02/23/2010 10:47:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pCustomerEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pCustomerEditCreate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pCustomerEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pCustomerEditCreate]
@FormMethod varchar(6) ,
@CustomerID int ,
@Company varchar(40) , 
@AdminFName varchar(25) , 
@AdminLName varchar(25) , 
@AdminPW varchar(10) , 
@AdminEmail varchar(50) , 
@UserID int,
@ReturnString varchar(255) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION


if @FormMethod =''CREATE''
begin
----check and make sure that the email address doesn''t already exist
declare @NumUsers int
select @NumUsers=count(*) from tUsers where Email=@AdminEmail
if @NumUsers>0 
	begin	

		set @ReturnString=''this email address already exists in the system''
		ROLLBACK TRAN
		return
	end


-------------------------------------Create

Insert into tCustomers ( CustomerName,  Company, AdminFName, AdminLName,  AdminPW, AdminEmail )
	 VALUES( @AdminLName + '', '' + @AdminFName,   @Company, @AdminFName, @AdminLName,  @AdminPW,  @AdminEmail)
	SET @CustomerID = @@Identity 
	--select @NewPKVal=CustomerGUID from tCustomers where CustomerID=@CustomerID
	SET @NewPKVal = @CustomerID

------------get the Guid and replace the password
declare @Guid uniqueidentifier
select @Guid=CustomerGuid from tCustomers where CustomerID=@CustomerID
set @AdminPW=substring(convert(varchar(50),@Guid),1,8)
update tCustomers set AdminPW=@AdminPW where CustomerID=@CustomerID

----- create the row in tUsers
Insert into tUsers ( UserName, UserFName, UserLName,   UserPW,   SecLvl,  Email,  IsActive ,  IsClient )
	 VALUES( @AdminLName + '', '' + @AdminFName, @AdminFName, @AdminLName,   @AdminPW, 50,  @AdminEmail,  1 , 1)
	SET @UserID = @@Identity 
--create the row in tCustomerUsers setting the IsCustomerAdmin value to 1
Insert into tCustomerUsers ( CustomerID, UserID, IsCustomerAdmin, SecLvl )
	 VALUES( @CustomerID, @UserID,  1, 50 )

--create the defaults for the lookup tables
exec pDefaultLookupsCreate @CustomerID

end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
	-- CustomerName,  AdminLogin,  AdminPW,  SecLvl, Email,  EmpID	
		Update tCustomers set 
		Company	=	@Company , 
		AdminFName	=	@AdminFName , 
		AdminLName	=	@AdminLName , 
		AdminPW		=	@AdminPW,
		AdminEmail	=	@AdminEmail,
		LastUpdated=	GetDate() 
		where CustomerID	=	@CustomerID

		--update the matching fields from the Users record
		Update tUsers set 
		Email=@AdminEmail, 
		UserPW =@AdminPW, 
		UserFName=@AdminFName ,
		UserLName =@AdminLName ,
		UserName=	@AdminLName + '', '' + @AdminFName
		where UserID=(Select UserID from tCustomerUsers where CustomerID=@CustomerID and IsCustomerAdmin=1)
End
------------------------
if @FormMethod =''DELETE''
	Begin
		
		DELETE tUsers where UserID=(select UserID from tCustomerUsers where CustomerID=@CustomerID)
		DELETE  tCustomerUsers where CustomerID=@CustomerID
		DELETE tCustomers  where CustomerID	=	@CustomerID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tCustomers'', ''CustomerID'', @CustomerID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
