USE [PM]
GO
/****** Object:  StoredProcedure [dbo].[pUploadEditCreate]    Script Date: 02/23/2010 10:47:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pUploadEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[pUploadEditCreate]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[pUploadEditCreate]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE  [dbo].[pUploadEditCreate]
@UserID int ,
@FormMethod varchar(10) ,
@UploadID int=null,
@KeyfieldID int,
@CustomerGuid varchar(50), 
@ParentLvl varchar(20), 
@FileString varchar(50), 
@DocDate varchar(20), 
@FileSize int,
@FileDescr varchar(200), 
@ReturnString varchar(500) output,
@NewPKVal int output  
AS 
SET XACT_ABORT ON
BEGIN TRANSACTION
-------------------------------------Create
if @FormMethod =''CREATE''
begin
Insert into tUploads ( KeyFieldID, CustomerGuid, ParentLvl, FileString, DocDate, FileSize, FileDescr )
	 VALUES( @KeyFieldID, @CustomerGuid, @ParentLvl, @FileString, @DocDate, @FileSize, @FileDescr)
	SET @UploadID = @@Identity 
	SET @NewPKVal = @UploadID
end
-------------------------------------------
-------------------------------------Update
if @FormMethod =''UPDATE''
	Begin
		Update tUploads set 
 		KeyFieldID	=	@KeyFieldID,
		CustomerGuid	=	@CustomerGuid, 
		ParentLvl	=	@ParentLvl, 
		FileString	=	@FileString, 
		DocDate	=	@DocDate, 
		FileSize		=	@FileSize,
		FileDescr	=	@FileDescr
		where UploadID	=	@UploadID
End
------------------------
if @FormMethod =''DELETE''
	Begin
		DELETE tUploads  where UploadID	=	@UploadID
	End
--------------------------------------------------------------------------
 exec pLogEvent @UserID, ''tUploads'', ''UploadID'', @UploadID, @FormMethod 
IF @@ERROR <>0 
	BEGIN
	ROLLBACK TRAN
	set @ReturnString= ''ERROR OCCURRED'' + @@ERROR
	RETURN
	END
COMMIT TRANSACTION
' 
END
GO
