USE [529EverbankConsumer]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateDailyFeed]    Script Date: 09/25/2009 10:38:11 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[pGenerateDailyFeed]
as

Truncate Table Dailyfeed

Truncate Table WorkAccountNumber

insert into workAccountNumber ( AccountNumber, Tipnumber)
select distinct Acctid, Tipnumber from affiliat
where acctstatus='A'

update dailypoints
set dailypoints.accountnumber=workaccountnumber.accountnumber
from dailypoints, workaccountnumber
where dailypoints.tipnumber=workaccountnumber.tipnumber

insert into dailyfeed (cardnumber, points)
select accountnumber, availablebal from dailypoints
where accountnumber>'0'
GO
