USE [529EverbankConsumer]
GO
/****** Object:  Table [dbo].[DailyPoints]    Script Date: 09/25/2009 10:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyPoints](
	[tipnumber] [char](20) NOT NULL,
	[availablebal] [int] NULL,
	[AccountNumber] [varchar](16) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
