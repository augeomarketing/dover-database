USE [529EverbankConsumer]
GO
/****** Object:  Table [dbo].[DailyFeed]    Script Date: 09/25/2009 10:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DailyFeed](
	[Cardnumber] [char](16) NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
