USE [529EverbankConsumer]
GO
/****** Object:  Table [dbo].[WorkAccountNumber]    Script Date: 09/25/2009 10:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkAccountNumber](
	[AccountNumber] [char](16) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
