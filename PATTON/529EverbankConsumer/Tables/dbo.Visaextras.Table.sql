USE [529EverbankConsumer]
GO
/****** Object:  Table [dbo].[Visaextras]    Script Date: 09/25/2009 10:38:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Visaextras](
	[F2] [nvarchar](20) NULL,
	[F4] [float] NULL,
	[acctid] [char](16) NULL,
	[points] [float] NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
