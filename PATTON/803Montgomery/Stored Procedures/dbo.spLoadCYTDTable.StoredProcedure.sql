USE [803]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCYTDTable]    Script Date: 02/16/2011 09:34:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCYTDTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCYTDTable]
GO

USE [803]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCYTDTable]    Script Date: 02/16/2011 09:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================
-- Author:		Dan Foster	
-- Create date: 8/10/2010
-- Description:	Load CYTD Transaction Table
-- Changes:     1)  Update lastdateprocessed column to current processing date in dbo.Cust_YTD_Transaction_stage
-- 2011-11-15   2)  Set YTD values to (0) when month(@lastdateprocessed) = '1'
-- ===============================================================================================================
CREATE PROCEDURE [dbo].[spLoadCYTDTable] @LastDateProcessed date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Change (2)
	Update dbo.Cust_YTD_Transaction_Stage set Current_YTD_Total = 0, Prev_YTD_Total = 0, Current_YTD_Count = 0, Prev_YTD_Count = 0
	where MONTH(@LastDateProcessed) = '1'

	update dbo.TransactionPrep set dim_wk_transactionprep_SIG_AMT = (dim_wk_transactionprep_SIG_AMT/100)
	
	insert dbo.Cust_YTD_Transaction_Stage (Customer_Id) 
	select distinct rtrim(dim_wk_transactionprep_AcctID) from TransactionPrep
	where rtrim(dim_wk_transactionprep_AcctID) not in (select rtrim(Customer_Id) from Cust_YTD_Transaction_stage)

	update dbo.Cust_YTD_Transaction_stage set Current_Month_Transaction = 0, Current_Month_Count = 0, 
	Prev_YTD_Total = Current_YTD_Total, Prev_YTD_Count = Current_YTD_Count 
	
    update dbo.Cust_YTD_Transaction_stage set Current_YTD_Total = 0, Current_YTD_Count = 0 
	
	update dbo.Cust_YTD_Transaction_stage set Current_YTD_Total = dim_wk_transactionprep_SIG_AMT, Current_YTD_Count = dim_wk_transactionprep_SIG_CT
	from dbo.Cust_YTD_Transaction_stage CYTD, Transactionprep CIW 
	where CYTD.Customer_Id = CIW.dim_wk_transactionprep_AcctID
	
	update dbo.Cust_YTD_Transaction_stage set Current_Month_Transaction = Current_YTD_Total - Prev_YTD_Total,
	Current_Month_Count = Current_YTD_Count - Prev_YTD_Count
	where Current_YTD_Total <> 0
	
	update dbo.Cust_YTD_Transaction_stage set Current_Month_Transaction = 0 
	where Current_YTD_Total - Prev_YTD_Total = 0 
	
	-- Change (1)
	update dbo.Cust_YTD_Transaction_Stage set Last_Date_Processed = @LastDateProcessed
	
END










GO


