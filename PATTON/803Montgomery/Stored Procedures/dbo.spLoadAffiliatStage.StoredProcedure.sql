USE [803]
GO
/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 10/26/2010 13:55:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  *****************************************/
/* Date:  7/26/2010                         */
/* Author:  Dan Foster                      */
/*  *****************************************/
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

create PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 

/************ Insert New Accounts into Affiliat Stage  ***********/

insert into AFFILIAT_Stage (ACCTID,tipnumber)
select distinct dim_wk_CustomerPrep_AcctID, dim_wk_CustomerPrep_TipNumber from CustomerPrep where not exists(select 1 from affiliat_stage
where ACCTID = CustomerPrep.dim_wk_CustomerPrep_AcctID)

update AFS set AcctType='Debit', DateAdded=@monthend, secid = null, AcctStatus='A',
 LastName=cs.dim_wk_CustomerPrep_LastName, YTDEarned=0, custid=dim_wk_CustomerPrep_TIN
 from AFFILIAT_Stage AFS join CustomerPrep CS on AFS.ACCTID=CS.dim_wk_CustomerPrep_AcctID
 where DATEADDED is null or DATEADDED = ' '

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctTypeDesc = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
GO
