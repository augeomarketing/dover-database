USE [803]
GO
/****** Object:  StoredProcedure [dbo].[sp_WelcomeKitLoad]    Script Date: 10/26/2010 13:55:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp_WelcomeKitLoad] 
	   @EndDate date

AS 
declare @kitscreated    int

Truncate Table dbo.Welcomekit 

insert into dbo.Welcomekit 
SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ADDRESS1, 
		ADDRESS2, ADDRESS3, City, State, ZipCode
FROM dbo.customer
WHERE (Year(DATEADDED) = Year(@EndDate)
	AND Month(DATEADDED) = Month(@EndDate)
	AND Upper(STATUS) <> 'C') 

set @KitsCreated = @@rowcount

select @kitscreated
GO
