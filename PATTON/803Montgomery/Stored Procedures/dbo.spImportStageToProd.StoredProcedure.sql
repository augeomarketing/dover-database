USE [803]
GO

/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 04/05/2011 13:25:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportStageToProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportStageToProd]
GO

USE [803]
GO

/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 04/05/2011 13:25:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/***************************************************************************************************/
/*  Import Transactions FROM Stage from Production Tables                                          */ 
/*  Changes: 1) Add cytd tables to staging logic                                                   */
/*           2) Uncomment onetimebonuses process                                                   */
/*			 3) Replace not exists code for onetimebonuses with updated code                       */
/***************************************************************************************************/

CREATE PROCEDURE [dbo].[spImportStageToProd] @TipFirst char(3)
AS 

Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 

/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
Update Affiliat 
set YTDEarned = S.YTDEarned from Affiliat_Stage S join Affiliat A on S.Tipnumber = A.Tipnumber

--	Insert New Affiliat accounts
Insert into Affiliat 
	select * from Affiliat_Stage where acctid not in (select acctid from Affiliat )


------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
Update Customer 
Set 
Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
Update Customer_Stage 	Set RunAvailable = 0 

--	Insert New Customers from Customers_Stage
Insert into Customer 
	select * from Customer_Stage where Tipnumber not in (select Tipnumber from Customer )

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
Update Customer 
	set RunBalance = C.RunBalance + S.RunAvailableNew,
	      RunAvailable = C.RunAvailable + S.RunAvailableNew
From Customer_Stage S Join Customer C On S.Tipnumber = C.Tipnumber

----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
Insert Into History select * from History_Stage where SecID = 'NEW'

-- Set SecID in History_Stage so it doesn't get double posted. 
Update History_Stage  set SECID = 'Imported to Production '+ convert(char(20), GetDate(), 120)  where SecID = 'NEW'

--Change (1)-------------------- Move Staging CYTD Table to Production -----------------------------------  
truncate table dbo.Cust_YTD_Transaction
insert dbo.Cust_YTD_Transaction
select * from dbo.Cust_YTD_Transaction_Stage

--Change (1)-------------------- Move New CYTD Transactions to CYTD History ----------------------------
insert dbo.Cust_YTD_Transaction_History (dim_Cust_YTD_Transaction_History_Customer_Id, dim_Cust_YTD_Transaction_History_Current_Month_Transaction, dim_Cust_YTD_Transaction_History_Current_Month_Count, dim_Cust_YTD_Transaction_History_Current_YTD_Total, dim_Cust_YTD_Transaction_History_Prev_YTD_Total, dim_Cust_YTD_Transaction_History_Current_YTD_Count, dim_Cust_YTD_Transaction_History_Prev_YTD_Count, dim_Cust_YTD_Transaction_History_Last_Date_Processed)
select Customer_Id, Current_Month_Transaction, Current_Month_Count, Current_YTD_Total, Prev_YTD_Total, Current_YTD_Count, Prev_YTD_Count, Last_Date_Processed  from dbo.Cust_YTD_Transaction_Stage

----------------------- OneTimeBonuses - ---------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.

--insert into OneTimeBonuses 
--select * from OneTimeBonuses_stage where not exists
--( select * from OneTimeBonuses 
--where OneTimeBonuses_stage.Tipnumber = OneTimeBonuses.Tipnumber and 
--OneTimeBonuses_stage.Trancode = OneTimeBonuses.Trancode)

--Change (3)
insert into OneTimeBonuses (TipNumber, AcctID, Trancode, CustID, DateAwarded)
select TipNumber, AcctID, Trancode, CustID, DateAwarded
from OneTimeBonuses_Stage 
where TipNumber+Trancode not in ( select tipnumber+trancode from OneTimeBonuses )
GO


