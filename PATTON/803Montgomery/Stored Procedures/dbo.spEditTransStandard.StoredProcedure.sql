USE [803]
/****** Object:  StoredProcedure [dbo].[spEditTransStandard]    Script Date: 02/03/2011 09:49:48 ******/
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
GO

/****** Object:  StoredProcedure [dbo].[spEditTransStandard]    Script Date: 02/03/2011 09:49:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spEditTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spEditTransStandard]
GO


/******************************************************************************/
/*    Edit the TransStandard Table                                            */
/* BY:  Dan Foster                                                            */
/* DATE: 07/26/2010                                                           */
/* Changes:                                                                   */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spEditTransStandard] @DateAdded varchar(10) AS

/******************************************************************************/
-- Change 1) add joining bonus of 500 points
/******************************************************************************/ 

UPDATE TS SET tipnumber = Cp.dim_wk_CustomerPrep_TipNumber
FROM TransStandard TS join customerprep Cp on TS.Acctid = Cp.dim_wk_CustomerPrep_AcctID

Update TransStandard set TranCode = '64'
where TranAmt > 0

update TransStandard set TranCode = '34'
where TranAmt <= 0

delete from TransStandard
where (tranamt > -2 and TranAmt < 2)

update TransStandard set TranAmt = round(TranAmt/2,0), TranDate = @dateadded

update TransStandard set TranAmt = -(TranAmt)
where TranAmt < 0

-- CHANGE (1) New
insert transstandard (tipnumber,trandate,trancode,trannum,tranamt)
select tipnumber,trandate,'BN','1','500' from TransStandard where
TipNumber not in(select TipNumber from OneTimeBonuses_Stage where TranCode = 'BN')
 
-- RDT 20120802 
insert into OneTimeBonuses_Stage
	(TipNumber, Trancode , DateAwarded)
	select tipnumber,'BN', TranDate  
	from TransStandard 
	where	TipNumber not in ( select TipNumber from OneTimeBonuses_Stage where TranCode = 'BN' )
  
/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode



GO


