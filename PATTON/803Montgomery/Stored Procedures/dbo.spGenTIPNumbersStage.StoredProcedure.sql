USE [803]
GO
/****** Object:  StoredProcedure [dbo].[spGenTIPNumbersStage]    Script Date: 10/26/2010 13:55:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGenTIPNumbersStage]
AS 

declare @newnum bigint,@LastTipUsed char(15)

-- Set by acctid
update CP set dim_wk_CustomerPrep_TipNumber  = afs.tipnumber
from dbo.CustomerPrep CP join Affiliat_stage afs on cp.dim_wk_CustomerPrep_AcctID = afs.acctid
where Cp.dim_wk_CustomerPrep_TipNumber is null

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select distinct dim_wk_CustomerPrep_AcctID,dim_wk_CustomerPrep_TipNumber 	
from customerprep where dim_wk_CustomerPrep_TipNumber is null

exec rewardsnow.dbo.spGetLastTipNumberUsed 803, @LastTipUsed output

select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 803000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 803, @newnum

update CP set dim_wk_CustomerPrep_TipNumber = GT.tipnumber
from dbo.CustomerPrep CP join GenTip GT on CP.dim_wk_CustomerPrep_AcctID = GT.custid
where CP.dim_wk_CustomerPrep_TipNumber is null or gt.tipnumber = ' '
GO
