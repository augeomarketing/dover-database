USE [803]
GO

/****** Object:  StoredProcedure [dbo].[uspMoveCYTDtoProduction]    Script Date: 01/24/2011 16:14:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uspMoveCYTDtoProduction]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspMoveCYTDtoProduction]
GO

USE [803]
GO

/****** Object:  StoredProcedure [dbo].[uspMoveCYTDtoProduction]    Script Date: 01/24/2011 16:14:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		<Dan Foster>
-- Create date: <01/21/2011>
-- Description:	<Move CYTD stage table to production, insert current stage into CYTD history table>
-- ================================================================================================
CREATE PROCEDURE [dbo].[uspMoveCYTDtoProduction] @processdate varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	update dbo.Cust_YTD_Transaction_stage set Last_Date_Processed = @processdate
	
	insert dbo.Cust_YTD_Transaction_History
	select * from dbo.Cust_YTD_Transaction_Stage

	truncate table dbo.Cust_YTD_Transaction

	insert dbo.Cust_YTD_Transaction
	select * from dbo.Cust_YTD_Transaction_Stage

END

GO


