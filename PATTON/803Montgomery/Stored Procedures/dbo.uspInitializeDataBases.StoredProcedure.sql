USE [803]
GO
/****** Object:  StoredProcedure [dbo].[uspInitializeDataBases]    Script Date: 10/26/2010 13:55:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 9/7/2010
-- Description:	Inialize DataBases
-- =============================================
CREATE PROCEDURE [dbo].[uspInitializeDataBases]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  truncate table customerprep
  truncate table customer_stage
  truncate table history_stage
  truncate table affiliat_stage
  truncate table transstandard
  truncate table onetimebonuses_stage
  truncate table transactionPrep
    
END
GO
