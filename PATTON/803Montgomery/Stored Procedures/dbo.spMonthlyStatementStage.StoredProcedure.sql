USE [803]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyStatementStage]    Script Date: 03/16/2016 11:17:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMonthlyStatementStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMonthlyStatementStage]
GO

USE [803]
GO

/****** Object:  StoredProcedure [dbo].[spMonthlyStatementStage]    Script Date: 03/16/2016 11:17:13 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/*******************************************************************************/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[spMonthlyStatementStage]  @StartDateParm char(10), @EndDateParm char(10)

AS 

-- S Blanchette 8/2013 Added code for Expiring Points back in.

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city, state, zipcode)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state), zipcode
from customer_Stage


/* Load the statmement file with CREDIT purchases          */
--update Monthly_Statement_File
--set pointspurchasedCR =(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')
--where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')

/* Load the statmement file  with CREDIT returns            */
--update Monthly_Statement_File
--set pointsreturnedCR=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')
--where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File
set pointspurchased=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like'6%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like '6%')

/* Load the statmement file with DEBIT  returns            */
update Monthly_Statement_File
set pointsreturned=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode Like '3%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like '3%')

/* Load the statmement file with bonuses            */

UPDATE	Monthly_Statement_File
SET		pointsbonus = (select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like '[BFGH]%' and TRANCODE not in ('F9','G9','H9'))
WHERE EXISTS	(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like  '[BFGH]%' and TRANCODE not in ('F9','G9','H9'))

/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
update Monthly_Statement_File
set pointsadded=pointsadded + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='DR')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='DR')


/* Load the statmement file with total point increases */
update Monthly_Statement_File
--set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded
set pointsincreased=  pointspurchased + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in ('DE','XP','F9','G9','H9'))
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in ('DE','XP','F9','G9','H9'))

/* Add EP to  minus adjustments    */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='EP')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='EP')

-- RDT 8/28/2007  Added expired Points 
/* Add expired Points */
--update Monthly_Statement_File
--set PointsExpire = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='XP')
--where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='XP')

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
--set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the monthly statement table */
--Update Monthly_Statement_file 
--set acctid = a.acctid from affiliat_Stage a where Monthly_Statement_file.tipnumber = a.tipnumber


GO


