USE [803]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 10/26/2010 13:55:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from CustomerPrep into the customer_STAGE table      */
/*    it only updates the customer demographic data                           */
/*                                                                            */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE                          */
/*                                                                            */
/******************************************************************************/	
create PROCEDURE [dbo].[spLoadCustomerStage] @EndDate varchar(10) 
as
/******************************************************************************/	
/*Add New Customers                                                           */
/******************************************************************************/	

update CUSTOMER_Stage set [STATUS] = 'I', StatusDescription = 'InActive[I]',Misc1 = Misc1 + 1

Insert into Customer_Stage (TIPNUMBER)
select distinct dim_wk_CustomerPrep_TipNumber from CustomerPrep
where not exists(select TipNumber from CUSTOMER_Stage where TipNumber = CustomerPrep.dim_wk_CustomerPrep_TipNumber)
and dim_wk_CustomerPrep_TipNumber is not null

update Customer_Stage set RunAvailable = '0',RUNBALANCE = '0', RunRedeemed = '0', RunBalanceNew = '0',
RunAvailableNew = '0', DATEADDED = @EndDate, Misc1 = '0'
where Customer_Stage.DATEADDED is null

/******************************************************************************/	
/* Update Existing Customers                                                  */
/******************************************************************************/	
Update CS
Set 
LASTNAME 	= Left(rtrim(CP.dim_wk_CustomerPrep_LastName),40)
,ACCTNAME1 	= left(rtrim(CP.dim_wk_CustomerPrep_Name1),40 )
,TIPFIRST = Left(CP.dim_wk_CustomerPrep_TipNumber,3)
,TIPLAST = RIGHT(CP.dim_wk_CustomerPrep_TipNumber,12)
,ACCTNAME2 	= left(rtrim(CP.dim_wk_CustomerPrep_Name2),40 )
,ACCTNAME3 	= ' ' --left(rtrim(CustomerPrep.acctNAME3),40 )
,ACCTNAME4 	= ' ' --left(rtrim(CustomerPrep.acctNAME4),40 )
,ACCTNAME5 	= ' ' --left(rtrim(CustomerPrep.acctNAME5),40 )
,ACCTNAME6 	= ' ' --left(rtrim(CustomerPrep.acctNAME6),40 )
,ADDRESS1 	= left(rtrim( CP.dim_wk_CustomerPrep_Address1),40)
,ADDRESS2  	= left(rtrim( CP.dim_wk_CustomerPrep_Address2),40)
,ADDRESS3  	= left(rtrim( CP.dim_wk_CustomerPrep_Address3),40)
,ADDRESS4  	= left(rtrim( CP.dim_wk_CustomerPrep_Address4),40)
,CITY 		= CP.dim_wk_CustomerPrep_City
,STATE		= CP.dim_wk_CustomerPrep_State
,ZIPCODE 	= ltrim(CP.dim_wk_CustomerPrep_ZipCode)
,STATUS	= 'A'
,StatusDescription = 'Active[A]'
,Misc1 = '0'
from customer_stage CS join CustomerPrep CP on CP.dim_wk_CustomerPrep_TipNumber = Cs.TIPNUMBER 

update cs set acctname2 = CP.dim_wk_CustomerPrep_Name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.dim_wk_CustomerPrep_TipNumber
where CP.dim_wk_CustomerPrep_Name1 not in (cs.acctname1)

update cs set acctname3 = CP.dim_wk_CustomerPrep_Name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.dim_wk_CustomerPrep_TipNumber
where CP.dim_wk_CustomerPrep_Name1 not in (cs.acctname1,cs.acctname2)

update cs set acctname4 = CP.dim_wk_CustomerPrep_Name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.dim_wk_CustomerPrep_TipNumber
where CP.dim_wk_CustomerPrep_Name1 not in (cs.acctname1,cs.acctname2,acctname3)

update cs set acctname5 = CP.dim_wk_CustomerPrep_Name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.dim_wk_CustomerPrep_TipNumber
where CP.dim_wk_CustomerPrep_Name1 not in (cs.acctname1,cs.acctname2,acctname3,acctname4)

update cs set acctname6 = CP.dim_wk_CustomerPrep_Name1
from CUSTOMER_Stage cs join CustomerPrep CP on cs.TIPNUMBER = CP.dim_wk_CustomerPrep_TipNumber
where CP.dim_wk_CustomerPrep_Name1 not in (cs.acctname1,cs.acctname2,acctname3,acctname4,acctname5)
GO
