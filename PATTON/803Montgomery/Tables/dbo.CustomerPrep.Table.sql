USE [803]
GO
/****** Object:  Table [dbo].[CustomerPrep]    Script Date: 10/26/2010 13:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerPrep](
	[dim_wk_CustomerPrep_RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_wk_CustomerPrep_BankNum] [varchar](4) NULL,
	[dim_wk_CustomerPrep_AcctID] [varchar](12) NULL,
	[dim_wk_CustomerPrep_Name1] [varchar](40) NULL,
	[dim_wk_CustomerPrep_Name2] [varchar](40) NULL,
	[dim_wk_CustomerPrep_Address1] [varchar](40) NULL,
	[dim_wk_CustomerPrep_Address2] [varchar](40) NULL,
	[dim_wk_CustomerPrep_Address3] [varchar](40) NULL,
	[dim_wk_CustomerPrep_Address4] [varchar](40) NULL,
	[dim_wk_CustomerPrep_TIN] [varchar](9) NULL,
	[dim_wk_CustomerPrep_Hphone] [varchar](10) NULL,
	[dim_wk_CustomerPrep_Wphone] [varchar](10) NULL,
	[dim_wk_CustomerPrep_NumBillPay] [varchar](3) NULL,
	[dim_wk_CustomerPrep_EmailStmt] [varchar](1) NULL,
	[dim_wk_CustomerPrep_City] [varchar](40) NULL,
	[dim_wk_CustomerPrep_State] [varchar](2) NULL,
	[dim_wk_CustomerPrep_ZipCode] [varchar](10) NULL,
	[dim_wk_CustomerPrep_LastName] [varchar](40) NULL,
	[dim_wk_CustomerPrep_TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_CustomerPrep] PRIMARY KEY CLUSTERED 
(
	[dim_wk_CustomerPrep_RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
