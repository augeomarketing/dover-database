USE [803]
GO
/****** Object:  Table [dbo].[OneTimeBonuses_Stage]    Script Date: 10/26/2010 13:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OneTimeBonuses_Stage](
	[TipNumber] [char](15) NULL,
	[AcctID] [char](25) NULL,
	[Trancode] [char](2) NULL,
	[CustID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
