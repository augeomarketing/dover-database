USE [803]
GO
/****** Object:  Table [dbo].[TransactionPrep]    Script Date: 10/26/2010 13:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPrep](
	[dim_wk_transactionprep_rowid] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_wk_transactionprep_banknum] [varchar](4) NULL,
	[dim_wk_transactionprep_AcctID] [varchar](12) NULL,
	[dim_wk_transactionprep_PIN_AMT] [numeric](18, 2) NULL,
	[dim_wk_transactionprep_PIN_CT] [int] NULL,
	[dim_wk_transactionprep_SIG_AMT] [numeric](18, 2) NULL,
	[dim_wk_transactionprep_SIG_CT] [int] NULL,
	[dim_wk_transactionprep_Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
