USE [803]
GO

/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 03/03/2011 15:39:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HISTORY_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[HISTORY_Stage]
GO

USE [803]
GO

/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 03/03/2011 15:39:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](9, 0) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


