USE [803]
GO
/****** Object:  Table [dbo].[TransStandard]    Script Date: 10/26/2010 13:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransStandard](
	[RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[TipNumber] [varchar](15) NULL,
	[TranDate] [varchar](10) NULL,
	[Acctid] [varchar](16) NULL,
	[TranCode] [varchar](2) NULL,
	[TranNum] [int] NULL,
	[TranAmt] [decimal](18, 2) NULL,
	[TranType] [varchar](50) NULL,
	[Ratio] [int] NULL,
 CONSTRAINT [PK_TransStandard] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
