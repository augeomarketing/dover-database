USE [803]
GO
/****** Object:  Table [dbo].[Cust_YTD_Transaction_Stage]    Script Date: 10/26/2010 13:56:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cust_YTD_Transaction_Stage](
	[Customer_Id] [varchar](16) NOT NULL,
	[Current_Month_Transaction] [numeric](18, 2) NULL,
	[Current_Month_Count] [int] NULL,
	[Current_YTD_Total] [numeric](18, 2) NULL,
	[Prev_YTD_Total] [numeric](18, 2) NULL,
	[Current_YTD_Count] [int] NULL,
	[Prev_YTD_Count] [int] NULL,
	[Last_Date_Processed] [date] NULL,
 CONSTRAINT [PK_Cust_YTD_Transaction_Stage] PRIMARY KEY CLUSTERED 
(
	[Customer_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
