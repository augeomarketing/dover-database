USE [803]
GO

/****** Object:  Table [dbo].[Cust_YTD_Transaction_History]    Script Date: 11/11/2010 08:33:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Cust_YTD_Transaction_History]') AND type in (N'U'))
DROP TABLE [dbo].[Cust_YTD_Transaction_History]
GO

USE [803]
GO

/****** Object:  Table [dbo].[Cust_YTD_Transaction_History]    Script Date: 11/11/2010 08:33:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Cust_YTD_Transaction_History](
	[sid_Cust_YTD_Transaction_History_RowID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_Cust_YTD_Transaction_History_Customer_Id] [varchar](16) NOT NULL,
	[dim_Cust_YTD_Transaction_History_Current_Month_Transaction] [numeric](18, 2) NULL,
	[dim_Cust_YTD_Transaction_History_Current_Month_Count] [int] NULL,
	[dim_Cust_YTD_Transaction_History_Current_YTD_Total] [numeric](18, 2) NULL,
	[dim_Cust_YTD_Transaction_History_Prev_YTD_Total] [numeric](18, 2) NULL,
	[dim_Cust_YTD_Transaction_History_Current_YTD_Count] [int] NULL,
	[dim_Cust_YTD_Transaction_History_Prev_YTD_Count] [int] NULL,
	[dim_Cust_YTD_Transaction_History_Last_Date_Processed] [date] NULL,
 CONSTRAINT [PK_Cust_YTD_Transaction_History] PRIMARY KEY CLUSTERED 
(
	[sid_Cust_YTD_Transaction_History_RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


