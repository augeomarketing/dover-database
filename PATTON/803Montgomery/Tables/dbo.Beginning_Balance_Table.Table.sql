USE [803]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 10/26/2010 13:56:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [varchar](15) NOT NULL,
	[MonthBeg1] [int] NOT NULL,
	[MonthBeg2] [int] NOT NULL,
	[MonthBeg3] [int] NOT NULL,
	[MonthBeg4] [int] NOT NULL,
	[MonthBeg5] [int] NOT NULL,
	[MonthBeg6] [int] NOT NULL,
	[MonthBeg7] [int] NOT NULL,
	[MonthBeg8] [int] NOT NULL,
	[MonthBeg9] [int] NOT NULL,
	[MonthBeg10] [int] NOT NULL,
	[MonthBeg11] [int] NOT NULL,
	[MonthBeg12] [int] NOT NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
