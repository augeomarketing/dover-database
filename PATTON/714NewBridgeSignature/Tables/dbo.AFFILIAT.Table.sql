USE [714NewBridgeSignature]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 11/17/2010 11:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctTYPE] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [float] NOT NULL,
	[CustID] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
