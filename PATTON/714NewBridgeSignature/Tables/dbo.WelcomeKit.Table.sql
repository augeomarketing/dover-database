USE [714NewBridgeSignature]
GO
/****** Object:  Table [dbo].[WelcomeKit]    Script Date: 11/17/2010 11:54:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WelcomeKit](
	[TIPNUMBER] [varchar](15) NULL,
	[AcctName1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [varchar](160) NULL,
	[AcctName4] [char](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
