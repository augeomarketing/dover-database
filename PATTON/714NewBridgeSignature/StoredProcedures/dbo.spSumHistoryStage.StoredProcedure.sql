USE [714NewBridgeSignature]
GO
/****** Object:  StoredProcedure [dbo].[spSumHistoryStage]    Script Date: 11/17/2010 11:53:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Foster
-- Create date: 9/24/2009
-- Description:	Sum Input transaction totals
-- =============================================
CREATE PROCEDURE [dbo].[spSumHistoryStage]  @Purchase numeric(8) output,@Returns numeric(8) output,
@Bonuses numeric(8) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select @Purchase = (SELECT SUM(Points)FROM history_stage where trancode = '63')
	select @Returns = (SELECT SUM(Points) FROM history_stage where trancode = '33')
	select @Bonuses = (SELECT SUM(Points) FROM history_stage where trancode like 'B%')
	
 END
GO
