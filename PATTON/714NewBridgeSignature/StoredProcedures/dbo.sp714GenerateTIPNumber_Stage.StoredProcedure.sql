USE [714NewBridgeSignature]
GO
/****** Object:  StoredProcedure [dbo].[sp714GenerateTIPNumber_Stage]    Script Date: 11/17/2010 11:53:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp714GenerateTIPNumber_Stage]
AS 
update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.custid = b.custid

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat b
where a.cardnumber = b.acctid

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from roll_customer

declare @newnum bigint

	/*    Create new tip          */
declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 714, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 714000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 714, @newnum

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.custid = b.custid and a. tipnumber is null or a.tipnumber = ' '
GO
