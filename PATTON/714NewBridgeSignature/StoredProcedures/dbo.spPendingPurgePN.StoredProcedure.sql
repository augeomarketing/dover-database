USE [714NewBridgeSignature]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 11/17/2010 13:04:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO

USE [714NewBridgeSignature]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 11/17/2010 13:04:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
/* REVISION: 1) add update status to 'C' where status is 'P' and no history record */
/* pending purge procedure for FI's Using Points Now                               */
/***********************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */

declare @tipnumber nvarchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

UPDATE input_delete_accounts SET Tipnumber = b.tipnumber
FROM input_delete_accounts a, affiliat b
WHERE a.cardnumber = b.acctid
	
UPDATE AFFILIAT SET AcctStatus = 'C'
WHERE (ACCTID = (SELECT cardnumber FROM input_delete_accounts
                 WHERE affiliat.acctid = input_delete_accounts.cardnumber))
                    
UPDATE CUSTOMER SET STATUS = 'P', StatusDescription = 'Pending Purge'
WHERE (STATUS = 'A') AND (NOT EXISTS(SELECT 1 FROM affiliat
WHERE tipnumber = customer.tipnumber AND (acctstatus = 'A' OR acctstatus IS NULL)))

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge(tipnumber)
select tipnumber from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

update customer set status = 'C', statusdescription = 'Closed'
from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
where @purgedate >= (select max(histdate) from history
where c.tipnumber  = tipnumber)

-- Revision (1)
update customer set status = 'C', statusdescription = 'Closed'
where Status = 'P' and TIPNUMBER not in (select TIPNUMBER from HISTORY)

delete from PendingPurge
    where tipnumber in (select tipnumber from customer where status = 'C')

GO


