USE [561SanDiegoNationalConsumer]
GO
/****** Object:  Table [dbo].[wrkcustid3]    Script Date: 09/25/2009 11:59:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkcustid3](
	[custid] [varchar](20) NOT NULL,
	[tip1] [char](15) NULL,
	[tip2] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
