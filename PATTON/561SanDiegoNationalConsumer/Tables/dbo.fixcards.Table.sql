USE [561SanDiegoNationalConsumer]
GO
/****** Object:  Table [dbo].[fixcards]    Script Date: 09/25/2009 11:59:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixcards](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NOT NULL,
	[custid] [char](13) NULL,
	[recid] [numeric](18, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
