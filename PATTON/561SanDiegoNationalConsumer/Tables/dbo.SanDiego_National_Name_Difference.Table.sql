USE [561SanDiegoNationalConsumer]
GO
/****** Object:  Table [dbo].[SanDiego_National_Name_Difference]    Script Date: 09/25/2009 11:59:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SanDiego_National_Name_Difference](
	[tipnumber] [char](15) NULL,
	[OriginalName1] [char](40) NULL,
	[OriginalName2] [char](40) NULL,
	[OriginalAddress1] [char](40) NULL,
	[OriginalAddress2] [char](40) NULL,
	[OriginalCity] [char](40) NULL,
	[OriginalState] [char](2) NULL,
	[OriginalZip] [char](9) NULL,
	[NewName1] [char](40) NULL,
	[NewName2] [char](40) NULL,
	[NewAddress1] [char](40) NULL,
	[NewAddress2] [char](40) NULL,
	[NewCity] [char](40) NULL,
	[NewState] [char](2) NULL,
	[NewZip] [char](9) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
