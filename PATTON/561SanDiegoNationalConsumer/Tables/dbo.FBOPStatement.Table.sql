USE [561SanDiegoNationalConsumer]
GO
/****** Object:  Table [dbo].[FBOPStatement]    Script Date: 09/25/2009 11:59:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FBOPStatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedBUS] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusBUS] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedBUS] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
	[PointstoExpire] [numeric](18, 0) NULL,
 CONSTRAINT [PK_FBOPStatement] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__18EBB532]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__19DFD96B]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1AD3FDA4]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1BC821DD]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1CBC4616]  DEFAULT (0) FOR [PointsPurchasedHE]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  DEFAULT (0) FOR [PointsPurchasedBUS]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1DB06A4F]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1EA48E88]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__1F98B2C1]  DEFAULT (0) FOR [PointsBonusHE]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  DEFAULT (0) FOR [PointsBonusBUS]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF_FBOPStatement_PointsBonusEmpCR]  DEFAULT (0) FOR [PointsBonusEmpCR]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF_FBOPStatement_PointsBonusEmpDB]  DEFAULT (0) FOR [PointsBonusEmpDB]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__208CD6FA]  DEFAULT (0) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__2180FB33]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__22751F6C]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__236943A5]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__245D67DE]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__25518C17]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__2645B050]  DEFAULT (0) FOR [PointsReturnedHE]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  DEFAULT (0) FOR [PointsReturnedBUS]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__2739D489]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__Point__282DF8C2]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__PNTDE__29221CFB]  DEFAULT (0) FOR [PNTDEBIT]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__PNTMO__2A164134]  DEFAULT (0) FOR [PNTMORT]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF__FBOPState__PNTHO__2B0A656D]  DEFAULT (0) FOR [PNTHOME]
GO
ALTER TABLE [dbo].[FBOPStatement] ADD  CONSTRAINT [DF_FBOPStatement_PointstoExpire]  DEFAULT (0) FOR [PointstoExpire]
GO
