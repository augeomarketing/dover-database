USE [561SanDiegoNationalConsumer]
GO
/****** Object:  Table [dbo].[wrkcustid2]    Script Date: 09/25/2009 11:59:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkcustid2](
	[custid] [char](13) NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[email] [varchar](50) NULL,
	[rd] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
