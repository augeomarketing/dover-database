USE [532LakeMillsCommercial]
GO
/****** Object:  Table [dbo].[wrkbeg]    Script Date: 09/25/2009 11:08:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkbeg](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
