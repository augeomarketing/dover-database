USE [262]
GO
/****** Object:  Table [dbo].[dupmemberSec]    Script Date: 09/25/2013 10:38:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dupmemberSec]') AND type in (N'U'))
DROP TABLE [dbo].[dupmemberSec]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dupmemberSec]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[dupmemberSec](
	[TipNumber] [varchar](15) NULL,
	[Member] [varchar](50) NULL,
	[Name1] [varchar](40) NOT NULL,
	[Name2] [varchar](40) NULL,
	[Street] [varchar](40) NOT NULL,
	[City] [varchar](40) NOT NULL,
	[State] [varchar](3) NOT NULL,
	[registered] [char](1) NULL,
	[flag] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
