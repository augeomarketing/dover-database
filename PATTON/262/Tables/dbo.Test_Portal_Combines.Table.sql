USE [262]
GO
/****** Object:  Table [dbo].[Test_Portal_Combines]    Script Date: 09/25/2013 10:38:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Test_Portal_Combines]') AND type in (N'U'))
DROP TABLE [dbo].[Test_Portal_Combines]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Test_Portal_Combines]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Test_Portal_Combines](
	[TipFirst] [char](3) NULL,
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NOT NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NOT NULL,
	[usid] [int] NOT NULL,
	[Member] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
