USE [262]
GO
/****** Object:  Table [dbo].[tippurchases]    Script Date: 09/25/2013 10:38:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tippurchases]') AND type in (N'U'))
DROP TABLE [dbo].[tippurchases]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tippurchases]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[tippurchases](
	[tipnumber] [varchar](15) NOT NULL,
	[total] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
