USE [262]
GO
/****** Object:  Table [dbo].[FBOPQuarterlyStatement]    Script Date: 09/25/2013 10:38:42 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__7FEAFD3E]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__7FEAFD3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__7FEAFD3E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__00DF2177]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__00DF2177]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__00DF2177]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__01D345B0]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__01D345B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__01D345B0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__02C769E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__02C769E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__02C769E9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__03BB8E22]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__03BB8E22]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__03BB8E22]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__04AFB25B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__04AFB25B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__04AFB25B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__05A3D694]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__05A3D694]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__05A3D694]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0697FACD]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0697FACD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0697FACD]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__078C1F06]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__078C1F06]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__078C1F06]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0880433F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0880433F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0880433F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__09746778]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__09746778]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__09746778]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0A688BB1]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0A688BB1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0A688BB1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0B5CAFEA]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0B5CAFEA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0B5CAFEA]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0C50D423]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0C50D423]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0C50D423]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0D44F85C]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0D44F85C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0D44F85C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0E391C95]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0E391C95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0E391C95]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0F2D40CE]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0F2D40CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__0F2D40CE]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__10216507]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__10216507]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__10216507]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__11158940]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__11158940]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__11158940]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__1209AD79]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__1209AD79]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__1209AD79]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__12FDD1B2]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__12FDD1B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__12FDD1B2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__13F1F5EB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__13F1F5EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__Point__13F1F5EB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__PNTDE__14E61A24]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__PNTDE__14E61A24]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__PNTDE__14E61A24]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__PNTMO__15DA3E5D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__PNTMO__15DA3E5D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__PNTMO__15DA3E5D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__PNTHO__16CE6296]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__PNTHO__16CE6296]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] DROP CONSTRAINT [DF__FBOPQuart__PNTHO__16CE6296]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]') AND type in (N'U'))
DROP TABLE [dbo].[FBOPQuarterlyStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FBOPQuarterlyStatement](
	[Tipnumber] [nchar](15) NOT NULL,
	[ACCTNAME1] [nvarchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsPurchasedHE] [numeric](18, 0) NULL,
	[PointsPurchasedBUS] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonusHE] [numeric](18, 0) NULL,
	[PointsBonusBUS] [numeric](18, 0) NULL,
	[PointsBonusEmpCR] [numeric](18, 0) NULL,
	[PointsBonusEmpDB] [numeric](18, 0) NULL,
	[PointsBonus] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturnedHE] [numeric](18, 0) NULL,
	[PointsReturnedBUS] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[PNTDEBIT] [numeric](18, 0) NULL,
	[PNTMORT] [numeric](18, 0) NULL,
	[PNTHOME] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[DDANUM] [char](25) NULL,
 CONSTRAINT [PK_FBOPQuarterlyStatement] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__7FEAFD3E]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__7FEAFD3E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__7FEAFD3E]  DEFAULT (0) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__00DF2177]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__00DF2177]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__00DF2177]  DEFAULT (0) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__01D345B0]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__01D345B0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__01D345B0]  DEFAULT (0) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__02C769E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__02C769E9]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__02C769E9]  DEFAULT (0) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__03BB8E22]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__03BB8E22]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__03BB8E22]  DEFAULT (0) FOR [PointsPurchasedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__04AFB25B]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__04AFB25B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__04AFB25B]  DEFAULT (0) FOR [PointsPurchasedBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__05A3D694]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__05A3D694]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__05A3D694]  DEFAULT (0) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0697FACD]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0697FACD]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0697FACD]  DEFAULT (0) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__078C1F06]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__078C1F06]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__078C1F06]  DEFAULT (0) FOR [PointsBonusHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0880433F]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0880433F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0880433F]  DEFAULT (0) FOR [PointsBonusBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__09746778]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__09746778]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__09746778]  DEFAULT (0) FOR [PointsBonusEmpCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0A688BB1]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0A688BB1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0A688BB1]  DEFAULT (0) FOR [PointsBonusEmpDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0B5CAFEA]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0B5CAFEA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0B5CAFEA]  DEFAULT (0) FOR [PointsBonus]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0C50D423]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0C50D423]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0C50D423]  DEFAULT (0) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0D44F85C]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0D44F85C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0D44F85C]  DEFAULT (0) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0E391C95]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0E391C95]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0E391C95]  DEFAULT (0) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__0F2D40CE]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__0F2D40CE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__0F2D40CE]  DEFAULT (0) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__10216507]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__10216507]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__10216507]  DEFAULT (0) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__11158940]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__11158940]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__11158940]  DEFAULT (0) FOR [PointsReturnedHE]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__1209AD79]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__1209AD79]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__1209AD79]  DEFAULT (0) FOR [PointsReturnedBUS]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__12FDD1B2]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__12FDD1B2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__12FDD1B2]  DEFAULT (0) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__Point__13F1F5EB]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__Point__13F1F5EB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__Point__13F1F5EB]  DEFAULT (0) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__PNTDE__14E61A24]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__PNTDE__14E61A24]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__PNTDE__14E61A24]  DEFAULT (0) FOR [PNTDEBIT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__PNTMO__15DA3E5D]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__PNTMO__15DA3E5D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__PNTMO__15DA3E5D]  DEFAULT (0) FOR [PNTMORT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__FBOPQuart__PNTHO__16CE6296]') AND parent_object_id = OBJECT_ID(N'[dbo].[FBOPQuarterlyStatement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__FBOPQuart__PNTHO__16CE6296]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[FBOPQuarterlyStatement] ADD  CONSTRAINT [DF__FBOPQuart__PNTHO__16CE6296]  DEFAULT (0) FOR [PNTHOME]
END


End
GO
