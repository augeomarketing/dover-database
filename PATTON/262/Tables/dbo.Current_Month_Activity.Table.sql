USE [262]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 09/25/2013 10:38:42 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Endin__3D5E1FD2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Endin__3D5E1FD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Endin__3D5E1FD2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Incre__3E52440B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Incre__3E52440B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Incre__3E52440B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Decre__3F466844]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Decre__3F466844]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Decre__3F466844]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Adjus__403A8C7D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Adjus__403A8C7D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] DROP CONSTRAINT [DF__Current_M__Adjus__403A8C7D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
DROP TABLE [dbo].[Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Endin__3D5E1FD2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Endin__3D5E1FD2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Endin__3D5E1FD2]  DEFAULT (0) FOR [EndingPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Incre__3E52440B]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Incre__3E52440B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Incre__3E52440B]  DEFAULT (0) FOR [Increases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Decre__3F466844]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Decre__3F466844]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Decre__3F466844]  DEFAULT (0) FOR [Decreases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Current_M__Adjus__403A8C7D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Current_Month_Activity]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Current_M__Adjus__403A8C7D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Current_Month_Activity] ADD  CONSTRAINT [DF__Current_M__Adjus__403A8C7D]  DEFAULT (0) FOR [AdjustedEndingPoints]
END


End
GO
