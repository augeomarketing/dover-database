USE [262]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 09/25/2013 10:38:42 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__YTDEar__1920BF5C]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__YTDEar__1920BF5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] DROP CONSTRAINT [DF__AFFILIAT__YTDEar__1920BF5C]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__CustID__1A14E395]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__CustID__1A14E395]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] DROP CONSTRAINT [DF__AFFILIAT__CustID__1A14E395]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
DROP TABLE [dbo].[AFFILIAT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AFFILIAT]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
PRIMARY KEY CLUSTERED 
(
	[ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__YTDEar__1920BF5C]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__YTDEar__1920BF5C]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] ADD  CONSTRAINT [DF__AFFILIAT__YTDEar__1920BF5C]  DEFAULT (0) FOR [YTDEarned]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__AFFILIAT__CustID__1A14E395]') AND parent_object_id = OBJECT_ID(N'[dbo].[AFFILIAT]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AFFILIAT__CustID__1A14E395]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AFFILIAT] ADD  CONSTRAINT [DF__AFFILIAT__CustID__1A14E395]  DEFAULT (0) FOR [CustID]
END


End
GO
