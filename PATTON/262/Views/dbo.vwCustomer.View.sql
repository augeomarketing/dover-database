USE [262]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 09/25/2013 10:40:20 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer]'))
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwCustomer]
			 as
			 select *
			 from [595StFrancisXFCU].dbo.customer

'
GO
