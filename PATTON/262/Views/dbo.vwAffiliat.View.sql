USE [262]
GO
/****** Object:  View [dbo].[vwAffiliat]    Script Date: 09/25/2013 10:40:20 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAffiliat]'))
DROP VIEW [dbo].[vwAffiliat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwAffiliat]'))
EXEC dbo.sp_executesql @statement = N'
			 create view [dbo].[vwAffiliat]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate(''x'', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			 from [595StFrancisXFCU].dbo.affiliat

'
GO
