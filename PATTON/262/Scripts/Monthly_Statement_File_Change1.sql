use [262]
go
/*
   Wednesday, September 25, 201310:44:43 AM
   User: .
   Server: doolittle\rn
   Database: 262
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Monthly_Statement_File ADD
	PurchasedPoints numeric(18, 0) NULL
GO
ALTER TABLE dbo.Monthly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
