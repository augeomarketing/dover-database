use [262]
go

/*
   Wednesday, November 06, 20138:57:15 AM
   User: 
   Server: doolittle\rn
   Database: 262
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.CustomerDeleted ADD
	BonusFlag char(1) NULL
GO
ALTER TABLE dbo.CustomerDeleted SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
