/*
   Tuesday, October 08, 20134:43:28 PM
   User: 
   Server: doolittle\rn
   Database: 262
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsBegin DEFAULT (0) FOR PointsBegin
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsPurchasedCR DEFAULT (0) FOR PointsPurchasedCR
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsBonusCR DEFAULT (0) FOR PointsBonusCR
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsAdded DEFAULT (0) FOR PointsAdded
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsPurchasedDB DEFAULT (0) FOR PointsPurchasedDB
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsBonusDB DEFAULT (0) FOR PointsBonusDB
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsBonusMER DEFAULT (0) FOR PointsBonusMER
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsIncreased DEFAULT (0) FOR PointsIncreased
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsRedeemed DEFAULT (0) FOR PointsRedeemed
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsReturnedCR DEFAULT (0) FOR PointsReturnedCR
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsSubtracted DEFAULT (0) FOR PointsSubtracted
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsReturnedDB DEFAULT (0) FOR PointsReturnedDB
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsDecreased DEFAULT (0) FOR PointsDecreased
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PointsToExpire DEFAULT (0) FOR PointsToExpire
GO
ALTER TABLE dbo.Monthly_Statement_File ADD CONSTRAINT
	DF_Monthly_Statement_File_PurchasedPoints DEFAULT (0) FOR PurchasedPoints
GO
ALTER TABLE dbo.Monthly_Statement_File SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
