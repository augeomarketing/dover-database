USE [52EBeneficialCons]
GO
/****** Object:  Table [dbo].[Transum]    Script Date: 09/24/2009 16:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transum](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [nchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[NUMPURCH] [nchar](6) NULL,
	[AMTPURCH] [decimal](9, 0) NULL,
	[NUMCR] [nchar](6) NULL,
	[AMTCR] [decimal](9, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [decimal](5, 0) NULL,
	[cardtype] [char](1) NULL,
	[SigvsPin] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
