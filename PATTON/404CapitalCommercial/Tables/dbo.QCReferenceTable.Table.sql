USE [404CapitalCommercial]
GO
/****** Object:  Table [dbo].[QCReferenceTable]    Script Date: 10/13/2009 10:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QCReferenceTable](
	[QCFlag] [varchar](2) NULL,
	[ROWCNT] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
