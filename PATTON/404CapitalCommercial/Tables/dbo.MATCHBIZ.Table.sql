USE [404CapitalCommercial]
GO
/****** Object:  Table [dbo].[MATCHBIZ]    Script Date: 10/13/2009 10:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MATCHBIZ](
	[name] [nvarchar](255) NULL,
	[NewCardTranslated] [nvarchar](255) NULL,
	[OldCardTranslated] [nvarchar](255) NULL,
	[NewAccountTranslated] [nvarchar](255) NULL,
	[OldAccountTranslated] [nvarchar](255) NULL,
	[Old TIP] [nvarchar](255) NULL,
	[OldAcctBalance] [nvarchar](255) NULL,
	[NewTip] [nvarchar](255) NULL,
	[NewAcctBalance] [nvarchar](255) NULL
) ON [PRIMARY]
GO
