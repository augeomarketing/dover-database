USE [404CapitalCommercial]
GO

/****** Object:  Table [dbo].[NewLastName]    Script Date: 12/14/2009 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[NewLastName](
	[Tipnumber] [varchar](15) NOT NULL,
	[AcctName1] [varchar](50) NULL,
	[ReversedName] [varchar](50) NULL,
	[LastSpace] [int] NULL,
	[Lastname] [varchar](20) NULL
) 

GO

SET ANSI_PADDING OFF
GO


