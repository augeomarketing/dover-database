USE [404CapitalCommercial]
GO
/****** Object:  Table [dbo].[lastmnth]    Script Date: 10/13/2009 10:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lastmnth](
	[MATCH1] [nvarchar](1) NULL,
	[MATCH2] [nvarchar](1) NULL,
	[MATCH3] [nvarchar](1) NULL,
	[MATCH4] [nvarchar](1) NULL,
	[MATCH5] [nvarchar](1) NULL,
	[RNUM] [nvarchar](2) NULL,
	[CUSTNUM] [nvarchar](20) NULL,
	[OLDCUST] [nvarchar](20) NULL,
	[STATUS] [nvarchar](1) NULL,
	[NEWCUST] [nvarchar](4) NULL,
	[LASTNAME] [nvarchar](10) NULL,
	[TIPNUMBER] [nvarchar](20) NULL,
	[BANK] [nvarchar](3) NULL,
	[BIN] [nvarchar](6) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[NAME1] [nvarchar](40) NULL,
	[NAME2] [nvarchar](40) NULL,
	[NAME3] [nvarchar](40) NULL,
	[NAME4] [nvarchar](40) NULL,
	[NAME5] [nvarchar](40) NULL,
	[NAME6] [nvarchar](40) NULL,
	[ADDRESS1] [nvarchar](50) NULL,
	[CSZ] [nvarchar](40) NULL,
	[CITY] [nvarchar](30) NULL,
	[STATE] [nvarchar](2) NULL,
	[ZIP] [nvarchar](10) NULL,
	[PHONE] [nvarchar](10) NULL,
	[LAST6] [nvarchar](6) NULL
) ON [PRIMARY]
GO
