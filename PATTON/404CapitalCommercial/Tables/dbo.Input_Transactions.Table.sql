USE [404CapitalCommercial]
GO
/****** Object:  Table [dbo].[Input_Transactions]    Script Date: 10/13/2009 10:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transactions](
	[CardNumber] [varchar](19) NULL,
	[Unused] [varchar](19) NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [int] NULL,
	[TipNumber] [varchar](15) NULL,
	[TransDate] [varchar](10) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
