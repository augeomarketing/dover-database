USE [404CapitalCommercial]
GO

/****** Object:  Table [dbo].[Input_customer]    Script Date: 06/02/2010 10:10:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_customer]') AND type in (N'U'))
DROP TABLE [dbo].[Input_customer]
GO

USE [404CapitalCommercial]
GO

/****** Object:  Table [dbo].[Input_customer]    Script Date: 06/02/2010 10:10:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_customer](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[Acctid] [varchar](25) NULL,
	[last6] [char](6) NULL,
	[Acctname1] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[City] [varchar](30) NULL,
	[State] [varchar](5) NULL,
	[Zip] [varchar](15) NULL,
	[HomePhone] [varchar](10) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [varchar](40) NULL,
	[Acctname4] [varchar](40) NULL,
	[Acctname5] [varchar](40) NULL,
	[acctname6] [varchar](40) NULL,
	[Acctname7] [varchar](40) NULL,
	[Custid] [varchar](20) NULL,
	[status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Lastname] [varchar](40) NULL,
	[WorkPhone] [varchar](10) NULL,
	[DateAdded] [datetime] NULL,
	[BusinessFlag] [varchar](1) NULL,
	[SegmentCode] [varchar](2) NULL,
	[Filler] [varchar](87) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


