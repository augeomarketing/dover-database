USE [404CapitalCommercial]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 04/09/2014 15:14:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatementFile]
GO

USE [404CapitalCommercial]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 04/09/2014 15:14:13 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spQuarterlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS 

/*******************************************************************************/
--	Changes 1) 3/18/2011 add vesdia columns and codes to statement
--
/*******************************************************************************/
/*******************************************************************************************/
--Changes: 1) Added where clause to Insert to include only Active Customers  SEB001
/*******************************************************************************************/

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, city, state, zipcode,status,rewardsonline)
select tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, rtrim(city) , rtrim(state) , left(zipcode,5), status,rewardsonline
from customer
where Status='A' -- SEB001

/* Load the statmement file with purchases          */
update Quarterly_Statement_File
set pointspurchased=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%'))

-- CHANGE 1
update Quarterly_Statement_File
set PointsBonusMN=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and TRANCODE in ('F0','F9','G0','G9'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and TRANCODE in ('F0','F9','G0','G9'))

/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')

--	CHANGE 1
/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased=pointspurchased + pointsbonus + pointsadded + PointsBonusMN

/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with returns            */
update Quarterly_Statement_File
set pointsreturned=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with decrease redemptions          */
update Quarterly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

/* Load the statmement file with decrease redemptions          */
update Quarterly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DZ')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DZ')

/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in('DE','XP'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in('DE','XP'))

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
/*update Quarterly_Statement_File
set pointsbegin=(select BeginningPoints from QTRBeginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber) */
set @SQLUpdate=N'update Quarterly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

-- CHANGE(1) add code to update expiring points for customers 
EXEC RewardsNOW.dbo.usp_ExpirePoints '404', 1 

UPDATE	s
SET		ExpPnts = e.points_expiring 
FROM		[404CapitalCommercial].dbo.Quarterly_Statement_File s
	INNER JOIN
		RewardsNow.dbo.RNIExpirationProjection e 
	ON
		s.Tipnumber = e.tipnumber
		
update Quarterly_Statement_File set exppnts = '0' where exppnts is null or ExpPnts < 0




GO


