USE [404CapitalCommercial]
GO

/****** Object:  StoredProcedure [dbo].[sp404StageMonthlyStatement]    Script Date: 03/18/2011 10:09:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp404StageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp404StageMonthlyStatement]
GO

USE [404CapitalCommercial]
GO

/****** Object:  StoredProcedure [dbo].[sp404StageMonthlyStatement]    Script Date: 03/18/2011 10:09:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/*******************************************************************************************/
--Changes: 1) redirected expired points to expiringpointsprojection table in RewardsNOW
--         2) add VESDIA coding 3/18/2011
/*******************************************************************************************/
-- SEB 05/02/2014  use new expired points projection

CREATE PROCEDURE [dbo].[sp404StageMonthlyStatement]  @StartDateParm char(10), @EndDateParm char(10)

AS 

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
declare @tipfirst varchar(3)

set @tipfirst='404'
print 'start date parm'
print @StartDateparm

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin= Month(@StartDate)
set @MonthBucket='MonthBeg' + @monthbegin

/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, city, state, zipcode,status)
select tipnumber, acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address3, rtrim(city) , rtrim(state) , left(zipcode,5), status
from customer_stage

update Monthly_Statement_File
set
PointsBegin = '0',Pointsend = '0',PointsPurchased = '0',PointsBonus = '0',PointsBonusMN = '0',PointsAdded = '0',
PointsIncreased = '0',PointsRedeemed = '0',PointsReturned = '0',PointsSubtracted = '0',
PointsDecreased = '0'

/* Load the statmement file with purchases          */
update Monthly_Statement_File
set pointspurchased=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')

/* Load the statmement file with bonuses            */
update Monthly_Statement_File
set pointsbonus=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%' or trancode like'M%'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode like'M%'))

/* CHANGE 1 */
update Monthly_Statement_File
set pointsbonusMN=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in('F0','F9','G0','G9'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in('F0','F9','G0','G9'))

/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode='IE' or trancode = 'XF'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode='IE' or trancode = 'XF'))

/* CHANGE 1 */
/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased=pointspurchased + pointsbonus + pointsadded + PointsBonusMN

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with decrease redemptions          */
update Monthly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

update Monthly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DZ')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DZ')

/* Load the statmement file with returns            */
update Monthly_Statement_File
set pointsreturned=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode='XP' or trancode='DE'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and  (trancode='XP' or trancode='DE'))

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturned + pointssubtracted 

--SEB 05/02/2014 calculate beginning balance instead of gettting from table.
update Monthly_Statement_File 
set pointsbegin = isnull((select SUM(points * ratio)
					from history 
					where Tipnumber = Monthly_Statement_File.TIPNUMBER
					and histdate < @Startdate),0)


/* Load the statmement file with ending points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

-- SEB 05/02/2014
Exec RewardsNOW.dbo.usp_ExpirePoints @TipFirst, 1 

-- SEB 05/02/2014
update monthly_statement_file set exppnts = ep.points_expiring
from monthly_statement_file m join RewardsNOW.dbo.RNIExpirationProjection ep on m.tipnumber = ep.tipnumber

update monthly_statement_file set exppnts = '0' where exppnts is null or ExpPnts < 0



GO


