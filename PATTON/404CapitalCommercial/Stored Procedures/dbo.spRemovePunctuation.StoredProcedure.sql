USE [404CapitalCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spRemovePunctuation]    Script Date: 10/13/2009 10:41:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRemovePunctuation] AS

update customerwork
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), addr1=replace(addr1,char(39), ' '),
addr4=replace(addr4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update customerwork
set acctname1=replace(acctname1,char(96), ' '),acctname2=replace(acctname2,char(96), ' '), addr1=replace(addr1,char(96), ' '),
addr4=replace(addr4,char(96), ' '),   city=replace(city,char(96), ' '),  lastname=replace(lastname,char(96), ' ')
 

update customerwork
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), addr1=replace(addr1,char(44), ' '),
addr4=replace(addr4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update customerwork
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), addr1=replace(addr1,char(46), ' '),
addr4=replace(addr4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')
GO
