USE [404CapitalCommercial]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/04/2010 13:59:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

USE [404CapitalCommercial]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/04/2010 13:59:16 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


/*****************************************************************************************************/
/* Date:  4/1/07                                                                                     */
/* Author:  Rich T                                                                                   */
/*  Description: Copies data from input_transaction to the Affiliat_stage table                      */
/*                                                                                                   */
/*  Revisions: 1) add UPDATE to the affiliat_stage table to allow the updating of the last4 of a new */
/*                 cardnumber.                                                                       */
/*****************************************************************************************************/

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.acctid, c.TipNumber, 'Credit', @monthend,  c.last6,  'A', 'Credit Card', c.LastName, 0, c.custid
	from roll_Customer c where c.acctid not in ( Select acctid from Affiliat_Stage)

-- Revisions: 1)
Update AFS Set secid = last6 
  from affiliat_stage afs Join Roll_customer rc on afs.acctid = rc.Acctid
  where (afs.acctid = rc.Acctid) and (SECID != last6)

/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

GO

