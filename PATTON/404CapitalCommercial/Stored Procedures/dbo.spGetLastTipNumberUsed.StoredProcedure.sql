USE [404CapitalCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spGetLastTipNumberUsed]    Script Date: 10/13/2009 10:41:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetLastTipNumberUsed] @tipfirst char(3), @LastTipUsed nchar(15) output
AS 

declare @DBName varchar(50), @SQLSelect nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNOW.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLSelect=N'SELECT @LastTipUsed = LastTipNumberUsed 
		from ' + QuoteName(@DBName) + N'.dbo.client '
Exec sp_executesql @SQLSelect, N'@LastTipUsed nchar(15) output', @LastTipUsed=@LastTipUsed output
GO
