USE [557ParkNationalConsumer]
GO
/****** Object:  Table [dbo].[chkcombine02]    Script Date: 09/25/2009 11:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chkcombine02](
	[acctname1] [varchar](40) NOT NULL,
	[oldtipnumber] [varchar](15) NOT NULL,
	[newtipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
