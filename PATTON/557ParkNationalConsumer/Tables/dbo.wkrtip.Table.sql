USE [557ParkNationalConsumer]
GO
/****** Object:  Table [dbo].[wkrtip]    Script Date: 09/25/2009 11:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wkrtip](
	[tipnumber] [varchar](15) NOT NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
