USE [557ParkNationalConsumer]
GO
/****** Object:  Table [dbo].[fixcards]    Script Date: 09/25/2009 11:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fixcards](
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NOT NULL,
	[DDA] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
