USE [557ParkNationalConsumer]
GO
/****** Object:  Table [dbo].[chkdups2]    Script Date: 09/25/2009 11:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[chkdups2](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[Card] [varchar](25) NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[SSN] [varchar](10) NULL,
	[DDA] [char](13) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
