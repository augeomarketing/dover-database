USE [658]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 07/08/2015 14:54:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_MonthlyStatementFileLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]
GO

USE [658]
GO

/****** Object:  StoredProcedure [dbo].[usp_MonthlyStatementFileLoad]    Script Date: 07/08/2015 14:54:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_MonthlyStatementFileLoad]  
        @tipfirst               varchar(3),
        @StartDateParm          datetime, 
        @EndDateParm            datetime,
        @debug                  int = 0
AS 
-- Changed to load PointsBegin using calc instead of lookup in Beginning Balance Table.

-- SEB 11/2013 Added codes H0 H9


Declare     @MonthBegin         char(2)
Declare     @StartDate          DateTime
Declare     @EndDate            DateTime

declare     @dbname             nvarchar(50)
declare     @sql                nvarchar(max)

declare		@Trancode			varchar(2)
declare		@Ratio				int
declare		@tcdesc				varchar(40)
declare @nbrrows int = 0
declare @ctr    int = 1

set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 


set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

set @MonthBegin = month(@StartDate)

if @debug = 1
BEGIN
    print @dbname
    print @Startdate 
    print @Enddate
    print @monthbegin
END


/* Load the statement file from the customer table  */

set @sql = 'delete from ' + @dbname + '.dbo.Monthly_Statement_File'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql

set @sql = 'insert into ' + @dbname + '.dbo.Monthly_Statement_File
            (tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
            select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode)
            from ' + @dbname + '.dbo.customer_Stage'
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
    
set @sql = 'update ' + @dbname + '.dbo.Monthly_Statement_File
			set Acctnum = af.acctid
			from ' + @dbname + '.dbo.Monthly_Statement_File msf inner join ' + @dbname + '.dbo.affiliat_Stage af
					on msf.tipnumber = af.tipnumber
			where af.accttype=''MEMBER'' '    
if @debug = 1
    print @sql
else
    exec sp_executesql @sql
 
/* Load the statmement file with CREDIT purchases          */
set @sql = 'update ' + @dbname + '.dbo.Monthly_Statement_File 
	            set	pointspurchasedCR = isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''63''), 0),
		            pointsreturnedCR =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''33''), 0),
		            pointspurchasedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''67''), 0),
		            pointsreturnedDB =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''37''), 0),

		            pointsadded =		isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''IE''), 0) +
						            isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''DR''), 0) +
						            isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''GB''), 0) +						            
						            isnull(dbo.fnTotalPointsFromHistoryFortranCode(TipNumber, @StartDate, @EndDate, ''TP''), 0),

		            pointssubtracted =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''DE''), 0) +
						            isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''EP''), 0) +
						            isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''XP''), 0),

		           PurchasedPoints =	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''PP''), 0),
					
				   PointsBonusMER = 	isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''F0''), 0) +
										isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''G0''), 0) +
										isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''H0''), 0) -
										isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''F9''), 0) -
										isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''G9''), 0) -
										isnull(dbo.fnTotalPointsFromHistoryForTranCode(TipNumber, @StartDate, @EndDate, ''H9''), 0)   '
    exec sp_executesql @sql, N'@startdate datetime, @enddate datetime', @startdate = @startdate, @EndDate = @EndDate
/* Load the statmement file with bonuses            */
--
update Monthly_Statement_File
	set pointsbonusDB=(select sum(points*ratio) 
				    from dbo.History_Stage 
				    where tipnumber=Monthly_Statement_File.tipnumber and 
						  histdate>=@startdate and histdate<=@enddate and ((trancode like 'B%' or trancode like 'F%') and trancode not in ('F0', 'G0', 'H0', 'F9', 'G9', 'H9')) )
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and ((trancode like 'B%' or trancode like 'F%') and trancode not in ('F0', 'G0', 'H0', 'F9', 'G9', 'H9')))



/* Load the statmement file with total point increases */
update dbo.Monthly_Statement_File
	set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonusDB + pointsadded + pointsbonusMER + PurchasedPoints

/* Load the statmement file with redemptions          */
update dbo.Monthly_Statement_File
	set pointsredeemed=(select sum(points*ratio*-1) from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from dbo.History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with total point decreases */
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
update dbo.Monthly_Statement_File
	set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 


/* Load the statmement file with the Beginning balance for the Month */
update dbo.Monthly_Statement_File 
set pointsbegin = (select isnull(SUM(points * ratio),0)
					from history 
					where Tipnumber = dbo.Monthly_Statement_File.TIPNUMBER
					and histdate < @Startdate)


/* Load the statmement file with beginning points */
update dbo.Monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased


GO


