SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spHandleOptIns] AS

/* delete OptOutControl records for cardnumbers in the OptIns table */
-- delete optoutcontrol where acctnumber in (select Acctnumber from OptIns)
delete ooc
from COOPWORK.dbo.optoutcontrol ooc join dbo.optins oi
	on ooc.acctnumber = oi.acctnumber
GO
