SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zOptIn](
	[Tipnumber] [varchar](15) NULL,
	[AcctID] [varchar](16) NULL,
	[Acctname1] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
