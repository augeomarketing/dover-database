SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkTipsToPurge](
	[tipnumber] [varchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[acctnum] [varchar](25) NULL,
	[points] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
