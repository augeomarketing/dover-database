USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[zzzDelete_March_Relationship]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[zzzDelete_March_Relationship]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzzDelete_March_Relationship](
	[acn] [varchar](8000) NULL,
	[rtype] [varchar](8000) NULL,
	[ramt] [varchar](8000) NULL,
	[joindate] [varchar](8000) NULL,
	[dateawarded] [varchar](8000) NULL
) ON [PRIMARY]
GO
