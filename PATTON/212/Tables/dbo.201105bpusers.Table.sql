USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[201105bpusers]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[201105bpusers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[201105bpusers](
	[UserID] [varchar](50) NULL,
	[UserName] [varchar](50) NULL,
	[BPUserID] [varchar](50) NULL,
	[DateStarted] [datetime] NULL,
	[NumberofBillsPaid] [int] NULL,
	[AccountNumber] [varchar](50) NULL,
	[Report Date] [datetime] NULL,
	[Column 7] [varchar](50) NULL
) ON [PRIMARY]
GO
