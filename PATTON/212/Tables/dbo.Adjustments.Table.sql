USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[Adjustments]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[Adjustments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adjustments](
	[Tipnumber] [nchar](15) NULL,
	[Account] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
GO
