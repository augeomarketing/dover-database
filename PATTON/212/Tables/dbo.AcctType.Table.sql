USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[AcctType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
