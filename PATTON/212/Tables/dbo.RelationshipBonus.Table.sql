USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[RelationshipBonus]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[RelationshipBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelationshipBonus](
	[Acn] [varchar](255) NULL,
	[RType] [varchar](255) NULL,
	[RAmt] [varchar](255) NULL,
	[JoinDate] [varchar](255) NULL,
	[DateAwarded] [varchar](255) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
