USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 06/30/2011 10:33:36 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__151B244E]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__160F4887]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__17036CC0]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__17F790F9]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__18EBB532]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__19DFD96B]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1AD3FDA4]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1BC821DD]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1CBC4616]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1DB06A4F]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1EA48E88]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__1F98B2C1]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__208CD6FA]
GO
DROP TABLE [dbo].[Quarterly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[DirectDep] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](15) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Quarterly_Statement_File] ON [dbo].[Quarterly_Statement_File] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__151B244E]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__160F4887]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__17036CC0]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__17F790F9]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__18EBB532]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__19DFD96B]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1AD3FDA4]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1BC821DD]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1CBC4616]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1DB06A4F]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1EA48E88]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1F98B2C1]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__208CD6FA]  DEFAULT (0) FOR [PointsDecreased]
GO
