USE [212IDB_IIC_FCU]
GO

/****** Object:  Table [dbo].[DemographicIn]    Script Date: 12/23/2011 12:36:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DemographicIn]') AND type in (N'U'))
DROP TABLE [dbo].[DemographicIn]
GO

USE [212IDB_IIC_FCU]
GO

/****** Object:  Table [dbo].[DemographicIn]    Script Date: 12/23/2011 12:36:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DemographicIn](
	[Pan] [varchar](16) NOT NULL,
	[Inst ID] [varchar](10) NULL,
	[CS] [varchar](5) NULL,
	[Prim DDA] [varchar](16) NULL,
	[1st DDA] [varchar](16) NULL,
	[2nd DDA] [varchar](16) NULL,
	[3rd DDA] [varchar](16) NULL,
	[4th DDA] [varchar](16) NULL,
	[5th DDA] [varchar](16) NULL,
	[Address #1] [varchar](40) NULL,
	[Address #2] [varchar](40) NULL,
	[City ] [varchar](40) NULL,
	[ST] [varchar](2) NULL,
	[Zip] [varchar](9) NULL,
	[First] [varchar](40) NULL,
	[Last] [varchar](40) NULL,
	[MI] [varchar](1) NULL,
	[First2] [varchar](40) NULL,
	[Last2] [varchar](40) NULL,
	[MI2] [varchar](1) NULL,
	[First3] [varchar](40) NULL,
	[Last3] [varchar](40) NULL,
	[MI3] [varchar](1) NULL,
	[First4] [varchar](40) NULL,
	[Last4] [varchar](40) NULL,
	[MI4] [varchar](1) NULL,
	[SSN] [varchar](16) NULL,
	[Home Phone] [varchar](10) NULL,
	[Work Phone] [varchar](10) NULL,
	[TipFirst] [varchar](3) NULL,
	[TipNumber] [varchar](15) NULL,
	[NA1] [varchar](40) NULL,
	[NA2] [varchar](40) NULL,
	[NA3] [varchar](40) NULL,
	[NA4] [varchar](40) NULL,
	[NA5] [varchar](40) NULL,
	[NA6] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
PRIMARY KEY CLUSTERED 
(
	[Pan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

