USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[wrkJune08Reversals]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[wrkJune08Reversals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkJune08Reversals](
	[acn] [varchar](10) NULL,
	[rtype] [varchar](2) NULL,
	[ramt] [varchar](5) NULL,
	[joindate] [varchar](10) NULL,
	[dateawarded] [varchar](20) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
