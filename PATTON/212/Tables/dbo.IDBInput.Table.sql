USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[IDBInput]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[IDBInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IDBInput](
	[ACN] [varchar](255) NULL,
	[Name] [varchar](255) NULL,
	[CardNum] [varchar](255) NULL,
	[CardHolder] [varchar](255) NULL,
	[Add1] [varchar](255) NULL,
	[Add2] [varchar](255) NULL,
	[City] [varchar](255) NULL,
	[St] [varchar](255) NULL,
	[Zip] [varchar](255) NULL,
	[Country] [varchar](255) NULL,
	[ForFlg] [varchar](255) NULL,
	[Activated] [varchar](255) NULL
) ON [PRIMARY]
GO
