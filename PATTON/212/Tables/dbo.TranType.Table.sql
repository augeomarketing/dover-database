USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[TranType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
