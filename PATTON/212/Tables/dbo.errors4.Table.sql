USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[errors4]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[errors4]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[errors4](
	[ACN] [char](7) NULL,
	[Name] [char](40) NULL,
	[CardNum] [char](16) NULL,
	[CardHolder] [char](40) NULL,
	[Add1] [char](40) NULL,
	[Add2] [char](40) NULL,
	[City] [char](40) NULL,
	[St] [char](2) NULL,
	[Zip] [char](5) NULL,
	[Country] [char](30) NULL,
	[ForFlg] [char](1) NULL,
	[Activated] [char](1) NULL,
	[ErrorMessage] [char](40) NULL
) ON [PRIMARY]
GO
