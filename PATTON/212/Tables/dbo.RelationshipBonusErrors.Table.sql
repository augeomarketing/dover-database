USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[RelationshipBonusErrors]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[RelationshipBonusErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RelationshipBonusErrors](
	[acn] [varchar](255) NULL,
	[rtype] [varchar](255) NULL,
	[ramt] [varchar](255) NULL,
	[joindate] [varchar](255) NULL
) ON [PRIMARY]
GO
