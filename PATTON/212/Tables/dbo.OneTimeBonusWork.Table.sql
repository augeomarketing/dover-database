USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[OneTimeBonusWork]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[OneTimeBonusWork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OneTimeBonusWork](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
GO
