USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[zTmp_IDB_Aggregate_01Nov_24Mar]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[zTmp_IDB_Aggregate_01Nov_24Mar]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zTmp_IDB_Aggregate_01Nov_24Mar](
	[acn] [varchar](7) NULL,
	[Aggregate_2] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
