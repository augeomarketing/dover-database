USE [212IDB_IIC_FCU]
GO
/****** Object:  Table [dbo].[CountryCodes]    Script Date: 06/30/2011 10:33:36 ******/
DROP TABLE [dbo].[CountryCodes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryCodes](
	[country_cd] [nvarchar](255) NULL,
	[country] [nvarchar](255) NULL
) ON [PRIMARY]
GO
