USE [212IDB_IIC_FCU]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 06/30/2011 10:33:36 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [212IDB_IIC_FCU].dbo.customer
GO
