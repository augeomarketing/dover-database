USE [212IDB_IIC_FCU]
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 06/30/2011 10:33:36 ******/
DROP VIEW [dbo].[vwCustomerdeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [212IDB_IIC_FCU].dbo.Customerdeleted
GO
