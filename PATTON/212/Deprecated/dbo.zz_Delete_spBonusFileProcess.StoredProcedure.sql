SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zz_Delete_spBonusFileProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, histdate, trancode, points)
select tipnumber, rtrim(acn), @datein, rtrim(rtype), rtrim(ramt)	
from RelationshipBonus

update transumbonus
set ratio='1', numdebit='1', overage='0', amtdebit=points

update transumbonus
set trancode='FM'
where trancode in ('M1', 'M2', 'M3')

update transumbonus
set trancode='FI'
where trancode in ('MM')

update transumbonus
set trancode='FC'
where trancode in ('BP')

update transumbonus
set trancode='FB'
where trancode in ('IB')

update transumbonus
set trancode='FN'
where trancode in ('AL')

update transumbonus
set description=b.description
from transumbonus a, trantype b
where a.trancode=b.trancode

insert into history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus
where amtdebit is not null 

update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null and trancode='FM'

update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null and trancode='FI'

update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null and trancode='FC'

update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null and trancode='FB'

update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null and trancode='FN'
GO
