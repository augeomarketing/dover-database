SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelationshipBonus](
	[Acn] [varchar](255) NULL,
	[RType] [varchar](255) NULL,
	[RAmt] [varchar](255) NULL,
	[JoinDate] [varchar](255) NULL,
	[DateAwarded] [varchar](255) NULL,
	[Tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
