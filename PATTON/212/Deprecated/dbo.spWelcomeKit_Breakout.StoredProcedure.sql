SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spWelcomeKit_Breakout] 
AS
	truncate table WelcomeKit_INTAMBANC

	Insert into WelcomeKit_INTAMBANC
		select * from WelcomeKitCNTY where left(Address1,5) ='INTAM' OR  left(Address1,5) ='STOP '

	Delete WelcomeKitCNTY  where left(Address1,5) ='INTAM'  OR left(Address1,5) ='STOP '

	truncate table WelcomeKit_Foreign
	Insert into WelcomeKit_Foreign
		select * from WelcomeKitCNTY where len(rtrim(ltrim(Country))) >0

	Delete WelcomeKitCNTY  where len(rtrim(ltrim(Country))) >0
GO
