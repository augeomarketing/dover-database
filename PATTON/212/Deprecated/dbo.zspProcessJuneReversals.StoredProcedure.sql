SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zspProcessJuneReversals] @EndDate nvarchar(10)
AS

truncate table zwrkTransStandard_Reversals

INSERT INTO zwrkTransStandard_Reversals (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, NULL, 'BX', 1, ramt , 'Bonus Reversal', '-1', ' ' from wrkJune08Reversals
GO
