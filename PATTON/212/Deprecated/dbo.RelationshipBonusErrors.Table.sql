SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelationshipBonusErrors](
	[acn] [varchar](255) NULL,
	[rtype] [varchar](255) NULL,
	[ramt] [varchar](255) NULL,
	[joindate] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
