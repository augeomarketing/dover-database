SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzzDelete_March_Relationship](
	[acn] [varchar](8000) NULL,
	[rtype] [varchar](8000) NULL,
	[ramt] [varchar](8000) NULL,
	[joindate] [varchar](8000) NULL,
	[dateawarded] [varchar](8000) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
