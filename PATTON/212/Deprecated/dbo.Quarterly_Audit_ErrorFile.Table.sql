SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quarterly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__22751F6C]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__236943A5]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__245D67DE]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__25518C17]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2645B050]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2739D489]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__282DF8C2]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__29221CFB]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2A164134]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2B0A656D]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2BFE89A6]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2CF2ADDF]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Point__2DE6D218]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Quarterly_Audit_ErrorFile] ADD  CONSTRAINT [DF__Quarterly__Curre__6A30C649]  DEFAULT (0) FOR [Currentend]
GO
