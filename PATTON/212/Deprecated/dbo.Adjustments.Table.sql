SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adjustments](
	[Tipnumber] [nchar](15) NULL,
	[Account] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
GO
