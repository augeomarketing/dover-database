SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zdelete_212MarchOutlyers](
	[tdhID] [bigint] NOT NULL,
	[TRANDATE] [char](10) NULL,
	[MSGTYPE] [char](4) NULL,
	[PAN] [nvarchar](19) NOT NULL,
	[PROCESSINGCODE] [char](6) NOT NULL,
	[AMOUNTTRAN] [decimal](12, 0) NOT NULL,
	[SIC] [nchar](10) NULL,
	[NETID] [nchar](10) NULL,
	[POINTS] [decimal](10, 0) NULL,
	[TIPNUMBER] [char](15) NULL,
	[NUMBEROFTRANS] [decimal](18, 0) NULL,
	[TERMID] [nchar](8) NULL,
	[ACCEPTORID] [nchar](15) NULL,
	[Expr1] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
