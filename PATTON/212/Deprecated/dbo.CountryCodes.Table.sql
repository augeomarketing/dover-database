SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryCodes](
	[country_cd] [nvarchar](255) NULL,
	[country] [nvarchar](255) NULL
) ON [PRIMARY]
GO
