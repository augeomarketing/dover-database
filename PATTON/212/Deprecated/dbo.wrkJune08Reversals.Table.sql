SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkJune08Reversals](
	[acn] [varchar](10) NULL,
	[rtype] [varchar](2) NULL,
	[ramt] [varchar](5) NULL,
	[joindate] [varchar](10) NULL,
	[dateawarded] [varchar](20) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
