SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE Procedure [dbo].[spHandleOptOuts]
as

/* Remove Demographicin records for Opt Out people  */
delete from IDBRewardsInput
where exists(select * from optoutcontrol where left(acctnumber,16)=right(rtrim(IDBRewardsInput.cardnum),16))


/* Remove transaction records for Opt Out people  */
delete from coopwork.dbo.transdetailhold
where exists(select * from optoutcontrol where left(acctnumber,16)=coopwork.dbo.transdetailhold.pan)
GO
