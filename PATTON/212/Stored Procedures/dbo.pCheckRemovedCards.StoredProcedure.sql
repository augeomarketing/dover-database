USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[pCheckRemovedCards]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[pCheckRemovedCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pCheckRemovedCards] 
AS 
/************************************************************************************/
/*                                                                                  */
/*  Acctid in Affiliat table is actually the last six of cardno                     */
/*  Secid in Affiliat table is actually the Acctno (DDA#) for Spirit Bank           */
/*  Custid in Affiliat table is actually the CIS# for Spirit Bank                   */
/*                                                                                  */
/************************************************************************************/

Update affiliat 
set acctstatus = 'C' where not exists(select * from custin where acct_num= affiliat.acctid)
GO
