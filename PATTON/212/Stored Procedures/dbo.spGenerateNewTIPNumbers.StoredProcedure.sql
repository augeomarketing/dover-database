USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateNewTIPNumbers]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[spGenerateNewTIPNumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 0 */
/* SCAN:  */
CREATE PROCEDURE [dbo].[spGenerateNewTIPNumbers] @tipfirst char(3)
AS 

declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000), @SQLSet nvarchar(1000), @newnum bigint, @NewTip char(15)


set @DBName=(SELECT  rtrim(DBNamePatton) from RewardsNow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

declare @LastTipUsed char(15)

exec rewardsnow.dbo.spGetLastTipNumberUsed @tipfirst, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @Newnum = cast(@LastTipUsed as bigint) + 1  

drop table wrktab2 

select distinct ACN, tipnumber
into wrktab2
from IDBRewardsInput
where tipnumber is null 
 
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING CardsIn TABLE                               */
/*                                                                            */
declare tip_crsr cursor
for select tipnumber
from wrktab2
for update

/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
		set @NewTip = Cast(@Newnum as char)
		set @NewTip = replicate('0',15 - Len( RTrim( @NewTip ) ) ) +  @NewTip   /* Added 11/29/2006 */
		
		update wrktab2	
		set tipnumber = @Newtip 
		where current of tip_crsr
		set @newnum = @newnum + 1		
		goto Next_Record
Next_Record:
		fetch tip_crsr
	end

Fetch_Error:
close  tip_crsr
deallocate  tip_crsr

update IDBRewardsInput
set tipnumber=(select tipnumber from wrktab2 where ACN=IDBRewardsInput.ACN )
where ACN is not null and tipnumber is null 

exec RewardsNOW.dbo.spPutLastTipNumberUsed @tipfirst, @NewTip
GO
