USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImport]    Script Date: 08/05/2013 11:46:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetupTransactionDataForImport]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[pSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 7/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* Changed logic to group by tip and pan */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */

-- S Blanchete change to use RNITransaction view
-- S Blanchette corrections to the process codes
truncate table transwork
truncate table transstandard

select *
into #tempTrans
from Rewardsnow.dbo.vw_212_TRAN_SOURCE_1 with (nolock)
where Trandate>=cast(@StartDate as DATE) and Trandate<=cast(@EndDate as DATE) /* SEB005 */

insert into transwork (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID)
select TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID
FROM #tempTrans 
where sic<>''6011'' and processingcode in (''000000'', ''002000'', ''200020'', ''200040'', ''500000'', ''500020'', ''500010'') and (left(msgtype,2) in (''02'', ''04'')) /* SEB002 */

drop table #temptrans

update TRANSWORK
set TIPNUMBER = afs.TIPNUMBER
from TRANSWORK tw join AFFILIAT afs on tw.PAN = afs.ACCTID

delete from TRANSWORK
where TIPNUMBER is null

-- Signature Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid in(''MCI'', ''VNT'') 

-- PIN Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid not in(''MCI'', ''VNT'') 

--Put to standard transtaction file format purchases.
-- SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, ''67'', NumberOfTrans, points, ''DEBIT'', ''1'', '' '' from transwork
--where processingcode in (''000000'', ''002000'', ''500000'', ''500020'', ''500010'') 		        	

-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, ''67'', sum(NumberOfTrans), sum(points), ''DEBIT'', ''1'', '' '' from transwork
/* SEB002 where processingcode in (''000000'', ''002000'', ''500000'', ''500020'', ''500010'') and left(msgtype,2) in (''02'', ''04'')) */		        	
/* SEB002 */ where processingcode in (''000000'', ''002000'', ''500000'', ''500020'', ''500010'') and left(msgtype,2) in (''02'') 		        	
group by tipnumber, Pan
	
--Put to standard transtaction file format returns.
-- SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, ''37'', NumberOfTrans, points, ''DEBIT'', ''-1'', '' '' from transwork
--where processingcode in (''200020'', ''200040'')

-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, ''37'', sum(NumberOfTrans), sum(points), ''DEBIT'', ''-1'', '' '' from transwork
/* SEB002 where processingcode in (''200020'', ''200040'') */
/* SEB002 */ where processingcode in (''200020'', ''200040'') or left(msgtype,2) in (''04'')
group by tipnumber, Pan
' 
END
GO
