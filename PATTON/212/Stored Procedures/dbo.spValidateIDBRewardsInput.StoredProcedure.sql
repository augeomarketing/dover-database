USE [212IDB_IIC_FCU]
GO

/****** Object:  StoredProcedure [dbo].[spValidateIDBRewardsInput]    Script Date: 12/23/2011 12:22:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spValidateIDBRewardsInput]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spValidateIDBRewardsInput]
GO

USE [212IDB_IIC_FCU]
GO

/****** Object:  StoredProcedure [dbo].[spValidateIDBRewardsInput]    Script Date: 12/23/2011 12:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[spValidateIDBRewardsInput]
AS

truncate table IDBRewardsInput

--update IDBInput
--set country=b.country
--from IDBInput a, CountryCodes b
--where (a.country is null or len(a.country)='0') and forflg='1.00' and st=b.country_cd
 
--insert into IDBRewardsInput (ACN, name, cardnum, cardholder, Add1, Add2, city, st, zip, country, forflg, activated)
--select left(ACN,7), left(name, 40), left(ltrim(cardnum),16), left(cardholder,40), Left(Add1,40), left(Add2, 40), left(city,40), left(st,2), left(zip,5), left(country,30), left(forflg,1), left(activated,1)
--from IDBInput
--where activated='Y'


--
-- 25 Oct 2011 PHB
-- Demographics coming through from coopwork.dbo.demographicin
--

insert into dbo.IDBRewardsInput
(ACN, Name, CardNum, CardHolder, Add1, Add2, City, St, Zip, Country, ForFlg, Activated, Tipnumber, Lastname)
select [Prim DDA], ltrim(rtrim([First])) + isnull(' ' + ltrim(rtrim([MI])), '') + ' ' + ltrim(rtrim([Last])) as [Name], PAN, ltrim(rtrim([First])) + isnull(' ' + ltrim(rtrim([MI])), '') + ' ' + ltrim(rtrim([Last])) as CardHolder,
        [Address #1], [Address #2], city, st, left(zip, 5) zip, '' as Country, 0 as forFlg, '' Activated, null, [last]
from dbo.demographicin




/* Update the Address1 and City values for INTAMBANC and STOP records so they don't error out*/
update IDBRewardsInput set Add1=City where rtrim(ltrim(City))='INTAMBANC'
update IDBRewardsInput set CITY=Add1 where rtrim(ltrim(Add1))='INTAMBANC' and CITY is null

update IDBRewardsInput set Add1=City where left(rtrim(ltrim(City)),5)='STOP '
update IDBRewardsInput set CITY=Add1 where left(rtrim(ltrim(Add1)),5)='STOP ' And City is null

update IDBRewardsInput set St='ST'  where left(rtrim(ltrim(Add1)),5)='STOP '


--
-- Set foreign flag =1 where states = "**" and add1 not like INTAMBANC
update dbo.IDBRewardsInput
    set forflg = 1
where st = '**' and add1 not like '%intambanc%'


truncate table errors

/*  ACN ERROR  */
insert into errors
select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'ACN ERROR' 
from IDBRewardsInput
where (ACN is null or left(ACN,2)='  ' or len(ACN)='0')

/*  NAME ERROR  */
insert into errors
select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'NAME ERROR' 
from IDBRewardsInput
where (Name is null or left(Name,2)='  ' or len(Name)='0')

/*  CARD# ERROR  */
insert into errors
select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'CARD# ERROR' 
from IDBRewardsInput
where (CardNum is null or left(CardNum,2)='  ' or len(CardNum)='0')

/*  ADDRESS 1 ERROR  */
insert into errors
select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'ADDRESS ERROR' 
from IDBRewardsInput
where (( Add1 is null and Add2 is null) or  (left(Add1,2)='  ' and left(Add2,2)='  ') or (len(Add1)='0' and len(Add2)='0'))    

/*  CITY ERROR  */
insert into errors
select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'CITY ERROR' 
from IDBRewardsInput
where( City is null or left(City,2)='  ' or len(City)='0') 

/*  STATE ERROR  */
insert into errors
select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'STATE ERROR' 
from IDBRewardsInput
where ((ST is null or left(ST,2)='  ' or len(ST)='0') and forflg='0')

/*  COUNTRY ERROR  */
--insert into errors
--select ACN, Name, CardNum, cardholder, Add1, Add2, city, st, zip, Country, forflg, activated, 'COUNTRY ERROR' 
--from IDBRewardsInput
--where ((Country is null or left(Country,2)='  ' or len(Country)='0') and forflg='1')

/*  DELETE FROM INPUT FILE  */

delete from IDBRewardsInput
where (ACN is null or left(ACN,2)='  ' or len(ACN)='0') 
	or (Name is null or left(Name,2)='  ' or len(Name)='0') 
	or (CardNum is null or left(CardNum,2)='  ' or len(CardNum)='0')
	or (Add1 is null and Add2 is null) or  (left(Add1,2)='  ' and left(Add2,2)='  ') or (len(Add1)='0' and len(Add2)='0')
	or (City is null or left(City,2)='  ' or len(City)='0')
	or ((ST is null or left(ST,2)='  ' or len(ST)='0') and forflg='0')
	--or ((Country is null or left(Country,2)='  ' or len(Country)='0') and forflg='1')

GO

