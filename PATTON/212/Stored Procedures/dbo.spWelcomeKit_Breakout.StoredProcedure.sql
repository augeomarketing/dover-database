USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit_Breakout]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[spWelcomeKit_Breakout]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spWelcomeKit_Breakout] 
AS
	truncate table WelcomeKit_INTAMBANC

	Insert into WelcomeKit_INTAMBANC
		select * from WelcomeKitCNTY where left(Address1,5) ='INTAM' OR  left(Address1,5) ='STOP '

	Delete WelcomeKitCNTY  where left(Address1,5) ='INTAM'  OR left(Address1,5) ='STOP '

	truncate table WelcomeKit_Foreign
	Insert into WelcomeKit_Foreign
		select * from WelcomeKitCNTY where len(rtrim(ltrim(Country))) >0

	Delete WelcomeKitCNTY  where len(rtrim(ltrim(Country))) >0
GO
