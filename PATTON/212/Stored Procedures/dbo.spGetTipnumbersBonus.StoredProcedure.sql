USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[spGetTipnumbersBonus]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[spGetTipnumbersBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 6/2007   */
/* REVISION: 0 */
/* SCAN: SEB */
/* Script to get tipnumbers from account reference table and delete records with null tipnumber  */

CREATE PROCEDURE [dbo].[spGetTipnumbersBonus]
AS

Update RelationshipBonus
set tipnumber=b.tipnumber
from RelationshipBonus a, customer b
where a.tipnumber is null and a.acn=b.misc1

Update RelationshipBonus
set tipnumber=b.tipnumber
from RelationshipBonus a, affiliat b
where a.tipnumber is null and a.acn=b.acctid 

Update RelationshipBonus
set tipnumber=b.tipnumber
from RelationshipBonus a, affiliat b
where a.tipnumber is null and a.acn=b.custid 

truncate table RelationshipBonusErrors

insert into RelationshipBonusErrors (acn, rtype, ramt, joindate)
select acn, rtype, ramt, joindate
from RelationshipBonus a
where a.tipnumber is null

delete from RelationshipBonus
where tipnumber is null
GO
