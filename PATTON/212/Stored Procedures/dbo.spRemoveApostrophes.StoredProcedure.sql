USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveApostrophes]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[spRemoveApostrophes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRemoveApostrophes] @TipFirst char(3)
AS

/*  *****************************************************************************************  	*/
/* Date: 3/30/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Removes apostrophes from name and address fields                               */
/*                                  								*/

/*  Tables:
		DBProcessInfo
		Demographicin  
*/
/*  Revisions: 
*/
/*  *****************************************************************************************  	*/


declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.IDBRewardsInput set Add1=replace(Add1,char(39), '' ''), Add2=replace(Add2,char(39), '' ''), CITY=replace(City,char(39), '' ''), NAME=replace(NAME,char(39), '' '') '
		exec sp_executesql @SQLUpdate
GO
