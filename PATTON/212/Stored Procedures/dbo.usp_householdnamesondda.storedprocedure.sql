USE [212IDB_IIC_FCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_householdnamesondda]    Script Date: 12/23/2011 12:33:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_householdnamesondda]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_householdnamesondda]
GO

USE [212IDB_IIC_FCU]
GO

/****** Object:  StoredProcedure [dbo].[usp_householdnamesondda]    Script Date: 12/23/2011 12:33:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[usp_householdnamesondda]

AS


declare @sql		    nvarchar(max) = ''
--declare @columnnames    varchar(max) = ''

declare @ctr		    int = 1
declare @maxnamesonDDA  int

set nocount on

-- Get max # names against a DDA#
set @maxnamesonDDA =    (select top 1 count(*)
							from dbo.demographicin
							where len([prim dda]) > 0
							group by [prim dda]
							order by count(*) desc)

if @maxnamesonDDA is not null
BEGIN

	if object_id('tempdb..#di') is not null
		drop table #di


	if object_id('tempdb..#cus') is not null
		drop table #cus


	create table #di
		(dda			varchar(16),
		 acctname		varchar(40))
	
	insert into #di
	(dda, acctname)
	select distinct [prim dda], ltrim(rtrim([first])) + ' ' + ltrim(rtrim([last]))
	from dbo.demographicin
	where len([prim dda]) > 0
	order by [prim dda]
	
	create table #cus
		(dda			varchar(16),
		 acctname1		varchar(40),
 		 acctname2		varchar(40),
		 acctname3		varchar(40),
		 acctname4		varchar(40),
		 acctname5		varchar(40),
		 acctname6		varchar(40))


    -- Build string of column names for the pivot for clause
    -- string will look something like:
    -- '[name_1], [name_2],....'
    --
    --while @ctr <= @maxnamesonDDA
    --BEGIN
	   --set @columnnames = @columnnames + '[Name_' + cast(@ctr as varchar) + ']' + ', '
	   --set @ctr += 1
    --END

    ---- Now get rid of trailing ', ' from the end of the string
    --set @columnnames = left(@columnnames, datalength(@columnnames)-2)

    --print @columnnames

    -- Now do the select, pivoting the results
    set @sql = '
			 insert into #cus
			 (dda, acctname1, acctname2, acctname3, acctname4, acctname5)
			 select *
			 from (select dda, acctname, ''name_'' + cast(row_number()  over(partition by dda order by dda) as varchar) rownbr 
				  from dbo.#di
				  group by dda, acctname) di
			 pivot
			 ( max(acctname)
				for rownbr in ([Name_1], [Name_2], [Name_3], [Name_4], [Name_5] )
			 ) as pvttable'

    -- Now replace the '<columnnames>' with the @columnnames string built in the while loop
    --set @sql = replace(@sql, '<columnnames>', @columnnames)

    --print @sql

    exec sp_executesql @sql

END


update c
	set acctname1 = tmp.acctname1,
		acctname2 = tmp.acctname2,
		acctname3 = tmp.acctname3,
		acctname4 = tmp.acctname4,
		acctname5 = tmp.acctname5
from #cus tmp join dbo.customer c
	on tmp.dda = c.misc1
	
	
	
/*  test harness


exec dbo.usp_householdnamesondda


*/

GO

