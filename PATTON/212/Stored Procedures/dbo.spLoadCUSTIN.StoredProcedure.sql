USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCUSTIN]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[spLoadCUSTIN]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadCUSTIN] @dateadded char(10), @tipFirst char(3)
AS 
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO LOAD CUSTIN TABLE                                            */
/*                                                                            */
/******************************************************************************/


set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.custin'
Exec sp_executesql @SQLTruncate

-- Add to CUSTIN TABLE
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.custin(ACCT_NUM, NAMEACCT1, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, DateAdded, MISC1, MISC2 ) 
		select CardNum, NAME, ''A'', TIPNUMBER, Add1, Add2, (rtrim([City ]) + '' '' + rtrim(st) + '' '' + rtrim(zip)), [City ], ST, ZIP, LASTNAME, @DateAdded, ACN, left(Country, 20)
		from ' + QuoteName(@DBName) + N'.DBO.IDBRewardsInput
		 order by tipnumber'

Exec sp_executesql @SQLInsert, N'@DATEADDED nchar(10)',	@Dateadded= @Dateadded
GO
