USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[sp212RelationshipBonusFileProcess]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[sp212RelationshipBonusFileProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp212RelationshipBonusFileProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
Truncate table transumbonus

insert into transumbonus (tipnumber, acctno, histdate, trancode, points)
select tipnumber, rtrim(acn), @datein, rtrim(rtype), rtrim(ramt)	
from RelationshipBonus

update transumbonus
set ratio='1', numdebit='1', overage='0', amtdebit=points

update transumbonus
set trancode='FM'
where trancode in ('M1', 'M2', 'M3')

update transumbonus
set trancode='FI'
where trancode in ('MM')

update transumbonus
set trancode='FC'
where trancode in ('BP')

update transumbonus
set trancode='FB'
where trancode in ('IB')

update transumbonus
set trancode='FN'
where trancode in ('AL')

update transumbonus
set trancode='FP'
where trancode in ('PL')

update transumbonus
set description=b.description
from transumbonus a, trantype b
where a.trancode=b.trancode

insert into history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus
where amtdebit is not null 


update cus
	set	runavailable = runavailable + tsb.sum_amtdebit,
		runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
						from dbo.transumbonus 
						where trancode = 'FM'
						group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber
--------------------------------------



update cus
	set	runavailable = runavailable + tsb.sum_amtdebit,
		runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
						from dbo.transumbonus 
						where trancode = 'FI'
						group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber


update cus
	set	runavailable = runavailable + tsb.sum_amtdebit,
		runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
						from dbo.transumbonus 
						where trancode = 'FC'
						group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber


update cus
	set	runavailable = runavailable + tsb.sum_amtdebit,
		runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
						from dbo.transumbonus 
						where trancode = 'FB'
						group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber

update cus
	set	runavailable = runavailable + tsb.sum_amtdebit,
		runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
						from dbo.transumbonus 
						where trancode = 'FN'
						group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber

update cus
	set	runavailable = runavailable + tsb.sum_amtdebit,
		runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
						from dbo.transumbonus 
						where trancode = 'FP'
						group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber
GO
