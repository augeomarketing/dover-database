USE [212IDB_IIC_FCU]
GO
/****** Object:  StoredProcedure [dbo].[zzspBonusFileProcess]    Script Date: 06/30/2011 10:33:35 ******/
DROP PROCEDURE [dbo].[zzspBonusFileProcess]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzspBonusFileProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, account, points	
from [adjustments]
where tipnumber is not null

update transumbonus
set ratio='1', trancode='67', description='Debit card Purchases', histdate=@datein, numdebit='1', overage='0'

update transumbonus
set amtdebit=ROUND((amtdebit/2), 10, 0)

insert into history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus

update customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=customer.tipnumber)
GO
