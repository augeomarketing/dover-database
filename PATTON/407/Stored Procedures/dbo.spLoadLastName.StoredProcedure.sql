USE [407]
GO
/****** Object:  StoredProcedure [dbo].[spLoadLastName]    Script Date: 01/28/2013 10:16:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadLastName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadLastName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadLastName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Dan Foster>
-- Create date: <12.15.2009>
-- Description:	<Load Last Name to Input_Customer>
-- Changes: 1)  changed update loading lastname
-- =============================================
CREATE PROCEDURE [dbo].[spLoadLastName]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Insert newlastname (acctid,tipnumber, acctname1)
	select acctid,tipnumber, acctname1 from roll_customer
		
	update newlastname set reversedname = reverse(rtrim(ltrim(acctname1)))
		
	update newlastname set lastspace = CHARINDEX('' '', reversedname) - 1 
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where lastspace >= 0
	
	update newlastname set reversedname = ltrim(reverse(left(acctname1, len(acctname1) -3)))
	where lastname = ''JR'' or lastname = ''SR'' or lastname = ''II'' or lastname = ''IV''
	
	update newlastname set reversedname = ltrim(reverse(left(acctname1, len(acctname1) -4)))
	where lastname = ''III''
	 
	update newlastname set lastspace = CHARINDEX('' '', reversedname) - 1 
	where lastspace <= 3
	
	update newlastname set lastname = reverse(substring(reversedname,1,lastspace))
	where (lastname = ''JR'' or lastname = ''SR'' or lastname = ''II'' or lastname = ''III'' or lastname = ''IV'')
		   	and lastspace > 0
	
	update newlastname set lastname = acctname1 where lastname = ''INC'' or lastname = ''CO''
		or lastname = ''CTR'' or lastname = ''LTD'' or lastname = ''CT'' or lastname = ''CH''
		or lastname = ''INC'' or lastname = ''CO'' or lastname = ''LLC''
	
	update newlastname set lastname = reverse(reversedname) where lastspace <= 1
	
	/*  Load lastname into Input_Customer Table */
	/*  Change(1) changed table from input_customer to roll_customer  */
	
	update Roll_customer set lastname = n.lastname 
	from newlastname n join Roll_customer c on c.tipnumber = n.tipnumber

END


' 
END
GO
