USE [407]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 01/28/2013 10:16:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

/******************************************************************************/
-- Clear TransStandard
/******************************************************************************/ 
Truncate table TransStandard 

/******************************************************************************/
-- Load the TransStandard table with rows from Input_Transaction
/******************************************************************************/
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [TranType] ) 
select  [TipNumber], @dateadded, [cardnumber], [TranCode], [trancount], convert(char(15), [Points]), [description]  from Input_Transactions 


/******************************************************************************************************/
-- Set the TranType to the Description found in the RewardsNow.TranCode table
/******************************************************************************************************/
--Update TransStandard set TranType = R.[Description] from RewardsNow.dbo.Trantype R join TransStandard T 
--on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode' 
END
GO
