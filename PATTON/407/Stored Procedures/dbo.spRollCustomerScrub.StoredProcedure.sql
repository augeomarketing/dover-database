USE [407]
GO
/****** Object:  StoredProcedure [dbo].[spRollCustomerScrub]    Script Date: 01/28/2013 10:16:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollCustomerScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRollCustomerScrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRollCustomerScrub]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spRollCustomerScrub] @enddate varchar(10)
 AS

/********************************************************************************************/
-- CHANGES:
-- RDT 4/15/2011 Changed 1Bridge Codes to Card Bin numbers. 
-- SEB001 6/3/2013 Added Credit BIN 445282
/********************************************************************************************/

update roll_customer  set accttype = ''Credit'', accttypedesc = ''Credit Card''
where left(acctid,6) in (''469325'', ''445282'') 

update roll_customer  set accttype = ''Debit'', accttypedesc = ''Debit Card''
where left(acctid,6) = ''470305''

update roll_customer set DateAdded =  (select dateadded from  CUSTOMER_stage
where customer_stage.tipnumber = roll_customer.tipnumber)

update roll_customer set dateadded = CONVERT(datetime, @enddate)
where  (dateadded is NULL)
' 
END
GO
