USE [407]
GO
/****** Object:  Table [dbo].[Monthly_Audit_Error_File]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_Error_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Audit_Error_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Audit_Error_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Audit_Error_File](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[CRPointsPurchased] [decimal](18, 0) NULL,
	[PLPointsPurchased] [decimal](18, 0) NULL,
	[DbPointsPurchased] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[CRPointsReturned] [decimal](18, 0) NULL,
	[PLPointsReturned] [decimal](18, 0) NULL,
	[DBPointsReturned] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[ErrorMsg] [varchar](50) NULL,
	[CurrentEnd] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
