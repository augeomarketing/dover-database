USE [407]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transactions]') AND type in (N'U'))
DROP TABLE [dbo].[Transactions]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transactions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Transactions](
	[TipNumber] [varchar](15) NULL,
	[CardNumber] [varchar](25) NULL,
	[TransDate] [varchar](10) NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [varchar](4) NULL,
	[Points] [varchar](15) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
