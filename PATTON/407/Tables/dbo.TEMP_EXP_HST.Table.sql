USE [407]
GO
/****** Object:  Table [dbo].[TEMP_EXP_HST]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TEMP_EXP_HST]') AND type in (N'U'))
DROP TABLE [dbo].[TEMP_EXP_HST]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TEMP_EXP_HST]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TEMP_EXP_HST](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[POINTS] [decimal](18, 0) NULL,
	[histdate] [datetime] NULL,
	[prevexp] [varchar](5) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
