USE [407]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Award]') AND type in (N'U'))
DROP TABLE [dbo].[Award]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Award]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Award](
	[AwardCode] [int] NOT NULL,
	[CatalogCode] [varchar](50) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NOT NULL,
	[Special] [char](1) NOT NULL,
	[Business] [char](1) NOT NULL,
	[Television] [char](1) NOT NULL,
	[ClientAwardPoints] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
