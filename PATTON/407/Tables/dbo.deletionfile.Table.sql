USE [407]
GO
/****** Object:  Table [dbo].[deletionfile]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[deletionfile]') AND type in (N'U'))
DROP TABLE [dbo].[deletionfile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[deletionfile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[deletionfile](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NULL,
	[RunAvailable] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
