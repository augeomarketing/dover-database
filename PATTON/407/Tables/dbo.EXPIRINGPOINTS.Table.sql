USE [407]
GO
/****** Object:  Table [dbo].[EXPIRINGPOINTS]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EXPIRINGPOINTS]') AND type in (N'U'))
DROP TABLE [dbo].[EXPIRINGPOINTS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EXPIRINGPOINTS]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EXPIRINGPOINTS](
	[tipnumber] [varchar](15) NOT NULL,
	[CredAddPoints] [float] NULL,
	[DebAddpoints] [float] NULL,
	[CredRetPoints] [float] NULL,
	[DebRetPoints] [float] NULL,
	[CredAddPointsNext] [float] NULL,
	[DebAddPointsNext] [float] NULL,
	[CredRetPointsNext] [float] NULL,
	[DebRetPointsNext] [float] NULL,
	[REDPOINTS] [float] NOT NULL,
	[POINTSTOEXPIRE] [float] NOT NULL,
	[PREVEXPIRED] [float] NULL,
	[DateofExpire] [nvarchar](25) NULL,
	[PointsToExpireNext] [float] NULL,
	[AddPoints] [float] NULL,
	[ADDPOINTSNEXT] [float] NULL,
	[PointsOther] [float] NULL,
	[PointsOthernext] [float] NULL,
	[EXPTODATE] [float] NULL,
	[ExpiredRefunded] [float] NULL,
	[dbnameonnexl] [nvarchar](50) NULL,
	[CredaddpointsPLUS3MO] [float] NULL,
	[DEbaddpointsPLUS3MO] [float] NULL,
	[CredRetpointsPLUS3MO] [float] NULL,
	[DEbRetpointsPLUS3MO] [float] NULL,
	[POINTSTOEXPIREPLUS3MO] [float] NULL,
	[ADDPOINTSPLUS3MO] [float] NULL,
	[PointsOtherPLUS3MO] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
