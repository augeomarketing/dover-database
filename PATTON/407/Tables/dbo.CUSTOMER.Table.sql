USE [407]
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CUSTOMER__dim_cu__4FD1D5C8]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CUSTOMER__dim_cu__4FD1D5C8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__dim_cu__4FD1D5C8]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CUSTOMER__dim_cu__50C5FA01]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CUSTOMER__dim_cu__50C5FA01]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__dim_cu__50C5FA01]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CUSTOMER__dim_cu__51BA1E3A]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CUSTOMER__dim_cu__51BA1E3A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMER] DROP CONSTRAINT [DF__CUSTOMER__dim_cu__51BA1E3A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMER]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[dim_customer_email] [varchar](254) NOT NULL,
	[dim_customer_country] [varchar](3) NOT NULL,
	[dim_customer_mobilephone] [varchar](10) NOT NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CUSTOMER__dim_cu__4FD1D5C8]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CUSTOMER__dim_cu__4FD1D5C8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ('') FOR [dim_customer_email]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CUSTOMER__dim_cu__50C5FA01]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CUSTOMER__dim_cu__50C5FA01]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ('USA') FOR [dim_customer_country]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CUSTOMER__dim_cu__51BA1E3A]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMER]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CUSTOMER__dim_cu__51BA1E3A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMER] ADD  DEFAULT ('') FOR [dim_customer_mobilephone]
END


End
GO
