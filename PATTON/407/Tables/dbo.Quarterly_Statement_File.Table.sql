USE [407]
GO
SET ANSI_PADDING On
Go 

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
BEGIN

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Acctname3] [char](40) NULL,
	[Acctname4] [char](40) NULL,
	[Acctname5] [char](40) NULL,
	[Acctname6] [char](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[PointsBegin] [decimal](18, 0) NULL default 0,
	[PointsEnd] [decimal](18, 0) NULL default 0,
	[PointsPurchased] [decimal](18, 0) NULL default 0,
	[PointsBonus] [decimal](18, 0) NULL default 0,
	[PointsAdded] [decimal](18, 0) NULL default 0,
	[PointsIncreased] [decimal](18, 0) NULL default 0,
	[PointsRedeemed] [decimal](18, 0) NULL default 0,
	[PointsReturned] [decimal](18, 0) NULL default 0,
	[PointsSubtracted] [decimal](18, 0) NULL default 0,
	[PointsDecreased] [decimal](18, 0) NULL default 0,
	[ExpPnts] [decimal](18, 0) NULL default 0,
	[Status] [varchar](10) NULL,
	[EmailStatement] [varchar](2) NULL,
	[PointsBonusMN] [decimal](18, 0) default 0 ,	

 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
go