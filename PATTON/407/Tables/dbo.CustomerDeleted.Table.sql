USE [407]
GO
/****** Object:  Table [dbo].[CustomerDeleted]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CustomerD__dim_c__52AE4273]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomerD__dim_c__52AE4273]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerDeleted] DROP CONSTRAINT [DF__CustomerD__dim_c__52AE4273]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CustomerD__dim_c__53A266AC]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomerD__dim_c__53A266AC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerDeleted] DROP CONSTRAINT [DF__CustomerD__dim_c__53A266AC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CustomerD__dim_c__54968AE5]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomerD__dim_c__54968AE5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerDeleted] DROP CONSTRAINT [DF__CustomerD__dim_c__54968AE5]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[CustomerDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerDeleted](
	[TIPNumber] [varchar](15) NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [decimal](10, 0) NULL,
	[RunRedeemed] [decimal](10, 0) NULL,
	[RunAvailable] [decimal](10, 0) NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[Notes] [text] NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BusinessFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[datedeleted] [datetime] NULL,
	[BonusFlag] [varchar](1) NULL,
	[dim_customer_email] [varchar](254) NOT NULL,
	[dim_customer_country] [varchar](3) NOT NULL,
	[dim_customer_mobilephone] [varchar](10) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CustomerD__dim_c__52AE4273]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomerD__dim_c__52AE4273]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerDeleted] ADD  DEFAULT ('') FOR [dim_customer_email]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CustomerD__dim_c__53A266AC]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomerD__dim_c__53A266AC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerDeleted] ADD  DEFAULT ('USA') FOR [dim_customer_country]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__CustomerD__dim_c__54968AE5]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerDeleted]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomerD__dim_c__54968AE5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomerDeleted] ADD  DEFAULT ('') FOR [dim_customer_mobilephone]
END


End
GO
