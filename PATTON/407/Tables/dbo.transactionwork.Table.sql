USE [407]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transactionwork]') AND type in (N'U'))
DROP TABLE [dbo].[transactionwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transactionwork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[transactionwork](
	[AcctID] [varchar](25) NULL,
	[TipNumber] [varchar](15) NULL,
	[Trancode] [varchar](2) NULL,
	[Last6] [varchar](6) NULL,
	[Bonus] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
