USE [407]
GO
/****** Object:  Table [dbo].[Input_Transactions_error]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_error]') AND type in (N'U'))
DROP TABLE [dbo].[Input_Transactions_error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_Transactions_error]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Input_Transactions_error](
	[CardNumber] [varchar](19) NULL,
	[Unused] [varchar](19) NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [int] NULL,
	[TipNumber] [varchar](15) NULL,
	[TransDate] [varchar](10) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [varchar](4) NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
