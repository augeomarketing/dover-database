USE [407]
GO
/****** Object:  Table [dbo].[DeletedReport]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeletedReport]') AND type in (N'U'))
DROP TABLE [dbo].[DeletedReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeletedReport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeletedReport](
	[Tipnumber] [nvarchar](255) NULL,
	[Acctname1] [nvarchar](255) NULL,
	[Acctname2] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[PointsEnd] [float] NULL,
	[PointsNow] [float] NULL
) ON [PRIMARY]
END
GO
