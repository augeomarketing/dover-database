USE [407]
GO
/****** Object:  Table [dbo].[1Security]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND type in (N'U'))
DROP TABLE [dbo].[1Security]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[1Security]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[1Security](
	[TipNumber] [char](15) NOT NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[Last6] [char](6) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [int] NULL,
	[username] [varchar](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
