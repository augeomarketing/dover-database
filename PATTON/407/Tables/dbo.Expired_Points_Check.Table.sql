USE [407]
GO
/****** Object:  Table [dbo].[Expired_Points_Check]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Expired_Points_Check_h_exp_pts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Expired_Points_Check_h_exp_pts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Expired_Points_Check] DROP CONSTRAINT [DF_Expired_Points_Check_h_exp_pts]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Expired_Points_Check_a_exp_pts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Expired_Points_Check_a_exp_pts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Expired_Points_Check] DROP CONSTRAINT [DF_Expired_Points_Check_a_exp_pts]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Expired_Points_Check_point_difference]') AND parent_object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Expired_Points_Check_point_difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Expired_Points_Check] DROP CONSTRAINT [DF_Expired_Points_Check_point_difference]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]') AND type in (N'U'))
DROP TABLE [dbo].[Expired_Points_Check]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Expired_Points_Check](
	[tipnumber] [varchar](15) NULL,
	[h_exp_pts] [decimal](18, 0) NOT NULL,
	[a_exp_pts] [decimal](18, 0) NOT NULL,
	[point_difference] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Expired_Points_Check_h_exp_pts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Expired_Points_Check_h_exp_pts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Expired_Points_Check] ADD  CONSTRAINT [DF_Expired_Points_Check_h_exp_pts]  DEFAULT ((0)) FOR [h_exp_pts]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Expired_Points_Check_a_exp_pts]') AND parent_object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Expired_Points_Check_a_exp_pts]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Expired_Points_Check] ADD  CONSTRAINT [DF_Expired_Points_Check_a_exp_pts]  DEFAULT ((0)) FOR [a_exp_pts]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Expired_Points_Check_point_difference]') AND parent_object_id = OBJECT_ID(N'[dbo].[Expired_Points_Check]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Expired_Points_Check_point_difference]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Expired_Points_Check] ADD  CONSTRAINT [DF_Expired_Points_Check_point_difference]  DEFAULT ((0)) FOR [point_difference]
END


End
GO
