USE [407]
GO
/****** Object:  Table [dbo].[TransactionSummary]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionSummary]') AND type in (N'U'))
DROP TABLE [dbo].[TransactionSummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransactionSummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TransactionSummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[CRPurchases] [decimal](18, 2) NULL,
	[CRReturned] [decimal](18, 2) NULL,
	[DBPurchases] [decimal](18, 2) NULL,
	[DBReturned] [decimal](18, 0) NULL,
	[PLPurchases] [decimal](18, 0) NULL,
	[PLReturned] [decimal](18, 0) NULL,
	[Bonuses] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
