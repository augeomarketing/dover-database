USE [407]
GO
/****** Object:  Table [dbo].[MonthlySummary]    Script Date: 01/28/2013 10:10:31 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_CRPurchases]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_CRPurchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_CRPurchases]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_CRReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_CRReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_CRReturned]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_DBPurchases]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_DBPurchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_DBPurchases]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_DBReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_DBReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_DBReturned]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_PLPurchases]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_PLPurchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_PLPurchases]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_PLReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_PLReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_PLReturned]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_Bonuses]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_Bonuses]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] DROP CONSTRAINT [DF_MonthlySummary_Bonuses]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlySummary]') AND type in (N'U'))
DROP TABLE [dbo].[MonthlySummary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MonthlySummary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MonthlySummary](
	[rowid] [int] IDENTITY(1,1) NOT NULL,
	[Dateadded] [datetime] NULL,
	[CRPurchases] [decimal](18, 2) NULL,
	[CRReturned] [decimal](18, 2) NULL,
	[DBPurchases] [decimal](18, 2) NULL,
	[DBReturned] [decimal](18, 0) NULL,
	[PLPurchases] [decimal](18, 0) NULL,
	[PLReturned] [decimal](18, 0) NULL,
	[Bonuses] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_CRPurchases]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_CRPurchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_CRPurchases]  DEFAULT (0) FOR [CRPurchases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_CRReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_CRReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_CRReturned]  DEFAULT (0) FOR [CRReturned]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_DBPurchases]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_DBPurchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_DBPurchases]  DEFAULT (0) FOR [DBPurchases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_DBReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_DBReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_DBReturned]  DEFAULT (0) FOR [DBReturned]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_PLPurchases]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_PLPurchases]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_PLPurchases]  DEFAULT (0) FOR [PLPurchases]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_PLReturned]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_PLReturned]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_PLReturned]  DEFAULT (0) FOR [PLReturned]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_MonthlySummary_Bonuses]') AND parent_object_id = OBJECT_ID(N'[dbo].[MonthlySummary]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_MonthlySummary_Bonuses]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MonthlySummary] ADD  CONSTRAINT [DF_MonthlySummary_Bonuses]  DEFAULT (0) FOR [Bonuses]
END


End
GO
