/****** Object:  Table [dbo].[Transactions]    Script Date: 02/16/2009 11:41:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transactions](
	[TipNumber] [varchar](15) NULL,
	[TransDate] [varchar](10) NULL,
	[CardNumber] [varchar](25) NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [varchar](4) NULL,
	[Points] [varchar](15) NULL CONSTRAINT [DF_Transactions_Points]  DEFAULT (0),
	[AccountType] [varchar](20) NULL,
	[Ratio] [varchar](4) NULL,
	[Unused] [varchar](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
