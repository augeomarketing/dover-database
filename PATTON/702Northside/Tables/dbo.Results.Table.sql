/****** Object:  Table [dbo].[Results]    Script Date: 02/16/2009 11:41:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Results](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [smalldatetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
