/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 02/16/2009 11:41:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[address4] [varchar](50) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [char](10) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0),
	[PointsPurchasedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0),
	[PointsPurchasedDb] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDb]  DEFAULT (0),
	[PointsBonus] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0),
	[PointsReturnedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0),
	[PointsReturnedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
