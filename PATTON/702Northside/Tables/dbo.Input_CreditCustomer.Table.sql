/****** Object:  Table [dbo].[Input_CreditCustomer]    Script Date: 02/19/2009 09:54:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_CreditCustomer](
	[AcctID] [varchar](25) NULL,
	[System Bank Identifier] [varchar](4) NULL,
	[Principal Bank Identifier] [varchar](4) NULL,
	[Agent Bank Identifier] [varchar](4) NULL,
	[AcctName1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[HomePhone] [varchar](10) NULL,
	[StatusCode] [varchar](1) NULL,
	[ExtStatusCode] [varchar](1) NULL,
	[SSN] [varchar](9) NULL,
	[SecondSSN] [varchar](9) NULL,
	[AcctID2] [varchar](25) NULL,
	[ChangeDate] [datetime] NULL,
	[Misc7] [varchar](8) NULL,
	[DateAdded] [datetime] NULL,
	[Tipnumber] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Address4] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
