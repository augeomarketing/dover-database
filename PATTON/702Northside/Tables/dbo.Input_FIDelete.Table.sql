USE [702NorthSide]
GO

SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[Input_FIDelete]    Script Date: 05/11/2011 10:58:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_FIDelete]') AND type in (N'U'))
DROP TABLE [dbo].[Input_FIDelete]
GO

USE [702NorthSide]
GO

/****** Object:  Table [dbo].[Input_FIDelete]    Script Date: 05/11/2011 10:58:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_FIDelete](
	[TipNumber] [char](15) NULL,
	[AcctName] [varchar](60) NULL,
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


