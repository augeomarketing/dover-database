USE [702NorthSide]
GO

/****** Object:  Table [dbo].[Input_DebitCustomer]    Script Date: 04/15/2010 13:42:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Input_DebitCustomer]') AND type in (N'U'))
DROP TABLE [dbo].[Input_DebitCustomer]
GO

USE [702NorthSide]
GO

/****** Object:  Table [dbo].[Input_DebitCustomer]    Script Date: 04/15/2010 13:42:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Input_DebitCustomer](
	[SSN] [varchar](9) NULL,
	[CardNumber] [varchar](25) NULL,
	[CardNumberTrunc] [varchar](16) NULL,
	[AcctName1] [varchar](40) NULL,
	[TaxOwnerName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[HomePhone] [varchar](15) NULL,
	[BusinessPhone] [varchar](15) NULL,
	[CardStatus] [varchar](1) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Address4] [varchar](40) NULL,
	[DateAdded] [datetime] NULL,
	[Misc7] [varchar](8) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


