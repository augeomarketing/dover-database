/****** Object:  Table [dbo].[DBProcessInfo]    Script Date: 02/16/2009 11:40:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBProcessInfo](
	[DBName] [varchar](50) NOT NULL,
	[DBNumber] [nchar](3) NOT NULL,
	[ActivationBonus] [decimal](18, 0) NOT NULL,
	[FirstUseBonus] [decimal](18, 0) NOT NULL,
	[OnlineRegistrationBonus] [decimal](18, 0) NOT NULL,
	[E-StatementBonus] [decimal](18, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
