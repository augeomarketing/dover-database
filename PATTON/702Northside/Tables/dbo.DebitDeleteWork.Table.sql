/****** Object:  Table [dbo].[DebitDeleteWork]    Script Date: 02/16/2009 11:40:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DebitDeleteWork](
	[Tin] [varchar](9) NULL,
	[CardNum] [varchar](6) NULL,
	[TipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
