/****** Object:  Table [dbo].[wrkhist]    Script Date: 02/16/2009 11:42:03 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkhist](
	[tipnumber] [varchar](15) NOT NULL,
	[trancode] [varchar](2) NULL,
	[points] [decimal](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
