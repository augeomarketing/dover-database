/****** Object:  Table [dbo].[transactionworkerror]    Script Date: 02/16/2009 11:41:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionworkerror](
	[TaxID] [varchar](9) NULL,
	[CardNumber] [varchar](15) NULL,
	[AcctNum] [varchar](10) NULL,
	[TranCode] [varchar](4) NULL,
	[Amount] [decimal](18, 0) NULL,
	[TranCount] [decimal](18, 0) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
