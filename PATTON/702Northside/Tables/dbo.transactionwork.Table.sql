/****** Object:  Table [dbo].[transactionwork]    Script Date: 02/16/2009 11:41:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transactionwork](
	[TaxID] [varchar](9) NULL,
	[CardNumber] [varchar](25) NULL,
	[AcctNum] [varchar](10) NULL,
	[TranCode] [varchar](4) NULL,
	[Amount] [decimal](18, 0) NULL CONSTRAINT [DF_transactionwork_Amount]  DEFAULT (0),
	[TranCount] [numeric](18, 0) NULL,
	[tipnumber] [char](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
