USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spMarkAcctsforDelete]    Script Date: 05/03/2010 16:23:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spMarkAcctsforDelete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spMarkAcctsforDelete]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spMarkAcctsforDelete]    Script Date: 05/03/2010 16:23:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spMarkAcctsforDelete]
as

UPDATE input_delete_accounts SET Tipnumber = b.tipnumber
FROM input_delete_accounts a join affiliat_stage b
on  a.cardnumber = b.acctid where a.cardnumber = b.acctid

delete input_delete_accounts where TIPNUMBER is null

/********************************************************************************/
/* 05/03/2010 code changed to add distinct clause to inner update to restrict   */
/* the return of a specific cardnumber multiple times.                          */
/********************************************************************************/
UPDATE AFFILIAT_Stage SET AcctStatus = 'C'
WHERE (ACCTID = (SELECT distinct cardnumber FROM input_delete_accounts
				 WHERE  affiliat_stage.acctid = input_delete_accounts.cardnumber))

UPDATE CUSTOMER_Stage SET STATUS = 'P', StatusDescription = 'Pending Purge'
WHERE (STATUS = 'A') 
AND (NOT EXISTS (SELECT 1 FROM affiliat_stage
				 WHERE      tipnumber = customer_stage.tipnumber AND (acctstatus = 'A' OR acctstatus IS NULL)))


GO


