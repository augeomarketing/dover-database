/****** Object:  StoredProcedure [dbo].[sp702GenerateTIPNumbersDebit]    Script Date: 02/16/2009 11:39:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[sp702GenerateTIPNumbersDebit]
AS 

--update debitcustomerwork 
--set Tipnumber = b.tipnumber
--from debitcustomerwork a,affiliat b
--where a.cardnumber= b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

update debitcustomerwork 
set Tipnumber = b.tipnumber
from debitcustomerwork a,affiliat b
where a.custid= b.custid and (a.tipnumber is null or a.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct custid, tipnumber	
from debitcustomerwork

declare @newnum bigint
declare @LastTipUsed char(15)

exec rewardsnow.dbo.spGetLastTipNumberUsed 702, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 702000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '


exec RewardsNOW.dbo.spPutLastTipNumberUsed 702, @newnum

update debitcustomerwork 
set Tipnumber = b.tipnumber
from debitcustomerwork a,gentip b
where a.custid = b.custid and (a.tipnumber is null or a.tipnumber = ' ')
GO
