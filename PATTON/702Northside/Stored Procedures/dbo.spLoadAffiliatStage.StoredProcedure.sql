USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/18/2010 10:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadAffiliatStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadAffiliatStage]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 11/18/2010 10:11:32 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

/***************************************************************************/
/*  Revisions: 1) Comment out resetting acctstatus to match input customer */
/***************************************************************************/

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
/*Declare @monthend datetime  */			
/*set @monthend = '02/01/2009' */

Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select c.cardnumber, c.TipNumber, 'Credit', @monthend, right(c.custid,4),  c.Statuscode, 'Credit Card', c.LastName, 0, c.custid
	from roll_Customer c where c.cardnumber not in ( Select acctid from Affiliat_Stage)


/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType

update affiliat_stage
set lastname = b.lastname 
from input_customer b, affiliat_stage a
where   b.tipnumber = a.tipnumber

-- Revision (1)
--update affiliat_stage
--set acctstatus = b.statuscode
--from input_customer b, affiliat_stage a
--where   b.tipnumber = a.tipnumber
GO


