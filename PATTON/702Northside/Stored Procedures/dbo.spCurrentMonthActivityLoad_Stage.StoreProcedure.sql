CREATE PROCEDURE spCurrentMonthActivityLoad_Stage @EndDateParm varchar(10)
AS

Declare @EndDate DateTime                         
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )   

delete from Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints)
select tipnumber, runavailable 
from Customer_stage

/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history_stage where histdate>@enddate and ratio='1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)
where exists(select * from history_stage where histdate>@enddate and ratio='1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history_stage where histdate>@enddate and ratio='-1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)
where exists(select * from history_stage where histdate>@enddate and ratio='-1' and history_stage.tipnumber=Current_Month_Activity.tipnumber)

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases

update Current_Month_Activity
set adjustedendingpoints=0 where adjustedendingpoints is null
GO
