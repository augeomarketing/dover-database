USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spInputCreditScrub]    Script Date: 11/16/2010 16:47:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputCreditScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputCreditScrub]
GO

/****** Object:  StoredProcedure [dbo].[spInputCreditScrub]    Script Date: 11/16/2010 16:47:35 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInputCreditScrub]
 AS

--RDT 6/13/2011 - remove null records
Delete From Input_CreditCustomer 
Where AcctID 	 is null and 
	  [System Bank Identifier]   is null and 
	  AcctName1  is null 

update Input_CreditCustomer set acctname1 = upper(acctname1),acctname2 = upper(acctname2)

update Input_CreditCustomer set acctname1 = ltrim(RTRIM(SUBSTRING(acctname1, CHARINDEX(',', acctname1) 
+ 1, LEN(RTRIM(acctname1)))) + ' ' + SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1))
where  (SUBSTRING(acctname1, 1, 1) NOT LIKE ' ') AND (acctname1  IS NOT NULL) AND (acctname1 LIKE '%,%')

update Input_CreditCustomer set acctname2 = ltrim(RTRIM(SUBSTRING(acctname2, CHARINDEX(',', acctname2) 
+ 1, LEN(RTRIM(acctname2)))) + ' ' + SUBSTRING(acctname2, 1, CHARINDEX(',', acctname2) - 1))
where  (SUBSTRING(acctname2, 1, 1) NOT LIKE ' ') AND (acctname2  IS NOT NULL) AND (acctname2 LIKE '%,%')

update Input_CreditCustomer set lastname = ltrim(reverse(left(reverse(ACCTNAME1),CHARINDEX(' ',reverse(acctname1)) ) )) 
from Input_CreditCustomer  where lastname is null 

update Input_CreditCustomer set zipcode =rtrim(ltrim(zipcode))

update Input_CreditCustomer set zipcode = replicate('0',5 - len(zipcode)) + zipcode where len(zipcode) < 5
update Input_CreditCustomer set zipcode = replicate('0',9 - len(zipcode)) + zipcode where (len(zipcode)  > 5 and len(zipcode) < 10)
update Input_CreditCustomer set zipcode = '00000' where zipcode is null


update Input_CreditCustomer set ssn = replicate('0',9 - len(ssn)) + ssn

--update Input_CreditCustomer set homephone = substring(homephone,2,3)+ substring(homephone,6,3)+ substring(homephone,10,4)
--update Input_CreditCustomer set businessphone = substring(businessphone,2,3)+ substring(businessphone,6,3)+ substring(businessphone,10,4)

update Input_CreditCustomer set statuscode = 'A'

-- Added zipcode change 1
update Input_CreditCustomer set address4 = rtrim(city) + ' ' + rtrim(state) + ' ' + LEFT(zipcode,5)

UPDATE Input_CreditCustomer SET DateAdded =(SELECT dateadded FROM CUSTOMER_stage
WHERE customer_stage.tipnumber = Input_CreditCustomer.tipnumber)
WHERE EXISTS(SELECT * FROM customer_stage
WHERE customer_stage.tipnumber = Input_CreditCustomer.tipnumber)
GO


