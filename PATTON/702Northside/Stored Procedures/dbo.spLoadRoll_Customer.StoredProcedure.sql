USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spLoadRoll_Customer]    Script Date: 04/15/2010 11:43:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadRoll_Customer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadRoll_Customer]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spLoadRoll_Customer]    Script Date: 04/15/2010 11:43:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spLoadRoll_Customer]
 AS

-------------------------------------------------------------------------------------------
--  changed insert to select distinct in order to remove duplicate entries df 6/7/2010
-------------------------------------------------------------------------------------------
insert Roll_Customer (CardNumber,TIPNUMBER)
select distinct acctid,tipnumber from input_creditcustomer

update Roll_Customer set cardnumber=icc.acctid,acctname1=icc.acctname1,acctname2=icc.acctname2,statuscode=icc.statuscode,lastname=icc.lastname,
address1=icc.address1,address2=icc.address2,address4=icc.address4,zipcode=icc.zipcode,city=icc.city,[state]=icc.[state],homephone=icc.homephone,
custid=ssn,dateadded=icc.dateadded,misc7=icc.misc7
from Roll_Customer RS join input_creditcustomer icc on RS.CardNumber = icc.acctid

-------------------------------------------------------------------------------------------
--  changed insert to select distinct in order to remove duplicate entries df 6/7/2010
-------------------------------------------------------------------------------------------
/***************************************************************************************************/
/*  changed code in select removed 'X' as hardcoded value for column MISC7   df 04/15/2010         */
/***************************************************************************************************/
insert Roll_Customer (CardNumber,TIPNUMBER)
select distinct cardnumber,tipnumber from input_debitcustomer

update Roll_Customer set cardnumber=idc.CardNumber,acctname1=idc.acctname1,acctname2=' ',statuscode=idc.cardstatus,lastname=idc.lastname,
address1=idc.address1,address2=' ',address4=idc.address4,zipcode=idc.zipcode,city=idc.city,[state]=idc.[state],homephone=idc.homephone,
custid=ssn,dateadded=idc.dateadded,misc7=idc.misc7
from Roll_Customer RS join input_debitcustomer idc on RS.CardNumber = idc.CardNumber

/*********************************************************************************************************/
/*  added next 3 updates to standardize content of acctname fields for later comparisons   df 04/15/2010 */
/*********************************************************************************************************/
update Roll_Customer set ACCTNAME1 = upper(lTRIM(rtrim(acctname1)))
update Roll_Customer set ACCTNAME2 = upper(lTRIM(rtrim(acctname2))) where acctname2 <> ' ' or acctname2 is not null
update roll_CUSTOMER set acctname1= SUBSTRING(acctname1,2,len(acctname1)) 
where substring(ACCTNAME1,1,1) = ' '

insert input_customer (tipnumber)
select distinct(tipnumber)from roll_customer 

update input_customer set cardnumber = b.cardnumber,acctname1 = b.acctname1,acctname2 = b.acctname2,statuscode = b.statuscode,
lastname = b.lastname,address1 = b.address1,address2 = b.address2,address4 = b.address4,zipcode =b.zipcode,city = b.city,
state = b.state,homephone = b.homephone,custid = b.custid,dateadded = b.dateadded 
from input_customer a, roll_customer b
where b.misc7 = 'Z' and a.acctname1 is null and a.tipnumber = b.tipnumber

update input_customer set cardnumber = b.cardnumber,acctname1 = b.acctname1,acctname2 = b.acctname2,statuscode = b.statuscode,
lastname = b.lastname,address1 = b.address1,address2 = b.address2,address4 = b.address4,zipcode =b.zipcode,city = b.city,
state = b.state,homephone = b.homephone,custid = b.custid,dateadded = b.dateadded 
from input_customer a, roll_customer b
where a.acctname1 is null and a.tipnumber = b.tipnumber

/***************************************************************************************************/
/*  changed code in next two updates added a.acctname2 = ' '     df 04/15/2010                     */
/***************************************************************************************************/
update input_customer set acctname2 = b.acctname1
from input_customer a, roll_customer b
where a.acctname1 <> b.acctname1 and a.tipnumber = b.tipnumber and (a.acctname2 is null or a.acctname2 = ' ')

update input_customer set acctname2 = b.acctname2
from input_customer a, roll_customer b
where a.acctname1 <> b.acctname2 and a.tipnumber = b.tipnumber and (a.acctname2 is null or a.acctname2 = ' ')

update input_customer set lastname = acctname1 where (lastname is null or lastname = ' ')

update input_customer set acctname1 = ltrim(acctname1), acctname2 = ltrim(acctname2) 

GO


