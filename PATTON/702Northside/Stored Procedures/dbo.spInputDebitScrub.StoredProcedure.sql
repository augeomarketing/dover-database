USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spInputDebitScrub]    Script Date: 10/15/2009 16:51:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[spInputDebitScrub]    Script Date: 06/13/2011 16:47:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputDebitScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputDebitScrub]
GO

/*
RDT 6/13/2011 - remove null records
*/

Create PROCEDURE [dbo].[spInputDebitScrub]
 AS

--RDT 6/13/2011 - remove null records
Delete From Input_DebitCustomer 
Where SSN		 is null and 
	  CardNumber is null and 
	  acctname1  is null 

update Input_DebitCustomer set acctname1 = upper(acctname1)

update Input_DebitCustomer set acctname1 = RTRIM(SUBSTRING(acctname1, CHARINDEX(',', acctname1) 
+ 1, LEN(RTRIM(acctname1)))) + ' ' + SUBSTRING(acctname1, 1, CHARINDEX(',', acctname1) - 1)
where  (SUBSTRING(acctname1, 1, 1) NOT LIKE ' ') AND (acctname1  IS NOT NULL) AND (acctname1 LIKE '%,%')

update input_debitcustomer set lastname = ltrim(reverse(left(reverse(ACCTNAME1),CHARINDEX(' ',reverse(acctname1)) ) ) )
from input_debitcustomer  where lastname is null 

update Input_DebitCustomer set lastname = upper(lastname)

update Input_DebitCustomer set address1 = upper(address1)

update Input_DebitCustomer set city = upper(city)

update Input_DebitCustomer set zipcode =rtrim(ltrim(zipcode))

update Input_DebitCustomer set zipcode = replicate('0',5 - len(zipcode)) + zipcode where len(zipcode) < 5
update Input_DebitCustomer set zipcode = replicate('0',10 - len(zipcode)) + zipcode where (len(zipcode) > 5 and len(zipcode) < 10)
update Input_DebitCustomer set zipcode = '00000' where zipcode is null

update Input_DebitCustomer set ssn = replicate('0',9 - len(ssn)) + ssn

update Input_DebitCustomer set homephone = substring(homephone,2,3)+ substring(homephone,6,3)+ substring(homephone,10,4)

update Input_DebitCustomer set businessphone = substring(businessphone,2,3)+ substring(businessphone,6,3)+ substring(businessphone,10,4)

update Input_DebitCustomer set cardstatus = 'A'

update Input_DebitCustomer set address4 = city + ' ' + state + ' '  + zipcode