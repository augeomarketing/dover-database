USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 11/16/2010 16:49:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 11/16/2010 16:49:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
        
/* BY:  D Foster  */
/* DATE: 09/2009   */
/* REVISION: 1 add delete customers w/o history                               */
/* pending purge procedure for FI's Using Points Now                          */
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedateParm varchar(10)AS    


/* Testing Parameters */

--declare @purgedateParm nvarchar(10)
--set @purgedateParm = '12/31/2011'

/* Declarations */
Declare @purgedate DateTime     --SEB 01/13/2012
set @purgedate = convert(datetime, @purgedateParm+' 23:59:59:990' )    --SEB 01/13/2012


declare @tipnumber varchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge(tipnumber,DeletionDate)
select tipnumber, @purgedate from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

update customer set status = 'C', statusdescription = 'Closed'
from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
where @purgedate >= (select max(histdate) from history
where c.tipnumber  = tipnumber)

--  revision 1

update customer set status = 'C', statusdescription = 'Closed'
where Status = 'P' and TIPNUMBER not in (select TIPNUMBER from HISTORY)

delete from PendingPurge
    where tipnumber in (select tipnumber from customer where status = 'C')


GO


