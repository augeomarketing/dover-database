USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spScrubTransactions_Stage]    Script Date: 11/16/2010 15:01:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spScrubTransactions_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spScrubTransactions_Stage]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spScrubTransactions_Stage]    Script Date: 11/16/2010 15:01:53 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



CREATE PROCEDURE [dbo].[spScrubTransactions_Stage] @monthendingdate varchar(10)
AS

/**********************************************************************************/
/* Changes: 1) change points awarded on credit purchases from 1/1 to 1/2          */
/**********************************************************************************/

UPDATE transactionworkcr SET custid = replicate('0',9 - len(custid))+ custid

UPDATE transactionworkcr SET Tipnumber = b.tipnumber
FROM transactionworkcr a, affiliat_stage b
WHERE (a.custid = b.custid or a.cardnumber = b.acctid) AND (a.tipnumber IS NULL OR a.tipnumber = ' ')

-- Change 1 --

INSERT INTO input_transaction (TipNumber, CardNumber, TranCode, trancnt, Points, AcctType, ratio)
SELECT  TipNumber, CardNumber, '33', '1', ROUND([RETURNS]/2,0,0)as Points, 'Credit', '-1' 
FROM transactionworkcr
WHERE ([RETURNS] > 0)

INSERT INTO input_transaction (TipNumber, CardNumber, TranCode, trancnt, Points, AcctType, ratio)
SELECT  TipNumber, CardNumber, '63', '1', ROUND(Purchase/2,0,0)as Points, 'Credit', '1' 
FROM transactionworkcr
WHERE (PURCHASE > 0)

INSERT INTO input_transaction (TipNumber, CardNumber, TranCode, trancnt, Points, AcctType, ratio)
SELECT  TipNumber, CardNumber, 'BI', '1', Bonus, 'Credit', '1' 
FROM transactionworkcr
WHERE (Bonus > 0)

UPDATE transactionwork SET taxid = replicate('0',9 - len(taxid))+ taxid

UPDATE transactionwork SET Tipnumber = b.tipnumber
FROM transactionwork a, affiliat_stage b
WHERE a.taxid = b.custid 

INSERT into input_transaction (TipNumber, CardNumber, TranCode, TranCnt, Points,  AcctType, Ratio)
SELECT tipnumber, CardNumber, '37', TranCount, ROUND(Amount/4,0,0), 'Debit', '-1' AS ratio  
FROM  transactionwork WHERE(TranCode = 'ret')

INSERT into  input_transaction (TipNumber, CardNumber, TranCode, TranCnt, Points,  AcctType, Ratio)
SELECT tipnumber, CardNumber, '67', TranCount, ROUND(Amount/4,0,0), 'Debit', '1' AS ratio  
FROM transactionwork WHERE (TranCode <> 'ret')

UPDATE input_Transaction SET DateAdded = @monthendingdate

insert input_transaction_error
select * from input_transaction
where (tipnumber is null or tipnumber = ' ')

delete  input_transaction where (tipnumber is null or tipnumber = ' ')


GO


