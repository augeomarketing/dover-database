/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 02/16/2009 11:39:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10), @pagect varchar(6)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear
set @pagect = (select count(*) from welcomekit)

set @filename='W' + @TipPrefix + @currentdate + '_' + @pagect + '.xls'
 
set @newname='O:\702\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='O:\702\Output\WelcomeKits\' + @filename
GO
