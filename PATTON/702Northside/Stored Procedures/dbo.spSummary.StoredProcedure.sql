SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
CREATE  PROCEDURE spSummary @enddate char(10)
AS
declare @inputCRPurchase numeric (9), @outputCRPurchase numeric(9), @inputDBPurchase numeric (9), @OutputDBPurchase numeric (9),
@inputCRReturn numeric(9), @outputCRReturn numeric(9), @inputDBReturn numeric(9), @outputDBReturn numeric(9),
@inputBonus numeric(9), @outputBonus numeric(9)    

set @inputCRPurchase = (select sum(purchase) from transactionworkcr) 
set @inputCRReturn = (select sum([returns]) from transactionworkcr)

set @outputCRPurchase = (select sum(convert(numeric(9),points)) from transstandard where trancode = '63')
set @outputCRReturn = (select sum(convert(numeric(9),points)) from transstandard where trancode = '33')

set @inputDBPurchase = (select sum(amount) from transactionwork where trancode <> 'ret') 
set @inputDBReturn = (select sum(amount) from transactionwork where trancode = 'ret')

set @outputDBPurchase = (select sum(convert(decimal(18,2),points)) from transstandard where trancode = '67')
set @outputDBReturn = (select sum(convert(decimal(18,2),points)) from transstandard where trancode = '37')

set @inputCRPurchase = (select sum(purchase) from transactionworkcr) 
set @inputCRReturn = (select sum([returns]) from transactionworkcr)

set @outputBonus = (select sum(convert(numeric(9),points)) from transstandard where trancode = 'BI')
set @inputbonus = (select sum(bonus) from transactionworkcr)

insert summary (rundate) values (@enddate)

update summary set crpurchase_in = @inputcrpurchase, crpurchase_out = @outputCRPurchase, crreturn_in = @inputCRReturn, crreturn_out = @outputCRReturn
where  rowid = (select max(rowid)from summary)

update summary set dbpurchase_in = @inputDBPurchase, dbpurchase_out = @outputDBPurchase, dbreturn_in = @inputDBReturn, dbreturn_out = @outputDBReturn
where  rowid = (select max(rowid)from summary)

update summary set Bonuses_in = @inputbonus, Bonuses_out = @outputBonus
where  rowid = (select max(rowid)from summary)

GO

