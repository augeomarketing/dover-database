/****** Object:  StoredProcedure [dbo].[spDebitRemovePunctuation]    Script Date: 02/16/2009 11:39:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spDebitRemovePunctuation] AS

update debitcustomerwork
set acctname1=replace(acctname1,char(39), ' '),acctname2=replace(acctname2,char(39), ' '), address1=replace(address1,char(39), ' '),
address4=replace(address4,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')
 
update debitcustomerwork
set acctname1=replace(acctname1,char(44), ' '),acctname2=replace(acctname2,char(44), ' '), address1=replace(address1,char(44), ' '),
address4=replace(address4,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update debitcustomerwork
set acctname1=replace(acctname1,char(46), ' '),acctname2=replace(acctname2,char(46), ' '), address1=replace(address1,char(46), ' '),
address4=replace(address4,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update debitcustomerwork
set acctname1=replace(acctname1,char(96), ' '),acctname2=replace(acctname2,char(96), ' '), address1=replace(address1,char(96), ' '),
address4=replace(address4,char(96), ' '),   city=replace(city,char(96), ' '),  lastname=replace(lastname,char(96), ' ')
GO
