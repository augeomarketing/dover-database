USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spScrubTransactions]    Script Date: 11/03/2010 16:13:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spScrubTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spScrubTransactions]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spScrubTransactions]    Script Date: 11/03/2010 16:13:13 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spScrubTransactions] @monthendingdate varchar(10)
AS

/***********************************************************************************/
/*  Changes: 1) Points awarded on debit purchases changed from 1/2 to 1/4          */
/***********************************************************************************/

UPDATE transactionwork SET taxid = replicate('0',9 - len(taxid))+ taxid

UPDATE transactionwork SET Tipnumber = b.tipnumber
FROM transactionwork a, affiliat b
WHERE a.taxid = b.custid AND (a.tipnumber IS NULL OR a.tipnumber = ' ')

UPDATE transactionwork set cardnumber = b.acctid
from transactionwork a, affiliat b
where a.tipnumber= b.tipnumber and right(acctid,6) = cardnumber and taxid = custid

insert transactionworkerror
select * from transactionwork
where (tipnumber is null or tipnumber = ' ') or (cardnumber is null or cardnumber = ' ') or (taxid = '0'  or len(taxid) < 9)

delete  transactionwork where (tipnumber is null or tipnumber = ' ') or (cardnumber is null or cardnumber = ' ') or (taxid = '0'  or len(taxid) < 9)

-- Change 1 --

INSERT transactions
(Points, CardNumber, TranCode, Ratio, TranCount, TipNumber)
SELECT ROUND(Amount/4,0,0) AS Points, CardNumber, '37' AS trancode, '-1' AS ratio, TranCount, tipnumber
FROM  transactionwork WHERE(TranCode = 'ret')

INSERT INTO transactions
(Points, CardNumber, TranCode, Ratio, TranCount, TipNumber)
SELECT ROUND(Amount/4,0,0)AS Points, CardNumber, '67' AS trancode, '1' AS ratio, TranCount, tipnumber
FROM transactionwork WHERE (TranCode <> 'ret')

UPDATE Transactions SET AccountType = 'Debit'

UPDATE Transactions SET TransDate = @monthendingdate

GO


