USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 10/25/2010 14:35:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spQuarterlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spQuarterlyStatementFile]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[spQuarterlyStatementFile]    Script Date: 10/25/2010 14:35:12 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spQuarterlyStatementFile] @StartDateParm varchar(10), @EndDateParm varchar(10)
AS 

/*******************************************************************************/
/* Changes: (1) add trancode XP to calculation for points subtracted           */
/*******************************************************************************/
-- SEB 10/2013 changed PointsBegin to a calculation.

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:000')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006


Declare @MonthBucket char(10), @SQLUpdate nvarchar(1000), @monthbegin char(2)
set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
delete from Quarterly_Statement_File

insert into Quarterly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city,state,zip)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state) , zipcode
from customer

update Quarterly_Statement_File set zip =  left(Zip,5) + '-' + right(zip,4) where len(zip) = 9
update Quarterly_Statement_File set zip =  left(Zip,5)  where len(zip) < 9

/* Load the statmement file with purchases          */
update Quarterly_Statement_File
set pointspurchasedCR=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='63')

update Quarterly_Statement_File
set pointspurchasedDB=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='67')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='67')

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonus=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like'B%' or trancode = 'MB 'or trancode = 'NW'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode = 'MB' or trancode = 'NW'))


/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set pointsbonusMN=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode in('F0','F9','G0','G9','H0','H9')))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode in('F0','F9','G0','G9','H0','H9')))

/* Load the statmement file with bonuses            */
update Quarterly_Statement_File
set purchasedpoints=(select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode in('PP')))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like 'B%' or trancode in('PP')))

/* Load the statmement file with plus adjustments    */
update Quarterly_Statement_File
set pointsadded=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='IE')

/* Load the statmement file with total point increases */
update Quarterly_Statement_File
set pointsincreased=pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + pointsbonusmn + purchasedpoints

/* Load the statmement file with redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with returns            */
update Quarterly_Statement_File
set pointsreturnedCR=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='33')

update Quarterly_Statement_File
set pointsreturnedDB=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='37')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode='37')

-- CHANGE (1) add trancode XP
/* Load the statmement file with minus adjustments    */
update Quarterly_Statement_File
set pointssubtracted=(select sum(points) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in('DE','XP'))
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode in('DE','XP'))

/* Load the statmement file with decrease redemptions          */
update Quarterly_Statement_File
set pointsredeemed=pointsredeemed - (select sum(points*ratio) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode ='DR')

/* Load the statmement file with Manual redemptions          */
update Quarterly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode = 'IR')
where exists(select * from history where tipnumber=Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode = 'IR')

/* Load the statmement file with total point decreases */
update Quarterly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedCR +  pointsreturnedDB + pointssubtracted 

/* Load the statmement file with the Beginning balance for the Month */
/*update Quarterly_Statement_File
set pointsbegin=(select BeginningPoints from QTRBeginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber) */

-- SEB 10/2013 replaced below with calculation
--set @SQLUpdate=N'update Quarterly_Statement_File set pointsbegin=(select ' + Quotename(@MonthBucket) + N'from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)
--where exists(select * from Beginning_Balance_Table where tipnumber=Quarterly_Statement_File.tipnumber)'
--exec sp_executesql @SQLUpdate
update Quarterly_Statement_File 
set pointsbegin = (select isnull(SUM(points * ratio),0)
					from history 
					where Tipnumber = Quarterly_Statement_File.TIPNUMBER
					and histdate < @Startdate)


/* Load the statmement file with beginning points */
update Quarterly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

update quarterly_statement_file set expiringpoints = d.dim_ExpiringPointsProjection_PointsToExpireThisPeriod
from quarterly_statement_file m join [rewardsnow].dbo.ExpiringPointsProjection d on m.tipnumber = D.sid_ExpiringPointsProjection_Tipnumber
where dim_ExpiringPointsProjection_PointsToExpireThisPeriod > '0'


GO


