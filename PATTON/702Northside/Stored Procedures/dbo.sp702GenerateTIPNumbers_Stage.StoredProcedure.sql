USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[sp702GenerateTIPNumbers_Stage]    Script Date: 04/15/2010 10:18:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp702GenerateTIPNumbers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp702GenerateTIPNumbers_Stage]
GO

USE [702NorthSide]
GO

/****** Object:  StoredProcedure [dbo].[sp702GenerateTIPNumbers_Stage]    Script Date: 04/15/2010 10:18:27 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[sp702GenerateTIPNumbers_Stage]
AS 
update icc 
set Tipnumber = afs.tipnumber
from input_creditcustomer icc join affiliat_stage afs on icc.acctid = afs.acctid
where(icc.tipnumber is null or icc.tipnumber = ' ')

update icc 
set Tipnumber = afs.tipnumber
from input_creditcustomer icc join affiliat_stage afs on icc.ssn = afs.custid
where(icc.tipnumber is null or icc.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct ssn as custid, tipnumber	
from input_creditcustomer

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 702, @LastTipUsed output
select @LastTipUsed as LastTipUsed
set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 702000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 702, @newnum

update icc 
set Tipnumber = gt.tipnumber
from input_creditcustomer icc join GenTip gt on icc.ssn = gt.custid
where(icc.tipnumber is null or icc.tipnumber = ' ')

/************************************************************************/
/*			Set Debit Customers Tipnumbers                              */
/************************************************************************/ 

update idc 
set Tipnumber = afs.tipnumber
from input_debitcustomer idc join affiliat_stage afs on idc.CardNumber = afs.acctid
where(idc.tipnumber is null or idc.tipnumber = ' ')

update idc 
set Tipnumber = afs.tipnumber
from input_debitcustomer idc join affiliat_stage afs on idc.ssn = afs.custid
where(idc.tipnumber is null or idc.tipnumber = ' ')

DELETE FROM GenTip

insert into gentip (custid, tipnumber)
select   distinct ssn as custid, tipnumber	
from input_debitcustomer

exec rewardsnow.dbo.spGetLastTipNumberUsed 702, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 

if @newnum is null or @newnum = 0
	begin
	set @newnum= 702000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '


exec RewardsNOW.dbo.spPutLastTipNumberUsed 702, @newnum

update idc 
set Tipnumber = gt.tipnumber
from input_debitcustomer idc join GenTip gt on idc.ssn = gt.custid
where(idc.tipnumber is null or idc.tipnumber = ' ')


GO
