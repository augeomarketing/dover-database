CREATE PROCEDURE spLoadDateAdded @EndDate varchar(10)
AS 

--declare @EndDate varchar(10)
--set @EndDate = '02/28/2009'

UPDATE Input_CreditCustomer SET DateAdded =(SELECT dateadded FROM CUSTOMER_stage
WHERE customer_stage.tipnumber = Input_CreditCustomer.tipnumber)
WHERE EXISTS(SELECT * FROM customer_stage
WHERE customer_stage.tipnumber = Input_CreditCustomer.tipnumber)

UPDATE    input_creditcustomer set DateAdded = CONVERT(datetime, @EndDate)
WHERE     (DateAdded IS NULL)

UPDATE Input_DebitCustomer SET DateAdded =(SELECT dateadded FROM CUSTOMER_stage
WHERE customer_stage.tipnumber = Input_DebitCustomer.tipnumber)
WHERE EXISTS(SELECT * FROM customer_stage
WHERE customer_stage.tipnumber = Input_DebitCustomer.tipnumber)

UPDATE    input_debitcustomer set DateAdded = CONVERT(datetime, @EndDate)
WHERE     (DateAdded IS NULL)

go