USE [52V]
GO

/****** Object:  View [dbo].[vwHistory]    Script Date: 06/13/2013 08:36:33 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwHistory]'))
DROP VIEW [dbo].[vwHistory]
GO

USE [52V]
GO

/****** Object:  View [dbo].[vwHistory]    Script Date: 06/13/2013 08:36:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


			 create view [dbo].[vwHistory]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from [52V].dbo.history


GO


