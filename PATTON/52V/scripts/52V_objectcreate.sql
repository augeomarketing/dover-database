USE [52V]
GO
--/****** Object:  User [RNIColdfusion]    Script Date: 07/20/2011 10:18:42 ******/
--CREATE USER [RNIColdfusion] FOR LOGIN [RNIColdfusion] WITH DEFAULT_SCHEMA=[dbo]
--GO
/****** Object:  Table [dbo].[Results]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [smalldatetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reg]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reg](
	[tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reconcilliationout]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reconcilliationout](
	[field1] [varchar](1000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsBonus2PT] [numeric](18, 0) NULL,
	[PointsBonus3PT] [numeric](18, 0) NULL,
	[PointsBonusMER] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsReturned2PT] [numeric](18, 0) NULL,
	[PointsReturned3PT] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LoyaltyNumber] [char](25) NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFile]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Audit_ErrorFile](
	[Tipnumber] [nchar](15) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Missingnames]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Missingnames](
	[ssn] [nvarchar](9) NULL,
	[bank] [nvarchar](4) NULL,
	[agent] [nvarchar](4) NULL,
	[ddanum] [nvarchar](11) NULL,
	[acctnum] [nvarchar](25) NULL,
	[addr1] [nvarchar](40) NULL,
	[citystate] [nvarchar](40) NULL,
	[zip] [nvarchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KroegerReconcilliationData]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KroegerReconcilliationData](
	[ProcessDate] [char](10) NULL,
	[ValueInPoint1] [int] NULL,
	[ValueInPoint2] [int] NULL,
	[ValueInPoint3] [int] NULL,
	[ValueOutPoint1] [int] NULL,
	[ValueOutPoint2] [int] NULL,
	[ValueOutPoint3] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_HistoryDeleted_tipnumber_Acctid_Histdate_Trancode_Points_DateDeleted] ON [dbo].[HistoryDeleted] 
(
	[TipNumber] ASC,
	[AcctID] ASC,
	[HistDate] ASC,
	[TranCode] ASC,
	[Points] ASC,
	[DateDeleted] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HISTORY]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORY](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](5, 0) NULL
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [HISTTIP] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_History_tipnumber_Acctid_Histdate_Trancode_Points] ON [dbo].[HISTORY] 
(
	[TIPNUMBER] ASC,
	[ACCTID] ASC,
	[HISTDATE] ASC,
	[TRANCODE] ASC,
	[POINTS] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HistExp]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistExp](
	[TIPNumber] [varchar](15) NOT NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[Zipcode] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[TotEarned] [int] NULL,
	[RunRedemed] [int] NULL,
	[Available] [int] NULL,
	[LastStmtDT] [smalldatetime] NULL,
	[AcctID] [varchar](25) NULL,
	[DateAdded] [smalldatetime] NULL,
	[CardType] [varchar](20) NULL,
	[Descriptio] [nvarchar](40) NULL,
	[TranCode] [nvarchar](2) NULL,
	[Points] [numeric](18, 0) NULL,
	[HistDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerDeleted]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerDeleted](
	[TIPNumber] [varchar](15) NULL,
	[TIPFirst] [varchar](3) NULL,
	[TIPLast] [varchar](12) NULL,
	[AcctName1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[AcctName3] [varchar](40) NULL,
	[AcctName4] [varchar](40) NULL,
	[AcctName5] [varchar](40) NULL,
	[AcctName6] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[Address4] [varchar](40) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[LastName] [varchar](40) NULL,
	[Status] [varchar](1) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HomePhone] [varchar](20) NULL,
	[WorkPhone] [varchar](20) NULL,
	[RunBalance] [numeric](10, 0) NULL,
	[RunRedeemed] [numeric](10, 0) NULL,
	[RunAvailable] [numeric](10, 0) NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[DateAdded] [datetime] NULL,
	[Notes] [text] NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BusinessFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_CustomerDeleted_tipnumber] ON [dbo].[CustomerDeleted] 
(
	[TIPNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_Customer_Closed_tipnumber] ON [dbo].[Customer_Closed] 
(
	[TipNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY NONCLUSTERED 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [CUSTTIP] ON [dbo].[CUSTOMER] 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Current_Month_Activity](
	[Tipnumber] [nchar](15) NOT NULL,
	[EndingPoints] [int] NULL,
	[Increases] [int] NULL,
	[Decreases] [int] NULL,
	[AdjustedEndingPoints] [int] NULL,
 CONSTRAINT [PK_Current_Month_Activity] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ClientCode] [varchar](15) NULL,
	[ClientName] [varchar](50) NULL,
	[Description] [varchar](100) NULL,
	[TipFirst] [varchar](3) NOT NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](20) NULL,
	[State] [varchar](20) NULL,
	[Zipcode] [varchar](15) NULL,
	[Phone1] [varchar](30) NULL,
	[Phone2] [varchar](30) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[DateJoined] [datetime] NULL,
	[RNProgramName] [varchar](30) NULL,
	[TermsConditions] [text] NULL,
	[PointsUpdatedDT] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[TravelFlag] [varchar](1) NULL,
	[MerchandiseFlag] [varchar](1) NULL,
	[TravelIncMinPoints] [numeric](18, 0) NULL,
	[MerchandiseBonusMinPoints] [numeric](18, 0) NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[PointExpirationYears] [int] NULL,
	[ClientID] [varchar](50) NULL,
	[Pass] [varchar](30) NULL,
	[ServerName] [varchar](40) NULL,
	[DbName] [varchar](40) NULL,
	[UserName] [varchar](40) NULL,
	[Password] [varchar](40) NULL,
	[PointsExpire] [varchar](20) NULL,
	[LastTipNumberUsed] [varchar](50) NULL,
	[PointsExpireFrequencyCd] [nvarchar](2) NULL,
	[ClosedMonths] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CertificateDeleted]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [numeric](18, 0) NULL,
	[RedeemedNow] [numeric](18, 0) NOT NULL,
	[RunAvailable] [numeric](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificate](
	[CertificateID] [int] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [int] NULL,
	[RedeemedNow] [int] NULL,
	[RunAvailable] [int] NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NOT NULL,
	[MonthBeg1] [int] NULL,
	[MonthBeg2] [int] NULL,
	[MonthBeg3] [int] NULL,
	[MonthBeg4] [int] NULL,
	[MonthBeg5] [int] NULL,
	[MonthBeg6] [int] NULL,
	[MonthBeg7] [int] NULL,
	[MonthBeg8] [int] NULL,
	[MonthBeg9] [int] NULL,
	[MonthBeg10] [int] NULL,
	[MonthBeg11] [int] NULL,
	[MonthBeg12] [int] NULL,
 CONSTRAINT [PK_Beginning_Balance_Table] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Award]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Award](
	[AwardCode] [int] NOT NULL,
	[CatalogCode] [varchar](50) NULL,
	[AwardName] [varchar](300) NOT NULL,
	[AwardDesc] [varchar](1000) NULL,
	[ExtraAwardsPoints] [int] NULL,
	[AwardPic] [image] NULL,
	[Standard] [char](1) NOT NULL,
	[Special] [char](1) NOT NULL,
	[Business] [char](1) NOT NULL,
	[Television] [char](1) NOT NULL,
	[ClientAwardPoints] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](20) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_AffiliatDeleted_tipnumber_AcctID] ON [dbo].[AffiliatDeleted] 
(
	[TipNumber] ASC,
	[AcctID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AFFILIAT]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFILIAT](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](20) NULL,
 CONSTRAINT [PK_AFFILIAT] PRIMARY KEY NONCLUSTERED 
(
	[ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [AFFTIP] ON [dbo].[AFFILIAT] 
(
	[TIPNUMBER] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AcctType]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[testoutput]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[testoutput](
	[field1] [varchar](1000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StmtExp]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StmtExp](
	[TIPNUMBER] [varchar](15) NULL,
	[STMTDATE] [smalldatetime] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[ZIPCODE] [varchar](15) NULL,
	[BEGBAL] [numeric](10, 0) NULL,
	[ENDBAL] [numeric](10, 0) NULL,
	[CCPURCHASE] [numeric](10, 0) NULL,
	[BONUS] [numeric](10, 0) NULL,
	[PNTADD] [numeric](10, 0) NULL,
	[PNTINCRS] [numeric](10, 0) NULL,
	[REDEEMED] [numeric](10, 0) NULL,
	[PNTRETRN] [numeric](10, 0) NULL,
	[PNTSUBTR] [numeric](10, 0) NULL,
	[PNTDECRS] [numeric](10, 0) NULL,
	[LastFour] [char](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 07/20/2011 10:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[Status] [char](1) NOT NULL,
	[StatusDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[vwHistorydeleted]    Script Date: 07/20/2011 10:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistorydeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, datedeleted
			 from [52JKroegerPersonalFinance].dbo.historydeleted
GO
/****** Object:  View [dbo].[vwHistory]    Script Date: 07/20/2011 10:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistory]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from [52JKroegerPersonalFinance].dbo.history
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 07/20/2011 10:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [52JKroegerPersonalFinance].dbo.Customerdeleted
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 07/20/2011 10:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [52JKroegerPersonalFinance].dbo.customer
GO
/****** Object:  View [dbo].[vwAffiliatdeleted]    Script Date: 07/20/2011 10:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliatdeleted]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
			    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID, datedeleted
			  from [52JKroegerPersonalFinance].dbo.affiliatdeleted
GO
/****** Object:  View [dbo].[vwAffiliat]    Script Date: 07/20/2011 10:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwAffiliat]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TipNumber, LastName, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, YTDEarned, CustID
			 from [52JKroegerPersonalFinance].dbo.affiliat
GO
/****** Object:  StoredProcedure [dbo].[usp_ralphs_createaudit]    Script Date: 07/20/2011 10:18:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_ralphs_createaudit]

as

truncate table MetavanteWork.dbo.ralphs_auditwrk

Declare @string		char(200)
Declare @record		char(2)
Declare @client		char(4)
Declare	@transmit	char(8)
Declare @date		datetime
Declare @datejul	char(7)
Declare @Year		char(4)

Declare @Month		char(2)
Declare @Day		char(2)
Declare @reccount	char(9)

Set	@record =	'01'
Set	@client =	'META'
Set	@transmit =	' GRPPNTS'
Set @date =		GETDATE()
Set @Year =		YEAR(@date)
Set @Month =	(Select Right('00' + cast(MONTH(@date) as varchar(3)), 2))
Set @Day =		(Select Right('00' + cast(DAY(@date) as varchar(3)), 2))
Set @string =	(Select @record + @client + @transmit + @Year + @Month + @Day)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select @string

Create table #tmp (acctid char(19), loyalid char(19), trandate char(7), signa char(1), pointsearned char(13), signb char(1), pointsadjusted char(13))

Set @record =	'02'
set @date =		(SELECT cast((SUBSTRING(BatchDate,5,2) + '/' + SUBSTRING(BatchDate,7,2) + '/' + SUBSTRING(BatchDate,1,4)) as datetime)
				FROM MetavanteWork.dbo.Kroger_Daily_Batch_Header)
Set @datejul =	CAST(YEAR(@date) AS CHAR(4)) + RIGHT('000' + CAST(DATEPART(dy, @date) AS varchar(3)),3)


insert into #tmp
(acctid, loyalid, trandate, signa, signb)
Select distinct REPLICATE(' ',19), c.Misc2, @datejul,'+','+'
from HISTORY h join CUSTOMER c on h.TIPNUMBER = c.TIPNUMBER
where YEAR(histdate) = YEAR(@date) and MONTH(histdate) = MONTH(@date) and DAY(histdate) = DAY(@date)

update #tmp set pointsearned = (Select isnull(SUM(points*ratio),0) from HISTORY h join CUSTOMER c on h.TIPNUMBER = c.TIPNUMBER
where c.Misc2 = #tmp.loyalid
	and YEAR(histdate) = YEAR(@date) and MONTH(histdate) = MONTH(@date) and DAY(histdate) = DAY(@date)
	and TRANCODE in ('37','67','G3','G8'))
	
update #tmp set pointsadjusted = (Select isnull(SUM(points*ratio),0) from HISTORY h join CUSTOMER c on h.TIPNUMBER = c.TIPNUMBER
where c.Misc2 = #tmp.loyalid
	and YEAR(histdate) = YEAR(@date) and MONTH(histdate) = MONTH(@date) and DAY(histdate) = DAY(@date)
	and TRANCODE in ('IE','DE'))
	
update #tmp set signa = '-', pointsearned = pointsearned*-1 where pointsearned < 0
update #tmp set signa = '-', pointsadjusted = pointsadjusted*-1 where pointsadjusted < 0
update #tmp set pointsearned = RIGHT(REPLICATE('0',11)+cast(rtrim(pointsearned) as varchar(13))+'00',13), 
				pointsadjusted = RIGHT(REPLICATE('0',11)+cast(rtrim(pointsadjusted) as varchar(13))+'00',13)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select cast(@record+acctid+loyalid+REPLICATE(' ',9)+trandate+signa+pointsearned+SIGNB+pointsadjusted as char(200)) from #tmp

Drop table #tmp

Set @record =	'05'
Set @reccount =	(Select COUNT(col001) from MetavanteWork.dbo.ralphs_auditwrk where LEFT(col001,2) = '02')
Set @reccount =	RIGHT('000000000'+cast(rtrim(@reccount) as varchar(9)),9)
Set @string =	(Select @record + @client + @transmit + @reccount)

Insert into MetavanteWork.dbo.ralphs_auditwrk
Select @string
GO
/****** Object:  StoredProcedure [dbo].[spKroegerGenerateReconcilliationReport]    Script Date: 07/20/2011 10:18:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spKroegerGenerateReconcilliationReport]
	-- Add the parameters for the stored procedure here
	@Datein char(10)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table Reconcilliationout

	declare @datafieldIn varchar(1000), @datafieldOut varchar(1000)

	set @datafieldIn = 'Incoming Data Validation
		File Name Short Description Total Received
		CBCR1G9M 1pt Net Spend $' + (select cast(ValueInPoint1 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards2P 2pt Kroger Bonus $' + (select cast(ValueInPoint2 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards2P 3pt Kroger Bonus $' + (select cast(ValueInPoint3 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein)

	set @datafieldOut = 'Outbound Data Validation
		File Name Short Description Total Processed
		CBCR1G9M 1pt Net Spend Points ' + (select cast(ValueOutPoint1 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards2P 2pt Kroger Bonus Points ' + (select cast(ValueOutPoint2 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein) + '
		GPM-ToRewards3P 3pt Kroger Bonus Points (2x the Input) ' + (select cast(ValueOutPoint3 as char(10)) from dbo.KroegerReconcilliationData where Processdate = @Datein)

	insert into Reconcilliationout 
	values ('For ' + @datein) 
	
	insert into Reconcilliationout 
	values (@datafieldIn) 
	
	insert into Reconcilliationout 
	values (@datafieldOut)

END
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBegin]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsEnd]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsPurchasedCR]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBonusCR]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusCR]  DEFAULT ((0)) FOR [PointsBonusCR]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsAdded]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsPurchasedDB]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBonusDB]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusDB]  DEFAULT ((0)) FOR [PointsBonusDB]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBonus2PT]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus2PT]  DEFAULT ((0)) FOR [PointsBonus2PT]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBonus3PT]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus3PT]  DEFAULT ((0)) FOR [PointsBonus3PT]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsBonusMER]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMER]  DEFAULT ((0)) FOR [PointsBonusMER]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsIncreased]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsRedeemed]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsReturnedCR]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsSubtracted]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsReturnedDB]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsReturned2PT]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturned2PT]  DEFAULT ((0)) FOR [PointsReturned2PT]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsReturned3PT]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturned3PT]  DEFAULT ((0)) FOR [PointsReturned3PT]
GO
/****** Object:  Default [DF_Monthly_Statement_File_PointsDecreased]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO
/****** Object:  Default [DF_KroegerReconcilliationData_ValueInPoint1]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint1]  DEFAULT (0) FOR [ValueInPoint1]
GO
/****** Object:  Default [DF_KroegerReconcilliationData_ValueInPoint2]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint2]  DEFAULT (0) FOR [ValueInPoint2]
GO
/****** Object:  Default [DF_KroegerReconcilliationData_ValueInPoint3]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueInPoint3]  DEFAULT (0) FOR [ValueInPoint3]
GO
/****** Object:  Default [DF_KroegerReconcilliationData_ValueOutPoint1]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint1]  DEFAULT (0) FOR [ValueOutPoint1]
GO
/****** Object:  Default [DF_KroegerReconcilliationData_ValueOutPoint2]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint2]  DEFAULT (0) FOR [ValueOutPoint2]
GO
/****** Object:  Default [DF_KroegerReconcilliationData_ValueOutPoint3]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[KroegerReconcilliationData] ADD  CONSTRAINT [DF_KroegerReconcilliationData_ValueOutPoint3]  DEFAULT (0) FOR [ValueOutPoint3]
GO
/****** Object:  Default [DF__HISTORYTI__Overa__07020F21]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[HISTORYTIP] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__HistoryDe__Overa__0519C6AF]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[HistoryDeleted] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__HISTORY__Overage__03317E3D]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[HISTORY] ADD  DEFAULT (0) FOR [Overage]
GO
/****** Object:  Default [DF__AffiliatD__YTDEa__7A9C383C]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[AffiliatDeleted] ADD  DEFAULT (0) FOR [YTDEarned]
GO
/****** Object:  Default [DF__AFFILIAT__YTDEar__77BFCB91]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [YTDEarned]
GO
/****** Object:  Default [DF__AFFILIAT__CustID__78B3EFCA]    Script Date: 07/20/2011 10:18:44 ******/
ALTER TABLE [dbo].[AFFILIAT] ADD  DEFAULT (0) FOR [CustID]
GO
