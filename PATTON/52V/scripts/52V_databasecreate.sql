USE [master]
GO

/****** Object:  Database [52V]    Script Date: 07/20/2011 10:20:49 ******/
--CREATE DATABASE [52V] ON  PRIMARY 
--( NAME = N'52V', FILENAME = N'C:\SQLData_RN\52V.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 
--( NAME = N'52V_log', FILENAME = N'C:\SQLData_RN\52V_log.ldf' , SIZE = 5120KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
--GO

ALTER DATABASE [52V] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [52V].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [52V] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [52V] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [52V] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [52V] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [52V] SET ARITHABORT OFF 
GO

ALTER DATABASE [52V] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [52V] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [52V] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [52V] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [52V] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [52V] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [52V] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [52V] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [52V] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [52V] SET RECURSIVE_TRIGGERS OFF 
GO

--ALTER DATABASE [52V] SET  DISABLE_BROKER 
--GO

ALTER DATABASE [52V] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [52V] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [52V] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [52V] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [52V] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [52V] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [52V] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [52V] SET  READ_WRITE 
GO

ALTER DATABASE [52V] SET RECOVERY SIMPLE 
GO

ALTER DATABASE [52V] SET  MULTI_USER 
GO

ALTER DATABASE [52V] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [52V] SET DB_CHAINING OFF 
GO


