USE [546WashingtonTrustCommercial]
GO
/****** Object:  StoredProcedure [dbo].[sp548CreateSweepRedemptionFile]    Script Date: 09/25/2009 11:36:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp548CreateSweepRedemptionFile]
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10),
	@EndDateParm char(10),
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

	declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)


	set @DBName=(SELECT  rtrim(DBName) from DBProcessInfo
					where DBNumber=@TipFirst)

	set @SQLSelect=N'drop table ' + QuoteName(@DBName) + N' .dbo.sweepfile '
	exec sp_executesql @SQLSelect

	set @SQLInsert = N'Select A.Tipnumber, substring(A.Description, 21, 15) AS School, cast(round((A.points/100),2) as numeric (9,2) ) as Dollars, b.Acctname1
	into ' + QuoteName(@DBName) + N' .dbo.sweepfile
	from ' + QuoteName(@DBName) + N' .dbo.history a JOIN ' + QuoteName(@DBName) + N' .dbo.customer b on a.tipnumber = b.tipnumber
	where histdate >= @Startdate and histdate<= @Enddate and trancode =''RG'' ' 
	exec sp_executesql @SQLInsert, N'@StartDate DateTime, @EndDate DateTime', @StartDate=@StartDate, @EndDate=@EndDate

END
GO
