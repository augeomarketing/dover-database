USE [546WashingtonTrustCommercial]
GO
/****** Object:  Table [dbo].[546Bonuserror]    Script Date: 09/25/2009 11:36:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[546Bonuserror](
	[Tipnumber] [nvarchar](255) NULL,
	[CRDHLR_NBR] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
GO
