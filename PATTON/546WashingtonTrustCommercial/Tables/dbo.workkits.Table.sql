USE [546WashingtonTrustCommercial]
GO
/****** Object:  Table [dbo].[workkits]    Script Date: 09/25/2009 11:36:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[workkits](
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[workkits] ADD [bin] [varchar](8) NULL
GO
SET ANSI_PADDING OFF
GO
