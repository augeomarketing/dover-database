use [219DeanBank]
GO

if exists(select * from dbo.sysobjects where [name] = 'vw_histpoints' and xtype = 'V')	
	drop view dbo.vw_histpoints
GO

Create view dbo.vw_histpoints 

as 

select tipnumber, sum(points * ratio) as points 
from dbo.history_stage 
where secid = 'NEW' 
group by tipnumber
