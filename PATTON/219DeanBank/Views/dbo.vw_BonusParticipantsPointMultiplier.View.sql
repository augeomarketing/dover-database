use [219DeanBank]
GO

if exists(select * from dbo.sysobjects where name = 'vw_BonusParticipantsPointMultiplier' and xtype = 'v')
	drop view dbo.vw_BonusParticipantsPointMultiplier
GO

create view dbo.vw_BonusParticipantsPointMultiplier
as

select dim_BonusParticipants_TipNumber, dim_BonusProgram_PointMultiplier, dim_BonusProgram_EffectiveDate, dim_BonusProgram_ExpirationDate
from dbo.BonusParticipants bp join dbo.BonusProgram pgm
	on bp.sid_BonusType_Id = pgm.sid_BonusType_Id
