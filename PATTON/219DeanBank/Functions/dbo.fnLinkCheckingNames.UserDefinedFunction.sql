use [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[fnLinkCheckingNames]') AND xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION [dbo].[fnLinkCheckingNames]
GO


SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[fnLinkCheckingNames] (@Checking nvarchar(25))

RETURNS @LinkedNames table(Checking nvarchar(25) primary key, Name1 nvarchar(35), PrimaryLast4SSN nvarchar(4), Name2 nvarchar(35), 
						Name3 nvarchar(35), Name4 nvarchar(35), Name5 nvarchar(35),
						Name6 nvarchar(35) )

as

BEGIN

	declare @names table(id bigint identity (1,1) primary key, AcctName nvarchar(35), Last4SSN nvarchar(4))

	insert into @names
	select distinct customername, customerlast4ssn
	from dbo.impcustomer
	where PrimaryCustomerDDA = @Checking
	and PrimaryFlag = 'Y'

	-- Get distinct names from NAME1 column for rows that match the @Checking parm
	insert into @names
	select distinct CustomerName, CustomerLast4SSN
	from dbo.impCustomer
	where PrimaryCustomerDDA = @Checking and
		CustomerName not in (select AcctName from @Names)


	-- Flatten out the @names table by returning a single row of up to 6 names 
	insert into @LinkedNames
	select	@checking as Checking, (select AcctName from @names where id=1) as Name1, (Select Last4SSN from @names where id=1) as PrimaryLast4SSN,
			(select AcctName from @names where id=2) as Name2,
			(select AcctName from @names where id=3) as Name3,
			(select AcctName from @names where id=4) as Name4,
			(select AcctName from @names where id=5) as Name5,
			(select AcctName from @names where id=6) as Name6

	return
END