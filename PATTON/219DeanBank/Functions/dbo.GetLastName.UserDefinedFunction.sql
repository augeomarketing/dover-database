DROP FUNCTION [dbo].[GetLastName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetLastName]
	(@name	varchar(40))
returns varchar(40)

as

BEGIN
	declare @NameReversed	varchar(40)
	declare @lname			varchar(40)
	declare @startpos		int

	set @namereversed = reverse(@name)

	set @startpos = charindex(' ', @namereversed, 1)

	set @lname = right(@namereversed, len(@namereversed) - @startpos)

	set @lname = reverse(@lname)

--print @name
--print @namereversed
--print @startpos
--print @lname

	return @lname
END
GO
