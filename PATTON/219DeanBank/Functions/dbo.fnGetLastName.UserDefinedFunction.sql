use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'fnGetLastName' and xtype = 'FN')
	DROP FUNCTION [dbo].[fnGetLastName]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnGetLastName]
	(@name	varchar(40))
returns varchar(40)

as

BEGIN
	declare @NameReversed	varchar(40)
	declare @lname			varchar(40)
	declare @startpos		int

/* Dean Bank sends names as Last Name first  */
	set @name = ltrim(rtrim(@name))
	set @startpos = charindex(' ', @name, 1)
	set @lname = substring(@name, 1, @startpos -1)

	set @lname = replace(@lname, char(39), '') -- take out hyphens in last name

	return @lname
END
GO
