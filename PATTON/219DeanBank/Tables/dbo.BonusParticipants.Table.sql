USE [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusParticipants_BonusType]') AND type = 'F')
ALTER TABLE [dbo].[BonusParticipants] DROP CONSTRAINT [FK_BonusParticipants_BonusType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusParticipants_dim_BonusParticipants_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusParticipants] DROP CONSTRAINT [DF_BonusParticipants_dim_BonusParticipants_DateAdded]
END

GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[BonusParticipants]    Script Date: 03/06/2009 17:01:00 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusParticipants]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BonusParticipants]
GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[BonusParticipants]    Script Date: 03/06/2009 17:01:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusParticipants]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[BonusParticipants](
	[sid_BonusType_Id] [int] NOT NULL,
	[dim_BonusParticipants_TipNumber] [varchar](15) NOT NULL,
	[dim_BonusParticipants_EffectiveDate] [datetime] NULL,
	[dim_BonusParticipants_ExpirationDate] [datetime] NULL,
	[dim_BonusParticipants_DateAdded] [datetime] NULL,
	[dim_BonusParticipants_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusParticipants] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC,
	[dim_BonusParticipants_TipNumber] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusParticipants_BonusType]') AND type = 'F')
ALTER TABLE [dbo].[BonusParticipants]  WITH CHECK ADD  CONSTRAINT [FK_BonusParticipants_BonusType] FOREIGN KEY([sid_BonusType_Id])
REFERENCES [dbo].[BonusType] ([sid_BonusType_Id])
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusParticipants_BonusType]') AND type = 'F')
ALTER TABLE [dbo].[BonusParticipants] CHECK CONSTRAINT [FK_BonusParticipants_BonusType]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusParticipants_dim_BonusParticipants_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusParticipants] ADD  CONSTRAINT [DF_BonusParticipants_dim_BonusParticipants_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusParticipants_DateAdded]
END

GO


