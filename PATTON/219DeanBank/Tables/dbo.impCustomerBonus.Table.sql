use [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CK_impCustomerBonus_EndDate_GreaterThan_StartDate]') AND type = 'C')
ALTER TABLE [dbo].[impCustomerBonus] DROP CONSTRAINT [CK_impCustomerBonus_EndDate_GreaterThan_StartDate]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[impCustomerBonus]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[impCustomerBonus]
GO


CREATE TABLE [dbo].[impCustomerBonus](
	[sid_ImpCustomerBonus_Id] [bigint] IDENTITY(1,1) NOT NULL primary key,
	[CustomerNbr] [varchar](25) NOT NULL,
	[PrimaryCustomerDDA] varchar(25) not null
) ON [PRIMARY]

