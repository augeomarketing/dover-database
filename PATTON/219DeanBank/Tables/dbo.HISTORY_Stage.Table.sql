use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'history_stage' and xtype = 'U')
	DROP TABLE [dbo].[HISTORY_Stage]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NOT NULL,
	[TRANCODE] [varchar](2) NOT NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

CREATE NONCLUSTERED INDEX [ix_History_Stage_HistDate_TipNumber_TranCode_Points] ON [dbo].[HISTORY_Stage] 
(
	[HISTDATE] ASC,
	[TIPNUMBER] ASC,
	[TRANCODE] ASC,
	[POINTS] ASC,
	[Ratio] ASC
) ON [PRIMARY]