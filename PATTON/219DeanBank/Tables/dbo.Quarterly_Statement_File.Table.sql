USE [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBegin]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsEnd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsPurchasedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMN]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsIncreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsRedeemed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedCR]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsReturnedDB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsSubtracted]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsDecreased]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]
END

GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 07/15/2011 11:32:55 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 07/15/2011 11:32:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](15) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [int] NOT NULL,
	[PointsEnd] [int] NOT NULL,
	[PointsPurchasedCR] [int] NOT NULL,
	[PointsPurchasedDB] [int] NOT NULL,
	[PointsBonus] [int] NOT NULL,
	[PointsBonusMN] [int] NOT NULL,
	[PointsAdded] [int] NOT NULL,
	[PointsIncreased] [int] NOT NULL,
	[PointsRedeemed] [int] NOT NULL,
	[PointsReturnedCR] [int] NOT NULL,
	[PointsReturnedDB] [int] NOT NULL,
	[PointsSubtracted] [int] NOT NULL,
	[PointsDecreased] [int] NOT NULL,
	[acctid] [char](16) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL,
	[PointsExpire] [int] NOT NULL,
 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsExpire]  DEFAULT ((0)) FOR [PointsExpire]
GO

