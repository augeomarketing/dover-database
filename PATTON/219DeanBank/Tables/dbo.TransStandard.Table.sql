use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where name = 'transstandard' and xtype = 'U')
	DROP TABLE [dbo].[TransStandard]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransStandard](
	[TIP] [nvarchar](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NOT NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](20) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL,
 CONSTRAINT [PK_TransStandard] PRIMARY KEY CLUSTERED 
(
	[TIP] ASC,
	[TranDate] ASC,
	[TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
