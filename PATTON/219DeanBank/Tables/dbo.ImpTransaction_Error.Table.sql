ALTER TABLE [dbo].[ImpTransaction_Error] DROP CONSTRAINT [FK_ImpTransaction_Error_TranType]
GO
DROP TABLE [dbo].[ImpTransaction_Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImpTransaction_Error](
	[TxnId] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerAcctNbr] [varchar](25) NOT NULL,
	[TfrCustomerAcctNbr] [varchar](25) NULL,
	[DDANbr] [varchar](25) NOT NULL,
	[TxnCode] [varchar](3) NOT NULL,
	[TranCode] [nvarchar](2) NULL,
	[TxnAmount] [money] NOT NULL CONSTRAINT [DF_ImpTransaction_Error_TxnAmount]  DEFAULT (0),
	[TxnQuantity] [int] NOT NULL CONSTRAINT [DF_ImpTransaction_Error_TxnQuantity]  DEFAULT (0),
	[Points] [decimal](18, 0) NOT NULL CONSTRAINT [DF_ImpTransaction_Error_Points]  DEFAULT (0),
 CONSTRAINT [PK_ImpTransaction_Error] PRIMARY KEY CLUSTERED 
(
	[TxnId] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ImpTransaction_Error]  WITH NOCHECK ADD  CONSTRAINT [FK_ImpTransaction_Error_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[ImpTransaction_Error] CHECK CONSTRAINT [FK_ImpTransaction_Error_TranType]
GO
