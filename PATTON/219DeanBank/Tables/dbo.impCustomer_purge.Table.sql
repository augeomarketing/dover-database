USE [219DeanBank]
GO

/****** Object:  Table [dbo].[impCustomer_purge]    Script Date: 08/16/2011 16:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[impCustomer_purge]') AND type in (N'U'))
DROP TABLE [dbo].[impCustomer_purge]
GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[impCustomer_purge]    Script Date: 08/16/2011 16:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[impCustomer_purge](
	[CustomerRowKey] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerLastName] [varchar](40) NULL,
	[CustomerAddress] [varchar](40) NULL,
	[CustomerCity] [varchar](50) NULL,
	[CustomerState] [varchar](2) NULL,
	[CustomerZip] [varchar](15) NULL,
	[CustomerHomePhone] [varchar](10) NULL,
	[CustomerWorkPhone] [varchar](10) NULL,
	[CustomerStatusCd] [varchar](3) NOT NULL,
	[CustomerLast4SSN] [varchar](4) NULL,
	[CustomerName2] [varchar](40) NULL,
	[PrimaryCustomerDDA] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_ImpCustomer_purge] PRIMARY KEY CLUSTERED 
(
	[CustomerRowKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

