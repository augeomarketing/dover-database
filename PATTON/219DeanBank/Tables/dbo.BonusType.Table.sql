USE [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusType_dim_BonusType_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusType] DROP CONSTRAINT [DF_BonusType_dim_BonusType_DateAdded]
END

GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[BonusType]    Script Date: 03/06/2009 17:08:04 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BonusType]
GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[BonusType]    Script Date: 03/06/2009 17:08:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[BonusType](
	[sid_BonusType_Id] [int] IDENTITY(1,1) NOT NULL,
	[dim_BonusType_TranCode] [nvarchar](2) NOT NULL,
	[dim_BonusType_Description] [varchar](255) NOT NULL,
	[dim_BonusType_DateAdded] [datetime] NOT NULL,
	[dim_BonusType_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusType] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusType_dim_BonusType_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusType] ADD  CONSTRAINT [DF_BonusType_dim_BonusType_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusType_DateAdded]
END

GO

