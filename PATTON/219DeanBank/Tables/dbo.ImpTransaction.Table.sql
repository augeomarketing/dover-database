ALTER TABLE [dbo].[ImpTransaction] DROP CONSTRAINT [FK_ImpTransaction_TranType]
GO
DROP TABLE [dbo].[ImpTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImpTransaction](
	[TxnId] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerAcctNbr] [varchar](25) NOT NULL,
	[TfrCustomerAcctNbr] [varchar](25) NULL,
	[DDANbr] [varchar](25) NOT NULL,
	[TxnCode] [varchar](3) NOT NULL,
	[TranCode] [nvarchar](2) NULL,
	[TxnAmount] [money] NOT NULL CONSTRAINT [DF_ImpTransaction_TxnAmount]  DEFAULT (0),
	[TxnQuantity] [int] NOT NULL CONSTRAINT [DF_ImpTransaction_TxnQuantity]  DEFAULT (0),
	[Points] [decimal](18, 0) NOT NULL CONSTRAINT [DF_ImpTransaction_Points]  DEFAULT (0),
	[TIPNumber] [varchar](15) NULL,
 CONSTRAINT [PK_ImpTransaction] PRIMARY KEY CLUSTERED 
(
	[TxnId] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_ImpTransaction_Tip] ON [dbo].[ImpTransaction] 
(
	[TIPNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ImpTransaction_TxnCode_TranCode] ON [dbo].[ImpTransaction] 
(
	[TxnCode] ASC,
	[TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
ALTER TABLE [dbo].[ImpTransaction]  WITH NOCHECK ADD  CONSTRAINT [FK_ImpTransaction_TranType] FOREIGN KEY([TranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
ALTER TABLE [dbo].[ImpTransaction] CHECK CONSTRAINT [FK_ImpTransaction_TranType]
GO
