USE [219DeanBank]
GO

/****** Object:  Table [dbo].[web_Account]    Script Date: 10/21/2010 10:21:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[web_Account]') AND type in (N'U'))
DROP TABLE [dbo].[web_Account]
GO


/****** Object:  Table [dbo].[web_Account]    Script Date: 10/21/2010 10:21:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[web_Account](
	[TipNumber] [varchar](15) NOT NULL Primary Key,
	[LastName] [varchar](50) NOT NULL,
	[LastSix] [varchar](20) NOT NULL,
	[SSNLast4] [varchar](4) NULL,
	[RecNum] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [varchar](25) NULL,
	[MemberNumber] [varchar](25) NULL
) ON [PRIMARY]

GO


