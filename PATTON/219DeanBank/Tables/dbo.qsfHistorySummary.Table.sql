USE [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_qsfHistorySummary_Points]') AND type = 'D')
BEGIN
    ALTER TABLE [dbo].[qsfHistorySummary] DROP CONSTRAINT [DF_qsfHistorySummary_Points]
END

GO

/****** Object:  Table [dbo].[qsfHistorySummary]    Script Date: 07/15/2011 11:25:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[qsfHistorySummary]') AND type in (N'U'))
    DROP TABLE [dbo].[qsfHistorySummary]
GO




CREATE TABLE [dbo].[qsfHistorySummary](
	[TipNumber] [varchar](15) NOT NULL,
	[TranCode] [varchar](2) NOT NULL,
	[Points] [bigint] NOT NULL,
 CONSTRAINT [PK_qsfHistorySummary] PRIMARY KEY CLUSTERED 
(
	[TipNumber] ASC,
	[TranCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[qsfHistorySummary] ADD  CONSTRAINT [DF_qsfHistorySummary_Points]  DEFAULT (0) FOR [Points]
GO


