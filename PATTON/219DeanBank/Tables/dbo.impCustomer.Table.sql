use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'impCustomer' and xtype = 'U')
	DROP TABLE [dbo].[impCustomer]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[impCustomer](
	[CustomerRowKey] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerLastName] [varchar](40) NULL,
	[CustomerAddress] [varchar](40) NOT NULL,
	[CustomerCity] [varchar](50) NULL,
	[CustomerState] [varchar](2) NULL CONSTRAINT [DF_impCustomer_CustomerState]  DEFAULT (''),
	[CustomerZip] [varchar](15) NULL CONSTRAINT [DF_impCustomer_CustomerZip]  DEFAULT (''),
	[CustomerHomePhone] [varchar](10) NULL CONSTRAINT [DF_impCustomer_CustomerHomePhone]  DEFAULT (''),
	[CustomerWorkPhone] [varchar](10) NULL,
	[CustomerStatusCd] [varchar](3) NOT NULL,
	[CustomerLast4SSN] [varchar](4) NULL,
	[CustomerName2] [varchar](50) NULL,
	[CustomerName3] [varchar](50) NULL,
	[CustomerName4] [varchar](50) NULL,
	[CustomerName5] [varchar](50) NULL,
	[CustomerName6] [varchar](50) NULL,
	[PrimaryCustomerDDA] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NULL,
	[PrimaryFlag] [varchar](1) NOT NULL CONSTRAINT [DF_impCustomer_PrimaryFlag]  DEFAULT ('N'),
	[BusinessName] [varchar](40) NULL,
 CONSTRAINT [PK_ImpCustomer] PRIMARY KEY CLUSTERED 
(
	[CustomerRowKey] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

CREATE NONCLUSTERED INDEX [IX_impCustomer_PrimaryCustomerDDA] ON [dbo].[impCustomer] 
(
	[PrimaryCustomerDDA] ASC,
	[CustomerName] ASC,
	[CustomerName2] ASC,
	[CustomerName3] ASC,
	[CustomerName4] ASC,
	[CustomerName5] ASC,
	[CustomerName6] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_impCustomer_TipNumber] ON [dbo].[impCustomer] 
(
	[TipNumber] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]