USE [219DeanBank]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusProgram_BonusType]') AND type = 'F')
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [FK_BonusProgram_BonusType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_ExpirationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_ExpirationDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_PointMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_PointMultiplier]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]
END

GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 03/06/2009 17:01:53 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusProgram]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[BonusProgram]
GO

USE [219DeanBank]
GO

/****** Object:  Table [dbo].[BonusProgram]    Script Date: 03/06/2009 17:01:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[BonusProgram]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[BonusProgram](
	[sid_BonusType_Id] [int] NOT NULL,
	[dim_BonusProgram_EffectiveDate] [datetime] NOT NULL,
	[dim_BonusProgram_ExpirationDate] [datetime] NOT NULL,
	[dim_BonusProgram_PointMultiplier] [decimal](18, 2) NULL,
	[dim_BonusProgram_DateAdded] [datetime] NULL,
	[dim_BonusProgram_LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_BonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_BonusType_Id] ASC,
	[dim_BonusProgram_EffectiveDate] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusProgram_BonusType]') AND type = 'F')
ALTER TABLE [dbo].[BonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_BonusProgram_BonusType] FOREIGN KEY([sid_BonusType_Id])
REFERENCES [dbo].[BonusType] ([sid_BonusType_Id])
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_BonusProgram_BonusType]') AND type = 'F')
ALTER TABLE [dbo].[BonusProgram] CHECK CONSTRAINT [FK_BonusProgram_BonusType]
GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_ExpirationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_ExpirationDate]  DEFAULT ('12/31/9999') FOR [dim_BonusProgram_ExpirationDate]
END

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_PointMultiplier]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_PointMultiplier]  DEFAULT (1) FOR [dim_BonusProgram_PointMultiplier]
END

GO

IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[BonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]  DEFAULT (getdate()) FOR [dim_BonusProgram_DateAdded]
END

GO


