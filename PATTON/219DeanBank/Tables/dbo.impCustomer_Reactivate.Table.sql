USE [219DeanBank]
GO
/****** Object:  Table [dbo].[impCustomer_Reactivate]    Script Date: 03/17/2016 16:08:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impCustomer_Reactivate](
	[CustomerRowKey] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerLastName] [varchar](40) NULL,
	[CustomerAddress] [varchar](40) NOT NULL,
	[CustomerCity] [varchar](50) NULL,
	[CustomerState] [varchar](2) NULL,
	[CustomerZip] [varchar](15) NULL,
	[CustomerHomePhone] [varchar](10) NULL,
	[CustomerWorkPhone] [varchar](10) NULL,
	[CustomerStatusCd] [varchar](3) NOT NULL,
	[CustomerLast4SSN] [varchar](4) NULL,
	[CustomerName2] [varchar](50) NULL,
	[CustomerName3] [varchar](50) NULL,
	[CustomerName4] [varchar](50) NULL,
	[CustomerName5] [varchar](50) NULL,
	[CustomerName6] [varchar](50) NULL,
	[PrimaryCustomerDDA] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NULL,
	[PrimaryFlag] [varchar](1) NOT NULL,
	[BusinessName] [varchar](40) NULL,
 CONSTRAINT [PK_ImpCustomer__Reactivate] PRIMARY KEY CLUSTERED 
(
	[CustomerRowKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
