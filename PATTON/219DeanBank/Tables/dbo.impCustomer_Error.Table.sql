use [219DeanBank]
GO

DROP TABLE [dbo].[impCustomer_Error]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[impCustomer_Error](
	[CustomerRowKey] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerNbr] [varchar](25) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[CustomerLastName] [varchar](40) NULL,
	[CustomerAddress] [varchar](40) NOT NULL,
	[CustomerCity] [varchar](50) NOT NULL,
	[CustomerState] [varchar](2)  NULL CONSTRAINT [DF_impCustomer_Error_CustomerState]  DEFAULT (''),
	[CustomerZip] [varchar](15) NULL CONSTRAINT [DF_impCustomer_Error_CustomerZip]  DEFAULT (''),
	[CustomerHomePhone] [varchar](10) NOT NULL,
	[CustomerWorkPhone] [varchar](10) NULL,
	[CustomerStatusCd] [varchar](3) NOT NULL,
	[CustomerLast4SSN] [varchar](4) NULL,
	[CustomerName2] [varchar](40) NULL,
	[PrimaryCustomerDDA] [varchar](25) NOT NULL,
	[TipNumber] [varchar](15) NULL,
 CONSTRAINT [PK_ImpCustomer_Error] PRIMARY KEY CLUSTERED 
(
	[CustomerRowKey] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF