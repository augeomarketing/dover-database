

SET IDENTITY_INSERT [dbo].[webstatement] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[webstatement]
([sid_webstatement_id], [sid_webstatementtype_id], [dim_webstatement_dbfield], [dim_webstatement_tipfirst], [dim_webstatement_created], 
 [dim_webstatement_lastmodified], [dim_webstatement_active], [sid_reporttype_id])
SELECT 2892, 28, N'PNTVESDIA', N'219', '20110404 16:10:55.180', '20110404 16:10:55.180', 1, 1
COMMIT;


SET IDENTITY_INSERT [dbo].[webstatement] OFF;



insert into dbo.webstatementtrantype
(sid_webstatement_id, sid_trantype_trancode, dim_webstatementtrantype_active)
select 2400, '63', 1
union 
select 2401, trancode, 1 from dbo.trantype where (trancode like 'B%' or trancode like 'F%' or trancode like 'G%' or trancode like '0%') and trancode not in ('F0', 'F9', 'G0', 'G9')
union 
select 2402, trancode, 1 from dbo.trantype where trancode in ('IE', 'DR', 'XF')
union
select 2403, '33', 1
union
select 2404, trancode, 1 from dbo.trantype where trancode like 'R%'
union
select 2405, trancode, 1 from dbo.trantype where trancode in ('DE', 'IR', 'XP', 'EP')
union
select 2892, trancode, 1 from dbo.trantype where trancode in ('FO', 'F9', 'G0', 'G9')



