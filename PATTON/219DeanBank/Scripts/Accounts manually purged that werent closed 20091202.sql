select *
from dbo.customer
where status = 'C'



drop table #dupes
create table #dupes
    (tipnumber		varchar(15) primary key,
     misc4		varchar(25) )

insert into #dupes
select tipnumber, misc4
from dbo.customer
where misc4 in
(
'2000582110',
'2000675351',
'2000633194',
'2000633087',
'2000615712',
'2000678579',
'2000673109',
'2000636031',
'2000671368',
'2000666624',
'2000665220',
'2000660387',
'2000585972',
'2000641288',
'2000587010',
'2000589909',
'2000672606',
'2000238887',
'2000407144',
'2000402814',
'2000402566',
'2000378931',
'2000373924',
'2000368171',
'2000334702',
'2000328852',
'2000272035',
'2000449906',
'2000253225',
'2000416814',
'2000238754',
'2000234878',
'2000224861',
'2000200945',
'2000189684',
'2000107967',
'2000074100',
'38117149',
'18121335',
'18112946',
'2000263109',
'2000684643',
'2000538377',
'2000529293',
'2000527271',
'2000526430',
'2000519583',
'2000510335',
'2000500039',
'2000483632',
'2000482584',
'2000467858',
'2000407698',
'2000456067',
'2000408894',
'2000448494',
'2000456299',
'2000445490',
'2000440558',
'2000429155',
'2000424131',
'2000422309',
'2000421087',
'2000418661',
'2000551941',
'2000467106',
'2000710539',
'2000709424'
)


select max(histdate)
from history h join #dupes tmp
    on h.tipnumber = tmp.tipnumber



declare @datedeleted datetime
set @datedeleted = getdate()

begin tran


	-------------- purge remainging impCustomer_Purge records. 
	-- Insert customer to customerdeleted 
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, c.Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from dbo.Customer c join #dupes prg
		on c.tipnumber = prg.tipnumber


	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn't work. I keep getting a datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		from dbo.Affiliat aff join #dupes prg
		on aff.tipnumber = prg.tipnumber

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from dbo.History H join #dupes prg
	on h.tipnumber = prg.tipnumber


	-- Delete from customer 
	Delete c
	from dbo.Customer c join #dupes prg
	on c.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join #dupes prg
	on aff.tipnumber = prg.tipnumber


	-- Delete records from History 
	Delete h
	from dbo.History h join #dupes prg
	on h.tipnumber = prg.tipnumber


rollback tran  ---  commit tran




update c 
    set status = 'S'
from #dupes tmp join rn1.deanbank.dbo.customer c
    on tmp.tipnumber = c.tipnumber








