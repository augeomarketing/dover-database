USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete from dbo.[ssis configurations]
where configurationfilter like '219%'
GO


BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'219_OPS_M04_WelcomeKitAddressFiles', N'\\236722-sqlclus2\ops\219\Output\WelcomeKits\welcomekit.xls', N'\Package.Connections[welcomekit.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M04_WelcomeKitAddressFiles', N'\\236722-sqlclus2\ops\219\Output\WelcomeKits\219WelcomeKit.xls', N'\Package.Connections[219WelcomeKit.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M04_WelcomeKitAddressFiles', N'\\FTP\ike\Production Files\WelcomeKit\', N'\Package.Variables[User::ProdWelcomeKitPath].Properties[Value]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'\\236722-sqlclus2\ops\219\logs\219_OPS_M01_MonthlyImport_MSAccess.log', N'\Package.Connections[219_OPS_M01_MonthlyImport_MSAccess.log].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M05_PostPattonToWeb', N'\RewardsNow\219\219_SUB_M04_WebEStatementLoad', N'\Package\Post E-Statements to Web.Properties[PackageName]', N'String' UNION ALL
SELECT N'219_OPS_M04_WelcomeKitAddressFiles', N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\236722-sqlclus2\ops\219\Output\WelcomeKits\219welcomekit.xls;Extended Properties="EXCEL 8.0;HDR=YES";', N'\Package.Connections[Welcome Kit XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_SUB_M01_OneTimeBonusProcessing', N'Data Source=236722-sqlclus2\WEB;Initial Catalog=DeanBank;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RN1_DeanBank].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_SUB_M01_OneTimeBonusProcessing', N'500', N'\Package.Variables[User::OTBPoints_EStatements].Properties[Value]', N'Int32' UNION ALL
SELECT N'219_SUB_M04_EStatementLoad', N'Data Source=236718-SQLCLUS;Initial Catalog=DeanBank;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RN1_DeanBank].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_Step_01_ImportandAudit', N'\RewardsNow\219\219_OPS_M02_MonthlyAuditFile_Creation_Stage', N'\Package\Generate Audit Files.Properties[PackageName]', N'String' UNION ALL
SELECT N'219_OPS_M05_PostPattonToWeb', N'Data Source=236718-sqlclus;Initial Catalog=DeanBank;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RN1_DeanBank].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementLaunch', N'\\236722-sqlclus2\ops\SSIS_Packages\TNB\TNB_OPS_Q03_QuarterlyStatements_PRODUCTION.dtsx', N'\Package.Connections[TNB_OPS_Q03_QuarterlyStatements_PRODUCTION.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementLaunch', N'\\236722-sqlclus2\ops\SSIS_Packages\TNB\TNB_OPS_Q02_QuarterlyStatements_AUDIT.dtsx', N'\Package.Connections[TNB_OPS_Q02_QuarterlyStatements_AUDIT.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementLaunch', N'\\236722-sqlclus2\ops\NewTNB\Logs\TNB_OPS_Q01_QuarterlyStatementLaunch.log', N'\Package.Connections[TNB_OPS_Q01_QuarterlyStatementLaunch.log].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementFile', N'\\236722-sqlclus2\ops\219\Output\Statements\219QuarterlyStmt.csv', N'\Package.Connections[219QuarterlyStmt.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementFile', N'\\FTP\ike\Production Files\Statement\', N'\Package.Variables[User::ProdStmtPath].Properties[Value]', N'String' UNION ALL
SELECT N'219_Step_01_ImportandAudit', N'\RewardsNow\219\219_OPS_M01_MonthlyImport_MSAccess', N'\Package\Run Import to Stage.Properties[PackageName]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'', N'\Package.Variables[User::RW_Trans_DB_Path].Properties[Value]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'10/1/2009 12:00:00 AM', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'219_Step_01_ImportandAudit', N'Data Source=236722-sqlclus2\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package1-{0EA4E524-B5A2-4709-AF5B-62AF81C3F48C}Doolittle\RN.RewardsNOW;Auto Translate=False;', N'\Package.Connections[SSIS Server].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219', N'SmtpServer=172.20.140.178;UseWindowsAuthentication=False;EnableSsl=False;', N'\Package.Connections[SMTP].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219', N'Data Source=236722-sqlclus2\rn;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219', N'Data Source=236722-sqlclus2\rn;Initial Catalog=219DeanBank;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[219DeanBank].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219', N'03/01/2010 00:00:00', N'\Package.Variables[User::MonthStartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'219', N'06/30/2010 23:59:59', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'\RewardsNow\219\219_SUB_M01_OneTimeBonusProcessing', N'\Package\Process Imported Data\One Time Bonuses Processing.Properties[PackageName]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'SSIS Server', N'\Package\Process Imported Data\One Time Bonuses Processing.Properties[Connection]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'\\236722-sqlclus2\ops\219\Work\PERCONV.BAT', N'\Package\Import Data\Exec Personator.Properties[Executable]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'\\236722-sqlclus2\ops\219\Output\ClientFiles\TransactionError.txt', N'\Package.Connections[TransactionError].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'Data Source=236722-sqlclus2\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-219_OPS_M01_MonthlyImport_MSAccess-{091B7936-808D-4EFC-ABFA-F00E58CB6A87}Doolittle\RN.RewardsNOW;Auto Translate=False;', N'\Package.Connections[SSIS Server].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'\\236722-sqlclus2\ops\219\Input\RN_TRANS.mdb', N'\Package.Connections[RW_TRANS.mdb].Properties[ServerName]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'Data Source=\\236722-sqlclus2\ops\219\Input\RN_TRANS.mdb;Provider=Microsoft.Jet.OLEDB.4.0;', N'\Package.Connections[RW_TRANS.mdb].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'\\236722-sqlclus2\ops\219\Output\ClientFiles\CustomerError.txt', N'\Package.Connections[CustomerError.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'Data Source=\\236722-sqlclus2\ops\219\work;Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=dbase 5.0;', N'\Package.Connections[Personator].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M01_MonthlyImportMSAccess', N'12/31/2009 11:59:59 PM', N'\Package.Variables[User::MonthEndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'219_Step_02_Post', N'Data Source=236722-sqlclus2\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-219_Step_02_Post-{C1A2335F-DA32-488B-8825-ED6A56287724}Doolittle\RN.RewardsNOW 1;Auto Translate=False;', N'\Package.Connections[SSIS Server].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M02_MonthlyAuditFile_Creation_Stage', N'\\236722-sqlclus2\ops\219\Output\ClientFiles\Stmts.csv', N'\Package.Connections[Stmts].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M02_MonthlyAuditFile_Creation_Stage', N'\\236722-sqlclus2\OPS\219\Output\ClientFiles\Count.txt', N'\Package.Connections[Count].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_M02_MonthlyAuditFile_Creation_Stage', N'\\236722-sqlclus2\ops\219\Logs\219_OPS_M02_MonthlyAuditFile_Creation.log', N'\Package.Connections[219_OPS_M02_MonthlyAuditFile_Creation.log].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementFile', N'Data Source=236718-sqlclus;Initial Catalog=DeanBank;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[WEB_DeanBank].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementFile', N'\\236722-sqlclus2\ops\219\Output\Statements\219QuarterlyStmt.xls', N'\Package.Connections[Excel 219QuarterlyStmt.xls].Properties[ExcelFilePath]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementFile', N'\\236722-sqlclus2\ops\219\Output\Statements\EMPTY219QuarterlyStmt.xls', N'\Package.Connections[EMPTY219QuarterlyStmt.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'219_OPS_Q01_QuarterlyStatementFile', N'\\236722-sqlclus2\ops\219\Output\Statements\219QuarterlyStmt.xls', N'\Package.Connections[219QuarterlyStmt.xls].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

