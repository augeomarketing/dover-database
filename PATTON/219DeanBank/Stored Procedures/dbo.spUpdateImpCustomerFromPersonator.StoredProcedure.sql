use [219Deanbank]
GO

if exists(select 1 from dbo.sysobjects where name = 'spUpdateImpCustomerFromPersonator' and xtype = 'P')
    drop procedure dbo.spUpdateImpCustomerFromPersonator
GO


create procedure dbo.spUpdateImpCustomerFromPersonator

as

--
-- First for rows in nameprs where personator couldn't determine last name, take a "best guess"
-- by parsing it out

if object_id('tempdb..#lastnames') is not null
    drop table #lastnames


create table #lastnames
    (persnbr		    varchar(15),
     acctname1		    varchar(40) not null,
     lastname		    varchar(40),
     firstname			varchar(40),
     suffix				varchar(40)
    )

-- Load up temp table
insert into #lastnames
(persnbr, acctname1, lastname, firstname, suffix)
select persnbr, acctnm, acctnm, acctnm,
		  reverse(right( reverse(acctnm), len(acctnm)-charindex(' ', reverse(acctnm))))
from dbo.nameprs
where lastnm is null


--
-- check if suffix exists in lookup table.  after reversing, it'll be in the first few positions of the acctname1
-- if a suffix exists, replace it with an empty string.
-- Need to add new suffixes?  put them into the dbo.lastnamesuffix table
update ln
    set lastname = replace(acctname1, suffix, '') 
from #lastnames ln join mountainonefp.dbo.lastnamesuffix lns
    on ltrim(rtrim(suffix)) = lns.dim_lastnamesuffix_name


-- Now reverse full name, parse out last name, and reverse it back
update #lastnames
    set firstname = reverse(left(reverse(firstname), charindex(' ', reverse(firstname)))),
		lastname = reverse(right( reverse(lastname), len(lastname)-charindex(' ', reverse(lastname))))


update np
	set firstnm = ln.firstname,
		lastnm = ln.lastname
from #lastnames ln join dbo.nameprs np
	on ln.persnbr = np.persnbr


--
-- Now update impcustomer with the names

update cus
    set customerlastname = isnull(prs.lastnm, ''),
	   customername = isnull(prs.firstnm, '') + 
				    case
					   when prs.middlenm is null then ''
					   else ' ' + prs.middlenm
				    end + ' ' + isnull(prs.lastnm, '')
from dbo.nameprs prs join dbo.impcustomer cus
    on prs.persnbr = cus.customernbr



/* test harness 

exec dbo.spUpdateImpCustomerFromPersonator


*/