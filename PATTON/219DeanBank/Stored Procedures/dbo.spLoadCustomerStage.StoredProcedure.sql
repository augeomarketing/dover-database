use [219DeanBank]
GO


if exists(select 1 from dbo.sysobjects where [name] = 'spLoadCustomerStage' and xtype = 'P')
	DROP PROCEDURE [dbo].[spLoadCustomerStage]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table		*/
/*    it only updates the customer demographic data						*/
/*																*/
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE						*/
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 1/2007													*/
/* REVISION: 1														*/
/* 6/22/07 South Florida now sending the FULL name in the Name field.			*/
/*																*/
/* 4/15/08 Paul H. Butler:  Copied & modified for use with LOC FCU			*/
/********************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

set nocount on

declare @tipnumber	varchar(15)

/* Update Existing Customers                                         */
Update cstg
	set	lastname		= ltrim(rtrim(imp.customerlastname)),
		acctname1		= ltrim(rtrim(imp.CustomerName)),
		acctname2		= ltrim(rtrim(imp.CustomerName2)),
		acctname3		= ltrim(rtrim(imp.CustomerName3)),
		acctname4		= ltrim(rtrim(imp.CustomerName4)),
		acctname5		= ltrim(rtrim(imp.CustomerName5)),
		acctname6		= ltrim(rtrim(imp.CustomerName6)),
		address1		= case
						when imp.BusinessName is null then ltrim(rtrim(imp.CustomerAddress))
						else ltrim(rtrim(imp.BusinessName))
					  end,
		address2		= case
						when imp.BusinessName is null then ''
						else ltrim(rtrim(imp.CustomerAddress))
					  end,
		address4		= left(ltrim(rtrim(imp.CustomerCity)) + ' ' + ltrim(rtrim(imp.CustomerState)) + ' ' + ltrim(rtrim(imp.CustomerZip)), 40),
		city			= ltrim(rtrim(imp.CustomerCity)),
		state		= ltrim(rtrim(imp.CustomerState)),
		zipcode		= ltrim(rtrim(imp.CustomerZip)),
		homephone		= ltrim(rtrim(imp.CustomerHomePhone)),
		workphone		= ltrim(rtrim(imp.CustomerWorkPhone)),
		misc2		= isnull(right(ltrim(rtrim(txn.CustomerAcctNbr)), 6), ''),
		misc1		= ltrim(rtrim(imp.CustomerLast4SSN)),
		misc4		= ltrim(rtrim(imp.primarycustomerdda)),
		status		= case
						when ltrim(rtrim(imp.CustomerStatusCd)) = 'AC' then 'A'
						when ltrim(rtrim(imp.CustomerStatusCd)) = 'CLS' then 'C'
						when ltrim(rtrim(imp.customerstatuscd)) = 'CL' then 'C'
					  End

from dbo.Customer_Stage cstg join dbo.impCustomer imp
on	cstg.tipnumber = imp.tipnumber

left outer join (select distinct tipnumber, CustomerAcctNbr from dbo.imptransaction) txn
	on txn.tipnumber = imp.tipnumber


/* Add New Customers                                                      */

declare csrNewCustomers cursor FAST_FORWARD for
	select distinct imp.tipnumber
	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.tipnumber = stg.tipnumber
	where stg.tipnumber is null

open csrNewCustomers

fetch next from csrNewCustomers into @tipnumber

while @@FETCH_STATUS = 0
BEGIN

	Insert into Customer_Stage
		(tipnumber, tipfirst, tiplast, lastname,
		 acctname1, acctname2, acctname3, acctname4, acctname5, acctname6, address1, address2, address4,
		 city, state, zipcode, homephone, workphone,
		 misc4, misc2, misc1, status, dateadded, 
		 runavailable, runbalance, runredeemed, runavaliableNew)

	select top 1 imp.TIPNUMBER, left(imp.TIPNUMBER,3), right(ltrim(rtrim(imp.TIPNUMBER)),12), left(ltrim(rtrim(imp.customerlastname)),40),
		ltrim(rtrim(imp.CustomerName)), ltrim(rtrim(imp.CustomerName2)), ltrim(rtrim(imp.CustomerName3)), ltrim(rtrim(imp.CustomerName4)), 
		ltrim(rtrim(imp.CustomerName5)), ltrim(rtrim(imp.CustomerName6)), 

		case
			when imp.BusinessName is null then ltrim(rtrim(imp.CustomerAddress))
			else ltrim(rtrim(imp.BusinessName))
		end as Address1,
		case
			when imp.BusinessName is null then ''
			else ltrim(rtrim(imp.CustomerAddress))
		end as Address2,

		left(ltrim(rtrim(imp.CustomerCity)) + ' ' + ltrim(rtrim(imp.CustomerState)) + ' ' + ltrim(rtrim(imp.CustomerZip)), 40),
		ltrim(rtrim(imp.CustomerCity)), ltrim(rtrim(imp.CustomerState)), ltrim(rtrim(imp.customerzip)),
		ltrim(rtrim(imp.CustomerHomePhone)), ltrim(rtrim(imp.CustomerWorkPhone)), ltrim(rtrim(imp.primarycustomerdda)),
		isnull(right(ltrim(rtrim(txn.CustomerAcctNbr)), 6), ''), ltrim(rtrim(CustomerLast4SSN)),
		case
			when ltrim(rtrim(imp.CustomerStatusCd)) = 'AC' then 'A'
			when ltrim(rtrim(imp.CustomerStatusCd)) = 'CLS' then 'C'
			when ltrim(rtrim(imp.CustomerStatusCd)) = 'CL' then 'C'
		end as customerstatuscd, @EndDate, 0, 0, 0, 0

	from dbo.impCustomer imp left outer join dbo.Customer_Stage stg
		on imp.tipnumber = stg.tipnumber

left outer join (select distinct tipnumber, CustomerAcctNbr from dbo.imptransaction) txn
	on txn.tipnumber = imp.tipnumber

	where stg.tipnumber is null and imp.tipnumber = @tipnumber order by imp.CustomerNbr asc

	fetch next from csrNewCustomers into @tipnumber
END

close csrNewCustomers

deallocate csrNewCustomers




/* set Default status to A */
Update dbo.Customer_Stage
	Set STATUS = 'A' 
Where STATUS IS NULL 


/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update cstg
	Set	StatusDescription = S.StatusDescription 
from dbo.status S join dbo.Customer_Stage cstg
on S.Status = cstg.Status
GO



