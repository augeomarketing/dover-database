use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadTipNumbers' and xtype = 'P')
	DROP PROCEDURE [dbo].[spLoadTipNumbers]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spLoadTipNumbers] @TipPrefix char(3) AS

declare @NewTip varchar(15)

-----------------------------------------------------------------------
--
-- Update tips for existing customers
--
-----------------------------------------------------------------------
-- Try and match by DDA# which is stored in the misc4 column
update imp
	set tipnumber = stg.tipnumber
from dbo.impCustomer imp join dbo.Customer_Stage stg
	on imp.PrimaryCustomerDDA = stg.misc4


-- Try and match by acct# if there are transactions for the customer
update imp	
	set tipnumber = aff.tipnumber
from dbo.impcustomer imp join dbo.impTransaction txn
	on imp.PrimaryCustomerdda = txn.ddanbr

join dbo.affiliat_stage aff
	on aff.ACCTID = txn.CustomerAcctNbr
where imp.tipnumber is null




-----------------------------------------------------------------------
--
-- Generate TIPS for new customers
--
-----------------------------------------------------------------------

-- Get last used tip number
--set @NewTip = ( select LastTipNumberUsed  from dbo.Client )
exec rewardsnow.dbo.spGetLastTipNumberUsed '219', @NewTIP OUTPUT

-- Should it come back as null, initialize it to 12 zeroes
If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'

-- drop table #AssignTipsToDDA
create table #AssignTipsToDDA (PrimaryCustomerDDA varchar(25) primary key, TipNumber varchar(15) null)

insert into #AssignTipsToDDA
select distinct primarycustomerdda, space(15) as TipNumber
from dbo.impcustomer imp
where imp.tipnumber is null

update #AssignTipsToDDA
	set @NewTip = TipNumber = cast( cast(@NewTip as bigint) + 1 as varchar(15))


/* Assign Get New Tips to new customers */
update imp
	set  tipnumber = tmp.tipnumber
from #AssignTipsToDDA tmp join dbo.impcustomer imp
	on tmp.PrimaryCustomerDDA = imp.PrimaryCustomerDDA

-- Update LastTip -- 
--Update dbo.Client 
--	set LastTipNumberUsed = @NewTip  

exec rewardsnow.dbo.spPutLastTipNumberUsed '219', @NewTip


-- Update imptransaction table with TIPNumbers
update txn
	set tipnumber = cus.tipnumber
from dbo.imptransaction txn join dbo.impCustomer cus
	on txn.customernbr = cus.customernbr
GO



