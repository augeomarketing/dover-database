use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spHouseholdAccounts' and xtype = 'P')
	drop procedure dbo.spHouseholdAccounts
go

create procedure dbo.spHouseholdAccounts
as

set nocount on

declare @dda			varchar(25)

declare csrDDA cursor FAST_FORWARD for
	select distinct primarycustomerdda from dbo.impCustomer

open csrDDA

fetch next from csrDDA into @dda

while @@FETCH_STATUS = 0
BEGIN
	update imp
		set	customername =  tmp.name1,
			customername2 = tmp.name2,
			customername3 = tmp.name3,
			customername4 = tmp.name4,
			customername5 = tmp.name5,
			customername6 = tmp.name6,
			customerLast4SSN = tmp.PrimaryLast4SSN
	from [dbo].[fnLinkCheckingNames] (@dda) tmp join dbo.impCustomer imp
		on tmp.checking = imp.primarycustomerdda

	fetch next from csrDDA into @dda
END

close csrDDA

deallocate csrDDA

create table #primary
	(sid				bigint identity(1, 1) primary key,
	 primarycustomerdda varchar(25),
	 CustomerAddress	varchar(40),
	 CustomerCity		varchar(50),
	 CustomerState		varchar(2) null,
	 CustomerZip		varchar(15) null,
	 CustomerHomePhone	varchar(10) null,
	 CustomerWorkPhone	varchar(10) null)

create index ix_tmp_primaryAddress_001 on #primary
	(primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone)

create index ix_impCustomer_PrimaryFlag_AddressData_001 on dbo.impCustomer
(primaryflag, primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone)


insert into #primary
(primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone)
select primarycustomerdda, customeraddress, customercity, customerstate, customerzip, customerhomephone, customerworkphone
from dbo.impcustomer
where primaryflag = 'Y'

update cus
	set	CustomerAddress	= tmp.CustomerAddress,
		CustomerCity		= tmp.CustomerCity,
		CustomerState		= tmp.CustomerState,
		CustomerZip		= tmp.CustomerZip,
		CustomerHomePhone	= tmp.CustomerHomePhone,
		CustomerWorkPhone	= tmp.CustomerWorkPhone
from #primary tmp join dbo.impCustomer cus
	on tmp.primarycustomerdda = cus.primarycustomerdda

drop index dbo.impCustomer.ix_impCustomer_PrimaryFlag_AddressData_001