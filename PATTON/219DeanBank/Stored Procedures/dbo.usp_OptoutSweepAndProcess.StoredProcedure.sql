USE [219DeanBank]
GO
/****** Object:  StoredProcedure [dbo].[usp_OptoutSweepAndProcess]    Script Date: 08/18/2015 14:03:37 ******/
DROP PROCEDURE [dbo].[usp_OptoutSweepAndProcess]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 12/2013
-- Description:	To get opt out request from LRC and Process
-- =============================================
CREATE PROCEDURE [dbo].[usp_OptoutSweepAndProcess] 
	-- Add the parameters for the stored procedure here
	@TipFirst char(3)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @Tipnumber char(15), @DBNameNEXL varchar(50), @SQLDelete nvarchar(max), @SQLUpdate nvarchar(max)
	
	exec rewardsnow.dbo.usp_LRCOptOutUpdate @TipFirst, 'A'

	set @DBNameNEXL=(SELECT  rtrim(DBNameNEXL) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber= @TipFirst)
				
-- PURGE RECORDS ON WEB SIDE
	set @SQLDelete = N'delete [RN1].' + QuoteName(@DBNameNEXL) + N'.dbo.[1security] 
						from [RN1].' + QuoteName(@DBNameNEXL) + N'.dbo.[1security] sec join rewardsnow.dbo.optouttracking oot on sec.tipnumber = oot.tipnumber
						where sec.tipnumber is not null and oot.optoutposted is null'
	exec sp_executesql @SQLDelete
	
	set @SQLDelete = N'delete [RN1].' + QuoteName(@DBNameNEXL) + N'.dbo.Customer 
						from [RN1].' + QuoteName(@DBNameNEXL) + N'.dbo.Customer cs join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.tipnumber
						where cs.tipnumber is not null and oot.optoutposted is null'
	exec sp_executesql @SQLDelete

	set @SQLDelete = N'delete [RN1].' + QuoteName(@DBNameNEXL) + N'.dbo.Account 
						from [RN1].' + QuoteName(@DBNameNEXL) + N'.dbo.Account act join rewardsnow.dbo.optouttracking oot on act.tipnumber = oot.tipnumber
						where act.tipnumber is not null and oot.optoutposted is null'
	exec sp_executesql @SQLDelete

-- SET STATUS TO C ON PATTON
	update customer
	set status = 'O', StatusDescription = 'Opted Out'
	from dbo.Customer cs join rewardsnow.dbo.optouttracking oot on cs.tipnumber = oot.tipnumber
	where cs.tipnumber is not null and oot.optoutposted is null

	update AFFILIAT
	set AcctStatus = 'O'
	from dbo.AFFILIAT af join rewardsnow.dbo.optouttracking oot on af.tipnumber = oot.tipnumber
	where af.tipnumber is not null and oot.optoutposted is null

	Insert into dbo.Customerdeleted 
	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, BonusFlag, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, getdate() 
	from dbo.Customer 
	where status='O'
	
	Insert into dbo.Affiliatdeleted (TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, Datedeleted)
    SELECT TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, getdate() 
	FROM dbo.affiliat
	where AcctStatus = 'O'
	
	Insert into dbo.Historydeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
    SELECT hs.TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, getdate() 
	FROM dbo.History hs join customer cs on hs.tipnumber = cs.tipnumber
	where cs.status = 'O'
	
	delete from dbo.history
	from dbo.history hs join customer cs on hs.tipnumber = cs.tipnumber
	where cs.status = 'O'
	
	delete from dbo.affiliat
	where AcctStatus = 'O'

	update RewardsNow.dbo.OPTOUTTracking
	set optoutposted = GETDATE()
	from rewardsnow.dbo.optouttracking oot join customer cs on oot.tipnumber = cs.tipnumber
	where oot.optoutposted is null and cs.status='O'
		
	delete from dbo.customer
	where status = 'O'
	 
END
GO
