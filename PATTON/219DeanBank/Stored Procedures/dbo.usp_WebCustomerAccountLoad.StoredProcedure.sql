USE [219DeanBank]
GO

/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 10/21/2010 10:32:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_WebCustomerAccountLoad]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_WebCustomerAccountLoad]
GO

/****** Object:  StoredProcedure [dbo].[usp_WebCustomerAccountLoad]    Script Date: 10/21/2010 10:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create procedure [dbo].[usp_WebCustomerAccountLoad]

AS

delete from dbo.web_account
delete from dbo.web_customer

delete from rn1.deanbank.dbo.web_account
delete from rn1.deanbank.dbo.web_customer


insert into dbo.web_customer
	(TipNumber, TipFirst, TipLast, Name1, Name2, Name3, Name4, Name5, Address1, Address2, Address3, CityStateZip, ZipCode, 
	 EarnedBalance, Redeemed, AvailableBal, Status, city, state)
select tipnumber, left(tipnumber,3), right(tipnumber,12), acctname1, acctname2, acctname3, acctname4, acctname5,
		address1, address2, address3, address4, zipcode, RUNBALANCE, RunRedeemed, RunAvailable, status, city, state
from dbo.customer	
where status in ( 'A', 'S')




insert into dbo.web_account (TipNumber, LastName, LastSix, SSNLast4)
select tipnumber, isnull(lastname, 'NotAssigned') , misc4, left(misc1,4)
from dbo.customer
where status in( 'A', 'S')

GO


