USE [219DeanBank]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 05/13/2015 09:24:44 ******/
DROP PROCEDURE [dbo].[spPurgeClosedCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read impCustomer_Purge
-- staging tables don't need to have records moved to delete tables
-- Production tables need to have pending purge processing.

--RDT 3/21/08 and tipnumber Not In ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> 'RQ' )

--PHB 4/15/08 refactored sql inserts/updates.  Changed to match the tables used for 218LOCFCU

/******************************************************************************/
CREATE PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted datetime AS


----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin
	truncate table dbo.impcustomer_Purge

	insert into dbo.impCustomer_Purge
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, 
	 CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, 
	 PrimaryCustomerDDA, TipNumber)
	select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, 
	        CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, 
	        PrimaryCustomerDDA, TipNumber
	from dbo.impCustomer_Purge_Pending ipp 
	
	truncate table dbo.impCustomer_Purge_Pending
	

    insert into dbo.impcustomer_purge
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, 
	 CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, 
	 PrimaryCustomerDDA, TipNumber)
    select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, 
	 CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, 
	 PrimaryCustomerDDA, TipNumber
	from dbo.impcustomer
	where customerstatuscd = 'CL'

    ---- Delete any tips not in input file - assuming full file reload every month
    --insert into dbo.impcustomer_purge
    --(CustomerNbr, CustomerName, CustomerStatusCd, PrimaryCustomerDDA, tipnumber)
    --select stg.tipnumber, stg.acctname1, 'CL', stg.misc4, stg.tipnumber
    --from dbo.customer_stage stg left outer join dbo.impcustomer imp
    --    on stg.misc4 = imp.primarycustomerdda
    --where imp.customerrowkey is null



	
	--
	-- Can't do a delete on customers with current activity, so put them into pending so they'll purge next processing cycle
    insert into dbo.impcustomer_purge_pending
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, 
	 CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, 
	 PrimaryCustomerDDA, TipNumber)
	select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, 
	 CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, 
	 PrimaryCustomerDDA, pg.TipNumber
	from dbo.impcustomer_purge pg join (select tipnumber from dbo.history_stage where histdate > @DateDeleted and trancode not in('RQ', 'XP') ) HIS
	    on pg.tipnumber = his.tipnumber
        					   
					   
    delete pg
    from dbo.impcustomer_purge pg join dbo.impcustomer_purge_pending pp
        on pg.tipnumber = pp.tipnumber
    

    --
    -- Now purge staging
    if object_id('tempdb..#stage_deleted') is not null drop table #stage_deleted
  	create table #Stage_Deleted (tipnumber	varchar(15) primary key)

	insert into #Stage_Deleted
	select distinct tipnumber
	from dbo.impcustomer_purge
	where tipnumber is not null	




	delete txn
	from dbo.impTransaction txn join dbo.impCustomer_Purge prg
		on txn.ddanbr = prg.PrimaryCustomerDDA

    delete imp
    from dbo.impcustomer imp join dbo.impcustomer_purge prg
        on imp.primarycustomerdda = prg.primarycustomerdda

    delete his
	from dbo.history_stage his join #Stage_Deleted dlt
		on his.tipnumber = dlt.tipnumber
		
	delete aff
	from dbo.affiliat_stage aff join #Stage_Deleted dlt
	    on aff.tipnumber = dlt.tipnumber
	    
	delete cus
	from dbo.customer_stage cus join #Stage_Deleted dlt
	    on cus.tipnumber = dlt.tipnumber


End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin

	-- copy any impCustomer_Purge_pending into impCustomer_Purge 
	Insert into dbo.impCustomer_Purge
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, 
	 CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, 
	 CustomerLast4SSN, PrimaryCustomerDDA, TipNumber)
		Select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, 
				CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, 
				CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, PrimaryCustomerDDA, TipNumber
	from dbo.impCustomer_Purge_Pending

	-- Clear impCustomer_Purge_Pending 
	Truncate Table dbo.impCustomer_Purge_Pending

	-- Copy any customers from impCustomer_Purge to impCustomer_Purge_pending if they have History activity greater than the delete date
	Insert into dbo.impCustomer_Purge_Pending 
	(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, 
	 CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, 
	 CustomerLast4SSN, PrimaryCustomerDDA, TipNumber)
		select CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, 
				CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, 
				CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, PrimaryCustomerDDA, imp.TipNumber
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and	his.histdate > @datedeleted 
			and	'RQ' != his.trancode


	-- Remove any customers from impCustomer_Purge if they have current activity in history
	Delete imp
		from dbo.impCustomer_Purge imp join dbo.history his
			on	imp.tipnumber = his.tipnumber
			and  his.histdate > @datedeleted
			and	his.trancode != 'RQ'


	-------------- purge remainging impCustomer_Purge records. 
	-- Insert customer to customerdeleted 
	Insert Into dbo.CustomerDeleted
	(TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
	 LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
	 ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, 
	 BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, 
	 Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)

	select c.TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, 
			LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, 
			ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, 
			WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, 
			BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted  
	from dbo.Customer c join dbo.impCustomer_Purge prg
		on c.tipnumber = prg.tipnumber


	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn't work. I keep getting a datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into dbo.AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
	Select AcctId, aff.TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		from dbo.Affiliat aff join dbo.impCustomer_Purge prg
		on aff.tipnumber = prg.tipnumber

	-- copy history to historyDeleted 
	Insert Into dbo.HistoryDeleted
	(TipNumber, AcctID, HistDate, TranCode, TranCount, Points, Description, SecID, Ratio, Overage, DateDeleted)
	Select h.TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage , @DateDeleted as DateDeleted 
	from dbo.History H join dbo.impCustomer_Purge prg
	on h.tipnumber = prg.tipnumber


	-- Delete from customer 
	Delete c
	from dbo.Customer c join dbo.impCustomer_Purge prg
	on c.tipnumber = prg.tipnumber


	-- Delete records from affiliat 
	Delete aff
	from dbo.Affiliat aff join dbo.impCustomer_Purge prg
	on aff.tipnumber = prg.tipnumber


	-- Delete records from History 
	Delete h
	from dbo.History h join dbo.impCustomer_Purge prg
	on h.tipnumber = prg.tipnumber


	-- flag all Undeleted Customers "C" that have an impCustomer_Purge_pending record 
	Update c
		set status = 'C' 
	from dbo.customer c join dbo.impCustomer_Purge_Pending prg
	on c.tipnumber = prg.tipnumber


End

--
GO
