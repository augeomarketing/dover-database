use [219DeanBank]
GO

DROP PROCEDURE [dbo].[spAssignNewTipNumbers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spAssignNewTipNumbers]
as


declare csrCustomer cursor 
	for select TIPFIRST from dbo.Customer where tipnumber is null
	for update of TipNumber

declare @Tipfirst		varchar(3)
declare @NewTipNumber	nvarchar(15)

open csrCustomer

fetch next from csrCustomer into @TipFirst

while @@FETCH_STATUS = 0
BEGIN
	-- get next tip number
	exec dbo.GetLastTipNumberUsed @TipFirst, @NewTipNumber output

	update dbo.Customer
		set tipnumber = @NewTipNumber
	where current of csrCustomer

	fetch next from csrCustomer into @TipFirst
END

close csrCustomer

deallocate csrCustomer
GO
