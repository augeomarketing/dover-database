use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where name = 'spCalculatePoints' and xtype = 'p')
	DROP PROCEDURE [dbo].[spCalculatePoints]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************	*/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/* 4/15/08 PHB - Modified to work with		*/
/*				LOC FCU				*/
/*  **************************************	*/
/*  Calculates points in input_transaction	*/
/*  **************************************	*/
CREATE PROCEDURE [dbo].[spCalculatePoints]  AS   

declare @MonthEndDate datetime

--
-- get month end date from ssis configuratin table
--
set @MonthEndDate = (Select configuredvalue 
					from rewardsnow.dbo.[ssis configurations] 
					where packagepath = '\Package.Variables[User::MonthEndDate].Properties[Value]'
					and configurationfilter = '219')

--
-- calc points.  If tip is in the bonus participants table, get the point multipler.  If the tip
-- is not present in the bonus participants table, the point multiplier will be defaulted to 1
--
	Update imp 
		set Points = imp.TxnAmount * F.pointFactor * isnull(vw.dim_BonusProgram_PointMultiplier, 1)
	from dbo.impTransaction imp join dbo.Trancode_factor F 
	on imp.Trancode = f.Trancode

	left outer join dbo.vw_BonusParticipantsPointMultiplier vw
		on imp.tipnumber = vw.dim_BonusParticipants_TipNumber
		and @MonthEndDate between dim_BonusProgram_EffectiveDate and dim_BonusProgram_ExpirationDate



GO
