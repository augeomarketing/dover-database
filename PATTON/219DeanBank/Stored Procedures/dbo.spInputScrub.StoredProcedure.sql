use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spInputScrub' and xtype = 'P')
	DROP PROCEDURE [dbo].[spInputScrub]
GO


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/********************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables  */
/*																*/
/* BY:  R.Tremblay													*/
/* DATE: 4/2007													*/
/* REVISION: 0														*/
/*																*/
/* DATE: 04/16/2008													*/
/* REVISION: 01													*/
/* Enhanced to work with 219DeanBank									*/
/********************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] AS

/* clear error tables */
Truncate Table  ImpCustomer_Error
Truncate Table ImpTransaction_Error

--------------- Input Customer table
-- Remove customers that do not have transactions
--delete cus
--from dbo.impcustomer cus left outer join dbo.imptransaction txn
--	on cus.primarycustomerdda = txn.ddanbr
--where txn.txnid is null

/* Remove input_customer records with Firstname and lastname = null */
Insert into dbo.ImpCustomer_error 
(CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, PrimaryCustomerDDA)
	select  CustomerNbr, CustomerName, CustomerLastName, CustomerAddress, CustomerCity, CustomerState, CustomerZip, CustomerHomePhone, CustomerWorkPhone, CustomerStatusCd, CustomerLast4SSN, CustomerName2, PrimaryCustomerDDA
	from dbo.ImpCustomer 
	where CustomerName is null or ltrim(rtrim(CustomerStatusCd)) not in('AC', 'CL')

delete from dbo.ImpCustomer 
where CustomerName is null or ltrim(rtrim(CustomerStatusCd)) not in('AC', 'CL')

/* remove leading and trailing spaces in names and addresses */ 
Update dbo.ImpCustomer 
	set	CustomerName = Ltrim(Rtrim(CustomerName)) , 
		CustomerLastName = ltrim(rtrim(CustomerLastName)),
		CustomerAddress = Ltrim(Rtrim(CustomerAddress)),
		CustomerCity = ltrim(rtrim(CustomerCity)),
		PrimaryFlag = isnull(primaryflag, 'N')

-- Now validate transactions
Insert into dbo.ImpTransaction_error 
(CustomerNbr, CustomerAcctNbr, TfrCustomerAcctNbr, DDANbr, TxnCode, TxnAmount, TxnQuantity)
	select txn.CustomerNbr, CustomerAcctNbr, TfrCustomerAcctNbr, DDANbr, TxnCode, TxnAmount, TxnQuantity
	from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
		on txn.ddanbr = cus.PrimaryCustomerDDA
	where cus.CustomerNbr is null


Delete txn
	from dbo.ImpTransaction txn left outer join dbo.ImpCustomer cus
		on txn.ddanbr = cus.PrimaryCustomerDDA
	where cus.CustomerNbr is null

--------------- Input Transaction table
-- Convert Trancodes to Product Codes
Update dbo.impTransaction 
	set TranCode = case
					when upper(txncode) = '220'	then '67'
					when upper(txncode) = '37'	then '37'
				end


-- Reverse name.  Dean bank sends us the customer name as last name first, first name last

--update dbo.impCustomer
--	set customername = ltrim(rtrim(right(customername, charindex(' ', reverse(customername))))) + ' ' +
--	ltrim(rtrim( left(customername, len(customername) - charindex(' ', reverse(customername))))),
--	customerlastname = ltrim(rtrim( left(customername, len(customername) - charindex(' ', reverse(customername)))))


GO


