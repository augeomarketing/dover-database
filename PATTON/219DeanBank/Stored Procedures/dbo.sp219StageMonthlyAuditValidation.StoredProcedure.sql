use [219DeanBank]
GO


DROP PROCEDURE [dbo].[sp219StageMonthlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp219StageMonthlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedCR numeric(9),@pointspurchasedDB numeric(9), @pointsbonus numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedCR numeric(9), @pointsreturnedDB numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9)

delete from dbo.Monthly_Audit_ErrorFile

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Monthly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor FAST_FORWARD
	for select Tipnumber, pointsbegin, pointsend, pointspurchasedCR, pointspurchasedDB, pointsbonus, pointsadded, pointsincreased, 
				pointsredeemed, pointsreturnedCR, pointsreturnedDB, pointssubtracted, pointsdecreased 
		from dbo.Monthly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */

fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, @pointsadded, 
					@pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, @pointssubtracted, @pointsdecreased

/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'

	if @pointsend <> (select AdjustedEndingPoints from dbo.Current_Month_Activity where tipnumber=@tipnumber)
	begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from dbo.Current_Month_Activity where tipnumber=@tipnumber)		

		INSERT INTO dbo.Monthly_Audit_ErrorFile
       			values(  @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, 
				@pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, 
			            @pointssubtracted, @pointsdecreased, @errmsg, @currentend )

	end

	fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedCR, @pointspurchasedDB, @pointsbonus, 
				 @pointsadded, @pointsincreased, @pointsredeemed, @pointsreturnedCR, @pointsreturnedDB, 
				@pointssubtracted, @pointsdecreased
end

close  tip_crsr
deallocate  tip_crsr
GO
