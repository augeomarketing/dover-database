use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spGenerateEStatementBonus' and xtype = 'P')
	drop procedure dbo.spGenerateEStatementBonus
GO

create procedure dbo.spGenerateEStatementBonus (@MonthEndDate datetime, @BonusPoints int)

as

declare @EndDate datetime

set @EndDate = cast(  cast(year(@monthenddate) as varchar(4)) + '/' +
				  right('00' + cast( month(@monthEndDate) as varchar(2)), 2) + '/' +
				  right('00' + cast( day(@monthenddate) as varchar(2)), 2) as datetime)

--	drop table #EStatementBonuses
-- Create temp table of E-Statement Bonuses
create table #EStatementBonuses
			(sid_EstatementBonuses	bigint identity(1,1) PRIMARY  KEY,
			 TipNumber		varchar(15),
			 AcctId			varchar(25),
			 TranCode		varchar(2),
			 HistDate		datetime)

create index tmp_EStatementBonuses_TipNumber_DDA on #EStatementBonuses(TipNumber, AcctId, Trancode, HistDate)

-- Add E-Statement Bonuses to the temp estatement bonus table
insert into #EStatementBonuses
select distinct tmp.tipnumber, (select top 1 primarycustomerdda from dbo.impcustomer dda where dda.tipnumber = imp.tipnumber) as AcctID,
		 'BE' as TranCode , @EndDate
from dbo.tmpEStmtBonusTips tmp join dbo.impCustomer imp
	on tmp.tipnumber = imp.tipnumber

left outer join dbo.OneTimeBonuses otb
	on otb.tipnumber = tmp.tipnumber
	and 'BE' = otb.TranCode

where otb.tipnumber is null


--
-- Remove any newly created TIPS from combines so they don't get double-bonused
--
delete EStmt
from #EStatementBonuses EStmt join dbo.customerdeleted cd
	on EStmt.AcctId = cd.misc4 -- check customerdeleted by acctid in misc4

join dbo.OneTimeBonuses otb -- see if the old tip#s have already gotten bonus
	on otb.tipnumber = cd.tipnumber



-- Add Bonus Awards to the OneTimeBonuses_Stage table
insert into dbo.OneTimeBonuses_Stage
select tipnumber, trancode, acctid, histdate
from #EStatementBonuses


---- Now add to History_Stage
--insert into dbo.History_Stage
--(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage)
--select tipnumber, acctid, histdate, trancode, 1 as TranCount, @BonusPoints as Points, 'One-Time-Bonus: E-Statement Notification',
--		'NEW' as SECID, 1 as Ratio, 0 as Overage
--from #EStatementBonuses


---- Update Customer_Stage with points added
--update cus
--	set	RunAvailable = RunAvailable + @BonusPoints, 
--		RunBalance=RunBalance + @BonusPoints  
--from #EStatementBonuses tmp join dbo.customer_stage cus
--	on tmp.tipnumber = cus.tipnumber


insert into dbo.transstandard
(TIP, TranDate, AcctNum, TranCode, TranNum, TranAmt, TranType, Ratio, CrdActvlDt)
select tipnumber, histdate, acctid, trancode, 1 as TranCount, @BonusPoints as TranAmt, 'E-Statement Bonus',
			1 as ratio, @MonthEndDate as CrdActvlDt
from #EStatementBonuses


