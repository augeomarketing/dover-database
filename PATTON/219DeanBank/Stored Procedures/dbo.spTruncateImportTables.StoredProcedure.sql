use [219DeanBank]
GO


if exists(select 1 from dbo.sysobjects where [name] = 'spTruncateImportTables' and xtype = 'P')
	drop procedure dbo.spTruncateImportTables
GO

create procedure dbo.spTruncateImportTables

as

truncate table dbo.impCustomer

truncate table dbo.impCustomer_Error

truncate table dbo.impCustomer_purge

--truncate table dbo.impCustomer_purge_pending

truncate table dbo.impTransaction

truncate table dbo.impTransaction_Error

truncate table dbo.impCustomerBonus

truncate table dbo.nameprs