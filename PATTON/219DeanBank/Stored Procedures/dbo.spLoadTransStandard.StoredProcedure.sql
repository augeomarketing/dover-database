use [219DeanBank]
GO

if exists(select 1 from dbo.sysobjects where [name] = 'spLoadTransStandard' and xtype = 'P')
	DROP PROCEDURE [dbo].[spLoadTransStandard]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadTransStandard] @DateAdded datetime 
AS

-- Clear TransStandard 
Truncate table dbo.TransStandard 

declare @monthEndDate nvarchar(10)

set @monthEndDate = right('00' + cast(month(@dateadded) as nvarchar(2)), 2) + '/' +
				right('00' + cast(day(@dateadded) as nvarchar(2)), 2) + '/' +
				cast(year(@dateadded) as nvarchar(4))

-- Load the TransStandard table with rows from Input_Transaction
Insert dbo.TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  cus.[TipNumber], @monthEndDate, txn.[CustomerAcctNbr], [TranCode], sum([TxnQuantity]), sum([Points]), @monthEndDate
from dbo.impTransaction txn join (select distinct tipnumber, primarycustomerdda from dbo.impcustomer)  cus
	on txn.DDANbr = cus.primarycustomerdda
group by cus.[TipNumber], txn.[CustomerAcctNbr], [TranCode]

-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update dbo.TransStandard 
	set	TranType = R.Description,
		Ratio = R.Ratio 
from RewardsNow.dbo.Trantype R join dbo.TransStandard T 
on R.TranCode = T.Trancode
GO
