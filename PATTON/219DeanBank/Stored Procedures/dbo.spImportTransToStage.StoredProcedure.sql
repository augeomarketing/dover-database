use [219DeanBank]
GO

DROP PROCEDURE [dbo].[spImportTransToStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*********************************************/
/* Date: 8/7/06						*/
/* Author: Rich T						*/
/*									*/
/* Modifications						*/
/* 16 Apr 2008 PHB						*/
/* Copied from 208SouthFlorida & Modified	*/
/* for use with 218 LOC FCU				*/
/*********************************************/
/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/


CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst char(3)
AS 

Declare @MaxPointsPerYear decimal(18,0), @YTDEarned numeric(18,0), @AmtToPost numeric (18,0), @Overage numeric(18,0)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 


/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )

/*    Get max Points per year from client table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear from dbo.client where tipfirst = @tipfirst ) 

/***************************** HISTORY_STAGE *****************************/
/*  Insert TransStandard into History_stage */
 insert into dbo.history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select Tip, Acctnum, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, 'NEW', 0 
	From dbo.TransStandard

/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update H
		set Overage = H.Points - ( @MaxPointsPerYear - A.ytdearned  ) 
	FROM dbo.History_Stage H JOIN dbo.Affiliat_Stage A 
			on H.Tipnumber = A.Tipnumber
	where A.YTDEarned + H.Points > @MaxPointsPerYear 
End

/***************************** AFFILIAT_STAGE *****************************/
-- Update Affiliat YTDEarned 
Update A
	set YTDEarned  = A.YTDEarned  + H.Points 
FROM dbo.HISTORY_STAGE H JOIN dbo.AFFILIAT_Stage A 
	on H.Tipnumber = A.Tipnumber

-- Update History_Stage Points = Points - Overage
Update dbo.History_Stage 
	Set Points = Points - Overage 
where Points > Overage

/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update dbo.Customer_Stage 
	Set	RunAvaliableNew	= isnull(RunAvaliableNew, 0),
		RunAvailable		= isnull(RunAvailable, 0)

--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_histpoints]') and OBJECTPROPERTY(id, N'IsView') = 1)
--drop view [dbo].[vw_histpoints]

/* Create View */
--set @SQLStmt = 'Create view vw_histpoints as select tipnumber, sum(points * ratio) as points from history_stage where secid = ''NEW''group by tipnumber'
--exec sp_executesql @SQLStmt


Update C
	Set	RunAvailable  = RunAvailable + v.Points,
		RunAvaliableNew = v.Points 
From dbo.Customer_Stage C join dbo.vw_histpoints V 
	on C.Tipnumber = V.Tipnumber


--drop view [dbo].[vw_histpoints]
GO
