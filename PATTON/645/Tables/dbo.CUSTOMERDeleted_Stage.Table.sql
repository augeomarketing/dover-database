USE [645]
GO
/****** Object:  Table [dbo].[CUSTOMERDeleted_Stage]    Script Date: 11/22/2011 10:11:24 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMERDeleted_Stage_dim_customer_email]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMERDeleted_Stage_dim_customer_email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMERDeleted_Stage] DROP CONSTRAINT [DF_CUSTOMERDeleted_Stage_dim_customer_email]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMERDeleted_Stage_dim_customer_country]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMERDeleted_Stage_dim_customer_country]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMERDeleted_Stage] DROP CONSTRAINT [DF_CUSTOMERDeleted_Stage_dim_customer_country]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMERDeleted_Stage_dim_customer_mobilephone]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMERDeleted_Stage_dim_customer_mobilephone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMERDeleted_Stage] DROP CONSTRAINT [DF_CUSTOMERDeleted_Stage_dim_customer_mobilephone]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMERDeleted_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMERDeleted_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [datetime] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [text] NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL,
	[DateDeleted] [datetime] NULL,
	[dim_customer_email] [varchar](254) NOT NULL,
	[dim_customer_country] [varchar](3) NOT NULL,
	[dim_customer_mobilephone] [varchar](10) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMERDeleted_Stage_dim_customer_email]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMERDeleted_Stage_dim_customer_email]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMERDeleted_Stage] ADD  CONSTRAINT [DF_CUSTOMERDeleted_Stage_dim_customer_email]  DEFAULT ('') FOR [dim_customer_email]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMERDeleted_Stage_dim_customer_country]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMERDeleted_Stage_dim_customer_country]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMERDeleted_Stage] ADD  CONSTRAINT [DF_CUSTOMERDeleted_Stage_dim_customer_country]  DEFAULT ('USA') FOR [dim_customer_country]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_CUSTOMERDeleted_Stage_dim_customer_mobilephone]') AND parent_object_id = OBJECT_ID(N'[dbo].[CUSTOMERDeleted_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CUSTOMERDeleted_Stage_dim_customer_mobilephone]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CUSTOMERDeleted_Stage] ADD  CONSTRAINT [DF_CUSTOMERDeleted_Stage_dim_customer_mobilephone]  DEFAULT ('') FOR [dim_customer_mobilephone]
END


End
GO
