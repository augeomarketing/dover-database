USE [645]
GO
/****** Object:  Table [dbo].[TRANSsTANDARD_Bonuses]    Script Date: 11/22/2011 10:11:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARD_Bonuses]') AND type in (N'U'))
DROP TABLE [dbo].[TRANSsTANDARD_Bonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARD_Bonuses]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TRANSsTANDARD_Bonuses](
	[TFNO] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANNUM] [nvarchar](4) NULL,
	[TRANAMT] [nchar](15) NULL,
	[TRANTYPE] [nvarchar](40) NULL,
	[RATIO] [nvarchar](4) NULL,
	[CRDACTVLDT] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
