USE [645]
GO
/****** Object:  Table [dbo].[Account_Reference_Deleted]    Script Date: 11/22/2011 10:11:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Deleted]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference_Deleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Deleted]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference_Deleted](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
