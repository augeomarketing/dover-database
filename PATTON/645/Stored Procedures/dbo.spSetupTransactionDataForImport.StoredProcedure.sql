USE [645]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 10/16/2013 15:23:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO

USE [645]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 10/16/2013 15:23:20 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3), @DebitCreditFlag nvarchar(1)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 01/2011      */
/* SCAN: SEB003 */
/* Changed code to reflect business logic for Community Financial */
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 02/2011      */
/* SCAN: SEB004 */
/* Removed codes 090000 and 093000 from code */
/******************************************************************************/

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 05/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Change  Set tip to null and add date criteria  */

/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Changes to speed up process bring in only trans for this bin and current month  */

-- S Blanchete change to use RNITransaction view

TRUNCATE TABLE	transwork
TRUNCATE TABLE	transstandard
TRUNCATE TABLE	transdetailhold

SELECT	*
INTO	#tempTrans
FROM	Rewardsnow.dbo.vw_645_TRAN_SOURCE_1 with (nolock)
WHERE	Trandate <= cast(@EndDate as DATE) /* SEB005 */

INSERT INTO	transwork (TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID)
--select * from COOPWork.dbo.TransDetailHold
SELECT	TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID 
FROM	#tempTrans
/* SEB004 where left(tipnumber,3)=@tipfirst and sic<>'6011' and netid in('MCC') and processingcode in ('000000', '003000', '200030', '200000', '090000', '093000') and (left(msgtype,2) in ('02', '04')) and Trandate>=@StartDate and Trandate<=@Enddate */
/* SEB004 */ 
WHERE	sic <> '6011' and netid in('MCC') and processingcode in ('000000', '001000', '003000', '200030', '200000') and (left(msgtype,2) in ('02', '04')) 

DROP TABLE	#temptrans

UPDATE	TRANSWORK
SET		TIPNUMBER = afs.TIPNUMBER
FROM	TRANSWORK tw JOIN AFFILIAT_Stage afs on tw.PAN = afs.ACCTID

DELETE FROM	TRANSWORK
WHERE		TIPNUMBER is null

---CALC SIG DEBIT POINT VALUES
-- Signature Debit
UPDATE	transwork
SET		points = ROUND((amounttran/100), 10, 0)
--where netid in('MCC') 


--/* Added SEB003  */
--update TRANSWORK
--set POINTS=ROUND(points * (select factor from relationship where Acct=TRANSWORK.PAN), 10, 0)
--where PAN in (select Acct from relationship)


---CALC PIN DEBIT POINT VALUES
-- PIN Debit NO AWARD FOR PIN
/*
update transwork
set points=ROUND(((amounttran/100)/4), 10, 0)
where netid not in('MCI', 'VNT') 
--Put to standard transtaction file format purchases.
*/
IF	@DebitCreditFlag='C'    ---if this is a COOP DEBIT ONLY FI
	BEGIN
		-- SEB001
		INSERT INTO	TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		SELECT	tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' 
		FROM	transwork
		/* SEB004	where processingcode in ('000000', '003000', '090000', '093000') and left(msgtype,2) in ('02') */ 		        	
		/* SEB004 */	
		WHERE	processingcode in ('000000', '001000', '003000') and left(msgtype,2) in ('02') 		        	
		GROUP BY	tipnumber, Pan
	
-- SEB001
		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		SELECT	tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' 
		FROM	transwork
		WHERE	processingcode in ('200030', '200000') and left(msgtype,2) in ('02') or left(msgtype,2) in ('04')
		GROUP BY	tipnumber, Pan

--SIC BONUS AWARDS
		INSERT INTO	TransStandard
			(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		SELECT		tipnumber, @enddate, Pan, 'G2', sum(NumberOfTrans), sum(points), 'Spend Bonus', '1', ' ' 
		FROM		transwork
		WHERE		processingcode in ('000000', '003000') and left(msgtype,2) in ('02')  and SIC in ('5411','5812','5542','5541')		        	
		GROUP BY	tipnumber, Pan
	
	
END

GO


