USE [645]
GO
/****** Object:  StoredProcedure [dbo].[spStageCurrentMonthActivity]    Script Date: 11/22/2011 10:10:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageCurrentMonthActivity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageCurrentMonthActivity]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageCurrentMonthActivity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spStageCurrentMonthActivity] @EndDateParm varchar(10)
AS
/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
*/
Declare @EndDate DateTime 						--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+'' 23:59:59:990'' )		--RDT 10/09/2006 
truncate table Current_Month_Activity
insert into Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 from Customer_Stage

/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history_Stage where histdate>@enddate and ratio=''1''
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history_Stage where histdate>@enddate and ratio=''1''
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history_Stage where histdate>@enddate and ratio=''-1''
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history_Stage where histdate>@enddate and ratio=''-1''
 and History_Stage.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases' 
END
GO
