USE [250]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateYTDEarned]    Script Date: 01/21/2013 13:34:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_UpdateYTDEarned]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_UpdateYTDEarned]
GO

USE [250]
GO

/****** Object:  StoredProcedure [dbo].[usp_UpdateYTDEarned]    Script Date: 01/21/2013 13:34:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_UpdateYTDEarned]

as

  
	  
  update  [250].[dbo].[AFFILIAT]
  set YTDEarned = 0
  where YTDEarned is null
 
GO


