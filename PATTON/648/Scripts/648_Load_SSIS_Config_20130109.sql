USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'648Gerber', N'\\214249-web3\users\648Gerber\From_RN\MonthlyPoints.txt.gpg', N'\Package.Connections[FTP_MonthlyPoints.txt.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'648Gerber', N'\\patton\ops\6CO\Output\648\MonthlyPoints.txt', N'\Package.Connections[MonthlyPoints.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'648Gerber', N'\\patton\ops\6CO\Output\648\MonthlyPoints.txt', N'\Package.Connections[MonthlyPointsFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'648Gerber', N'\\patton\ops\6CO\Output\648\MonthlyPoints.txt.gpg', N'\Package.Connections[MonthlyPoints.txt.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'648Gerber', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'648Gerber', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=648;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{D5AE1BE1-E5AE-4B76-80F9-3D7892C20825}236722-SQLCLUS2\RN.648;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'648Gerber', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{0603A3FD-4F3F-4950-96F6-250DEDF500D2}236722-SQLCLUS2\RN.RewardsNow1;Auto Translate=False;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

