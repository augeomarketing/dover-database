use [648]
go

/*
   Wednesday, September 05, 20122:30:42 PM
   User: 
   Server: doolittle\rn
   Database: 648
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account_Reference_bak ADD
	DateAdded date NOT NULL CONSTRAINT DF_Account_Reference_bak_DateAdded DEFAULT getdate()
GO
ALTER TABLE dbo.Account_Reference_bak SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
