USE [648]
GO
/****** Object:  Table [dbo].[OneTimeBonuses]    Script Date: 07/23/2012 14:50:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND type in (N'U'))
DROP TABLE [dbo].[OneTimeBonuses]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OneTimeBonuses]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OneTimeBonuses](
	[TipNumber] [char](15) NOT NULL,
	[Trancode] [char](2) NULL,
	[AcctID] [char](16) NULL,
	[DateAwarded] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
