USE [648]
GO
/****** Object:  Table [dbo].[Account_Reference_Stage]    Script Date: 07/23/2012 14:50:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference_Stage](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL
) ON [PRIMARY]
END
GO
