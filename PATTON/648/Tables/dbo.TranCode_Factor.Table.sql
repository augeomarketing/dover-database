USE [648]
GO
/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 07/23/2012 14:50:42 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TranCode_Factor_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]'))
ALTER TABLE [dbo].[TranCode_Factor] DROP CONSTRAINT [FK_TranCode_Factor_TranType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND type in (N'U'))
DROP TABLE [dbo].[TranCode_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [varchar](2) NOT NULL,
	[PointFactor] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_TranCode_Factor] PRIMARY KEY CLUSTERED 
(
	[Trancode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TranCode_Factor_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]'))
ALTER TABLE [dbo].[TranCode_Factor]  WITH CHECK ADD  CONSTRAINT [FK_TranCode_Factor_TranType] FOREIGN KEY([Trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TranCode_Factor_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[TranCode_Factor]'))
ALTER TABLE [dbo].[TranCode_Factor] CHECK CONSTRAINT [FK_TranCode_Factor_TranType]
GO
