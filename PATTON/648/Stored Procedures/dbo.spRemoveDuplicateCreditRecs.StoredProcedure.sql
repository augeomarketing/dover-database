USE [648]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveDuplicateCreditRecs]    Script Date: 07/23/2012 14:54:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveDuplicateCreditRecs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spRemoveDuplicateCreditRecs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spRemoveDuplicateCreditRecs]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- 12/12 Added reference to OptOutControl file  SEB001

CREATE PROCEDURE [dbo].[spRemoveDuplicateCreditRecs]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	truncate table dbo.demographicin_Credit_work 

	insert into dbo.demographicin_Credit_work
	select *
	from dbo.DemographicIN_credit
	where pan not in (select AcctNumber from coopwork.dbo.optoutcontrol) /* SEB001 */

	truncate table dbo.DemographicIN_credit

	declare 	@PAN varchar(16),
				@AcctName varchar(40),
				@SSN varchar(9),
				@Address1 varchar(25),
				@Address2 varchar(25),
				@City varchar(25),
				@State varchar(3),
				@Zip varchar(5),
				@HomePhone varchar(10),
				@DDA varchar (16)

	/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN_CREDIT                               */
	                                                                      
	declare DEMO_crsr cursor
	for select  [PAN]
		  ,[AcctName]
		  ,[SSN]
		  ,[Address1]
		  ,[Address2]
		  ,[City]
		  ,[State]
		  ,[Zip]
		  ,[HomePhone]
		  ,[DDA]
	from dbo.demographicin_Credit_work  

	/*                                                                            */
	open DEMO_crsr

	fetch DEMO_crsr into  @PAN, @AcctName, @SSN, @Address1, @Address2, @City, @State, @Zip, @HomePhone, @DDA 
	/******************************************************************************/	
	/*                                                                            */
	/* MAIN PROCESSING  VERIFICATION                                              */
	/*                                                                            */
	if @@FETCH_STATUS = 1
		goto Fetch_Error
	/*                                                                            */
	while @@FETCH_STATUS = 0
	begin

	if @PAN not in (select pan from dbo.DemographicIN_credit) 

		begin
			insert into dbo.DemographicIN_credit (Pan, Acctname, SSN, Address1, Address2, City, State, Zip, HomePhone, DDA)
			values (@PAN, @AcctName, @SSN, @Address1, @Address2, @City, @State, @Zip, @HomePhone, @DDA)
		end
	else
		update dbo.DemographicIN_credit		
		set Acctname = @AcctName,
			SSN = @SSN,
			Address1 = @Address1,
			Address2 = @Address2,
			City = @City,
			State = @State,
			Zip = @Zip,
			HomePhone = @HomePhone,
			DDA = @DDA
		where pan=@PAN
				
		goto Next_Record
	Next_Record:
			fetch DEMO_crsr into  @PAN, @AcctName, @SSN, @Address1, @Address2, @City, @State, @Zip, @HomePhone, @DDA 

	end
	Fetch_Error:
	close  DEMO_crsr
	deallocate  DEMO_crsr


END

' 
END
GO
