USE [648]
GO

/****** Object:  StoredProcedure [dbo].[usp_PointsFileNotification]    Script Date: 02/26/2013 09:59:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_PointsFileNotification]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_PointsFileNotification]
GO

USE [648]
GO

/****** Object:  StoredProcedure [dbo].[usp_PointsFileNotification]    Script Date: 02/26/2013 09:59:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_PointsFileNotification] 
	-- Add the parameters for the stored procedure here
	@TipFirst varchar(3)
as
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @MsgTo varchar(1024)

	set @MsgTo='cardservices@gerberfcu.com;hmcguigan@rewardsnow.com;phaines@rewardsnow.com'

	insert into maintenance.dbo.perlemail
	(dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from)
	values('648 Gerber FCU Monthly Points File Upload', 'Monthly Points File has been uploaded to the FTP server', @MsgTo, 'opslogs@rewardsnow.com')

END

GO


