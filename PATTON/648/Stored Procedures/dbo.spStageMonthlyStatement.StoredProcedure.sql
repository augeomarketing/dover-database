USE [648]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 11/04/2013 14:49:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spStageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spStageMonthlyStatement]
GO

USE [648]
GO

/****** Object:  StoredProcedure [dbo].[spStageMonthlyStatement]    Script Date: 11/04/2013 14:49:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spStageMonthlyStatement]
	-- Add the parameters for the stored procedure here
	@StartDateParm char(10), 
	@EndDateParm char(10), 
	@Tipfirst char(3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


	Declare @StartDate DateTime     --RDT 10/09/2006
	Declare @EndDate DateTime     --RDT 10/09/2006
	set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
	set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

	declare @DBName varchar(100), @SQLUpdate nvarchar(max),  @MonthBucket char(10), @SQLTruncate nvarchar(max), @SQLInsert nvarchar(max), @monthbegin char(2), @SQLSelect nvarchar(max), @SQLIF nvarchar(max)


	set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
					where DBNumber=@TipFirst)

	set @monthbegin=CONVERT( int , left(@StartDateparm,2))   
	set @MonthBucket='MonthBeg' + @monthbegin

	/* Load the statement file from the customer table  */
	set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Monthly_Statement_File '
	exec sp_executesql @SQLTruncate

	set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip)
        				select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode) 
					from ' + QuoteName(@DBName) + N'.dbo.customer_Stage '
	Exec sp_executesql @SQLInsert

	IF @Tipfirst='608' or @Tipfirst='611' or @Tipfirst='615' or @Tipfirst='213'
	Begin
		set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File 
			set acctid = rtrim(b.MISC1) 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.customer_Stage b 
			where a.tipnumber = b.tipnumber '
		Exec sp_executesql @SQLUpdate
	END
	ELSE
		BEGIN
			set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File 
				set acctid = rtrim(b.acctid) 
				from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.affiliat_Stage b 
				where a.tipnumber = b.tipnumber and b.acctstatus=''A'' '
			Exec sp_executesql @SQLUpdate
		END

	set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set lastfour=right(rtrim(acctid),4) '
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with CREDIT purchases   63       */

	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchasedcr=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode=''63'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode=''63''  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   


	/* Load the statmement file with DEBIT purchases   67       */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointspurchaseddb=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode=''67'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode=''67''  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with bonuses            */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonuscr=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode=''FJ'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode=''FJ''  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* SEB002 SEB005 */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusDB=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and ((trancode like ''B%'' and trancode <> ''BX'')or trancode= ''NW'' or (trancode like ''F%'' and trancode not in (''FJ'', ''F0'', ''G0'', ''F9'', ''G9'') )) )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and ((trancode like ''B%'' and trancode <> ''BX'')or trancode= ''NW'' or (trancode like ''F%'' and trancode not in (''FJ'', ''F0'', ''G0'', ''F9'', ''G9'') ))  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   


	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusDB=pointsbonusDB-
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode=''BX'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode=''BX''  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* SEB005 add bonus for Vesdia and Access  */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusMN=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''F0'', ''G0'', ''H0'') )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode in (''F0'', ''G0'', ''H0'')  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* SEB005 add bonus for Vesdia and Access  */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbonusMN= pointsbonusMN -
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''F9'', ''G9'', ''H9'') )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode in (''F9'', ''G9'', ''H9'')  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with plus adjustments    */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsadded=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''IE'', ''PP'', ''TP'') )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode in (''IE'', ''PP'', ''TP'')  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with total point increases SEB005 */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsincreased=pointspurchasedcr + pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded + pointsbonusMN '
		Exec sp_executesql @SQLUpdate

	/* Load the statmement file with redemptions          */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and ( trancode like ''R%'' or  trancode = ''IR'') )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and ( trancode like ''R%'' or  trancode = ''IR'')  ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file decreased redeemed         */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsredeemed=pointsredeemed-
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode = ''DR'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode = ''DR'' ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with CREDIT returns            */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturnedcr=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode = ''33'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode = ''33'' ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with DEBIT returns            */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsreturneddb=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode = ''37'' )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode = ''37'' ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with minus adjustments   */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointssubtracted=
			(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode in (''DE'', ''XP'') )
				where exists(select * from ' + QuoteName(@DBName) + N'.dbo.History_Stage where tipnumber=' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File.tipnumber
				and histdate>=@startdate and histdate<=@enddate and trancode in (''DE'', ''XP'') ) '
	Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate = @StartDate, @EndDate = @EndDate   

	/* Load the statmement file with total point decreases */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsdecreased=pointsredeemed + pointsreturnedcr + pointsreturneddb + pointssubtracted '
	Exec sp_executesql @SQLUpdate

	/* Load the statmement file with the Beginning balance for the Month */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsbegin = b.' + Quotename(@MonthBucket) + N' 
			from ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File a, ' + QuoteName(@DBName) + N'.dbo.Beginning_Balance_Table b
			where a.tipnumber = b.tipnumber '
	exec sp_executesql @SQLUpdate

	/* Load the statmement file with ending points */
	set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Monthly_Statement_File set pointsend=pointsbegin + pointsincreased - pointsdecreased '
	exec sp_executesql @SQLUpdate	
	
END


GO


