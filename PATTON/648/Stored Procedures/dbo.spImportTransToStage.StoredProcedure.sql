USE [648]
GO
/****** Object:  StoredProcedure [dbo].[spImportTransToStage]    Script Date: 07/23/2012 14:54:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportTransToStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportTransToStage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

/*  
Import Transactions into History_Stage and Customer_Stage and Affiliat_Stage from TransStandard  
Update Customer_Stage totals
*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[spImportTransToStage] @TipFirst char(3), @enddate char(10)
AS 
Declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
Declare @dbName varchar(50) 
Declare @SQLStmt nvarchar(2000) 
/*    Get Database name                                      */
set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )
/*    Get max Points per year from client table                               */
set @MaxPointsPerYear = ( Select MaxPointsPerYear from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst ) 
/***************************** HISTORY_STAGE *****************************/
----  Insert TransStandard into History_stage 
 insert into history_Stage 	
	(Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, Ratio, Secid, overage) 	
	select TFNO, Acct_num, Trandate, Trancode, Trannum, TranAmt, TranType, Ratio, ''NEW'', 0 From TransStandard
/*  Update History_stage points and overage if over MaxPointsPerYear */
-- Calc overage
If @MaxPointsPerYear > 0 
Begin 
	update History_Stage
		set Overage = (H.Points * H.Ratio) - ( @MaxPointsPerYear - A.ytdearned  ) 
		FROM History_Stage H JOIN Affiliat_Stage A on H.Tipnumber = A.Tipnumber 
		and  A.AcctID = H.AcctID
		where A.YTDEarned + H.Points > @MaxPointsPerYear 
End
/***************************** AFFILIAT_STAGE -ROLLUP BY TIP*****************************/
/*
-- Update Affiliat YTDEarned 
Update Affiliat_Stage
	set YTDEarned  = A.YTDEarned  + H.Points 
	FROM HISTORY_STAGE H JOIN AFFILIAT_Stage A on H.Tipnumber = A.Tipnumber
-- Update History_Stage Points = Points - Overage
Update History_Stage 
	Set Points = Points - Overage where Points > Overage
*/
/***************************** AFFILIAT_STAGE -ROLLUP BY CARD (AcctID)*****************************/
-- Update Affiliat_stage YTDEarned 
SELECT     A.ACCTID, A.Tipnumber, SUM(H.POINTS*H.Ratio) AS AcctIDPoints
into #AP
FROM         AFFILIAT_stage A INNER JOIN
                      HISTORY_stage H ON A.ACCTID = H.ACCTID AND A.TIPNUMBER = H.TIPNUMBER
		where H.SecID=''NEW''
		GROUP BY A.ACCTID, A.Tipnumber
---Join on the temp table and update 
Update Affiliat_stage
	set YTDEarned=YTDEarned +  AP.AcctIDPoints
FROM         AFFILIAT_stage A INNER JOIN
                      #AP AP ON A.ACCTID = AP.ACCTID AND A.TIPNUMBER = AP.TIPNUMBER
Drop table #AP
---?????? why 
Update History_Stage 
	Set Points = Points - Overage where Points > Overage
/***************************** CUSTOMER_STAGE *****************************/
/* Update the Customer_Stage RunAvailable  with new history points  */ 
Update Customer_Stage Set RunAvaliableNew  = 0 where RunAvaliableNew  is null
Update Customer_Stage Set RunAvailable  = 0 where RunAvailable is null
if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[vw_histpoints]'') and OBJECTPROPERTY(id, N''IsView'') = 1)
drop view [dbo].[vw_histpoints]
/* Create View */
set @SQLStmt = ''Create view vw_histpoints as select tipnumber, sum(points*ratio) as points from history_stage where secid = ''''NEW''''group by tipnumber''
exec sp_executesql @SQLStmt
Update customer_stage 
Set RunAvailable  = RunAvailable + v.Points 
From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber
Update customer_stage 
Set RunAvaliableNew  = v.Points 
From Customer_Stage C join vw_histpoints V on C.Tipnumber = V.Tipnumber
drop view [dbo].[vw_histpoints]


' 
END
GO
