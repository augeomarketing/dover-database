USE [648]
GO
/****** Object:  StoredProcedure [dbo].[sp648ParseAndUpdateCreditNames]    Script Date: 07/23/2012 14:54:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp648ParseAndUpdateCreditNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp648ParseAndUpdateCreditNames]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp648ParseAndUpdateCreditNames]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[sp648ParseAndUpdateCreditNames] AS


select pan,acctname, ''                                        '' as tmp, NULL as HasMI, ''                   '' as firstname, ''                    '' as lastname, null as commapos
into #tmp from DemographicIN_credit
--select * from #tmp
update #tmp set acctname=RTRIM(ltrim(acctname))
update #tmp set commapos= charindex('','',AcctName)
update #tmp set 
	lastname=SUBSTRING(acctname,1,commapos-1),
	tmp=charindex('' '',AcctName, commapos+1)

update #tmp set tmp=SUBSTRING(acctname,commapos+1,len(acctname)-commapos+1)
update #tmp set tmp=LTRIM(rtrim(tmp))
update #tmp set HasMI=charindex('' '',tmp)
update #tmp set tmp=SUBSTRING (tmp,1,HasMI) where HasMI>0
update #tmp set firstname=tmp
update #tmp set tmp=firstname + '' '' + lastname

--update #tmp set firstname=ltrim(rtrim(SUBSTRING(acctname,commapos+1,len(acctname) - commapos))) where tmp=0
--update #tmp set firstname=ltrim(SUBSTRING(acctname,commapos+1,len(acctname) - charindex('' '',AcctName, commapos))) where tmp>0
--update #tmp set firstname=SUBSTRING(firstname,1,charindex('' '',firstname)) where tmp > 0


select * from #tmp
select * from DemographicIN_credit




update D
set 
	D.RNAcctName=T.tmp,
	D.FirstName=T.FirstName,
	D.LastName=T.Lastname
from DemographicIn_CREDIT D join #tmp T on T.AcctName=D.AcctName

drop table #tmp


' 
END
GO
