USE [648]
GO
/****** Object:  View [dbo].[vwCustomer_stage]    Script Date: 07/23/2012 14:52:39 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
DROP VIEW [dbo].[vwCustomer_stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomer_stage]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomer_stage]
			 as
			 Select *
			  from [219DeanBank].dbo.Customer_stage
'
GO
