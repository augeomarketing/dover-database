USE [648]
GO
/****** Object:  View [dbo].[vwCustomerdeleted]    Script Date: 07/23/2012 14:52:39 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomerdeleted]'))
DROP VIEW [dbo].[vwCustomerdeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vwCustomerdeleted]'))
EXEC dbo.sp_executesql @statement = N'create view [dbo].[vwCustomerdeleted]
			 as
			 select *
			  from [219DeanBank].dbo.Customerdeleted
'
GO
