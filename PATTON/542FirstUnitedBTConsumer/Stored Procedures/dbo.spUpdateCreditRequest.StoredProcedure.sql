USE [542FirstUnitedBTConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCreditRequest]    Script Date: 09/25/2009 11:33:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- RDT 12/21/06 -- Added code to get only Credit Cards. 
-- Need code to check for blank accounts (missing credit cards)

CREATE PROCEDURE [dbo].[spUpdateCreditRequest] AS  

declare @NewRecordNumber as numeric
declare @RecordNumber as numeric
declare @recnumber int
set @NewRecordNumber = 0
truncate table CreditRequest
/* Load CreditRequest Table with  rows from History */
Insert into CreditRequest 
	(TipNumber, HistDate, Value, Trancode, CatalogDesc)
	select 
	TipNumber,HistDate, cast(left(tipnumber,3)as nvarchar) + cast(points as nvarchar)   ,Trancode,Description 
        from history where acctid is null  and trancode = 'RB'

/* Load CreditRequest table with customer information  */
Update CreditRequest 
set 	CreditRequest.ACCTNAME1 = Customer.ACCTNAME1 ,
	CreditRequest.ACCTNAME2 = Customer.ACCTNAME2,
	CreditRequest.ADDRESS1  = Customer.ADDRESS1,
	CreditRequest.ADDRESS2  = Customer.ADDRESS2,
	CreditRequest.City      = Customer.City,
	CreditRequest.State     = Customer.State,
	CreditRequest.ZipCode   = Customer.ZipCode,
	CreditRequest.HOMEPHONE = Customer.HomePhone
from CreditRequest, Customer
where CreditRequest.Tipnumber = CUSTOMER.Tipnumber and  CreditRequest.SentToFi is null

/* Update CreditRequest Table with the Request Type  if SentToFI is null   */
/*  1= Account credit 2=Check requested */
  Update CreditRequest set RebateType = '1', productcode = '01' where SentToFi is null  


/* Update CreditRequest Table with AcctID if SentToFI is null   */
Update CreditRequest 
set acctId = right(Affiliat.Acctid,4)
from CreditRequest, Affiliat
-- rdt 12/21/06 where CreditRequest.Tipnumber = Affiliat.Tipnumber and  CreditRequest.SentToFi is null
where CreditRequest.Tipnumber = Affiliat.Tipnumber and  CreditRequest.SentToFi is null 


/*Update CreditRequest */
Update CreditRequest 
set  CreditRequest.value  =  Fullfillment.dbo.subcashbackvalue.cashvalue
from Fullfillment.dbo.subcashbackvalue
where  CreditRequest.value =  Fullfillment.dbo.subcashbackvalue.tipfirstpoints

/* Update CreditRequest Table recordnumber column with 0 if null  */
Update CreditRequest set recordnumber = 0  where recordnumber is null


 	set @recnumber = '0'

	
	Update CreditRequest
	 set
	 @recnumber = @recnumber + 1,
	 RecordNumber = @recnumber  
	where 
	     SentToFI is null
	
		

SET QUOTED_IDENTIFIER OFF
GO
