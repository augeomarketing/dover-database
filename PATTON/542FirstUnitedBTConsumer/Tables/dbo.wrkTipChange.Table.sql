USE [542FirstUnitedBTConsumer]
GO
/****** Object:  Table [dbo].[wrkTipChange]    Script Date: 09/25/2009 11:33:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkTipChange](
	[oldtip] [nchar](15) NULL,
	[newtip] [nchar](15) NULL
) ON [PRIMARY]
GO
