USE [542FirstUnitedBTConsumer]
GO
/****** Object:  Table [dbo].[CommercialCardsinConsumerTip]    Script Date: 09/25/2009 11:33:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommercialCardsinConsumerTip](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctid] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
