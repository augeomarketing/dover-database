USE [542FirstUnitedBTConsumer]
GO
/****** Object:  Table [dbo].[FixPoints]    Script Date: 09/25/2009 11:33:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FixPoints](
	[tipnumber] [varchar](15) NOT NULL,
	[PointsGiven] [numeric](38, 0) NULL,
	[Dollars] [numeric](38, 0) NULL,
	[CorrectPoints] [numeric](38, 1) NULL,
	[PointsDue] [numeric](38, 0) NULL,
	[Registered] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
