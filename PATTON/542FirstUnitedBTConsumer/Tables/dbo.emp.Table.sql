USE [542FirstUnitedBTConsumer]
GO
/****** Object:  Table [dbo].[emp]    Script Date: 09/25/2009 11:33:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emp](
	[Name] [nvarchar](255) NULL,
	[Street Line 1] [nvarchar](255) NULL,
	[F3] [nvarchar](255) NULL,
	[Acctname1] [nvarchar](255) NULL,
	[acctname2] [nvarchar](255) NULL,
	[address1] [nvarchar](255) NULL,
	[tipnumber] [nvarchar](255) NULL
) ON [PRIMARY]
GO
