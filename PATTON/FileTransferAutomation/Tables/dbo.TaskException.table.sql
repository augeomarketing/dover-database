USE [FileTransferAutomation]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_TaskException_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TaskException] DROP CONSTRAINT [DF_TaskException_DateAdded]
END

GO

USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[TaskException]    Script Date: 02/22/2016 12:43:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskException]') AND type in (N'U'))
DROP TABLE [dbo].[TaskException]
GO

USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[TaskException]    Script Date: 02/22/2016 12:43:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TaskException](
	[TaskExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[TaskID] [int] NULL,
	[RunDate] [datetime] NULL,
	[ExceptionMessage] [varchar](1024) NULL,
	[DateAdded] [datetime] NULL,
 CONSTRAINT [PK_TaskException] PRIMARY KEY CLUSTERED 
(
	[TaskExceptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TaskException] ADD  CONSTRAINT [DF_TaskException_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


