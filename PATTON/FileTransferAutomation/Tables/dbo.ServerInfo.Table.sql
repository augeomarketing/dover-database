USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[ServerInfo]    Script Date: 8/7/2015 1:33:56 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ServerInfo]') AND type in (N'U'))
DROP TABLE [dbo].[ServerInfo]
GO

SET ANSI_PADDING ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ServerInfo](
	[ServerInfoID] [int] IDENTITY(1,1) NOT NULL,
	[Hostname] [varchar](255) NOT NULL,
	[Username] [varchar](255) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Password] [varbinary](1024) NOT NULL,
	[HostKey] [varchar](100) NULL,
	[Protocol] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ServerInfo] PRIMARY KEY CLUSTERED 
(
	[ServerInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ServerInfo]  WITH CHECK ADD  CONSTRAINT [chk_Protocol] CHECK  (([Protocol]='FTP' OR [Protocol]='SFTP'))
GO

ALTER TABLE [dbo].[ServerInfo] CHECK CONSTRAINT [chk_Protocol]
GO



