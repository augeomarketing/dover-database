USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[Schedule]    Script Date: 8/7/2015 1:05:42 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Schedule]') AND type in (N'U'))
DROP TABLE [dbo].[Schedule]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Schedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[Frequency] [varchar](50) NOT NULL,
	[RecurrenceNumber] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[RunOnMonTF] [bit] NULL,
	[RunOnTueTF] [bit] NULL,
	[RunOnWedTF] [bit] NULL,
	[RunOnThuTF] [bit] NULL,
	[RunOnFriTF] [bit] NULL,
	[RunOnSatTF] [bit] NULL,
	[RunOnSunTF] [bit] NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[LastRunDateTime] [datetime] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [chk_Frequency] CHECK  (([Frequency]='Weekly' OR [Frequency]='Daily' OR [Frequency]='One time'))
GO

ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [chk_Frequency]
GO



