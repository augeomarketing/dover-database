USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[TaskEmail]    Script Date: 11/17/2015 3:21:36 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskEmail]') AND type in (N'U'))
DROP TABLE [dbo].[TaskEmail]
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TaskEmail](
	[TaskID] [int] NOT NULL,
	[EmailID] [int] NOT NULL,
	[RecipientTypeID] [int] NOT NULL,
 CONSTRAINT [PK_TaskEmail] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC,
	[EmailID] ASC,
	[RecipientTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TaskEmail]  WITH CHECK ADD  CONSTRAINT [FK_TaskEmail_Email] FOREIGN KEY([EmailID])
REFERENCES [dbo].[Email] ([EmailID])
GO

ALTER TABLE [dbo].[TaskEmail] CHECK CONSTRAINT [FK_TaskEmail_Email]
GO

ALTER TABLE [dbo].[TaskEmail]  WITH CHECK ADD  CONSTRAINT [FK_TaskEmail_RecipientType] FOREIGN KEY([RecipientTypeID])
REFERENCES [dbo].[RecipientType] ([RecipientTypeID])
GO

ALTER TABLE [dbo].[TaskEmail] CHECK CONSTRAINT [FK_TaskEmail_RecipientType]
GO

ALTER TABLE [dbo].[TaskEmail]  WITH CHECK ADD  CONSTRAINT [FK_TaskEmail_Task] FOREIGN KEY([TaskID])
REFERENCES [dbo].[Task] ([TaskID])
GO

ALTER TABLE [dbo].[TaskEmail] CHECK CONSTRAINT [FK_TaskEmail_Task]
GO



