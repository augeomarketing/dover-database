USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[TransferredFile]    Script Date: 8/7/2015 1:50:26 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TransferredFile]') AND type in (N'U'))
DROP TABLE [dbo].[TransferredFile]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TransferredFile](
	[FileTransferredID] [int] IDENTITY(1,1) NOT NULL,
	[Filename] [varchar](255) NOT NULL,
	[TaskHistoryID] [int] NOT NULL,
 CONSTRAINT [PK_TransferredFile] PRIMARY KEY CLUSTERED 
(
	[FileTransferredID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TransferredFile]  WITH CHECK ADD  CONSTRAINT [FK_TransferredFile_TaskHistory] FOREIGN KEY([TaskHistoryID])
REFERENCES [dbo].[TaskHistory] ([TaskHistoryID])
GO

ALTER TABLE [dbo].[TransferredFile] CHECK CONSTRAINT [FK_TransferredFile_TaskHistory]
GO



