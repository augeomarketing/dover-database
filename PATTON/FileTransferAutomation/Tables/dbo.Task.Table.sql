USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[Task]    Script Date: 8/7/2015 1:46:03 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Task]') AND type in (N'U'))
DROP TABLE [dbo].[Task]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Task](
	[TaskID] [int] IDENTITY(1,1) NOT NULL,
	[TaskName] [varchar](255) NOT NULL,
	[RemotePath] [varchar](max) NOT NULL,
	[LocalPath] [varchar](max) NOT NULL,
	[SourceLocation] [varchar](20) NOT NULL,
	[DestinationLocation] [varchar](20) NOT NULL,
	[Aftermath] [varchar](50) NOT NULL,
	[ServerInfoID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ScheduleID] [int] NOT NULL,
	[ArchivePath] [varchar](max) NULL,
	[ActiveYN] [bit] NOT NULL CONSTRAINT [DF_Task_ActiveYN]  DEFAULT ((1)),
	[SourceFileMatch] [varchar](100) NULL,
	[LastModifiedBy] [varchar](100) NOT NULL,
	[ExecuteNow] [bit] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Schedule]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_ServerInfo] FOREIGN KEY([ServerInfoID])
REFERENCES [dbo].[ServerInfo] ([ServerInfoID])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_ServerInfo]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [chk_Aftermatch] CHECK  (([Aftermath]='Leave Alone' OR [Aftermath]='Archive' OR [Aftermath]='Delete'))
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [chk_Aftermatch]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [chk_Destination] CHECK  (([DestinationLocation]='Local' OR [DestinationLocation]='Remote'))
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [chk_Destination]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [chk_Source] CHECK  (([SourceLocation]='Local' OR [SourceLocation]='Remote'))
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [chk_Source]
GO



