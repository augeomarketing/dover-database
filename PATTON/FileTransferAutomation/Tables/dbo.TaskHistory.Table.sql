USE [FileTransferAutomation]
GO

/****** Object:  Table [dbo].[TaskHistory]    Script Date: 8/7/2015 1:48:35 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TaskHistory]') AND type in (N'U'))
DROP TABLE [dbo].[TaskHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TaskHistory](
	[TaskHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[TaskID] [int] NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[ErrorOccurred] [bit] NOT NULL,
	[ErrorMessage] [varchar](1024) NULL,
	[EmailSentYN] [bit] NOT NULL CONSTRAINT [DF_TaskHistory_EmailSentYN]  DEFAULT ((0)),
 CONSTRAINT [PK_TaskHistory] PRIMARY KEY CLUSTERED 
(
	[TaskHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



