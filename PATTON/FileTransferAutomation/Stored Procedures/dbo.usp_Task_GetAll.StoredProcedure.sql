USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Task_GetAll]    Script Date: 8/7/2015 3:03:13 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Task_GetAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Task_GetAll]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.28.2015
-- Description:	Gets all tasks
-- =============================================
CREATE PROCEDURE [dbo].[usp_Task_GetAll] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TaskID,
		TaskName,
		RemotePath,
		LocalPath,
		SourceLocation,
		DestinationLocation,
		Aftermath,
		ServerInfoID,
		dbo.Task.ScheduleID,
		Frequency,
		RecurrenceNumber,
		StartDate,
		LastRunDateTime,
		RunOnMonTF,
		RunOnTueTF,
		RunOnWedTF,
		RunOnThuTF,
		RunOnFriTF,
		RunOnSatTF,
		RunOnSunTF,
		ISNULL(ArchivePath, '') AS ArchivePath,
		ISNULL(SourceFileMatch, '') AS SourceFileMatch,
		LastModifiedBy,
		Task.ModifiedDate,
		ActiveYN,
		ExecuteNow
	FROM
		dbo.Task
	INNER JOIN
		dbo.Schedule ON dbo.Schedule.ScheduleID = dbo.Task.ScheduleID
	ORDER BY 
		TaskName
END

GO



