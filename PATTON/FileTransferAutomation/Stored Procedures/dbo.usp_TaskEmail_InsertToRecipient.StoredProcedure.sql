USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskEmail_InsertToRecipient]    Script Date: 11/17/2015 3:15:11 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskEmail_InsertToRecipient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskEmail_InsertToRecipient]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/16/2015
-- Description:	Adds a new to recipient address for given task.
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskEmail_InsertToRecipient] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER, 
	@EmailID INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.TaskEmail
	(
		TaskID,
		EmailID,
		RecipientTypeID
	)
	SELECT
		@TaskID,
		@EmailID,
		RecipientTypeID
	FROM
		dbo.RecipientType
	WHERE
		[Name] = 'To'


END

GO



