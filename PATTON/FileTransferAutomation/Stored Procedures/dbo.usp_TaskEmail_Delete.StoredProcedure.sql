USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskEmail_Delete]    Script Date: 11/17/2015 2:37:06 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskEmail_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskEmail_Delete]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/16/2015
-- Description:	Deletes given recipient from given task notifications.
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskEmail_Delete] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER, 
	@EmailID INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM dbo.TaskEmail WHERE TaskID = @TaskID AND EmailID = @EmailID
END

GO



