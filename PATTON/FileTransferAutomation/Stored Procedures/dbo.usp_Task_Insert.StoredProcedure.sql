USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Task_Insert]    Script Date: 8/7/2015 3:05:04 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Task_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Task_Insert]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.8.2015
-- Description:	Inserts new task into table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Task_Insert] 
	-- Add the parameters for the stored procedure here
	@TaskName VARCHAR(255), 
	@RemotePath VARCHAR(MAX),
	@LocalPath VARCHAR(MAX),
	@SourceLocation VARCHAR(20),
	@DestinationLocation VARCHAR(20),
	@Aftermath VARCHAR(50),
	@ServerInfoID INTEGER,
	@ScheduleID INTEGER,
	@ArchivePath VARCHAR(MAX) = NULL,
	@SourceFileMatch VARCHAR(100) = NULL,
	@LastModifiedBy VARCHAR(100),
	@ActiveYN BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

        -- Insert statements for procedure here
        DECLARE @CurrentDate DATETIME = GETDATE()
	DECLARE @NextTaskID INTEGER

	INSERT INTO dbo.Task
	(
		TaskName,
		RemotePath,
		LocalPath,
		SourceLocation,
		DestinationLocation,
		Aftermath,
		ServerInfoID,
		ModifiedDate,
		CreateDate,
		ScheduleID,
		ArchivePath,
		SourceFileMatch,
		LastModifiedBy,
		ActiveYN,
		ExecuteNow
	)
	VALUES
	(
		@TaskName,
		@RemotePath,
		@LocalPath,
		@SourceLocation,
		@DestinationLocation,
		@Aftermath,
		@ServerInfoID,
		@CurrentDate,
		@CurrentDate,
		@ScheduleID,
		@ArchivePath,
		@SourceFileMatch,
		@LastModifiedBy,
		@ActiveYN,
		0
	)

	SELECT @NextTaskID = SCOPE_IDENTITY()

	INSERT INTO dbo.TaskEmail
	(
		TaskID,
		EmailID,
		RecipientTypeID
	)
	VALUES
	(
		@NextTaskID,
		1,	-- To
		1	-- AutomatedFTPWatchers group
	)

	SELECT @NextTaskID AS TaskID
		

END

GO

