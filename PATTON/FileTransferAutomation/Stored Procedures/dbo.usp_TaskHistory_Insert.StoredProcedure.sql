USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskHistory_Insert]    Script Date: 8/7/2015 3:18:14 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskHistory_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskHistory_Insert]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.24.2015
-- Description:	Inserts new row into TaskHistory table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskHistory_Insert] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER, 
	@RunDate DATETIME,
	@ErrorOccurred BIT,
	@ErrorMessage VARCHAR(1024)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.TaskHistory
	(
		TaskID,
		RunDate,
		ErrorOccurred,
		ErrorMessage
	)
	VALUES
	(
		@TaskID,
		@RunDate,
		@ErrorOccurred,
		@ErrorMessage
	)

	SELECT SCOPE_IDENTITY() AS TaskHistoryID

END

GO



