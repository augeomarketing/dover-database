USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Schedule_Update]    Script Date: 8/7/2015 1:56:42 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Schedule_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Schedule_Update]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.8.2015
-- Description:	Updates schedule data
-- =============================================
CREATE PROCEDURE [dbo].[usp_Schedule_Update] 
	-- Add the parameters for the stored procedure here
	@ScheduleID INTEGER, 
	@Frequency VARCHAR(50),
	@RecurrenceNumber INTEGER,
	@StartDate DATETIME,
	@RunOnMon BIT = NULL,
	@RunOnTue BIT = NULL,
	@RunOnWed BIT = NULL,
	@RunOnThu BIT = NULL,
	@RunOnFri BIT = NULL,
	@RunOnSat BIT = NULL,
	@RunOnSun BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATETIME = GETDATE()
	DECLARE @TempStartDate DATETIME
	SET @TempStartDate = (SELECT StartDate FROM dbo.Schedule WHERE ScheduleID = @ScheduleID)

	IF @TempStartDate <> @StartDate
	BEGIN
		UPDATE dbo.Schedule SET Frequency = @Frequency, RecurrenceNumber = @RecurrenceNumber,
							StartDate = @StartDate, RunOnMonTF = @RunOnMon, RunOnTueTF = @RunOnTue,
							RunOnWedTF = @RunOnWed, RunOnThuTF = @RunOnThu, RunOnFriTF = @RunOnFri,
							RunOnSatTF = @RunOnSat, RunOnSunTF = @RunOnSun, ModifiedDate = @CurrentDate,
							LastRunDateTime = NULL
		WHERE ScheduleID = @ScheduleID
	END
	ELSE
	BEGIN
		UPDATE dbo.Schedule SET Frequency = @Frequency, RecurrenceNumber = @RecurrenceNumber,
							StartDate = @StartDate, RunOnMonTF = @RunOnMon, RunOnTueTF = @RunOnTue,
							RunOnWedTF = @RunOnWed, RunOnThuTF = @RunOnThu, RunOnFriTF = @RunOnFri,
							RunOnSatTF = @RunOnSat, RunOnSunTF = @RunOnSun, ModifiedDate = @CurrentDate
		WHERE ScheduleID = @ScheduleID
	END
END

GO



