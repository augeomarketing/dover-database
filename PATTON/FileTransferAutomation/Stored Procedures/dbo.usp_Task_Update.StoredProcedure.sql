USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Task_Update]    Script Date: 8/7/2015 3:07:21 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Task_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Task_Update]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.9.2015
-- Description:	Updates the task info for record with given taskid.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Task_Update] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER, 
	@TaskName VARCHAR(255),
	@RemotePath VARCHAR(MAX),
	@LocalPath VARCHAR(MAX),
	@SourceLocation VARCHAR(20),
	@DestinationLocation VARCHAR(20),
	@Aftermath VARCHAR(50),
	@ServerInfoID INTEGER,
	@ArchivePath VARCHAR(255) = NULL,
	@SourceFileMatch VARCHAR(100) = NULL,
	@LastModifiedBy VARCHAR(100),
	@ActiveYN BIT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATETIME = GETDATE()

	UPDATE dbo.Task SET TaskName = @TaskName, RemotePath = @RemotePath, LocalPath = @LocalPath,
	                    SourceLocation = @SourceLocation, DestinationLocation = @DestinationLocation,
						Aftermath = @Aftermath, ServerInfoID = @ServerInfoID, ArchivePath = @ArchivePath,
						ModifiedDate = @CurrentDate, LastModifiedBy = @LastModifiedBy, SourceFileMatch = @SourceFileMatch, ActiveYN = @ActiveYN
	WHERE TaskID = @TaskID
		
END

GO



