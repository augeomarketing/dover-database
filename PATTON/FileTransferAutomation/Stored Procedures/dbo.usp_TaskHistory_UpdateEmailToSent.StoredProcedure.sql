USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskHistory_UpdateEmailToSent]    Script Date: 2/10/2016 1:00:39 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskHistory_UpdateEmailToSent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskHistory_UpdateEmailToSent]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas T. Parsons
-- Create date: 2/10/2016
-- Description:	Updates EmailSentYN field in TaskHistory table.
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskHistory_UpdateEmailToSent] 
	-- Add the parameters for the stored procedure here
	@TaskHistoryID INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.TaskHistory SET EmailSentYN = 1 WHERE TaskHistoryID = @TaskHistoryID

END

GO



