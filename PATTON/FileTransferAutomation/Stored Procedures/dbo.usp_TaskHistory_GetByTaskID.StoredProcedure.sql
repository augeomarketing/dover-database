USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskHistory_GetByTaskID]    Script Date: 8/7/2015 3:09:22 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskHistory_GetByTaskID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskHistory_GetByTaskID]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.24.2015
-- Description:	Gets task history for given task id
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskHistory_GetByTaskID] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TaskID,
		RunDate,
		[Filename],
		ErrorOccurred,
		ErrorMessage
	FROM
		dbo.TaskHistory AS TH
	LEFT OUTER JOIN
		dbo.TransferredFile AS TF ON TH.TaskHistoryID = TF.TaskHistoryID
	WHERE
		TaskID = @TaskID
	ORDER BY
		RunDate DESC 
END

GO



