USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskHistory_Get]    Script Date: 11/17/2015 3:17:01 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskHistory_Get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskHistory_Get]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/16/2015
-- Description:	Gets all task history for current day.
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskHistory_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TH.TaskID,
		T.TaskName,
		RunDate,
		ErrorOccurred,
		ErrorMessage,
		[Filename],
		EmailSentYN,
		TH.TaskHistoryID
	FROM
		dbo.TaskHistory AS TH
	LEFT OUTER JOIN
		dbo.TransferredFile AS TF ON TH.TaskHistoryID = TF.TaskHistoryID
	INNER JOIN dbo.Task AS T ON T.TaskID = TH.TaskID
	WHERE 
		CONVERT(DATE, RunDate) = CONVERT(DATE, GETDATE())
	
END

GO



