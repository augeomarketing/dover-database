USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Task_UpdateExecuteNow]    Script Date: 11/17/2015 2:35:52 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Task_UpdateExecuteNow]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Task_UpdateExecuteNow]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/13/2015
-- Description:	Updates "ExecuteNow" field of task table
--              for given task id.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Task_UpdateExecuteNow] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER,
	@ExecuteNow BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Task SET ExecuteNow = @ExecuteNow WHERE TaskID = @TaskID
END

GO



