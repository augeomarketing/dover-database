USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TransferredFile_Insert]    Script Date: 8/7/2015 3:37:17 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TransferredFile_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TransferredFile_Insert]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.24.2015
-- Description:	Inserts new records into TransferredFile table.
--
-- Sample XML passed:
--   <TransferredFile>
--      <Row taskhistoryid="44" filename="foobar.txt" />
--      <Row taskhistoryid="122" filename="acme.doc" />
--   </TransferredFile>
-- =============================================
CREATE PROCEDURE [dbo].[usp_TransferredFile_Insert] 
	-- Add the parameters for the stored procedure here
	@FilesToInsert XML
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO TransferredFile
	(
		TaskHistoryID,
		[Filename]
	)
	SELECT
		FilesToInsert.x.value('@taskhistoryid', 'int') AS TaskHistoryID,
		FilesToInsert.x.value('@filename', 'varchar(1024)') AS [Filename]
	FROM
		@FilesToInsert.nodes('/TransferredFile/Row') AS FilesToInsert(x)


END

GO



