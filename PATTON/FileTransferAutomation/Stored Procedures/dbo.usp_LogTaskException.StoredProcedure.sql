USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_LogTaskException]    Script Date: 02/22/2016 10:41:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_LogTaskException]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_LogTaskException]
GO

USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_LogTaskException]    Script Date: 02/22/2016 10:41:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ============================================
/*
this is called from a trigger on the Task history table 
and logs any tasks that don't move any files or that have an error
within 'N' days of being run. 

-- perlemail check
--select top 10 dim_perlemail_sent,dim_perlemail_created,* from Maintenance.dbo.perlemail order by sid_perlemail_id desc
-- truncate table TaskException
-- select * from TaskException
*/

/* sample call
declare @TaskID int = 14
exec usp_LogTaskException @TaskID
*/
-- =============================================
CREATE PROCEDURE [dbo].[usp_LogTaskException]
	-- Add the parameters for the stored procedure here
	@TaskID int
AS
BEGIN

	SET NOCOUNT ON;

	--declare @TaskID int=14
	--select * from Task where TaskID=@TaskID
	declare @TaskName varchar(255), @RunDate datetime, @TaskHistoryID int,@FileName varchar(255), @ErrorOccurred bit, @ErrorMessage varchar(1024)
	
	SELECT top 1 
	@TaskName=Task.TaskName
	,@RunDate=TH.RunDate
	,@TaskHistoryID=TH.TaskHistoryID
	,@FileName=TF.Filename 
	,@ErrorOccurred=TH.ErrorOccurred
	,@ErrorMessage=TH.ErrorMessage
	FROM Task join TaskHistory TH on Task.TaskID=TH.TaskID
			left join TransferredFile TF on TH.TaskHistoryID=TF.TaskHistoryID
	where Task.TaskID=@TaskID
	and TH.RunDate > getdate()-1
	and isnull(TF.Filename,'') = ''
/* test for above query
	--declare @TaskID int=14	
	SELECT top 1 TaskName,TH.RunDate,	TH.TaskHistoryID,TF.Filename ,TH.ErrorOccurred,	TH.ErrorMessage	FROM Task join TaskHistory TH on Task.TaskID=TH.TaskID left join TransferredFile TF on TH.TaskHistoryID=TF.TaskHistoryID where Task.TaskID=@TaskID	
	and TH.RunDate > getdate()-1 and isnull(TF.Filename,'') = ''
	
	
	
*/

---------------------------------------------------------------
--------Load variables, check and if warranted, insert PerlEmail
-------------------------------

	declare @Sub	Varchar (255),
	@Body	Varchar(6144)		,
	@To		Varchar(1024)		,
	@From	Varchar  (50)		,
	@bcc	Int				= 0	,
	@Att	varchar(1024)	= null,
	----
	@EmailRecipients	varchar(1024)
	
	IF @FileName IS NULL 
	begin
		-- set Perl Email  variable values
		set @Sub = 'NO FILE TRANSFERRED for  - ' + @TaskName +  ' at ' + cast(@RunDate  as varchar)
		set @Body = 'No file was transferred for the task  - ' + @TaskName +  ' at ' + cast(@RunDate  as varchar)
		set @From='AutomatedFTPWatchers@RewardsNow.com'
		-- look up the CarbonCopy email addresses associated with this task and put into a comma separated string
				select 
				@EmailRecipients = STUFF((
				select 
				',' + E.Address
				from Task T
					join TaskEmail M on T.TaskID=M.TaskID
					join Email E on M.EmailID=E.EmailID
					join RecipientType R on M.RecipientTypeID=R.RecipientTypeID
				where T.TaskID=14	and R.RecipientTypeID=2
					
				FOR XML PATH('')
				)
				,1,1,'') 
	
	
	--insert perlemail
		--print '@Sub:' + @Sub + ' | '+ '@Body:' + @Body + ' | '+ '@EmailRecipients:' + @EmailRecipients + ' | '
		exec rewardsnow.dbo.spPerlemail_insert @Sub,@Body,@EmailRecipients,@From,@bcc, @Att
	-- insert the TaskException Record	
		insert Into TaskException(TaskID, RunDate, ExceptionMessage)
						values(@TaskID, @RunDate, @Sub)
	end 

END

GO


