USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Email_Get]    Script Date: 11/17/2015 2:33:10 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Email_Get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Email_Get]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/16/2015
-- Description:	Gets all active email info.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Email_Get] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		EmailID,
		[Address] AS EmailAddress,
		FirstName,
		LastName
	FROM
		dbo.Email
	WHERE
		ActiveYN = 1
END

GO



