USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Schedule_Insert]    Script Date: 8/7/2015 1:53:01 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Schedule_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Schedule_Insert]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.7.2015
-- Description:	Inserts new schedule data.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Schedule_Insert] 
	-- Add the parameters for the stored procedure here
	@StartDate DATETIME, 
	@RecurrenceNumber INTEGER = NULL,
	@Frequency VARCHAR(50),
	@RunOnMon BIT = NULL,
	@RunOnTue BIT = NULL,
	@RunOnWed BIT = NULL,
	@RunOnThu BIT = NULL,
	@RunOnFri BIT = NULL,
	@RunOnSat BIT = NULL,
	@RunOnSun BIT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATETIME = GETDATE()

	INSERT INTO dbo.Schedule
	(
		Frequency,
		RecurrenceNumber,
		StartDate,
		RunOnMonTF,
		RunOnTueTF,
		RunOnWedTF,
		RunOnThuTF,
		RunOnFriTF,
		RunOnSatTF,
		RunOnSunTF,
		CreateDate,
		ModifiedDate
	)
	VALUES
	(
		@Frequency,
		@RecurrenceNumber,
		@StartDate,
		@RunOnMon,
		@RunOnTue,
		@RunOnWed,
		@RunOnThu,
		@RunOnFri,
		@RunOnSat,
		@RunOnSun,
		@CurrentDate,
		@CurrentDate
	)

	SELECT SCOPE_IDENTITY() AS ScheduleID

END

GO



