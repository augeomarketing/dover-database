USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_ServerInfo_Get]    Script Date: 8/7/2015 2:01:36 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ServerInfo_Get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ServerInfo_Get]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.7.2015
-- Description:	Gets all server info values.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ServerInfo_Get] 
	-- Add the parameters for the stored procedure here
	@ServerInfoID INTEGER = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		ServerInfoID,
		Hostname,
		Username,
		[Password],
		Protocol,
		HostKey
	FROM
		dbo.ServerInfo
	WHERE
		@ServerInfoID = 0 OR ServerInfoID = @ServerInfoID
	ORDER BY
		Hostname

END

GO



