USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_ServerInfo_Insert]    Script Date: 8/7/2015 2:03:36 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ServerInfo_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ServerInfo_Insert]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.7.2015
-- Description:	Inserts new server info data.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ServerInfo_Insert] 
	-- Add the parameters for the stored procedure here
	@Hostname VARCHAR(255), 
	@Username VARCHAR(255),
	@Password VARBINARY(1024),
	@Protocol VARCHAR(50),
	@HostKey VARCHAR(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATETIME = GETDATE()
	DECLARE @RowsFound INTEGER
	SET @RowsFound = (SELECT COUNT(1) FROM dbo.ServerInfo WHERE Hostname = @Hostname)

	IF @RowsFound = 0
	BEGIN
		INSERT INTO dbo.ServerInfo
		(
			Hostname,
			Username,
			[Password],
			CreateDate,
			ModifiedDate,
			Protocol,
			HostKey
		)
		VALUES
		(
			@Hostname,
			@Username,
			@Password,
			@CurrentDate,
			@CurrentDate,
			@Protocol,
			@HostKey
		)

		SELECT SCOPE_IDENTITY() AS ServerInfoID
	END
END

GO



