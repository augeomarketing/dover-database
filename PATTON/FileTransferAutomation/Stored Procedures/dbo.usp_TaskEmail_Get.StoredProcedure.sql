USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_TaskEmail_Get]    Script Date: 11/17/2015 2:46:06 PM ******/
SET ANSI_NULLS ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_TaskEmail_Get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_TaskEmail_Get]
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 11/16/2015
-- Description:	Gets email recipient information for given task.
-- =============================================
CREATE PROCEDURE [dbo].[usp_TaskEmail_Get] 
	-- Add the parameters for the stored procedure here
	@TaskID INTEGER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		[Name] AS RecipientType,
		[Address] AS EmailAddress,
		FirstName,
		LastName
	FROM
		dbo.TaskEmail AS TE
	INNER JOIN 
		dbo.Task AS T ON T.TaskID = TE.TaskID
	INNER JOIN
		dbo.Email AS E ON E.EmailID = TE.EmailID
	INNER JOIN
		dbo.RecipientType AS RT ON RT.RecipientTypeID = TE.RecipientTypeID
	INNER JOIN
		dbo.TaskHistory AS TH ON TH.TaskID = TE.TaskID
	WHERE
		TE.TaskID = @TaskID AND
		E.ActiveYN = 1 AND
		CONVERT(DATE, TH.RunDate) = CONVERT(DATE, GETDATE()) 


		
END

GO



