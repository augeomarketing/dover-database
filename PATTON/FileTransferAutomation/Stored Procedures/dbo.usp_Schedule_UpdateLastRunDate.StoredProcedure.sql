USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_Schedule_UpdateLastRunDate]    Script Date: 8/7/2015 1:58:41 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_Schedule_UpdateLastRunDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_Schedule_UpdateLastRunDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.17.2015
-- Description:	Updates last run date field in schedule.
-- =============================================
CREATE PROCEDURE [dbo].[usp_Schedule_UpdateLastRunDate] 
	-- Add the parameters for the stored procedure here
	@ScheduleID INTEGER, 
	@LastRunDateTime DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Schedule SET LastRunDateTime = @LastRunDateTime WHERE ScheduleID = @ScheduleID
END

GO



