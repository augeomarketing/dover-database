USE [FileTransferAutomation]
GO

/****** Object:  StoredProcedure [dbo].[usp_ServerInfo_Update]    Script Date: 8/7/2015 2:14:43 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ServerInfo_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ServerInfo_Update]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Nicholas Parsons
-- Create date: 7.7.2015
-- Description:	Updates server info
-- =============================================
CREATE PROCEDURE [dbo].[usp_ServerInfo_Update] 
	-- Add the parameters for the stored procedure here
	@ServerInfoID INTEGER, 
	@Hostname VARCHAR(255),
	@Username VARCHAR(255),
	@Password VARBINARY(1024) = NULL,
	@Protocol VARCHAR(50),
	@HostKey VARCHAR(100) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CurrentDate DATETIME = GETDATE()

	IF @Password IS NULL
	BEGIN
		UPDATE dbo.ServerInfo SET Hostname = @Hostname, Username = @Username, ModifiedDate = @CurrentDate,
		                          Protocol = @Protocol, HostKey = @HostKey 
		WHERE ServerInfoID = @ServerInfoID
	END
	ELSE
	BEGIN
		UPDATE dbo.ServerInfo SET Hostname = @Hostname, Username = @Username, [Password] = @Password, ModifiedDate = @CurrentDate,
		                          Protocol = @Protocol, HostKey = @HostKey 
		WHERE ServerInfoID = @ServerInfoID
	END
END

GO



