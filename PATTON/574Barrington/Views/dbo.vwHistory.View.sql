USE [574Barrington]
GO
/****** Object:  View [dbo].[vwHistory]    Script Date: 09/25/2009 13:53:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistory]
			 as
			 select case
				    when len(acctid) = 16 then left(ltrim(rtrim(acctid)),6) + replicate('x', 6) + right(ltrim(rtrim(acctid)),4) 
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from [574Barrington].dbo.history
GO
