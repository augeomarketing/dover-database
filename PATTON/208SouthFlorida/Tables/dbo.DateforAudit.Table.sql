/****** Object:  Table [dbo].[DateforAudit]    Script Date: 03/09/2009 12:18:26 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DateforAudit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [DateforAudit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DateforAudit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [DateforAudit](
	[Datein] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
