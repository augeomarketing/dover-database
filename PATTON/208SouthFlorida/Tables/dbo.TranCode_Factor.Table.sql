/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 03/09/2009 12:19:23 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[TranCode_Factor]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [TranCode_Factor]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[TranCode_Factor]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
