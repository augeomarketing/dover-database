/****** Object:  Table [dbo].[Current_Month_Activity]    Script Date: 03/09/2009 12:17:55 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Current_Month_Activity]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Current_Month_Activity]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Current_Month_Activity]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [Current_Month_Activity](
	[Tipnumber] [nchar](15) NOT NULL,
	[EndingPoints] [int] NULL CONSTRAINT [DF_Current_Month_Activity_EndingPoints]  DEFAULT (0),
	[Increases] [int] NULL CONSTRAINT [DF_Current_Month_Activity_Increases]  DEFAULT (0),
	[Decreases] [int] NULL CONSTRAINT [DF_Current_Month_Activity_Decreases]  DEFAULT (0),
	[AdjustedEndingPoints] [int] NULL CONSTRAINT [DF_Current_Month_Activity_AdjustedEndingPoints]  DEFAULT (0)
) ON [PRIMARY]
END
GO
