/****** Object:  Table [dbo].[phb_AprilCards]    Script Date: 03/09/2009 12:19:09 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[phb_AprilCards]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [phb_AprilCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[phb_AprilCards]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [phb_AprilCards](
	[CardType] [varchar](1) NOT NULL,
	[MemberNbr] [int] NOT NULL,
 CONSTRAINT [PK_phb_AprilCards] PRIMARY KEY CLUSTERED 
(
	[CardType] ASC,
	[MemberNbr] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
