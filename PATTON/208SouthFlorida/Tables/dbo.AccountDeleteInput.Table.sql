/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 03/09/2009 12:17:36 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AccountDeleteInput]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [AccountDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AccountDeleteInput]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
