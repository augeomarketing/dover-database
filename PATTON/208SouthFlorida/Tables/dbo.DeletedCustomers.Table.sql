/****** Object:  Table [dbo].[DeletedCustomers]    Script Date: 03/09/2009 12:18:29 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DeletedCustomers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [DeletedCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DeletedCustomers]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [DeletedCustomers](
	[RNNumber] [varchar](15) NULL,
	[membernum] [varchar](20) NULL,
	[name1] [varchar](40) NULL,
	[StatusDescription] [varchar](50) NULL,
	[CustDateAdded] [datetime] NULL,
	[CustDateDeleted] [datetime] NULL,
	[Available] [int] NULL,
	[EarnedTot] [int] NULL,
	[RedeemedTot] [int] NULL,
	[accttype] [varchar](20) NULL,
	[AcctDateAdded] [datetime] NULL,
	[acctid] [varchar](25) NULL,
	[AcctDateDeleted] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
