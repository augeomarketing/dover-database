/****** Object:  Table [dbo].[Input_FixBonusPoints]    Script Date: 03/09/2009 12:18:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Input_FixBonusPoints]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Input_FixBonusPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Input_FixBonusPoints]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [Input_FixBonusPoints](
	[MemberNumber] [char](8) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
