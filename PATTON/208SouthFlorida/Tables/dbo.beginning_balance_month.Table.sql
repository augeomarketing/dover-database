/****** Object:  Table [dbo].[beginning_balance_month]    Script Date: 03/09/2009 12:17:44 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[beginning_balance_month]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [beginning_balance_month]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[beginning_balance_month]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [beginning_balance_month](
	[monthbegin] [datetime] NOT NULL,
	[beginbalance] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
