/****** Object:  Table [dbo].[Input_Customer_error]    Script Date: 03/09/2009 12:18:42 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Input_Customer_error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Input_Customer_error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Input_Customer_error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [Input_Customer_error](
	[MemberNum] [char](20) NOT NULL,
	[OldMemberNum] [char](20) NULL,
	[Name] [char](50) NULL,
	[LastName] [char](40) NULL,
	[Address1] [char](50) NULL,
	[Address2] [char](50) NULL,
	[City] [char](50) NULL,
	[State] [char](10) NULL,
	[Zip] [char](5) NULL,
	[HomePhone] [char](13) NULL,
	[CardType] [char](1) NULL,
	[StatusCode] [char](1) NULL,
	[Last4] [char](4) NULL,
	[EmployeeFlag] [char](1) NULL,
	[BonusPoints] [int] NULL,
	[TipNumber] [char](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
