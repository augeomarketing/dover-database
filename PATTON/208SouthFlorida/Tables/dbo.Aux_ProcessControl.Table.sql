/****** Object:  Table [dbo].[Aux_ProcessControl]    Script Date: 03/09/2009 12:17:43 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Aux_ProcessControl]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Aux_ProcessControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Aux_ProcessControl]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [Aux_ProcessControl](
	[RecordNumber] [int] IDENTITY(1,1) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[MonthBegin] [datetime] NOT NULL,
	[MonthEnd] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
