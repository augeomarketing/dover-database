/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 03/09/2009 12:19:06 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Monthly_Statement_File]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Monthly_Statement_File]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0),
	[PointsEnd] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0),
	[PointsPurchasedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0),
	[PointsPurchasedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0),
	[PointsBonus] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0),
	[PointsAdded] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0),
	[PointsIncreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0),
	[PointsRedeemed] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0),
	[PointsReturnedCR] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0),
	[PointsReturnedDB] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0),
	[PointsSubtracted] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0),
	[PointsDecreased] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0),
	[acctid] [char](16) NULL,
	[cardseg] [char](10) NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[pointfloor] [char](11) NULL CONSTRAINT [DF_Monthly_Statement_File_pointfloor]  DEFAULT (0),
	[PointsExpire] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT (0),
	[FuturePointsExpire] [decimal](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_FuturePointsExpire]  DEFAULT (0)
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
