/****** Object:  Table [dbo].[AcctType]    Script Date: 03/09/2009 12:17:37 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AcctType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [AcctType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[AcctType]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [AcctType](
	[AcctType] [varchar](20) NOT NULL,
	[AcctTypeDesc] [varchar](50) NOT NULL,
	[Acctmultiplier] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
