use [208SouthFlorida] 
go 
/****** Object:  Table [dbo] .[Input_Transaction]    Script Date: 03/09/2009 12:18:55 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Input_Transaction]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [Input_Transaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[Input_Transaction]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [Input_Transaction](
	[MemberNum] [char](15) NOT NULL,
	[AccountNum] [char](16) NOT NULL,
	[OldAccountNum] [char](16) NULL,
	[TranCode] [char](6) NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[trandate] [char](10) NULL,
	[BonusPoints] int NULL,
	[TipNUmber] [char](15) NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
