/****** Object:  Table [dbo].[TransStandard]    Script Date: 03/09/2009 12:19:25 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[TransStandard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [TransStandard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[TransStandard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [TransStandard](
	[TIP] [nvarchar](15) NULL,
	[TranDate] [nvarchar](10) NULL,
	[AcctNum] [nvarchar](25) NULL,
	[TranCode] [nvarchar](2) NULL,
	[TranNum] [nvarchar](4) NULL,
	[TranAmt] [nchar](15) NULL,
	[TranType] [nvarchar](20) NULL,
	[Ratio] [nvarchar](4) NULL,
	[CrdActvlDt] [nvarchar](10) NULL
) ON [PRIMARY]
END
GO
