/****** Object:  Table [dbo].[RN1_EstatementTips]    Script Date: 03/09/2009 12:19:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[RN1_EstatementTips]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [RN1_EstatementTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[RN1_EstatementTips]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [RN1_EstatementTips](
	[Tipnumber] [char](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
