/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 03/09/2009 12:17:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCalculatePoints]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spCalculatePoints]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCalculatePoints]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Calculates points in input_transaction  */
/*  **************************************  */
CREATE PROCEDURE [spCalculatePoints]  AS   

	Update Input_Transaction 
	set Points = I.purchamt * F.pointFactor 
	from input_Transaction I join Trancode_factor F on i.Trancode = f.Trancode
' 
END
GO
