USE [208SouthFlorida]
GO
/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 03/09/2009 12:17:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadTipNumbers]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadTipNumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/*****************************************/
/* This updates the TIPNUMBER in the input_CustTran table   */
/* First It looks up the input_CustTran Old Account Number in the affiliat */
/* Second it  looks up the input_CustTran  Account Number in the affiliat */
/*****************************************/
/*Author: Rich Tremblay  */
/* Date 1/21/2007  */
/* Revised: */
/* 
RDT 2010/09/24	Added rewardsnow.dbo.spGetLastTipNumberUsed 
RDT 2010/09/24	Added rewardsnow.dbo.spPutLastTipNumberUsed 
-- RDT 2010/10/28  Reversed order of tip assignment for old and new members.
*/
/*****************************************/

CREATE PROCEDURE [spLoadTipNumbers] @TipPrefix char(3) AS

Declare @NewTip bigint

/* Update Tipnumber where OLD member number  =  in Customer_Stage  */
-- RDT 2010/10/28  UPDATE Input_Customer
-- RDT 2010/10/28  SET TIPNUMBER = Customer_Stage.TIPNUMBER
-- RDT 2010/10/28  FROM Customer_Stage, Input_Customer  
-- RDT 2010/10/28  WHERE Customer_Stage.Misc2 = Input_Customer.OLDMemberNum
-- RDT 2010/10/28  and Input_Customer.TIPNUMBER  is NULL

/* Update Tipnumber where NEW member number  =  in Customer_Stage  */
UPDATE Input_Customer
SET TIPNUMBER = Customer_Stage.TIPNUMBER
FROM Customer_Stage, Input_Customer  
	WHERE Customer_Stage.Misc2 = Input_Customer.MemberNum
	and Input_Customer.TIPNUMBER  is NULL

-- RDT 2010/10/28  moved from above to below
UPDATE Input_Customer
	SET TIPNUMBER = Customer_Stage.TIPNUMBER
		FROM  Customer_Stage, Input_Customer  
		WHERE Customer_Stage.Misc2 = Input_Customer.OLDMemberNum
			  and Input_Customer.TIPNUMBER  is NULL

--RDT 2010/09/24 set @NewTip = ( select LastTipNumberUsed  from Client )
exec rewardsnow.dbo.spGetLastTipNumberUsed @TipPrefix, @NewTip OUTPUT	--RDT 2010/09/24
select @NewTip as LastTipUsed											--RDT 2010/09/24

If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'

/* Assign Get New Tips to new customers */
update Input_Customer
	set  @NewTip = ( @NewTip + 1 ),
	      TIPNUMBER =  @NewTip 
	where TipNumber is null 

-- Update LastTip 
--RDT 2010/09/24 Update Client set LastTipNumberUsed = @NewTip  
exec rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip			--RDT 2010/09/24


/* Load Tipnumbers to Input_Transaction from Input_customer Table */
UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = Input_Customer.TIPNUMBER
FROM Input_Transaction, Input_Customer  WHERE Input_Transaction.MemberNum = Input_Customer.MemberNum
and Input_Transaction.TIPNUMBER  is NULL

GO
