USE [208SouthFlorida]
GO

/****** Object:  StoredProcedure [dbo].[spStatementScrub]    Script Date: 10/16/2009 14:44:22 ******/
DROP PROCEDURE [dbo].[spStatementScrub]
GO
/****** Object:  StoredProcedure [dbo].[spStatementScrub]    Script Date: 10/16/2009 14:44:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rich T
-- Create date: 10/16/2009
-- Description:	Procedure to remove unwanted statement records
-- =============================================
CREATE PROCEDURE [dbo].[spStatementScrub]
	
AS
BEGIN
	SET NOCOUNT ON;
    
    -- Delete Tips who want estatements
	DELETE from Quarterly_Statement_File
		WHERE   Tipnumber IN
	    (SELECT  tipnumber FROM  rn1_EstatementTips)
	-- Delete Tips who have no points
	DELETE from Quarterly_Statement_File
	WHERE pointsend = 0  and pointsbegin =  0
		
END

GO

