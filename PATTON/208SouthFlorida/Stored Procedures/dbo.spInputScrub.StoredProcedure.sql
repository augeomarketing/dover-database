use [208SouthFlorida] 

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 03/09/2009 12:17:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spInputScrub]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spInputScrub]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 4/2007   */
/* REVISION: 0 */

--------------- Input Customer table
-- remove leading spaces in name and lastname 
-- remove ' from name and lastname 


--------------- Input Transaction table
--  Delete transaction records that don't have amounts
--  Remove transaction records that don't have a customer.
--  Replace null with 0 on purhase and trancounts in transaction 
-- Replace NewCard with "N" if null or empty string 07/13/2007

/* 11/25/2008 
	Bonus amount have been added to the customer and transaction files sent
	by South Florida. The customer file now creates a bonus transaction. 
	The credit and debit files contain the new card bonus amounts.
	South Florida will track if bonuses have been awarded. 
*/
-- RDT 10/29/2010 add condition to check for bonus amount before removing input record with purchase amount of 0 
-- RDT 11/08/2010 added Address2 to logic

/******************************************************************************/	

CREATE  PROCEDURE [spInputScrub] AS

/* clear error tables */
Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

--------------- Input Customer table

/* Remove input_customer records with Firstname and lastname = null */
Insert into Input_customer_error 
	select * from Input_customer where name is null and lastname is null 
delete from Input_customer where name is null and lastname is null 


-- RDT 11/08/2010 - Move Address2 to address1 if address1 is null
Update Input_Customer 
Set Address1 = Address2,
Address2 = null 
Where Len(rtrim(Address1)) = 0 and Len(rtrim(Address2)) > 0 


/* remove leading and trailing spaces in names and addresses */ 
Update Input_customer set 
	Name = Ltrim(Rtrim(name)) , 
	LastName = Ltrim(Rtrim(LastName)),
	Address1 = Ltrim(Rtrim(Address1))
	, Address2 = Ltrim(Rtrim(Address2)) -- RDT 11/08/2010 Address2 
	
Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where membernum not in (select membernum from input_Customer) 

Delete from Input_Transaction
	where membernum not in (select membernum from input_Customer) 


--- Delete Duplicate Transactions from input_transaction if the purchcnt = 0 
Delete from input_transaction 
where accountnum in 
	( select accountnum  from input_transaction group by accountnum having count(*) > 1 ) 
     and purchcnt = 0 
     and BonusPoints = 0 -- RDT 10/29/2010 

--------------- Input Transaction table
/*Replace null with 0 on purhase and trancounts in transaction */
Update Input_transaction 
set Purchcnt  = 0 
where Purchcnt  is null

/* Round the amounts */
Update input_transaction 
set  purchamt = round(purchamt,0)

-- Convert Trancodes to Product Codes
Update Input_transaction 
	set TranCode = '63' 
	where Upper( TranCode ) = 'AG'

Update Input_transaction 
	set TranCode = '67' 
	where Upper( TranCode ) = 'WG'

-- Replace NewCard with "N" if null or empty string 07/13/2007
-- RDT 11/25/2008 update dbo.Input_Transaction set newcard = 'N' where newcard is null 
-- RDT 11/25/2008 update dbo.Input_Transaction set newcard = 'N' where newcard <> 'Y'   and newcard <> 'N'

-- 11/25/2008 RDT Cleanup Bad bonus points data 
--Update Input_Transaction 
--set BonusPoints = 
--	Case 
--		When BonusPoints = Char(10) then 0 
--		When BonusPoints is null then 0 
--		Else BonusPoints 
--	End
Update Input_Transaction 
set BonusPoints = 0 where BonusPoints is null 

/* 11/25/2008 add customer bonuses to transaction table */
Insert into Input_Transaction 
(MemberNum, AccountNum, TranCode, PurchCnt, Points)
select MemberNum, MemberNum, 'BI', 1, BonusPoints from input_Customer where BonusPoints > 0 

/* 11/25/2008 add Credit and Debit bonuses to transaction table */
Insert into Input_Transaction 
(MemberNum, AccountNum, TranCode, PurchCnt, Points)
select MemberNum, AccountNum, 'BN', 1, BonusPoints From Input_Transaction 
where BonusPoints > 0 

	
