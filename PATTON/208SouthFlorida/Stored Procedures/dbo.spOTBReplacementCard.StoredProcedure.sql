/****** Object:  StoredProcedure [dbo].[spOTBReplacementCard]    Script Date: 03/09/2009 12:17:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spOTBReplacementCard]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spOTBReplacementCard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spOTBReplacementCard]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*****************************************/
/* This updates the ONETIMEBONUS_Stage accounts that were replaced.  */
/* If a new Account is replaceing an existing Account the new Account s not elegible for a NewCard Bonus */
/*****************************************/
/*Author: Tremblay  */
/* Date 6/27/2007  */
/* Revised: */
/*****************************************/

CREATE PROCEDURE [spOTBReplacementCard] @TranCode Char(2) AS

Insert into onetimebonuses_stage
select i.tipnumber, o.trancode, i.accountnum, o.dateawarded from input_transaction i join onetimebonuses_stage o 
	on i.tipnumber = o.tipnumber 
	and i.oldaccountnum = o.acctid 
	and o.trancode = @TranCode
' 
END
GO
