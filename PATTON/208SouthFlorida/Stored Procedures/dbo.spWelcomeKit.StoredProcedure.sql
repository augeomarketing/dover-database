/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 03/09/2009 12:17:36 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spWelcomeKit]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spWelcomeKit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spWelcomeKit]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'


/*  RDT 3/16/07  */

/* 

This retrieves all customers that were added in the previous month
It does not do an exact date match on the custom date added column

  */
CREATE PROCEDURE [spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 


Truncate Table Welcomekit 

insert into Welcomekit SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
				ADDRESS2, ADDRESS3, City, State, ZipCode 
		         FROM customer WHERE (Year(DATEADDED) = Year(@EndDate)AND Month(DATEADDED) = Month(@EndDate)AND Upper(STATUS) <> ''C'') 

update DateforAudit set Datein=@EndDate
' 
END
GO
