Use [208SouthFlorida] 
go 
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 03/09/2009 12:17:35 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spPurgeClosedCustomers]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spPurgeClosedCustomers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 
-- @BonusAmt int, 
-- @TranCode (Trancode to add )
-- @TransactionType (Debit or Credit) 

-- read input_purge
-- staging tables don''t need to have records moved to delete tables
-- Production tables need to have pending purge processing.

--RDT 3/21/08 and tipnumber Not In ( select distinct tipnumber from history where histdate > @DateDeleted and TranCode <> ''RQ'' )
-- RDT 7/7/08 	S.F. Wants deleted customer counts with staged data.
-- RDT 12/01/2008 - Remove rows from Input_Transaction that don''t have a customer record 
--                  to remove the added bonuses.
-- RDT 12/7/2009 - Receiving two records for one customer. One record flagged to be deleted the other is active. 
--                 This deletes causes the points to be deleted but not the record.
--                 Code added to remove any purge records with an active record in the input table.
-- RDT 06/02/2011 - JIRA SouthFlorida-7 Audit failed because purge didn't remove customer records that had expired points. 
--					The issue was date vs date time.
-- RDT 07/12/2011 -- Commented out dim_customer_email, dim_customer_country, dim_customer_mobilephone columns 

/******************************************************************************/
CREATE PROCEDURE [spPurgeClosedCustomers]  @Production_Flag char(1), 
		-- RDT 06/02/2011 @DateDeleted char(10) 
		@DateDeleted DateTime   -- RDT 06/02/2011 
		AS
BEGIN 
Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)

Declare @DateDeletedNoTime date
set @DateDeletedNoTime =  @DateDeleted 

set @DateDeleted = @DateDeleted + ' 23:59:59'-- RDT 06/02/2011 23:59:59

----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin
	Truncate Table Input_Purge 
	Truncate Table Customer_Stage_Delete 			-- RDT 7/7/08 	 	
	Insert into Input_Purge select * from Input_Purge_Pending 	-- RDT 7/7/08 	 	

	Insert Into Input_Purge 
		Select * from Input_Customer where StatusCode = 'D'
		and tipnumber Not In ( select distinct tipnumber 
								from history 
								where histdate > @DateDeleted and TranCode <> 'RQ' ) -- RDT 3/21/08

-- RDT 12/07/2009 -- 
	Delete from input_purge	where tipnumber in 
		( select tipnumber from input_customer where statuscode <> 'D' ) 
-- RDT 12/07/2009 -- 

	Insert into Customer_Stage_Delete 			
		( TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew) 	 		
		select  TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew
		from customer_stage 					
		where TipNumber in       (Select TipNumber from Input_Purge)	

	Delete from Input_Customer 	
		where Tipnumber in ( select tipnumber from Input_Purge  ) 

-- RDT 12/01/2008 --
	Delete from Input_Transaction 
		where Tipnumber not in (select Tipnumber from Input_Customer) 
-- RDT 12/01/2008 --

	Delete from Customer_stage 	
		where TipNumber in (Select TipNumber from Input_Purge)

	Delete from Affiliat_Stage 	
		where TipNumber not in (select TipNumber from Customer_Stage)

	Delete from History_stage 	
		where TipNumber not in (select TipNumber from Customer_Stage)

End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin

	-- copy any input_purge_pending into input_purge 
	Insert into Input_Purge select * from Input_Purge_Pending

	-- Clear Input_Purge_Pending 
	Truncate Table Input_Purge_Pending

	-- Copy any customers from input_purge to input_purge_pending if they have History activity greater than the delete date
	Insert into input_Purge_Pending 
		select * from input_Purge  
		where tipnumber in ( select distinct tipnumber 
								from history 
								where histdate > @DateDeleted and TranCode <> 'RQ' )

	-- Remove any customers from input_purge if they have current activity in history
	Delete from input_Purge 
		where tipnumber in ( select distinct tipnumber 
								from history
								where histdate > @DateDeleted and TranCode <> 'RQ' )

	-------------- purge remainging input_purge records. 
	-- Insert customer to customerdeleted 
	Insert Into CustomerDeleted 
		( TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate, NextStmtDate, 
		  STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, 
		  ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, 
		  StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, 
		  RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, 
		  RunAvaliableNew, DateDeleted
		  --, dim_customer_email, dim_customer_country, dim_customer_mobilephone
		  )
		Select 
		TIPNUMBER, RunAvailable, RUNBALANCE, RunRedeemed, LastStmtDate,	NextStmtDate, 
		STATUS, DATEADDED, LASTNAME, TIPFIRST, TIPLAST, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, 
		ACCTNAME5, ACCTNAME6, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, City, State, ZipCode, 
		StatusDescription, HOMEPHONE, WORKPHONE, BusinessFlag, EmployeeFlag, SegmentCode, ComboStmt, 
		RewardsOnline, NOTES, BonusFlag, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, 
		RunAvaliableNew, @DateDeletedNoTime 
		--, dim_customer_email, dim_customer_country, dim_customer_mobilephone
		from Customer c 
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn''t work. I keep getting a 
-- datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, DateDeleted )
		
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustId, @DateDeletedNoTime as DateDeleted 
		from Affiliat  where TipNumber in ( Select TipNumber from Input_Purge) 

	-- copy history to historyDeleted 
	Insert Into HistoryDeleted 
		Select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage, @DateDeletedNoTime as DateDeleted 
		from History H 
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Delete from customer 
	Delete from Customer
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Delete records from affiliat 
	Delete from Affiliat   
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- Delete records from History 
	Delete from History 
		where TipNumber in ( Select Distinct TipNumber from Input_Purge) 

	-- flag all Undeleted Customers "C" that have an input_purge_pending record 
	Update customer set status = 'C'
		where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)

End

--' 
END
GO
