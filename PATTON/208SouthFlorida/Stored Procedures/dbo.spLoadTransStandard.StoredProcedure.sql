/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 03/09/2009 12:17:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadTransStandard]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadTransStandard]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadTransStandard]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'


/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* SQL2008 throws a trucation error  
/******************************************************************************/
CREATE PROCEDURE [spLoadTransStandard] @DateAdded char(10) AS

-- Clear TransStandard 
Truncate table TransStandard 

-- Convert Credit Purchase TranCode
Update Input_Transaction 
set TranCode = ''63'' 
where UPPER(Trancode ) = ''AG''

-- Convert Debit Purchase Trancode
Update Input_Transaction 
set TranCode = ''67'' 
where UPPER(Trancode ) = ''WG''

-- Load the TransStandard table with rows from Input_Transaction
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt], [CrdActvlDt] ) 
select  [TipNumber], @DateAdded, [AccountNum], [TranCode], [PurchCnt], convert(char(15), [Points]) ,[trandate] from Input_Transaction 

-- Set the TranType to the Description found in the RewardsNow.TranCode table
-- Update TransStandard set TranType = R.Description from RewardsNow.dbo.Trantype R join TransStandard T on R.TranCode = T.Trancode 
Update TransStandard set TranType = Left( R.Description,20) from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode
' 
END
GO
