/****** Object:  StoredProcedure [dbo].[spBonusNewCardStage]    Script Date: 03/09/2009 12:17:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spBonusNewCardStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spBonusNewCardStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spBonusNewCardStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
/******************************************************************************/
/*    This Stored Procedure awards Bonuses  to PointsNow Tables */

/*	Award Points if 1st use
	New check (debit) card: points  on 1st use
 */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @DateAdded char(10), 
-- @BonusAmt int, 
-- @TranType 
-- Add a bonus for all cards = ''N'' 
-- add a record in the onetime bonus for all cards = ''N''
--  RDT 09/30/2008 Changed code to insert records into Input_Transaction instead of directly into the Cust & Hist Stage tables
--	This is more inline with processing and will report better on the input Transcation report.

/******************************************************************************/
CREATE PROCEDURE [spBonusNewCardStage]  @DateAdded char(10), @BonusAmt int, @TranCode Char(2) AS

Declare  @SQLDynamic nvarchar(1000)
Declare @Tipnumber 	char(15)
Declare @AcctId	char(16)
Declare @TrancodeDesc char(20)
Declare @Ratio Float



-- Retrieve the Trancode fields
Set @TrancodeDesc 	= (Select Description from Trantype where trancode = @TranCode)
Set @Ratio		= (Select Ratio from Trantype where trancode = @TranCode)

-- RDT 09/30/2008 -- start
/*
UPDATE Customer_Stage
set RunAvaliableNew = RunAvaliableNew + @BonusAmt  
where tipnumber in ( select Tipnumber from input_transaction where newcard = ''Y''  )
	
INSERT INTO History_Stage (TipNumber,Acctid, HistDate,TranCode,TranCount,Points,Ratio,Description,Overage,SecID)
( select Tipnumber, AccountNum, convert(char(10), @DateAdded,101), @TranCode, ''1'', @BonusAmt, @Ratio, @TrancodeDesc, ''0'',''NEW'' 
  from input_transaction where newcard = ''Y''  )
 */
Insert Input_Transaction 
	( MemberNum, AccountNum, OldAccountNum, TranCode, PurchAmt, PurchCnt, Trandate, NewCard, TipNUmber, Points )
	select 
	 MemberNum, AccountNum, OldAccountNum, @TranCode, 1, 1, Trandate, ''1'', TipNumber, @BonusAmt 
	from Input_Transaction 
		where NewCard = ''Y'' 
		and AccountNum Not in ( Select acctid from OneTimeBonuses_Stage )
				
-- RDT 09/30/2008 -- End 

INSERT INTO OneTimeBonuses_Stage (Tipnumber, Trancode, Acctid, DateAwarded)
( select Tipnumber, @TranCode, AccountNum, convert(char(10), @DateAdded,101) 
   from input_transaction where newcard = ''Y''  )' 
END
GO
