/****** Object:  StoredProcedure [dbo].[spCheckCardDoublePoints]    Script Date: 03/09/2009 12:17:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCheckCardDoublePoints]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spCheckCardDoublePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCheckCardDoublePoints]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Rich Tremblay
-- Create date: 3/2/2009
-- Description:	Double the points on check card file if flag = ''Y''
-- =============================================
CREATE PROCEDURE [spCheckCardDoublePoints]
@Factor Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Input_transaction 
		set Points = Points * @Factor 
		where trancode = ''67'' and 
		PAN in (select pan from input_CheckCard where DoublePointsFlag = ''Y'')

END
' 
END
GO
