/****** Object:  StoredProcedure [dbo].[spCorrectCustBalances]    Script Date: 03/09/2009 12:17:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCorrectCustBalances]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spCorrectCustBalances]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCorrectCustBalances]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'










/******************************************************************************/
/*    This will recalculate and Correct the runbalance and runavailable and runredeemed     */
/*    using the tipnumber extracted from the customer table                                 */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [spCorrectCustBalances]   AS   
Declare @TipNumber char(15)
Declare @AcctID char(25)
Declare @RunAvailable numeric(10)
Declare @histpoints   numeric(10)
Declare @diff   numeric(10)
Declare @RunBalanceold numeric(10)
Declare @RunBalanceNew numeric(10)
Declare @RunBalance  numeric(10)
Declare @RunRedeemed numeric(10)
DECLARE @RunRedeemedIN NUMERIC(10)
Declare @RunAvailiableNew numeric(10)
Declare @BEGBAL numeric(10)
Declare @OLDESTDATE DATETIME

declare @RunredeemedHst numeric(10)
 
drop table wrktable1

select tipnumber
 into wrktable1
from customer
 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select *
From wrktable1 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	
set @RunBalanceNew = ''0''
set @RunBalanceold = ''0''
set @RunRedeemed = ''0''

set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 	
)

set @RunRedeemed  = 
(	select sum(points) from history 
	where @tipnumber = history.tipnumber 
	and (history.trancode like(''R%''))
)

if @RunAvailiableNew is null 
set @RunAvailiableNew = ''0''

if @RunRedeemed is null 
set @RunRedeemed = ''0''
		
Update Customer 
set
 runbalance  = @RunAvailiableNew + @RunRedeemed
 ,runavailable = @RunAvailiableNew
 ,runredeemed = @RunRedeemed
where tipnumber = @TipNumber 

/*set @BEGBAL = ''0''

select @BEGBAL = sum(points*ratio)
 from history where tipnumber = @TipNumber
and histdate < ''2007-10-01''

update beginning_balance_table
set
monthbeg10 = @BEGBAL
where tipnumber = @TipNumber */

Fetch custfix_crsr  
into  @tipnumber
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr










' 
END
GO
