/****** Object:  StoredProcedure [dbo].[spCorrectBeginningfromauditerror]    Script Date: 03/09/2009 12:17:32 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCorrectBeginningfromauditerror]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spCorrectBeginningfromauditerror]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spCorrectBeginningfromauditerror]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'/******************************************************************************/
/*    This will recalculate and Correct the month% of the Beginning_Balance_Table     */
/*    using the tipnumber extracted from the monthly_audit_errorfile                              */
/* */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [spCorrectBeginningfromauditerror]  @monthbucket nvarchar(10) , @monthToCorrect nvarchar(10) AS
Declare @TipNumber char(15)
Declare @RunAvailiableNew nvarchar (10) -- numeric 
Declare @SQLUpdate nvarchar(1000)
 
/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare custfix_crsr   cursor
for Select tipnumber
From monthly_audit_errorfile order by tipnumber 

Open custfix_crsr  
/*                  */

Fetch custfix_crsr  
into  @tipnumber 
IF @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN 	    
	

set @RunAvailiableNew = ''0''


set @RunAvailiableNew  = 
(	select sum(points*ratio) from history 
	where @tipnumber = history.tipnumber 
	and histdate < 	@monthToCorrect
)


if @RunAvailiableNew is null 
set @RunAvailiableNew = ''0''

print @monthbucket +  '' Tip:'' + @tipnumber+ '' Amt: '' + cast( @RunAvailiableNew as char(40) ) 
		

set @SQLUpdate=N''update Beginning_Balance_Table set ''+ 
Quotename(@MonthBucket) + N''= ''+ @RunAvailiableNew + '' where tipnumber = ''+ '''''''' + @TipNumber +''''''''
print @SQLUpdate
 exec sp_executesql @SQLUpdate
--Update beginning_balance_table set MonthBeg7  = @RunAvailiableNew    where tipnumber = @TipNumber 


Fetch custfix_crsr  
into  @tipnumber 
	
END /*while */

GoTo EndPROC

Fetch_Error:
Print ''Fetch Error''

EndPROC:
close  custfix_crsr
deallocate  custfix_crsr' 
END
GO
