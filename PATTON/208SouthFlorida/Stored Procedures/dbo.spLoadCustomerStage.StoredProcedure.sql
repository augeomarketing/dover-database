Use [208SouthFlorida] 
/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 03/09/2009 12:17:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadCustomerStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadCustomerStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 1 */
/* 6/22/07 South Florida now sending the FULL name in the Name field. */
-- RDT 11/08/2010 South Florida now sending address2 field.
-- RDT 03/28/2012 - New member numbers are not updating the customer_stage correctly (duplicate rows) 
/******************************************************************************/	
CREATE PROCEDURE [spLoadCustomerStage] @EndDate DateTime AS
/* Update Existing Customers                                            */

-------------------------- RDT 03/28/2012 -------  START 
--Update Customer_Stage
--Set 
--LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
----,ACCTNAME1 	= left(rtrim(Input_Customer.NAME) + ' ' + rtrim(Input_Customer.LASTNAME),40)
--,ACCTNAME1 	= left(rtrim(Input_Customer.NAME),40 )
--,ADDRESS1 	= Input_Customer.ADDRESS1
--,ADDRESS2  	= Input_Customer.ADDRESS2 -- RDT 11/08/2010 
----,ADDRESS4     = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIP) +' '+ ltrim(rtrim(Input_Customer.Zip4)) ),40)
--,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIP)) , 40 )
--,CITY 		= Input_Customer.CITY
--,STATE		= left(Input_Customer.STATE,2)
--,ZIPCODE 	= ltrim(Input_Customer.ZIP)
--,HOMEPHONE 	= left(Input_Customer.HOMEPHONE,10)
--,EmployeeFlag	= Input_Customer.EMPLOYEEFLAG
--,STATUS	= Input_Customer.STATUSCODE
--,MISC2		= Input_Customer.MEMBERNUM
--From Input_Customer
--Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

Select * into #Input_Customer 
	From Input_Customer 
		Where TipNumber in 
					( 
					Select tipnumber 
					From Input_Customer
						Group by TipNumber 
						Having COUNT(*) > 1 
					) 
		and StatusCode = 'A' 

Insert into #Input_Customer 
(MemberNum, OldMemberNum, Name, LastName, Address1, Address2, City, State, Zip, HomePhone, 
	CardType, StatusCode, Last4, EmployeeFlag, BonusPoints, TipNumber)
Select MemberNum, OldMemberNum, Name, LastName, Address1, Address2, City, State, Zip, HomePhone, 
	CardType, StatusCode, Last4, EmployeeFlag, BonusPoints, TipNumber
	From Input_Customer 
	Where TipNumber not in 
		( 
			Select tipnumber 
			From Input_Customer
			Group by TipNumber 
			Having COUNT(*) > 1 
		) 

Update Customer_Stage
Set 
	LASTNAME 	= Left(rtrim(IC.LASTNAME),40)
	,ACCTNAME1 	= left(rtrim(IC.NAME),40 )
	,ADDRESS1 	= IC.ADDRESS1
	,ADDRESS2  	= IC.ADDRESS2 
	,ADDRESS4      = left(ltrim(rtrim( IC.CITY))+' ' +ltrim(rtrim( IC.STATE))+' ' +ltrim( rtrim( IC.ZIP)) , 40 )
	,CITY 		= IC.CITY
	,STATE		= left(IC.STATE,2)
	,ZIPCODE 	= ltrim(IC.ZIP)
	,HOMEPHONE 	= left(IC.HOMEPHONE,10)
	,EmployeeFlag	= IC.EMPLOYEEFLAG
	,STATUS	= IC.STATUSCODE
	,MISC2		= IC.MEMBERNUM
	From #Input_Customer ic
		Where ic.TIPNUMBER = Customer_Stage.TIPNUMBER 

-------------------------- RDT 03/28/2012 -------  END 

/*Add New Customers                                                      */
	Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ADDRESS1, ADDRESS2, -- RDT 11/08/2010 
	ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE,  EMPLOYEEFLAG, DATEADDED, STATUS, MISC2, 
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), left(rtrim(LASTNAME),40),
--	left(rtrim(Input_Customer.NAME) + ' ' + rtrim(Input_Customer.LASTNAME),40), 
	left(rtrim(Input_Customer.NAME),40) , 
	Left(rtrim(ADDRESS1),40), 
	Left(rtrim(ADDRESS2),40), -- RDT 11/08/2010 
	left( ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIP)  ),40),
	CITY, left(STATE,2), rtrim(ZIP),
	left(HOMEPHONE,10), EMPLOYEEFLAG, @EndDate, STATUSCODE,  MEMBERNUM
	,0, 0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set STATUS = 'A' 
Where STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null
