/****** Object:  StoredProcedure [dbo].[spSetWelcomeKitFileName]    Script Date: 03/09/2009 12:17:35 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spSetWelcomeKitFileName]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spSetWelcomeKitFileName]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spSetWelcomeKitFileName]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [spSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)
declare @WelcomeKitCount		int

set @endingDate=(select top 1 datein from dbo.DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @WelcomeKitCount = (Select count(*) from dbo.WelcomeKit)

set @filename=''W'' + @TipPrefix + @currentdate + ''_'' + cast(@WelcomeKitCount as nvarchar(9)) + ''.xls''

set @newname=''O:\''+@TipPreFix+''\Output\WelcomeKits\Welcome.bat '' + @filename
set @ConnectionString=''O:\''+@TipPrefix+''\Output\WelcomeKits\'' + @filename' 
END
GO
