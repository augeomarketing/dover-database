use [208SouthFlorida] 
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadFutureExpire]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadFutureExpire]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Calculates points in input_transaction  */
/*  **************************************  */
-- RDT 11/10/2010 Show points to expire two months out.
-- RDT 12/12/2010 Show points to expire ONE month out.
CREATE PROCEDURE [spLoadFutureExpire]  AS   

Update Monthly_Statement_File 
	set FuturePointsExpire = 0
	
Update Monthly_Statement_File 
	set FuturePointsExpire = 
		-- ( dim_ExpiringPointsProjection_PointsToExpireThisPeriod + dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus1)  
--		dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus2 -- Show points to expire two months out.
		dim_ExpiringPointsProjection_PointsToExpire_CurrentPeriodPlus1 -- RDT 12/12/2010 points to expire one month out.

	from Rewardsnow.dbo.ExpiringPointsProjection 
	where Rewardsnow.dbo.ExpiringPointsProjection.sid_ExpiringPointsProjection_Tipnumber  = Monthly_Statement_File.Tipnumber 

Update Monthly_Statement_File 
	set FuturePointsExpire = PointsEnd  
	where FuturePointsExpire  > PointsEnd 

Update Monthly_Statement_File	
	set FuturePointsExpire = 0 
	where FuturePointsExpire < 0 

