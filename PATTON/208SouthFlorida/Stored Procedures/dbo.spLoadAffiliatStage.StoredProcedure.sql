/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 03/09/2009 12:17:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadAffiliatStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadAffiliatStage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadAffiliatStage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New Accounts into Affiliat Stage  ***********/
 Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select t.AccountNum, t.TipNumber, t.TranCode, @MonthEnd, c.StatusCode, t.TranCode, c.LastName, 0, t.MemberNum
	from input_Customer c join Input_Transaction t on c.Tipnumber = t.Tipnumber
	where t.AccountNum not in ( Select acctid from Affiliat_Stage)


/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType' 
END
GO
