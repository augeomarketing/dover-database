/****** Object:  StoredProcedure [dbo].[spFixBonusPoints]    Script Date: 03/09/2009 12:17:33 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spFixBonusPoints]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spFixBonusPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spFixBonusPoints]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Rich
-- Create date: 9/28/2008
-- Description:	bonuses were not awarded by FI correctly. 
-- A FixBounsPoints file was sent to award missing new card bonus.
-- =============================================
CREATE PROCEDURE [spFixBonusPoints] 
		@DateAdded char(10), 
		@BonusAmt int, 
		@TranCode Char(2)  AS
BEGIN
	Declare @TrancodeDesc char(20), @Ratio Float
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Trim  input_FixBonusPoints 
	Update input_FixBonusPoints 
		set MemberNumber = LTrim(MemberNumber) 

	-- Initialize the stage tables.
	Declare @spErrMsg nvarchar(80)
	exec RewardsNow.dbo.spInitializeStageTables ''208'', @DateAdded,   @spErrMsg  output 
	select @spErrMsg  as spErrMsg
	If @spErrMsg <> ''no'' Return -1

	-- Load the Stage with member bonus
	Set @TrancodeDesc 	= (Select Description from Trantype where trancode = @TranCode)
	Set @Ratio		= (Select Ratio from Trantype where trancode = @TranCode)

	-- Load Input_Transaction 
	Insert Input_Transaction 
	( MemberNum, AccountNum, TranCode, PurchAmt, PurchCnt, Tipnumber, Points)
		select 
			f.MemberNumber, 
			f.MemberNumber, 
			''BN'',
			1,
			1,
			s.Tipnumber, 
			1000
		from customer_stage s join input_FixBonusPoints f
		on s.misc2 = f.MemberNumber 
  

--	UPDATE Customer_Stage
--	set RunAvaliableNew = RunAvaliableNew + @BonusAmt  
--	where Tipnumber in ( select tipnumber from customer_stage s join input_FixBonusPoints f
--							on s.misc2 = f.MemberNumber )
--
--	INSERT INTO History_Stage
--		select s.Tipnumber, f.MemberNumber, convert(char(10), @DateAdded,101), 
--			@TranCode, ''1'', @BonusAmt, @TrancodeDesc, ''NEW'', @Ratio, 0
--		from customer_stage s join input_FixBonusPoints f
--		on s.misc2 = f.MemberNumber 
 
 
exec spLoadTransStandard @DateAdded

exec spImportTransToStage ''208'', @DateAdded

	INSERT INTO OneTimeBonuses_Stage 
	(Tipnumber, Trancode, Acctid, DateAwarded)
		( select s.Tipnumber, @TranCode, f.MemberNumber, 
			convert(char(10), @DateAdded,101) 
		from customer_stage s join input_FixBonusPoints f on s.misc2 = f.MemberNumber  )

END
' 
END
GO
