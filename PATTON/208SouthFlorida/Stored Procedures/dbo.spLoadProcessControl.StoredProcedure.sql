/****** Object:  StoredProcedure [dbo].[spLoadProcessControl]    Script Date: 03/09/2009 12:17:34 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadProcessControl]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [spLoadProcessControl]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[spLoadProcessControl]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
BEGIN
EXEC dbo.sp_executesql @statement = N'
/******************************************************************************/
/* BY:  R.Tremblay  */
/* dd records to the process control table
*/
/******************************************************************************/
Create PROCEDURE [spLoadProcessControl] @MonthEnd char(12) AS

Declare @MonthBegin char(12) 

Set @MonthBegin = Convert( varchar(4), year( @monthEnd ) ) +''/'' + convert( Varchar(2), month( @monthEnd )) + ''/01''

Insert into aux_ProcessControl 
( Rundate , MonthBegin, MonthEnd )
values
( GetDate() , @MonthBegin, @MonthEnd)' 
END
GO
