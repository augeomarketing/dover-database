SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rich T
-- Create date: 06/22/09
-- Description:	Creates a table listing the Max(Histdate) and TranCode 
--              for each tip in the History. The table will be dropped and
--              recreated each time.  
-- parms: @Trancode - the trancode you want to get the last activity for.   
-- =============================================
alter PROCEDURE spLoadLastActivity 
	@Trancode char(3) AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

If exists ( select * from sysobjects  where xtype='u' and name = 'aux_LastActivity ' )
  Begin  
	 Drop table aux_LastActivity 
  End

Select tipnumber, Trancode, Convert (Char(12), Max(Histdate) , 101) as LastAct  
	into aux_LastActivity 
	From history 
		Where Trancode = @Trancode 
	group by tipnumber , Trancode

	
END
GO
