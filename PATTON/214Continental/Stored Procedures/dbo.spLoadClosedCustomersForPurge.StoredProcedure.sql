USE [214Continental]
GO
/****** Object:  StoredProcedure [dbo].[spLoadClosedCustomersForPurge]    Script Date: 01/13/2010 13:20:59 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
-- This is a new module which will import Customer_Closed_Cards and then load
-- AccountDeleteInput with the accounts to be closed. This table will then be used
-- as an update to Input_Purge in spPurgeClosedCustomers prior to the purging of accounts
-- RDT 01/13/2010 Purge of input_customer was using Misc2 (DDA) -> CIF 
--					Changed to Misc2 -> Acctid
/******************************************************************************/
ALTER PROCEDURE [dbo].[spLoadClosedCustomersForPurge]   AS

Declare @SQLDynamic nvarchar(2000)
Declare @SQLIf nvarchar(2000)
Declare @Tipnumber 	char(15)
Declare @Acctid 	char(16)
Declare @ATPanRN 	char(16)


set @SQLIf=N'if exists(select * from dbo.sysobjects where xtype=''u'' and name = ''AcctWrk1'')
	Begin
		DROP TABLE dbo.AcctWrk1   
	End '
	--print '@SQLIf'
--print @SQLIf
exec sp_executesql @SQLIf

set @SQLIf=N'if exists(select * from dbo.sysobjects where xtype=''u'' and name = ''AcctWrk2'')
	Begin
		DROP TABLE dbo.AcctWrk2   
	End '
	--print '@SQLIf'
--print @SQLIf
exec sp_executesql @SQLIf



-- RDT 01/13/2010-- select tipnumber,misc2,misc3
select tipnumber,misc2,misc4
into AcctWrk1
from customer_stage where misc2 in
(select acctid from input_Customer_Closed_Cards)

-- RDT 01/13/2010 -- select distinct(tipnumber),misc3
select distinct(tipnumber),misc4
into AcctWrk2
from AcctWrk1

insert  into accountdeleteinput
(acctid, 
dda)
select left(acctid,16),
left(custid,8) 
from affiliat_stage 
where tipnumber in (select tipnumber from AcctWrk2)

-- RDT 01/13/2010 delete from  input_customer where misc2 in (select cifnumber from Input_Customer_Closed_Cards)
delete from  input_customer where misc2 in (select acctid from Input_Customer_Closed_Cards)

delete from  input_Transaction where tipnumber in (select tipnumber from AcctWrk2)

delete from  [Input_BillsPaid] where cifnum in (select cifnumber from Input_Customer_Closed_Cards)

delete from  [Input_CheckCard] where pan in (select acctid from accountdeleteinput)

delete from  [Input_CIF] where tipnumber in (select tipnumber from AcctWrk2)

-- RDT 01/13/2010 delete from  [Input_CIF2] where ssn in (select misc3 from AcctWrk2)
delete from  [Input_CIF2] where CIFNum in (select misc4 from AcctWrk2)

delete from  [Input_DirectDeposit] where cifnum in (select cifnumber from Input_Customer_Closed_Cards)

delete from  [Input_Estatement] where cifnum in (select cifnumber from Input_Customer_Closed_Cards)

delete from  [Input_LineOfCredit] where cifnum in (select cifnumber from Input_Customer_Closed_Cards)
 
delete from  [Input_SavingMMKt] where acctnum in (select misc2 from AcctWrk1)

delete from  [Input_HotCard] where cifnum in (select cifnumber from Input_Customer_Closed_Cards)

delete from  [input_RNI_emailbonus] where tipnumber in (select tipnumber from AcctWrk2)

delete from Input_NewCards where acctnum in (select acctid from dbo.Input_Customer_Closed_Cards)