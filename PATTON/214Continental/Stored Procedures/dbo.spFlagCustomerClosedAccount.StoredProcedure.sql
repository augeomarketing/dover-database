/****** Object:  StoredProcedure [dbo].[spFlagCustomerClosedAccount]    Script Date: 03/17/2009 15:17:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
 Variable: stage / production 
set status on affiliat/affiliat_Stage accounts from Input_Purge (marked as C or H )
check customer/customer_stage for any active affiliat/affiliat_stage 
	and mark as closed if no active accounts.
The purge happens in the spPurgeClosedCustomers 

*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spFlagCustomerClosedAccount] @StageFlag char(1) AS

declare @Affiliat_table char(20) 
declare @Customer_table char(20) 
declare @SQLcmd nvarchar(2000)
declare @Tipnumber char(15)
 
If @StageFlag <> 'P' 
	Begin
	set @Affiliat_table = 'Affiliat_stage'
	set @Customer_table = 'Customer_stage' 
	Declare csr_Customer Cursor for 
		Select Tipnumber 
		From Customer_Stage 
		Where Tipnumber in (Select Tipnumber from Input_Purge) 
	End
Else
	Begin
	set @Affiliat_table = 'Affiliat'
	set @Customer_table = 'Customer' 
	Declare csr_Customer Cursor for 
		Select Tipnumber 
		From Customer 
		Where Tipnumber in (Select Tipnumber from Input_Purge) 
	End

-- Update the affiliat.status with the input_purge.cardstatus 
set @SQLCmd = 'Update '+@Affiliat_table + ' set  acctStatus = cardstatus from '+ @Affiliat_table+ '   a join input_purge p on a.acctid = p.PAN'
-- print @SQLCmd 
exec sp_executesql @SQLCmd 

Open csr_Customer
Fetch csr_Customer into @TipNumber 

If @@Fetch_Status = 1 GoTo Fetch_Error

While @@Fetch_Status = 0 
	Begin 
	set @SQLCmd = 'Update '+ @Customer_table +'set status = ( dbo.ufGetActiveAccounts ('''
			+@Tipnumber+''', '''+@StageFlag+''')  ) Where tipnumber = '''+@Tipnumber +''''
-- 	print @SQLCmd 
	exec sp_executesql @SQLCmd 

	Fetch csr_Customer into @TipNumber 
--	print @TipNumber 
	End 


Fetch_Error:
close  csr_Customer
deallocate  csr_Customer
GO
