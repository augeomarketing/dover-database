USE [214Continental]
SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[spLoadTransStandard]    Script Date: 04/08/2010 15:13:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadTransStandard]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadTransStandard]
GO 
/******************************************************************************/
/*    Load the TransStandard Table from the Transaction_input table*/
/* BY:  R.Tremblay  */
/* DATE: 1/2008   */
/* REVISION: 0 */
-- RDT 2008/06
/*  NOTE:  This uses the RewardsNow.dbo.Trantype table NOT the local one */
-- RDT 2010/04/08 -- Don't add transactions that don't have a customer 
/******************************************************************************/
-- ALTER PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS
Create PROCEDURE [dbo].[spLoadTransStandard] @DateAdded char(10) AS

-- Clear TransStandard 
Truncate table TransStandard 

-- RDT 2008/06 -- Move the PAN to the acctnum if the acctnum is null 
Update Input_Transaction 
	Set AcctNum = Pan 
	Where AcctNum is null 

-- RDT 2008/06 -- Move the CIFNum  to the acctnum if the acctnum and PAN are null 
Update Input_Transaction 
	Set AcctNum = CIFNUM 
	Where AcctNum is null and PAN Is Null 

-- RDT 2010/04/08 -- Don't add transactions that don't have a customer 
Delete From Input_Transaction where TipNumber Not in ( Select TipNumber from Customer_Stage )

-- Load the TransStandard table with rows from Input_Transaction
Insert TransStandard 
	( [TIP], [TranDate], [AcctNum], [TranCode], [TranNum], [TranAmt] ) 
select  [TipNumber], @DateAdded, [AcctNum], [TranCode], [PurchCnt], [Points] from Input_Transaction 

-- Set the TranType to the Description found in the RewardsNow.TranCode table
Update TransStandard set TranType = R.Description from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode 

-- Set the Ratio to the Ratio found in the RewardsNow.TranCode table
Update TransStandard set Ratio  = R.Ratio  from RewardsNow.dbo.Trantype R join TransStandard T 
on R.TranCode = T.Trancode