/****** Object:  StoredProcedure [dbo].[spLoadMissingNewCards]    Script Date: 03/17/2009 15:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.spLoadMissingNewCards    Script Date: 9/23/2008 3:30:19 PM ******/

/****** Object:  Stored Procedure dbo.spLoadMissingNewCards    Script Date: 9/11/2008 11:26:33 AM ******/
CREATE   PROCEDURE [dbo].[spLoadMissingNewCards]
	
AS
BEGIN
	Begin Transaction 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	truncate table input_newcards

	insert into input_NewCards
	(dateIssued, pan, acctnum, cifnum ) 
	select getdate() , pan, accountnum, cifnum from input_cif where 
	pan not in (select acctid from affiliat) 

	If exists ( SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[Temp_NewCards]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 )
	Begin
		Drop Table Temp_NewCards	
	End
	Select dateIssued, PAN, Acctnum, CIFNUM, RecordNum, Newflag 
		into Temp_NewCards
		From input_newcards 

	-- get tipnumbers to Temp_NewCards
	Update Temp_NewCards 
		Set tipnumber = a.tipnumber
		From Temp_NewCards n join affiliat a on n.acctnum = a.custid

	-- Remove dupes 
	Declare @Temp table  (Tipnumber char(15), PAN char(16), acctnum char(10))
	insert into @temp 
		select distinct Tipnumber,pan, acctnum from Temp_NewCards where tipnumber is not null 
	Truncate table Temp_NewCards 
	insert into Temp_NewCards (PAN,Acctnum,Tipnumber, recordnum )
		select PAN,Acctnum,Tipnumber,1  from @Temp 


	-- add the newcards to the affiliat table 
	Insert into affiliat_stage 
--	Insert into affiliat
	(Tipnumber, acctid, Accttype, Dateadded, acctstatus, Custid)
	select tipnumber, PAN, 'DEBIT', GetDate(), 'A', acctnum 
	From Temp_NewCards 
		where tipnumber is not null and 
		PAN not in ( select Acctid from affiliat) 

	-- Update lastname of affiliat where null
	Update affiliat_stage
--	Update affiliat
		Set LastName = c.LastName 
		From Customer c join Affiliat a on c.tipnumber = a.tipnumber 

	
	Commit tran
END
GO
