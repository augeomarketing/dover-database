use [214Continental] 
DROP PROCEDURE [dbo].[sp214StageMonthlyStatement]  
/****** Object:  StoredProcedure [dbo].[sp214StageMonthlyStatement]    Script Date: 03/17/2009 15:17:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- RDT 2/4/2008 Added FullSpectrum 
-- RDT 6/16/2008 Added SSN (misc3)
-- RDT 1/7/2009 Added IR to redemptions 

*/
/*******************************************************************************/

CREATE PROCEDURE [dbo].[sp214StageMonthlyStatement]  @StartDateParm char(10), @EndDateParm char(10)

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 	--RDT 10/09/2006 
Declare @EndDate DateTime 	--RDT 10/09/2006 
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	--RDT 10/09/2006 
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

-- RDT 6/16/2008 Added SSN (misc3) 
insert into Monthly_Statement_File (tipnumber, acctid, acctname1, acctname2, address1, address2, address3, citystatezip, ssn )
select tipnumber, Misc2, acctname1, acctname2, address1, address2, address3, (rtrim(city) + ' ' + rtrim(state) + ' ' + zipcode) , Misc3 
from customer_Stage


/* Load the statmement file with CREDIT purchases          */
update Monthly_Statement_File
set pointspurchasedCR =(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='63')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='63')

/* Load the statmement file  with CREDIT returns            */
update Monthly_Statement_File
set pointsreturnedCR=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
			 and histdate>=@startdate and histdate<=@enddate and trancode='33')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='33')

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File
set pointspurchasedDB=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='67')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='67')

/* Load the statmement file with DEBIT  returns            */
update Monthly_Statement_File
set pointsreturnedDB=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='37')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			 and histdate>=@startdate and histdate<=@enddate and trancode='37')

/* Load the statmement file with bonuses            */
update Monthly_Statement_File
set pointsbonus=(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'B%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode like  'B%')

-- Split Bonuses BA - activation
update Monthly_Statement_File
set BonusBA =(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BA')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BA')

-- Split Bonuses BI - Client Estatment 
update Monthly_Statement_File
set BonusBI =(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BI')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BI')

-- Split Bonuses BE - RNI Estatement
update Monthly_Statement_File
set BonusBE =(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BE')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BE')

-- Split Bonuses Bonuses BN - new account 
update Monthly_Statement_File
set BonusBN =(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BN')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber
			and histdate>=@startdate and histdate<=@enddate and trancode = 'BN')

/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode='IE')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
				and histdate>=@startdate and histdate<=@enddate and trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
update Monthly_Statement_File
set pointsadded=pointsadded + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DR')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DR')

/* Load the statmement file with FullSpectrum Points  */
update Monthly_Statement_File
set FullSpectrum = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'F%')
 where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'F%')


/* Add FSONlineBill (FC) Points */
update Monthly_Statement_File
set FSOnlineBill  = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FC')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FC')

/* Add FSDirectDep (FD) Points */
update Monthly_Statement_File
set FSDirectDep  = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FD')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FD')

/* Add Personal Loan (FP) Points */
update Monthly_Statement_File
set FSLoan  = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FP')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FP')

/* Add SavingsBal (FS) Points */
update Monthly_Statement_File
set FSSavingBal  = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FS')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FS')
/* Add Line of credit (FT) Points */
update Monthly_Statement_File
set FSLineCredit  = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FT')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='FT')
/* Load the statmement file with total point increases */
/* Load the statmement file with total point increases */
update Monthly_Statement_File
set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded + FullSpectrum 

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with minus adjustments    */
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DE')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='DE')

/* Add EP to  minus adjustments    */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='EP')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='EP')

/* Add IR to  minus adjustments    */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='IR')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='IR')

/* Add expired Points */
update Monthly_Statement_File
set PointsExpire = (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='XP')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
			and histdate>=@startdate and histdate<=@enddate and trancode='XP')

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
-- RDT 8/28/2007  Added expired Points  set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted 
set pointsdecreased=pointsredeemed + pointsreturnedCR + pointsreturnedDB + pointssubtracted + PointsExpire


/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update Monthly_Statement_File
set pointsbegin=(select monthbeg'+ @MonthBegin + N' from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)
where exists(select * from Beginning_Balance_Table where tipnumber=Monthly_Statement_File.tipnumber)'

exec sp_executesql @SQLUpdate


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased

/* Load the acctid to the monthly statement table */
-- RDT Use CIFNum -- Update Monthly_Statement_file  set acctid = a.acctid from affiliat_Stage a where Monthly_Statement_file.tipnumber = a.tipnumber
GO
