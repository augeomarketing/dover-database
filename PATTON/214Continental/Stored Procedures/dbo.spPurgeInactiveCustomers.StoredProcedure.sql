/****** Object:  StoredProcedure [dbo].[spPurgeInactiveCustomers]    Script Date: 03/17/2009 15:17:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* Purge Customers from Staged or Production Tables if there is No Activity in the History  */
/* BY:  R.Tremblay  */
-- Parms. 
-- @InActiveMonths = number of months remove customer if greater than the DateLastUsed.
/* RDT 07/08/2009
	Changed code to use the DateLastUsed or if null use the customer DateAdded.
   RDT 10/08/2009
	Added Code to calculate the end of month correctly. 
*/
/******************************************************************************/
alter PROCEDURE [dbo].[spPurgeInactiveCustomers]  @InActiveMonths int , @DateDeleted char(10) AS

If @InactiveMonths > 0 Set @InactiveMonths = @InactiveMonths * -1 

--    RDT 10/08/2009
declare @monthPurge datetime
set @monthPurge = @DateDeleted
-- Add a day to @DateDeleted to get the first date of the next month
set @monthPurge = DateAdd( day, 1 , @monthPurge) 
print @monthPurge 
-- Subtract the number of inactive months from @DateDeleted
set @monthPurge = DateAdd( Month, @InactiveMonths, @monthPurge) 
print @monthPurge 
-- Subtract one day from the @DateDeleted to get the last day of the previous month.
set @monthPurge = DateAdd( day, -1, @monthPurge) 
print @monthPurge 

-- Get records from Customer_Stage and if the DateLastUsed (misc1) is greater than the 
-- @inactive month, shove it into the purge table.

Insert into Input_Purge 	
	(Tipnumber , PAN , CardStatus, CIFNum, DateIssued) 
	Select C.Tipnumber , A.AcctId, 'I', C.Misc4, Misc1
	From Customer_Stage C join Affiliat_Stage A on C.Tipnumber = A.Tipnumber
	Where Misc1 <= @monthPurge 
	and Misc1 is not null 

-- If DateLastUsed (misc1) is null use the DateAdded column  
Insert into Input_Purge 	
	(Tipnumber , PAN , CardStatus, CIFNum, DateIssued) 
	Select C.Tipnumber , A.AcctId, 'I', C.Misc4, C.DateAdded
	From Customer_Stage C join Affiliat_Stage A on C.Tipnumber = A.Tipnumber
	Where C.DateAdded <= @monthPurge
	and Misc1 is null 


GO
