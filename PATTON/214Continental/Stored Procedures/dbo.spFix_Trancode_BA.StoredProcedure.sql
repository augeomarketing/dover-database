/****** Object:  StoredProcedure [dbo].[spFix_Trancode_BA]    Script Date: 03/17/2009 15:17:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spFix_Trancode_BA] AS

Declare @Tipnumber char(15) 
Declare @Points int
--  cursor thru the FIX_TRancode_BA table and subtract the points from the customer 
Declare csr_FIx Cursor for Select Distinct Tipnumber from Fix_Trancode_BA

Open Csr_Fix
Fetch csr_Fix into @Tipnumber 

If  @@FETCH_STATUS = 1
	goto Fetch_Error
	
/*                                                                            */
While @@FETCH_STATUS = 0
BEGIN 	    
	set @Points = ( Select sum(points) from Fix_Trancode_BA where tipnumber = @Tipnumber) 

	-- add a "DE" record in the history table
	Insert into History 
	(Tipnumber, HistDate, Trancode, Points, Description, Ratio ) 
	Values
	(@Tipnumber, GetDate(), 'DE', @points, 'Decrease Earned', -1)

	-- subtract the points from runBalance and runAvailable in Customer
	Update Customer
	Set 	runbalance = runbalance - @Points, 
		runavailable = runavailable - @points
	Where Tipnumber = @Tipnumber 

	Fetch csr_Fix
	into  @tipnumber
	
END /*while */

Fetch_Error:
close  csr_Fix
deallocate  csr_Fix
GO
