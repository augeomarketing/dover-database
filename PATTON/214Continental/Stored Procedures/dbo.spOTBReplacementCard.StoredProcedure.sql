/****** Object:  StoredProcedure [dbo].[spOTBReplacementCard]    Script Date: 03/17/2009 15:17:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*****************************************/
/* This updates the ONETIMEBONUS_Stage accounts that were replaced.  */
/* If a new Account is replaceing an existing Account the new Account s not elegible for a NewCard Bonus */
/*****************************************/
/*Author: Tremblay  */
/* Date 6/27/2007  */
/* Revised: */
/*****************************************/

CREATE PROCEDURE [dbo].[spOTBReplacementCard] @TranCode Char(2) AS

Insert into onetimebonuses_stage
select i.tipnumber, o.trancode, i.accountnum, o.dateawarded from input_transaction i join onetimebonuses_stage o 
	on i.tipnumber = o.tipnumber 
	and i.oldaccountnum = o.acctid 
	and o.trancode = @TranCode
GO
