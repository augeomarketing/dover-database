/****** Object:  StoredProcedure [dbo].[spEmailNotification]    Script Date: 03/17/2009 15:17:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 

*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spEmailNotification] @EndDate char(12) , @TipPrefix char(4) AS

Declare @MonthStr char(12)
Declare @Message varchar(50)
Set @MonthStr = DATENAME(month, @EndDate) 

set @Message = @TipPrefix + 'Processing Complete for '+ @MonthStr 

insert into Maintenance.dbo.[PerleMail] 
( dim_perlemail_subject, dim_perlemail_body, 
dim_perlemail_to ,dim_perlemail_from, dim_perlemail_bcc )
values
(
@Message , 
'Please Send EMail Notifications for ' + @MonthStr,
'ssmith@rewardsnow.com', 
'rtremblay@rewardsnow.com'
 ,1)
GO
