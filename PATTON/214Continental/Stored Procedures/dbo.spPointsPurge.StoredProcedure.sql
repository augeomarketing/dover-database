USE [214Continental]
GO

/****** Object:  StoredProcedure [dbo].[spPointsPurge]    Script Date: 01/14/2011 15:18:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPointsPurge]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPointsPurge]
GO

USE [214Continental]
GO

/****** Object:  StoredProcedure [dbo].[spPointsPurge]    Script Date: 01/14/2011 15:18:00 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spPointsPurge] @DateInput nvarchar(10) AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/* Modified 11/2007 BJQ now Points to the Stage Files  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
/*  **************************************  */
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */

   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat_stage set AcctStatus = 'A' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer_stage set Status = 'A' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat_stage	
	set Acctstatus = '9'
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat_stage.acctid = AccountDeleteInput.acctid )
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted_stage 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID ) 

	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID 
	 FROM affiliat_stage 
	 WHERE  affiliat_stage.acctstatus = '9'
	
	/*  **************************************  */
	/* STATEMENT MOVED TO THE END OF THE PROCEDURE               */
	--Delete from affiliat_stage where acctstatus = '9'
	
	/*  **************************************  */
	---- Find Customers without accounts and mark for delete 
	--update Customer_stage
	--set Status='9'
	--where tipnumber not in 
	--	(select  distinct(tipnumber)  from affiliat_stage )
	
	-- Statement modified to mark only customers for whom all accounts have been deleted
	update Customer_stage
	set Status='9'
	where tipnumber  in 
		(select  distinct(tipnumber)  from affiliat_stage  where acctstatus = '9')
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted_stage]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,' ',RunAvaliableNew, @DateDeleted 
	from customer_stage WHERE  status = '9'


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted_stage
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio],overage )

	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], overage 
	from history_stage 
	where tipnumber in 
	(select tipnumber from customer_stage where status = '9')
	/*  **************************************  */
	-- Delete History
	Delete from history_stage where tipnumber in 
	(select tipnumber from customer_stage where status = '9')
	
	/*  **************************************  */
	/*  DELETE FROM AFFILIAT                    */
	Delete from affiliat_stage where acctstatus = '9'
	
	-- Delete Customer
	Delete from customer_stage where status = '9'
	/* Return 0 */
	

End




GO


