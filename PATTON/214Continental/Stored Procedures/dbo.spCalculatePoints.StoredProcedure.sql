/****** Object:  StoredProcedure [dbo].[spCalculatePoints]    Script Date: 03/17/2009 15:17:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  
	Loads Input_Transaction and 
	Calculates points in input_transaction  

****** This sProc Calls spNewCardBonus ******
****** This sProc Calls spNewCardBonus ******
****** This sProc Calls spNewCardBonus ******

Trancode 	Desc
BA 		Debit card first use (Activation)
BE 		Bonus Email Notification (RNI)
BI 		Estatements (sent by FI)
BN		New DDA account -- calls spNewCardBonus @EndDate
FC		Online BillPaid
FD		Input_DirectDeposit - onetime bonus
FP		New Loan Bonus
FS		Savings Account Balance
FT 		Line of Credit
67		Check card
tip = account number 


*/
/*  **************************************  */
CREATE PROCEDURE [dbo].[spCalculatePoints]  @EndDate Char(10)  AS   

Declare @PurchAmt int

--- No points for Jan 2008
If year ( @EndDate ) = 2008 and Month (@EndDate ) = 1 
	Begin
		set @PurchAmt = 0 
	End
	Else 	set @PurchAmt = 1 


Truncate table Input_Transaction 

/*		dbo.Input_BillsPaid	*/
Insert into Input_transaction
	(CIFNum, AcctNum, PurchAmt, Trancode, Trandate )
	select CIFNum, AcctNum, Expense, 'FC' , @EndDate  from Input_BillsPaid


/*   BN NEW card bonus  to input_transaction */
/****** This sProc Calls spNewCardBonus ******/
exec spNewCardBonus @EndDate
/****** This sProc Calls spNewCardBonus ******/

/*		dbo.Input_CheckCard (debit cards) purchases	*/
Insert into Input_transaction
	(PAN, PurchAmt, Trancode, Trandate  )
	select PAN, sum(tranamount), '67', @EndDate from Input_CheckCard 
		where trancode = '40'  	--03/05/08
--		where trancode = '427' 	-- 03/05/08
		group by PAN

-- Add the acctid from the input_checkcard
-- RDT 06/05/2008 	Update Input_Transaction set acctnum = c.acctnum
Update Input_Transaction set acctnum = c.PAN  	-- RDT 06/05/2008 
	from input_checkcard c , input_transaction t where c.pan  = t.PAN 
	and t.Trancode = '67' 


-------- BONUS POINTS for first time use of debit card @PurchAmt
Insert into Input_transaction
	(PAN, PurchAmt, Trancode, Trandate  )
	select PAN, @PurchAmt, 'BA', @EndDate from Input_CheckCard 
-- RDT 06/05/2008 where trancode = '40'  and  PAN not in (select acctid from OneTimeBonuses_stage  ) 
		where trancode = '40'  and  PAN not in (select acctid from OneTimeBonuses_stage where trancode = 'BA' )  -- RDT 06/05/2008		
		group by PAN 

-- Add the acctid from the input_checkcard
Update Input_Transaction set acctnum = c.acctnum 
	from input_checkcard c , input_transaction t where c.pan  = t.PAN 
	and t.Trancode = 'BA' 

-- 	Add BA records to OneTimeBonuses_Stage	--
Insert into OneTimeBonuses_Stage 
	(Tipnumber, Trancode, Acctid, DateAwarded ) 
	select Tipnumber, Trancode, PAN, @EndDate from Input_Transaction 
		where Trancode = 'BA' --	and PAN not in ( select acctid from OneTimeBonuses_Stage where Trancode = 'BA' )

/*		dbo.Input_CheckCard (debit cards) returns 	*/
Insert into Input_transaction
	( PAN , PurchAmt, Trancode, Trandate  )
	select PAN , sum(tranamount), '37', @EndDate from Input_CheckCard 
		where trancode = '450'  group by PAN 

-- Add the acctid from the input_checkcard
Update Input_Transaction set acctnum = c.acctnum 
	from input_checkcard c , input_transaction t where c.pan  = t.PAN 
	and t.Trancode = '37' 

/*		dbo.Input_DirectDeposit - one time award		*/
Insert into Input_transaction
--3/6/08--	(CIFNum, Acctnum, Trancode,  purchamt, Trandate )
--3/6/08--	select Distinct CIFNum, Acctnum, 'FD',  @PurchAmt,  @EndDate from input_DirectDeposit
	(Acctnum, Trancode,  purchamt, Trandate )
	select Distinct Acctnum, 'FD',  @PurchAmt,  @EndDate from input_DirectDeposit
		 where Acctnum not in (select Acctid from OneTimeBonuses_stage where Trancode = 'FD') 

-- 	Add FD records to OneTimeBonuses_Stage	--
Insert into OneTimeBonuses_Stage 
	(Tipnumber, Trancode, Acctid, DateAwarded ) 
	select Tipnumber, Trancode, Acctnum, @EndDate from Input_Transaction 	
		where Trancode = 'FD' and Acctnum not in ( select acctid from OneTimeBonuses_Stage where Trancode = 'FD' )

/* 		dbo.Input_Estatement		*/
Insert into Input_transaction
	(cifNum, Trancode, purchamt, Trandate )
	select Distinct CIFNum, 'BI', 1 , @EndDate from Input_Estatement 
		where Month( OpenDate ) = Month( @EndDate ) 
		    and Year( OpenDate ) = Year( @EndDate ) 
		and  CIFNum not in (select Acctid from OneTimeBonuses_stage where Trancode = 'BI')

-- 	Add BI records to OneTimeBonuses_Stage (CIFNum) 	--
Insert into OneTimeBonuses_Stage 
	(Tipnumber, Trancode, Acctid, DateAwarded ) 
	select Tipnumber, Trancode, CIFNum, TranDate from Input_Transaction 
		where Trancode = 'BI' 

/*		dbo.Input_LineOfCredit		*/ 
-- No Points added for < 5,000
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, purchamt, Trandate )
	select CIFNum, 'FT', Acctnum, 0, 		@EndDate
	from Input_LineOfCredit where currbalance < 5000 

--   $5,000 to $10,000 = 100 points ( 50 points  * 2 )
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, purchamt, Trandate )
	select CIFNum, 'FT', Acctnum,  2, 		@EndDate
	from Input_LineOfCredit 
		where currbalance between 5000 and 10000 

--   $10,000 to $50,000 = 150 points ( 50 points * 3 )
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, purchamt, Trandate )
	select CIFNum, 'FT', Acctnum,  3,		@EndDate
	from Input_LineOfCredit 
		where currbalance between 10001 and 50000 

--   Over $50,000 = 200 points ( 50 points * 4 ) 
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, purchamt, Trandate )
	select CIFNum, 'FT', Acctnum, 4,		 @EndDate
	from Input_LineOfCredit 
		where currbalance >= 50001

/*		dbo.Input_Loans		*/
Insert into Input_transaction
	(CIFNum, AcctNum, Trancode, purchamt, Trandate )
--RDT 4/3/08 Select Distinct AcctNum, 'FP',    1 , @EndDate from Input_Loans
	select Distinct CIFNUM, AcctNum, 'FP',    1 , @EndDate from Input_Loans
		 where Year( OriginalDate ) = Year( @EndDate ) 
			and Month( OriginalDate ) = Month( @EndDate ) 
			and AcctNum not in (select Acctid from OneTimeBonuses_stage where Trancode = 'FP')

-- 	Add records to OneTimeBonuses_Stage	--
Insert into OneTimeBonuses_Stage 
	(Tipnumber, Trancode, Acctid, DateAwarded ) 
	select Tipnumber, Trancode, AcctNum, @EndDate from Input_Transaction 
		where Trancode = 'FP' and CIFNum not in ( select acctid from OneTimeBonuses_Stage where Trancode = 'FP' )

/*		Input_SavingMMKt		*/
--      $1,000 to $10,000 = 50 points ( 50 points  * 1 )
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, Trandate, purchamt)
	select CIFNum, 'FS', Acctnum , @EndDate, 1
	from Input_SavingMMKt 
		where currbalance between 1001 and 10000 

--   $10,001 to $50,000 = 100 points ( 50 points * 2 )
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, Trandate, purchamt)
	select CIFNum, 'FS', Acctnum, @EndDate, 2
	from Input_SavingMMKt 
		where currbalance between 10001 and 50000 

--   Over $50,000 = 150 points ( 50 points * 3 ) 
Insert into Input_transaction
	(CIFNum, Trancode, Acctnum, Trandate, purchamt)
	select CIFNum, 'FS', Acctnum , @EndDate, 3
	from Input_SavingMMKt 
		where currbalance >= 50001

-- BE Email Notification Bonus
Insert Into Input_Transaction 
	(Tipnumber, Trancode, Trandate, PurchAmt ) 
	select tipnumber, 'BE',  @EndDate, 1 from Input_RNI_EmailBonus 
		where Tipnumber not in ( select TipNumber from OneTimeBonuses_Stage where Trancode = 'BE' )

Insert into OneTimeBonuses_Stage 
	(Tipnumber, Trancode, DateAwarded ) 
	select Tipnumber, Trancode, @EndDate from Input_Transaction 
		where Trancode = 'BE' -- and Tipnumber not in ( select TipNumber from OneTimeBonuses_Stage where Trancode = 'BE' )


--- Calculate points from Trancode_factor table
Update Input_Transaction 
	set Points = I.purchamt * F.pointFactor 
	from input_Transaction I join Trancode_factor F on i.Trancode = f.Trancode
-- If the customer wants to override the point factor add "where points is null". 
-- see Ticket #31423
GO
