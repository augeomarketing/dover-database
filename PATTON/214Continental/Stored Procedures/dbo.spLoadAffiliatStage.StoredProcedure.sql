/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 03/17/2009 15:17:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_CIF to the Affiliat_stage table   
		this allows for adding an account that doesn't have a transaction */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
-- -- RDT 09/04/2008 Update the affiliat_stage with new tips 
-- RDT 06/03/2009 If a new affiliat is added to a 
--				closed customer the status should be changed to 'A'
	
/*  **************************************  */

Alter PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 


/************ Insert New DEBIT Accounts into Affiliat Stage  from input_CIF by TIPNUMBER ***********/
 Insert Into Affiliat_Stage
	( Acctid, Tipnumber, AcctType, DateAdded, AcctStatus, CustId )
	select distinct PAN, TipNumber, AcctType, @MonthEnd, 'A', AccountNum
	from input_CIF 
	where PAN  not in ( Select Acctid from Affiliat_Stage) 


-- RDT 09/04/2008 Update the affiliat Stage with the new tipnumbers
Update affiliat_stage
	set tipnumber = c.tipnumber 
	from affiliat_Stage a join input_cif c on a.acctid = c.pan
	Where a.Tipnumber is null 


/***** Update the Affiliat_Stage desctription from TranType ****/
Update Affiliat_Stage
	Set AcctType = ATT.AcctTypeDesc 
	from AcctType ATT join Affiliat_Stage AFF on ATT.AcctType = Aff.Accttype 


/************ Update Last name from Input_Customer by TIPNUMBER ***********/
Update Affiliat_Stage 
	Set LastName  = C.LastName 
	from Input_customer  C Join Affiliat_Stage A on C.Tipnumber = A.TipNumber

-- RDT 06/03/2009 
Update Customer_Stage 
	set status = 'A' 
	where status = 'C' and	Tipnumber in 
		(select distinct tipnumber from affiliat_stage where acctstatus = 'A')

GO
