/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 03/17/2009 15:17:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/*  RDT 3/16/07  */

/* 

This retrieves all customers that were added in the previous month
It does not do an exact date match on the custom date added column

RDT 4/17/08 Stripped last 4 from zipcode 
  */
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 


Truncate Table Welcomekit 

insert into Welcomekit SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
				ADDRESS2, ADDRESS3, City, State, Left(ZipCode,5) as ZipCode
--ZipCode 
		         FROM customer WHERE (Year(DATEADDED) = Year(@EndDate)AND Month(DATEADDED) = Month(@EndDate)AND Upper(STATUS) <> 'C') 

update DateforAudit set Datein=@EndDate
GO
