
USE [214Continental]
GO
/****** Object:  StoredProcedure [dbo].[spPurgeClosedCustomers]    Script Date: 11/18/2009 15:51:07 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    Purge Customers from Staged or Production Tables based on Flag parameter.  */
/* BY:  R.Tremblay  */
/* DATE: 1/2007   */
/* REVISION: 0 */
-- Parms. 
-- @Production_Flag ( Default =  Staged tables )
-- @DateDeleted char(10), 

-- !!!!! calls procedure spFlagCustomerClosedAccount !!!!!!
-- !!!!! calls procedure spFlagCustomerClosedAccount !!!!!!

-- RDT 10/06/2008 Add code to hold purge for 2 months. 
-- Purge will happen on CURRENT month (GetDate())
-- Input_Purge_hold 

-- This will delete all customers with a status of 'C'losed OR 'H'ot card.

-- RDT 12/05/2008 delete records from input_Customer Also.

-- RDT 06/22/2009 Added call to spPurgeInactiveCustomers
-- !!!!! calls procedure spPurgeInactiveCustomers !!!!!!
--  This will purge inactive customers. It will not hold them for 2 months.
-- Don't start purging inactive Customers until August Processing. 
-- RDT 10/9/2009 - commented out call spPurgeInactiveCustomers  per Dawn. 
--  They aren't comfortable with the number and types of accounts being deleted using their logic.
--  I bet they set a flag in the CIF .RDT.
--   

/******************************************************************************/
ALTER   PROCEDURE [dbo].[spPurgeClosedCustomers]  @Production_Flag char(1), @DateDeleted char(10) AS

--declare @Production_Flag char(1), @DateDeleted char(10)
--set @Production_Flag = 'S'
--set @DateDeleted = '10/31/2009'
Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)

-- RDT 10/06/2008
Declare @MonthsToHold int
declare @DateDeletedDatetime char(10) 
declare @DatetoPurge datetime 

set @MonthsToHold = 2
set @DateDeletedDatetime = 	cast (Year(@DateDeleted ) as char(4)) +'/'+ rtrim(cast ( Month(@DateDeleted ) as char(2)) ) +'/01'
set @DateToPurge = DateAdd(Month, 2, @DateDeletedDatetime )
--print '@DateDeletedDatetime'
--print @DateDeletedDatetime
--print '@DateToPurge'
--print @DateToPurge

---- Purge Pending Process
	-- copy any input_purge_pending into input_purge 
	Insert into Input_Purge 
		select * from Input_Purge_Pending

	-- Clear Input_Purge_Pending 
	Truncate Table Input_Purge_Pending
	
	
-- BJQ 11/09/2009
								
	Insert into input_Purge_Pending 
		select *
		from Purge_Customer_Input
		where tipnumber in ( select distinct tipnumber 
							 from history_stage 
							 where histdate > @DateDeleted and TranCode <> 'RQ' )		
	  

-- BJQ 11/09/2009	

	-- Copy any customers from input_purge to input_purge_pending if 
	-- they have History activity greater than the delete date
	Insert into input_Purge_Pending 
		select * from input_Purge  
		where tipnumber in ( select distinct tipnumber 
-- RDT 5/11/09  			 from history
							 from history_stage 
							 where histdate > @DateDeleted and TranCode <> 'RQ' )



	-- Remove any customers from input_purge if they have 
	-- current activity in input_Purge_Pending (copied from history_stage )
	Delete from input_Purge 
		where tipnumber in ( Select Distinct tipnumber From input_Purge_Pending )


-- BJQ 11/09/2009

	Delete from Purge_Customer_Input 
		where tipnumber in ( Select Distinct tipnumber From input_Purge_Pending )
			

		
-- BJQ 11/09/2009			

-- RDT 10/06/2008 start
-- Insert new records from input_purge into Input_Purge_hold
	Insert into Input_Purge_Hold 
	Select	DateIssued, 
			PAN, 
			CardStatus, 
			HotCardDate, 
			CIFNum, 
			Tipnumber, 
			@DateDeleted, 
			@DateToPurge
		From Input_Purge where PAN not in (select PAN from Input_Purge_Hold )
/*
Truncate Input_purge and load with records from Input_Purge_Hold 
Only if the DateToPurge year and month matches the current processing month.
*/
Truncate Table Input_Purge


-- BJQ 11/09/2009		

insert into input_Purge
select *
from Purge_Customer_Input
		
-- BJQ 11/09/2009		

Insert into Input_Purge
select DateIssued, PAN, CardStatus, HotCardDate, CIFNum, TipNumber 
	from Input_Purge_Hold
	where Year( DateToPurge ) = Year( @DateDeleted ) and 
		  Month( DateToPurge ) = Month( @DateDeleted )
-- RDT 06/22/2009 
-- RDT 10/9/2009 If @DateDeleted > '09/01/2009' exec spPurgeInactiveCustomers  8 , @DateDeleted 

-- RDT 10/06/2008 end 
exec spFlagCustomerClosedAccount @Production_Flag



----------- Stage Table Processing ----------
If @Production_Flag <> 'P'
----------- Stage Table Processing ----------
Begin
	Truncate Table CustomerDeleted_stage 	
	Truncate Table AffiliatDeleted_Stage 	
	Truncate Table HistoryDeleted_stage 	

	Insert into CustomerDeleted_stage 
		select *, @DateDeleted from   Customer_stage 	
			where Status <> 'A' 	

	Insert into AffiliatDeleted_Stage 	
		select * from Affiliat_Stage 	
			where TipNumber in 
			   	(select TipNumber from Customer_Stage where Status <> 'A') 
			or AcctStatus <> 'A'

	Insert into HistoryDeleted_stage 	
		select * from History_stage 	
			where TipNumber in 
				(select TipNumber from Customer_Stage where Status <> 'A') 

-- RDT 12/05/2008 delete records from input_Customer Also.
	Delete from Input_Customer 	where Tipnumber in 
		(select Tipnumber from Customer_stage where Status <> 'A' )

	Delete from Customer_stage 	
		where Status <> 'A' 

	Delete from Affiliat_Stage 	
		where TipNumber not in (select TipNumber from Customer_Stage) 
				or AcctStatus <> 'A'
	Delete from History_stage 	
		where TipNumber not in (select TipNumber from Customer_Stage)

End

----------- Production Table Processing ----------
If @Production_Flag = 'P'
----------- Production Table Processing ----------
Begin
	Begin Tran 

	-- flag all Undeleted Customers "A" that have an input_purge_pending record 
	Update customer set status = 'A' 
		where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)

	-- Insert customer to customerdeleted 
	Insert Into CustomerDeleted 
		Select c.*, @DateDeleted  
		From Customer c 
		Where Status <> 'A'

	-- Insert affiliat to affiliatdeleted 
-- I have no idea why this doesn't work. I keep getting a datetime conversion error on the datedeleted. Yet it works in the CustomerDelete table

	Insert Into AffiliatDeleted 
		(AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
		 LastName, YTDEarned, CustId, DateDeleted )
		Select AcctId, TipNumber, AcctType, DateAdded, SecId, AcctStatus, AcctTypeDesc, 
			   LastName, YTDEarned, CustId, @DateDeleted as DateDeleted 
		From Affiliat  
		Where AcctStatus <> 'A' 

	-- copy history to historyDeleted 
	Insert Into HistoryDeleted 
		Select H.* , @DateDeleted as DateDeleted 
		From History H 
		Where Tipnumber in (Select Tipnumber from Customer Where Status <> 'A' )

	-- Delete records from History 
	Delete from History 
		where TipNumber in (Select Tipnumber from Customer Where Status <> 'A')

	-- Delete records from affiliat 
	Delete from Affiliat   
		Where AcctStatus <> 'A'

	-- Delete from customer 
	Delete from Customer
		Where Status <> 'A'

	-- flag all Undeleted Customers "C" that have an input_purge_pending record can't redeem
	Update customer set status = 'C' 
		where tipnumber in (Select Distinct Tipnumber from input_Purge_Pending)

	Commit Transaction 

End
