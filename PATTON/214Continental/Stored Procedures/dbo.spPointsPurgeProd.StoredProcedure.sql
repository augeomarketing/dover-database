USE [214Continental]
GO

/****** Object:  StoredProcedure [dbo].[spPointsPurgeProd]    Script Date: 01/14/2011 15:19:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPointsPurgeProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPointsPurgeProd]
GO

USE [214Continental]
GO

/****** Object:  StoredProcedure [dbo].[spPointsPurgeProd]    Script Date: 01/14/2011 15:19:59 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[spPointsPurgeProd] @DateInput as varchar(10) AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/* Modified 11/2007 BJQ Points to the production Files  */
/*  **************************************  */
/*  Marks accouts for Delete from AccountDeleteInput */
/*  Copy flagged Accouts to Accountdeleted  */
/*  Delete Account */
/*  Check for customers without accounts and flag for delete */
/*  Mark history for delete where customer flagged for delete */
/*  Copy flagged customers to Customerdeleted  */
/*  Copy flagged history to Historydeleted  */
/*  Delete History */
/*  Delete Customer*/
--- RDT 01/13/2011 added delete date to affiliatdeleted and historydeleted

/*  **************************************  */
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */

   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat set AcctStatus = 'A' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer set Status = 'A' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat 	
	set Acctstatus = '9'
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat.acctid = AccountDeleteInput.acctid )
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	 INSERT INTO AffiliatDeleted  
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID 
	   , DateDeleted ) --- RDT 01/13/2011 
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID
	 , @DateDeleted --- RDT 01/13/2011 
	 FROM affiliat  
	 WHERE  affiliat.acctstatus = '9'
	
	/*  **************************************  */
	/* STATEMENT MOVED TO THE END OF THE PROCEDURE               */
	--Delete from affiliat  where acctstatus = '9'
	
	/*  **************************************  */
	---- Find Customers without accounts and mark for delete 
	--update Customer 
	--set Status='9'
	--where tipnumber not in 
	--	(select  distinct(tipnumber)  from affiliat  )
	
	-- Statement modified to mark only customers for whom all accounts have been deleted
	update Customer 
	set Status='9'
	where tipnumber  in 
		(select  distinct(tipnumber)  from affiliat   where acctstatus = '9')
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,RunBalanceNew,RunAvaliableNew, DateDeleted)
	select 
	TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	Misc1,Misc2,Misc3,Misc4,Misc5,' ',RunAvaliableNew, @DateDeleted 
	from customer  WHERE  status = '9'


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9
	INSERT INTO HistoryDeleted 
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio],overage
	,DateDeleted ) --- RDT 01/13/2011 
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], overage
	, @DateDeleted --- RDT 01/13/2011 
	from history  
	where tipnumber in 
	(select tipnumber from customer  where status = '9')
	/*  **************************************  */
	-- Delete History
	Delete from history  where tipnumber in 
	(select tipnumber from customer  where status = '9')
	
	/*  **************************************  */
	/*  DELETE FROM AFFILIAT                    */
	Delete from affiliat  where acctstatus = '9'
	
	-- Delete Customer
	Delete from customer  where status = '9'
	/* Return 0 */
	

End




GO


