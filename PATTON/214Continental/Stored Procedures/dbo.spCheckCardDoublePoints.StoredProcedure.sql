/****** Object:  StoredProcedure [dbo].[spCheckCardDoublePoints]    Script Date: 03/17/2009 15:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rich Tremblay
-- Create date: 3/2/2009
-- Description:	Double the points on check card file if flag = 'Y'
-- =============================================
CREATE PROCEDURE [dbo].[spCheckCardDoublePoints]
@Factor Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Input_transaction 
		set Points = Points * @Factor 
		where trancode = '67' and 
		PAN in (select pan from input_CheckCard where DoublePointsFlag = 'Y')

END
GO
