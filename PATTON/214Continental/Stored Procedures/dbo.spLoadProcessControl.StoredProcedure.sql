/****** Object:  StoredProcedure [dbo].[spLoadProcessControl]    Script Date: 03/17/2009 15:17:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
  Hot Card.csv "hot"
  New Cards in Jan.csv "new"

Read the "new" file and assign record numbers based on the order of the records in the input file.
Match the "new" CIF number of the "hot" CIF number.
If the HotCardDate column in the "hot" file matches the DateIssued column in the "new" file, 
the PAN in the "new" file is considered a replacement. The bonus for a New account is not to be awarded for that PAN. 

If the new card is not a replacement use the account number to link the PAN to the RewardsNow number. 
If there are multiple account numbers use the account number from the first record. 

read input_CIF and load the work_names with account (dda) and all names associated with input_Cif account
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadProcessControl] @MonthEnd char(12) AS

Declare @MonthBegin char(12) 

Set @MonthBegin = Convert( varchar(4), year( @monthEnd ) ) +'/' + convert( Varchar(2), month( @monthEnd )) + '/01'

Insert into aux_ProcessControl 
( Rundate , MonthBegin, MonthEnd )
values
( GetDate() , @MonthBegin, @MonthEnd)
GO
