/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 03/17/2009 15:17:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_customer into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* BY:  R.Tremblay  */
/* DATE: 1/2008   */
/* RDT 7/8/2009 - Added code to update the Misc1 field with the latest date of DateLastUsed 
					from the input_cif table. 
*/
/******************************************************************************/	
alter PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS
/* Update Existing Customers                                            */
Update Customer_Stage
set 	STATUS = I.STATUS,
	LASTNAME = I.LASTNAME,
	ACCTNAME1 = I.ACCTNAME1,
	ACCTNAME2 = I.ACCTNAME2,
	ACCTNAME3 = I.ACCTNAME3,
	ACCTNAME4 = I.ACCTNAME4,
	ACCTNAME5 = I.ACCTNAME5,
	ACCTNAME6 = I.ACCTNAME6,
	ADDRESS1 = I.ADDRESS1,
	ADDRESS2 = I.ADDRESS2,
	ADDRESS3 = I.ADDRESS3,
	ADDRESS4 = I.ADDRESS4,
	City = I.City,
	State = I.State,
	ZipCode = I.ZipCode,
	HOMEPHONE = I.HOMEPHONE,
	WORKPHONE = I.WORKPHONE,
	BusinessFlag = I.BusinessFlag,
	EmployeeFlag = I.EmployeeFlag,
	SegmentCode = I.SegmentCode,
	ComboStmt = I.ComboStmt,
	NOTES = I.NOTES,
	BonusFlag = I.BonusFlag,
	Misc1 = I.Misc1,
	Misc2 = I.Misc2,
	Misc3 = I.Misc3,
	Misc4 = I.Misc4,
	Misc5 = I.Misc5
From Input_Customer I, Customer_Stage S
Where I.TIPNUMBER = S.TIPNUMBER 

/*Add New Customers                                                      */
Insert into Customer_Stage
	select * from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
	Set STATUS = 'A' 
	Where STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
	Set StatusDescription = 
	S.StatusDescription 
	from status S join Customer_Stage C on S.Status = C.Status

/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
	Set 
	Address1 = Address2, 
	Address2 = null 
	where address1 is null
-- RDT 07/08/2009 Start

Declare @Tip_LastDateUsed table (tipnumber char(15) , DateLastUsed char(10) )
Insert into  @Tip_LastDateUsed 
	Select Tipnumber, Convert( char(10) , Max(DateLastUsed), 101 )from Input_cif
	Group by tipnumber 
	
Update Customer_Stage 
set Misc1 = DateLastUsed 
	from @Tip_LastDateUsed t join Customer_Stage C 
	on t.TipNumber  = c.TipNumber 

-- RDT 07/08/2009 End 

GO
