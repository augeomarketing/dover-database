USE [214Continental]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/13/2011 11:44:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spInputScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spInputScrub]
GO

/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 03/17/2009 15:17:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This scrubs (cleans up) the inport_Customer and Input_Transaction tables*/
/* */
/* BY:  R.Tremblay  */
/* DATE: 1/2008   */
/* REVISION: 0 */
-- Remove records from imput files that don't have a CIF record.
-- Load the input_customer table from CIF and CIF2 
-- Tips are created at the DDA level not the CIF --

-- Calls Stored procedure spLoadNames to set the primary name on Input_CIF 

-- RDT 05/01/2008 added ssn to input_cif
-- RDT 05/15/2008 REMOVED ssn FROM input_cif
-- RDT 06/15/2008 Added ssn , address1, address2 , and name FROM input_cif
-- RDT 08/01/2008 Added Input_Purge scrub
-- RDT 09/05/2008 Added CIFNum to input_customer misc4
-- RDT 11/19/2008 Add leading zero to zipcode 
-- RDT 06/09/2009 Added Update of Affiliat_stage.CustId from Input_cif joined on PAN
-- RDT 06/13/2011 Set Default Account Type to "D" in Input_CIF 

/******************************************************************************/	

CREATE  PROCEDURE [dbo].[spInputScrub] @EndDate DateTime AS

/* clear error tables */
Truncate Table  Input_Customer
Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error
Truncate TABLE [Input_BillsPaid_Error] 
Truncate  TABLE [Input_CheckCard_Error] 
Truncate  TABLE [Input_CIF_Error] 
Truncate  TABLE [Input_CIF2_Error] 
Truncate  TABLE [Input_DirectDeposit_Error] 
Truncate  TABLE [Input_Estatement_Error] 
Truncate  TABLE [Input_LineOfCredit_Error] 
Truncate  TABLE [Input_Loans_Error] 
Truncate  TABLE [Input_SavingMMKt_Error] 
Truncate  TABLE [Input_HotCard_Error] 
Truncate  TABLE [Input_NewCards_Error] 

delete from Input_NewCards
where acctnum in (select acctid from input_Customer_Closed_Cards)

------------------------------------ Scrub Input_CIF ----------------------------------
-- Load Input_Cif with lastname, city, state and zip from input_CIF2
Update Input_CIF 
Set 	Lastname = CIF2.Lastname,
	City 	= CIF2.City, 
	State 	= CIF2.State,
	ZipCode = CIF2.ZipCode
-- RDT 05/15/2008 REMOVED ssn FROM input_cif	ssn	= CIF2.SSN 	--RDT 05/01/2008	
-- RDT 06/15/2008 added ssn
--	,ssn	= CIF2.SSN
	, CardName = CIF2.FullName
	, Address1 = CIF2.Address1
	, Address2 = CIF2.Address2
From input_CIF cif Join Input_CIF2 CIF2 on CIF.CIFNum = CIF2.CIFNum

-- RDT 06/13/2011 Start
Update Input_CIF 
	set Accttype = 'D' 
	where Accttype is null 
-- RDT 06/13/2011 End 

-- Load CIF's into Input Customer 
-- RDT 06/15/2008 Insert into Input_Customer 
-- RDT 06/15/2008 	(misc2, DateAdded )
-- RDT 06/15/2008 	Select distinct AccountNum , @EndDate from Input_CIF
--RDT 05/01/2008 added ssn to input_customer
-- RDT 06/15/2008 removed  ssn from input_customer - ssn is assigned in spLoadNames
Insert into Input_Customer 
	(misc2, DateAdded )
	Select distinct AccountNum, @EndDate from Input_CIF -- each dda gets a tip NOT the CIF

-- Load the input_customer table from Input_CIF2 ONLY If the CIFNum exists in Input_CIF
-- Remove any CIF2 

Insert Into [Input_CIF2_Error]  
	Select *, 'CIFNum not in CIF Data LIst'  from Input_CIF2 
		where CIFNum not in ( Select CIFNum from Input_CIF ) 
Delete from [Input_CIF2]  
		where CIFNum not in ( Select CIFNum from Input_CIF ) 

-- Input_cif2 contains all CIF numbers found in Input_cif 
-- Input_cif does NOT contains all CIF numbers found in Input_cif2 
-- THIS DOESN'T WORK BECAUSE I NEED ACCOUNT AND PAN NUMBERS TO JOIN TO OTHER TABLES. 
-- CIF MUST BE THE PRIMARY TABLE NOT CIF2

-- Call Stored procedure to set the primary name on Input_CIF
exec spLoadNames

-- Update input_customer table from CIF
Update Input_Customer 
	set 	Acctname1 = CIF.CardName, 
		LastName 	= CIF.LastName,
		HomePhone = CIF.HomPhone, 
		WorkPhone = CIF.BusPhone, 
		Address1 = CIF.Address1,
		Address2 = CIF.Address2,
		City	= CIF.City,
		State 	= CIF.State,
		ZipCode = CIF.ZipCode
-- RDT 05/15/2008 REMOVED ssn FROM input_cif		misc3 	= CIF.ssn
-- RDT 06/16/2008 Added ssn 
		, misc3 	= CIF.ssn
	from Input_CIF CIF, Input_Customer c where CIF.AccountNum = c.MIsc2 

-- RDT 09/05/2008 Added CIFNum to input_customer misc4
Update Input_Customer
	set Misc4 = w.CIFNum from Work_Names w join Input_Customer c on w.accountNum = c.Misc2

--RDT 11/19/2008 Add leading zero to zipcode 
Update Input_Customer set zipcode = '0'+zipcode  where len(ZipCode ) = 8


------------------------------------ Scrub Input_BillsPaid ----------------------------------
-- delete records without corresponding CIF 
Insert into Input_BillsPaid_Error
	select * , 'CIFNum not in CIF Data LIst '  from Input_BillsPaid 
		where CIFNum not in ( select distinct CIFNum from Input_CIF ) 

delete from Input_BillsPaid 
		where CIFNum not in ( select distinct CIFNum from Input_CIF ) 

---------------------------------- Scrub Input_CheckCard ----------------------------------
-- delete records without corresponding AccountNum  
Insert into [Input_CheckCard_Error] 
	select * , 'PAN Number not in CIF Data LIst ' from [Input_CheckCard] 
		where PAN not in (Select PAN  from input_cif )

delete from [Input_CheckCard] 
		where PAN not in (Select PAN  from input_cif )

------------------------------------ Scrub Input_DirectDeposit ----------------------------------
-- delete records without corresponding CIF AND Account Num 
-- 03/06/08 -- delete records without corresponding Account Num 
Insert into [Input_DirectDeposit_Error]
--RDT 02/13/2008	select * , 'CIF Number not in CIF Data LIst ' from [Input_DirectDeposit] where 	cifnum  not in (Select cifnum  from input_cif) 
-- 03/06/08 	select * , 'AcctNum / CIF not in CIF Data LIst ' from [Input_DirectDeposit]  	where 	( CIFnum  Not in (Select cifnum  from input_cif) ) and  ( Acctnum Not in (select Accountnum from input_cif) )
	select * , 'AcctNum not in CIF Data LIst ' from [Input_DirectDeposit]  	
		where 	( Acctnum Not in (select Accountnum from input_cif) )

delete from [Input_DirectDeposit] 
--RDT 02/13/2008		where 	cifnum not in (Select cifnum from input_cif) 
-- 03/06/08	where 	( CIFnum  Not in (Select cifnum  from input_cif) ) and 	( Acctnum Not in (select Accountnum from input_cif) )
		where 	( Acctnum Not in (select Accountnum from input_cif) )


------------------------------------ Scrub Input_Estatement ----------------------------------
-- delete records without corresponding CIF 
Insert into [Input_Estatement_Error]
	select * , 'CIFNum not in CIF Data LIst '  from [Input_Estatement] 
		where CIFNum not in (Select CIFNum from input_cif)

delete from [Input_Estatement] 
		where CIFNum not in (Select CIFNum from input_cif)
 

------------------------------------ Scrub Input_LineOfCredit ----------------------------------
-- delete records without corresponding CIF 
Insert into [Input_LineOfCredit_Error]
	select *, 'CIFNum not in CIF Data LIst '  from [Input_LineOfCredit] 
		where CIFNum not in (Select CIFNum from input_cif)

delete from [Input_LineOfCredit] 
		where CIFNum not in (Select CIFNum from input_cif)
 

------------------------------------ Scrub Input_Loans ----------------------------------
-- delete records without corresponding CIF 
Insert into Input_Loans_Error
	select *, 'CIFNum not in CIF Data LIst '  from Input_Loans 
		where CIFNum not in ( select CIFNum from Input_CIF ) 

delete from Input_Loans 
		where CIFNum not in ( select CIFNum from Input_CIF ) 

------------------------------------ Scrub Input_SavingMMKT ----------------------------------
-- delete records without corresponding CIF and Account
Insert into Input_SavingMMKt_Error
	select *, 'CIFNum not in CIF Data LIst '  from Input_SavingMMKt 
--		where CIFNum not in ( select CIFNum from Input_CIF ) 
	where 	( cifnum  not in (Select cifnum  from input_cif) ) 
	and 	( acctnum Not in ( select accountnum from input_cif) )


delete from Input_SavingMMKt 
--		Where CIFNum not in ( select CIFNum from Input_CIF ) 
	where 	( cifnum  not in (Select cifnum  from input_cif) ) 
	and 	( acctnum Not in ( select accountnum from input_cif) )


------------------------------------ Scrub Input_HotCards ----------------------------------
-- delete records without corresponding Account in CIF 
Insert into input_HotCard_Error
	select *, 'CIF Not in CIF Data List' from input_HotCard
	where cifnum not in (select cifnum from input_cif) 

Delete from input_HotCard
	where cifnum not in (select cifnum from input_cif) 

------------------------------------ Scrub Input_NewCards ----------------------------------
-- delete records without  
Insert into input_NewCards_Error
	select DateIssued, PAN, AcctNum, CIFNum, 'CIF Not in CIF Data List' from input_NewCards
	where cifnum not in (select cifnum from input_cif) 

Delete from input_NewCards
	where cifnum not in (select cifnum from input_cif) 


------------------------------------ Scrub input_RNI_emailbonus ----------------------------------
-- delete records that already have bonuses 
Delete from input_RNI_EmailBonus 
	where tipnumber in ( Select Tipnumber from OneTimeBonuses where TranCode = 'BE' )

Delete from input_RNI_EmailBonus where tipnumber not in (select tipnumber from Customer_Stage )

---------------------------------- Scrub Input_Purge  ----------------------------------
Delete from Input_Purge where PAN not in (select pan from affiliat) 

-----------------------

-- RDT 06/09/2009 Added Update of Affiliat_stage.CustId from Input_cif joined on PAN
Update Affiliat_stage 
	Set Custid = AccountNum
	From affiliat_stage a join input_cif c on a.acctid = c.PAN


GO
