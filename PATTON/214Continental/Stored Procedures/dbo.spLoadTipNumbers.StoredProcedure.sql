/****** Object:  StoredProcedure [dbo].[spLoadTipNumbers]    Script Date: 03/17/2009 15:17:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Stored Procedure dbo.spLoadTipNumbers    Script Date: 12/4/2008 2:53:52 PM ******/
/*****************************************/
/*Author: Rich Tremblay  */
/* Date1/2008  */
/* Revised: */
/* 07/30/2008 - added code to account for combines */
-- 
-- RDT 08/25/2008 - add tips to input_purge 
/*
RDT 12/05/2008 - changes code to account for 
	combines by looking at the affiliat instead of the TipTracking 
	(Must have been smoking crack that day. Thanks Sarah)
	The affiliat_Stage.custid MUST have equal Input_Customer.Misc2
-- RDT 2009/01/05 change udpate to use spPutlastTipNumberUsed
*/

/*****************************************/

CREATE  PROCEDURE [dbo].[spLoadTipNumbers] @TipPrefix char(3) AS
-- Declare @TipPrefix char(3) 
-- set @TipPrefix = '214'

Declare @NewTip bigint

/*
-- RDT 7/30/08 Update the New_TipTracking214 table with new records from OnlineHistoryWork.dbo.New_TipTracking 
INSERT into New_TipTracking214 
	Select * From OnlineHistoryWork.dbo.New_TipTracking 
		Where left(NewTip,3) =  @TipPrefix
		and NewTip not in (Select newtip From New_TipTracking214 )

--  RDT 7/30/08 update the input Tip with the deleted tips of combined accounts
UPDATE Input_Customer 
	Set  TipNumber = D.TipNumber 
	From  customerdeleted D join Input_Customer I on D.Misc2 = I.Misc2
	Where D.statusdescription like '%combined to%'

-- RDT 07/30/08 Update the old Tip with the new tips 
UPDATE Input_customer 
	Set Tipnumber = NewTip
	From New_TipTracking214 join Input_Customer C on OldTIp = c.Tipnumber 

*/
-- RDT 12/05/2008 
Update Input_customer 
	Set  Tipnumber = A.Tipnumber
	From Affiliat_stage a join Input_Customer i on A.Custid = i.Misc2

/* Update Tipnumber where Misc2  =  in Customer_Stage  */
UPDATE Input_Customer
	Set  TipNumber = Customer_Stage.TipNumber 
	From Customer_Stage, Input_Customer  
		Where Customer_Stage.Misc2 = Input_Customer.Misc2
		and Input_Customer.TipNumber  is NULL

exec rewardsnow.dbo.spGetLastTipNumberUsed @TipPrefix, @NewTip output
select @NewTip as LastTipUsed


If @NewTip is NULL  Set @NewTip = @TipPrefix+'000000000000'


/* Assign Get New Tips to new customers */
UPDATE  Input_Customer
	Set  @NewTip = ( @NewTip + 1 ),
	      TIPNUMBER =  @NewTip 
	Where TipNumber is null 

-- Update LastTip -- 
-- UPDATE Client Set LastTipNumberUsed = @NewTip
exec rewardsnow.dbo.spPutLastTipNumberUsed @TipPrefix, @NewTip 


-- Update the Input_CIF table with the TIPS from Input_customer
UPDATE Input_CIF 
	Set Tipnumber = c.Tipnumber
	From  Input_Customer  C, Input_CIF I
	Where C.Misc2 = I.AccountNum 
	and I.TipNumber  is NULL

-- Update Input_Transaction from CIF on AcctNum
UPDATE Input_Transaction 
	Set Tipnumber = cif.TipNumber
	From Input_CIF cif , Input_Transaction trn  
	where cif.AccountNum = trn.AcctNum and trn.Tipnumber is null 

-- Update Input_Transaction from CIF on PAN
UPDATE Input_Transaction 
	Set Tipnumber = cif.TipNumber
	From Input_CIF cif , Input_Transaction trn  
	where cif.PAN = trn.PAN	and trn.Tipnumber is null 

-- Update Input_Transaction from CIF on CIFNUM
UPDATE Input_Transaction 
	Set Tipnumber = cif.TipNumber
	From Input_CIF cif , Input_Transaction trn  
	where cif.CIFNum = trn.CIFNum

/********** OneTimeBonuses_stage  *********/

-- Update OneTimeBonuses_stage  Tipnumber from input_Transction on AcctNum 
UPDATE OneTimeBonuses_stage 
	Set Tipnumber = Trn.TipNumber
	From Input_Transaction Trn , OneTimeBonuses_stage one  
	where Trn.AcctNum = One.AcctId 	and One.Tipnumber is null 

-- Update OneTimeBonuses_stage  Tipnumber from input_Transction on PAN 
UPDATE OneTimeBonuses_stage 
	Set Tipnumber = Trn.TipNumber
	From Input_Transaction Trn , OneTimeBonuses_stage one  
	where Trn.PAN = One.AcctId and One.Tipnumber is null 
-- Update OneTimeBonuses_stage  Tipnumber from input_Transction on CIF 
UPDATE OneTimeBonuses_stage 
	Set Tipnumber = Trn.TipNumber
	From Input_Transaction Trn , OneTimeBonuses_stage one  
	where Trn.CIFNum = One.AcctId and One.Tipnumber is null 
--

-- RDT 08/25/2008 Added code to update tips on Input_Purge
Update Input_Purge 
	set Tipnumber = a.Tipnumber from Affiliat_Stage a join Input_Purge P on a.Acctid = p.PAN

Update Input_Purge_Hold 
	set Tipnumber = a.Tipnumber from Affiliat_Stage a join Input_Purge_Hold P on a.Acctid = p.PAN
GO
