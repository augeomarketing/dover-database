/****** Object:  StoredProcedure [dbo].[spNewCardBonus]    Script Date: 03/17/2009 15:17:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/* BY:  R.Tremblay  */
/* 
  Hot Card.csv "hot"
  New Cards in Jan.csv "new"

Read the "new" file and assign record numbers based on the order of the records in the input file.
Match the "new" CIF number of the "hot" CIF number.
If the HotCardDate column in the "hot" file matches the DateIssued column in the "new" file, 
the PAN in the "new" file is considered a replacement. The bonus for a New account is not to be awarded for that PAN. 

If the new card is not a replacement use the account number to link the PAN to the RewardsNow number. 
-- I had to also link to the CIFNumber.
If there are multiple account numbers use the account number from the first record. 

read input_CIF and load the work_names with account (dda) and all names associated with input_Cif account
RDT 10/01/2008 - Changed code to track awards by CIF not PAN
			   - Added all existing CIF to OneTimeBonus
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spNewCardBonus] @EndDate char(10) AS

Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)
Declare @CIFNum 	char(7)

Declare @TempNew Table ( 
	DateIssued datetime ,
	PAN   char (16) ,
	AcctNum char (10) ,
	CIFNum char (7) ,
	RecordNum int ,
	NewFlag char (1) )

-- Set the Input_NewCards.NewFlag = input_HotCard.CardStatus 
-- Since I don't know what the flag might be, should be "H", if NewFlag is Null it's a new card.
Update Input_NewCards
	set NewFlag = H.CardStatus 
		from Input_NewCards N, input_hotcard H
		where N.CIFNum = H.CIFNum and 
			H.HotCardDate = N.DateIssued

Declare csr_New Cursor For 
	Select Distinct CIFNum from Input_NewCards  Order by CIFNum 

Open csr_New 
Fetch csr_NEW into  @CIFNum

While @@Fetch_Status = 0 
Begin
	Insert into @TempNew 
		( DateIssued, PAN , AcctNum , CIFNum , RecordNum , NewFlag )
		Select top 1 * from Input_NewCards where CIFNum =  @CIFNum Order by RecordNum

	Fetch Next from csr_New Into @CIFNum

End

Close csr_New
Deallocate csr_New

Truncate table Input_NewCards
Set  IDENTITY_INSERT Input_NewCards ON
Insert into Input_NewCards 
	( DateIssued, PAN , AcctNum , CIFNum , RecordNum , NewFlag )
	select 	DateIssued, PAN , AcctNum , CIFNum , RecordNum , NewFlag  from @TempNew

Insert Into Input_Transaction 
	( CIFNum, AcctNum, PAN, Trancode, PurchAmt, TranDate) 
	Select  CIFNum,AcctNum, PAN, 'BN', 1, @EndDate 
		from Input_NewCards 
		where NewFlag is Null
		and CIFNum not in ( Select Acctid From OneTimeBonuses_stage where Trancode = 'BN' )

Insert Into OneTimeBonuses_stage 
	(Trancode, Acctid, DateAwarded)
-- RDT 10/01/2008 -- Select Trancode, AcctNum, Trandate 
	Select Trancode, CIFNum, Trandate 
		from Input_Transaction where Trancode = 'BN' 
-- RDT 10/01/2008 -- and AcctNum Not in ( Select Acctid from OneTimeBonuses_stage where Trancode = 'BN' )
		and CIFNum Not in ( Select Acctid from OneTimeBonuses_stage where Trancode = 'BN' )
GO
