/****** Object:  StoredProcedure [dbo].[spLoadNames]    Script Date: 03/17/2009 15:17:21 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    */
/* BY:  R.Tremblay  */
/* DATE: 2/2008   */
/* REVISION: 0 */
/* 
	read input_CIF and load the work_names with account (dda) and all names associated with input_Cif account
	
*/
/******************************************************************************/
CREATE PROCEDURE [dbo].[spLoadNames] AS

Declare @SQLDynamic nvarchar(2000)
Declare @Tipnumber 	char(15)
Declare @AccountNum	Char(10),  @CIFNum char(10),  @CardName varchar(50),  @Name2 varchar(50), @LastName varchar(50)
Declare @AccountBreak  char(10)
Declare @Count Int
Declare csr_CIF Cursor for 
	Select AccountNum, CIFNum, CardName, Name2, LastName  from Input_CIF 
		Group by AccountNum, CIFNum, CardName, Name2 , LastName
		Order by accountNum, Right(cifnum , 6)
Open csr_CIF

Fetch csr_CIF into 
	@AccountNum, @CIFNum,  @CardName, @Name2 , @LastName 

Truncate Table Work_Names

While @@Fetch_Status = 0 
Begin

	-- If same Account add names to fields. 
	If @AccountBreak  = @AccountNum
	Begin 
		Set @Count = @Count + 1
		/* postponed adding names to primary record until more time - Right now just get the first name and skip the others*/
	End
	Else
	Begin 
		-- Add new record to work table
		Insert into work_names 
			( AccountNum, CIFNum, LastName, Name1, Name2, ssn  )
			Values
			( @AccountNum, @CIFNum, @LastName, @CardName, @Name2, dbo.ufnRetrievessn( @AccountNum)  )
		Set @Count =  1
		Set @AccountBreak = @AccountNum

	End

	Fetch Next from csr_CIF
	into @AccountNum, @CIFNum,  @CardName, @Name2 , @LastName 

End

Close csr_CIF
Deallocate csr_CIF
---************ load names to input_CIF from work_table
Update input_cif 
set CardName = w.Name1,
	Name2 = w.Name2, 
	Lastname = w.LastName, 
	ssn 	= w.ssn
from Input_Cif c Join Work_names w on c.AccountNum = w.AccountNum
GO
