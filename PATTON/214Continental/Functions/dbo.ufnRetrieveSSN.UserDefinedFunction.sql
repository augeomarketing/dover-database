/****** Object:  UserDefinedFunction [dbo].[ufnRetrieveSSN]    Script Date: 03/02/2009 10:35:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ufnRetrieveSSN]') AND xtype in (N'FN', N'IF', N'TF'))
BEGIN
execute dbo.sp_executesql @statement = N'create function [dbo].[ufnRetrieveSSN] (@AccountNum char(10))
returns char(9)

as

BEGIN

declare @ssn	char(9)

set @ssn = (select top 1 cif2.ssn
	    from dbo.input_cif2 cif2 join dbo.input_cif cif
	    on cif2.cifnum = cif.cifnum
	    where accountnum = @accountnum
	    order by cif2.CIFNum)

return @ssn

END
' 
END
GO
