/****** Object:  UserDefinedFunction [dbo].[ufGetActiveAccounts]    Script Date: 03/02/2009 10:35:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ufGetActiveAccounts]') AND xtype in (N'FN', N'IF', N'TF'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[ufGetActiveAccounts] ( @Tipnumber char(15), @StageFlag char(1)  )  
RETURNS char(1) AS  

BEGIN 
Declare @Status char(1)
If @StageFlag <> ''P'' 
	Begin
	set  @Status = ( select Top 1 AcctStatus from affiliat_stage where tipnumber = @tipnumber order by acctstatus)
	End
Else
	Begin
	set @Status = ( select Top 1 AcctStatus from affiliat where tipnumber = @tipnumber order by acctstatus )
	End

Return @Status 

END' 
END
GO
