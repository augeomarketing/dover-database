/****** Object:  Table [dbo].[rdt_NewCards]    Script Date: 03/02/2009 10:35:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[rdt_NewCards]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[rdt_NewCards](
	[dateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[Acctnum] [char](10) NOT NULL,
	[CIFNUM] [char](7) NOT NULL,
	[RecordNum] [int] IDENTITY(1,1) NOT NULL,
	[Newflag] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
