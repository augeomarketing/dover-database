USE [214Continental]
GO
/****** Object:  Table [dbo].[Purge_Customer_Input]    Script Date: 11/10/2009 09:45:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Purge_Customer_Input](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [nchar](16) NOT NULL,
	[CardStatus] [char](1) NULL,
	[HotCardDate] [datetime] NULL,
	[CIFNum] [nchar](8) NULL,
	[Tipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
