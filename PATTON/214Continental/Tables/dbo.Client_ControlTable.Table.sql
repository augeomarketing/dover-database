/****** Object:  Table [dbo].[Client_ControlTable]    Script Date: 03/02/2009 10:33:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Client_ControlTable]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Client_ControlTable](
	[Input_Table] [char](20) NULL,
	[RecordsIn] [int] NULL,
	[RecordsOut] [int] NULL,
	[Trandate] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
