/****** Object:  Table [dbo].[Input_NewCards]    Script Date: 03/02/2009 10:34:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_NewCards]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_NewCards](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[AcctNum] [char](10) NOT NULL,
	[CIFNum] [char](7) NOT NULL,
	[RecordNum] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[NewFlag] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
