/****** Object:  Table [dbo].[Input_Estatement]    Script Date: 03/02/2009 10:34:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_Estatement]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_Estatement](
	[ESType] [char](2) NULL,
	[OpenDate] [datetime] NULL,
	[AcctNum] [char](16) NULL,
	[Name] [char](40) NULL,
	[CIFNum] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
