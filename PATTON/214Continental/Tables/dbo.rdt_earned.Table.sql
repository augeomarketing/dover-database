/****** Object:  Table [dbo].[rdt_earned]    Script Date: 03/02/2009 10:35:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[rdt_earned]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[rdt_earned](
	[tipnumber] [varchar](15) NOT NULL,
	[earned] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
