/****** Object:  Table [dbo].[New_TipTracking214]    Script Date: 03/02/2009 10:34:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[New_TipTracking214]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[New_TipTracking214](
	[NewTIP] [char](15) NOT NULL,
	[OldTip] [char](15) NOT NULL,
	[TranDate] [datetime] NOT NULL,
	[OldTipPoints] [int] NULL,
	[OldTipRedeemed] [int] NULL,
	[OldTipRank] [char](1) NULL,
	[CopyFlagTransfered] [datetime] NULL,
	[Transid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
