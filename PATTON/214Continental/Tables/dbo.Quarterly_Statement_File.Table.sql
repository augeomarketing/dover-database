USE [214Continental]
GO
SET ANSI_NULLS ON
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 05/12/2011 15:19:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO

USE [214Continental]
GO

/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 05/12/2011 15:19:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[acctid] [char](16) NULL,
	[PointsExpire] [decimal](18, 0) NULL,
	[FullSpectrum] [decimal](18, 0) NULL,
	[FSLineCredit] [decimal](18, 0) NULL,
	[FSSavingBal] [decimal](18, 0) NULL,
	[FSDirectDep] [decimal](18, 0) NULL,
	[FSOnlineBill] [decimal](18, 0) NULL,
	[FSLoan] [decimal](18, 0) NULL,
	[BonusBN] [decimal](18, 0) NULL,
	[BonusBA] [decimal](18, 0) NULL,
	[BonusBI] [decimal](18, 0) NULL,
	[BonusBE] [decimal](18, 0) NULL,
	[ssn] [char](9) NULL,
	[PointBonusMN] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING On
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__13F1F5EB]  DEFAULT ((0)) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__14E61A24]  DEFAULT ((0)) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__15DA3E5D]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__16CE6296]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__17C286CF]  DEFAULT ((0)) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__18B6AB08]  DEFAULT ((0)) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__19AACF41]  DEFAULT ((0)) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1A9EF37A]  DEFAULT ((0)) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1B9317B3]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1C873BEC]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1D7B6025]  DEFAULT ((0)) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1E6F845E]  DEFAULT ((0)) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__1F63A897]  DEFAULT ((0)) FOR [PointsExpire]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__FullS__2057CCD0]  DEFAULT ((0)) FOR [FullSpectrum]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__FSLin__214BF109]  DEFAULT ((0)) FOR [FSLineCredit]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__FSSav__22401542]  DEFAULT ((0)) FOR [FSSavingBal]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__FSDir__2334397B]  DEFAULT ((0)) FOR [FSDirectDep]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__FSOnl__24285DB4]  DEFAULT ((0)) FOR [FSOnlineBill]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_FSLoan]  DEFAULT ((0)) FOR [FSLoan]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Bonus__251C81ED]  DEFAULT ((0)) FOR [BonusBN]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Bonus__2610A626]  DEFAULT ((0)) FOR [BonusBA]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Bonus__2704CA5F]  DEFAULT ((0)) FOR [BonusBI]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Bonus__27F8EE98]  DEFAULT ((0)) FOR [BonusBE]
GO

ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT ((0)) FOR [PointBonusMN]
GO


