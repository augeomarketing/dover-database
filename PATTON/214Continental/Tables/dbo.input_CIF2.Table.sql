/****** Object:  Table [dbo].[input_CIF2]    Script Date: 03/02/2009 10:33:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[input_CIF2]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[input_CIF2](
	[CIFNum] [char](10) NOT NULL,
	[FullName] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [char](5) NULL,
	[LastName] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [char](2) NULL,
	[ZipCode] [char](10) NULL,
	[ssn] [char](9) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
