/****** Object:  Table [dbo].[AccountDeleteInput]    Script Date: 03/02/2009 10:32:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[AccountDeleteInput]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[AccountDeleteInput](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
