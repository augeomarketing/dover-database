/****** Object:  Table [dbo].[RDT_Input_NewCards]    Script Date: 03/02/2009 10:35:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RDT_Input_NewCards]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[RDT_Input_NewCards](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[AcctNum] [char](10) NOT NULL,
	[CIFNum] [char](7) NOT NULL,
	[RecordNum] [int] IDENTITY(1,1) NOT NULL,
	[NewFlag] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
