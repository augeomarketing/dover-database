/****** Object:  Table [dbo].[input_purge_Pending]    Script Date: 03/02/2009 10:34:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[input_purge_Pending]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[input_purge_Pending](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [nchar](16) NOT NULL,
	[CardStatus] [char](1) NULL,
	[HotCardDate] [datetime] NULL,
	[CIFNum] [nchar](8) NULL,
	[Tipnumber] [nchar](15) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
