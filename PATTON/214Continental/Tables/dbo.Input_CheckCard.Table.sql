/****** Object:  Table [dbo].[Input_CheckCard]    Script Date: 03/02/2009 10:33:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_CheckCard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin 
	Drop TABLE [dbo].[Input_CheckCard]
End 
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_CheckCard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_CheckCard](
	[AcctNum] [char](16) NULL,
	[TranAmount] [money] NULL,
	[TranCode] [int] NULL,
	[TranDate] [datetime] NULL,
	[ATMDesc] [char](30) NULL,
	[PAN] [char](16) NOT NULL,
	[ShortName] [char](13) NULL, 
	[DoublePointsFlag] [char](1)
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
