/****** Object:  Table [dbo].[Input_HotCard_Error]    Script Date: 03/02/2009 10:34:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_HotCard_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_HotCard_Error](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[CardStatus] [char](1) NOT NULL,
	[HotCardDate] [datetime] NOT NULL,
	[CIFNum] [char](7) NOT NULL,
	[ErrorDesc] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
