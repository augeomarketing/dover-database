/****** Object:  Table [dbo].[Input_NewCards_Error]    Script Date: 03/02/2009 10:34:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_NewCards_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_NewCards_Error](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[AcctNum] [char](10) NOT NULL,
	[CIFNum] [char](7) NOT NULL,
	[ErrorDesc] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
