/****** Object:  Table [dbo].[Input_Transaction_Error]    Script Date: 03/02/2009 10:34:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_Transaction_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_Transaction_Error](
	[MemberNum] [char](15) NOT NULL,
	[AccountNum] [char](16) NOT NULL,
	[OldAccountNum] [char](16) NULL,
	[TranCode] [char](6) NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[trandate] [char](10) NULL,
	[NewCard] [char](1) NULL,
	[TipNUmber] [char](15) NULL,
	[Points] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
