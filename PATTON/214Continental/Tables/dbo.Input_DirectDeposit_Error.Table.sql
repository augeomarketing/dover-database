/****** Object:  Table [dbo].[Input_DirectDeposit_Error]    Script Date: 03/02/2009 10:34:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_DirectDeposit_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_DirectDeposit_Error](
	[ShortName] [char](30) NULL,
	[AcctNum] [char](16) NULL,
	[TranCode] [int] NULL,
	[TranDate] [datetime] NULL,
	[CIFNum] [char](10) NULL,
	[ErrorDesc] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
