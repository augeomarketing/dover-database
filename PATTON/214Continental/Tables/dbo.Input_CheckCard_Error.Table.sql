/****** Object:  Table [dbo].[Input_CheckCard_Error]    Script Date: 03/02/2009 10:33:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_CheckCard_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
Begin 
	Drop TABLE [dbo].[Input_CheckCard_Error]
End 
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_CheckCard_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_CheckCard_Error](
	[AcctNum] [char](16) NULL,
	[TranAmount] [money] NULL,
	[TranCode] [int] NULL,
	[TranDate] [datetime] NULL,
	[ATMDesc] [char](30) NULL,
	[PAN] [char](16) NULL,
	[ShortName] [char](13) NULL,
	[DoublePointsFlag] [char](1),
	[ErrorDesc] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
