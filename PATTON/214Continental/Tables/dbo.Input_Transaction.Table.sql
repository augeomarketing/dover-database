/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 03/02/2009 10:34:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_Transaction]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_Transaction](
	[TipNumber] [char](15) NULL,
	[CIFNUM] [char](10) NULL,
	[Acctnum] [char](20) NULL,
	[PAN] [char](16) NULL,
	[TranCode] [char](2) NULL,
	[PurchAmt] [money] NULL,
	[PurchCnt] [int] NULL,
	[trandate] [char](10) NULL,
	[Points] [decimal](18, 0) NULL CONSTRAINT [DF_Input_Transaction_Points]  DEFAULT (0)
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
