/****** Object:  Table [dbo].[Input_HotCard]    Script Date: 03/02/2009 10:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_HotCard]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_HotCard](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[CardStatus] [char](1) NOT NULL,
	[HotCardDate] [datetime] NOT NULL,
	[CIFNum] [char](7) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
