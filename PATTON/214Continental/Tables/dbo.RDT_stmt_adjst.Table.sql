/****** Object:  Table [dbo].[RDT_stmt_adjst]    Script Date: 03/02/2009 10:35:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[RDT_stmt_adjst]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[RDT_stmt_adjst](
	[tipnumber] [varchar](15) NOT NULL,
	[adjust] [float] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
