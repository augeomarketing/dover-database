/****** Object:  Table [dbo].[Work_Names]    Script Date: 03/02/2009 10:35:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Work_Names]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Work_Names](
	[AccountNum] [char](10) NOT NULL,
	[CIFNum] [char](10) NOT NULL,
	[lastName] [varchar](50) NULL,
	[Name1] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[ssn] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
