/****** Object:  Table [dbo].[Input_DirectDeposit]    Script Date: 03/02/2009 10:34:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_DirectDeposit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_DirectDeposit](
	[ShortName] [char](30) NULL,
	[AcctNum] [char](16) NULL,
	[TranCode] [int] NULL,
	[TranDate] [datetime] NULL,
	[CIFNum] [char](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
