/****** Object:  Table [dbo].[Input_LineOfCredit]    Script Date: 03/02/2009 10:34:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_LineOfCredit]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_LineOfCredit](
	[LOCType] [int] NULL,
	[CurrBalance] [money] NULL,
	[CIFNum] [char](10) NULL,
	[AcctNum] [char](16) NULL,
	[RelationShip] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
