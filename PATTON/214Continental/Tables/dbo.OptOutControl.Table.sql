/****** Object:  Table [dbo].[OptOutControl]    Script Date: 03/02/2009 10:34:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[OptOutControl]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[OptOutControl](
	[Tipnumber] [char](15) NOT NULL,
	[AcctNumber] [char](25) NOT NULL,
	[Name] [char](40) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
