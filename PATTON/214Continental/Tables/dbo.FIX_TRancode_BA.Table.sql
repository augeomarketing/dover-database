/****** Object:  Table [dbo].[FIX_TRancode_BA]    Script Date: 03/02/2009 10:33:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FIX_TRancode_BA]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[FIX_TRancode_BA](
	[rec] [int] NOT NULL,
	[Tipnumber] [char](15) NOT NULL,
	[histdate] [datetime] NULL,
	[points] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
