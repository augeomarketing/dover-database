/****** Object:  Table [dbo].[rdt_stmt_25]    Script Date: 03/02/2009 10:35:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[rdt_stmt_25]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[rdt_stmt_25](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[acctid] [char](16) NULL,
	[PointsExpire] [decimal](18, 0) NULL,
	[FullSpectrum] [decimal](18, 0) NULL,
	[FSLineCredit] [decimal](18, 0) NULL,
	[FSSavingBal] [decimal](18, 0) NULL,
	[FSDirectDep] [decimal](18, 0) NULL,
	[FSOnlineBill] [decimal](18, 0) NULL,
	[FSLoan] [decimal](18, 0) NULL,
	[BonusBN] [decimal](18, 0) NULL,
	[BonusBA] [decimal](18, 0) NULL,
	[BonusBI] [decimal](18, 0) NULL,
	[BonusBE] [decimal](18, 0) NULL,
	[ssn] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
