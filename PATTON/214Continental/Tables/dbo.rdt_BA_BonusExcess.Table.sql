/****** Object:  Table [dbo].[rdt_BA_BonusExcess]    Script Date: 03/02/2009 10:35:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[rdt_BA_BonusExcess]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[rdt_BA_BonusExcess](
	[Tipnumber] [char](15) NULL,
	[acctid] [nchar](20) NULL,
	[ExcessBonus] [int] NULL,
	[Available] [int] NULL,
	[earned] [int] NULL,
	[redeemed] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
