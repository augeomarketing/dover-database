/****** Object:  Table [dbo].[Aux_ProcessControl]    Script Date: 03/02/2009 10:32:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Aux_ProcessControl]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Aux_ProcessControl](
	[RecordNumber] [int] IDENTITY(1,1) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[MonthBegin] [datetime] NOT NULL,
	[MonthEnd] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
