
USE [214Continental]
SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 02/10/2011 15:48:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO

USE [214Continental]
GO

/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 02/10/2011 15:48:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[acctid] [char](16) NULL,
	[PointsExpire] [decimal](18, 0) NULL,
	[FullSpectrum] [decimal](18, 0) NULL,
	[FSLineCredit] [decimal](18, 0) NULL,
	[FSSavingBal] [decimal](18, 0) NULL,
	[FSDirectDep] [decimal](18, 0) NULL,
	[FSOnlineBill] [decimal](18, 0) NULL,
	[FSLoan] [decimal](18, 0) NULL,
	[BonusBN] [decimal](18, 0) NULL,
	[BonusBA] [decimal](18, 0) NULL,
	[BonusBI] [decimal](18, 0) NULL,
	[BonusBE] [decimal](18, 0) NULL,
	[ssn] [char](9) NULL,
	[LastDateUsed] [char](10) NULL,
	[DateAdded] [char](10) NULL,
	[PointBonusMN] [int] DEFAULT (0),
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT (0) FOR [PointsPurchasedCR]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT (0) FOR [PointsBonus]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT (0) FOR [PointsReturnedCR]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsExpire]  DEFAULT (0) FOR [PointsExpire]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_FullSectrum]  DEFAULT (0) FOR [FullSpectrum]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_FSLineCredit]  DEFAULT (0) FOR [FSLineCredit]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_FSSavingBal]  DEFAULT (0) FOR [FSSavingBal]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_FSDirectDep]  DEFAULT (0) FOR [FSDirectDep]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_FSOnlineBill]  DEFAULT (0) FOR [FSOnlineBill]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_FSLoan]  DEFAULT (0) FOR [FSLoan]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_BonusBN]  DEFAULT (0) FOR [BonusBN]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_BonusBA]  DEFAULT (0) FOR [BonusBA]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_BonusBI]  DEFAULT (0) FOR [BonusBI]
GO

ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_BonusBE]  DEFAULT (0) FOR [BonusBE]
GO

