/****** Object:  Table [dbo].[Input_Purge_Hold]    Script Date: 03/02/2009 10:34:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_Purge_Hold]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_Purge_Hold](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [nchar](16) NOT NULL,
	[CardStatus] [char](1) NULL,
	[HotCardDate] [datetime] NULL,
	[CIFNum] [nchar](8) NULL,
	[Tipnumber] [nchar](15) NULL,
	[DateSentToRNI] [datetime] NOT NULL,
	[DateToPurge] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
