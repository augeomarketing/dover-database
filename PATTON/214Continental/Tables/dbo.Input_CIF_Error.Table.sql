/****** Object:  Table [dbo].[Input_CIF_Error]    Script Date: 03/02/2009 10:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_CIF_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_CIF_Error](
	[PAN] [char](16) NOT NULL,
	[CardName] [char](50) NULL,
	[BusPhone] [char](10) NULL,
	[HomPhone] [char](10) NULL,
	[AccountNum] [char](10) NULL,
	[CIFNum] [char](10) NULL,
	[Accttype] [char](1) NULL,
	[Address1] [char](50) NULL,
	[Address2] [char](50) NULL,
	[CityStateZip] [char](80) NULL,
	[DateLastUsed] [datetime] NULL,
	[Name2] [char](50) NULL,
	[ssn] [char](9) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
