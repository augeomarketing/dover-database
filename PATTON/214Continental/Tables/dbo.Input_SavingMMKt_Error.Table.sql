/****** Object:  Table [dbo].[Input_SavingMMKt_Error]    Script Date: 03/02/2009 10:34:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_SavingMMKt_Error]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_SavingMMKt_Error](
	[CurrBalance] [money] NOT NULL,
	[CIFNum] [char](10) NOT NULL,
	[ServiceCode] [char](2) NOT NULL,
	[AcctNum] [char](16) NULL,
	[Relationship] [char](1) NULL,
	[ErrorDesc] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
