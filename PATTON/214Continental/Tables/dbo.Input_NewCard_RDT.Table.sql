/****** Object:  Table [dbo].[Input_NewCard_RDT]    Script Date: 03/02/2009 10:34:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_NewCard_RDT]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_NewCard_RDT](
	[DateIssued] [datetime] NOT NULL,
	[PAN] [char](16) NOT NULL,
	[AcctNum] [char](10) NOT NULL,
	[CIFNum] [char](7) NOT NULL,
	[RecordNum] [int] NULL,
	[NewFlag] [char](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
