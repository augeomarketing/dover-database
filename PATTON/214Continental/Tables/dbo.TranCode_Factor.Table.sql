/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 03/02/2009 10:35:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TranCode_Factor]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
