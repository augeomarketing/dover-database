/****** Object:  Table [dbo].[Input_Loans]    Script Date: 03/02/2009 10:34:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_Loans]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_Loans](
	[LoanType] [int] NULL,
	[CurrBalance] [money] NULL,
	[CIFNum] [char](10) NULL,
	[AcctNum] [char](16) NULL,
	[Relationship] [char](1) NULL,
	[OriginalDate] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
