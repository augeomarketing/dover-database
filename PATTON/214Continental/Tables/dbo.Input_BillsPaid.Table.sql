/****** Object:  Table [dbo].[Input_BillsPaid]    Script Date: 03/02/2009 10:33:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[Input_BillsPaid]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [dbo].[Input_BillsPaid](
	[AcctNum] [char](20) NOT NULL,
	[Name] [varchar](50) NULL,
	[Expense] [int] NULL,
	[CIFNum] [char](10) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
