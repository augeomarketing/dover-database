SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrktip1](
	[ssn] [nchar](16) NULL,
	[pan] [nchar](16) NULL,
	[prim dda] [nchar](16) NULL
) ON [PRIMARY]
GO
