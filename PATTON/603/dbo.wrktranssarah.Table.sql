SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrktranssarah](
	[tfno] [nvarchar](15) NULL,
	[newpoints] [float] NULL,
	[oldpoints] [float] NULL,
	[pointdelta] [float] NULL
) ON [PRIMARY]
GO
