SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[spDeleteSingleTipnumber] @Tipnumber as varchar(15), @DateInput as datetime AS   

--set @DateInput = '03/24/2008'
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 




	/*  **************************************  */
	-- Mark affiliat records for deletion with "C"
	update Affiliat	
	set Acctstatus = 'C'
	where tipnumber =@Tipnumber
	


	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted
	
	 INSERT INTO AffiliatDeleted 
	  (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	   YTDEarned,CustID,DateDeleted )
	 SELECT 
	 TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 YTDEarned,CustID,@DateDeleted
	 FROM affiliat 
	 where tipnumber =@TipNumber
	

	/*  **************************************  */
	Delete from affiliat where Tipnumber=@TipNumber
	
		
	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 'C'

	INSERT INTO HistoryDeleted
	([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	select 
	[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	from history 
	where tipnumber = @TipNumber



	/*  **************************************  */
	-- Delete History
	Delete from history where tipnumber =@TipNumber
	


	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted
	INSERT INTO [CustomerDeleted]
	(TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, DateDeleted)
	select 
	TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted 
	from customer WHERE tipnumber = @TipNumber


	

	/*  **************************************  */
	-- Delete Customer
	Delete from customer where Tipnumber = @TipNumber
	/* Return 0 */
	
	
	--delete account_reference
	insert into account_reference_Deleted(Tipnumber, acctnumber, TipFirst)
	select Tipnumber, acctnumber, TipFirst from account_reference where tipnumber=@Tipnumber
	
	delete account_reference where tipnumber=@tipnumber
	
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
