USE [603TLCCommunityConsumer]
GO
/****** Object:  Table [dbo].[Monthly_Statement_Summary]    Script Date: 09/19/2012 13:56:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_Summary]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_Summary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_Summary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_Summary](
	[NumAccounts] [decimal](18, 0) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[EndDate] [datetime] NULL,
	[PointsBonusMN] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
