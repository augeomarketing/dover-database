USE [603TLCCommunityConsumer]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 05/16/2011 13:33:22 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__07C12930]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__07C12930]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__08B54D69]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__08B54D69]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__09A971A2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__09A971A2]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0A9D95DB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0A9D95DB]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0B91BA14]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0B91BA14]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0C85DE4D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0C85DE4D]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0D7A0286]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0D7A0286]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0E6E26BF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0E6E26BF]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__0F624AF8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__0F624AF8]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__10566F31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__10566F31]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__114A936A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__114A936A]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__123EB7A3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__123EB7A3]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Monthly_S__Point__1332DBDC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF__Monthly_S__Point__1332DBDC]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Monthly_Statement_File_PointsBonusMN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[stdate] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__07C12930]  DEFAULT (0),
	[PointsEnd] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__08B54D69]  DEFAULT (0),
	[PointsPurchasedCR] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__09A971A2]  DEFAULT (0),
	[PointsBonusCR] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__0A9D95DB]  DEFAULT (0),
	[PointsAdded] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__0B91BA14]  DEFAULT (0),
	[PointsPurchasedDB] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__0C85DE4D]  DEFAULT (0),
	[PointsBonusDB] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__0D7A0286]  DEFAULT (0),
	[PointsIncreased] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__0E6E26BF]  DEFAULT (0),
	[PointsRedeemed] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__0F624AF8]  DEFAULT (0),
	[PointsReturnedCR] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__10566F31]  DEFAULT (0),
	[PointsSubtracted] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__114A936A]  DEFAULT (0),
	[PointsReturnedDB] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__123EB7A3]  DEFAULT (0),
	[PointsDecreased] [numeric](18, 0) NULL CONSTRAINT [DF__Monthly_S__Point__1332DBDC]  DEFAULT (0),
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL CONSTRAINT [DF_Monthly_Statement_File_PointsBonusMN]  DEFAULT ((0))
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
