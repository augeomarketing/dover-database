SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrktransdelta](
	[tfno] [nvarchar](15) NULL,
	[pointdelta] [float] NULL
) ON [PRIMARY]
GO
