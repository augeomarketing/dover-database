SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spBonusFileProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus

insert into transumbonus (tipnumber, acctno, amtdebit)
select tipnumber, account, points	
from [adjustments]

update transumbonus
set ratio='1', trancode='BI', description='Bonus Points Increase', histdate=@datein, numdebit='1', overage='0'

insert into history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus

update customer
set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=customer.tipnumber), runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=customer.tipnumber)
where exists(select tipnumber from transumbonus where tipnumber=customer.tipnumber)
GO
