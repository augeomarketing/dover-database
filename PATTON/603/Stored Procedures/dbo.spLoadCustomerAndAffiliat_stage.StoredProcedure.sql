USE [603TLCCommunityConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerAndAffiliat_stage]    Script Date: 04/23/2013 09:24:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadCustomerAndAffiliat_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage]
GO

USE [603TLCCommunityConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spLoadCustomerAndAffiliat_stage]    Script Date: 04/23/2013 09:24:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spLoadCustomerAndAffiliat_stage]   
		@TipFirst	VARCHAR(3)
	,	@EndDate	DATETIME
	
AS

BEGIN
SET NOCOUNT ON;

/* Update all of the Customer_Stage records to a status of ''C'' .... As new records are inserted they have a status of A from the Custin table, 
As records are udated, they already have  a status of A in Custin too 
Anything remaining means that this is the first month that they HAVEN''T appeared, their status is already C and they will be added 
to the Customer_Closed table with MonthEndToDelete set to Two months beyond the EndDate passed in
*/

--Get the dateToDelete (x months out based on Client.ClosedMonths)
DECLARE	@tmpDate		DATETIME		= DATEADD(d,1,@Enddate)
	,	@NumMonths		SMALLINT		= (SELECT ClosedMonths FROM RewardsNow.dbo.DBProcessInfo WHERE DBNUmber = @TipFirst)
	,	@DateClosed		DATETIME		= @EndDate
	,	@DateToDelete	DATETIME
	,	@DBName			VARCHAR(50)		= (SELECT  RTRIM(DBNamePatton) FROM Rewardsnow.dbo.DBProcessInfo WHERE DBNumber = @TipFirst)
	,	@sqlUpdate		NVARCHAR(MAX)
	,	@sqlTruncate	NVARCHAR(MAX)
	,	@sqlInsert		NVARCHAR(MAX)
	,	@sqlDelete		NVARCHAR(MAX)
	,	@sqlDeclare		NVARCHAR(MAX)
	,	@sqlif			NVARCHAR(MAX)

SELECT	@tmpDate = DATEADD(m,@NumMonths,@tmpDate)
SELECT	@DateToDelete = DATEADD(d,-1,@tmpDate)   --usually 2 months beyond the Close date

-- SET STATUS TO C 
UPDATE	dbo.customer_stage 
SET		status = 'C'

UPDATE	dbo.Affiliat_stage 
SET		acctstatus = 'C'

-- Update/Insert CUSTOMER TABLE
UPDATE	dbo.customer_stage
SET		Status = uci.STATUS, AcctName1 = uci.NAMEACCT1, AcctName2 = uci.NAMEACCT2, AcctName3 = uci.NAMEACCT3
	,	AcctName4 = uci.NAMEACCT4, AcctName5 = uci.NAMEACCT5, AcctName6 = uci.NAMEACCT6, Address1 = uci.ADDRESS1
	,	Address2 = uci.ADDRESS2, Address4 = uci.ADDRESS4, City = rtrim(uci.CITY), State = rtrim(uci.STATE)
	,	Zipcode = rtrim(uci.ZIP), HomePhone = uci.HOMEPHONE, WorkPhone = uci.WORKPHONE, lastname = uci.LASTNAME
	,	misc1 = uci.MISC1, misc2 = uci.MISC2, misc3 = uci.MISC3
FROM	dbo.CUSTOMER_Stage cs JOIN ufn_UniqueCustInRecord() uci ON cs.TIPNUMBER = uci.TIPNUMBER
--WHERE	cs.tipnumber = uci.TIPNUMBER

INSERT INTO dbo.Customer_stage 
	(AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Status, TIPNumber, 
	 TIPFirst, TIPLast, Address1, Address2, Address4, City, State, Zipcode,  LASTNAME, 
	 HomePhone, WorkPhone, DateAdded, Misc1, Misc2, Misc3)
SELECT 
	ufn.NAMEACCT1, ufn.NAMEACCT2, ufn.NAMEACCT3, ufn.NAMEACCT4, ufn.NAMEACCT5, ufn.NAMEACCT6, ufn.STATUS, ufn.TIPNUMBER, 
	ufn.TIPFIRST, ufn.TIPLAST, ufn.ADDRESS1, ufn.ADDRESS2, ufn.ADDRESS4, ufn.CITY, ufn.STATE, ufn.ZIP, ufn.LASTNAME, 
	ufn.HOMEPHONE, ufn.WORKPHONE, ufn.DATEADDED, ufn.MISC1, ufn.MISC2, ufn.MISC3
FROM	ufn_UniqueCustInRecord() ufn
	LEFT OUTER JOIN CUSTOMER_Stage cs ON ufn.TIPNUMBER = cs.TIPNUMBER
WHERE	cs.TIPNUMBER is null 

UPDATE	CUSTOMER_Stage
SET		RunAvailable = 0
WHERE	RunAvailable is null
 
UPDATE	CUSTOMER_Stage
SET		RUNBALANCE = 0
WHERE	RUNBALANCE is null

UPDATE	CUSTOMER_Stage
SET		RunRedeemed = 0
WHERE	RunRedeemed is null
	
UPDATE	CUSTOMER_Stage
SET		Misc2 = csin.MISC2 
FROM	dbo.CUSTOMER_Stage cs JOIN CUSTIN csin ON cs.TIPNUMBER = csin.TIPNUMBER
WHERE	LEN(csin.MISC2) > 0 AND LEN(cs.Misc2) = 0 

-- Update/Insert AFFILIAT TABLE
UPDATE	dbo.AFFILIAT_stage	
SET		lastname = ci.lastname, acctstatus = ci.status, SECID = LEFT(LTRIM(ci.MISC1),10), CustID = ISNULL(RIGHT(RTRIM(ci.MISC2),13),'')
FROM	dbo.CUSTIN ci
WHERE	acctid = acct_num

INSERT INTO dbo.affiliat_stage 
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned, SECID, CustID) 
SELECT	ci.ACCT_NUM, ci.TIPNUMBER, 'Debit', cast(ci.DateAdded as datetime), 'A', 'Debit Card', ci.LASTNAME, '0', 
		left(ltrim(ci.MISC1),10), isnull(right(rtrim(ci.MISC2),13),'') 
FROM	dbo.CUSTIN ci LEFT OUTER JOIN affiliat_stage afs ON ci.ACCT_NUM = afs.ACCTID
WHERE	afs.TIPNUMBER IS NULL 
	AND	ACCT_NUM LIKE '423857%'

INSERT INTO dbo.affiliat_stage 
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned, SECID, CustID) 
SELECT	ci.ACCT_NUM, ci.TIPNUMBER, 'Credit', cast(ci.DateAdded as datetime), 'A', 'Credit Card', ci.LASTNAME, '0', 
		left(ltrim(ci.MISC1),10), isnull(right(rtrim(ci.MISC2),13),'') 
FROM	dbo.CUSTIN ci LEFT OUTER JOIN affiliat_stage afs ON ci.ACCT_NUM = afs.ACCTID
WHERE	afs.TIPNUMBER IS NULL
	AND	ACCT_NUM NOT LIKE '423857%' 

-- Insert CUSTOMER CLOSED TABLE
INSERT INTO	dbo.Customer_Closed 
SELECT	cs.Tipnumber, @DateClosed, @DateToDelete 
FROM	dbo.Customer_stage cs LEFT OUTER JOIN dbo.Customer_Closed cc ON cs.tipnumber = cc.tipnumber
WHERE	cs.Status = 'C' AND cc.tipnumber is null 

--see if any of the closed customers have transactions for > DateToDelete...if so , bump their DateToDelete value up by a month
UPDATE	dbo.Customer_Closed 
SET		DateToDelete = DATEADD(d,-1,DATEADD(m,1,DATEADD(d,1,DateToDelete))) 
WHERE	tipnumber in (select Tipnumber from dbo.History_stage where histdate >= @DateToDelete) 

-- REMOVE FROM CUSTOMER CLOSED TABLE IF TIPNUMBER HAS STATUS OF A 
DELETE FROM	dbo.Customer_Closed WHERE TipNumber in (select TipNumber from dbo.CUSTOMER_Stage where STATUS = 'A') 

--Update the statusDescription field
UPDATE	dbo.Customer_Stage 
SET		StatusDescription = rs.StatusDescription 
FROM	dbo.Customer_Stage cs join Rewardsnow.dbo.Status rs on cs.status = rs.status 

--Fix * in address2 field
UPDATE	dbo.Customer_Stage 
SET		Address2 =	(
					CASE	
						WHEN Address2 LIKE '%[0-9a-Z]%' 
						THEN Address2 
						ELSE REPLACE(Address2, '*', '') 
					END
					) 
END


GO


