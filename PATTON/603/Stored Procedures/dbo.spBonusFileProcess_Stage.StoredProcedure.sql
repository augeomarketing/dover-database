USE [603TLCCommunityConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spBonusFileProcess_Stage]    Script Date: 09/21/2012 16:07:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcess_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spBonusFileProcess_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spBonusFileProcess_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 9/2012
-- Description:	Bring bonus points in
-- =============================================
CREATE PROCEDURE [dbo].[spBonusFileProcess_Stage] 
	@DateIn varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	delete from transumbonus

	insert into transumbonus (tipnumber, acctno, amtdebit)
	select tipnumber, account, points	
	from [adjustments]

	update transumbonus
	set ratio=''1'', trancode=''BI'', description=''Bonus Points Increase'', histdate=@datein, numdebit=''1'', overage=''0''

	insert into HISTORY_Stage
	select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ''NEW'', ratio, overage
	from transumbonus

	update CUSTOMER_Stage
	set runavailable=runavailable + (select sum(amtdebit) from transumbonus where tipnumber=CUSTOMER_Stage.tipnumber), RunAvaliableNew=RunAvaliableNew + (select sum(amtdebit) from transumbonus where tipnumber=CUSTOMER_Stage.tipnumber),runbalance=runbalance + (select sum(amtdebit) from transumbonus where tipnumber=CUSTOMER_Stage.tipnumber)
	where exists(select tipnumber from transumbonus where tipnumber=CUSTOMER_Stage.tipnumber)END
' 
END
GO
