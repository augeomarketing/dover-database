USE [603TLCCommunityConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spGetandGenerateTipNumbers_Stage]    Script Date: 03/02/2017 09:56:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetandGenerateTipNumbers_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetandGenerateTipNumbers_Stage]
GO

USE [603TLCCommunityConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spGetandGenerateTipNumbers_Stage]    Script Date: 03/02/2017 09:56:08 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE PROCEDURE [dbo].[spGetandGenerateTipNumbers_Stage] @EndDate varchar(10) 
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO GET EXISTING AND GENERATE NEW TIPNUMBERS                     */
/*                                                                            */
/******************************************************************************/

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
-- Changed logic to employ last tip used instead of max tip

DECLARE	@PAN			NCHAR(16)
--	,	@SSN			NCHAR(16)
	,	@PrimDDA		NCHAR(10)
	,	@tipfirst		NCHAR(3)
	,	@newtipnumber	BIGINT
	,	@tipnumber		NCHAR(15)
	,	@UpdateTip		NCHAR(15)
	,	@process		INT
	,	@worktip		NCHAR(15)
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
DECLARE	DEMO_crsr CURSOR
FOR SELECT	TipFirst, Tipnumber, /*SSN, */Pan, LEFT([Prim DDA],10)
	FROM	Demographicin
FOR	UPDATE

OPEN DEMO_crsr
                                                                         
SET	@newtipnumber='0'
		
FETCH	DEMO_crsr INTO	@tipFirst, @Tipnumber, /*@SSN, */@PAN, @PrimDDA
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
IF	@@FETCH_STATUS = 1
	GOTO	Fetch_Error

WHILE	@@FETCH_STATUS = 0
BEGIN
SET	@worktip='0'
		
-- Check if SSN already assigned to a tip number.
--IF	@SSN is not null and left(@SSN,1) <> ' ' and exists(select tipnumber from Account_Reference_Stage where acctnumber=@SSN)
--	BEGIN
--	SET	@Worktip = (select tipnumber from Account_Reference_Stage where acctnumber = @SSN)
--	IF	@workTip <> '0' and @worktip is not null
--		BEGIN
--		IF	@Pan is not null and left(@PAN,1) <> ' ' and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @PAN)
--			BEGIN
--			INSERT INTO	Account_Reference_Stage
--			VALUES		(@Worktip, @Pan, left(@worktip,3), @EndDate)
--			END
--		IF	ISNULL(@PrimDDA, '') not in ('',' ','0000000000') and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @PrimDDA)
--			BEGIN
--			INSERT INTO	Account_Reference_Stage
--			VALUES		(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
--			END
--		END
--	END
--ELSE
-- Check if PAN already assigned to a tip number.
	IF	@PAN is not null and left(@PAN,1) <> ' ' and exists(select tipnumber from Account_Reference_Stage where acctnumber = @PAN)
		BEGIN
		SET	@Worktip = (select tipnumber from Account_Reference_Stage where acctnumber = @PAN)
		IF	@workTip <> '0' and @worktip is not null
			BEGIN
			--IF	@SSN is not null and left(@SSN,1) <> ' ' and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @SSN)
			--	BEGIN
			--	INSERT INTO	Account_Reference_Stage
			--	VALUES		(@Worktip, @SSN, left(@worktip,3), @EndDate)
			--	END
			IF ISNULL(@PrimDDA, '') not in ('',' ','0000000000') and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @PrimDDA)
				BEGIN
				INSERT INTO	Account_Reference_Stage
				VALUES		(@Worktip, @PrimDDA, left(@worktip,3), @EndDate)
				END
			END
		END
	ELSE
-- Check if Primary DDA already assigned to a tip number.
		IF	ISNULL(@PrimDDA, '') not in ('',' ','0000000000') and exists(select tipnumber from Account_Reference_Stage where acctnumber = @PrimDDA)
			BEGIN
			SET	@Worktip = (select tipnumber from Account_Reference_Stage where acctnumber = @PrimDDA)
			IF	@workTip <>'0' and @worktip is not null
				BEGIN
				--IF	@SSN is not null and left(@SSN,1) <> ' ' and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @SSN)
				--	BEGIN
				--	INSERT INTO	Account_Reference_Stage
				--	VALUES		(@Worktip, @SSN, left(@worktip,3), @EndDate)
				--	END
				IF	@PAN is not null and left(@PAN,1) <> ' ' and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @PAN)
					BEGIN
					INSERT INTO	Account_Reference_Stage
					VALUES		(@Worktip, @PAN, left(@worktip,3), @EndDate)
					END
				END
			END
IF	@workTip = '0' or @worktip is null
	BEGIN
	/*********  Start SEB002  *************************/
	DECLARE	@LastTipUsed CHAR(15)
		
	EXEC	rewardsnow.dbo.spGetLastTipNumberUsed '603', @LastTipUsed OUTPUT
	SELECT	@LastTipUsed AS LastTipUsed
	SET		@newtipnumber = cast(@LastTipUsed AS BIGINT) + 1
	/*********  End SEB002  *************************/
	SET	@UpdateTip=@newtipnumber
		
	--IF	@SSN is not null and left(@SSN,1) <> ' ' and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @SSN)
	--	BEGIN
	--	INSERT INTO	Account_Reference_Stage
	--	VALUES		(@UpdateTip, @SSN, left(@UpdateTip,3), @EndDate)
	--	END
	IF	@PAN is not null and left(@PAN,1) <> ' ' and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @PAN)
		BEGIN
		INSERT INTO	Account_Reference_Stage
		VALUES		(@UpdateTip, @PAN, left(@UpdateTip,3), @EndDate)
		END
	IF	ISNULL(@PrimDDA, '') not in ('',' ','0000000000') and not exists(select tipnumber from Account_Reference_Stage where acctnumber = @PrimDDA)
		BEGIN
		INSERT INTO	Account_Reference_Stage
		Values		(@UpdateTip, @PrimDDA, left(@UpdateTip,3), @EndDate)
		END

	EXEC	RewardsNOW.dbo.spPutLastTipNumberUsed '603', @newtipnumber  /*SEB002 */
	END
ELSE
	IF	@workTip <> '0' and @worktip is not null
		BEGIN
		SET	@updateTip = @worktip
		END	

UPDATE	demographicin	
SET		tipnumber = @UpdateTip 
WHERE CURRENT OF demo_crsr 
GOTO	Next_Record
Next_Record:
	FETCH DEMO_crsr INTO @tipFirst, @Tipnumber, /*@SSN, */@PAN, @PrimDDA
END

Fetch_Error:
	CLOSE		DEMO_crsr
	DEALLOCATE	DEMO_crsr

GO


