USE [603TLCCommunityConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spGetTipForAdjustments]    Script Date: 09/21/2012 16:07:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTipForAdjustments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTipForAdjustments]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTipForAdjustments]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		S Blanchette
-- Create date: 9/2012
-- Description:	Assign Tip Numbers to Adjustments
-- =============================================
CREATE PROCEDURE [dbo].[spGetTipForAdjustments] @Errcnt int output
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*Remove the crap data header rows that come in from the spreadsheet*/
	--delete Adjustments where isnumeric(account)=0

	/*clear table, insert into errors table any adjustments that don''t have a matching affiliat record*/
	truncate table adjustments_errors
	insert into adjustments_errors Select * from Adjustments where Account not in(select AcctID from Affiliat)
	/* delete these from the adjustments table*/
	Delete adjustments where account in (select account from adjustments_errors)

	update adjustments
	set tipnumber=b.tipnumber
	from adjustments a, affiliat b
	where account=acctid

	update adjustments
	set tipnumber=b.tipnumber
	from adjustments a, account_reference b
	where account=acctnumber and a.tipnumber is null

	delete from adjustments
	where tipnumber is null and points is null
	
	set @Errcnt = (select COUNT(*) from dbo.Adjustments_errors)
	
END

' 
END
GO
