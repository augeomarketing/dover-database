USE [603TLCCommunityConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 05/01/2017 08:22:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupTransactionDataForImport]
GO

USE [603TLCCommunityConsumer]
GO

/****** Object:  StoredProcedure [dbo].[spSetupTransactionDataForImport]    Script Date: 05/01/2017 08:22:37 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spSetupTransactionDataForImport] 
	@StartDate CHAR(10), @EndDate CHAR(10), @TipFirst CHAR(3)

AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/* BY:  S.Blanchette  */
/* DATE: 7/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* Changed logic to group by tip and pan */

/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* Change msgtype 04 to a reversal/return and omit the 500xxx series trans. */

/* BY:  S.Blanchette  */
/* DATE: 07/2011   */
/* REVISION: 3 */
/* SCAN: SEB003 */
/* Change  Point award value from  */

/* BY:  S.Blanchette  */
/* DATE: 05/2012   */
/* REVISION: 4 */
/* SCAN: SEB004 */
/* Change  Set tip to null and add date criteria  */

/* BY:  S.Blanchette  */
/* DATE: 11/2012   */
/* REVISION: 5 */
/* SCAN: SEB005 */
/* Changes to speed up process bring in only trans for this bin and current month  */

/* BY:  S.Blanchette  */
/* DATE:1/2013  */
/* REVISION: 6 */
/* SCAN: SEB006 */
/* Changes to points award per FI award being changed from  1 for $3 to 1 for $5 */

-- S Blanchete change to use RNITransaction view

TRUNCATE TABLE	transwork
TRUNCATE TABLE	transstandard


SELECT	*
INTO	#tempTrans
FROM	Rewardsnow.dbo.vw_603_TRAN_SOURCE_100 WITH (NOLOCK)
WHERE	Trandate >= CAST(@StartDate as DATE) AND Trandate <= CAST(@EndDate as DATE) /* SEB005 */

INSERT INTO	transwork
	(TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID)
SELECT	TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID
FROM	#tempTrans
WHERE	sic <> '6011' 
	AND	(
		(processingcode IN ('000000',  '001000','002000', '200020', '200040') AND (left(msgtype,2) in ('02', '04')))
		OR	PROCESSINGCODE in ('00500', '02500')
		)
		
DROP TABLE	#temptrans

UPDATE	TRANSWORK
SET		TIPNUMBER = afs.TIPNUMBER
FROM	TRANSWORK tw JOIN AFFILIAT_Stage afs ON tw.PAN = afs.ACCTID

DELETE FROM TRANSWORK WHERE TIPNUMBER is null

-- Signature Debit
UPDATE	transwork
SET		points = ROUND(((amounttran/100)/5), 10, 0)
WHERE	netid in('MCI', 'VNT') 

-- PIN Debit
UPDATE	transwork
SET		points = '0'
WHERE	netid not in('MCI', 'VNT', 'CCC')

-- Credit
UPDATE	transwork
SET		points = ROUND((amounttran/100), 10, 0)
WHERE	netid in('CCC') 

	
-- SEB001 DEBIT SPEND
INSERT INTO	TransStandard
	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' 
FROM	transwork
WHERE	processingcode IN ('000000',  '001000','002000')
	AND	LEFT(msgtype,2) IN ('02') /* SEB002 */		        	
GROUP BY	tipnumber, Pan

-- SEB001 DEBIT RETURN
INSERT INTO	TransStandard
	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' 
FROM	transwork
WHERE	processingcode in ('200020', '200040') or left(msgtype,2) in ('04') /* SEB002 */
GROUP BY	tipnumber, Pan

-- CREDIT SPEND
INSERT INTO	TransStandard
	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' 
FROM	transwork
WHERE	processingcode in ('00500')		        	
GROUP BY	tipnumber, Pan

-- CREDIT RETURN
INSERT INTO	TransStandard
	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' 
FROM	transwork
WHERE	processingcode in ('02500')
GROUP BY	tipnumber, Pan

DELETE FROM	Transstandard WHERE	TRANAMT='0'

GO
