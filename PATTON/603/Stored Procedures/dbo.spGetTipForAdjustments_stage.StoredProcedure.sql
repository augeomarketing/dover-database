USE [603TLCCommunityConsumer]
GO
/****** Object:  StoredProcedure [dbo].[spGetTipForAdjustments_stage]    Script Date: 10/31/2012 13:37:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTipForAdjustments_stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spGetTipForAdjustments_stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spGetTipForAdjustments_stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- =============================================
-- Author:		S Blanchette
-- Create date: 9/2012
-- Description:	Assign Tip Numbers to Adjustments
-- =============================================
CREATE PROCEDURE [dbo].[spGetTipForAdjustments_stage] @Errcnt int output
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*Remove the crap data header rows that come in from the spreadsheet*/
	--delete Adjustments where isnumeric(account)=0

	/*clear table, insert into errors table any adjustments that don''t have a matching affiliat record*/
	truncate table adjustments_errors
	insert into adjustments_errors Select * from Adjustments where Account not in(select AcctID from AFFILIAT_Stage)
	/* delete these from the adjustments table*/
	Delete adjustments where account in (select account from adjustments_errors)

	update adjustments
	set tipnumber=b.tipnumber
	from adjustments a, AFFILIAT_Stage b
	where account=acctid

	update adjustments
	set tipnumber=b.tipnumber
	from adjustments a, Account_Reference_Stage b
	where account=acctnumber and a.tipnumber is null

	delete from adjustments
	where tipnumber is null and points is null
	
	set @Errcnt = (select COUNT(*) from dbo.Adjustments_errors)
	
END


' 
END
GO
