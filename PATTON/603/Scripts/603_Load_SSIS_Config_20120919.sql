USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'603_M01_ImportAndAudit', N'ExcelFilePath=\\patton\ops\6CO\Output\AuditFiles\_AuditM_tmp.XLS;FormatType=Xls;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[AuditM_tmp.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'ExcelFilePath=\\patton\ops\6CO\Output\ErrorFiles\_tmpMissingDDAs.XLS;FormatType=Xls;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[tmpMissingDDA.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'S', N'\Package.Variables[User::ProductionFlag].Properties[Value]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'0', N'\Package.Variables[User::ErrCnt].Properties[Value]', N'Int32' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'603', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5ff42387-e726-4648-85b1-05161d935cbc}SSIS Config;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{8FF0A412-4CDD-4EAB-9547-D2A6184E9203}236722-SQLCLUS2\RN.RewardsNow1;Auto Translate=False;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=603TLCCommunityConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{6BC95C30-04F0-4932-A224-73EA0C7AF996}236722-SQLCLUS2\RN.603TLCCommunityConsumer;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{32C66EE0-3C4B-4A98-BE3A-848A37E7CF63}236722-SQLCLUS2\RN.COOPWork1;Auto Translate=False;', N'\Package.Connections[COOPWORK].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'ExcelFilePath=\\patton\ops\6CO\Output\AuditFiles\Summary\_tmpSummaryM.xls;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[tmpSummaryM.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'\\patton\ops\6CO\Output\AuditFiles\Summary\_tmpSummaryM.xls', N'\Package.Connections[_tmpSummaryM.xls].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'\\patton\ops\6CO\Output\ErrorFiles\_tmpMissingDDAs.XLS', N'\Package.Connections[_tmpMissingDDAs.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M01_ImportAndAudit', N'\\patton\ops\6CO\Output\AuditFiles\_AuditM_tmp.XLS', N'\Package.Connections[_AuditM_tmp.XLS].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M02_PostStageToProduction', N'603', N'\Package.Variables[User::DBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'603_M02_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5ff42387-e726-4648-85b1-05161d935cbc}SSIS Config;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M02_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{8FF0A412-4CDD-4EAB-9547-D2A6184E9203}236722-SQLCLUS2\RN.RewardsNow1;Auto Translate=False;', N'\Package.Connections[REWARDSNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M02_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=603TLCCommunityConsumer;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{6BC95C30-04F0-4932-A224-73EA0C7AF996}236722-SQLCLUS2\RN.603TLCCommunityConsumer;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'603_M02_PostStageToProduction', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=COOPWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{32C66EE0-3C4B-4A98-BE3A-848A37E7CF63}236722-SQLCLUS2\RN.COOPWork1;Auto Translate=False;', N'\Package.Connections[COOPWORK].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

