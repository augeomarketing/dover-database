SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ResearchBonus](
	[TipNumber] [char](20) NOT NULL,
	[username] [varchar](25) NULL,
	[Password] [varchar](250) NULL,
	[SecretQ] [varchar](50) NULL,
	[SecretA] [varchar](50) NULL,
	[EmailStatement] [char](1) NULL,
	[Email] [varchar](50) NULL,
	[Email2] [varchar](50) NULL,
	[EMailOther] [char](1) NULL,
	[RegDate] [datetime] NULL,
	[Nag] [char](1) NULL,
	[RecNum] [bigint] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
