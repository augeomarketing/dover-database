SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Fixbadtips](
	[tipnumber] [char](15) NULL,
	[pan] [char](16) NULL,
	[dda] [char](25) NULL,
	[ssn] [char](9) NULL,
	[name] [char](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
