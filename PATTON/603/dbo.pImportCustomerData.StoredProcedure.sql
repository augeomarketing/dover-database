SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pImportCustomerData] @TipFirst char(3)
AS 
declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20)
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT CUSTOMER DATA                                         */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
/******************************************************************************/
/* Revision:  1                                                                                 */
/* By: S. Blanchette                                                                        */
/* Date: 8/3/2007                                                                          */
/* SCAN: SEB001                                                                          */
/* Description:  On update to customer remove MISC 1  field        */
/******************************************************************************/

declare CUSTIN_crsr cursor
for select ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3
from CUSTIN
order by tipnumber, status desc
/*                                                                            */
open CUSTIN_crsr
fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	if (not exists (select tipnumber from customer where tipnumber=@tipnumber) and @tipnumber is not null and left(@tipnumber,1)<>' ')
		begin
			-- Add to CUSTOMER TABLE
			INSERT INTO Customer (TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, Status, HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3)
			values(@tipnumber, '0', '0', '0', LEFT(@tipnumber,3), right(@tipnumber,12), @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @ADDRESS1, @ADDRESS2, @ADDRESS4, rtrim(@City), rtrim(@state), rtrim(@zip), @STATUS, @HomePhone, @WorkPhone, cast(@DateAdded as datetime), @LastName, @Misc1, @Misc2, @Misc3) 
		end
	else
		begin
			-- Update CUSTOMER TABLE
			update customer
			set AcctName1=@NAMEACCT1, AcctName2=@NAMEACCT2, AcctName3=@NAMEACCT3, AcctName4=@NAMEACCT4, AcctName5=@NAMEACCT5, AcctName6=@NAMEACCT6, Address1=@Address1, Address2=@Address2, Address4=@Address4, City=rtrim(@City), State=rtrim(@STATE), Zipcode=rtrim(@zip), HomePhone=@HomePhone, WorkPhone=@WorkPhone, lastname=@lastname, misc2=@misc2, misc3=@misc3
			where tipnumber=@tipnumber 
		end
	if (not exists (select acctid from affiliat where acctid= @ACCT_NUM) and @ACCT_NUM is not null and left(@ACCT_NUM,1)<>' ')
		begin
			-- Add to AFFILIAT TABLE
			INSERT INTO affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
			values(@ACCT_NUM, @tipnumber, 'Debit', cast(@DateAdded as datetime), 'A', 'Debit Card', @lastname, '0') 
		end
	else
		begin
			-- Update AFFILIAT TABLE
			update AFFILIAT	
			set lastname=@lastname, acctstatus=@status 
			where acctid=@acct_num
		End
	
goto Next_Record
Next_Record:
		fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
end
Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr
update customer
set StatusDescription=(select statusdescription from status where status=customer.status)
GO
