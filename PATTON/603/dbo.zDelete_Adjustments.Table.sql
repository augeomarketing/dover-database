SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zDelete_Adjustments](
	[Tipnumber] [nchar](15) NULL,
	[Account] [nvarchar](255) NULL,
	[Name1] [nvarchar](255) NULL,
	[Name2] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[City,State,Zip] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[Add] [nvarchar](255) NULL,
	[Points] [float] NULL
) ON [PRIMARY]
GO
