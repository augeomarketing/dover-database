USE [268]
GO
/****** Object:  UserDefinedFunction [dbo].[fnTotalPointsFromHistoryForTranCode]    Script Date: 03/31/2014 09:46:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnTotalPointsFromHistoryForTranCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnTotalPointsFromHistoryForTranCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnTotalPointsFromHistoryForTranCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'
create function [dbo].[fnTotalPointsFromHistoryForTranCode] (@TipNumber nvarchar(16), @StartDate datetime, @EndDate datetime, @TranCode nvarchar(2))

returns int

AS

BEGIN
declare @points	int

	set @points = 0

	set @points = (select sum(points) 
					from dbo.History_Stage
					where tipnumber=@TipNumber
					and histdate between @StartDate and @EndDate
					and trancode=@TranCode)

	return isnull(@points, 0)

END

' 
END
GO
