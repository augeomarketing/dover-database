USE [268]
GO
/****** Object:  Table [dbo].[wrkHDRTransaction]    Script Date: 03/31/2014 09:44:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkHDRTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[wrkHDRTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkHDRTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkHDRTransaction](
	[HeaderID] [varchar](8) NULL,
	[PDFBusinessDate] [varchar](6) NULL,
	[FileCreationDate] [varchar](6) NULL,
	[FileCreationTime] [varchar](6) NULL,
	[Reserved] [varchar](54) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
