USE [268]
GO
/****** Object:  Table [dbo].[wrkTRLRTransaction]    Script Date: 03/31/2014 09:44:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkTRLRTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[wrkTRLRTransaction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkTRLRTransaction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkTRLRTransaction](
	[RecID01] [varchar](1) NULL,
	[RecordNumber01] [varchar](6) NULL,
	[TrailerFlagIdentifier] [varchar](4) NULL,
	[GrossDebitAmountATM] [varchar](11) NULL,
	[GrossDebitCountATM] [varchar](10) NULL,
	[GrossCreditAmountATM] [varchar](11) NULL,
	[GrossCreditCountATM] [varchar](10) NULL,
	[Reserved01] [varchar](11) NULL,
	[GrossFeeATM] [varchar](11) NULL,
	[Reserved02] [varchar](5) NULL,
	[RecID02] [varchar](1) NULL,
	[RecordNumber02] [varchar](6) NULL,
	[ReversalDebitAmountATM] [varchar](11) NULL,
	[ReversalDebitCountATM] [varchar](10) NULL,
	[ReversalCreditAmountATM] [varchar](11) NULL,
	[ReversalCreditCountATM] [varchar](10) NULL,
	[DetailRecordCountATM] [varchar](7) NULL,
	[Reserved03] [varchar](11) NULL,
	[ReversalFeeATM] [varchar](11) NULL,
	[Reserved04] [varchar](2) NULL,
	[RecID03] [varchar](1) NULL,
	[RecordNumber03] [varchar](6) NULL,
	[Reserved05] [varchar](4) NULL,
	[GrossDebitAmountPOS] [varchar](11) NULL,
	[GrossDebitCountPOS] [varchar](10) NULL,
	[GrossCreditAmountPOS] [varchar](11) NULL,
	[GrossCreditCountPOS] [varchar](10) NULL,
	[Reserved06] [varchar](11) NULL,
	[GrossFeePOS] [varchar](11) NULL,
	[Reserved07] [varchar](5) NULL,
	[RecID04] [varchar](1) NULL,
	[RecordNumber04] [varchar](6) NULL,
	[ReversalDebitAmountPOS] [varchar](11) NULL,
	[ReversalDebitCountPOS] [varchar](10) NULL,
	[ReversalCreditAmountPOS] [varchar](11) NULL,
	[ReversalCreditCountPOS] [varchar](10) NULL,
	[DetailRecordCountPOS] [varchar](7) NULL,
	[Reserved08] [varchar](11) NULL,
	[ReversalFeePOS] [varchar](11) NULL,
	[Reserved09] [varchar](2) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
