USE [268]
GO
/****** Object:  Table [dbo].[wrkTransactionImport]    Script Date: 03/31/2014 09:44:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkTransactionImport]') AND type in (N'U'))
DROP TABLE [dbo].[wrkTransactionImport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[wrkTransactionImport]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[wrkTransactionImport](
	[RecID01] [varchar](1) NULL,
	[RecordNumber01] [varchar](6) NULL,
	[MessageType] [varchar](4) NULL,
	[AcquirerInstitutionIdentificationCode] [varchar](8) NULL,
	[NetworkIdentifier] [varchar](6) NULL,
	[CardAcceptorTerminalID] [varchar](8) NULL,
	[RetrievalReferenceNumber] [varchar](6) NULL,
	[IssuerInstitutionIdentificationCode] [varchar](8) NULL,
	[PrimaryAccountNumber] [varchar](25) NULL,
	[MerchantType] [varchar](4) NULL,
	[CardAcceptorBusinessCode] [varchar](4) NULL,
	[RecID02] [varchar](1) NULL,
	[RecordNumber02] [varchar](6) NULL,
	[LocalTransactionDateTime] [varchar](12) NULL,
	[NetworkPostingDate] [varchar](4) NULL,
	[IssuerPostingDate] [varchar](6) NULL,
	[TransactionProcessingCodeANSI] [varchar](6) NULL,
	[DebitCreditFlag] [varchar](2) NULL,
	[ReconciliationAmount] [varchar](12) NULL,
	[ReconciliationConversionRate] [varchar](8) NULL,
	[TransactionResponseCode] [varchar](3) NULL,
	[ReversalReasonCode] [varchar](2) NULL,
	[MemberNumber] [varchar](1) NULL,
	[CaptureDate] [varchar](4) NULL,
	[OwnerFee] [varchar](4) NULL,
	[ActionCode] [varchar](3) NULL,
	[FunctionCode] [varchar](3) NULL,
	[MediaType] [varchar](1) NULL,
	[OverdispenseFlag] [varchar](1) NULL,
	[Reserved001] [varchar](1) NULL,
	[RecID03] [varchar](1) NULL,
	[RecordNumber03] [varchar](6) NULL,
	[InterchangeFees] [varchar](4) NULL,
	[CardAcceptorLocation] [varchar](40) NULL,
	[CardAcceptorIDCode] [varchar](16) NULL,
	[MessageReasonCode] [varchar](4) NULL,
	[SystemTraceAuditNumber] [varchar](6) NULL,
	[Reserved002] [varchar](3) NULL,
	[RecID04] [varchar](1) NULL,
	[RecordNumber04] [varchar](6) NULL,
	[AcquirerPostingDate] [varchar](6) NULL,
	[PointOfServiceCode] [varchar](3) NULL,
	[AcquirerProcessingFlag] [varchar](3) NULL,
	[IssuerProcessingFlag] [varchar](3) NULL,
	[TransactionCurrencyCode] [varchar](3) NULL,
	[PointOfServiceDataCode] [varchar](12) NULL,
	[AcquirerFundsTransactionAmount] [varchar](12) NULL,
	[IssuerFundsTransactionAmount] [varchar](12) NULL,
	[TransactionProcessingCode] [varchar](6) NULL,
	[IssuerFundsSurcharge] [varchar](9) NULL,
	[OriginalMessageType] [varchar](4) NULL,
	[RecID05] [varchar](1) NULL,
	[RecordNumber05] [varchar](6) NULL,
	[ReconciliationSurcharge] [varchar](9) NULL,
	[AcquirerFundsSurcharge] [varchar](9) NULL,
	[CardAcceptorName] [varchar](15) NULL,
	[NetworkData] [varchar](8) NULL,
	[Reserved003] [varchar](9) NULL,
	[IssuerLogo] [varchar](4) NULL,
	[AcquirerLogo] [varchar](4) NULL,
	[IssuerFundsOriginalAmount] [varchar](12) NULL,
	[Reserved004] [varchar](3) NULL,
	[RecID06] [varchar](1) NULL,
	[RecordNumber06] [varchar](6) NULL,
	[AccountIdentification01] [varchar](20) NULL,
	[AccountIdentification02] [varchar](20) NULL,
	[AcquirerFundsOriginalAmount] [varchar](12) NULL,
	[ISAFee] [varchar](7) NULL,
	[CCAFee] [varchar](7) NULL,
	[Reserved005] [varchar](7) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
