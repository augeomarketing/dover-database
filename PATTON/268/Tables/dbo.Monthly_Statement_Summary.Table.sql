USE [268]
GO
/****** Object:  Table [dbo].[Monthly_Statement_Summary]    Script Date: 03/31/2014 09:44:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_Summary]') AND type in (N'U'))
DROP TABLE [dbo].[Monthly_Statement_Summary]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Monthly_Statement_Summary]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Monthly_Statement_Summary](
	[NumAccounts] [decimal](18, 0) NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsBonusCR] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonusDB] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[EndDate] [datetime] NULL,
	[PointsBonusMER] [decimal](18, 0) NULL,
	[PurchasedPoints] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
