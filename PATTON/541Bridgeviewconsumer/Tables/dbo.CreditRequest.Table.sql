USE [541BridgeviewConsumer]
GO
/****** Object:  Table [dbo].[CreditRequest]    Script Date: 09/25/2009 11:32:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditRequest](
	[TipNumber] [nvarchar](15) NOT NULL,
	[HistDate] [smalldatetime] NOT NULL,
	[Email] [varchar](50) NULL,
	[Points] [int] NOT NULL,
	[TranDesc] [varchar](100) NULL,
	[PostFlag] [tinyint] NULL,
	[TransID] [uniqueidentifier] ROWGUIDCOL  NULL,
	[Trancode] [char](2) NULL,
	[CatalogCode] [varchar](15) NULL,
	[CatalogDesc] [varchar](150) NOT NULL,
	[CatalogQty] [int] NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[RebateType] [char](1) NULL,
	[ProductCode] [char](2) NULL,
	[AcctId] [char](16) NULL,
	[recordnumber] [numeric](18, 0) NULL,
	[SentToFI] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
