USE [541BridgeviewConsumer]
GO
/****** Object:  Table [dbo].[deleteaudit]    Script Date: 09/25/2009 11:32:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[deleteaudit](
	[Tipnumber] [nvarchar](255) NULL,
	[Acctname1] [nvarchar](255) NULL,
	[Acctname2] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[Address2] [nvarchar](255) NULL,
	[Address3] [nvarchar](255) NULL,
	[CityStateZip] [nvarchar](255) NULL,
	[stdate] [nvarchar](255) NULL,
	[PointsBegin] [float] NULL,
	[PointsEnd] [float] NULL,
	[PointsPurchasedCR] [float] NULL,
	[PointsBonusCR] [float] NULL,
	[PointsAdded] [float] NULL,
	[PointsPurchasedDB] [float] NULL,
	[PointsBonusDB] [float] NULL,
	[PointsIncreased] [float] NULL,
	[PointsRedeemed] [float] NULL,
	[PointsReturnedCR] [float] NULL,
	[PointsSubtracted] [float] NULL,
	[PointsReturnedDB] [float] NULL,
	[PointsDecreased] [float] NULL,
	[Zip] [nvarchar](255) NULL,
	[LastFour] [nvarchar](255) NULL
) ON [PRIMARY]
GO
