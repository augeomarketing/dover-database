SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditTransIn](
	[PAN] [char](16) NULL,
	[TranDate] [char](8) NULL,
	[NumPurchase] [char](4) NULL,
	[AmtPurchase] [float] NULL,
	[NumReturns] [char](4) NULL,
	[AmtReturns] [float] NULL,
	[AccountNumber] [char](20) NULL,
	[Tipnumber] [char](15) NULL,
	[PointPurchase] [numeric](18, 0) NULL,
	[PointReturn] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
