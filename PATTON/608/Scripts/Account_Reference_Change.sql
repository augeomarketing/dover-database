/*
   Thursday, October 18, 20128:56:15 AM
   User: 
   Server: doolittle\rn
   Database: 608BayFederalCommercial
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account_Reference ADD
	DateAdded date NULL
GO
ALTER TABLE dbo.Account_Reference SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
