SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGenerateFirstUseBonus] @startDate varchar(10), @EndDate varchar(10), @TipFirst char(3)
AS 
/****************************************************************************/
/*                                                                          */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
declare @DBName varchar(50), @SQLCursor nvarchar(1000), @SQLInsert nvarchar(1000), @SQLUpdate nvarchar(1000), @BonusPoints numeric(9), @Tipnumber char(15), @Trandate varchar(10), @Acctid char(16), @ProcessFlag char(1)

set @Trandate=@EndDate

set @DBName=(SELECT  rtrim(DBName) from coopwork.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
set @BonusPoints=(SELECT  [FirstUseBonus] from coopwork.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct tipnumber
from history 
where histdate >= @Startdate 
	And histdate <= @Enddate 
	and trancode in ('63', '67', '33', '37') 
/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag='N'		
		if not exists(select * from OneTimeBonuses where tipnumber=@tipnumber AND trancode='BF')
		Begin 
			set @ProcessFlag='Y' 
		End 	  
		if @ProcessFlag='Y'
		Begin
 
			Update Customer 
			set RunAvailable = RunAvailable + @BonusPoints, RunBalance=RunBalance + @BonusPoints  
			where tipnumber = @Tipnumber
			INSERT INTO history(TipNumber,HistDate,TranCode,TranCount,Points,Ratio,Description,Overage)
        		Values(@Tipnumber, @Trandate, 'BF', '1', @BonusPoints, '1', 'Bonus for First Trans Activity', '0')
 
			INSERT INTO OneTimeBonuses(TipNumber,TranCode, DateAwarded)
        		Values(@Tipnumber, 'BF', @Trandate)
		End
		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
