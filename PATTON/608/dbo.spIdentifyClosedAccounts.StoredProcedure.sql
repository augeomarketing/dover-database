SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spIdentifyClosedAccounts] 
AS 

/*  *****************************************************************************************  	*/
/* Date: 4/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Identify accounts to remove                                                    */                  								
/*												*/
/*  Tables:
		Account_Reference
		Client
		Demographicin  
		UniqueID_Tipnumber_Xref  */
/*  Revisions: 											*/
/*  *****************************************************************************************  	*/

declare @TFNO nvarchar(15), @Pan nchar(16), @ACCount nvarchar(20)  
 
/******************************************************************************/
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare Demographicin_crsr cursor
for select Tipnumber, pan, account
from demographicin
where status='C'
/*                                                                            */
open Demographicin_crsr
fetch Demographicin_crsr into @TFNO, @Pan, @ACCount
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

	if (@Tfno is null)
	Begin
		/*********************************************************************/
		/*  get tipnumber                                                    */
		/*********************************************************************/
		set @Tfno=(select tipnumber from account_reference where acctnumber=@account)
		
		if (@Tfno is null)
		Begin
			set @Tfno=(select tipnumber from account_reference where acctnumber=@Pan)
		End
	
		if (@Tfno is null)
		Begin
			goto NextRecord
		End
	End
	
	Update customer
	set status='C'
	where tipnumber=@Tfno

	Update Affiliat
	set acctstatus='C'
	where tipnumber=@Tfno

NextRecord:
	fetch Demographicin_crsr into @TFNO, @Pan, @ACCount
	
end

Fetch_Error:
close  Demographicin_crsr
deallocate  Demographicin_crsr
GO
