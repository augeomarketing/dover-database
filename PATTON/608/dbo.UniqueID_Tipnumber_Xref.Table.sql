SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniqueID_Tipnumber_Xref](
	[Tipnumber] [nchar](16) NOT NULL,
	[UniqueID] [nchar](10) NOT NULL
) ON [PRIMARY]
GO
