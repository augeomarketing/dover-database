USE [608BayFederalCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spImportStageToProd]    Script Date: 02/14/2014 11:26:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportStageToProd]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportStageToProd]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportStageToProd]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'



/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  **************************************  */
/*  Import Transactions FROM Stage from Production Tables   */
/*  **************************************  */
/*************************************************/
/* S Blanchette                                  */
/* 3/2011                                        */
/* Added code to zero out RunBalance in stage    */
/* SEB001                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 5/2012                                        */
/* Added field to update acctstatus in affiliat  */
/* SEB002                                        */
/*************************************************/
/*************************************************/
/* S Blanchette                                  */
/* 2/14                                       */
/* Not consistenly dealing with custid, misc1 in affiliat*/
/* SEB003                                        */
/*************************************************/


CREATE PROCEDURE [dbo].[spImportStageToProd] 
	@TipFirst char(3)
AS 

	declare @DBName varchar(100), @sqlUpdate nvarchar(max), @sqlTruncate nvarchar(max), @sqlInsert nvarchar(max), @sqlDelete nvarchar(max), @sqlDeclare nvarchar(max), @sqlif nvarchar(max)

/*    Get Database name                                      */
	set @dbName = ( Select DBNamePatton from RewardsNow.dbo.DBProcessInfo where DBNUmber = @TipFirst )
----------------------- Affiliat ----------------------- 
--	Update Existing accounts with YTDEarned from Affiliat_Stage
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Affiliat 
						set YTDEarned = S.YTDEarned from '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage S join '' + QuoteName(@DBName) + N''.dbo.Affiliat A on S.Tipnumber = A.Tipnumber ''
	exec sp_executesql @sqlUpdate

--	SEB002 Update Existing accounts with ACCTSTATUS from Affiliat_Stage
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Affiliat 
						set Acctstatus = S.Acctstatus from '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage S join '' + QuoteName(@DBName) + N''.dbo.Affiliat A on S.Tipnumber = A.Tipnumber and S.Acctid = A.acctid ''
	exec sp_executesql @sqlUpdate

--	SEB003 Update Existing accounts with Custid from Affiliat_Stage
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Affiliat 
						set Custid = S.Custid from '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage S join '' + QuoteName(@DBName) + N''.dbo.Affiliat A on S.Tipnumber = A.Tipnumber and S.Acctid = A.acctid ''
	exec sp_executesql @sqlUpdate

--	Insert New Affiliat accounts
	set @sqlInsert = N''Insert into '' + QuoteName(@DBName) + N''.dbo.Affiliat 
						select * from '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage where AcctID not in (select AcctID from '' + QuoteName(@DBName) + N''.dbo.Affiliat ) ''
	exec sp_executesql @sqlInsert
						
------------------------- Customer ----------------------- 
---	Update Existing Customers from Customers_Stage NOT THE TOTALS 
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer 
						Set 
						Laststmtdate = S.Laststmtdate , Nextstmtdate = S.Nextstmtdate , Status = S.Status , Dateadded =  S.Dateadded ,
						Lastname = S.Lastname , Tipfirst = S.Tipfirst , Tiplast = S.Tiplast , Acctname1 = S.Acctname1 , Acctname2 = S.Acctname2 ,
						Acctname3 = S.Acctname3 , Acctname4 = S.Acctname4 , Acctname5 = S.Acctname5 , Acctname6 =  S.Acctname6 ,
						Address1 =  S.Address1 , Address2 = S.Address2 , Address3 = S.Address3 , Address4 = S.Address4 ,
						City = S.City , State = S.State , Zipcode = S.Zipcode , Statusdescription = S.Statusdescription , Homephone = S.Homephone ,
						Workphone = S.Workphone , Businessflag = S.Businessflag , Employeeflag = S.Employeeflag , Segmentcode = S.Segmentcode ,
						Combostmt = S.Combostmt , Rewardsonline = S.Rewardsonline , Notes = S.Notes , Bonusflag = S.Bonusflag , Misc1 = S.Misc1 ,
						Misc2 = S.Misc2 , Misc3 = S.Misc3 , Misc4 = S.Misc4 , Misc5 = S.Misc5 
						From '' + QuoteName(@DBName) + N''.dbo.Customer_Stage S Join '' + QuoteName(@DBName) + N''.dbo.Customer C On S.Tipnumber = C.Tipnumber ''
	exec sp_executesql @sqlUpdate
						

-- Set the RunAvailable to ZERO in Customer_Stage before the insert. Otherwise the amounts will double 
/***************************/
/* START SEB001            */
/***************************/
	--Update dbo.Customer_Stage 	
	--	Set RunAvailable = 0 

	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer_Stage 	
						Set RunAvailable = 0, RunBalance=0 ''
	exec sp_executesql @sqlUpdate

/***************************/
/* END SEB001            */
/***************************/

--	Insert New Customers from Customers_Stage
	set @sqlInsert = N''Insert into '' + QuoteName(@DBName) + N''.dbo.Customer 
						select * from '' + QuoteName(@DBName) + N''.dbo.Customer_Stage where 
						Tipnumber not in (select Tipnumber from '' + QuoteName(@DBName) + N''.dbo.Customer ) ''
	exec sp_executesql @sqlInsert

--	Add RunBalanceNew (net) to RunBalance and RunAvailable 
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.Customer 
							set RunBalance = C.RunBalance + S.RunAvaliableNew,
								  RunAvailable = C.RunAvailable + S.RunAvaliableNew
						From '' + QuoteName(@DBName) + N''.dbo.Customer_Stage S Join '' + QuoteName(@DBName) + N''.dbo.Customer C On S.Tipnumber = C.Tipnumber ''
	exec sp_executesql @sqlUpdate
						
----------------------- HISTORY ----------------------- 
-- History  Insert New Records from History_Stage to History  .
	set @sqlInsert = N''Insert into '' + QuoteName(@DBName) + N''.dbo.History 
						select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage from '' + QuoteName(@DBName) + N''.dbo.History_Stage 
						where SecID = ''''NEW'''' ''
	exec sp_executesql @sqlInsert
						
-- Set SecID in History_Stage so it doesn''t get double posted. 
	set @sqlUpdate = N''Update '' + QuoteName(@DBName) + N''.dbo.History_Stage  set SECID = ''''Imported to Production ''''+ convert(char(20), GetDate(), 120)  where SecID = ''''NEW'''' and Tipnumber not in (select tipnumber from '' + QuoteName(@DBName) + N''.dbo.HistoryDeleted) ''
	exec sp_executesql @sqlUpdate
	
----------------------- OneTimeBonuses ----------------------- 
-- Insert  Records from OneTimeBonuses_Stage where @Tipnumber, @Trancode,  @DateAdded are not found in production table.
-- and the tipnumber isn''t in the customer deleted table
	set @sqlInsert = N''Insert into '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses 
						select * from '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses_stage 
						where not exists
							( select * from '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses 
							where '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses_stage.Tipnumber = '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses.Tipnumber and 
							'' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses_stage.Trancode = '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses.Trancode) ''
	exec sp_executesql @sqlInsert

----------------------- Account-Reference ----------------------- 
-- Account Reference Insert New Records from Account_Reference_Stage  .
	set @sqlInsert = N''Insert into '' + QuoteName(@DBName) + N''.dbo.Account_Reference (TIPNUMBER, Acctnumber, TipFirst, DateAdded)
						select TIPNUMBER, Acctnumber, TipFirst, DateAdded from '' + QuoteName(@DBName) + N''.dbo.Account_Reference_Stage 
						where Acctnumber not in (select Acctnumber from '' + QuoteName(@DBName) + N''.dbo.Account_Reference ) ''
	exec sp_executesql @sqlInsert
							
-- Truncate Stage Tables so''s we don''t double post.
	set @sqlTruncate = N''Truncate Table '' + QuoteName(@DBName) + N''.dbo.Customer_Stage ''
	exec sp_executesql @sqlTruncate
	
	set @sqlTruncate = N''Truncate Table '' + QuoteName(@DBName) + N''.dbo.Affiliat_Stage ''
	exec sp_executesql @sqlTruncate
	
	set @sqlTruncate = N''Truncate Table '' + QuoteName(@DBName) + N''.dbo.History_Stage ''
	exec sp_executesql @sqlTruncate
	
	set @sqlTruncate = N''Truncate Table '' + QuoteName(@DBName) + N''.dbo.OneTimeBonuses_stage ''
	exec sp_executesql @sqlTruncate

	set @sqlTruncate = N''Truncate Table '' + QuoteName(@DBName) + N''.dbo.Account_Reference_Stage ''
	exec sp_executesql @sqlTruncate
	
--


' 
END
GO
