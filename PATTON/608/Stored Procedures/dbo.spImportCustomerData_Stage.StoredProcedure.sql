USE [608BayFederalCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spImportCustomerData_Stage]    Script Date: 10/19/2012 10:12:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerData_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spImportCustomerData_Stage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spImportCustomerData_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spImportCustomerData_Stage] @TipFirst char(3)
AS 

declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20), @TypeCard char(1)
declare @AcctType nchar(20), @AcctTypeDesc nchar(50)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT Customer_Stage DATA                                         */
/*                                                                            */
/******************************************************************************/

/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
-- SEB 2/14/14 fix issue with not updating and inserting @MISC1 into CUSTID

declare CUSTIN_crsr cursor
for select ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, DATEADDED, MISC1, MISC2, MISC3, TypeCard
from CUSTIN
where status=''A''
order by tipnumber, status desc
/*                                                                            */
open CUSTIN_crsr
fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3, @TypeCard
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	if (not exists (select tipnumber from Customer_Stage where tipnumber=@tipnumber) and @tipnumber is not null and left(@tipnumber,1)<>'' '')
		begin
			-- Add to Customer_Stage TABLE
			INSERT INTO Customer_Stage (TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, Status, HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3, segmentcode)
			values(@tipnumber, ''0'', ''0'', ''0'', LEFT(@tipnumber,3), right(@tipnumber,12), @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @ADDRESS1, @ADDRESS2, @ADDRESS4, rtrim(@City), rtrim(@state), rtrim(@zip), @STATUS, @HomePhone, @WorkPhone, cast(@DateAdded as datetime), @LastName, @Misc1, @Misc2, @Misc3, @TypeCard) 
		end
	else
		begin
			-- Update Customer_Stage TABLE
			update Customer_Stage
			set AcctName1=@NAMEACCT1, AcctName2=@NAMEACCT2, AcctName3=@NAMEACCT3, AcctName4=@NAMEACCT4, AcctName5=@NAMEACCT5, AcctName6=@NAMEACCT6, Address1=@Address1, Address2=@Address2, Address4=@Address4, City=rtrim(@City), State=rtrim(@STATE), Zipcode=rtrim(@zip), Status=@STATUS, HomePhone=@HomePhone, WorkPhone=@WorkPhone, lastname=@lastname, misc1=@misc1, misc2=@misc2, misc3=@misc3
			where tipnumber=@tipnumber 
		end

	if (not exists (select acctid from affiliat_stage where acctid= @ACCT_NUM) and @ACCT_NUM is not null and left(@ACCT_NUM,1)<>'' '')
		begin
			if @TypeCard=''D''
			Begin
				set @AcctType=''Debit''
				set @AcctTypeDesc=(select AcctTypeDesc from Accttype where Accttype=@Accttype)
			End
			 	
			if @TypeCard=''C''
			Begin
				set @AcctType=''Credit''
				set @AcctTypeDesc=(select AcctTypeDesc from accttype where accttype=@Accttype)
			End
			
		-- Add to affiliat_stage TABLE
			INSERT INTO affiliat_stage (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, Custid, YTDEarned)
			values(@ACCT_NUM, @tipnumber, @AcctType, cast(@DateAdded as datetime), @Status, @AcctTypeDesc, @lastname, @Misc1, ''0'') 
		end
	else
		begin
			-- Update affiliat_stage TABLE
			update affiliat_stage	
			set lastname=@lastname, acctstatus=@status, custid=@MISC1 
			where acctid=@acct_num
		End
	
goto Next_Record
Next_Record:
		fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3, @TypeCard

end

Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr

update Customer_Stage
set StatusDescription=(select statusdescription from rewardsnow.dbo.status where status=Customer_Stage.status)
' 
END
GO
