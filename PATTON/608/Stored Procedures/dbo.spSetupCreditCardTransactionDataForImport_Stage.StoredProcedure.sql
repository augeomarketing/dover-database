USE [608BayFederalCommercial]
GO
/****** Object:  StoredProcedure [dbo].[spSetupCreditCardTransactionDataForImport_Stage]    Script Date: 10/19/2012 10:59:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupCreditCardTransactionDataForImport_Stage]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spSetupCreditCardTransactionDataForImport_Stage]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spSetupCreditCardTransactionDataForImport_Stage]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[spSetupCreditCardTransactionDataForImport_Stage] @dateadded char(10)
AS 

/*  *****************************************************************************************  	*/
/* Date: 4/2/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Post spend transactions to Pointsnow                                           */                  								
/*												*/
/*  Tables:
		Account_Reference
		Client
		Demographicin  
		UniqueID_Tipnumber_Xref  */
/*  Revisions: 											*/
/*  *****************************************************************************************  	*/

--truncate table transstandard

-- Get tipnumbers
update CreditTransIn
set tipnumber=b.tipnumber
from CreditTransIn a, affiliat_stage b
where a.pan=b.acctid and a.tipnumber is null

-- Calculate Points
update CreditTransIn
set pointpurchase=ROUND((AmtPurchase/100), 0), pointReturn=ROUND((AmtReturns/100), 0)

--Put to standard transtaction file format purchases.
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @dateadded, Pan, ''63'', ltrim(NumPurchase), pointpurchase, ''Credit'', ''1'', '' '' from CreditTransIn
where pointpurchase>''0'' and tipnumber is not null 		        	
	
--Put to standard transtaction file format returns.
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @dateadded, Pan, ''33'', ltrim(NumReturns), pointReturn, ''Credit'', ''-1'', '' '' from CreditTransIn
where pointreturn>''0'' and tipnumber is not null
' 
END
GO
