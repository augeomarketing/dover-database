SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetupCreditCardTransactionDataForImport] @dateadded char(10)
AS 

/*  *****************************************************************************************  	*/
/* Date: 4/2/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Post spend transactions to Pointsnow                                           */                  								
/*												*/
/*  Tables:
		Account_Reference
		Client
		Demographicin  
		UniqueID_Tipnumber_Xref  */
/*  Revisions: 											*/
/*  *****************************************************************************************  	*/

truncate table transstandard

-- Get tipnumbers
update CreditTransIn
set tipnumber=b.tipnumber
from CreditTransIn a, affiliat b
where a.pan=b.acctid and a.tipnumber is null

-- Calculate Points
update CreditTransIn
set pointpurchase=ROUND((AmtPurchase/100), 0), pointReturn=ROUND((AmtReturns/100), 0)

--Put to standard transtaction file format purchases.
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @dateadded, Pan, '63', ltrim(NumPurchase), pointpurchase, 'Credit', '1', ' ' from CreditTransIn
where pointpurchase>'0' and tipnumber is not null 		        	
	
--Put to standard transtaction file format returns.
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @dateadded, Pan, '33', ltrim(NumReturns), pointReturn, 'Credit', '-1', ' ' from CreditTransIn
where pointreturn>'0' and tipnumber is not null
GO
