USE [608BayFederalCommercial]
GO
/****** Object:  Table [dbo].[Account_Reference_Stage]    Script Date: 09/26/2012 10:42:18 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_Reference_Stage_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_Reference_Stage_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account_Reference_Stage] DROP CONSTRAINT [DF_Account_Reference_Stage_DateAdded]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[Account_Reference_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account_Reference_Stage](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateAdded] [date] NOT NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_Reference_Stage_DateAdded]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account_Reference_Stage]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_Reference_Stage_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account_Reference_Stage] ADD  CONSTRAINT [DF_Account_Reference_Stage_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
END


End
GO
