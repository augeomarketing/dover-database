USE [608BayFederalCommercial]
GO
/****** Object:  Table [dbo].[UniqueID_Tipnumber_Xref_Stage]    Script Date: 10/17/2012 16:54:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UniqueID_Tipnumber_Xref_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[UniqueID_Tipnumber_Xref_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UniqueID_Tipnumber_Xref_Stage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UniqueID_Tipnumber_Xref_Stage](
	[Tipnumber] [nchar](16) NOT NULL,
	[UniqueID] [nchar](10) NOT NULL
) ON [PRIMARY]
END
GO
