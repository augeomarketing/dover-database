SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[errors](
	[Pan] [char](16) NULL,
	[Name1] [char](40) NULL,
	[Address1] [char](40) NULL,
	[City] [char](40) NULL,
	[ST] [char](10) NULL,
	[UniqueID] [char](10) NULL,
	[Account] [char](20) NULL,
	[Error Message] [char](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
