SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidateDemographicIn]
AS

declare @Pan nchar(16), @Account nchar(20), @address1 char(40), @city char(40), @state char(2), @zip nchar(9), @Name1 char (20), @SSN nchar(9)

update Demographicin
set zip=ltrim(zip)

truncate table errors

/*  PAN ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'PAN ERROR' 
from DemographicIn
where (Pan is null or left(pan,2)='  ')

/*  SSN ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'UniqueID ERROR' 
from DemographicIn
where UniqueID is null or left(UniqueID,2)='  ' 

/*  ADDRESS 1 ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'ADDRESS 1 ERROR' 
from DemographicIn
where [address #1] is null or left([address #1],2)='  ' 

/*  CITY ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'CITY ERROR' 
from DemographicIn
where city is null or left(city,2)='  ' 

/*  STATE ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'STATE ERROR' 
from DemographicIn
where st is null or left(st,1)=' '

/*  ACCOUNT ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'ACCOUNT ERROR' 
from DemographicIn
where Account is null or left(Account,1)=' '

/*  NAME ERROR  */
insert into errors
select pan, NA1, [address #1], city, st, UniqueID, Account, 'NAME ERROR' 
from DemographicIn
where NA1 is null or left(NA1,2)='  ' 

/*  DELETE FROM INPUT FILE  */

delete from DemographicIn
--where (Pan is null or left(pan,1)=' ' and [Prim DDA] is null or left([Prim DDA],1)=' ' and [1st DDA] is null or left([1st DDA],1)=' ' and [2nd DDA] is null or left([2nd DDA],1)=' ' and [3rd DDA] is null or left([3rd DDA],1)=' ' and [4th DDA] is null or left([4th DDA],1)=' ' and [5th DDA] is null or left([5th DDA],1)=' ' and SSN is null or left(SSN,1)=' ' ) or [Inst ID] is null or left([Inst ID],1)=' ' or [address #1] is null or left([address #1],1)=' ' or city is null or left(city,1)=' ' or st is null or left(st,1)=' ' or First is null or left(First,1)=' ' or Last is null or left(Last,1)=' '
where Pan is null or left(pan,2)='  ' or UniqueID is null or left(UniqueID,2)='  ' or [address #1] is null or left([address #1],2)='  ' or city is null or left(city,2)='  ' or st is null or left(st,1)=' ' or Account is null or left(Account,2)='  ' or NA1 is null or left(NA1,2)='  '
GO
