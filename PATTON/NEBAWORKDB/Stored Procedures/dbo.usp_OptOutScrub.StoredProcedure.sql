USE NEBAWORKDB 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_OptOutScrub]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_OptOutScrub]
GO


CREATE  PROCEDURE [dbo].[usp_OptOutScrub] @TipFirst Varchar(3),  @spErrMsgr varchar(80) Output  
 AS

Declare @dbName VARCHAR(25) = (SELECT rtrim(dbnamepatton) FROM Rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst)
			, @sqlCmnd NVARCHAR(MAX)

IF ISNULL(@dbname, '') = ''
BEGIN
	set @spErrMsgr = 'Tip Not found in dbProcessInfo' 
	return -100 
END

set @SQLCmnd = REPLACE('DELETE FROM  [<DBNAME>].dbo.OneTimeBonuses_Stage' , '<DBNAME>', @dbname)
print @dbName 
set @SQLCmnd = REPLACE('
delete from inc  
from [<dbName>].dbo.Input_CUSTOMER inc 
join [<dbName>].dbo.AffiliatDeleted afd		on afd.CustID = inc.CustID 
join Rewardsnow.dbo.OPTOUTTracking opt	on opt.ACCTID = afd.CustID
where opt.TipPrefix = ' , '<dbName>', @dbName )  

set @sqlCmnd = @sqlCmnd + ''''+ @TipFirst +''''
												
Exec sp_executeSql @SQLCmnd 

GO 
