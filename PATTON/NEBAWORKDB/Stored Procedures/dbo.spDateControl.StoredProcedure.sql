USE [NEBAWORKDB]
GO
/****** Object:  StoredProcedure [dbo].[spDateControl]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDateControl]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spDateControl]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spDateControl]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spDateControl]  @start_Date char(10), @end_date char(10)
AS

insert date_control (start_date, end_date)
values (@start_date, @end_date)' 
END
GO
