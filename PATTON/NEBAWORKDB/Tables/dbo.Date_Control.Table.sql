USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[Date_Control]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Date_Control]') AND type in (N'U'))
DROP TABLE [dbo].[Date_Control]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Date_Control]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Date_Control](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Start_Date] [char](10) NULL,
	[End_Date] [char](10) NULL
) ON [PRIMARY]
END
GO
