USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[CUSTOMERWORK]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERWORK]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMERWORK]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMERWORK]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CUSTOMERWORK](
	[CardNumber] [char](25) NULL,
	[CompanyName] [varchar](40) NULL,
	[CompanyIdentifier] [varchar](10) NULL,
	[SystemBankID] [char](10) NULL,
	[PrincipleBankID] [char](10) NULL,
	[AgentBankID] [char](10) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[AcctName2] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[InternalStatusCode] [char](1) NULL,
	[ExternalStatusCode] [char](1) NULL,
	[CustID] [char](9) NULL,
	[AcctID2] [char](40) NULL,
	[LastChangeDate] [varchar](50) NULL,
	[LstName] [char](40) NULL,
	[Address4] [varchar](50) NULL,
	[Misc7] [char](8) NULL,
	[Misc8] [char](8) NULL,
	[CR3] [varchar](25) NULL,
	[addrid] [char](1) NULL
) ON [PRIMARY]
END
GO
