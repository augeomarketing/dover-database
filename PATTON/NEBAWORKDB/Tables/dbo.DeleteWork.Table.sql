USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[DeleteWork]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteWork]') AND type in (N'U'))
DROP TABLE [dbo].[DeleteWork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteWork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DeleteWork](
	[Account Identifier] [nvarchar](255) NULL,
	[System Bank Identifier] [float] NULL,
	[Principal Bank Identifier] [float] NULL,
	[Agent Bank Identifier] [float] NULL,
	[Primary Industry Standard Customer Name] [nvarchar](255) NULL,
	[Secondary Industry Standard Customer Name] [nvarchar](255) NULL,
	[Address Line One Text] [nvarchar](255) NULL,
	[Address Line Two Text] [nvarchar](255) NULL,
	[Address City Name] [nvarchar](255) NULL,
	[Address State Name] [nvarchar](255) NULL,
	[Address Zip Code] [nvarchar](255) NULL,
	[Primary Home Phone Text] [nvarchar](255) NULL,
	[Internal Status Code] [nvarchar](255) NULL,
	[External Status Code] [nvarchar](255) NULL,
	[Primary Social Security Number Text] [nvarchar](255) NULL,
	[Secondary Social Security Number Text] [nvarchar](255) NULL,
	[Account Identifier2] [nvarchar](255) NULL,
	[External Status Code Last Change Date] [nvarchar](255) NULL,
	[Miscellaneous Seventh Text] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
