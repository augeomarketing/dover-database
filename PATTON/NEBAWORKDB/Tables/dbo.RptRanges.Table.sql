USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[RptRanges]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RptRanges]') AND type in (N'U'))
DROP TABLE [dbo].[RptRanges]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RptRanges]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RptRanges](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[NumAccounts] [int] NOT NULL,
	[Range] [int] NOT NULL,
	[RunDate] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
