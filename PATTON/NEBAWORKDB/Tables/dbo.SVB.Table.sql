USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[SVB]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SVB]') AND type in (N'U'))
DROP TABLE [dbo].[SVB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SVB]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SVB](
	[Company Name] [nvarchar](255) NULL,
	[Company Identifier] [nvarchar](255) NULL,
	[Primary Industry Standard Customer Name] [nvarchar](255) NULL,
	[Miscellaneous Seventh Text] [nvarchar](255) NULL,
	[Miscellaneous Eighth Text] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
