USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[transactionwork]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_transactionwork_Add Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[transactionwork]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_transactionwork_Add Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transactionwork] DROP CONSTRAINT [DF_transactionwork_Add Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_transactionwork_Subtract Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[transactionwork]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_transactionwork_Subtract Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transactionwork] DROP CONSTRAINT [DF_transactionwork_Subtract Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_transactionwork_Bonus Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[transactionwork]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_transactionwork_Bonus Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transactionwork] DROP CONSTRAINT [DF_transactionwork_Bonus Points]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transactionwork]') AND type in (N'U'))
DROP TABLE [dbo].[transactionwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[transactionwork]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[transactionwork](
	[AcctID] [varchar](16) NOT NULL,
	[SystemBankId] [varchar](4) NOT NULL,
	[PrincipleBankId] [varchar](4) NOT NULL,
	[AgentBankId] [varchar](4) NOT NULL,
	[CustID] [varchar](9) NULL,
	[trancount] [varchar](4) NULL,
	[TranCode] [varchar](3) NULL,
	[Add Points] [numeric](18, 0) NOT NULL,
	[Subtract Points] [numeric](18, 0) NOT NULL,
	[Bonus Points] [numeric](18, 0) NOT NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_transactionwork_Add Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[transactionwork]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_transactionwork_Add Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transactionwork] ADD  CONSTRAINT [DF_transactionwork_Add Points]  DEFAULT (0) FOR [Add Points]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_transactionwork_Subtract Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[transactionwork]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_transactionwork_Subtract Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transactionwork] ADD  CONSTRAINT [DF_transactionwork_Subtract Points]  DEFAULT (0) FOR [Subtract Points]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_transactionwork_Bonus Points]') AND parent_object_id = OBJECT_ID(N'[dbo].[transactionwork]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_transactionwork_Bonus Points]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[transactionwork] ADD  CONSTRAINT [DF_transactionwork_Bonus Points]  DEFAULT (0) FOR [Bonus Points]
END


End
GO
