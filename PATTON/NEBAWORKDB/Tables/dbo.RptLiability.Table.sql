USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[RptLiability]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RptLiability]') AND type in (N'U'))
DROP TABLE [dbo].[RptLiability]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RptLiability]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RptLiability](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[MonthAsStr] [varchar](10) NULL,
	[BeginBal] [decimal](18, 0) NOT NULL,
	[EndBal] [decimal](18, 0) NOT NULL,
	[NetPtDelta] [decimal](18, 0) NOT NULL,
	[RedeemBal] [decimal](18, 0) NOT NULL,
	[RedeemDelta] [decimal](18, 0) NOT NULL,
	[NoCusts] [int] NOT NULL,
	[RedeemCusts] [int] NOT NULL,
	[Redemptions] [decimal](18, 0) NOT NULL,
	[Adjustments] [decimal](18, 0) NOT NULL,
	[BonusDelta] [decimal](18, 0) NOT NULL,
	[ReturnPts] [decimal](18, 0) NOT NULL,
	[CCNetPtDelta] [decimal](18, 0) NOT NULL,
	[CCNoCusts] [int] NOT NULL,
	[CCReturnPts] [decimal](18, 0) NOT NULL,
	[CCOverage] [decimal](18, 0) NOT NULL,
	[DCNetPtDelta] [decimal](18, 0) NOT NULL,
	[DCNoCusts] [int] NOT NULL,
	[DCReturnPts] [decimal](18, 0) NOT NULL,
	[DCOverage] [decimal](18, 0) NOT NULL,
	[AvgRedeem] [decimal](18, 0) NOT NULL,
	[AvgTotal] [decimal](18, 0) NOT NULL,
	[RunDate] [datetime] NOT NULL,
	[BEBonus] [decimal](18, 0) NULL,
	[PURGEDPOINTS] [decimal](18, 0) NULL,
	[SUBMISC] [decimal](18, 0) NULL,
	[ExpiredPoints] [decimal](18, 0) NULL,
	[TieredCCPoints] [decimal](18, 0) NULL,
	[TieredCCReturns] [decimal](18, 0) NULL,
	[TieredCCOverage] [decimal](18, 0) NULL,
	[RedReturns] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
