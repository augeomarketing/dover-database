USE [NEBAWORKDB]
GO
/****** Object:  Table [dbo].[Date_Test_Table]    Script Date: 03/07/2012 17:07:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Date_Test_Table]') AND type in (N'U'))
DROP TABLE [dbo].[Date_Test_Table]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Date_Test_Table]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Date_Test_Table](
	[rowid] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[datetime_test] [varchar](10) NULL,
	[datetime_test2] [datetime] NULL
) ON [PRIMARY]
END
GO
