USE [Purell]
GO
/****** Object:  Table [dbo].[pancrossreference]    Script Date: 02/02/2010 15:56:44 ******/
ALTER TABLE [dbo].[pancrossreference] DROP CONSTRAINT [FK_pancrossreference_accountnumber]
GO
ALTER TABLE [dbo].[pancrossreference] DROP CONSTRAINT [FK_pancrossreference_bin]
GO
ALTER TABLE [dbo].[pancrossreference] DROP CONSTRAINT [FK_pancrossreference_extendedbin]
GO
ALTER TABLE [dbo].[pancrossreference] DROP CONSTRAINT [FK_pancrossreference_financialinstitution]
GO
ALTER TABLE [dbo].[pancrossreference] DROP CONSTRAINT [DF_pancrossreference_dim_pancrossreference_created]
GO
ALTER TABLE [dbo].[pancrossreference] DROP CONSTRAINT [DF_pancrossreference_dim_pancrossreference_lastmodified]
GO
DROP TABLE [dbo].[pancrossreference]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pancrossreference](
	[sid_pancrossreference_pan] [varchar](16) NOT NULL,
	[sid_financialinstitution_id] [bigint] NOT NULL,
	[sid_bin_id] [bigint] NOT NULL,
	[sid_extendedbin_id] [varchar](1) NOT NULL,
	[sid_accountnumber_id] [bigint] NOT NULL,
	[dim_pancrossreference_crossreferencenumber] [varchar](16) NOT NULL,
	[dim_pancrossreference_created] [datetime] NOT NULL,
	[dim_pancrossreference_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_pancrossreference] PRIMARY KEY CLUSTERED 
(
	[sid_pancrossreference_pan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_pancrossreference_crossreferencenumber] UNIQUE NONCLUSTERED 
(
	[dim_pancrossreference_crossreferencenumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_pancrossreference_crossreferencenumber_pan] ON [dbo].[pancrossreference] 
(
	[dim_pancrossreference_crossreferencenumber] ASC,
	[sid_pancrossreference_pan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_pancrossreference_fi_bin_extbin_acctnbr] ON [dbo].[pancrossreference] 
(
	[sid_financialinstitution_id] ASC,
	[sid_bin_id] ASC,
	[sid_extendedbin_id] ASC,
	[sid_accountnumber_id] ASC,
	[dim_pancrossreference_crossreferencenumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_pancrossreference_pan_crossreferencenumber] ON [dbo].[pancrossreference] 
(
	[sid_pancrossreference_pan] ASC,
	[dim_pancrossreference_crossreferencenumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[pancrossreference]  WITH CHECK ADD  CONSTRAINT [FK_pancrossreference_accountnumber] FOREIGN KEY([sid_accountnumber_id])
REFERENCES [dbo].[accountnumber] ([sid_accountnumber_id])
GO
ALTER TABLE [dbo].[pancrossreference] CHECK CONSTRAINT [FK_pancrossreference_accountnumber]
GO
ALTER TABLE [dbo].[pancrossreference]  WITH CHECK ADD  CONSTRAINT [FK_pancrossreference_bin] FOREIGN KEY([sid_bin_id])
REFERENCES [dbo].[bin] ([sid_bin_id])
GO
ALTER TABLE [dbo].[pancrossreference] CHECK CONSTRAINT [FK_pancrossreference_bin]
GO
ALTER TABLE [dbo].[pancrossreference]  WITH CHECK ADD  CONSTRAINT [FK_pancrossreference_extendedbin] FOREIGN KEY([sid_extendedbin_id])
REFERENCES [dbo].[extendedbin] ([sid_extendedbin_id])
GO
ALTER TABLE [dbo].[pancrossreference] CHECK CONSTRAINT [FK_pancrossreference_extendedbin]
GO
ALTER TABLE [dbo].[pancrossreference]  WITH CHECK ADD  CONSTRAINT [FK_pancrossreference_financialinstitution] FOREIGN KEY([sid_financialinstitution_id])
REFERENCES [dbo].[financialinstitution] ([sid_financialinstitution_id])
GO
ALTER TABLE [dbo].[pancrossreference] CHECK CONSTRAINT [FK_pancrossreference_financialinstitution]
GO
ALTER TABLE [dbo].[pancrossreference] ADD  CONSTRAINT [DF_pancrossreference_dim_pancrossreference_created]  DEFAULT (getdate()) FOR [dim_pancrossreference_created]
GO
ALTER TABLE [dbo].[pancrossreference] ADD  CONSTRAINT [DF_pancrossreference_dim_pancrossreference_lastmodified]  DEFAULT (getdate()) FOR [dim_pancrossreference_lastmodified]
GO
