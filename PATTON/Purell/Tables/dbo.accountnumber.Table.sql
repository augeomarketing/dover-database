USE [Purell]
GO
/****** Object:  Table [dbo].[accountnumber]    Script Date: 02/02/2010 15:56:44 ******/
ALTER TABLE [dbo].[accountnumber] DROP CONSTRAINT [DF_accountnumber_dim_accountnumber_created]
GO
ALTER TABLE [dbo].[accountnumber] DROP CONSTRAINT [DF_accountnumber_dim_accountnumber_lastmodified]
GO
DROP TABLE [dbo].[accountnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[accountnumber](
	[sid_accountnumber_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_accountnumber_number] [varchar](7) NOT NULL,
	[dim_accountnumber_created] [datetime] NOT NULL,
	[dim_accountnumber_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_accountnumber] PRIMARY KEY CLUSTERED 
(
	[sid_accountnumber_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_accountnumber_number_id] ON [dbo].[accountnumber] 
(
	[dim_accountnumber_number] ASC,
	[sid_accountnumber_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[accountnumber] ADD  CONSTRAINT [DF_accountnumber_dim_accountnumber_created]  DEFAULT (getdate()) FOR [dim_accountnumber_created]
GO
ALTER TABLE [dbo].[accountnumber] ADD  CONSTRAINT [DF_accountnumber_dim_accountnumber_lastmodified]  DEFAULT (getdate()) FOR [dim_accountnumber_lastmodified]
GO
