USE [Purell]
GO
/****** Object:  Table [dbo].[bin]    Script Date: 02/02/2010 15:56:44 ******/
ALTER TABLE [dbo].[bin] DROP CONSTRAINT [DF_bin_dim_bin_created]
GO
ALTER TABLE [dbo].[bin] DROP CONSTRAINT [DF_bin_dim_bin_lastmodified]
GO
DROP TABLE [dbo].[bin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bin](
	[sid_bin_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_bin_code] [varchar](2) NOT NULL,
	[dim_bin_created] [datetime] NOT NULL,
	[dim_bin_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_bin] PRIMARY KEY CLUSTERED 
(
	[sid_bin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_bin_code_id] ON [dbo].[bin] 
(
	[dim_bin_code] ASC,
	[sid_bin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bin] ADD  CONSTRAINT [DF_bin_dim_bin_created]  DEFAULT (getdate()) FOR [dim_bin_created]
GO
ALTER TABLE [dbo].[bin] ADD  CONSTRAINT [DF_bin_dim_bin_lastmodified]  DEFAULT (getdate()) FOR [dim_bin_lastmodified]
GO
