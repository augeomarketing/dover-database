USE [Purell]
GO
/****** Object:  Table [dbo].[extendedbin]    Script Date: 02/02/2010 15:56:44 ******/
ALTER TABLE [dbo].[extendedbin] DROP CONSTRAINT [DF_extendedbin_dim_extendedbin_created]
GO
ALTER TABLE [dbo].[extendedbin] DROP CONSTRAINT [DF_extendedbin_dim_extendedbin_lastmodified]
GO
DROP TABLE [dbo].[extendedbin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[extendedbin](
	[sid_extendedbin_id] [varchar](1) NOT NULL,
	[dim_extendedbin_code] [varchar](1) NOT NULL,
	[dim_extendedbin_created] [datetime] NOT NULL,
	[dim_extendedbin_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_extendedbin] PRIMARY KEY CLUSTERED 
(
	[sid_extendedbin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_extendedbin_code_id] ON [dbo].[extendedbin] 
(
	[dim_extendedbin_code] ASC,
	[sid_extendedbin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[extendedbin] ADD  CONSTRAINT [DF_extendedbin_dim_extendedbin_created]  DEFAULT (getdate()) FOR [dim_extendedbin_created]
GO
ALTER TABLE [dbo].[extendedbin] ADD  CONSTRAINT [DF_extendedbin_dim_extendedbin_lastmodified]  DEFAULT (getdate()) FOR [dim_extendedbin_lastmodified]
GO
