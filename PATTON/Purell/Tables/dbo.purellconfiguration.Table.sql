USE [Purell]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_purellconfiguration_dim_purellconfiguration_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[purellconfiguration] DROP CONSTRAINT [DF_purellconfiguration_dim_purellconfiguration_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_purellconfiguration_dim_purellconfiguration_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[purellconfiguration] DROP CONSTRAINT [DF_purellconfiguration_dim_purellconfiguration_lastmodified]
END

GO

USE [Purell]
GO

/****** Object:  Table [dbo].[purellconfiguration]    Script Date: 02/05/2010 13:38:19 ******/
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[purellconfiguration]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[purellconfiguration]
GO

USE [Purell]
GO

/****** Object:  Table [dbo].[purellconfiguration]    Script Date: 02/05/2010 13:38:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[purellconfiguration](
	[sid_purellconfiguration_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_purellconfiguration_parameter] [varchar](50) NOT NULL,
	[dim_purellconfiguration_value] [varchar](255) NOT NULL,
	[dim_purellconfiguration_created] [datetime] NOT NULL,
	[dim_purellconfiguration_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_purellconfiguration] PRIMARY KEY CLUSTERED 
(
	[sid_purellconfiguration_id] ASC
) ON [PRIMARY],
 CONSTRAINT [UK_purellconfiguration_parameter] UNIQUE NONCLUSTERED 
(
	[dim_purellconfiguration_parameter] ASC
) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [Purell]
/****** Object:  Index [IX_purellconfiguration_parameter_value]    Script Date: 02/05/2010 13:38:19 ******/
CREATE NONCLUSTERED INDEX [IX_purellconfiguration_parameter_value] ON [dbo].[purellconfiguration] 
(
	[dim_purellconfiguration_parameter] ASC,
	[dim_purellconfiguration_value] ASC
) ON [PRIMARY]
GO

/****** Object:  Trigger [dbo].[trig_purellconfiguration_Update]    Script Date: 02/05/2010 13:38:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[trig_purellconfiguration_Update]
   ON  [dbo].[purellconfiguration]
   AFTER insert, update
AS 
BEGIN
    SET NOCOUNT ON;

    update pc
	   set dim_purellconfiguration_lastmodified = getdate()
    from inserted ins join dbo.purellconfiguration pc
	   on ins.sid_purellconfiguration_id = pc.sid_purellconfiguration_id

END

GO

ALTER TABLE [dbo].[purellconfiguration] ADD  CONSTRAINT [DF_purellconfiguration_dim_purellconfiguration_created]  DEFAULT (getdate()) FOR [dim_purellconfiguration_created]
GO

ALTER TABLE [dbo].[purellconfiguration] ADD  CONSTRAINT [DF_purellconfiguration_dim_purellconfiguration_lastmodified]  DEFAULT (getdate()) FOR [dim_purellconfiguration_lastmodified]
GO


