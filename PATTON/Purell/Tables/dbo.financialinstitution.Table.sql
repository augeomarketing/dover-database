USE [Purell]
GO
/****** Object:  Table [dbo].[financialinstitution]    Script Date: 02/02/2010 15:56:44 ******/
ALTER TABLE [dbo].[financialinstitution] DROP CONSTRAINT [DF_financialinstitution_dim_financialinstitution_created]
GO
ALTER TABLE [dbo].[financialinstitution] DROP CONSTRAINT [DF_financialinstitution_dim_financialinstitution_lastmodified]
GO
DROP TABLE [dbo].[financialinstitution]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[financialinstitution](
	[sid_financialinstitution_id] [bigint] IDENTITY(0,1) NOT NULL,
	[dim_financialinstitution_banknumber] [varchar](6) NOT NULL,
	[dim_financialinstitution_created] [datetime] NOT NULL,
	[dim_financialinstitution_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_financialinstitution] PRIMARY KEY CLUSTERED 
(
	[sid_financialinstitution_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_financialinstitution_banknumber_id] ON [dbo].[financialinstitution] 
(
	[dim_financialinstitution_banknumber] ASC,
	[sid_financialinstitution_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_created]  DEFAULT (getdate()) FOR [dim_financialinstitution_created]
GO
ALTER TABLE [dbo].[financialinstitution] ADD  CONSTRAINT [DF_financialinstitution_dim_financialinstitution_lastmodified]  DEFAULT (getdate()) FOR [dim_financialinstitution_lastmodified]
GO
