USE [Purell]
GO
/****** Object:  StoredProcedure [dbo].[spSanitizeDatabase]    Script Date: 02/08/2010 15:25:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[spSanitizeDatabase]
    @DBToSanitize	    nvarchar(50)
AS

set nocount on

declare @DBnm			   nvarchar(50)
declare @tablenm		   nvarchar(250)
declare @columnnm		   nvarchar(250)

declare @sql			   nvarchar(4000)

declare @luhncount		   int
declare @rowcount		   int

declare @threshold		   decimal(4,3) = 0.300


create table #DBTableColumns
    (sid_temp			int identity(1,1) primary key,
     DB		 		nvarchar(50),
     Tablenm			nvarchar(250),
     Columnnm			nvarchar(250) )

--
-- Build temp table containint the db name, tables in the db, and the columns within each table
--
-- we are only looking at columns that have a length >= 16  (typical PAN size)
-- and only datatypes char, nchar, varchar, nvarchar
--
set @sql = 'insert into #DBtableColumns
		  select quotename(@DBToSanitize), quotename(tb.name), quotename(cl.name)
		  from ' + quotename(@DBToSanitize) + '.sys.tables tb join ' + quotename(@DBToSanitize) + '.sys.columns cl
			 on tb.object_id = cl.object_id

		  join sys.types st
			 on st.system_type_id = cl.system_type_id
		  where tb.type = ''U''
		  and cl.max_length >= 16
		  and st.name like ''%char%''
		  order by tb.name'
exec sp_executesql @sql, N'@DBToSanitize nvarchar(50)', @DBToSanitize = @DBToSanitize


declare csrLuhnCheck cursor FAST_FORWARD for
    select DB, TableNm, Columnnm 
    from #DBTableColumns

open csrLuhnCheck

fetch next from csrLuhnCheck into @DBnm, @tablenm, @columnnm
while @@FETCH_STATUS = 0
BEGIN
    print replicate('=', 100)
    print '**Checking ' + @DBnm + ', table: ' + @tablenm + ', Column: ' + @columnnm
    print replicate('=', 100)


    --
    -- Get rowcount within table being inspected
    set @rowcount = 0
    set @sql = 'set @rowcount = (select count(*) from ' + @DBnm + '.dbo.' + @tablenm + ')'
    exec sp_executesql @sql, N'@rowcount int output', @rowcount = @rowcount OUTPUT


    --
    -- Get rowcount of table being inspected that has PAN data in the column being inspected
    set @luhncount = 0
    set @sql = 'set @luhncount = (SELECT sum( [RewardsNOW].[dbo].[fnCheckLuhn10] (' + @columnnm + ') )
							 from ' + @dbnm + '.dbo.' + @tablenm + 
						    ' where isnumeric(' + @columnnm + ') = 1 and
							 len(' + @columnnm + ') = 16 and 
			 				 RewardsNOW.dbo.fnCheckLuhn10 (' + @columnnm + ') = 1 )'

    exec sp_executesql @sql, N'@luhncount int output', @luhncount = @luhncount OUTPUT     


    --
    -- If > 1 rows has suspected PAN data
    -- AND a certain percentage threshold  (set at top of sproc) is met or exceeded - then the
    -- column needs to be sanitized
    if @luhncount <> 0 and ((@luhncount * 1.000) / (@rowcount * 1.000)) > @threshold
    BEGIN -- a table with PAN has been identified.  now we need to scrub it
	   print '	Table: ' + @tablenm + ', Column: ' + @columnnm + ' has ' + cast(@luhncount as varchar(10)) + ' cards.'

	   
	   --
	   -- update the column in the table being processed with already assigned cross references.
	   -- then cursor throw the table, processing the specific column and create & update with new PAN cross references
	   -- The SQL is compacted to keep the length of the dynamic sql to a minimum
	   set @sql = 'set nocount on; 
				declare @pan varchar(50), @xref varchar(50), @validpan varchar(1); declare csr cursor fast_forward for select distinct' + @columnnm + ' from ' + @DBnm + '.dbo.' + @tablenm + ' with (nolock); open csr;
				fetch next from csr into @pan; while @@fetch_status = 0 BEGIN  exec purell.dbo.sptranslatePantoXref @pan, @xref output, @validpan output; 
				update ' + @DBnm + '.dbo.' + @tablenm + '  set ' + @columnnm + ' = @xref where ' + @columnnm + ' = @pan and @validpan = ''Y'';
				fetch next from csr into @pan END
				close csr; deallocate csr;'
	   --print @sql
	   exec sp_executesql @sql, N'@columnnm varchar(250), @Tablenm varchar(250)', @columnnm = @columnnm, @tablenm = @tablenm


    END

    print 'DONE with ' + @DBNM + ', table: ' + @tablenm + ', Column: ' + @columnnm
    print ' '
    print ' '

    fetch next from csrLuhnCheck into @DBnm, @tablenm, @columnnm
END

close csrLuhnCheck
deallocate csrLuhnCheck


