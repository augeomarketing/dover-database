USE [Purell]
GO

/****** Object:  StoredProcedure [dbo].[spPrepare_RN_CentralProcessingTables_BAK_DBForDevMigration]    Script Date: 03/20/2015 09:12:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPrepare_RN_CentralProcessingTables_BAK_DBForDevMigration]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPrepare_RN_CentralProcessingTables_BAK_DBForDevMigration]
GO

USE [Purell]
GO

/****** Object:  StoredProcedure [dbo].[spPrepare_RN_CentralProcessingTables_BAK_DBForDevMigration]    Script Date: 03/20/2015 09:12:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[spPrepare_RN_CentralProcessingTables_BAK_DBForDevMigration]
    @DBNumber		nvarchar(3) = '',   
    @DropPRLDBIfExists  nvarchar(1) = 'Y',
    @BackupPathFilenm   nvarchar(255) OUTPUT

AS
/*
declare @BackupPathFilenm   nvarchar(255);  
exec spPrepare_RN_CentralProcessingTables_BAK_DBForDevMigration '712','Y', @BackupPathFilenm output
print @BackupPathFilenm
*/

declare @SQL nvarchar(4000)

DECLARE @RC int

declare  @DBName nvarchar(50)='RN_CentralProcessingTables_BAK'

DECLARE @DBName_Purell nvarchar(50)
DECLARE @DBName_Purell_Backup nvarchar(50)

DECLARE @CompressionLvl int

DECLARE @DataFileNm	    nvarchar(255)
declare @LogFileNm	    nvarchar(255)
declare @DataFileNmPath nvarchar(255)
declare @LogFileNmPath  nvarchar(255)

declare @BackupPathFileNmB4Sanitize  nvarchar(255)

declare @WithMoveDataFile nvarchar(512)
declare @withMoveLogFile nvarchar(512)

--
-- Validate database name passed in.

-- Do not cleanse these databases
if @DBName in ('master', 'model', 'msdb', 'tempdb', 'Purell')
    raiserror(N'Invalid database name specified.', 16, 1)



/*
set @DBName = '405NewCastle'
set @BackupPathFilenm = 'C:\ProcessingBackups\prl' + @DBName + '.bak'
*/

set @DBName_Purell = 'prl' + @DBName

set @DBName_Purell_Backup = @DBName_Purell + '_' + CAST( datepart(yyyy,getdate()) as varchar(4)) +
									right( '00' + CAST( datepart(mm, getdate()) as varchar(2)),2) +
								     RIGHT( '00' + cast( datepart(dd, getdate()) as varchar(2)), 2) + 
								     '_'  + @DBNumber + '.bak'
								     


if @DropPRLDBIfExists = 'N' and exists(select 1 from sys.databases where name = @DBName_Purell)
    raiserror(N'Purell Database exists, and caller specified DO NOT DROP.', 16, 1)

--if the db prlRN_CentralProcessingTables_BAK exists, drop it
if exists(select 1 from sys.databases where name = @DBName_Purell)
BEGIN 
    BEGIN TRY
	   set @SQL = 'drop database ' + @DBName_Purell
	   exec sp_executesql @SQL
    END TRY
    BEGIN CATCH
	   raiserror(N'Error occurred deleting the PRL version of the database.', 16, 1)
    END CATCH
END

-- below sql returns: T:\ProcessingBackups\
set @BackupPathFileNmB4Sanitize = (select dim_purellconfiguration_value from Purell.dbo.purellconfiguration 
				     where dim_purellconfiguration_parameter = 'BackupPathB4Sanitize')

if @BackupPathFileNmB4Sanitize is null	  
    raiserror(N'Backup File parameter is null or missing in the PurellConfiguration table.', 16, 1)
	   
set @BackupPathFileNmB4Sanitize = @BackupPathFileNmB4Sanitize + @DBName_Purell_Backup


-- below returns T:\SanitizedDBBackups\
set @BackupPathFilenm = (select dim_purellconfiguration_value from Purell.dbo.purellconfiguration 
				     where dim_purellconfiguration_parameter = 'BackupPathAfterSanitize')

if @BackupPathFilenm is null	  
    raiserror(N'Backup File parameter is null or missing in the PurellConfiguration table.', 16, 1)
	   
set @BackupPathFilenm = @BackupPathFilenm + @DBName_Purell_Backup

BEGIN TRY

    set @CompressionLvl = 11


    -- Retrieve the data file name, and data file name path from sys.databases 
    set @sql = 'select @DataFileNm = name, @DataFileNmPath = physical_name
			 from ' + QUOTENAME(@DBName) + '.sys.database_files
			 where type = 0'  -- Get the data (mdf) file info
    exec sp_executesql @sql, N'@DataFileNm nvarchar(255) OUTPUT, @DataFileNmPath nvarchar(255) OUTPUT',	
				    @DataFileNm = @DataFileNm OUTPUT, @DataFileNmPath = @DataFileNmPath OUTPUT


    -- Retrieve the log file name, and the log file name path from sys.databases
    set @sql = 'select @LogFileNm = name, @LogFileNmPath = physical_name
			 from ' + QUOTENAME(@DBName) + '.sys.database_files
			 where type = 1'  -- Get the log (ldf) file info
    exec sp_executesql @sql, N'@LogFileNm nvarchar(255) OUTPUT, @LogFileNmPath nvarchar(255) OUTPUT',	
				    @LogFileNm = @LogFileNm OUTPUT, @LogFileNmPath = @LogFileNmPath OUTPUT


    -- Extract just the file path from the DataFileNmPath and LogFileNmPath
    set @DataFileNmPath = REVERSE( RIGHT(REVERSE(@DataFileNmPath), LEN(@DataFileNmPath) - charindex('\', REVERSE(@DataFileNmPath)) +1) )
    set @LogFileNmPath = REVERSE( RIGHT(REVERSE(@logfilenmpath), LEN(@logfilenmpath) - charindex('\', REVERSE(@logfilenmpath)) +1) )


    -- Backup the database to be sanitized
    EXECUTE @RC = [RewardsNOW].[dbo].[spBackupDBLiteSpeed] @DBName, @BackupPathFileNmB4Sanitize, @CompressionLvl


    --
    -- Now restore the database to one named prl<database name>
    -- This will be the database that gets sanitized
    set @WithMoveDataFile = 'MOVE ' + CHAR(39) + @DataFileNm + CHAR(39) + ' to ' + 
					   CHAR(39) + @DataFileNmPath + @DBName_Purell + '.mdf' + CHAR(39) 


    set @WithMovelogFile =  'MOVE ' + CHAR(39) + @logFileNm  + CHAR(39) + ' to ' + 
					   CHAR(39) + @logFileNmPath + @DBName_Purell + '_log.ldf' + CHAR(39)


    -- Restore the database to be sanitized to a different name
    exec master.dbo.xp_restore_database 
	   @database = @DBName_Purell,
	   @filename = @BackupPathFileNmB4Sanitize,
	   @filenumber = 1,
	   @with = N'REPLACE',
	   @with = N'STATS = 10',

	   @with = @WithMoveDataFile,
	   @with = @withMoveLogFile,

	   @affinity = 0,
	   @logging = 0


    --
    -- Now sanitize the database
    exec dbo.spSanitizeDatabase @dbname_purell


    -- Now backup the sanitized database.  This will be the backup file that gets moved down to the Dev environment

    --EXECUTE @RC = [RewardsNOW].[dbo].[spBackupDBLiteSpeed] @DBName_purell, @BackupPathFilenm, @CompressionLvl
	set @SQL= 'BACKUP DATABASE ' + @DBName_purell + ' TO DISK =''' + @BackupPathFilenm + ''''
	
	print '@DBName_purell:' + @DBName_purell
	print '@BackupPathFilenm:' + @BackupPathFilenm
	-- WORKS   BACKUP DATABASE prlRN_CentralProcessingTables_BAK TO DISK = 'T:\SanitizedDBBackups\prlRN_CentralProcessingTables_BAK_20150318_712.bak'
	exec sp_executesql @SQL

	--if @rc = 0
	--BEGIN
	--	set @SQL = 'drop database ' + @DBName_purell
	--	exec sp_executesql @SQL
		
		
	--END

END TRY

BEGIN CATCH
    raiserror(N'Error occurred during the sanitization process.', 16, 1)

END CATCH

GO


