use purell
GO

if exists(select top 1 * from sys.objects where name = 'spTranslatePantoXref' and type = 'P')
    drop procedure dbo.spTranslatePanToXref
GO

create procedure dbo.spTranslatePanToXref
    @PAN			varchar(16),
    @XrefNumber	varchar(16) output,
    @ValidPAN		varchar(1) output
as


declare @financialinstitution			    varchar(6)
declare @sid_financialinstitution		    bigint = 0

declare @bin						    varchar(2)
declare @sid_bin					    bigint = 0

declare @extendedbin				    varchar(1)
declare @sid_extendedbin				    varchar(1)

declare @accountnumber				    varchar(7)
declare @sid_accountnumber			    bigint = 0

declare @zeros						    varchar(7) = '0000000'


if len(@pan) = 16
    set @validpan = 'Y'
else
    set @validpan = 'N'

if @validpan = 'Y'
BEGIN

    set @financialinstitution   = left(@pan, 6)
    set @bin				   =	substring(@pan, 7, 2)
    set @extendedbin		   = substring(@pan, 9, 1)
    set @accountnumber		   = substring(@pan, 10, 7)


    --
    -- Financial Institution
    set @sid_financialinstitution = (select sid_financialinstitution_id
							  from dbo.financialinstitution
							  where dim_financialinstitution_banknumber = @financialinstitution)

    if @sid_financialinstitution is null
    BEGIN  -- add new financial institution to table
	   insert into dbo.financialinstitution
	   (dim_financialinstitution_banknumber)
	   values(@financialinstitution)

	   set @sid_financialinstitution = scope_identity()
    END


    --
    -- bin
    set @sid_bin = (select sid_bin_id
				from dbo.bin
				where dim_bin_code = @bin)

    if @sid_bin is null
    BEGIN  -- add new bin to table
	   insert into dbo.bin
	   (dim_bin_code)
	   values(@bin)

	   set @sid_bin = scope_identity()
    END

    
    --
    -- Extended Bin
    set @sid_ExtendedBin = (select sid_extendedbin_id
					   from dbo.extendedbin
					   where dim_extendedbin_code = @extendedbin)

    if @sid_extendedbin is null
    BEGIN  -- add new extended bin to table
	   set @ValidPan = 'N'  -- Extended bin is an alpha - A-I which represents 1-10.  these are already prepopulated
    END


    --
    -- account number
    set @sid_accountnumber = (select sid_accountnumber_id
					     from dbo.accountnumber
					     where dim_accountnumber_number = @accountnumber)

    if @sid_accountnumber is null
    BEGIN  -- add new account number to table
	   insert into dbo.accountnumber
	   (dim_accountnumber_number)
	   values(@accountnumber)

	   set @sid_accountnumber = scope_identity()
    END


    --
    -- 07 Apr 2010 Paul H Butler
    -- Add test for validpan = y
    -- @ValidPan can be "N" if sid_extendedbin is not found.
    if @ValidPan = 'Y'
    BEGIN
	   --
	   -- We now have the identity values (sids) for each part of the PAN
	   -- Now assemble the cross reference number, padding zeroes to each section


	   set @XrefNumber = ( select dim_pancrossreference_crossreferencenumber
					   from dbo.pancrossreference
					   where sid_pancrossreference_pan = @pan)

	   if @xrefnumber is null
	   BEGIN  -- xref number does not exist in the cross reference table.  now add it

		  set @XrefNumber = right(@zeros + cast(@sid_financialinstitution as varchar(6)), 6) + 
				 right(@zeros + cast(@sid_bin as varchar(2)), 2) +
				 @sid_extendedbin +
				 right(@zeros + cast(@sid_accountnumber as varchar(7)), 7)

		  insert into dbo.pancrossreference
		  (sid_pancrossreference_pan, sid_financialinstitution_id, sid_bin_id, sid_extendedbin_id, sid_accountnumber_id, dim_pancrossreference_crossreferencenumber)
		  values(@pan, @sid_financialinstitution, @sid_bin, @sid_extendedbin, @sid_accountnumber, @xrefnumber)

	   END
    END
    

end






/*

declare @xref varchar(16)
declare @validpan varchar(1)

exec dbo.spTranslatePanToXref '4037050173456789', @xref OUTPUT, @validpan output

select @xref, @validpan

select *
from dbo.pancrossreference


-------------------------------------------------

delete from dbo.pancrossreference
delete from dbo.accountnumber
delete from dbo.bin
delete from dbo.financialinstitution
-- DO NOT DELETE from EXTENDED BIN


*/

