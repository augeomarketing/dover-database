
use purell
GO

if exists(select 1 from sys.objects where name = 'spTranslateXrefToPan' and type = 'P')
    drop procedure dbo.spTranslateXrefToPan
GO

create procedure dbo.spTranslateXrefToPan
    @Xref			varchar(16),
    @PAN			varchar(16) OUTPUT,
    @ValidXref		varchar(1) OUTPUT

As

set @ValidXref = 'N'  -- by default, set this to NO: not valid

set @PAN = (select sid_pancrossreference_pan
		  from dbo.pancrossreference
		  where dim_pancrossreference_crossreferencenumber = @Xref)

if @PAN is not null
    set @ValidXref = 'Y'


/*

declare @PAN		varchar(16)
declare @ValidXref	varchar(1)

exec dbo.spTranslateXrefToPan
    '00000804A0000003', @PAN output, @ValidXref output

select @pan, @validxref

*/