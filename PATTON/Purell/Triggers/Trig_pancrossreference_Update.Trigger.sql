USE [Purell]
GO
/****** Object:  Trigger [Trig_pancrossreference_Update]    Script Date: 02/02/2010 15:56:44 ******/
DROP TRIGGER [dbo].[Trig_pancrossreference_Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Trig_pancrossreference_Update]
   ON  [dbo].[pancrossreference]
   AFTER INSERT,UPDATE
AS 
BEGIN
    SET NOCOUNT ON;

    update pc
	   set dim_pancrossreference_lastmodified = getdate()
    from inserted ins join dbo.pancrossreference pc
	   on ins.sid_pancrossreference_pan = pc.sid_pancrossreference_pan

END
GO
