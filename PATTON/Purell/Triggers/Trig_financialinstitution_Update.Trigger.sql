USE [Purell]
GO
/****** Object:  Trigger [Trig_financialinstitution_Update]    Script Date: 02/02/2010 15:56:44 ******/
DROP TRIGGER [dbo].[Trig_financialinstitution_Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Trig_financialinstitution_Update]
   ON  [dbo].[financialinstitution]
   AFTER INSERT,UPDATE
AS 
BEGIN
    SET NOCOUNT ON;

    update fi
	   set dim_financialinstitution_lastmodified = getdate()
    from inserted ins join dbo.financialinstitution fi
	   on ins.sid_financialinstitution_id = fi.sid_financialinstitution_id
    
    

END
GO
