USE [Purell]
GO
/****** Object:  Trigger [Trig_bin_Update]    Script Date: 02/02/2010 15:56:44 ******/
DROP TRIGGER [dbo].[Trig_bin_Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Trig_bin_Update]
   ON  [dbo].[bin]
   AFTER INSERT,UPDATE
AS 
BEGIN
    SET NOCOUNT ON;

    update b
	   set dim_bin_lastmodified = getdate()
    from inserted ins join dbo.bin b
	   on ins.sid_bin_id = b.sid_bin_id
    
    

END
GO
