USE [Purell]
GO
/****** Object:  Trigger [Trig_extendedbin_Update]    Script Date: 02/02/2010 15:56:44 ******/
DROP TRIGGER [dbo].[Trig_extendedbin_Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Trig_extendedbin_Update]
   ON  [dbo].[extendedbin]
   AFTER INSERT,UPDATE
AS 
BEGIN
    SET NOCOUNT ON;

    update eb
	   set dim_extendedbin_lastmodified = getdate()
    from inserted ins join dbo.extendedbin eb
	   on ins.sid_extendedbin_id = eb.sid_extendedbin_id
    
    

END
GO
