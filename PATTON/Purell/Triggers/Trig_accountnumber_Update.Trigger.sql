USE [Purell]
GO
/****** Object:  Trigger [Trig_accountnumber_Update]    Script Date: 02/02/2010 15:56:44 ******/
DROP TRIGGER [dbo].[Trig_accountnumber_Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[Trig_accountnumber_Update]
   ON  [dbo].[accountnumber]
   AFTER INSERT,UPDATE
AS 
BEGIN
    SET NOCOUNT ON;

    update an
	   set dim_accountnumber_lastmodified = getdate()
    from inserted ins join dbo.accountnumber an
	   on ins.sid_accountnumber_id = an.sid_accountnumber_id
    
    

END
GO
