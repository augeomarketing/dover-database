declare @sql nvarchar(2000)
declare @myvariable varchar(255) 
declare mycursor cursor forward_only for 
  select QUOTENAME(name) 
    from sys.databases 
    where database_id > 4
      AND LEFT(name,3) NOT IN ('prl')
      AND LEFT(name,1) NOT IN ('z')
  
open mycursor 
while (1=1)
begin  
  fetch next from mycursor into @myvariable  
  if @@fetch_status <> 0  break; 
  set @sql = 'SELECT 1 FROM ' + @myvariable + '.sys.tables 
				WHERE name = ''Affiliat'''
  
  exec sp_executesql @sql
  IF @@ROWCOUNT = 1
  begin
    print 'Working on: ' + @myvariable 
	set @sql = 'UPDATE tc
	SET TCLastSix = RIGHT(RTRIM(p.dim_pancrossreference_crossreferencenumber), 6)
	FROM ' + @myvariable + '.dbo.AFFILIAT a
	JOIN prlFullfillment.dbo.TravelCert tc
	ON RIGHT(RTRIM(a.ACCTID),6) = tc.TCLastSix
	JOIN prlFullfillment.dbo.Main m
	ON tc.TransID = m.TransID
	AND a.tipnumber = m.tipnumber
	JOIN Purell.dbo.pancrossreference p
	ON p.sid_pancrossreference_pan = a.ACCTID'
    exec sp_executesql @sql
    --print @sql
	
  end
end 
close mycursor 
deallocate mycursor