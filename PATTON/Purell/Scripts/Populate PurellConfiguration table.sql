USE [Purell];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[purellconfiguration] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[purellconfiguration]([sid_purellconfiguration_id], [dim_purellconfiguration_parameter], [dim_purellconfiguration_value], [dim_purellconfiguration_created], [dim_purellconfiguration_lastmodified])
SELECT 3, N'BackupPath', N'C:\ProcessingBackups\', '20100205 13:58:07.953', '20100205 13:58:07.957'
COMMIT;
RAISERROR (N'[dbo].[purellconfiguration]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[purellconfiguration] OFF;
