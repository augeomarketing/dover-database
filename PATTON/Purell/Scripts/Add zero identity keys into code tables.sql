
/*

truncate table dbo.pancrossreference

delete from dbo.accountnumber
delete from dbo.bin
delete from dbo.financialinstitution

DBCC CHECKIDENT (accountnumber, reseed, 0)
DBCC CHECKIDENT (bin, reseed, 0)
DBCC CHECKIDENT (financialinstitution, reseed, 0)

*/



set identity_insert dbo.bin on

insert into dbo.bin
(sid_bin_id, dim_bin_code)
values(0, '24')

set identity_insert dbo.bin off


set identity_insert dbo.accountnumber on

insert into dbo.accountnumber
(sid_accountnumber_id, dim_accountnumber_number)
values(0, '0000069')

set identity_insert dbo.accountnumber off


set identity_insert dbo.financialinstitution on

insert into dbo.financialinstitution
(sid_financialinstitution_id, dim_financialinstitution_banknumber)
values(0, '146927')

set identity_insert dbo.financialinstitution off
