use purell
GO

declare @SQL nvarchar(4000)

DECLARE @RC int
DECLARE @DBName nvarchar(50)
DECLARE @DBName_Purell nvarchar(50)

DECLARE @CompressionLvl int

DECLARE @BackupPathFilenm nvarchar(255)

DECLARE @DataFileNm	    nvarchar(255)
declare @LogFileNm	    nvarchar(255)
declare @DataFileNmPath nvarchar(255)
declare @LogFileNmPath  nvarchar(255)


declare @WithMoveDataFile nvarchar(512)
declare @withMoveLogFile nvarchar(512)

set @DBName = '405NewCastle'
set @DBName_Purell = 'prl' + @DBName

set @CompressionLvl = 11
set @BackupPathFilenm = 'C:\ProcessingBackups\prl' + @DBName + '.bak'


-- Retrieve the data file name, and data file name path from sys.databases 
set @sql = 'select @DataFileNm = name, @DataFileNmPath = physical_name
		  from ' + QUOTENAME(@DBName) + '.sys.database_files
		  where type = 0'  -- Get the data (mdf) file info
exec sp_executesql @sql, N'@DataFileNm nvarchar(255) OUTPUT, @DataFileNmPath nvarchar(255) OUTPUT',	
				@DataFileNm = @DataFileNm OUTPUT, @DataFileNmPath = @DataFileNmPath OUTPUT


-- Retrieve the log file name, and the log file name path from sys.databases
set @sql = 'select @LogFileNm = name, @LogFileNmPath = physical_name
		  from ' + QUOTENAME(@DBName) + '.sys.database_files
		  where type = 1'  -- Get the log (ldf) file info
exec sp_executesql @sql, N'@LogFileNm nvarchar(255) OUTPUT, @LogFileNmPath nvarchar(255) OUTPUT',	
				@LogFileNm = @LogFileNm OUTPUT, @LogFileNmPath = @LogFileNmPath OUTPUT


-- Extract just the file path from the DataFileNmPath and LogFileNmPath
set @DataFileNmPath = REVERSE( RIGHT(REVERSE(@DataFileNmPath), LEN(@DataFileNmPath) - charindex('\', REVERSE(@DataFileNmPath)) +1) )
set @LogFileNmPath = REVERSE( RIGHT(REVERSE(@logfilenmpath), LEN(@logfilenmpath) - charindex('\', REVERSE(@logfilenmpath)) +1) )



-- Backup the database to be sanitized
EXECUTE @RC = [RewardsNOW].[dbo].[spBackupDBLiteSpeed] @DBName, @BackupPathFilenm, @CompressionLvl


--
-- Now restore the database to one named prl<database name>
-- This will be the database that gets sanitized
set @WithMoveDataFile = 'MOVE ' + CHAR(39) + @DataFileNm + CHAR(39) + ' to ' + 
				    CHAR(39) + @DataFileNmPath + @DBName_Purell + '.mdf' + CHAR(39) 


set @WithMovelogFile =  'MOVE ' + CHAR(39) + @logFileNm  + CHAR(39) + ' to ' + 
				    CHAR(39) + @logFileNmPath + @DBName_Purell + '_log.ldf' + CHAR(39)


-- Restore the database to be sanitized to a different name
exec master.dbo.xp_restore_database 
    @database = @DBName_Purell,
    @filename = @backuppathfilenm,
    @filenumber = 1,
    @with = N'REPLACE',
    @with = N'STATS = 10',

    @with = @WithMoveDataFile,
    @with = @withMoveLogFile,

    @affinity = 0,
    @logging = 0








