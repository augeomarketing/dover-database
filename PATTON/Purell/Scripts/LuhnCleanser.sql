set nocount on

declare @tablenm		   varchar(250)
declare @columnnm		   varchar(250)

declare @sql			   nvarchar(4000)

declare @luhncount		   int
declare @rowcount		   int

declare @threshold		   decimal(4,3) = 0.600

declare csrLuhnCheck cursor FAST_FORWARD for
    select quotename(tb.name) TableNm, quotename(cl.name) Columnnm
    from sys.tables tb join sys.columns cl
	   on tb.object_id = cl.object_id

    join sys.types st
	   on st.system_type_id = cl.system_type_id

    where tb.type = 'U'		  -- User tables only
    and cl.max_length >= 16	  -- make sure length of column is at least 16
    and st.name like '%char%'	  -- data types of char, nchar, varchar, nvarchar
    order by tb.name

open csrLuhnCheck

fetch next from csrLuhnCheck into @tablenm, @columnnm
while @@FETCH_STATUS = 0
BEGIN
    print replicate('=', 100)
    print '**Checking table: ' + @tablenm + ', Column: ' + @columnnm
    print replicate('=', 100)


    set @rowcount = 0
    set @sql = 'set @rowcount = (select count(*) from dbo.' + @tablenm + ')'
    exec sp_executesql @sql, N'@rowcount int output', @rowcount = @rowcount OUTPUT


    set @luhncount = 0
    set @sql = 'set @luhncount = (SELECT sum( [RewardsNOW].[dbo].[fnCheckLuhn10] (' + @columnnm + ') )
							 from dbo.' + @tablenm + 
						    ' where isnumeric(' + @columnnm + ') = 1 and
							 len(' + @columnnm + ') = 16 and 
			 				 RewardsNOW.dbo.fnCheckLuhn10 (' + @columnnm + ') = 1 )'

    exec sp_executesql @sql, N'@luhncount int output', @luhncount = @luhncount OUTPUT     


    if @luhncount <> 0 and ((@luhncount * 1.000) / (@rowcount * 1.000)) > @threshold
    BEGIN -- a table with PAN has been identified.  now we need to scrub it
	   print '	Table: ' + @tablenm + ', Column: ' + @columnnm + ' has ' + cast(@luhncount as varchar(10)) + ' cards.'

	   
	   set @sql = 'set nocount on; 
				update tbl set ' + @columnnm + ' = xref.dim_pancrossreference_crossreferencenumber
                    from dbo.' + @tablenm + ' tbl join purell.dbo.pancrossreference xref
				    on tbl.' + @columnnm + ' = xref.sid_pancrossreference_pan;
				declare @pan varchar(50), @xref varchar(50), @validpan varchar(1); declare csr cursor fast_forward for select distinct' + @columnnm + ' from dbo.' + @tablenm + ' with (nolock); open csr;
				fetch next from csr into @pan; while @@fetch_status = 0 BEGIN  exec purell.dbo.sptranslatePantoXref @pan, @xref output, @validpan output; 
				update dbo.' + @tablenm + '  set ' + @columnnm + ' = @xref where ' + @columnnm + ' = @pan and @validpan = ''Y'';
				fetch next from csr into @pan END
				close csr; deallocate csr;'
	   exec sp_executesql @sql, N'@columnnm varchar(250), @Tablenm varchar(250)', @columnnm = @columnnm, @tablenm = @tablenm


    END

    print 'DONE with table: ' + @tablenm + ', Column: ' + @columnnm
    print ' '
    print ' '

    fetch next from csrLuhnCheck into @tablenm, @columnnm
END

close csrLuhnCheck
deallocate csrLuhnCheck


