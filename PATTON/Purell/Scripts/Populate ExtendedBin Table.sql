-----------------------------------------------------------------------------------------
--  Script to populate the dbo.ExtendedBin table.  All other tables use identity       --
--  values.                                                                            --
-----------------------------------------------------------------------------------------

use purell
GO


delete from dbo.extendedbin


insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('A', '7')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('J', '2')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('C', '6')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('V', '3')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('E', '8')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('F', '4')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('Q', '1')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('H', '5')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('K', '0')
insert into dbo.extendedbin (sid_extendedbin_id, dim_extendedbin_code) values('M', '9')
