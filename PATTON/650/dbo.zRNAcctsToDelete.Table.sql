SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zRNAcctsToDelete](
	[misc1] [varchar](20) NULL,
	[status] [char](1) NULL,
	[DateAdded] [datetime] NULL,
	[Tipnumber] [varchar](15) NOT NULL,
	[RewardsNow_DDA] [varchar](20) NULL,
	[RunAvailable] [int] NULL,
	[Runbalance] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastName] [varchar](40) NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
