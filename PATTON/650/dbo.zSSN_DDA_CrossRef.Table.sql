SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zSSN_DDA_CrossRef](
	[Pan] [varchar](16) NULL,
	[NameOnCard] [varchar](50) NULL,
	[MemberNum] [varchar](20) NULL,
	[SSN] [varchar](9) NULL,
	[Old_DDA] [varchar](20) NULL,
	[New_DDA] [varchar](20) NULL,
	[Tipnumber] [nvarchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
