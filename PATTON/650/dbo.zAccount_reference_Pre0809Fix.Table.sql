SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zAccount_reference_Pre0809Fix](
	[Tipnumber] [nchar](15) NOT NULL,
	[acctnumber] [nchar](25) NOT NULL,
	[TipFirst] [nchar](3) NOT NULL,
	[DateAdded] [datetime] NULL
) ON [PRIMARY]
GO
