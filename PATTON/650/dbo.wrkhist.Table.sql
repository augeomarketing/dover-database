SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkhist](
	[tipnumber] [varchar](15) NOT NULL,
	[trancode] [varchar](2) NULL,
	[points] [decimal](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
