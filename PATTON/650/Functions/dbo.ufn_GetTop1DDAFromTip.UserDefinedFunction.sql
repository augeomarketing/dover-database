SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
create function [dbo].[ufn_GetTop1DDAFromTip]
	(@TipNumber		varchar(15))
returns varchar(25)
as
BEGIN
	declare @DDA varchar(15)
	set @dda = (select top 1 dda
			  from dbo.tip_dda_reference
			  where tipnumber = @tipnumber
			  order by lastupdated desc)
	return @dda
END
GO
