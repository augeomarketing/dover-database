SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zTip_dda_reference_0809_orig](
	[Tipnumber] [nchar](15) NOT NULL,
	[DDA] [nchar](25) NOT NULL,
	[LastUpdated] [datetime] NULL
) ON [PRIMARY]
GO
