SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[transumbonus_summary](
	[SumOfPoints] [int] NULL,
	[MonthEndDate] [char](10) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
