USE [650Genisys]
GO
/****** Object:  Table [dbo].[Pointsfile]    Script Date: 11/19/2014 13:33:37 ******/
ALTER TABLE [dbo].[Pointsfile] DROP CONSTRAINT [DF_Pointsfile_PointsToExpire]
GO
ALTER TABLE [dbo].[Pointsfile] DROP CONSTRAINT [DF_Pointsfile_PointsBonusMN]
GO
DROP TABLE [dbo].[Pointsfile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pointsfile](
	[DDA] [char](25) NULL,
	[Tipnumber] [char](15) NULL,
	[PreviousPoints] [numeric](18, 0) NULL,
	[PointsEarnedDB] [numeric](18, 0) NULL,
	[PointsEarnedCR] [numeric](18, 0) NULL,
	[BonusPoints] [numeric](18, 0) NULL,
	[PointsAdjusted] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[TotalPoints] [numeric](18, 0) NULL,
	[PointsToExpire] [int] NULL,
	[ExpirationDate] [varchar](10) NULL,
	[PointsBonusMN] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Pointsfile] ADD  CONSTRAINT [DF_Pointsfile_PointsToExpire]  DEFAULT ((0)) FOR [PointsToExpire]
GO
ALTER TABLE [dbo].[Pointsfile] ADD  CONSTRAINT [DF_Pointsfile_PointsBonusMN]  DEFAULT ((0)) FOR [PointsBonusMN]
GO
