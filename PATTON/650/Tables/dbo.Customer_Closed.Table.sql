SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nvarchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nvarchar](6) NOT NULL
) ON [PRIMARY]
GO
