SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PointAdjustmentFile](
	[Type] [nchar](2) NULL,
	[Points] [int] NULL,
	[Tip] [nchar](15) NULL,
	[Pan] [nchar](25) NULL,
	[Name1] [nchar](40) NULL,
	[Name2] [nchar](40) NULL,
	[Address] [nchar](40) NULL,
	[CityStateZip] [nchar](50) NULL,
	[Phone] [nchar](10) NULL
) ON [PRIMARY]
GO
