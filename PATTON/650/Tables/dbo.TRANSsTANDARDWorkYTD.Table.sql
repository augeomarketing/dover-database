USE [650Genisys]
GO
/****** Object:  Table [dbo].[TRANSsTANDARDWorkYTD]    Script Date: 06/11/2013 13:03:06 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__TRANSsTAN__AmtTo__3DE82FB7]') AND parent_object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARDWorkYTD]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TRANSsTAN__AmtTo__3DE82FB7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TRANSsTANDARDWorkYTD] DROP CONSTRAINT [DF__TRANSsTAN__AmtTo__3DE82FB7]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__TRANSsTAN__Overa__3EDC53F0]') AND parent_object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARDWorkYTD]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TRANSsTAN__Overa__3EDC53F0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TRANSsTANDARDWorkYTD] DROP CONSTRAINT [DF__TRANSsTAN__Overa__3EDC53F0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARDWorkYTD]') AND type in (N'U'))
DROP TABLE [dbo].[TRANSsTANDARDWorkYTD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARDWorkYTD]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TRANSsTANDARDWorkYTD](
	[TFNO] [nvarchar](15) NULL,
	[TRANDATE] [nvarchar](10) NULL,
	[ACCT_NUM] [nvarchar](25) NULL,
	[TRANCODE] [nvarchar](2) NULL,
	[TRANNUM] [nvarchar](4) NULL,
	[TRANAMT] [nchar](15) NULL,
	[TRANTYPE] [nvarchar](20) NULL,
	[RATIO] [nvarchar](4) NULL,
	[CRDACTVLDT] [nvarchar](10) NULL,
	[AmtToPost] [decimal](18, 0) NULL,
	[Overage] [decimal](18, 0) NULL
) ON [PRIMARY]
END
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__TRANSsTAN__AmtTo__3DE82FB7]') AND parent_object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARDWorkYTD]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TRANSsTAN__AmtTo__3DE82FB7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TRANSsTANDARDWorkYTD] ADD  DEFAULT ('0') FOR [AmtToPost]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__TRANSsTAN__Overa__3EDC53F0]') AND parent_object_id = OBJECT_ID(N'[dbo].[TRANSsTANDARDWorkYTD]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TRANSsTAN__Overa__3EDC53F0]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[TRANSsTANDARDWorkYTD] ADD  DEFAULT ('0') FOR [Overage]
END


End
GO
