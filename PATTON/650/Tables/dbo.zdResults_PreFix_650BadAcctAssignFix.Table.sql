SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zdResults_PreFix_650BadAcctAssignFix](
	[Card] [nvarchar](50) NULL,
	[Column 1] [nvarchar](50) NULL,
	[Tip1] [nvarchar](50) NULL,
	[Tip2] [nvarchar](50) NULL,
	[Column 4] [nvarchar](50) NULL
) ON [PRIMARY]
GO
