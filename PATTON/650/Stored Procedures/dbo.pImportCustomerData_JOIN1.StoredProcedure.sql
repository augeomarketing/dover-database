SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pImportCustomerData_JOIN1] @TipFirst char(3)
AS 
declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20)


--trim the values
Update CustIn set 
Tipnumber =ltrim(rtrim(tipnumber)) ,
Acct_Num=ltrim(rtrim(Acct_Num))

/*
declare CUSTIN_crsr cursor
for select ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, 
		TIPNUMBER, ADDRESS1, ADDRESS2, ADDRESS4, CITY, STATE, ZIP, LASTNAME, HOMEPHONE, WORKPHONE, 
		DATEADDED, MISC1, MISC2, MISC3
from CUSTIN
order by tipnumber, status desc
/*                                                                            */
open CUSTIN_crsr
fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, 
	@NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, 
		@LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
*/
-----
--
-----
	-- Do CUSTOMER Updates
	update cus
		set AcctName1=NAMEACCT1, 
			AcctName2=NAMEACCT2, 
			AcctName3=NAMEACCT3, 
			AcctName4=NAMEACCT4, 
			AcctName5=NAMEACCT5, 
			AcctName6=NAMEACCT6, 
			Address1=ci.Address1, 
			Address2=ci.Address2, 
			Address4=ci.Address4, 
			City=rtrim(ci.City), 
			State=rtrim(ci.STATE), 
			Zipcode=rtrim(ci.zip), 
			HomePhone=ci.HomePhone, 
			WorkPhone=ci.WorkPhone, 
			lastname=ci.lastname, 
			misc1 = ci.misc1,
			misc2=ci.misc2, 
			misc3=ci.misc3
	from dbo.custin ci join dbo.customer cus
		on ci.tipnumber = cus.tipnumber

	-- Do CUSTOMER Inserts
	insert into dbo.Customer
	(TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, 
	 AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, 
	 Status, HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3)
	select ci.tipnumber, 0, 0, 0, left(ci.tipnumber,3), right(ci.tipnumber,12), nameacct1, nameacct2, 
		nameacct3, nameacct4, nameacct5, nameacct6, ci.address1, ci.address2, ci.address4, ci.city, ci.state, ci.zip,
		ci.status, ci.homephone, ci.workphone, ci.dateadded, ci.lastname, ci.misc1, ci.misc2, ci.misc3
	from dbo.custin ci left outer join dbo.customer cus
		on ci.tipnumber = cus.tipnumber
	left outer join dbo.affiliat aff
		on ci.tipnumber = aff.tipnumber
	where cus.tipnumber is null
	and aff.tipnumber is null


	-- Do AFFILIAT Updates
	update aff
		set lastname=ci.lastname, 
			acctstatus=ci.status
	from dbo.custin ci join dbo.affiliat aff
		on ci.acct_num = aff.acctid
			
	-- Do AFFILIAT Inserts
	INSERT INTO dbo.affiliat
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	select ci.ACCT_NUM, ci.tipnumber, 'Debit', cast(ci.dateadded as datetime), 'A', 'Debit Card', ci.lastname, 0
	from dbo.custin ci left outer join dbo.affiliat aff
		on ci.acct_num = aff.acctid
	where aff.acctid is null


update customer
set StatusDescription=(select statusdescription from status where status=customer.status)


-----
--
-----
/*
	--added addition criteria per Rob that ALSO checks that there are no matching cards for this cursor step in affiliat
	if (not exists (select tipnumber from customer where tipnumber=@tipnumber) and @tipnumber is not null )  AND (not exists (select acctid from affiliat where acctid= @ACCT_NUM) and @ACCT_NUM is not null )
		begin
			-- Add to CUSTOMER TABLE
			INSERT INTO Customer (TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, Status, HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3)
			values(@tipnumber, '0', '0', '0', LEFT(@tipnumber,3), right(@tipnumber,12), @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @ADDRESS1, @ADDRESS2, @ADDRESS4, rtrim(@City), rtrim(@state), rtrim(@zip), @STATUS, @HomePhone, @WorkPhone, cast(@DateAdded as datetime), @LastName, @Misc1, @Misc2, @Misc3) 
		end
	else
		begin
			-- Update CUSTOMER TABLE
			update customer
			set AcctName1=@NAMEACCT1, AcctName2=@NAMEACCT2, AcctName3=@NAMEACCT3, AcctName4=@NAMEACCT4, AcctName5=@NAMEACCT5, AcctName6=@NAMEACCT6, Address1=@Address1, Address2=@Address2, Address4=@Address4, City=rtrim(@City), State=rtrim(@STATE), Zipcode=rtrim(@zip), HomePhone=@HomePhone, WorkPhone=@WorkPhone, lastname=@lastname, misc2=@misc2, misc3=@misc3
			where tipnumber=@tipnumber 
		end


	if (not exists (select acctid from affiliat where acctid= @ACCT_NUM) and @ACCT_NUM is not null )
		begin
			-- Add to AFFILIAT TABLE
			INSERT INTO affiliat (ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
			values(@ACCT_NUM, @tipnumber, 'Debit', cast(@DateAdded as datetime), 'A', 'Debit Card', @lastname, '0') 
		end
	else
		begin
			-- Update AFFILIAT TABLE
			update AFFILIAT	
			set lastname=@lastname, acctstatus=@status 
			where acctid=@acct_num
		End
	
goto Next_Record
Next_Record:
		fetch CUSTIN_crsr into @ACCT_NUM, @NAMEACCT1, @NAMEACCT2, @NAMEACCT3, @NAMEACCT4, @NAMEACCT5, @NAMEACCT6, @STATUS, @TIPNUMBER, @ADDRESS1, @ADDRESS2, @ADDRESS4, @CITY, @STATE, @ZIP, @LASTNAME, @HOMEPHONE, @WORKPHONE, @DATEADDED, @MISC1, @MISC2, @MISC3
end
Fetch_Error:
close  CUSTIN_crsr
deallocate  CUSTIN_crsr
*/
GO
