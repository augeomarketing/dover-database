USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImport]    Script Date: 10/14/2014 10:55:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pSetupTransactionDataForImport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pSetupTransactionDataForImport]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImport]    Script Date: 10/14/2014 10:55:33 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)

AS 

TRUNCATE TABLE	transwork
TRUNCATE TABLE	transstandard

SELECT	*
INTO	#tempTrans
FROM	Rewardsnow.dbo.vw_650_TRAN_SOURCE_1	WITH (nolock)
WHERE	Trandate <= CAST(@EndDate as DATE)

INSERT INTO	transwork	(TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, NUMBEROFTRANS, TERMID, ACCEPTORID)
SELECT		TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMTTRAN, SIC, NETID, 0, 1, TERMID, ACCEPTORID 
FROM		#tempTrans
WHERE		sic <> '6011' 
	AND		processingcode in ('02500', '001000','00500','000000', '002000', '003000', 
								'200000', '200020', '200030', '200040', 
								'500000', '500010', '500020', '503000') 
	AND		(left(msgtype,2) in ('02', '04')) 

DROP TABLE	#temptrans

UPDATE	TRANSWORK
SET		TIPNUMBER = afs.TIPNUMBER
FROM	TRANSWORK tw join AFFILIAT afs on tw.PAN = afs.ACCTID

DELETE FROM TRANSWORK WHERE TIPNUMBER is null

-- Signature Debit
UPDATE	transwork
SET		points = ROUND(((amounttran/100)/2), 10, 0)
WHERE	netid in ('MCI', 'VNT') 
	AND	PAN NOT LIKE ('517326%')
-- PIN Debit
UPDATE	transwork
SET		points='0'
WHERE	netid not in('MCI', 'VNT')
	AND	PAN NOT LIKE ('517326%') 
-- CALCULATE CREDIT POINT EARNINGS (1/$1)
UPDATE	transwork
SET		POINTS = ROUND(((amounttran/100)), 10, 0)
WHERE	PAN LIKE ('517326%')


INSERT INTO TransStandard	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' 
FROM	transwork
WHERE	processingcode in ('000000', '001000', '002000', '500000', '500020', '500010') 
	AND	LEFT(msgtype,2) in ('02')
	AND	PAN NOT LIKE ('517326%')			        	
GROUP BY	tipnumber, Pan

INSERT INTO TransStandard	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' 
FROM	transwork
WHERE	(processingcode in ('200020', '200040') or left(msgtype,2) in ('04'))
	AND	PAN NOT LIKE ('517326%') 
GROUP BY	tipnumber, Pan

INSERT INTO TransStandard	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' 
FROM	transwork
WHERE	processingcode in ('00500', '000000', '001000', '003000', '500000', '503000') 
	AND	LEFT(msgtype,2) in ('02')
	AND	PAN LIKE ('517326%')			        	
GROUP BY	tipnumber, Pan

INSERT INTO TransStandard	(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
SELECT	tipnumber, @enddate, Pan, '33', sum(NumberOfTrans), sum(points), 'CREDIT', '-1', ' ' 
FROM	transwork
WHERE	(processingcode in ('02500','200000','20030') or left(msgtype,2) in ('04'))
	AND	PAN LIKE ('517326%') 
GROUP BY	tipnumber, Pan


GO


