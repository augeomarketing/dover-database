USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[spLoadPointsFile]    Script Date: 03/09/2015 13:55:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spLoadPointsFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spLoadPointsFile]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[spLoadPointsFile]    Script Date: 03/09/2015 13:55:45 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



/*********************************************************/
/* S Blanchette                                          */
/* 11/10                                                 */
/* SEB001                                                */
/* add pointsexpired element to calculation              */
/*********************************************************/

/*********************************************************/
/* S Blanchette                                          */
/* 11/10                                                 */
/* SEB002                                                */
/* Reference the projection file for expired points      */
/*********************************************************/

/*********************************************************/
/* S Blanchette                                          */
/* 11/11   11/21/2011                                              */
/* SEB003                                                */
/* Add logic for Merchant Funding to its own field       */
/*********************************************************/

--SEB 4/24/2014  New points expiration process

--SEB 11/2014 Seperate field for debit and credit earning.

CREATE PROCEDURE [dbo].[spLoadPointsFile] @Enddate char(10)

AS  

DECLARE	@tmp	TABLE	(tipnumber varchar(15), p1 int, p2 int, p3 int)

Truncate table PointsFile

declare @PointExpDate varchar(10)
select distinct @PointExpDate =  convert(varchar(10),cast(DateOfExpire as datetime),101) from EXPIRINGPOINTS 




insert into pointsfile (DDA, Tipnumber)
values (@enddate, @PointExpDate)






-- new for 08/09 processing- delete tip_dda_reference records where we don't see the dda in current month's demographics
--delete Tip_DDA_Reference where DDA not in (select [prim dda] from demographicIn)
-------------

/* seb003 insert into Pointsfile (DDA, Tipnumber, PreviousPoints, PointsEarned, BonusPoints, PointsAdjusted, PointsRedeemed, TotalPoints) */
insert into Pointsfile (DDA, Tipnumber, PreviousPoints, PointsEarnedDB, PointsEarnedCR, BonusPoints, PointsAdjusted, PointsRedeemed, TotalPoints, PointsBonusMN) /* SEB003 */
	select rtrim(DDA), a.Tipnumber, '0', '0', '0', '0', '0', '0', '0', '0' 
	from	Tip_DDA_Reference a 
--		JOIN 
--			(Select tipnumber, MIN(len(dda)) as dda_length from Tip_DDA_Reference group by Tipnumber) b 
--		on	LEN(a.dda) = b.dda_length and a.Tipnumber = b.Tipnumber




	
update Pointsfile
set PreviousPoints=b.PointsBegin
	, PointsEarnedDB=(b.PointsPurchasedDB - b.PointsReturnedDB)
	, PointsEarnedCR=(b.PointsPurchasedCR - b.PointsReturnedCR)
	, BonusPoints=( b.PointsBonusCR + b.PointsBonusDB ) 
	, PointsAdjusted=(b.PointsAdded - (b.PointsSubtracted + b.PointsExpired) ) /* SEB001 */
	, PointsRedeemed= b.PointsRedeemed
	, TotalPoints= b.PointsEnd	
	, PointsBonusMN=isnull(b.PointsBonusMN,0) /*SEB003 */
from PointsFile a, Monthly_Statement_File b
where a.tipnumber = b.tipnumber


--update the PointsToExpire and the ExpirationDate Fields in the Pointsfile with Expiringoints values
/* SEB002   update P set 
	p.PointsToExpire=isnull(e.pointsToExpire,0),
	p.ExpirationDate=convert(varchar(10),cast(e.DateOfExpire as datetime),101) 
	from Pointsfile P
		left outer join expiringPoints e on p.Tipnumber =e.tipnumber  */

-- SEB 04/24/2014
--exec RewardsNOW.dbo.usp_ExpirePoints '650',3 

INSERT INTO	@tmp
EXEC	RewardsNow.dbo.usp_ExpirePoints_report_multipleinterval '650', 3, 1

/* start seb002  */		
UPDATE	P	
SET		p.PointsToExpire=isnull(t.p3,0),
		p.ExpirationDate=convert(varchar(10),e.expiration_date) 
FROM	Pointsfile p
	LEFT OUTER JOIN
		RewardsNOW.dbo.RNIExpirationProjection e ON p.Tipnumber =e.Tipnumber
	LEFT OUTER JOIN
		@tmp t ON p.Tipnumber = t.tipnumber	
/* end seb002 */


--null out the default value of 0 in the first record		
update pointsfile set PointsToExpire = null where DDA=@EndDate		



		
		
		



GO


