SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
/* Script to get tipnumbers from account reference table and delete records with null tipnumber  */
CREATE PROCEDURE [dbo].[spGetTipnumbers]
AS
Update PointAdjustmentfile
set tip=b.tipnumber
from PointAdjustmentfile a, Affiliat b
where a.pan=b.acctid

delete from PointAdjustmentfile
where tip is null
GO
