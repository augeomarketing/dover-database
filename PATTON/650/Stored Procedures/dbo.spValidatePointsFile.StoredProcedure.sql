USE [650Genisys]
GO
/****** Object:  StoredProcedure [dbo].[spValidatePointsFile]    Script Date: 11/19/2014 13:42:51 ******/
DROP PROCEDURE [dbo].[spValidatePointsFile]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidatePointsFile] 
AS  


truncate table Pointsfile_validate

select Pointsfile.* into #tmp from Pointsfile
where  ISDATE(dda)<>1

--create table #t ([fld] [numeric](18, 0) NULL)

insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where PreviousPoints is null

insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where PointsEarnedDB is null
insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where PointsEarnedCR is null
insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where BonusPoints is null
insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where PointsAdjusted is null
insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where PointsRedeemed is null
insert into Pointsfile_validate(fld)
select COUNT(*) from #tmp where TotalPoints is null

select COUNT(*) from Pointsfile_validate  where fld>0


drop table #tmp
GO
