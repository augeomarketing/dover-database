USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pImportCustomerData_JOIN]    Script Date: 11/20/2014 11:48:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pImportCustomerData_JOIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pImportCustomerData_JOIN]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pImportCustomerData_JOIN]    Script Date: 11/20/2014 11:48:30 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[pImportCustomerData_JOIN] @TipFirst char(3)
AS 

-- SEB001 4/19/12 add code to make sure that Monica's name is first tip 650000000120766

declare @ACCT_NUM nvarchar(25), @NAMEACCT1 nvarchar(40), @NAMEACCT2 nvarchar(40), @NAMEACCT3 nvarchar(40), @NAMEACCT4 nvarchar(40), @NAMEACCT5 nvarchar(40), @NAMEACCT6	nvarchar(40), @STATUS nvarchar(1), @TIPNUMBER nvarchar(15), @ADDRESS1 nvarchar(40), @ADDRESS2 nvarchar(40), @ADDRESS4 nvarchar(40), @CITY nvarchar(38), @STATE char(2), @ZIP nvarchar(15), @LASTNAME nvarchar(40), @HOMEPHONE nvarchar(10), @WORKPHONE nvarchar(10), @DATEADDED nvarchar(10), @MISC1 nvarchar(20), @MISC2 nvarchar(20), @MISC3 nvarchar(20)


--trim the values
Update CustIn set 
Tipnumber =ltrim(rtrim(tipnumber)) ,
Acct_Num=ltrim(rtrim(Acct_Num))


-----
--
-----
	-- Do CUSTOMER Updates
	update cus
		set AcctName1=NAMEACCT1, 
			AcctName2=NAMEACCT2, 
			AcctName3=NAMEACCT3, 
			AcctName4=NAMEACCT4, 
			AcctName5=NAMEACCT5, 
			AcctName6=NAMEACCT6, 
			Address1=ci.Address1, 
			Address2=ci.Address2, 
			Address4=ci.Address4, 
			City=rtrim(ci.City), 
			State=rtrim(ci.STATE), 
			Zipcode=rtrim(ci.zip), 
			HomePhone=ci.HomePhone, 
			WorkPhone=ci.WorkPhone, 
			lastname=ci.lastname, 
			misc1 = ci.misc1,
			misc2=ci.misc2, 
			misc3=ci.misc3
	from dbo.custin ci join dbo.customer cus
		on ci.tipnumber = cus.tipnumber
------------------------------------
--customer Inserts

create table #cus
    (tipnumber          varchar(15) primary key,
     nameacct1          varchar(40),
     nameacct2          varchar(40),
     nameacct3          varchar(40),
     nameacct4          varchar(40),
     nameacct5          varchar(40),
     nameacct6          varchar(40),
     address1           varchar(40),
     address2           varchar(40),
     address4           varchar(40),
     city               varchar(38),
     State        varchar(2),
     zip                varchar(15),
     status       varchar(1),
     homephone          varchar(10),
     workphone          varchar(10),
     dateadded          datetime,
      lastname          varchar(40),
     misc1        varchar(20),
     misc2        varchar(20),
     misc3        varchar(20) )
----     
-- Insert rows into temp table that are new customers ONLY (without any Affiliat records).  Only put in distinct tipnumber 
-- to prevent duplicate rows
insert into #cus
(tipnumber)
select distinct ci.tipnumber
from dbo.custin ci left outer join dbo.customer cus
      on ci.tipnumber = cus.tipnumber
left outer join dbo.affiliat aff
      on ci.tipnumber = aff.tipnumber
where cus.tipnumber is null
and aff.tipnumber is null
and ci.TIPNUMBER is not null

-- Now that distinct tip #s are in the temp table, backfill the table with the rest of the data
update c
    set nameacct1 = ci.nameacct1,
         nameacct2 = ci.nameacct2,
     nameacct3		=ci.nameacct3,
     nameacct4		=ci.nameacct4,
     nameacct5		=ci.nameacct5,
     nameacct6		=ci.nameacct6,
     address1		=ci.address1,
     address2		=ci.address2,
     address4       =ci.address4,
     city			=ci.city,
     State			=ci.State,
     zip			=ci.zip,
     status			=ci.status,
     homephone		=ci.homephone,
     workphone		=ci.workphone,
     dateadded      =ci.dateadded,
     lastname		=ci.lastname,
     misc1			=ci.misc1,
     misc2			=ci.misc2,
     misc3			=ci.misc3 

from #cus c join dbo.custin ci
    on c.tipnumber = ci.tipnumber


-- Now, insert into customer
insert into dbo.customer
(TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, 
 AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address4, City, State, Zipcode, 
 Status, HomePhone, WorkPhone, DateAdded, Lastname, Misc1, Misc2, Misc3)
select TIPNumber, 0, 0, 0, left(tipnumber,3), right(tipnumber,12), nameacct1, nameacct2, 
		nameacct3, nameacct4, nameacct5, nameacct6, address1, address2, address4, city, state, zip,
		status, homephone, workphone, dateadded, lastname, misc1, misc2, misc3
from #cus
drop table #cus
----------------------------------------------------------------------

	-- Do AFFILIAT Updates
	update aff
		set lastname=ci.lastname, 
			acctstatus=ci.status
	from dbo.custin ci join dbo.affiliat aff
		on ci.acct_num = aff.acctid
			
	-- Do AFFILIAT Inserts
	INSERT INTO	dbo.affiliat
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	SELECT	ci.ACCT_NUM, ci.tipnumber, 'Debit', cast(ci.dateadded as datetime), 'A', 'Debit Card', ci.lastname, 0
	FROM	dbo.custin ci left outer join dbo.affiliat aff
		ON	ci.acct_num = aff.acctid
	WHERE	aff.acctid is null
		AND	ci.TIPNUMBER is not null
		AND ci.ACCT_NUM not like '517326%'

	INSERT INTO	dbo.affiliat
		(ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned)
	SELECT	ci.ACCT_NUM, ci.tipnumber, 'Credit', cast(ci.dateadded as datetime), 'A', 'Credit Card', ci.lastname, 0
	FROM	dbo.custin ci left outer join dbo.affiliat aff
		ON	ci.acct_num = aff.acctid
	WHERE	aff.acctid is null
		AND	ci.TIPNUMBER is not null
		AND ci.ACCT_NUM like '517326%'

update customer
set StatusDescription=(select statusdescription from status where status=customer.status)

/******************/
/* Start SEB001 ***/
/******************/
update customer
set ACCTNAME1='MONICA CONSTANTINO'
	,ACCTNAME2='JAMES CONSTANTINO'
WHERE TIPNUMBER=(RewardsNow.dbo.[ufn_GetCurrentTip] ('650000000120766'))
/******************/
/* End SEB001   ***/
/******************/

-----
--
-----


GO


