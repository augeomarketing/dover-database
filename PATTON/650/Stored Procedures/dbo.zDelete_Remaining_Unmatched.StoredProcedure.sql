SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zDelete_Remaining_Unmatched]

AS
BEGIN
--open a cursor and loop through, deleting all of these accounts
--select * from affiliatDeleted
declare @Tipnumber varchar(15), @DateDeleted datetime
set @DateDeleted='07/15/2009'  --hardcoded value to look up in the future

declare csr cursor
for 
select tipnumber from zRNAcctsToDelete

open csr
fetch csr into @Tipnumber
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin

--history
INSERT INTO historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, @DateDeleted 
	FROM History where tipnumber=@TipNumber

delete from history
where tipnumber=@TipNumber

--affiliat
INSERT INTO AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, Datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, AcctID, AcctStatus, AcctTypeDesc, lastname, YTDEARNED, CustID, @DateDeleted
	FROM affiliat where Tipnumber=@TipNumber
	
delete from affiliat
where Tipnumber=@TipNumber

--Customer
INSERT INTO CustomerDeleted
       	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @DateDeleted 
	FROM Customer where Tipnumber=@TipNumber

delete from customer 
where Tipnumber=@TipNumber	



goto Next_Record
Next_Record:
		fetch csr into @Tipnumber
end
Fetch_Error:
close  csr
deallocate  csr



END
GO
