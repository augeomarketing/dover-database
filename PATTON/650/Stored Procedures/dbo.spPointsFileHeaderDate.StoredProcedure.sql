USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[spPointsFileHeaderDate]    Script Date: 11/24/2014 16:02:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPointsFileHeaderDate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPointsFileHeaderDate]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[spPointsFileHeaderDate]    Script Date: 11/24/2014 16:02:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:                            S Blanchette       
-- Create date: 6/2011
-- Description:   Project date for header record
--modified 11/24/14 BL - Changed criteria in update statement
-- =============================================
CREATE PROCEDURE [dbo].[spPointsFileHeaderDate]
                -- Add the parameters for the stored procedure here
AS
BEGIN
                SET NOCOUNT ON;

                DECLARE              @ProjectedDate varchar(15)

                SET                         @ProjectedDate = (Select distinct convert(varchar(10), expiration_date, 101) from RewardsNow.dbo.RNIExpirationProjection where tipnumber like '650%')

                -- was updating multiple values instead of just the header row because DDA value with 4 chars had ISDATE evaluating to true 
                --UPDATE            pointsfile 
                --SET                      tipnumber = @ProjectedDate 
                --WHERE              ISDATE (dda) <> 0
                
                UPDATE pointsfile 
                SET    tipnumber = @ProjectedDate 
                WHERE len(rtrim(Ltrim(TIPNUMBER))) <> 15
                
END   

GO


