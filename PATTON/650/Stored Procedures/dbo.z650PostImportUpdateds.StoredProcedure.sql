SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[z650PostImportUpdateds] AS

--1. Get rid of all of the old unmatchable xref recs
Delete zSSN_DDA_CrossRef where old_dda='NONE' OR new_dda='NONE'
---------------------------------------------------------
--1.5  --select * from zSSN_DDA_CrossRef
--------------------------------------------------------

--2)	Update all 634 customers'  SSN and DDA values to those in the X-Ref table.
UPDATE C 
set C.Misc1 =z.New_DDA,
C.Misc2 =z.SSN  
FROM  zSSN_DDA_CrossRef z
join Customer C on
z.Old_DDA = C.Misc1
where C.tipnumber >650000000100000

-------------------------------------------------------------------------
--Try and update any customers who still don't have a Misc2 value (ssn)
--using the LatestValidation table 
UPDATE C 
set C.Misc1 =z.New_DDA,
C.Misc2 =z.SSN  
FROM  zLatestValidation z
join Customer C on
z.RewardsNow_DDA = C.Misc1
where C.tipnumber >650000000100000
and c.Misc2 is null
------------------------------------
--4)insert remaining unmatched records (no SSN value in Misc2) into zAcctsToDelete
Truncate table zRNAcctsToDelete

Insert into zRNAcctsToDelete
select misc1,status,DateAdded,Tipnumber,Misc1 as RewardsNow_DDA, RunAvailable,  Runbalance,  RunRedeemed, LastName, acctname1,acctname2 
from customer 
	where Tipnumber >650000000100000	and Misc2 is null
-----------------------------------------------------
GO
