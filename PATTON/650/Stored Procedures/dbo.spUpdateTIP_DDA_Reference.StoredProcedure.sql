USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateTIP_DDA_Reference]    Script Date: 11/21/2014 10:54:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spUpdateTIP_DDA_Reference]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spUpdateTIP_DDA_Reference]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateTIP_DDA_Reference]    Script Date: 11/21/2014 10:54:15 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


/* This loads new DDAs into the Tip_DDA_Reference table.                      */
/*                                                                         */
CREATE PROCEDURE [dbo].[spUpdateTIP_DDA_Reference]
AS  

--update the Tip_DDA_Reference table with the current DDA coming in 
/*
UPDATE T set T.Tipnumber =D.tipnumber 
FROM  DemographicIn D
join Tip_DDA_Reference T on
T.DDA = D.[PRIM DDA]
*/

insert into tip_DDA_reference(DDA, Tipnumber)
select distinct [Prim DDA], tipnumber
from demographicin
where  [Prim DDA] not in (select dda from tip_DDA_reference) AND [Prim DDA] is not NULL

insert into tip_DDA_reference(DDA, Tipnumber)
select distinct [Prim DDA], tipnumber
from demographicin
where tipnumber not in (select tipnumber from tip_DDA_reference) AND [Prim DDA] is not NULL


/*
select count(*),tipnumber,dda from tip_dda_reference
group by tipnumber, dda
order by count(*) desc
*/
GO