USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbers_SSN_DDA]    Script Date: 11/20/2014 11:00:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pGetandGenerateTipNumbers_SSN_DDA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pGetandGenerateTipNumbers_SSN_DDA]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pGetandGenerateTipNumbers_SSN_DDA]    Script Date: 11/20/2014 11:00:50 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



CREATE PROCEDURE [dbo].[pGetandGenerateTipNumbers_SSN_DDA] @TipFirstParm varchar(3)
AS 

declare @LastTipUsed char(15)
declare  @PAN nchar(16), @SSN varchar(16), @PrimDDA nchar(16), @1stDDA nchar(16), @2ndDDA nchar(16), @3rdDDA nchar(16), @4thDDA nchar(16), @5thDDA nchar(16), @tipfirst nchar(3), @newtipnumber bigint, @tipnumber nchar(15), @UpdateTip nchar(15)
declare @ExceptionFlag char(1)
update demographicIn set pan=rtrim(ltrim(pan))
update demographicIn set [SSN]=null where len(rtrim(ltrim([SSN])))=0
update demographicIn set [Prim DDA]=null where len(rtrim(ltrim([Prim DDA])))=0
update demographicIn set [1st DDA]=null where len(rtrim(ltrim([1st DDA])))=0
update demographicIn set [2nd DDA]=null where len(rtrim(ltrim([2nd DDA])))=0
update demographicIn set [3rd DDA]=null where len(rtrim(ltrim([3rd DDA])))=0
update demographicIn set [4th DDA]=null where len(rtrim(ltrim([4th DDA])))=0
update demographicIn set [5th DDA]=null where len(rtrim(ltrim([5th DDA])))=0

-- S Blanchette  9/2012
-- Added code to check the exception file to control house holding
-- Added code to insert new PrimDDA and SSN for existin tips

-- S Blanchette  6/2013
-- removed SSN from householding
    
--pass 2 (update by PrimDDA) 
-- S Blanchette 6/2013 strip off the right most 5 digits from DDA  

-- check for existing customers that used old logic ssn
	update di
	  set TipNumber = ar.tipnumber,
			tipfirst = @TipFirstParm
	from dbo.DemographicIn di join dbo.account_reference ar
	  on di.[SSN] = ar.acctnumber
	  where di.Tipnumber is null
		and di.[SSN] not in (
								select rtrim(dim_HouseHoldException_ExceptionValue) 
								from [COOPWork].dbo.HouseHoldException 
								where sid_HouseHoldException_TipFirst = @TipFirstParm 
								and sid_HouseHoldException_ExceptionType = 'SSN'
								and dim_HouseHoldException_active = 1
							)
		and ar.AcctType is null

-- check for existing customers that used old logic DDA
update di
	  set TipNumber = ar.tipnumber,
			tipfirst = @TipFirstParm
from dbo.DemographicIn di join dbo.account_reference ar
	  on di.[Prim DDA] = ar.acctnumber      
	  where di.Tipnumber is null and ar.AcctType is null

-- check under new logic   
update di
	  set TipNumber = ar.tipnumber,
			tipfirst = @TipFirstParm
from dbo.DemographicIn di join dbo.account_reference ar
	  on [Prim DDA]  = ar.acctnumber      
	  where di.Tipnumber is null and ar.AcctType = 'Member'
    
-- Capture new Member for existing tips
insert into Account_Reference (Tipnumber, acctnumber, TipFirst, AcctType)
select distinct tipnumber, [Prim DDA], @TipFirstParm, 'Member'
from DemographicIn
where TipNumber is not null and [Prim DDA] not in (select acctnumber from Account_Reference where AcctType = 'Member')

						
---------==============================================
--create a temp table based on Distinct SSNs and DDAs for recs still w/o tipnumber
select distinct [Prim DDA] as PrimDDA
into #tmpSSN_DDA
from DemographicIn 
where [Tipnumber] is null

declare csrSSN_DDA cursor FAST_FORWARD for
select PrimDDA from #tmpSSN_DDA
open csrSSN_DDA
------------------
	  exec rewardsnow.dbo.spGetLastTipNumberUsed @TipFirstParm, @LastTipUsed output
	  select @LastTipUsed as LastTipUsed
	  set @newtipnumber=@LastTipUsed
	  set @newtipnumber = cast(@newtipnumber as bigint) + 1  
	  --set the value of the first new tipnumber
---------------     



fetch next from csrSSN_DDA
	  into  @PrimDDA

while @@FETCH_STATUS = 0
BEGIN
	  -- Get new tip#

--	Begin
	  -- if PrimDDA are in account_reference, get a new tip and add both
		if NOT exists(select acctnumber from account_reference where acctnumber=@PrimDDA and AcctType = 'Member')
			begin
				set @newtipnumber = cast(@newtipnumber as bigint) + 1  
				set @UpdateTip=@newtipnumber
				--
				if @PrimDDA is not null
				insert into dbo.account_reference (Tipnumber, acctnumber, TipFirst, AcctType)
					values(@UpdateTip, @PrimDDA, @TipFirstParm, 'Member')					
			end

		  fetch next from csrSSN_DDA
		  into  @PrimDDA
END

close csrSSN_DDA
deallocate csrSSN_DDA

-- Update last Tip Number Used
exec RewardsNOW.dbo.spPutLastTipNumberUsed @TipFirstParm, @newtipnumber 
---===============================================

--re-update Demographic in to give tips to the NEW card numbers
update di
	  set TipNumber = ar.tipnumber
			, tipfirst = @TipFirstParm
from dbo.DemographicIn di join dbo.account_reference ar
	  on [Prim DDA] = ar.acctnumber
 where di.Tipnumber is null and ar.AcctType = 'Member'
-----------==================================================
GO


