SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBonusFileProcess] @DateIn varchar(10)
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO process the Bonus input file Step 2                          */
/*    This actually adds records to the History table and updates the balance */
/*                                                                            */
/******************************************************************************/
delete from transumbonus
insert into transumbonus (tipnumber, acctno, histdate, trancode, points)
select tip, pan, @datein, type, points	
from PointAdjustmentFile

update transumbonus
set ratio='1', description='Points Increase', numdebit='1', overage='0', amtdebit=points
where trancode in ('IE', 'BI')

update transumbonus
set ratio='-1', description='Points Decrease', numcredit='1', overage='0', amtcredit=points
where trancode in ('DE')

insert into history
select tipnumber, acctno, histdate, trancode, numdebit, amtdebit, description, ' ', ratio, overage
from transumbonus
where trancode in ('IE', 'BI') and amtdebit is not null

insert into history
select tipnumber, acctno, histdate, trancode, numcredit, amtcredit, description, ' ', ratio, overage
from transumbonus
where trancode in ('DE') and amtcredit is not null
/* old code-doesn't update Runavail and Runbal to the SUM of the amounts in the bonus table. For example..if a bonus was awarded twice because two new cards were opened the runbalance would only be updated to the value of ONE of the bonuses.
code below fixes this
update customer
set runavailable=runavailable + b.amtdebit, runbalance=runbalance + b.amtdebit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtdebit is not null
*/
update cus
set runavailable = runavailable + tsb.sum_amtdebit, runbalance = runbalance + tsb.sum_amtdebit
from dbo.customer cus join (select tipnumber, sum(amtdebit) as sum_amtdebit
from dbo.transumbonus 
group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber
/*ditto below
update customer
set runavailable=runavailable - b.amtcredit, runbalance=runbalance - b.amtcredit
from customer a, transumbonus b          
where a.tipnumber=b.tipnumber and b.amtcredit is not null
*/
update cus
set runavailable = runavailable - tsb.sum_amtcredit, runbalance = runbalance - tsb.sum_amtcredit
from dbo.customer cus join (select tipnumber, sum(amtcredit) as sum_amtcredit
from dbo.transumbonus 
group by tipnumber) tsb
on cus.tipnumber = tsb.tipnumber
GO
