USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pImportTransactionData_JOIN]    Script Date: 01/29/2015 07:56:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pImportTransactionData_JOIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pImportTransactionData_JOIN]
GO

USE [650Genisys]
GO

/****** Object:  StoredProcedure [dbo].[pImportTransactionData_JOIN]    Script Date: 01/29/2015 07:56:22 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[pImportTransactionData_JOIN] @TipFirst char(3), @StartDate char(10), @enddate char(10)
AS 

-- S BLANCHETTE  06/11/2013  ADDED CODE TO HANDLE CORRECT POINT EARNING CAP RULE

declare @TFNO nvarchar(15), @Trandate nchar(10), @ACCT_NUM nvarchar(25), @TRANCODE nchar(2), @TRANNUM nchar(4), @TRANAMT nchar(15), @TRANTYPE nchar(20), @RATIO nchar(4), @CRDACTVLDT nchar(10)  
declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)

	set @MaxPointsPerYear=(select MaxPointsPerYear from client where tipfirst=@TipFirst)
	If @MaxPointsPerYear='0'
		begin
		set @MaxPointsPerYear='999999999'
		end

	Truncate Table transstandardworkytd

	insert into transstandardworkytd (TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT, AmtToPost, Overage)
	select TFNO, TRANDATE, ACCT_NUM, TRANCODE, sum(cast(TRANNUM as int)), sum(cast(TRANAMT as int)), TRANTYPE, RATIO, CRDACTVLDT, 0, 0
	from TRANSsTANDARD
	group by TFNO, ACCT_NUM, TRANCODE, TRANDATE, TRANTYPE, RATIO, CRDACTVLDT
	order by TFNO asc, ACCT_NUM asc, TRANCODE asc

	/* RETURNS */
	update transstandardworkytd
	set AmtToPost =  tranamt, overage = 0
	where trancode IN ('33','37')

	update AFFILIAT
	set ytdearned = (YTDEarned - tranamt)
	from AFFILIAT join transstandardworkytd  on ACCTID  = acct_num 
	where trancode IN ('33','37')

	/* PURCHASES */
	update transstandardworkytd
	set AmtToPost = case 
						when (YTDEarned) >= @MaxPointsPerYear then 0
						when (YTDEarned + tranamt) <= @MaxPointsPerYear then tranamt
						when (YTDEarned + tranamt) > @MaxPointsPerYear then (@MaxPointsPerYear - YTDEarned)
						end
	from transstandardworkytd join AFFILIAT on acct_num = ACCTID
	where trancode in ('63','67')

	update transstandardworkytd
	set overage = (tranamt - AmtToPost)
	where trancode in ('63','67') 

	update AFFILIAT
	set YTDEarned = 
			CASE
				when (ytdearned) >= @MaxPointsPerYear then YTDEarned
				when (ytdearned + AmtToPost) <= @MaxPointsPerYear then (ytdearned + AmtToPost)
				when (ytdearned + AmtToPost) > @MaxPointsPerYear then YTDEarned
			end
	from AFFILIAT join transstandardworkytd on ACCTID = acct_num
	where trancode in ('63','67')

	insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
	select TFNO, ACCT_NUM, TRANDATE, TRANCODE, TRANNUM, AmtToPost, TRANTYPE, '', RATIO, Overage
	from transstandardworkytd

	select tfno, sum(AmtToPost * ratio) as amttopost
	into #tmp
	from transstandardworkytd
	group by tfno

	update customer
	set runavailable=runavailable + AmtToPost, runbalance=runbalance + AmtToPost
	from CUSTOMER join #tmp on TIPNUMBER = TFNO

	DROP TABLE #tmp


 /* EVERYTHING BELOW THIS LINE IS OLD CODE
--declare @enddate char(10)   --THESE TWO LINES FOR TESTING                                                                  
--set @EndDate ='07/31/2009'.

--get the TRANSSTANDARD Records

Select TFNO as tipnumber, Acct_Num as AcctID, @EndDate as HistDate, Trancode, TranNum as Trancount, tranamt as points, 'DEBIT' as Description, 
      case Trancode
            when '67' then 1
            when '37' then -1
      end ratio
into #P
from dbo.transstandard 
order by tipnumber 


create index ix_temp_p on #p (tipnumber)
create index ix_temp_p2 on #p (tipnumber, points, ratio)



insert Into dbo.History
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description,  Ratio )
Select TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, Description,  Ratio 
from #P



create table #tippoints
    (tipnumber		varchar(15) primary key,
     NewPoints		int)


insert into #tippoints
(tipnumber, newpoints)
select Tipnumber, Sum(cast(points as int) * ratio) 
from #P
group by tipnumber


UPDATE c set 
      c.Runavailable =c.Runavailable + #TipPoints.NewPoints,
      c.RunBalance =c.RunBalance + #TipPoints.NewPoints
from customer c Join #TipPoints on
C.Tipnumber=#TipPoints.TipNumber    


drop table #P
drop table #TipPoints
*/

GO


