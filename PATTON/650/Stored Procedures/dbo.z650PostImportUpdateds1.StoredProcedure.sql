SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[z650PostImportUpdateds1] AS

--1. Get rid of all of the old unmatchable xref recs
Delete zSSN_DDA_CrossRef where old_dda='NONE' OR new_dda='NONE'

--2a. update the xref.tipnumber based on customer misc1
UPDATE z set z.TIPNUMBER =c.tipnumber 
FROM  Customer c
join zSSN_DDA_CrossRef z on
z.old_DDA = C.Misc1
where c.Tipnumber>650000000100000

--2b. Clear and move unmatchable recs to _Unmatched table
truncate table zSSN_DDA_CrossRef_Unmatched

--if any xref recs remain unmatched, copy to the _Unmatched table
insert into zSSN_DDA_CrossRef_Unmatched
select * from zSSN_DDA_CrossRef where tipnumber is null

-- and delete the _unmatched from the x-ref table
delete zSSN_DDA_CrossRef where Tipnumber is null


--4. Update the customer misc2 field with the ssn
UPDATE c set c.Misc2 =z.ssn
FROM  Customer c
join zSSN_DDA_CrossRef z on
z.old_dda = C.misc1
where c.Tipnumber>650000000100000
GO
