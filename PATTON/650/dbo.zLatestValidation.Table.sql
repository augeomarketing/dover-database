SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zLatestValidation](
	[status] [nvarchar](1) NULL,
	[DateAdded] [datetime] NULL,
	[Tipnumber] [nvarchar](15) NULL,
	[RewardsNow_DDA] [nvarchar](20) NULL,
	[Runbalance] [int] NULL,
	[LastName] [nvarchar](40) NULL,
	[acctname1] [nvarchar](40) NULL,
	[acctname2] [nvarchar](40) NULL,
	[New_DDA] [nvarchar](20) NULL,
	[Reason] [varchar](255) NULL,
	[SSN] [varchar](11) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
