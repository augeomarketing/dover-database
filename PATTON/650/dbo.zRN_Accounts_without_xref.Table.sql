SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zRN_Accounts_without_xref](
	[status] [varchar](1) NULL,
	[DateAdded] [datetime] NULL,
	[Tipnumber] [varchar](15) NULL,
	[RewardsNow_DDA] [varchar](20) NULL,
	[Runbalance] [int] NULL,
	[LastName] [varchar](40) NULL,
	[acctname1] [varchar](40) NULL,
	[acctname2] [varchar](40) NULL,
	[New_DDA] [varchar](20) NULL,
	[Reason] [nvarchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
