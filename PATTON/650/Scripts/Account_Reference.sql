/*
   Thursday, June 27, 20132:14:09 PM
   User: 
   Server: doolittle\rn
   Database: 650Genisys
   Application: 
*/
use [650Genisys]
go

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account_Reference ADD
	AcctType varchar(10) NULL
GO
ALTER TABLE dbo.Account_Reference SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
