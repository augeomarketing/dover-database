/*
   Wednesday, October 24, 20121:32:36 PM
   User: 
   Server: doolittle\rn
   Database: 625CPM_FCU
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account_Reference ADD
	DateAdded date NOT NULL CONSTRAINT DF_Account_Reference_DateAdded DEFAULT getdate()
GO
ALTER TABLE dbo.Account_Reference SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
