SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pQuarterlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedcr numeric(9), @pointsbonuscr numeric(9), @pointspurchasedDB numeric(9), @pointsbonusdb numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedcr numeric(9), @pointsreturneddb numeric(9), @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9) 
delete from Quarterly_Audit_ErrorFile
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Quarterly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, pointsreturneddb, pointsdecreased 
from Quarterly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, @pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, @pointsreturneddb, @pointsdecreased
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Quarterly_Audit_ErrorFile
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, @pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, @pointsreturneddb, @pointsdecreased, @errmsg, @currentend)
		goto Next_Record
		end
		
goto Next_Record
Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, @pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, @pointsreturneddb, @pointsdecreased
		
	end
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
