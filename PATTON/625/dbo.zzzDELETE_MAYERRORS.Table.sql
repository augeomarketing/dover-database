SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzzDELETE_MAYERRORS](
	[Pan] [nvarchar](255) NULL,
	[LastSix] [nvarchar](255) NULL,
	[First] [nvarchar](255) NULL,
	[Last] [nvarchar](255) NULL,
	[Address #1] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[ST] [nvarchar](255) NULL,
	[Error Message] [nvarchar](255) NULL
) ON [PRIMARY]
GO
