SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 7/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* Changed logic to group by tip and pan */
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */
truncate table transwork
truncate table transstandard
update COOPWork.dbo.TransDetailHold
set COOPWork.dbo.TransDetailHold.tipnumber=affiliat.tipnumber
from COOPWork.dbo.TransDetailHold, affiliat
where COOPWork.dbo.TransDetailHold.pan=affiliat.acctid and COOPWork.dbo.TransDetailHold.tipnumber is null

insert into transwork( TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID)
select TRANDATE, MSGTYPE, PAN, PROCESSINGCODE, AMOUNTTRAN, SIC, NETID, POINTS, TIPNUMBER, NUMBEROFTRANS, TERMID, ACCEPTORID 
from COOPWork.dbo.TransDetailHold
where left(tipnumber,3)=@tipfirst and sic<>'6011' and processingcode in ('000000', '002000', '200020', '200040') and (left(msgtype,2) in ('02', '04')) and Trandate>=@StartDate and Trandate<=@Enddate

-- Signature Debit
update transwork
set points=ROUND(((amounttran/100)/2), 10, 0)
where netid in('MCI', 'VNT') 
-- PIN Debit
update transwork
set points='0'
where netid not in('MCI', 'VNT') 
--Put to standard transtaction file format purchases.
-- SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, '67', NumberOfTrans, points, 'DEBIT', '1', ' ' from transwork
--where processingcode in ('000000', '002000', '500000', '500020', '500010') 		        	
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' from transwork
/* SEB002 where processingcode in ('000000', '002000', '500000', '500020', '500010') and left(msgtype,2) in ('02', '04')) */		        	
/* SEB002 */ where processingcode in ('000000', '002000') and left(msgtype,2) in ('02') 		        	
group by tipnumber, Pan
	
--Put to standard transtaction file format returns.
-- SEB001
--INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
--select tipnumber, Trandate, Pan, '37', NumberOfTrans, points, 'DEBIT', '-1', ' ' from transwork
--where processingcode in ('200020', '200040')
-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '37', sum(NumberOfTrans), sum(points), 'DEBIT', '-1', ' ' from transwork
/* SEB002 where processingcode in ('200020', '200040') */
/* SEB002 */ where processingcode in ('200020', '200040') or left(msgtype,2) in ('04')
group by tipnumber, Pan
GO
