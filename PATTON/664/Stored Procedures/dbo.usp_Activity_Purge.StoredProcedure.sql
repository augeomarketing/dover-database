USE [664]
GO
/****** Object:  StoredProcedure [dbo].[usp_Activity_Purge]    Script Date: 12/07/2016 14:39:39 ******/
DROP PROCEDURE [dbo].[usp_Activity_Purge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_Activity_Purge] 
	-- Add the parameters for the stored procedure here
	@MonthEnd date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select cs.TIPNUMBER
	into #tmp
	from CUSTOMER cs
	left outer join (select tipnumber, SUM(points*ratio) points from HISTORY where TRANCODE in ('33','63') group by TIPNUMBER)  hs
	on cs.TIPNUMBER = hs.TIPNUMBER
	where DATEDIFF(month, cs.dateadded,@monthend) =13
	and (hs.points <6000 or hs.TIPNUMBER is null) 

	update CUSTOMER
	set STATUS='C'
	where TIPNUMBER in (select TIPNUMBER from #tmp)

	insert into [rn1].rewardsnow.dbo.optoutrequest
	(dim_optoutrequest_tipnumber,	dim_optoutrequest_active,	dim_optoutrequest_requestor,	sid_action_id)
	select distinct TIPNUMBER, '1','5785','1'
	from #tmp

END
GO
