USE [272]
GO
/****** Object:  StoredProcedure [dbo].[usp_HOLIDAY_Generate_Tiered_Points]    Script Date: 11/18/2015 08:41:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_HOLIDAY_Generate_Tiered_Points]
	-- Add the parameters for the stored procedure here
	@sid_dbprocessinfo_dbnumber varchar(3)
	, @MonthEndDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF OBJECT_ID(N'tempdb..#tmp') IS NOT NULL  
 	drop table #tmp

    declare 
    @sqlInsert NVARCHAR(MAX)
    , @dbname VARCHAR(50) = (SELECT dbnamepatton FROM RewardsNow.dbo.dbprocessinfo WHERE dbnumber = @sid_dbprocessinfo_dbnumber)  

	select distinct dim_RNITransaction_RNIId, 0 Spend, 0 MReturn, 0 NetTot
	into #tmp
	from RewardsNow.dbo.RNITransaction rnit With (NoLock) 
		join dbo.customer_stage css
			on rnit.dim_RNITransaction_RNIId = css.tipnumber
	where sid_dbprocessinfo_dbnumber=@sid_dbprocessinfo_dbnumber
	and dim_RNITransaction_RNIId is not null
	and css.status = 'A'

	update #tmp
	set Spend = isnull((select sum(dim_RNITransaction_PointsAwarded)
				from RewardsNow.dbo.RNITransaction With (NoLock) 
				where sid_dbprocessinfo_dbnumber=@sid_dbprocessinfo_dbnumber
				and dim_RNITransaction_RNIId = #tmp.dim_RNITransaction_RNIId
				and sid_trantype_trancode in ('65','67','BI')
				group by dim_RNITransaction_RNIId
				),0)
		,
		MReturn = isnull((select sum(dim_RNITransaction_PointsAwarded)
				from RewardsNow.dbo.RNITransaction With (NoLock) 
				where sid_dbprocessinfo_dbnumber=@sid_dbprocessinfo_dbnumber
				and dim_RNITransaction_RNIId = #tmp.dim_RNITransaction_RNIId
				and sid_trantype_trancode in ('35','37')
				group by dim_RNITransaction_RNIId
				),0)
				
	update #tmp
	set NetTot=spend - MReturn
				
	delete #tmp
	where NetTot<=0

	INSERT INTO dbo.[HISTORY_Stage]
			   ([TIPNUMBER],[ACCTID],[HISTDATE],[TRANCODE],[TranCount],[POINTS],[Description]
			   ,[SECID],[Ratio],[Overage]
			   )
	select dim_RNITransaction_RNIId, NULL, @MonthEndDate, '67', '1'
			, case
				when nettot>=1600000 then 3000
				when (nettot>1000000 and nettot<1600000) then (1500 + (round(((nettot-1000000) * .0025)/100,2)*100))
				when nettot<=1000000 then round(((nettot) * .0015)/100,2)*100
			  end  Points
			, 'Cash Award', 'NEW', '1', '0'
	from #tmp

END
GO
