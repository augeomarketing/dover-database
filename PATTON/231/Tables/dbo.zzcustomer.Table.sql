USE [231]
GO
/****** Object:  Table [dbo].[zzcustomer]    Script Date: 06/22/2010 13:49:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzcustomer](
	[tipnumber] [varchar](15) NOT NULL,
	[misc5] [varchar](20) NULL,
	[address1] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
