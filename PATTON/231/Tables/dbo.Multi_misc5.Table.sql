USE [231]
GO
/****** Object:  Table [dbo].[Multi_misc5]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Multi_misc5]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Multi_misc5](
	[cnt] [int] NULL,
	[misc5] [varchar](20) NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
