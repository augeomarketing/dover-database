USE [231]
GO
/****** Object:  Table [dbo].[welcomekitcred]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[welcomekitcred]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[welcomekitcred](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTNAME1] [varchar](40) NOT NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[DateAdded] [datetime] NULL,
	[Runavailable] [int] NOT NULL
) ON [PRIMARY]
GO
