USE [231]
GO
/****** Object:  Table [dbo].[temp_affil]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[temp_affil]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_affil](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[custid] [char](13) NULL
) ON [PRIMARY]
GO
