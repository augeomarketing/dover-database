USE [231]
GO
/****** Object:  Table [dbo].[MonthlyTotals]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[MonthlyTotals]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonthlyTotals](
	[FIELDNAME] [char](50) NULL,
	[TOTALPOINTS] [numeric](18, 0) NULL,
	[ITEM_COUNT] [int] NULL,
	[RUNDATE] [datetime] NULL,
	[POSTDATE] [nvarchar](10) NULL
) ON [PRIMARY]
GO
