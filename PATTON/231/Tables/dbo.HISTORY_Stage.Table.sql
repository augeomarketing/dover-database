USE [231]
GO
/****** Object:  Table [dbo].[HISTORY_Stage]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[HISTORY_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORY_Stage](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [decimal](18, 0) NULL,
	[Description] [varchar](255) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_HISTORY_Stage] ON [dbo].[HISTORY_Stage] 
(
	[ACCTID] ASC,
	[TIPNUMBER] ASC
)
INCLUDE ( [HISTDATE],
[TRANCODE],
[POINTS],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ix_historystage_histdate_trancode_includes] ON [dbo].[HISTORY_Stage] 
(
	[HISTDATE] ASC,
	[TRANCODE] ASC
)
INCLUDE ( [TIPNUMBER],
[ACCTID],
[POINTS],
[Ratio]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
