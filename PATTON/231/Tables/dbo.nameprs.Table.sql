USE [231]
GO
/****** Object:  Table [dbo].[nameprs]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[nameprs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nameprs](
	[TIPNUMBER] [nvarchar](15) NULL,
	[NAME1] [nvarchar](40) NULL,
	[FIRSTNAME] [nvarchar](40) NULL,
	[MIDDLENAME] [nvarchar](40) NULL,
	[LASTNAME] [nvarchar](40) NULL,
	[SUFFIX] [nvarchar](40) NULL,
	[ACCTNUM] [nvarchar](16) NULL,
	[MEMBERNUM] [nvarchar](9) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_nameprs_tipnumber_inc_firstname_lastname_middlename] ON [dbo].[nameprs] 
(
	[TIPNUMBER] ASC
)
INCLUDE ( [FIRSTNAME],
[LASTNAME],
[MIDDLENAME]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
