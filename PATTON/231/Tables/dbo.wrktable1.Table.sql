USE [231]
GO
/****** Object:  Table [dbo].[wrktable1]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[wrktable1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrktable1](
	[tipnumber] [varchar](15) NOT NULL,
	[runavailable] [int] NULL,
	[hstpnt] [float] NULL,
	[diff] [float] NULL
) ON [PRIMARY]
GO
