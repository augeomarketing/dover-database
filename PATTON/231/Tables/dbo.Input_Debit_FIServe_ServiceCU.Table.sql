USE [231]
GO
/****** Object:  Table [dbo].[Input_Debit_FIServe_ServiceCU]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Input_Debit_FIServe_ServiceCU]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Debit_FIServe_ServiceCU](
	[sid_Debit_Input_RecNum] [int] IDENTITY(1,1) NOT NULL,
	[dim_Debit_Input_Record_ID1] [numeric](1, 0) NOT NULL,
	[dim_Debit_Input_RecNum1] [numeric](6, 0) NULL,
	[dim_Debit_Input_MessageType] [numeric](4, 0) NULL,
	[dim_Debit_Input_AIIC] [varchar](8) NULL,
	[dim_Debit_Input_NetID] [varchar](6) NULL,
	[dim_Debit_Input_CardAcceptorTerminalID] [varchar](8) NULL,
	[dim_Debit_Input_RetrievalReferenceNumber] [varchar](6) NULL,
	[dim_Debit_Input_IssuerInstIDCode] [varchar](8) NULL,
	[dim_Debit_Input_Primary_Account_Number] [varchar](25) NULL,
	[dim_Debit_Input_Merchant_Type] [varchar](4) NULL,
	[dim_Debit_Input_Card_Acceptor_Bus_Code] [varchar](4) NULL,
	[dim_Debit_Input_Record_ID2] [numeric](1, 0) NULL,
	[dim_Debit_Input_RecNum2] [numeric](6, 0) NULL,
	[dim_Debit_Input_DateTime_Local_Trans] [datetime] NULL,
	[dim_Debit_Input_Network_Posting_Date] [numeric](4, 0) NULL,
	[dim_Debit_Input_Issuer_Posting_date] [numeric](6, 0) NULL,
	[dim_Debit_Input_Trans_Processing_Code] [varchar](6) NULL,
	[dim_Debit_Input_Debit_Credit_Flag] [varchar](2) NULL,
	[dim_Debit_Input_Amt_Reconciliation] [numeric](12, 2) NULL,
	[dim_Debit_Input_Conversion_Rate_Recon] [numeric](8, 0) NULL,
	[dim_Debit_Input_Transaction_Responce_code] [varchar](3) NULL,
	[dim_Debit_Input_Reversal_Reason_Code] [varchar](2) NULL,
	[dim_Debit_Input_Member_Number] [varchar](1) NULL,
	[dim_Debit_Input_Date_Capture] [numeric](4, 0) NULL,
	[dim_Debit_Input_Owner_Fee] [numeric](4, 0) NULL,
	[dim_Debit_Input_Action_Code] [varchar](3) NULL,
	[dim_Debit_Input_Function_Code] [varchar](3) NULL,
	[dim_Debit_Input_Media_Type] [varchar](1) NULL,
	[dim_Debit_Input_OverDispence_Flag] [varchar](1) NULL,
	[dim_Debit_Input_Reserved_Space1] [varchar](1) NULL,
	[dim_Debit_Input_Record_ID3] [numeric](1, 0) NULL,
	[dim_Debit_Input_RecNum3] [numeric](6, 0) NULL,
	[dim_Debit_Input_InterChangeFee] [varchar](4) NULL,
	[dim_Debit_Input_Card_Acceptor_Location] [varchar](40) NULL,
	[dim_Debit_Input_Card_Acceptor_ID_Code] [varchar](16) NULL,
	[dim_Debit_Input_Message_Reason_Code] [varchar](4) NULL,
	[dim_Debit_Input_System_Trace_Audit_Number] [numeric](6, 0) NULL,
	[dim_Debit_Input_Reserved_space2] [varchar](2) NULL,
	[dim_Debit_Input_Record_ID4] [numeric](1, 0) NULL,
	[dim_Debit_Input_RecNum4] [numeric](6, 0) NULL,
	[dim_Debit_Input_Acquirer_Posting_date] [numeric](6, 0) NULL,
	[dim_Debit_Input_Point_Of_Service_Code] [varchar](3) NULL,
	[dim_Debit_Input_Acquirer_Processing_Flag] [varchar](3) NULL,
	[dim_Debit_Input_Issuer_Processing_Flag] [varchar](3) NULL,
	[dim_Debit_Input_Currency_Code_Transaction] [varchar](3) NULL,
	[dim_Debit_Input_Point_Of_Service_Datat_Code] [varchar](12) NULL,
	[dim_Debit_Input_Amount_Transaction_Acquirer_Funds] [numeric](12, 0) NULL,
	[dim_Debit_Input_Amount_Transaction_Issuer_Funds] [numeric](12, 0) NULL,
	[dim_Debit_Input_Transaction_Processing_Code] [numeric](6, 0) NULL,
	[dim_Debit_Input_Surcharge_Issuer_Funds] [varchar](9) NULL,
	[dim_Debit_Input_Original_Message_Type] [numeric](4, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[sid_Debit_Input_RecNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
