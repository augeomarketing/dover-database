USE [231]
GO
/****** Object:  Table [dbo].[SCORECARDReduction]    Script Date: 08/17/2012 11:34:59 ******/
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_orddte]
GO
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_retdte]
GO
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_ordnum]
GO
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_itemnum]
GO
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_desc]
GO
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_qty]
GO
ALTER TABLE [dbo].[SCORECARDReduction] DROP CONSTRAINT [DF_SCORECARDReduction_points]
GO
DROP TABLE [dbo].[SCORECARDReduction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCORECARDReduction](
	[sid_SCORECARDReduction_AccountNumber] [varchar](16) NOT NULL,
	[dim_SCORECARDReduction_orddte] [varchar](8) NOT NULL,
	[dim_SCORECARDReduction_retdte] [varchar](8) NOT NULL,
	[dim_SCORECARDReduction_ordnum] [varchar](9) NOT NULL,
	[dim_SCORECARDReduction_itemnum] [varchar](8) NOT NULL,
	[dim_SCORECARDReduction_desc] [varchar](50) NOT NULL,
	[dim_SCORECARDReduction_qty] [int] NOT NULL,
	[dim_SCORECARDReduction_points] [int] NOT NULL,
	[dim_SCORECARDReduction_TranCode] [varchar](2) NULL,
	[dim_SCORECARDReduction_MemberNumber] [varchar](9) NULL,
	[dim_SCORECARDReduction_Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_orddte]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_orddte]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_retdte]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_retdte]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_ordnum]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_ordnum]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_itemnum]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_itemnum]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_desc]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_desc]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_qty]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_qty]
GO
ALTER TABLE [dbo].[SCORECARDReduction] ADD  CONSTRAINT [DF_SCORECARDReduction_points]  DEFAULT ((0)) FOR [dim_SCORECARDReduction_points]
GO
