USE [231]
GO
/****** Object:  Table [dbo].[Monthly_Statement_File]    Script Date: 08/17/2012 11:34:59 ******/
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBA]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBC]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBE]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBI]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBM]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBN]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFC]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFD]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFH]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFI]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFJ]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFK]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFL]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFM]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFP]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointFloor]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsToExpire]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] DROP CONSTRAINT [DF_Monthly_Statement_File_PointsExpired]
GO
DROP TABLE [dbo].[Monthly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[PointsBegin] [int] NULL,
	[PointsEnd] [int] NULL,
	[PointsPurchasedCR] [int] NOT NULL,
	[PointsPurchasedDB] [int] NOT NULL,
	[PointsBonus] [int] NOT NULL,
	[PointsAdded] [int] NOT NULL,
	[PointsIncreased] [int] NOT NULL,
	[PointsRedeemed] [int] NOT NULL,
	[PointsReturnedCR] [int] NOT NULL,
	[PointsReturnedDB] [int] NOT NULL,
	[PointsSubtracted] [int] NOT NULL,
	[PointsDecreased] [int] NOT NULL,
	[Acctid] [varchar](25) NULL,
	[PointsBonusBA] [int] NOT NULL,
	[PointsBonusBC] [int] NOT NULL,
	[PointsBonusBE] [int] NOT NULL,
	[PointsBonusBI] [int] NOT NULL,
	[PointsBonusBM] [int] NOT NULL,
	[PointsBonusBN] [int] NOT NULL,
	[PointsBonusBT] [int] NOT NULL,
	[PointsBonusBR] [int] NOT NULL,
	[PointsBonusFB] [int] NOT NULL,
	[PointsBonusFC] [int] NOT NULL,
	[PointsBonusFD] [int] NOT NULL,
	[PointsBonusFH] [int] NOT NULL,
	[PointsBonusFI] [int] NOT NULL,
	[PointsBonusFJ] [int] NOT NULL,
	[PointsBonusFK] [int] NOT NULL,
	[PointsBonusFL] [int] NOT NULL,
	[PointsBonusFM] [int] NOT NULL,
	[PointsBonusFP] [int] NOT NULL,
	[status] [char](1) NULL,
	[lastfour] [char](4) NULL,
	[PointFloor] [int] NOT NULL,
	[PointsToExpire] [int] NOT NULL,
	[DateOfExpiration] [varchar](25) NULL,
	[PointsExpired] [int] NOT NULL,
 CONSTRAINT [PK_Monthly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBegin]  DEFAULT ((0)) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsEnd]  DEFAULT ((0)) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedCR]  DEFAULT ((0)) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsPurchasedDB]  DEFAULT ((0)) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonus]  DEFAULT ((0)) FOR [PointsBonus]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsAdded]  DEFAULT ((0)) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsIncreased]  DEFAULT ((0)) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsRedeemed]  DEFAULT ((0)) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedCR]  DEFAULT ((0)) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsReturnedDB]  DEFAULT ((0)) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsSubtracted]  DEFAULT ((0)) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsDecreased]  DEFAULT ((0)) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBA]  DEFAULT ((0)) FOR [PointsBonusBA]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBC]  DEFAULT ((0)) FOR [PointsBonusBC]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBE]  DEFAULT ((0)) FOR [PointsBonusBE]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBI]  DEFAULT ((0)) FOR [PointsBonusBI]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBM]  DEFAULT ((0)) FOR [PointsBonusBM]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBN]  DEFAULT ((0)) FOR [PointsBonusBN]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBT]  DEFAULT ((0)) FOR [PointsBonusBT]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusBR]  DEFAULT ((0)) FOR [PointsBonusBR]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFB]  DEFAULT ((0)) FOR [PointsBonusFB]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFC]  DEFAULT ((0)) FOR [PointsBonusFC]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFD]  DEFAULT ((0)) FOR [PointsBonusFD]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFH]  DEFAULT ((0)) FOR [PointsBonusFH]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFI]  DEFAULT ((0)) FOR [PointsBonusFI]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFJ]  DEFAULT ((0)) FOR [PointsBonusFJ]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFK]  DEFAULT ((0)) FOR [PointsBonusFK]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFL]  DEFAULT ((0)) FOR [PointsBonusFL]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFM]  DEFAULT ((0)) FOR [PointsBonusFM]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsBonusFP]  DEFAULT ((0)) FOR [PointsBonusFP]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointFloor]  DEFAULT ((0)) FOR [PointFloor]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsToExpire]  DEFAULT ((0)) FOR [PointsToExpire]
GO
ALTER TABLE [dbo].[Monthly_Statement_File] ADD  CONSTRAINT [DF_Monthly_Statement_File_PointsExpired]  DEFAULT ((0)) FOR [PointsExpired]
GO
