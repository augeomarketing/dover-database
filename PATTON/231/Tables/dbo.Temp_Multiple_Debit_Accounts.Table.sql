USE [231]
GO
/****** Object:  Table [dbo].[Temp_Multiple_Debit_Accounts]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Temp_Multiple_Debit_Accounts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp_Multiple_Debit_Accounts](
	[numdebit] [int] NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
