USE [231]
GO
/****** Object:  Table [dbo].[ServiceCUInput]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[ServiceCUInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceCUInput](
	[sid_ServiceCUInput_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ServiceCUInput_AcctNum] [varchar](12) NULL,
	[dim_ServiceCUInput_AcctType] [varchar](2) NULL,
	[dim_ServiceCUInput_CardLastSix] [varchar](2) NULL,
	[dim_ServiceCUInput_AcctName1] [varchar](40) NULL,
	[dim_ServiceCUInput_AcctName2] [varchar](40) NULL,
	[dim_ServiceCUInput_AcctName3] [varchar](40) NULL,
	[dim_ServiceCUInput_Address1] [varchar](40) NULL,
	[dim_ServiceCUInput_Address2] [varchar](40) NULL,
	[dim_ServiceCUInput_Address3] [varchar](40) NULL,
	[dim_ServiceCUInput_City] [varchar](40) NULL,
	[dim_ServiceCUInput_State] [varchar](2) NULL,
	[dim_ServiceCUInput_Zip] [varchar](5) NULL,
	[dim_ServiceCUInput_ZipPlus4] [varchar](4) NULL,
	[dim_ServiceCUInput_ApoAddress1] [varchar](40) NULL,
	[dim_ServiceCUInput_ApoAddress2] [varchar](40) NULL,
	[dim_ServiceCUInput_ApoAddress3] [varchar](40) NULL,
	[dim_ServiceCUInput_ApoCity] [varchar](40) NULL,
	[dim_ServiceCUInput_ApoState] [varchar](2) NULL,
	[dim_ServiceCUInput_ApoZip] [varchar](5) NULL,
	[dim_ServiceCUInput_ApoZipPlus4] [varchar](4) NULL,
	[dim_ServiceCUInput_HomePhone] [varchar](10) NULL,
	[dim_ServiceCUInput_WorkPhone] [varchar](10) NULL,
	[dim_ServiceCUInput_AcctStatus] [varchar](1) NULL
) ON [PRIMARY]
GO
