USE [231]
GO
/****** Object:  Table [dbo].[phb_wrkMemberNumberErrors]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[phb_wrkMemberNumberErrors]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[phb_wrkMemberNumberErrors](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[acctname1] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[nbrdupes] [int] NULL,
 CONSTRAINT [PK_phb_wrkMemberNumberErrors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
