USE [231]
GO
/****** Object:  Table [dbo].[Temp_SCU_Debit]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Temp_SCU_Debit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp_SCU_Debit](
	[acctid] [varchar](16) NOT NULL,
	[Tipnumber] [nvarchar](15) NULL,
	[MemberNum] [nvarchar](9) NULL,
	[TransamtPur] [numeric](10, 2) NULL,
	[TranscntPur] [int] NULL,
	[TransamtRet] [numeric](10, 2) NULL,
	[TranscntRet] [int] NULL,
	[PostDate] [varchar](6) NULL
) ON [PRIMARY]
GO
