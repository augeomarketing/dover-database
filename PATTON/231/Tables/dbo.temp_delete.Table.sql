USE [231]
GO
/****** Object:  Table [dbo].[temp_delete]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[temp_delete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp_delete](
	[flag] [varchar](4) NOT NULL,
	[member] [varchar](9) NOT NULL
) ON [PRIMARY]
GO
