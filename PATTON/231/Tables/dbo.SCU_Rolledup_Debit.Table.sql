USE [231]
GO
/****** Object:  Table [dbo].[SCU_Rolledup_Debit]    Script Date: 08/17/2012 11:34:59 ******/
ALTER TABLE [dbo].[SCU_Rolledup_Debit] DROP CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_MEMBERNUM]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] DROP CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANAMTPUR]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] DROP CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANCNTPUR]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] DROP CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANAMTRET]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] DROP CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANCNTRET]
GO
DROP TABLE [dbo].[SCU_Rolledup_Debit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCU_Rolledup_Debit](
	[SID_SCU_Rolledup_Debit_ACCTID] [varchar](16) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TIPNUMBER] [nvarchar](15) NOT NULL,
	[DIM_SCU_Rolledup_Debit_MEMBERNUM] [nvarchar](9) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANAMTPUR] [numeric](9, 2) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANCNTPUR] [int] NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANAMTRET] [numeric](9, 2) NOT NULL,
	[DIM_SCU_Rolledup_Debit_TRANCNTRET] [int] NOT NULL,
	[DIM_SCU_Rolledup_Debit_POSTDATE] [date] NOT NULL,
 CONSTRAINT [PK_SCU_Rolledup_Debit] PRIMARY KEY CLUSTERED 
(
	[SID_SCU_Rolledup_Debit_ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] ADD  CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_MEMBERNUM]  DEFAULT ((0)) FOR [DIM_SCU_Rolledup_Debit_MEMBERNUM]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] ADD  CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANAMTPUR]  DEFAULT ((0)) FOR [DIM_SCU_Rolledup_Debit_TRANAMTPUR]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] ADD  CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANCNTPUR]  DEFAULT ((0)) FOR [DIM_SCU_Rolledup_Debit_TRANCNTPUR]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] ADD  CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANAMTRET]  DEFAULT ((0)) FOR [DIM_SCU_Rolledup_Debit_TRANAMTRET]
GO
ALTER TABLE [dbo].[SCU_Rolledup_Debit] ADD  CONSTRAINT [DF_Table_1_SCU_Rolledup_Debit_TRANCNTRET]  DEFAULT ((0)) FOR [DIM_SCU_Rolledup_Debit_TRANCNTRET]
GO
