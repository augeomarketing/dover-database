USE [231]
GO
/****** Object:  Table [dbo].[fixed_scorecardbycard]    Script Date: 08/17/2012 11:34:59 ******/
ALTER TABLE [dbo].[fixed_scorecardbycard] DROP CONSTRAINT [DF_fixed_scorecardbycard_points]
GO
DROP TABLE [dbo].[fixed_scorecardbycard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fixed_scorecardbycard](
	[acctid] [varchar](16) NULL,
	[ST] [varchar](2) NULL,
	[BALANCE] [varchar](7) NULL,
	[points] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[fixed_scorecardbycard] ADD  CONSTRAINT [DF_fixed_scorecardbycard_points]  DEFAULT ((0)) FOR [points]
GO
