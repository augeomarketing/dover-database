USE [231]
GO
/****** Object:  Table [dbo].[scu_involve]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[scu_involve]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scu_involve](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[Category] [varchar](25) NOT NULL,
	[CatData] [numeric](27, 6) NULL,
	[RunDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
