USE [231]
GO
/****** Object:  Table [dbo].[Input_Customer_wk]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Input_Customer_wk]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Input_Customer_wk](
	[SID_INPUTCUSTOMER_MEMBERNUMBER] [nvarchar](9) NULL,
	[TIPNUMBER] [nvarchar](15) NULL
) ON [PRIMARY]
GO
