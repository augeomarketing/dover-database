USE [231]
GO
/****** Object:  Table [dbo].[ScoreCardReds]    Script Date: 08/17/2012 11:34:59 ******/
ALTER TABLE [dbo].[ScoreCardReds] DROP CONSTRAINT [DF_ScoreCardReds_BegBalance]
GO
ALTER TABLE [dbo].[ScoreCardReds] DROP CONSTRAINT [DF_ScoreCardReds_CurrentEarned]
GO
ALTER TABLE [dbo].[ScoreCardReds] DROP CONSTRAINT [DF_ScoreCardReds_CurrentAdjusted]
GO
ALTER TABLE [dbo].[ScoreCardReds] DROP CONSTRAINT [DF_ScoreCardReds_CurrentRedeemed]
GO
ALTER TABLE [dbo].[ScoreCardReds] DROP CONSTRAINT [DF_ScoreCardReds_CurrentExpired]
GO
ALTER TABLE [dbo].[ScoreCardReds] DROP CONSTRAINT [DF_ScoreCardReds_CurrentAvailable]
GO
DROP TABLE [dbo].[ScoreCardReds]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScoreCardReds](
	[sid_scorecardreds_AccountNumber] [varchar](16) NOT NULL,
	[dim_scorecardreds_ST] [char](1) NOT NULL,
	[dim_scorecardreds_PT] [char](1) NOT NULL,
	[dim_scorecardreds_BegBalance] [int] NOT NULL,
	[dim_scorecardreds_CurrentEarned] [int] NOT NULL,
	[dim_scorecardreds_CurrentAdjusted] [int] NOT NULL,
	[dim_scorecardreds_CurrentRedeemed] [int] NOT NULL,
	[dim_scorecardreds_CurrentExpired] [int] NOT NULL,
	[dim_scorecardreds_CurrentAvailable] [int] NOT NULL,
	[dim_scorecardreds_MemberNumber] [varchar](9) NOT NULL,
	[dim_scorecardreds_TIPNUMBER] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScoreCardReds] ADD  CONSTRAINT [DF_ScoreCardReds_BegBalance]  DEFAULT ((0)) FOR [dim_scorecardreds_BegBalance]
GO
ALTER TABLE [dbo].[ScoreCardReds] ADD  CONSTRAINT [DF_ScoreCardReds_CurrentEarned]  DEFAULT ((0)) FOR [dim_scorecardreds_CurrentEarned]
GO
ALTER TABLE [dbo].[ScoreCardReds] ADD  CONSTRAINT [DF_ScoreCardReds_CurrentAdjusted]  DEFAULT ((0)) FOR [dim_scorecardreds_CurrentAdjusted]
GO
ALTER TABLE [dbo].[ScoreCardReds] ADD  CONSTRAINT [DF_ScoreCardReds_CurrentRedeemed]  DEFAULT ((0)) FOR [dim_scorecardreds_CurrentRedeemed]
GO
ALTER TABLE [dbo].[ScoreCardReds] ADD  CONSTRAINT [DF_ScoreCardReds_CurrentExpired]  DEFAULT ((0)) FOR [dim_scorecardreds_CurrentExpired]
GO
ALTER TABLE [dbo].[ScoreCardReds] ADD  CONSTRAINT [DF_ScoreCardReds_CurrentAvailable]  DEFAULT ((0)) FOR [dim_scorecardreds_CurrentAvailable]
GO
