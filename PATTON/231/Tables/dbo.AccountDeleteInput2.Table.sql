USE [231]
GO
/****** Object:  Table [dbo].[AccountDeleteInput2]    Script Date: 08/17/2012 11:34:58 ******/
DROP TABLE [dbo].[AccountDeleteInput2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDeleteInput2](
	[acctid] [char](25) NOT NULL,
	[dda] [char](25) NULL
) ON [PRIMARY]
GO
