USE [231]
GO
/****** Object:  Table [dbo].[DebitInput_char]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[DebitInput_char]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DebitInput_char](
	[sid_DebitInput_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_DebitInput_Record_ID1] [varchar](1) NOT NULL,
	[dim_DebitInput_RecNum1] [varchar](6) NULL,
	[dim_DebitInput_MessageType] [varchar](4) NULL,
	[dim_DebitInput_AIIC] [varchar](8) NULL,
	[dim_DebitInput_NetID] [varchar](6) NULL,
	[dim_DebitInput_CardAcceptorTerminalID] [varchar](8) NULL,
	[dim_DebitInput_RetrievalReferenceNumber] [varchar](6) NULL,
	[dim_DebitInput_IssuerInstIDCode] [varchar](8) NULL,
	[dim_DebitInput_Primary_Account_Number] [varchar](25) NULL,
	[dim_DebitInput_Merchant_Type] [varchar](4) NULL,
	[dim_DebitInput_Card_Acceptor_Bus_Code] [varchar](4) NULL,
	[dim_DebitInput_Record_ID2] [varchar](1) NULL,
	[dim_DebitInput_RecNum2] [varchar](6) NULL,
	[dim_DebitInput_DateTime_Local_Trans] [varchar](12) NULL,
	[dim_DebitInput_Network_Posting_Date] [varchar](6) NULL,
	[dim_DebitInput_Issuer_Posting_date] [varchar](6) NULL,
	[dim_DebitInput_Trans_Processing_Code] [varchar](6) NULL,
	[dim_DebitInput_Debit_Credit_Flag] [varchar](2) NULL,
	[dim_DebitInput_Amt_Reconciliation] [varchar](10) NULL,
	[dim_DebitInput_Conversion_Rate_Recon] [varchar](8) NULL,
	[dim_DebitInput_Transaction_Responce_code] [varchar](3) NULL,
	[dim_DebitInput_Reversal_Reason_Code] [varchar](2) NULL,
	[dim_DebitInput_Member_Number] [varchar](1) NULL,
	[dim_DebitInput_Date_Capture] [varchar](4) NULL,
	[dim_DebitInput_Owner_Fee] [varchar](4) NULL,
	[dim_DebitInput_Action_Code] [varchar](3) NULL,
	[dim_DebitInput_Function_Code] [varchar](3) NULL,
	[dim_DebitInput_Media_Type] [varchar](1) NULL,
	[dim_DebitInput_OverDispence_Flag] [varchar](1) NULL,
	[dim_DebitInput_Reserved_Space1] [varchar](1) NULL,
	[dim_DebitInput_Record_ID3] [varchar](1) NULL,
	[dim_DebitInput_RecNum3] [varchar](6) NULL,
	[dim_DebitInput_InterChangeFee] [varchar](4) NULL,
	[dim_DebitInput_Card_Acceptor_Location] [varchar](40) NULL,
	[dim_DebitInput_Card_Acceptor_ID_Code] [varchar](16) NULL,
	[dim_DebitInput_Message_Reason_Code] [varchar](4) NULL,
	[dim_DebitInput_System_Trace_Audit_Number] [varchar](6) NULL,
	[dim_DebitInput_Reserved_space2] [varchar](3) NULL,
	[dim_DebitInput_Record_ID4] [varchar](1) NULL,
	[dim_DebitInput_RecNum4] [varchar](6) NULL,
	[dim_DebitInput_Acquirer_Posting_date] [varchar](6) NULL,
	[dim_DebitInput_Point_Of_Service_Code] [varchar](3) NULL,
	[dim_DebitInput_Acquirer_Processing_Flag] [varchar](3) NULL,
	[dim_DebitInput_Issuer_Processing_Flag] [varchar](3) NULL,
	[dim_DebitInput_Currency_Code_Transaction] [varchar](3) NULL,
	[dim_DebitInput_Point_Of_Service_Datat_Code] [varchar](12) NULL,
	[dim_DebitInput_Amount_Transaction_Acquirer_Funds] [varchar](12) NULL,
	[dim_DebitInput_Amount_Transaction_Issuer_Funds] [varchar](12) NULL,
	[dim_DebitInput_Transaction_Processing_Code] [varchar](6) NULL,
	[dim_DebitInput_Surcharge_Issuer_Funds] [varchar](9) NULL,
	[dim_DebitInput_Original_Message_Type] [varchar](4) NULL,
 CONSTRAINT [PK_DebitInput_char] PRIMARY KEY CLUSTERED 
(
	[sid_DebitInput_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_dic_primary_inc_sid] ON [dbo].[DebitInput_char] 
(
	[dim_DebitInput_Primary_Account_Number] ASC
)
INCLUDE ( [sid_DebitInput_id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
