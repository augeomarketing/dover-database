USE [231]
GO
/****** Object:  Table [dbo].[ZZSCTEMP]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[ZZSCTEMP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZSCTEMP](
	[TIPNUMBER] [varchar](15) NULL,
	[tdpoints] [int] NOT NULL,
	[HistPts] [int] NOT NULL,
	[tppoints] [int] NOT NULL,
	[POINTSREDEEMED] [int] NOT NULL,
	[pointsremaining] [int] NOT NULL
) ON [PRIMARY]
GO
