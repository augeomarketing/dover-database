USE [231]
GO
/****** Object:  Table [dbo].[SCUCusts86]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[SCUCusts86]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCUCusts86](
	[Account] [varchar](6) NULL,
	[Ignore001] [varchar](5) NULL,
	[CardNumber] [varchar](16) NULL,
	[Ignore002] [varchar](4) NULL,
	[Type] [varchar](1) NULL,
	[Ignore003] [varchar](4) NULL,
	[Status] [varchar](1) NULL,
	[Ignore004] [varchar](6) NULL,
	[AcctName           ] [varchar](31) NULL,
	[ADDRESS1] [varchar](31) NULL,
	[ADDRESS2] [varchar](31) NULL,
	[CITY] [varchar](19) NULL,
	[StateCd] [varchar](2) NULL,
	[Ignore005] [varchar](5) NULL,
	[PostalCode] [varchar](10) NULL,
	[Ignore006] [varchar](2) NULL,
	[PHONE] [varchar](13) NULL,
	[Ignore007] [varchar](5) NULL
) ON [PRIMARY]
GO
