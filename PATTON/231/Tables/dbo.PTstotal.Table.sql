USE [231]
GO
/****** Object:  Table [dbo].[PTstotal]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[PTstotal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PTstotal](
	[tipnumber] [varchar](15) NOT NULL,
	[totpts] [float] NULL
) ON [PRIMARY]
GO
