USE [231]
GO
/****** Object:  Table [dbo].[tptotal]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[tptotal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tptotal](
	[tipnumber] [varchar](15) NOT NULL,
	[tp] [float] NULL
) ON [PRIMARY]
GO
