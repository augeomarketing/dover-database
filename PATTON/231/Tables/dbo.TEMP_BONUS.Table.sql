USE [231]
GO
/****** Object:  Table [dbo].[TEMP_BONUS]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[TEMP_BONUS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEMP_BONUS](
	[tipnumber] [nchar](15) NULL,
	[acctid] [nchar](20) NULL,
	[Points] [int] NULL
) ON [PRIMARY]
GO
