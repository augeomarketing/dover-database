USE [231]
GO
/****** Object:  Table [dbo].[CustomerPurge]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[CustomerPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerPurge](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[datedeleted] [date] NOT NULL,
 CONSTRAINT [PK_CustomerPurge] PRIMARY KEY CLUSTERED 
(
	[tipnumber] ASC,
	[acctid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
