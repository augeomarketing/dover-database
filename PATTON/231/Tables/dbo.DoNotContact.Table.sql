USE [231]
GO
/****** Object:  Table [dbo].[DoNotContact]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[DoNotContact]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DoNotContact](
	[sid_DoNotContact_Membernumber] [varchar](9) NULL,
	[dim_DoNotContact_TipNumber] [varchar](15) NULL,
	[dim_DoNotContact_Name] [varchar](40) NULL,
	[dim_DoNotContact_Address1] [varchar](40) NULL,
	[dim_DoNotContact_Address2] [varchar](40) NULL,
	[dim_DoNotContact_Address3] [nvarchar](40) NULL,
	[dim_DoNotContact_City] [nvarchar](40) NULL,
	[dim_DoNotContact_State] [varchar](2) NULL,
	[dim_DoNotContact_zipcode] [nvarchar](10) NULL,
	[dim_DoNotContact_MailCode] [varchar](3) NULL,
	[dim_DoNotContact_SSN] [nvarchar](9) NULL,
	[dim_DoNotContact_DateAdded] [date] NULL
) ON [PRIMARY]
GO
