USE [231]
GO
/****** Object:  Table [dbo].[wrkPurge_APO_20120630]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[wrkPurge_APO_20120630]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkPurge_APO_20120630](
	[tipnumber] [varchar](15) NOT NULL,
	[acctname1] [varchar](40) NOT NULL,
	[acctname2] [varchar](40) NULL,
	[acctname3] [varchar](40) NULL,
	[address1] [varchar](40) NULL,
	[address2] [varchar](40) NULL,
	[city] [varchar](40) NULL,
	[state] [varchar](2) NULL,
	[zipcode] [varchar](15) NULL,
	[runavailable] [int] NULL,
	[maxhistdate] [datetime] NULL
) ON [PRIMARY]
GO
