USE [231]
GO
/****** Object:  Table [dbo].[PendingPurge]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[PendingPurge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PendingPurge](
	[ACCTID] [char](25) NOT NULL,
	[tipnumber] [varchar](15) NOT NULL
) ON [PRIMARY]
GO
