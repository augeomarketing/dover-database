USE [231]
GO
/****** Object:  Table [dbo].[zzonly_one_acct]    Script Date: 06/22/2010 13:49:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zzonly_one_acct](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
