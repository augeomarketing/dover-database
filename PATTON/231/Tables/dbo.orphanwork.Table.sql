USE [231]
GO
/****** Object:  Table [dbo].[orphanwork]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[orphanwork]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orphanwork](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[histdate] [datetime] NULL,
	[trancode] [varchar](2) NULL,
	[trancount] [int] NULL,
	[points] [decimal](18, 0) NULL,
	[description] [varchar](50) NULL,
	[secid] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
