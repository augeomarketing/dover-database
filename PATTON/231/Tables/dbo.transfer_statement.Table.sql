USE [231]
GO
/****** Object:  Table [dbo].[transfer_statement]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[transfer_statement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transfer_statement](
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TRANCODE] [varchar](2) NULL,
	[POINTS] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
