USE [231]
GO
/****** Object:  Table [dbo].[Temp_Multiple_Debit_Accounts_2]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Temp_Multiple_Debit_Accounts_2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp_Multiple_Debit_Accounts_2](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NOT NULL,
	[dateadded] [datetime] NOT NULL,
	[membernumber] [char](13) NULL
) ON [PRIMARY]
GO
