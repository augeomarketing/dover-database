USE [231]
GO
/****** Object:  Table [dbo].[no_match_scorecard]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[no_match_scorecard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[no_match_scorecard](
	[sid_scorecardreds_AccountNumber] [varchar](16) NOT NULL,
	[dim_scorecardreds_ST] [char](1) NOT NULL,
	[dim_scorecardreds_PT] [char](1) NOT NULL,
	[dim_scorecardreds_BegBalance] [int] NOT NULL,
	[dim_scorecardreds_CurrentEarned] [int] NOT NULL,
	[dim_scorecardreds_CurrentAdjusted] [int] NOT NULL,
	[dim_scorecardreds_CurrentRedeemed] [int] NOT NULL,
	[dim_scorecardreds_CurrentExpired] [int] NOT NULL,
	[dim_scorecardreds_CurrentAvailable] [int] NOT NULL,
	[dim_scorecardreds_AnticipatedToExpire] [int] NOT NULL,
	[dim_scorecardreds_MemberNumber] [varchar](9) NULL,
	[dim_scorecardreds_Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
