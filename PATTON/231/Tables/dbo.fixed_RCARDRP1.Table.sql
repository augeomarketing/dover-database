USE [231]
GO
/****** Object:  Table [dbo].[fixed_RCARDRP1]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[fixed_RCARDRP1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fixed_RCARDRP1](
	[ACCOUNT] [varchar](255) NULL,
	[OLD-CARD-NO] [varchar](255) NULL,
	[NAME] [varchar](255) NULL,
	[ADD-1] [varchar](255) NULL,
	[ADD-2] [varchar](255) NULL,
	[CITY] [varchar](255) NULL,
	[ST] [varchar](255) NULL,
	[ZIP] [varchar](255) NULL,
	[PHONE] [varchar](255) NULL
) ON [PRIMARY]
GO
