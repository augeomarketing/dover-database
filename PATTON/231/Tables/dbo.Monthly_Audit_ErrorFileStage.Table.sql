USE [231]
GO
/****** Object:  Table [dbo].[Monthly_Audit_ErrorFileStage]    Script Date: 08/17/2012 11:34:59 ******/
DROP TABLE [dbo].[Monthly_Audit_ErrorFileStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Monthly_Audit_ErrorFileStage](
	[Tipnumber] [nchar](15) NOT NULL,
	[PointsBegin] [decimal](18, 0) NULL,
	[PointsEnd] [decimal](18, 0) NULL,
	[PointsPurchasedCR] [decimal](18, 0) NULL,
	[PointsPurchasedDB] [decimal](18, 0) NULL,
	[PointsBonus] [decimal](18, 0) NULL,
	[PointsAdded] [decimal](18, 0) NULL,
	[PointsIncreased] [decimal](18, 0) NULL,
	[PointsRedeemed] [decimal](18, 0) NULL,
	[PointsReturnedCR] [decimal](18, 0) NULL,
	[PointsReturnedDB] [decimal](18, 0) NULL,
	[PointsSubtracted] [decimal](18, 0) NULL,
	[PointsDecreased] [decimal](18, 0) NULL,
	[Errormsg] [varchar](50) NULL,
	[Currentend] [decimal](18, 0) NULL,
	[PointsBonusBA] [decimal](18, 0) NULL,
	[PointsBonusBC] [decimal](18, 0) NULL,
	[PointsBonusBE] [decimal](18, 0) NULL,
	[PointsBonusBI] [decimal](18, 0) NULL,
	[PointsBonusBM] [decimal](18, 0) NULL,
	[PointsBonusBN] [decimal](18, 0) NULL,
	[PointsBonusBT] [decimal](18, 0) NULL,
	[PointsBonusBR] [decimal](18, 0) NULL,
	[PointsBonusFB] [decimal](18, 0) NULL,
	[PointsBonusFC] [decimal](18, 0) NULL,
	[PointsBonusFD] [decimal](18, 0) NULL,
	[PointsBonusFH] [decimal](18, 0) NULL,
	[PointsBonusFI] [decimal](18, 0) NULL,
	[PointsBonusFJ] [decimal](18, 0) NULL,
	[PointsBonusFK] [decimal](18, 0) NULL,
	[PointsBonusFL] [decimal](18, 0) NULL,
	[PointsBonusFM] [decimal](18, 0) NULL,
	[PointsBonusFP] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
