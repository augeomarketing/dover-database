USE [231]
GO
/****** Object:  StoredProcedure [dbo].[pSetWelcomeKitFileName]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[pSetWelcomeKitFileName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[pSetWelcomeKitFileName] @TipPrefix char (3), @newname nchar(100)  output, @ConnectionString nchar(100) output
AS

declare @Filename char(50), @currentdate nchar(4), @workmonth nchar(2), @workyear nchar(2), @endingDate char(10)

set @endingDate=(select top 1 datein from DateforAudit)

--Section to put together the current date.  Handle problem with date returning a one position month and day
--
set @workmonth=left(@endingdate,2)
set @workyear=right(@endingdate,2)

set @currentdate=@workmonth + @workyear

set @filename='W' + @TipPrefix + @currentdate + '.xls'
 
set @newname='O:\231\Output\WelcomeKits\Welcome.bat ' + @filename
set @ConnectionString='O:\231\Output\WelcomeKits\' + @filename
GO
