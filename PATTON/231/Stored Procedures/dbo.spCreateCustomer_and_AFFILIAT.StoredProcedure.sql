USE [231]
GO

/****** Object:  StoredProcedure [dbo].[spCreateCustomer_and_AFFILIAT]    Script Date: 09/05/2012 17:09:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spCreateCustomer_and_AFFILIAT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spCreateCustomer_and_AFFILIAT]
GO

USE [231]
GO

/****** Object:  StoredProcedure [dbo].[spCreateCustomer_and_AFFILIAT]    Script Date: 09/05/2012 17:09:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spCreateCustomer_and_AFFILIAT] @ProcessDATE nvarchar(10) AS    
--declare @counter numeric(9)
--set @counter = '0'
--declare @ProcessDATE nvarchar(10)  
--set  @ProcessDATE = '03/31/2010'

Declare @MEMBERNUM nvarchar(15)
Declare @filler1 nvarchar(1)
Declare @acctid nvarchar(16)
Declare @filler2 nvarchar(4)
Declare @ACCTTYPE nvarchar(1)
Declare @filler3 nvarchar(4)
Declare @STATUS nvarchar(1)
Declare @filler4 nvarchar(2)
Declare @COLL nvarchar(3)
Declare @filler5 nvarchar(5)
Declare @ACCTNAME1 char(40)
Declare @address1 char(40)
Declare @address2 char(40)
Declare @filler6 nvarchar(10)
Declare @City char(40)
Declare @State char(2)
Declare @filler7 nvarchar(5)
Declare @Zip char(15)
Declare @filler8 nvarchar(2)
Declare @homephone char(10)
Declare @TIPNUMBER NUMERIC(15)
Declare @custnum nvarchar(9)
Declare @LASTNAME char(40)
Declare @NAME1 nvarchar(40)
Declare @NAME nvarchar(40)

DECLARE @AcctTypeDesc NVARCHAR(20)
Declare @ACCTNAME2 char(40)
Declare @ACCTNAME3 char(40)
Declare @ACCTNAME4 char(40)
Declare @address3 char(40)
Declare @address4 char(40)
Declare @Zipplus4 char(5)
Declare @STATUSDESCRIPTION char(40)
Declare @workphone char(10)
Declare @DateAdded char(10)
Declare @RunAvailable float
Declare @RunBalance   float
Declare @RunRedeemed   float
Declare @RunBalanceNew char(8)
Declare @RunAvailableNew char(8)
DECLARE @CityState NVARCHAR(40)

delete from INPUT_CUSTOMER where DIM_INPUTCUSTOMER_STATUS not in ('0','1','4')

delete from INPUT_CUSTOMER where LEFT(DIM_INPUTCUSTOMER_ACCTID,4) = '0000'
or DIM_INPUTCUSTOMER_ACCTID like '%  '
or DIM_INPUTCUSTOMER_ACCTID is null

Insert into CUSTOMER_Stage 
( TIPNUMBER,dateadded,ACCTNAME1,RUNAVAILABLE,runavaliablenew, 
RunRedeemed, RUNBALANCE,Misc5,TIPFIRST,tiplast) 
SELECT distinct 
DIM_INPUTCUSTOMER_TIPNUMBER,@ProcessDATE,DIM_INPUTCUSTOMER_ACCTNAME1,'0','0','0','0',
ltrim(rtrim(SID_INPUTCUSTOMER_MEMBERNUMBER)),LEFT(DIM_INPUTCUSTOMER_TIPNUMBER,3),
RIGHT(DIM_INPUTCUSTOMER_TIPNUMBER,12)
FROM INPUT_CUSTOMER ic left outer join (select tipnumber from dbo.customer_stage) cs
    on dim_inputcustomer_tipnumber = cs.tipnumber
where cs.tipnumber is null




update cs
    set acctname1 = LEFT(IC.DIM_INPUTCUSTOMER_ACCTNAME1,40),
        TIPFIRST = left(DIM_INPUTCUSTOMER_TipNumber,3),
        TIPLAST = right(DIM_INPUTCUSTOMER_TipNumber,12),
        ADDRESS1 = LEFT(IC.DIM_INPUTCUSTOMER_ADDRESS1,40),
        ADDRESS2 = LEFT(IC.DIM_INPUTCUSTOMER_ADDRESS2,40),
        ADDRESS4 = (Ltrim(rtrim(IC.DIM_INPUTCUSTOMER_CITY)) + ' ' + Ltrim(rtrim(IC.DIM_INPUTCUSTOMER_STATE)) + ' ' + Ltrim(rtrim(IC.DIM_INPUTCUSTOMER_ZIPCODE))),
        CITY = IC.DIM_INPUTCUSTOMER_CITY,
        STATE = IC.DIM_INPUTCUSTOMER_STATE,
        ZIPCODE = IC.DIM_INPUTCUSTOMER_ZIPCODE,
        STATUS = DIM_INPUTCUSTOMER_STATUS,
        HOMEPHONE = LEFT(IC.DIM_INPUTCUSTOMER_HOMEPHONE,10) 
FROM INPUT_CUSTOMER IC INNER JOIN CUSTOMER_Stage CS
--    ON ltrim(rtrim(IC.SID_INPUTCUSTOMER_MEMBERNUMBER)) = ltrim(rtrim(CS.MISC5))  --<< ?? WTF  Why do this when tip# already assigned in inputcustomer?
    on ic.dim_inputcustomer_tipnumber = cs.tipnumber
--WHERE ltrim(rtrim(IC.SID_INPUTCUSTOMER_MEMBERNUMBER)) IN (SELECT ltrim(rtrim(CS.MISC5)) FROM INPUT_CUSTOMER)


Update dbo.customer_stage 
    set acctname1=replace(acctname1,char(47), ' '),
        dateadded = isnull(dateadded, @processdate),
        [status] = case
                    when [status] = '1' then 'A'
                    when [status] = '4' then 'R'
                    else 'C'
                   end,
        statusdescription = case
                    when [status] = '1' then 'ACTIVE'
                    when [status] = '4' then 'RESTRICTED'
                    else 'CLOSED'
                   end,
        address2 = ltrim(rtrim(address2))
  

--update CUSTOMER_Stage
--set DATEADDED = @ProcessDATE
--where DATEADDED is null


--update CUSTOMER_Stage
--set STATUS = 'A',StatusDescription = 'ACTIVE'
--where STATUS = '1'

--update CUSTOMER_Stage
--set STATUS = 'R',StatusDescription = 'RESTRICTED'
--where STATUS = '4'		    
	    
--update CUSTOMER_Stage
--set ADDRESS2 = LTRIM(RTRIM(ADDRESS2))



insert into AFFILIAT_Stage
(ACCTID, TIPNUMBER, AcctType, DATEADDED, SECID, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
SELECT distinct DIM_INPUTCUSTOMER_ACCTID, DIM_INPUTCUSTOMER_TIPNUMBER, DIM_INPUTCUSTOMER_ACCTTYPE,
                @ProcessDATE,' ',DIM_INPUTCUSTOMER_STATUS,' ',' ','0',LTRIM(RTRIM(SID_INPUTCUSTOMER_MEMBERNUMBER))
                
FROM dbo.INPUT_CUSTOMER ic left outer join dbo.AFFILIAT_Stage aff
    on ltrim(rtrim(ic.DIM_INPUTCUSTOMER_ACCTID)) = ltrim(rtrim(aff.ACCTID))
where aff.ACCTID is null and DIM_INPUTCUSTOMER_ACCTID != ''



update dbo.affiliat_stage
    set accttypedesc =  case
                            when accttype = 'D' then 'DEBIT CARD'
                            when accttype = 'F' then 'CREDIT CARD'
                            else accttype
                        end,
        accttype = case
                            when accttype = 'D' then 'DEBIT'
                            when accttype = 'F' then 'CREDIT'
                            else accttype
                        end
where accttype in ('D', 'F')


--UPDATE AFFILIAT_Stage 
--SET AcctTypeDesc = 'DEBIT CARD',
--	AcctType = 'DEBIT'
--WHERE AcctType = 'D'

--UPDATE AFFILIAT_Stage 
--SET AcctTypeDesc = 'CREDIT CARD',
--	AcctType = 'CREDIT'
--WHERE AcctType = 'F'

 
	  

--insert into beginning_balance_table
--( tipnumber, Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
--    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 )
--select distinct(DIM_INPUTCUSTOMER_TipNumber), '0','0','0','0','0','0','0','0','0','0','0','0' 
--FROM INPUT_CUSTOMER
--where DIM_INPUTCUSTOMER_TipNumber not in (select tipnumber from beginning_balance_table)
--GO

insert into dbo.beginning_balance_table
    (tipnumber)
select distinct ic.DIM_INPUTCUSTOMER_TipNumber
from dbo.input_customer ic left outer join dbo.beginning_balance_table bbt
    on ic.DIM_INPUTCUSTOMER_TipNumber = bbt.tipnumber
where bbt.tipnumber is null


/*  TEST HARNESS

exec spCreateCustomer_and_AFFILIAT '12/31/2011'


*/

GO

