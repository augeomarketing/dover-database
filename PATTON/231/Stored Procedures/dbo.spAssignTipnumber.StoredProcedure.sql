USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spAssignTipnumber]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spAssignTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*     Updating the TIPNUMBER From The Customer File Using ACCTNUM            */
/*  - Update customer_stage        */
/* BY:  B.QUINN  */
/* DATE: 2/2010   */
/* REVISION: 0 */
/* */
/******************************************************************************/

--
-- SEB 08/2014 delete from input customer if in optout tracking
--
CREATE PROCEDURE [dbo].[spAssignTipnumber]  @POSTDATE NVARCHAR(10),@tipfirst nvarchar(3)   AS  
/* TEST DATA */
--DECLARE @POSTDATE nvarchar(10)
--declare @tipfirst nvarchar(3) 
--set @tipfirst = '231'
--set @POSTDATE = '02/28/2010' 


/* input */
Declare @MEMBERNUM nvarchar(15)
Declare @filler1 nvarchar(1)
Declare @acctid nvarchar(16)
Declare @filler2 nvarchar(4)
Declare @ACCTTYPE nvarchar(1)
Declare @filler3 nvarchar(4)
Declare @STATUS nvarchar(1)
Declare @filler4 nvarchar(2)
Declare @COLL nvarchar(3)
Declare @filler5 nvarchar(5)
Declare @ACCTNAME1 char(50)
Declare @address1 char(40)
Declare @address2 char(40)
Declare @filler6 nvarchar(10)
Declare @City char(40)
Declare @State char(2)
Declare @filler7 nvarchar(5)
Declare @Zip char(15)
Declare @filler8 nvarchar(2)
Declare @homephone char(13)
Declare @TIPNUMBER NUMERIC(15)

SET @TIPNUMBER = '000000000000000'


update dbo.input_customer
    set dim_inputcustomer_tipnumber = null,
        sid_inputcustomer_membernumber = ltrim(rtrim(sid_inputcustomer_membernumber)),
        DIM_INPUTCUSTOMER_ACCTID = ltrim(rtrim(dim_inputcustomer_acctid))

delete dbo.input_customer
where DIM_INPUTCUSTOMER_ACCTID in (select acctid from RewardsNOW.dbo.optouttracking)

delete dbo.input_customer
where sid_inputcustomer_membernumber in (select acctid from RewardsNOW.dbo.optouttracking)


update cust
    set DIM_INPUTCUSTOMER_TIPNUMBER = aff.TipNumber
from dbo.affiliat_stage aff inner JOIN dbo.Input_Customer cust
	on ltrim(rtrim(aff.custid)) = cust.SID_INPUTCUSTOMER_MEMBERNUMBER
	where (cust.DIM_INPUTCUSTOMER_TIPNUMBER is null) 

update cust
    set dim_inputcustomer_tipnumber = aff.tipnumber
from dbo.affiliat_stage aff join dbo.input_customer cust
    on aff.acctid = cust.dim_inputcustomer_acctid
where dim_inputcustomer_tipnumber is null

	

if object_id('input_customer_wk') is not null
    drop table Input_Customer_wk

select DISTINCT(ltrim(rtrim(SID_INPUTCUSTOMER_MEMBERNUMBER))) as SID_INPUTCUSTOMER_MEMBERNUMBER, DIM_INPUTCUSTOMER_TIPNUMBER  AS TIPNUMBER 
into Input_Customer_wk 
from Input_Customer
WHERE DIM_INPUTCUSTOMER_TIPNUMBER = '0'
or DIM_INPUTCUSTOMER_TIPNUMBER  = ' '
or DIM_INPUTCUSTOMER_TIPNUMBER  is null 



DECLARE @LastTipUsed nchar(15)
EXECUTE RewardsNow.dbo.spGetLastTipNumberUsed '231', @LastTipUsed OUTPUT

set @tipnumber = cast(@lasttipused as numeric)



--select @Tipnumber = LastTipNumberUsed
--from Rewardsnow.dbo.dbprocessinfo	
----where DBNumber = '231'
--where DBNumber = @TipFIRST



update Input_Customer_wk
set @TIPNUMBER = @TIPNUMBER + 1 ,
    TIPNUMBER = @TIPNUMBER 
WHERE   TIPNUMBER = 0
or    TIPNUMBER  is null


update Input_Customer
    set DIM_INPUTCUSTOMER_TIPNUMBER = wk.tipnumber
from dbo.Input_Customer_wk wk inner join dbo.Input_Customer ic
    on ltrim(rtrim(wk.SID_INPUTCUSTOMER_MEMBERNUMBER)) = ltrim(rtrim(ic.SID_INPUTCUSTOMER_MEMBERNUMBER))
where ic.DIM_INPUTCUSTOMER_TIPNUMBER is null


--Update client
-- set LastTipNumberUsed = @Tipnumber
 

--Update Rewardsnow.dbo.dbprocessinfo
-- set LastTipNumberUsed = @Tipnumber
-- where DBNumber = @tipfirst
--GO

set @lasttipused = cast(@tipnumber as nchar(15))
EXECUTE RewardsNow.dbo.spPutLastTipNumberUsed '231', @LastTipUsed OUTPUT
GO
