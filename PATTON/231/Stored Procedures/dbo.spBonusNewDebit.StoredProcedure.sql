USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewDebit]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spBonusNewDebit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
create PROCEDURE [dbo].[spBonusNewDebit] @monthendDate nvarchar(10), @TRANCODE varchar(2),  @Points numeric(9)  AS 


Declare @TIPNUMBER varchar(15)
declare  @Acctid varchar(25) 

--   BEGIN TEST PARAMETERS
--declare @monthendDate nvarchar(10)
--set @monthendDate   = '2010-10-31'
--DECLARE @TRANCODE VARCHAR(2)
--DECLARE @POINTS NVARCHAR(4)
--SET @TRANCODE = 'BN'
--SET @POINTS = '1000'

-- END TEST PARAMETERS


if OBJECT_ID('tempdb..#Temp_Affiliat') is not null
	drop table #Temp_Affiliat
	
create table #Temp_Affiliat
	(Tipnumber varchar(15),
	 ACCTID    varchar(16),
	 POINTS    INT )
	 
-- THE DATE IN THIS INSERT IS HARD CODED TO PREVENT ANY CARDS FROM THE INITIAL LOAD OF THE DATA BASE FROM RECEIVING
-- NEW CARD BONUS POINTS. NO CARD ISSUED BEFORE JULY WILL HAVE BONUS POINTS AWARDED
	 
INSERT INTO #Temp_Affiliat
	(Tipnumber,acctid,POINTS )
SELECT
TIPNUMBER,acctid,'1000'
FROM affiliat_STAGE 
WHERE 
AcctType like 'DEB%'
and DateAdded > '2010-06-30'	



if OBJECT_ID('tempdb..#Temp_PREV_AWARDED_Debit') is not null
	drop table #Temp_PREV_AWARDED_Debit
	
create table #Temp_PREV_AWARDED_Debit
	(Tipnumber varchar(15),
	 ACCTID    varchar(16) )
	 
INSERT INTO #Temp_PREV_AWARDED_Debit
	(Tipnumber,acctid )
SELECT
TIPNUMBER,ACCTID
FROM HISTORY 
WHERE 
TRANCODE = 'BN' 

DELETE FROM #Temp_Affiliat WHERE ACCTID IN (SELECT ACCTID FROM #Temp_PREV_AWARDED_Debit)


update CUSTOMER_Stage
set  RunAvaliableNew = '0'
WHERE RunAvaliableNew IS NULL

update CS
set  RunAvaliableNew = (RunAvaliableNew + POINTS)
FROM CUSTOMER_Stage AS CS JOIN (SELECT TIPNUMBER,  SUM( ISNULL (POINTS, 0))  POINTS
								FROM #Temp_Affiliat
								GROUP BY TIPNUMBER) TA
ON TA.TIPNUMBER = CS.TIPNUMBER



Insert into HISTORY_Stage
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
TIPNUMBER,
' ',
@monthendDate,
@TRANCODE,
'1',
@POINTS,
'BONUS NEW DEBIT CARD',
'NEW',
'1',
'0'
from #Temp_Affiliat


--SELECT * FROM #Temp_Affiliat WHERE Tipnumber NOT IN (SELECT Tipnumber FROM CUSTOMER_Stage)
GO
