USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spProcessScorecard]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spProcessScorecard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Scorecard transactions For Service Credit Union   */
/* */
/*  - Update CUSTOMER_Stage      */
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 11/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spProcessScorecard] @POSTDATE nvarchar(10) AS    

/* Test Data */
--declare @POSTDATE nvarchar(10)  
--set  @POSTDATE = '03/31/2010' 

-- Input record fields   
declare @AccountNumber nvarchar(20)
declare @ST nvarchar(1)
declare @PT nvarchar(1)
declare @BegBalance nvarchar(9)
declare @CurrentEarned int
declare @CurrentAdjusted int
declare @CurrentRedeemed int
declare @CurrentExpired int
declare @CurrentAvailable int
declare @AnticipatedToExpire int
declare @MemberNumber nvarchar(9)
declare @Tipnumber nvarchar(15)

-- Work Fields

declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @RunDate dateTIME
Declare @INSTID char(10)
Declare @LASTNAME char(40)
Declare @AcctType char(1)
Declare @DDA char(20)
Declare @tranAmt int
Declare @tranCnt numeric(3)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @STATUSDESCRIPTION char(40)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @SSLAST4 char(13)
Declare @MAXPOINTSPERYEAR numeric(10)
declare @social nvarchar(9)

declare @SECID NVARCHAR(10)
Declare @TRANCODE nvarchar(2)


update CS
set  RunAvaliableNew = (RunAvaliableNew + dim_scorecardreds_CurrentAvailable)
FROM CUSTOMER_Stage AS CS JOIN (SELECT dim_scorecardreds_MemberNumber, SUM( ISNULL(dim_scorecardreds_CurrentAvailable, 0)) dim_scorecardreds_CurrentAvailable
								FROM dbo.ScoreCardReds
								GROUP BY dim_scorecardreds_MemberNumber) IT
ON rtrim(IT.dim_scorecardreds_MemberNumber) = rtrim(CS.MISC5)
 
 
Insert into HISTORY_Stage
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
dim_scorecardreds_TipNumber,
dim_scorecardreds_MemberNumber,
@POSTDATE,
'TP',
'1',
dim_scorecardreds_CurrentAvailable,
'Transfered Points',
'NEW',
'1',
'0'
from dbo.ScoreCardReds
WHERE dim_scorecardreds_MemberNumber IN (SELECT MISC5 FROM CUSTOMER_Stage)
AND dim_scorecardreds_CurrentAvailable > '0'
GO
