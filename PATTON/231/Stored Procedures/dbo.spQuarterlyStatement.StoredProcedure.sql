USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spQuarterlyStatement]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spQuarterlyStatement]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates 
- Changed names on input parms 
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
-- RDT 5/11/2007 chaged source tables from production to stage
-- RDT 8/28/2007  Added expired Points 
-- PHB 7/02/2008 Revised to work for 218 LOC FCU
-- SEB 2/21/14 Changed to calculate beginning balance instead of bbt*/
-- SEB 11/2014  Add code for tranocde PP and Shoping Fling bonuses  F0, F9, G0, G9, H0, H9
-- SEB 11/2014  add code for points to expire
/*******************************************************************************/

CREATE PROCEDURE [dbo].[spQuarterlyStatement]  @StartDate datetime, @EndDate datetime, @TipFirst varchar(3)

AS 

declare @SQL				nvarchar(max)
declare @DB					nvarchar(100)

Declare  @MonthBegin		varchar(2)
Declare  @SQLUpdate			nvarchar(max)

set @DB = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @TipFirst)

set @MonthBegin = month(Convert(datetime, @StartDate) )

--
-- Add 23 hours, 59 minutes, 59 seconds to the enddate.  This catches any transactions that have
-- this as part of the histdate - ie Expiring Points
--
set @enddate = dateadd(hh, 23, @enddate)
set @enddate = dateadd(mi, 59, @enddate)
set @enddate = dateadd(ss, 59, @enddate)

-- 
-- clear the quarterly statement file and the statement summary file
truncate table dbo.monthly_Statement_File
truncate table dbo.qsfHistorySummary

-- Build the history summary 
set @SQL = '
		insert into dbo.qsfHistorySummary
		(TipNumber, TranCode, Points)
		select tipnumber, trancode, sum(points) points
		from ' + @DB + '.dbo.history
		where histdate >= @startdate and histdate <= @endDate
		group by tipnumber, trancode'
print @SQL
exec sp_executesql @SQL, N'@startdate datetime, @enddate datetime', @startdate = @startdate, @enddate = @enddate


--
-- Add in customer data
--

--
-- NOTE:  There is a "hack job" - joining to the monthly_statement_file to limit the # statements going out
--        This is because customers aren't getting deleted like they should be when we post to productin
--        Make sure this is a temporary fix!!
set @SQL = '
	insert into dbo.monthly_Statement_File 
	(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, status)
	select cus.tipnumber, cus.acctname1, cus.acctname2, cus.address1, cus.address2, cus.address3, 
			(rtrim(cus.city) + '' '' + rtrim(cus.state) + '' '' + cus.zipcode), cus.status
	from ' + @DB + '.dbo.customer cus
	where left(cus.tipnumber,3) = ' + char(39) + @TipFirst + char(39) + '  and status = ''A'' '
print @SQL
exec sp_executesql @SQL



	update qsf
		set pointspurchasedCR = pointspurchasedCR + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('63')), 0)
	from dbo.monthly_Statement_File qsf

	update qsf
		set pointspurchasedDB = pointspurchasedDB + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('67')), 0)
	from dbo.monthly_Statement_File qsf

	--
	update qsf
		set pointsreturnedCR = pointsreturnedCR + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('33')), 0)
	from dbo.monthly_Statement_File qsf

	update qsf
		set pointsreturnedDB = pointsreturnedDB + isnull((select sum(points)
								   from dbo.qsfHistorySummary hs
								   where hs.tipnumber = qsf.tipnumber
								   and hs.trancode in ('37')), 0)
	from dbo.monthly_Statement_File qsf


/* Load the statmement file with bonuses            */
--update qsf
--	set pointsbonus = pointsbonus + hs.points
--from dbo.monthly_Statement_File qsf join dbo.qsfHistorySummary hs
--	on qsf.tipnumber = hs.tipnumber
--where hs.trancode like 'b%'
update qsf
	set pointsbonus = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and (hs.trancode like 'b%' or hs.trancode like 'f%' or hs.trancode like 'G%' or hs.trancode like '0%') and
							       hs.trancode not in ('F0', 'G0', 'F9', 'G9')), 0)
from dbo.monthly_Statement_File qsf


update qsf
	set POINTSBONUSMER = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('F0', 'G0','H0')), 0)
from dbo.monthly_Statement_File qsf


update qsf
	set POINTSBONUSMER = POINTSBONUSMER - isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('F9', 'G9','H9')), 0)
from dbo.monthly_Statement_File qsf

update qsf
	set PurchasedPoints = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode in ('PP')), 0)
from dbo.monthly_Statement_File qsf


/* Load the statmement file with plus adjustments    */

update qsf
	set pointsadded = pointsadded + isnull((select sum(points)
							   from dbo.qsfHistorySummary hs
							   where hs.tipnumber = qsf.tipnumber
							   and hs.trancode in ('IE', 'TR', 'TX', 'TP', 'DR', 'XF')), 0)
from dbo.monthly_Statement_File qsf





/* Load the statmement file with total point increases */
update dbo.monthly_Statement_File
	set pointsincreased= pointspurchaseddb + pointspurchasedcr + pointsbonus + pointsadded  + POINTSBONUSMER + PurchasedPoints


/* Load the statmement file with redemptions          */
--update qsf
--	set pointsredeemed = pointsredeemed + hs.points
--from dbo.monthly_Statement_File qsf join dbo.qsfHistorySummary hs
--	on qsf.tipnumber = hs.tipnumber
--where hs.trancode like 'R%'

update qsf
	set pointsredeemed = isnull((select sum(points) 
							from dbo.qsfHistorySummary hs 
							where hs.tipnumber = qsf.tipnumber 
							and hs.trancode like 'r%'), 0)
from dbo.monthly_Statement_File qsf



/* Load the statmement file with minus adjustments    */
--update qsf
--	set pointssubtracted = pointssubtracted + hs.points
--from dbo.monthly_Statement_File qsf join dbo.qsfHistorySummary hs
--	on qsf.tipnumber = hs.tipnumber
--where hs.trancode in ('DE', 'IR', 'EP')

update qsf
	set pointssubtracted = isnull((select sum(points)
							   from dbo.qsfHistorySummary hs
							   where hs.tipnumber = qsf.tipnumber
							   and hs.trancode in  ('DE', 'IR', 'EP')), 0)
from dbo.monthly_Statement_File qsf




/* Add expired Points */
update qsf
	set pointsexpired = pointsexpired + hs.points
from dbo.monthly_Statement_File qsf join dbo.qsfHistorySummary hs
	on qsf.tipnumber = hs.tipnumber
where hs.trancode = 'XP'



/* Load the statmement file with total point decreases */
update dbo.monthly_Statement_File
	set pointsdecreased=pointsredeemed +  pointsreturneddb + pointsreturnedcr + pointssubtracted + PointsExpired

/* Added SEB 2/21/14  */
select tipnumber, sum(points*ratio) as BegBal
into #tmp
from history 
where histdate < @Startdate
group by tipnumber

-- SEB 02/19/2014 use history instead of beginning balance table 
set @SQLUpdate=N'update Monthly_Statement_File
			set pointsbegin = ISNULL(tmp.begbal,0)
			from Monthly_Statement_File msf join #tmp tmp on msf.tipnumber = tmp.tipnumber '
exec sp_executesql @SQLUpdate

-- below removed SEB 2/21/14
--/* Load the statmement file with the Beginning balance for the Month */
--set @SQL =	'update qsf
--				set pointsbegin = monthbeg' + @MonthBegin + N'
--			from dbo.monthly_Statement_File qsf join dbo.beginning_balance_table bbt
--				on qsf.tipnumber = bbt.tipnumber'
--print @SQL
--exec sp_executesql @SQL


/* Load the statmement file with beginning points */
update dbo.monthly_Statement_File
	set pointsend=pointsbegin + pointsincreased - pointsdecreased

--/* Load the acctid to the monthly statement table */
--set @SQL = '
--	Update qsf
--		set acctid = a.acctid 
--	from dbo.monthly_Statement_File qsf join ' + @DB + '.dbo.affiliat a 
--		on qsf.tipnumber = a.tipnumber'
--print @SQL
--exec sp_executesql @SQL

Exec RewardsNOW.dbo.usp_ExpirePoints @TipFirst, 1 

update dbo.monthly_Statement_File
	set PointsToExpire = isnull( (select points_expiring from RewardsNOW.dbo.RNIExpirationProjection
								where tipnumber=monthly_Statement_File.tipnumber), 0)  
GO
