USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spBonusNewMemberStage]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spBonusNewMemberStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spBonusNewMemberStage]  @DateAdded char(10),  @POINTS int, @TranCode Char(2)  AS



Declare @TIPNUMBER varchar(15)
----BEGIN TEST PARAMETERS
--declare @DateAdded nvarchar(10)
--set @DateAdded   = '2010-04-30'
--DECLARE @TRANCODE VARCHAR(2)
--DECLARE @POINTS numeric(4)
--SET @TRANCODE = 'BN'

--SET @POINTS = '2500'

-- END TEST PARAMETERS


if OBJECT_ID('tempdb..#Temp_Customer') is not null
	drop table #Temp_Customer
	
create table #Temp_Customer
	(Tipnumber varchar(15),
	 MemberNum    varchar(20),
	 POINTS    numeric(4) )
	 
-- THE DATE IN THIS INSERT IS HARD CODED TO PREVENT ANY CARDS FROM THE INITIAL LOAD OF THE DATA BASE FROM RECEIVING
-- NEW CARD BONUS POINTS. NO CARD ISSUED BEFORE JULY WILL HAVE BONUS POINTS AWARDED
	 
INSERT INTO #Temp_Customer
	(Tipnumber,MemberNum,POINTS )
SELECT
TIPNUMBER,rtrim(Misc5),@POINTS
FROM customer_STAGE 
WHERE DateAdded = @DateAdded



if OBJECT_ID('tempdb..#Temp_PREV_AWARDED') is not null
	drop table #Temp_PREV_AWARDED
	
create table #Temp_PREV_AWARDED
	(Tipnumber varchar(15))
	 
INSERT INTO #Temp_PREV_AWARDED
	(Tipnumber )
SELECT
distinct(TIPNUMBER)
FROM HISTORY 
WHERE 
TRANCODE in ('BN','FJ')  

DELETE FROM #Temp_Customer WHERE tipnumber IN (SELECT tipnumber FROM #Temp_PREV_AWARDED)


update CUSTOMER_Stage
set  RunAvaliableNew = '0'
WHERE RunAvaliableNew IS NULL

update CS
set  RunAvaliableNew = (RunAvaliableNew + POINTS)
FROM CUSTOMER_Stage AS CS JOIN (SELECT TIPNUMBER,  SUM( ISNULL (POINTS, 0))  POINTS
								FROM #Temp_Customer
								GROUP BY TIPNUMBER) TA
ON TA.TIPNUMBER = CS.TIPNUMBER



Insert into HISTORY_Stage
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
TIPNUMBER,
' ',
@DateAdded,
@TRANCODE,
'1',
@POINTS,
'BONUS NEW MEMBER',
'NEW',
'1',
'0'
from #Temp_Customer


--SELECT * FROM #Temp_Customer WHERE Tipnumber NOT IN (SELECT Tipnumber FROM CUSTOMER_Stage)
GO
