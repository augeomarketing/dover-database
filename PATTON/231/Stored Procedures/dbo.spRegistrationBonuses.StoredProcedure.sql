USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spRegistrationBonuses]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spRegistrationBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spRegistrationBonuses] @monthendDate nvarchar(10), @Points int  AS 
declare @Tipnumber char(15) 
declare @acctid varchar(25)
declare  @Trandate datetime
Declare @DateIn datetime
--declare @Points int 
--declare @monthendDate nvarchar(10)
--set @monthendDate = '2010-05-31'
--set @Points='1000'

set @DateIn = @monthendDate  


set @Trandate=@Datein



if OBJECT_ID('tempdb..#sb_hist') is not null
	drop table #sb_hist
	
create table #sb_hist
	(tipnumber		varchar(15) primary key)
	
insert into #sb_hist
select distinct tipnumber
from dbo.HISTORY 
where TRANCODE = 'BR'


delete sb
from dbo.SignupBonus sb join #sb_hist tmp
	on sb.tipnumber = tmp.tipnumber



update CS
set 
  RunAvaliableNew  = (RunAvaliableNew + @Points)
from dbo.SignupBonus as sb inner join dbo.CUSTOMER_Stage as cs
	on sb.tipnumber = cs.tipnumber



Insert Into History_Stage (TipNumber,HistDate,TranCode,TranCount,Points,secid,Ratio,Description)
select Tipnumber, @Trandate, 'BR', '1', @Points, 'NEW', '1', 'Bonus Registration'
from dbo.signupbonus
GO
