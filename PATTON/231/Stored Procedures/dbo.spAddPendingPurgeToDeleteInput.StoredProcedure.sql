USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spAddPendingPurgeToDeleteInput]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spAddPendingPurgeToDeleteInput]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 5/2007   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spAddPendingPurgeToDeleteInput] @POSTDATE nvarchar(10)AS
/* input */
/*********XXXXXXXXXXXXX**/
 /* input */

Declare @TipNumber char(15)
Declare @acctid char(16)
Declare @pendeddate datetime


             
	insert into accountdeleteinput
	(acctid, DDA) 	
	select 	 
	   acctid,  dda
	from   accountdeleteinput2 
	where acctid not in
	(select acctid from accountdeleteinput)
GO
