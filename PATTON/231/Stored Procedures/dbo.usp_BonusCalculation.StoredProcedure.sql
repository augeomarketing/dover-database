USE [231]
GO
/****** Object:  StoredProcedure [dbo].[usp_BonusCalculation]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[usp_BonusCalculation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_BonusCalculation]
        @tipfirst               varchar(3),
        @processingdate         date

AS


declare @rowid              int = 0

declare @sid                int
declare @bpfi_id            int
declare @trancode           varchar(2)
declare @programtype        varchar(1)
declare @bonuspoints        int
declare @pointmultiplier    numeric(5,2)
declare @durationtypeshort  varchar(3)
declare @durationperiod     int
declare @sproc              nvarchar(50)

declare @sql                nvarchar(max)

if object_id('tempdb..#bonusesavailable') is not null
    drop table #bonusesavailable

if object_id('tempdb..#bonuses') is not null
    drop table #bonuses

create table #bonusesavailable
    (sid_bonusesavailable_id            int identity primary key,
     tipfirst                           varchar(3),
     sid_bonusprogramfi_id              int,
     trancode                           varchar(2),
     programtype                        varchar(1),
     bonuspoints                        int,
     pointmultiplier                    numeric(5,2),
     durationtypeshort                  varchar(3),
     durationperiod                     int,
     storedproc                         nvarchar(50))

-- 
-- Put all bonus programs FI is eligible for with the processing date provided into a temp work table
insert into #bonusesavailable
(tipfirst, sid_bonusprogramfi_id, trancode, programtype, bonuspoints, pointmultiplier, durationtypeshort, durationperiod, storedproc)
select sid_dbprocessinfo_dbnumber, bpfi.sid_rnibonusprogramfi_id, sid_trantype_tranCode, sid_rnibonusprogramtype_code, dim_rnibonusprogramfi_bonuspoints,
        dim_rnibonusprogramfi_pointmultiplier, dim_rnibonusprogramdurationtype_shortcode, dim_rnibonusprogramfi_duration,
        dim_rnibonusprogramfi_storedprocedure
from rewardsnow.dbo.rnibonusprogramfi bpfi join rewardsnow.dbo.rnibonusprogram bp
    on bpfi.sid_rnibonusprogram_id = bp.sid_rnibonusprogram_id
left outer join rewardsnow.dbo.RNIBonusProgramDurationType bpdt
    on bpfi.sid_rnibonusprogramdurationtype_id = bpdt.sid_rnibonusprogramdurationtype_id
where sid_dbprocessinfo_dbnumber = @tipfirst
and @processingdate between dim_rnibonusprogramfi_effectivedate and dim_rnibonusprogramfi_expirationdate

--
--  Loop through all of the active bonus programs for the fi
if @@rowcount > 0  -- FI has an eligible bonus program in effect as of the processing end date
BEGIN
    
    -- Get the parameters for the bonus
    select top 1 @sid               = sid_bonusesavailable_id,
                 @bpfi_id           = sid_bonusprogramfi_id, 
                 @trancode          = trancode,
                 @programtype       = programtype, 
                 @bonuspoints       = bonuspoints,
                 @pointmultiplier   = pointmultiplier, 
                 @durationtypeshort = durationtypeshort, 
                 @durationperiod    = durationperiod, 
                 @sproc             = storedproc
    from #bonusesavailable
    where sid_bonusesavailable_id > @rowid
    order by sid_bonusesavailable_id
    
    while @sid > @rowid
    BEGIN
    
        set @rowid = @sid
        
        
        if @programtype = 'C'  -- calculated bonus points (double points, etc.)
        BEGIN
        
            -- build dynamic SQL providing the parameters    
            set @sql = 'EXEC ' + @sproc + ' ' + quotename(@trancode) + ', ' +
                                                quotename(@pointmultiplier) + ', ' +
                                                quotename(@processingdate)
            --print @sql
            -- add in the bonuses
            exec sp_executesql @sql
        
        
        END
        
        if @programtype = 'F' -- Fixed award (i.e. estatement bonus)
        BEGIN

            set @sql = 'EXEC ' + @sproc + ' ' + quotename(@tipfirst) + ', ' +
                                                quotename(@processingdate) + ', ' +
                                                quotename(@trancode) + ', ' +
                                                quotename(@bonuspoints)
            
            exec sp_executesql @sql

        END
        
    





        select top 1 @sid               = sid_bonusesavailable_id,
                     @bpfi_id           = sid_bonusprogramfi_id, 
                     @trancode          = trancode, 
                     @programtype       = programtype, 
                     @bonuspoints       = bonuspoints,
                     @pointmultiplier   = pointmultiplier, 
                     @durationtypeshort = durationtypeshort, 
                     @durationperiod    = durationperiod, 
                     @sproc             = storedproc
        from #bonusesavailable
        where sid_bonusesavailable_id > @rowid
        order by sid_bonusesavailable_id
    END
END





/* Test harness 


exec dbo.usp_BonusCalculation '231', '01/31/2011'


*/
GO
