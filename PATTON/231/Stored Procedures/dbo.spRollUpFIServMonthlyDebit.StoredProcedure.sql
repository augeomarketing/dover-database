USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spRollUpFIServMonthlyDebit]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spRollUpFIServMonthlyDebit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Process the Monthly DebitTrans For Central Bank         */
/* */
/*  - Update CUSTOMER_Stage      */
/*  -  Update AFFILIAT_Stage       */ 
/*  - Update History_Stage          */
/* BY:  B.QUINN  */
/* DATE: 08/2010   */
/* REVISION: 0 */
/* */
/******************************************************************************/
CREATE PROCEDURE [dbo].[spRollUpFIServMonthlyDebit]   @runstart nvarchar(10), @RUNEND NVARCHAR (10) AS    

/* Test Data */
--declare @runstart nvarchar(10)
--DECLARE @RUNEND NVARCHAR (10)
--set  @runstart = '07/01/2010' 
--set  @RUNEND = '07/31/2010' 
declare @SQLSet nvarchar(4000), @SQLInsert nvarchar(4000), @SQLUpdate nvarchar(4000), @SQLSelect nvarchar(4000), @SQLDelete nvarchar(4000), @SQLIf nvarchar(4000)

DECLARE @MONTHSTART nvarCHAR(6)
DECLARE @MONTHEND nvarCHAR(6)
declare @stday char(2)
DECLARE @STMO CHAR(2)
DECLARE @STYR CHAR(2)
declare @endMO char(2)
declare @endday char(2)
declare @endYR char(2)
 
declare @NetID  varchar (6) 
declare @Primary_Account_Number  varchar (16) 
declare @Debit_Credit_Flag  varchar (2) 
declare @Amt_Reconciliation  varchar (10) 
declare @Trans_Processing_Code  varchar (6) 
declare @Network_Posting_Date varchar (6)
declare @Reversal_Reason_Code varchar(2)

declare @TransamtPur  numeric(10,2)
declare @TransamtRet  numeric(10,2)
declare @TranscntPur  numeric(4) 
declare @TranscntRet  numeric(4) 
declare @tipnumber    nvarchar(15)

-- @Tipnumber set to all zeros to pad file before update from AFFILIAT
set @tipnumber = '231000000000000'

 
declare @dtot1 int
declare @dtot2 int

--PRINT @runstart
SET @STYR = RIGHT(@runstart,2)
--PRINT @STYR
SET @STMO = RIGHT(@runstart,10)
--PRINT @STMO
SET @STDAY = RIGHT(@runstart,7)
--PRINT @STDAY
--PRINT @runEND
SET @ENDYR = RIGHT(@RUNEND,2)
--PRINT @ENDYR
SET @ENDMO = RIGHT(@RUNEND,10)
--PRINT @ENDMO
SET @ENDDAY = RIGHT(@RUNEND,7)
--PRINT @ENDDAY
SET @MONTHSTART = (@STYR + @STMO + @STDAY)
--PRINT '@MONTHSTART'
--PRINT @MONTHSTART
SET @MONTHEND = (@ENDYR + @ENDMO + @ENDDAY)
--PRINT '@MONTHEND'
--PRINT @MONTHEND 


if OBJECT_ID('tempdb..#Temp_FIServ_Daily') is not null
	drop table #Temp_FIServ_Daily
	
create table #Temp_FIServ_Daily
	(acctid		varchar(16) primary key
	 ,Tipnumber nvarchar(15)
	 ,MemberNum nvarchar(9)	
	 ,TransamtPur numeric(10,2)
	 ,TranscntPur int
	 ,TransamtRet numeric(10,2)  
	 ,TranscntRet int 
	 ,PostDate varchar(6))

/*   - declare @ CURSOR AND OPEN TABLES  */
declare FID_CRSR  cursor fast_forward
for Select
 dim_DebitInput_Primary_Account_Number ,  dim_DebitInput_NetID ,
 dim_DebitInput_Trans_Processing_Code , dim_DebitInput_Debit_Credit_Flag ,
 dim_DebitInput_Amt_Reconciliation , dim_DebitInput_Network_Posting_Date,
 dim_DebitInput_Reversal_Reason_Code
From  dbo.debitinput 
order by dim_DebitInput_Primary_Account_Number

Open FID_CRSR 
/*                  */



Fetch FID_CRSR  
into 
 
     @Primary_Account_Number 
  ,  @NetID   
  ,  @Trans_Processing_Code  
  ,  @Debit_Credit_Flag   
  ,  @Amt_Reconciliation 
  ,  @Network_Posting_Date
  ,  @Reversal_Reason_Code

  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


                                                                           
while @@FETCH_STATUS = 0
BEGIN




--IF @Network_Posting_Date <  @MONTHSTART or @Network_Posting_Date  >   @MONTHEND 
--GOTO FETCH_NEXT

if @Reversal_Reason_Code > '00'
GOTO FETCH_NEXT


if @Trans_Processing_Code < '440000'
GOTO FETCH_NEXT


if @Amt_Reconciliation is null
set @Amt_Reconciliation = '0'


	/*  - Check For daily Record       */
 	if not exists (select acctid from #Temp_FIServ_Daily
                    where acctid = @Primary_Account_Number)	
		begin
		set @TransamtPur = '0'
		set @TransamtRet = '0'
		set @TranscntPur = '0'
		set @TranscntRet = '0'
		set @Transcntret = '0'
		set @Transcntpur = '0'
		if @Debit_Credit_Flag = 'CR'
		   begin
		     set @TransamtRet = cast(@Amt_Reconciliation  as money) /100 
		     set @Transcntret = '1'
		     set @Transcntpur = '0'
		   end
		if @Debit_Credit_Flag = 'DB'		
		   begin
		     set @Transamtpur = cast(@Amt_Reconciliation  as money) /100 
		     set @Transcntpur = '1'
		     set @Transcntret = '0'
		   end
		Insert into #Temp_FIServ_Daily              
		(	acctid  ,Tipnumber , TransamtPur , TranscntPur , 
			TransamtRet , TranscntRet  , PostDate   )
	    values
 		(	@Primary_Account_Number  ,@Tipnumber , @Transamtpur , @Transcntpur ,
			@TransamtRet , @TranscntRet , @MONTHEND  )
	    end
	else
		begin
		if @Debit_Credit_Flag = 'CR'
		   begin
		     set @Transamtret = cast(@Amt_Reconciliation  as money) /100 
			 update #Temp_FIServ_Daily 
		      set	 
		 	  Transamtret = Transamtret + @Transamtret,
			  Transcntret = Transcntret + 1
			 WHERE
			   ACCTID = @Primary_Account_Number
		   end
		if @Debit_Credit_Flag = 'DB'
		   begin
		     set @Transamtpur = cast(@Amt_Reconciliation  as money) /100
			 update #Temp_FIServ_Daily 
			  set	 
			  Transcntpur = Transcntpur + 1,
			  Transamtpur = Transamtpur + @Transamtpur
			 WHERE
		       ACCTID = @Primary_Account_Number
		   end		
	END        

FETCH_NEXT:
	
	Fetch FID_CRSR   
	into 
     @Primary_Account_Number 
  ,  @NetID   
  ,  @Trans_Processing_Code  
  ,  @Debit_Credit_Flag   
  ,  @Amt_Reconciliation 
  ,  @Network_Posting_Date
  ,	 @Reversal_Reason_Code
	


END /*while */	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:

close  FID_CRSR 
deallocate  FID_CRSR

-- ADD TIPNUMBER AND MEMBER NUMBER TO THE TRANSACTIONS FROM THE AFFILIAT RECORD


update #Temp_FIServ_Daily
set 
 #Temp_FIServ_Daily.tipnumber = a.tipnumber
 ,#Temp_FIServ_Daily.MemberNum = left(a.custid,9)
from AFFILIAT_stage as a
join #Temp_FIServ_Daily as t
on t.acctid = a.acctid





insert into INPUT_TRANSACTION
	(SID_INPUTTRANSACTION_MEMBERNUMBER,
	 DIM_INPUTTRANSACTION_FILLER1,
	 DIM_INPUTTRANSACTION_ACCTTYPE,
	 DIM_INPUTTRANSACTION_FILLER2,
	 DIM_INPUTTRANSACTION_COLL,
	 DIM_INPUTTRANSACTION_FILLER3,
	 DIM_INPUTTRANSACTION_TRANCNT,
	 DIM_INPUTTRANSACTION_AMOUNT,
	 DIM_INPUTTRANSACTION_CARDTYPE,
	 DIM_INPUTTRANSACTION_TIPNUMBER,
	 DIM_INPUTTRANSACTION_TRANCODE)
	 select
	 MemberNum  ,' ', 'DB',' ',' ',' ', 
	 TranscntPur ,TransamtPur, 'DEBIT',Tipnumber , '67'
from #Temp_FIServ_Daily
where TransamtPur > '0'

insert into INPUT_TRANSACTION
	(SID_INPUTTRANSACTION_MEMBERNUMBER,
	 DIM_INPUTTRANSACTION_FILLER1,
	 DIM_INPUTTRANSACTION_ACCTTYPE,
	 DIM_INPUTTRANSACTION_FILLER2,
	 DIM_INPUTTRANSACTION_COLL,
	 DIM_INPUTTRANSACTION_FILLER3,
	 DIM_INPUTTRANSACTION_TRANCNT,
	 DIM_INPUTTRANSACTION_AMOUNT,
	 DIM_INPUTTRANSACTION_CARDTYPE,
	 DIM_INPUTTRANSACTION_TIPNUMBER,
	 DIM_INPUTTRANSACTION_TRANCODE)
	 select
	 MemberNum  ,' ', 'DB',' ',' ',' ', 
	 Transcntret ,(Transamtret * -1), 'DEBIT',Tipnumber , '37'
from #Temp_FIServ_Daily
where Transamtret > '0' 


--REMOVE TRANSACTIONS THAT HAVE NO ACTIVE MEMBER NUMBER

delete from  INPUT_TRANSACTION where SID_INPUTTRANSACTION_MEMBERNUMBER is null

-- DROP PREVIOUS TEMP TABLE IF IT EXISTS

set @SQLIf=N'if exists(select * from  .dbo.sysobjects where xtype=''u'' and name = ''Temp_SCU_Debit'')
Begin
	drop TABLE dbo.Temp_SCU_Debit 
End '
exec sp_executesql @SQLIf

--print @SQLIf

-- CREATE TEMPORARY FILE FOR RESEARCH. #Temp_FIServ_Daily GOES AWAY AT THE END OF THE JOB

select * into Temp_SCU_Debit from #Temp_FIServ_Daily

delete from  Temp_SCU_Debit where MemberNum is null
GO
