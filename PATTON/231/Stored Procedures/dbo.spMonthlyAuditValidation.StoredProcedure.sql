USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spMonthlyAuditValidation]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spMonthlyAuditValidation]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spMonthlyAuditValidation] AS 

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Monthly file balances                          */
/*                                                                            */
/******************************************************************************/

delete from Monthly_Audit_ErrorFile 
                                                                          


	insert into Monthly_Audit_ErrorFile 
	(
 	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, errormsg
	, currentend
	, pointsbonusBA
	, pointsbonusBC
	, pointsbonusBE
	, pointsbonusBI
	, pointsbonusBM
	, pointsbonusBN
	, pointsbonusBT
	, pointsbonusFB
	, pointsbonusFC
	, pointsbonusFD
	, pointsbonusFH 
	, pointsbonusFI
	 ,pointsbonusFJ
 	, pointsbonusFK
 	, pointsbonusFL
 	, pointsbonusFM
 	, pointsbonusFP
 ) 
	select
	  Tipnumber
	, pointsbegin
	, pointsend
	, pointspurchasedCR
	, pointspurchasedDB
	, pointsbonus
	, pointsadded
	, pointsincreased
	, pointsredeemed
	, pointsreturnedCR
	, pointsreturnedDB
	, pointssubtracted
	, pointsdecreased
	, 'Beginning and END DO NOT MATCH'
	, pointsend
	, pointsbonusBA
	, pointsbonusBC
	, pointsbonusBE
	, pointsbonusBI
	, pointsbonusBM
	, pointsbonusBN
	, pointsbonusBT
	, pointsbonusFB
	, pointsbonusFC
	, pointsbonusFD
	, pointsbonusFH 
	, pointsbonusFI
	 ,pointsbonusFJ
 	, pointsbonusFK
 	, pointsbonusFL
 	, pointsbonusFM
 	, pointsbonusFP
	from
	 Monthly_Statement_File
	 where pointsend <>
	 (select AdjustedEndingPoints from CURRENT_MONTH_ACTIVITY 	  
	  where Monthly_Statement_File.Tipnumber = CURRENT_MONTH_ACTIVITY.TIPNUMBER)
GO
