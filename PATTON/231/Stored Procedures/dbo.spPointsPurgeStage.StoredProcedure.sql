USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spPointsPurgeStage]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spPointsPurgeStage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spPointsPurgeStage] @DateInput as datetime AS   
/*  **************************************  */
/* Date: 8/7/06 */
/* Author: Rich T  */
/*  ***************************************************************************  */
/*  CLONED FROM PROCESS WRITTEN BY R. TREMBLAY                                   */
/*  Modified to Process against Stage Tables does not write to the Deleted files */
/*  Marks accouts for Delete from AccountDeleteInput                             */
/*  Copy flagged Accouts to Accountdeleted                                       */
/*  Delete Account                                                               */
/*  Check for customers without accounts and flag for delete                     */
/*  Mark history for delete where customer flagged for delete                    */
/*  Copy flagged customers to Customerdeleted                                    */
/*  Copy flagged history to Historydeleted                                       */
/*  Delete History                                                               */
/*  Delete Customer                                                              */
/*  **************************************************************************** */
Declare @DateDeleted as datetime

/*  **************************************  */
/*  Process only if dateinput is a valid date */
If IsDate(@DateInput) = 1 
   Begin
	Set @DateDeleted = @DateInput 

	-- Set Affiliat records that are null to active 
	update Affiliat_stage set AcctStatus = 'A' where AcctStatus is null

	/*  **************************************  */
	-- Set Customer status = null to active 
	update Customer_stage set Status = 'A' where Status is null

	/*  **************************************  */
	-- Mark affiliat records for deleted with "9"
	update Affiliat_stage	
	set Acctstatus = '9'
	where exists 
		( select acctid from AccountDeleteInput 
		  where  affiliat_stage.acctid = AccountDeleteInput.acctid )
	
	/*  **************************************  */
	-- Copy deleted accounts to AffiliatedDeleted not performed on stage
	 --INSERT INTO AffiliatDeleted 
	 -- (TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 --  YTDEarned,CustID,DateDeleted )
	 --SELECT 
	 --TIPNUMBER,ACCTID,LastName,AcctType,DATEADDED,SECID,AcctStatus,AcctTypeDesc,
	 --YTDEarned,CustID,@DateDeleted
	 --FROM affiliat 
	 --WHERE  affiliat_stage.acctstatus = '9'
	
	/*  **************************************  */
	Delete from affiliat_stage where acctstatus = '9'
	
	/*  **************************************  */
	-- Find Customers without accounts and mark for delete 
	update Customer_stage
	set Status='9'
	where tipnumber not in 
		(select  distinct(tipnumber)  from affiliat_stage )
	
	
	/*  **************************************  */
	--Copy  accounts flagged for delete to CustomerDeleted not performed on stage
	--INSERT INTO [CustomerDeleted]
	--(TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	--DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	--ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	--WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	--Misc1,Misc2,Misc3,Misc4,Misc5,RunAvaliableNew, DateDeleted)
	--select 
	--TIPNUMBER,RunAvailable,RUNBALANCE,RunRedeemed,LastStmtDate,NextStmtDate,STATUS,
	--DATEADDED,LASTNAME,TIPFIRST,TIPLAST,ACCTNAME1,ACCTNAME2,ACCTNAME3,ACCTNAME4,ACCTNAME5,
	--ACCTNAME6,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,City,State,ZipCode,StatusDescription,HOMEPHONE,
	--WORKPHONE,BusinessFlag,EmployeeFlag,SegmentCode,ComboStmt,RewardsOnline,NOTES,BonusFlag,
	--Misc1,Misc2,Misc3,Misc4,Misc5,RunAvaliableNew, @DateDeleted 
	--from customer_stage WHERE  status = '9'


	/*  **************************************  */
	-- Copy History records into HistoryDeleted whre customers status = 9 not performed on stage
	--INSERT INTO HistoryDeleted
	--([TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	--[POINTS], [Description], [SECID], [Ratio], [DateDeleted],overage)
	--select 
	--[TIPNUMBER], [ACCTID], [HISTDATE], [TRANCODE], [TranCount], 
	--[POINTS], [Description], [SECID], [Ratio], @DateDeleted,overage
	--from history_stage
	--where tipnumber in 
	--(select tipnumber from customer_stage where status = '9')

	/*  **************************************  */
	-- Delete History
	Delete from history_stage where tipnumber in (select tipnumber from customer_stage where status = '9')
	
	/*  **************************************  */
	-- Delete Customer
	Delete from customer_stage where status = '9'
	/* Return 0 */
End
Else
	Print ' Bad date'
	/* Return 99 */
GO
