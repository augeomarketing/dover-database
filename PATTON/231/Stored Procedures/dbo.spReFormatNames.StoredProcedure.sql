USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spReFormatNames]    Script Date: 06/22/2010 13:49:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReFormatNames]  AS    
declare @counter numeric(9)
set @counter = '0'
--declare @ProcessDATE nvarchar(10)  
--set  @ProcessDATE = '03/15/2010'

Declare @MEMBERNUM nvarchar(15)
Declare @filler1 nvarchar(1)
Declare @acctid nvarchar(16)
Declare @filler2 nvarchar(4)
Declare @ACCTTYPE nvarchar(1)
Declare @filler3 nvarchar(4)
Declare @STATUS nvarchar(1)
Declare @filler4 nvarchar(2)
Declare @COLL nvarchar(3)
Declare @filler5 nvarchar(5)
Declare @ACCTNAME1 char(40)
Declare @address1 char(40)
Declare @address2 char(40)
Declare @filler6 nvarchar(10)
Declare @City char(40)
Declare @State char(2)
Declare @filler7 nvarchar(5)
Declare @Zip char(15)
Declare @filler8 nvarchar(2)
Declare @homephone char(10)
Declare @TIPNUMBER NUMERIC(15)
Declare @custnum nvarchar(9)
Declare @LASTNAME char(255)
Declare @NAME1 nvarchar(255)
Declare @NAME nvarchar(255)

DECLARE @AcctTypeDesc NVARCHAR(20)
Declare @ACCTNAME2 char(40)
Declare @ACCTNAME3 char(40)
Declare @ACCTNAME4 char(40)
Declare @address3 char(40)
Declare @address4 char(40)
Declare @Zipplus4 char(5)
Declare @STATUSDESCRIPTION char(40)
Declare @workphone char(10)
Declare @DateAdded char(10)
Declare @RunAvailable float
Declare @RunBalance   float
Declare @RunRedeemed   float
Declare @RunBalanceNew char(8)
Declare @RunAvailableNew char(8)
DECLARE @CityState NVARCHAR(40)

declare @firstname varchar(255)
declare @newfullname varchar(255)

declare @justfirstname varchar(255)


declare @delimiterpos int
declare @delimiterpos2 int


Declare CST_CRSR  cursor fast_forward
for Select *
From Input_Customer

Open CST_CRSR 


Fetch CST_CRSR  
into 
 @MEMBERNUM , @filler1 , @ACCTID , @filler2 , @ACCTTYPE , @FILLER3 , @STATUS , @FILLER4 , @COLL , @FILLER5 ,
 @ACCTNAME1 , @address1 , @address2 , @FILLER6 , @City , @State , @FILLER7 , @Zip , @FILLER8 , @HOMEPHONE , @TIPNUMBER

 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


while @@FETCH_STATUS = 0
begin 
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

set @counter = @counter + 1
print '@counter'
print @counter

if @counter > 500
GoTo EndPROC

--set @name = 'BISHOP III/ROBERT L.'
set @name = @ACCTNAME1

set @delimiterpos = charindex('/', @name)

print 'name start'
print @name
print  'delimiterpos'
print  @delimiterpos

set @lastname = left(@name, @delimiterpos - 1)

print 'lastname'
print @lastname



set @firstname = right(@name, len(@name) - @delimiterpos2)

print '@firstname 1'
print @firstname

set @delimiterpos2 = charindex(' ', @firstname)
if @delimiterpos2 != 0
BEGIN
     set @firstname = left(@firstname, @delimiterpos2 - 1)
END

print 'firstname'
print @firstname

set @newfullname = (@firstname + ' ' + @lastname)

print 'newfullname'
print @newfullname

-------------

--set @delimiterpos2 = charindex(' ', @firstname)
--select @delimiterpos2

--set @justfirstname = left(@firstname, @delimiterpos2 - 1)

--select @justfirstname

--select @justfirstname + ' ' + @lastname


update CUSTOMER_Stage
set 
CUSTOMER_Stage.acctname1 = LEFT(@newfullname,40),
CUSTOMER_Stage.lastname = LEFT(@lastname,40) 
WHERE tipnumber = @TIPNUMBER 



FETCH_NEXT:
 	 
     

Fetch CST_CRSR  
into 
 @MEMBERNUM , @filler1 , @ACCTID , @filler2 , @ACCTTYPE , @FILLER3 , @STATUS , @FILLER4 , @COLL , @FILLER5 ,
 @ACCTNAME1 , @address1 , @address2 , @FILLER6 , @City , @State , @FILLER7 , @Zip , @FILLER8 , @HOMEPHONE , @TIPNUMBER
 
 
END 



	




 
GoTo EndPROC
Fetch_Error:
Print 'Fetch Error'
EndPROC:
close  CST_CRSR
deallocate CST_CRSR

---- Code used to create proc

--declare @name varchar(255)
--declare @lastname varchar(255)
--declare @firstname varchar(255)
--declare @newfullname varchar(255)

--declare @justfirstname varchar(255)


--declare @delimiterpos int
--declare @delimiterpos2 int

--set @name = 'BISHOP III/ROBERT L.'

--set @delimiterpos = charindex('/', @name)

--select @name, @delimiterpos

--set @lastname = left(@name, @delimiterpos - 1)

--select @lastname

--set @firstname = right(@name, len(@name) - @delimiterpos)

--set @delimiterpos2 = charindex(' ', @firstname)
--if @delimiterpos2 != 0
--BEGIN
--     set @firstname = left(@firstname, @delimiterpos2 - 1)
--END

--select @firstname

--set @newfullname = @firstname + ' ' + @lastname

--select @newfullname

-------------

--set @delimiterpos2 = charindex(' ', @firstname)
--select @delimiterpos2

--set @justfirstname = left(@firstname, @delimiterpos2 - 1)

--select @justfirstname

--select @justfirstname + ' ' + @lastname
GO
