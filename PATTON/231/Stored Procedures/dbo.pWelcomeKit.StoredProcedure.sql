USE [231]
GO
/****** Object:  StoredProcedure [dbo].[pWelcomeKit]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[pWelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[pWelcomeKit] @BEGDate varchar(10), @TipFirst nchar(3) AS 
--declare @BEGDate varchar(10)
--declare @TipFirst nchar(3)
--set @BEGDate = '2010-01-01'
--set @TipFirst = '231'

declare @EndDate datetime
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @EndDate = cast(@BEGDate as datetime)
set @EndDate = Dateadd(month, 1, @BEGDate)
set @EndDate = convert(nvarchar(25),(Dateadd(millisecond, -3, @EndDate)),121)




set @DBName=(SELECT  rtrim(DBNamePatton) from [PATTON\RN].[RewardsNOW].[dbo].DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode, Dateadded FROM ' + QuoteName(@DBName) + N' .dbo.customer WHERE (STATUS <> ''C'' and DATEADDED between @begDate AND @endDate ) '
exec sp_executesql @SQLInsert, N'@begdate varchar(10),@Enddate datetime',@begdate=@begdate,@Enddate=@Enddate
GO
