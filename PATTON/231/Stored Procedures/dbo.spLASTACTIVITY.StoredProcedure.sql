USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spLASTACTIVITY]    Script Date: 06/22/2010 13:49:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With The Debit  ACCT Info     */
/*  - Update DCCUST        */
/* BY:  B.QUINN  */
/* DATE: 10/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

 
CREATE PROCEDURE [dbo].[spLASTACTIVITY]   AS 
 
Declare @DateOfLastActivity  datetime
set @DateOfLastActivity = getdate()
set @DateOfLastActivity = '2009-09-30'
set @DateOfLastActivity = convert(nvarchar(25),(Dateadd(day, -90, @DateOfLastActivity)),121)
print '@DateOfLastActivity'
print @DateOfLastActivity

truncate table LASTACTIVITY
drop table zztip
drop table zztip1

select tipnumber into zztip from customer 

select tipnumber,max(histdate) as LastActivityDate  into zztip1 from history 
where tipnumber in (select tipnumber from zztip) group by tipnumber order by LastActivityDate

insert into LASTACTIVITY
(tipnumber,LastActivityDate)
select 
 tipnumber,LastActivityDate  
from zztip1
where tipnumber not in (select tipnumber from LASTACTIVITY)

 
update LASTACTIVITY   
set 
 LASTACTIVITY.currentpoints = c.runbalance,
 LASTACTIVITY.acctname1 = c.acctname1, 
 LASTACTIVITY.acctname2 = c.acctname2,
 LASTACTIVITY.address1 = c.address1,
 LASTACTIVITY.address2 = c.address1,
 LASTACTIVITY.city = c.city,
 LASTACTIVITY.state = c.state,
 LASTACTIVITY.zipcode = c.zipcode 
from customer as c
join LASTACTIVITY as la
on la.tipnumber = c.tipnumber

update LASTACTIVITY   
set 
 LASTACTIVITY.acctid = left(a.Acctid,16)
from Affiliat as A
join LASTACTIVITY as la
on la.tipnumber = a.tipnumber

update LASTACTIVITY   
set 
 AcctToBeDeleted = 'y'
where lastactivitydate <= @DateOfLastActivity

delete from LASTACTIVITY where AcctToBeDeleted is null




--select * from LASTACTIVITY where AcctToBeDeleted = 'y' order by lastactivitydate asc
--select sum(currentpoints) from LASTACTIVITY where AcctToBeDeleted = 'y' order by lastactivitydate asc
GO
