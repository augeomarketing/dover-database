USE [231]
GO
/****** Object:  StoredProcedure [dbo].[usp_BonusEmailStatement]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[usp_BonusEmailStatement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_BonusEmailStatement]
            @Tipfirst               varchar(3),
            @ProcessingDate         date,
            @Trancode               varchar(2),
            @BonusPoints            int

AS

declare @sql                nvarchar(max)
declare @dbweb              nvarchar(50)
declare @dbpatton           nvarchar(50)
declare @webserver          nvarchar(50)

declare @trantypedesc       varchar(50)
declare @ratio              int

select @trantypedesc = [description], @ratio = ratio
from rewardsnow.dbo.trantype 
where trancode = @trancode



select  @dbweb = quotename(dbnamenexl),
        @dbpatton = quotename(dbnamepatton)
from rewardsnow.dbo.dbprocessinfo
where dbnumber = @tipfirst

--
-- Create temp table of tips with e-statement only notifications
create table #who_has_estmts
    (tipnumber      varchar(15) primary key)

--
-- retrieve list of tips from web database
set @sql = '
            insert into #who_has_estmts
            select tipnumber
            from [rn1].' + @dbweb + '.dbo.[1security]
            where left(tipnumber,3) = ' + char(39) + @tipfirst + char(39) + ' 
            and EmailStatement = ''y''
            and email is not null
            and (password is not null or thislogin is not null)'

exec sp_executesql @sql

--
-- remove tips that have already been awarded an estatement bonus
set @sql = '
            delete tmp
            from #who_has_estmts tmp join (select tipnumber
                                           from ' + @dbpatton + '.dbo.history
                                           where trancode = ' + char(39) + @trancode + char(39) + '
                                           union
                                           select tipnumber
                                           from ' + @dbpatton + '.dbo.history_stage
                                           where trancode = ' + char(39) + @trancode + char(39) + ') hs
                on tmp.tipnumber = hs.tipnumber'

exec sp_executesql @sql


--
-- Award tips with bonus

--
-- Insert bonus transactions into history_stage
set @sql = '
            insert into ' + @dbpatton + '.dbo.history_stage
            (TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, description, SECID, Ratio, Overage)
            select tipnumber, '''', @processingdate, @trancode, 1, @BonusPoints, @trantypedesc, ''NEW'', @ratio, 0
            from #who_has_estmts'

exec sp_executesql @sql, N'@processingdate date, @trancode varchar(2), @bonuspoints int, @trantypedesc varchar(50), @ratio int',
                         @processingdate = @processingdate, @trancode = @trancode, @bonuspoints = @bonuspoints, 
                            @trantypedesc = @trantypedesc, @ratio = @ratio


--
-- Update runavaliableNew in customer stage with bonus points
set @sql = '
            update cs
            set  RunAvaliableNew = RunAvaliableNew + hs.pts
            FROM ' + @dbpatton + '.dbo.CUSTOMER_Stage CS join (select tipnumber, sum(points * ratio) pts
                                             from ' + @dbpatton + '.dbo.history_stage
                                             where trancode = @trancode
                                             and histdate = @processingdate
                                             group by tipnumber) hs
                on cs.tipnumber = hs.tipnumber
            join #who_has_estmts tmp
                on cs.tipnumber = tmp.tipnumber'

exec sp_executesql @sql, N'@trancode varchar(2), @processingdate date', 
                         @trancode = @trancode, @processingdate = @processingdate


/*  TEST HARNESS

exec dbo.usp_BonusEmailStatement '231', '10/31/2011', 'BE', 1000



select *
from dbo.history_stage
where trancode = 'be'
and histdate= '01/31/2011'



*/
GO
