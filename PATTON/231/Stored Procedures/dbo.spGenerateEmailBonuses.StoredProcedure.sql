USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateEmailBonuses]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spGenerateEmailBonuses]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--PROCEDURE HAS BEEN RE WRITTEN TO USE JOINS FOR UPDATING CUSTOMER_STAGE AND HISTORY_STAGE
create PROCEDURE [dbo].[spGenerateEmailBonuses] @monthendDate nvarchar(10), @TRANCODE VARCHAR(2), @Points numeric(9)  AS 
--declare @monthendDate nvarchar(8)

Declare @TIPNUMBER varchar(15)
declare  @Acctid varchar(25)
 
 


--   BEGIN TEST PARAMETERS

--DECLARE @ProcessDATE nvarchar(10) = '2010-03-31'
--DECLARE @TRANCODE VARCHAR(2)
--DECLARE @POINTS NVARCHAR(4)
--SET @TRANCODE = 'BE'
--SET @POINTS = '1000'

-- END TEST PARAMETERS



if OBJECT_ID('tempdb..#Temp_EmailBonus') is not null
	drop table #Temp_EmailBonus
	
create table #Temp_EmailBonus
	(Tipnumber nvarchar(15))
	 
INSERT INTO #Temp_EmailBonus
	(Tipnumber )
SELECT
TIPNUMBER
FROM HISTORY 
WHERE 
TRANCODE = 'BE' 

update CUSTOMER_Stage
set  RunAvaliableNew = '0'
WHERE RunAvaliableNew IS NULL


DELETE FROM EMLSTMT WHERE rtrim(tipnumber) IN
(SELECT rtrim(TIPNUMBER) FROM #Temp_EmailBonus)


update CUSTOMER_Stage
set  RunAvaliableNew = (RunAvaliableNew + @POINTS)
FROM CUSTOMER_Stage AS CS 
JOIN dbo.EMLSTMT AS  IB
ON rtrim(ltrim(ib.tipnumber)) = rtrim(ltrim(CS.TIPNUMBER))



Insert into HISTORY_Stage
(TIPNUMBER,ACCTID,HISTDATE,TRANCODE,TranCount,POINTS,Description,SECID,Ratio,OVERAGE)
select
TIPNUMBER,
' ',
@monthendDate,
@TRANCODE,
'1',
@POINTS,
'BONUS EMAIL STATEMENT',
'NEW',
'1',
'0'
from dbo.EMLSTMT
GO
