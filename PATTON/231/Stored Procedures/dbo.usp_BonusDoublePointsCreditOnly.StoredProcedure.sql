USE [231]
GO
/****** Object:  StoredProcedure [dbo].[usp_BonusDoublePointsCreditOnly]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[usp_BonusDoublePointsCreditOnly]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_BonusDoublePointsCreditOnly]
                @trancode               varchar(2),
                @pointmultiplier        numeric(5,2),
                @processingdate         date

as

declare @trantypedesc       varchar(50)
declare @ratio              int

select @trantypedesc = [description], @ratio = ratio
from rewardsnow.dbo.trantype 
where trancode = @trancode

--
-- Insert bonus transactions into history_stage
insert into dbo.history_stage
(TIPNUMBER, ACCTID, HISTDATE, TRANCODE, TranCount, POINTS, description, SECID, Ratio, Overage)
select tipnumber, acctid, @processingdate, @trancode, 1, sum(hs.points * hs.ratio) * @pointmultiplier points, 
            @trantypedesc, 'NEW', @ratio, 0
from dbo.history_stage hs 
where hs.trancode in ('63', '33')
and histdate = @processingdate
group by tipnumber, acctid


--
-- Update runavaliableNew in customer stage with bonus points
update cs
set  RunAvaliableNew = RunAvaliableNew + hs.pts
FROM dbo.CUSTOMER_Stage CS join (select tipnumber, sum(points * ratio) pts
                                 from dbo.history_stage
                                 where trancode = @trancode
                                 and histdate = @processingdate
                                 group by tipnumber) hs
    on cs.tipnumber = hs.tipnumber


/* Test Harness

delete from dbo.history_stage where trancode = 'BM' and histdate = '01/31/2011'

exec dbo.usp_BonusDoublePointsCreditOnly 'BM', 1.00, '01/31/2011'


select *
from dbo.history_stage
where trancode = 'bm'
and histdate = '01/31/2011'


select sum(points * ratio)
from dbo.history_stage
where trancode = 'bm'
and histdate = '01/31/2011'


select sum(points * ratio)
from dbo.history_stage
where trancode in ('63', '33')
and histdate = '01/31/2011'


*/
GO
