USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spAddNamesReturnedFromPersonator]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spAddNamesReturnedFromPersonator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With NAMES PROCESSED BY PERSONATOR       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spAddNamesReturnedFromPersonator] AS
/* input */

 

BEGIN 
	

	update nameprs
	set
	FIRSTNAME = ' '
	where FIRSTNAME is null

	update nameprs
	set
	lastname = ' '
	where lastname is null
	
	update nameprs
	set
	MIDDLENAME = ' '
	where MIDDLENAME is null

	
	update CUSTOMER_STAGE	
	set	         
	CUSTOMER_STAGE.LASTNAME = pr.LASTNAME
	,CUSTOMER_STAGE.acctname1 = ( RTRIM(pr.firstname) + ' ' +  RTRIM(PR.MIDDLENAME) + ' ' + RTRIM(pr.LASTNAME))    
	from dbo.nameprs as pr
	inner join dbo.CUSTOMER_STAGE as CS
	on pr.tipnumber = CS.tipnumber 	     
	where pr.tipnumber in (select CS.tipnumber from CUSTOMER_STAGE)
	and pr.LASTNAME not like '%trust%'

	update CUSTOMER_STAGE	
	set	         
	CUSTOMER_STAGE.LASTNAME = pr.LASTNAME
	,CUSTOMER_STAGE.acctname1 = ( RTRIM(pr.firstname) + ' ' +  RTRIM(PR.MIDDLENAME) + ' ' + RTRIM(pr.LASTNAME))    
	from dbo.nameprs as pr
	inner join dbo.CUSTOMER_STAGE as CS
	on pr.tipnumber = CS.tipnumber 	     
	where pr.tipnumber in (select CS.tipnumber from CUSTOMER_STAGE)
	and pr.LASTNAME  like '%trust%'	   

	update affiliat_STAGE	
	set	         
	affiliat_STAGE.LASTNAME = c.LASTNAME
    from  dbo.CUSTOMER_STAGE as C
	inner join affiliat_STAGE as A
	on a.tipnumber = C.tipnumber 	     
	where c.tipnumber in (select a.tipnumber from affiliat_STAGE)
 

END  
EndPROC:
GO
