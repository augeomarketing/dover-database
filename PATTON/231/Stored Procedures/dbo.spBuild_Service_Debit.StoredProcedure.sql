USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spBuild_Service_Debit]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spBuild_Service_Debit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spBuild_Service_Debit] AS    


DECLARE @counter int
declare @Input_Rec varchar (80)

declare @out_Rec1 varchar (80)
declare @out_Rec2 varchar (80)
declare @out_Rec3 varchar (80)
declare @out_Rec4 varchar (80)
declare @empty_rec varchar (81)
declare @rec_length int
declare @bigrec varchar (161)


declare @Output_Rec nvarchar(320)

truncate table service_work2

/*   - DECLARE CURSOR AND OPEN TABLES  */


Declare SCU_CRSR  cursor fast_forward
for Select  ltrim(rtrim(Service_work_rec)) 
From Service_work1 

Open SCU_CRSR 
 
Fetch SCU_CRSR  
into 	@Input_Rec
 	
set @Counter = 0	 

/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN
	
set @Counter = @Counter + 1

--set @empty_rec = ('                                                                                1')
set @empty_rec = REPLICATE(' ', 80) --+ '1'

set @bigrec = ''  --= @empty_rec + @empty_rec

IF LEFT(@Input_Rec,1) = 1
BEGIN

	set @out_Rec1 = left(@Input_Rec + @empty_rec, 80)

END	

IF LEFT(@Input_Rec,1) = 2
BEGIN

	set @out_Rec2 = left(@Input_Rec + @empty_rec, 80)

END	

IF LEFT(@Input_Rec,1) = 3
BEGIN

	set @out_Rec3 = left(@Input_Rec + @empty_rec, 80)

END	

IF LEFT(@Input_Rec,1) = 4
BEGIN

	set @out_Rec4 = left(@Input_Rec + @empty_rec, 80)

END	


if @counter = 4
begin

print len( @out_Rec1 + @out_Rec2 + @out_Rec3 + @out_Rec4 )
print datalength(@out_Rec1 + @out_Rec2 + @out_Rec3 + @out_Rec4 )

print @out_Rec1 + @out_Rec2 + @out_Rec3 + @out_Rec4
print ' '
print ' '

	insert into service_work2
	(Service_work_rec2) 
	VALUES 
	( (@out_Rec1 + @out_Rec2 + @out_Rec3 + @out_Rec4))

	set @out_Rec1 = ''
	set @out_Rec2 = ''
	set @out_Rec3 = ''
	set @out_Rec4 = ''

	set @Counter = 0
end
	

FETCH_NEXT:

Fetch SCU_CRSR  
into 
	@Input_Rec

END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  SCU_CRSR 
deallocate  SCU_CRSR
GO
