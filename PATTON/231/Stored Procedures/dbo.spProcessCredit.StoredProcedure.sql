USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spProcessCredit]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spProcessCredit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spProcessCredit] @Rundate nvarchar(10) AS    

/* Test Data */
--declare @Rundate nvarchar(10)  
--set  @Rundate = '11/30/2009' 
declare @POSTDATE nvarchar(10) 
declare @TranamtPur  numeric(9,2) 
declare @TrancntPur  numeric(9) 
declare @TranamtRet  numeric(9,2)
declare @Trancntret  numeric(9)   
declare @TransactSign  char (1) 
Declare @TIPNUMBER char(15)
declare @Multiplier float   
Declare @TRANDESC char(40)
Declare @INSTID char(10)
Declare @LASTNAME char(40)
Declare @DDA char(20)
Declare @DateAdded char(10)
Declare @CARDLASTSIX char(6)
Declare @STATUS char(1)
Declare @RunAvailableNew int
Declare @POINTS int
Declare @YTDEarned int
Declare @MAXPOINTSPERYEAR numeric(10)
declare @PurchamtN int
declare @Result  int
declare @ResultPur int
declare @ResultRet int
Declare @TRANCODE nvarchar(2)
Declare @RATIO nvarchar(2)
Declare @MEMBERNUMBER nvarchar(10)
Declare @ACCTTYPE nvarchar(1)
Declare @COLL nvarchar (3)
Declare @TRANCNT nvarchar (4)
Declare @AMOUNT nvarchar(13)
Declare @CARDTYPE nvarchar (13)
Declare @SECID nvarchar (13)  


/*   - DECLARE CURSOR AND OPEN TABLES  */

Declare SCU_CRSR  cursor 
for Select
SID_INPUTTRANSACTION_MEMBERNUMBER 
,DIM_INPUTTRANSACTION_ACCTTYPE 
,DIM_INPUTTRANSACTION_COLL  
,DIM_INPUTTRANSACTION_TRANCNT 
,DIM_INPUTTRANSACTION_AMOUNT 
,DIM_INPUTTRANSACTION_CARDTYPE  
,DIM_INPUTTRANSACTION_TIPNUMBER  
From INPUT_TRANSACTION 

Open SCU_CRSR 
/*                  */



Fetch SCU_CRSR  
into  @MEMBERNUMBER, @ACCTTYPE, @COLL, @TRANCNT, @AMOUNT, @CARDTYPE 
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	set @SECID = 'ServiceCreditUnion'
	SET @DateAdded = @POSTDATE 	
	SET @YTDEarned = 0
	

while @@FETCH_STATUS = 0
BEGIN

if @CARDTYPE != 'CREDIT'

	SET @RunAvailableNew = '0'
                                  

	set @RESULTPur = '0'
	set @RESULTRet = '0'
	set @RESULT = '0'
	set @Multiplier = '0'



 
	if @AMOUNT > '0'		   
	   Begin
	    SET @TranCode = '63'      		      
	   end

	if @AMOUNT < '0'		   
	   Begin
	    SET @TranCode = '33'      		      
	   end
	   
  	Select 
	  @Multiplier = Multiplier
	From Trancode_Factor
	where
	  @TranCode = '33'
	  
	  	If @Multiplier is null
	   set @Multiplier = '1.0'


	set @Multiplier = '0'

  	Select 
	  @Multiplier = Multiplier
	From Trancode_Factor
	where
	  TranCode = @TranCode



	If @Multiplier is null
	   set @Multiplier = '1.0'

	if @tranamtpur > '0'
	begin
	  set @RESULTPur =  (@tranamtpur * @Multiplier) 
	  set @RESULTPur = round(@RESULTPur, 0)
	    Begin
	      set @RunAvailableNew = @RunAvailableNew + @RESULTPur     		      
	    End  
	end



/*  UPDATE THE CUSTOMER RECORD WITH THE debit TRANSACTION DATA          */


		Update Customer_Stage
		Set 
		RunAvaliableNew = @RunAvailablenew  + RunAvaliableNew
		Where @TipNumber = Customer_Stage.TIPNUMBER


/*  				- Create HISTORY     			   */
/*  		ADD HISTORY RECORD FOR PURCHASES     			   */



  		   Select 
		      @TRANDESC = Description
	           From [RewardsNOW].[dbo].TranType
		   where
			TranCode = '63'
			
			
	       set @POINTS =  @RESULTPur 
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	       ,@CARDTYPE
	       ,@RunDate
	       ,'63'
	       ,@tranCntpur
	       ,@POINTS
		   ,@TRANDESC	            
	       ,'NEW'
		   ,'1'
	       ,'0'
	            )
	         END	        
	 
		IF @RESULTPur <> '0'
	        BEGIN
  		   Select 
		      @TRANDESC = Description
	           From [RewardsNOW].[dbo].TranType
		   where
		   TranCode = '63'
	           set @POINTS =  @RESULTPur 
		   print 'tran desc'
	           print  @TRANDESC 
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	       ,@MEMBERNUMBER
	       ,@RunDate
	       ,'63'
	       ,@tranCntpur
	       ,@POINTS
		   ,@TRANDESC	            
	       ,'NEW'
		   ,'1'
	       ,'0'
	            )
		End







/* Post Returns    */




		IF @RESULTRet <> '0'        
		Begin	
  		   Select 
		      @TRANDESC = Description
	           From [RewardsNOW].[dbo].TranType
		   where
		   TranCode = '33'
	           set @POINTS =  @RESULTRet
		   print 'tran desc'
	           print  @TRANDESC   
		   Insert into HISTORY_Stage
		   (
		      TIPNUMBER
		      ,ACCTID
		      ,HISTDATE
		      ,TRANCODE
		      ,TranCount
		      ,POINTS
		      ,Description
		      ,SECID
		      ,Ratio
		      ,OVERAGE
		   )
		   Values
		   (
		    @TIPNUMBER
	            ,@MEMBERNUMBER
	            ,@RunDate
	            ,'33'
	            ,@tranCntret
	            ,@POINTS
		    ,@TRANDESC	            
	            ,'NEW'
		    ,'-1'
	            ,'0'
	            )
	 
		 


	set @RESULT = '0'


 
        

FETCH_NEXT:


	
	Fetch SCU_CRSR   
	into  @MEMBERNUMBER, @ACCTTYPE, @COLL, @TRANCNT, @AMOUNT, @CARDTYPE 
		
END /*while */


	 

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  SCU_CRSR 
deallocate  SCU_CRSR
GO
