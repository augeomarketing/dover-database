USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spCreateCustomer]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spCreateCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateCustomer] @ProcessDATE nvarchar(10) AS    
declare @counter numeric(9)
set @counter = '0'
--declare @ProcessDATE nvarchar(10)  
--set  @ProcessDATE = '03/15/2010'

Declare @MEMBERNUM nvarchar(15)
Declare @filler1 nvarchar(1)
Declare @acctid nvarchar(16)
Declare @filler2 nvarchar(4)
Declare @ACCTTYPE nvarchar(1)
Declare @filler3 nvarchar(4)
Declare @STATUS nvarchar(1)
Declare @filler4 nvarchar(2)
Declare @COLL nvarchar(3)
Declare @filler5 nvarchar(5)
Declare @ACCTNAME1 char(40)
Declare @address1 char(40)
Declare @address2 char(40)
Declare @filler6 nvarchar(10)
Declare @City char(40)
Declare @State char(2)
Declare @filler7 nvarchar(5)
Declare @Zip char(15)
Declare @filler8 nvarchar(2)
Declare @homephone char(10)
Declare @TIPNUMBER NUMERIC(15)
Declare @custnum nvarchar(9)
Declare @LASTNAME char(40)
Declare @NAME1 nvarchar(40)
Declare @NAME nvarchar(40)

DECLARE @AcctTypeDesc NVARCHAR(20)
Declare @ACCTNAME2 char(40)
Declare @ACCTNAME3 char(40)
Declare @ACCTNAME4 char(40)
Declare @address3 char(40)
Declare @address4 char(40)
Declare @Zipplus4 char(5)
Declare @STATUSDESCRIPTION char(40)
Declare @workphone char(10)
Declare @DateAdded char(10)
Declare @RunAvailable float
Declare @RunBalance   float
Declare @RunRedeemed   float
Declare @RunBalanceNew char(8)
Declare @RunAvailableNew char(8)
DECLARE @CityState NVARCHAR(40)


Declare CST_CRSR  cursor fast_forward
for Select *
From Input_Customer

Open CST_CRSR 


Fetch CST_CRSR  
into 
 @MEMBERNUM , @filler1 , @ACCTID , @filler2 , @ACCTTYPE , @FILLER3 , @STATUS , @FILLER4 , @COLL , @FILLER5 ,
 @ACCTNAME1 , @address1 , @address2 , @FILLER6 , @City , @State , @FILLER7 , @Zip , @FILLER8 , @HOMEPHONE , @TIPNUMBER

 	
IF @@FETCH_STATUS = 1
	goto Fetch_Error


while @@FETCH_STATUS = 0
begin 
 	
  
	  	
	
IF @@FETCH_STATUS = 1
	goto Fetch_Error

set @counter = @counter + 1
print '@counter'
print @counter

	SET @DateAdded = @ProcessDATE 	
	SET @RunBalanceNew = 0		
	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0                                                                          

IF @STATUS = '1'
BEGIN
SET @STATUS = 'A'
end


	SET @RUNBALANCE = 0		
	SET @RunAvailable = 0		
	SET @RunRedeemed = 0
	set @CityState = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address3 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))
	set @Address4 = (rtrim(@City)+' '+rtrim(@State)+' '+rtrim(@Zip))



	--select @STATUSDESCRIPTION = statusDescription
	--from [rewardsnow].dbo.status
	--where 
	--status = @STATUS




 	if exists (select dbo.Customer_Stage.TIPNUMBER from dbo.Customer_Stage 
                    where dbo.Customer_Stage.TIPNUMBER = @TipNumber)	
     Begin	
     PRINT 'UPDATE'
     PRINT @TIPNUMBER
		Update Customer_Stage
		Set 
	        ACCTNAME1 = @ACCTNAME1 
	       ,lastname = @lastname 
	       ,ADDRESS1 = @Address1 ,ADDRESS2 = @Address2
		   ,ADDRESS3 = ' ' ,ADDRESS4 = ' '  ,CITY = @CITY ,STATE = @STATE  
	       ,ZIPCODE = @ZIP      
		Where @TipNumber = Customer_Stage.TIPNUMBER
	 END		
	else
		BEGIN	
		PRINT 'ADD'	
		PRINT @TIPNUMBER
		Insert into CUSTOMER_Stage  
		(	
		LASTNAME ,ACCTNAME1  ,ACCTNAME2 ,ACCTNAME3	,ADDRESS1 ,ADDRESS2 ,ADDRESS3 ,ADDRESS4,     
		 CITY ,STATE ,ZIPCODE ,HOMEPHONE ,MISC1	,MISC2	,MISC3 	,MISC4	,MISC5,    		
		 DATEADDED ,RUNAVAILABLE ,RUNBALANCE ,RUNREDEEMED ,TIPNUMBER ,STATUS ,TIPFIRST,
		 TIPLAST ,StatusDescription	,laststmtdate ,nextstmtdate	,acctname4,	
		 businessflag ,employeeflag	,segmentcode ,combostmt	,rewardsonline ,notes ,runavaliablenew
	      )		
		values
		 (
		@Lastname, @ACCTNAME1,@ACCTNAME2,@ACCTNAME3, @address1, @address2,@address3, @address4,	
 		--@City, @State, @Zip, @HOMEPHONE, ' ', ' ', ' ', ' ', ' ',
 		@City, @State, @Zip, right(@HOMEPHONE,10), ' ', ' ', ' ', ' ', @MEMBERNUM,
 		@ProcessDATE, @RunAvailable, @RunBalance, @RunRedeemed,	@TIPNUMBER, @STATUS,
		left(@TipNumber,3), right(@TipNumber,12), @STATUSDESCRIPTION,
		' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '0'	
	         )
	   END

IF @accttype = 'D'
BEGIN 
SET @AcctTypeDesc = 'DEBIT'
END
IF @accttype = 'F'
BEGIN 
SET @AcctTypeDesc = 'CREDIT'
END

print 'acctid'
print @acctid


 	if NOT exists (select dbo.AFFILIAT_Stage.custid from dbo.AFFILIAT_Stage      
                   where dbo.AFFILIAT_Stage.custid = @MEMBERNUM AND
                   dbo.AFFILIAT_Stage.ACCTID = @acctid )             
     Begin	
     print 'adding acctid'
     print @acctid
     Insert into AFFILIAT_stage
            ( ACCTID, TipNumber, LastName,  DateAdded, AcctStatus,
	         YTDEarned, CustID, AcctType, AcctTypeDesc )
	        values
 		    ( @acctid, @TipNumber, ' ', @ProcessDATE, 'A',
		     '0', @MEMBERNUM,  @accttype, @AcctTypeDesc )
	 END

		



 	  if not exists (select dbo.beginning_balance_table.TIPNUMBER from dbo.beginning_balance_table 
         where dbo.beginning_balance_table.TIPNUMBER = @TipNumber)
	  begin
	  PRINT 'adding tip to beg balance'
	  print @TipNumber
		  insert into beginning_balance_table
		  ( tipnumber,
		    Monthbeg1, Monthbeg2, Monthbeg3, Monthbeg4, Monthbeg5, Monthbeg6, 
		    Monthbeg7, Monthbeg8, Monthbeg9, Monthbeg10, Monthbeg11, Monthbeg12 
		  )
		  values
		  (@TIPNUMBER, '0','0','0','0','0','0','0','0','0','0','0','0')
	  end



FETCH_NEXT:
 	 
     

Fetch CST_CRSR  
into 
 @MEMBERNUM , @filler1 , @ACCTID , @filler2 , @ACCTTYPE , @FILLER3 , @STATUS , @FILLER4 , @COLL , @FILLER5 ,
 @ACCTNAME1 , @address1 , @address2 , @FILLER6 , @City , @State , @FILLER7 , @Zip , @FILLER8 , @HOMEPHONE , @TIPNUMBER
 
 
END 



	




 
GoTo EndPROC
Fetch_Error:
Print 'Fetch Error'
EndPROC:
close  CST_CRSR
deallocate CST_CRSR
GO
