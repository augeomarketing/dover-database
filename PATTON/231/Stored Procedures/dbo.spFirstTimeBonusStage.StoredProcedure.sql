USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spFirstTimeBonusStage]    Script Date: 06/22/2010 13:49:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spFirstTimeBonusStage]  @EndDateParm varchar(10) AS 

/****************************************************************************/
/*                                                                                                              */
/*   Procedure to generate the Activation bonus based on first use          */
/*                                                                                                              */
/*                                                                                                             */
/****************************************************************************/

--declare @EndDateParm varchar(10)
declare @enddate datetime


--set @Startdate = convert(datetime, @StartDateparm  + ' 00:00:00:001')    
set @Enddate = convert(datetime, @EndDateparm +' 00:00:00:000' ) 

declare @BonusPoints numeric(9)
declare @Tipnumber char(15)
declare @Trandate datetime
declare @Acctid char(16)
declare @ProcessFlag char(1)
declare @membernumber varchar(9)
 
set @Trandate=@EndDate
set @bonuspoints='500'
	
/*                                                                            */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor 
for select distinct DIM_INPUTTRANSACTION_TIPNUMBER
from dbo.INPUT_TRANSACTION 
where DIM_INPUTTRANSACTION_TRANCODE in ('63', '67') 

/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber

/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	
		set @ProcessFlag='N'		
		if not exists(select * from OneTimeBonuses_stage where tipnumber=@tipnumber AND trancode='BF')
		Begin 
			set @ProcessFlag='Y' 
		End 	  

		if @ProcessFlag='Y'
		Begin
 
			INSERT INTO HISTORY_Stage (TipNumber,acctid,histdate,TranCode,TranCount,Points,Description,SecID,Ratio,Overage)
        		                Values(@Tipnumber, @membernumber,@Trandate, 'BF', '1', @BonusPoints, 'Bonus for First Trans Activity', 'NEW','1','0')
        --Values(@Tipnumber, @Trandate, 'BF', '1', @BonusPoints, '1', 'Bonus for First Trans Activity', 'NEW','0')  
 
			INSERT INTO OneTimeBonuses_stage(TipNumber,TranCode, acctid, DateAwarded)
        		Values(@Tipnumber, 'BF', @membernumber, @Trandate)

		End

		goto Next_Record
Next_Record:
		fetch tip_crsr into @tipnumber
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
