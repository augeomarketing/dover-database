USE [231]
GO
/****** Object:  StoredProcedure [dbo].[spUPDATE_MEMBERNUMBERS]    Script Date: 08/17/2012 13:24:57 ******/
DROP PROCEDURE [dbo].[spUPDATE_MEMBERNUMBERS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This will Update the CUST File With NAMES PROCESSED BY PERSONATOR       */
/*  - Update CCCUST        */
/* BY:  B.QUINN  */
/* DATE: 7/2006   */
/* REVISION: 0 */
/* */
/******************************************************************************/

CREATE PROCEDURE [dbo].[spUPDATE_MEMBERNUMBERS] AS

update cs
set MISC5 = ltrim(rtrim(IMR.SID_INPUT_MEMBERNUM_REPLACEMENT_NEW_MEMBERNUM))
FROM INPUT_MEMBERNUM_REPLACEMENT IMR INNER JOIN CUSTOMER_Stage CS
    ON ltrim(rtrim(IMR.DIM_INPUT_MEMBERNUM_REPLACEMENT_OLD_MEMBERNUM)) = CS.MISC5


update afs
    set CUSTID = ltrim(rtrim(IMR.SID_INPUT_MEMBERNUM_REPLACEMENT_NEW_MEMBERNUM))
FROM INPUT_MEMBERNUM_REPLACEMENT IMR JOIN AFFILIAT_Stage AFS
    ON ltrim(rtrim(IMR.DIM_INPUT_MEMBERNUM_REPLACEMENT_OLD_MEMBERNUM)) = AFS.CUSTID
GO
