/*
   Thursday, January 26, 20123:19:55 PM
   User: 
   Server: doolittle\rn
   Database: 231
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Beginning_Balance_Table
	(
	Tipnumber varchar(15) NOT NULL,
	MonthBeg1 int NOT NULL,
	MonthBeg2 int NOT NULL,
	MonthBeg3 int NOT NULL,
	MonthBeg4 int NOT NULL,
	MonthBeg5 int NOT NULL,
	MonthBeg6 int NOT NULL,
	MonthBeg7 int NOT NULL,
	MonthBeg8 int NOT NULL,
	MonthBeg9 int NOT NULL,
	MonthBeg10 int NOT NULL,
	MonthBeg11 int NOT NULL,
	MonthBeg12 int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg1 DEFAULT 0 FOR MonthBeg1
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg2 DEFAULT 0 FOR MonthBeg2
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg3 DEFAULT 0 FOR MonthBeg3
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg4 DEFAULT 0 FOR MonthBeg4
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg5 DEFAULT 0 FOR MonthBeg5
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg6 DEFAULT 0 FOR MonthBeg6
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg7 DEFAULT 0 FOR MonthBeg7
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg8 DEFAULT 0 FOR MonthBeg8
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg9 DEFAULT 0 FOR MonthBeg9
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg10 DEFAULT 0 FOR MonthBeg10
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg11 DEFAULT 0 FOR MonthBeg11
GO
ALTER TABLE dbo.Tmp_Beginning_Balance_Table ADD CONSTRAINT
	DF_Beginning_Balance_Table_MonthBeg12 DEFAULT 0 FOR MonthBeg12
GO
IF EXISTS(SELECT * FROM dbo.Beginning_Balance_Table)
	 EXEC('INSERT INTO dbo.Tmp_Beginning_Balance_Table (Tipnumber, MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12)
		SELECT CONVERT(varchar(15), Tipnumber), MonthBeg1, MonthBeg2, MonthBeg3, MonthBeg4, MonthBeg5, MonthBeg6, MonthBeg7, MonthBeg8, MonthBeg9, MonthBeg10, MonthBeg11, MonthBeg12 FROM dbo.Beginning_Balance_Table WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Beginning_Balance_Table
GO
EXECUTE sp_rename N'dbo.Tmp_Beginning_Balance_Table', N'Beginning_Balance_Table', 'OBJECT' 
GO
ALTER TABLE dbo.Beginning_Balance_Table ADD CONSTRAINT
	PK_Beginning_Balance_Table PRIMARY KEY CLUSTERED 
	(
	Tipnumber
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
