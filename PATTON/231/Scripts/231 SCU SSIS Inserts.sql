USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'231', N'Data Source=236722-sqlclus2\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-231_OPS_M01_Import_Stage_Audit-{E3400DC0-9405-4086-BFDF-76D6A3993D5B}doolittle\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'231', N'Data Source=236722-sqlclus2\rn;Initial Catalog=231;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-231_OPS_M01_Import_Stage_Audit-{1E40F5D8-0970-4B23-94F3-90F87F920B40}doolittle\rn.231;Auto Translate=False;', N'\Package.Connections[FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'231', N'231', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'231', N'06/30/2011', N'\Package.Variables[User::StartDate].Properties[Value]', N'String' UNION ALL
SELECT N'231', N'06/01/2011', N'\Package.Variables[User::EndDate].Properties[Value]', N'String' UNION ALL
SELECT N'231_OPS_M01_Import_Stage_Audit', N'\\patton\ops\231\Input\OLDACCT.txt', N'\Package.Connections[OLDACCT].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'231_OPS_M01_Import_Stage_Audit', N'\\patton\ops\231\Input\CUSTOMER.txt', N'\Package.Connections[Customer].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'231_OPS_M01_Import_Stage_Audit', N'\\patton\ops\231\Input\CREDIT.csv', N'\Package.Connections[CREDIT].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'231_OPS_M01_Import_Stage_Audit', N'\\patton\ops\231\Input\FISERV_Files\PDSERV01', N'\Package.Variables[User::strDebitFileNm].Properties[Value]', N'String' UNION ALL
SELECT N'231_OPS_M01_Import_Stage_Audit', N'\\patton\ops\231\input\archive\', N'\Package.Variables[User::FileArchivePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

