USE [231]
GO
/****** Object:  View [dbo].[vwHistory_Stage]    Script Date: 06/30/2011 10:29:45 ******/
DROP VIEW [dbo].[vwHistory_Stage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwHistory_Stage]
			 as
			 select case
				    when rewardsnow.dbo.fnCheckLuhn10(acctid) = 1 then left(ltrim(rtrim(acctid)),6) + replicate('x', len(acctid) - 10 ) + right(ltrim(rtrim(acctid)),4)  
				    else ACCTID
				    end as AcctID, 
				    TIPNUMBER, HISTDATE, TRANCODE, TranCount, POINTS, Description, SECID, Ratio, Overage
			 from [231].dbo.history_Stage
GO
