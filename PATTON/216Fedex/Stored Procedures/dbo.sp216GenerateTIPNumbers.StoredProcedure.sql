USE [216FedExEmployeesCA]
GO
/****** Object:  StoredProcedure [dbo].[sp216GenerateTIPNumbers]    Script Date: 01/07/2010 09:49:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp216GenerateTIPNumbers]
AS 
/****************************************************************************************/
/*	One Tipnumber for every card regardless of SSN or acctnumber                          */
/****************************************************************************************/
update roll_customer 
set tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.cardnumber = b.acctid

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,affiliat_stage b
where a.ssn = b.secid and (a.tipnumber is null or a.tipnumber = ' ')

Truncate table GenTip

insert into gentip (acctid, tipnumber)
select   distinct cardnumber as acctid, tipnumber	
from roll_customer where tipnumber is null or tipnumber = ' '

declare @newnum bigint

declare @LastTipUsed char(15)
exec rewardsnow.dbo.spGetLastTipNumberUsed 216, @LastTipUsed output
select @LastTipUsed as LastTipUsed

set @newnum = cast(@LastTipUsed as bigint) 


if @newnum is null or @newnum = 0
	begin
	set @newnum= 216000000000000
	end

update gentip 
   set	@newnum = (@newnum  + 1)
	,TIPNUMBER =  @newnum where tipnumber is null or tipnumber = ' '

exec RewardsNOW.dbo.spPutLastTipNumberUsed 216, @newnum


update roll_customer
set Tipnumber = b.tipnumber
from roll_customer a,gentip b
where a.cardnumber = b.acctid and (a.tipnumber is null or a.tipnumber = ' ')

/* Load Tipnumbers to Input_Transaction from roll_customer Table */
UPDATE Input_Transaction 
SET Input_Transaction.TIPNUMBER = roll_Customer.TIPNUMBER
FROM Input_Transaction, roll_Customer  WHERE Input_Transaction.cardnumber = roll_Customer.cardnumber
and Input_Transaction.TIPNUMBER  is NULL

/* Load Tipnumbers to Input_delete_accts from Input_customer Table */
UPDATE Input_delete_accts
SET Input_delete_accts.TIPNUMBER = roll_customer.tipnumber
FROM Input_delete_accts, roll_Customer  WHERE Input_delete_accts.cardnumber = roll_Customer.cardnumber