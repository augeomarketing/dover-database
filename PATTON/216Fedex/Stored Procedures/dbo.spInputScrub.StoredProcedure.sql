/****** Object:  StoredProcedure [dbo].[spInputScrub]    Script Date: 06/05/2009 09:50:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[spInputScrub] AS

declare @QCFlag varchar(1), @QCFlag1 varchar(1)
set @QCFlag = '0'
set @QCFlag1 = '0'

Truncate Table  Input_Customer_error
Truncate Table Input_Transaction_error

update Input_Customer
set [name1]=replace([name1],char(39), ' '), address1=replace(address1,char(39), ' '),
address2=replace(address2,char(39), ' '),   city=replace(city,char(39), ' '),  lastname=replace(lastname,char(39), ' ')

update Input_Customer
set [name1]=replace([name1],char(140), ' '), address1=replace(address1,char(140), ' '),
address2=replace(address2,char(140), ' '),   city=replace(city,char(140), ' '),  lastname=replace(lastname,char(140), ' ')
  
update Input_Customer
set [name1]=replace([name1],char(44), ' '), address1=replace(address1,char(44), ' '),
address2=replace(address2,char(44), ' '),   city=replace(city,char(44), ' '),  lastname=replace(lastname,char(44), ' ')
  
update Input_Customer
set [name1]=replace([name1],char(46), ' '), address1=replace(address1,char(46), ' '),
address2=replace(address2,char(46), ' '),   city=replace(city,char(46), ' '),  lastname=replace(lastname,char(46), ' ')

update Input_Customer
set [name1]=replace([name1],char(34), ' '), address1=replace(address1,char(34), ' '),
address2=replace(address2,char(34), ' '),   city=replace(city,char(34), ' '),  lastname=replace(lastname,char(34), ' ')

update Input_Customer
set [name1]=replace([name1],char(35), ' '), address1=replace(address1,char(35), ' '),
address2=replace(address2,char(35), ' '),   city=replace(city,char(35), ' '),  lastname=replace(lastname,char(35), ' ')


--------------- Input Customer table

/********************************************************************/
/* Remove input_customer records with SSN,Cardnumber,name1 = null   */
/********************************************************************/

Insert into Input_customer_error 
	select * from Input_customer 
	where (ssn is null or ssn = ' ') or  
	      (cardnumber is null or cardnumber = ' ') or
	      (name1 is null or name1 = ' ')

delete from Input_customer 
where (ssn is null or ssn = ' ') or  
      (cardnumber is null or cardnumber = ' ') or
      (name1 is null or name1 = ' ')

If (select count(*) from input_customer_error) > '0'
   set @QCFlag = '1'

update input_customer set homephone =  Left(Input_Customer.homephone,3) + substring(Input_Customer.homephone,5,3) + right(Input_Customer.homephone,4)
update input_customer set workphone  = Left(Input_Customer.workphone,3) + substring(Input_Customer.workphone,5,3) + right(Input_Customer.workphone,4)

update roll_customer 
set Tipnumber = b.tipnumber
from roll_customer a,input_customer b
where a.cardnumber = b.cardnumber

--------------- Input Transaction table

Insert into Input_Transaction_error 
	select * from Input_Transaction 
	where (cardnumber is null or cardnumber = ' ') or
	      (ssn is null or ssn = ' ') or
	      (points is null )
	       
Delete from Input_Transaction
where (cardnumber is null or cardnumber = ' ') or
      (ssn is null or ssn = ' ') or
      (points is null  or points =  0)

if (select count(*) from input_transaction_error) > '0'
   set @QCFlag1 = '1'

/********************************************************************/
/* Round the transaction amounts                                    */
/********************************************************************/

Update input_transaction 
set  points = round(points/2,0)

Update input_transaction
set trancode = '67' 
where points > 0

Update input_transaction
set trancode = '37' 
where points < 0


Update qcreferencetable set qcflag = @qcflag, qcflag1 = @qcflag1
GO
