/****** Object:  StoredProcedure [dbo].[spLoadCustomerStage]    Script Date: 06/05/2009 09:50:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************/
/*    This imports data from input_custTran into the customer_STAGE  table*/
/*    it only updates the customer demographic data   */
/* */
/*   THIS DOES NOT UPDATE RUNAVAILABLE OR RUNBALANCE  */
/* */
/******************************************************************************/	
CREATE PROCEDURE [dbo].[spLoadCustomerStage] @EndDate DateTime AS

/******************************************************************************/	
/* Update Existing Customers                                                          */
/******************************************************************************/	
Update Customer_Stage
Set 
LASTNAME 	= Left(rtrim(Input_Customer.LASTNAME),40)
,ACCTNAME1 	= left(rtrim(Input_Customer.NAME1),40 )
,ADDRESS1 	= left(rtrim( Input_Customer.ADDRESS1),40)
,ADDRESS2  	= left(rtrim( Input_Customer.ADDRESS2),40)
,ADDRESS4      = left(ltrim(rtrim( Input_Customer.CITY))+' ' +ltrim(rtrim( Input_Customer.STATE))+' ' +ltrim( rtrim( Input_Customer.ZIPcode)) , 40 )
,CITY 		= Input_Customer.CITY
,STATE		= left(Input_Customer.STATE,2)
,ZIPCODE 	= ltrim(Input_Customer.ZIPcode)
,HOMEPHONE 	= Ltrim(Input_Customer.homephone)
,WORKPHONE  = Ltrim(Input_Customer.workphone) 
,STATUS	= Input_Customer.STATUS
,MISC2		= ' '
From Input_Customer
Where Input_Customer.TIPNUMBER = Customer_Stage.TIPNUMBER 

/******************************************************************************/	
/*Add New Customers                                                                     */
/******************************************************************************/	
Insert into Customer_Stage
(
	TIPNUMBER, TIPFIRST, TIPLAST, LASTNAME,
	ACCTNAME1, ADDRESS1,  ADDRESS2, 
	ADDRESS4,
	CITY, STATE, ZIPCODE , 
	HOMEPHONE,  WORKPHONE,DATEADDED, STATUS, MISC2, 
	RUNAVAILABLE, RUNBALANCE, RUNREDEEMED, RunAvaliableNew
)
select 
	distinct TIPNUMBER, left(TIPNUMBER,3), right(rtrim(TIPNUMBER),6), left(rtrim(LASTNAME),40),
	left(rtrim(Input_Customer.NAME1),40) , 
	Left(rtrim(ADDRESS1),40), 
  	Left(rtrim(ADDRESS2),40), 
	left( ltrim(rtrim(CITY))+' ' +ltrim(rtrim(STATE))+' ' +ltrim( rtrim(ZIPcode)  ),40),
	CITY, left(STATE,2), rtrim(ZIPcode),
	Ltrim(Input_Customer.homephone) , 
	Ltrim(Input_Customer.workphone) , @enddate, STATUS,  ' '
	,0, 0, 0, 0
from  Input_Customer 
	where Input_Customer.tipnumber not in (select TIPNUMBER from Customer_Stage)

/* set Default status to A */
Update Customer_Stage
Set STATUS = 'A' 
Where STATUS IS NULL 

/* Update the status description because the PointsNOW doesn't reference the status table like it SHOULD */
Update Customer_Stage
Set StatusDescription = 
S.StatusDescription 
from status S join Customer_Stage C on S.Status = C.Status
/*                                                                            */
/* Move Address2 to address1 if address1 is null */
Update Customer_Stage 
Set 
Address1 = Address2, 
Address2 = null 
where address1 is null
GO
