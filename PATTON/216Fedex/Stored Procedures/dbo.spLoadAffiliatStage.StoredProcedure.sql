/****** Object:  StoredProcedure [dbo].[spLoadAffiliatStage]    Script Date: 06/05/2009 09:50:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*  **************************************  */
/* Date:  4/1/07 */
/* Author:  Rich T */
/*  **************************************  */
/*  Description: Copies data from input_transaction to the Affiliat_stage table   */
/*  Tables:  
	input_Transaction - select
	Input_Customer  - select 
	Affiliat_Stage	- Insert , update 
*/
/*  Revisions: */
/*  **************************************  */

CREATE PROCEDURE [dbo].[spLoadAffiliatStage]   @MonthEnd char(20)  AS 

update roll_customer  set tipnumber = b.tipnumber
from roll_customer a, affiliat_stage b
where a.tipnumber is null and a.acctnumber = b.custid

/************ Insert New Accounts into Affiliat Stage  ***********/
Insert Into Affiliat_Stage
(AcctID, Tipnumber, AcctType, DateAdded,  secid, AcctStatus, AcctTypeDesc, LastName, YTDEarned, CustID)
	select  c.cardnumber, c.TipNumber, 'Debit', @monthend,  c.ssn, c.Status, 'Debit Card', c.LastName, 0,c.acctnumber
	from roll_Customer c where c.cardnumber not in ( Select acctid from Affiliat_Stage)


/***** Update the Affiliat_Stage desctription from AcctType ****/
Update Affiliat_Stage 
	Set AcctType = T.AcctTypeDesc from AcctType T Join Affiliat_Stage A on T.AcctType = A.AcctType
GO
