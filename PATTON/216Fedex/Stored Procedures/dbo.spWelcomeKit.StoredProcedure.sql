/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 06/05/2009 09:50:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 


Truncate Table Welcomekit 

insert into Welcomekit SELECT 	TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, 
				ADDRESS2, ADDRESS3, City, State, ZipCode
		         FROM customer_stage WHERE (Year(DATEADDED) = Year(@EndDate)AND Month(DATEADDED) = Month(@EndDate)AND Upper(STATUS) <> 'C') 

update DateforAudit set Datein=@EndDate

/* First set of Welcome Kits */


/*

insert welcomekit (custid)
select distinct custid from affiliat
WHERE (Year(DATEADDED) = Year(@EndDate) AND Month(DATEADDED) = Month(@EndDate))

update welcomekit set tipnumber = b.tipnumber
from welcomekit a, affiliat b
where a.custid = b.custid

update welcomekit set acctname1 = b.acctname1,acctname2 = b.acctname2,acctname3 = b.acctname3,
acctname4 = b.acctname4,address1 = b.address1,address2 = b.address2,address3 = b.address3,
city = b.city,state = b.state,zipcode = b.zipcode
from welcomekit a, customer b
where a.tipnumber = b.tipnumber 

*/
GO
