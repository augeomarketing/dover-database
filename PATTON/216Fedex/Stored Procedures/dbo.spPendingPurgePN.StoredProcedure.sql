USE [216FedExEmployeesCA]
GO

/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 05/23/2011 14:56:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spPendingPurgePN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[spPendingPurgePN]
GO
/****** Object:  StoredProcedure [dbo].[spPendingPurgePN]    Script Date: 05/23/2011 14:56:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/******************************************************************************/
        
/* BY:  D Foster  */
/* DATE: 09/2009   */
/* REVISION: 0 */
/* pending purge procedure for FI's Using Points Now                          */
/* 
	RDT20110523 - added code to delete customers who have no history records. 
*/
/******************************************************************************/
CREATE  PROCEDURE [dbo].[spPendingPurgePN] @purgedate nvarchar(10)AS    

/* Testing Parameters */

--declare @purgedate nvarchar(10)
--set @purgedate = '09/17/2009'

/* Declarations */

declare @tipnumber nvarchar(15)
--declare @PostPurgeActivity varchar (1)
drop table PendWrk

select tipnumber
into PendWrk
from customer
where status = 'P'

insert PendingPurge(tipnumber)
select tipnumber from PendWrk
where tipnumber not in (select tipnumber from PendingPurge) 

/* RDT20110523 commented code block below 
	update customer set status = 'C', statusdescription = 'Closed'
	from customer c join pendingpurge p on c.tipnumber = p.tipnumber 
	where @purgedate >= (select max(histdate) from history
	where c.tipnumber  = tipnumber)
*/
-- RDT20110523  - Start 
Update customer 
	set status = 'C', statusdescription = 'Closed'
From customer c join pendingpurge p 
	on c.tipnumber = p.tipnumber 
	Where @purgedate >= (select max(histdate) from history where c.tipnumber  = tipnumber) 
	or 	c.TIPNUMBER not in ( select Distinct Tipnumber from History ) 
-- RDT20110523  - End 

delete from PendingPurge
    where tipnumber in (select tipnumber from customer where status = 'C')

go
