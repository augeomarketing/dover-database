/****** Object:  StoredProcedure [dbo].[spSetDeletedCustomers]    Script Date: 06/05/2009 09:50:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spSetDeletedCustomers]   AS

update input_delete_accts set tipnumber = b.tipnumber
from input_delete_accts a, affiliat_stage b
where a.cardnumber = b.acctid

delete input_delete_accts where tipnumber is null

update affiliat_stage set acctstatus = 'C' 
where acctid in(select distinct cardnumber from input_delete_accts)  

update customer_stage set status = 'D',statusdescription = 'Deletion Pending'
where (status = 'A') and (not exists (select 1 from affiliat_stage where tipnumber = customer_stage.tipnumber and
(acctstatus = 'A' or acctstatus is null))) 

update customer_stage set status = 'A',statusdescription = 'Active [A]'
where (status = 'D') and (exists (select 1 from affiliat_stage where tipnumber = customer_stage.tipnumber and
(acctstatus = 'A' or acctstatus is null))) 

insert pending_purge_accts (tipnumber)
select tipnumber from customer_stage 
where tipnumber not in(select tipnumber from pending_purge_accts) and status = 'D'

update pending_purge_accts set cyclenumber = cyclenumber + 1

update customer_stage set status = 'P',statusdescription = 'Pending Deletion'
where status = 'D' and tipnumber in(select tipnumber from pending_purge_accts where cyclenumber >= 3)

delete pending_purge_accts where cyclenumber >= 3 or  exists(select tipnumber from customer_stage where status = 'A'and
pending_purge_accts.tipnumber = customer_stage.tipnumber)
GO
