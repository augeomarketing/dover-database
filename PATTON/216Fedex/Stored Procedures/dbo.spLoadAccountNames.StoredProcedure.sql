/****** Object:  StoredProcedure [dbo].[spLoadAccountNames]    Script Date: 06/05/2009 09:50:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spLoadAccountNames]  AS

delete customer_stage  where rowid > (select min(a.rowid) from customer_stage a,
customer_stage b where a.tipnumber = b.tipnumber and a.rowid != b.rowid)

update customer_stage  set acctname2 =  b.name1 
from customer_stage a, input_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.acctname1)and 
(acctname2 is null or acctname2 = ' ') 

update customer_stage  set acctname3 =  b.name1 
from customer_stage a, input_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.acctname1) and 
rtrim(b.name1) != rtrim(a.acctname2) and (acctname3 is null or acctname3 = ' ')

update customer_stage  set acctname4 =  b.name1 
from customer_stage a, input_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.acctname1) and 
rtrim(b.name1) != rtrim(a.acctname2) and rtrim(b.name1) != rtrim(a.acctname3) and 
(acctname4 is null or acctname4 = ' ')

update customer_stage  set acctname5 =  b.name1 
from customer_stage a, input_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.acctname1) and 
rtrim(b.name1) != rtrim(a.acctname2) and rtrim(b.name1) != rtrim(a.acctname3) and 
rtrim(b.name1) != rtrim(a.acctname4) and (acctname5 is null or acctname5 = ' ')

update customer_stage  set acctname6 =  b.name1 
from customer_stage a, input_customer b 
where a.tipnumber = b.tipnumber and  rtrim(b.name1) != rtrim(a.acctname1) and 
rtrim(b.name1) != rtrim(a.acctname2) and rtrim(b.name1) != rtrim(a.acctname3) and 
rtrim(b.name1) != rtrim(a.acctname4) and rtrim(b.name1) != rtrim(a.acctname5) and
(acctname6 is null or acctname6 = ' ')

delete input_customer  where rowid > (select min(a.rowid) from input_customer a,
input_customer b where a.cardnumber = b.cardnumber and a.rowid != b.rowid)
GO
