/****** Object:  StoredProcedure [dbo].[spSummary_Totals]    Script Date: 06/05/2009 09:50:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spSummary_Totals]  @monthend varchar(10)
 AS



insert summary (Trandate) values (@monthend)

update summary set input_purchases = (select sum(points) from input_transaction
 where purchase > 0  and trandate = @monthend)

update summary set input_returns = (select sum(points) from input_transaction
 where purchase < 0  and trandate = @monthend)

update summary set stmt_purchases = (select sum(pointspurchaseddb) from monthly_statement_file
 where  trandate = @monthend )

update summary set stmt_returns = (select sum(pointsreturneddb) from monthly_statement_file
 where  trandate = @monthend )

update summary set total_purchases = (select sum(purchase) from input_transaction
 where  trandate = @monthend )

update summary set participants_processed = (select count(*) from input_customer
where trandate = @monthend)

update summary set input_participants = (select count(*) from roll_customer)

update summary set input_transactions = (select count(*) from input_transaction)

update summary set transactions_processed = (select count(*) from transstandard
 where  trandate = @monthend )
GO
