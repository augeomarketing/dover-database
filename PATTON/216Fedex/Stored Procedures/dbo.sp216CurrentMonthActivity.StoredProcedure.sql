/****** Object:  StoredProcedure [dbo].[sp216CurrentMonthActivity]    Script Date: 06/05/2009 09:50:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
RDT 10/09/2006 
- Changed parameter to EndDateParm
- added hh:mm:ss:mmm to End date
-- RDT 5/11/2007 Changed source table from production to staged tables. 
*/


CREATE PROCEDURE [dbo].[sp216CurrentMonthActivity] @EndDateParm varchar(10)
AS


Declare @EndDate DateTime 						--RDT 10/09/2006 
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )		--RDT 10/09/2006 

truncate table Current_Month_Activity

insert into Current_Month_Activity (Tipnumber, EndingPoints,Increases, Decreases, AdjustedEndingPoints)
select tipnumber, RunAvailable,0 ,0 ,0 from Customer



/* Load the current activity table with increases for the current month         */
update Current_Month_Activity
set increases=(select sum(points) from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the current activity table with decreases for the current month         */
update Current_Month_Activity
set decreases=(select sum(points) from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )
where exists(select * from history where histdate>@enddate and ratio='-1'
 and History.Tipnumber = Current_Month_Activity.Tipnumber )

/* Load the calculate the adjusted ending balance        */
update Current_Month_Activity
set adjustedendingpoints=endingpoints - increases + decreases
GO
