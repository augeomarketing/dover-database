USE [216FedExEmployeesCA]
GO

/****** Object:  StoredProcedure [dbo].[sp216StageMonthlyStatement]    Script Date: 12/08/2011 07:49:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp216StageMonthlyStatement]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp216StageMonthlyStatement]
GO

USE [216FedExEmployeesCA]
GO

/****** Object:  StoredProcedure [dbo].[sp216StageMonthlyStatement]    Script Date: 12/08/2011 07:49:04 ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO



/***************************************************************************************************************/
--  Changes 1. added code to pickup expired points in the monthly statement and to show future expiring points
-- RDT20111208 - added Trancode 'PP' 
-- RDT 05/07/2014 - JIRA FEDEX-60
/***************************************************************************************************************/

CREATE PROCEDURE [dbo].[sp216StageMonthlyStatement]  @StartDateParm char(10), @EndDateParm char(10)

AS 

Declare  @MonthBegin char(2),  @SQLUpdate nvarchar(1000)

Declare @StartDate DateTime 
Declare @EndDate DateTime 	
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')	
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )	
print @Startdate 
print @Enddate 

set @MonthBegin = month(Convert(datetime, @StartDate) )

/* Load the statement file from the customer table  */
delete from Monthly_Statement_File

insert into Monthly_Statement_File (tipnumber, acctname1, acctname2, address1, address2, address3, city, state, zip)
select tipnumber, acctname1, acctname2, address1, address2, address3, rtrim(city) , rtrim(state), zipcode
from customer_Stage

/* Load the statmement file with DEBIT purchases          */
update Monthly_Statement_File
set pointspurchasedDB=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode='67')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode='67')

/* Load the statmement file with DEBIT  returns            */
update Monthly_Statement_File
set pointsreturnedDB=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode='37')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate
 and histdate<=@enddate and trancode='37')

/* Load the statmement file with bonuses            */

update Monthly_Statement_File
set pointsbonus=(select sum(points*ratio) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode like 'B%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode like  'B%')

update Monthly_Statement_File
set PointsBonusMN=(select sum(points*ratio) from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate
 and histdate<=@enddate and trancode  in ('F0','F9','G0','G9'))
where exists(select * from history_stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate
 and histdate<=@enddate and trancode  in ('F0','F9','G0','G9'))

/* Load the statmement file with plus adjustments    */
update Monthly_Statement_File
set pointsadded=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate
 and histdate<=@enddate and trancode='IE')
where exists (select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate
 and histdate<=@enddate and trancode='IE')

/* Add  DECREASED REDEEMED to adjustments     */
update Monthly_Statement_File
set pointsadded=pointsadded + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode='DR')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and 
histdate<=@enddate and trancode='DR')

/* Add  DECREASED REDEEMED to adjustments     */
update Monthly_Statement_File
set pointsadded=pointsadded + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode='PP')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and 
histdate<=@enddate and trancode='PP')




/* Load the statmement file with total point increases */
update Monthly_Statement_File
--set pointsincreased= pointspurchasedCR + pointspurchasedDB + pointsbonus + pointsadded
set pointsincreased=  pointspurchasedDB + pointsbonus + pointsadded

/* Load the statmement file with redemptions          */
update Monthly_Statement_File
set pointsredeemed=(select sum(points*ratio*-1) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode like 'R%')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate and 
histdate<=@enddate and trancode like 'R%')

/* Load the statmement file with minus adjustments    */
-- CHANGE(1) add code to pickup expired points for customers 
update Monthly_Statement_File
set pointssubtracted=(select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and (trancode='DE' or trancode='XP'))
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and (trancode='DE' or trancode='XP'))

/* Add EP to  minus adjustments    */
update Monthly_Statement_File
set pointssubtracted= pointssubtracted + (select sum(points) from History_Stage where tipnumber=Monthly_Statement_File.tipnumber 
and histdate>=@startdate and histdate<=@enddate and trancode='EP')
where exists(select * from History_Stage where tipnumber=Monthly_Statement_File.tipnumber and histdate>=@startdate 
and histdate<=@enddate and trancode='EP')

/* Load the statmement file with total point decreases */
update Monthly_Statement_File
set pointsdecreased=pointsredeemed + pointsreturnedDB + pointssubtracted 

--/* Load the statmement file with the Beginning balance for the Month */
-- RDT 05/07/2014
update Monthly_Statement_File 
set pointsbegin = isnull((select SUM(points * ratio)
					from history 
					where Tipnumber = Monthly_Statement_File.TIPNUMBER
					and histdate < @Startdate),0)


/* Load the statmement file with beginning points */
update Monthly_Statement_File
set pointsend=pointsbegin + pointsincreased - pointsdecreased


-- RDT 05/07/2014
Exec RewardsNOW.dbo.usp_ExpirePoints '216', 1 

-- RDT 05/07/2014
Update monthly_statement_file 
	Set ExpiringPoints  = ep.points_expiring 
	FROM  monthly_statement_file m 
		join RewardsNOW.dbo.RNIExpirationProjection ep 
			 on m.tipnumber = ep.tipnumber


update monthly_statement_file set ExpiringPoints = '0' where ExpiringPoints is null or ExpiringPoints < 0



GO


