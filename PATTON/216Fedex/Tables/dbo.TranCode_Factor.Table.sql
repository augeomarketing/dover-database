/****** Object:  Table [dbo].[TranCode_Factor]    Script Date: 05/14/2009 11:41:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TranCode_Factor](
	[Trancode] [char](2) NOT NULL,
	[PointFactor] [float] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
