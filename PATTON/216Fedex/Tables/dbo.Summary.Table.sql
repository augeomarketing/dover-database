/****** Object:  Table [dbo].[Summary]    Script Date: 05/14/2009 11:41:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Summary](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[TranDate] [nvarchar](10) NULL,
	[Input_Participants] [numeric](10, 0) NULL CONSTRAINT [DF_Summary_Participants]  DEFAULT (0),
	[Participants_Processed] [numeric](18, 0) NULL CONSTRAINT [DF_Summary_Participants_Processed]  DEFAULT (0),
	[Input_Transactions] [numeric](18, 0) NULL CONSTRAINT [DF_Summary_Input_Transactions]  DEFAULT (0),
	[Transactions_Processed] [numeric](18, 0) NULL CONSTRAINT [DF_Summary_Transactions_Processed]  DEFAULT (0),
	[Input_Purchases] [decimal](18, 2) NULL CONSTRAINT [DF_Summary_Input_Purchases]  DEFAULT (0),
	[Input_Returns] [decimal](18, 2) NULL CONSTRAINT [DF_Summary_Input_Returns]  DEFAULT (0),
	[Total_Purchases] [decimal](18, 2) NULL CONSTRAINT [DF_Summary_Total_Purchases]  DEFAULT (0),
	[Stmt_Purchases] [decimal](18, 2) NULL CONSTRAINT [DF_Summary_Stmt_Purchases]  DEFAULT (0),
	[Stmt_Returns] [decimal](18, 2) NULL CONSTRAINT [DF_Summary_Stmt_Returns]  DEFAULT (0)
) ON [PRIMARY]
GO
