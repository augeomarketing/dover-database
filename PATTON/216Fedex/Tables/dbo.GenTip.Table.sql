/****** Object:  Table [dbo].[GenTip]    Script Date: 05/14/2009 11:40:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GenTip](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tipnumber] [varchar](15) NULL,
	[acctid] [varchar](25) NULL,
	[custid] [char](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
