/****** Object:  Table [dbo].[Pending_Purge_Accts]    Script Date: 05/14/2009 11:41:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pending_Purge_Accts](
	[RowID] [decimal](18, 0) IDENTITY(1,1) NOT NULL,
	[cardnumber] [varchar](25) NULL,
	[acctname] [varchar](50) NULL,
	[tipnumber] [varchar](15) NOT NULL,
	[CycleNumber] [numeric](18, 0) NOT NULL CONSTRAINT [DF_Pending_Purge_Accts_CycleNumber]  DEFAULT (0)
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
