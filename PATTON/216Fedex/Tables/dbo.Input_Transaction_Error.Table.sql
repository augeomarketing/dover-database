/****** Object:  Table [dbo].[Input_Transaction_Error]    Script Date: 05/14/2009 11:41:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction_Error](
	[RowID] [decimal](18, 0) NOT NULL,
	[cardnumber] [varchar](25) NULL,
	[ssn] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Purchase] [decimal](18, 2) NULL,
	[points] [decimal](18, 2) NULL,
	[trancnt] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
