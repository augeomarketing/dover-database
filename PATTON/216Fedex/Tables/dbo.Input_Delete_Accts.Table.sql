/****** Object:  Table [dbo].[Input_Delete_Accts]    Script Date: 05/14/2009 11:41:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Delete_Accts](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cardnumber] [varchar](25) NOT NULL,
	[AcctNumber] [varchar](10) NULL,
	[name] [varchar](50) NULL,
	[tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
