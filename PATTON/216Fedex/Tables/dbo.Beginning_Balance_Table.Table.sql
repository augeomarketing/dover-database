/****** Object:  Table [dbo].[Beginning_Balance_Table]    Script Date: 05/14/2009 11:40:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Beginning_Balance_Table](
	[Tipnumber] [nchar](15) NULL,
	[MonthBeg1] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg1]  DEFAULT (0),
	[MonthBeg2] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg2]  DEFAULT (0),
	[MonthBeg3] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg3]  DEFAULT (0),
	[MonthBeg4] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg4]  DEFAULT (0),
	[MonthBeg5] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg5]  DEFAULT (0),
	[MonthBeg6] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg6]  DEFAULT (0),
	[MonthBeg7] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg7]  DEFAULT (0),
	[MonthBeg8] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg8]  DEFAULT (0),
	[MonthBeg9] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg9]  DEFAULT (0),
	[MonthBeg10] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg10]  DEFAULT (0),
	[MonthBeg11] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg11]  DEFAULT (0),
	[MonthBeg12] [int] NULL CONSTRAINT [DF_Beginning_Balance_Table_MonthBeg12]  DEFAULT (0)
) ON [PRIMARY]
GO
