/****** Object:  Table [dbo].[Input_Transaction]    Script Date: 05/14/2009 11:41:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Transaction](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cardnumber] [varchar](25) NULL,
	[ssn] [varchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[Tipnumber] [varchar](15) NULL,
	[Purchase] [decimal](18, 2) NULL CONSTRAINT [DF_Input_Transaction_Purchase]  DEFAULT (0),
	[points] [decimal](18, 2) NULL,
	[trancnt] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
