/****** Object:  Table [dbo].[Input_Customer]    Script Date: 05/14/2009 11:41:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Input_Customer](
	[RowID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[SSN] [varchar](10) NULL,
	[CardNumber] [varchar](25) NULL,
	[AcctNumber] [varchar](10) NULL,
	[Name1] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zipcode] [varchar](10) NULL,
	[HomePhone] [varchar](15) NULL,
	[WorkPhone] [varchar](15) NULL,
	[Status] [varchar](1) NULL,
	[Last4] [varchar](4) NULL,
	[Tipnumber] [varchar](50) NULL,
	[Name2] [varchar](50) NULL,
	[Name3] [varchar](50) NULL,
	[Name4] [varchar](50) NULL,
	[Name5] [varchar](50) NULL,
	[Name6] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
