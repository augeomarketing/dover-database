USE [213Redwood]
GO
/****** Object:  View [dbo].[vwCustomer]    Script Date: 06/30/2011 10:32:43 ******/
DROP VIEW [dbo].[vwCustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vwCustomer]
			 as
			 select *
			 from [213Redwood].dbo.customer
GO
