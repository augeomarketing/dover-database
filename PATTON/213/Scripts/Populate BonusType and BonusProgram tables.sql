USE [213Redwood];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[BonusType] ON;
BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusType]([sid_BonusType_Id], [dim_BonusType_TranCode], [dim_BonusType_Description], [dim_BonusType_DateAdded], [dim_BonusType_LastUpdated])
SELECT 1, N'FJ', N'New Credit Bonus', '20101001 16:43:07.433', NULL UNION ALL
SELECT 2, N'BN', N'New Debit Bonus', '20101001 16:43:38.237', NULL
COMMIT;
RAISERROR (N'[dbo].[BonusType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[BonusType] OFF;



BEGIN TRANSACTION;
INSERT INTO [dbo].[BonusProgram]([sid_BonusType_Id], [dim_BonusProgram_EffectiveDate], [dim_BonusProgram_ExpirationDate], [dim_BonusProgram_PointMultiplier], [dim_bonusprogram_bonuspoints], [dim_BonusProgram_DateAdded], [dim_BonusProgram_LastUpdated])
SELECT 1, '20071231 00:00:00.000', '20100831 23:59:59.000', 0.00, 2500, '20101001 16:48:59.860', NULL UNION ALL
SELECT 1, '20100901 00:00:00.000', '20101231 23:59:59.000', 0.00, 5000, '20101001 16:49:46.560', NULL UNION ALL
SELECT 1, '20110101 00:00:00.000', '99991231 00:00:00.000', 0.00, 2500, '20101001 16:51:26.373', NULL UNION ALL
SELECT 2, '20071231 00:00:00.000', '99991231 00:00:00.000', 0.00, 1000, '20101001 16:50:36.247', NULL
COMMIT;
RAISERROR (N'[dbo].[BonusProgram]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

