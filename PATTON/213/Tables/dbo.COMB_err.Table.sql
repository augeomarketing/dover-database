USE [213Redwood]
GO
/****** Object:  Table [dbo].[COMB_err]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[COMB_err]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMB_err](
	[NAME1] [varchar](30) NULL,
	[TIP_PRI] [varchar](15) NULL,
	[NAME2] [varchar](30) NULL,
	[TIP_SEC] [varchar](15) NULL,
	[TRANDATE] [datetime] NULL,
	[ERRMSG] [varchar](80) NULL,
	[ERRMSG2] [varchar](80) NULL
) ON [PRIMARY]
GO
