USE [213Redwood]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File_TransferPoints]    Script Date: 06/30/2011 10:32:43 ******/
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsDecreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] DROP CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_TransferedCreditPoints]
GO
DROP TABLE [dbo].[Quarterly_Statement_File_TransferPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quarterly_Statement_File_TransferPoints](
	[Tipnumber] [nchar](15) NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [numeric](18, 0) NULL,
	[PointsEnd] [numeric](18, 0) NULL,
	[PointsPurchasedCR] [numeric](18, 0) NULL,
	[PointsBonusCR] [numeric](18, 0) NULL,
	[PointsAdded] [numeric](18, 0) NULL,
	[PointsPurchasedDB] [numeric](18, 0) NULL,
	[PointsBonusDB] [numeric](18, 0) NULL,
	[PointsIncreased] [numeric](18, 0) NULL,
	[PointsRedeemed] [numeric](18, 0) NULL,
	[PointsReturnedCR] [numeric](18, 0) NULL,
	[PointsSubtracted] [numeric](18, 0) NULL,
	[PointsReturnedDB] [numeric](18, 0) NULL,
	[PointsDecreased] [numeric](18, 0) NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[TransferedCreditPoints] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Quarterly_Statement_File_TransferPoints_Tipnumber] ON [dbo].[Quarterly_Statement_File_TransferPoints] 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsBegin]  DEFAULT (0) FOR [PointsBegin]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsEnd]  DEFAULT (0) FOR [PointsEnd]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsPurchasedCR]  DEFAULT (0) FOR [PointsPurchasedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsBonusCR]  DEFAULT (0) FOR [PointsBonusCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsAdded]  DEFAULT (0) FOR [PointsAdded]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsPurchasedDB]  DEFAULT (0) FOR [PointsPurchasedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsBonusDB]  DEFAULT (0) FOR [PointsBonusDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsIncreased]  DEFAULT (0) FOR [PointsIncreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsRedeemed]  DEFAULT (0) FOR [PointsRedeemed]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsReturnedCR]  DEFAULT (0) FOR [PointsReturnedCR]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsSubtracted]  DEFAULT (0) FOR [PointsSubtracted]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsReturnedDB]  DEFAULT (0) FOR [PointsReturnedDB]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_PointsDecreased]  DEFAULT (0) FOR [PointsDecreased]
GO
ALTER TABLE [dbo].[Quarterly_Statement_File_TransferPoints] ADD  CONSTRAINT [DF_Quarterly_Statement_File_TransferPoints_TransferedCreditPoints]  DEFAULT (0) FOR [TransferedCreditPoints]
GO
