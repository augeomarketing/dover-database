USE [213Redwood]
GO
/****** Object:  Table [dbo].[wRCUCreditDeconversion20080508]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[wRCUCreditDeconversion20080508]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wRCUCreditDeconversion20080508](
	[ACN] [varchar](10) NOT NULL,
	[Col002] [varchar](40) NULL,
	[Col003] [varchar](30) NULL,
	[Col004] [varchar](30) NULL,
	[Col005] [varchar](20) NULL,
	[Col006] [varchar](20) NULL,
	[Col007] [varchar](20) NULL,
	[Col008] [varchar](40) NULL,
	[Col009] [varchar](50) NULL,
	[Col010] [varchar](20) NULL,
	[Col011] [varchar](10) NULL,
	[Col012] [varchar](10) NULL,
	[Col013] [varchar](10) NULL,
	[Col014] [varchar](10) NULL,
	[Col015] [varchar](10) NULL,
	[Col016] [varchar](10) NULL,
	[Col017] [varchar](20) NULL,
	[Col018] [varchar](2) NULL,
	[Col019] [varchar](10) NULL,
	[Col020] [varchar](10) NULL,
	[Col021] [varchar](20) NULL,
	[Col022] [varchar](10) NULL,
 CONSTRAINT [PK_wRCUCreditDeconversion20080508] PRIMARY KEY CLUSTERED 
(
	[ACN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
GO
