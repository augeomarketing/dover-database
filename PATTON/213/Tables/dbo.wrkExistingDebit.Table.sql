USE [213Redwood]
GO
/****** Object:  Table [dbo].[wrkExistingDebit]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[wrkExistingDebit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkExistingDebit](
	[Tipnumber] [varchar](15) NOT NULL,
	[AcctID] [varchar](25) NOT NULL
) ON [PRIMARY]
GO
