USE [213Redwood]
GO
/****** Object:  Table [dbo].[AFFILIAT_STAGE]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[AFFILIAT_STAGE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AFFILIAT_STAGE](
	[ACCTID] [varchar](25) NOT NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[AcctType] [varchar](20) NOT NULL,
	[DATEADDED] [datetime] NOT NULL,
	[SECID] [varchar](10) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL
) ON [PRIMARY]
GO
