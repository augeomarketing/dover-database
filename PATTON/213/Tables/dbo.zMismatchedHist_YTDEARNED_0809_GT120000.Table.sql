USE [213Redwood]
GO
/****** Object:  Table [dbo].[zMismatchedHist_YTDEARNED_0809_GT120000]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[zMismatchedHist_YTDEARNED_0809_GT120000]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zMismatchedHist_YTDEARNED_0809_GT120000](
	[tipnumber] [varchar](15) NOT NULL,
	[HistSpend] [float] NULL,
	[YTDSpend] [float] NULL,
	[OK] [int] NULL
) ON [PRIMARY]
GO
