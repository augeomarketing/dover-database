USE [213Redwood]
GO
/****** Object:  Table [dbo].[zTmp04_07Raw]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[zTmp04_07Raw]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zTmp04_07Raw](
	[Col001] [varchar](8000) NULL,
	[Col002] [varchar](8000) NULL,
	[Col003] [varchar](8000) NULL,
	[Col004] [varchar](8000) NULL,
	[Col005] [varchar](8000) NULL,
	[Col006] [varchar](8000) NULL,
	[Col007] [varchar](8000) NULL,
	[Col008] [varchar](8000) NULL,
	[Col009] [varchar](8000) NULL,
	[Col010] [varchar](8000) NULL,
	[Col011] [varchar](8000) NULL,
	[Col012] [varchar](8000) NULL,
	[Col013] [varchar](8000) NULL,
	[Col014] [varchar](8000) NULL,
	[Col015] [varchar](8000) NULL,
	[Col016] [varchar](8000) NULL,
	[Col017] [varchar](8000) NULL,
	[Col018] [varchar](8000) NULL,
	[Col019] [varchar](8000) NULL,
	[RawFileDate] [char](10) NULL
) ON [PRIMARY]
GO
