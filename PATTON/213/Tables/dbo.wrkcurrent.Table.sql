USE [213Redwood]
GO
/****** Object:  Table [dbo].[wrkcurrent]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[wrkcurrent]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrkcurrent](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
