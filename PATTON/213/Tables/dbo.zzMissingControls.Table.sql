USE [213Redwood]
GO
/****** Object:  Table [dbo].[zzMissingControls]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[zzMissingControls]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zzMissingControls](
	[Tipnumber] [nvarchar](15) NULL,
	[Acctname1] [nvarchar](40) NULL,
	[ACN] [nvarchar](10) NULL,
	[AcctName2] [nvarchar](255) NULL,
	[Address1] [nvarchar](255) NULL,
	[F5] [nvarchar](255) NULL,
	[F6] [nvarchar](255) NULL,
	[F7] [nvarchar](255) NULL,
	[F8] [nvarchar](255) NULL,
	[F9] [float] NULL,
	[F10] [float] NULL,
	[F11] [float] NULL,
	[F12] [float] NULL,
	[F13] [float] NULL,
	[F14] [float] NULL,
	[F15] [float] NULL,
	[F16] [float] NULL,
	[F17] [float] NULL,
	[F18] [float] NULL,
	[F19] [float] NULL,
	[F20] [float] NULL,
	[F21] [float] NULL,
	[F22] [nvarchar](255) NULL,
	[F24] [nvarchar](255) NULL
) ON [PRIMARY]
GO
