USE [213Redwood]
GO
/****** Object:  Table [dbo].[zROB]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[zROB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zROB](
	[tipnumber] [char](20) NOT NULL,
	[redeemed] [int] NULL,
	[PtsTimesQty] [int] NULL
) ON [PRIMARY]
GO
