USE [213Redwood]
GO
/****** Object:  Table [dbo].[HISTORYTIP]    Script Date: 06/30/2011 10:32:43 ******/
ALTER TABLE [dbo].[HISTORYTIP] DROP CONSTRAINT [DF__HISTORYTI__Overa__29572725]
GO
DROP TABLE [dbo].[HISTORYTIP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HISTORYTIP](
	[TIPNUMBER] [varchar](15) NULL,
	[ACCTID] [varchar](25) NULL,
	[HISTDATE] [datetime] NULL,
	[TRANCODE] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[POINTS] [numeric](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SECID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [numeric](9, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[HISTORYTIP] ADD  CONSTRAINT [DF__HISTORYTI__Overa__29572725]  DEFAULT (0) FOR [Overage]
GO
