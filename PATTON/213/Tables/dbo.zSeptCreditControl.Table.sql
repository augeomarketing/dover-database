USE [213Redwood]
GO
/****** Object:  Table [dbo].[zSeptCreditControl]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[zSeptCreditControl]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zSeptCreditControl](
	[acn] [nchar](10) NOT NULL,
	[name1] [nchar](40) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
