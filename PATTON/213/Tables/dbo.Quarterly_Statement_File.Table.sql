USE [213Redwood]
GO
/****** Object:  Table [dbo].[Quarterly_Statement_File]    Script Date: 04/17/2014 08:51:24 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2739D489]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2739D489]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__282DF8C2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__282DF8C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__282DF8C2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__29221CFB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__29221CFB]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2A164134]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2A164134]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2B0A656D]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2BFE89A6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2BFE89A6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2CF2ADDF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2CF2ADDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2CF2ADDF]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PurchasedPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PurchasedPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PurchasedPoints]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2DE6D218]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2DE6D218]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2DE6D218]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2EDAF651]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2EDAF651]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2EDAF651]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2FCF1A8A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2FCF1A8A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__2FCF1A8A]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__30C33EC3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__30C33EC3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__30C33EC3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsExpired]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpired]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsExpired]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__31B762FC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__31B762FC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__31B762FC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__32AB8735]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__32AB8735]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__32AB8735]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__668030F6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__668030F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF__Quarterly__Point__668030F6]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsToExpire_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsToExpire_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsToExpire_1]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsToExpire_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsToExpire_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsToExpire_2]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsToExpire_3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsToExpire_3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] DROP CONSTRAINT [DF_Quarterly_Statement_File_PointsToExpire_3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
DROP TABLE [dbo].[Quarterly_Statement_File]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Quarterly_Statement_File](
	[Tipnumber] [varchar](15) NOT NULL,
	[Acctname1] [varchar](40) NULL,
	[Acctname2] [varchar](40) NULL,
	[Address1] [varchar](40) NULL,
	[Address2] [varchar](40) NULL,
	[Address3] [varchar](40) NULL,
	[CityStateZip] [varchar](50) NULL,
	[STDATE] [char](30) NULL,
	[PointsBegin] [int] NOT NULL,
	[PointsEnd] [int] NOT NULL,
	[PointsPurchasedCR] [int] NOT NULL,
	[PointsBonusCR] [int] NOT NULL,
	[PointsAdded] [int] NOT NULL,
	[PointsPurchasedDB] [int] NOT NULL,
	[PointsBonusDB] [int] NOT NULL,
	[PurchasedPoints] [int] NOT NULL,
	[PointsIncreased] [int] NOT NULL,
	[PointsRedeemed] [int] NOT NULL,
	[PointsReturnedCR] [int] NOT NULL,
	[PointsSubtracted] [int] NOT NULL,
	[PointsExpired] [int] NOT NULL,
	[PointsReturnedDB] [int] NOT NULL,
	[PointsDecreased] [int] NOT NULL,
	[Zip] [char](5) NULL,
	[Acctnum] [char](25) NULL,
	[LastFour] [char](4) NULL,
	[PointsBonusMN] [int] NULL,
	[PointsToExpire_1] [int] NULL,
	[PointsToExpire_2] [int] NULL,
	[PointsToExpire_3] [int] NULL,
 CONSTRAINT [PK_Quarterly_Statement_File] PRIMARY KEY CLUSTERED 
(
	[Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2739D489]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2739D489]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2739D489]  DEFAULT ((0)) FOR [PointsBegin]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__282DF8C2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__282DF8C2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__282DF8C2]  DEFAULT ((0)) FOR [PointsEnd]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__29221CFB]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__29221CFB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__29221CFB]  DEFAULT ((0)) FOR [PointsPurchasedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2A164134]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2A164134]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2A164134]  DEFAULT ((0)) FOR [PointsBonusCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2B0A656D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2B0A656D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2B0A656D]  DEFAULT ((0)) FOR [PointsAdded]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2BFE89A6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2BFE89A6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2BFE89A6]  DEFAULT ((0)) FOR [PointsPurchasedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2CF2ADDF]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2CF2ADDF]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2CF2ADDF]  DEFAULT ((0)) FOR [PointsBonusDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PurchasedPoints]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PurchasedPoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PurchasedPoints]  DEFAULT ((0)) FOR [PurchasedPoints]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2DE6D218]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2DE6D218]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2DE6D218]  DEFAULT ((0)) FOR [PointsIncreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2EDAF651]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2EDAF651]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2EDAF651]  DEFAULT ((0)) FOR [PointsRedeemed]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__2FCF1A8A]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__2FCF1A8A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__2FCF1A8A]  DEFAULT ((0)) FOR [PointsReturnedCR]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__30C33EC3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__30C33EC3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__30C33EC3]  DEFAULT ((0)) FOR [PointsSubtracted]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsExpired]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsExpired]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsExpired]  DEFAULT ((0)) FOR [PointsExpired]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__31B762FC]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__31B762FC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__31B762FC]  DEFAULT ((0)) FOR [PointsReturnedDB]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__32AB8735]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__32AB8735]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF__Quarterly__Point__32AB8735]  DEFAULT ((0)) FOR [PointsDecreased]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Quarterly__Point__668030F6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Quarterly__Point__668030F6]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  DEFAULT ((0)) FOR [PointsBonusMN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsToExpire_1]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsToExpire_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsToExpire_1]  DEFAULT ((0)) FOR [PointsToExpire_1]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsToExpire_2]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsToExpire_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsToExpire_2]  DEFAULT ((0)) FOR [PointsToExpire_2]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Quarterly_Statement_File_PointsToExpire_3]') AND parent_object_id = OBJECT_ID(N'[dbo].[Quarterly_Statement_File]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Quarterly_Statement_File_PointsToExpire_3]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Quarterly_Statement_File] ADD  CONSTRAINT [DF_Quarterly_Statement_File_PointsToExpire_3]  DEFAULT ((0)) FOR [PointsToExpire_3]
END


End
GO
