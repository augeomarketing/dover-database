USE [213Redwood]
GO
/****** Object:  Table [dbo].[CertificateDeleted]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[CertificateDeleted]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateDeleted](
	[CertificateID] [bigint] NOT NULL,
	[CertificateNum] [varchar](30) NULL,
	[TIPNUMBER] [varchar](15) NOT NULL,
	[TIPFIRST] [varchar](3) NOT NULL,
	[RUNBALANCE] [numeric](18, 0) NULL,
	[RedeemedNow] [numeric](18, 0) NOT NULL,
	[RunAvailable] [numeric](18, 0) NULL,
	[PrintedFlag] [char](3) NOT NULL,
	[DateOfIssue] [datetime] NULL,
	[RedeemedDate] [datetime] NULL,
	[RedeemedTotal] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
