USE [213Redwood]
GO
/****** Object:  Table [dbo].[tippurchases]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[tippurchases]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tippurchases](
	[tipnumber] [varchar](15) NOT NULL,
	[total] [float] NULL
) ON [PRIMARY]
GO
