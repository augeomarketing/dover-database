USE [213Redwood]
GO
/****** Object:  Table [dbo].[WelcomeDO]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[WelcomeDO]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WelcomeDO](
	[TIPNUMBER] [nvarchar](255) NULL,
	[ACCTNAME1] [nvarchar](255) NULL,
	[ACCTNAME2] [nvarchar](255) NULL,
	[ACCTNAME3] [nvarchar](255) NULL,
	[ACCTNAME4] [nvarchar](255) NULL,
	[ADDRESS1] [nvarchar](255) NULL,
	[ADDRESS2] [nvarchar](255) NULL,
	[ADDRESS3] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZipCode] [nvarchar](255) NULL
) ON [PRIMARY]
GO
