USE [213Redwood]
GO
/****** Object:  Table [dbo].[YTDMismatched]    Script Date: 06/30/2011 10:32:43 ******/
ALTER TABLE [dbo].[YTDMismatched] DROP CONSTRAINT [DF_YTDMismatched_RunDate]
GO
DROP TABLE [dbo].[YTDMismatched]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[YTDMismatched](
	[NumRecs] [int] NULL,
	[RunDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[YTDMismatched] ADD  CONSTRAINT [DF_YTDMismatched_RunDate]  DEFAULT (getdate()) FOR [RunDate]
GO
