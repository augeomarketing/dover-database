USE [213Redwood]
GO
/****** Object:  Table [dbo].[zRINotInDeconversion_AND_NotIn0808_149]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[zRINotInDeconversion_AND_NotIn0808_149]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zRINotInDeconversion_AND_NotIn0808_149](
	[tipnumber] [nchar](15) NULL,
	[ACN] [nchar](10) NOT NULL
) ON [PRIMARY]
GO
