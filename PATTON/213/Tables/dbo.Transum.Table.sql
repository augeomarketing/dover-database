USE [213Redwood]
GO
/****** Object:  Table [dbo].[Transum]    Script Date: 06/30/2011 10:32:43 ******/
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__NUMPURC__18EBB532]
GO
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__AMTPURC__19DFD96B]
GO
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__NUMCR__1AD3FDA4]
GO
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__AMTCR__1BC821DD]
GO
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF_Transum_ratio]
GO
ALTER TABLE [dbo].[Transum] DROP CONSTRAINT [DF__Transum__overage__1DB06A4F]
GO
DROP TABLE [dbo].[Transum]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transum](
	[tipnumber] [varchar](15) NULL,
	[acctno] [nchar](16) NULL,
	[histdate] [nchar](10) NULL,
	[trancode] [varchar](2) NULL,
	[NUMPURCH] [nchar](6) NULL,
	[AMTPURCH] [numeric](7, 0) NULL,
	[NUMCR] [nchar](6) NULL,
	[AMTCR] [numeric](7, 0) NULL,
	[description] [varchar](50) NULL,
	[ratio] [float] NULL,
	[overage] [numeric](5, 0) NULL,
	[cardtype] [char](1) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [NUMPURCH]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [AMTPURCH]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [NUMCR]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [AMTCR]
GO
ALTER TABLE [dbo].[Transum] ADD  CONSTRAINT [DF_Transum_ratio]  DEFAULT (0) FOR [ratio]
GO
ALTER TABLE [dbo].[Transum] ADD  DEFAULT (0) FOR [overage]
GO
