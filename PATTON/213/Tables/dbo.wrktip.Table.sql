USE [213Redwood]
GO
/****** Object:  Table [dbo].[wrktip]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[wrktip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[wrktip](
	[misc1] [varchar](20) NULL,
	[pritipnumber] [nchar](15) NULL,
	[sectipnumber] [nchar](15) NULL
) ON [PRIMARY]
GO
