USE [213Redwood]
GO
/****** Object:  Table [dbo].[ZRedwoodInput0808]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[ZRedwoodInput0808]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZRedwoodInput0808](
	[ACN] [nchar](10) NOT NULL,
	[Name1] [nchar](40) NULL,
	[Name2] [nchar](40) NULL,
	[Name3] [nchar](40) NULL,
	[Name4] [nchar](40) NULL,
	[Name5] [nchar](40) NULL,
	[Name6] [nchar](40) NULL,
	[Addr1] [nchar](40) NULL,
	[Addr2] [nchar](40) NULL,
	[City] [nchar](40) NULL,
	[State] [nchar](10) NULL,
	[ZipCode] [nchar](10) NULL,
	[HomePhone] [nchar](10) NULL,
	[WorkPhone] [nchar](10) NULL,
	[AcctType] [nchar](2) NULL,
	[ActiveDate] [char](10) NULL,
	[Quantity] [nchar](10) NULL,
	[Amount] [float] NULL,
	[Status] [nchar](10) NULL,
	[Tipnumber] [nchar](15) NULL,
	[Lastname] [nchar](40) NULL,
	[ActiveDateCR] [char](10) NULL,
	[QuantityCR] [nchar](10) NULL,
	[AmountCR] [float] NULL,
	[StatusCR] [nchar](10) NULL,
	[IsDebitOpen] [bit] NULL
) ON [PRIMARY]
GO
