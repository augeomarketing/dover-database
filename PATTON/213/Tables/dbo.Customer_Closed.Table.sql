USE [213Redwood]
GO
/****** Object:  Table [dbo].[Customer_Closed]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[Customer_Closed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer_Closed](
	[TipNumber] [nchar](15) NOT NULL,
	[DateClosed] [datetime] NOT NULL,
	[DateToDelete] [nchar](6) NOT NULL
) ON [PRIMARY]
GO
