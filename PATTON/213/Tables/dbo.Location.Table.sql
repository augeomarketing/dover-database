USE [213Redwood]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[Location]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Location](
	[LocCode] [varchar](25) NOT NULL,
	[LocDesc] [varchar](80) NULL,
	[ClientCode] [varchar](15) NULL,
	[CompanyCode] [varchar](15) NULL
) ON [PRIMARY]
GO
