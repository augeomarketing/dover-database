USE [213Redwood]
GO
/****** Object:  Table [dbo].[CREDIT_CONTROL]    Script Date: 06/30/2011 10:32:43 ******/
DROP TABLE [dbo].[CREDIT_CONTROL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CREDIT_CONTROL](
	[ACN] [varchar](10) NULL,
	[AcctName1] [varchar](40) NULL,
	[Tipnumber] [varchar](15) NULL
) ON [PRIMARY]
GO
