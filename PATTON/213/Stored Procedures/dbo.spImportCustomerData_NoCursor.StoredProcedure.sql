USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spImportCustomerData_NoCursor]    Script Date: 09/28/2016 14:03:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spImportCustomerData_NoCursor] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Update Customer
	update cs
	set cs.AcctName1=ci.NAMEACCT1, cs.AcctName2=ci.NAMEACCT2, cs.AcctName3=ci.NAMEACCT3, cs.AcctName4=ci.NAMEACCT4, cs.AcctName5=ci.NAMEACCT5, cs.Address1=ci.Address1, cs.Address2=ci.Address2, cs.Address4=ci.Address4, cs.City=rtrim(ci.City), State=rtrim(ci.STATE), cs.Zipcode=rtrim(ci.zip), cs.Status=ci.Status, cs.lastname=ci.lastname, cs.misc1=ci.misc1, cs.misc2=ci.misc2, cs.misc3=ci.misc3, cs.NOTES = ci.Email
	from customer cs
	join custin ci
	on cs.tipnumber = ci.tipnumber

	--INSERT INTO Customer 
	INSERT INTO Customer 
	(TIPNumber, Runavailable, Runbalance, Runredeemed, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, Acctname5,  Address1, Address2, Address4, City, State, Zipcode, Status, DateAdded, Lastname, Misc1, Misc2, Misc3, NOTES )
	select ci.tipnumber, '0', '0', '0', LEFT(ci.tipnumber,3), right(ci.tipnumber,12), ci.NAMEACCT1, ci.NAMEACCT2, ci.NAMEACCT3, ci.NAMEACCT4, ci.NAMEACCT5,  ci.ADDRESS1, ci.ADDRESS2, ci.ADDRESS4, rtrim(ci.City), rtrim(ci.state), rtrim(ci.zip), ci.STATUS, cast(ci.DateAdded as datetime), ci.LastName, ci.Misc1, ci.Misc2, ci.Misc3, ci.Email
	from Custin ci
	left outer join Customer cs
	on ci.tipnumber = cs.tipnumber
	where ci.tipnumber is not null
	and len(ci.tipnumber)>2
	and LEFT(ci.tipnumber,1) <> ' '
	and cs.tipnumber is null

	-- UPDATE Affiliat
	update af
	set af.lastname=ci.lastname, af.AcctStatus=ci.Status
	from AFFILIAT af
	join CUSTIN ci
	on af.ACCTID = ci.ACCT_NUM

	--INSERT INTO affiliat 
	INSERT INTO affiliat 
	(ACCTID, TIPNUMBER, AcctType, DATEADDED, AcctStatus, AcctTypeDesc, LastName, YTDEarned, custid)
	select ci.ACCT_NUM, ci.tipnumber, 'Credit/Debit', cast(ci.DateAdded as datetime), ci.Status, 'Credit/Debit Card', ci.lastname, '0', ci.misc1
	from Custin ci
	left outer join AFFILIAT af
	on ci.ACCT_NUM = af.ACCTID
	where ci.ACCT_NUM is not null
	and len(ci.ACCT_NUM)>2
	and LEFT(ci.ACCT_NUM,1) <> ' '
	and af.ACCTID is null

	update customer
	set StatusDescription=(select statusdescription from status where status=customer.status)

END
GO
