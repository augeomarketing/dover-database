USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[usp_ImportTransactionData]    Script Date: 09/28/2016 14:30:19 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ImportTransactionData] 
	@TipFirst varchar(3), @StartDate varchar(10), @enddate varchar(10)

AS 

DECLARE	@TFNO nvarchar(15)
	,	@Trandate nchar(10)
	,	@ACCT_NUM nvarchar(25)
	,	@TRANCODE nvarchar(2)
	,	@TRANNUM nvarchar(4)
	,	@TRANAMT nvarchar(15)
	,	@TRANTYPE nvarchar(20)
	,	@RATIO nvarchar(4)
	,	@CRDACTVLDT nvarchar(10)  
	,	@MaxPointsPerYear decimal(9) = (select MaxPointsPerYear from client where  tipfirst = @tipfirst)
	,	@YTDEarned numeric(9)
	,	@AmtToPost numeric (9)
	,	@Overage numeric(9)
 
IF	@MaxPointsPerYear = '0'	BEGIN SET @MaxPointsPerYear='999999999' END

DECLARE @tbl table (tip varchar(15) primary key, earn_status int)
DECLARE @tbl_a table (tip varchar(15) primary key, new int)
DECLARE @tbl_b table (tip varchar(15) primary key, ytd int)

INSERT INTO @tbl_a
SELECT tfno, SUM(cast(rtrim(tranamt) as int)) FROM TRANSsTANDARD WHERE TRANCODE in ('63','67') GROUP BY TFNO
INSERT INTO @tbl_b
SELECT tipnumber, isnull(MAX(isnull(ytdearned, 0)),0) FROM AFFILIAT GROUP BY TIPNUMBER
INSERT INTO @tbl
SELECT a.tip, 1 FROM @tbl_a a INNER JOIN @tbl_b b ON a.tip = b.tip WHERE b.ytd >= @MaxPointsPerYear	
INSERT INTO @tbl
SELECT a.tip, 2 FROM @tbl_a a INNER JOIN @tbl_b b ON a.tip = b.tip WHERE a.new + b.ytd <= @MaxPointsPerYear	
INSERT INTO @tbl
SELECT a.tip, 3 FROM @tbl_a a INNER JOIN @tbl_b b ON a.tip = b.tip WHERE a.new + b.ytd > @MaxPointsPerYear and b.ytd < @MaxPointsPerYear	
INSERT INTO @tbl
SELECT a.tip, 4 FROM @tbl_a a LEFT OUTER JOIN CUSTOMER b ON a.tip = b.TIPNUMBER WHERE b.TIPNUMBER is null

INSERT INTO HISTORY (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
SELECT	TFNO, ACCT_NUM, TRANDATE, TRANCODE, TRANNUM, '0', TRANTYPE, '', RATIO, rtrim(TRANAMT)
FROM	TransStandard ts join @tbl tbl on ts.TFNO = tbl.tip
WHERE	tbl.earn_status = 1

INSERT INTO HISTORY (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
SELECT	TFNO, ACCT_NUM, TRANDATE, TRANCODE, TRANNUM, rtrim(TRANAMT), TRANTYPE, '', RATIO, '0'
FROM	TransStandard ts join @tbl tbl on ts.TFNO = tbl.tip
WHERE	tbl.earn_status = 2

	
DECLARE		Tran_crsr CURSOR
	FOR SELECT	TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, rtrim(TRANAMT), TRANTYPE, RATIO, CRDACTVLDT
FROM	TransStandard ts join @tbl tbl on ts.TFNO = tbl.tip
WHERE	tbl.earn_status = 3
	ORDER BY TFNO

OPEN	Tran_crsr
FETCH	Tran_crsr INTO @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT

IF	@@FETCH_STATUS = 1
	GOTO	Fetch_Error

WHILE	@@FETCH_STATUS = 0
	BEGIN
	SET	@Overage= (@YTDEarned + rtrim(@Tranamt))- @MAXPOINTSPERYEAR
	IF @Overage < 0 BEGIN SET @Overage = 0 END
	SET	@AmtToPost = convert(numeric,rtrim(@TRANAMT) - @OVERAGE)
	SET	@YTDEarned = @YTDEarned + @AmttoPost

	INSERT INTO history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
	VALUES				(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, ' ', @RATIO, @OVERAGE) 			
	
	UPDATE	affiliat
	SET		YTDEarned = @YTDEarned 
	WHERE	Tipnumber = @TFNO	
	GOTO NextRecord

	NEXTRECORD:
		FETCH Tran_crsr INTO @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
	END
FETCH_ERROR:
CLOSE		Tran_crsr
DEALLOCATE	Tran_crsr

INSERT INTO HISTORY (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
SELECT	TFNO, ACCT_NUM, TRANDATE, TRANCODE, TRANNUM, rtrim(TRANAMT), TRANTYPE, '', RATIO, '0'
FROM	TransStandard
WHERE	TRANCODE in ('TP','BN','FJ','33','37')

UPDATE	a
SET		YTDEarned = h.ytdearned
FROM	AFFILIAT a
	INNER JOIN	(
				Select tipnumber, SUM(points*ratio) as ytdearned 
				From HISTORY 
				where TRANCODE in ('63','67') AND YEAR(histdate) = YEAR(CAST(@enddate as DATE)) 
				Group by TIPNUMBER
				) h
	ON	a.TIPNUMBER = h.TIPNUMBER
	
EXEC RewardsNow.dbo.usp_BalanceCustomerPointsFromHistory @tipfirst
GO
