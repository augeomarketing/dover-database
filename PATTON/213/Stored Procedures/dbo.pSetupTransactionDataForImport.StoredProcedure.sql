USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[pSetupTransactionDataForImport]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[pSetupTransactionDataForImport]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*									       */

truncate table transwork
truncate table transworkCredit
truncate table transstandard
insert into transwork (Trandate, Pan, processingcode, Amounttran, tipnumber, numberoftrans)
	select @enddate, acn, ' ', amount, tipnumber, quantity
	from RedwoodInput
	--new criteria 
	where IsDebitOpen = 1   ---CRITERIA TO IDENTIFY ACTIVE DEBIT PEOPLE. Flag for this field was set back in spLoadCustin
----------------------------------------------------------
insert into transworkCredit (Trandate, Pan, processingcode, Amounttran, tipnumber, numberoftrans)
	select @enddate, acn, ' ', amountCR, tipnumber, quantityCR
	from RedwoodInput
	--new criteria 
	where StatusCR = 'OPEN'
-- Removed the divide by 2 2/1/2017
--update transwork
--set points=ROUND(((amounttran)/2), 10, 0)
update transwork
set points=ROUND(((amounttran)), 10, 0)

--
update transworkCredit
set points=ROUND(((amounttran)), 10, 0)
-- Debit transactions
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' from transwork
group by tipnumber, Pan
-----------Credit Transactions
--MODIFY TranCODE and TranTYPE to 63 and CREDIT AFTER august points Transfer
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '63', sum(NumberOfTrans), sum(points), 'CREDIT', '1', ' ' from transworkCredit
group by tipnumber, Pan
/*  transfer of points below
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, 'TP', sum(NumberOfTrans), sum(points), 'Transfer Standard', '1', ' ' from transworkCredit
group by tipnumber, Pan
*/
GO
