USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[sp213CheckYTDEarned]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[sp213CheckYTDEarned]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp213CheckYTDEarned] @StartDate char(10)
AS 

truncate table YTDMismatched


declare @YearStartDate varchar(10),@NumRecs int

set @YearStartDate ='01/01/' + right(@StartDate,4)


Select Tipnumber, Sum(points*ratio) as HistSpend Into #HistYTDEarned
from history 
where trancode in ('33','37','63','67')
and histdate > @YearStartDate
group by tipnumber
order by sum(points*ratio) desc

---get the top 1(YTDEarned) from affiliat by tipnumber

Select a.tipnumber, (select top 1 ytdEarned  from affiliat aa where aa.tipnumber=a.tipnumber) as YTDSpend Into #AffYTDEarned
from affiliat a
group by Tipnumber



select h.tipnumber, H.HistSpend, A.YTDSpend , H.HistSpend-A.YTDSpend as Diff
from #HistYTDEarned H join #AffYTDEarned A on H.Tipnumber=A.tipnumber
where H.HistSpend<> A.YTDSpend
--and A.YTDSpend>120000
order by h.tipnumber
--order by A.YTDSpend

set @NumRecs=@@rowcount

drop table #HistYTDEarned
drop table #AFFYTDEarned

insert into YTDMisMatched(NumRecs) values(@NumRecs)
GO
