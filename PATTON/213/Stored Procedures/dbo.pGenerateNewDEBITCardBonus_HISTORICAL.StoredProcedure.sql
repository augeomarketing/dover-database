USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateNewDEBITCardBonus_HISTORICAL]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[pGenerateNewDEBITCardBonus_HISTORICAL]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pGenerateNewDEBITCardBonus_HISTORICAL] @startDate varchar(10), @EndDate varchar(10), @TipFirst char(3)
AS 
/****************************************************************************/
/*                                                                          */
/*   --------HARD CODED TRANDATE VALUE-ONLY TO BE RUN IN AUGUST TO MAKEUP THE OLD BONUSES          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/


declare @BonusPointsDebit nchar(15)
set @BonusPointsDebit='1000'	
--TransStandard Variables
declare @Trandate varchar(10), @Tipnumber char(15), @ACN nvarchar(25), @TranCode nvarchar(2), @TranNum nvarchar(4), @TranAmt nchar(15), @TranType nvarchar(20), @Ratio nvarchar(4), @CRDACTVLDT nvarchar(10)


set @Trandate=@EndDate

--Put the Orpahned(no tipnumber) ACNs in a table
insert into ztmpNoTipACNs select ACN from RedwoodInput where tipnumber is null
delete RedwoodInput where tipnumber is null

/*          remove so that the date cpnversion following will work */
update dbo.redwoodinput
	set activedate = null
where isdate(activedate) = 0

/* Setup Cursor for processing                                                */
declare Tip_crsr cursor for 
	select distinct tipnumber, ACN 
	from RedwoodInput
	where isdate(ActiveDate) =1
	And convert(datetime,ActiveDate,1) Between @StartDate and  @Enddate 
	and Tipnumber not in (select Tipnumber from OneTimeBonuses where Trancode='BN'   )
/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber,@ACN
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	

		Begin
 
		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select @tipnumber, '08/31/2008', @ACN, 'BN', 1, @BonusPointsDebit, 'Bonus New Account', '1', ' ' 

			INSERT INTO OneTimeBonuses(TipNumber,TranCode, DateAwarded)
        		Values(@Tipnumber, 'BN', '08/31/2008')
		End
		goto Next_Record
Next_Record:
		fetch Tip_crsr into @Tipnumber,@ACN
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
