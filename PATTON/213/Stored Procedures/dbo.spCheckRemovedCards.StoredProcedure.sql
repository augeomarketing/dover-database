USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spCheckRemovedCards]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spCheckRemovedCards]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCheckRemovedCards] 
AS 
/************************************************************************************/
/*                                                                                  */
/*  Acctid in Affiliat table is actually the last six of cardno                     */
/*  Secid in Affiliat table is actually the Acctno (DDA#) for Spirit Bank           */
/*  Custid in Affiliat table is actually the CIS# for Spirit Bank                   */
/*                                                                                  */
/************************************************************************************/

Update affiliat 
set acctstatus = 'C' where not exists(select * from redwoodinput where acn= affiliat.acctid)

Update affiliat
set acctstatus='C' where acctid in(select acn from redwoodinput where status<>'OPEN')
GO
