USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spGetExistingTipNumbers]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spGetExistingTipNumbers]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spGetExistingTipNumbers] @TipFirst char(3)
AS
 /* Revision                                   */
/* By Sarah Blanchette                         */
/* Date 11/24/2007                             */
/* SCAN  SEB001                                */
declare @DBName varchar(50), @SQLUpdate nvarchar(1000),  @MonthBucket char(10), @SQLTruncate nvarchar(1000), @SQLInsert nvarchar(1000), @monthbegin char(2), @SQLSelect nvarchar(1000)
set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
/*  Get tipnumber based on DDA number  */
set @SQLUpdate='update RedwoodInput 
		set tipnumber=b.tipnumber
		from RedwoodInput a, ' + QuoteName(@DBName) + N'.dbo.affiliat b
		where a.tipnumber is null and a.ACN=b.acctid '
Exec sp_executesql @SQLUpdate
GO
