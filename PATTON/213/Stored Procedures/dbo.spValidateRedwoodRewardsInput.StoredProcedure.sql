USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spValidateRedwoodRewardsInput]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spValidateRedwoodRewardsInput]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spValidateRedwoodRewardsInput]
AS

truncate table RedwoodInputerrors

/*trim out leading spaces in Name1*/
Update RedwoodInput set Name1=ltrim(Name1)


/*  ACN ERROR  */
insert into RedwoodInputerrors
select ACN, Name1, Name2, Name3, Name4, Name5, Name6, Addr1, Addr2, city, state, zipcode, homephone, workphone, Accttype, Quantity, amount, status, 'ACN ERROR' 
from RedwoodInput
where (ACN is null or left(ACN,2)='  ' or len(ACN)='0')

/*  NAME ERROR  */
insert into RedwoodInputerrors
select ACN, Name1, Name2, Name3, Name4, Name5, Name6, Addr1, Addr2, city, state, zipcode, homephone, workphone, Accttype, Quantity, amount, status, 'NAME ERROR' 
from RedwoodInput
where (Name1 is null or left(Name1,2)='  ' or len(Name1)='0')

/*  ADDRESS 1 ERROR  */
insert into RedwoodInputerrors
select ACN, Name1, Name2, Name3, Name4, Name5, Name6, Addr1, Addr2, city, state, zipcode, homephone, workphone, Accttype, Quantity, amount, status, 'ADDRESS ERROR' 
from RedwoodInput
where ( (Addr1 is null) or  (left(Addr1,2)='  ') or (len(Addr1)='0') )

/*  CITY ERROR  */
insert into RedwoodInputerrors
select ACN, Name1, Name2, Name3, Name4, Name5, Name6, Addr1, Addr2, city, state, zipcode, homephone, workphone, Accttype, Quantity, amount, status, 'CITY ERROR'
from RedwoodInput
where (City is null or left(City,2)='  ' or len(City)='0')

/*  STATE ERROR  */
insert into RedwoodInputerrors
select ACN, Name1, Name2, Name3, Name4, Name5, Name6, Addr1, Addr2, city, state, zipcode, homephone, workphone, Accttype, Quantity, amount, status, 'STATE ERROR'
from RedwoodInput
where ((state is null or left(state,2)='  ' or len(state)='0') )

/*  DELETE FROM INPUT FILE  */

delete from RedwoodInput
where (ACN is null or left(ACN,2)='  ' or len(ACN)='0') 
	or (Name1 is null or left(Name1,2)='  ' or len(Name1)='0') 
	or (Addr1 is null or  left(Addr1,2)='  '  or len(Addr1)='0' )
	or (City is null or left(City,2)='  ' or len(City)='0')
	or ((state is null or left(state,2)='  ' or len(state)='0') )
GO
