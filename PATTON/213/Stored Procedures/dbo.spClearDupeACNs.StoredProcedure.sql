USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spClearDupeACNs]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spClearDupeACNs]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[spClearDupeACNs] AS


truncate table RedwoodInput_Error_KeyViolation



--get recs that have more than one record per ACN
SELECT     ACN AS acn
into #tmp
FROM         RedwoodInput_orig
GROUP BY ACN
HAVING      (COUNT(*) > 1)
ORDER BY COUNT(*) DESC

--insert these into the error table
insert into RedwoodInput_error_KeyViolation
	select * from RedwoodInput_Orig where acn in (select acn from #tmp)

--Delete from the original file
delete from RedwoodInput_Orig where acn in  (select acn from #tmp)

drop table #tmp
---------------------------------------------------------------------------------------------------------------------------------
--do the same looking for records  that have an acn that is IN the Replaced_acct field
select  acn as acn 
into #tmpB
 from RedwoodInput_Orig where ACN in (select Replacement_Acct from redwoodInput_orig)

--insert these into the error table
Insert into RedwoodInput_Error_KeyViolation
	select  * from RedwoodInput_Orig where ACN in (select acn from #tmpB)
--Delete from the original file
delete from RedwoodInput_Orig where acn in  (select acn from #tmpB)
GO
