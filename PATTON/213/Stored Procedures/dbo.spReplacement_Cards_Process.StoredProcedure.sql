USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spReplacement_Cards_Process]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spReplacement_Cards_Process]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReplacement_Cards_Process] @EndDate char(10)
AS 


/*CHANGE TABLE TO REDWOODINPUT*/


-- error out the records that have more than one rec per ACN in _orig or where acn is also in the replacement_acct field
exec spClearDupeACNs
--
--truncate and populate Redwood input (original minus the errors)
truncate table RedwoodInput
insert into RedwoodInput select * from RedwoodInput_orig


insert into Replaced_Accts_History
	select * from  Replaced_Accts  

truncate table Replaced_Accts
truncate table Replaced_errorsWrk

insert into Replaced_Accts  
		Select *,  @EndDate, NULL  from RedwoodInput where Replacement_Acct is NOT null

/*
Below are rules per Jamie Bliss  in 12/10/08 email

Old Account                                       New Account                           Scenario #

Does not exist on RN system      Does not exist on RN system      	1
Exists on RN system                        Does not exist on RN system      2
Does not exist on RN system      Exists on RN system                        3
Exist on RN system                          Exists on RN system                    4

Assuming a debit or a credit card has the status of "Open", the action would be as follows:

Scenario #           Action Symbol   Action
1                              a                              Create new RN record with New Account number 
                                b                             Add reported points to New Account account

2                              a                              Create new RN record with New Account number 
                                b                             Add reported points to New Account account
                                c                              Move points from Old Account to account represented by New Account
                                d                             Remove Old Account from RN system

3                              b                             Add reported points to New Account account

4                              b                             Add reported points to New Account account
                                c                              Move points from Old Account to account represented by New Account
                                d                             Remove Old Account from RN system

These four different actions are all I can see needing to be performed.  The only other situation would be when both the debit and credit cards are "Closed/NA", "NA/Closed" or "NA/NA" (I don't think you'd ever see the last).  In these situations, both the Old Account and New Account would need to be removed from the RN system (if they exist on the RN system at all) as all points are forfeit.
*/

declare @OldACN varchar(10), @NewACN varchar(10), @Scenario varchar(1), @CurrentAcctID varchar(10),@NumRecs int, @CurrentTip varchar(15), @OldExists int, @NewExists int

declare Rpl_Csr cursor 
for select ACN, Replacement_Acct, Scenario
from Replaced_Accts
for update


/*                                                                            */
open Rpl_Csr
fetch Rpl_Csr into @OldACN, @NewACN, @Scenario
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	set @Scenario='0'

	Select @OldExists = Count(*)  from affiliat where acctid=@OldACN
	Select @NewExists =Count(*)  from affiliat where acctid=@NewACN
		if (@OldExists=0) and (@NewExists =0) set @Scenario='1'  --NEITHER old NOR new are in the system
		if (@OldExists>0) and (@NewExists =0) set @Scenario='2'  --old exists but new doesn't
		if (@OldExists=0) and (@NewExists >0) set @Scenario='3'
		if (@OldExists>0) and (@NewExists >0) set @Scenario='4'

		update Replaced_Accts set Scenario=@Scenario where current of Rpl_Csr
	
		if @Scenario='1'
		begin	--If Neither Old or New ACN exists, change value of  ACN in RedwoodInput to value of NEW
			--this will create a NEW tipnumber for the new acn and apply the points
			begin 
				update RedwoodInput set ACN=@NewACN where ACN=@OldACN
			end
		end

		if @Scenario='2'
		begin	--get the tipnumber for the OLD acn the exists
			select @CurrentTip=Tipnumber from affiliat where acctid=@OldACN
			if @CurrentTip is not null
			begin 	-- update the value of the empty tipnumber and ACN fields here (it won't be included in the get existing or get new tipnumbers procedures later
				update RedwoodInput set tipnumber =@CurrentTip, ACN=@NewACN where ACN=@OldACN
				--replace all of the values in all tables
				--exec spReplace_ACN @EndDate, @OldACN, @NewACN
			end
		end

		if @Scenario='3'
		begin	--get the tipnumber for the NEW acn the exists
			select @CurrentTip=Tipnumber from affiliat where acctid=@NewACN
			if @CurrentTip is not null
			begin 	-- update the value of the empty tipnumber and ACN fields here (it won't be included in the get existing or get new tipnumbers procedures later
				update RedwoodInput set tipnumber =@CurrentTip, ACN=@NewACN where ACN=@OldACN

			end
		end
		
		if @Scenario='4'
		begin	--get the tipnumber for the NEW acn the exists

			--make sure that the New and old ACNs belong to the same tip. If not, put into an error file and delete the records from Redwood Input
			declare @TipOld varchar(15), @TipNew varchar(15)
			select @TipOld=Tipnumber from affiliat where acctid=@OldACN
			select @TipNew=Tipnumber from affiliat where acctid=@NewACN
			
			if @TipNew=@TipOld
			begin
				select @CurrentTip=Tipnumber from affiliat where acctid=@NewACN
				if @CurrentTip is not null
				begin 	-- update the value of the empty tipnumber and ACN fields here (it won't be included in the get existing or get new tipnumbers procedures later
					update RedwoodInput set tipnumber =@CurrentTip, ACN=@NewACN where ACN=@OldACN
	
				end
			end
			else
			begin
				insert into Replaced_errorsWrk
					select * from Replaced_Accts where acn=@OldACN
				delete redwoodInput where acn=@OldACN OR acn=@NewACN OR Replacement_Acct=@NewACN or Replacement_ACCT=@OldACN
			end




		end






	
	goto Next_Record
	Next_Record:
			fetch Rpl_Csr into @OldACN, @NewACN, @Scenario
end
Fetch_Error:
close  Rpl_Csr
deallocate  Rpl_Csr


--USE LINE BELOW WITHIN THE CURSOR
--Update RedwoodInput set ACN=@NewACN
GO
