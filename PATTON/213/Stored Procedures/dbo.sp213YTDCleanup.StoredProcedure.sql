USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[sp213YTDCleanup]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[sp213YTDCleanup]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp213YTDCleanup] @StartDate char(10)
AS 

declare @ThisYear datetime
set @ThisYear ='01/01/' + right(@StartDate,4)


Declare @afFound Varchar(1)
Declare @tipnumber Varchar(15)
Declare @acctid Varchar(25)
Declare @lastname Varchar(50)
Declare @cardtype Varchar(20)
Declare @dateadded Varchar(10)
Declare @secid Varchar(10)
Declare @acctstatus Varchar(50)
declare @acctdesc Varchar(50)
Declare @ytdearned float
Declare @CLIENTID Varchar(13)
DECLARE @RESULT INT

/*   - DECLARE CURSOR AND OPEN TABLES  */
Declare Cust_crsr cursor
for Select Tipnumber
From Customer

Open Cust_crsr
/*                  */



Fetch Cust_crsr  
into   @tipnumber
IF @@FETCH_STATUS = 1
	goto Fetch_Error

	SET @RESULT = 0




/*                                                                            */
while @@FETCH_STATUS = 0
BEGIN

	SET @RESULT = 0	
	set @RESULT = (select sum(points*ratio)
	 from history
	 where tipnumber = @tipnumber
	 and trancode in ('33','37','63','67')
	 and histdate > @ThisYear) 

	if @RESULT is null
	   set @RESULT = '0'

	Update AFFILIAT
	Set 
	    YTDEarned =  @RESULT 
	where TIPNUMBER = @tipnumber
    




FETCH_NEXT:
	
	Fetch Cust_crsr  
        into   @tipnumber
END /*while */


	

GoTo EndPROC

Fetch_Error:
Print 'Fetch Error'

EndPROC:
close  Cust_crsr
deallocate  Cust_crsr
GO
