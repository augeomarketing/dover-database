USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKit]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spWelcomeKit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spWelcomeKit] @EndDate varchar(10), @TipFirst nchar(3)
AS 
---this is now the DebitOnly WelcomeKit. 
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode FROM ' + QuoteName(@DBName) + N' .dbo.customer 
WHERE DATEADDED = @EndDate AND STATUS <> ''c'' 
AND 
Tipnumber in (Select tipnumber from RedwoodInput where (StatusCR=''CLOSED'' OR StatusCR=''NA'') ) '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate
GO
