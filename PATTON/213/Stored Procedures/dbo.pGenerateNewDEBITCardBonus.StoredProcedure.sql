USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateNewDEBITCardBonus]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[pGenerateNewDEBITCardBonus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGenerateNewDEBITCardBonus] 
	   @startDate varchar(10), 
	   @EndDate varchar(10), 
	   @TipFirst char(3)

AS 

/****************************************************************************/
/*                                                                          */
/*   ---------DEBIT CARD NEW ACCT BONUSES          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

declare @BonusPointsDebit nchar(15)
set @BonusPointsDebit='1000'	

--TransStandard Variables
--declare @Trandate varchar(10), @Tipnumber char(15), @ACN nvarchar(25), @TranCode nvarchar(2), @TranNum nvarchar(4), @TranAmt nchar(15), @TranType nvarchar(20), @Ratio nvarchar(4), @CRDACTVLDT nvarchar(10)


--set @Trandate=@EndDate

--Put the Orpahned(no tipnumber) ACNs in a table
insert into ztmpNoTipACNs
select ACN 
from dbo.RedwoodInput 
where tipnumber is null

delete RedwoodInput where tipnumber is null


-- remove so that the date conversion following will work 
update dbo.redwoodinput
	set activedate = null
where isdate(activedate) = 0


/*
--Setup Cursor for processing
declare Tip_crsr cursor for 
	select distinct tipnumber, ACN 
	from RedwoodInput
	where isdate(ActiveDate) =1
	And convert(datetime,ActiveDate,1) Between @StartDate and  @Enddate 
	and Tipnumber not in (select Tipnumber from OneTimeBonuses where Trancode='BN'   )
	and ACN not in (select ACN from Replaced_Accts UNION Select Replacement_Acct from Replaced_Accts )

open Tip_crsr

fetch Tip_crsr into @Tipnumber,@ACN

while @@FETCH_STATUS = 0
begin	

		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select @tipnumber, @enddate, @ACN, 'BN', 1, @BonusPointsDebit, 'Bonus New Account', '1', ' ' 

		INSERT INTO OneTimeBonuses(TipNumber,TranCode, AcctID, DateAwarded)
    		Values(@Tipnumber, 'BN', @ACN, @Trandate)

	fetch Tip_crsr into @Tipnumber,@ACN
End

close  tip_crsr
deallocate  tip_crsr
*/

---------------------------------------------------------------------------------------------
--
--
--
---------------------------------------------------------------------------------------------


-- Get bonus points to be awarded
set @BonusPointsDebit=(
    select top 1 dim_bonusprogram_bonuspoints
    from dbo.bonusprogram bp join dbo.bonustype bt
	   on bp.sid_bonustype_id = bt.sid_bonustype_id
    where dim_bonustype_trancode = 'BN'
    and @enddate between dim_BonusProgram_EffectiveDate and dim_BonusProgram_ExpirationDate
    order by dim_bonusprogram_effectivedate desc)


INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select distinct ri.tipnumber, @enddate, ri.acn, 'BN', 1, @BonusPointsDebit, 'Bonus New Account', '1', ' '
from dbo.RedwoodInput RI left outer join (select Tipnumber from dbo.OneTimeBonuses where Trancode='BN') bn
    on ri.tipnumber = bn.tipnumber

left outer join (select ACN from dbo.Replaced_Accts UNION Select Replacement_Acct from dbo.Replaced_Accts ) ra
    on ri.acn = ra.acn

where   isdate(ActiveDate) =1
And	   convert(datetime, ActiveDate, 1) >= @StartDate and convert(datetime, ActiveDate, 1) <= @EndDate
and bn.tipnumber is null -- Row not found in the OneTimebonuses table
and ra.acn is null  -- row not found in the Replacement_Acct table


-- Add to OneTimeBonus table

INSERT INTO OneTimeBonuses(TipNumber,TranCode, AcctID, DateAwarded)
select distinct ri.tipnumber, 'BN', ri.acn, @enddate
from dbo.redwoodinput ri left outer join (select Tipnumber from dbo.OneTimeBonuses where Trancode='BN') bn
    on ri.tipnumber = bn.tipnumber

left outer join (select ACN from dbo.Replaced_Accts UNION Select Replacement_Acct from dbo.Replaced_Accts ) ra
    on ri.acn = ra.acn

where isdate(activedate) = 1
and convert(datetime, ActiveDate, 1) >= @StartDate and convert(datetime, ActiveDate, 1) <= @EndDate
and bn.tipnumber is null -- Row not found in the OneTimebonuses table
and ra.acn is null  -- row not found in the Replacement_Acct table
GO
