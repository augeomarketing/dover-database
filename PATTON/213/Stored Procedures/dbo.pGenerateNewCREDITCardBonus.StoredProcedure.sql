USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[pGenerateNewCREDITCardBonus]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[pGenerateNewCREDITCardBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGenerateNewCREDITCardBonus] 
	   @startDate varchar(10), 
	   @EndDate varchar(10), 
	   @TipFirst char(3)

AS 


/****************************************************************************/
/*                                                                          */
/*   ---------CREDIT CARD NEW ACCT BONUSES                                  */
/*                                                                          */
/*                                                                          */
/****************************************************************************/


declare @BonusPointsCredit nchar(15)


--TransStandard Variables
--declare @Trandate varchar(10), 
--	   @Tipnumber char(15), 
--	   @ACN nvarchar(25), 
--	   @TranCode nvarchar(2), 
--	   @TranNum nvarchar(4), 
--	   @TranAmt nchar(15), 
--	   @TranType nvarchar(20), 
--	   @Ratio nvarchar(4), 
--	   @CRDACTVLDT nvarchar(10)


--set @Trandate=@EndDate

/*          remove so that the date conversion following will work */
update dbo.redwoodinput
	set activedateCR = null
where isdate(activedateCR) = 0

/*

/*                                                                            */
/* Setup Cursor for processing                                                */
/* Award any tips a credit card bonus if they have a credit card and haven't gotten the bonus yet and they werent in the deconversion file                                              */

declare Tip_crsr cursor fast_forward for 
	   select distinct ri.tipnumber, ri.acn
	   from dbo.redwoodinput ri left outer join dbo.credit_control cc
		  on ri.acn = cc.acn

	   left outer join dbo.onetimebonuses otb
		  on ri.acn = otb.acctid
		  and 'FJ'  = otb.trancode

	   left outer join (select ACN 
					    from Replaced_Accts_History 
					    UNION 
					    Select Replacement_Acct 
					    from Replaced_Accts_History 
					    UNION 
					    select ACN 
					    from Replaced_Accts 
					    UNION 
					    Select Replacement_Acct 
					    from Replaced_Accts) rah
		  on ri.acn = rah.acn
	   where ri.statuscr = 'OPEN'
	   and	 cc.acn is null
	   and   otb.acctid is null
	   and   rah.acn is null

/* this is the old query in the above cursor
	select distinct tipnumber, ACN 
	from RedwoodInput
	where StatusCR = 'OPEN'
	and ACN not in (select ACN from [CREDIT_CONTROL])
	and ACN not in (select ACCTID from OneTimeBonuses where Trancode='FJ'   )
	-- and ACN not in (select ACN from Replaced_Accts UNION Select Replacement_Acct from Replaced_Accts )   --- changed for Jan 09 data processsing
	and ACN not in (select ACN 
				 from Replaced_Accts_History 
				 UNION 
				 Select Replacement_Acct 
				 from Replaced_Accts_History 
				 UNION 
				 select ACN 
				 from Replaced_Accts 
				 UNION 
				 Select Replacement_Acct 
				 from Replaced_Accts)

*/


/* bad for October initial processing
declare Tip_crsr cursor for 
	select distinct tipnumber, ACN 
	from RedwoodInput
	where isdate(ActiveDateCR) =1
	and ACN not in (select ACN from [zSeptCreditControl])
	and Tipnumber not in (select Tipnumber from OneTimeBonuses where Trancode='FJ'   )
*/



/* old BAD code 
declare Tip_crsr cursor for 
	select distinct tipnumber, ACN 
	from RedwoodInput
	where isdate(ActiveDate) =1
	And ActiveDate Between @StartDate and  @Enddate 
	and Tipnumber not in (select Tipnumber from OneTimeBonuses where Trancode='FJ'   )
*/

open Tip_crsr

fetch Tip_crsr into @Tipnumber,@ACN

while @@FETCH_STATUS = 0
begin	

		Begin
 
		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select @tipnumber, @enddate, @ACN, 'FJ', 1, @BonusPointsCredit, 'New Credit Card', '1', ' ' 

			INSERT INTO OneTimeBonuses(TipNumber,TranCode, AcctID,DateAwarded)
        		Values(@Tipnumber, 'FJ', @ACN,@Trandate)


		End

		fetch Tip_crsr into @Tipnumber,@ACN
End
Fetch_Error:

close  tip_crsr
deallocate  tip_crsr


*/
----------------------------------------------------------------------------
--
--
--
----------------------------------------------------------------------------

-- Get bonus points to be awarded
set @BonusPointsCredit=(
    select top 1 dim_bonusprogram_bonuspoints
    from dbo.bonusprogram bp join dbo.bonustype bt
	   on bp.sid_bonustype_id = bt.sid_bonustype_id
    where dim_bonustype_trancode = 'FJ'
    and @enddate between dim_BonusProgram_EffectiveDate and dim_BonusProgram_ExpirationDate
    order by dim_bonusprogram_effectivedate desc)



insert into dbo.transstandard
(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select distinct ri.tipnumber, @enddate, ri.acn, 'FJ', 1, @BonusPointsCredit, 'New Credit Card', '1', ' '

from dbo.redwoodinput ri left outer join dbo.credit_control cc
	  on ri.acn = cc.acn

left outer join (select tipnumber from dbo.history where trancode = 'FJ') H 
    on ri.tipnumber = h.tipnumber

left outer join (select ACN 
			    from dbo.Replaced_Accts_History 
			    UNION 
			    Select Replacement_Acct 
			    from dbo.Replaced_Accts_History 
			    UNION 
			    select ACN 
			    from dbo.Replaced_Accts 
			    UNION 
			    Select Replacement_Acct 
			    from dbo.Replaced_Accts) rah
	  on ri.acn = rah.acn
where ri.statuscr = 'OPEN'
    and	 cc.acn is null
    and   h.tipnumber is null
    and   rah.acn is null


-- Add to OneTimeBonus table

INSERT INTO OneTimeBonuses(TipNumber,TranCode, AcctID, DateAwarded)
select distinct ri.tipnumber, 'FJ', ri.acn, @enddate
from dbo.redwoodinput ri left outer join (select Tipnumber from dbo.OneTimeBonuses where Trancode='FJ') bn
    on ri.tipnumber = bn.tipnumber

left outer join (select ACN from dbo.Replaced_Accts UNION Select Replacement_Acct from dbo.Replaced_Accts ) ra
    on ri.acn = ra.acn

where isdate(activedate) = 1
and convert(datetime, ActiveDate, 1) >= @StartDate and convert(datetime, ActiveDate, 1) <= @EndDate
and bn.tipnumber is null -- Row not found in the OneTimebonuses table
and ra.acn is null  -- row not found in the Replacement_Acct table
GO
