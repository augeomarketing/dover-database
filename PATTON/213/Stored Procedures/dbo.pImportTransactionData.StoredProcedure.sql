USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[pImportTransactionData]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[pImportTransactionData]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pImportTransactionData] @TipFirst char(3), @StartDate char(10), @enddate char(10)
AS 
declare @TFNO nvarchar(15), @Trandate nchar(10), @ACCT_NUM nvarchar(25), @TRANCODE nvarchar(2), @TRANNUM nvarchar(4), @TRANAMT nvarchar(15), @TRANTYPE nvarchar(20), @RATIO nvarchar(4), @CRDACTVLDT nvarchar(10)  
declare @MaxPointsPerYear decimal(9), @YTDEarned numeric(9), @AmtToPost numeric (9), @Overage numeric(9)
 
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                      */
/*                                                                            */
/******************************************************************************/
/* Rev=1         */
/* Date 9/2007   */
/*  By SEB  */
/* SCAN: SEB001  */
/* Reason:  to fix error with posting the wrong amount to history when there is an overage */
set @MaxPointsPerYear=(select MaxPointsPerYear from client where  tipfirst=@tipfirst)
If @MaxPointsPerYear='0'
	begin
	set @MaxPointsPerYear='999999999'
	end
--set @MaxPointsPerYear=(select MaxPointsPerYear from client where clientcode='AdvantageOne')
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING DEMOGRAPHICIN                               */
/*                                                                            */
declare Tran_crsr cursor
for select TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, rtrim(TRANAMT), TRANTYPE, RATIO, CRDACTVLDT
from TransStandard
--where TFNO='213000000000036'
order by TFNO
/*                                                                            */
open Tran_crsr
fetch Tran_crsr into @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
 
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING                                                            */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin
	/*********************************************************************/
	/*  Purchase transaction                                             */
	/*********************************************************************/
	if (@Trancode='67' OR @Trancode='63')
	Begin
		/*********************************************************************/
		/*  Check MAX YTD Earned                                             */
		/*********************************************************************/

		--All affiliate records have the same value in YTDEarned, so get the top 1	
		set @YTDEarned=isnull((select top 1 YTDEarned from affiliat where Tipnumber=@TFNO),0)	
		if (@YTDEarned) >= @MAXPOINTSPERYEAR
				Begin
					set @OVERAGE = @TRANAMT
					set @AmtToPost='0'
						
					insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
					values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, ' ', @RATIO, @OVERAGE) /* SEB001 */			
					
					Goto NextRecord
				End
		--if the YTDEarned + TranAmt <= maxPoints, post transaction to history and update YTDEarned for all affiliat records
		if (@YTDEarned + @Tranamt)<= @MAXPOINTSPERYEAR
		        Begin
			        set @OVERAGE = '0'
			        set @AmtToPost=@TRANAMT
					set @YTDEarned= @YTDEarned + @AmttoPost
					insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
					values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, ' ', @RATIO, @OVERAGE) 			
					
					update customer
					set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
					where tipnumber=@TFNO		
					
					update affiliat
					set YTDEarned=@YTDEarned 
					where Tipnumber=@TFNO		
					Goto NextRecord
				End
		--if YTDEarned + TranAmt GT MaxPoints, detrmine amount to post,amt to put in overage and amt to increase YTDEarned
		if (@YTDEarned + @Tranamt)> @MAXPOINTSPERYEAR
				Begin
					set @OVERAGE = (@YTDEarned + rtrim(@Tranamt))- @MAXPOINTSPERYEAR
			        set @AmtToPost=convert(numeric,rtrim(@TRANAMT) - @OVERAGE)
					set @YTDEarned= @YTDEarned + @AmttoPost


					insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
					values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, ' ', @RATIO, @OVERAGE) 			
					update customer
					set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
					where tipnumber=@TFNO	
	
					update affiliat
					set YTDEarned=@YTDEarned 
					where Tipnumber=@TFNO	
						
					Goto NextRecord
				End
	End
	/*********************************************************************/
	/*  TRANSFER BONUS                                              */
	/*********************************************************************/
	--if transferring points or New debit or credit card
	if (@Trancode='TP' or @Trancode='BN' Or @Trancode='FJ')
	Begin
	        	set @AmtToPost = @TRANAMT
		insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
		/* SEB001 values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, ' ', @RATIO, @OVERAGE) */			
		values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, ' ', @RATIO, @OVERAGE) /* SEB001 */	
	
		update customer
		set runavailable=runavailable + @AmtToPost, runbalance=runbalance + @AmtToPost
		where tipnumber=@TFNO	
	

		
		Goto NextRecord
	End

	/*********************************************************************/
	/*  Return transaction            THIS NEVER APPLIES SINCE TranAmt comes in NEGATIVE and gets a ratio of 1                                  */
	/*********************************************************************/
	if (@Trancode='37' or @Trancode='33')
	Begin
	        	set @AmtToPost = @TRANAMT
		insert into history (Tipnumber, Acctid, Histdate, Trancode, Trancount, Points, Description, SECID, Ratio, Overage)
		/* SEB001 values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, ' ', @RATIO, @OVERAGE) */			
		values(@TFNO, @ACCT_NUM, @TRANDATE, @TRANCODE, @TRANNUM, @AmtToPost, @TRANTYPE, ' ', @RATIO, @OVERAGE) /* SEB001 */	
	
		update customer
		set runavailable=runavailable - @AmtToPost, runbalance=runbalance - @AmtToPost
		where tipnumber=@TFNO			
		Goto NextRecord
	End


NextRecord:
	fetch Tran_crsr into @TFNO, @TRANDATE, @ACCT_NUM, @TRANCODE, @TRANNUM, @TRANAMT, @TRANTYPE, @RATIO, @CRDACTVLDT
end
Fetch_Error:
close  Tran_crsr
deallocate  Tran_crsr
GO
