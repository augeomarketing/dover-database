USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spReplacement_Cards_Process1]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spReplacement_Cards_Process1]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReplacement_Cards_Process1] @EndDate char(10)
AS 

--this is the old version used up through Nov data processing

/*CHANGE TABLE TO REDWOODINPUT*/

update  RedwoodInput set Replacement_Acct=NULL where len(rtrim(ltrim(Replacement_Acct)))=0

truncate table Replaced_Accts

insert into Replaced_Accts  
		Select *,  @EndDate from RedwoodInput where Replacement_Acct is NOT null

insert into Replaced_Accts_History
	select * from  Replaced_Accts   



--update the ACN of the input so that customers and input data will match
Update RedwoodInput set ACN=Replacement_Acct where Replacement_Acct is not null

---------------update all of the working tables with the NEW account number
--Customer
update a set Misc1=w.Replacement_Acct
	from Replaced_Accts w
	join Customer a 
	on w.ACN = a.Misc1

--CustomerDeleted
update a set Misc1=w.Replacement_Acct
	from Replaced_Accts w
	join CustomerDeleted a 
	on w.ACN = a.Misc1



--AFFILIAT
update a set 
	AcctID=w.Replacement_Acct,
	CustID=w.Replacement_Acct
	from Replaced_Accts w
	join Affiliat a 
	on w.ACN = a.AcctID

--AFFILIATDELETED
update a set AcctID=w.Replacement_Acct
	from Replaced_Accts w
	join AffiliatDeleted a 
	on w.ACN = a.AcctID


--History
update a set AcctID=w.Replacement_Acct
	from Replaced_Accts w
	join History a 
	on w.ACN = a.AcctID
--HistoryDeleted
update a set AcctID=w.Replacement_Acct
	from Replaced_Accts w
	join HistoryDeleted a 
	on w.ACN = a.AcctID
GO
