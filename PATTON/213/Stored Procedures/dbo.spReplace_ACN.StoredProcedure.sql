USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spReplace_ACN]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spReplace_ACN]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spReplace_ACN] @EndDate char(10), @OldACN nvarchar(10), @NewACN nvarchar(10)
AS 



---------------update all of the working tables with the NEW account number
--Customer
update Customer set Misc1=@NewACN
	where Misc1=@OldACN

--CustomerDeleted
update CustomerDeleted set Misc1=@NewACN
	where Misc1=@OldACN



--AFFILIAT
update Affiliat set 
	AcctID=@NewACN
	WHERE AcctID=@OldACN

--AFFILIATDELETED
update AffiliatDeleted set
	AcctID=@NewACN
	WHERE AcctID=@OldACN


--History
update History set
	AcctID=@NewACN
	WHERE AcctID=@OldACN

--HistoryDeleted
update HistoryDeleted set
	AcctID=@NewACN
	WHERE AcctID=@OldACN
GO
