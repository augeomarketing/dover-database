USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[zzzDELETEpGenerateNewCREDITCardBonus]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[zzzDELETEpGenerateNewCREDITCardBonus]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzzDELETEpGenerateNewCREDITCardBonus] @startDate varchar(10), @EndDate varchar(10), @TipFirst char(3)
AS 
/****************************************************************************/
/*                                                                          */
/*   ---------CREDIT CARD NEW ACCT BONUSES          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
declare @BonusPointsCredit nchar(15)
set @BonusPointsCredit='2500'	
--TransStandard Variables
declare @Trandate varchar(10), @Tipnumber char(15), @ACN nvarchar(25), @TranCode nvarchar(2), @TranNum nvarchar(4), @TranAmt nchar(15), @TranType nvarchar(20), @Ratio nvarchar(4), @CRDACTVLDT nvarchar(10)


set @Trandate=@EndDate

/*          remove so that the date conversion following will work */
update dbo.redwoodinput
	set activedateCR = null
where isdate(activedateCR) = 0

/*                                                                            */
/* Setup Cursor for processing                                                */
/* Setup Cursor for processing                                                */
declare Tip_crsr cursor for 
	select distinct tipnumber, ACN 
	from RedwoodInput
	where isdate(ActiveDateCR) =1
	And convert(datetime,ActiveDateCR,1) Between @StartDate and  @Enddate 
	and Tipnumber not in (select Tipnumber from OneTimeBonuses where Trancode='FJ'   )
/* old BAD code 
declare Tip_crsr cursor for 
	select distinct tipnumber, ACN 
	from RedwoodInput
	where isdate(ActiveDate) =1
	And ActiveDate Between @StartDate and  @Enddate 
	and Tipnumber not in (select Tipnumber from OneTimeBonuses where Trancode='FJ'   )
*/
/*                                                                            */
open Tip_crsr
/*                                                                            */
fetch Tip_crsr into @Tipnumber,@ACN
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
begin	

		Begin
 
		INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
		select @tipnumber, @enddate, @ACN, 'FJ', 1, @BonusPointsCredit, 'New Credit Card', '1', ' ' 

			INSERT INTO OneTimeBonuses(TipNumber,TranCode, DateAwarded)
        		Values(@Tipnumber, 'FJ', @Trandate)
		End
		goto Next_Record
Next_Record:
		fetch Tip_crsr into @Tipnumber,@ACN
End
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
