USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[zzzDELETE_spLoadCUSTIN]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[zzzDELETE_spLoadCUSTIN]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzzDELETE_spLoadCUSTIN] @dateadded char(10), @tipFirst char(3)
AS 
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLupdate nvarchar(1000)

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO LOAD CUSTIN TABLE                                            */
/*                                                                            */
/******************************************************************************/


set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.custin'
Exec sp_executesql @SQLTruncate

/***  Set Status Code  ***/

set @SQLupdate='Update ' + QuoteName(@DBName) + N'.DBO.RedwoodInput
			set status=''C'' 
			where status=''CLOSED'' '
exec sp_executesql @SQLupdate

set @SQLupdate='Update ' + QuoteName(@DBName) + N'.DBO.RedwoodInput
			set status=''A'' 
			where status <> ''CLOSED'' and status<> ''C'' '
exec sp_executesql @SQLupdate

-- Add to CUSTIN TABLE
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.custin(ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, Homephone, workphone, DateAdded, MISC1 ) 
		select ACN, NAME1, NAME2, NAME3, NAME4, NAME5, NAME6, STATUS, TIPNUMBER, Addr1, Addr2, (rtrim([City ]) + '' '' + rtrim(state) + '' '' + rtrim(zipcode)), [City ], left(STate,2), ZIPcode, LASTNAME, homephone, workphone, @DateAdded, ACN
		from ' + QuoteName(@DBName) + N'.DBO.RedwoodInput
		 order by tipnumber'

Exec sp_executesql @SQLInsert, N'@DATEADDED nchar(10)',	@Dateadded= @Dateadded
GO
