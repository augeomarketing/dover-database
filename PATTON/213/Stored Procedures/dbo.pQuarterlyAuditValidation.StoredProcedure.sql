USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[pQuarterlyAuditValidation]    Script Date: 06/30/2011 10:32:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pQuarterlyAuditValidation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pQuarterlyAuditValidation]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[pQuarterlyAuditValidation] 
AS
/******************************************************************************/
/*                                                                            */
/*    THIS IS TO make sure the Quarterly file balances                          */
/*                                                                            */
/******************************************************************************/
declare @Tipnumber nchar(15), @pointsbegin numeric(9), @pointsend numeric(9), @pointspurchasedcr numeric(9), 
@pointsbonuscr numeric(9), @pointspurchasedDB numeric(9), @pointsbonusdb numeric(9), @pointsadded numeric(9),
 @pointsincreased numeric(9), @pointsredeemed numeric(9), @pointsreturnedcr numeric(9), @pointsreturneddb numeric(9), 
 @pointssubtracted numeric(9), @pointsdecreased numeric(9), @errmsg varchar(50), @currentend numeric(9),
 @pointsbonusMN numeric(9)
truncate table Quarterly_Audit_ErrorFile
/*                                                                            */
/*  DECLARE CURSOR FOR PROCESSING Quarterly Statement TABLE                     */
/*                                                                            */
declare tip_crsr cursor
for select Tipnumber, pointsbegin, pointsend, pointspurchasedcr, pointsbonuscr, pointsadded, pointspurchaseddb, 
pointsbonusdb, pointsincreased, pointsredeemed, pointsreturnedcr, pointssubtracted, 
pointsreturneddb, pointsdecreased, pointsbonusMN 
from Quarterly_Statement_File
/*                                                                            */
open tip_crsr
/*                                                                            */
fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, 
@pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted,
 @pointsreturneddb, @pointsdecreased, @pointsbonusMN
/******************************************************************************/	
/*                                                                            */
/* MAIN PROCESSING  VERIFICATION                                              */
/*                                                                            */
if @@FETCH_STATUS = 1
	goto Fetch_Error
/*                                                                            */
while @@FETCH_STATUS = 0
	begin	
	set @errmsg=NULL
	set @currentend='0'
	if @pointsend<>(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)
		begin
		set @errmsg='Ending Balances do not match'
		set @currentend=(select AdjustedEndingPoints from Current_Month_Activity where tipnumber=@tipnumber)		
		INSERT INTO Quarterly_Audit_ErrorFile
       			values(@Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, 
       			@pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, 
       			@pointssubtracted, @pointsreturneddb, @pointsdecreased, @errmsg, @currentend,@pointsbonusMN)
		goto Next_Record
		end
		
goto Next_Record
Next_Record:
		fetch tip_crsr into @Tipnumber, @pointsbegin, @pointsend, @pointspurchasedcr, @pointsbonuscr, @pointsadded, 
		@pointspurchaseddb, @pointsbonusdb, @pointsincreased, @pointsredeemed, @pointsreturnedcr, @pointssubtracted, 
		@pointsreturneddb, @pointsdecreased,@pointsbonusMN
		
	end
Fetch_Error:
close  tip_crsr
deallocate  tip_crsr
GO
