USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spRemoveApostrophes]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spRemoveApostrophes]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spRemoveApostrophes] @TipFirst char(3)
AS

/*  *****************************************************************************************  	*/
/* Date: 3/30/2007                                                                          	*/
/* Author:  S. Blanchette  				 					*/
/*  *****************************************************************************************  	*/
/*  Description: Removes apostrophes from name and address fields                               */
/*                                  								*/

/*  Tables:
		DBProcessInfo
		Demographicin  
*/
/*  Revisions: 
*/
/*  *****************************************************************************************  	*/


declare @DBName varchar(50), @SQLUpdate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @sqlupdate=N'Update ' + QuoteName(@DBName) + N' .dbo.RedwoodInput set Addr1=replace(Addr1,char(39), '' ''), Addr2=replace(Addr2,char(39), '' ''), CITY=replace(City,char(39), '' ''), NAME1=replace(NAME1,char(39), '' ''), NAME2=replace(NAME2,char(39), '' ''), NAME3=replace(NAME3,char(39), '' ''), NAME4=replace(NAME4,char(39), '' ''), NAME5=replace(NAME5,char(39), '' ''), NAME6=replace(NAME6,char(39), '' '') '
		exec sp_executesql @SQLUpdate
GO
