USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[zzz_DELETE_pCheckRemovedCards]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[zzz_DELETE_pCheckRemovedCards]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[zzz_DELETE_pCheckRemovedCards] 
AS 
/************************************************************************************/
/*                                                                                  */
/*  Acctid in Affiliat table is actually the last six of cardno                     */
/*  Secid in Affiliat table is actually the Acctno (DDA#) for Spirit Bank           */
/*  Custid in Affiliat table is actually the CIS# for Spirit Bank                   */
/*                                                                                  */
/************************************************************************************/

Update affiliat 
set acctstatus = 'C' where not exists(select * from redwoodinput where acn= affiliat.acctid)

Update affiliat
set acctstatus='C' where acctid in(select acn from redwoodinput where status<>'OPEN')
GO
