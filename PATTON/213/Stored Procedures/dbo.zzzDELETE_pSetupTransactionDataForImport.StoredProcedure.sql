USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[zzzDELETE_pSetupTransactionDataForImport]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[zzzDELETE_pSetupTransactionDataForImport]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[zzzDELETE_pSetupTransactionDataForImport] @StartDate char(10), @EndDate char(10), @TipFirst char(3)
AS 



/******************************************************************************/
/*                                                                            */
/*    THIS IS TO IMPORT TRANSACTION DATA                                         */
/*                                                                            */
/******************************************************************************/
/******************************************************************************/
/*									       */
/* BY:  S.Blanchette  */
/* DATE: 7/2007   */
/* REVISION: 1 */
/* SCAN: SEB001 */
/* Changed logic to group by tip and pan */

/*									       */
/* BY:  S.Blanchette  */
/* DATE: 12/2007   */
/* REVISION: 2 */
/* SCAN: SEB002 */
/* fixed logic so that msgtype 04 reflects a reversal or return */

truncate table transwork
truncate table transstandard

insert into transwork (Trandate, Pan, processingcode, Amounttran, tipnumber, numberoftrans)
select @enddate, acn, ' ', amount, tipnumber, quantity
from RedwoodInput
-- 
update transwork
set points=ROUND(((amounttran)/2), 10, 0)


-- SEB001
INSERT INTO TransStandard(TFNO, TRANDATE, ACCT_NUM, TRANCODE, TRANNUM, TRANAMT, TRANTYPE, RATIO, CRDACTVLDT)
select tipnumber, @enddate, Pan, '67', sum(NumberOfTrans), sum(points), 'DEBIT', '1', ' ' from transwork
group by tipnumber, Pan
GO
