USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spLoadCUSTIN]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spLoadCUSTIN]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spLoadCUSTIN] @dateadded char(10), @tipFirst char(3)
AS 
declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000), @SQLupdate nvarchar(1000)

--====================================================
--new code added 10/9/09 that checks to see if there are any recs that have no open
--debit or credit accounts. Any of these that do NOT already exist in the database 
--are deleted from the input file and written off to the RedwoodInput_Deleted table
--this is because FI is including records for people that they are unsure about whether
--or not they already have a RN acccount and don't want them to get one.
--
-- SEB 10/22/2014  Added field [Email]
truncate table RedwoodInput_Deleted

insert into RedwoodInput_Deleted
select RedwoodInput.* from RedwoodInput 
			where
			(
				( 
				(status='CLOSED'  AND statusCR='CLOSED')
				OR (status='CLOSED'  AND statusCR='NA')
				OR (status='NA'  AND statusCR='CLOSED')
				)
			 AND
				(ACN not in (select acctid from affiliat))
			)

Delete RedwoodInput where acn in (select ACN from RedwoodInput_Deleted)


--end 10/9/09 code 
--==========================================================

/******************************************************************************/
/*                                                                            */
/*    THIS IS TO LOAD CUSTIN TABLE                                            */
/*                                                                            */
set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)
set @SQLTruncate='Truncate Table ' + QuoteName(@DBName) + N'.dbo.custin'
Exec sp_executesql @SQLTruncate

--Updates the IsDebitOpen field which is used late in pSetupTransactionDataForImport. Need to do this here because the status gets changed to A or C later in this proc.
Update redwoodinput set IsDebitOpen=1 where (Status='OPEN' ) 

/***  Set RedwoodInput.Status Code  =C where both the Debit and Credit status are BOTH 'CLOSED or where one is closed and the other is NA'***/
set @SQLupdate='Update ' + QuoteName(@DBName) + N'.DBO.RedwoodInput
			set status=''C'' 
			where (status=''CLOSED''  AND statusCR=''CLOSED'')
			OR (status=''CLOSED''  AND statusCR=''NA'')
			OR (status=''NA''  AND statusCR=''CLOSED'')'
exec sp_executesql @SQLupdate

--Set RedwoodInput.Status='A' where status<>'CLOSED' and status<>'C'
set @SQLupdate='Update ' + QuoteName(@DBName) + N'.DBO.RedwoodInput
			set status=''A'' 
			where (status <> ''CLOSED'' and status<> ''C'' ) OR StatusCR=''OPEN'''
exec sp_executesql @SQLupdate



-- Add to CUSTIN TABLE from RedwoodInput
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.custin(ACCT_NUM, NAMEACCT1, NAMEACCT2, NAMEACCT3, NAMEACCT4, NAMEACCT5, NAMEACCT6, STATUS, TIPNUMBER, Address1, Address2, Address4, City, State, Zip, LASTNAME, Homephone, workphone, DateAdded, MISC1, EMAIL ) 
		select ACN, NAME1, NAME2, NAME3, NAME4, NAME5, NAME6, STATUS, TIPNUMBER, Addr1, Addr2, left(rtrim([City ]) + '' '' + rtrim(state) + '' '' + left(rtrim(zipcode),5),40   )   , [City], left(STate,2), ZIPcode, LASTNAME, homephone, workphone, @DateAdded, ACN, Email
		from ' + QuoteName(@DBName) + N'.DBO.RedwoodInput
		 order by tipnumber'
Exec sp_executesql @SQLInsert, N'@DATEADDED nchar(10)',	@Dateadded= @Dateadded
GO
