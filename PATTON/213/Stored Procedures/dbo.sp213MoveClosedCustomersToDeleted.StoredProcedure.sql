USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[sp213MoveClosedCustomersToDeleted]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[sp213MoveClosedCustomersToDeleted]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE procedure [dbo].[sp213MoveClosedCustomersToDeleted]  @EnddateParm char(10)
as

-- S Blanchette  8/2013  Changed to add time stamp to end date
-- S Blanchette 4/2014 Changed @DateDeleted time stamp to 0 because of issue liability .

declare @DBName varchar(50), @SQLUpdate nvarchar(1000), @SQLDelete nvarchar(1000), @SQLInsert nvarchar(1000), @DateToDelete char(6), @dateclosed datetime, @newmonth nchar(2), @newyear nchar(4)

Declare @EndDate DateTime -- S Blanchette  8/2013	
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990')	-- S Blanchette  8/2013

declare @datedeleted datetime
set @datedeleted=convert(datetime, @EndDateParm+' 00:00:00:000')	-- S Blanchette  4/2014
--set @datedeleted=getdate()

/********************************************************************/
/*  Code to build work table with tipnumbers that are to be deleted */
/********************************************************************/
set @newmonth= cast(datepart(month, @datedeleted) as char(2)) 
set @newyear= cast(datepart(yyyy,@datedeleted) as char(4)) 

if CONVERT( int , @newmonth)<'10' 
	begin
	set @newmonth='0' + left(@newmonth,1)
	end	

set @DateToDelete = @newmonth + @newyear

--Drop Table #ClosedCustomers
CREATE TABLE #ClosedCustomers (Tipnumber nchar(15))

INSERT INTO [#ClosedCustomers] (Tipnumber)
       	SELECT TIPNumber FROM Customer_Closed where DateToDelete = @DateToDelete
/********************************************************************/
/*  End Code to build work table with tipnumbers that to be deleted */
/********************************************************************/

/**********************************************************/
/*  Start add code  SEB001                                */
/**********************************************************/

delete from [#ClosedCustomers]
			where tipnumber in (select tipnumber from history where histdate>@enddate) 



/**********************************************************/

INSERT INTO CustomerDeleted 
       	SELECT TIPNumber, TIPFirst, TIPLast, AcctName1, AcctName2, AcctName3, AcctName4, AcctName5, AcctName6, Address1, Address2, Address3, Address4, City, State, Zipcode, LastName, Status, StatusDescription, HomePhone, WorkPhone, RunBalance, RunRedeemed, RunAvailable, LastStmtDate, NextStmtDate, DateAdded, Notes, ComboStmt, RewardsOnline, EmployeeFlag, BusinessFlag, SegmentCode, Misc1, Misc2, Misc3, Misc4, Misc5, RunBalanceNew, RunAvaliableNew, @datedeleted 
	FROM Customer where status='C' and Tipnumber in (Select Tipnumber from [#ClosedCustomers])

INSERT INTO AffiliatDeleted (TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, datedeleted)
       	SELECT TIPNumber, accttype, dateadded, SecID, acctid, AcctStatus, AcctTypeDesc, lastname, ytdearned, custid, @datedeleted
	FROM affiliat 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 

INSERT INTO historyDeleted (TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, datedeleted)
       	SELECT TIPNumber, acctid, histdate, trancode, trancount, points, description, secid, ratio, overage, @datedeleted
	FROM history
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 

/* */
/* NO ACCOUNT REFERENCE_DELETED TABLE IN 213
INSERT INTO Account_Reference_Deleted (TIPNumber, acctnumber, tipfirst, datedeleted)
       	SELECT TIPNumber, acctnumber, tipfirst, @Datedeleted
	FROM Account_Reference 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 
 */
/* */
/* */
Delete from affiliat 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 

/* */
/* */
Delete from history 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 

/* */
/* */
Delete from customer 
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 

/* */
/* NO ACCOUNT REFERENCE TABLE IN 213
Delete from [Account_Reference]
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 
 */
/* */
/* */
Delete from [Beginning_Balance_Table]
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers]) 



Delete from [Customer_Closed]
	where Tipnumber in (Select Tipnumber from [#ClosedCustomers])
GO
