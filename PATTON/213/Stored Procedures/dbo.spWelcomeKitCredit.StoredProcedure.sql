USE [213Redwood]
GO
/****** Object:  StoredProcedure [dbo].[spWelcomeKitCredit]    Script Date: 06/30/2011 10:32:42 ******/
DROP PROCEDURE [dbo].[spWelcomeKitCredit]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spWelcomeKitCredit] @EndDate varchar(10), @TipFirst nchar(3)
AS 
--base the sql for the CREDIT only or Credit/Debit  welcome kits on StatusCR='OPEN' and Tip not in Welcomkit (and DATEADDED = @EndDate AND Status <> ''c'')'

declare @DBName varchar(50), @SQLInsert nvarchar(1000), @SQLTruncate nvarchar(1000)

set @DBName=(SELECT  rtrim(DBNamePatton) from rewardsnow.dbo.DBProcessInfo where DBNumber=@TipFirst)

set @sqlTruncate=N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Welcomekit '
exec sp_executesql @SQLTruncate

set @sqlInsert=N'insert into ' + QuoteName(@DBName) + N' .dbo.Welcomekit SELECT TIPNUMBER, ACCTNAME1, ACCTNAME2, ACCTNAME3, ACCTNAME4, ADDRESS1, ADDRESS2, ADDRESS3, City, State, ZipCode FROM ' + QuoteName(@DBName) + N' .dbo.customer 
WHERE DATEADDED = @EndDate AND Status<> ''c''
AND Tipnumber in (Select tipnumber from RedwoodInput where StatusCR=''OPEN'')  '
exec sp_executesql @SQLInsert, N'@Enddate nchar(10)',@Enddate=@Enddate
GO
