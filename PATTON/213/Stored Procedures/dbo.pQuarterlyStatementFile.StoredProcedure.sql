USE [213Redwood]
GO

/****** Object:  StoredProcedure [dbo].[pQuarterlyStatementFile]    Script Date: 07/18/2012 15:17:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pQuarterlyStatementFile]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pQuarterlyStatementFile]
GO

USE [213Redwood]
GO

/****** Object:  StoredProcedure [dbo].[pQuarterlyStatementFile]    Script Date: 07/18/2012 15:17:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[pQuarterlyStatementFile] @StartDateParm char(10), @EndDateParm char(10), @Tipfirst char(3)
--CREATE PROCEDURE pQuarterlyStatementFile @StartDate varchar(10), @EndDate varchar(10), @Tipfirst char(3)
AS 

/*******************************************************************************/
/* RDT 10/09/2006 Added HH:MM:SS:MMM to dates
- Changed names on input parms
- declared old parms as datetime
- added HH:MM:SS:MMM to input parms and loaded old parms
*/
/*******************************************************************************/
/*******************************************************************************/
/* SEB 7/07 Added check for acctstatus when getting gard number
*/
/*******************************************************************************/

-- SEB 10/2013 Add Bonus codes H0 and H9

Declare @StartDate DateTime     --RDT 10/09/2006
Declare @EndDate DateTime     --RDT 10/09/2006
set @Startdate = convert(datetime, @StartDateParm + ' 00:00:00:001')    --RDT 10/09/2006
set @Enddate = convert(datetime, @EndDateParm+' 23:59:59:990' )    --RDT 10/09/2006

declare 
		@DBName varchar(50), 
		@SQLUpdate nvarchar(max),  
		@MonthBucket char(10), 
		@SQLTruncate nvarchar(max), 
		@SQLInsert nvarchar(max),
		@monthbegin char(2)


set @DBName=(SELECT  rtrim(DBNamePatton) from Rewardsnow.dbo.DBProcessInfo
				where DBNumber=@TipFirst)

set @monthbegin=CONVERT( int , left(@StartDateParm,2))
set @MonthBucket='MonthBeg' + @monthbegin

/*******************************************************************************/
/*******************************************************************************/
/*                                                                             */
/*          ISSUES WITH ADJUSTMENTS                                            */
/*                                                                             */
/*******************************************************************************/
/*******************************************************************************/
/* Load the statement file from the customer table  */
set @sqlTruncate= N'Truncate Table ' + QuoteName(@DBName) + N' .dbo.Quarterly_Statement_File '
exec sp_executesql @SQLTruncate

/*set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip, acctnum, lastfour)
        	select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + zipcode), zipcode, 
		from ' + QuoteName(@DBName) + N'.dbo.customer ' */
set @SQLInsert='INSERT INTO ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File(tipnumber, acctname1, acctname2, address1, address2, address3, citystatezip, zip)
        	select tipnumber, acctname1, acctname2, address1, address2, address3, (rtrim(city) + '' '' + rtrim(state) + '' '' + LEFT(zipcode,5)), LEFT(zipcode,5) 
		from ' + QuoteName(@DBName) + N'.dbo.customer '
Exec sp_executesql @SQLInsert
 
set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set acctnum=(select top 1 rtrim(acctid) from ' + QuoteName(@DBName) + N'.dbo.affiliat where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber and acctstatus=''A'' ) '
Exec sp_executesql @SQLUpdate

set @SQLUpdate='Update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set lastfour=right(rtrim(acctnum),4) '
Exec sp_executesql @SQLUpdate

/* Load the statmement file with CREDIT purchases   37       */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointspurchasedcr=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''63'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''63'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with DEBIT purchases   67       */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointspurchaseddb=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''67'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''67'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with bonuses            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsbonuscr=
		(select sum(points*ratio) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''B%'' or trancode=''NW''or trancode=''FJ'')) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''B%'' or trancode=''NW'' or trancode=''FJ'')) '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with plus adjustments    */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsadded=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and
		 trancode in(''IE'', ''TR'')) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and
		 trancode in(''IE'', ''TR'')) '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with Purchased Points "PP"   */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set PurchasedPoints=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and 
		trancode in(''PP'')) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and 
		trancode in(''PP'')) '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with PointsBonusMN  F0.F9.G0.G9   */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set PointsBonusMN=
		(select sum(points*ratio) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and 
		trancode in(''F0'',''F9'',''G0'',''G9'',''H0'',''H9'')) 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and 
		trancode in(''F0'',''F9'',''G0'',''G9'',''H0'',''H9'')) '
		
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate



/* Load the statmement file with total point increases */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsincreased=pointspurchasedcr + 
pointspurchaseddb + pointsbonuscr + pointsbonusdb + pointsadded + PurchasedPoints + PointsBonusMN'
Exec sp_executesql @SQLUpdate

/* Load the statmement file with redemptions          */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsredeemed=
		(select sum(points*ratio*-1) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) +
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and ( trancode like ''R%'' 
		or  trancode = ''IR'')   ) 	
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) +
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and (trancode like ''R%''  
		or  trancode = ''IR'')  ) '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file decreased redeemed         */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsredeemed=pointsredeemed -
		(select sum(points*ratio) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''DR'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode =''DR'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with CREDIT returns            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsreturnedcr=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''33'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) +
		 N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''33'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with DEBIT returns            */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsreturneddb=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''37'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''37'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate

/* Load the statmement file with minus adjustments    */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointssubtracted=
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''DE'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''DE'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate


/* Load the statmement file with Point Expirations   */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsexpired =
		(select sum(points) from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''XP'') 
		where exists(select * from ' + QuoteName(@DBName) + N'.dbo.history where tipnumber= ' + QuoteName(@DBName) + 
		N'.dbo.Quarterly_Statement_File.tipnumber and histdate>=@startdate and histdate<=@enddate and trancode=''XP'') '
Exec sp_executesql @SQLUpdate, N'@StartDate DateTime, @EndDate DateTime', @StartDate= @StartDate, @EndDate=@EndDate



/* Load the statmement file with total point decreases */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsdecreased=pointsredeemed + 
pointsreturnedcr + pointsreturneddb + pointssubtracted +  pointsexpired'
Exec sp_executesql @SQLUpdate

/* Load the statmement file with the Beginning balance for the Month */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsbegin=
		(select ' + Quotename(@MonthBucket) + N'from ' + QuoteName(@DBName) + 
		N'.dbo.Beginning_Balance_Table where tipnumber= ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber)
		where exists(select * from ' + QuoteName(@DBName) + 
		N'.dbo.Beginning_Balance_Table where tipnumber=' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File.tipnumber)'
exec sp_executesql @SQLUpdate

/* Load the statmement file with ending points */
set @SQLUpdate=N'update ' + QuoteName(@DBName) + N'.dbo.Quarterly_Statement_File set pointsend=pointsbegin + 
	pointsincreased - pointsdecreased '
exec sp_executesql @SQLUpdate

--remove the customers that get e-statements
--
--Logic replaced 10/26/2010 PHB
-- Pull estatement registrations from the FI web db, not from rn1backups.
create table #etips (tipnumber varchar(15) primary key)

insert into #etips
select tipnumber
from rn1.redwood.dbo.[1security]
where emailstatement = 'Y'

delete qsf
from dbo.quarterly_statement_file qsf join #etips tmp
    on qsf.tipnumber = tmp.tipnumber

GO


