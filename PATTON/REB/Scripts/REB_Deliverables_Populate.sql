--- This needs to be run on PATTON 
--- This needs to be run on PATTON 
use deliverables

declare @emailAddress VARCHAR(1024) = 'jsirvydas@rewardnow.com' -- contact person at FI
declare @tipfirst varchar(3) = 'reb'		-- Tip First of FI
declare @contactId INT = 0
declare @delivType VARCHAR(255) = '\\ftp\users\rebreba\from_rn'	-- put FTP path if available
declare @processor INT = 13					-- put sid_processor_id if known, 13 if unknown
declare @copyFrom varchar(3) = '232'		-- choose tipfirst to copy from

select @contactId = sid_contact_id FROM Deliverables.dbo.contact where dim_contact_email = @emailAddress
if @contactId = 0 OR @contactId is null
begin
	insert into Deliverables.dbo.contact (dim_contact_email)
	Values (@emailAddress)
	SET @contactId = SCOPE_IDENTITY()
end

declare @fiId INT = 0
select top 1 @fiid = sid_fiinfo_id FROM deliverables.dbo.fiinfo where dim_fiinfo_tip = @tipfirst
IF @fiId = 0 OR @fiId is null
BEGIN
  INSERT INTO Deliverables.dbo.fiinfo (dim_fiinfo_tip, dim_fiinfo_dbname, dim_fiinfo_shrtdbname, dim_fiinfo_netpath, sid_processor_id, sid_contact_id, dim_fiinfo_delivtype)
  SELECT @tipfirst, DBNamePatton, DBNameNEXL, 'c:\reports\bin', @processor, @contactId, @delivType
	  FROM Rewardsnow.dbo.dbprocessinfo
	  where DBNumber = @tipfirst
	  
  set @fiId = SCOPE_IDENTITY()
END

INSERT INTO Deliverables.dbo.liab (sid_fiinfo_id, dim_liab_format, dim_liab_fileformat, dim_liab_upload)
	SELECT @fiId, REPLACE(l1.dim_liab_format, @copyfrom, @tipfirst), l1.dim_liab_fileformat, @delivType
		from Deliverables.dbo.liab l1
		WHERE l1.sid_fiinfo_id = (select sid_fiinfo_id from Deliverables.dbo.fiinfo where dim_fiinfo_tip = @copyFrom)
		AND (SELECT COUNT(*) from Deliverables.dbo.liab 
				WHERE sid_fiinfo_id = @fiId 
					AND dim_liab_format = REPLACE(l1.dim_liab_format, @copyfrom, @tipfirst)) = 0

INSERT INTO Deliverables.dbo.job (sid_fiinfo_id, dim_Job_format, dim_job_fileformat, dim_job_upload)
	SELECT @fiId, REPLACE(l1.dim_job_format, @copyfrom, @tipfirst), l1.dim_job_fileformat, @delivType
		from Deliverables.dbo.job l1
		WHERE l1.sid_fiinfo_id = (select sid_fiinfo_id from Deliverables.dbo.fiinfo where dim_fiinfo_tip = @copyFrom)
		AND (SELECT COUNT(*) from Deliverables.dbo.job 
				WHERE sid_fiinfo_id = @fiId 
					AND dim_job_format = REPLACE(l1.dim_job_format, @copyfrom, @tipfirst)) = 0

