USE [REB]
GO

/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 10/17/2011 16:30:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PointsExpireFrequency]') AND type in (N'U'))
DROP TABLE [dbo].[PointsExpireFrequency]
GO

USE [REB]
GO

/****** Object:  Table [dbo].[PointsExpireFrequency]    Script Date: 10/17/2011 16:30:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PointsExpireFrequency](
	[PointsExpireFrequencyCd] [nvarchar](2) NOT NULL,
	[PointsExpireFrequencyNm] [nvarchar](512) NULL
) ON [PRIMARY]

GO


