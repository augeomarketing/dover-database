USE [REB]
GO

/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 10/17/2011 16:30:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HistoryDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[HistoryDeleted]
GO

USE [REB]
GO

/****** Object:  Table [dbo].[HistoryDeleted]    Script Date: 10/17/2011 16:30:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[HistoryDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctID] [varchar](25) NULL,
	[HistDate] [datetime] NULL,
	[TranCode] [varchar](2) NULL,
	[TranCount] [int] NULL,
	[Points] [decimal](18, 0) NULL,
	[Description] [varchar](50) NULL,
	[SecID] [varchar](50) NULL,
	[Ratio] [float] NULL,
	[Overage] [decimal](5, 0) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


