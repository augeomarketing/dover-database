USE [REB]
GO

/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 10/17/2011 16:20:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AffiliatDeleted]') AND type in (N'U'))
DROP TABLE [dbo].[AffiliatDeleted]
GO

USE [REB]
GO

/****** Object:  Table [dbo].[AffiliatDeleted]    Script Date: 10/17/2011 16:20:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[AffiliatDeleted](
	[TipNumber] [varchar](15) NULL,
	[AcctType] [varchar](20) NULL,
	[DateAdded] [datetime] NULL,
	[SecID] [varchar](10) NULL,
	[AcctID] [varchar](25) NULL,
	[AcctStatus] [varchar](1) NULL,
	[AcctTypeDesc] [varchar](50) NULL,
	[LastName] [varchar](40) NULL,
	[YTDEarned] [real] NULL,
	[CustID] [char](13) NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


