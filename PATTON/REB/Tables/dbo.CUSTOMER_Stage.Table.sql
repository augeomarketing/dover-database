USE [REB]
GO

/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 10/17/2011 16:29:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CUSTOMER_Stage]') AND type in (N'U'))
DROP TABLE [dbo].[CUSTOMER_Stage]
GO

USE [REB]
GO

/****** Object:  Table [dbo].[CUSTOMER_Stage]    Script Date: 10/17/2011 16:29:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CUSTOMER_Stage](
	[TIPNUMBER] [varchar](15) NULL,
	[RunAvailable] [int] NULL,
	[RUNBALANCE] [int] NULL,
	[RunRedeemed] [int] NULL,
	[LastStmtDate] [datetime] NULL,
	[NextStmtDate] [datetime] NULL,
	[STATUS] [char](1) NULL,
	[DATEADDED] [date] NULL,
	[LASTNAME] [varchar](40) NULL,
	[TIPFIRST] [varchar](3) NULL,
	[TIPLAST] [varchar](12) NULL,
	[ACCTNAME1] [varchar](40) NULL,
	[ACCTNAME2] [varchar](40) NULL,
	[ACCTNAME3] [varchar](40) NULL,
	[ACCTNAME4] [varchar](40) NULL,
	[ACCTNAME5] [varchar](40) NULL,
	[ACCTNAME6] [varchar](40) NULL,
	[ADDRESS1] [varchar](40) NULL,
	[ADDRESS2] [varchar](40) NULL,
	[ADDRESS3] [varchar](40) NULL,
	[ADDRESS4] [varchar](40) NULL,
	[City] [varchar](40) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](15) NULL,
	[StatusDescription] [varchar](50) NULL,
	[HOMEPHONE] [varchar](10) NULL,
	[WORKPHONE] [varchar](10) NULL,
	[BusinessFlag] [char](1) NULL,
	[EmployeeFlag] [char](1) NULL,
	[SegmentCode] [char](2) NULL,
	[ComboStmt] [char](1) NULL,
	[RewardsOnline] [char](1) NULL,
	[NOTES] [varchar] (max) NULL,
	[BonusFlag] [char](1) NULL,
	[Misc1] [varchar](20) NULL,
	[Misc2] [varchar](20) NULL,
	[Misc3] [varchar](20) NULL,
	[Misc4] [varchar](20) NULL,
	[Misc5] [varchar](20) NULL,
	[RunBalanceNew] [int] NULL,
	[RunAvaliableNew] [int] NULL, 
	[sid_customerstage_id] INT IDENTITY (1,1) PRIMARY KEY
)
GO

CREATE NONCLUSTERED INDEX IX__CUSTOMERSTAGE__TIPNUMBER
	ON CUSTOMER_STAGE (TIPNUMBER)
GO


CREATE TRIGGER TRIG__CUSTOMERSTAGE__TIPNUMBERUNIQUEIFUSED
	ON CUSTOMER_STAGE
FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @TIPCOUNT TINYINT 
	DECLARE @NEWTIP VARCHAR(15)

	SELECT @NEWTIP = TIPNUMBER FROM INSERTED

	IF @NEWTIP IS NOT NULL
	BEGIN
	
		SET @TIPCOUNT = (SELECT COUNT(*) FROM CUSTOMER_STAGE WHERE TIPNUMBER = @NEWTIP)
		
		IF @TIPCOUNT <> 1
		BEGIN
			RAISERROR ('Cannot insert duplicate tipnumber (%s) INTO Customer_Stage', 16, 0, @NEWTIP)
			ROLLBACK TRANSACTION
		END
	END
END

GO

/*
SELECT * FROM CUSTOMER_Stage

insert into CUSTOMER_Stage (TIPNUMBER, STATUS, DATEADDED)
SELECT '1', 'A', '1/1/2011'

insert into CUSTOMER_Stage (TIPNUMBER, STATUS, DATEADDED)
SELECT '1', 'A', '1/1/2011'


insert into CUSTOMER_Stage (TIPNUMBER, STATUS, DATEADDED)
SELECT NULL, 'A', '1/1/2011'

insert into CUSTOMER_Stage (TIPNUMBER, STATUS, DATEADDED)
SELECT NULL, 'A', '1/1/2011'

*/

