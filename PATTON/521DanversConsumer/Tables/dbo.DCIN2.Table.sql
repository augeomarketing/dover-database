USE [521DanversConsumer]
GO
/****** Object:  Table [dbo].[DCIN2]    Script Date: 09/25/2009 10:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DCIN2](
	[CLIENT] [nvarchar](11) NULL,
	[ACCTNUM] [nvarchar](17) NULL,
	[SSN] [nvarchar](9) NULL,
	[NA1] [nvarchar](26) NULL,
	[NA2] [nvarchar](26) NULL,
	[ADDR1] [nvarchar](26) NULL,
	[ADDR2] [nvarchar](26) NULL,
	[CITYSTATE] [nvarchar](21) NULL,
	[ZIP] [nvarchar](5) NULL,
	[SIGN] [nvarchar](1) NULL,
	[POINTS] [float] NULL,
	[PHONE1] [nvarchar](10) NULL,
	[PHONE2] [nvarchar](10) NULL,
	[TRANCODE] [nvarchar](3) NULL,
	[STATUS] [nvarchar](1) NULL,
	[NEWFLAG] [nvarchar](1) NULL,
	[CODENAME] [nvarchar](8) NULL,
	[BIRTH1] [nvarchar](6) NULL,
	[BIRTH2] [nvarchar](6) NULL,
	[MAINTFLAG] [nvarchar](1) NULL,
	[ENROLLSWTC] [nvarchar](1) NULL,
	[ENROLLPNTS] [float] NULL,
	[ACCT2] [nvarchar](19) NULL,
	[EXPIRE] [nvarchar](8) NULL,
	[ORIGDATE] [nvarchar](8) NULL,
	[LSTREISSDT] [nvarchar](8) NULL,
	[DATELSTUSE] [nvarchar](8) NULL,
	[BANK] [nvarchar](5) NULL,
	[CARDSTATUS] [nvarchar](1) NULL,
	[DDANUM] [nvarchar](11) NULL,
	[SAVNUM] [nvarchar](11) NULL,
	[FILL] [nvarchar](194) NULL
) ON [PRIMARY]
GO
