USE [521DanversConsumer]
GO
/****** Object:  Table [dbo].[DanversRegistered]    Script Date: 09/25/2009 10:31:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DanversRegistered](
	[tipnumber] [char](20) NOT NULL,
	[name1] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
