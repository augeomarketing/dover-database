USE [521DanversConsumer]
GO
/****** Object:  Table [dbo].[CUST521]    Script Date: 09/25/2009 10:31:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUST521](
	[Col001] [char](16) NULL,
	[Col002] [char](9) NULL,
	[Col003] [char](40) NULL,
	[Col004] [char](40) NULL,
	[Col005] [char](160) NULL,
	[Col006] [char](136) NULL,
	[Col007] [char](40) NULL,
	[Col008] [char](15) NULL,
	[Col009] [char](60) NULL,
	[Col010] [char](106) NULL,
	[Col011] [char](60) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
