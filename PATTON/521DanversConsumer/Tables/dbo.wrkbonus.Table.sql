USE [521DanversConsumer]
GO
/****** Object:  Table [dbo].[wrkbonus]    Script Date: 09/25/2009 10:31:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkbonus](
	[tipnumber] [varchar](15) NOT NULL,
	[acctid] [varchar](25) NULL,
	[trancode] [varchar](2) NULL,
	[points] [numeric](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
