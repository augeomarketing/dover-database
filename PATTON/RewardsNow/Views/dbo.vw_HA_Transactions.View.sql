USE [RewardsNow]
GO

/****** Object:  View [dbo].[vw_HA_Transactions]    Script Date: 12/21/2015 12:28:11 ******/
SET ANSI_NULLS ON
GO

IF OBJECT_ID(N'vw_HA_Transactions') IS NOT NULL
	DROP VIEW vw_HA_Transactions
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_HA_Transactions] AS 
SELECT 
	sid_AzigoTransactions_Identity AS ReferenceID,
	dim_AzigoTransactions_TransactionAmount AS TransactionAmount,
	dim_AzigoTransactions_AwardAmount AS AwardAmount,
	0.00 AS AwardPoints
FROM
	dbo.AzigoTransactions

UNION ALL

SELECT
	sid_RNITransaction_ID AS ReferenceID,
	dim_ProsperoTransactions_TransactionAmount AS TransactionAmount,
	dim_ProsperoTransactions_AwardAmount AS AwardAmount,
	dim_ProsperoTransactions_AwardPoints AS AwardPoints
FROM
	dbo.ProsperoTransactions
	
UNION ALL

SELECT
	sid_RNITransaction_ID AS ReferenceID,
	dim_ZaveeTransactions_TransactionAmount AS TransactionAmount,
	dim_ZaveeTransactions_AwardAmount AS AwardAmount,
	dim_ZaveeTransactions_AwardPoints AS AwardPoints
FROM
	dbo.ZaveeTransactions


GO
