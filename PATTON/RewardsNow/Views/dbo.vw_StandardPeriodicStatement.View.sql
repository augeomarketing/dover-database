USE [RewardsNow]
GO

IF OBJECT_ID(N'vw_StandardPeriodicStatement') IS NOT NULL
	DROP VIEW vw_StandardPeriodicStatement
GO

/****** Object:  View [dbo].[vw_StandardPeriodicStatement]    Script Date: 5/26/2015 1:53:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vw_StandardPeriodicStatement]  
AS  
	SELECT 
		DBNumber,
		StartDate,
		EndDate,
		TipNumber,
		ISNULL(Acctname1, '') AS Acctname1,
		ISNULL(Acctname2, '') AS Acctname2,
		ISNULL(Address1, '') AS Address1,
		ISNULL(Address2, '') AS Address2,
		ISNULL([City], '') AS City,
		ISNULL([State], '') AS State,
		ISNULL(Zip, '') AS Zip,
		ISNULL(PointsBegin, 0) AS PointsBegin,
		ISNULL(PointsEnd, 0) AS PointsEnd,
		ISNULL(PointsPurchasedCredit, 0) AS PointsPurchasedCredit,
		ISNULL(PointsPurchasedDebit, 0) AS PointsPurchasedDebit,
		ISNULL(PointsBonus, 0) AS PointsBonus,
		ISNULL(PointsShopping, 0) AS PointsShopping,
		ISNULL(PointsPurchased, 0) AS PointsPurchased,
		ISNULL([PointsAdded], 0) AS PointsAdded,
		ISNULL([TotalPointsAdded], 0) AS TotalPointsAdded,
		ISNULL([PointsReturnedCredit], 0) AS PointsReturnedCredit,
		ISNULL([PointsReturnedDebit], 0) AS PointsReturnedDebit,
		ISNULL([PointsRedeemed], 0) AS PointsRedeemed,
		ISNULL([PointsSubtracted], 0) AS PointsSubtracted,
		ISNULL([TotalPointsSubtracted], 0) AS TotalPointsSubtracted,
		ISNULL([PointsTransferred], 0) AS PointsTransferred,
		ISNULL([PointsExpired1], 0) AS PointsExpired1,
		ISNULL([PointsExpired2], 0) AS PointsExpired2,
		ISNULL([PointsExpired3], 0) AS PointsExpired3,
		ISNULL([PointsExpired4], 0) AS PointsExpired4
	FROM
		dbo.RNIStandardReport
     


GO
