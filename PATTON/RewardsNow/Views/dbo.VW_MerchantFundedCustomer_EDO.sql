USE [RewardsNow]
GO

/****** Object:  View [dbo].[VW_MerchantFundedCustomer_EDO]    Script Date: 08/08/2013 14:57:47 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VW_MerchantFundedCustomer_EDO]'))
DROP VIEW [dbo].[VW_MerchantFundedCustomer_EDO]
GO

USE [RewardsNow]
GO

/****** Object:  View [dbo].[VW_MerchantFundedCustomer_EDO]    Script Date: 08/08/2013 14:57:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE view [dbo].[VW_MerchantFundedCustomer_EDO]
as
SELECT 
--get view of edo customers 
	 recordNewUpdate =
		  CASE mfc.dim_MerchantFundedCustomer_Status
			 WHEN 'C' THEN 'U'
			 ELSE 'A'
		  END,

   'cdadd884-ea49-11e2-8f50-06a7ec768dec' as  financialInstitutionId,
	mfc.dim_MerchantFundedCustomer_TipPrefix as cardProgramId,
	mfcc.dim_MerchantFundedCustomerCard_Tipnumber as Tipnumber,
	mfcc.dim_MerchantFundedCustomerCard_HashedTipnumberCard as cardToken,
	1 as cardStatus,
	mfc.dim_MerchantFundedCustomer_EmailPref  as receiveEmail,
	mfc.dim_MerchantFundedCustomer_EmailPref  as receiveSMS,
	'' as birthDate,
	'' as cellPhone,
	mfc.dim_MerchantFundedCustomer_EmailAddress as emailAddress,
	'' as gender,
	'' as homePhone,
	mfc.dim_MerchantFundedCustomer_ZipCode as postalCode,
	'' as postalCode2,
	'' as postalCode3,
	'' as postalCode4,
	'' as postalCode5,
	'840' as countryCode,
	'' as referralCode
	
	from   dbo.MerchantFundedCustomer  mfc
	join dbo.MerchantFundedCustomerCard  mfcc  on mfc.dim_MerchantFundedCustomer_Tipnumber = mfcc.dim_MerchantFundedCustomerCard_Tipnumber
	 where YEAR(dim_MerchantFundedCustomer_EDateProcessed) = YEAR(getdate())
	 and MONTH(dim_MerchantFundedCustomer_EDateProcessed) = MONTH(getdate())
	 and DAY(dim_MerchantFundedCustomer_EDateProcessed) = DAY(getdate())
  


GO


