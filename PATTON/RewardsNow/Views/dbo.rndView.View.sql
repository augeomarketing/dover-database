USE [RewardsNow]
GO

IF OBJECT_ID(N'rndView') IS NOT NULL
	DROP VIEW rndView
GO

CREATE VIEW rndView
AS
SELECT RAND() rndResult
GO