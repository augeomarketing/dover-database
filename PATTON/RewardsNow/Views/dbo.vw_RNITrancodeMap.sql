USE [Rewardsnow]
GO

/****** Object:  View [dbo].[vw_RNITrancodeMap]    Script Date: 09/27/2011 12:27:52 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_RNITrancodeMap]'))
DROP VIEW [dbo].[vw_RNITrancodeMap]
GO

USE [Rewardsnow]
GO

/****** Object:  View [dbo].[vw_RNITrancodeMap]    Script Date: 09/27/2011 12:27:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_RNITrancodeMap]
AS
select
	mtcg.sid_rnitrancodegroup_id
	, dim_rnitrancodegroup_name
	, dim_rnitrancodegroup_desc
	, sid_trantype_trancode
	, description
	, tt.ratio
from
	Rewardsnow.dbo.mappingtrancodegroup mtcg
	inner join Rewardsnow.dbo.rniTrancodeGroup rtcg
	on mtcg.sid_rnitrancodegroup_id = rtcg.sid_rnitrancodegroup_id
	inner join Rewardsnow.dbo.TranType tt
	on mtcg.sid_trantype_trancode = tt.TranCode

GO

