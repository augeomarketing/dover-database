USE [RewardsNow]
GO

IF OBJECT_ID(N'vw_RNIBonusProgram') IS NOT NULL
	DROP VIEW vw_RNIBonusProgram
GO

CREATE VIEW vw_RNIBonusProgram
AS

SELECT bp.sid_rnibonusprogram_id
	, bp.dim_rnibonusprogram_description
	, bp.dim_rnibonusprogram_effectivedate
	, bp.dim_rnibonusprogram_expirationdate
	, bp.sid_rnibonusprogramtype_code
	, bpt.dim_rnibonusprogramtype_description
	, bp.sid_trantype_tranCode
	, tt.Description
	, bpfi.sid_rnibonusprogramfi_id
	, bpfi.sid_dbprocessinfo_dbnumber
	, bpfi.dim_rnibonusprogramfi_effectivedate
	, bpfi.dim_rnibonusprogramfi_expirationdate
	, bpfi.dim_rnibonusprogramfi_bonuspoints
	, bpfi.dim_rnibonusprogramfi_pointmultiplier
	, bpfi.dim_rnibonusprogramfi_duration
	, bpfi.sid_rnibonusprogramdurationtype_id
	, bpd.dim_rnibonusprogramdurationtype_shortcode
	, bpd.dim_rnibonusprogramdurationtype_description
	, bpfi.dim_rnibonusprogramfi_storedprocedure
	, bpc.sid_rnibonusprogramcriteria_id
	, bpc.dim_rnibonusprogramcriteria_code01
	, bpc.dim_rnibonusprogramcriteria_code02
	, bpc.dim_rnibonusprogramcriteria_code03
	, bpc.dim_rnibonusprogramcriteria_code04
	, bpc.dim_rnibonusprogramcriteria_code05
	, bpc.dim_rnibonusprogramcriteria_code06
	, bpc.dim_rnibonusprogramcriteria_code07
	, bpc.dim_rnibonusprogramcriteria_code09
	, bpc.dim_rnibonusprogramcriteria_code10
	, bpc.dim_rnibonusprogramcriteria_effectivedate
	, bpc.dim_rnibonusprogramcriteria_expirationdate
FROM RNIBonusProgram bp
INNER JOIN RNIBonusProgramFI bpfi
	ON bp.sid_rnibonusprogram_id = bpfi.sid_rnibonusprogram_id
INNER JOIN RNIBonusProgramType bpt
	ON bp.sid_rnibonusprogramtype_code = bpt.sid_rnibonusprogramtype_code
INNER JOIN TranType tt
	ON bp.sid_trantype_tranCode = tt.TranCode
INNER JOIN RNIBonusProgramDurationType bpd
	ON bpfi.sid_rnibonusprogramdurationtype_id = bpd.sid_rnibonusprogramdurationtype_id
LEFT OUTER JOIN RNIBonusProgramCriteria bpc
	ON bpfi.sid_rnibonusprogramfi_id = bpc.sid_rnibonusprogramfi_id

GO




