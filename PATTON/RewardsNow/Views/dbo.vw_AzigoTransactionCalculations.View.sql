USE [RewardsNow]
GO

/****** Object:  View [dbo].[vw_AzigoTransactionCalculations]    Script Date: 01/15/2014 14:41:47 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_AzigoTransactionCalculations]'))
DROP VIEW [dbo].[vw_AzigoTransactionCalculations]
GO

USE [RewardsNow]
GO

/****** Object:  View [dbo].[vw_AzigoTransactionCalculations]    Script Date: 01/15/2014 14:41:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







CREATE  VIEW [dbo].[vw_AzigoTransactionCalculations]
AS

 
SELECT T1.tranid,T1.TipFirst,T1.TipNumber,T1.TranAmt,T1.TranDate,T1.AwardPercentage,T1.AwardAmt
,T1.RNIRebate,T1.TranStatus,T1.points,T1.RNIPercentage,T1.ClientPercentage,

RNICalculatedAmt =
CASE T1.RNIPercentage 
when 0 then T1.RNIRebate
else (T1.RNIRebate + T1.AwardAmt) * T1.RNIPercentage/(T1.RNIPercentage + T1.ClientPercentage)
END,


FICalculatedAmt =
CASE T1.CLIENTPercentage 
when 100 then T1.AwardAmt
else (T1.RNIRebate + T1.AwardAmt) * T1.ClientPercentage/ (T1.RNIPercentage + T1.ClientPercentage)
END

FROM
(SELECT  az.sid_AzigoTransactions_Identity as TranId,dim_AzigoTransactions_FinancialInstituionID AS TipFirst
      ,dim_AzigoTransactions_Tipnumber as TipNumber
      ,dim_AzigoTransactions_TransactionAmount as TranAmt
      ,dim_AzigoTransactions_TransactionDate as TranDate
      ,dim_AzigoTransactions_AwardPercentage as AwardPercentage
      ,dim_AzigoTransactions_AwardAmount as AwardAmt
      ,dim_AzigoTransactions_RNIRebate as RNIRebate
      ,dim_AzigoTransactions_TranStatus as TranStatus
      ,dim_AzigoTransactions_AwardAmount  * 100 as  points  -- .01/point
      ,coalesce(sp.dim_ShoppingFlingPricing_RNI,0) as RNIPercentage
      ,coalesce(sp.dim_ShoppingFlingPricing_Client,0) as ClientPercentage
    
  FROM RewardsNow.dbo.AzigoTransactions az
  left outer join ShoppingFlingPricing sp on az.dim_AzigoTransactions_FinancialInstituionID = sp.dim_ShoppingFlingPricing_TIP
     
) T1








GO


