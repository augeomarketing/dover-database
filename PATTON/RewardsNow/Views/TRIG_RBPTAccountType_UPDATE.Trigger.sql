USE [Rewardsnow]
GO
/****** Object:  Trigger [TRIG_RBPTAccountType_UPDATE]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTAccountType_UPDATE] ON [dbo].[RBPTAccountType] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTAccountType_DateLastModified = getdate()
	FROM dbo.RBPTAccountType c JOIN deleted del
		ON c.sid_RBPTAccountType_id = del.sid_RBPTAccountType_id
 END
GO
