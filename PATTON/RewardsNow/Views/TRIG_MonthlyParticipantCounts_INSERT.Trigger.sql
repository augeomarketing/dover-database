USE [Rewardsnow]
GO
/****** Object:  Trigger [TRIG_MonthlyParticipantCounts_INSERT]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_MonthlyParticipantCounts_INSERT] ON [dbo].[MonthlyParticipantCounts]  
      FOR Insert  
      AS  
 BEGIN  
 
     insert into dbo.MonthlyParticipantCounts_History 
     (TipFirst, DBFormalName, TotCustomers, TotAffiliatCnt, TotCreditCards, TotDebitCards,  
      TotEmailCnt, TotDebitWithDDA, TotDebitWithoutDDA, TotBillable, TotGroupedDebits, Rundate, DateDeleted, UpdateType, MonthEndDate, billedstat, billeddate, DateJoined)
     select TipFirst, DBFormalName, TotCustomers, TotAffiliatCnt, TotCreditCards, TotDebitCards,  
     TotEmailCnt, TotDebitWithDDA, TotDebitWithoutDDA, TotBillable, TotGroupedDebits, Rundate, getdate(), 'Inserted', MonthEndDate, billedstat, billeddate, DateJoined
     from inserted 
 
 END
GO
