USE [RewardsNow]
GO

/****** Object:  View [dbo].[VW_MerchantFundedCustomer]    Script Date: 08/06/2013 10:58:00 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[VW_MerchantFundedCustomer]'))
DROP VIEW [dbo].[VW_MerchantFundedCustomer]
GO

USE [RewardsNow]
GO

/****** Object:  View [dbo].[VW_MerchantFundedCustomer]    Script Date: 08/06/2013 10:58:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE view [dbo].[VW_MerchantFundedCustomer]
as
SELECT  
	 recordNewUpdate =
		  CASE mfc.dim_MerchantFundedCustomer_Status
			 WHEN 'C' THEN 'U'
			 ELSE 'A'
		  END,

   'cdadd884-ea49-11e2-8f50-06a7ec768dec' as  financialInstitutionId,
	mfc.dim_MerchantFundedCustomer_TipPrefix as cardProgramId,
	mfcc.dim_MerchantFundedCustomerCard_Tipnumber as Tipnumber,
	mfcc.dim_MerchantFundedCustomerCard_HashedTipnumberCard as cardToken,
	1 as cardStatus,
	mfc.dim_MerchantFundedCustomer_EmailPref  as receiveEmail,
	mfc.dim_MerchantFundedCustomer_EmailPref  as receiveSMS,
	'' as birthDate,
	'' as cellPhone,
	mfc.dim_MerchantFundedCustomer_EmailAddress as emailAddress,
	'' as gender,
	'' as homePhone,
	mfc.dim_MerchantFundedCustomer_ZipCode as postalCode,
	'' as postalCode2,
	'' as postalCode3,
	'' as postalCode4,
	'' as postalCode5,
	'840' as countryCode,
	'' as referralCode
	
	from   dbo.MerchantFundedCustomer  mfc
	join dbo.MerchantFundedCustomerCard  mfcc  on mfc.dim_MerchantFundedCustomer_Tipnumber = mfcc.dim_MerchantFundedCustomerCard_Tipnumber
	 where YEAR(DIM_MERCHANTFUNDEDCUSTOMER_DATELASTMODIFIED) = YEAR(getdate())
	 and MONTH(DIM_MERCHANTFUNDEDCUSTOMER_DATELASTMODIFIED) = MONTH(getdate())
	 and DAY(DIM_MERCHANTFUNDEDCUSTOMER_DATELASTMODIFIED) = DAY(getdate())
  

GO


