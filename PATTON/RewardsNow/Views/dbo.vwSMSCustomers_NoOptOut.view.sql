USE [RewardsNow]
GO

/****** Object:  View [dbo].[vwSMSCustomers]    Script Date: 05/07/2015 09:56:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwSMSCustomers_NoOptOut]
AS
 select * 
 from 
	(
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.Enterprise.dbo.[1Security] s inner join RN1.Enterprise.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.ELGA.dbo.[1Security] s inner join RN1.ELGA.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.Eagle.dbo.[1Security] s inner join RN1.Eagle.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.Advantage.dbo.[1Security] s inner join RN1.Advantage.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.TexasTrust.dbo.[1Security] s inner join RN1.TexasTrust.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.OECU.dbo.[1Security] s inner join RN1.OECU.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.michigan1st.dbo.[1Security] s inner join RN1.michigan1st.dbo.Customer c on s.TipNumber = c.TipNumber where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
		union
		select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.COOP.dbo.[1Security] s inner join RN1.COOP.dbo.Customer c on s.TipNumber = c.TipNumber  where ISNULL(s.Email,'') <> '' and ISNULL(s.EmailOther, 'Y') = 'Y'
			AND SUBSTRING(s.TipNumber, 1, 3) IN 
				('611','617','631','603','643','644','645','646','647','648','649','653','658','637','650','654','636','661','662')
	) as AllSmsUsers

GO

