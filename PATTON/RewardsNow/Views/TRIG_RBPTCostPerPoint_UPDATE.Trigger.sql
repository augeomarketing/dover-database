USE [Rewardsnow]
GO
/****** Object:  Trigger [TRIG_RBPTCostPerPoint_UPDATE]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTCostPerPoint_UPDATE] ON [dbo].[RBPTCostPerPoint] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTCostPerPoint_DateLastModified = getdate()
	FROM dbo.RBPTCostPerPoint c JOIN deleted del
		ON c.sid_TipFirst = del.sid_TipFirst
			AND c.dim_RBPTCostPerPoint_EffectiveDate = del.dim_RBPTCostPerPoint_EffectiveDate
			AND c.sid_trantype_trancode = del.sid_trantype_trancode
 END
GO
