USE [RewardsNow]
GO

/****** Object:  View [dbo].[vwSMSCustomers]    Script Date: 08/07/2015 13:39:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID(N'vwSMSCustomers') IS NOT NULL
	DROP VIEW vwSMSCustomers
GO

CREATE VIEW [dbo].[vwSMSCustomers]
AS
 select * from 
(select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.Enterprise.dbo.[1Security] s inner join RN1.Enterprise.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.ELGA.dbo.[1Security] s inner join RN1.ELGA.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.Eagle.dbo.[1Security] s inner join RN1.Eagle.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.TexasTrust.dbo.[1Security] s inner join RN1.TexasTrust.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.OECU.dbo.[1Security] s inner join RN1.OECU.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.REBA.dbo.[1Security] s inner join RN1.REBA.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.TestClass990.dbo.[1Security] s inner join RN1.TestClass990.dbo.Customer c on s.TipNumber = c.TipNumber where s.TipNumber like '9809%'
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.Advantage.dbo.[1Security] s inner join RN1.Advantage.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.MSUFCU.dbo.[1Security] s inner join RN1.MSUFCU.dbo.Customer c on s.TipNumber = c.TipNumber where SUBSTRING(s.TipNumber, 1, 3) IN ('257','266')
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.michigan1st.dbo.[1Security] s inner join RN1.michigan1st.dbo.Customer c on s.TipNumber = c.TipNumber
union
select s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email from RN1.COOP.dbo.[1Security] s inner join RN1.COOP.dbo.Customer c on s.TipNumber = c.TipNumber where SUBSTRING(s.TipNumber, 1, 3) IN 
('603','611','615','617','631','633','636','637','638','641','643','644','645','646','647','648','649','650','653','654','658',
'659','660','661','662')) a
left outer join (select dim_RNICustomer_RNIId, min(dim_RNICustomer_CustomerCode) as CustomerCode, max(dim_RNICustomer_LastModified) as LastModified from RNICustomer where dim_RNICustomer_TipPrefix IN 
('241','248','270','274','276','603','611','615','617','631','633','636','637','638','641','643','644','645','646','647','648','649','650','651','653','654','658',
'659','660','661','662') group by dim_RNICustomer_RNIId) c on a.TipNumber = c.dim_RNICustomer_RNIId




GO
