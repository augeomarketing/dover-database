USE [RewardsNow]
GO

IF OBJECT_ID(N'vw_PromotionalPointLedgerCurrentBalance') IS NOT NULL
	DROP VIEW vw_PromotionalPointLedgerCurrentBalance
GO

CREATE VIEW vw_PromotionalPointLedgerCurrentBalance
AS
	SELECT ppl.sid_promotionalpointledger_id, ppl.sid_dbprocessinfo_dbnumber, ppl.dim_promotionalpointledger_awarddate, ppl.dim_promotionalpointledger_expirationdate, ppl.dim_promotionalpointledger_maxamount, bals.balance + ISNULL(portal.pa_points, 0) as currentbalance
	FROM PromotionalPointLedger ppl
	INNER JOIN
	(
		SELECT sid_promotionalpointledger_id, SUM(dim_promotionalpointledgerdetail_points) balance FROM PromotionalPointLedgerDetail
		GROUP BY sid_promotionalpointledger_id
	) bals
	ON ppl.sid_promotionalpointledger_id = bals.sid_promotionalpointledger_id
	LEFT OUTER JOIN
	(
		SELECT ppl.sid_promotionalpointledger_id, SUM(pa.points * pa.Ratio) * -1 pa_points 
		FROM RN1.OnlineHistoryWork.dbo.portal_adjustments pa
		INNER JOIN RewardsNow.dbo.PromotionalPointTrancodeXref ppx
			ON pa.trancode = ppx.sid_trantype_trancode_out
				AND ppx.sid_promotionalpointledger_id = 0
				AND pa.CopyFlag IS NULL
		INNER JOIN RewardsNow.dbo.PromotionalPointLedger ppl
			ON pa.Histdate between ppl.dim_promotionalpointledger_awarddate and ppl.dim_promotionalpointledger_expirationdate
				AND pa.TipFirst = ppl.sid_dbprocessinfo_dbnumber
		GROUP BY ppl.sid_promotionalpointledger_id
	) portal
	ON ppl.sid_promotionalpointledger_id = portal.sid_promotionalpointledger_id

GO

