USE [Rewardsnow]
GO
/****** Object:  Trigger [tru_Cycle]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tru_Cycle]
   ON  [dbo].[Cycle]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update c
		set LastModified = getdate()
	from inserted ins join dbo.cycle c
		on ins.Cycle_Id = c.Cycle_Id

END
GO
