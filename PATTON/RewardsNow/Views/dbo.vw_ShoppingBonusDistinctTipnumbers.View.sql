USE [RewardsNow]
GO

/****** Object:  View [dbo].[vw_ShoppingBonusDistinctTipnumbers]    Script Date: 10/17/2013 16:28:00 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_ShoppingBonusDistinctTipnumbers]'))
DROP VIEW [dbo].[vw_ShoppingBonusDistinctTipnumbers]
GO

/****** Object:  View [dbo].[vw_ShoppingBonusDistinctTipnumbers]    Script Date: 10/17/2013 16:28:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_ShoppingBonusDistinctTipnumbers]

AS 

SELECT DISTINCT dim_AzigoTransactions_Tipnumber AS Tipnumber
FROM dbo.AzigoTransactions
--WHERE dim_AzigoTransactions_EmailSent = '1/1/1900'
UNION 
SELECT DISTINCT dim_ZaveeTransactions_MemberID AS Tipnumber
FROM dbo.ZaveeTransactions
--WHERE dim_ZaveeTransactions_EmailSent = '1/1/1900'
UNION
SELECT DISTINCT dim_ProsperoTransactions_TipNumber AS Tipnumber
FROM dbo.ProsperoTransactions

GO
