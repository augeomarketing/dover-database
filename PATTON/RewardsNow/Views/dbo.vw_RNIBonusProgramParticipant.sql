USE [RewardsNow]
GO

IF OBJECT_ID(N'vw_RNIBonusProgramParticipant') IS NOT NULL
	DROP VIEW vw_RNIBonusProgramParticipant
GO


CREATE VIEW vw_RNIBonusProgramParticipant
AS
SELECT bp.sid_rnibonusprogram_id
	, bp.dim_rnibonusprogram_description
	, bp.dim_rnibonusprogram_effectivedate
	, bp.dim_rnibonusprogram_expirationdate
	, bp.sid_rnibonusprogramtype_code
	, bpt.dim_rnibonusprogramtype_description
	, bp.sid_trantype_tranCode
	, tt.Description
	, bpfi.sid_rnibonusprogramfi_id
	, bpfi.sid_dbprocessinfo_dbnumber
	, bpfi.dim_rnibonusprogramfi_effectivedate
	, bpfi.dim_rnibonusprogramfi_expirationdate
	, bpfi.dim_rnibonusprogramfi_bonuspoints
	, bpfi.dim_rnibonusprogramfi_pointmultiplier
	, bpfi.dim_rnibonusprogramfi_duration
	, bpfi.sid_rnibonusprogramdurationtype_id
	, bpd.dim_rnibonusprogramdurationtype_shortcode
	, bpd.dim_rnibonusprogramdurationtype_description
	, bpfi.dim_rnibonusprogramfi_storedprocedure
FROM RNIBonusProgram bp
INNER JOIN RNIBonusProgramFI bpfi
	ON bp.sid_rnibonusprogram_id = bpfi.sid_rnibonusprogram_id
INNER JOIN RNIBonusProgramType bpt
	ON bp.sid_rnibonusprogramtype_code = bpt.sid_rnibonusprogramtype_code
INNER JOIN TranType tt
	ON bp.sid_trantype_tranCode = tt.TranCode
INNER JOIN RNIBonusProgramDurationType bpd
	ON bpfi.sid_rnibonusprogramdurationtype_id = bpd.sid_rnibonusprogramdurationtype_id
INNER JOIN RNIBonusProgramParticipant bpp
	ON bpfi.sid_rnibonusprogramfi_id = bpp.sid_rnibonusprogramfi_id
	
GO