USE [RewardsNow]
GO

IF OBJECT_ID(N'vw_processingjob') IS NOT NULL
	DROP VIEW vw_processingjob
GO

CREATE VIEW dbo.vw_processingjob
AS

	select top 100 PERCENT
		pj.sid_processingjob_id
		, pj.dim_processingjob_created
		, pj.sid_dbprocessinfo_dbnumber
		, pjs.dim_processingjobstatus_description
		, ps.dim_processingstep_description
		, dbpi.ClientName 
		, srd.dim_stmtreportdefinition_desc
		, pj.dim_processingjob_stepparameterstartdate
		, pj.dim_processingjob_stepparameterenddate
		, pj.dim_processingjob_jobstartdate
		, pj.dim_processingjob_jobcompletiondate
		, pj.dim_processingjob_outputpathoverride
		, srd.dim_stmtreportdefinition_outputpath
		, pj.sid_processingstep_id
		, pj.sid_processingjobstatus_id
		, pj.dim_processingjob_lastmodified
		, pj.sid_stmtreportdefinition_id
	from processingjob pj
	inner join dbprocessinfo dbpi
		ON pj.sid_dbprocessinfo_dbnumber = dbpi.DBNumber
	inner join processingstep ps
		ON pj.sid_processingstep_id = ps.sid_processingstep_id
	inner join processingjobstatus pjs
		ON pj.sid_processingjobstatus_id = pjs.sid_processingjobstatus_id	
	inner join stmtReportDefinition srd
		ON pj.sid_stmtreportdefinition_id = srd.sid_stmtreportdefinition_id
	order by sid_processingjob_id desc


/*

	SELECT * FROM vw_processingjob
	
*/
