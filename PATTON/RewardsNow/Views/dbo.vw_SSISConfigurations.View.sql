USE [RewardsNow]
GO

/****** Object:  View [dbo].[vw_SSISConfigurations]    Script Date: 11/02/2010 15:46:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_SSISConfigurations]'))
DROP VIEW [dbo].[vw_SSISConfigurations]
GO

USE [RewardsNow]
GO

/****** Object:  View [dbo].[vw_SSISConfigurations]    Script Date: 11/02/2010 15:46:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_SSISConfigurations]  
AS  
 SELECT cfg.ConfigurationFilter, nam.KeyName, cfg.PackagePath, cfg.ConfiguredValue, cfg.ConfiguredValueType  
 FROM [SSIS Configurations] cfg  
 LEFT OUTER JOIN SSISConfigurationKeyName nam  
 ON cfg.ConfigurationFilter = nam.ConfigurationFilter  
 AND cfg.PackagePath = nam.PackagePath  
GO


