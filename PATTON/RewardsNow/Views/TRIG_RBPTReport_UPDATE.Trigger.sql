USE [Rewardsnow]
GO
/****** Object:  Trigger [TRIG_RBPTReport_UPDATE]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTReport_UPDATE] ON [dbo].[RBPTReport] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTReport_DateLastModified = getdate()
	FROM dbo.RBPTReport c JOIN deleted del
		ON c.sid_RBPTReport_id = del.sid_RBPTReport_id
 END
GO
