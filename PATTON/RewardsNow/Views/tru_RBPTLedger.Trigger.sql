USE [Rewardsnow]
GO
/****** Object:  Trigger [tru_RBPTLedger]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tru_RBPTLedger]
   ON  [dbo].[RBPTLedger]
   AFTER update
AS 
BEGIN
	SET NOCOUNT ON;

	update l
		set dim_RBPTLedger_DateLastModified  = getdate()
	from inserted ins join dbo.RBPTLedger l
		on ins.sid_TipNumber = l.sid_TipNumber
		and ins.sid_AccountType_Id = l.sid_AccountType_Id
		and ins.dim_RBPTLedger_HistDate = l.dim_RBPTLedger_HistDate
END
GO
