USE [Rewardsnow]
GO
/****** Object:  Trigger [TRIG_FiProdStatus_UPDATE]    Script Date: 09/30/2009 14:10:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create TRIGGER [dbo].[TRIG_FiProdStatus_UPDATE] ON [dbo].[FiProdStatus] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
 
	UPDATE fps
		SET dim_FiProdStatus_lastmodified = getdate()
	from inserted ins join dbo.FiProdStatus fps
		on ins.sid_FiProdStatus_statuscode = fps.sid_FiProdStatus_statuscode
       
 END
GO
