USE [RewardsNow]
GO

/****** Object:  View [dbo].[vwCLOTransactions]    Script Date: 02/23/2016 12:32:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vwCLOTransactions]
AS
 SELECT
			dim_ZaveeTransactions_FinancialInstituionID AS DBNumber,
		    dbpi.ClientName,
		    dbo.ufn_GetCurrentTip(dim_ZaveeTransactions_MemberID) AS TipNumber, 
			dim_ZaveeTransactions_TransactionDate AS TransactionDate,
			((CASE 
			   WHEN dim_ZaveeTransactions_CancelledDate IS NOT NULL AND dim_ZaveeTransactions_CancelledDate <> '1900-01-01' THEN 'Cancelled' ELSE
			   (CASE WHEN dim_ZaveeTransactions_PaidDate IS NOT NULL AND dim_ZaveeTransactions_PaidDate <> '1900-01-01' THEN 'Paid' ELSE 'Pending' END) END)
			   ) AS TransactionStatus, 
			dim_ZaveeTransactions_TransactionId AS TranID,
			(CASE WHEN dim_ZaveeTransactions_AgentName IN ('NIMM5', 'IMM6', 'IMM5', 'XIMM6', 'XIMM5') THEN 'ShopMainStreet-IMM' ELSE 'ShopMainStreet-Zavee' END) AS ChannelType, 
			dim_ZaveeTransactions_TransactionAmount AS TranAmt, 
			dim_ZaveeTransactions_AwardPoints AS Points, 
			dim_ZaveeTransactions_AwardAmount AS AwardAmount,
			dim_ZaveeTransactions_MerchantId AS MerchantID, 
			dim_ZaveeTransactions_MerchantName AS MerchantName,
			(CASE WHEN dim_ZaveeTransactions_PaidDate = '1900-01-01' THEN NULL ELSE dim_ZaveeTransactions_PaidDate END) AS PaidDate,
			(CASE WHEN dim_ZaveeTransactions_CancelledDate = '1900-01-01' THEN NULL ELSE dim_ZaveeTransactions_CancelledDate END) AS CancelledDate
		FROM
			ZaveeTransactions zt
			join dbprocessinfo dbpi on zt.dim_ZaveeTransactions_FinancialInstituionID = dbpi.DBNumber

UNION ALL

SELECT
			LEFT([dim_affinitymtf_householdid], 3) AS DBNumber,
			dbpi.ClientName,
			[dim_affinitymtf_householdid] AS TipNumber,
			[dim_affinitymtf_trandatetime] AS TransactionDate,
			((CASE WHEN ISNULL([dim_affinitymtf_cancelleddate], '1900-01-01') <> '1900-01-01' THEN 'Cancelled' ELSE 
			(CASE WHEN ISNULL([dim_affinitymtf_paiddate], '1900-01-01') <> '1900-01-01' THEN 'Paid' ELSE 
			( CASE WHEN [dim_affinitymtf_isProcessed] = 1 THEN 'Processed' ELSE 'Pending' END) END) END)
			) AS TransactionStatus,
			[dim_affinitymtf_afsredemptionid] AS TranID,
			'ShopMainStreet-Affinity' AS ChannelType,
			[dim_affinitymtf_tranamount] AS TranAmt,
			FLOOR([dim_affinitymtf_amount] * 100.001) AS Points,
			[dim_affinitymtf_amount] AS AwardAmount,
			[dim_affinitymtf_midcaid] AS MerchantID,
			[dim_affinitymtf_merchdesc] AS MerchantName,
			(CASE WHEN [dim_affinitymtf_paiddate] = '1900-01-01' THEN NULL ELSE [dim_affinitymtf_paiddate] END) AS PaidDate,
			(CASE WHEN [dim_affinitymtf_cancelleddate] = '1900-01-01' THEN NULL ELSE [dim_affinitymtf_cancelleddate] END) AS CancelledDate
		FROM
			[dbo].[AffinityMatchedTransactionFeed] AS at
		INNER JOIN dbprocessinfo dbpi on LEFT([dim_affinitymtf_householdid], 3) = dbpi.DBNumber

UNION ALL

SELECT 			CASE 
					WHEN CHARINDEX('.', dim_ProsperoTransactions_TipNumber) = 4 OR CHARINDEX('.', dim_ProsperoTransactions_TipNumber) = 0
					THEN SUBSTRING(dim_ProsperoTransactions_TipNumber, 1, 3) 
					ELSE SUBSTRING(dim_ProsperoTransactions_TipNumber, 1, 4) 
				END as DBNumber,
				dbpi.ClientName, [dim_ProsperoTransactions_TipNumber] AS TipNumber, 
                      [dim_ProsperoTransactions_TransactionDate] AS TransactionDate, ((CASE WHEN ISNULL([dim_ProsperoTransactionsWork_CancelledDate], 
                      '1900-01-01') <> '1900-01-01' THEN 'Cancelled' ELSE (CASE WHEN ISNULL([dim_ProsperoTransactionsWork_PaidDate], '1900-01-01') 
                      <> '1900-01-01' THEN 'Paid' ELSE 'Pending' END) END)) AS TransactionStatus, CONVERT(VARCHAR(50), [sid_RNITransaction_ID]) AS TranID, 
                      'ShopMainStreet-Prospero' AS ChannelType, 
                      CASE WHEN dim_ProsperoTransactions_TransactionCode = 'D' THEN [dim_ProsperoTransactions_TransactionAmount] ELSE - [dim_ProsperoTransactions_TransactionAmount]
                       END AS TranAmt, 
                      CAST(CASE WHEN dim_ProsperoTransactions_TipFirst = 'C04' THEN CASE WHEN dim_ProsperoTransactions_TransactionCode = 'D' THEN (dim_ProsperoTransactions_AwardAmount)
                       ELSE - dim_ProsperoTransactions_AwardAmount END ELSE CASE WHEN dim_ProsperoTransactions_TransactionCode = 'D' THEN FLOOR([dim_ProsperoTransactions_AwardAmount]
                       * 100.001) ELSE (- dim_ProsperoTransactions_AwardAmount * 100.001) END END AS INT) AS Points, 
                      CASE WHEN dim_ProsperoTransactions_TipFirst = 'C04' THEN CASE WHEN dim_ProsperoTransactions_TransactionCode = 'D' THEN (dim_ProsperoTransactions_AwardAmount
                       / 100.0) ELSE - (dim_ProsperoTransactions_AwardAmount / 100.0) 
                      END ELSE CASE WHEN dim_ProsperoTransactions_TransactionCode = 'D' THEN dim_ProsperoTransactions_AwardAmount ELSE - dim_ProsperoTransactions_AwardAmount
                       END END AS AwardAmount, '' AS MerchantID, [dim_ProsperoTransactions_MerchantName] AS MerchantName, 
                      (CASE WHEN [dim_ProsperoTransactionsWork_PaidDate] = '1900-01-01' THEN NULL ELSE [dim_ProsperoTransactionsWork_PaidDate] END) 
                      AS PaidDate, (CASE WHEN [dim_ProsperoTransactionsWork_CancelledDate] = '1900-01-01' THEN NULL 
                      ELSE [dim_ProsperoTransactionsWork_CancelledDate] END) AS CancelledDate,0 AS RNIRebate
FROM         RewardsNow.dbo.[ProsperoTransactions] LEFT OUTER JOIN
                      RewardsNow.dbo.dbprocessinfo dbpi ON [dim_ProsperoTransactions_TipFirst] = dbpi.DBNumber

UNION ALL

SELECT        at.dim_AzigoTransactions_FinancialInstituionID AS DBNumber,
					 dbpi.ClientName,
					 dbo.ufn_GetCurrentTip(at.dim_AzigoTransactions_Tipnumber) AS TipNumber, 
							 at.dim_AzigoTransactions_TransactionDate AS TransactionDate, 
							 ((CASE WHEN at.dim_AzigoTransactions_CancelledDate IS NOT NULL AND at.dim_AzigoTransactions_CancelledDate <> '1900-01-01' THEN 'Cancelled ' ELSE
							 (CASE WHEN ap.dim_AzigoTransactionsProcessing_ProcessedDate IS NOT NULL AND ap.dim_AzigoTransactionsProcessing_ProcessedDate <> '1900-01-01' THEN 'Paid' ELSE 'Pending' END) 
							 END )) AS TransactionStatus,
							 at.dim_AzigoTransactions_TranID AS TranID, 'ShoppingFLING' AS ChannelType, 
							 at.dim_AzigoTransactions_TransactionAmount AS TranAmt,
							 at.dim_AzigoTransactions_Points AS Points,
							  at.dim_AzigoTransactions_AwardAmount AS AwardAmount, 
							  '' AS MerchantID,
							   at.dim_AzigoTransactions_MerchantName AS MerchantName, 
							  (CASE WHEN ap.dim_AzigoTransactionsProcessing_ProcessedDate = '1900-01-01' THEN NULL ELSE ap.dim_AzigoTransactionsProcessing_ProcessedDate END) AS PaidDate,
							  (CASE WHEN at.dim_AzigoTransactions_CancelledDate = '1900-01-01' THEN NULL ELSE at.dim_AzigoTransactions_CancelledDate END) AS CancelledDate
		FROM         AzigoTransactions AS at 
		INNER JOIN   AzigoTransactionsProcessing AS ap ON at.sid_AzigoTransactions_Identity = ap.sid_AzigoTransactions_Identity
		JOIN dbprocessinfo dbpi on at.dim_AzigoTransactions_FinancialInstituionID = dbpi.DBNumber







GO
