USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter like 'ad_redeem%'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-AD_Redeem-{23E69301-CCFE-4BA7-B8DF-5A53E3D268DC}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Redeem', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Connections[RedeemEdited.csv].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'"', N'\Package.Connections[RedeemEdited.csv].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Connections[RedeemEdited.csv].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package.Connections[RedeemEdited.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'RedeemEdited.csv', N'\Package.Connections[RedeemEdited.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'1033', N'\Package.Connections[RedeemEdited.csv].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[RedeemEdited.csv].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_Redeem', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_Redeem', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'OpenPGP Task  - Decrypt Redeem File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[ForcedExecutionValue]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemHistory', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-RedeemHistory-{EDE1D6AB-BEC5-4840-A793-9195D4C01EFB}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'
', N'\Package.Connections[RedeemEdited.csv].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Redeem', N'Delimited', N'\Package.Connections[RedeemEdited.csv].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[RedeemEdited.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Connections[RedeemEdited.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[RedeemEdited.csv].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\RedeemEdited.csv', N'\Package.Connections[RedeemEdited.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package.Connections[RedeemEdited.csv].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'1252', N'\Package.Connections[RedeemEdited.csv].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Connections[RedeemCSV].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'"', N'\Package.Connections[RedeemCSV].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Connections[RedeemCSV].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package.Connections[RedeemCSV].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Redeem', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Redeem', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'RedeemCSV', N'\Package.Connections[RedeemCSV].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'1033', N'\Package.Connections[RedeemCSV].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[RedeemCSV].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'
', N'\Package.Connections[RedeemCSV].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Redeem', N'Delimited', N'\Package.Connections[RedeemCSV].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[RedeemCSV].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Connections[RedeemCSV].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'0', N'\Package.Connections[RedeemCSV].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Connections[RedeemCSV].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Redeem', N'True', N'\Package.Connections[RedeemCSV].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_Redeem', N'1252', N'\Package.Connections[RedeemCSV].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package.Connections[Redeem.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'Redeem.csv', N'\Package.Connections[Redeem.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Redeem', N'1', N'\Package.Connections[Redeem.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Redeem', N'', N'\Package.Connections[Redeem.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Redeem', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Connections[Redeem.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'OpenPGP Task  - Decrypt Redeem File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 8.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 9.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Source]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 10.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'3', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'File System Task - Rename for connection mgr', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User::CSVFile', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 11.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 12.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 13.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 14.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-AD_Redeem-{23E69301-CCFE-4BA7-B8DF-5A53E3D268DC}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Connections[RedeemEdited.csv].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'"', N'\Package.Connections[RedeemEdited.csv].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Connections[RedeemEdited.csv].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package.Connections[RedeemEdited.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'RedeemEdited.csv', N'\Package.Connections[RedeemEdited.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1033', N'\Package.Connections[RedeemEdited.csv].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[RedeemEdited.csv].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'
', N'\Package.Connections[RedeemEdited.csv].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'Delimited', N'\Package.Connections[RedeemEdited.csv].Properties[Format]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[RedeemEdited.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Connections[RedeemEdited.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[RedeemEdited.csv].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\RedeemEdited.csv', N'\Package.Connections[RedeemEdited.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Connections[RedeemEdited.csv].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1252', N'\Package.Connections[RedeemEdited.csv].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Connections[RedeemCSV].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'"', N'\Package.Connections[RedeemCSV].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Connections[RedeemCSV].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package.Connections[RedeemCSV].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'RedeemCSV', N'\Package.Connections[RedeemCSV].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1033', N'\Package.Connections[RedeemCSV].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[RedeemCSV].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'
', N'\Package.Connections[RedeemCSV].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'Delimited', N'\Package.Connections[RedeemCSV].Properties[Format]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[RedeemCSV].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Connections[RedeemCSV].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'0', N'\Package.Connections[RedeemCSV].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Connections[RedeemCSV].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 15.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'True', N'\Package.Connections[RedeemCSV].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'1252', N'\Package.Connections[RedeemCSV].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package.Connections[Redeem.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'Redeem.csv', N'\Package.Connections[Redeem.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'1', N'\Package.Connections[Redeem.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Connections[Redeem.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Connections[Redeem.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'AD-RN-2199*Redeem.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 16.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 17.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\UNZip Task.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'REDEEM-DATA', N'\Package\Foreach Loop Container\UNZip Task.Properties[Target]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\UNZip Task.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\UNZip Task.Properties[Password]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'UNZip Task', N'\Package\Foreach Loop Container\UNZip Task.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'2', N'\Package\Foreach Loop Container\UNZip Task.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'1033', N'\Package\Foreach Loop Container\UNZip Task.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1048576', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[IncludeSubfolders]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'-1', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[EncryptionType]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'Zip Task', N'\Package\Foreach Loop Container\UNZip Task.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[CompressionType]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\UNZip Task.Properties[CompressionLevel]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 18.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 19.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'ST_151e12077aad4923b61bc4b2b6c6a3c8', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ScriptProjectName]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'Microsoft Visual Basic 2008', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ScriptLanguage]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'User::FileSize', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ReadWriteVariables]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ReadOnlyVariables]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'Script Task - Get File Size     If file is empty we will not unzip in next step', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1033', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'1048576', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'-1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'Main', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[EntryPoint]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'Script Task', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 20.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 21.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'OpenPGP Task  - Decrypt Redeem DATA File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsTargetVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 22.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 23.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[Value]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 24.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'Variable', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Redeem DATA File.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 25.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 26.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 27.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'Variable', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 28.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[RewardsNow].Properties[UserName]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'patton\rn', N'\Package.Connections[RewardsNow].Properties[ServerName]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Connections[RewardsNow].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[RewardsNow].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'********', N'\Package.Connections[RewardsNow].Properties[Password]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'RewardsNow', N'\Package.Connections[RewardsNow].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'RewardsNow', N'\Package.Connections[RewardsNow].Properties[InitialCatalog]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[RewardsNow].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'Data Source=patton\rn;User ID=;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-RedeemData-{018E554C-5A8A-4860-B997-7CAED2151E8E}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[REDEEM-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'REDEEM-DATA', N'\Package.Connections[REDEEM-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'2', N'\Package.Connections[REDEEM-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[REDEEM-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\REDEEM-DATA', N'\Package.Connections[REDEEM-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 29.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'AD-RN-2199*REDEEM-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 30.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Redeem.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'@[User::FilePath]  +  "Redeem.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'\\patton\ops\AccessDevelopment\Input\Archive\Redeem-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 31.....Done!', 10, 1) WITH NOWAIT;
GO

