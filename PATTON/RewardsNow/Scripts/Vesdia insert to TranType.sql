INSERT INTO [RewardsNow].[dbo].[TranType]
           ([TranCode],[Description],[IncDec],[CntAmtFxd],[Points],[Ratio],[TypeCode])
     VALUES            ('F0','Vesdia Bonus','I','A',1,1,7)
GO


INSERT INTO [RewardsNow].[dbo].[TranType]
           ([TranCode],[Description],[IncDec],[CntAmtFxd],[Points],[Ratio],[TypeCode])
     VALUES            ('F9','Vesdia Bonus Reversal','D','A',1,-1,7)
GO