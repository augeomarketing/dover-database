USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'ZaveeTransactionFix', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-ZaveeTransactionFix-{7A63EFB8-8B8E-4C70-862B-C4B81B90BD79}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeTransactionFix', N'ExcelFilePath=\\patton\ops\EDO\Output\EDO_Transaction_Fix.xlsx;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[Excel Connection Manager 1].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

