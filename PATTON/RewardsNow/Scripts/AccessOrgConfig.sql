USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'ad_org'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Org', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_Org', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'OpenPGP Task  - Decrypt Organization File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Org', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Org', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Org', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_Org', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_Org', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Org', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_Org', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_Org', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\test.csv.pgp', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Org', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Org', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'REPLACE(,SUBSTRING(@[User::FileNameAndPath],28,50),".pgp","")', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'True', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'AD-RN-2199*Organization.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Org', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Org', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'@[User::FilePath]  +   @[User::FileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\Organization.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Org', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'@[User::FilePath]  +  "Organization.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\Archive\Organization\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Org', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_Org', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Org', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Org', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'c:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Org', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Organization-{72E65C71-4045-47CD-92BF-5AEF6EBD31CD}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Connections[OrganizationCSV].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'"', N'\Package.Connections[OrganizationCSV].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Connections[OrganizationCSV].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_Org', N'1', N'\Package.Connections[OrganizationCSV].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Org', N'OrganizationCSV', N'\Package.Connections[OrganizationCSV].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'1033', N'\Package.Connections[OrganizationCSV].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Org', N'0', N'\Package.Connections[OrganizationCSV].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Org', N'
', N'\Package.Connections[OrganizationCSV].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Org', N'Delimited', N'\Package.Connections[OrganizationCSV].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Org', N'0', N'\Package.Connections[OrganizationCSV].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Connections[OrganizationCSV].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'0', N'\Package.Connections[OrganizationCSV].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\Organization.csv', N'\Package.Connections[OrganizationCSV].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Org', N'True', N'\Package.Connections[OrganizationCSV].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'1252', N'\Package.Connections[OrganizationCSV].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Org', N'1', N'\Package.Connections[Organization.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Org', N'Organization.csv', N'\Package.Connections[Organization.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Org', N'1', N'\Package.Connections[Organization.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Org', N'', N'\Package.Connections[Organization.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\Organization.csv', N'\Package.Connections[Organization.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Org', N'O:\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Org', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Org', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

