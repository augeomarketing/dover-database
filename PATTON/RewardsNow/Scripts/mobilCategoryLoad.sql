USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[MobileCategory] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[MobileCategory]([MobileCategoryIdentity], [CategoryName], [CategoryIdentifier])
SELECT 1, N'Everything', N'50' UNION ALL
SELECT 2, N'Everything', N'45' UNION ALL
SELECT 3, N'Everything', N'1008' UNION ALL
SELECT 4, N'Everything', N'1009' UNION ALL
SELECT 5, N'Everything', N'59' UNION ALL
SELECT 6, N'Everything', N'44' UNION ALL
SELECT 7, N'Everything', N'1010' UNION ALL
SELECT 8, N'Everything', N'1019' UNION ALL
SELECT 9, N'Everything', N'56' UNION ALL
SELECT 10, N'Everything', N'1067' UNION ALL
SELECT 11, N'Everything', N'1077' UNION ALL
SELECT 12, N'Everything', N'1087' UNION ALL
SELECT 13, N'Everything', N'1081' UNION ALL
SELECT 14, N'Everything', N'1016' UNION ALL
SELECT 15, N'Everything', N'1068' UNION ALL
SELECT 16, N'Everything', N'1099' UNION ALL
SELECT 17, N'Everything', N'1084' UNION ALL
SELECT 18, N'Everything', N'1091' UNION ALL
SELECT 19, N'Everything', N'1013' UNION ALL
SELECT 21, N'Everything', N'1017' UNION ALL
SELECT 22, N'Everything', N'1014' UNION ALL
SELECT 23, N'Everything', N'1018' UNION ALL
SELECT 24, N'Everything', N'1102' UNION ALL
SELECT 25, N'Everything', N'1096' UNION ALL
SELECT 26, N'Everything', N'1093' UNION ALL
SELECT 27, N'Everything', N'1063' UNION ALL
SELECT 28, N'Everything', N'1015' UNION ALL
SELECT 29, N'Everything', N'1065' UNION ALL
SELECT 30, N'Everything', N'1095' UNION ALL
SELECT 31, N'Everything', N'1080' UNION ALL
SELECT 32, N'Everything', N'1070' UNION ALL
SELECT 33, N'Everything', N'1092' UNION ALL
SELECT 34, N'Everything', N'1086' UNION ALL
SELECT 35, N'Everything', N'1054' UNION ALL
SELECT 36, N'Everything', N'1052' UNION ALL
SELECT 37, N'Everything', N'1097' UNION ALL
SELECT 38, N'Everything', N'1012' UNION ALL
SELECT 39, N'Everything', N'1011' UNION ALL
SELECT 40, N'Everything', N'1088' UNION ALL
SELECT 41, N'Everything', N'1078' UNION ALL
SELECT 42, N'Everything', N'1085' UNION ALL
SELECT 43, N'Everything', N'1071' UNION ALL
SELECT 44, N'Everything', N'1061' UNION ALL
SELECT 45, N'Everything', N'1073' UNION ALL
SELECT 46, N'Everything', N'1066' UNION ALL
SELECT 47, N'Everything', N'1042' UNION ALL
SELECT 48, N'Everything', N'1094' UNION ALL
SELECT 49, N'Everything', N'1007' UNION ALL
SELECT 50, N'Everything', N'1100' UNION ALL
SELECT 51, N'Everything', N'1090'
COMMIT;
RAISERROR (N'[dbo].[MobileCategory]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[MobileCategory]([MobileCategoryIdentity], [CategoryName], [CategoryIdentifier])
SELECT 52, N'Everything', N'1075' UNION ALL
SELECT 53, N'Everything', N'1053' UNION ALL
SELECT 54, N'Everything', N'39' UNION ALL
SELECT 55, N'Everything', N'1058' UNION ALL
SELECT 56, N'Everything', N'1098' UNION ALL
SELECT 57, N'Everything', N'1051' UNION ALL
SELECT 58, N'Everything', N'1083' UNION ALL
SELECT 59, N'Everything', N'1055' UNION ALL
SELECT 60, N'Everything', N'1045' UNION ALL
SELECT 61, N'Everything', N'1082' UNION ALL
SELECT 62, N'Everything', N'58'
COMMIT;
RAISERROR (N'[dbo].[MobileCategory]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[MobileCategory] OFF;

