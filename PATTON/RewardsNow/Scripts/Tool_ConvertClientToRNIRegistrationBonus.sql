USE Rewardsnow
GO

DECLARE @tipfirst VARCHAR(3) = 'XXX'
DECLARE @oldtrancode VARCHAR(2) = 'BR'
--DECLARE @oldtrancode VARCHAR(2) = 'BS'
DECLARE @dbnamepatton VARCHAR(50)
DECLARE @BonusPoints int =xxxx


SELECT @dbnamepatton = dbnamepatton from dbprocessinfo where DBNumber = @tipfirst


--TURN OFF POST TO WEB TO PREVENT AWARDS DURING THIS PROCESS
UPDATE [SSIS Configurations] 
SET ConfiguredValue = 'STOP' 
WHERE ConfigurationFilter = 'BNW_Post2WebParent' 
	AND PackagePath = '\Package.Variables[User::StopGo].Properties[Value]';

--WAIT 5 MINUTES TO BE SURE PTW STOPS
WAITFOR DELAY '00:05:00'

--DECLARE VARIABLES
DECLARE @bonusprogramid BIGINT = (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram WHERE dim_rnibonusprogram_description = 'Registration Bonus')
DECLARE @bonusprogramfiid BIGINT

--ADD CLIENT TO PROGRAM
INSERT INTO RNIBonusProgramFI (sid_dbprocessinfo_dbnumber, sid_rnibonusprogram_id, dim_rnibonusprogramfi_effectivedate, dim_rnibonusprogramfi_expirationdate, dim_rnibonusprogramfi_bonuspoints, dim_rnibonusprogramfi_pointmultiplier, sid_rnibonusprogramdurationtype_id, dim_rnibonusprogramfi_duration, dim_rnibonusprogramfi_storedprocedure)
VALUES (@tipfirst, @bonusprogramid, '1/1/1900', '12/31/9999', @BonusPoints, 1.0, 3, 0, 'Rewardsnow.dbo.usp_RNIRegistrationBonus');

--GET CLIENT ENTRY ID
SET @bonusprogramfiid = SCOPE_IDENTITY();


DECLARE @sql nvarchar(max)

--INSERT CURRENTLY AWARDED PARTICIPANTS TO PREVENT RE-AWARDING

SET @sql = REPLACE(REPLACE(REPLACE(
'
INSERT INTO RNIBonusProgramParticipant (sid_customer_tipnumber, sid_rnibonusprogramfi_id, dim_rnibonusprogramparticipant_isbonusawarded, dim_rnibonusprogramparticipant_dateawarded, dim_rnibonusprogramparticipant_remainingpointstoearn)
SELECT TIPNUMBER, <bonusprogramfiid>, 1, MIN(HISTDATE), 0
FROM
(
	SELECT TIPNUMBER, HISTDATE
	FROM [<dbnamepatton>].dbo.HISTORY 
	where TRANCODE = ''<oldtrancode>''
	UNION
	SELECT TIPNUMBER, HISTDATE
	FROM [<dbnamepatton>].dbo.HistoryDeleted 
	where TRANCODE = ''<oldtrancode>''
) HST
GROUP BY TIPNUMBER
'
, '<bonusprogramfiid>', @bonusprogramfiid)
, '<dbnamepatton>', @dbnamepatton)
, '<oldtrancode>', @oldtrancode)

exec sp_executesql @sql

--restart ptw
UPDATE [SSIS Configurations] 
SET ConfiguredValue = 'GO' 
WHERE ConfigurationFilter = 'BNW_Post2WebParent' 
	AND PackagePath = '\Package.Variables[User::StopGo].Properties[Value]'

EXEC msdb.dbo.sp_start_job 'Global Post To Web'

PRINT 'BE SURE TO DISABLE ANY MONTHLY PROCESS FOR THIS BONUS'
