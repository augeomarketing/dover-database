USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Status', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_Status', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_Status', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_Status', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'OpenPGP Task  - Decrypt Status  File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Status', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Status', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Status', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_Status', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'@[User::FilePath]  +   @[User::FileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'O:\AccessDevelopment\Input\Status.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'@[User::FilePath]  +  "Status.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'O:\AccessDevelopment\Input\Archive\Status\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Status', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_Status', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Status  File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Connections[Status.csv].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'"', N'\Package.Connections[Status.csv].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Status', NULL, N'\Package.Connections[Status.csv].Properties[RowDelimiter]', N'DBNull' UNION ALL
SELECT N'AD_Status', N'1', N'\Package.Connections[Status.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Status', N'Status.csv', N'\Package.Connections[Status.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'1033', N'\Package.Connections[Status.csv].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Status', N'0', N'\Package.Connections[Status.csv].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Status', N'
', N'\Package.Connections[Status.csv].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Status', N'Delimited', N'\Package.Connections[Status.csv].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Status', N'0', N'\Package.Connections[Status.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Connections[Status.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'0', N'\Package.Connections[Status.csv].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Status', N'O:\AccessDevelopment\Input\Status.csv', N'\Package.Connections[Status.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package.Connections[Status.csv].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'1252', N'\Package.Connections[Status.csv].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Status', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Status', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'c:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Status', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-StatusStage-{6EAA9ACB-648C-45CC-931B-CE01CF9C3264}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Status', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'O:\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'O:\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'REPLACE(,SUBSTRING(@[User::FileNameAndPath],28,50),".pgp","")', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'True', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'AD-RN-2199*Status.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Status', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Status', N'O:\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Status', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_StatusHistory', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-StatusHistory-{9BBF4167-0315-466D-9A33-D17F54F2C333}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

