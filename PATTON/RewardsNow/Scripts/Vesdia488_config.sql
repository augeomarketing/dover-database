USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations]   where ConfigurationFilter in  ( 'Vesdia488WithExtractFile','Vesdia488Stage')
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488WithExtractFile', N'ASB', N'\Package.Variables[User::vDBName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Sep11', N'\Package.Variables[User::v488Date].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Variables[User::NbrRecords].Properties[Value]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Variables[User::CtlVersion].Properties[Value]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Variables[User::CtlRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Variables[User::CtlClientId].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Variables[User::ClientIdName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'\\patton\ops\Vesdia\Output\002_SFACTUALTRANS_20120618.CSV', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'\\patton\ops\Vesdia\Output\002_SF_20120618.CTL', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'@[User::FilePath]  +   @[User::ArchiveCTLFile]', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Properties[VersionMinor]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'1', N'\Package.Properties[VersionMajor]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'', N'\Package.Properties[VersionComments]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'384', N'\Package.Properties[VersionBuild]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'True', N'\Package.Properties[UpdatePackage]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[UpdateObjects]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'1', N'\Package.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[SuppressConfigurationWarnings]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'\', N'\Package.Properties[SQLFolder]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[SaveCheckpoints]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'1', N'\Package.Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'5', N'\Package.Properties[PackageType]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Properties[PackagePriorityClass]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'********', N'\Package.Properties[PackagePassword]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[OfflineMode]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'Vesdia488Stage01', N'\Package.Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'1', N'\Package.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'-1', N'\Package.Properties[MaxConcurrentExecutables]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'1033', N'\Package.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'1048576', N'\Package.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[InteractiveMode]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'-1', N'\Package.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[EncryptCheckpoints]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'\\patton\ops\Vesdia\Output\Archive_ActualExtracts\', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Vesdia488-{7946C33B-D25F-4E04-B777-DDD4C572DD9F}RN1.RewardsNOW;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'Data Source=patton\rn;Initial Catalog=OnlineHistoryWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Vesdia488-{E621B03B-E757-41EF-9AD6-34EA642C7EDD}patton\rn.OnlineHistoryWork;', N'\Package.Connections[OnlineHistoryWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'002_SF_20120618.CTL', N'\Package.Variables[User::ArchiveCTLFile].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'002_SFACTUALTRANS_20120618.CSV', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'True', N'\Package.Properties[EnableDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[DumpOnAnyError]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488Stage', N'', N'\Package.Properties[DumpDescriptor]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'', N'\Package.Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'REWARDSNOW\dirish', N'\Package.Properties[CreatorName]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'RN155', N'\Package.Properties[CreatorComputerName]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'11/9/2010 2:41:32 PM', N'\Package.Properties[CreationDate]', N'DateTime' UNION ALL
SELECT N'Vesdia488Stage', N'True', N'\Package.Properties[CheckSignatureOnLoad]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Properties[CheckpointUsage]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'', N'\Package.Properties[CheckpointFileName]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Properties[CertificateContext]', N'Int64' UNION ALL
SELECT N'Vesdia488Stage', N'False', N'\Package.Connections[Vesdia 488].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'"', N'\Package.Connections[Vesdia 488].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'', N'\Package.Connections[Vesdia 488].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'1', N'\Package.Connections[Vesdia 488].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'Vesdia 488', N'\Package.Connections[Vesdia 488].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'1033', N'\Package.Connections[Vesdia 488].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Connections[Vesdia 488].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'
', N'\Package.Connections[Vesdia 488].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'Delimited', N'\Package.Connections[Vesdia 488].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Connections[Vesdia 488].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488Stage', N'', N'\Package.Connections[Vesdia 488].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'0', N'\Package.Connections[Vesdia 488].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'\\patton\ops\Vesdia\Input\Vesdia488.csv', N'\Package.Connections[Vesdia 488].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'True', N'\Package.Connections[Vesdia 488].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488Stage', N'1252', N'\Package.Connections[Vesdia 488].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488Stage', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Vesdia488-{73BC8B52-1059-4C77-9B11-42D06E3B1498}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'', N'\Package.Variables[User::vNbrErrors].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'PetCo', N'\Package.Variables[User::vMerchantName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'1234', N'\Package.Variables[User::vMerchantID].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'236', N'\Package.Variables[User::vDBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'ASB', N'\Package.Variables[User::vDBName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488Stage', N'Sep11', N'\Package.Variables[User::v488Date].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Data Source=patton\rn;Initial Catalog=OnlineHistoryWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Vesdia488-{E621B03B-E757-41EF-9AD6-34EA642C7EDD}patton\rn.OnlineHistoryWork;', N'\Package.Connections[OnlineHistoryWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'False', N'\Package.Connections[controlFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'"', N'\Package.Connections[controlFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[controlFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1', N'\Package.Connections[controlFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'controlFile', N'\Package.Connections[controlFile].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1033', N'\Package.Connections[controlFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'
', N'\Package.Connections[controlFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Delimited', N'\Package.Connections[controlFile].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Vesdia488-{7946C33B-D25F-4E04-B777-DDD4C572DD9F}RN1.RewardsNOW;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'False', N'\Package.Connections[Vesdia 488].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'"', N'\Package.Connections[Vesdia 488].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[Vesdia 488].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1', N'\Package.Connections[Vesdia 488].Properties[ProtectionLevel]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488WithExtractFile', N'Vesdia 488', N'\Package.Connections[Vesdia 488].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1033', N'\Package.Connections[Vesdia 488].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[Vesdia 488].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'
', N'\Package.Connections[Vesdia 488].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Delimited', N'\Package.Connections[Vesdia 488].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[Vesdia 488].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[Vesdia 488].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[Vesdia 488].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'\\patton\ops\Vesdia\Input\Vesdia488.csv', N'\Package.Connections[Vesdia 488].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Connections[Vesdia 488].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1252', N'\Package.Connections[Vesdia 488].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Vesdia488-{73BC8B52-1059-4C77-9B11-42D06E3B1498}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[controlFile].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\_SF_20130226.ctl', N'\Package.Connections[controlFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Connections[controlFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1252', N'\Package.Connections[controlFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'False', N'\Package.Connections[ClientExportFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'"', N'\Package.Connections[ClientExportFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[ClientExportFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1', N'\Package.Connections[ClientExportFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'ClientExportFile', N'\Package.Connections[ClientExportFile].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1033', N'\Package.Connections[ClientExportFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'
', N'\Package.Connections[ClientExportFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Delimited', N'\Package.Connections[ClientExportFile].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[ClientExportFile].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\_SFACTUALTRANS_20130226.csv', N'\Package.Connections[ClientExportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Connections[ClientExportFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1252', N'\Package.Connections[ClientExportFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'PetCo', N'\Package.Variables[User::vMerchantName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1234', N'\Package.Variables[User::vMerchantID].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'236', N'\Package.Variables[User::vDBNumber].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

