﻿USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIImportFileType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIImportFileType]([sid_rniimportfiletype_id], [dim_rniimportfiletype_name], [dim_rniimportfiletype_desc], [dim_rniimportfiletype_dateadded], [dim_rniimportfiletype_lastmodified], [dim_rniimportfiletype_lastmodifiedby])
SELECT 1, N'ACCT', N'Customer Account Data', '20110726 16:33:08.490', '20110726 16:33:08.493', N'REWARDSNOW\cheit' UNION ALL
SELECT 2, N'TRAN', N'Transaction Data', '20110726 16:33:08.497', '20110726 16:33:08.497', N'REWARDSNOW\cheit' UNION ALL
SELECT 3, N'APRG', N'Customer Purge', '20110726 16:33:08.497', '20110726 16:33:08.497', N'REWARDSNOW\cheit' UNION ALL
SELECT 4, N'TPRG', N'Card Purge', '20110726 16:33:08.497', '20110726 16:33:08.497', N'REWARDSNOW\cheit' UNION ALL
SELECT 5, N'BNS', N'Bonus Transaction', '20110726 16:33:08.497', '20110726 16:33:08.497', N'REWARDSNOW\cheit'
COMMIT;
RAISERROR (N'[dbo].[RNIImportFileType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIImportFileType] OFF;

