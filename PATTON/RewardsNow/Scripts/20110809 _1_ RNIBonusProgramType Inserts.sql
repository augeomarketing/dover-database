USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProgramDurationType] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusProgramDurationType]([sid_rnibonusprogramdurationtype_id], [dim_rnibonusprogramdurationtype_description], [dim_rnibonusprogramdurationtype_shortcode], [dim_rnibonusprogramdurationtype_dateadded], [dim_rnibonusprogramdurationtype_lastmodified], [dim_rnibonusprogramdurationtype_lastupdatedby])
SELECT 1, N'Calendar Days', N'DAY', '20110804 18:12:17.910', '20110804 18:12:17.913', N'REWARDSNOW\pbutler' UNION ALL
SELECT 2, N'Processing Months', N'MO', '20110804 18:12:26.577', '20110804 18:12:26.577', N'REWARDSNOW\pbutler' UNION ALL
SELECT 3, N'Not Applicable', N'N/A', '20110805 15:10:40.643', '20110805 15:10:40.647', N'REWARDSNOW\pbutler'
COMMIT;
RAISERROR (N'[dbo].[RNIBonusProgramDurationType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProgramDurationType] OFF;

