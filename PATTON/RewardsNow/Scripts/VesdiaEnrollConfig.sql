USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter like 'VesdiaEnrollment%'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollment', N'VesdiaEnrollment_child.dtsx', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[IsRemoteVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[DestinationFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::VesdiaDestinationFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::TTLRecordCount].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'TTLRecordCount', N'\Package.Variables[User::TTLRecordCount].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::TTLRecordCount].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::VesdiaDestinationFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[DestinationFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[DestinationFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[DestinationFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'DestinationFile', N'\Package.Connections[DestinationFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\EnrollmentFile.csv', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1252', N'\Package.Connections[EnrollmentFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1048576', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[IsLocalVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileSeqNbr].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileSeqNbr', N'\Package.Variables[User::FileSeqNbr].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSeqNbr].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSeqNbr].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::FileNameToArchive].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileNameToArchive].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileNameToArchive', N'\Package.Variables[User::FileNameToArchive].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileNameToArchive].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileNameToArchive].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Ves_RN_Enroll_', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Variables[User::FileHeader].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileHeader].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileHeader', N'\Package.Variables[User::FileHeader].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileHeader].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileHeader].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_112920121418555.csv', N'\Package.Variables[User::FileFound].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileFound].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileFound', N'\Package.Variables[User::FileFound].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileFound].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileFound].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileExtension].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileExtension', N'\Package.Variables[User::FileExtension].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileExtension].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileExtension].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDetailSeqNbr', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'@[User::FileDetailSeqNbr]', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Variables[User::FileDetail].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDetail].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDetail', N'\Package.Variables[User::FileDetail].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDetail].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDetail].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_10212010.csv', N'\Package.Variables[User::FileDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDestination].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDestination', N'\Package.Variables[User::FileDestination].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDestination].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDestination].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'10312010', N'\Package.Variables[User::FileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDate', N'\Package.Variables[User::FileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FIDatabase].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIDatabase].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DatedFiles', N'\Package.Variables[User::DestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::DestinationPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'DestinationPath', N'\Package.Variables[User::DestinationPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DestinationPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DestinationPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DatabaseForDeletes].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseForDeletes].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225OrlandoFCU', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'DatabaseBeingProcessed', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Archive\Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::ArchiveFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::ArchiveFullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'ArchiveFullPath', N'\Package.Variables[User::ArchiveFullPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'@[User::ArchivePath]   +     @[User::FileNameToArchive]', N'\Package.Variables[User::ArchiveFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::ArchiveFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::ArchiveFullPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[IsFilterVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'ServerHost=172.20.141.175;ServerPort=22;ServerUser=rewards-now;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentDetail].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package.Connections[DestinationFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::TTLRecordCount].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentTrailer].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[IncludeSubfolders]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'SSH Connection Manager 1', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[FtpConnection]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'-1', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Filter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::FullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'InBound', N'\Package.Variables[User::FTPPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FTPPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FTPPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FTPPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FTPPath', N'\Package.Variables[User::FTPPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FTPPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FTPPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FTPPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FTPPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'OrlandoFCU', N'\Package.Variables[User::FIWebName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FIWebName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FIWebName', N'\Package.Variables[User::FIWebName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FIWebName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FIWebName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Variables[User::FileTrailer].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileTrailer].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileTrailer', N'\Package.Variables[User::FileTrailer].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileTrailer].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileTrailer].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileSource].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileSource', N'\Package.Variables[User::FileSource].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSource].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSource].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'00009', N'\Package.Variables[User::FileSeqNbr].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DatedFiles', N'\Package.Variables[User::DestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'VesdiaDestinationFile', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TTLRecordCount].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::SourceFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::SourceFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225OrlandoFCU', N'\Package.Variables[User::PattonNameForDelete].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::PattonNameForDelete].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::PattonNameForDelete].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::PattonNameForDelete].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'PattonNameForDelete', N'\Package.Variables[User::PattonNameForDelete].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::PattonNameForDelete].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::PattonNameForDelete].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::PattonNameForDelete].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::PattonNameForDelete].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::MyCounter].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::MyCounter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::MyCounter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::MyCounter].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'MyCounter', N'\Package.Variables[User::MyCounter].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::MyCounter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::MyCounter].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::MyCounter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::MyCounter].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'AE17A3AC;F8A80FCC', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Key]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=RN1;Initial Catalog=OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{32E3CF38-FFD3-4669-852C-29340423D7F5}RN1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[EnrollmentDetail].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Connections[DestinationFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1252', N'\Package.Connections[DestinationFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::VesdiaDestinationFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TTLRecordCount].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::TTLRecordCount].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225', N'\Package.Variables[User::TipFirstForDelete].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TipFirstForDelete].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'TipFirstForDelete', N'\Package.Variables[User::TipFirstForDelete].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TipFirstForDelete].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TipFirstForDelete].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_012132012144204.csv.pgp', N'\Package.Variables[User::TargetFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TargetFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TargetFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::TargetFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'TargetFile', N'\Package.Variables[User::TargetFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TargetFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'   @[User::FilePath]       + @[User::FileNameToArchive]  + ".pgp"', N'\Package.Variables[User::TargetFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::TargetFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::TargetFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::SourceFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::SourceFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::SourceFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::SourceFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'SourceFile', N'\Package.Variables[User::SourceFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::SourceFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'   @[User::FilePath]       + @[User::FileNameToArchive]', N'\Package.Variables[User::SourceFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::VesdiaDestinationFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentTrailer].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[DestinationFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[DestinationFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[DestinationFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[ExtendedRemoteFileInfo]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'SFTP Task', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Action]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User::TargetFile', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Target]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User::SourceFile', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Source]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentDetail].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[EnrollmentDetail].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'EnrollmentDetail', N'\Package.Connections[EnrollmentDetail].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package.Connections[EnrollmentDetail].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Result]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'PubKeyRing.gpg', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[PublicKeyRing]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Password]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'OpenPGP Task - Encrypt csv', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'PubKeyRing.gpg', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[KeyRing]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentTrailer].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentTrailer].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentTrailer].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentTrailer].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentTrailer].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1048576', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'-1', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{32E3CF38-FFD3-4669-852C-29340423D7F5}RN1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'C:\RewardsNOW SVN Repository\database\SSIS\VesdiaEnrollment\VesdiaEnrollment\VesdiaEnrollment\VesdiaEnrollment _child.dtsx', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{F23654BF-1292-4902-9EEE-09A78754E27C}patton\rn.236;Auto Translate=False;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentTrailer].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentTrailer].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentTrailer].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Connections[EnrollmentTrailer].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentTrailer].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentTrailer].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentHeader].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentHeader].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentHeader].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentHeader].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentHeader', N'\Package.Connections[EnrollmentHeader].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentHeader].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentHeader].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentHeader].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentHeader].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentHeader].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentHeader].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentHeader].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Connections[EnrollmentHeader].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentHeader].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentHeader].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentFile', N'\Package.Connections[EnrollmentFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\EnrollmentFile.csv', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentDetail].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentDetail].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentDetail].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentDetail', N'\Package.Connections[EnrollmentDetail].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentDetail].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentDetail].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentDetail].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentDetail].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentDetail].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Connections[EnrollmentDetail].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentDetail].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentDetail].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_01032013144328.csv', N'\Package.Variables[User::FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Variables[User::FileTrailer].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Variables[User::FileHeader].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Variables[User::FileDetail].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_10212010.csv', N'\Package.Variables[User::FileDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentTrailer', N'\Package.Connections[EnrollmentTrailer].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentDetail].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentDetail].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentDetail].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Connections[EnrollmentDetail].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'OpenPGP Task', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[DelayValidation]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 8.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera\OpenPGP Task - Encrypt csv.Properties[Action]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Connections[SSH Connection Manager 1].Properties[TransferBinary]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'rewards-now', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[DestinationFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::TTLRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TipFirstForDelete].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::TipFirstForDelete].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Archive , pgp and ftp file to Cartera.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Archive , pgp and ftp file to Cartera', N'\Package\Archive , pgp and ftp file to Cartera.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Archive , pgp and ftp file to Cartera.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package\Archive , pgp and ftp file to Cartera.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1048576', N'\Package\Archive , pgp and ftp file to Cartera.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'-1', N'\Package\Archive , pgp and ftp file to Cartera.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Foreach Loop Container', N'\Package\Archive , pgp and ftp file to Cartera.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Result]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'/', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[RemotePathSeparator]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User::FTPPath', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Remote]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[NoRemoteFilesFail]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'SFTP Task - ftp to Cartera (Vesdia)', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User::TargetFile', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[Local]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Archive , pgp and ftp file to Cartera\SFTP Task - ftp to Cartera (Vesdia).Properties[IsRemoteWildcard]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'sgm!9tFGK$$9', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentDetail].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerKeyFile]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'172.20.141.175', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[SSH Connection Manager 1].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyUser]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyType]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'80', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPort]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 9.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPassword]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyHost]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'SSH Connection Manager 1', N'\Package.Connections[SSH Connection Manager 1].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[LogFile]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'2', N'\Package.Connections[SSH Connection Manager 1].Properties[BackendVersion]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[PubKeyRing.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'PubKeyRing.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[PubKeyRing.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[PubKeyRing.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{F23654BF-1292-4902-9EEE-09A78754E27C}patton\rn.236;Auto Translate=False;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[EnrollmentFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'EnrollmentFile', N'\Package.Connections[EnrollmentFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package.Connections[EnrollmentFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[EnrollmentFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1252', N'\Package.Connections[EnrollmentDetail].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::TTLRecordCount].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::TipFirstForDelete].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::TipFirstForDelete].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentTrailer].Properties[HeaderRowsToSkip]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 10.....Done!', 10, 1) WITH NOWAIT;
GO

