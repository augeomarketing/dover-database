/****** add 2 columns to the list table to keep track of when a row was added ******/
 
  
  alter table [RewardsNow].[dbo].[VesdiaEnrollmentList]  
    [dim_VesdiaEnrollmentList_DateAdded] [datetime] NULL,
	[dim_VesdiaEnrollmentList_AddedFileNbr] [varchar]  (5)NULL,
	[dim_VesdiaEnrollmentList_LastModifiedDate] [datetime] NULL,
	[dim_VesdiaEnrollmentList_UpdatedFileNbr] [varchar]  (5)NULL,