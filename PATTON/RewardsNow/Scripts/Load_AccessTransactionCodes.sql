USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[AccessTransactionCodes]([sid_AccessTransactionCodes_Code], [dim_AccessTransactionCodes_Description])
SELECT N'AMT', N'Award reduced to maximum' UNION ALL
SELECT N'DNQ', N'Did not Qualify' UNION ALL
SELECT N'DNR', N'Merchant or member participation date not in range' UNION ALL
SELECT N'DPQ', N'Duplicate prior transaction' UNION ALL
SELECT N'DUP', N'Duplicate transaction in file' UNION ALL
SELECT N'EXC', N'Disqualified due to exclusion date' UNION ALL
SELECT N'LOC', N'MID or location not found' UNION ALL
SELECT N'LOW', N'Award lower than best awards' UNION ALL
SELECT N'MAX', N'Disqualified due to maximum uses' UNION ALL
SELECT N'MCM', N'Member is over maximum lifetime award total' UNION ALL
SELECT N'MDR', N'Reserved' UNION ALL
SELECT N'MDT', N'MID is inactive' UNION ALL
SELECT N'MEM', N'Member is not found' UNION ALL
SELECT N'MID', N'MID is not found' UNION ALL
SELECT N'MIN', N'Miniumum amount not met' UNION ALL
SELECT N'MIS', N'Reserved' UNION ALL
SELECT N'MON', N'Month is excluded' UNION ALL
SELECT N'MTX', N'Valid Transaction but member not found' UNION ALL
SELECT N'OFN', N'No offer available' UNION ALL
SELECT N'OIN', N'Offer inactive' UNION ALL
SELECT N'ORG', N'Organization not found' UNION ALL
SELECT N'PAY', N'Payment card not found' UNION ALL
SELECT N'PRD', N'Program not found' UNION ALL
SELECT N'PRI', N'Program invalid' UNION ALL
SELECT N'REW', N'Not a reward Offer' UNION ALL
SELECT N'REX', N'Return expired' UNION ALL
SELECT N'RNS', N'Return rejected' UNION ALL
SELECT N'SDR ', N'Reserved' UNION ALL
SELECT N'SET', N'Setup invalid' UNION ALL
SELECT N'TIM', N'Invalid time of day' UNION ALL
SELECT N'XDW', N'Excluded day' UNION ALL
SELECT N'XLC', N'Excluded date' UNION ALL
SELECT N'XLD', N'Excluded day of week' UNION ALL
SELECT N'XLM', N'Excluded month' UNION ALL
SELECT N'XLR', N'Excluded date range' UNION ALL
SELECT N'XLS', N'Excluded date'
COMMIT;
RAISERROR (N'[dbo].[AccessTransactionCodes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

