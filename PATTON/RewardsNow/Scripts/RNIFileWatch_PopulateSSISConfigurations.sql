USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1 of 1.....Start!', 10, 1) WITH NOWAIT;
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'RNIFileWatch', N'Data Source=doolittle\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-RNIFileWatch-{C3EC8E6F-BF07-4A13-BEE8-55671472E6B7}doolittle\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'RNIFileWatch', N'C:\PERL\BIN\PERL', N'\Package.Variables[User::PerlPath].Properties[Value]', N'String' UNION ALL
SELECT N'RNIFileWatch', N'C:\SVN\DATABASE\UTIL\RNIFIleHandling_FTP2Processing.pl', N'\Package.Variables[User::FileHandlerPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1 of 1.....Done!', 10, 1) WITH NOWAIT;
GO

