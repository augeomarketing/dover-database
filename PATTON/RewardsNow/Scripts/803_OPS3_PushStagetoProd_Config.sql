USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'803_Ops3_DTX', N'O:\SSIS_Packages\803Montgomery\803Montgomery\bin\803_OPS5_WelcomeKit.dtsx', N'\Package.Connections[803_OPS5_WelcomeKit.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'803_Ops3_DTX', N'O:\SSIS_Packages\803Montgomery\803Montgomery\bin\803_OPS4_Push_ProdToWeb.dtsx', N'\Package.Connections[803_OPS4_Push_ProdToWeb.dtsx].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

