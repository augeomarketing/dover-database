USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\EDO_Transaction_20130709.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\Archive\EDO_Transaction_20130709.csv', N'\Package.Variables[User::ArchiveFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'<none>', N'\Package.Connections[Flat File Connection Manager].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', NULL, N'\Package.Connections[Flat File Connection Manager].Properties[RowDelimiter]', N'DBNull' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\OutputTransactionFile.csv', N'\Package.Connections[Flat File Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\OutputTransactionFile.csv', N'\Package.Variables[User::SourceFile].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-EDO_CreateTransaction-{769DA684-8ECC-48AC-8196-026AA580DC96}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\Archive\', N'\Package.Variables[User::ArchiveFilePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

