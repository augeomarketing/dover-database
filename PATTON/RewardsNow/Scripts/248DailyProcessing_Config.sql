USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'248DailyProcessing', N'\\patton\ops\248\Input\TBSPIHFiles\', N'\Package.Variables[User::TBSPPath].Properties[Value]', N'String' UNION ALL
SELECT N'248DailyProcessing', N'\\patton\ops\248\Input\TBSPIHFiles\TBSPIH.131209_131209', N'\Package.Variables[User::FullFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'248DailyProcessing', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-248DailyProcessing-{AC4DBCFC-BDC6-47CF-A153-6E8E891A3991}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

