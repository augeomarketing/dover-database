USE [RewardsNow]
GO

INSERT INTO dbprocessinfo (DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName, DBLocationPatton, DBLocationNexl, LastTipNumberUsed, PointExpirationYears, PointsExpireFrequencyCd, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, TransferHistToRn1, CalcDailyExpire, hasonlinebooking, AccessDevParticipant, VesdiaParticipant, MinCatalogPointValue, MaxCatalogPointValue, ExpPointsDisplayPeriodOffset, ExpPointsDisplay, sid_editaddress_id, SSOExactMatch, ShoppingFlingExportFile, MDTAutoUpdateLiabilitySent, ParseFirstName, LocalMerchantParticipant)
SELECT 
'7XX', 'NEBAHolding', 'NEBAHolding', DBAvailable, 'NEBA', 'NEBA', 'NEBAHolding', 'NEBAHolding', 'NEBAHolding', REPLACE(LastTipNumberUsed, 'RNI', '7XX'), PointExpirationYears, PointsExpireFrequencyCd, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, TransferHistToRn1, CalcDailyExpire, hasonlinebooking, AccessDevParticipant, VesdiaParticipant, MinCatalogPointValue, MaxCatalogPointValue, ExpPointsDisplayPeriodOffset, ExpPointsDisplay, sid_editaddress_id, SSOExactMatch, ShoppingFlingExportFile, MDTAutoUpdateLiabilitySent, ParseFirstName, LocalMerchantParticipant
FROM dbprocessinfo WHERE DBNumber = 'RNI'

EXEC usp_RNIRawImportDataDefinition_InsertStandardLayouts '7XX', '226', 'FIXED'

EXEC usp_GenerateRawDataViews '7XX'

INSERT INTO RNICustomerLoadSource (sid_dbprocessinfo_dbnumber, dim_rnicustomerloadsource_sourcename, dim_rnicustomerloadsource_fileload)
VALUES ('7XX', 'vw_7XX_ACCT_TARGET_226', 0)

/*
INSERT INTO RNICustomerLoadColumn (sid_rnicustomerloadsource_id, dim_rnicustomerloadcolumn_sourcecolumn, dim_rnicustomerloadcolumn_targetcolumn, dim_rnicustomerloadcolumn_keyflag, dim_rnicustomerloadcolumn_loadtoaffiliat, dim_rnicustomerloadcolumn_affiliataccttype, dim_rnicustomerloadcolumn_primaryorder, dim_rnicustomerloadcolumn_primarydescending, dim_rnicustomerloadcolumn_required)
SELECT 30, dim_rnicustomerloadcolumn_sourcecolumn, dim_rnicustomerloadcolumn_targetcolumn, 0, 0, NULL, 0, 0, 0
FROM RNICustomerLoadColumn
WHERE sid_rnicustomerloadsource_id = 20
*/

INSERT INTO RNIMappedValueType (dim_rnimappedvaluetype_name, dim_rnimappedvaluetype_desc)
VALUES ('ACCTTYPE', 'Affiliat Account Type')
	, ('TIPFIRST', 'Tipfirst')

INSERT INTO RNIMappedValue (sid_rnimappedvaluetype_id, dim_rnimappedvalue_value)
VALUES (1, 'CREDIT')
	, (1, 'DEBIT')

INSERT INTO RNIMappedValue (sid_rnimappedvaluetype_id, dim_rnimappedvalue_value)
SELECT 2, dbnumber
FROM dbprocessinfo
WHERE 
	(DBNUMBER LIKE '7%' OR DBNumber = '229') 
	AND sid_FiProdStatus_statuscode = 'P'


INSERT INTO RNISourceMappedValueXRef 
(
	dim_rnisourcemappedvaluexref_sourcetable, dim_rnisourcemappedvaluexref_sourcecolumn, dim_rnisourcemappedvaluexref_startindex
	, dim_rnisourcemappedvaluexref_length, dim_rnisourcemappedvaluexref_sourcevalue, sid_rnimappedvalue_id
)
VALUES ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '229', 3)
	, ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '702', 4)
	, ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '704', 5)
	, ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '707', 6)
	, ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '709', 7)
	, ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '712', 8)
	, ('RNICustomer', 'DIM_RNICUSTOMER_PORTFOLIO', 1, 3, '717', 9)
