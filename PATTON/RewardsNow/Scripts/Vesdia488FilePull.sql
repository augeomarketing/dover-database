USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [SSIS Configurations]  where ConfigurationFilter = 'Vesdia488FilePull'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Key]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Action]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Connections[SSH Connection Manager 1].Properties[TransferBinary]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'rewardsnow', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'Yi<cj9rj', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerKeyFile]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'sftp1.vesdia.com', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Connections[SSH Connection Manager 1].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyUser]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyType]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'80', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPort]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPassword]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyHost]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'SSH Connection Manager 1', N'\Package.Connections[SSH Connection Manager 1].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[LogFile]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'ServerHost=sftp1.vesdia.com;ServerPort=22;ServerUser=rewardsnow;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'2', N'\Package.Connections[SSH Connection Manager 1].Properties[BackendVersion]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaFilePull-{F1D8BA63-E3A0-40B3-A850-93F02533B0D5}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488FilePull', N'1', N'\Package.Connections[pubring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'0', N'\Package.Connections[pubring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[pubring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'1', N'\Package.Connections[Input].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'Input', N'\Package.Connections[Input].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'2', N'\Package.Connections[Input].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Connections[Input].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\Vesdia\Input', N'\Package.Connections[Input].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'/OutBound/Ves_RN-Offline Online - March 00.csv.pgp', N'\Package.Variables[User::VesdiaPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::VesdiaPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'VesdiaPathAndName', N'\Package.Variables[User::VesdiaPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]      +  " " +  @[User::NextFileMonth]        +  " "  +    RIGHT((DT_STR, 4,1252) @[User::NextFileYear] ,2)         +  ".csv.pgp"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::VesdiaPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::VesdiaPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'VesdiaPath', N'\Package.Variables[User::VesdiaPath].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'2000', N'\Package.Variables[User::NextFileYear].Properties[Value]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileYear].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileYear].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::NextFileYear].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'NextFileYear', N'\Package.Variables[User::NextFileYear].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Variables[User::NextFileYear].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::NextFileYear].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileYear].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::NextFileYear].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'March', N'\Package.Variables[User::NextFileMonth].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileMonth].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileMonth].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::NextFileMonth].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'NextFileMonth', N'\Package.Variables[User::NextFileMonth].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileMonth].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::NextFileMonth].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileMonth].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::NextFileMonth].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'2012-01-25', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488FilePull', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\Vesdia\Input\Ves_RN-Offline Online - January 12.csv.pgp', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::LocalPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'LocalPathAndName', N'\Package.Variables[User::LocalPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::LocalPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::LocalPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'LocalPath', N'\Package.Variables[User::LocalPath].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'2011-11-25', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'Ves_RN-Offline Online - January 12.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'Vesdia488FilePull', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Target]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User::LocalPathAndName', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Source]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Result]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Password]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488FilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'OpenPGP Task - decrypt Accrual File', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'Ves_RN-Offline Online -', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\Vesdia\Input\Ves_RN-Offline Online - January 12.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'Ves_RN-Offline Online - January 12.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\Vesdia\Input\Vesdia488.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'@[User::LocalPath]  +  "Vesdia488.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'\\patton\ops\Vesdia\Input\Archive\488\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488FilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488FilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

