  	/*
  select * from dbprocessinfo where RNProgramName is not null
  select * from dbprocessinfo where servername is not null
  select * from DBProcessinfo where dbnumber='951'
	*/
	use [RewardsNOW] 
	--BEGIN DECLARATIONS
	declare 
		@DBNumber			varchar(50), 
		@DBNamePatton		varchar(50), 
		@DBNameNEXL			varchar(50), 
		@DBAvailable		varchar(1), 
		@ClientCode			varchar(50), 
		@ClientName			varchar(256), 
		@ProgramName		varchar(256), 
		@DBLocationPatton	varchar(50), 
		@DBLocationNexl		varchar(50), 
		@PointExpirationYears int, 
		@DateJoined			datetime, 
		@MinRedeemNeeded	int, 
		@MaxPointsPerYear	numeric(18,0), 
		@LastTipNumberUsed	varchar(15), 
		@PointsExpireFrequencyCd varchar(2), 
		@ClosedMonths		int, 
		@WelcomeKitGroupName varchar(50), 
		@GenerateWelcomeKit varchar(1), 
		@sid_FiProdStatus_statuscode varchar(1), 
		@IsStageModel		int, 
		@ExtractGiftCards	varchar(1), 
		@ExtractCashBack	varchar(1), 
		@RNProgramName		varchar(50), --use as TPM Name
		@TransferHistToRn1	varchar(1), 
		@CalcDailyExpire	varchar(1)
		
		--END DECLARATIONS


		set @DBNumber = '952' 
		set @DBNamePatton = '952' 
		set @DBNameNEXL  = 'TestClass952' 
		set @DBAvailable  = 'Y'
		set @ClientCode  = '952ClientCode' 
		set @ClientName  = '952ClientName' 
		set @ProgramName  = 'Test' 
		set @DBLocationPatton  = '952' 
		set @DBLocationNexl  = 'TestClass952' 
		set @PointExpirationYears  = '3' 
		set @DateJoined  = '05/15/2010' 
		set @MinRedeemNeeded  = '750' 
		set @MaxPointsPerYear  = '100000' 
		set @LastTipNumberUsed  = '952000000000000' 
		set @PointsExpireFrequencyCd  = 'YE' 
		set @ClosedMonths  = 2 
		set @WelcomeKitGroupName  = NULL 
		set @GenerateWelcomeKit  = 'Y' 
		set @sid_FiProdStatus_statuscode  = 'P' 
		set @IsStageModel  = '1' 
		set @ExtractGiftCards  = 'N' 
		set @ExtractCashBack  = 'N' 
		set @RNProgramName  = 'Test952'  --this is the TPM name (coop,metavante,etc)
		set @TransferHistToRn1  = 'Y' 
		set @CalcDailyExpire  = 'N' 
--SHAWN ADD YOUR VARIABLE test values here HERE	(ones not included above)	
-- These are for searchdb and client tables
		declare
		@cashback INT = 0, 
		@custtravelfee BIT = 1,
		@display_decimal INT = 0, 
		@loginURL VARCHAR(1024) = '',
		@loginUnrecognizedURL VARCHAR(1024) = '',
		@TravelCCReqdInProgram TINYINT = 1, 
		@yearlyredemptioncap INT = '', 
		@travelratio INT = 100,
		@passphrase VARCHAR(65) = '',
		@travelinc int = 2500,
		@travelbottom int = 5000,
		@cashbackminimum int = 9999999,
		@airfee int = 10,
		@logo varchar(50) = '',
		@landing varchar(255) = 'rewardsaccount.asp',
		@stmtnum int = 2,
		@customerservicephone varchar(50) = '1-877-RNI-TEST'
		
			
--FIX BEFORE LIVE		
	
exec RN1.rewardsnow.dbo.usp_NewClient_PopulateSearchDB		
						@DBNumber	,	
						@ClientName	,	
						@DBNameNEXL ,    
						@cashback ,		
						@custtravelfee ,
						@display_decimal ,	
						@loginURL ,			
						@loginUnrecognizedURL, 
						@TravelCCReqdInProgram ,
						@yearlyredemptioncap ,
						@travelratio ,
						@passphrase		

		
exec RN1.rewardsnow.dbo.usp_NewClient_PopulateRN1
	@dbnumber, @clientcode, @clientname, @programname, @travelinc, @travelbottom, @cashbackminimum, 
	@airfee, @logo, @landing, @stmtnum, @customerservicephone
				

--END SHAWNS VARIABLES	

--======================================================================================		
--Create	 the Database
exec usp_NewClient_CreateDatabase @DBNumber
				
--===================================================================		
--Begin DBProcessINFO	
	
exec usp_NewClient_Populate_DBPI
		@DBNumber			, 
		@DBNamePatton		, 
		@DBNameNEXL			, 
		@DBAvailable		, 
		@ClientCode			, 
		@ClientName			, 
		@ProgramName		, 
		@DBLocationPatton	, 
		@DBLocationNexl		, 
		@PointExpirationYears , 
		@DateJoined			, 
		@MinRedeemNeeded	, 
		@MaxPointsPerYear	, 
		@LastTipNumberUsed	, 
		@PointsExpireFrequencyCd , 
		@ClosedMonths		, 
		@WelcomeKitGroupName , 
		@GenerateWelcomeKit , 
		@sid_FiProdStatus_statuscode , 
		@IsStageModel		, 
		@ExtractGiftCards	, 
		@ExtractCashBack	, 
		@RNProgramName		, 
		@TransferHistToRn1	, 
		@CalcDailyExpire
		
--END DBProcessINFO
--===========================================

--BEGIN FI.Client table
 exec usp_NewClient_Populate_ClientTable
						@ClientCode,  
						@ClientName, 
						@DBNumber, 
						@DateJoined,
						@RNProgramName, 
						@MinRedeemNeeded, 
						@MaxPointsPerYear, 
						@PointExpirationYears,
						@ClosedMonths

--END FI.Client table
--==================================================	


--BEGIN rewardsnow.rptCtlClients table
 exec usp_NewClient_Populate_RptCtlClients
		@ClientCode	, 
		@DBNumber,
		@ClientName	,  
		@DBLocationPatton, 
		'[Bradley\RN]' , --hardcoded coming in to [Bradley\RN]
		@DBLocationNexl	, 
		'[RN1]'  --hardcoded '[RN1]'

--END rewardsnow.rptCtlClients table
--==================================================	




	
--review and delete	DBProcessInfoRecord, Client and RptCtl	

declare @DBName varchar(50), @SQL nvarchar(2000)
	
	select @DBName = ltrim(rtrim(DBNamePatton))
	from rewardsnow.dbo.dbprocessinfo
	where dbnumber = @DBNumber

	select * from rewardsnow.dbo.dbprocessinfo where DBNumber=@DBNumber 
	
	select * from RptCtlClients where ClientNum=@DBNumber 

	set @SQL='SELECT * FROM ' + QuoteName(@DBName) + N'.dbo.Client
			 WHERE ClientCode=@ClientCode' 
			print @SQL
	Exec sp_executesql @SQL, N'@ClientCode varchar(50) ', @ClientCode=@ClientCode


----------DELETE THE NEW RECORDS
	--client record in FI Database
/*	
	set @SQL='DELETE ' + QuoteName(@DBName) + N'.dbo.Client
			 WHERE ClientCode=@ClientCode' 
			print @SQL
	
	Exec sp_executesql @SQL, N'@ClientCode varchar(50) ', @ClientCode=@ClientCode
	
	--RptCtlClients record in Rewardsnow
	delete RptCtlClients where ClientNum=@DBNumber 	
	--RptCtlClients record in Rewardsnow
	delete dbprocessinfo where DBNumber=@DBNumber 	
*/
---------------------------------------------------
--SHAWN- Add your delete statements here for testing purposes

---------------------------------------------------

/*
select * from dbprocessinfo where DBNumber='951'
select * from RptCtlClients where ClientNum='951'
*/

