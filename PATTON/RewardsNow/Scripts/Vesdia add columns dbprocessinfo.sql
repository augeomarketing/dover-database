 
 ALTER TABLE [rewardsnow].[dbo].[dbprocessinfo] ADD   [VesdiaParticipant] [varchar](1) NOT NULL
GO
 ALTER TABLE [rewardsnow].[dbo].[dbprocessinfo] ADD 	[AccessDevParticipant] [varchar](1) NOT NULL
GO
  
 ALTER TABLE [rewardsnow].[dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_VesdiaParticipant]  DEFAULT ('N') FOR [VesdiaParticipant]
GO
ALTER TABLE [rewardsnow].[dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_AccessDevelopment]  DEFAULT ('N') FOR [AccessDevParticipant]
GO
 
update[rewardsnow].[dbo].[dbprocessinfo] set VesdiaParticipant = 'N' 
go
update[rewardsnow].[dbo].[dbprocessinfo] set AccessDevParticipant = 'N'
go
 