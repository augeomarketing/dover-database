USE [RewardsNow]
GO

/****** Object:  Table [dbo].[OPTOUTTracking]    Script Date: 01/21/2013 14:52:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SELECT [TipPrefix]
      ,[TIPNUMBER]
      ,[ACCTID]
      ,[FIRSTNAME]
      ,[LASTNAME]
      ,[OPTOUTDATE]
      ,[OPTOUTSOURCE]
      ,[OPTOUTPOSTED]
INTO #TEMP_TRACKING
FROM [dbo].[OPTOUTTracking]

DROP TABLE [dbo].[OPTOUTTracking]

CREATE TABLE [dbo].[OPTOUTTracking](
	[TipPrefix] [nvarchar](3) NULL,
	[TIPNUMBER] [nvarchar](15) NOT NULL,
	[ACCTID] [nvarchar](25) NOT NULL,
	[FIRSTNAME] [nvarchar](50) NOT NULL,
	[LASTNAME] [nvarchar](50) NULL,
	[OPTOUTDATE] [datetime] NOT NULL,
	[OPTOUTSOURCE] [nvarchar](40) NOT NULL,
	[OPTOUTPOSTED] [datetime] NULL,
 CONSTRAINT [PK_OPTOUTTracking] PRIMARY KEY CLUSTERED 
(
	[TIPNUMBER] ASC,
	[ACCTID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OPTOUTTracking] ADD  CONSTRAINT [DF_OPTOUTTracking_LASTNAME]  DEFAULT ('') FOR [LASTNAME]
GO

INSERT INTO [dbo].[OPTOUTTracking] ([TipPrefix]
      ,[TIPNUMBER]
      ,[ACCTID]
      ,[FIRSTNAME]
      ,[LASTNAME]
      ,[OPTOUTDATE]
      ,[OPTOUTSOURCE]
      ,[OPTOUTPOSTED]
      )
SELECT [TipPrefix]
      ,[TIPNUMBER]
      ,[ACCTID]
      ,[FIRSTNAME]
      ,[LASTNAME]
      ,[OPTOUTDATE]
      ,[OPTOUTSOURCE]
      ,[OPTOUTPOSTED]
FROM #TEMP_TRACKING

DROP TABLE #TEMP_TRACKING


