USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'REPLACE(,SUBSTRING(@[User::FileNameAndPath],28,80),".pgp","")', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'True', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'AD-RN-2199*ORGANIZATION-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'@[User::FilePath]  +   @[User::FileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\Organization.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'@[User::FilePath]  +  "Organization.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\Archive\Organization-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-OrganizationData-{410FD4B1-62E7-4633-A97E-DE954DC92A46}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_OrgData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'c:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\ORGANIZATION-DATA', N'\Package.Connections[ORGANIZATION-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\test.zip.pgp', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

