
USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_editaddress_dim_editaddress_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[editaddress] DROP CONSTRAINT [DF_editaddress_dim_editaddress_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_editaddress_dim_editaddress_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[editaddress] DROP CONSTRAINT [DF_editaddress_dim_editaddress_lastmodified]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_editaddress_dim_editaddress_active]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[editaddress] DROP CONSTRAINT [DF_editaddress_dim_editaddress_active]
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[editaddress]') AND type in (N'U'))
DROP TABLE [dbo].[editaddress]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[editaddress](
	[sid_editaddress_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_editaddress_name] [varchar](50) NOT NULL,
	[dim_editaddress_created] [datetime] NOT NULL,
	[dim_editaddress_lastmodified] [datetime] NOT NULL,
	[dim_editaddress_active] [int] NOT NULL,
 CONSTRAINT [PK_editaddress] PRIMARY KEY CLUSTERED 
(
	[sid_editaddress_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[editaddress] ADD  CONSTRAINT [DF_editaddress_dim_editaddress_created]  DEFAULT (getdate()) FOR [dim_editaddress_created]
GO

ALTER TABLE [dbo].[editaddress] ADD  CONSTRAINT [DF_editaddress_dim_editaddress_lastmodified]  DEFAULT (getdate()) FOR [dim_editaddress_lastmodified]
GO

ALTER TABLE [dbo].[editaddress] ADD  CONSTRAINT [DF_editaddress_dim_editaddress_active]  DEFAULT ((1)) FOR [dim_editaddress_active]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

insert into editaddress (dim_editaddress_name)
values ('Edit Denied')
insert into editaddress (dim_editaddress_name)
values ('Edit Allowed: User Data Precedes FI Data')
insert into editaddress (dim_editaddress_name)
values ('Edit Allowed: FI Data Precedes User Data')

GO

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.editaddress SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.dbprocessinfo ADD
	sid_editaddress_id int NOT NULL CONSTRAINT DF_dbprocessinfo_sid_editaddress_id DEFAULT 1
GO
ALTER TABLE dbo.dbprocessinfo ADD CONSTRAINT
	FK_dbprocessinfo_editaddress FOREIGN KEY
	(
	sid_editaddress_id
	) REFERENCES dbo.editaddress
	(
	sid_editaddress_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.dbprocessinfo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


