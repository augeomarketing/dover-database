USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'ZaveeViewTransaction'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'ZaveeViewTransaction', N'\\patton\ops\Zavee\Logs\ProcessingLog.txt', N'\Package.Connections[ProcessingLog.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeViewTransaction', N'http://dalservice.rewardsnow.com/TransDataService.svc', N'\Package.Variables[User::ServiceUrl].Properties[Value]', N'String' UNION ALL
SELECT N'ZaveeViewTransaction', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-ViewTransaction-{4EFB8B79-05B9-44B9-9DA1-5C3B062D0C25}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeViewTransaction', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-ViewTransaction-{34C9A2F4-2F0F-45DA-B826-2D80FD2984AD}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

