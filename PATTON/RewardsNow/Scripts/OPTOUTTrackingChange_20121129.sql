/*
   Thursday, November 29, 201210:04:21 AM
   User: 
   Server: doolittle\rn
   Database: RewardsNow
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
USE [RewardsNow]
GO

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OPTOUTTracking ADD CONSTRAINT
	DF_OPTOUTTracking_LASTNAME DEFAULT '' FOR LASTNAME
GO
ALTER TABLE dbo.OPTOUTTracking SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
