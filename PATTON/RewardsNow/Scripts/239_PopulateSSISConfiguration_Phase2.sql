USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'239', N'\\doolittle\ops\239\Utility\Templates\WelcomeKitTemplate.xls', N'\Package.Variables[User::WelcomeKitTemplate].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'doolittle\web', N'\Package.Variables[User::WebDBServer].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'MutualSavingsCU', N'\Package.Variables[User::WebDBName].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'RewardsNow', N'\Package.Variables[User::UtilityDBName].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'True', N'\Package.Variables[User::Run045EStatements].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'True', N'\Package.Variables[User::Run040PostToWeb].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::Run030WelcomeKits].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::Run020PostToProduction].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'\\doolittle\ops\239\Output', N'\Package.Variables[User::ProductionDir].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'\\doolittle\ops\239', N'\Package.Variables[User::OperationsDir].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'239', N'\Package.Variables[User::FIDBName].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'doolittle\rn', N'\Package.Variables[User::DBServer].Properties[Value]', N'String'
COMMIT;
GO

