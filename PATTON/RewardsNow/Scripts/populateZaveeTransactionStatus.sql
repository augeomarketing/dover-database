USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[ZaveeTransactionStatus] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[ZaveeTransactionStatus]([sid_ZaveeTransactionStatus_id], [dim_ZaveeTransactionStatus_name], [dim_ZaveeTransactionStatus_desc], [dim_ZaveeTransactionStatus_dateadded], [dim_ZaveeTransactionStatus_lastmodified], [dim_ZaveeTransactionStatus_lastmodifiedby])
SELECT 0, N'Sent', N'Sent to Zavee', '20130516 10:01:16.997', NULL, N'dirish' UNION ALL
SELECT 1, N'Accepted', N'Accepted by Zavee', '20130516 10:01:41.073', NULL, N'dirish' UNION ALL
SELECT 2, N'Pending', N'Pending and awaiting Zavees confirmaiton', '20130516 10:02:34.693', NULL, N'dirish' UNION ALL
SELECT 3, N'Earned', N'Earned on Zavee''s Books', '20130516 10:02:55.507', NULL, N'dirish' UNION ALL
SELECT 4, N'Invalid Merchant', N'RNI does not have record of Merchant', '20130516 10:03:22.983', NULL, N'dirish' UNION ALL
SELECT 5, N'Rejected By Zavee ', N'Rejected by Zavee', '20130516 10:03:41.487', NULL, N'dirish' UNION ALL
SELECT 6, N'Invalid Member', N'Rejected by Zavee - invalid Member', '20130516 10:04:14.950', NULL, N'dirish'
COMMIT;
RAISERROR (N'[dbo].[ZaveeTransactionStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[ZaveeTransactionStatus] OFF;

