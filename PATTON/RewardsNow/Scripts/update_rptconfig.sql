use RewardsNow
 
 delete RptConfig where RptType = 'involvement' and  ClientID = 'Std'
go

INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-1','TotalPoints')   
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-2','totRedemptions')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-3','RedeemedAccts')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-4','NoNeverRedeemed')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-5','PctHouseholdRedeem')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-6','AvgNoRdmptPerAA')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-7','AvgNoRdmptPer')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-8','NoRegOnline')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-9','NoEStatements')
INSERT INTO [RewardsNow].[dbo].[RptConfig]     ([ClientID] ,[RptType] ,[RecordType] ,[RptCtl])
     VALUES ('Std','Involvement','RowID-10','UniqueLogins')
     --=========================
     
    