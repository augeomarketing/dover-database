USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter like 'ad_category%'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_Category', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Properties[VersionMinor]', N'Int32' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Properties[VersionMajor]', N'Int32' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Properties[VersionComments]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'31', N'\Package.Properties[VersionBuild]', N'Int32' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Properties[UpdatePackage]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[UpdateObjects]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[SuppressConfigurationWarnings]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'\', N'\Package.Properties[SQLFolder]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[SaveCheckpoints]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Category', N'5', N'\Package.Properties[PackageType]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Properties[PackagePriorityClass]', N'Object' UNION ALL
SELECT N'AD_Category', N'********', N'\Package.Properties[PackagePassword]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[OfflineMode]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'CategoryStage', N'\Package.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Category', N'-1', N'\Package.Properties[MaxConcurrentExecutables]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Category', N'0', N'\Package.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Category', N'1033', N'\Package.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Category', N'1048576', N'\Package.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[InteractiveMode]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'-1', N'\Package.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[EncryptCheckpoints]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Properties[EnableDump]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[DumpOnAnyError]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Properties[DumpDescriptor]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'REWARDSNOW\dirish', N'\Package.Properties[CreatorName]', N'String' UNION ALL
SELECT N'AD_Category', N'RN155', N'\Package.Properties[CreatorComputerName]', N'String' UNION ALL
SELECT N'AD_Category', N'8/28/2012 11:46:19 AM', N'\Package.Properties[CreationDate]', N'DateTime' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Properties[CheckSignatureOnLoad]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Properties[CheckpointUsage]', N'Object' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Properties[CheckpointFileName]', N'String' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Properties[CertificateContext]', N'Int64' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Category', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Category', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Category-{4C1FB0CE-AE1A-43F4-968B-EC25B7050294}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Connections[Category.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Category', N'Category.csv', N'\Package.Connections[Category.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Connections[Category.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Connections[Category.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Connections[Category.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Connections[category].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'"', N'\Package.Connections[category].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Connections[category].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_Category', N'1', N'\Package.Connections[category].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Category', N'category', N'\Package.Connections[category].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'1033', N'\Package.Connections[category].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Connections[category].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Category', N'
', N'\Package.Connections[category].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Category', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'Delimited', N'\Package.Connections[category].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[CompressionType]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\UNZip Task.Properties[CompressionLevel]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryHistory', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-CategoryHistory-{43009E35-72A3-4833-8BFA-1A6B7AB94523}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Connections[category].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Connections[category].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'0', N'\Package.Connections[category].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Connections[category].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Connections[category].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'1252', N'\Package.Connections[category].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\UNZip Task.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'CATEGORY-DATA', N'\Package\Foreach Loop Container\UNZip Task.Properties[Target]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\UNZip Task.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\UNZip Task.Properties[Password]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'UNZip Task', N'\Package\Foreach Loop Container\UNZip Task.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'2', N'\Package\Foreach Loop Container\UNZip Task.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'1033', N'\Package\Foreach Loop Container\UNZip Task.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1048576', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[IncludeSubfolders]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'-1', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForcedExecutionValue]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[EncryptionType]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'Zip Task', N'\Package\Foreach Loop Container\UNZip Task.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Category', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Category', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Category', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_Category', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_Category', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_Category', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'OpenPGP Task  - Decrypt Category File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Category', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Category', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Category', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Key]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Category', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Category', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Category', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Category', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 8.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'OpenPGP Task  - Decrypt Category File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Category File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 9.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 10.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 11.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'Variable', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OperationName]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 12.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'3', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'File System Task - Rename for connection mgr', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User::CSVFile', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 13.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 14.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 15.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 16.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Properties[VersionMinor]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Properties[VersionMajor]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Properties[VersionComments]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'31', N'\Package.Properties[VersionBuild]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Properties[UpdatePackage]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[UpdateObjects]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[SuppressConfigurationWarnings]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'\', N'\Package.Properties[SQLFolder]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[SaveCheckpoints]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'5', N'\Package.Properties[PackageType]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Properties[PackagePriorityClass]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'********', N'\Package.Properties[PackagePassword]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[OfflineMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'CategoryStage', N'\Package.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package.Properties[MaxConcurrentExecutables]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'1048576', N'\Package.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[InteractiveMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'-1', N'\Package.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[EncryptCheckpoints]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Properties[EnableDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[DumpOnAnyError]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Properties[DumpDescriptor]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REWARDSNOW\dirish', N'\Package.Properties[CreatorName]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 17.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'RN155', N'\Package.Properties[CreatorComputerName]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'8/28/2012 11:46:19 AM', N'\Package.Properties[CreationDate]', N'DateTime' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Properties[CheckSignatureOnLoad]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Properties[CheckpointUsage]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Properties[CheckpointFileName]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Properties[CertificateContext]', N'Int64' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Category-{4C1FB0CE-AE1A-43F4-968B-EC25B7050294}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Connections[Category.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'Category.csv', N'\Package.Connections[Category.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Connections[Category.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Connections[Category.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Connections[Category.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Connections[category].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'"', N'\Package.Connections[category].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Connections[category].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1', N'\Package.Connections[category].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'category', N'\Package.Connections[category].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'1033', N'\Package.Connections[category].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Connections[category].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'
', N'\Package.Connections[category].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'Delimited', N'\Package.Connections[category].Properties[Format]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Connections[category].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Connections[category].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'0', N'\Package.Connections[category].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Connections[category].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Connections[category].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'1252', N'\Package.Connections[category].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 18.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'AD-RN-2199*Category.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Category.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'@[User::FilePath]  +  "Category.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Category\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 19.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 20.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'Microsoft Visual Basic 2008', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ScriptLanguage]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'User::FileSize', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ReadWriteVariables]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ReadOnlyVariables]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ST_6caa457696a14819a78a9d9a73a3509e', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ScriptProjectName]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'Script Task - Get File Size     If file is empty we will not unzip in next step', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'1033', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'1048576', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'-1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'Main', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[EntryPoint]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'Script Task', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 21.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 22.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'OpenPGP Task  - Decrypt CATEGORY DATA File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Disable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 23.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 24.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'Variable', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt CATEGORY DATA File.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 25.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 26.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 27.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 28.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'Variable', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_CategoryData', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-CategoryData-{499BE294-527D-4C1A-9174-F7A9D250A93B}SSIS Config;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'1', N'\Package.Connections[CATEGORY-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'CATEGORY-DATA', N'\Package.Connections[CATEGORY-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'2', N'\Package.Connections[CATEGORY-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Connections[CATEGORY-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\CATEGORY-DATA', N'\Package.Connections[CATEGORY-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 29.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'AD-RN-2199*CATEGORY-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 30.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryData', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'\\patton\ops\AccessDevelopment\Input\Archive\Category-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_CategoryData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_CategoryData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 31.....Done!', 10, 1) WITH NOWAIT;
GO

