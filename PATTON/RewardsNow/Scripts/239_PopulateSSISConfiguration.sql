﻿USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'239', N'\\236722-SQLCLUS2\ops\239\Input\rewards.memberinfo.201008.csv', N'\Package.Variables[User::DemographicImportFile].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::Debug].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'Data Source=236722-SQLCLUS2\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{CE1B7066-A342-44FB-AD30-DAAC1F2D7ED9}236722-SQLCLUS2\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[UTILITYDB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'Data Source=236722-SQLCLUS2\rn;Initial Catalog=239;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Package-{015787FF-7458-4C26-AA06-8F0156DD3256}236722-SQLCLUS2\rn.239;', N'\Package.Connections[FIDB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'239', N'\\236722-SQLCLUS2\ops\239\Utility\239_TransactionUploadFormat.xml', N'\Package.Variables[User::TransactionImportFormatFile].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'\\236722-SQLCLUS2\ops\239\Input\rewards.points.201008.csv', N'\Package.Variables[User::TransactionImportFile].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'239', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'239', N'10/01/2010 00:00:00:000', N'\Package.Variables[User::StartDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'239', N'10/12/2010 10:44:11 AM', N'\Package.Variables[User::RunDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'239', N'True', N'\Package.Variables[User::Run010ImportToAudit].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'False', N'\Package.Variables[User::ResetDemog].Properties[Value]', N'Boolean' UNION ALL
SELECT N'239', N'10/31/2010 23:59:59:990', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'239', N'\\236722-SQLCLUS2\ops\239\Utility\239_DemographicUploadFormat.xml', N'\Package.Variables[User::DemographicImportFormatFile].Properties[Value]', N'String'
COMMIT;
GO

