USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[mappingdestination] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[mappingdestination]([sid_mappingdestination_id], [dim_mappingdestination_columnname], [dim_mappingdestination_active], [dim_mappingdestination_created], [dim_mappingdestination_lastmodified])
SELECT 1, N'LastSix', 1, '20101228 15:49:17.917', NULL UNION ALL
SELECT 2, N'SSNLast4', 1, '20101228 15:49:22.927', NULL UNION ALL
SELECT 3, N'MemberID', 1, '20101228 15:49:52.543', NULL UNION ALL
SELECT 4, N'MemberNumber', 1, '20101228 15:49:57.823', NULL UNION ALL
SELECT 5, N'dim_statementvalue_key', 1, '20101230 14:01:56.767', NULL UNION ALL
SELECT 6, N'dim_statementvalue_value', 1, '20101230 14:02:03.770', NULL UNION ALL
SELECT 7, N'dim_statementvalue_valuetype', 1, '20101230 14:02:17.327', NULL
COMMIT;
RAISERROR (N'[dbo].[mappingdestination]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[mappingdestination] OFF;

