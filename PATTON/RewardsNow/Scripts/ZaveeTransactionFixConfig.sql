USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [SSIS Configurations] where configurationfilter = 'ZaveeTransactionFix'
go

 
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'ZaveeTransactionFix', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeTransactionFix', N'https://dal.rewardsnow.com/TransDataService.svc', N'\Package.Connections[HTTP Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeTransactionFix', N'https://dal.rewardsnow.com/TransDataService.svc', N'\Package.Variables[User::ServiceUrl].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

