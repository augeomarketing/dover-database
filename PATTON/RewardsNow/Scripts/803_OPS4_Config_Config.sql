USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete from [dbo].[SSIS Configurations] where [ConfigurationFilter] = '803_OPS4_DTSX' or [ConfigurationFilter] = '803'
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'803', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-Package-{5B11480B-33BA-4A20-8780-DFE1A472B18F}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'803', N'Data Source=patton\rn;Initial Catalog=803;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-803_OPS5_WelcomeKit-{008044C9-51DB-46EB-8E02-0ECAFFB5D35F}patton\rn.803;', N'\Package.Connections[803Montgomery].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'803_OPS4_DTSX', N'Data Source=rn1;Initial Catalog=HorizonBank;Provider=SQLOLEDB.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-803_OPS4_Push_ProdToWeb-{0C73BA50-1325-4377-86B1-2CB981B01508}rn1.HorizonBank;', N'\Package.Connections[HorizonBank].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

