USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [dbo].[SSIS Configurations] where [ConfigurationFilter] like '%803_OPS6%'
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'803_OPS6_DTSX', N'O:\803\OutPut\Audit\803_Quarterly_Statement.Csv', N'\Package.Connections[803_Quarterly_Audit].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

