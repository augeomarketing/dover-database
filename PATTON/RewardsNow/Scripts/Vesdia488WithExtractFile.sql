USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations]
where configurationfilter = 'Vesdia488WithExtractFile'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488WithExtractFile', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Vesdia488-{7946C33B-D25F-4E04-B777-DDD4C572DD9F}RN1.RewardsNOW;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'False', N'\Package.Connections[Vesdia 488].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'"', N'\Package.Connections[Vesdia 488].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[Vesdia 488].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1', N'\Package.Connections[Vesdia 488].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Vesdia 488', N'\Package.Connections[Vesdia 488].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1033', N'\Package.Connections[Vesdia 488].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[Vesdia 488].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'
', N'\Package.Connections[Vesdia 488].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Delimited', N'\Package.Connections[Vesdia 488].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[Vesdia 488].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[Vesdia 488].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[Vesdia 488].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'\\patton\ops\Vesdia\Input\Vesdia488.csv', N'\Package.Connections[Vesdia 488].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Connections[Vesdia 488].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1252', N'\Package.Connections[Vesdia 488].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'False', N'\Package.Connections[controlFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'"', N'\Package.Connections[controlFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'ASB', N'\Package.Variables[User::vDBName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Sep11', N'\Package.Variables[User::v488Date].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Variables[User::CtlVersion].Properties[Value]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Variables[User::CtlRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Variables[User::CtlClientId].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Variables[User::ClientIdName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\002_SFACTUALTRANS_20120618.CSV', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\002_SF_20120618.CTL', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'@[User::FilePath]  +   @[User::ArchiveCTLFile]', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\Archive_ActualExtracts\', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'002_SF_20120618.CTL', N'\Package.Variables[User::ArchiveCTLFile].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Data Source=Patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Vesdia488-{73BC8B52-1059-4C77-9B11-42D06E3B1498}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Data Source=Patton\RN;Initial Catalog=OnlineHistoryWork;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Vesdia488-{E621B03B-E757-41EF-9AD6-34EA642C7EDD}Patton\RN.OnlineHistoryWork;', N'\Package.Connections[OnlineHistoryWork].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[controlFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1', N'\Package.Connections[controlFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'controlFile', N'\Package.Connections[controlFile].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1033', N'\Package.Connections[controlFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'
', N'\Package.Connections[controlFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Delimited', N'\Package.Connections[controlFile].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[controlFile].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\_SF_20120813.ctl', N'\Package.Connections[controlFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Connections[controlFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1252', N'\Package.Connections[controlFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'False', N'\Package.Connections[ClientExportFile].Properties[Unicode]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'Vesdia488WithExtractFile', N'"', N'\Package.Connections[ClientExportFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[ClientExportFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1', N'\Package.Connections[ClientExportFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'ClientExportFile', N'\Package.Connections[ClientExportFile].Properties[Name]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1033', N'\Package.Connections[ClientExportFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'
', N'\Package.Connections[ClientExportFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'Delimited', N'\Package.Connections[ClientExportFile].Properties[Format]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'', N'\Package.Connections[ClientExportFile].Properties[Description]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'O:\Vesdia\Output\_SFACTUALTRANS_20120813.csv', N'\Package.Connections[ClientExportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'True', N'\Package.Connections[ClientExportFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1252', N'\Package.Connections[ClientExportFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'PetCo', N'\Package.Variables[User::vMerchantName].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'1234', N'\Package.Variables[User::vMerchantID].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'236', N'\Package.Variables[User::vDBNumber].Properties[Value]', N'String' UNION ALL
SELECT N'Vesdia488WithExtractFile', N'002_SFACTUALTRANS_20120618.CSV', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

