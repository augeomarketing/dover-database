USE RewardsNow
GO

INSERT INTO TranType(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
SELECT 
	CASE LEFT(tt.TranCode, 1)
		WHEN 'B' THEN '-'
		WHEN 'F' THEN '+'
		WHEN 'G' THEN '='
		WHEN '0' THEN '*'
	  END
	+ RIGHT(tt.TranCode, 1) as TranCode
	, CASE tt.TranCode
		WHEN 'BF' THEN '*NB-Bonus First Time Card Use'
		WHEN 'F9' THEN '*NB-National Merchant Funded Bonus Rev'
		ELSE '*NB-' + Description 
	  END AS Description
	, IncDec
	, CntAmtFxd
	, Points
	, Ratio
	, 0 as TypeCode
FROM TranType tt 
WHERE TranCode LIKE '[BFG0]%'


INSERT INTO RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
VALUES ('RNI', 'DEBUG_PROMOPOINTCONVERSION_TIP', 'RNI', 1)

--ADD A 'DEFAULT' RNI LEDGER
SET IDENTITY_INSERT PromotionalPointLedger ON

INSERT INTO PromotionalPointLedger (sid_promotionalpointledger_id, sid_dbprocessinfo_dbnumber, dim_promotionalpointledger_maxamount, dim_promotionalpointledger_awarddate, dim_promotionalpointledger_expirationdate)
VALUES (0, 'RNI', -1, '1/1/1900', '12/31/2999')

SET IDENTITY_INSERT PromotionalPointLedger OFF

--SAMPLE SETUP USING ELGA INFORMATION
--INSERT INTO PromotionalPointLedger (sid_dbprocessinfo_dbnumber, dim_promotionalpointledger_maxamount, dim_promotionalpointledger_awarddate, dim_promotionalpointledger_expirationdate)
--VALUES ('248', 1000000, '6/1/2012', '6/1/2013')

-- INSERT DEFAULT BONUS TO PROMOTIONAL TRANCODE MAPPINGS
INSERT INTO PromotionalPointTrancodeXref (sid_promotionalpointledger_id, sid_trantype_trancode_in, sid_trantype_trancode_out, dim_promotionalpointtrancodexref_precedence, dim_promotionalpointtrancodexref_startdate, dim_promotionalpointtrancodexref_enddate)
SELECT 0 AS sid_promotionalpointledger_id
        , tt.TranCode AS sid_trantype_trancode_in
        , CASE LEFT(tt.TranCode, 1)
                WHEN 'B' THEN '-'
                WHEN 'F' THEN '+'
                WHEN 'G' THEN '='
                WHEN '0' THEN '*'
          END
        + RIGHT(tt.TranCode, 1) AS sid_trantype_trancode_out
        , 0 AS dim_promotionalpointtrancodexref_precedence
        , '1/1/1900' AS dim_promotionalpointtrancodexref_startdate
        , '12/31/2999' AS dim_promotionalpointtrancodexref_startdate
FROM TranType tt
WHERE TranCode LIKE '[BFG0]%'
