USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'VesdiaEnrollmentChild'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[EnrollmentDetail].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileHeader].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileHeader].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_112920121418555.csv', N'\Package.Variables[User::FileFound].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileFound].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::FileNameToArchive].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentDetail].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentDetail].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentDetail].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Connections[EnrollmentDetail].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentDetail].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1252', N'\Package.Connections[EnrollmentDetail].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[DestinationFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[DestinationFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[DestinationFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[DestinationFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'DestinationFile', N'\Package.Connections[DestinationFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package.Connections[DestinationFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[DestinationFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[DestinationFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[DestinationFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Connections[DestinationFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[DestinationFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1252', N'\Package.Connections[DestinationFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileNameToArchive].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDetail].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_10212010.csv', N'\Package.Variables[User::FileDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDestination].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDestination', N'\Package.Variables[User::FileDestination].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDestination].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDestination].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDestination].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'10312010', N'\Package.Variables[User::FileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDate', N'\Package.Variables[User::FileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FIDatabase].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIDatabase].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DatedFiles', N'\Package.Variables[User::DestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::DestinationPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'DestinationPath', N'\Package.Variables[User::DestinationPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DestinationPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileNameToArchive].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileHeader', N'\Package.Variables[User::FileHeader].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileHeader].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileFound].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileFound', N'\Package.Variables[User::FileFound].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileFound].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileExtension].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileExtension].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileExtension].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDetailSeqNbr', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'@[User::FileDetailSeqNbr]', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetailSeqNbr].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Variables[User::FileDetail].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileDetail].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileDetail', N'\Package.Variables[User::FileDetail].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileDetail].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileDetail].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'OrlandoFCU', N'\Package.Variables[User::FIWebName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileNameToArchive', N'\Package.Variables[User::FileNameToArchive].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=RN1;Initial Catalog=OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{32E3CF38-FFD3-4669-852C-29340423D7F5}RN1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{F23654BF-1292-4902-9EEE-09A78754E27C}patton\rn.236;Auto Translate=False;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[EnrollmentFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'EnrollmentFile', N'\Package.Connections[EnrollmentFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package.Connections[EnrollmentFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[EnrollmentFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\EnrollmentFile.csv', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1252', N'\Package.Connections[EnrollmentFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Connections[EnrollmentDetail].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[EnrollmentDetail].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package.Connections[EnrollmentDetail].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'EnrollmentDetail', N'\Package.Connections[EnrollmentDetail].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package.Connections[EnrollmentDetail].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Variables[User::FileHeader].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileExtension].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DestinationPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DestinationPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DatabaseForDeletes].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseForDeletes].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225OrlandoFCU', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'DatabaseBeingProcessed', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FIWebName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FIWebName', N'\Package.Variables[User::FIWebName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FIWebName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FIWebName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FIWebName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Variables[User::FileTrailer].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileTrailer].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileTrailer', N'\Package.Variables[User::FileTrailer].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileTrailer].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileTrailer].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileTrailer].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileSource].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileSource', N'\Package.Variables[User::FileSource].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSource].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileNameToArchive].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileHeader].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileFound].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileExtension', N'\Package.Variables[User::FileExtension].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Archive\Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::ArchiveFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::ArchiveFullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'ArchiveFullPath', N'\Package.Variables[User::ArchiveFullPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'@[User::ArchivePath]   +     @[User::FileNameToArchive]', N'\Package.Variables[User::ArchiveFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Variables[User::ArchiveFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::ArchiveFullPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileNameToArchive].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Ves_RN_Enroll_', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSource].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'00009', N'\Package.Variables[User::FileSeqNbr].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FileSeqNbr].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FileSeqNbr', N'\Package.Variables[User::FileSeqNbr].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSeqNbr].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FileSeqNbr].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FileSeqNbr].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

