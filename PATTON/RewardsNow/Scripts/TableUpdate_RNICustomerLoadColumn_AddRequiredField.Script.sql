USE RewardsNow
GO

ALTER TABLE RNICustomerLoadColumn
ADD dim_rnicustomerloadcolumn_required INT DEFAULT(0)
GO

/*

update LC
set 
	dim_rnicustomerloadcolumn_required = CASE WHEN LC.dim_rnicustomerloadcolumn_sourcecolumn in ('Portfolio_Number', 'Member_Number') THEN 1 ELSE 0 END
FROM rnicustomerloadcolumn LC
INNER JOIN rnicustomerloadsource LS
	ON LC.sid_rnicustomerloadsource_id = LS.sid_rnicustomerloadsource_id
WHERE LS.sid_dbprocessinfo_dbnumber = '6EB'

SELECT * FROM rnicustomerloadcolumn LC
INNER JOIN rnicustomerloadsource LS
	ON LC.sid_rnicustomerloadsource_id = LS.sid_rnicustomerloadsource_id
WHERE LS.sid_dbprocessinfo_dbnumber = '6EB'


*/