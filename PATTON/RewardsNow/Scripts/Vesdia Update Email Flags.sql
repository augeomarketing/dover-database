

*** sql to update email flags ****
  update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMobile = 'N'
  
  
  update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailGeneral = 'Y'
  
 -- 002,003 
  update  [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'N'
  from [doolittle\web].[ASB].dbo.[1security]  sec
  inner join   [RewardsNow].[dbo].[VesdiaEnrollmentList] ves
  on  sec.tipnumber = ves.sid_VesdiaEnrollmentList_Tip 
   where emailother = 'N'
   and SUBSTRING(sec.tipnumber,1,3)  in ('002','003')
   
   update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'Y'
  where  dim_VesdiaEnrollmentList_EmailMktg  <> 'N'
  and SUBSTRING(sid_VesdiaEnrollmentList_Tip,1,3) in ('002','003')
  
  --NewTNB 150,160,164
  
   update  [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'N'
  from [doolittle\web].NewTNB.dbo.[1security]  sec
  inner join   [RewardsNow].[dbo].[VesdiaEnrollmentList] ves
  on  sec.tipnumber = ves.sid_VesdiaEnrollmentList_Tip 
   where emailother = 'N'
    and SUBSTRING(sec.tipnumber,1,3)  in ('150','160','164')
   
   update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'Y'
  where  dim_VesdiaEnrollmentList_EmailMktg <> 'N'
  and SUBSTRING(sid_VesdiaEnrollmentList_Tip,1,3) in ('150','160','164')
  
  
  --Fidelity 206
   update  [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'N'
  from [doolittle\web].Fidelity.dbo.[1security]  sec
  inner join   [RewardsNow].[dbo].[VesdiaEnrollmentList] ves
  on  sec.tipnumber = ves.sid_VesdiaEnrollmentList_Tip 
   where emailother = 'N'
   and SUBSTRING(sec.tipnumber,1,3) = '206'
   
   update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'Y'
  where  dim_VesdiaEnrollmentList_EmailMktg <> 'N'
  and SUBSTRING(sid_VesdiaEnrollmentList_Tip,1,3) = '206'  
  
  --Continental 214
    update  [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'N'
  from [doolittle\web].Continental.dbo.[1security]  sec
  inner join   [RewardsNow].[dbo].[VesdiaEnrollmentList] ves
  on  sec.tipnumber = ves.sid_VesdiaEnrollmentList_Tip 
   where emailother = 'N'
   and SUBSTRING(sec.tipnumber,1,3) = '214'
   
   update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'Y'
  where  dim_VesdiaEnrollmentList_EmailMktg <> 'N'
  and SUBSTRING(sid_VesdiaEnrollmentList_Tip,1,3) = '214'   
  
  --Coop  611 617
  
    update  [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'N'
  from [doolittle\web].Coop.dbo.[1security]  sec
  inner join   [RewardsNow].[dbo].[VesdiaEnrollmentList] ves
  on  sec.tipnumber = ves.sid_VesdiaEnrollmentList_Tip 
   where emailother = 'N'
   and SUBSTRING(sec.tipnumber,1,3) in ('611','617','625','636','637')
   
   update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'Y'
  where  dim_VesdiaEnrollmentList_EmailMktg <> 'N'
  and SUBSTRING(sid_VesdiaEnrollmentList_Tip,1,3)   in ('611','617','625','636','637')
  
    --Advantage  615
  
    update  [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'N'
  from [doolittle\web].Advantage.dbo.[1security]  sec
  inner join   [RewardsNow].[dbo].[VesdiaEnrollmentList] ves
  on  sec.tipnumber = ves.sid_VesdiaEnrollmentList_Tip 
   where emailother = 'N'
   and SUBSTRING(sec.tipnumber,1,3) = '615'
   
   update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailMktg = 'Y'
  where  dim_VesdiaEnrollmentList_EmailMktg <> 'N'
  and SUBSTRING(sid_VesdiaEnrollmentList_Tip,1,3) = '615'  
   