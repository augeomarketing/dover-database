USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MerchantData-{B85DCE9B-7029-4B33-B9E6-3F9BB38BC337}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'V:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[MerchantDataToUnZip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'MerchantDataToUnZip', N'\Package.Connections[MerchantDataToUnZip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Connections[MerchantDataToUnZip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[MerchantDataToUnZip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'O:\AccessDevelopment\Input\MerchantData.zip', N'\Package.Connections[MerchantDataToUnZip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[MerchantData.zip.pgp].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'MerchantData.zip.pgp', N'\Package.Connections[MerchantData.zip.pgp].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Connections[MerchantData.zip.pgp].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[MerchantData.zip.pgp].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'O:\AccessDevelopment\Input\MerchantData.zip.pgp', N'\Package.Connections[MerchantData.zip.pgp].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[MerchantData.zip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'MerchantData.zip', N'\Package.Connections[MerchantData.zip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[MerchantData.zip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[MerchantData.zip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'O:\AccessDevelopment\Input\MerchantData.zip', N'\Package.Connections[MerchantData.zip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[MERCHANT-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'MERCHANT-DATA', N'\Package.Connections[MERCHANT-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'2', N'\Package.Connections[MERCHANT-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[MERCHANT-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'O:\AccessDevelopment\Input\MERCHANT-DATA', N'\Package.Connections[MERCHANT-DATA].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

