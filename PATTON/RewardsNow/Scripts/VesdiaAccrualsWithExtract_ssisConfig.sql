USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFile', N'Initial Catalog=RewardsNow; Data Source=patton\RN;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[Accrual File].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'<none>', N'\Package.Connections[Accrual File].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[Accrual File].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1', N'\Package.Connections[Accrual File].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Accrual File', N'\Package.Connections[Accrual File].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1033', N'\Package.Connections[Accrual File].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Accrual File].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'
', N'\Package.Connections[Accrual File].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'RaggedRight', N'\Package.Connections[Accrual File].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Accrual File].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[Accrual File].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Accrual File].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'\\patton\ops\Vesdia\Input\AccrualFile.csv', N'\Package.Connections[Accrual File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[Accrual File].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1252', N'\Package.Connections[Accrual File].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Variables[User::RecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Variables[User::RecordCount].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Variables[User::RecordCount].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'User', N'\Package.Variables[User::RecordCount].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'RecordCount', N'\Package.Variables[User::RecordCount].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Variables[User::RecordCount].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Variables[User::RecordCount].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Variables[User::RecordCount].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Variables[User::RecordCount].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

