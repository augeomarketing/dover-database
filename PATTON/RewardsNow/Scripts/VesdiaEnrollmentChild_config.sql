USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter =   'VesdiaEnrollmentChild'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'ServerHost=sftp1.vesdia.com;ServerPort=22;ServerUser=rewardsnow;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[EnrollmentDetail].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Connections[EnrollmentDetail].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[DestinationFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'', N'\Package.Connections[DestinationFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[DestinationFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Delimited', N'\Package.Connections[DestinationFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Connections[DestinationFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::TTLRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'patton\rn', N'\Package.Connections[rn_all_FI].Properties[ServerName]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{F23654BF-1292-4902-9EEE-09A78754E27C}patton\rn.236;Auto Translate=False;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'
', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\EnrollmentFile.csv', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_012132012144204.csv.pgp', N'\Package.Variables[User::TargetFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output', N'\Package\Foreach Loop - Archive , pgp and ftp file to Cartera.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=rn1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}rn1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Data Source=rn1;Initial Catalog=OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{32E3CF38-FFD3-4669-852C-29340423D7F5}rn1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'True', N'\Package.Connections[SSH Connection Manager 1].Properties[TransferBinary]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'rewardsnow', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Yi<cj9rj', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'sftp1.vesdia.com', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::SourceFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225OrlandoFCU', N'\Package.Variables[User::PattonNameForDelete].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::MyCounter].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'InBound', N'\Package.Variables[User::FTPPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollmentChild', N'OrlandoFCU', N'\Package.Variables[User::FIWebName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Variables[User::FileTrailer].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'00009', N'\Package.Variables[User::FileSeqNbr].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::FileNameToArchive].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'Ves_RN_Enroll_', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Variables[User::FileHeader].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_112920121418555.csv', N'\Package.Variables[User::FileFound].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'0', N'\Package.Variables[User::FileDetailSeqNbr].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Variables[User::FileDetail].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_10212010.csv', N'\Package.Variables[User::FileDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'10312010', N'\Package.Variables[User::FileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\DatedFiles', N'\Package.Variables[User::DestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225OrlandoFCU', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'\\patton\ops\Vesdia\Output\Archive\Ves_RN_Enroll_012132012144204.csv', N'\Package.Variables[User::ArchiveFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollmentChild', N'225', N'\Package.Variables[User::TipFirstForDelete].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

