USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'RNI_Decrypt', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{40CB136B-4FC7-4A97-AE6B-3E12ECF58A0E}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'RNI_Decrypt', N'\\patton\ops\OPS-DECRYPT-PGP\test.csv.gpg', N'\Package.Variables[User::PathAndFile].Properties[Value]', N'String' UNION ALL
SELECT N'RNI_Decrypt', N'\\patton\ops\OPS-DECRYPT-PGP\', N'\Package.Variables[User::Path].Properties[Value]', N'String' UNION ALL
SELECT N'RNI_Decrypt', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[pubring.gpg].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

