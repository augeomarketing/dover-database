USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'dbProcessInfo', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-PublishDBProcessInfo-{E545AC86-D246-467B-9824-A39B8246559F}RN1 - REWARDSNOW;', N'\Package.Connections[Web].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'dbProcessInfo', N'Data Source=PATTON\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-PublishDBProcessInfo-{2746A1B4-CA11-4788-9B29-AB59184396F6}PATTON\RN.RewardsNow;', N'\Package.Connections[ssis_config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'dbProcessInfo', N'Initial Catalog=RewardsNow; Data Source=PATTON\RN;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[db].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

