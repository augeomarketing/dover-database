USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'CSI_OPS1_Load_Standard', N'O:\803\OutPut\Audit\Transaction_Errors.txt', N'\Package.Connections[Transaction_Errors.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'O:\803\Input\DD0V580721EX ERROR.txt', N'\Package.Connections[CSI_Input_Error_File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'Data Source=patton\rn;Initial Catalog=CSI_Input_Work;Provider=SQLOLEDB.1;Integrated Security=SSPI;Application Name=SSIS-New_FI_CYTD_Init_Load-{686FCD73-8D8D-4650-A3D9-003381E940A0}patton\rn.CSI_Input_Work;Auto Translate=False;', N'\Package.Connections[CSI_Input_Work].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CSI_OPS1_Load_Standard', N'O:\803\Input\DD0V580721EX.txt', N'\Package.Connections[CSI_Initialization_File].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

