USE RewardsNow
GO

ALTER TABLE RNIBonusProgram
ADD dim_rnibonusprogram_maxawards tinyint not null default(0)
GO

UPDATE RNIBonusProgram SET dim_rnibonusprogram_maxawards = 1
WHERE dim_rnibonusprogram_description NOT IN ('SCU Double Points Promo - Platinum only')
GO

ALTER TABLE RNIBonusProgramParticipant
DROP CONSTRAINT PK_RNIBonusProgramParticipant
GO

ALTER TABLE RNIBonusProgramParticipant
ADD sid_rnibonusprogramparticipant_id BIGINT IDENTITY(1,1) PRIMARY KEY
GO
