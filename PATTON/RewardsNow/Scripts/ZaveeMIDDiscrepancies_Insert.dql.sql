USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[ZaveeMIDDiscrepancies]([MerchantName], [Location Name], [Zavee Merchant Id], [RNI MID], [Processor], [Issue Code Id], [Feed], [Cards], [Status], [Match], [Date], [Notes])
SELECT N'Martin''s Flower Mart', N'Martin''s Flower Mart', N'38021238020', N'03-8021238020   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'08/13/2013', NULL UNION ALL
SELECT N'Ballardvale Cafe', N'Ballardvale Cafe', N'19023654', N'0019023654      ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'08/13/2013', NULL UNION ALL
SELECT N'Facial Beauty Dental', N'Facial Beauty Dental', N'8788520006687', N'008788520006687', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'08/12/2013', NULL UNION ALL
SELECT N'Grecian Table', N'Grecian Table', N'8788290189462', N'008788290189462', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'08/12/2013', NULL UNION ALL
SELECT N'Lowell Mini-Mart', N'Lowell Mini-Mart', N'178023551792', N'17-8023551792   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'08/05/2013', NULL UNION ALL
SELECT N'Legends of Ice', N'Legends of Ice', N'8788520053217', N'008788520053217', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/30/2013', N'I think wrong address in Admin, should be:
4819 Elmhurst Ave.
Royal Oak, MI 48073
' UNION ALL
SELECT N'Stepping Stone Child Development Center', N'Stepping Stone Child Development Center', N'14822261', N'0014822261     ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/24/2013', NULL UNION ALL
SELECT N'Pure Tanning Salon', N'Pure Tanning & Hair Salon', N'8788520053389', N'008788520053389', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/24/2013', NULL UNION ALL
SELECT N'Sunrise Coney Island', N'Sunrise Coney Island', N'270700075525', N'000270700075525', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/24/2013', NULL UNION ALL
SELECT N'The Art of Custom Framing Inc', N'The Art of Custom Framing Inc', N'8788520053218', N'008788520053218', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/23/2013', NULL UNION ALL
SELECT N'Picasso of Pizza', N'Picasso of Pizza', N'16765588', N'0016765588     ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/19/2013', NULL UNION ALL
SELECT N'Trini & Carmen''s', N'Trini & Carmen''s', N'12060326', N'0012060326     ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/19/2013', NULL UNION ALL
SELECT N'NRJ Flowers & Gifts Shop', N'NRJ Flowers & Gifts Shop', N'19457178', N'0019457178', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Candy Cane Christmas Tree Farm', N'Candy Cane Christmas Tree Farm', N'78018630387', N'07-8018630387', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Natural Way Vitamins & Health Foods', N'Natural Way Vitamins & Health Foods', N'8788520008112', N'008788520008112', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'The Pita Peddler', N'The Pita Peddler', N'78018866759', N'???', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', N'Pending' UNION ALL
SELECT N'Yuppy Puppy Dog Boutique', N'Yuppy Puppy Dog Boutique', N'8788014062358', N'008788014062358', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Studio 515 Hair Salon', N'Studio 515 Hair Salon', N'16721784', N'0016721784     ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Pioneer Reproductions', N'Pioneer Reproductions', N'178015190450', N'17-8015190450   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'North Cromwell Paint', N'North Cromwell Paint', N'8788290285711', N'008788290285711', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Merrimack Valley Pavilion (MVP)', N'Merrimack Valley Pavilion (MVP)', N'8788430144697', N'008788430144697 ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Legends of Ice', N'Legends of Ice', N'8788520053218', N'XXX', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Deactivated', 0, N'07/23/2013', N'Deactivated MID in Admin' UNION ALL
SELECT N'Holland Heating And Cooling', N'Holland Heating And Cooling', N'8788290207168', N'008788290207168', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Davison Bridal', N'Davison Bridal', N'270700076630', N'000270700076630', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Blue Dog Party Shoppe', N'Blue Dog Party Shoppe', N'650000006806981', N'518089350100228', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', N'518089350100228, by address (SMS_TestCardTransactions.xls)' UNION ALL
SELECT N'Curious Book Shop', N'The Archives Book Shop', N'8788290174624', N'008788290174624', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Curious Book Shop', N'Curious Book Shop', N'8788290174622', N'008788290174622', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'The New Daily Bagel', N'The New Daily Bagel', N'8788240010014', N'008788240010014', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Siegel''s Deli', N'Siegel''s Deli', N'273200093790', N'000273200093790', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Chef Noodles BBQ & Corned Beef', N'Chef Noodles BBQ & Corned Beef', N'8788290328248', N'008788290328248', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Greek Islands Coney Restaurant', N'Greek Islands Coney Restaurant', N'8788290311116', N'008788290311116', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Dr. Bruce D Serven PC', N'Dr. Bruce D Serven PC', N'8788290201146', N'008788290201146', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Iversen''s Bakery', N'Iversen''s Bakery', N'272300090425', N'000272300090425', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Julian Brothers Catering', N'Julian Brothers Catering', N'8788520053418', N'008788520053418', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'Farmington Deli', N'Farmington Deli', N'8788290171992', N'008788290171992', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'07/18/2013', NULL UNION ALL
SELECT N'test business', N'test business', N'123456', N'XXX', N'Me', NULL, N'P', N'VISA / MasterCard', N'Active', 0, N'05/30/2013', N'test' UNION ALL
SELECT N'BREW''D AWAKENING COFFEE HAUS', N'BREW''D AWAKENING COFFEE HAUS', N'188418000053360', N'XXX', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Canceled', 0, N'07/29/2013', N'closed forever' UNION ALL
SELECT N'Subway', N'Subway', N'255000651510', N'XXX', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'05/22/2013', N'wont go live' UNION ALL
SELECT N'Lowell Auto Service', N'Lowell Auto Service', N'19434997', N'0019434997', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 0, N'05/22/2013', N'19434997001' UNION ALL
SELECT N'USA Subs', N'USA Subs', N'650000003415349', N'650000003415349 ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/13/2013', NULL UNION ALL
SELECT N'Charles Rage Jewelers', N'Charles Rage Jewelers', N'275200047346', N'275200047346    ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/13/2013', NULL UNION ALL
SELECT N'Unique You', N'Unique You', N'422369591000417', N'422369591000417 ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Backyard Coney Island', N'Backyard Coney Island', N'650000005205995', N'650000005205995', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Al Oumara Restaurant', N'Al Oumara Restaurant', N'542929804779064', N'542929804779064', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'United Vacuum', N'United Vacuum -Orchard Lake', N'650000000513476', N'650000000513476', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Collectible Treasures', N'Collectible Treasures', N'541469401196650', N'541469401196650', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Fraser Center of Natural Therapies', N'Fraser Center of Natural Therapies', N'267575975888', N'267575975888   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Fraser Flowers & Gifts', N'Fraser Flowers & Gifts', N'318320010043110', N'318320010043110', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'United Vacuum', N'United Vacuum -Waterford', N'650000000513484', N'650000000513484', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Gest Omelettes', N'Gest Omelettes', N'428209717886', N'428209717886   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL
COMMIT;
RAISERROR (N'[dbo].[ZaveeMIDDiscrepancies]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[ZaveeMIDDiscrepancies]([MerchantName], [Location Name], [Zavee Merchant Id], [RNI MID], [Processor], [Issue Code Id], [Feed], [Cards], [Status], [Match], [Date], [Notes])
SELECT N'Maaco-2239', N'Maaco-2239', N'498219161888', N'498219161888   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Anytime Fitness', N'Anytime Fitness - Davison', N'367320501888', N'367320501888   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Sahara Restaurant', N'Sahara Restaurant', N'434600394887', N'434600394887   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/12/2013', NULL UNION ALL
SELECT N'Biggby Coffee - Waterford', N'Biggby Coffee - Waterford', N'376242465999', N'376242465999   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/05/2013', NULL UNION ALL
SELECT N'Shernni''s Candies', N'Shernni''s Candies - Washington Location', N'524771200066135', N'524771200066135', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/05/2013', NULL UNION ALL
SELECT N'M&K Fashions', N'M&K Fashions', N'510165400108494', N'510165400108494', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/05/2013', NULL UNION ALL
SELECT N'Ninja Japanese Restaurant', N'Ninja Japanese Restaurant', N'530960180102022', N'530960180102022', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/05/2013', NULL UNION ALL
SELECT N'The Bagel Factory', N'The Bagel Factory', N'422899690002994', N'422899690002994', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/05/2013', NULL UNION ALL
SELECT N'Yogurtown', N'Yogurtown', N'434601258883', N'434601258883   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'08/05/2013', NULL UNION ALL
SELECT N'Relaxing Waters Spa', N'Relaxing Waters Spa', N'201001000776278', N'201001000776278', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Aim Academics Tutoring/Test Prep', N'Aim Academics Tutoring/Test Prep', N'524771000172582', N'524771000172582', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Ray''s Transmission', N'Ray''s Transmission', N'611000000080690', N'611000000080690', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Northside Deli', N'Northside Deli', N'267369946889', N'267369946889   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Dave & Amy''s', N'Dave & Amy''s', N'899000002166625', N'899000002166625', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Dr. Gregory J. Gossick, DDS', N'Dr. Gregory J. Gossick, DDS', N'180001216043', N'180001216043   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Advanced Auto Center', N'Advanced Auto Center', N'498226346886', N'498226346886   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/24/2013', NULL UNION ALL
SELECT N'Downtown Smoking Club', N'Downtown Smoking Club', N'130434453013881', N'130434453013881', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/22/2013', NULL UNION ALL
SELECT N'The Venue Bar and Grille', N'The Venue Grille', N'550654901246153', N'550654901246153', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/22/2013', NULL UNION ALL
SELECT N'Flower Stop', N'Flower Stop', N'611000000053479', N'611000000053479', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/22/2013', NULL UNION ALL
SELECT N'Health Nut Vitamin', N'Health Nut Vitamin', N'542929805082591', N'542929805082591', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/19/2013', NULL UNION ALL
SELECT N'Shernni''s Candies', N'Shernni''s Candies - Utica Location', N'524771200066208', N'524771200066208', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/19/2013', NULL UNION ALL
SELECT N'Bai Mai Thai', N'Bai Mai Thai', N'650000003986083', N'650000003986083', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/19/2013', NULL UNION ALL
SELECT N'Baby B''s Restaurant', N'Baby B''s Restaurant', N'422369630036745', N'422369630036745', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Vineyards Cafe & Catering', N'Vineyards Cafe & Catering', N'543469080102890', N'543469080102890', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Shanks'' Mare Thrift Shop', N'Shanks'' Mare Thrift Shop', N'611000000028791', N'611000000028791 ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Rudys Waffle & Pancake House', N'Rudys Waffle & Pancake House', N'535354740100961', N'535354740100961', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Pierson Pet Hospital', N'Pierson Pet Hospital', N'543138750100364', N'543138750100364', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Our Salon', N'Our Salon', N'650000005666501', N'650000005666501', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Jan''s Professional Dry Cleaners', N'Jan''s Professional Dry Cleaners', N'277236506998', N'277236506998   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Hair Trendz Salon', N'Hair Trendz Salon', N'275000671691', N'275000671691', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Hair & More', N'Hair & More', N'267541772880', N'267541772880   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Copper Mug Batting Cages', N'Copper Mug Batting Cages', N'533395701160322', N'533395701160322', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Brough Carpet and Mattress', N'Brough Carpet and Mattress -Lapeer', N'610014001056408', N'610014001056408', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Brough Carpet and Mattress', N'Brough Carpet and Mattress -Flint', N'610014001083683', N'610014001083683', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Ben''s Repair', N'Ben''s Repair', N'555733501279083', N'555733501279083', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Apollo Lanes', N'Apollo Lanes', N'982160871884', N'982160871884   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Accent on Art', N'Accent on Art', N'510165310129135', N'510165310129135', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Action Board Sports', N'Action Board Sports', N'524771200023329', N'524771200023329', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Spotted Dog Cafe', N'Spotted Dog Cafe', N'277233476997', N'277233476997   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Insty-Prints Downtown', N'Insty-Prints Downtown', N'130444473317526', N'130444473317526', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Great Lakes Coney Island', N'Great Lakes Coney Island', N'267575478883', N'267575478883   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Moon Light Mediterranean Cuisine', N'Moon Light Mediterranean Cuisine', N'542929805052016', N'542929805052016', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Arta''s Ham and Corned Beef Restaurant', N'Arta''s Ham and Corned Beef Restaurant', N'267572516883', N'267572516883   ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'RC Transmissions & Gear', N'RC Transmissions & Gear', N'517927100100114', N'517927100100114', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Thai On Main', N'Thai On Main', N'542929804584027', N'542929804584027', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Pasta e Pasta', N'Pasta e Pasta', N'4445108547646', N'4445108547646  ', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Marvin''s Marvelous Mechanical Museum', N'Marvin''s Marvelous Mechanical Museum', N'498894100020361', N'498894100020361', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Ever Ready Carpet Cleaning', N'Ever Ready Carpet Cleaning', N'524771000122577', N'524771000122577', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Motor City Battery', N'Motor City Battery', N'542929804394591', N'542929804394591', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Casey''s Sports Pub & Sushi Bar', N'Casey''s Sports Pub & Sushi Bar', N'650000002989484', N'650000002989484', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL
COMMIT;
RAISERROR (N'[dbo].[ZaveeMIDDiscrepancies]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[ZaveeMIDDiscrepancies]([MerchantName], [Location Name], [Zavee Merchant Id], [RNI MID], [Processor], [Issue Code Id], [Feed], [Cards], [Status], [Match], [Date], [Notes])
SELECT N'Jump Station of Michigan', N'Jump Station of Michigan', N'524771001131696', N'524771001131696', N'RNI', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'07/18/2013', NULL UNION ALL
SELECT N'Paul''s Diner', N'Paul''s Diner', N'8018698434', N'8018698434', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', NULL UNION ALL
SELECT N'Charles A Kokinos, DMD', N'Charles A Kokinos, DMD', N'8021659175', N'8021659175', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', N'Pending' UNION ALL
SELECT N'Diana''s Hair Salon and Spa', N'Diana''s Hair Salon and Spa', N'8788510005580', N'8788510005580   ', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Canceled', 1, N'08/07/2013', N'closed' UNION ALL
SELECT N'Espresso Pizza of Lowell', N'Espresso Pizza of Lowell', N'8788430028053', N'8788430028053   ', N'Unkown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', NULL UNION ALL
SELECT N'The Barkery, Inc.', N'The Barkery, Inc.', N'8788430077713', N'8788430077713   ', N'unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Canceled', 1, N'07/18/2013', N'closed' UNION ALL
SELECT N'Jillies Family Restaurant', N'Jillies Family Restaurant', N'422899071100126', N'422899071100126 ', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', NULL UNION ALL
SELECT N'Oscars Pinatas', N'Oscars Pinatas', N'387007347884000', N'387007347884000 ', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', NULL UNION ALL
SELECT N'Market Street Market', N'Market Street Market', N'362338999885', N'362338999885    ', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', NULL UNION ALL
SELECT N'Athenian Corner Restaurant and Lounge', N'Athenian Corner Restaurant and Lounge', N'2849530', N'2849530', N'Unknown', NULL, N'P', N'VISA / MasterCard / American Express / Discover', N'Active', 1, N'05/22/2013', NULL UNION ALL
SELECT NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
COMMIT;
RAISERROR (N'[dbo].[ZaveeMIDDiscrepancies]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

