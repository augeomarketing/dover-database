USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'ZaveeCustomerEnrollment', N'Data Source=RN1;Initial Catalog=reba;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-ZaveeCustomerEnrollment-{8DB4F59F-D5DF-4D4E-B7B8-11D956818B05}RN1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'Data Source=patton\rn;Initial Catalog=reb;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-ZaveeCustomerEnrollment-{9924F5CF-2E79-4E5F-9394-488FA18D3805}patton\rn.REB;Auto Translate=False;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'<none>', N'\Package.Connections[EDO output file].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'', N'\Package.Connections[EDO output file].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'\\patton\ops\EDO\Output\Customer.csv', N'\Package.Connections[EDO output file].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'True', N'\Package.Connections[EDO output file].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'1252', N'\Package.Connections[EDO output file].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'\\patton\ops\EDO\Output\', N'\Package.Variables[User::SourcePath].Properties[Value]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'\\patton\ops\EDO\Output\Customer.csv', N'\Package.Variables[User::FullSourceNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'\\patton\ops\EDO\Output\EDO_Customer_20130806_104935.csv', N'\Package.Variables[User::FullDestinationNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'\\patton\ops\EDO\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'ZaveeCustomerEnrollment', N'\\patton\ops\EDO\Output\Archive\EDO_Customer*.csv', N'\Package.Variables[User::ArchiveNameAndPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

