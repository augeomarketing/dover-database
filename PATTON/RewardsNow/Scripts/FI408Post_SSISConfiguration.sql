USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete FROM [SSIS Configurations] 
WHERE ConfigurationFilter = '408Post'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'408Post', N'Data Source=Patton\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name="SSIS-Post-{eb878f09-0551-4f14-9eae-72545462c257}Data Source=Patton\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integ";', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Post', N'Data Source=Patton\RN;Initial Catalog=408;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Import-{66DA4C7B-E130-4DC6-B61D-80AEF33A541A}Patton\RN.241;', N'\Package.Connections[FI408].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Post', N'Data Source=Patton\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

