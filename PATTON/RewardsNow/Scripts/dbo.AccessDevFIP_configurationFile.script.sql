USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AccessMemberFIP', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{C02B3128-62BB-4B53-9E08-59B90F29EE4E}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MemberFIP-{CF086DBC-E494-42A8-8D33-E2FC78E7791D}patton\rn.225OrlandoFCU;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{EE293457-DFD3-4759-8423-26B80847ED02}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RN.RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Connections[AccessMemberFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'"', N'\Package.Connections[AccessMemberFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Connections[AccessMemberFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'1', N'\Package.Connections[AccessMemberFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AccessMemberFIP', N'AccessMemberFile', N'\Package.Connections[AccessMemberFile].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'1033', N'\Package.Connections[AccessMemberFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'0', N'\Package.Connections[AccessMemberFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'
', N'\Package.Connections[AccessMemberFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'Delimited', N'\Package.Connections[AccessMemberFile].Properties[Format]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'0', N'\Package.Connections[AccessMemberFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Connections[AccessMemberFile].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'0', N'\Package.Connections[AccessMemberFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Connections[AccessMemberFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'True', N'\Package.Connections[AccessMemberFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'1252', N'\Package.Connections[AccessMemberFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20120507-095127-MEMBER.csv', N'\Package.Variables[User::FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'True', N'\Package.Variables[User::FullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'OrlandoFCU', N'\Package.Variables[User::FIWebName].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FIWebName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FIWebName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'User', N'\Package.Variables[User::FIWebName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'FIWebName', N'\Package.Variables[User::FIWebName].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FIWebName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FIWebName].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FIWebName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FIWebName].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileSource].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileSource].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'User', N'\Package.Variables[User::FileSource].Properties[Namespace]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'FileSource', N'\Package.Variables[User::FileSource].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileSource].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FileSource].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FileSource].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'RN-AD-2199', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AccessMemberFIP', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileExtension].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileExtension].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'User', N'\Package.Variables[User::FileExtension].Properties[Namespace]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'FileExtension', N'\Package.Variables[User::FileExtension].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileExtension].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FileExtension].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::FileExtension].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::FileExtension].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MemberExtract-{74369AA7-B7E0-46F8-9155-1FF6B97147F9}patton\rn.225OrlandoFCU;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MemberExtract-{1ABD1FAB-2660-4742-9D69-FEDE6A41186C}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'False', N'\Package.Connections[Flat File Connection Manager].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AccessDevExtract', N'<none>', N'\Package.Connections[Flat File Connection Manager].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'', N'\Package.Connections[Flat File Connection Manager].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'1', N'\Package.Connections[Flat File Connection Manager].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AccessDevExtract', N'Flat File Connection Manager', N'\Package.Connections[Flat File Connection Manager].Properties[Name]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'1033', N'\Package.Connections[Flat File Connection Manager].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AccessDevExtract', N'0', N'\Package.Connections[Flat File Connection Manager].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AccessDevExtract', N'
', N'\Package.Connections[Flat File Connection Manager].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'Delimited', N'\Package.Connections[Flat File Connection Manager].Properties[Format]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'0', N'\Package.Connections[Flat File Connection Manager].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AccessDevExtract', N'', N'\Package.Connections[Flat File Connection Manager].Properties[Description]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'0', N'\Package.Connections[Flat File Connection Manager].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AccessDevExtract', N'\\patton\ops\AccessDevelopment\Output\MEMBERExtract.csv', N'\Package.Connections[Flat File Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'False', N'\Package.Connections[Flat File Connection Manager].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AccessDevExtract', N'1252', N'\Package.Connections[Flat File Connection Manager].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AccessDevExtract', N'\\patton\ops\AccessDevelopment\Output\RN-AD-Extract-05082012-095756.csv', N'\Package.Variables[User::FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'\\patton\ops\AccessDevelopment\Output\MemberExtract.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'\\patton\ops\AccessDevelopment\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'RN-AD-Extract-', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'', N'\Package.Variables[User::FIDatabase].Properties[Expression]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'225OrlandoFCU', N'\Package.Variables[User::DatabaseBeingProcessed].Properties[Value]', N'String' UNION ALL
SELECT N'AccessDevExtract', N'225', N'\Package.Variables[User::ClientID].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO
