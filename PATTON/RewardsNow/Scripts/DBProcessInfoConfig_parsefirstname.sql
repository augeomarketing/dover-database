BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.dbprocessinfo ADD
	ParseFirstName varchar(1) NULL
GO
ALTER TABLE dbo.dbprocessinfo ADD CONSTRAINT
	DF_dbprocessinfo_ParseFirstName DEFAULT 'N' FOR ParseFirstName
GO
ALTER TABLE dbo.dbprocessinfo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT




update rewardsnow.dbo.dbprocessinfo
    set parsefirstname = 'N'



ALTER TABLE dbo.dbprocessinfo ADD
	ParseFirstName varchar(1) NOT NULL CONSTRAINT DF_dbprocessinfo_ParseFirstName DEFAULT 'N'
GO
