USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'MemberFIP_FTP'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'MemberFIP_FTP', N'OrlandoFCU', N'\Package.Variables[User::FIWebName].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{C02B3128-62BB-4B53-9E08-59B90F29EE4E}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Data Source=patton\rn;User ID=;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MemberFIP-{CF086DBC-E494-42A8-8D33-E2FC78E7791D}patton\rn.225OrlandoFCU;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'rewards-now', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'sgm!9tFGK$$9', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerKeyFile]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'172.20.141.175', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'ServerHost=172.20.141.175;ServerPort=22;ServerUser=rewards-now;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{EE293457-DFD3-4759-8423-26B80847ED02}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RN.RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'rewards-now', N'\Package.Connections[FTP Connection Manager].Properties[ServerUserName]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'22', N'\Package.Connections[FTP Connection Manager].Properties[ServerPort]', N'Int16' UNION ALL
SELECT N'MemberFIP_FTP', N'********', N'\Package.Connections[FTP Connection Manager].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'AccessDev', N'\Package.Connections[FTP Connection Manager].Properties[ServerName]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'AccessDev:22', N'\Package.Connections[FTP Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'"', N'\Package.Connections[AccessMemberFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Delimited', N'\Package.Connections[AccessMemberFile].Properties[Format]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Connections[AccessMemberFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Connections[AccessMemberFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20121120-151542-MEMBER.csv.pgp', N'\Package.Variables[User::PGPTarget].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::FilePath]  +   SUBSTRING(@[User::ArchiveCSVFile],1,34)  +  "csv.pgp"', N'\Package.Variables[User::PGPTarget].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::PGPTarget].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::PGPSource].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::PGPSource].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::PGPSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'RN-AD-2199', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::ArchiveSource].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveSource].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::ArchiveSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\Archive\RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::ArchiveFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::ArchivePath]  +  @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::ArchiveFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'to_access', N'\Package.Variables[User::ADPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

