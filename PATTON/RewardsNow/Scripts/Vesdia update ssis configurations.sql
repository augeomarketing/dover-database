USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

DELETE [dbo].[SSIS Configurations] where [ConfigurationFilter] = 'VesdiaEnrollment'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentHeader].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentHeader].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentHeader].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Connections[EnrollmentHeader].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentHeader].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentFile', N'\Package.Connections[EnrollmentFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentDetail].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentDetail].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Connections[EnrollmentDetail].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentDetail].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentDetail].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_05102011153819.csv', N'\Package.Variables[User::FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\EnrollmentFile.csv', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentDetail].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Variables[User::FileTrailer].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentDetail].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentDetail].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentDetail', N'\Package.Connections[EnrollmentDetail].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentDetail].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Variables[User::FileHeader].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Variables[User::FileDetail].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_10212010.csv', N'\Package.Variables[User::FileDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DatedFiles', N'\Package.Variables[User::DestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentDetail].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentDetail].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentHeader].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentDetail].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{32E3CF38-FFD3-4669-852C-29340423D7F5}RN1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{F23654BF-1292-4902-9EEE-09A78754E27C}patton\rn.236;Auto Translate=False;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentTrailer].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentTrailer].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentTrailer].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentTrailer].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentTrailer', N'\Package.Connections[EnrollmentTrailer].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentTrailer].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentTrailer].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentTrailer].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentTrailer].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentTrailer].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentTrailer].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentTrailer].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Connections[EnrollmentTrailer].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentTrailer].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentTrailer].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'False', N'\Package.Connections[EnrollmentHeader].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentHeader].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[EnrollmentHeader].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[EnrollmentHeader].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'EnrollmentHeader', N'\Package.Connections[EnrollmentHeader].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1033', N'\Package.Connections[EnrollmentHeader].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[EnrollmentHeader].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'
', N'\Package.Connections[EnrollmentHeader].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Delimited', N'\Package.Connections[EnrollmentHeader].Properties[Format]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

