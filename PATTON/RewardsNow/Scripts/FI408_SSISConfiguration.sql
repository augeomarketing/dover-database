USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
Delete FROM [SSIS Configurations] 
WHERE ConfigurationFilter =  '408Import'

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'408Import', N'<none>', N'\Package.Connections[TransactionFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'408Import', N'\\Patton\ops\408\Input\transaction.txt', N'\Package.Connections[TransactionFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Import', N'Data Source=Patton\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Import', N'Data Source=Patton\RN;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Import', N'<none>', N'\Package.Connections[Monthly408Audit].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'408Import', N'\\Patton\ops\408\Output\ClientFiles\M408_Audit.csv', N'\Package.Connections[Monthly408Audit].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Import', N'Data Source=Patton\RN;Initial Catalog=408;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Import-{66DA4C7B-E130-4DC6-B61D-80AEF33A541A}Patton\RN.241;', N'\Package.Connections[FI408].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'408Import', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'408Import', N'\\Patton\ops\408\Input\enrollment.txt', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' 
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

