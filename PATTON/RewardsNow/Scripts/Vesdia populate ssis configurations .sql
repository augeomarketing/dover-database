USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO


delete [dbo].[SSIS Configurations]
where ConfigurationFilter = 'VesdiaEnrollment'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollment', N'Initial Catalog=RewardsNow; Data Source=patton\RN;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentTrailer].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentHeader].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{32E3CF38-FFD3-4669-852C-29340423D7F5}RN1.RewardsNOW;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FE280A7A-C688-4E39-8794-47B16E01EF80}patton\rn.236;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=225OrlandoFCU;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{F23654BF-1292-4902-9EEE-09A78754E27C}patton\rn.236;Auto Translate=False;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

