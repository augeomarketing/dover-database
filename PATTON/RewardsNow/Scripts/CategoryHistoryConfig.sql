USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_CategoryHistory', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-CategoryHistory-{43009E35-72A3-4833-8BFA-1A6B7AB94523}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

