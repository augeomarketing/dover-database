

declare @sql		    nvarchar(max) = ''
declare @columnnames    varchar(max) = ''

declare @ctr		    int = 1
declare @maxcardsontip  int

set nocount on

-- Get max # cards against a tip#
set @maxcardsontip =    (select top 1  count(distinct acctid)
				     from dbo.affiliat
				     group by tipnumber
					having count(*) > 1
				     order by count(*) desc)

if @maxcardsontip is not null
BEGIN

    -- Build string of column names for the pivot for clause
    -- string will look something like:
    -- '[Card_1], [Card_2],....'
    --
    while @ctr <= @maxcardsontip
    BEGIN
	   set @columnnames = @columnnames + '[Card_' + cast(@ctr as varchar) + ']' + ', '
	   set @ctr += 1
    END

    -- Now get rid of trailing ', ' from the end of the string
    set @columnnames = left(@columnnames, datalength(@columnnames)-2)

    print @columnnames

    -- Now do the select, pivoting the results
    set @sql = 'select *
			 from (select tipnumber, acctid, ''Card_'' + cast(row_number()  over(partition by tipnumber order by tipnumber) as varchar) rownbr 
				  from dbo.affiliat
				  group by tipnumber, acctid) aff
			 pivot
			 ( max(acctid)
				for rownbr in (<columnnames>)
			 ) as pvttable'

    -- Now replace the '<columnnames>' with the @columnnames string built in the while loop
    set @sql = replace(@sql, '<columnnames>', @columnnames)

    print @sql

    exec sp_executesql @sql

END
