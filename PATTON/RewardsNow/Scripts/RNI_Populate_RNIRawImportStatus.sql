﻿USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIRawImportStatus] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIRawImportStatus]([sid_rnirawimportstatus_id], [dim_rnirawimportstatus_name], [dim_rnirawimportstatus_desc], [dim_rnirawimportstatus_dateadded], [dim_rnirawimportstatus_lastmodified], [dim_rnirawimportstatus_lastmodifiedby])
SELECT 0, N'NEW', N'Newly imported data', '20110727 09:47:11.217', '20110727 09:47:11.220', N'REWARDSNOW\cheit' UNION ALL
SELECT 1, N'IMPORTED', N'Data that has been imported', '20110727 09:47:11.217', '20110727 09:47:11.220', N'REWARDSNOW\cheit' UNION ALL
SELECT 2, N'DNI', N'Data that is not to be imported based on business rules', '20110727 09:47:11.217', '20110727 09:47:11.220', N'REWARDSNOW\cheit' UNION ALL
SELECT 3, N'ARCHIVE', N'Data marked for archival', '20110727 09:47:11.217', '20110727 09:47:11.220', N'REWARDSNOW\cheit'
COMMIT;
RAISERROR (N'[dbo].[RNIRawImportStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIRawImportStatus] OFF;

