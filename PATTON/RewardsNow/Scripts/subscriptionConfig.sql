USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter =   'AD_SubscriptionStage'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\AD-RN-2199-20120926-065146-SUBSCRIPTION.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'AD-RN-2199-20120926-065146-SUBSCRIPTION.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\Subscription.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'@[User::FilePath]  +  "Subscription.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Subscription\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Source]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OperationName]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionStage', N'3', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'File System Task - Rename for connection mgr', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'2092', N'\Package.Variables[User::ActiveYear].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveYear].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveYear].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::ActiveYear].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'ActiveYear', N'\Package.Variables[User::ActiveYear].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveYear].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N' SUBSTRING(@[User::FileNameAndPath],52,4)', N'\Package.Variables[User::ActiveYear].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::ActiveYear].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::ActiveYear].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'09', N'\Package.Variables[User::ActiveMonth].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveMonth].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveMonth].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::ActiveMonth].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N' SUBSTRING(@[User::FileNameAndPath],53,2)', N'\Package.Variables[User::ActiveMonth].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::ActiveMonth].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::ActiveMonth].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'26', N'\Package.Variables[User::ActiveDay].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveDay].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveDay].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::ActiveDay].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'ActiveDay', N'\Package.Variables[User::ActiveDay].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveDay].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N' SUBSTRING(@[User::FileNameAndPath],55,2)', N'\Package.Variables[User::ActiveDay].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::ActiveDay].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::ActiveDay].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'20120926', N'\Package.Variables[User::ActiveDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::ActiveDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'ActiveDate', N'\Package.Variables[User::ActiveDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N' SUBSTRING(@[User::FileNameAndPath],49,8)', N'\Package.Variables[User::ActiveDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::ActiveDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::ActiveDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionValue]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::CSVFile', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Connections[SubscriptionFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'"', N'\Package.Connections[SubscriptionFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Connections[SubscriptionFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package.Connections[SubscriptionFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'SubscriptionFile', N'\Package.Connections[SubscriptionFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1033', N'\Package.Connections[SubscriptionFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package.Connections[SubscriptionFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'
', N'\Package.Connections[SubscriptionFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'Delimited', N'\Package.Connections[SubscriptionFile].Properties[Format]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package.Connections[SubscriptionFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Connections[SubscriptionFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package.Connections[SubscriptionFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\Subscription.csv', N'\Package.Connections[SubscriptionFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Connections[SubscriptionFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1252', N'\Package.Connections[SubscriptionFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package.Connections[Subscription.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'Subscription.csv', N'\Package.Connections[Subscription.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package.Connections[Subscription.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Connections[Subscription.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\Subscription.csv', N'\Package.Connections[Subscription.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', NULL, N'\Package.Connections[RewardsNow].Properties[UserName]', N'DBNull' UNION ALL
SELECT N'AD_SubscriptionStage', NULL, N'\Package.Connections[RewardsNow].Properties[ServerName]', N'DBNull' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Connections[RewardsNow].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package.Connections[RewardsNow].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'********', N'\Package.Connections[RewardsNow].Properties[Password]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'RewardsNow', N'\Package.Connections[RewardsNow].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'RewardsNow', N'\Package.Connections[RewardsNow].Properties[InitialCatalog]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Connections[RewardsNow].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'201314', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'AD-RN-2199*Subscription.csv.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'ActiveMonth', N'\Package.Variables[User::ActiveMonth].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::ActiveMonth].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'\\patton\ops\AccessDevelopment\Input\AD-RN-2199-20120926-065146-SUBSCRIPTION.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'AD-RN-2199-20120926-065146-SUBSCRIPTION.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'AD-RN-2199*Subscription.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'OpenPGP Task  - Decrypt Subscription File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_SubscriptionStage', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_SubscriptionStage', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_SubscriptionStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Subscription File.Properties[IsKeyRingVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

