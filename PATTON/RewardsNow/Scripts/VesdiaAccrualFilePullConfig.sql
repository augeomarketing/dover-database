USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'VesdiaAccrualFilePull'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'Data Source=patton\rn;User ID=;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaFilePull-{F1D8BA63-E3A0-40B3-A850-93F02533B0D5}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'0', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Action]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'rewardsnow', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'Yi<cj9rj', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'sftp1.vesdia.com', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'ServerHost=sftp1.vesdia.com;ServerPort=22;ServerUser=rewardsnow;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'\\patton\ops\Vesdia\Input', N'\Package.Connections[Input].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]     + "_"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'01022012', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'01012012', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'SUBSTRING( @[User::NextFileDateToPull]  ,5,4)  +  "-" +   SUBSTRING( @[User::NextFileDateToPull]  ,1,2)  +  "-"  + SUBSTRING( @[User::NextFileDateToPull]  ,3,2) ', N'\Package.Variables[User::FormattedNextFileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package.Variables[User::FormattedNextFileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'text.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'Ves_RN_Accrual', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'@[User::LocalPath]  +  "Accrual.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'\\patton\ops\Vesdia\Input\Archive\Accrual\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Target]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'User::LocalPathAndName', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Source]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[SecretKeyRing]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualFilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Result]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Password]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'OpenPGP Task - decrypt Accrual File', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'0', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[Key]', N'String' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Accrual File.Properties[IsSecretKeyRingVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

