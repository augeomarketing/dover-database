USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaCardHashFix', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaCardHashFix-{0C7252BC-55BA-4E97-926E-2DB2420C15F0}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaCardHashFix', N'True', N'\Package.Connections[Excel Source].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'VesdiaCardHashFix', N'False', N'\Package.Connections[Excel Source].Properties[Recalculate]', N'Boolean' UNION ALL
SELECT N'VesdiaCardHashFix', N'0', N'\Package.Connections[Excel Source].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaCardHashFix', NULL, N'\Package.Connections[Excel Source].Properties[OpenPassword]', N'DBNull' UNION ALL
SELECT N'VesdiaCardHashFix', N'Excel Source', N'\Package.Connections[Excel Source].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaCardHashFix', NULL, N'\Package.Connections[Excel Source].Properties[ModifyPassword]', N'DBNull' UNION ALL
SELECT N'VesdiaCardHashFix', N'0', N'\Package.Connections[Excel Source].Properties[FormatType]', N'Int32' UNION ALL
SELECT N'VesdiaCardHashFix', N'O:\Vesdia\Input\RewardsNow_memberData.xlsx', N'\Package.Connections[Excel Source].Properties[ExcelFilePath]', N'String' UNION ALL
SELECT N'VesdiaCardHashFix', N'', N'\Package.Connections[Excel Source].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaCardHashFix', N'ExcelFilePath=O:\Vesdia\Input\RewardsNow_memberData.xlsx;FormatType=Auto;Recalculate=False;RetainSameConnection=True;', N'\Package.Connections[Excel Source].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

