USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'EDO_CreateTransaction', N'<none>', N'\Package.Connections[TransactionFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'', N'\Package.Connections[TransactionFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'Delimited', N'\Package.Connections[TransactionFile].Properties[Format]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\Transaction.csv', N'\Package.Connections[TransactionFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\Archive\EDO_Transaction_20130808.csv', N'\Package.Variables[User::ArchiveFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'1252', N'\Package.Connections[TransactionFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'EDO_CreateTransaction', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-EDO_CreateTransaction-{769DA684-8ECC-48AC-8196-026AA580DC96}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\itops\Encryption Keys\KeyRing\pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\Transaction.csv', N'\Package.Variables[User::SourceFile].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\EDO_Transaction_20130808_140742.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateTransaction', N'\\patton\ops\EDO\Output\Archive\', N'\Package.Variables[User::ArchiveFilePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

