USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[mappingsourcetype] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[mappingsourcetype]([sid_mappingsourcetype_id], [dim_mappingsourcetype_description], [dim_mappingsourcetype_active], [dim_mappingsourcetype_created], [dim_mappingsourcetype_lastmodified])
SELECT 1, N'Column', 1, '20101229 16:46:01.533', NULL UNION ALL
SELECT 2, N'User Defined Scalar Function', 1, '20101229 16:46:16.443', NULL UNION ALL
SELECT 3, N'User Defined Table Function', 1, '20101229 16:46:23.713', NULL UNION ALL
SELECT 4, N'Stored Procedure', 1, '20101229 16:46:29.840', NULL
COMMIT;
RAISERROR (N'[dbo].[mappingsourcetype]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[mappingsourcetype] OFF;

