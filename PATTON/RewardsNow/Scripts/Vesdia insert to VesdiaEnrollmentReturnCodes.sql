USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[VesdiaEnrollmentReturnCodes]([sid_ VesdiaEnrollmentReturnCodes_Code], [dim_ VesdiaEnrollmentReturnCodes ShortDesc], [dim_ VesdiaEnrollmentReturnCodes DetailDescr])
SELECT N'01', N'On An Activation Request', N'This card number was active for another member. Vesdia could not add card.' UNION ALL
SELECT N'02', N'On An Activation Request', N'This card is already Active for the same member. Vesdia did not add this card.' UNION ALL
SELECT N'03', N'On a Deactivation Request', N'The member is not found in the system.' UNION ALL
SELECT N'04', N'On a Deactivation Request', N'The card is active on a different member account. Vesdia did not deactivate this card.' UNION ALL
SELECT N'05', N'On a Deactivation Request', N'The card is already inactive for this member.' UNION ALL
SELECT N'06', N'On a Deactivation Request', N'The card does not exist in the Vesdia system.' UNION ALL
SELECT N'07', N'On Activation/Deactivation', N'A required field is missing.'
COMMIT;
RAISERROR (N'[dbo].[VesdiaEnrollmentReturnCodes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

