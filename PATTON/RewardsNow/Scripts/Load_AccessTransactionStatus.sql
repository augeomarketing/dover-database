USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[AccessTransactionStatus]([sid_AccessTransactionStatus_Code], [dim_AccessTransactionCodes_Description])
SELECT N'DTX', N'Disqualified transaction' UNION ALL
SELECT N'QTX', N'Qualified transaction' UNION ALL
SELECT N'RTX', N'Return transaction' UNION ALL
SELECT N'TTX', N'Test transaction'
COMMIT;
RAISERROR (N'[dbo].[AccessTransactionStatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

