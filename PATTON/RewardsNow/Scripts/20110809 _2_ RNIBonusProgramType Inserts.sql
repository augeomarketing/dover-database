USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusProgramType]([sid_rnibonusprogramtype_code], [dim_rnibonusprogramtype_description], [dim_rnibonusprogramtype_dateadded], [dim_rnibonusprogramtype_lastupdated])
SELECT N'C', N'Calculated Bonus Point Award - Double Points', '20110804 18:08:10.920', '20110804 18:08:10.920' UNION ALL
SELECT N'F', N'Fixed Bonus Point Award', '20110804 18:08:28.243', '20110804 18:08:28.243'
COMMIT;
RAISERROR (N'[dbo].[RNIBonusProgramType]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

