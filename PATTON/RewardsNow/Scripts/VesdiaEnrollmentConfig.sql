USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'VesdiaEnrollment'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentTrailer].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Connections[EnrollmentTrailer].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentTrailer].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentHeader].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Connections[EnrollmentHeader].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Connections[EnrollmentDetail].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_3trailer.csv', N'\Package.Variables[User::FileTrailer].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_1header.csv', N'\Package.Variables[User::FileHeader].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{FADE2A04-FD84-4474-A1EB-007C6BB4107F}rn1.RewardsNOW;', N'\Package.Connections[Web_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'VesdiaEnrollment_child.dtsx', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'0', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollment', N'', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DestinationFile.csv', N'\Package.Variables[User::VesdiaDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=RN1;Initial Catalog=REBA;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{D74A2769-49A4-4C00-9D0C-4A71F9D1F44B}RN1.COOP;', N'\Package.Connections[web_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\SSIS_Packages\VesdiaEnrollment\VesdiaEnrollment\VesdiaEnrollment\VesdiaEnrollment _child.dtsx', N'\Package.Connections[VesdiaEnrollment_child.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=REB;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{6899978E-FC72-44EB-90A9-84328B38BBDF}patton\rn.650Genisys;', N'\Package.Connections[rn_all_FI].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaEnrollment-{8E888E54-C9AA-4CC1-BDA9-2A727B4C2B3C}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'Data Source=patton\rn;Initial Catalog=REB;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaEnrollment-{DAC75C8C-7FA2-4018-9B16-AA8960603EA3}patton\rn.650Genisys;', N'\Package.Connections[FI_for_Deletes].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'1252', N'\Package.Connections[EnrollmentHeader].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'<none>', N'\Package.Connections[EnrollmentDetail].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_06212013094558.csv', N'\Package.Variables[User::FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\EnrollmentFile.csv', N'\Package.Connections[EnrollmentFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\VesdiaEnrollment_2detail.csv', N'\Package.Variables[User::FileDetail].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Ves_RN_Enroll_10212010.csv', N'\Package.Variables[User::FileDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\DatedFiles', N'\Package.Variables[User::DestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollment', N'\\patton\ops\Vesdia\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

