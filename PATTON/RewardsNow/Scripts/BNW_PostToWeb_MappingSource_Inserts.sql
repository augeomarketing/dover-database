USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[mappingsource] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[mappingsource]([sid_mappingsource_id], [dim_mappingsource_description], [sid_mappingtable_id], [sid_mappingsourcetype_id], [dim_mappingsource_source], [dim_mappingsource_active], [dim_mappingsource_created], [dim_mappingsource_lastmodified])
SELECT 96, N'Last Six of Card from Affiliat', 3, 2, N'ufn_mappingextractlast6', 1, '20101230 10:32:01.357', NULL UNION ALL
SELECT 97, N'AcctName1', 4, 1, N'Acctname1', 1, '20101230 14:04:32.910', NULL UNION ALL
SELECT 98, N'AcctName2', 4, 1, N'Acctname2', 1, '20101230 14:04:43.360', NULL UNION ALL
SELECT 99, N'Address1', 4, 1, N'Address1', 1, '20101230 14:04:50.493', NULL UNION ALL
SELECT 100, N'Address2', 4, 1, N'Address2', 1, '20101230 14:04:58.543', NULL UNION ALL
SELECT 101, N'Address3', 4, 1, N'Address3', 1, '20101230 14:05:06.467', NULL UNION ALL
SELECT 102, N'CityStateZip', 4, 1, N'CityStateZip', 1, '20101230 14:05:15.937', NULL UNION ALL
SELECT 103, N'PointsBegin', 4, 1, N'PointsBegin', 1, '20101230 14:05:22.763', NULL UNION ALL
SELECT 104, N'PointsEnd', 4, 1, N'Pointsend', 1, '20101230 14:05:28.217', NULL UNION ALL
SELECT 105, N'PointsPurchasedCR', 4, 1, N'PointsPurchasedCR', 1, '20101230 14:05:39.920', NULL UNION ALL
SELECT 106, N'PointsPurchasedDB', 4, 1, N'PointsPurchasedDB', 1, '20101230 14:05:49.443', NULL UNION ALL
SELECT 107, N'PointsBonus', 4, 1, N'PointsBonus', 1, '20101230 14:05:57.587', NULL UNION ALL
SELECT 108, N'PointsAdded', 4, 1, N'PointsAdded', 1, '20101230 14:06:04.787', NULL UNION ALL
SELECT 109, N'PointsIncreased', 4, 1, N'PointsIncreased', 1, '20101230 14:06:13.777', NULL UNION ALL
SELECT 110, N'Pointsredeemed', 4, 1, N'PointsRedeemed', 1, '20101230 14:06:22.120', NULL UNION ALL
SELECT 111, N'PointsReturnedCR', 4, 1, N'PointsReturnedCR', 1, '20101230 14:06:31.240', NULL UNION ALL
SELECT 112, N'PointsReturnedDB', 4, 1, N'PointsReturnedDB', 1, '20101230 14:06:42.090', NULL UNION ALL
SELECT 113, N'PointsSubtracted', 4, 1, N'PointsSubtracted', 1, '20101230 14:06:51.250', NULL UNION ALL
SELECT 114, N'PointsDecreased', 4, 1, N'PointsDecreased', 1, '20101230 14:06:59.413', NULL UNION ALL
SELECT 115, N'Status', 4, 1, N'Status', 1, '20101230 14:07:24.617', NULL UNION ALL
SELECT 116, N'PointsExpire', 4, 1, N'PointsExpire', 1, '20101230 14:07:37.717', NULL UNION ALL
SELECT 117, N'MISC4 from Customer', 5, 2, N'ufn_mappingextractCustomerMisc4', 1, '20110104 17:14:09.187', NULL UNION ALL
SELECT 118, N'MISC1 from Customer', 5, 2, N'ufn_mappingextractCustomerMisc1', 1, '20110104 17:18:08.507', NULL UNION ALL
SELECT 119, N'Member Number from CUSTID in Affiliat', 3, 2, N'ufn_mappingextractaffiliatcustid', 1, '20110208 16:44:37.720', NULL
COMMIT;
RAISERROR (N'[dbo].[mappingsource]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[mappingsource] OFF;

