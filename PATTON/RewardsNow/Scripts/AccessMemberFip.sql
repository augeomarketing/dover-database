USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AccessMemberFIP', N'Delimited', N'\Package.Connections[AccessMemberFile].Properties[Format]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'0', N'\Package.Connections[AccessMemberFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Connections[AccessMemberFile].Properties[Description]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{C02B3128-62BB-4B53-9E08-59B90F29EE4E}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MemberFIP-{CF086DBC-E494-42A8-8D33-E2FC78E7791D}patton\rn.225OrlandoFCU;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{EE293457-DFD3-4759-8423-26B80847ED02}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RN.RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'False', N'\Package.Connections[AccessMemberFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'"', N'\Package.Connections[AccessMemberFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'', N'\Package.Connections[AccessMemberFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'1', N'\Package.Connections[AccessMemberFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AccessMemberFIP', N'AccessMemberFile', N'\Package.Connections[AccessMemberFile].Properties[Name]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'1033', N'\Package.Connections[AccessMemberFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'0', N'\Package.Connections[AccessMemberFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'
', N'\Package.Connections[AccessMemberFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'True', N'\Package.Connections[AccessMemberFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AccessMemberFIP', N'1252', N'\Package.Connections[AccessMemberFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'0', N'\Package.Connections[AccessMemberFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Connections[AccessMemberFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20130702-094901-MEMBER.csv', N'\Package.Variables[User::FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AccessMemberFIP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

