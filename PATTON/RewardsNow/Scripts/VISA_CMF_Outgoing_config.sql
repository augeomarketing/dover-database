USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete   [SSIS Configurations] where ConfigurationFilter = 'VISA_CMF_Outgoing'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VISA_CMF_Outgoing', N'VCMF_20130321115702.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VISA_CMF_Outgoing-{862E9E23-BE77-454B-AFA7-3631F90B1CEC}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'gauan8812u', N'\Package.Connections[FTPS Connection Manager].Properties[ServerUser]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'21', N'\Package.Connections[FTPS Connection Manager].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'0', N'\Package.Connections[FTPS Connection Manager].Properties[ServerPasswordEncryption]', N'Int32' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'agaudetteFTP01', N'\Package.Connections[FTPS Connection Manager].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'198.241.168.20', N'\Package.Connections[FTPS Connection Manager].Properties[ServerHost]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Logs\VISA_Logs.log', N'\Package.Connections[FTPS Connection Manager].Properties[LogFile]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'gauan8812u', N'\Package.Connections[FTPS Connection Manager].Properties[CertificatePassword]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\gauan8812u.p12', N'\Package.Connections[FTPS Connection Manager].Properties[CertificateFile]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'<none>', N'\Package.Connections[DestinationFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'RaggedRight', N'\Package.Connections[DestinationFile].Properties[Format]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'0', N'\Package.Connections[DestinationFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Output\DestinationFile.csv', N'\Package.Connections[DestinationFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Output\VCMF_20130321115702.csv', N'\Package.Variables[User::SourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'/130003/outbox/vin/prod/mm5', N'\Package.Variables[User::RemotePath].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'', N'\Package.Variables[User::RecordCount].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Output\DestinationFile.csv', N'\Package.Variables[User::GenericFile].Properties[Value]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

