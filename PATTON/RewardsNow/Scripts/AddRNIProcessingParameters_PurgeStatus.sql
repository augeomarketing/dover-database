USE [RewardsNow]
GO

INSERT INTO RNIProcessingParameter (sid_dbprocessinfo_dbnumber, dim_rniprocessingparameter_key, dim_rniprocessingparameter_value, dim_rniprocessingparameter_active)
SELECT 'RNI', 'PURGESTATUS_CLOSED', 'C', 1
UNION SELECT 'RNI', 'PURGESTATUS_ENDOFPROGRAM', 'E', 1
