USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'c:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-RedeemData-{018E554C-5A8A-4860-B997-7CAED2151E8E}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[RedeemDataToZip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'RedeemDataToZip', N'\Package.Connections[RedeemDataToZip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Connections[RedeemDataToZip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[RedeemDataToZip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'O:\AccessDevelopment\Input\RedeemData.zip', N'\Package.Connections[RedeemDataToZip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[RedeemData.zip.pgp].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'RedeemData.zip.pgp', N'\Package.Connections[RedeemData.zip.pgp].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'0', N'\Package.Connections[RedeemData.zip.pgp].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[RedeemData.zip.pgp].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'O:\AccessDevelopment\Input\RedeemData.zip.pgp', N'\Package.Connections[RedeemData.zip.pgp].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[RedeemData.zip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'RedeemData.zip', N'\Package.Connections[RedeemData.zip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[RedeemData.zip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[RedeemData.zip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'O:\AccessDevelopment\Input\RedeemData.zip', N'\Package.Connections[RedeemData.zip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'1', N'\Package.Connections[REDEEM-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'REDEEM-DATA', N'\Package.Connections[REDEEM-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'2', N'\Package.Connections[REDEEM-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_RedeemData', N'', N'\Package.Connections[REDEEM-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_RedeemData', N'O:\AccessDevelopment\Input\REDEEM-DATA', N'\Package.Connections[REDEEM-DATA].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

