USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[processingstep] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[processingstep]([sid_processingstep_id], [dim_processingstep_name], [dim_processingstep_description], [dim_processingstep_created], [dim_processingstep_lastmodified])
SELECT 2, N'Post To Web', N'Global Post To Web', '20110117 18:14:30.970', '20110117 00:00:00.000'
COMMIT;
RAISERROR (N'[dbo].[processingstep]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[processingstep] OFF;

