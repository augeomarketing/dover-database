USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'uploadVesdiaAccrualToWeb', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{0FAD6081-9988-4DFE-8F7C-F20D8BE487F7}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'uploadVesdiaAccrualToWeb', N'Initial Catalog=RewardsNow; Data Source=PATTON\RN;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'uploadVesdiaAccrualToWeb', N'Data Source=PATTON\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{3E31AE50-348D-40B9-B8BC-8326DE0907D4}PATTON\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

