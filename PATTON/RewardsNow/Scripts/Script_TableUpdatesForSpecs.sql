USE [RewardsNow]
GO

--UPDATE CUSTOMER TABLE

ALTER TABLE RNICustomer 
ADD dim_RNICustomer_TransferPortfolio VARCHAR(50) NULL
GO

ALTER TABLE RNICustomer 
ADD dim_RNICustomer_TransferMember VARCHAR(50) NULL
GO

ALTER TABLE RNICustomer 
ADD dim_RNICustomer_TransferPrimaryID VARCHAR(50) NULL
GO

ALTER TABLE RNICustomer 
ADD dim_RNICustomer_TransferCardNumber VARCHAR(16) NULL
GO

--UPDATE TRANSACTION TABLE

ALTER TABLE RNITransaction 
ADD dim_rnitransaction_merchantname VARCHAR(40) 
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantaddress1 VARCHAR(40)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantaddress2 VARCHAR(40)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantaddress3 VARCHAR(40)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantcity VARCHAR(40)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantstateregion VARCHAR(3)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantecountrycode VARCHAR(3)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantpostalcode VARCHAR(20)
GO

ALTER TABLE RNITransaction
ADD dim_rnitransaction_merchantcategorycode VARCHAR(4)
GO


--UPDATE ARCHIVE TABLE

ALTER TABLE RNITransactionArchive 
ADD dim_rnitransaction_merchantname VARCHAR(40) 
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantaddress1 VARCHAR(40)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantaddress2 VARCHAR(40)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantaddress3 VARCHAR(40)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantcity VARCHAR(40)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantstateregion VARCHAR(3)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantecountrycode VARCHAR(3)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantpostalcode VARCHAR(20)
GO

ALTER TABLE RNITransactionArchive
ADD dim_rnitransaction_merchantcategorycode VARCHAR(4)
GO
