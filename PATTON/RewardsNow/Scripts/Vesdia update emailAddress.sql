 use [RewardsNow]
  
  go
     ALTER TABLE [RewardsNow].[dbo].[VesdiaEnrollmentList] ADD  dim_VesdiaEnrollmentList_EmailAddress [VARCHAR] (50) NULL DEFAULT '' WITH VALUES
 
  GO
    update [RewardsNow].[dbo].[VesdiaEnrollmentList]
  set dim_VesdiaEnrollmentList_EmailAddress = det.Email
  from dbo.VesdiaEmailDetail det
  inner  join [RewardsNow].[dbo].[VesdiaEnrollmentList]  lst
  on det.MemberID = lst.sid_VesdiaEnrollmentList_Tip  --2355
   
  