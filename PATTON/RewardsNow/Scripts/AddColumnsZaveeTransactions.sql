
 
 use RewardsNow
  alter table [RewardsNow].[dbo].[ZaveeTransactions] add  dim_ZaveeTransactions_AgentName varchar(32) 

 alter table [RewardsNow].[dbo].[ZaveeTransactions] add  dim_ZaveeTransactions_RNIRebate numeric (16,2) DEFAULT 0.00
 alter table [RewardsNow].[dbo].[ZaveeTransactions] add  dim_ZaveeTransactions_ZaveeRebate numeric (16,2)  DEFAULT 0.00
 alter table [RewardsNow].[dbo].[ZaveeTransactions] add  dim_ZaveeTransactions_ClientRebate numeric (16,2)  DEFAULT 0.00
 
 UPDATE [RewardsNow].[dbo].[ZaveeTransactions]
 SET  dim_ZaveeTransactions_RNIRebate = 0, dim_ZaveeTransactions_ZaveeRebate=0,dim_ZaveeTransactions_ClientRebate=0