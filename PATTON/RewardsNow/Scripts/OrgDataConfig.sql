USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_OrgData', N'1', N'\Package.Connections[OrganizationData.zip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'OrganizationData.zip', N'\Package.Connections[OrganizationData.zip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'1', N'\Package.Connections[OrganizationData.zip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Connections[OrganizationData.zip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\OrganizationData.zip', N'\Package.Connections[OrganizationData.zip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\ORGANIZATION-DATA', N'\Package.Connections[ORGANIZATION-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OrgData', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-OrganizationData-{410FD4B1-62E7-4633-A97E-DE954DC92A46}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'V:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'1', N'\Package.Connections[OrgDataToUNzip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'OrgDataToUNzip', N'\Package.Connections[OrgDataToUNzip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'0', N'\Package.Connections[OrgDataToUNzip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Connections[OrgDataToUNzip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\OrganizationData.zip', N'\Package.Connections[OrgDataToUNzip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'1', N'\Package.Connections[OrganizationData.zip.pgp].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'OrganizationData.zip.pgp', N'\Package.Connections[OrganizationData.zip.pgp].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'0', N'\Package.Connections[OrganizationData.zip.pgp].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Connections[OrganizationData.zip.pgp].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OrgData', N'O:\AccessDevelopment\Input\OrganizationData.zip.pgp', N'\Package.Connections[OrganizationData.zip.pgp].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OrgData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OrgData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_OrgData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OrgData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

