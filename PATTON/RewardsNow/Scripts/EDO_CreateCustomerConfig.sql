USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'EDO_CreateCustomer', N'\\patton\ops\EDO\Output\Archive\', N'\Package.Variables[User::ArchiveFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'\\patton\ops\EDO\Output\Archive\EDO_Customer_20130709.csv', N'\Package.Variables[User::ArchiveFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-Package-{35E29F92-6D27-4050-925C-65E70BC5836B}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'<none>', N'\Package.Connections[OutputFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'\\patton\ops\EDO\Output\OutputFile.csv', N'\Package.Connections[OutputFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'\\patton\ops\EDO\Output\OutputFile.csv', N'\Package.Variables[User::SourceFile].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'\\patton\ops\EDO\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'EDO_CreateCustomer', N'\\patton\ops\EDO\Output\EDO_Customer_20130709.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

