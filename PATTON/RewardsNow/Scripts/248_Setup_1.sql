USE [RewardsNow]
GO


BEGIN TRANSACTION;
RAISERROR (N'[dbo].[TranType]: Insert Batch: 1 of 1.....Start!', 10, 1) WITH NOWAIT;
INSERT INTO TranType(TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
VALUES ('B4', 'Mobile Banking Bonus', 'I', 'A', 1, 1, 5)
	, ('B5', 'Phone Banking Bonus', 'I', 'A', 1, 1, 5)
	, ('B6', 'Loan Interest Bonus', 'I', 'A', 1, 1, 5)
	, ('B7', 'Multiple Product/Service Bonus', 'I', 'A', 1, 1, 5)
	, ('B8', 'Branded Deposit Bonus', 'I', 'A', 1, 1, 5)
	, ('B9', 'Branded Direct Deposit Bonus', 'I', 'A', 1, 1, 5)
	, ('BB', 'Anniversary Bonus', 'I', 'A', 1, 1, 5)
COMMIT
RAISERROR (N'[dbo].[TranType]: Insert Batch: 1 of 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
RAISERROR (N'[dbo].[rniTrantypeXref]: Insert Batch: 1 of 1.....Start!', 10, 1) WITH NOWAIT;
INSERT INTO [dbo].[rniTrantypeXref]([sid_dbprocessinfo_dbnumber], [sid_trantype_trancode], [dim_rnitrantypexref_code01], [dim_rnitrantypexref_code02], [dim_rnitrantypexref_code03], [dim_rnitrantypexref_code04], [dim_rnitrantypexref_code05], [dim_rnitrantypexref_code06], [dim_rnitrantypexref_code07], [dim_rnitrantypexref_code08], [dim_rnitrantypexref_code09], [dim_rnitrantypexref_code10], [dim_rnitrantypexref_factor], [dim_rnitrantypexref_active], [dim_rnitrantypexref_created], [dim_rnitrantypexref_lastmodified], [dim_rnitrantypexref_lastmodifiedby], [dim_rnitrantypexref_fixedpoints], [dim_rnitrantypexref_conversiontype])
SELECT N'248', N'G6', N'10', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 25, 103 UNION ALL
SELECT N'248', N'FC', N'12', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 25, 103 UNION ALL
SELECT N'248', N'B4', N'30', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 25, 103 UNION ALL
SELECT N'248', N'FB', N'31', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 25, 103 UNION ALL
SELECT N'248', N'B5', N'32', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 25, 103 UNION ALL
SELECT N'248', N'BP', N'13', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 50, 103 UNION ALL
SELECT N'248', N'FP', N'28', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 500, 103 UNION ALL
SELECT N'248', N'B7', N'34', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 500, 103 UNION ALL
SELECT N'248', N'FM', N'29', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 1000, 103 UNION ALL
SELECT N'248', N'BB', N'18', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1.000, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 103 UNION ALL
SELECT N'248', N'B8', N'23', N'1', N'9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.040, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203 UNION ALL
SELECT N'248', N'B9', N'24', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.040, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203 UNION ALL
SELECT N'248', N'B6', N'26', N'1', N'%', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.200, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203 UNION ALL
SELECT N'248', N'34', N'21', N'1', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.500, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203 UNION ALL
SELECT N'248', N'64', N'21', N'1', N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.500, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203 UNION ALL
SELECT N'248', N'35', N'22', N'1', N'0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.500, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203 UNION ALL
SELECT N'248', N'65', N'22', N'1', N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.500, 1, '20120525 11:59:02.503', '20120525 11:59:02.503', NULL, 0, 203
COMMIT;
RAISERROR (N'[dbo].[rniTrantypeXref]: Insert Batch: 1 of 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN

DECLARE @loadsourceid BIGINT

INSERT INTO [dbo].[RNICustomerLoadSource]([sid_dbprocessinfo_dbnumber], [dim_rnicustomerloadsource_sourcename], [dim_rnicustomerloadsource_fileload])
SELECT N'248', N'vw_248_ACCT_TARGET_226', 0

SET @loadsourceid = SCOPE_IDENTITY();

INSERT INTO [dbo].[RNICustomerLoadColumn]([sid_rnicustomerloadsource_id], [dim_rnicustomerloadcolumn_sourcecolumn], [dim_rnicustomerloadcolumn_targetcolumn], [dim_rnicustomerloadcolumn_fidbcustcolumn], [dim_rnicustomerloadcolumn_keyflag], [dim_rnicustomerloadcolumn_loadtoaffiliat], [dim_rnicustomerloadcolumn_affiliataccttype], [dim_rnicustomerloadcolumn_primaryorder], [dim_rnicustomerloadcolumn_primarydescending], [dim_rnicustomerloadcolumn_required])
SELECT @loadsourceid, N'ADDRESS1', N'dim_RNICustomer_Address1', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'ADDRESS2', N'dim_RNICustomer_Address2', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'ADDRESS3', N'dim_RNICustomer_Address3', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'BusinessFlag', N'dim_RNICustomer_BusinessFlag', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'CardNumber', N'dim_RNICustomer_CardNumber', NULL, 1, 1, N'DEBIT', 0, 0, 1 UNION ALL
SELECT @loadsourceid, N'City', N'dim_RNICustomer_City', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'CountryCode', N'dim_RNICustomer_CountryCode', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'CustomerCode', N'dim_RNICustomer_CustomerCode', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'CustomerType', N'dim_RNICustomer_CustomerType', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'EmailAddress', N'dim_RNICustomer_EmailAddress', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'EmployeeFlag', N'dim_RNICustomer_EmployeeFlag', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'InstitutionID', N'dim_RNICustomer_InstitutionID', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'MemberNumber', N'dim_RNICustomer_Member', NULL, 1, 1, N'MEMBER', 0, 0, 1 UNION ALL
SELECT @loadsourceid, N'NAME1', N'dim_RNICustomer_Name1', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'NAME2', N'dim_RNICustomer_Name2', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'NAME3', N'dim_RNICustomer_Name3', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'NAME4', N'dim_RNICustomer_Name4', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'PortfolioNumber', N'dim_RNICustomer_Portfolio', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'PostalCode', N'dim_RNICustomer_PostalCode', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'PrimaryIndicator', N'dim_RNICustomer_PrimaryIndicator', NULL, 1, 0, NULL, 1, 0, 0 UNION ALL
SELECT @loadsourceid, N'PrimaryIndicatorId', N'dim_RNICustomer_PrimaryId', NULL, 1, 1, N'PRIMARYID', 0, 0, 1 UNION ALL
SELECT @loadsourceid, N'PrimaryMobilePhone', N'dim_RNICustomer_PriMobilPhone', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'PrimaryPhone', N'dim_RNICustomer_PriPhone', NULL, 0, 0, NULL, 0, 0, 0 UNION ALL
SELECT @loadsourceid, N'StateRegion', N'dim_RNICustomer_StateRegion', NULL, 0, 0, NULL, 0, 0, 0


END

--setup householding

INSERT INTO [dbo].[RNIHouseholding]([sid_dbprocessinfo_dbnumber], [dim_rnihouseholding_precedence], [dim_rnihouseholding_field01], [dim_rnihouseholding_field02], [dim_rnihouseholding_field03], [dim_rnihouseholding_field04])
SELECT N'248', 1, N'dim_RNICustomer_PrimaryID', NULL, NULL, NULL

--setup data definitions
INSERT INTO [dbo].[RNIRawImportDataDefinition]([sid_rnirawimportdatadefinitiontype_id], [sid_rniimportfiletype_id], [sid_dbprocessinfo_dbnumber], [dim_rnirawimportdatadefinition_outputfield], [dim_rnirawimportdatadefinition_outputdatatype], [dim_rnirawimportdatadefinition_sourcefield], [dim_rnirawimportdatadefinition_startindex], [dim_rnirawimportdatadefinition_length], [dim_rnirawimportdatadefinition_multipart], [dim_rnirawimportdatadefinition_multipartpart], [dim_rnirawimportdatadefinition_multipartlength], [dim_rnirawimportdatadefinition_dateadded], [dim_rnirawimportdatadefinition_lastmodified], [dim_rnirawimportdatadefinition_lastmodifiedby], [dim_rnirawimportdatadefinition_version], [dim_rnirawimportdatadefinition_applyfunction])
SELECT 2, 1, N'248', N'Address1', N'VARCHAR', N'FIELD10', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'Address2', N'VARCHAR', N'FIELD11', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'Address3', N'VARCHAR', N'FIELD12', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'BusinessFlag', N'VARCHAR', N'FIELD20', 1, 10, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'CardNumber', N'VARCHAR', N'FIELD23', 1, 16, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'City', N'VARCHAR', N'FIELD13', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'CountryCode', N'VARCHAR', N'FIELD15', 1, 3, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'CustomerCode', N'VARCHAR', N'FIELD19', 1, 10, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'CustomerType', N'VARCHAR', N'FIELD25', 1, 10, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'EmailAddress', N'VARCHAR', N'FIELD24', 1, 254, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.203', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'EmployeeFlag', N'VARCHAR', N'FIELD21', 1, 10, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'InstitutionID', N'VARCHAR', N'FIELD22', 1, 20, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:26.730', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'MemberNumber', N'VARCHAR', N'FIELD02', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.203', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'Name1', N'VARCHAR', N'FIELD06', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'Name2', N'VARCHAR', N'FIELD07', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'Name3', N'VARCHAR', N'FIELD08', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'Name4', N'VARCHAR', N'FIELD09', 1, 40, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'PortfolioNumber', N'VARCHAR', N'FIELD01', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.203', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'PostalCode', N'VARCHAR', N'FIELD16', 1, 20, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:26.730', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'PrimaryIndicator', N'VARCHAR', N'FIELD05', 1, 10, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD03', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.203', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'PrimaryMobilePhone', N'VARCHAR', N'FIELD18', 1, 20, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:26.730', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'PrimaryPhone', N'VARCHAR', N'FIELD17', 1, 20, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:26.730', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'RNICustomerNumber', N'VARCHAR', N'FIELD04', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 1, N'248', N'StateRegion', N'VARCHAR', N'FIELD14', 1, 3, 0, 1, 1, '20120514 21:57:12.753', '20120604 13:53:05.200', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'AuthorizationCode', N'VARCHAR', N'FIELD17', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'CardNumber', N'VARCHAR', N'FIELD06', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'CurrencyCode', N'VARCHAR', N'FIELD14', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'DDANumber', N'VARCHAR', N'FIELD10', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'MemberNumber', N'VARCHAR', N'FIELD02', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'MerchantID', N'VARCHAR', N'FIELD15', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'PortfolioNumber', N'VARCHAR', N'FIELD01', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD03', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'ProcessingCode', N'VARCHAR', N'FIELD05', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'RNICustomerNumber', N'VARCHAR', N'FIELD04', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionActionCode', N'VARCHAR', N'FIELD18', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionAmount', N'VARCHAR', N'FIELD11', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionCode', N'VARCHAR', N'FIELD09', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionCount', N'VARCHAR', N'FIELD12', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionDate', N'VARCHAR', N'FIELD07', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionDescription', N'VARCHAR', N'FIELD13', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransactionIdentifier', N'VARCHAR', N'FIELD16', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 2, N'248', N'TransferCardNumber', N'VARCHAR', N'FIELD08', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 3, N'248', N'CustomerCode', N'VARCHAR', N'FIELD05', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 3, N'248', N'MemberNumber', N'VARCHAR', N'FIELD02', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 3, N'248', N'PortfolioNumber', N'VARCHAR', N'FIELD01', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 3, N'248', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD03', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 3, N'248', N'RNICustomerNumber', N'VARCHAR', N'FIELD04', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 4, N'248', N'CardNumber', N'VARCHAR', N'FIELD06', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 4, N'248', N'MemberNumber', N'VARCHAR', N'FIELD02', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 4, N'248', N'PortfolioNumber', N'VARCHAR', N'FIELD01', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 4, N'248', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD03', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 4, N'248', N'ProcessingCode', N'VARCHAR', N'FIELD05', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 4, N'248', N'RNICustomerNumber', N'VARCHAR', N'FIELD04', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'CardNumber', N'VARCHAR', N'FIELD06', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'MemberNumber', N'VARCHAR', N'FIELD02', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'PortfolioNumber', N'VARCHAR', N'FIELD01', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'PrimaryIndicatorID', N'VARCHAR', N'FIELD03', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'ProcessingCode', N'VARCHAR', N'FIELD05', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'RNICustomerNumber', N'VARCHAR', N'FIELD04', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120514 21:57:12.763', N'REWARDSNOW\cheit', 226, NULL UNION ALL
SELECT 2, 5, N'248', N'TransactionAmount', N'VARCHAR', N'FIELD07', 1, 255, 0, 1, 1, '20120514 21:57:12.753', '20120521 14:21:57.980', N'REWARDSNOW\cheit', 226, NULL


--setup data definition where clause

INSERT INTO [dbo].[RNIRawImportDataDefinitionWhereClause]([sid_rnirawimportdatadefinitiontype_id], [sid_rniimportfiletype_id], [sid_dbprocessinfo_dbnumber], [dim_rnirawimportdatadefinition_version], [dim_rnirawimportdatadefinitionwhereclause_whereclause], [dim_rnirawimportdatadefinitionwhereclause_dateadded], [dim_rnirawimportdatadefinitionwhereclause_lastmodified])
SELECT 2, 1, N'248', 226, N'dim_rnirawimport_field02 not like ''248_ACCT%''', '20120514 22:15:13.410', '20120514 22:15:13.410' UNION ALL
SELECT 2, 2, N'248', 226, N'dim_rnirawimport_field02 not like ''248_TRAN%''', '20120517 13:46:40.903', '20120517 13:46:40.903' UNION ALL
SELECT 2, 5, N'248', 226, N'dim_rnirawimport_field02 not like ''248_%'' and dim_rnirawimport_field02 <> ''''', '20120517 16:03:14.473', '20120517 16:03:14.473'


insert into RNITransactionLoadSource (sid_dbprocessinfo_dbnumber, dim_rnitransactionloadsource_sourcename, dim_rnitransactionloadsource_fileload)
select '248', 'vw_248_TRAN_TARGET_226', 1

