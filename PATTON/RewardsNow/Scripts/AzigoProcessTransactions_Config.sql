USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [SSIS Configurations] where ConfigurationFilter = 'AzigoProcessTransactions'

go



BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AzigoProcessTransactions', N'\\patton\ops\Azigo\Logs\AzigoProcessTransactionsProcessingLog.txt', N'\Package.Connections[AzigoProcessTransactionsProcessingLog.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoProcessTransactions', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AzigoProcessTransactions-{4160B2B7-9B3A-454E-B81E-731664D73393}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[WebRewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoProcessTransactions', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AzigoProcessTransactions-{351D269E-1B64-455C-A226-F6AD746A7636}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

