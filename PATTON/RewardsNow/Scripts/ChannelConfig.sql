USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations]  where ConfigurationFilter like 'AD_ChannelStage%'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'OpenPGP Task  - Decrypt Channel File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Source]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'3', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'File System Task - Rename for connection mgr', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User::CSVFile', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_ChannelStage', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Channel-{8F84622A-DC7A-4A28-9D87-E91119746BCB}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Connections[ChannelFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'AD-RN-2199*Channel.csv.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Channel File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'"', N'\Package.Connections[ChannelFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Connections[ChannelFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package.Connections[ChannelFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'ChannelFile', N'\Package.Connections[ChannelFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1033', N'\Package.Connections[ChannelFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package.Connections[ChannelFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'
', N'\Package.Connections[ChannelFile].Properties[HeaderRowDelimiter]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_ChannelStage', N'Delimited', N'\Package.Connections[ChannelFile].Properties[Format]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package.Connections[ChannelFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Connections[ChannelFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package.Connections[ChannelFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\Channel.csv', N'\Package.Connections[ChannelFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package.Connections[ChannelFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'1252', N'\Package.Connections[ChannelFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package.Connections[Channel.csv.pgp].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'Channel.csv.pgp', N'\Package.Connections[Channel.csv.pgp].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'0', N'\Package.Connections[Channel.csv.pgp].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Connections[Channel.csv.pgp].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\Channel.csv.pgp', N'\Package.Connections[Channel.csv.pgp].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package.Connections[Channel.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'Channel.csv', N'\Package.Connections[Channel.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'1', N'\Package.Connections[Channel.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Connections[Channel.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\Channel.csv', N'\Package.Connections[Channel.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\test.csv.pgp', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'test.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'AD-RN-2199*Channel.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'@[User::FilePath]  +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\Channel.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'@[User::FilePath]  +  "Channel.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Channel\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_ChannelStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_ChannelStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

