DECLARE	@cmd varchar(max)

SELECT	@cmd =	'
	IF ''?'' IN (Select dbnamepatton from rewardsnow.dbo.dbprocessinfo)
		AND ''?'' IN (select name from sys.databases)
		BEGIN
			USE [?]
				IF EXISTS(SELECT name FROM sys.tables WHERE name = ''history'') 
			BEGIN 
				ALTER TABLE history			ALTER COLUMN description varchar(255)
			END;	
			IF EXISTS(SELECT name FROM sys.tables WHERE name = ''historydeleted'') 
			BEGIN 
				ALTER TABLE historydeleted	ALTER COLUMN description varchar(255)
			END;
			IF EXISTS(SELECT name FROM sys.tables WHERE name = ''history_stage'') 
			BEGIN 
				ALTER TABLE history_stage	ALTER COLUMN description varchar(255) 
			END 
		END 
				'

EXEC sp_MSforeachdb @cmd
