USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations]   where ConfigurationFilter = 'VesdiaEnrollResponeFilePull'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'{EEB62672-2254-4126-B3FA-EFE0952F8915}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Ves_RN_Enroll_Resp*.csv.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/Ves_RN_Enroll_Resp_01022012*', N'\Package.Variables[User::VesdiaPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPathAndName', N'\Package.Variables[User::VesdiaPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]     + "_"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPath', N'\Package.Variables[User::VesdiaPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01022012', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv.pgp', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPathAndName', N'\Package.Variables[User::LocalPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPath', N'\Package.Variables[User::LocalPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01012012', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Target]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User::LocalPathAndName', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Source]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Result]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Password]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'OpenPGP Task - decrypt Enrollment Response File', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Key]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsTargetVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2012-01-02', N'\Package.Variables[User::FormattedNextFileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FormattedNextFileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FormattedNextFileDate', N'\Package.Variables[User::FormattedNextFileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'SUBSTRING( @[User::NextFileDateToPull]  ,5,4)  +  "-" +   SUBSTRING( @[User::NextFileDateToPull]  ,1,2)  +  "-"  + SUBSTRING( @[User::NextFileDateToPull]  ,3,2) ', N'\Package.Variables[User::FormattedNextFileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::FormattedNextFileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FormattedNextFileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Ves_RN_Enroll_Resp', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[Action]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/Ves_RN_Enroll_Resp_01022012*', N'\Package.Variables[User::VesdiaPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPathAndName', N'\Package.Variables[User::VesdiaPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]     + "_"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPath', N'\Package.Variables[User::VesdiaPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01022012', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\OpenPGP Task - decrypt Enrollment Response File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv.pgp', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPathAndName', N'\Package.Variables[User::LocalPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPath', N'\Package.Variables[User::LocalPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01012012', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2012-01-02', N'\Package.Variables[User::FormattedNextFileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FormattedNextFileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FormattedNextFileDate', N'\Package.Variables[User::FormattedNextFileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'SUBSTRING( @[User::NextFileDateToPull]  ,5,4)  +  "-" +   SUBSTRING( @[User::NextFileDateToPull]  ,1,2)  +  "-"  + SUBSTRING( @[User::NextFileDateToPull]  ,3,2) ', N'\Package.Variables[User::FormattedNextFileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::FormattedNextFileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FormattedNextFileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Ves_RN_Enroll_Resp', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\EnrollmentResponseFile.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]  +  "EnrollmentResponseFile.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\Archive\Enrollment\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/Ves_RN_Enroll_Resp_01022012*', N'\Package.Variables[User::VesdiaPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPathAndName', N'\Package.Variables[User::VesdiaPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]     + "_"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPath', N'\Package.Variables[User::VesdiaPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01022012', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv.pgp', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPathAndName', N'\Package.Variables[User::LocalPathAndName].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPath', N'\Package.Variables[User::LocalPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01012012', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2012-01-02', N'\Package.Variables[User::FormattedNextFileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FormattedNextFileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FormattedNextFileDate', N'\Package.Variables[User::FormattedNextFileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'SUBSTRING( @[User::NextFileDateToPull]  ,5,4)  +  "-" +   SUBSTRING( @[User::NextFileDateToPull]  ,1,2)  +  "-"  + SUBSTRING( @[User::NextFileDateToPull]  ,3,2) ', N'\Package.Variables[User::FormattedNextFileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::FormattedNextFileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FormattedNextFileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Ves_RN_Enroll_Resp', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\EnrollmentResponseFile.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]  +  "EnrollmentResponseFile.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\Archive\Enrollment\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User::LocalPathAndName', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 8.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/Ves_RN_Enroll_Resp_01022012*', N'\Package.Variables[User::VesdiaPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPathAndName', N'\Package.Variables[User::VesdiaPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]     + "_"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPath', N'\Package.Variables[User::VesdiaPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01022012', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv.pgp', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPathAndName', N'\Package.Variables[User::LocalPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPath].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 9.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPath', N'\Package.Variables[User::LocalPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01012012', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2012-01-02', N'\Package.Variables[User::FormattedNextFileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FormattedNextFileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FormattedNextFileDate', N'\Package.Variables[User::FormattedNextFileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'SUBSTRING( @[User::NextFileDateToPull]  ,5,4)  +  "-" +   SUBSTRING( @[User::NextFileDateToPull]  ,1,2)  +  "-"  + SUBSTRING( @[User::NextFileDateToPull]  ,3,2) ', N'\Package.Variables[User::FormattedNextFileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::FormattedNextFileDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FormattedNextFileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Ves_RN_Enroll_Resp', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 10.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\EnrollmentResponseFile.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]  +  "EnrollmentResponseFile.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\Archive\Enrollment\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 11.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Connections[SSH Connection Manager 1].Properties[TransferBinary]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'rewardsnow', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Yi<cj9rj', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerKeyFile]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'sftp1.vesdia.com', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Connections[SSH Connection Manager 1].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyUser]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyType]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'80', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPort]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPassword]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyHost]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'SSH Connection Manager 1', N'\Package.Connections[SSH Connection Manager 1].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[LogFile]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'ServerHost=sftp1.vesdia.com;ServerPort=22;ServerUser=rewardsnow;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2', N'\Package.Connections[SSH Connection Manager 1].Properties[BackendVersion]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-VesdiaFilePull-{F1D8BA63-E3A0-40B3-A850-93F02533B0D5}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package.Connections[pubring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'0', N'\Package.Connections[pubring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[pubring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'1', N'\Package.Connections[Input].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Input', N'\Package.Connections[Input].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2', N'\Package.Connections[Input].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Connections[Input].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input', N'\Package.Connections[Input].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/Ves_RN_Enroll_Resp_01022012*', N'\Package.Variables[User::VesdiaPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPathAndName', N'\Package.Variables[User::VesdiaPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::VesdiaPath]  +   @[User::FileBeginsWith]     + "_"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::VesdiaPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::VesdiaPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'/OutBound/', N'\Package.Variables[User::VesdiaPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 12.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::VesdiaPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'VesdiaPath', N'\Package.Variables[User::VesdiaPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::VesdiaPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::VesdiaPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01022012', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv.pgp', N'\Package.Variables[User::LocalPathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPathAndName', N'\Package.Variables[User::LocalPathAndName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +   @[User::FileName]', N'\Package.Variables[User::LocalPathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::LocalPathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\', N'\Package.Variables[User::LocalPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LocalPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LocalPath', N'\Package.Variables[User::LocalPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LocalPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LocalPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'01012012', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'2012-01-02', N'\Package.Variables[User::FormattedNextFileDate].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FormattedNextFileDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FormattedNextFileDate', N'\Package.Variables[User::FormattedNextFileDate].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FormattedNextFileDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'SUBSTRING( @[User::NextFileDateToPull]  ,5,4)  +  "-" +   SUBSTRING( @[User::NextFileDateToPull]  ,1,2)  +  "-"  + SUBSTRING( @[User::NextFileDateToPull]  ,3,2) ', N'\Package.Variables[User::FormattedNextFileDate].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::FormattedNextFileDate].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 13.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FormattedNextFileDate].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv.pgp', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'Ves_RN_Enroll_Resp', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\text.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\EnrollmentResponseFile.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]  +  "EnrollmentResponseFile.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\Archive\Enrollment\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 14.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponeFilePull', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]   +     @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'text.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\EnrollmentResponseFile.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'@[User::LocalPath]  +  "EnrollmentResponseFile.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'\\patton\ops\Vesdia\Input\Archive\Enrollment\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponeFilePull', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 15.....Done!', 10, 1) WITH NOWAIT;
GO

