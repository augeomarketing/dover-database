USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_SubscriptionHistory', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_SubscriptionHistory-{150A2BB1-F0B6-4DBA-A544-C0DBFCB91B3B}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

