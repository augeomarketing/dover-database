USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [dbo].[SSIS Configurations] where ConfigurationFilter = 'VesdiaEnrollResponse'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaEnrollResponse', N'Initial Catalog=RewardsNow; Data Source=patton\RN;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'False', N'\Package.Connections[EnrollmentResponseFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponse', N'<none>', N'\Package.Connections[EnrollmentResponseFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'
', N'\Package.Connections[EnrollmentResponseFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'1', N'\Package.Connections[EnrollmentResponseFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponse', N'EnrollmentResponseFile', N'\Package.Connections[EnrollmentResponseFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'1033', N'\Package.Connections[EnrollmentResponseFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponse', N'0', N'\Package.Connections[EnrollmentResponseFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponse', N'
', N'\Package.Connections[EnrollmentResponseFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'RaggedRight', N'\Package.Connections[EnrollmentResponseFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'0', N'\Package.Connections[EnrollmentResponseFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaEnrollResponse', N'', N'\Package.Connections[EnrollmentResponseFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'0', N'\Package.Connections[EnrollmentResponseFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaEnrollResponse', N'\\patton\ops\Vesdia\Input\EnrollmentResponseFile.csv', N'\Package.Connections[EnrollmentResponseFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaEnrollResponse', N'False', N'\Package.Connections[EnrollmentResponseFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaEnrollResponse', N'1252', N'\Package.Connections[EnrollmentResponseFile].Properties[CodePage]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

