USE [USE RewardsNow
GO

INSERT INTO RNIBonusProgram 
(
	dim_rnibonusprogram_description
	, dim_rnibonusprogram_effectivedate
	, dim_rnibonusprogram_expirationdate
	, sid_rnibonusprogramtype_code
	, sid_trantype_tranCode
)
SELECT 'First Usage Bonus', '2011-09-01', '9999-12-31', 'F', 'BF'


SELECT * FROM RNIBonusProgramFI

DECLARE @sid_rnibonusprogram_id INT
SET @sid_rnibonusprogram_id = (SELECT sid_rnibonusprogram_id FROM RNIBonusProgram WHERE dim_rnibonusprogram_description = 'First Usage Bonus')

INSERT INTO RNIBonusProgramFI 
(
	sid_dbprocessinfo_dbnumber
	, sid_rnibonusprogram_id
	, dim_rnibonusprogramfi_effectivedate
	, dim_rnibonusprogramfi_expirationdate
	, dim_rnibonusprogramfi_bonuspoints
	, dim_rnibonusprogramfi_pointmultiplier
	, sid_rnibonusprogramdurationtype_id
	, dim_rnibonusprogramfi_duration
	, dim_rnibonusprogramfi_storedprocedure
)
SELECT
	'241'
	, @sid_rnibonusprogram_id
	, '2011-08-01'
	, '9999-12-31'
	, 1000
	, 1.00
	, 3
	, 0
	, '[RewardsNow].dbo.usp_RNIFirstUseBonusForFI'

GO

GO

