USE [RewardsNow]
GO

--setup mappings for 206 Post to Web

DELETE FROM mappingdefinition WHERE sid_dbprocessinfo_dbnumber IN ('206', '214', '150', '164', '713')

INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 1, 96, 0
FROM
(
	SELECT '206' AS dbnumber
	UNION SELECT '214'
	UNION SELECT '713'
	UNION SELECT '150'
	UNION SELECT '164'
) db


INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT '713', 1, 2, (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatCustIDLast4'), 0

