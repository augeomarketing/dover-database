USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'MemberFIP_FTP'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[IsFilterVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[IncludeSubfolders]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'SSH Connection Manager 1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[FtpConnection]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\PGP_KeyRing\pubring.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Connections[FTP Connection Manager].Properties[UsePassiveMode]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'60', N'\Package.Connections[FTP Connection Manager].Properties[Timeout]', N'Int16' UNION ALL
SELECT N'MemberFIP_FTP', N'rewards-now', N'\Package.Connections[FTP Connection Manager].Properties[ServerUserName]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'22', N'\Package.Connections[FTP Connection Manager].Properties[ServerPort]', N'Int16' UNION ALL
SELECT N'MemberFIP_FTP', N'********', N'\Package.Connections[FTP Connection Manager].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'AccessDev', N'\Package.Connections[FTP Connection Manager].Properties[ServerName]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'5', N'\Package.Connections[FTP Connection Manager].Properties[Retries]', N'Int16' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package.Connections[FTP Connection Manager].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'FTP Connection Manager', N'\Package.Connections[FTP Connection Manager].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[FTP Connection Manager].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'AccessDev:22', N'\Package.Connections[FTP Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package.Connections[FTP Connection Manager].Properties[ChunkSize]', N'Int16' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Connections[AccessMemberFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'"', N'\Package.Connections[AccessMemberFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[AccessMemberFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package.Connections[AccessMemberFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'AccessMemberFile', N'\Package.Connections[AccessMemberFile].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'1033', N'\Package.Connections[AccessMemberFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[AccessMemberFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'
', N'\Package.Connections[AccessMemberFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Delimited', N'\Package.Connections[AccessMemberFile].Properties[Format]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[AccessMemberFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[AccessMemberFile].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[AccessMemberFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Connections[AccessMemberFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Connections[AccessMemberFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'1252', N'\Package.Connections[AccessMemberFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20121120-151542-MEMBER.csv.pgp', N'\Package.Variables[User::PGPTarget].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::PGPTarget].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::PGPTarget].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::PGPTarget].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'PGPTarget', N'\Package.Variables[User::PGPTarget].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::PGPTarget].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::FilePath]  +   SUBSTRING(@[User::ArchiveCSVFile],1,34)  +  "csv.pgp"', N'\Package.Variables[User::PGPTarget].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::PGPTarget].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::PGPTarget].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::PGPSource].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::PGPSource].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::PGPSource].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::PGPSource].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'PGPSource', N'\Package.Variables[User::PGPSource].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::PGPSource].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::PGPSource].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::PGPSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::PGPSource].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::FullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'OrlandoFCU', N'\Package.Variables[User::FIWebName].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FIWebName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FIWebName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::FIWebName].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'FIWebName', N'\Package.Variables[User::FIWebName].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FIWebName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FIWebName].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FIWebName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FIWebName].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\AccessMember.csv', N'\Package.Variables[User::FileSource].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileSource].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileSource].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::FileSource].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'FileSource', N'\Package.Variables[User::FileSource].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileSource].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FileSource].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FileSource].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'RN-AD-2199', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'.csv', N'\Package.Variables[User::FileExtension].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileExtension].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileExtension].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::FileExtension].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'FileExtension', N'\Package.Variables[User::FileExtension].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileExtension].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FileExtension].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::FileExtension].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::FileExtension].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::ArchiveSource].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveSource].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveSource].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::ArchiveSource].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'MemberFIP_FTP', N'ArchiveSource', N'\Package.Variables[User::ArchiveSource].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveSource].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveSource].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::ArchiveSource].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ArchiveSource].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\Archive\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'\\patton\ops\AccessDevelopment\Output\Archive\RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::ArchiveFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::ArchiveFullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'ArchiveFullPath', N'\Package.Variables[User::ArchiveFullPath].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveFullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'@[User::ArchivePath]  +  @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Variables[User::ArchiveFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ArchiveFullPath].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'RN-AD-2199-20121120-151542-MEMBER.csv', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::ArchiveCSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'ArchiveCSVFile', N'\Package.Variables[User::ArchiveCSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ArchiveCSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ArchiveCSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'/to_access/', N'\Package.Variables[User::ADPath].Properties[Value]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ADPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ADPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User', N'\Package.Variables[User::ADPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'ADPath', N'\Package.Variables[User::ADPath].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ADPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ADPath].Properties[Expression]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Variables[User::ADPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Variables[User::ADPath].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'-1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Filter]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[ExtendedRemoteFileInfo]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'SFTP Task', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Action]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'User::PGPTarget', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Target]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'User::PGPSource', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Source]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Result]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'PubKeyRing.gpg', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Password]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'OpenPGP Task - Encrypt csv', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'1033', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'PubKeyRing.gpg', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[KeyRing]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'F8A80FCC', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Key]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'1048576', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'-1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'OpenPGP Task', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\OpenPGP Task - Encrypt csv.Properties[Action]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', NULL, N'\Package.Connections[SSIS CONFIG].Properties[UserName]', N'DBNull' UNION ALL
SELECT N'MemberFIP_FTP', N'Data Source=RN1;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{C02B3128-62BB-4B53-9E08-59B90F29EE4E}RN1.RewardsNOW;Auto Translate=False;', N'\Package.Connections[web.RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'patton\rn', N'\Package.Connections[SSIS CONFIG].Properties[ServerName]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Connections[SSIS CONFIG].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package.Connections[SSIS CONFIG].Properties[ProtectionLevel]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'MemberFIP_FTP', N'********', N'\Package.Connections[SSIS CONFIG].Properties[Password]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'SSIS CONFIG', N'\Package.Connections[SSIS CONFIG].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'RewardsNow', N'\Package.Connections[SSIS CONFIG].Properties[InitialCatalog]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSIS CONFIG].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MemberFIP-{CF086DBC-E494-42A8-8D33-E2FC78E7791D}patton\rn.225OrlandoFCU;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package.Connections[SSH Connection Manager 1].Properties[TransferBinary]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'rewards-now', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'sgm!9tFGK$$9', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerKeyFile]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'172.20.141.175', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'False', N'\Package.Connections[SSH Connection Manager 1].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyUser]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyType]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'80', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPort]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPassword]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyHost]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'SSH Connection Manager 1', N'\Package.Connections[SSH Connection Manager 1].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[LogFile]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'ServerHost=172.20.141.175;ServerPort=22;ServerUser=rewards-now;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'2', N'\Package.Connections[SSH Connection Manager 1].Properties[BackendVersion]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package.Connections[PubKeyRing.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'PubKeyRing.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package.Connections[PubKeyRing.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package.Connections[PubKeyRing.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MemberExtract-{EE293457-DFD3-4759-8423-26B80847ED02}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RN.RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Result]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'/', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[RemotePathSeparator]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'User::ADPath', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Remote]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[NoRemoteFilesFail]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'SFTP Task - ftp to accessDevelopment', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Name]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'MemberFIP_FTP', N'1033', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'User::PGPTarget', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[Local]', N'String' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[IsRemoteWildcard]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[IsRemoteVariable]', N'Boolean' UNION ALL
SELECT N'MemberFIP_FTP', N'1048576', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'MemberFIP_FTP', N'True', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)\SFTP Task - ftp to accessDevelopment.Properties[IsLocalVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

