USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'250SimpleTuition', N'Data Source=PATTON\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-250SimpleTuitionProcessing-{5D8BC687-A77B-408C-9C39-DA3BB1C7720C}PATTON\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS CONFIG].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'Data Source=PATTON\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-250SimpleTuitionProcessing-{0DEF2DF5-E6DA-4127-91EB-B2DE6DF8C933}PATTON\RN.RewardsNow;', N'\Package.Connections[rewardsnow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'250', N'\Package.Variables[User::TipFirst].Properties[Value]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::TipFirst].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::TipFirst].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'User', N'\Package.Variables[User::TipFirst].Properties[Namespace]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'TipFirst', N'\Package.Variables[User::TipFirst].Properties[Name]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::TipFirst].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'', N'\Package.Variables[User::TipFirst].Properties[Expression]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::TipFirst].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'', N'\Package.Variables[User::TipFirst].Properties[Description]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'1/31/2012', N'\Package.Variables[User::ProcessingDate].Properties[Value]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::ProcessingDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::ProcessingDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'User', N'\Package.Variables[User::ProcessingDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'ProcessingDate', N'\Package.Variables[User::ProcessingDate].Properties[Name]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'True', N'\Package.Variables[User::ProcessingDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'', N'\Package.Variables[User::ProcessingDate].Properties[Expression]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::ProcessingDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'', N'\Package.Variables[User::ProcessingDate].Properties[Description]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'250_ACCT_20120709_2.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'250SimpleTuition', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'250SimpleTuition', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

