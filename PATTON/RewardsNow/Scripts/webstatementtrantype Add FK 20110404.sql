/*
   Monday, April 04, 20114:02:06 PM
   User: 
   Server: doolittle\web
   Database: RewardsNOW
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webstatement SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webstatementtrantype ADD CONSTRAINT
	FK_webstatementtrantype_webstatement FOREIGN KEY
	(
	sid_webstatement_id
	) REFERENCES dbo.webstatement
	(
	sid_webstatement_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.webstatementtrantype SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
