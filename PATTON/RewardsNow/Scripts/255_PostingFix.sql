USE [RewardsNow]
GO

INSERT INTO mappingsource (dim_mappingsource_description, sid_mappingtable_id, sid_mappingsourcetype_id, dim_mappingsource_source, dim_mappingsource_active)
VALUES ('ACCTID From Affiliat (AcctType = MEMBER) - Active Only', 3, 2, 'ufn_mappingExtractActiveMember', 1)
GO


BEGIN
	DECLARE @src INTEGER
	SELECT @src = sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingExtractActiveMember'

	UPDATE mappingdefinition 
	SET sid_mappingsource_id = @src
	WHERE sid_dbprocessinfo_dbnumber = '255'
		AND sid_mappingsource_id = 132
END

EXEC usp_AddFIPostToWeb '255', '6/1/2014', '6/30/2014'

INSERT INTO Maintenance.dbo.perlemail (dim_perlemail_subject, dim_perlemail_body, dim_perlemail_to, dim_perlemail_from, dim_perlemail_bcc)
VALUES 
(
	'Heritage Update'
	, 'Update for Heritage Fix is Done'
	, 'cheit@rewardsnow.com'
	, 'cheit@rewardsnow.com'
	, 0
)

