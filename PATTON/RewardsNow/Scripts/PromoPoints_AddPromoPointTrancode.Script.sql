USE [RewardsNow]
GO

-- INSERT NEW TRAN TYPES FOR USE WITH THE LEDGER DETAIL ONLY

INSERT INTO TranType (TranCode, Description, IncDec, CntAmtFxd, Points, Ratio, TypeCode)
VALUES
	('$+', '* Generic Promo Points Ledger Add', 'I', 'A', 1, 1, 0)
	, ('$-', '* Generic Promo Points Ledger Subt', 'D', 'A', 1, -1, 0)
GO