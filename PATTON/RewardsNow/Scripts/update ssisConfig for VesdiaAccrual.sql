USE [RewardsNow];
delete [dbo].[SSIS Configurations] where ConfigurationFilter = 'VesdiaAccruals'
go
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccruals', N'False', N'\Package.Connections[Accrual File].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccruals', N'<none>', N'\Package.Connections[Accrual File].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'', N'\Package.Connections[Accrual File].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'1', N'\Package.Connections[Accrual File].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccruals', N'Accrual File', N'\Package.Connections[Accrual File].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'1033', N'\Package.Connections[Accrual File].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccruals', N'0', N'\Package.Connections[Accrual File].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccruals', N'
', N'\Package.Connections[Accrual File].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'RaggedRight', N'\Package.Connections[Accrual File].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'0', N'\Package.Connections[Accrual File].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccruals', N'', N'\Package.Connections[Accrual File].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'0', N'\Package.Connections[Accrual File].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccruals', N'\\patton\ops\Vesdia\Input\AccrualFile.csv', N'\Package.Connections[Accrual File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccruals', N'False', N'\Package.Connections[Accrual File].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccruals', N'1252', N'\Package.Connections[Accrual File].Properties[CodePage]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

