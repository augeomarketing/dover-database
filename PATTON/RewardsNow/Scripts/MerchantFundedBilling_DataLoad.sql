USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[MerchantFundedBilling] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[MerchantFundedBilling]([sid_MerchantFundedBilling_ID], [dim_MerchantFundedBilling_Agent], [dim_MerchantFundedBilling_AgentDescription], [dim_MerchantFundedBilling_RNIPercentage], [dim_MerchantFundedBilling_ZaveePercentage], [dim_MerchantFundedBilling_ClientPercentage], [dim_MerchantFundedBilling_SponsoringMerchantPercentage], [dim_MerchantFundedBilling_ItsMyMichiganPercentage], [dim_MerchantFundedBilling_DateAdded], [dim_MerchantFundedBilling_DateLastModified])
SELECT 1, N'None', N'No sponsored merchant', 3.00, 3.00, 0.00, 0.00, 0.00, '20130905 15:47:07.143', '20130905 15:47:07.143' UNION ALL
SELECT 2, N'241', N'Enterprise Bank', 2.50, 2.50, 1.00, 0.00, 0.00, '20130905 15:48:35.090', '20130905 15:48:35.090'
COMMIT;
RAISERROR (N'[dbo].[MerchantFundedBilling]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[MerchantFundedBilling] OFF;

