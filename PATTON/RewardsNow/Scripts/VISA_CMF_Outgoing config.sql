USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [SSIS Configurations] where ConfigurationFilter = 'VISA_CMF_Outgoing'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VISA_CMF_Outgoing', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VISA_CMF_Outgoing-{862E9E23-BE77-454B-AFA7-3631F90B1CEC}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'<none>', N'\Package.Connections[DestinationFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'RaggedRight', N'\Package.Connections[DestinationFile].Properties[Format]', N'String' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'0', N'\Package.Connections[DestinationFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VISA_CMF_Outgoing', N'\\patton\ops\VISA\Output\DestinationFile.csv', N'\Package.Connections[DestinationFile].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

