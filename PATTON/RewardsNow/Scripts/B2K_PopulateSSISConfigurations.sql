USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'b2k', N'Data Source=doolittle\rn;Initial Catalog=MetavanteWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;', N'\Package.Variables[User::WorkgroupDBConnectString].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'Data Source=doolittle\rn;Initial Catalog=RewardsNow;Provider=SQLOLEDB.1;Integrated Security=SSPI;', N'\Package.Variables[User::UtilityDBConnectString].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'\\Doolittle\ops\5xx\Input\B2K\util\TransactionLayoutFormat.fmt', N'\Package.Variables[User::TransactionImportFormat].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'\\Doolittle\ops\5xx\Input\B2K\trns00~1.txt.new', N'\Package.Variables[User::TransactionImportFile].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'C:\u_drive\base2000fis', N'\Package.Variables[User::ImportDataDir].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'\\doolittle\ops\5xx\input\b2k', N'\Package.Variables[User::FileProcessingDir].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'\\doolittle\ops\5xx\input\b2k', N'\Package.Variables[User::FileArchiveDir].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'Data Source=doolittle\rn;Initial Catalog=MetavanteWork;Provider=SQLOLEDB.1;Integrated Security=SSPI;', N'\Package.Variables[User::FIDBConnectString].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'10/31/2010 12:00:00 AM', N'\Package.Variables[User::EndDate].Properties[Value]', N'DateTime' UNION ALL
SELECT N'b2k', N'\\Doolittle\ops\5xx\Input\B2K\util\DemographicLayoutFormat.fmt', N'\Package.Variables[User::DemogImportFormat].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'\\Doolittle\ops\5xx\Input\B2K\cmnt00~1.txt.new', N'\Package.Variables[User::DemogImportFile].Properties[Value]', N'String' UNION ALL
SELECT N'b2k', N'True', N'\Package.Variables[User::Debug].Properties[Value]', N'Boolean' UNION ALL
SELECT N'b2k', N'10/1/2010 12:00:00 AM', N'\Package.Variables[User::BeginDate].Properties[Value]', N'DateTime'
COMMIT;
GO

