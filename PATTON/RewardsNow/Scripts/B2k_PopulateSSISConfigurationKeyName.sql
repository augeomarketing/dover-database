USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
	UPDATE SSISConfigurationKeyName SET KeyName = 'UTILITYDBCONNECTSTRING' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::UtilityDBConnectString].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'TRANSACTIONIMPORTFORMAT' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::TransactionImportFormat].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'TRANSACTIONIMPORTFILE' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::TransactionImportFile].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'FIDBCONNECTSTRING' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::FIDBConnectString].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'DEMOGIMPORTFORMAT' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::DemogImportFormat].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'DEMOGIMPORTFILE' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::DemogImportFile].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'DEBUG' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::Debug].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'WORKGROUPDBCONNECTSTRING'WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::WorkgroupDBConnectString].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'ENDDATE' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::EndDate].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'IMPORTDATADIR' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::ImportDataDir].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'FILEPROCESSINGDIR' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::FileProcessingDir].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'FILEARCHIVEDIR' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::FileArchiveDir].Properties[Value]'
	UPDATE SSISConfigurationKeyName SET KeyName = 'BEGINDATE' WHERE ConfigurationFilter = 'b2k' AND PackagePath = '\Package.Variables[User::BeginDate].Properties[Value]'
COMMIT;
GO

