USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProgramFI] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusProgramFI]([sid_rnibonusprogramfi_id], [sid_dbprocessinfo_dbnumber], [sid_rnibonusprogram_id], [dim_rnibonusprogramfi_effectivedate], [dim_rnibonusprogramfi_expirationdate], [dim_rnibonusprogramfi_bonuspoints], [dim_rnibonusprogramfi_pointmultiplier], [sid_rnibonusprogramdurationtype_id], [dim_rnibonusprogramfi_duration], [dim_rnibonusprogramfi_storedprocedure], [dim_rnibonusprogramfi_dateadded], [dim_rnibonusprogramfi_lastupdated], [dim_rnibonusprogramfi_lastupdatedby])
SELECT 3, N'231', 1, '20110101 00:00:00.000', '20110831 00:00:00.000', 0, 1.00, 3, 0, N'[231].dbo.usp_BonusDoublePointsCreditOnly', '20110805 15:11:05.660', '20110808 14:29:28.203', N'REWARDSNOW\pbutler' UNION ALL
SELECT 6, N'237', 1, '20110101 00:00:00.000', '20110831 00:00:00.000', 0, 1.00, 3, 0, N'[237].dbo.usp_BonusDoublePointsCreditOnly', '20110805 15:19:14.750', '20110808 14:29:32.370', N'REWARDSNOW\pbutler' UNION ALL
SELECT 7, N'231', 2, '20110101 00:00:00.000', '99991231 00:00:00.000', 1000, 1.00, 3, 0, N'[231].dbo.usp_BonusEmailStatement', '20110809 13:33:55.553', '20110809 13:34:39.210', N'REWARDSNOW\pbutler'
COMMIT;
RAISERROR (N'[dbo].[RNIBonusProgramFI]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProgramFI] OFF;

