USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'DailyHistoryPushtoWeb', N'\RewardsNow\Scheduled Jobs\Daily\DailyHistoryPushtoWebChild', N'\Package\For each DB to push history\Execute Push SSIS.Properties[PackageName]', N'String' UNION ALL
SELECT N'DailyHistoryPushtoWeb', N'Data Source=236722-sqlclus2\rn;Initial Catalog=msdb;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-DailyHistoryPushtoWeb-{DDB8164D-5E34-4095-BC7A-6FE9FE6EDA39}doolittle\rn.msdb;Auto Translate=False;', N'\Package.Connections[SSIS Package Store].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'DailyHistoryPushtoWeb', N'Data Source=236718-sqlclus\web;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-DailyHistoryPushtoWeb-{2BF82058-D70E-4E58-B0A3-D6982156D855}doolittle\web.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Rewardsnow_Web].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'DailyHistoryPushtoWeb', N'Data Source=236722-sqlclus2\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-DailyHistoryPushtoWeb-{23AE655C-228E-4C2C-9AE7-016198BBD82D}doolittle\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[Rewardsnow_Prod].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'DailyHistoryPushtoWeb', N'C:\SVN\Database\SSIS\RewardsNow_Prod_daily_Export_Hist\RewardsNow_Prod_daily_Export_Hist\DailyHistoryPushtoWebChild.dtsx', N'\Package.Connections[DailyHistoryPushtoWebChild.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'DailyHistoryPushtoWebChild', N'Data Source=236718-sqlclus\web;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-DailyHistoryPushtoWebChild-{8D5FCEDE-35CB-43B6-9C0A-D4F2043CDF35}doolittle\web.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Rewardsnow_Web].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'DailyHistoryPushtoWebChild', N'Data Source=236722-sqlclus2\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-DailyHistoryPushtoWebChild-{8F088300-E1A2-4EBC-8457-536407D2896A}doolittle\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[Rewardsnow_Prod].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

