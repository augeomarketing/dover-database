USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[processingjobstatus] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[processingjobstatus]([sid_processingjobstatus_id], [dim_processingjobstatus_description], [dim_processingjobstatus_created], [dim_processingjobstatus_lastmodified])
SELECT 1, N'In Queue', '20110117 18:21:51.207', '20110117 18:21:51.207' UNION ALL
SELECT 2, N'Processing', '20110117 18:21:55.210', '20110117 18:21:55.210' UNION ALL
SELECT 3, N'Completed successfully', '20110117 18:22:06.570', '20110117 18:22:06.570' UNION ALL
SELECT 4, N'Completed with warnings', '20110117 18:22:11.813', '20110117 18:22:11.813' UNION ALL
SELECT 5, N'FAILED', '20110117 18:22:15.963', '20110117 18:22:15.963'
COMMIT;
RAISERROR (N'[dbo].[processingjobstatus]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[processingjobstatus] OFF;

