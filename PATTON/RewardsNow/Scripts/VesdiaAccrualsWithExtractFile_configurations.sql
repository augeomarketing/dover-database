USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'VesdiaAccrualsWithExtractFileEncryption'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Accrual File].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'RecordCount', N'\Package.Variables[User::RecordCount].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SFPENDINGTRANS_20120618.csv.pgp', N'\Package.Variables[User::PGPFile_CSV].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'NewFileSeqNumber', N'\Package.Variables[User::NewFileSeqNumber].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::NewFileSeqNumber].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::PGPFile_CSV].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::PGPFile_CSV].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::RecordCount].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::RecordCount].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::RecordCount].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::RecordCount].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'{03BD1995-A4A8-46AA-BBE6-A17CF24EFF32}', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'FTPCTL_FullPath', N'\Package.Variables[User::FTPCTL_FullPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Input\AccrualFile.csv', N'\Package.Connections[Accrual File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Connections[Accrual File].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaAccrualsWithExtractFile-{C8C57154-CE9A-4F5A-B6D6-AF6CB8B610E6}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package.Connections[PubKeyRing.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'PubKeyRing.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[PubKeyRing.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[PubKeyRing.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\itops\Encryption Keys\KeyRing\pubring.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP)', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1033', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1048576', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'-1', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\REB_SFPENDINGTRANS_XXXX.csv', N'\Package.Variables[User::SourceFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::SourceFilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'SourceFilePath', N'\Package.Variables[User::SourceFilePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::SourceFilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::SourceFilePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'REB_SFPENDINGTRANS_', N'\Package.Variables[User::SourceFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::SourceFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'SourceFileName', N'\Package.Variables[User::SourceFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::PGPFile_CTL].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'PGPFile_CSV', N'\Package.Variables[User::PGPFile_CSV].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::NewFileSeqNumber].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::LastFileSeqNumber].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FTP_Path]  +   RIGHT(@[User::PGPFile_CTL],23)', N'\Package.Variables[User::FTPCTL_FullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FilePath]  +  @[User::SourceFileName]  + (DT_STR, 4, 1252) YEAR(GETDATE())  +  RIGHT("00" +  (DT_STR, 2, 1252) MONTH(GETDATE()),2) +  RIGHT("00" + (DT_STR, 2, 1252) DAY(GETDATE()),2)    + ".csv"', N'\Package.Variables[User::DestFilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::DestFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'DestFileName', N'\Package.Variables[User::DestFileName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::DestFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::DestFileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::CtlVersion].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlVersion].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlVersion].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::CtlVersion].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'CtlVersion', N'\Package.Variables[User::CtlVersion].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::CtlVersion].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlVersion].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlVersion].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlVersion].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::CtlRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlRecordCount].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlRecordCount].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::CtlRecordCount].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'CtlRecordCount', N'\Package.Variables[User::CtlRecordCount].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::CtlRecordCount].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'{03BD1995-A4A8-46AA-BBE6-A17CF24EFF32}', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'*SF*.ctl', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Foreach Loop Container - Archive CTL Extract Files', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1033', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1048576', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'-1', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Foreach Loop Container', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CTL Extract Files.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'*SFPENDINGTRANS*.csv', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[FileSpec]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ArchiveSourceFullPath', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SF_20120618.ctl', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ArchiveSourceCTLFullPath', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FilePath]  +   @[User::ArchiveCTLFile]', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\Archive_PendingExtracts\', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveDestinationPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveDestinationPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ArchiveDestinationPath', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveDestinationPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveDestinationPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'002_SF_20120618.ctl', N'\Package.Variables[User::ArchiveCTLFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCTLFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCTLFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'FTP_Path', N'\Package.Variables[User::FTP_Path].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTP_Path].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FTP_Path].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTP_Path].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FTP_Path].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FileToBeRenamed].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FileToBeRenamed].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FileToBeRenamed].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::FileToBeRenamed].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'FileToBeRenamed', N'\Package.Variables[User::FileToBeRenamed].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FileToBeRenamed].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FileToBeRenamed].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FileToBeRenamed].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FileToBeRenamed].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\REB_SFPENDINGTRANS_20130117.csv', N'\Package.Variables[User::DestFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::DestFilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'DestFilePath', N'\Package.Variables[User::DestFilePath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::DestFilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::DestFilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::DestFilePath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'207_SFPENDINGTRANS_20120101.CSV', N'\Package.Variables[User::DestFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[Disable]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Foreach Loop Container', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Connections[Exportfile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::PGPFile_CTL].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::PGPFile_CSV].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::NewFileSeqNumber].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::LastFileSeqNumber].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FilePath]  +   SUBSTRING(@[User::ArchiveCSVFile],1,28)  +  "csv.pgp"

 ', N'\Package.Variables[User::PGPFile_CSV].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::PGPFile_CSV].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::PGPFile_CTL].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::SourceFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::SourceFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'"', N'\Package.Connections[Exportfile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[Exportfile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package.Connections[Exportfile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Exportfile', N'\Package.Connections[Exportfile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Exportfile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'
', N'\Package.Connections[Exportfile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Delimited', N'\Package.Connections[Exportfile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Exportfile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[Exportfile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::RowCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::PGPFile_CTL].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::NewFileSeqNumber].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::LastFileSeqNumber].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N' \\web3\users\250SimpleTuition\FROM_RN\002_SF_20120618.ctl.pgp', N'\Package.Variables[User::FTPCTL_FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N' \\web3\users\250SimpleTuition\FROM_RN\002_SFPENDINGTRANS_20120618.csv.pgp', N'\Package.Variables[User::FTPCSV_FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Exportfile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\ExportFile.csv', N'\Package.Connections[Exportfile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Connections[Exportfile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1252', N'\Package.Connections[Exportfile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Connections[controlFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'"', N'\Package.Connections[controlFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[controlFile].Properties[RowDelimiter]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1033', N'\Package.Connections[Exportfile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1252', N'\Package.Connections[Accrual File].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::SourceFileName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::RowCount].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SF_20120618.ctl.pgp', N'\Package.Variables[User::PGPFile_CTL].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'PGPFile_CTL', N'\Package.Variables[User::PGPFile_CTL].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FilePath]  +   SUBSTRING(@[User::ArchiveCTLFile],1,28)  +  ".pgp"', N'\Package.Variables[User::PGPFile_CTL].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::PGPFile_CSV].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::PGPFile_CSV].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::NewFileSeqNumber].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::NewFileSeqNumber].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::LastFileSeqNumber].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::LastFileSeqNumber].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::LastFileSeqNumber].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::LastFileSeqNumber].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTPCTL_FullPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTPCTL_FullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::FTPCTL_FullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FTPCTL_FullPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTPCSV_FullPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'FTPCSV_FullPath', N'\Package.Variables[User::FTPCSV_FullPath].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTPCSV_FullPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'@[User::FTP_Path]  +  RIGHT(@[User::PGPFile_CSV],35)', N'\Package.Variables[User::FTPCSV_FullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::FTPCSV_FullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FTPCSV_FullPath].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N' \\web3\users\250SimpleTuition\FROM_RN\', N'\Package.Variables[User::FTP_Path].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTP_Path].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTP_Path].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::FTP_Path].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package.Connections[controlFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::RowCount].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::RowCount].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'controlFile', N'\Package.Connections[controlFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1033', N'\Package.Connections[controlFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[controlFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'
', N'\Package.Connections[controlFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Delimited', N'\Package.Connections[controlFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[controlFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[controlFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[controlFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'O:\Vesdia\Output\_SF_20130117.ctl', N'\Package.Connections[controlFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Connections[controlFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1252', N'\Package.Connections[controlFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Connections[ClientExportFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'"', N'\Package.Connections[ClientExportFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[ClientExportFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'RowCount', N'\Package.Variables[User::RowCount].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::RowCount].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::RowCount].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::RowCount].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package.Connections[ClientExportFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ClientExportFile', N'\Package.Connections[ClientExportFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1033', N'\Package.Connections[ClientExportFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::PGPFile_CTL].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::NewFileSeqNumber].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'LastFileSeqNumber', N'\Package.Variables[User::LastFileSeqNumber].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTPCTL_FullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::FTPCSV_FullPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::ArchiveCTLFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ArchiveCTLFile', N'\Package.Variables[User::ArchiveCTLFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCTLFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveCTLFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCTLFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveCTLFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'002_SFPENDINGTRANS_20120618.csv', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::ArchiveCSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ArchiveCSVFile', N'\Package.Variables[User::ArchiveCSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveCSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveCSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ArchiveCSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[ClientExportFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::RowCount].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::PGPFile_CTL].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::NewFileSeqNumber].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Variables[User::LastFileSeqNumber].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::FTPCTL_FullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::FTPCSV_FullPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'
', N'\Package.Connections[ClientExportFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Delimited', N'\Package.Connections[ClientExportFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[ClientExportFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[ClientExportFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[ClientExportFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'O:\Vesdia\Output\_SFPENDINGTRANS_20130117.csv', N'\Package.Connections[ClientExportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Connections[ClientExportFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1252', N'\Package.Connections[ClientExportFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::RecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::RecordCount].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::RecordCount].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::RecordCount].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Connections[Accrual File].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'<none>', N'\Package.Connections[Accrual File].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[Accrual File].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1', N'\Package.Connections[Accrual File].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Accrual File', N'\Package.Connections[Accrual File].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1033', N'\Package.Connections[Accrual File].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Accrual File].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'
', N'\Package.Connections[Accrual File].Properties[HeaderRowDelimiter]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'RaggedRight', N'\Package.Connections[Accrual File].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Accrual File].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[Accrual File].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlRecordCount].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlRecordCount].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlRecordCount].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlDbNumber].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlDbNumber].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::CtlDbNumber].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'CtlDbNumber', N'\Package.Variables[User::CtlDbNumber].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlDbNumber].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlDbNumber].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlClientId].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlClientId].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlClientId].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::CtlClientId].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'CtlClientId', N'\Package.Variables[User::CtlClientId].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlClientId].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlClientId].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::CtlClientId].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlClientId].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ClientIdName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ClientIdName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ClientIdName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'User', N'\Package.Variables[User::ClientIdName].Properties[Namespace]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'ClientIdName', N'\Package.Variables[User::ClientIdName].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ClientIdName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ClientIdName].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ClientIdName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ClientIdName].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SFPENDINGTRANS_20120618.csv', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'False', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

