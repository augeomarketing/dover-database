USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'BNW_Post2WebParent', N'Data Source=236722-SQLCLUS2\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-BNW_Post2WebParent-{19C06998-A722-498A-8FEE-6230D121E842}doolittle\rn.RewardsNow;', N'\Package.Connections[RewardsNOW Prod].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_Post2WebParent', N'C:\SVN\Database\SSIS\RewardsNow_Prod_daily_Export_Hist\RewardsNow_Prod_daily_Export_Hist\DailyHistoryPushtoWebChild.dtsx', N'\Package.Connections[DailyHistoryPushtoWebChild.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNWPostToWeb', N'\\236722-sqlclus2\ops\SSIS_Packages\Scheduled_Jobs\BraveNewWorld\BNWPostToWeb.log', N'\Package.Connections[BNWPostToWeb.log].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_Post2WebParent', N'Data Source=236722-sqlclus2\rn;Initial Catalog=master;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-BNW_Post2WebParent-{72AEA5F4-4AB0-4D97-BC47-F1AC38234DE6}SSIS.doolittle\rn;', N'\Package.Connections[SSIS Package Store].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_Post2WebParent', N'C:\SVN\Database\SSIS\BraveNewWorld\BraveNewWorld\PostToWeb.dtsx', N'\Package.Connections[PostToWeb.dtsx].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_Post2WebParent', N'GO', N'\Package.Variables[User::StopGo].Properties[Value]', N'String' UNION ALL
SELECT N'BNWPostToWeb', N'Data Source=236718-sqlclus;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-PostToWeb-{54B1F9D0-C76D-44C0-8A51-119576643C0A}doolittle\web.RewardsNOW;Auto Translate=False;', N'\Package.Connections[Web_RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNWPostToWeb', N'Data Source=236722-sqlclus2\rn;Initial Catalog=msdb;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-PostToWeb-{373FA1AA-9A94-4E02-9047-DEC83EAC7D57}doolittle\rn.msdb;Auto Translate=False;', N'\Package.Connections[SSISPackageStore].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNWPostToWeb', N'Data Source=236722-sqlclus2\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-PostToWeb-{28E9EFAB-D170-41AC-8511-45E747082C41}doolittle\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[Prod_RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_Post2WebParent', N'18000', N'\Package.Variables[User::SleepInterval].Properties[Value]', N'Int64' UNION ALL
SELECT N'BNWPostToWeb', N'C:\SVN\Database\SSIS\RewardsNow_Prod_daily_Export_Hist\RewardsNow_Prod_daily_Export_Hist\DailyHistoryPushtoWebChild.dtsx', N'\Package.Connections[DailyHistoryPushtoWebChild.dtsx].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

