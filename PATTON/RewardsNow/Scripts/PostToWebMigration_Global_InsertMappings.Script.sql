use RewardsNow
GO

--LAST SIX OF CARD IN AFFILIAT ACCTID TO LASTSIX IN ACCOUNT 
INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 1, 96, 1
FROM
(
	SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
	(
		  '002', '003', '102', '105', '106', '109', '000', '10A', '10B', '10C'
		, '10D', '10E', '111', '112', '113', '114', '115', '116', '117', '118'
		, '119', '120', '121', '122', '123', '124', '125', '127', '128', '129'
		, '130', '131', '132', '133', '134', '135', '136', '137', '138', '140'
		, '141', '142', '143', '144', '145', '146', '147', '148', '149', '150'
		, '151', '152', '153', '154', '155', '156', '157', '158', '159', '160'
		, '161', '162', '163', '164', '165', '166', '167', '168', '169', '170'
		, '171', '172', '173', '174', '175', '176', '177', '178', '179', '180'
		, '181', '182', '183', '184', '185', '186', '187', '188', '189', '190'
		, '191', '192', '193', '194', '195', '199', '204', '206', '210', '211'
		, '212', '214', '216', '220', '222', '225', '226', '227', '229', '231'
		, '232', '234', '235', '237', '500', '501', '502', '504', '506', '507'
		, '508', '509', '50A', '50B', '50C', '50D', '50E', '50F', '50G', '50H'
		, '50J', '50K', '50M', '50N', '50P', '50Q', '50R', '50S', '50T', '50U'
		, '50V', '50W', '50X', '50Y', '50Z', '510', '511', '512', '513', '514'
		, '515', '516', '517', '518', '519', '51A', '51B', '51C', '51D', '51E'
		, '51H', '51J', '51K', '51M', '51N', '51P', '51Q', '51R', '51S', '51T'
		, '51U', '51V', '51W', '51X', '51Y', '51Z', '520', '521', '522', '523'
		, '525', '526', '527', '528', '529', '52A', '52B', '52C', '52D', '52E'
		, '52F', '52G', '52H', '52J', '52k', '52L', '52M', '52N', '52P', '52Q'
		, '52R', '530', '531', '532', '533', '535', '536', '537', '538', '539'
		, '540', '541', '542', '544', '545', '546', '547', '548', '549', '550'
		, '551', '552', '553', '554', '556', '557', '558', '559', '560', '561'
		, '562', '563', '565', '566', '567', '568', '569', '570', '571', '572'
		, '573', '574', '575', '576', '577', '578', '579', '580', '581', '582'
		, '583', '584', '585', '586', '587', '588', '589', '590', '591', '592'
		, '593', '594', '595', '596', '597', '598', '599', '601', '603', '605'
		, '608', '609', '611', '613', '615', '617', '619', '621', '624', '625'
		, '627', '629', '630', '631', '632', '633', '634', '636', '637', '638'
		, '639', '640', '641', '642', '650', '700', '701', '702', '703', '704'
		, '705', '706', '707', '708', '709', '712', '713', '213', '230', '717'			
	)
	AND sid_FIProdStatus_statuscode = 'P'
) db
LEFT OUTER JOIN mappingdefinition md 
	ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
	AND md.sid_mappingsource_id = 96
WHERE
	md.sid_dbprocessinfo_dbnumber IS NULL


--MISC4 IN CUSTOMER TO LASTSIX IN ACCOUNT  (1, 1, 117, 0)
INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 1, 117, 1
FROM
(
	SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
	(
		'219', '236'
	)
	AND sid_FIProdStatus_statuscode = 'P'
) db
LEFT OUTER JOIN mappingdefinition md 
	ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
	AND md.sid_mappingsource_id = 117
WHERE
	md.sid_dbprocessinfo_dbnumber IS NULL


--LAST 4 OF ACCTID IN AFFILIAT TO LASTSIX IN ACCOUNT (1,1,<ufn_mappingAffiliatCustIDLast4>, 0)

IF (SELECT ISNULL(sid_mappingsource_id, 0) FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatAcctIDLast4') != 0
BEGIN

	INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
	SELECT db.dbnumber, 1, 1, (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatAcctIDLast4'), 1
	FROM
	(
		SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
		(
			  '714', '716', '207'
		)
		AND sid_FIProdStatus_statuscode = 'P'
	) db
	LEFT OUTER JOIN mappingdefinition md 
		ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
		AND md.sid_mappingsource_id = (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatAcctIDLast4')
	WHERE
		md.sid_dbprocessinfo_dbnumber IS NULL
END

--CUSTID IN AFFILIAT TO LASTSIX IN ACCOUNT
INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 1, 119, 1
FROM
(
	SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
	(
		'205', '208'
	)
	AND sid_FIProdStatus_statuscode = 'P'
) db
LEFT OUTER JOIN mappingdefinition md 
	ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
	AND md.sid_mappingsource_id = 119
WHERE
	md.sid_dbprocessinfo_dbnumber IS NULL

--AFFILIAT CUSTID TO SSNLAST4
INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 2, 119, 1
FROM
(
	SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
	(
		'226', '229', '595'
	)
	AND sid_FIProdStatus_statuscode = 'P'
) db
LEFT OUTER JOIN mappingdefinition md 
	ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
	AND md.sid_mappingsource_id = 119
WHERE
	md.sid_dbprocessinfo_dbnumber IS NULL


	
--SECID IN AFFILIAT TO LASTSIX IN ACCOUNT
INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 1, 128, 1
FROM
(
	SELECT dbnumber FROM dbprocessinfo WHERE DBNumber LIKE '40[012345]'
	AND sid_FIProdStatus_statuscode = 'P'
) db
LEFT OUTER JOIN mappingdefinition md 
	ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
	AND md.sid_mappingsource_id = 128
WHERE
	md.sid_dbprocessinfo_dbnumber IS NULL

--SECID IN AFFILIAT TO MEMBER NUMBER	
INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
SELECT db.dbnumber, 1, 4, 128, 1
FROM
(
	SELECT dbnumber FROM dbprocessinfo WHERE DBNumber = '52E'
	AND sid_FIProdStatus_statuscode = 'P'
) db
LEFT OUTER JOIN mappingdefinition md 
	ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
	AND md.sid_mappingsource_id = 128
WHERE
	md.sid_dbprocessinfo_dbnumber IS NULL
	
	
--LAST FOUR OF CUST ID IN AFFILIAT TO SSNLAST4 IN ACCOUNT

IF (SELECT ISNULL(sid_mappingsource_id, 0) FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatCustIDLast4') != 0
BEGIN

	INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
	SELECT db.dbnumber, 1, 2, (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatCustIDLast4'), 1
	FROM
	(
		SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
		(
			  '229', '230', '700', '701', '702', '703', '704'
			, '705', '706', '707', '708', '712', '716', '717'

		)
		AND sid_FIProdStatus_statuscode = 'P'
	) db
	LEFT OUTER JOIN mappingdefinition md 
		ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
		AND md.sid_mappingsource_id = (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingAffiliatCustIDLast4')
	WHERE
		md.sid_dbprocessinfo_dbnumber IS NULL
END

--AFFILIAT ACCTID TO SSNLAST4
IF (SELECT ISNULL(sid_mappingsource_id, 0) FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractaffiliatacctid') != 0
BEGIN

	INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
	SELECT db.dbnumber, 1, 2, (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractaffiliatacctid'), 1
	FROM
	(
		SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
		(
			  '213'
		)
		AND sid_FIProdStatus_statuscode = 'P'
	) db
	LEFT OUTER JOIN mappingdefinition md 
		ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
		AND md.sid_mappingsource_id = (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractaffiliatacctid')
	WHERE
		md.sid_dbprocessinfo_dbnumber IS NULL
END

--CUSTOMER MISC2 TO SSNLAST4
IF (SELECT ISNULL(sid_mappingsource_id, 0) FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractCustomerMisc2') != 0
BEGIN

	INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
	SELECT db.dbnumber, 1, 2, (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractCustomerMisc2'), 1
	FROM
	(
		SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
		(
			  '236'
		)
		AND sid_FIProdStatus_statuscode = 'P'
	) db
	LEFT OUTER JOIN mappingdefinition md 
		ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
		AND md.sid_mappingsource_id = (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractCustomerMisc2')
	WHERE
		md.sid_dbprocessinfo_dbnumber IS NULL
--CUSTOMER MISC2 TO MEMBER NUMBER

	INSERT INTO mappingdefinition (sid_dbprocessinfo_dbnumber, sid_mappingtable_id, sid_mappingdestination_id, sid_mappingsource_id, dim_mappingdefinition_active)
	SELECT db.dbnumber, 1, 4, (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractCustomerMisc2'), 1
	FROM
	(
		SELECT dbnumber FROM dbprocessinfo WHERE DBNumber IN
		(
			  '52U', '52J'
		)
		AND sid_FIProdStatus_statuscode = 'P'
	) db
	LEFT OUTER JOIN mappingdefinition md 
		ON db.DBNumber = md.sid_dbprocessinfo_dbnumber
		AND md.sid_mappingsource_id = (SELECT sid_mappingsource_id FROM mappingsource WHERE dim_mappingsource_source = 'ufn_mappingextractCustomerMisc2')
	WHERE
		md.sid_dbprocessinfo_dbnumber IS NULL





END

