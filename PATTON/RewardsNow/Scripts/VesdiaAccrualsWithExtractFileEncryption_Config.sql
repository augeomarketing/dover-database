USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter = 'VesdiaAccrualsWithExtractFileEncryption'
go

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SFPENDINGTRANS_20120618.csv.pgp', N'\Package.Variables[User::PGPFile_CSV].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::NewFileSeqNumber].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::LastFileSeqNumber].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N' \\web3\users\250SimpleTuition\FROM_RN\002_SF_20120618.ctl.pgp', N'\Package.Variables[User::FTPCTL_FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N' \\web3\users\250SimpleTuition\FROM_RN\002_SFPENDINGTRANS_20120618.csv.pgp', N'\Package.Variables[User::FTPCSV_FullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N' \\web3\users\250SimpleTuition\FROM_RN\', N'\Package.Variables[User::FTP_Path].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::FileToBeRenamed].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\_SFPENDINGTRANS_20130214.csv', N'\Package.Variables[User::DestFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'207_SFPENDINGTRANS_20120101.CSV', N'\Package.Variables[User::DestFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::CtlVersion].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::CtlRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output', N'\Package\Foreach Loop Container - Archive CTL Extract Files.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output', N'\Package\Foreach Loop Container - Archive CSV Extract Files and Encrypt (PGP).ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaAccrualsWithExtractFile-{C8C57154-CE9A-4F5A-B6D6-AF6CB8B610E6}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\itops\Encryption Keys\KeyRing\pubring.gpg', N'\Package.Connections[PubKeyRing.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'"', N'\Package.Connections[Exportfile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[Exportfile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Delimited', N'\Package.Connections[Exportfile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Exportfile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\ExportFile.csv', N'\Package.Connections[Exportfile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Connections[Exportfile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1252', N'\Package.Connections[Exportfile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'"', N'\Package.Connections[controlFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[controlFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[controlFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\controlFile.ctl', N'\Package.Connections[controlFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Connections[controlFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'"', N'\Package.Connections[ClientExportFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'Delimited', N'\Package.Connections[ClientExportFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\exportFile.csv', N'\Package.Connections[ClientExportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'True', N'\Package.Connections[ClientExportFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'<none>', N'\Package.Connections[Accrual File].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Connections[Accrual File].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Connections[Accrual File].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Input\AccrualFile.csv', N'\Package.Connections[Accrual File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'1252', N'\Package.Connections[Accrual File].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\REB_SFPENDINGTRANS_XXXX.csv', N'\Package.Variables[User::SourceFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::RowCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'0', N'\Package.Variables[User::RecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::CtlClientId].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'', N'\Package.Variables[User::ClientIdName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SFPENDINGTRANS_20120618.csv', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SF_20120618.ctl', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\Archive_PendingExtracts\', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'002_SF_20120618.ctl', N'\Package.Variables[User::ArchiveCTLFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'002_SFPENDINGTRANS_20120618.csv', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\exportFile.csv', N'\Package.Variables[User::AccrualExportSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\_SFPENDINGTRANS_20130214.csv', N'\Package.Variables[User::AccrualExportDestination].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\ControlFile.ctl', N'\Package.Variables[User::AccrualCtlSource].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\_SF_20130214.ctl', N'\Package.Variables[User::AccrualCtlDestination].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFileEncryption', N'\\patton\ops\Vesdia\Output\002_SF_20120618.ctl.pgp', N'\Package.Variables[User::PGPFile_CTL].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

