USE [RewardsNow]
GO


TRUNCATE TABLE RNICardTypeByBin

--GET ALL OF THE EXISTING AFFILIAT BINS AND CARD TYPES

DECLARE @sql1 NVARCHAR(2000)
	, @sql2 NVARCHAR(2000)

SET @sql1 = '
IF (SELECT COUNT(*) FROM [?].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''AFFILIAT'') <> 0
BEGIN
	INSERT INTO RewardsNow.dbo.RNICardTypeByBin (dim_rnicardtypebybin_bin, dim_rnicardtypebybin_cardtype)
	SELECT DISTINCT LEFT(LTRIM(RTRIM(AFF.ACCTID)), 6)
	, CASE
		WHEN AFF.ACCTTYPE LIKE ''CRE%'' THEN ''CREDIT''
		WHEN AFF.ACCTTYPE LIKE ''DEB%'' THEN ''DEBIT''
		ELSE ''OTHER''
	  END
	FROM [?].dbo.AFFILIAT AFF
	LEFT OUTER JOIN RewardsNow.dbo.RNICardTypeByBin CTB
	ON LEFT(LTRIM(RTRIM(AFF.ACCTID)), 6) = CTB.dim_rnicardtypebybin_bin
	WHERE RewardsNow.dbo.fnCheckLuhn10(AFF.ACCTID) = 1
	AND LEN(LTRIM(RTRIM(AFF.ACCTID))) BETWEEN 16 AND 20
	AND CTB.sid_rnicardtypebybin_id IS NULL
END;
'
SET @sql2 = '
IF (SELECT COUNT(*) FROM [?].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ''AFFILIATDeleted'') <> 0
BEGIN
	INSERT INTO RewardsNow.dbo.RNICardTypeByBin (dim_rnicardtypebybin_bin, dim_rnicardtypebybin_cardtype)
	SELECT DISTINCT LEFT(LTRIM(RTRIM(AFF.ACCTID)), 6)
	, CASE
		WHEN AFF.ACCTTYPE LIKE ''CRE%'' THEN ''CREDIT''
		WHEN AFF.ACCTTYPE LIKE ''DEB%'' THEN ''DEBIT''
		ELSE ''OTHER''
	  END
	FROM [?].dbo.AFFILIATDeleted AFF
	LEFT OUTER JOIN RewardsNow.dbo.RNICardTypeByBin CTB
	ON LEFT(LTRIM(RTRIM(AFF.ACCTID)), 6) = CTB.dim_rnicardtypebybin_bin
	WHERE RewardsNow.dbo.fnCheckLuhn10(AFF.ACCTID) = 1
	AND LEN(LTRIM(RTRIM(AFF.ACCTID))) BETWEEN 16 AND 20
	AND CTB.sid_rnicardtypebybin_id IS NULL
END;
'
exec sp_MSforeachdb @sql1, '?', @sql2


-- SET ANY THAT ARE MARKED WITH BOTH 'OTHER' AND ANY OTHER TYPE TO THAT OTHER TYPE
UPDATE  ctb
SET dim_rnicardtypebybin_cardtype = ctb2.dim_rnicardtypebybin_cardtype
FROM RNICardTypeByBin ctb
INNER JOIN RNICardTypeByBin ctb2
ON ctb.dim_rnicardtypebybin_bin = ctb2.dim_rnicardtypebybin_bin
WHERE ctb.dim_rnicardtypebybin_cardtype = 'OTHER'
	AND ctb2.dim_rnicardtypebybin_cardtype != 'OTHER'

--DEDUPE WHATEVER IS LEFT
;WITH nr
AS
(
	SELECT ROW_NUMBER() OVER (PARTITION BY dim_rnicardtypebybin_bin, dim_rnicardtypebybin_cardtype ORDER BY dim_rnicardtypebybin_cardtype, sid_rnicardtypebybin_id) r
	, *
	FROM RNICardTypeByBin 
)
DELETE
FROM nr
where r > 1

--IF THERE ARE ANY LEFT WITH BOTH DEBIT AND CREDIT FOR THE SAME BIN, SET THEM TO 'OTHER'
;WITH nr
AS
(
	SELECT dim_rnicardtypebybin_bin
	FROM RNICardTypeByBin
	GROUP BY dim_rnicardtypebybin_bin
	HAVING COUNT(*) > 1
)
UPDATE ctb
SET dim_rnicardtypebybin_cardtype = 'OTHER'
FROM RNICardTypeByBin ctb
INNER JOIN nr
	ON ctb.dim_rnicardtypebybin_bin = nr.dim_rnicardtypebybin_bin

--DEDUPE THE REMAINING (WE DEFINITELY CREATED DUPLICATES IN THE ABOVE STEP)	
;WITH nr
AS
(
	SELECT ROW_NUMBER() OVER (PARTITION BY dim_rnicardtypebybin_bin, dim_rnicardtypebybin_cardtype ORDER BY dim_rnicardtypebybin_cardtype, sid_rnicardtypebybin_id) r
	, *
	FROM RNICardTypeByBin 
)
DELETE
FROM nr
where r > 1

--NOW RUN THIS THROUGH SHAWNS SCRUBBER ROUTINE THAT GOES OUT TO THE INTERWEBS 
--AND RETURNS THE CARDTYPE FROM A DATABASE