USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_OfferData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OfferData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OfferData', N'c:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OfferData', N'1', N'\Package.Connections[OfferDataToUnzip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'OfferDataToUnzip', N'\Package.Connections[OfferDataToUnzip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OfferData', N'Data Source=patton\RN;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-OfferData-{1CDBECD0-4F02-483F-9C24-734A735BAEB5}patton\RN.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OfferData', N'0', N'\Package.Connections[OfferDataToUnzip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'', N'\Package.Connections[OfferDataToUnzip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OfferData', N'O:\AccessDevelopment\Input\OfferData.zip', N'\Package.Connections[OfferDataToUnzip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OfferData', N'1', N'\Package.Connections[OfferData.zip.pgp].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'OfferData.zip.pgp', N'\Package.Connections[OfferData.zip.pgp].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OfferData', N'0', N'\Package.Connections[OfferData.zip.pgp].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'', N'\Package.Connections[OfferData.zip.pgp].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OfferData', N'O:\AccessDevelopment\Input\OfferData.zip.pgp', N'\Package.Connections[OfferData.zip.pgp].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OfferData', N'1', N'\Package.Connections[OfferData.zip].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'OfferData.zip', N'\Package.Connections[OfferData.zip].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OfferData', N'1', N'\Package.Connections[OfferData.zip].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'', N'\Package.Connections[OfferData.zip].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OfferData', N'O:\AccessDevelopment\Input\OfferData.zip', N'\Package.Connections[OfferData.zip].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OfferData', N'1', N'\Package.Connections[OFFER-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'OFFER-DATA', N'\Package.Connections[OFFER-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OfferData', N'2', N'\Package.Connections[OFFER-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_OfferData', N'', N'\Package.Connections[OFFER-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_OfferData', N'O:\AccessDevelopment\Input\OFFER-DATA', N'\Package.Connections[OFFER-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_OfferData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_OfferData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_OfferData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_OfferData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_OfferData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_OfferData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_OfferData', NULL, N'\Package.Variables[User::FileSize].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_OfferData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_OfferData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

