USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[AccessRecordTypes]([sid_AccessRecordTypes_FieldName], [dim_AccessRecordTypes_Description])
SELECT N'BRN_SYN', N'Synchronize Brand' UNION ALL
SELECT N'CRD_SYN', N'Synchronize Card' UNION ALL
SELECT N'LOC_SYN', N'Synchronize location' UNION ALL
SELECT N'MEM_SYN', N'Synchronize Member' UNION ALL
SELECT N'MID_SYN', N'Synchronize MID' UNION ALL
SELECT N'MID_VER', N'Verify MID' UNION ALL
SELECT N'OFR_SYN', N'Synchronize Offer' UNION ALL
SELECT N'ORG_SYN', N'Synchronize Organization' UNION ALL
SELECT N'SET', N'Merchant Settlements' UNION ALL
SELECT N'STA_FIL', N'File Error' UNION ALL
SELECT N'STA_REC', N'Record Error' UNION ALL
SELECT N'STM', N'Member Statements' UNION ALL
SELECT N'SUB_SYN', N'Synchronize Subscription' UNION ALL
SELECT N'TXN', N'Member Transactions'
COMMIT;
RAISERROR (N'[dbo].[AccessRecordTypes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

