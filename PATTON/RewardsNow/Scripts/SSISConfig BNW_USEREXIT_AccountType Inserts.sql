USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'BNW_USEREXIT_AccountType', N'Data Source=doolittle\web;Initial Catalog=RewardsNOW;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-BNW_USEREXIT_AccountType-{34FCAB5F-7F9E-40FB-A1E2-70A7772080B8}doolittle\web.RewardsNOW;', N'\Package.Connections[WEB_RewardsNOW].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_USEREXIT_AccountType', N'Data Source=doolittle\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-BNW_USEREXIT_AccountType-{A450434E-66FE-4D94-9C3E-7217E957D06B}doolittle\rn.RewardsNow;', N'\Package.Connections[SSIS Package Store].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'BNW_USEREXIT_AccountType', N'Data Source=doolittle\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-BNW_USEREXIT_AccountType-{998AE301-4670-4943-96DE-27F722306FAB}doolittle\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

