USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProgram] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[RNIBonusProgram]([sid_rnibonusprogram_id], [dim_rnibonusprogram_description], [dim_rnibonusprogrameffectivedate], [dim_rnibonusprogramexpirationdate], [sid_rnibonusprogramtype_code], [sid_trantype_tranCode], [dim_rnibonusprogram_dateadded], [dim_rnibonusprogram_lastupdated])
SELECT 1, N'SCU Double Points Promo - Platinum only', '20110701 00:00:00.000', '20110831 00:00:00.000', N'C', N'BM', '20110805 14:53:53.733', '20110805 14:53:53.733' UNION ALL
SELECT 2, N'RewardsNOW E-Statement Bonus', '20110101 00:00:00.000', '99991231 00:00:00.000', N'F', N'BE', '20110809 11:50:13.733', '20110809 11:50:13.733'
COMMIT;
RAISERROR (N'[dbo].[RNIBonusProgram]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[RNIBonusProgram] OFF;

