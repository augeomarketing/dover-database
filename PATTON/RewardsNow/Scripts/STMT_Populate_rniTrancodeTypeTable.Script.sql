USE REWARDSNOW
GO

SET IDENTITY_INSERT rniTrancodeGroup ON
GO

-----------------------------------------------------------
--POINTINCREASE (1)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'POINTINCREASE' WHERE sid_rniTrancodeGroup_id = 1
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (1, 'POINTINCREASE')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 1

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 1, '67'
UNION SELECT 1, '64'
UNION SELECT 1, '65'
UNION SELECT 1, '6B'
UNION SELECT 1, '61'
UNION SELECT 1, '63'
UNION SELECT 1, '6M'
UNION SELECT 1, '6H'
UNION SELECT 1, '68'
UNION SELECT 1, '69'
UNION SELECT 1, '0A'
UNION SELECT 1, '0B'
UNION SELECT 1, '0C'
UNION SELECT 1, '0D'
UNION SELECT 1, '0E'
UNION SELECT 1, '0F'
UNION SELECT 1, '0G'
UNION SELECT 1, '0H'
UNION SELECT 1, '0I'
UNION SELECT 1, '0J'
UNION SELECT 1, '0K'
UNION SELECT 1, 'B1'
UNION SELECT 1, 'BA'
UNION SELECT 1, 'BC'
UNION SELECT 1, 'BE'
UNION SELECT 1, 'BF'
UNION SELECT 1, 'BG'
UNION SELECT 1, 'BH'
UNION SELECT 1, 'BI'
UNION SELECT 1, 'BJ'
UNION SELECT 1, 'BK'
UNION SELECT 1, 'BL'
UNION SELECT 1, 'BM'
UNION SELECT 1, 'BN'
UNION SELECT 1, 'BP'
UNION SELECT 1, 'BR'
UNION SELECT 1, 'BS'
UNION SELECT 1, 'BT'
UNION SELECT 1, 'BV'
UNION SELECT 1, 'G4'
UNION SELECT 1, 'G5'
UNION SELECT 1, 'G6'
UNION SELECT 1, 'RQ'
UNION SELECT 1, 'F1'
UNION SELECT 1, 'F2'
UNION SELECT 1, 'F3'
UNION SELECT 1, 'F4'
UNION SELECT 1, 'F5'
UNION SELECT 1, 'F6'
UNION SELECT 1, 'F7'
UNION SELECT 1, 'F8'
UNION SELECT 1, 'FA'
UNION SELECT 1, 'FB'
UNION SELECT 1, 'FC'
UNION SELECT 1, 'FD'
UNION SELECT 1, 'FE'
UNION SELECT 1, 'FF'
UNION SELECT 1, 'FG'
UNION SELECT 1, 'FH'
UNION SELECT 1, 'FI'
UNION SELECT 1, 'FJ'
UNION SELECT 1, 'FK'
UNION SELECT 1, 'FL'
UNION SELECT 1, 'FM'
UNION SELECT 1, 'FN'
UNION SELECT 1, 'FO'
UNION SELECT 1, 'FP'
UNION SELECT 1, 'FQ'
UNION SELECT 1, 'FS'
UNION SELECT 1, 'FT'
UNION SELECT 1, 'FU'
UNION SELECT 1, 'FV'
UNION SELECT 1, 'FW'
UNION SELECT 1, 'FX'
UNION SELECT 1, 'FY'
UNION SELECT 1, 'FZ'
UNION SELECT 1, 'F0'
UNION SELECT 1, 'G0'
UNION SELECT 1, '32'
UNION SELECT 1, '36'
UNION SELECT 1, 'II'
UNION SELECT 1, 'IE'
UNION SELECT 1, 'DR'
UNION SELECT 1, 'DZ'
UNION SELECT 1, 'XF'
UNION SELECT 1, 'TI'
UNION SELECT 1, 'TP'
UNION SELECT 1, 'G2'
UNION SELECT 1, 'G3'

-----------------------------------------------------------
--POINTDECREASE (2)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'POINTDECREASE' WHERE sid_rniTrancodeGroup_id = 2
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (2, 'POINTDECREASE')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 2

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 2, '31'
UNION SELECT 2, '33'
UNION SELECT 2, '3M'
UNION SELECT 2, '34'
UNION SELECT 2, '35'
UNION SELECT 2, '37'
UNION SELECT 2, '3B'
UNION SELECT 2, '3H'
UNION SELECT 2, '38'
UNION SELECT 2, '39'
UNION SELECT 2, 'G7'
UNION SELECT 2, 'G8'
UNION SELECT 2, 'GX'
UNION SELECT 2, '62'
UNION SELECT 2, '66'
UNION SELECT 2, 'BX'
UNION SELECT 2, 'F9'
UNION SELECT 2, 'G9'
UNION SELECT 2, 'FR'
UNION SELECT 2, 'DE'
UNION SELECT 2, 'IR'
UNION SELECT 2, 'XP'
UNION SELECT 2, 'TD'
UNION SELECT 2, 'RB'
UNION SELECT 2, 'RC'
UNION SELECT 2, 'RD'
UNION SELECT 2, 'RF'
UNION SELECT 2, 'RG'
UNION SELECT 2, 'RI'
UNION SELECT 2, 'RK'
UNION SELECT 2, 'RM'
UNION SELECT 2, 'RP'
UNION SELECT 2, 'RR'
UNION SELECT 2, 'RS'
UNION SELECT 2, 'RT'
UNION SELECT 2, 'RU'
UNION SELECT 2, 'RV'
UNION SELECT 2, 'RZ'

-----------------------------------------------------------
--INCREASE_PURCHASE (3)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_PURCHASE' WHERE sid_rniTrancodeGroup_id = 3
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (3, 'INCREASE_PURCHASE')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 3

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 3, '67'
UNION SELECT 3, '64'
UNION SELECT 3, '65'
UNION SELECT 3, '6B'
UNION SELECT 3, '61'
UNION SELECT 3, '63'
UNION SELECT 3, '6M'
UNION SELECT 3, '6H'
UNION SELECT 3, '68'
UNION SELECT 3, '69'

-----------------------------------------------------------
--INCREASE_PURCHASE_DEBIT (4)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_PURCHASE_DEBIT' WHERE sid_rniTrancodeGroup_id = 4
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (4, 'INCREASE_PURCHASE_DEBIT')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 4

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 4, '67'
UNION SELECT 4, '64'
UNION SELECT 4, '65'
UNION SELECT 4, '6B'

-----------------------------------------------------------
--INCREASE_PURCHASE_CREDIT (5)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_PURCHASE_CREDIT' WHERE sid_rniTrancodeGroup_id = 5
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (5, 'INCREASE_PURCHASE_CREDIT')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 5

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 5, '61'
UNION SELECT 5, '63'
UNION SELECT 5, '6M'

-----------------------------------------------------------
--INCREASE_PURCHASE_HELOC (6)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_PURCHASE_HELOC' WHERE sid_rniTrancodeGroup_id = 6
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (6, 'INCREASE_PURCHASE_HELOC')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 6

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 6, '6H'

-----------------------------------------------------------
--INCREASE_PURCHASE_OTHER (7)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_PURCHASE_OTHER' WHERE sid_rniTrancodeGroup_id = 7
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (7, 'INCREASE_PURCHASE_OTHER')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 7

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 7, '68'
UNION SELECT 7, '69'

-----------------------------------------------------------
--INCREASE_BONUS (8)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_BONUS' WHERE sid_rniTrancodeGroup_id = 8
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (8, 'INCREASE_BONUS')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 8

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 8, '0A'
UNION SELECT 8, '0B'
UNION SELECT 8, '0C'
UNION SELECT 8, '0D'
UNION SELECT 8, '0E'
UNION SELECT 8, '0F'
UNION SELECT 8, '0G'
UNION SELECT 8, '0H'
UNION SELECT 8, '0I'
UNION SELECT 8, '0J'
UNION SELECT 8, '0K'
UNION SELECT 8, 'B1'
UNION SELECT 8, 'BA'
UNION SELECT 8, 'BC'
UNION SELECT 8, 'BE'
UNION SELECT 8, 'BF'
UNION SELECT 8, 'BG'
UNION SELECT 8, 'BH'
UNION SELECT 8, 'BI'
UNION SELECT 8, 'BJ'
UNION SELECT 8, 'BK'
UNION SELECT 8, 'BL'
UNION SELECT 8, 'BM'
UNION SELECT 8, 'BN'
UNION SELECT 8, 'BP'
UNION SELECT 8, 'BR'
UNION SELECT 8, 'BS'
UNION SELECT 8, 'BT'
UNION SELECT 8, 'BV'
UNION SELECT 8, 'G4'
UNION SELECT 8, 'G5'
UNION SELECT 8, 'G6'
UNION SELECT 8, 'RQ'
UNION SELECT 8, 'F1'
UNION SELECT 8, 'F2'
UNION SELECT 8, 'F3'
UNION SELECT 8, 'F4'
UNION SELECT 8, 'F5'
UNION SELECT 8, 'F6'
UNION SELECT 8, 'F7'
UNION SELECT 8, 'F8'
UNION SELECT 8, 'FA'
UNION SELECT 8, 'FB'
UNION SELECT 8, 'FC'
UNION SELECT 8, 'FD'
UNION SELECT 8, 'FE'
UNION SELECT 8, 'FF'
UNION SELECT 8, 'FG'
UNION SELECT 8, 'FH'
UNION SELECT 8, 'FI'
UNION SELECT 8, 'FJ'
UNION SELECT 8, 'FK'
UNION SELECT 8, 'FL'
UNION SELECT 8, 'FM'
UNION SELECT 8, 'FN'
UNION SELECT 8, 'FO'
UNION SELECT 8, 'FP'
UNION SELECT 8, 'FQ'
UNION SELECT 8, 'FS'
UNION SELECT 8, 'FT'
UNION SELECT 8, 'FU'
UNION SELECT 8, 'FV'
UNION SELECT 8, 'FW'
UNION SELECT 8, 'FX'
UNION SELECT 8, 'FY'
UNION SELECT 8, 'FZ'
UNION SELECT 8, 'F0'
UNION SELECT 8, 'G0'


-----------------------------------------------------------
--INCREASE_BONUS_NOSF (9)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_BONUS_NOSF' WHERE sid_rniTrancodeGroup_id = 9
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (9, 'INCREASE_BONUS_NOSF')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 9

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 9,'0A'
UNION SELECT 9,'0B'
UNION SELECT 9,'0C'
UNION SELECT 9,'0D'
UNION SELECT 9,'0E'
UNION SELECT 9,'0F'
UNION SELECT 9,'0G'
UNION SELECT 9,'0H'
UNION SELECT 9,'0I'
UNION SELECT 9,'0J'
UNION SELECT 9,'0K'
UNION SELECT 9,'B1'
UNION SELECT 9,'BA'
UNION SELECT 9,'BC'
UNION SELECT 9,'BE'
UNION SELECT 9,'BF'
UNION SELECT 9,'BG'
UNION SELECT 9,'BH'
UNION SELECT 9,'BI'
UNION SELECT 9,'BJ'
UNION SELECT 9,'BK'
UNION SELECT 9,'BL'
UNION SELECT 9,'BM'
UNION SELECT 9,'BN'
UNION SELECT 9,'BP'
UNION SELECT 9,'BR'
UNION SELECT 9,'BS'
UNION SELECT 9,'BT'
UNION SELECT 9,'BV'
UNION SELECT 9,'G4'
UNION SELECT 9,'G5'
UNION SELECT 9,'G6'
UNION SELECT 9,'RQ'
UNION SELECT 9,'F1'
UNION SELECT 9,'F2'
UNION SELECT 9,'F3'
UNION SELECT 9,'F4'
UNION SELECT 9,'F5'
UNION SELECT 9,'F6'
UNION SELECT 9,'F7'
UNION SELECT 9,'F8'
UNION SELECT 9,'FA'
UNION SELECT 9,'FB'
UNION SELECT 9,'FC'
UNION SELECT 9,'FD'
UNION SELECT 9,'FE'
UNION SELECT 9,'FF'
UNION SELECT 9,'FG'
UNION SELECT 9,'FH'
UNION SELECT 9,'FI'
UNION SELECT 9,'FJ'
UNION SELECT 9,'FK'
UNION SELECT 9,'FL'
UNION SELECT 9,'FM'
UNION SELECT 9,'FN'
UNION SELECT 9,'FO'
UNION SELECT 9,'FP'
UNION SELECT 9,'FQ'
UNION SELECT 9,'FS'
UNION SELECT 9,'FT'
UNION SELECT 9,'FU'
UNION SELECT 9,'FV'
UNION SELECT 9,'FW'
UNION SELECT 9,'FX'
UNION SELECT 9,'FY'
UNION SELECT 9,'FZ'

-----------------------------------------------------------
--INCREASE_BONUS_NOSF_FULLSPECTRUM (10)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_BONUS_NOSF_FULLSPECTRUM' WHERE sid_rniTrancodeGroup_id = 10
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (10, 'INCREASE_BONUS_NOSF_FULLSPECTRUM')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 10

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 10, 'F1'
UNION SELECT 10, 'F2'
UNION SELECT 10, 'F3'
UNION SELECT 10, 'F4'
UNION SELECT 10, 'F5'
UNION SELECT 10, 'F6'
UNION SELECT 10, 'F7'
UNION SELECT 10, 'F8'
UNION SELECT 10, 'FA'
UNION SELECT 10, 'FB'
UNION SELECT 10, 'FC'
UNION SELECT 10, 'FD'
UNION SELECT 10, 'FE'
UNION SELECT 10, 'FF'
UNION SELECT 10, 'FG'
UNION SELECT 10, 'FH'
UNION SELECT 10, 'FI'
UNION SELECT 10, 'FJ'
UNION SELECT 10, 'FK'
UNION SELECT 10, 'FL'
UNION SELECT 10, 'FM'
UNION SELECT 10, 'FN'
UNION SELECT 10, 'FO'
UNION SELECT 10, 'FP'
UNION SELECT 10, 'FQ'
UNION SELECT 10, 'FS'
UNION SELECT 10, 'FT'
UNION SELECT 10, 'FU'
UNION SELECT 10, 'FV'
UNION SELECT 10, 'FW'
UNION SELECT 10, 'FX'
UNION SELECT 10, 'FY'
UNION SELECT 10, 'FZ'

-----------------------------------------------------------
--INCREASE_BONUS_SHOPPINGFLING (11)
-----------------------------------------------------------
UPDATE rniTrancodeGroup SET dim_rniTrancodeGroup_desc = 'INCREASE_BONUS_SHOPPINGFLING' WHERE sid_rniTrancodeGroup_id = 11
IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc)
		VALUES (11, 'INCREASE_BONUS_SHOPPINGFLING')
	END

DELETE FROM mappingtrancodegroup where sid_rniTrancodeGroup_id = 11

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode)
SELECT 11, 'F0'
UNION SELECT 11, 'G0'

-----------------------------------------------------------
--INCREASE_BONUS_<TRANCODE> (12-79)
-----------------------------------------------------------

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (12, 'INCREASE_BONUS_0A')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (12, '0A')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (13, 'INCREASE_BONUS_0B')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (13, '0B')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (14, 'INCREASE_BONUS_0C')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (14, '0C')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (15, 'INCREASE_BONUS_0D')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (15, '0D')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (16, 'INCREASE_BONUS_0E')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (16, '0E')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (17, 'INCREASE_BONUS_0F')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (17, '0F')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (18, 'INCREASE_BONUS_0G')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (18, '0G')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (19, 'INCREASE_BONUS_0H')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (19, '0H')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (20, 'INCREASE_BONUS_0I')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (20, '0I')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (21, 'INCREASE_BONUS_0J')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (21, '0J')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (22, 'INCREASE_BONUS_0K')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (22, '0K')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (23, 'INCREASE_BONUS_B1')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (23, 'B1')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (24, 'INCREASE_BONUS_BA')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (24, 'BA')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (25, 'INCREASE_BONUS_BC')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (25, 'BC')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (26, 'INCREASE_BONUS_BE')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (26, 'BE')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (27, 'INCREASE_BONUS_BF')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (27, 'BF')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (28, 'INCREASE_BONUS_BG')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (28, 'BG')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (29, 'INCREASE_BONUS_BH')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (29, 'BH')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (30, 'INCREASE_BONUS_BI')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (30, 'BI')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (31, 'INCREASE_BONUS_BJ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (31, 'BJ')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (32, 'INCREASE_BONUS_BK')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (32, 'BK')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (33, 'INCREASE_BONUS_BL')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (33, 'BL')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (34, 'INCREASE_BONUS_BM')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (34, 'BM')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (35, 'INCREASE_BONUS_BN')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (35, 'BN')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (36, 'INCREASE_BONUS_BP')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (36, 'BP')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (37, 'INCREASE_BONUS_BR')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (37, 'BR')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (38, 'INCREASE_BONUS_BS')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (38, 'BS')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (39, 'INCREASE_BONUS_BT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (39, 'BT')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (40, 'INCREASE_BONUS_BV')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (40, 'BV')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (41, 'INCREASE_BONUS_G4')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (41, 'G4')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (42, 'INCREASE_BONUS_G5')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (42, 'G5')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (43, 'INCREASE_BONUS_G6')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (43, 'G6')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (44, 'INCREASE_BONUS_RQ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (44, 'RQ')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (45, 'INCREASE_BONUS_F1')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (45, 'F1')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (46, 'INCREASE_BONUS_F2')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (46, 'F2')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (47, 'INCREASE_BONUS_F3')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (47, 'F3')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (48, 'INCREASE_BONUS_F4')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (48, 'F4')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (49, 'INCREASE_BONUS_F5')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (49, 'F5')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (50, 'INCREASE_BONUS_F6')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (50, 'F6')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (51, 'INCREASE_BONUS_F7')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (51, 'F7')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (52, 'INCREASE_BONUS_F8')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (52, 'F8')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (53, 'INCREASE_BONUS_FA')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (53, 'FA')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (54, 'INCREASE_BONUS_FB')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (54, 'FB')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (55, 'INCREASE_BONUS_FC')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (55, 'FC')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (56, 'INCREASE_BONUS_FD')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (56, 'FD')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (57, 'INCREASE_BONUS_FE')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (57, 'FE')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (58, 'INCREASE_BONUS_FF')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (58, 'FF')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (59, 'INCREASE_BONUS_FG')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (59, 'FG')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (60, 'INCREASE_BONUS_FH')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (60, 'FH')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (61, 'INCREASE_BONUS_FI')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (61, 'FI')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (62, 'INCREASE_BONUS_FJ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (62, 'FJ')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (63, 'INCREASE_BONUS_FK')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (63, 'FK')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (64, 'INCREASE_BONUS_FL')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (64, 'FL')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (65, 'INCREASE_BONUS_FM')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (65, 'FM')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (66, 'INCREASE_BONUS_FN')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (66, 'FN')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (67, 'INCREASE_BONUS_FO')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (67, 'FO')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (68, 'INCREASE_BONUS_FP')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (68, 'FP')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (69, 'INCREASE_BONUS_FQ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (69, 'FQ')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (70, 'INCREASE_BONUS_FS')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (70, 'FS')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (71, 'INCREASE_BONUS_FT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (71, 'FT')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (72, 'INCREASE_BONUS_FU')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (72, 'FU')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (73, 'INCREASE_BONUS_FV')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (73, 'FV')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (74, 'INCREASE_BONUS_FW')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (74, 'FW')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (75, 'INCREASE_BONUS_FX')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (75, 'FX')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (76, 'INCREASE_BONUS_FY')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (76, 'FY')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (77, 'INCREASE_BONUS_FZ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (77, 'FZ')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (78, 'INCREASE_BONUS_F0')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (78, 'F0')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (79, 'INCREASE_BONUS_G0')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (79, 'G0')

-----------------------------------------------------------	
--INCREASE_RETURNREVERSAL (80)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (80, 'INCREASE_RETURNREVERSAL')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (80, '32')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (80, 'INCREASE_RETURNREVERSAL')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (80, '36')

-----------------------------------------------------------	
--INCREASE_RETURNREVERSAL_DEBIT (81)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (81, 'INCREASE_RETURNREVERSAL_DEBIT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (81, '36')

-----------------------------------------------------------	
--INCREASE_RETURNREVERSAL_CREDIT (82)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (82, 'INCREASE_RETURNREVERSAL')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (82, '32')


-----------------------------------------------------------	
--INCREASE_ADJUSTMENT (83)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (83, 'INCREASE_ADJUSTMENT')

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 83, 'II'
UNION SELECT 83, 'IE'
UNION SELECT 83, 'DR'
UNION SELECT 83, 'DZ'
UNION SELECT 83, 'XF'

-----------------------------------------------------------	
--INCREASE_ADJUSTMENT_<TRANCODE> (84-88)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (84, 'INCREASE_ADJUSTMENT_II')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (84, 'II')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (85, 'INCREASE_ADJUSTMENT_IE')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (85, 'IE')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (86, 'INCREASE_ADJUSTMENT_DR')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (86, 'DR')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (87, 'INCREASE_ADJUSTMENT_DZ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (87, 'DZ')

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (88, 'INCREASE_ADJUSTMENT_XF')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (88, 'XF')


-----------------------------------------------------------	
--INCREASE_TRANSFER (89)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (89, 'INCREASE_TRANSFER')

INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 89, 'TI'
UNION SELECT 89, 'TP'

-----------------------------------------------------------	
--INCREASE_2PT (90)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (90, 'INCREASE_2PT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (90, 'G2')

-----------------------------------------------------------	
--INCREASE_3PT (91)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (91, 'INCREASE_3PT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) VALUES (91, 'G3')


-----------------------------------------------------------	
--DECREASE_RETURN (92)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (92, 'DECREASE_RETURN')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 92, '31'
UNION SELECT 92, '33'
UNION SELECT 92, '3M'
UNION SELECT 92, '34'
UNION SELECT 92, '35'
UNION SELECT 92, '37'
UNION SELECT 92, '3B'
UNION SELECT 92, '3H'
UNION SELECT 92, '38'
UNION SELECT 92, '39'
UNION SELECT 92, 'G7'
UNION SELECT 92, 'G8'
UNION SELECT 92, 'GX'

-----------------------------------------------------------	
--DECREASE_RETURN_CREDIT (93)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (93, 'DECREASE_RETURN_CREDIT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 93, '31'
UNION SELECT 93, '33'
UNION SELECT 93, '3M'

-----------------------------------------------------------	
--DECREASE_RETURN_DEBIT (94)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (94, 'DECREASE_RETURN_DEBIT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 94, '34'
UNION SELECT 94, '35'
UNION SELECT 94, '37'
UNION SELECT 94, '3B'

-----------------------------------------------------------	
--DECREASE_RETURN_HELOC (95)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (95, 'DECREASE_RETURN_HELOC')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 95, '3H'

-----------------------------------------------------------	
--DECREASE_RETURN_2PT (96)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (96, 'DECREASE_RETURN_2PT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 96, 'G7'

-----------------------------------------------------------	
--DECREASE_RETURN_3PT (97)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (97, 'DECREASE_RETURN_3PT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 97, 'G8'

-----------------------------------------------------------	
--DECREASE_RETURN_CORPORATE (98)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (98, 'DECREASE_RETURN_CORPORATE')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 98, 'GX'

-----------------------------------------------------------	
--DECREASE_RETURN_OTHER (99)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (99, 'DECREASE_RETURN_OTHER')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 99, '38'
UNION SELECT 99, '39'
		

-----------------------------------------------------------	
--DECREASE_REVERSAL (100)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (100, 'DECREASE_REVERSAL')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 100, '62'
UNION SELECT 100, '66'
UNION SELECT 100, 'BX'
UNION SELECT 100, 'F9'
UNION SELECT 100, 'G9'
UNION SELECT 100, 'FR'

-----------------------------------------------------------	
--DECREASE_REVERSAL_CREDIT (101)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (101, 'DECREASE_REVERSAL_CREDIT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 101, '62'

-----------------------------------------------------------	
--DECREASE_REVERSAL_DEBIT (102)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (102, 'DECREASE_REVERSAL_DEBIT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 102, '66'

-----------------------------------------------------------	
--DECREASE_REVERSAL_FULLSPECTRUM (103)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (103, 'DECREASE_REVERSAL_FULLSPECTRUM')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 103, 'FR'

-----------------------------------------------------------	
--DECREASE_REVERSAL_BONUS (104)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (104, 'DECREASE_REVERSAL_BONUS')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 104, 'BX'
UNION SELECT 104, 'F9'
UNION SELECT 104, 'G9'


-----------------------------------------------------------	
--DECREASE_REVERSAL_BONUS_NOSF (105)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (105, 'DECREASE_REVERSAL_BONUS_NOSF')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 105, 'BX'


-----------------------------------------------------------	
--DECREASE_REVERSAL_BONUS_SHOPPINGFLING (106)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (106, 'DECREASE_REVERSAL_BONUS_SHOPPINGFLING')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 106, 'F9'
UNION SELECT 106, 'G9'


-----------------------------------------------------------	
--DECREASE_ADJUSTMENT (107)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (107, 'DECREASE_ADJUSTMENT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 107, 'DE'
UNION SELECT 107, 'IR'
UNION SELECT 107, 'XP'

-----------------------------------------------------------	
--DECREASE_ADJUSTMENT_<TRANCODE> (108-110)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (108, 'DECREASE_ADJUSTMENT_DE')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 108, 'DE'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (109, 'DECREASE_ADJUSTMENT_IR')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 109, 'IR'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (110, 'DECREASE_ADJUSTMENT_XP')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 110, 'XP'


-----------------------------------------------------------	
--DECREASE_REDEMPTION (111)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (111, 'DECREASE_REDEMPTION')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 111, 'RB'
UNION SELECT 111, 'RC'
UNION SELECT 111, 'RD'
UNION SELECT 111, 'RF'
UNION SELECT 111, 'RG'
UNION SELECT 111, 'RI'
UNION SELECT 111, 'RK'
UNION SELECT 111, 'RM'
UNION SELECT 111, 'RP'
UNION SELECT 111, 'RR'
UNION SELECT 111, 'RS'
UNION SELECT 111, 'RT'
UNION SELECT 111, 'RU'
UNION SELECT 111, 'RV'
UNION SELECT 111, 'RZ'

-----------------------------------------------------------	
--DECREASE_REDEMPTION_<TRANCODE> (112-126)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (112, 'DECREASE_REDEMPTION_RB')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 112, 'RB'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (113, 'DECREASE_REDEMPTION_RC')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 113, 'RC'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (114, 'DECREASE_REDEMPTION_RD')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 114, 'RD'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (115, 'DECREASE_REDEMPTION_RF')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 115, 'RF'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (116, 'DECREASE_REDEMPTION_RG')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 116, 'RG'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (117, 'DECREASE_REDEMPTION_RI')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 117, 'RI'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (118, 'DECREASE_REDEMPTION_RK')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 118, 'RK'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (119, 'DECREASE_REDEMPTION_RM')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 119, 'RM'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (120, 'DECREASE_REDEMPTION_RP')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 120, 'RP'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (121, 'DECREASE_REDEMPTION_RR')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 121, 'RR'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (122, 'DECREASE_REDEMPTION_RS')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 122, 'RS'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (123, 'DECREASE_REDEMPTION_RT')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 123, 'RT'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (124, 'DECREASE_REDEMPTION_RU')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 124, 'RU'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (125, 'DECREASE_REDEMPTION_RV')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 125, 'RV'

INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (126, 'DECREASE_REDEMPTION_RZ')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 126, 'RZ'

-----------------------------------------------------------	
--DECREASE_TRANSFER (127)
-----------------------------------------------------------	
INSERT INTO rniTrancodeGroup (sid_rniTrancodeGroup_id, dim_rniTrancodeGroup_desc) VALUES (127, 'DECREASE_TRANSFER')
INSERT INTO mappingtrancodegroup (sid_rniTrancodeGroup_id, sid_trantype_trancode) 
SELECT 127, 'TD'


SET IDENTITY_INSERT rniTrancodeGroup OFF
GO