USE [RewardsNow]
GO

IF OBJECT_ID(N'zzzDeleteAfter_20120215_RptCtlClients') IS NOT NULL
	DROP TABLE zzzDeleteAfter_20120215_RptCtlClients
GO


SELECT * INTO zzzDeleteAfter_20120215_RptCtlClients FROM RptCtlClients
go

CREATE VIEW tmp_RptCtlClients
AS
SELECT 
	DBNameNEXL AS RnName
	, DBNumber AS ClientNum
	, ClientName AS FormalName
	, QUOTENAME(DBNamePatton) as ClientDBName
	, QUOTENAME(@@SERVERNAME) as ClientDBLocation
	, QUOTENAME(DBNameNEXL) as OnlClientDBName
	, '[RN1]' as OnlClientDBLocation
FROM dbprocessinfo
GO

DROP TABLE RptCtlClients
GO

SP_RENAME 'tmp_RptCtlClients', 'RptCtlClients'
GO



 