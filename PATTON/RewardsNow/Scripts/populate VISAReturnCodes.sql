USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[VISAReturnCodes] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[VISAReturnCodes]([VISAReturnCodes_identity], [ReasonCode], [REasonCodeDescription])
SELECT 1, N'A1', N'Invalid Action' UNION ALL
SELECT 2, N'A2', N'Last Name Missing' UNION ALL
SELECT 3, N'A3', N'First Name Missing' UNION ALL
SELECT 4, N'A4', N'Last 4 digits SSN invalid' UNION ALL
SELECT 5, N'A5', N'Add record already exists' UNION ALL
SELECT 6, N'A6', N'change record received for an account number that does not exist' UNION ALL
SELECT 7, N'A7', N'Account number invalid' UNION ALL
SELECT 8, N'A8', N'Account number is not in an eligible account range for this issuer' UNION ALL
SELECT 9, N'A9', N'Invalid account status' UNION ALL
SELECT 10, N'B0', N'Account status is missing on add' UNION ALL
SELECT 11, N'B1', N'Effective Date is not a valid date' UNION ALL
SELECT 12, N'B2', N'Name prefix is invalid' UNION ALL
SELECT 13, N'B3', N'Name suffix is invalid' UNION ALL
SELECT 14, N'B4', N'Address line 1 missing on change to field' UNION ALL
SELECT 15, N'B5', N'City is missing on change to field' UNION ALL
SELECT 16, N'B6', N'State code in invalid' UNION ALL
SELECT 17, N'B7', N'Statement ZIP Code is invalid' UNION ALL
SELECT 18, N'B8', N'EMail address is invalid' UNION ALL
SELECT 19, N'B9', N'Phone number is invalid' UNION ALL
SELECT 20, N'C1', N'Rewards Program ID invalid' UNION ALL
SELECT 21, N'C2', N'Product ID invalid' UNION ALL
SELECT 22, N'DA', N'Account in CMF is currently active under a different sender ID' UNION ALL
SELECT 23, N'DB', N'Product is not registered for this Sender ID' UNION ALL
SELECT 24, N'DC', N'Sender ID for Type 1 Header record is invalid' UNION ALL
SELECT 25, N'DD', N'Sender ID for Type 2 Header record is invalid' UNION ALL
SELECT 26, N'DE', N'CMF input not allowed for this Rewards Program Identification Number (RPIN)' UNION ALL
SELECT 27, N'D2', N'Preferred Method of Contact invalid' UNION ALL
SELECT 28, N'D5', N'Account is not in a registered account range and no Program ID' UNION ALL
SELECT 29, N'D6', N'Missing Program ID' UNION ALL
SELECT 30, N'D7', N'Account is not in a registered account range' UNION ALL
SELECT 31, N'D8', N'Delete record received for an account number that does not exist' UNION ALL
SELECT 32, N'D9', N'Email address is missing' UNION ALL
SELECT 33, N'G6', N'Mobile telephone number is missing' UNION ALL
SELECT 34, N'G7', N'Mobile telephone number is invalid' UNION ALL
SELECT 35, N'G8', N'Account opened date is invalid' UNION ALL
SELECT 36, N'G9', N'Account opened date is missing' UNION ALL
SELECT 37, N'H1', N'Invalid Visa FeatureSelect RPIN portfolio effective date' UNION ALL
SELECT 38, N'I1', N'Account number in the submitted file is not in the visa database' UNION ALL
SELECT 39, N'I2', N'Account number is in the visa database but not in the submitted file' UNION ALL
SELECT 40, N'I3', N'Account number is in the visa database and in the submitted file but the data is different' UNION ALL
SELECT 41, N'LZ', N'Other account linking related error' UNION ALL
SELECT 42, N'L2', N'Link reason code invalid' UNION ALL
SELECT 43, N'L3', N'Unlink indicator in invalid' UNION ALL
SELECT 44, N'L4', N'Linked Card number is specified bur both link reason code and unlink indicator contain spaces' UNION ALL
SELECT 45, N'L6', N'Cannot have values in both link reason code and unlink indicator fields' UNION ALL
SELECT 46, N'L7', N'Linked Card number invalid' UNION ALL
SELECT 47, N'L8', N'Linked card number is not in an eligible account range for this issuer'
COMMIT;
RAISERROR (N'[dbo].[VISAReturnCodes]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[VISAReturnCodes] OFF;

