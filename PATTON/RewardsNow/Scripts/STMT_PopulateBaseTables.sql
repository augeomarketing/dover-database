USE RewardsNow
GO

SET IDENTITY_INSERT mappingtable ON
GO

--Populate mappingtable for welcomekits
INSERT INTO mappingtable (sid_mappingtable_id, dim_mappingtable_tablename, dim_mappingtable_active)
SELECT 6, 'history', 1
UNION ALL SELECT 7, 'welcomekit', 1
GO

SET IDENTITY_INSERT mappingtable OFF

