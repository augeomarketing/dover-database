USE [RewardsNow]
GO

PRINT 'Populating RNITransactionAux - Start'

TRUNCATE TABLE RNITransactionAuxKey

INSERT INTO RNITransactionAuxKey (dim_rnitransactionauxkey_name, dim_rnitransactionauxkey_desc)
VALUES ('SIC', 'Standard Industrial Classification Code')
	, ('MERCHANTNAME', 'Merchant Name')
	, ('MERCHANTLOCATION', 'Merchant Location')

PRINT 'Populating RNITransactionAux - End'
