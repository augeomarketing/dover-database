USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'CentralProcessing', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RewardsNow_DB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CentralProcessing', N'Data Source=patton\rn;Initial Catalog=;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[FI_DB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CentralProcessing_PostAudit', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[RewardsNow_DB].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'CentralProcessing_PostAudit', N'Data Source=patton\rn;Initial Catalog=Maintenance;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;', N'\Package.Connections[Maintenance_DB].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

