USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where configurationfilter = 'VesdiaAccrualsWithExtractFile'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFile', N'REB_SFPENDINGTRANS_', N'\Package.Variables[User::SourceFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Variables[User::RowCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Variables[User::RecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Variables[User::FileToBeRenamed].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\REB_SFPENDINGTRANS_20120813.csv', N'\Package.Variables[User::DestFilePath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Variables[User::DestFilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'@[User::FilePath]  +  @[User::SourceFileName]  + (DT_STR, 4, 1252) YEAR(GETDATE())  +  RIGHT("00" +  (DT_STR, 2, 1252) MONTH(GETDATE()),2) +  RIGHT("00" + (DT_STR, 2, 1252) DAY(GETDATE()),2)    + ".csv"', N'\Package.Variables[User::DestFilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'207_SFPENDINGTRANS_20120101.CSV', N'\Package.Variables[User::DestFileName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Variables[User::CtlVersion].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Variables[User::CtlRecordCount].Properties[Value]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Variables[User::CtlDbNumber].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Variables[User::CtlClientId].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Variables[User::ClientIdName].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\002_SFPENDINGTRANS_20120618.CSV', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'@[User::FilePath]  +   @[User::ArchiveCSVFile]', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Variables[User::ArchiveSourceFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\002_SF_20120618.CTL', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'@[User::FilePath]  +   @[User::ArchiveCTLFile]', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[Expression]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Variables[User::ArchiveSourceCTLFullPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\Archive_PendingExtracts\', N'\Package.Variables[User::ArchiveDestinationPath].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'002_SF_20120618.CTL', N'\Package.Variables[User::ArchiveCTLFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'002_SFPENDINGTRANS_20120618.CSV', N'\Package.Variables[User::ArchiveCSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-VesdiaAccrualsWithExtractFile-{C8C57154-CE9A-4F5A-B6D6-AF6CB8B610E6}RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[Exportfile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'"', N'\Package.Connections[Exportfile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[Exportfile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1', N'\Package.Connections[Exportfile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Exportfile', N'\Package.Connections[Exportfile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1033', N'\Package.Connections[Exportfile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Exportfile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'
', N'\Package.Connections[Exportfile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Delimited', N'\Package.Connections[Exportfile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Exportfile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[Exportfile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Exportfile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'\\patton\ops\Vesdia\Output\ExportFile.csv', N'\Package.Connections[Exportfile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Connections[Exportfile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1252', N'\Package.Connections[Exportfile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[controlFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'"', N'\Package.Connections[controlFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[controlFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1', N'\Package.Connections[controlFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'controlFile', N'\Package.Connections[controlFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1033', N'\Package.Connections[controlFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'
', N'\Package.Connections[controlFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Delimited', N'\Package.Connections[controlFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[controlFile].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[controlFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\_SF_20120813.ctl', N'\Package.Connections[controlFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Connections[controlFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1252', N'\Package.Connections[controlFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[ClientExportFile].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'"', N'\Package.Connections[ClientExportFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[ClientExportFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1', N'\Package.Connections[ClientExportFile].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'ClientExportFile', N'\Package.Connections[ClientExportFile].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1033', N'\Package.Connections[ClientExportFile].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'
', N'\Package.Connections[ClientExportFile].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Delimited', N'\Package.Connections[ClientExportFile].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[ClientExportFile].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[ClientExportFile].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\_SFPENDINGTRANS_20120813.csv', N'\Package.Connections[ClientExportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'True', N'\Package.Connections[ClientExportFile].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1252', N'\Package.Connections[ClientExportFile].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[Accrual File].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'<none>', N'\Package.Connections[Accrual File].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[Accrual File].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1', N'\Package.Connections[Accrual File].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'Accrual File', N'\Package.Connections[Accrual File].Properties[Name]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1033', N'\Package.Connections[Accrual File].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Accrual File].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'
', N'\Package.Connections[Accrual File].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'RaggedRight', N'\Package.Connections[Accrual File].Properties[Format]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Accrual File].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'', N'\Package.Connections[Accrual File].Properties[Description]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'0', N'\Package.Connections[Accrual File].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'\\patton\ops\Vesdia\Input\AccrualFile.csv', N'\Package.Connections[Accrual File].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'False', N'\Package.Connections[Accrual File].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'1252', N'\Package.Connections[Accrual File].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'VesdiaAccrualsWithExtractFile', N'O:\Vesdia\Output\REB_SFPENDINGTRANS_XXXX.csv', N'\Package.Variables[User::SourceFilePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

