USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

delete [SSIS Configurations] where ConfigurationFilter ='AzigoSendActualFile'

go


BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AzigoSendActualFile', N'Delimited', N'\Package.Connections[ClientExportFile].Properties[Format]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\test.csv.pgp', N'\Package.Variables[User::DeletePGP].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\test.csv', N'\Package.Variables[User::DeletePath].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\250_SF_20131106.ctl', N'\Package.Variables[User::CTLDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\250_SFPENDINGTRANS_20131106.csv', N'\Package.Variables[User::CSVDestinationFile].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'', N'\Package.Connections[Flat File Connection Manager].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'Delimited', N'\Package.Connections[Flat File Connection Manager].Properties[Format]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\output.csv', N'\Package.Connections[Flat File Connection Manager].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'', N'\Package.Connections[controlFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\controlFile.ctl', N'\Package.Variables[User::ControlSource].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\Archive', N'\Package.Variables[User::ArchiveDestination].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\', N'\Package.Variables[User::SourcePath].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\clientExportFile.csv', N'\Package.Variables[User::SourceFullPath].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\itops\Encryption Keys\KeyRing\pubring.gpg', N'\Package.Connections[pubring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'<none>', N'\Package.Connections[Flat File Connection Manager].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\clientExportFile.csv', N'\Package.Connections[ClientExportFile].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'"', N'\Package.Connections[controlFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'', N'\Package.Connections[ClientExportFile].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Logs\AzigoSendActualProcessingLog.txt', N'\Package.Connections[AzigoSendActualProcessingLog.txt].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AzigoProcessTransactions-{351D269E-1B64-455C-A226-F6AD746A7636}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\test.csv', N'\Package.Variables[User::pgpSourceFileAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\test.csv.pgp', N'\Package.Variables[User::pgpDestination].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\web3\users\250SimpleTuition\FROM_RN\test.csv.pgp', N'\Package.Variables[User::FTPPathAndFile].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\web3\users\250SimpleTuition\FROM_RN\', N'\Package.Variables[User::FTP_Path].Properties[Value]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'"', N'\Package.Connections[ClientExportFile].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'Delimited', N'\Package.Connections[controlFile].Properties[Format]', N'String' UNION ALL
SELECT N'AzigoSendActualFile', N'\\patton\ops\Azigo\Output\controlFile.ctl', N'\Package.Connections[controlFile].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

