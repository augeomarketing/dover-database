USE RewardsNow
GO

ALTER TABLE processingjobstatus
ADD dim_processingjobstatus_name VARCHAR(20)
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'QUEUE'
WHERE dim_processingjobstatus_description = 'In Queue'
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'PROCESSING'
WHERE dim_processingjobstatus_description = 'Processing'
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'SUCCESS'
WHERE dim_processingjobstatus_description = 'Completed successfully'
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'WARN'
WHERE dim_processingjobstatus_description = 'Completed with warnings'
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'FAILED'
WHERE dim_processingjobstatus_description = 'FAILED'
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'DATA'
WHERE dim_processingjobstatus_description = 'Awaiting Data Generation'
GO

UPDATE processingjobstatus
SET dim_processingjobstatus_name = 'OUTPUT'
WHERE dim_processingjobstatus_description = 'Awaiting File Output'
GO

