USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Main', N'172.20.141.175', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerHost]', N'String' UNION ALL
SELECT N'AD_Main', N'True', N'\Package.Variables[User::PathAndName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'True', N'\Package.Connections[SSH Connection Manager 1].Properties[TransferBinary]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'rewards-now', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerUser]', N'String' UNION ALL
SELECT N'AD_Main', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerType]', N'Int32' UNION ALL
SELECT N'AD_Main', N'60', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerTimeout]', N'Int32' UNION ALL
SELECT N'AD_Main', N'22', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPort]', N'Int32' UNION ALL
SELECT N'AD_Main', N'sgm!9tFGK$$9', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerPassword]', N'String' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ServerKeyFile]', N'String' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::FIP].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'AD-RN-', N'\Package.Variables[User::FileBeginsWith].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'User', N'\Package.Variables[User::FileBeginsWith].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Main', N'FileBeginsWith', N'\Package.Variables[User::FileBeginsWith].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FileBeginsWith].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::FileBeginsWith].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Connections[SSH Connection Manager 1].Properties[RetainSameConnection]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyUser]', N'String' UNION ALL
SELECT N'AD_Main', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyType]', N'Int32' UNION ALL
SELECT N'AD_Main', N'80', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPort]', N'Int32' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyPassword]', N'String' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[ProxyHost]', N'String' UNION ALL
SELECT N'AD_Main', N'0', N'\Package.Connections[SSH Connection Manager 1].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Main', N'SSH Connection Manager 1', N'\Package.Connections[SSH Connection Manager 1].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::PathAndName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[LogFile]', N'String' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[SSH Connection Manager 1].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'2', N'\Package.Connections[SSH Connection Manager 1].Properties[BackendVersion]', N'Int32' UNION ALL
SELECT N'AD_Main', N'1', N'\Package.Connections[Input].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Main', N'Input', N'\Package.Connections[Input].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'2', N'\Package.Connections[Input].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Connections[Input].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'\\patton\ops\AccessDevelopment\Input', N'\Package.Connections[Input].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Main', N'/from_access/', N'\Package.Variables[User::Path].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Main', N'ServerHost=172.20.141.175;ServerPort=22;ServerUser=rewards-now;ServerKeyFile=;ServerTimeout=60;LogFile=;TransferBinary=True;ServerType=Unknown;RetainSameConnection=False;ProxyType=None;ProxyHost=;ProxyPort=80;ProxyUser=;', N'\Package.Connections[SSH Connection Manager 1].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Main', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Main-{221B4AEF-7D54-4F01-A6CA-DA61DC2F2FB7}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::PathAndName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::PathAndName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'@[User::Path]  +   @[User::FileBeginsWith] +  @[User::FIP]   + "-"  +  @[User::NextFileDateToPull]  +  "*"', N'\Package.Variables[User::PathAndName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::Path].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'Path', N'\Package.Variables[User::Path].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::Path].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::Path].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::Path].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::Path].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'20120910', N'\Package.Variables[User::NextFileDateToPull].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Main', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'User', N'\Package.Variables[User::NextFileDateToPull].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Main', N'NextFileDateToPull', N'\Package.Variables[User::NextFileDateToPull].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::NextFileDateToPull].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::NextFileDateToPull].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'20120910', N'\Package.Variables[User::LastFileDatePulled].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'User', N'\Package.Variables[User::LastFileDatePulled].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Main', N'LastFileDatePulled', N'\Package.Variables[User::LastFileDatePulled].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::LastFileDatePulled].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::LastFileDatePulled].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Main', N'2199', N'\Package.Variables[User::FIP].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FIP].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FIP].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'User', N'\Package.Variables[User::FIP].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Main', N'FIP', N'\Package.Variables[User::FIP].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FIP].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'', N'\Package.Variables[User::FIP].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::FIP].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'/from_access/AD-RN-2199-20120910*', N'\Package.Variables[User::PathAndName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::PathAndName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'False', N'\Package.Variables[User::Path].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Main', N'User', N'\Package.Variables[User::PathAndName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Main', N'User', N'\Package.Variables[User::Path].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Main', N'PathAndName', N'\Package.Variables[User::PathAndName].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

