USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO
delete [SSIS Configurations] where ConfigurationFilter like 'ad_merchant%'
go
BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'Merchant.csv', N'\Package.Connections[Merchant.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package.Connections[Merchant.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Connections[Merchant.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Connections[Merchant.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Merchant', N'20121212', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Connections[MerchantEdited.csv].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'"', N'\Package.Connections[MerchantEdited.csv].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Merchant', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Channel-{8F84622A-DC7A-4A28-9D87-E91119746BCB}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Merchant', N'0', N'\Package.Connections[MerchantEdited.csv].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'1252', N'\Package.Connections[MerchantEdited.csv].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\UNZip Task.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'MERCHANT-DATA', N'\Package\Foreach Loop Container\UNZip Task.Properties[Target]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\UNZip Task.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\UNZip Task.Properties[Password]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'UNZip Task', N'\Package\Foreach Loop Container\UNZip Task.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'2', N'\Package\Foreach Loop Container\UNZip Task.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'1033', N'\Package\Foreach Loop Container\UNZip Task.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Connections[MerchantEdited.csv].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_Merchant', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_Merchant', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_Merchant', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'OpenPGP Task  - Decrypt Merchant  File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Merchant', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package.Connections[MerchantEdited.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'MerchantEdited.csv', N'\Package.Connections[MerchantEdited.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'1033', N'\Package.Connections[MerchantEdited.csv].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'0', N'\Package.Connections[MerchantEdited.csv].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Merchant', N'
', N'\Package.Connections[MerchantEdited.csv].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Merchant', N'Delimited', N'\Package.Connections[MerchantEdited.csv].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Merchant', N'0', N'\Package.Connections[MerchantEdited.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Connections[MerchantEdited.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantHistory', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-MerchantHistory-{C917C335-8062-40D4-865B-25BA4BC64965}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Merchant', N'\\patton\ops\AccessDevelopment\Input\MerchantEdited.csv', N'\Package.Connections[MerchantEdited.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'True', N'\Package.Connections[MerchantEdited.csv].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1048576', N'\Package\Foreach Loop Container\UNZip Task.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\UNZip Task.Properties[IncludeSubfolders]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'-1', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForceExecutionResult]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[EncryptionType]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'Zip Task', N'\Package\Foreach Loop Container\UNZip Task.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\UNZip Task.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[CompressionType]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\UNZip Task.Properties[CompressionLevel]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\UNZip Task.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Merchant', N'1', N'\Package.Connections[Merchant.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Merchant', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Merchant', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Merchant', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 6.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'20121224', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 7.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'OpenPGP Task  - Decrypt Merchant  File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[KeyRing]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 8.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Merchant  File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'20121224', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 9.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 10.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'20121224', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 11.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'3', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'File System Task - Rename for connection mgr', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[FailPackageOnFailure]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 12.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User::CSVFile', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Rename for connection mgr.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'20121224', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 13.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 14.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'20121224', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 15.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 16.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-AD_Channel-{8F84622A-DC7A-4A28-9D87-E91119746BCB}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Connections[MerchantEdited.csv].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'"', N'\Package.Connections[MerchantEdited.csv].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Connections[MerchantEdited.csv].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package.Connections[MerchantEdited.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'MerchantEdited.csv', N'\Package.Connections[MerchantEdited.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1033', N'\Package.Connections[MerchantEdited.csv].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package.Connections[MerchantEdited.csv].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'
', N'\Package.Connections[MerchantEdited.csv].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'Delimited', N'\Package.Connections[MerchantEdited.csv].Properties[Format]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package.Connections[MerchantEdited.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Connections[MerchantEdited.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'0', N'\Package.Connections[MerchantEdited.csv].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\MerchantEdited.csv', N'\Package.Connections[MerchantEdited.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Connections[MerchantEdited.csv].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'1252', N'\Package.Connections[MerchantEdited.csv].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package.Connections[Merchant.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'Merchant.csv', N'\Package.Connections[Merchant.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'1', N'\Package.Connections[Merchant.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Connections[Merchant.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Connections[Merchant.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'20121224', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 17.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'AD-RN-2199*Merchant.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +    @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'test.csv', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 18.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Merchant.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'@[User::FilePath]  +  "Merchant.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantStage', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantStage', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Recurse]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'{D6ED15E4-8FE8-4CA9-8817-C1EB52FA361D}', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileSpec]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[FileNameRetrieval]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Directory]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.ForEachEnumerator.Properties[CollectionEnumerator]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'1033', N'\Package\Foreach Loop Container.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'1048576', N'\Package\Foreach Loop Container.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'-1', N'\Package\Foreach Loop Container.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'Foreach Loop Container', N'\Package\Foreach Loop Container.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 19.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'ST_5568bb1cfd6542a584ce40139134950d', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ScriptProjectName]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'Microsoft Visual Basic 2008', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ScriptLanguage]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'User::FileSize', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ReadWriteVariables]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ReadOnlyVariables]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'Script Task - Get File Size     If file is empty we will not unzip in next step', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[LoggingMode]', N'Object'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 20.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'1033', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'1048576', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'-1', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'Main', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[EntryPoint]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'Script Task', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\Script Task - Get File Size     If file is empty we will not unzip in next step.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 21.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[PublicKeyRing]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[OutputASCII]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'OpenPGP Task  - Decrypt Organization DATA File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Name]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 22.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsPublicKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 23.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 24.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'Variable', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Organization DATA File.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 25.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'2', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'File System Task - Delete pgp file', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'1033', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'1048576', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'-1', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DelayValidation]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 26.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Delete pgp file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 27.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'Variable', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Variables[User::Variable].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Source]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OverwriteDestinationFile]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[OperationName]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Operation]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'File System Task - Archive decrypted file', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'1033', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsSourcePathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'1048576', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[IsDestinationPathVariable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'-1', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailParentOnFailure]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 28.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User::ArchivePath', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Destination]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'File System Task', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package\Foreach Loop Container\File System Task - Archive decrypted file.Properties[Attributes]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Application Name=SSIS-MerchantData-{B85DCE9B-7029-4B33-B9E6-3F9BB38BC337}patton\rn.RewardsNow;Auto Translate=False;', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\itops\Encryption Keys\KeyRing\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'1', N'\Package.Connections[MERCHANT-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'MERCHANT-DATA', N'\Package.Connections[MERCHANT-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'2', N'\Package.Connections[MERCHANT-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Connections[MERCHANT-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\MERCHANT-DATA', N'\Package.Connections[MERCHANT-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]   +   @[User::FileName]', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 29.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_MerchantData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'AD-RN-2199*MERCHANT-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'@[User::FilePath]  +   @[User::DecryptedFileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'test.zip', N'\Package.Variables[User::DecryptedFileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::DecryptedFileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'DecryptedFileName', N'\Package.Variables[User::DecryptedFileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::DecryptedFileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'REPLACE(@[User::FileName], ".pgp","")', N'\Package.Variables[User::DecryptedFileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'True', N'\Package.Variables[User::DecryptedFileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::DecryptedFileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'\\patton\ops\AccessDevelopment\Input\Archive\Merchant-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_MerchantData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_MerchantData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 30.....Done!', 10, 1) WITH NOWAIT;
GO

