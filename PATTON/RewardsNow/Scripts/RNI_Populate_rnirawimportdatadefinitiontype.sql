﻿USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[rnirawimportdatadefinitiontype] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[rnirawimportdatadefinitiontype]([sid_rnirawimportdatadefinitiontype_id], [dim_rnirawimportdatadefinitiontype_name], [dim_rnirawimportdatadefinitiontype_desc], [dim_rnirawimportdatadefinitiontype_dateadded], [dim_rnirawimportdatadefinitiontype_lastmodified], [dim_rnirawimportdatadefinitiontype_lastmodifiedby])
SELECT 1, N'SOURCE', N'Definition Defines Data as it applies to the source file or table', '20110727 13:52:38.760', '20110727 13:52:38.763', N'REWARDSNOW\cheit' UNION ALL
SELECT 2, N'TARGET', N'Definition Defines Data as it applies to a target table', '20110727 13:52:38.760', '20110727 13:52:38.763', N'REWARDSNOW\cheit'
COMMIT;
RAISERROR (N'[dbo].[rnirawimportdatadefinitiontype]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[rnirawimportdatadefinitiontype] OFF;

