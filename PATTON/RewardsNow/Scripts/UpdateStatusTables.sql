USE [RewardsNow]
GO

select * from INFORMATION_SCHEMA.TABLES where TABLE_NAME like '%status%'

select * from RNICustomerCodeStatus
select * from RNICustomerCodeStatusProperty

update RNICustomerCodeStatusProperty
set dim_rnicustomercodestatusproperty_name = 'PREVENT_HOUSEHOLD'
	, dim_rnicustomercodestatusproperty_desc = 'RECORDS IN THIS STATUS CANNOT BE HOUSEHOLDED TO'
where sid_rnicustomercodestatusproperty_id = 5

update RNICustomerCodeStatusProperty
set dim_rnicustomercodestatusproperty_name = 'PREVENT_UPDATE'
	, dim_rnicustomercodestatusproperty_desc = 'RECORDS IN THIS STATUS CANNOT BE UPDATED DURING UPSERT FROM SOURCE.'
where sid_rnicustomercodestatusproperty_id = 6

update RNICustomerCodeStatusProperty
set dim_rnicustomercodestatusproperty_desc = 'PREVENTS INSERT OF RECORD INTO STAGING'
where sid_rnicustomercodestatusproperty_id = 1

insert into status (Status, StatusDescription)
values ('0', '[0] Purge')

update RNICustomerCodeStatus 
set dim_status_id = '0'
	, dim_status_description = '[0] Purge'
	, dim_rnicustomercodestatus_propertysettings = (SELECT 16 + 8 + 2 + 32 + 1)
where sid_rnicustomercode_id = 98

--Add properties to the opt out status
update RNICustomerCodeStatus
set dim_rnicustomercodestatus_propertysettings = 35
where sid_rnicustomercode_id = 97



