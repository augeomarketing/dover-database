USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

SET IDENTITY_INSERT [dbo].[mappingtable] ON;

BEGIN TRANSACTION;
INSERT INTO [dbo].[mappingtable]([sid_mappingtable_id], [dim_mappingtable_tablename], [dim_mappingtable_active], [dim_mappingtable_created], [dim_mappingtable_lastmodified])
SELECT 1, N'account', 1, '20101228 15:45:26.850', NULL UNION ALL
SELECT 2, N'statementvalue', 1, '20101230 13:59:41.913', NULL UNION ALL
SELECT 3, N'affiliat', 1, '20101230 14:16:09.593', NULL UNION ALL
SELECT 4, N'monthly_statement_file', 1, '20101230 14:16:28.097', NULL UNION ALL
SELECT 5, N'customer', 1, '20110104 17:12:12.463', NULL
COMMIT;
RAISERROR (N'[dbo].[mappingtable]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

SET IDENTITY_INSERT [dbo].[mappingtable] OFF;

