﻿USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;

UPDATE SSISConfigurationKeyName SET KeyName = 'UTILITYDBCONNECTSTRING'      WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Connections[UTILITYDB].Properties[ConnectionString]'								
UPDATE SSISConfigurationKeyName SET KeyName = 'FIDBCONNECTSTRING'           WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Connections[FIDB].Properties[ConnectionString]'											
UPDATE SSISConfigurationKeyName SET KeyName = 'TIPFIRST'                    WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::TipFirst].Properties[Value]'												
UPDATE SSISConfigurationKeyName SET KeyName = 'STARTDATE'                   WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::StartDate].Properties[Value]'												
UPDATE SSISConfigurationKeyName SET KeyName = 'RUNDATE'                     WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::RunDate].Properties[Value]'													
UPDATE SSISConfigurationKeyName SET KeyName = 'RUN010IMPORTTOAUDIT'         WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::Run010ImportToAudit].Properties[Value]'							
UPDATE SSISConfigurationKeyName SET KeyName = 'RESETDEMOG'                  WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::ResetDemog].Properties[Value]'											
UPDATE SSISConfigurationKeyName SET KeyName = 'ENDDATE'                     WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::EndDate].Properties[Value]'													
UPDATE SSISConfigurationKeyName SET KeyName = 'DEMOGRAPHICIMPORTFORMATFILE' WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::DemographicImportFormatFile].Properties[Value]'			
UPDATE SSISConfigurationKeyName SET KeyName = 'DEMOGRAPHICIMPORTFILE'       WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::DemographicImportFile].Properties[Value]'						
UPDATE SSISConfigurationKeyName SET KeyName = 'DEBUG'                       WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::Debug].Properties[Value]'														
UPDATE SSISConfigurationKeyName SET KeyName = 'TRANSACTIONIMPORTFORMATFILE' WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::TransactionImportFormatFile].Properties[Value]'			
UPDATE SSISConfigurationKeyName SET KeyName = 'TRANSACTIONIMPORTFILE'       WHERE ConfigurationFilter = '239' AND PackagePath = '\Package.Variables[User::TransactionImportFile].Properties[Value]'						


COMMIT;
GO

