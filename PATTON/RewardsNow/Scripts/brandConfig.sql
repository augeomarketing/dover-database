USE [RewardsNow];
SET NOCOUNT ON;
SET XACT_ABORT ON;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Brand', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[StopOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'Initial Catalog=RewardsNow; Data Source=patton\rn;Provider=SQLNCLI10.1; Integrated Security=SSPI;', N'\Package.Connections[SSIS Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_BrandData', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_BrandData', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'V:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_BrandData', N'1', N'\Package.Connections[BRAND-DATA].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_BrandData', N'BRAND-DATA', N'\Package.Connections[BRAND-DATA].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'2', N'\Package.Connections[BRAND-DATA].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[VerifySignature]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[TransactionOption]', N'Object' UNION ALL
SELECT N'AD_Brand', N'User::DecryptedFileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Target]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[SuspendRequired]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', NULL, N'\Package.Variables[User::FileFilter].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'O:\AccessDevelopment\Input\test.zip', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_BrandData', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'@[User::FilePath]  +   @[User::FileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_BrandData', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'O:\AccessDevelopment\Input\Archive\Brand-Data\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'User::FileNameAndPath', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Source]', N'String' UNION ALL
SELECT N'AD_Brand', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[SecretKeyRing]', N'String' UNION ALL
SELECT N'AD_Brand', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Result]', N'String' UNION ALL
SELECT N'AD_Brand', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Brand-{499BE294-527D-4C1A-9174-F7A9D250A93B}patton\rn.RewardsNow;', N'\Package.Connections[rewardsnow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Connections[BrandEdited].Properties[Unicode]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'"', N'\Package.Connections[BrandEdited].Properties[TextQualifier]', N'String' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Connections[BrandEdited].Properties[RowDelimiter]', N'String' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package.Connections[BrandEdited].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[RemoveSource]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'BrandEdited', N'\Package.Connections[BrandEdited].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'1033', N'\Package.Connections[BrandEdited].Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'0', N'\Package.Connections[BrandEdited].Properties[HeaderRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'
', N'\Package.Connections[BrandEdited].Properties[HeaderRowDelimiter]', N'String' UNION ALL
SELECT N'AD_Brand', N'Delimited', N'\Package.Connections[BrandEdited].Properties[Format]', N'String' UNION ALL
SELECT N'AD_Brand', N'0', N'\Package.Connections[BrandEdited].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Connections[BrandEdited].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'0', N'\Package.Connections[BrandEdited].Properties[DataRowsToSkip]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\BrandEdited.csv', N'\Package.Connections[BrandEdited].Properties[ConnectionString]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 1.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Brand', N'True', N'\Package.Connections[BrandEdited].Properties[ColumnNamesInFirstDataRow]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'1252', N'\Package.Connections[BrandEdited].Properties[CodePage]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package.Connections[Brand.csv].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Brand', N'Brand.csv', N'\Package.Connections[Brand.csv].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package.Connections[Brand.csv].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Connections[Brand.csv].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\Brand.csv', N'\Package.Connections[Brand.csv].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Brand', N'20121129', N'\Package.Variables[User::RunDate].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::RunDate].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::RunDate].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::RunDate].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'RunDate', N'\Package.Variables[User::RunDate].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::RunDate].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N' (DT_STR, 4,1252) YEAR( GETDATE())     +   (DT_STR, 2,1252) MONTH( GETDATE())    +  (DT_STR, 2,1252) DAY( GETDATE())  ', N'\Package.Variables[User::RunDate].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'True', N'\Package.Variables[User::RunDate].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::RunDate].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FilePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'test.csv', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', NULL, N'\Package.Variables[User::FileName].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'AD-RN-2199*Brand.csv.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'FileFilter', N'\Package.Variables[User::FileFilter].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileFilter].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[PublicKeyRing]', N'String'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 2.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Brand', N'19RN-OperaTion$', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Password]', N'String' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Connections[BRAND-DATA].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'\\patton\ops\AccessDevelopment\Input\BRAND-DATA', N'\Package.Connections[BRAND-DATA].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_BrandData', N'0', N'\Package.Variables[User::FileSize].Properties[Value]', N'Int32' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileSize].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileSize].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::FileSize].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_BrandData', N'FileSize', N'\Package.Variables[User::FileSize].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'True', N'\Package.Variables[User::FileSize].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', NULL, N'\Package.Variables[User::FileSize].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileSize].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::FileSize].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'O:\AccessDevelopment\Input\', N'\Package.Variables[User::FilePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FilePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FilePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::FilePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FileFilter].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::FileFilter].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::FileFilter].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\test.csv', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'DecryptedFileNameAndPath', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'@[User::FilePath]  +   @[User::FileName]', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'True', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::DecryptedFileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'FilePath', N'\Package.Variables[User::FilePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FilePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', NULL, N'\Package.Variables[User::FilePath].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\Brand.csv', N'\Package.Variables[User::CSVFile].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FilePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', NULL, N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[OldFormat]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'OpenPGP Task  - Decrypt Brand  File', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[MaximumErrorCount]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[LoggingMode]', N'Object' UNION ALL
SELECT N'AD_Brand', N'1033', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[LocaleID]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'secring.gpg', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[KeyRing]', N'String' UNION ALL
SELECT N'AD_Brand', N'', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Key]', N'String' UNION ALL
SELECT N'AD_Brand', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsTargetVariable]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsSourceVariable]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsSourceSigned]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsSecretKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsPublicKeyRingVariable]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 3.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_Brand', N'1048576', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsolationLevel]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IsKeyRingVariable]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[IncludeData]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[ForceExecutionValue]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'-1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[ForceExecutionResult]', N'Object' UNION ALL
SELECT N'AD_Brand', N'0', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[ForcedExecutionValue]', N'Object' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[FailParentOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[FailPackageOnFailure]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[DisableEventHandlers]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Disable]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'OpenPGP Task', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[DelayValidation]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[DebugMode]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[Action]', N'Int32' UNION ALL
SELECT N'AD_Brand', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-Brand-{480CBB71-3DC7-4D0D-907D-3D1B08B8E7E4}patton\rn.RewardsNow;', N'\Package.Connections[SSIS_Config].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Brand', N'1', N'\Package.Connections[secring.gpg].Properties[ProtectionLevel]', N'Object' UNION ALL
SELECT N'AD_Brand', N'secring.gpg', N'\Package.Connections[secring.gpg].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'0', N'\Package.Connections[secring.gpg].Properties[FileUsageType]', N'Object' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Connections[secring.gpg].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'c:\Program Files\GNU\GnuPG\secring.gpg', N'\Package.Connections[secring.gpg].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Brand', N'True', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[OverwriteExisting]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::CSVFile].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::CSVFile].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::CSVFile].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'CSVFile', N'\Package.Variables[User::CSVFile].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::CSVFile].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'@[User::FilePath]  +  "Brand.csv"', N'\Package.Variables[User::CSVFile].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'True', N'\Package.Variables[User::CSVFile].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::CSVFile].Properties[Description]', N'String' UNION ALL
SELECT N'AD_Brand', N'O:\AccessDevelopment\Input\Archive\Brand\', N'\Package.Variables[User::ArchivePath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::ArchivePath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::ArchivePath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'User', N'\Package.Variables[User::ArchivePath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_Brand', N'ArchivePath', N'\Package.Variables[User::ArchivePath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::ArchivePath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::ArchivePath].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package.Variables[User::ArchivePath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_Brand', N'', N'\Package.Variables[User::ArchivePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::FilePath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'O:\AccessDevelopment\Input\test.zip.pgp', N'\Package.Variables[User::FileNameAndPath].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::FileNameAndPath].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_BrandData', N'FileNameAndPath', N'\Package.Variables[User::FileNameAndPath].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', NULL, N'\Package.Variables[User::FileNameAndPath].Properties[Expression]', N'DBNull' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileNameAndPath].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::FileNameAndPath].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'test.zip', N'\Package.Variables[User::FileName].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileName].Properties[ReadOnly]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 4.....Done!', 10, 1) WITH NOWAIT;
GO

BEGIN TRANSACTION;
INSERT INTO [dbo].[SSIS Configurations]([ConfigurationFilter], [ConfiguredValue], [PackagePath], [ConfiguredValueType])
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileName].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::FileName].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_BrandData', N'FileName', N'\Package.Variables[User::FileName].Properties[Name]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileName].Properties[IncludeInDebugDump]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'REPLACE(,SUBSTRING(@[User::FileNameAndPath],28,80),".pgp","")', N'\Package.Variables[User::FileName].Properties[Expression]', N'String' UNION ALL
SELECT N'AD_BrandData', N'True', N'\Package.Variables[User::FileName].Properties[EvaluateAsExpression]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'', N'\Package.Variables[User::FileName].Properties[Description]', N'String' UNION ALL
SELECT N'AD_BrandData', N'AD-RN-2199*BRAND-DATA.zip.pgp', N'\Package.Variables[User::FileFilter].Properties[Value]', N'String' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileFilter].Properties[ReadOnly]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'False', N'\Package.Variables[User::FileFilter].Properties[RaiseChangedEvent]', N'Boolean' UNION ALL
SELECT N'AD_BrandData', N'User', N'\Package.Variables[User::FileFilter].Properties[Namespace]', N'String' UNION ALL
SELECT N'AD_BrandHistory', N'Data Source=patton\rn;Initial Catalog=RewardsNow;Provider=SQLNCLI10.1;Integrated Security=SSPI;Auto Translate=False;Application Name=SSIS-BrandHistory-{9E13313E-CB02-4D1D-B46F-7BB95D51C8B6}patton\rn.RewardsNow;', N'\Package.Connections[RewardsNow].Properties[ConnectionString]', N'String' UNION ALL
SELECT N'AD_Brand', N'False', N'\Package\Foreach Loop Container\OpenPGP Task  - Decrypt Brand  File.Properties[OutputASCII]', N'Boolean'
COMMIT;
RAISERROR (N'[dbo].[SSIS Configurations]: Insert Batch: 5.....Done!', 10, 1) WITH NOWAIT;
GO

