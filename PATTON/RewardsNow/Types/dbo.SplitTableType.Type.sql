CREATE TYPE SplitTableType AS TABLE
(
	KeyItem VARCHAR(MAX)
	, ItemList VARCHAR(MAX)
	, ListDelim VARCHAR(1)
)
GO
