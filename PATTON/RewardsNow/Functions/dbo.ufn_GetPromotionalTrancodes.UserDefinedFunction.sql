USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetPromotionalTrancodes]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetPromotionalTrancodes]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetPromotionalTrancodes]()
RETURNS @tc TABLE 
(
	sid_trantype_trancode VARCHAR(2)
)
AS
BEGIN
	INSERT @tc (sid_trantype_trancode)

	SELECT sid_trantype_trancode_out 
	FROM PromotionalPointTrancodeXref pptx
	INNER JOIN PromotionalPointLedger ppl
		ON pptx.sid_promotionalpointledger_id = ppl.sid_promotionalpointledger_id
	WHERE ppl.sid_promotionalpointledger_id = 0
	GROUP BY sid_trantype_trancode_out	

	RETURN;
END
GO
