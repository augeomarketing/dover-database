USE [Rewardsnow]
GO
/****** Object:  UserDefinedFunction [dbo].[fnRptMoLast]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* Function returns the last day of the month, given the month number and the year, as integers
   The day is returned as a two character CHAR value */

CREATE FUNCTION [dbo].[fnRptMoLast] (@MonthNum INT, @Year INT)
RETURNS CHAR(2)
AS
BEGIN
	DECLARE @Answer CHAR(2)
	-- DECLARE @MonthNum INT, @Year INT 	SET @MonthNum = 12 SET @Year = 2100
	/* Process leap years, if required */
	If @MonthNum = 2					-- Is it February?
		If (@Year % 400) = 0				-- 2000, 2400,... ARE leap years
			SET @Answer = '29'
		ELSE
			IF (@Year % 100) = 0
				SET @Answer = '28'		-- but other century years are not
			ELSE
				IF (@Year % 4) = 0
					SET @Answer = '29'	-- Years divisible by 4 are leap years
				ELSE
					SET @Answer = '28'	-- All others are not
	ELSE
		SET @Answer = CASE @MonthNum
			WHEN 1 THEN '31'
			WHEN 3 THEN '31'
			WHEN 4 THEN '30'
			WHEN 5 THEN '31'
			WHEN 6 THEN '30'
			WHEN 7 THEN '31'
			WHEN 8 THEN '31'
			WHEN 9 THEN '30'
			WHEN 10 THEN '31'
			WHEN 11 THEN '30'
			WHEN 12 THEN '31'
			ELSE 'January '
		END
RETURN @Answer
-- SELECT @Answer AS Number_days
END
GO
