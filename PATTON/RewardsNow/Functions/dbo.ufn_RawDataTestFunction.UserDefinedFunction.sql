USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataTestFunction]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawDataTestFunction]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawDataTestFunction](@tipfirst VARCHAR(3), @dataval VARCHAR(MAX))
RETURNS VARCHAR(255)
AS
BEGIN
	SET @dataval = @tipfirst + '_' + REVERSE(@dataval)
	RETURN @dataval
END
GO
