USE [Rewardsnow]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerGetStatusAndDescriptionFromCodeWithDefault]    Script Date: 02/07/2012 21:21:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_RNICustomerGetStatusAndDescriptionFromCodeWithDefault]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_RNICustomerGetStatusAndDescriptionFromCodeWithDefault]
GO


--USE [Rewardsnow]
--GO
--
--/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerGetStatusAndDescriptionFromCodeWithDefault]    Script Date: 02/07/2012 21:21:44 ******/
--SET ANSI_NULLS ON
--GO
--
--SET QUOTED_IDENTIFIER ON
--GO
--
--
--
--CREATE FUNCTION [dbo].[ufn_RNICustomerGetStatusAndDescriptionFromCode](@intDefault INT, @intCode INT)
--RETURNS VARCHAR(50)
--AS
--BEGIN
--	DECLARE @output varchar(50)
--	
--	--TODO:  Prevent override of status for purged customers
--	
--	IF @intDefault <> 0
--	BEGIN
--		IF @intCode = 1
--			SET @intCode = @intDefault
--	END
--	
--	SELECT @output = dim_status_id + 'Ç' + dim_status_description
--	FROM RNICustomerCodeStatus
--	WHERE sid_rnicustomercode_id = @intCode
--	
--	RETURN @output
--END
--	
--	
--	
--GO

