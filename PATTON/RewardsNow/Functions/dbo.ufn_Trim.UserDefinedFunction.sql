USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_Trim]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_Trim]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_Trim]
(
	@input VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @output VARCHAR(MAX)
	
	SET @output = RTRIM(LTRIM(@input))
	
	RETURN @output
END
GO
