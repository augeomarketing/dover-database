USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractCustomerCity]    Script Date: 06/03/2011 15:08:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_mappingextractCustomerCityIfStateMarked]
(
	@tipfirst varchar(3)
)
returns nvarchar(max)
as
begin
	declare @output nvarchar(max);
	declare @dbname nvarchar(50);
	
	set @output = 'SELECT tipnumber, lastname, CASE WHEN state = ''**'' THEN city ELSE NULL END from [<DBNAME>].dbo.customer where status != ''C''';
	select @dbname = dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst;
	set @dbname = rewardsnow.dbo.ufn_trim(@dbname);
	
	set @output = replace(@output, '<DBNAME>', @dbname);
	
	return @output;
end		
