USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_webGetMerchantFundedPendingForStatementing]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_webGetMerchantFundedPendingForStatementing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_webGetMerchantFundedPendingForStatementing]
(
	@tipnumber VARCHAR(20),
	@startdate DATETIME,
	@enddate DATETIME
)
RETURNS
@merchantFunded TABLE
(
	Tipnumber VARCHAR(20),
	finalrow VARCHAR(MAX),
	id VARCHAR(MAX)
)

AS
BEGIN
	
	DECLARE @tmpMerchantFunded TABLE
	(
		id VARCHAR(20),
		transdate DATETIME,
		merchantname VARCHAR(200),
		transactionamount DECIMAL(16, 2),
		pointsearned INT
	)
	
	INSERT INTO @tmpMerchantFunded
	SELECT DISTINCT 'Z' + CAST(sid_ZaveeTransactions_Identity AS VARCHAR) AS sid_ZaveeTransactions_Identity,
		dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
		ISNULL(dim_ZaveeMerchant_MerchantName, '[Unknown Merchant]') as MerchantName, 
		dim_ZaveeTransactions_TransactionAmount AS TransactionAmount, 
		dim_ZaveeTransactions_AwardPoints AS PointsEarned
	FROM RewardsNOW.dbo.ZaveeTransactions zt
	LEFT OUTER JOIN RewardsNOW.dbo.ZaveeMerchant ZM
		ON ZT.dim_ZaveeTransactions_MerchantId = ZM.dim_ZaveeMerchant_MerchantId
	WHERE dim_ZaveeTransactions_MemberID = @tipnumber
		AND ISNULL(dim_ZaveeTransactions_CancelledDate, '1/1/1900') = '1/1/1900'
		AND dim_ZaveeTransactions_TransactionDate < DATEADD("d", 1, @enddate)
		AND ISNULL(dim_ZaveeTransactions_PaidDate, '1/1/1900') = '1/1/1900'
		AND ISNULL(dim_ZaveeTransactions_EmailSent, '1/1/1900') = '1/1/1900'
		
	UNION
		
	SELECT DISTINCT 'A' + CAST(sid_AzigoTransactions_Identity AS VARCHAR) AS sid_AzigoTransactions_Identity,
		dim_AzigoTransactions_TransactionDate AS TransactionDate, 
		dim_AzigoTransactions_MerchantName AS MerchantName, 
		dim_AzigoTransactions_TransactionAmount AS TransactionAmount, 
		dim_AzigoTransactions_Points AS PointsEarned
	FROM RewardsNOW.dbo.AzigoTransactions zt
	WHERE dim_AzigoTransactions_Tipnumber = @tipnumber
		AND dim_AzigoTransactions_PendingDate < DATEADD("d", 1, @enddate)	
		AND ISNULL(dim_AzigoTransactions_CancelledDate, '1/1/1900') = '1/1/1900'
		AND ISNULL(dim_AzigoTransactions_PaidDate, '1/1/1900') = '1/1/1900'
		AND ISNULL(dim_AzigoTransactions_EmailSent, '1/1/1900') = '1/1/1900'
		AND dim_AzigoTransactions_TransactionDate < DATEADD("d", 1, @enddate)
	
	UNION
		
	SELECT DISTINCT 
		'P' + CAST(sid_ProsperoTransactions_Id AS VARCHAR) AS sid_ProsperoTransactions_Identity,
		dim_ProsperoTransactions_TransactionDate AS TransactionDate, 
		ISNULL(dim_ProsperoMerchant_MerchantName, '[Merchant Not Identified]') as MerchantName,
		dim_ProsperoTransactions_TransactionAmount AS TransactionAmount, 
		dim_ProsperoTransactions_AwardPoints AS PointsEarned
	from 
		ProsperoTransactions as PT
		 LEFT OUTER JOIN RNITransaction as RT
			on RT.sid_RNITransaction_ID = PT.sid_RNITransaction_ID
		 LEFT OUTER JOIN RNITransactionArchive as RTA
			on RTA.sid_RNITransaction_ID = PT.sid_RNITransaction_ID
		 LEFT OUTER JOIN ProsperoMerchant as PM
			on 
				(PM.dim_ProsperoMerchant_MerchantId = RT.dim_RNITransaction_MerchantID
				 OR PM.dim_ProsperoMerchant_MerchantId = RTA.dim_RNITransaction_MerchantID)
	WHERE dim_ProsperoTransactions_Tipnumber = @tipnumber
		AND dim_ProsperoTransactionsWork_OutstandingDate < DATEADD("d", 1, @enddate)	
		AND ISNULL(dim_ProsperoTransactionsWork_CancelledDate, '1/1/1900') = '1/1/1900'
		AND ISNULL(dim_ProsperoTransactionsWork_PaidDate, '1/1/1900') = '1/1/1900'
		AND ISNULL(dim_ProsperoTransactions_EmailSent, '1/1/1900') = '1/1/1900'
		AND dim_ProsperoTransactions_TransactionDate < DATEADD("d", 1, @enddate)

		
	ORDER BY TransactionDate
	
	DECLARE @rowfield NVARCHAR(MAX) = '', @idfield VARCHAR(MAX)
	DECLARE BuildCursor CURSOR FOR 
		SELECT '<tr><td>' + CONVERT(VARCHAR, transdate, 101) + '</td><td>' + merchantname + '</td><td align="right">' +
			CASE CAST(transactionamount AS VARCHAR) WHEN '0.00' THEN 'In Process' ELSE '$' + CAST(transactionamount AS VARCHAR) END + '</td><td align="right">' + 
			CASE CAST(pointsearned AS VARCHAR) WHEN '0' THEN 'In Process' ELSE CAST(pointsearned AS VARCHAR) END + '</td></tr>',
			id + ', '
		FROM @tmpMerchantFunded
		WHERE pointsearned > 0

	OPEN BuildCursor 
	FETCH NEXT FROM BuildCursor INTO @rowfield, @idfield

	DECLARE @finalrow VARCHAR(MAX) = ''
	DECLARE @idrow VARCHAR(MAX) = ''
	
	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		SET @finalrow = RTRIM(@finalrow) + RTRIM(@rowfield)
		SET @idrow = RTRIM(@idfield) + RTRIM(@idrow)
		FETCH NEXT FROM BuildCursor INTO @rowfield, @idfield
	END 
	CLOSE BuildCursor 

	DEALLOCATE BuildCursor 
	
	IF @finalrow <> ''
	BEGIN
		INSERT INTO @merchantFunded(tipnumber, finalrow, id)
		VALUES (@tipnumber, @finalrow, LEFT(@idrow, LEN(@idrow) - 1))
	END
	
	-- Return the result of the function
	RETURN

END
GO
