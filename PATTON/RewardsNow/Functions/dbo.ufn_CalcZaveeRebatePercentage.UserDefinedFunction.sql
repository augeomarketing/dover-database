USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_CalcZaveeRebatePercentage]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_CalcZaveeRebatePercentage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_CalcZaveeRebatePercentage]
(
	@agent VARCHAR(10)
)
RETURNS DECIMAL
AS
BEGIN
	DECLARE @percentage DECIMAL(18,2)
	DECLARE @agentname VARCHAR(10)
	
	set @agentname = @agent

	IF @agent = '' OR @agent IS NULL
		BEGIN
			set @agentname = 'None'
		END
		
	SELECT
		@percentage = CASE dim_ZaveeBilling_Agent
			WHEN '241' THEN dim_ZaveeBilling_RNIPercentage + dim_ZaveeBilling_ZaveePercentage + dim_ZaveeBilling_ClientPercentage + dim_ZaveeBilling_ItsMyMichiganPercentage
			WHEN 'IMM5' THEN dim_ZaveeBilling_RNIPercentage + dim_ZaveeBilling_ZaveePercentage + dim_ZaveeBilling_ClientPercentage + dim_ZaveeBilling_ItsMyMichiganPercentage + dim_ZaveeBilling_650Percentage
			WHEN 'IMM6' THEN dim_ZaveeBilling_RNIPercentage + dim_ZaveeBilling_ZaveePercentage + dim_ZaveeBilling_ClientPercentage + dim_ZaveeBilling_ItsMyMichiganPercentage + dim_ZaveeBilling_650Percentage
			WHEN 'XIMM5' THEN dim_ZaveeBilling_RNIPercentage + dim_ZaveeBilling_ZaveePercentage + dim_ZaveeBilling_ClientPercentage + dim_ZaveeBilling_ItsMyMichiganPercentage + dim_ZaveeBilling_650Percentage
			WHEN 'XIMM6' THEN dim_ZaveeBilling_RNIPercentage + dim_ZaveeBilling_ZaveePercentage + dim_ZaveeBilling_ClientPercentage + dim_ZaveeBilling_ItsMyMichiganPercentage + dim_ZaveeBilling_650Percentage
			ELSE dim_ZaveeBilling_RNIPercentage + dim_ZaveeBilling_ZaveePercentage + dim_ZaveeBilling_ClientPercentage + dim_ZaveeBilling_ItsMyMichiganPercentage
		END
	FROM ZaveeBilling
	WHERE dim_ZaveeBilling_Agent = @agentname
	
	RETURN @percentage
END
GO
