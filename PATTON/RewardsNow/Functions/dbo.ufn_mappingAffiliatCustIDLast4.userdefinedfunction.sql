USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingAffiliatCustIDLast4]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingAffiliatCustIDLast4]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_mappingAffiliatCustIDLast4]
    (@tipfirst varchar(3))
RETURNS nvarchar(max)

AS

BEGIN

    DECLARE @db         nvarchar(50)
    DECLARE @sql        nvarchar(max)

    SET @db = (SELECT QUOTENAME(dbnamepatton) FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst)

    SET @sql = 'SELECT tipnumber, lastname, right( ltrim(rtrim(custid)), 4) mapvalue
                FROM ' + @db + '.dbo.affiliat
                '
                
    RETURN @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingAffiliatCustIDLast4 ('713')

-- resulting @SQL value:
select tipnumber, lastname, right( ltrim(rtrim(custid)), 4) mapvalue                  from [713].dbo.affiliat                  

*/
GO
