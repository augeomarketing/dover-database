USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_RemoveAlpha]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_RemoveAlpha]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_RemoveAlpha] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(max)  
AS  
BEGIN  
 DECLARE @output VARCHAR(max)  
 SET @output = Rewardsnow.dbo.ufn_RemoveAlpha(@data)  
 RETURN @output  
END
GO
