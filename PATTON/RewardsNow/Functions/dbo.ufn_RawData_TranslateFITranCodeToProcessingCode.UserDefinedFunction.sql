USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_TranslateFITranCodeToProcessingCode]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_TranslateFITranCodeToProcessingCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_RawData_TranslateFITranCodeToProcessingCode]
	(@tipfirst			varchar(3),
	 @data			varchar(255))
RETURNS varchar(2)

AS
/* test
select dbo.ufn_RawData_TranslateFITranCodeToProcessingCode('233','SP')

*/
BEGIN

      declare @output varchar(2)

      --include a default just in case
      set @output = @data

      if @tipfirst = '233'
      begin
            set @output = case @data
                  when 'SP' then 21
                  when 'PP' then 22
                  when 'SR' then 21
                  when 'PR' then 22
            end
      end

      return @output


END
GO
