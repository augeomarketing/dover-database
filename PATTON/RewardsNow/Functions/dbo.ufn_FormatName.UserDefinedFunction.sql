USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_FormatName]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_FormatName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_FormatName] (
	@type INT
	, @namepart1 VARCHAR(40) --First or Full Name
	, @namepart2 VARCHAR(40) = '' --Last Name (if sent separately)
	, @namepart3 VARCHAR(40) = '' --Middle Name (if sent separately)
)
RETURNS VARCHAR(MAX) --Only 40 characters are returned except when type 0 is chosen (for help).

-- taken from http://www.fostersolutions.com  2/27/2013
-- works well except when there are 2 first names

BEGIN

	DECLARE @Output VARCHAR(MAX)
	DECLARE @HelpText VARCHAR(MAX)
	DECLARE @tempName VARCHAR(MAX)
	DECLARE @nl VARCHAR(2)
	
	SET @nl = CHAR(13) + CHAR(10)
	SET @Output = ''
	
	SET @HelpText = 'Syntax: dbo.ufn_FormatName(type, first or fullname, lastname (if any), middle name (if any)'
		+ @nl  
		+ @nl + '0 - Help' 
		+ @nl + '1 - Combine from Input-First, Last Output-[First Last]'
		+ @nl + '2 - Combine from Input-First, Last, Middle Output-[First Middle Last]'
		+ @nl + '3 - Combine from Input-First, Last, Middle Output-[First MI Last]'
		+ @nl + '100 - Split from namepart1 Input-[Last, First Middle] Output-[First Middle Last]'
		+ @nl + '101 - Split from namepart1 Input-[Last, First Middle] Output-[First MI Last]'
		+ @nl + '102 - Split from namepart1 Input-[Last, First Middle] Output-[First Last]'
		+ @nl + '103 - Split from namepart1 Input-[Last Pedigree, First MI] Output = [First MI Last Pedigree]'
		+ @nl + '200 - Split from namepart1 Input-[Last Pedigree First Middle] Output = [First Middle Last Pedigree]'
		+ @nl + '201 - Split from namepart1 Input-[FirstPart<type>LastPart] Output = [First Last]'
		+ @nl + '      Used to change format of corporate and consumer names (from rawimport)'

	SET @namepart1 = RTRIM(LTRIM(ISNULL(@namepart1, '')))
	SET @namepart2 = RTRIM(LTRIM(ISNULL(@namepart2, '')))
	SET @namepart3 = RTRIM(LTRIM(ISNULL(@namepart3, '')))
	
	SET @type = ISNULL(@type, 0)
	
	IF @type = 0
		SET @Output = @HelpText
	
	IF @type = 1
		SET @tempName = @namepart1 + ' ' + @namepart2
		
	IF @type = 2
	--First, Last, Middle all in separate fields
	BEGIN
		SET @tempName = 
			CASE
				WHEN LEN(@namepart3) > 0 THEN @namepart1 + ' ' + @namepart3 + ' ' + @namepart2
				ELSE @namepart1 + ' ' + @namepart2
			END
	END

	IF @type = 3
	--First, Last, Middle all in separate fields output: First MI Last
	BEGIN
		SET @tempName = 
			CASE
				WHEN LEN(@namepart3) > 0 THEN @namepart1 + ' ' + SUBSTRING(@namepart3, 1, 1) + ' ' + @namepart2
				ELSE @namepart1 + ' ' + @namepart2
			END
	END
	
	
	
	

	
	IF @type = 100
	--Input Last, First Middle Output First Middle Last
	--Algorithm courtesy of Paul Butler
	BEGIN
	    SET @tempName =
 		  CASE 
			 WHEN CHARINDEX(',', @namepart1) != 0 THEN RIGHT(@namepart1, LEN(@namepart1) - CHARINDEX(',', @namepart1)) + ' ' + LEFT(@namepart1, CHARINDEX(',', @namepart1)-1)
			 ELSE @namepart1
		  END
	END

	IF @type in (101, 102)
	--Input Last, First, Middle Output First MI Last
	--Derived from an algorithm shamlessly stolen from Paul Butler
	BEGIN
		DECLARE @tempFirst VARCHAR(40)
		DECLARE @tempLast VARCHAR(40)
		DECLARE @tempMiddle VARCHAR(40)
		
		SET @tempName = @namepart1
		SET @tempMiddle = ''
	
		IF CHARINDEX(',', @namepart1) != 0
		BEGIN
			SET @tempFirst = LTRIM(RTRIM(RIGHT(@namepart1, LEN(@namepart1) - CHARINDEX(',', @namepart1))))
			SET @tempLast = LEFT(@namepart1, CHARINDEX(',', @namepart1)-1)
			SET @tempFirst = REVERSE(@tempFirst)
			IF CHARINDEX(' ', @tempFirst) != 0
			BEGIN
				SET @tempMiddle = LEFT(@tempFirst, CHARINDEX(' ', @tempFirst)-1)
				SET @tempFirst = SUBSTRING(@tempFirst, (CHARINDEX(' ', @tempFirst) + 1), 40)
				SET @tempMiddle = LTRIM(RTRIM(REVERSE(@tempMiddle)))
			END
			SET @tempFirst = LTRIM(RTRIM(REVERSE(@tempFirst)))

			SET @tempName = CASE WHEN LEN(@tempMiddle) > 0 AND @type <> 102 
				THEN @tempFirst + ' ' + LEFT(@tempMiddle, 1) + ' ' + @tempLast
				ELSE @tempFirst + ' ' + @tempLast END
							
		END	
	END
	
	IF @type = 103
	BEGIN
		SET @tempName = 
			ltrim(reverse(substring(reverse(@namepart1), 1, charindex(',', reverse(@namepart1))-1)))
			+ ' '
			+ substring(@namepart1, 1, charindex(',', @namepart1)-1)
	END
	
	if @type = 200
	BEGIN

        declare @ctr int = 0
        declare @startpos int = 0
        declare @spacefound int = -1

        declare @stringpos table 
	        (id			int identity(1,1) primary key,
	         startpos	int)

        -- loop through name string counting the number of spaces
        -- Insert results into table variable to use on multi-part names.
        -- values inserted are the occurrence # of the space, and what position in the string the space is found
        while @spacefound != 0
        BEGIN
	        set @spacefound = charindex(' ', @namepart1, @startpos)
        	
	        if @spacefound != 0
	        BEGIN
		        set @ctr = @ctr + 1
		        set @startpos = @spacefound + 1

		        insert into @stringpos (startpos) values(@spacefound)
        --		print @name + ' | ' + cast(@ctr as varchar(4)) + ' | Start Position: ' + cast(@spacefound as varchar(4))
	        END
        END


        if @ctr <= 2  -- only first name and last name or first name, MI and last name
        BEGIN 
	        select @tempName = right(@namepart1, len(@namepart1) - charindex(' ', @namepart1)) + ' ' + 
	                            left(@namepart1, charindex(' ', @namepart1))
        END


        if @ctr >= 3  -- multi-part names - usually with some type of suffix such as DR.
        BEGIN

	        -- Remove the last row in the temp table.  This is the position of the last space in the name.
	        delete tmp
	        from @stringpos tmp join (select top 1 id from @stringpos order by id desc) sp
		        on tmp.id = sp.id
        	
	        -- Now get the last (second to last - as the last was actually deleted above) position a space was found
	        -- This is the demarkation on where the name will be parsed	
	        select @startpos = (select top 1 startpos from @stringpos order by id desc)
        	
	        select @tempName =  right(@namepart1, len(@namepart1) - @startpos) + ' ' + left(@namepart1, @startpos)

        END	
	END
	
	/* Added by CWH.
		Primarily for use with concatenated values from Central Processing.  The values would have the first part of the name
		separated by the BusinessFlag value (0 = consumer, 1 = business) and then then last part of the name.
		The names would look like this for consumer Robert0McKeagney IV and like this for corporations INC1RewardsNow.
		This routine splits them at the first 0 or 1 and then reorders them based on that value.  So from our example, the 
		consumer would be Robert McKeagney IV and the corporation would be RewardsNow Inc.
	*/
	
	IF @type = 201
	BEGIN
	
		DECLARE @part1 VARCHAR(255) = ''
		DECLARE @part2 VARCHAR(255) = ''
		DECLARE @nametype INT = -1
		DECLARE @char VARCHAR(1)

		DECLARE @pos INT = 1
		DECLARE @len INT

		SET @len = LEN(@namepart1)
		WHILE @pos <= @len
		BEGIN
			SET @char = SUBSTRING(@namepart1, @pos, 1)
	
			IF @nametype = -1 --type not determined, everything goes into part1
			BEGIN
				IF @char IN ('0', '1')
				BEGIN
					SET @nametype = CONVERT(INT, @char)
				END
			ELSE
				BEGIN
					SET @part1 = @part1 + @char
				END
			END
			ELSE
			BEGIN
				SET @part2 = @part2 + @char
			END

			SET @pos = @pos + 1
		END

		SET @part1 = LTRIM(RTRIM(ISNULL(@part1, '')))
		SET @part2 = LTRIM(RTRIM(ISNULL(@part2, '')))
		SET @tempName = ''

		SET @tempname = 
			CASE @nametype
				WHEN 0 THEN LTRIM(RTRIM(@part1 + ' ' + @part2))
				WHEN 1 THEN LTRIM(RTRIM(@part2 + ' ' + @part1))
				ELSE @part1
			END
	END
	
	IF @type != 0
		SET @Output = SUBSTRING(@tempName, 1, 40)
	
	RETURN @Output
END


/*

PRINT dbo.ufn_FormatName(0,'','','')
SELECT dbo.ufn_FormatName(1,'  John  ', ' Public ', '')
SELECT dbo.ufn_FormatName(2,'  John  ', ' Public   ', ' Quincy ')
SELECT dbo.ufn_FormatName(3,'  John  ', ' Public   ', ' Quincy ')
SELECT dbo.ufn_FormatName(100,'Public, John Quincy', '', '')
SELECT dbo.ufn_FormatName(101,'Public, John Quincy', '', '')
SELECT dbo.ufn_FormatName(102,'Public, John Quincy', '', '')

SELECT dbo.ufn_FormatName(103, 'Public Jr, John Quincy',default,default)
SELECT dbo.ufn_FormatName(103, 'Public Jr, John Quincy',NULL,NULL)
SELECT dbo.ufn_FormatName(103, 'Public Jr, John Quincy','','')

SELECT dbo.ufn_FormatName(200, 'Public Jr John Quincy','','')
SELECT dbo.ufn_FormatName(200, 'Public John Quincy','','')
SELECT dbo.ufn_FormatName(200, 'Public Quincy','','')
SELECT dbo.ufn_FormatName(200, 'Quincy','','')

SELECT dbo.ufn_FormatName(201, 'John0Q Public Jr', NULL, NULL)
SELECT dbo.ufn_FormatName(201, 'Corp1Weyland-Yutani', NULL, NULL)

*/
GO
