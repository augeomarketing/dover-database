USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_FieldMerge]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_FieldMerge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_FieldMerge](@tipfirst varchar(3), @data varchar(max))
RETURNS varchar(max)

AS 

BEGIN

	DECLARE @mfl INT = 0

	SELECT	@mfl = dim_rniprocessingparameter_value 
	FROM	RewardsNOW.dbo.RNIProcessingParameter 
	WHERE	sid_dbprocessinfo_dbnumber = @tipfirst AND dim_rniprocessingparameter_key = 'MergeField_TruncateLength'

	SET		@data = LTRIM(RTRIM(@data))

	DECLARE @output VARCHAR(MAX)
	
	DECLARE @PART1 VARCHAR(255), @PART2 VARCHAR(255)
	
	SELECT	@PART1 = SUBSTRING(@data, 1, CHARINDEX('|', @data)-1)
	SELECT	@PART2 = SUBSTRING(@data, CHARINDEX('|', @data)+1, LEN(@data))
	
	SET		@PART1 =	CASE	
							WHEN LEN(@PART1)-@mfl > 0 
							THEN LEFT(@PART1, LEN(@PART1)-@mfl)
							ELSE @PART1 
						END

	SET		@PART2 =	CASE	
							WHEN LEN(@PART2)-@mfl > 0 
							THEN LEFT(@PART2, LEN(@PART2)-@mfl)
							ELSE @PART2 
						END
	
	SET		@output = CASE ISNULL(@PART1, '') WHEN '' THEN @PART2 ELSE @PART1 END
	
	RETURN	@output

END
GO
