USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerGetActiveCardListForTipfirst]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNICustomerGetActiveCardListForTipfirst]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RNICustomerGetActiveCardListForTipfirst]
(
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
)
RETURNS TABLE
AS
RETURN
	SELECT 
		dim_rnicustomer_rniid
		, LTRIM(RTRIM([1]))
			+ CASE WHEN [2] IS NOT NULL THEN '|' + LTRIM(RTRIM([2])) ELSE '' END
			+ CASE WHEN [3] IS NOT NULL THEN '|' + LTRIM(RTRIM([3])) ELSE '' END 
			+ CASE WHEN [4] IS NOT NULL THEN '|' + LTRIM(RTRIM([4])) ELSE '' END
			+ CASE WHEN [5] IS NOT NULL THEN '|' + LTRIM(RTRIM([5])) ELSE '' END
			+ CASE WHEN [6] IS NOT NULL THEN '|' + LTRIM(RTRIM([6])) ELSE '' END
			+ CASE WHEN [7] IS NOT NULL THEN '|' + LTRIM(RTRIM([7])) ELSE '' END
			+ CASE WHEN [8] IS NOT NULL THEN '|' + LTRIM(RTRIM([8])) ELSE '' END
			+ CASE WHEN [9] IS NOT NULL THEN '|' + LTRIM(RTRIM([9])) ELSE '' END
			+ CASE WHEN [10] IS NOT NULL THEN '|' + LTRIM(RTRIM([10])) ELSE '' END
		AS CardList
	FROM
	(

		SELECT dim_rnicustomer_rniid, [1], [2], [3], [4], [5], [6], [7], [8], [9], [10]
		FROM

		(
			SELECT dim_rnicustomer_rniid, dim_rnicustomer_cardnumber, ROW_NUMBER() over (partition by dim_rnicustomer_rniid order by dim_rnicustomer_cardnumber) as mycounter
			FROM
			(
				SELECT dim_rnicustomer_rniid, dim_RNICustomer_CardNumber
				from Rewardsnow.dbo.RNICustomer 
				where sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
					and dim_RNICustomer_CustomerCode <> '99'
				group by dim_rnicustomer_rniid, dim_RNICustomer_CardNumber 
			) memnum
		) T1
		PIVOT
		(
			MIN (dim_rnicustomer_CardNumber)
			FOR mycounter
			IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10])
		) PVT
	) GRID
GO
