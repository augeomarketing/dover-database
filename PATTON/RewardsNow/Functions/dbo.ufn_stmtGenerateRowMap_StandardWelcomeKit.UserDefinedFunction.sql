USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_stmtGenerateRowMap_StandardWelcomeKit]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_stmtGenerateRowMap_StandardWelcomeKit]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_stmtGenerateRowMap_StandardWelcomeKit] (@jobid bigint)
RETURNS nvarchar(max)
AS
BEGIN
	--RETURNS A STATEMENT TO BE EXECUTED.
	
	--GET VALUES FROM PROCESSING JOB BY JOB ID
	
	DECLARE @sqlOut NVARCHAR(MAX)
	DECLARE @tipfirst VARCHAR(15)
	DECLARE @startDate VARCHAR(10)
	DECLARE @endDate VARCHAR(10)
	DECLARE @dbname VARCHAR(50)
	
	SELECT 
		@tipfirst = sid_dbprocessinfo_dbnumber		
		, @startdate = CONVERT(VARCHAR(10), dim_processingjob_stepparameterstartdate, 101)
		, @enddate = CONVERT(VARCHAR(10), dim_processingjob_stepparameterenddate, 101)
	FROM
		processingjob
	WHERE sid_processingjob_id = @jobid
	
	SELECT @dbname = dbnamepatton from dbprocessinfo where dbnumber = @tipfirst
	
	SET @sqlOut = REPLACE(REPLACE(REPLACE(
	'
	SELECT TIPNUMBER
	FROM [<DBNAME>].dbo.CUSTOMER C
	WHERE (CONVERT(DATE, c.DATEADDED) BETWEEN ''<STARTDATE>'' AND ''<ENDDATE>'')
		and c.status=''A''
	'	
	, '<DBNAME>', @dbname)
	, '<STARTDATE>', @startDate)
	, '<ENDDATE>', @endDate)
	
	RETURN @sqlOut

END
GO
