USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractaffiliatconcatsecidcustid]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractaffiliatconcatsecidcustid]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractaffiliatconcatsecidcustid]
    (@tipfirst              varchar(3))

returns nvarchar(max)

as

BEGIN

    declare @db         nvarchar(50)
    declare @sql        nvarchar(max)

    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    set @sql = 'select tipnumber, lastname, ltrim(rtrim(isnull(secid, ''''))) + ltrim(rtrim(isnull(custid, ''''))) mapvalue from ' + @db + '.dbo.affiliat where acctstatus != ''C'' '
                
    return @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingextractaffiliatconcatsecidcustid ('803')

-- resulting @SQL value:
select tipnumber, lastname, ltrim(rtrim(isnull(secid, ''))) + ltrim(rtrim(isnull(custid, ''))) mapvalue                  from [803].dbo.affiliat                  where acctstatus != 'C' 

select tipnumber, lastname, ltrim(rtrim(isnull(secid, '))) + ltrim(rtrim(isnull(custid, '))) mapvalue                  from [803].dbo.affiliat                  where acctstatus != 'C' 


*/
GO
