USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_Strip_Out_Spaces_COOP]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_Strip_Out_Spaces_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_Strip_Out_Spaces_COOP] (@tipfirst varchar(3), @data varchar(max)) 
RETURNS VARCHAR(40)
AS
BEGIN
	DECLARE @output VARCHAR(40)
	DECLARE @type INT
	
	SET @data = ltrim(rtrim(replace(replace(replace(@data,' ','Çü'),'üÇ',''),'Çü',' ')))
	SET @output = @data
	
	RETURN @output

END
GO
