USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_ProcessingCode]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_ProcessingCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_ProcessingCode] (@tipfirst varchar(3), @data varchar(max))  
RETURNS int 

AS  

BEGIN
	DECLARE @output int

    set @output = (select dim_rniprocessingparameter_value
                   from rewardsnow.dbo.rniprocessingparameter
                   where dim_rniprocessingparameter_key = 'PROCESSINGCODE'
                   and sid_dbprocessinfo_dbnumber = @tipfirst)

    if isnull(@output,'') = ''
        set @output = (select dim_rniprocessingparameter_value
                   from rewardsnow.dbo.rniprocessingparameter
                   where dim_rniprocessingparameter_key = 'PROCESSINGCODE'
                   and sid_dbprocessinfo_dbnumber = 'RNI')
        

 
 RETURN @output  
END
GO
