USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_TranslateTransactionTypeCode]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_TranslateTransactionTypeCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_TranslateTransactionTypeCode]  
 (@tipfirst   varchar(3),  
  @data   varchar(255))  
RETURNS varchar(2)  
  
AS  
  
BEGIN  
 declare @xlatestatus varchar(2)  
 
   
 set @data = 'TRANSACTIONTYPECODE_' + ltrim(rtrim(@data))  
   
 set @xlatestatus = (select top 1 dim_rniprocessingparameter_value  
      from rewardsnow.dbo.rniprocessingparameter  
      where sid_dbprocessinfo_dbnumber = @tipfirst  
      and dim_rniprocessingparameter_key = @data  
      and dim_rniprocessingparameter_active = 1)  
 return @xlatestatus  
  
END
GO
