USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ConvertAccountingStyleNumber]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ConvertAccountingStyleNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ConvertAccountingStyleNumber](@moneystring VARCHAR(MAX)) 
RETURNS INTEGER
AS
BEGIN
	DECLARE @output INT

	SET @moneystring = 
		REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
		@moneystring
		, '(', '-')	
		, ')', '')
		, ',', '')
		, '.', '')
		, '$', '')
	
	SET @output = CONVERT(BIGINT, @moneystring)
		
	RETURN @output
END
GO
