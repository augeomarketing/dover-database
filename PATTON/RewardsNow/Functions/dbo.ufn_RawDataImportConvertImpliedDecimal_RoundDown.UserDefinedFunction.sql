USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataImportConvertImpliedDecimal_RoundDown]    Script Date: 09/12/2016 09:09:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_RawDataImportConvertImpliedDecimal_RoundDown] (@tipfirst varchar(3), @value varchar(max))
	returns decimal(18,2)

AS

BEGIN

declare @decimalvalue		decimal(18,2) = 0

set @decimalvalue = cast((select dbo.ufn_RawDataConvertStringtodecimal(@tipfirst, @value) / 100) as int)

return @decimalvalue

END


/*  Test harness


select dbo.ufn_RawDataImportConvertImpliedDecimal('135', '8335')


*/
GO
