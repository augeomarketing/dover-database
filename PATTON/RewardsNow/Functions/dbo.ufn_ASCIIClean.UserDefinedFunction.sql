USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ASCIIClean]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ASCIIClean]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ASCIIClean](@myString VARCHAR(MAX)) RETURNS VARCHAR(MAX)
AS
BEGIN
    DECLARE @myNewString VARCHAR(max)
    DECLARE @index as INT
    SELECT @index = 0
    WHILE @index <= len(@myString)
    BEGIN
        SELECT @myNewString = ISNULL(@myNewString,'') +
        CASE 
			WHEN ASCII(SUBSTRING(@myString,@index,1)) < 127
				THEN SUBSTRING(@myString,@index,1) 
			ELSE '' 
		END
        SELECT @index = @index + 1
    END
        RETURN @myNewString
END
GO
