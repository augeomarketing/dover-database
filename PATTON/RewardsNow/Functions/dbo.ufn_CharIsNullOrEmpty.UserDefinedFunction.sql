USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_CharIsNullOrEmpty]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_CharIsNullOrEmpty]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_CharIsNullOrEmpty](@value VARCHAR(MAX)) RETURNS BIT
AS
BEGIN
	DECLARE @out BIT
	SET @out = 0
	
	IF ISNULL(RewardsNow.dbo.ufn_Trim(@value), '') = ''
		SET @out = 1
		
	RETURN @out
END
GO
