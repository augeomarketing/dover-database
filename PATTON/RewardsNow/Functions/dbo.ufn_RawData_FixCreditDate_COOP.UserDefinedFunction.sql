USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_FixCreditDate_COOP]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_FixCreditDate_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_FixCreditDate_COOP] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(8)  
AS  
BEGIN  
DECLARE @output VARCHAR(8)  
 	,@year varchar(4)
	,@month varchar(2)
	
	set @year = datepart(yyyy,getdate())
	set @month = datepart(mm,getdate())

	if(substring(@data,3,2) > @month) 
	begin
		set @year = @year - 1
	end

	SET @data = rtrim(@year) + right(rtrim(@data),4)
	SET @output = @data	

 RETURN @output  
END
GO
