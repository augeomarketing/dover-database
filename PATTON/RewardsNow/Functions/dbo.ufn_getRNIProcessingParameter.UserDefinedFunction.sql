USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_getRNIProcessingParameter]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_getRNIProcessingParameter]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_getRNIProcessingParameter]
(
	@tipfirst VARCHAR(3),
	@key VARCHAR(255)
)
RETURNS VARCHAR(255)
AS
BEGIN

	DECLARE @value VARCHAR(255)
	
	SELECT @value = dim_rniProcessingParameter_value
	FROM rewardsnow.dbo.RNIProcessingParameter
	WHERE dim_rniProcessingParameter_key = @key
		AND sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniProcessingParameter_active = 1
		
	IF ISNULL(@value, '') = '' OR @@ROWCOUNT = 0
	BEGIN
		SELECT @value = dim_rniProcessingParameter_value
		FROM rewardsnow.dbo.RNIProcessingParameter
		WHERE dim_rniProcessingParameter_key = @key
			AND sid_dbprocessinfo_dbnumber = 'RNI'
			AND dim_rniProcessingParameter_active = 1
	END
	
	RETURN @value

END
GO
