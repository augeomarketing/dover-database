USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_wildcardstring]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_wildcardstring]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_wildcardstring](@str nvarchar(MAX)) RETURNS nvarchar(MAX) AS
BEGIN
   DECLARE @ret nvarchar(MAX),
           @sq  char(1)
   SELECT @sq = '%'
   SELECT @ret = replace(@str, @sq, @sq + @sq)
   RETURN(@sq + @ret + @sq)
END
GO
