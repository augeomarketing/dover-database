USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingExtractActiveMember]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingExtractActiveMember]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_mappingExtractActiveMember] (@tipfirst VARCHAR(3))
RETURNS NVARCHAR(MAX)
AS
BEGIN
	DECLARE @db VARCHAR(50) = (SELECT QUOTENAME(DBNAMEPATTON) FROM RewardsNow.dbo.dbprocessinfo WHERE DBNumber = @tipfirst)
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = 'SELECT TIPNUMBER, LASTNAME, ACCTID mapvalue FROM <DBNAME>.dbo.AFFILIAT WHERE AcctType = ''MEMBER'' AND AcctStatus = ''A'''
	SET @sql = REPLACE(@sql, '<DBNAME>', @db)
	
	RETURN @sql
END
GO
