USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_633_CleanUp_Address2]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_633_CleanUp_Address2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_633_CleanUp_Address2] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(max)  
AS  
BEGIN  
 DECLARE @output VARCHAR(max)  
 SET @output = (CASE WHEN @data LIKE '%[0-9a-Z]%' THEN @data 
						ELSE REPLACE(@data, '*', '') 
						END )  
 RETURN @output  
END
GO
