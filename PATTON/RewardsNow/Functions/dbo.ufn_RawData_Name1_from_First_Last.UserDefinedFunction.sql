USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_Name1_from_First_Last]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_Name1_from_First_Last]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ufn_RawData_Name1_from_First_Last]
(
@tipfirst varchar(3), @data varchar(max)
)
RETURNS Varchar(255)
AS

BEGIN
/*
--select * from rnirawimport
select * from zz233_ACCT_stage
select * from RNIRawImport where sid_dbprocessinfo_dbnumber='233'
select * from dbo.vw_233_ACCT_TARGET_230
select * from dbo.vw_233_TRAN_TARGET_230
 select dbo.ufn_RawData_Name1_from_First_Last('233','|Joe|Schmoe')
*/
--declare @tipfirst varchar(3), @data varchar(max)
--set @tipfirst='233'; set @data='|Joe|Schmoe'


	-- Declare the return variable here
	DECLARE @output varchar(255)
	declare @fname varchar(50), @lname varchar(50);
	declare @Delim varchar(1), @DelimPos int; 
	set @Delim=substring(@data,1,1)
	set @DelimPos=charindex(@Delim,@data,2)	
	
	--print '@Delim:' + @Delim + char(10) + ' @DelimPos:' + cast(@DelimPos as varchar)
	set @fname=SUBSTRING(@data,2,@DelimPos-2)
	set @lname=SUBSTRING(@data,@DelimPos+1,LEN(@data)-@DelimPos+1)
	--print '@fname:' + @fname
	--Print '@lname:' + @lname

	set @output=@fname + ' '  + @lname

	-- Return the result of the function
	RETURN @output

END
GO
