USE REWARDSNOW
GO

IF OBJECT_ID(N'ufn_ExcludeByBin') IS NOT NULL
	DROP FUNCTION ufn_ExcludeByBin
GO


CREATE FUNCTION dbo.ufn_ExcludeByBin (@jobid BIGINT) 
	RETURNS @ExcludeTips TABLE (tipnumber VARCHAR(15) PRIMARY KEY)          
AS  
BEGIN      
	DECLARE @sid_dbprocessinfo_dbnumber VARCHAR(3)     
	SET @sid_dbprocessinfo_dbnumber = (SELECT sid_dbprocessinfo_dbnumber FROM rewardsnow.dbo.processingjob WHERE sid_processingjob_id = @jobid)          

	IF @sid_dbprocessinfo_dbnumber = '229'
	BEGIN

		INSERT INTO @excludeTips (tipnumber)   
		SELECT DISTINCT dim_rnicustomer_rniid AS tipnumber   
		FROM RNICustomer rnic   
		WHERE sid_dbprocessinfo_dbnumber='229'
			AND dim_rnicustomer_cardnumber LIKE '493720%'
		
	END
	
	RETURN

END 

GO