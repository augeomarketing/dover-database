IF OBJECT_ID(N'ufn_SplitTable', N'TF') is not null
	DROP FUNCTION ufn_SplitTable
GO

CREATE FUNCTION ufn_SplitTable
(
	@inputTable SplitTableType READONLY
)
RETURNS @splitTable TABLE (KeyItem VARCHAR(MAX), Item VARCHAR(MAX))
AS
BEGIN
	DECLARE @KeyItem VARCHAR(MAX);
	DECLARE @List VARCHAR(MAX);
	DECLARE @Delim VARCHAR(1);
	DECLARE @MyID INT;
	
	DECLARE @MyInputTable TABLE
	(
		MyID INT IDENTITY (1,1)
		, KeyItem VARCHAR(MAX)
		, ItemList VARCHAR(MAX)
		, ListDelim VARCHAR(1)
	);
	
	INSERT INTO @MyInputTable(KeyItem, ItemList, ListDelim)
	SELECT KeyItem, ItemList, ListDelim FROM @inputTable;
	
	SET @MyID = (SELECT TOP 1 MyID FROM @MyInputTable);
	WHILE @MyID IS NOT NULL
	BEGIN
		SELECT @KeyItem = KeyItem
			, @List = ItemList
			, @Delim = ListDelim
		FROM @MyInputTable
		WHERE MyID = @MyID;
		
		INSERT INTO @splitTable (KeyItem, Item)
		SELECT KeyItem, ListElement FROM RewardsNow.dbo.ufn_SplitWithKey(@KeyItem, @List, @Delim);
		
		DELETE FROM @MyInputTable WHERE MyID = @MyID;
		SET @MyID = (SELECT TOP 1 MyID FROM @MyInputTable);
	
	END;
	RETURN;
END

/*
DECLARE @MyTable SplitTableType
INSERT INTO @MyTable(KeyItem, ItemList, ListDelim)
SELECT 1, 'A,B,C,D', ','
UNION ALL SELECT 2, 'A,B,C,D', ','
UNION ALL SELECT 3, 'D,E,F,G', ','

SELECT * FROM ufn_SplitTable(@MyTable)
*/


GO