USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetFirstOfMonth]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetFirstOfMonth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetFirstOfMonth]
(
	@inputDate DATETIME
)
RETURNS DATE
AS
BEGIN
	DECLARE @out DATE
	SET @out = CONVERT(DATE, CAST(FLOOR(CAST(@inputDate AS DECIMAL(12, 5))) - (DAY(@inputDate) - 1) AS DATETIME))
	
	RETURN @out
END
GO
