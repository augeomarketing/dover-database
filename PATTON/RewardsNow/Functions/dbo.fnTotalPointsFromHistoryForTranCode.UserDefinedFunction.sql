USE REWARDSNOW
GO

if object_id('fnTotalPointsFromHistoryForTrancode') is not null
    drop function dbo.fnTotalPointsFromHistoryForTrancode
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create function [dbo].[fnTotalPointsFromHistoryForTranCode] 
    (@TipNumber     nvarchar(16), 
     @StartDate     datetime, 
     @EndDate       datetime, 
     @TranCode      nvarchar(2) )

returns int

AS

BEGIN
    declare @dbname             nvarchar(50)
    declare @sql                nvarchar(max)

    declare @points	int = 0

    set @dbname = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = left(@tipnumber,3))


    set @sql = 'set @points = (select sum(points) 
					            from ' + @dbname + '.dbo.History_Stage
            					where tipnumber = <<@TipNumber>>
            					and histdate between <<@StartDate>> and <<@EndDate>>
            					and trancode = <<@TranCode>> )'

    set @sql = replace(
                    replace(
                        replace(
                            replace(@sql, '<<@tipnumber>>', char(39) + @tipnumber + char(39)), 
                        '<<@startdate>>', char(39) + cast(@startdate as nvarchar(20)) + char(39)),
                    '<<@enddate>>', char(39) + cast(@enddate as nvarchar(20)) + char(39)),
                '<<@trancode>>', char(39) + @trancode + char(39))  


    exec sp_executesql @sql, N'@points int OUTPUT', @points = @points OUTPUT

	return isnull(@points, 0)

END
GO
