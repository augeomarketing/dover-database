USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_stmtGetFlagValueForTipnumber]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_stmtGetFlagValueForTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		S Blanchette
-- Create date: 10/2011
-- Description:	Get flag value from stmtflag table
-- Mod:         C Heit
-- Description: Added steps for flagging based on
--              Employee Flag
--
-- SEB 5/1/2014 Add code for flag for 657
-- =============================================
CREATE FUNCTION [dbo].[ufn_stmtGetFlagValueForTipnumber]  
(
	-- Add the parameters for the function here
	@processingjobid bigint
	, @outputColumn int
	, @outputRow int
	, @tipnumber varchar(15) 
)
RETURNS nvarchar(max)  
AS
BEGIN
	-- Declare the return variable here
	DECLARE @tipfirst varchar(3)  
			, @dbname varchar(255)  
			, @sql nvarchar(max)

	set @tipfirst = left(@tipnumber, 3)  
	select @dbname = dbnamepatton from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst  


	--UNIQUE VALUES HERE...
	--FLAG VALUES BASED ON EMPLOYEE FLAG
	if @tipfirst IN ('241') 
	BEGIN
		set @sql = '
		INSERT INTO Rewardsnow.dbo.stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) 
		SELECT <PROCESSINGJOBID>, <COLUMN>, <ROW> 
		, CASE  
			WHEN ISNUMERIC(EmployeeFlag) = 1 THEN EmployeeFlag 
			WHEN EmployeeFlag = ''Y'' THEN ''1'' 
			WHEN EmployeeFlag = ''N'' THEN ''0'' 
			ELSE ''0'' 
		END 		
		FROM [<DBNAME>].dbo.customer WHERE TIPNUMBER = ''<TIPNUMBER>''
		'	
	END
	ELSE
		if @tipfirst IN ('657') 
		BEGIN
		set @sql = '
		INSERT INTO Rewardsnow.dbo.stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) 
		SELECT <PROCESSINGJOBID>, <COLUMN>, <ROW> 
		, CASE  
			WHEN MISC1 = ''USU'' THEN ''1'' 
			ELSE ''0'' 
		END 		
		FROM [<DBNAME>].dbo.customer WHERE TIPNUMBER = ''<TIPNUMBER>''
		'	
		END
		
		ELSE

		--DEFAULT HERE
		--FLAG VALUES BASED ON BIN/ENTRY IN STMTFLAG
		BEGIN

			-- Add the T-SQL statements to compute the return value here
			set @sql = 'INSERT INTO Rewardsnow.dbo.stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) 
						SELECT <PROCESSINGJOBID>, <COLUMN>, <ROW>, isnull((select dim_stmtflag_flagvalue 
													from Rewardsnow.dbo.stmtflag
													where sid_dbprocessinfo_dbnumber=''<TIPFIRST>'' 
														and dim_stmtflag_Code1 like left((select top 1 LEFT(ltrim(acctid),9)
																							from [<DBNAME>].dbo.affiliat
																							where tipnumber = ''<TIPNUMBER>'' and acctstatus=''A'')
																						,len(dim_stmtflag_Code1))),''0'') '
		END	 
   
	set @sql = replace(replace(replace(replace(replace(replace(@sql
	, '<DBNAME>', @dbname)
	, '<TIPFIRST>', @tipfirst)
	, '<TIPNUMBER>', @tipnumber)
	, '<PROCESSINGJOBID>', @processingjobid)
	, '<COLUMN>', @outputColumn)
	, '<ROW>', @outputRow)
  
	-- Return the result of the function
	RETURN @sql 

END
GO
