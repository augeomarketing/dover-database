USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RemovePrefixParseFirstName]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RemovePrefixParseFirstName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RemovePrefixParseFirstName]
(
	@name           varchar(max)
)
RETURNS VARCHAR(MAX)

AS

BEGIN
    declare @firstname              varchar(max) = ''
    declare @prefix                 varchar(max) = ''

    declare @ctr                    int
    declare @hldctr                 int

    declare @nbrspaces              int = 0
    declare @position               int = 1
    declare @firstspaceposition     int = 0

    select @ctr = 0, @hldctr = 0

    --
    -- Should there be excessive spaces in the name, remove them
    select @name = replace(@name, '  ', ' |') -- replace double spaces with space + |
    select @name = replace(replace(@name, '| ', ''), '|', '') -- replace "| " with empty string, then remove any remnant "|" with empty string

    --
    -- Count number of spaces in the name
    -- If name has more than 3 spaces, assume its a business name, and return entire name, don't parse it.
    -- 
    select @ctr = charindex(' ', @name, @position)

    -- Store the first space's position
    select @firstspaceposition = @ctr

    -- Loop until no more spaces found
    while @ctr > 0
    BEGIN
        select @nbrspaces += 1;
        select @position = @ctr + 1
        select @ctr = charindex(' ', @name, @position)
    END

    if (@nbrspaces >3) OR (@firstspaceposition = 0) -- if business name or name has no spaces
        select @firstname = @name
    else
    BEGIN  -- Regular personal name
        select @prefix = left(@name, @firstspaceposition-1)  -- extract first part of name
        
        -- If the first part of name is in the namepart table, its a prefix (ie Dr, Mr, MRS, etc)
        if exists(select 1
                  from rewardsnow.dbo.rninamepart rnp join rewardsnow.dbo.rninameparttype rnpt
                    on rnp.sid_rninameparttype_id = rnpt.sid_rninameparttype_id
                  where dim_rninameparttype_name like '%prefix%'
                  and dim_rninamepart_active = 1
                  and dim_rninamepart_namepart = @prefix)
        BEGIN              -- It is a prefix, hack it off
            select @firstname = ltrim(rtrim( right(@name, len(@name)-@firstspaceposition) ))
        END
        ELSE  -- no prefix found, leave name alone
        BEGIN
            select @firstname = @name    
        END
        
        -- Find first space in the name
        select @position = charindex(' ', @firstname, 1)
        
        -- Section off first name and return as the result
        select @firstname = left(@firstname, @position-1)

    END
    
    -- for any names -sometimes business names that get parsed- they return 1 letter
    -- ex.  "A Soulful Integration"  would pass the first "spaces" test, and ultimately
    -- return "A".
    -- This test will simply return the full name if the length of the first name is 1 character long
    if len(@firstname) <2
        set @firstname = @name

    return @firstname
END
/* Test Harness

SELECT [RewardsNow].[dbo].[ufn_RemovePrefixParseFirstName] ('DR Paul H Butler')

*/
GO
