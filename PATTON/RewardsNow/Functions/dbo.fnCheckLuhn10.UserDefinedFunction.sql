USE [RewardsNow]
GO

/****** Object:  UserDefinedFunction [dbo].[fnCheckLuhn10]    Script Date: 12/21/2010 10:30:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnCheckLuhn10]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnCheckLuhn10]
GO

USE [RewardsNow]
GO

/****** Object:  UserDefinedFunction [dbo].[fnCheckLuhn10]    Script Date: 12/21/2010 10:30:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnCheckLuhn10]
/**********************************************************************************************************************
 The function accepts a credit card or other number either as a VARCHAR or an INT and returns a 1 if the numbers match
 the LUHN 10 checksum specification and 0 if not.

 The input number does NOT need to be digits only.  Numbers like 1234-5678-9012-3456 or 1234 5678 9012 3456 are
 acceptable.

 Revision history:
 Rev 00 - 03/08/2005 - Jeff Moden - Initial creation and test.
 Rev 01 - 12/28/2006 - Jeff Moden - Performance enhancement using @Multiple thanks to Peter Larson
**********************************************************************************************************************/
--===== Declare I/O parameters
        (
        @Luhn VARCHAR(8000)
        )
RETURNS INT
     AS
  BEGIN
--=====================================================================================================================
--===== Declare local variables
DECLARE @CleanedLuhn  VARCHAR(8000),          --The Luhn number stripped of non-numeric characters
        @DigitProduct INT,                    --The result of multiplying the digit times the multiplier
        @Multiplier   INT,                    --1 for odd position digits, 2 for even position digits
        @Sum          INT,                    --The Luhn 10 sum
        @WorkLuhn     VARCHAR(8000)           --The clean Luhn number 

--===== If present, remove all non-digit characters
     IF PATINDEX('%[^0-9]%',@Luhn) > 0        --If any non-digit characters exist, then...
 SELECT @CleanedLuhn = ISNULL(@CleanedLuhn,'') 
                     + SUBSTRING(@Luhn,t.N,1)
   FROM rewardsnow.dbo.Tally t                           --Contains a list of whole numbers
  WHERE t.N <= LEN(@Luhn)                     --Limits the join/set based "loop" to the length of the Luhn
    AND SUBSTRING(@Luhn,t.N,1) LIKE '[0-9]'   --Non-digits are ignored, only 0-9 are included

    -- PHB 21 Dec 2010
    -- Added check if @cleanedluhn is null and test if the @luhn (PAN) is not numeric
    -- if this test passes - force a value of 1 into the @sum.  The return statement does a 1-sign(1%10) which will return a 0 to the calling sproc
    -- signifying that the PAN is not a valid card#
    if @cleanedluhn is null and isnumeric(@luhn) = 0
        set @sum = 1  -- this will return a 1 - failed luhn test
    else
    BEGIN  -- still appears to be a valid PAN.... continue test
        --===== Presets
             -- Note: Use the cleaned Luhn if it needed cleaning or the original Luhn if not
         SELECT @Sum         = 0,
                @Multiplier  = 1,
                @WorkLuhn    = ISNULL(@CleanedLuhn,@Luhn)

        --===== Calculate the Luhn 10 sum
         SELECT @DigitProduct = @Multiplier           --1 for odd numbers, 2 for even numbers
                              * SUBSTRING(@WorkLuhn, t.N, 1), --A given digit in the Luhn
                @Sum          = @Sum                  --Luhn 10 sum starts at 0
                              + @DigitProduct / 10    --The 1st digit for products > 9, 0 for product < 10
                              + @DigitProduct % 10,   --The 2nd digit for products > 9 or only digit for product < 10
                @Multiplier   = 3 - @Multiplier       --3-1=2, then 3-2=1, repeats  
           FROM rewardsnow.dbo.Tally t WITH (NOLOCK)             --Contains a list of whole numbers
          WHERE t.N <= LEN(@WorkLuhn)                 --Limits the join/set based "loop" to the length of the cleaned Luhn
          ORDER BY t.N DESC
    END
        --===== If the sum is evenly divisible by 10, then check is ok... return 1.  
             -- Otherwise, return 0 as "Failed" check
 RETURN 1-SIGN(@SUM%10)
--=====================================================================================================================
    END

GO


