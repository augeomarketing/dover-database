USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_AddSingleSpace]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_AddSingleSpace]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_AddSingleSpace] (@tipfirst varchar(3), @data varchar(max))    
RETURNS varchar(max)    
AS    
BEGIN    
 DECLARE @output VARCHAR(max)    
 SET @output = RTRIM(LTRIM(@data))
 SET @output = @output + ' '    
 RETURN @output    
END
GO
