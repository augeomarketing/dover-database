USE [Rewardsnow]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].ufn_RawDataMichFirstState (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(max)

AS  

BEGIN
    RETURN	CASE
				when (select rewardsnow.[dbo].[ufn_RawDataExtractMerchantDescriptionState](@tipfirst, @data))   = 'MI' then 'MI'
				else 'ZZ'
			END

END  

GO


/* Test harness

select dbo].ufn_RawDataMichFirstState ('651', 'Test Merchant DD')


*/
