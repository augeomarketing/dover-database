USE [RewardsNow]
GO
IF OBJECT_ID(N'ufn_LegacyConvert_CustomerCode') IS NOT NULL
	DROP FUNCTION ufn_LegacyConvert_CustomerCode
GO

CREATE FUNCTION ufn_LegacyConvert_CustomerCode
(
	@sid_dbprocessinfo_dbnumber VARCHAR(50)
	, @inputdata VARCHAR(MAX)
) RETURNS varchar(max)
AS
BEGIN
	DECLARE @output VARCHAR(MAX)
	SET @output = @inputdata
	
	SET @output = CASE @inputdata
		WHEN 'A' THEN '1'
		WHEN 'S' THEN '1'
		WHEN 'I' THEN '3'
		WHEN 'C' THEN '99'
		ELSE @inputdata
	END
	
	RETURN @output
END
GO
