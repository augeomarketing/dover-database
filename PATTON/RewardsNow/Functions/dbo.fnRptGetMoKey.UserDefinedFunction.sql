USE [Rewardsnow]
GO
/****** Object:  UserDefinedFunction [dbo].[fnRptGetMoKey]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* Function returns key value for the month supplied as an integer.  A key value for a month */
/* is a five character string of the form '01Jan', '02Feb',...'12Dec' wich will collate as */
/* desired.  If the argument not in the range 1 - 12 is supplied, 'ERROR' is returned */

CREATE FUNCTION [dbo].[fnRptGetMoKey] (@MonthNum INT)
RETURNS CHAR(5)
AS
BEGIN
	RETURN CASE @MonthNum		-- Set the month string for the range records we will build
		When  1 Then '01Jan'
		When  2 Then '02Feb'
		When  3 Then '03Mar'
		When  4 Then '04Apr'
		When  5 Then '05May'
		When  6 Then '06Jun'
		When  7 Then '07Jul'
		When  8 Then '08Aug'
		When  9 Then '09Sep'
		When 10 Then '10Oct'
		When 11 Then '11Nov'
		When 12 Then '12Dec'
		ELSE 'ERROR'
	END
END
GO
