USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_stmtGetFirstNameForTipnumber]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_stmtGetFirstNameForTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_stmtGetFirstNameForTipnumber]  
(
	@processingjobid BIGINT
	, @outputColumn INT
	, @outputRow INT
	, @tipnumber VARCHAR(15)  
)  
RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @tipfirst VARCHAR(3)  
 DECLARE @dbname VARCHAR(255)  
 DECLARE @sql NVARCHAR(MAX)  
   
 SET @sql = 'INSERT INTO Rewardsnow.dbo.stmtValue(sid_processingjob_id, dim_stmtvalue_column, dim_stmtvalue_row, dim_stmtvalue_value) 
 select <PROCESSINGJOBID>, <COLUMN>, <ROW>, Rewardsnow.dbo.ufn_Trim(LEFT(ACCTNAME1, CHARINDEX('' '', ACCTNAME1))) from [<DBNAME>].dbo.CUSTOMER where TIPNUMBER = ''<TIPNUMBER>'''  
   
 SET @tipfirst = LEFT(@tipnumber, 3)  
 SELECT @dbname = dbnamepatton FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst  
   
 SET @sql = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@sql
	, '<DBNAME>', @dbname)
	, '<TIPNUMBER>', @tipnumber)
	, '<PROCESSINGJOBID>', @processingjobid)
	, '<COLUMN>', @outputColumn)
	, '<ROW>', @outputRow)
   
 RETURN @sql  
  
END
GO
