USE [Rewardsnow]
GO

if object_id('ufn_mappingextractRight4affiliatcustidWithLeading') is not null
	drop function dbo.ufn_mappingextractRight4affiliatcustidWithLeading


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[ufn_mappingextractRight4affiliatcustidWithLeading]
    (@tipfirst              varchar(3))

returns nvarchar(max)

as

BEGIN

    declare @db         nvarchar(50)
    declare @sql        nvarchar(max)

    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    --set @sql = 'if object_id(''tempdb..#__temp'') is not null drop table #__temp
				--create table #__temp (tipnumber varchar(15),
				--					  lastname  varchar(40),
				--					  mapvalue  varchar(max) )
				
				--insert into #__temp (tipnumber, lastname, mapvalue)
			 --   select distinct tipnumber, lastname, ''#'' + (select top 1 right(ltrim(rtrim(custid)),4)
				--								     from ' + @db + '.dbo.affiliat a1
				--								     where a1.tipnumber = aff.tipnumber
				--								     and isnull(a1.acctstatus, ''A'') != ''C'') mapvalue
    --            from ' + @db + '.dbo.affiliat aff
    --            where aff.acctstatus != ''C'' 
                
    --            /* TARGET */
    --            select tipnumber, lastname, mapvalue from #__temp where 1=1
    --            '

	set @sql = 'select distinct tipnumber, lastname, ''#'' + (select top 1 right( ltrim(rtrim(custid)), 4)
															  from ' + @db + '.dbo.affiliat a1
															  where a1.tipnumber = aff.tipnumber
															  and isnull(a1.acctstatus, ''A'') != ''C'') mapvalue
				from ' + @db + '.dbo.affiliat aff
				where isnull(aff.acctstatus, ''A'') != ''C'' '
                
    return @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingextractRight4affiliatcustidWithLeading ('207')

*/



GO


