USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataConvertStringToDecimal]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawDataConvertStringToDecimal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawDataConvertStringToDecimal] (@tipfirst varchar(3), @data varchar(max))  
RETURNS decimal(18,2)

AS  

BEGIN
    RETURN cast( ltrim(rtrim(@data)) as decimal(18,2))
END
GO
