USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetCoopTipnumber]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetCoopTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the point value from the Portal_Bonus_Codes or TranType table


CREATE FUNCTION [dbo].[ufn_GetCoopTipnumber] (@guid varchar(512))  
RETURNS VARCHAR(20)
AS 
 
BEGIN 
	DECLARE @Tipnumber VARCHAR(20)
	BEGIN
		SELECT TOP 1 @Tipnumber = rtrim(ltrim(tipnumber))
		from rn1.Coop.dbo.Account 
		WHERE membernumber = @guid
		
		

	END

	RETURN @Tipnumber

END
GO
