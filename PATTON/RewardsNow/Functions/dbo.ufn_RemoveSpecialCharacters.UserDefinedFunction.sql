USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RemoveSpecialCharacters]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RemoveSpecialCharacters]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
	10 Line Feed 
	13 Carriage Return
	34 Double Quote
	35 Pound Sign
	39 Apostrophe (Single Quote)
	44 Comma
	46 Period
	47 Forward Slash
	96 Backwards Quote
	140 A foreign string from original procedures, included here for completness

	Double Spaces also Removed 
*/

CREATE FUNCTION [dbo].[ufn_RemoveSpecialCharacters](@INPUT VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @OUTPUT VARCHAR(MAX)

	SET @OUTPUT = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
		@INPUT
		, CHAR(10), '')
		, CHAR(13), '')
		, CHAR(34), '')
		, CHAR(35), '')
		, CHAR(39), '')
		, CHAR(44), '')
		, CHAR(46), '')
		, CHAR(47), '')
		, CHAR(96), '')
		, CHAR(140), '')
		
		
	SET @OUTPUT = RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(
		@OUTPUT
		,' ','<>')
		,'><','')
		,'<>',' ')
		))
	RETURN @OUTPUT
END
GO
