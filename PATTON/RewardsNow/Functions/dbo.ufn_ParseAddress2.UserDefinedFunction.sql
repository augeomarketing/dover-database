USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ParseAddress2]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ParseAddress2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_ParseAddress2]
	(@tipfirst			varchar(3),
	 @address			varchar(255))
RETURNS varchar(50)

AS

BEGIN
	declare @address2	varchar(50)
-- 135TBA sends us address2 & city/state/zip inconsistently in either ADDRESS2 or ADDRESS3
-- This function will parse out ADDRESS2

	if len(@address) >= 30  -- Address does contain a valid address2 line and not city/state/zip
		set @address2 = left(@address,30)
	else
		set @address2 = ''

return @address2
END
GO
