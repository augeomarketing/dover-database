USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerPrimarySIDsForTip]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNICustomerPrimarySIDsForTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	Will return the first record (by tip) order first by the primary indicator and then the sid.
	If primary is flagged, return that, otherwise, return the first you find
*/
CREATE FUNCTION [dbo].[ufn_RNICustomerPrimarySIDsForTip] (@tipfirst varchar(3))
RETURNS TABLE
AS
RETURN 
	SELECT dim_rnicustomer_rniid, sid_rnicustomer_id
	FROM
	(
		SELECT TOP 100 PERCENT ROW_NUMBER() OVER (PARTITION BY dim_rnicustomer_rniid ORDER BY dim_RNICustomer_CustomerCode, dim_RNICustomer_PrimaryIndicator desc, sid_RNICustomer_id desc) Ranking
		,	dim_RNICustomer_RNIId
		,	sid_rnicustomer_id
		FROM	RNICustomer
		WHERE	dim_RNICustomer_TipPrefix = @tipfirst
		ORDER BY
				dim_RNICustomer_CustomerCode
			,	isnull(dim_RNICustomer_PrimaryIndicator, 0) desc
			,	sid_RNICustomer_id
	) p
	WHERE	p.Ranking = 1
GO
