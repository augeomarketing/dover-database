USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_RemoveLineFeed]    Script Date: 04/20/2016 11:09:50 ******/
DROP FUNCTION [dbo].[ufn_RawData_RemoveLineFeed]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_RemoveLineFeed] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(max)  
AS  
BEGIN  
 DECLARE @output VARCHAR(max)  
 SET @output = ltrim(rtrim(REPLACE(@data, char(10),'') ))
 RETURN @output  
END
GO
