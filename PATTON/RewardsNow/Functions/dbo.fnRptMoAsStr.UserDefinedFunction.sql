USE [Rewardsnow]
GO
/****** Object:  UserDefinedFunction [dbo].[fnRptMoAsStr]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnRptMoAsStr] (@MonthNum INT)
RETURNS VARCHAR(9)
AS
BEGIN
    RETURN CASE @MonthNum
	WHEN 1 THEN 'January '
	WHEN 2 THEN 'February '
	WHEN 3 THEN 'March '
	WHEN 4 THEN 'April '
	WHEN 5 THEN 'May '
	WHEN 6 THEN 'June '
	WHEN 7 THEN 'July '
	WHEN 8 THEN 'August '
	WHEN 9 THEN 'September '
	WHEN 10 THEN 'October '
	WHEN 11 THEN 'November '
	WHEN 12 THEN 'December '
	ELSE 'January '
    END
END
GO
