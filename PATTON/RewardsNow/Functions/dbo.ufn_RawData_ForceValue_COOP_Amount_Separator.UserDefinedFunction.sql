USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_ForceValue_COOP_Amount_Separator]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_ForceValue_COOP_Amount_Separator]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_ForceValue_COOP_Amount_Separator] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(1)  
AS  
BEGIN  
 DECLARE @output VARCHAR(1)  
 SET @output = char(128) 
 RETURN @output  
END
GO
