USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_rawdata_parseMSUMID]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_rawdata_parseMSUMID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_rawdata_parseMSUMID] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(max)

AS  

BEGIN
	set @data = ltrim(rtrim(@data))

	return replace( substring(@data, 1, charindex(':', @data)-1), ' ', '')

END
GO
