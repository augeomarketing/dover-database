USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ParseCityStateZip]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ParseCityStateZip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_ParseCityStateZip]
	(@address			varchar(255),
	 @parsetype			int = 0)
RETURNS varchar(50)

AS

--
-- PARSE TYPE
-- 1 = City
-- 2 = State
-- 3 = Postal Code
--

BEGIN

	declare @wrkaddress		varchar(50)
	declare @city			varchar(50) = ''
	declare @state			varchar(3) = ''
	declare @postalcode		varchar(10) = ''

	declare @parsedaddress	varchar(50)

	set @wrkaddress = reverse( ltrim(rtrim(@address)))

	set @postalcode = ltrim(rtrim( reverse(left(@wrkaddress, charindex(' ',@wrkaddress, 1))) ))

	set @wrkaddress = ltrim(rtrim( right(@wrkaddress, len(@wrkaddress)-charindex(' ',@wrkaddress, 1)) ))

	set @state = reverse(left(@wrkaddress, charindex(' ',@wrkaddress, 1)))

	set @city = reverse( ltrim(rtrim( right(@wrkaddress, len(@wrkaddress) - charindex(' ',@wrkaddress,1)) )) )


	set @parsedaddress =	CASE
								when @parsetype = 1 then @city
								when @parsetype = 2 then @state
								when @parsetype = 3 then @postalcode
								else @address
							END

	RETURN @PARSEDADDRESS
END

/* test harness

select dbo.ufn_ParseCityStateZip('Dover NH 03820', 0)
select dbo.ufn_ParseCityStateZip('Dover NH 03820', 1)
select dbo.ufn_ParseCityStateZip('Dover NH 03820', 2)
select dbo.ufn_ParseCityStateZip('Dover NH 03820', 3)

*/
GO
