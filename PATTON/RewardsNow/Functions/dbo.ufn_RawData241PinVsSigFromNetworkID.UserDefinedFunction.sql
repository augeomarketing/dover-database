USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData241PinVsSigFromNetworkID]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData241PinVsSigFromNetworkID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData241PinVsSigFromNetworkID] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(1)  
AS  
BEGIN
	DECLARE @output VARCHAR(1)  
	SET @output = 1

	IF @data LIKE '655000%'
		SET @output = 2
 
 RETURN @output  
END
GO
