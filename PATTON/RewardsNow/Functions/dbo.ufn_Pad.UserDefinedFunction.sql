USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_Pad]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_Pad]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_Pad] 
(
	@inputstring VARCHAR(MAX)
	, @direction VARCHAR(1)
	, @padlength INTEGER
	, @padcharacter VARCHAR(1)
) RETURNS VARCHAR(MAX)
AS
BEGIN
	--Trim extra spaces from front and back of string
	SET @inputstring = RewardsNow.dbo.ufn_Trim(@inputstring)
	DECLARE @output VARCHAR(MAX)

	SET @output = @inputstring --default if nothing else changes it...
	--if anything goes wrong or any of the parameters don't fit, the input is returned as the output
	
	DECLARE @padstring VARCHAR(MAX)

	--functions don't have default values for input variables, so they must be declared
	SET @direction = UPPER(ISNULL(@direction, 'L'))
	
	SET @padlength = ISNULL(@padlength, 0)
	IF @padlength < 0 
		SET @padlength = 0
	
	SET @padcharacter = ISNULL(@padcharacter, ' ')
		
	--if pad length is less than or equal to input then return input
	IF (ISNULL(@inputstring, 'Ç') <> 'Ç' AND @padlength >= len(@inputstring))
	BEGIN
		--build out characters to pad. Example: '000000' would result from '0', '6'...
		SET @padstring = REPLICATE(@padcharacter, @padlength)
		
		--build output (direction is defaulted to 'L', so only need to test for 'R'
		--L = '000000' + 'INPUT' OR '000000INPUT'; R = 'INPUT' + '000000' OR 'INPUT000000'
		--This value is then trimmed down to the length specified by @padlength
		--In our example L = '0INPUT'; R = 'INPUT0'
		IF @direction = 'R'
			SET @output = LEFT((@inputstring + @padstring), @padlength)
		ELSE
			SET @output = RIGHT((@padstring + @inputstring), @padlength)		
		--END IF (I really wish there was an END-IF in TSQL to help with code readability....)	
	END
	
	RETURN @output
END
	
/*	


select dbo.ufn_pad('1234567', 'L', 10, '0')
select dbo.ufn_pad('1234567', 'R', 10, '0')

@inputstring VARCHAR(MAX)
	, @direction VARCHAR(1)
	, @padlength INTEGER
	, @padcharacter VARCHAR(1)


*/
GO
