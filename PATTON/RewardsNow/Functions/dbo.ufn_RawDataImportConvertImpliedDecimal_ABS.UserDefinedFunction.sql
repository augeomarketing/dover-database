USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataImportConvertImpliedDecimal_ABS]    Script Date: 10/16/2015 15:42:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_RawDataImportConvertImpliedDecimal_ABS] (@tipfirst varchar(3), @value varchar(max))
	returns decimal(18,2)

AS

BEGIN

declare @decimalvalue		decimal(18,2) = 0

set @decimalvalue = ABS((select dbo.ufn_RawDataConvertStringtodecimal(@tipfirst, @value) / 100))

return @decimalvalue

END


/*  Test harness


select dbo.ufn_RawDataImportConvertImpliedDecimal('135', '8335')


*/
GO
