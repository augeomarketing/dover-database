USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_webGetURL]    Script Date: 11/18/2010 09:41:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_webGetURL]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_webGetURL]
GO

USE [RewardsNOW]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_webGetURL]    Script Date: 11/18/2010 09:41:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ufn_webGetURL] 
(
	-- Add the parameters for the function here
	@tipfirst VARCHAR(3)
)
RETURNS varchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @url varchar(50)

	-- Add the T-SQL statements to compute the return value here
	select @url = dim_webinit_baseurl 
	from rn1.RewardsNow.dbo.webinit 
	where 1=1 
	and dim_webinit_active = 1 
	and dim_webinit_baseurl not like '%testing%' 
	and dim_webinit_baseurl not like '%/qa/%' 
	and (dim_webinit_defaulttipprefix = LEFT(@tipfirst,3)  
	OR 	dim_webinit_defaulttipprefix in ( 
		select dim_loyaltytip_prefix   
		from Catalog.dbo.loyaltytip   
		where sid_loyalty_id in (
			select sid_loyalty_id 
			from Catalog.dbo.loyaltytip 
			where dim_loyaltytip_prefix = LEFT(@tipfirst,3)   
			AND dim_loyaltytip_active = 1  
			) 
		)
	)
	ORDER BY sid_webinit_id 	

	-- Return the result of the function
	RETURN @url

END

GO


