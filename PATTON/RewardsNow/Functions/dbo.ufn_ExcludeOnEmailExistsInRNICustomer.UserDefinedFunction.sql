USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ExcludeOnEmailExistsInRNICustomer]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ExcludeOnEmailExistsInRNICustomer]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ExcludeOnEmailExistsInRNICustomer] (@jobid BIGINT)
RETURNS @ExcludeTips TABLE (tipnumber          varchar(15) primary key)
        
AS

BEGIN

    DECLARE @sid_dbprocessinfo_dbnumber VARCHAR(3)
    set @sid_dbprocessinfo_dbnumber = (SELECT sid_dbprocessinfo_dbnumber FROM rewardsnow.dbo.processingjob where sid_processingjob_id = @jobid)

        insert into @excludeTips (tipnumber)
		select distinct dim_rnicustomer_rniid as tipnumber
		from RNICustomer rnic
		left outer join 
		(
			select dim_emailwelcomekit_tipnumber 
			from rn1.RewardsNow.dbo.emailwelcomekit 
			where dim_emailwelcomekit_sent = 2
			and dim_emailwelcomekit_tipfirst = @sid_dbprocessinfo_dbnumber
		) as wk
		        on rnic.dim_RNICustomer_RNIId = wk.dim_emailwelcomekit_tipnumber
		where 
			isnull(rnic.dim_RNICustomer_EmailAddress, '') <> '' 
			and rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
			and wk.dim_emailwelcomekit_tipnumber is null

return 

END
GO
