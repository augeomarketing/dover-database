USE [Rewardsnow]
GO
/****** Object:  UserDefinedFunction [dbo].[CAPICOMEncrypt]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CAPICOMEncrypt] (@TextToEncrypt varchar(4000), @Secret varchar(1000))
RETURNS varchar(4000)
AS
BEGIN
  DECLARE @EncryptedText varchar(4000)
  DECLARE @rc int
  DECLARE @object int
  DECLARE @Method_call varchar(4000)
  EXEC @rc = sp_OACreate 'CAPICOM.EncryptedData', @object OUT
  if @rc <> 0
    RETURN NULL
  
  EXEC @rc = sp_OASetProperty @Object, 'Algorithm.Name', 3 -- 3DES
  if @rc <> 0
    RETURN NULL

  set @method_call = 'SetSecret("' + @Secret + '")'
  EXEC @rc=sp_OAMethod @Object, @method_call
  if @rc <> 0
    RETURN NULL
  
  EXEC @rc=sp_OASetProperty @Object, 'Content',@TextToEncrypt
  if @rc <> 0
    RETURN NULL

  EXEC @rc=sp_OAMethod @Object, 'Encrypt(0)', @EncryptedText out
  if @rc <> 0
    RETURN NULL

  EXEC @rc = sp_OADestroy @object

  RETURN @EncryptedText
END
GO
