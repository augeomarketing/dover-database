USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_Least]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_Least]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_Least](@numberlist varchar(max), @separator varchar(1))
RETURNS DECIMAL
AS
BEGIN
	DECLARE @output DECIMAL
	DECLARE @num1 DECIMAL
	DECLARE @num2 DECIMAL
	declare @curnum int
	
	
	declare @vals table(myid int identity(1,1) primary key, item varchar(50))
	
	insert into @vals(item)
	select Item from RewardsNow.dbo.split(@numberlist, @separator)
	
	set @num1 = (select cast(item as decimal) from @vals where myid = 1)
	
	set @curnum = 2
	
	while @curnum <= (select max(myid) from @vals)
	begin
		set @num2 = (select cast(item as decimal) from @vals where myid = @curnum)
		
		set @num1 = (0.5 * ((@num1 + @num2) - abs(@num1 - @num2)))
		
		set @curnum = @curnum + 1	
	end
	
	set @output = @num1
		
	RETURN @output 
END
/* TEST HARNESS

SELECT RewardsNow.dbo.ufn_Least('5|34|15|12|8|4', '|')
--Output = 4

*/
GO
