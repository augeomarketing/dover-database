USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractaffiliatacctid_EMAIL]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractaffiliatacctid_EMAIL]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractaffiliatacctid_EMAIL]    
    (@tipfirst              varchar(3))    
    
returns nvarchar(max)    
    
as    
    
BEGIN    
    
    declare @db         nvarchar(50)    
    declare @sql        nvarchar(max)    
    
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)    
    
    set @sql = 'select tipnumber, isnull(lastname, '''') lastname, LTRIM(RTRIM(acctid)) mapvalue    
                from ' + @db + '.dbo.affiliat    
                where accttype = ''EMAILADDRESS'''    
                    
    return @sql    
END    
    
    
/*  Test harness    
    
select rewardsnow.dbo.ufn_mappingextractaffiliatacctid_EMAIL ('C01')    
    
-- resulting @SQL value:    
select tipnumber, lastname, ltrim(rtrim(acctid)) mapvalue                  from [C01].dbo.affiliat                  where ACCTTYPE = 'EMAILADDRESS'     
    
    
*/
GO
