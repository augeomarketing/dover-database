USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractlast6_CARD]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractlast6_CARD]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractlast6_CARD]  
    (@tipfirst              varchar(3))  
  
returns nvarchar(max)  
  
as  
  
BEGIN  
  
    declare @db         nvarchar(50)  
    declare @sql        nvarchar(max)  
  
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)  
  
    set @sql = 'select tipnumber, isnull(lastname, '''') lastname, right(rtrim(acctid),6) mapvalue  
                from ' + @db + '.dbo.affiliat  
                where accttype = ''CARD'''  
                  
    return @sql  
END  
  
  
/*  Test harness  
  
select rewardsnow.dbo.ufn_mappingextractlast6_CARD ('a02')  
  
-- resulting @SQL value:  
select tipnumber, lastname, right( ltrim(rtrim(acctid)), 6) mapvalue                  from [a02].dbo.affiliat                  where acctstatus != 'C'   
  
  
*/
GO
