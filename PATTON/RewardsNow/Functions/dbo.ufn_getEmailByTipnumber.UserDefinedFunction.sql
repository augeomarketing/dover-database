USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_getEmailByTipnumber]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_getEmailByTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_getEmailByTipnumber]
(
	@tipnumber VARCHAR(20)
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @email VARCHAR(50)

	SELECT @email = ISNULL(email, '')
	FROM rn1.rewardsnow.dbo.[1security]
	WHERE Tipnumber = @tipnumber
	
	IF @@ROWCOUNT = 0
		SET @email = ''

	-- Return the result of the function
	RETURN @email

END
GO
