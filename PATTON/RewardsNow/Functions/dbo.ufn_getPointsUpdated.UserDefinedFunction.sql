USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_getPointsUpdated]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_getPointsUpdated]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ufn_getPointsUpdated]
(
	@tipfirst VARCHAR(3)
)
RETURNS SMALLDATETIME
AS
BEGIN
	DECLARE @pointsUpdated SMALLDATETIME
	SET @pointsUpdated = (SELECT Pointsupdated FROM rewardsnow.dbo.dbprocessinfo WHERE dbnumber = @tipfirst)
	RETURN @pointsUpdated

END
GO
