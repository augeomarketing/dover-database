USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_COOPForeignCurrency]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_COOPForeignCurrency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_COOPForeignCurrency](@tipfirst varchar(3), @data varchar(max))
RETURNS varchar(max)

AS 

BEGIN

	SET @data = LTRIM(RTRIM(@data))

	DECLARE @output VARCHAR(MAX)
	
	DECLARE @PART1 VARCHAR(255), @PART2 VARCHAR(255)
	DECLARE @INT_PART1 INT, @INT_PART2 INT
	
	SELECT @PART1 = SUBSTRING(@data, 1, CHARINDEX('|', @data)-1)
	SELECT @PART2 = SUBSTRING(@data, CHARINDEX('|', @data)+1, LEN(@data))
	
	SET @INT_PART1 = CASE ISNUMERIC(@PART1) WHEN 1 THEN CONVERT(INT, @PART1) ELSE 0 END
	SET @INT_PART2 = CASE ISNUMERIC(@PART2) WHEN 1 THEN CONVERT(INT, @PART2) ELSE 0 END
		
	SET @output = CASE @INT_PART2 WHEN 0 THEN @INT_PART1 ELSE @INT_PART2 END
	
	RETURN @output

END
GO
