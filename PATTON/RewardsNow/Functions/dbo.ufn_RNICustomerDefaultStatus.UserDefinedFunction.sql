USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerDefaultStatus]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNICustomerDefaultStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RNICustomerDefaultStatus] ( @sid_dbprocessinfo_dbnumber VARCHAR(3) )
RETURNS INT
AS
BEGIN
	DECLARE @output INT
	
	SELECT @output = CONVERT(INT, ISNULL(dim_rniprocessingparameter_value, '0'))
	FROM Rewardsnow.dbo.RNIProcessingParameter
	WHERE 
		sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		AND dim_rniprocessingparameter_key = 'RNICUSTOMERDEFAULTSTATUS'
		
	RETURN @output
END
GO
