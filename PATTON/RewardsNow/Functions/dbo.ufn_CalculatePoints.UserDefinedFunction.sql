USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_CalculatePoints]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_CalculatePoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SEB 09/2014  @dtpfactor was (18,2) changed to (18,3) because it was dropping the last digit of a 3 decimal factor

CREATE FUNCTION [dbo].[ufn_CalculatePoints]
(
	@pointstring VARCHAR(255)
	, @dtpfactor DECIMAL(18,3)
	, @roundtype INT
)
RETURNS INTEGER
AS
BEGIN
	DECLARE @output INT
	DECLARE @tempPoints INT
	
	SET @output = NULL
	SET @tempPoints = NULL
	
	SET @dtpfactor = ISNULL(@dtpfactor, 1)
	SET @roundtype = ISNULL(@roundtype, 1)
	SET @pointstring = RewardsNow.dbo.ufn_Trim(@pointstring)
	
	/*
	--roundtype 1 = round initial decimal conversion and after applying dtpfactor
		ROUND(ROUND((CAST(@pointstring) AS DECIMAL(18,2)) / 100), 0) * @dtpfactor, 0)
	--roundtype 2 = rounded after applying dtpfactor
		ROUND(((CAST(@pointstring AS DECIMAL(18,2)) / 100) * @dtpfactor), 0)
	--roundtype 3 = truncated
		ROUND(ROUND((CAST(@pointstring) AS DECIMAL(18,2)) / 100), 0, 1) * @dtpfactor, 0, 1)
		
	--roundtype BETWEEN 100 AND 199 = Whole Number without implied decimal
		trim any trailing .00 in case it accidentally comes in with one
		convert to decimal with .00 
		convert type to type - 100 
		continue with rounding as above
	--roundtype BETWEEN 200 AND 299 = Implied decimal with extraneous .00 on the end.
		strip .00
		convert type to type - 200
		continue with rounding as above
		 
	*/
	
	IF @roundtype >= 100
	BEGIN
		SET @pointstring = CONVERT(VARCHAR, CONVERT(INT, CONVERT(DECIMAL, @pointstring))) 

		IF @roundtype BETWEEN 100 AND 199
			SET @pointstring = @pointstring + '.00'
			
		WHILE @roundtype >= 100
		BEGIN
			SET @roundtype = @roundtype - 100
		END
	END

	
	
	IF CHARINDEX('.', @pointstring) > 0
		SET @pointstring = RewardsNow.dbo.ufn_Trim(REPLACE(@pointstring, '.', ''))
	
	IF ISNUMERIC(@pointstring) = 1
	BEGIN
		IF @roundtype = 1
		BEGIN
			SET @tempPoints = ROUND(ROUND((CAST(@pointstring AS DECIMAL(18,2)) / 100), 0) * @dtpfactor, 0)
		END
		
		IF @roundtype = 2
		BEGIN
			SET @tempPoints = ROUND(((CAST(@pointstring AS DECIMAL(18,2)) / 100) * @dtpfactor), 0)
		END
		
		IF @roundtype = 3
		BEGIN
			SET @tempPoints = ROUND(ROUND((CAST(@pointstring AS DECIMAL(18,2)) / 100), 0, 1) * @dtpfactor, 0, 1)
		END
		
		SET @output = @tempPoints
	END
	
	RETURN @output
END
GO
