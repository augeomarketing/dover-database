USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractRight4affiliatcustidWithLeading]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractRight4affiliatcustidWithLeading]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_mappingextractRight4affiliatcustidWithLeading]
    (@tipfirst              varchar(3))

returns nvarchar(max)

as

BEGIN

    declare @db         nvarchar(50)
    declare @sql        nvarchar(max)

    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    --set @sql = 'if object_id(''tempdb..#__temp'') is not null drop table #__temp
				--create table #__temp (tipnumber varchar(15),
				--					  lastname  varchar(40),
				--					  mapvalue  varchar(max) )
				
				--insert into #__temp (tipnumber, lastname, mapvalue)
			 --   select distinct tipnumber, lastname, ''#'' + (select top 1 right(ltrim(rtrim(custid)),4)
				--								     from ' + @db + '.dbo.affiliat a1
				--								     where a1.tipnumber = aff.tipnumber
				--								     and isnull(a1.acctstatus, ''A'') != ''C'') mapvalue
    --            from ' + @db + '.dbo.affiliat aff
    --            where aff.acctstatus != ''C'' 
                
    --            /* TARGET */
    --            select tipnumber, lastname, mapvalue from #__temp where 1=1
    --            '

	set @sql = 'select distinct tipnumber, lastname, ''#'' + (select top 1 right( ltrim(rtrim(custid)), 4)
															  from ' + @db + '.dbo.affiliat a1
															  where a1.tipnumber = aff.tipnumber
															  and isnull(a1.acctstatus, ''A'') != ''C'') mapvalue
				from ' + @db + '.dbo.affiliat aff
				where isnull(aff.acctstatus, ''A'') != ''C'' '
                
    return @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingextractRight4affiliatcustidWithLeading ('207')

*/
GO
