USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNITransactionAux_Vendor1]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNITransactionAux_Vendor1]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RNITransactionAux_Vendor1]() RETURNS TABLE
AS
RETURN
	
SELECT sid_rnitransaction_id, CITY,MCC,MERCHANTLOCATION,MERCHANTNAME,SIC 
FROM 
( 
	select  
		rnita.sid_rnitransaction_id 
		, rnitak.dim_rnitransactionauxkey_name 
		, rnita.dim_rnitransactionaux_value 
	from RNITransactionAux rnita 
	inner join RNITransactionAuxKey rnitak 
		on rnita.sid_rnitransactionauxkey_id = rnitak.sid_rnitransactionauxkey_id 
	where sid_smstranstatus_id_vendor1 = 0 
) kvp 
PIVOT (MIN(dim_rnitransactionaux_value) FOR dim_rnitransactionauxkey_name IN (CITY,MCC,MERCHANTLOCATION,MERCHANTNAME,SIC)) AS pvt 

;
GO
