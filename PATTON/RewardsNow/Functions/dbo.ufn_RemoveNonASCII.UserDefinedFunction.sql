USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RemoveNonASCII]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RemoveNonASCII]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RemoveNonASCII]  
(  
 @in nvarchar(max)  
)  
RETURNS VARCHAR(max)  
AS  
BEGIN  
 DECLARE @out VARCHAR(MAX)  
 SET @out = ''  
 DECLARE @char NVARCHAR(1)  
 DECLARE @pos INT  
   
 SET @pos = 1  
   
 WHILE @pos <= LEN(@in)  
 BEGIN  
  SET @char = SUBSTRING(@in, @pos, 1)  
  IF UNICODE(@char) BETWEEN 32 AND 255 AND ASCII(@char) < 128  
   SET @out = @out + @char  
    
  SET @pos = @pos + 1  
 END  
  
 RETURN @out  
   
END
GO
