USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_rawdata_parseMSUMerchantName]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_rawdata_parseMSUMerchantName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_rawdata_parseMSUMerchantName] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(max)

AS  

BEGIN
	set @data = ltrim(rtrim(@data))

	return substring(@data, charindex(':', @data)+1, len(@data) -  charindex(':', @data))

END
GO
