USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_SwapTipFirst_OldNew]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_SwapTipFirst_OldNew]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_SwapTipFirst_OldNew] (@tipnumber varchar(15), @oldtipfirst VARCHAR(3), @newtipfirst VARCHAR(3)) RETURNS VARCHAR(15)
AS
BEGIN
	DECLARE @output VARCHAR(15) = @tipnumber
	
	IF LEFT(@tipnumber, 3) = @oldtipfirst
	BEGIN
		SET @output = @newtipfirst + substring(@output, 4, len(@output))	
	END
	
	return @output

END
GO
