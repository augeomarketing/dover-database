USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataImportConvertImpliedDecimal]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawDataImportConvertImpliedDecimal]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_RawDataImportConvertImpliedDecimal] (@tipfirst varchar(3), @value varchar(max))
	returns decimal(18,2)

AS

BEGIN

declare @decimalvalue		decimal(18,2) = 0

set @decimalvalue = (select dbo.ufn_RawDataConvertStringtodecimal(@tipfirst, @value) / 100)

return @decimalvalue

END


/*  Test harness


select dbo.ufn_RawDataImportConvertImpliedDecimal('135', '8335')


*/
GO
