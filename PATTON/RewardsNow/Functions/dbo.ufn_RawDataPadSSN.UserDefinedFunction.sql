USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataPadSSN]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawDataPadSSN]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_PadSSN]    Script Date: 01/11/2012 08:32:24 ******/

-- =============================================
-- Author:		Rich 
-- Create date: 2011-10-30
-- Description:	pad column with leading zeros
-- =============================================
CREATE FUNCTION [dbo].[ufn_RawDataPadSSN]
(
		@TipFirst varchar(3), 
		@Data varchar(max)
)  
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Output VARCHAR(MAX)

	set @Output =  REPLICATE ( '0',9 - Len ( Rewardsnow.dbo.ufn_Trim( @Data ) ) ) + @Data

	RETURN @Output 

END
GO
