USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractsanitizedcard_CREDIT_OR_DEBIT]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractsanitizedcard_CREDIT_OR_DEBIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractsanitizedcard_CREDIT_OR_DEBIT]    
    (@tipfirst              varchar(3))    
    
returns nvarchar(max)    
    
as    
    
BEGIN    
    
    declare @db         nvarchar(50)    
    declare @sql        nvarchar(max)    
    
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)    
    
    set @sql = 'select tipnumber, isnull(lastname, '''') lastname, rewardsnow.dbo.ufn_displaycard(acctid) mapvalue    
                from ' + @db + '.dbo.affiliat    
                where accttype LIKE ''%CREDIT%'' OR accttype LIKE ''%DEBIT%'''    
                    
    return @sql    
END    
    
    
/*  Test harness    
    
select rewardsnow.dbo.ufn_mappingextractsanitizedcard_CREDIT_OR_DEBIT ('243')    
    
-- resulting @SQL value:    
select tipnumber, isnull(lastname, '') lastname, rewardsnow.dbo.ufn_displaycard(acctid) mapvalue                      from [243].dbo.affiliat                      where accttype LIKE '%CREDIT%' OR accttype LIKE '%DEBIT%'
    
    
*/
GO
