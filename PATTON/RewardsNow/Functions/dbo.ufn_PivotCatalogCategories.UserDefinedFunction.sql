USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_PivotCatalogCategories]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_PivotCatalogCategories]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_PivotCatalogCategories] (@CatalogId int)  
RETURNS VARCHAR(100)
AS 
 --written by diana irish 4/26/13
 --called by rwardsnow.dbo.usp_CatalogListing
BEGIN 
	DECLARE @Categories VARCHAR(100)
	BEGIN
		 
				
			select  @Categories = REPLACE(REPLACE( COALESCE( [1],'*')   + '$' + COALESCE([2],'*') + '$' + COALESCE([3],'*') + '$' + COALESCE([4],'*')  + '$' + COALESCE([5],'*')  , '$*','')  ,'$','/')
			from
			(select cc.sid_catalog_id  ,cg.dim_category_name   ,   ROW_NUMBER() OVER (PARTITION BY cc.sid_catalog_id order by cc.sid_catalog_id)
			as RowNum
			from   Catalog.dbo.catalogcategory cc
			inner join Catalog.dbo.category  cg on cc.sid_category_id = cg.sid_category_id 
			 and  sid_catalog_id =  @CatalogId
			)
			a
			pivot (max(dim_category_name) for RowNum in ([1],[2],[3],[4],[5] )) as pvt
			
	END

	RETURN @Categories

END
GO
