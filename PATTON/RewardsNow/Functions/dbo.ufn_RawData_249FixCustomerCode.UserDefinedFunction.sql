USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_249FixCustomerCode]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_249FixCustomerCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_249FixCustomerCode] (@tipfirst varchar(3), @data varchar(max))  
RETURNS VARCHAR(40) 
AS 
BEGIN  
	DECLARE @output VARCHAR(40)  
	DECLARE @type INT    
	SET @output = @data    
	
	IF @data = '30'
		SET @output = '1'
		
	If @data = '01'
		SET @output = '1'
	
	RETURN @output  
END
GO
