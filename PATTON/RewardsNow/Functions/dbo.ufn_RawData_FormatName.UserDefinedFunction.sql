USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_FormatName]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_FormatName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_FormatName] (@tipfirst varchar(3), @data varchar(max)) 
RETURNS VARCHAR(40)
AS
BEGIN
	DECLARE @output VARCHAR(40)
	DECLARE @type INT
	
	SET @data = ltrim(rtrim(replace(replace(replace(@data,' ','Çü'),'üÇ',''),'Çü',' ')))
	SET @output = @data
	
	SELECT @type = ISNULL(dim_rniprocessingparameter_value, -1)
	FROM RewardsNow.dbo.RNIProcessingParameter
	WHERE sid_dbprocessinfo_dbnumber = @tipfirst
		AND dim_rniprocessingparameter_key = 'RNICUSTOMER_NAMEFORMATTYPE'
		AND dim_rniprocessingparameter_active = 1

	
	SET @output = 
		CASE 
			WHEN @type = -1 
				THEN @data
			ELSE RewardsNow.dbo.ufn_FormatName(@type, @data, '', '')
		END
			
			
	RETURN @output

END
GO
