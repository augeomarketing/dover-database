USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetCoopManagerid]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetCoopManagerid]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Enter guid of employee to find manager's guid or id

CREATE FUNCTION [dbo].[ufn_GetCoopManagerid] (@guid varchar(512))  
RETURNS VARCHAR(512)
AS 
 
BEGIN 
	DECLARE @ManagerId VARCHAR(512)
	BEGIN
		 SELECT   TOP 1 @ManagerId = a1.memberid --  mgrName
		 from RN1.Coop.dbo.Account a1
		 inner join RN1.coop.dbo.account a2  on a1.MemberNumber = a2.MemberID
		 --inner join RN1.coop.dbo.customer cust on  a1.tipnumber = cust.tipnumber
		 WHERE a2.membernumber = @guid
		
	END

	RETURN @ManagerId

END
GO
