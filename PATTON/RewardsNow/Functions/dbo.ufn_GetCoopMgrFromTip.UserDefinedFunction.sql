USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetCoopMgrFromTip]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetCoopMgrFromTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the point value from the Portal_Bonus_Codes or TranType table


create FUNCTION [dbo].[ufn_GetCoopMgrFromTip] (@tip varchar(15))  
RETURNS VARCHAR(512)
AS 
 
BEGIN 
	DECLARE @ManagerId VARCHAR(512)
	BEGIN
		 			
		select top 1 @ManagerId =  Acc.memberID 
		from rn1.Coop.dbo.Account acc
		join [RN1].rewardsnow.[dbo].[corevaluesubmission] cvs
		on Acc.membernumber= cvs.dim_corevaluesubmission_acct
		where Acc.tipnumber =@tip

	END

	RETURN @ManagerId

END
GO
