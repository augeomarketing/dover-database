USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_webGetMerchantFundedInfoForStatementing]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_webGetMerchantFundedInfoForStatementing]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_webGetMerchantFundedInfoForStatementing]
(
	@tipnumber VARCHAR(15),
	@startdate DATETIME,
	@enddate DATETIME
)
RETURNS
@merchantFunded TABLE
(
	Tipnumber VARCHAR(20),
	finalrow VARCHAR(MAX),
	id VARCHAR(200)
)

AS
BEGIN
	
	DECLARE @tmpMerchantFunded TABLE
	(
		id VARCHAR(20),
		transdate DATETIME,
		merchantname VARCHAR(200),
		transactionamount DECIMAL(16, 2),
		pointsearned INT
	)
	
	INSERT INTO @tmpMerchantFunded
	SELECT DISTINCT 'Z' + CAST(sid_ZaveeTransactions_Identity AS VARCHAR) AS sid_ZaveeTransactions_Identity,
		dim_ZaveeTransactions_TransactionDate AS TransactionDate, 
		dim_ZaveeMerchant_MerchantName AS MerchantName, 
		dim_ZaveeTransactions_TransactionAmount AS TransactionAmount, 
		dim_ZaveeTransactions_AwardPoints AS PointsEarned
	FROM RewardsNOW.dbo.ZaveeTransactions zt
	INNER JOIN RewardsNOW.dbo.ZaveeMerchant ZM
		ON ZT.dim_ZaveeTransactions_MerchantId = ZM.dim_ZaveeMerchant_MerchantId
	WHERE dim_ZaveeTransactions_MemberID = @tipnumber
		AND dim_ZaveeTransactions_ProcessedDate >= @startdate
		AND dim_ZaveeTransactions_ProcessedDate < DATEADD("d", 1, @enddate)	
		AND dim_ZaveeTransactions_PaidDate >= '7/1/2013'
		AND ISNULL(dim_ZaveeTransactions_EmailSent, '1900-01-01 00:00:00.000') = '1900-01-01 00:00:00.000'
		AND dim_ZaveeTransactions_AwardPoints > 0
		
	UNION
		
	SELECT DISTINCT 'A' + CAST(sid_AzigoTransactions_Identity AS VARCHAR) AS sid_AzigoTransactions_Identity,
		dim_AzigoTransactions_TransactionDate AS TransactionDate, 
		dim_AzigoTransactions_MerchantName AS MerchantName, 
		dim_AzigoTransactions_TransactionAmount AS TransactionAmount, 
		dim_AzigoTransactions_Points AS PointsEarned
	FROM RewardsNOW.dbo.AzigoTransactions zt
	WHERE dim_AzigoTransactions_Tipnumber = @tipnumber
		AND dim_AzigoTransactions_FundedDate >= @startdate
		AND dim_AzigoTransactions_FundedDate < DATEADD("d", 1, @enddate)	
		AND dim_AzigoTransactions_PaidDate >= '8/22/2013'
		AND dim_AzigoTransactions_TranStatus NOT IN ('CANCELED', 'PENDING')
		AND ISNULL(dim_AzigoTransactions_EmailSent, '1900-01-01 00:00:00.000') = '1900-01-01 00:00:00.000'
		AND dim_AzigoTransactions_Points > 0
	
	DECLARE @rowfield NVARCHAR(MAX) = '', @idfield VARCHAR(MAX)
	DECLARE BuildCursor CURSOR FOR 
		SELECT '<tr><td>' + CONVERT(VARCHAR, transdate, 101) + '</td><td>' + merchantname + '</td><td>$' +
			CAST(transactionamount AS VARCHAR) + '</td><td>' + CAST(pointsearned AS VARCHAR) + '</td></tr>',
			id + ', '
		FROM @tmpMerchantFunded

	OPEN BuildCursor 
	FETCH NEXT FROM BuildCursor INTO @rowfield, @idfield

	DECLARE @finalrow VARCHAR(MAX) = ''
	DECLARE @idrow VARCHAR(MAX) = ''
	
	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		SET @finalrow = RTRIM(@finalrow) + RTRIM(@rowfield)
		SET @idrow = RTRIM(@idfield) + RTRIM(@idrow)
		FETCH NEXT FROM BuildCursor INTO @rowfield, @idfield
	END 
	CLOSE BuildCursor 

	DEALLOCATE BuildCursor 
	
	IF @finalrow <> ''
	BEGIN
		INSERT INTO @merchantFunded(tipnumber, finalrow, id)
		VALUES (@tipnumber, @finalrow, LEFT(@idrow, LEN(@idrow) - 1))
	END
	
	-- Return the result of the function
	RETURN

END
GO
