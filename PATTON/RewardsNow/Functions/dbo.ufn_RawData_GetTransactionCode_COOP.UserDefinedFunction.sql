USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_GetTransactionCode_COOP]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_GetTransactionCode_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_GetTransactionCode_COOP](@sid_dbprocessinfo_dbnumber VARCHAR(50), @data VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @msgtype VARCHAR(2)
	DECLARE @processingcode VARCHAR(6)
	DECLARE @return VARCHAR(1)
	
	SET @msgtype = (SELECT Item FROM ufn_splitWithIndex(@data, '|') where ItemIndex = 1)
	SET @processingcode = (SELECT Item FROM ufn_splitWithIndex(@data, '|') where ItemIndex = 2)
	
	SET @return = '1'
	
	IF @msgtype = '04'
	BEGIN
		SET @return = '0'
	END
	ELSE
	BEGIN
		IF @processingcode LIKE '2000[1234][10]'
		BEGIN
			SET @return = '0'
		END
	END

	RETURN @return
END
GO
