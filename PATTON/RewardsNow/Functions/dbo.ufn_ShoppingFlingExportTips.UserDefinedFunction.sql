USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ShoppingFlingExportTips]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_ShoppingFlingExportTips]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	Gets all primary tips and sids for all clients wanting the accrual file
	and 488 exported to them.
*/
CREATE FUNCTION [dbo].[ufn_ShoppingFlingExportTips] ( )
RETURNS TABLE
AS
RETURN 
	select dbpi.dbnumber, pri.sid_rnicustomer_id, pri.dim_rnicustomer_rniid
    from dbprocessinfo dbpi
cross apply
	RewardsNow.dbo.ufn_RNICustomerPrimarySIDsForTip(dbpi.dbnumber) as pri
    where dbpi.ShoppingFlingExportFile = 'Y'
    --where dbpi.dbnumber = 'reb'  --- for testing only
GO
