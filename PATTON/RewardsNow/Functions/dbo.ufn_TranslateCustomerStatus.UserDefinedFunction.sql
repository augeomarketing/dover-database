USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_TranslateCustomerStatus]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_TranslateCustomerStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_TranslateCustomerStatus]
	(@tipfirst			varchar(3),
	 @status			varchar(255))
RETURNS varchar(2)

AS

BEGIN
	declare @xlatestatus	varchar(2)
	
	set @status = 'CUSTOMERSTATUS_' + ltrim(rtrim(@status))
	
	set @xlatestatus = (select top 1 dim_rniprocessingparameter_value
						from rewardsnow.dbo.rniprocessingparameter
						where sid_dbprocessinfo_dbnumber = @tipfirst
						and dim_rniprocessingparameter_key = @status
						and dim_rniprocessingparameter_active = 1)
	return @xlatestatus

END
GO
