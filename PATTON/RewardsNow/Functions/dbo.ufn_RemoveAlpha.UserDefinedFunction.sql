USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RemoveAlpha]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RemoveAlpha]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RemoveAlpha](@input varchar(2047)) RETURNS varchar(2047)
AS
BEGIN
	declare @out varchar(2047)
	set @out = '';
	
	with EachChar(thisChar)
	as
	(
		SELECT SUBSTRING(@input,number,1)
        FROM master.dbo.spt_values
        WHERE type='p'
        AND number BETWEEN 1 AND LEN(@input)
	)
	select @out = @out + case when thisChar like '[0-9]' then thisChar else '' end from EachChar
	
	return @out
END
GO
