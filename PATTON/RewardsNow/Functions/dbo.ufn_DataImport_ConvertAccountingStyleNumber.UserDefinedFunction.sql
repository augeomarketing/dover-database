USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_DataImport_ConvertAccountingStyleNumber]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_DataImport_ConvertAccountingStyleNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_DataImport_ConvertAccountingStyleNumber](@tipfirst varchar(3), @moneystring varchar(max))
RETURNS INTEGER
AS
BEGIN
	DECLARE @output INT
	SELECT @output = rewardsnow.dbo.ufn_ConvertAccountingStyleNumber(@moneystring)
	
	RETURN @output
END
GO
