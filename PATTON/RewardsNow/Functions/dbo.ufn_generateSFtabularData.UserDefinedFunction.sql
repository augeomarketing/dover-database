USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_generateSFtabularData]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_generateSFtabularData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_generateSFtabularData]
(
	@tipnumber VARCHAR(20) = '002000000084754',
	@yr INT = 2013,
	@mo INT = 1
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @rowtable TABLE( azigorow NVARCHAR(MAX) )
	DECLARE @rowfield NVARCHAR(MAX) = ''
	DECLARE @finalrow NVARCHAR(MAX)= ''
	DECLARE BuildCursor CURSOR FOR SELECT azigorow FROM @rowtable

	INSERT INTO @rowtable
	SELECT RTRIM('<tr><td>' + dim_VesdiaAccrualHistory_MerchantName + '</td><td>' 
		+ CAST(dim_VesdiaAccrualHistory_MemberReward AS VARCHAR) + '<td></tr>')
	FROM rewardsnow.dbo.VesdiaAccrualHistory
	WHERE dim_VesdiaAccrualHistory_MemberID = @tipnumber
		AND YEAR(dim_VesdiaAccrualHistory_RNIProcessDate) = @yr 
		AND MONTH(dim_VesdiaAccrualHistory_RNIProcessDate) = @mo
		
	OPEN BuildCursor 
	FETCH NEXT FROM BuildCursor INTO @rowfield

	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		SET @finalrow = RTRIM(@finalrow) + RTRIM(@rowfield)
		FETCH NEXT FROM BuildCursor INTO @rowfield
	END 
	CLOSE BuildCursor 

	DEALLOCATE BuildCursor 
	
	RETURN @finalrow

END
GO
