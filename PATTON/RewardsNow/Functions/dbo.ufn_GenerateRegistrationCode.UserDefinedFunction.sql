USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GenerateRegistrationCode]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GenerateRegistrationCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GenerateRegistrationCode]
(
	@sid_dbprocessinfo_dbnumber as varchar(3)
	, @inString as varchar(max)
) RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @startDirectionIndicator INT = 60
		, @position_shift INT
		, @currentFactor int 
		, @curChar varchar(1)
		, @curCharAscii int
		, @newCharAscii int
		, @newChar varchar(1)
		, @outString varchar(6) = ''
		, @curPos int = 1
		, @randomNumberSeed DECIMAL(18,18)
		, @loopPos int = 1
	
	SET @inString = LTRIM(RTRIM(@inString))
	
	IF LEN(@inString) < 5
	BEGIN
		SET @inString = LEFT((@inString + 'R3W4RD5'), 5)
	END

	WHILE @startDirectionIndicator between 58 and 64
	BEGIN
		SELECT @randomNumberSeed = rndResult FROM RewardsNow.dbo.rndView
		SET @startDirectionIndicator = ROUND(((90 - 48 - 1) * @randomNumberSeed + 48), 0)	
	END

	IF @sid_dbprocessinfo_dbnumber IN ('275')
	BEGIN
		SET @startDirectionIndicator = ASCII(SUBSTRING(@inString, 5, 1))
	END


	SET @position_shift = @startDirectionIndicator % 10
	SET @currentFactor = case when (@position_shift % 2) = 0 THEN 1 ELSE -1 END

	WHILE @loopPos <= 6
	BEGIN
		if @loopPos = 3
		BEGIN
			SET @newCharAscii = @startDirectionIndicator
			SET @newChar = CHAR(@newCharAscii)
		END
		ELSE
		BEGIN
		
			set @curChar = SUBSTRING(@inString, @curPos, 1)
			set @curCharAscii = ASCII(@curChar)
			set @newCharAscii = @curCharAscii + (@position_shift * @currentFactor)
			
			
			IF @newCharAscii < 48 or @newCharAscii > 90 or @newCharAscii between 58 and 64
			BEGIN
				if @newCharAscii > 90
				BEGIN
					SET @newCharAscii = 47 + (@newCharAscii - 90)			
				END
				
				IF @newCharAscii < 48
				BEGIN
					SET @newCharAscii = 91 - (48 - @newCharAscii)			
				END
				
				IF @newCharAscii BETWEEN 58 AND 64
				BEGIN
					IF @currentFactor = 1
					BEGIN
						SET @newCharAscii = 65 + (@newCharAscii - 58)				
					END
					
					IF @currentFactor = -1
					BEGIN
						SET @newCharAscii = 57 - (64 - @newCharAscii)				
					END
				END		
			END
			
			set @newChar = CHAR(@newCharAscii)
			
			set @currentFactor *= -1
			SET @curPos += 1
		END

		SET @outString += @newChar
		SET @loopPos += 1

	END
	
	RETURN 'R' + @outString 
	
END



/*
0-9 = 48-59
A-Z = 65-90

*/
GO
