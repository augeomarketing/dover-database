USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_ADD_Year_To_TRANSDATE_COOP]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_ADD_Year_To_TRANSDATE_COOP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_ADD_Year_To_TRANSDATE_COOP] (@tipfirst varchar(3), @data varchar(max)) 
RETURNS VARCHAR(40)
AS
BEGIN
	DECLARE @output VARCHAR(40)
	DECLARE @type INT
	Declare @year varchar(4)
	Declare @month varchar(2)

	set @year = datepart(yyyy,getdate())
	set @month = right('00' + convert(varchar(2),month(getdate())),2)
	
	if(left(@data,2) > @month) 
	begin
		set @year = @year - 1
	end

	SET @data = rtrim(@year) + ltrim(rtrim(@data))

	SET @output = @data	
	RETURN @output

END
GO
