USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerGetStatusByProperty]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNICustomerGetStatusByProperty]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RNICustomerGetStatusByProperty] (@property VARCHAR(50))
RETURNS @status TABLE (sid_rnicustomercode_id BIGINT, dim_status_id VARCHAR(1), dim_status_description VARCHAR(50))
AS
BEGIN
	--IF PROPERTY = 'LIST', 'HELP', OR BLANK/NULL THEN OUTPUT A LIST OF ALL PROPERTIES AND THEIR SETTING VALUES
	IF ISNULL(@property, '') IN ('LIST', 'HELP', '')
	BEGIN
		INSERT INTO @status 
		(
			sid_rnicustomercode_id
			, dim_status_id
			, dim_status_description
		)
		SELECT 
			dim_rnicustomercodestatusproperty_setting
			, NULL
			, dim_rnicustomercodestatusproperty_name
		FROM
			RewardsNow.dbo.RNICustomerCodeStatusProperty
	END 
	ELSE
	BEGIN
		DECLARE @setting BIGINT 
	
		SELECT @setting = ISNULL(dim_rnicustomercodestatusproperty_setting, 0) 
		FROM RewardsNow.dbo.RNICustomerCodeStatusProperty 
		WHERE dim_rnicustomercodestatusproperty_name = @property
		
		IF @setting != 0
		BEGIN
			INSERT INTO @status 
			(
				sid_rnicustomercode_id
				, dim_status_id
				, dim_status_description
			)
			SELECT 
				sid_rnicustomercode_id
				, dim_status_id
				, dim_status_description
			FROM
				RewardsNow.dbo.RNICustomerCodeStatus
			WHERE
				@setting & dim_rnicustomercodestatus_propertysettings = @setting
		END
	END
	
	RETURN
END
GO
