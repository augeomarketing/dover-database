USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_DisplayCard]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_DisplayCard]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_DisplayCard](@cardin VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
BEGIN
	RETURN 
		CASE WHEN RewardsNow.dbo.fnCheckLuhn10(@cardin) = 1
		THEN 
			LEFT(RewardsNow.dbo.ufn_Trim(@cardin), 6)
			+ REPLICATE('x', LEN(RewardsNow.dbo.ufn_Trim(@cardin)) - 10)
			+ RIGHT(RewardsNow.dbo.ufn_Trim(@cardin), 4)
		ELSE
			''
		END

END
GO
