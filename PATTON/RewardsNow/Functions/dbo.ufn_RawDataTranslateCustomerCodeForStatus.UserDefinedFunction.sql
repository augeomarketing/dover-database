USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataTranslateCustomerCodeForStatus]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawDataTranslateCustomerCodeForStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawDataTranslateCustomerCodeForStatus] (@tipfirst varchar(3), @data varchar(max))
RETURNS VARCHAR(1)
AS
BEGIN
	DECLARE @output VARCHAR(1)
	
	SET @data = dbo.ufn_Trim(@data)
	
	SET @output =
		CASE WHEN @data IN ('01', '1') THEN 'A'
		ELSE @data
		END
		
	RETURN @output
END
GO
