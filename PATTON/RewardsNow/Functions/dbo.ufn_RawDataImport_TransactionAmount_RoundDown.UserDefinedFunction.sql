USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataImport_TransactionAmount_RoundDown]    Script Date: 10/18/2016 16:05:39 ******/
DROP FUNCTION [dbo].[ufn_RawDataImport_TransactionAmount_RoundDown]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_RawDataImport_TransactionAmount_RoundDown] (@tipfirst varchar(3), @value varchar(max))
	returns decimal(18,2)

AS

BEGIN

declare @decimalvalue		decimal(18,2) = 0
declare @valuenum  decimal(18,2)
set @valuenum = @value

set @decimalvalue = isnull(cast(@valuenum as int),0)

return @decimalvalue

END
GO
