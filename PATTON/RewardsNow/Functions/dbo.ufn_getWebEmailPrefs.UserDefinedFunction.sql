USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_getWebEmailPrefs]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_getWebEmailPrefs]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Shawn Smith, Chris Heit
-- Create date: March, 2013
-- Description:	Get Customer email prefs
-- =============================================
CREATE FUNCTION [dbo].[ufn_getWebEmailPrefs]
(	
	@emailpreftype INT, 
	@tipfirst VARCHAR(3) = '',
	@setting INT = -1
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT dim_emailpreferencescustomer_tipnumber, dim_emailpreferencescustomer_setting
	FROM rn1.rewardsnow.dbo.emailpreferencescustomer
	WHERE sid_emailpreferencestype_id = @emailpreftype
		AND dim_emailpreferencescustomer_tipnumber LIKE @tipfirst + '%'
		AND dim_emailpreferencescustomer_setting = (CASE WHEN @setting <> -1 THEN @setting ELSE dim_emailpreferencescustomer_setting END)
)
/*
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(1, '', -1) --for all tips of type 1
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(1, '', 0) --for all tips of type 1 and setting 0 (not opted in)
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(1, '', 1) --for all tips of type 1 and setting 1 (opted in)
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(1, 'REB', -1) --for REBAs of type 1 
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(1, 'REB', 0) --for REBAs of type 1 and setting 0 (not opted in)
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(1, 'REB', 1) --for REBAs of type 1 and setting 1 (opted in)
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(2, 'REB', -1) --for REBAs of type 2
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(2, 'REB', 0) --for REBAs of type 2 and setting 0 (not opted in)
select * from RewardsNOW.dbo.ufn_getWebEmailPrefs(2, 'REB', 1) --for REBAs of type 2 and setting 1 (opted in)
*/
GO
