USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetCoopName]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetCoopName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the point value from the Portal_Bonus_Codes or TranType table


CREATE FUNCTION [dbo].[ufn_GetCoopName] (@guid varchar(512))  
RETURNS VARCHAR(50)
AS 
 
BEGIN 
	DECLARE @Name VARCHAR(50)
	BEGIN
		SELECT TOP 1 @Name = cust.name1
		from rn1.Coop.dbo.Account acc
		join RN1.coop.dbo.customer cust on  Acc.tipnumber = cust.tipnumber
		WHERE membernumber = @guid
		
		

	END

	RETURN @Name

END
GO
