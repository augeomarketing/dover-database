USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetDistanceInMiles]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetDistanceInMiles]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetDistanceInMiles]
(
	@FromLatitude   float,
	@FromLongitude  float,
	@ToLatitude     float,
	@ToLongitude    float
)
RETURNS  varchar(32)
 AS
 BEGIN
 /* created by diana irish 11/1/2012
 this function calculates distance between 2 sets of  coordinates.
 4326 is the SRID , an Industry standard for computing geography.
  the metric system is used in geospatial calculations so we need to divide by 1609.244 to 
  to get miles( 1 mile = 1609.344 meters).
 */
 
		DECLARE @point1 geography, @point2 geography
		DECLARE @out  varchar(32)
	
	    set @point1 = geography::Point(@FromLatitude, @FromLongitude,4326)
        set @point2 =geography::Point(@ToLatitude, @ToLongitude,4326)

		SET @out =   @point1.STDistance(@point2)/ 1609.344
		 
	
		 RETURN @out
		   
 END
GO
