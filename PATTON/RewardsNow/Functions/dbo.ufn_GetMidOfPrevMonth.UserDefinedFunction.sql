USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetMidOfPrevMonth]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetMidOfPrevMonth]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetMidOfPrevMonth]  
(  
 @inputDate DATETIME  
)  
RETURNS DATE  
AS  
BEGIN  
 DECLARE @out DATE  
 SET @out = CONVERT(DATE, DATEADD(m, -1, RewardsNow.dbo.ufn_GetFirstOfMonth(@inputDate)))
 SET @out = CONVERT(DATE, DATEADD(d, +14, @out))
    
 RETURN @out  
END
GO
