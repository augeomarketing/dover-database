USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetWebStatusFromProcessingStatus]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetWebStatusFromProcessingStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetWebStatusFromProcessingStatus]
(
	@tipfirst VARCHAR(3)
	, @processingstatus VARCHAR(1)
) RETURNS VARCHAR(1)
AS
BEGIN
	DECLARE @out VARCHAR(1)
	
	SET @tipfirst = UPPER(ISNULL(@tipfirst, 'ÇÇÇ'))	
	SET @processingstatus = UPPER(@processingstatus)
	
	SET @out =
		(
			SELECT TOP 1 dim_mappingstatuscode_webstatuscode
			FROM mappingstatuscode
			WHERE dim_mappingstatuscode_tipfirst = @tipfirst
				AND dim_mappingstatuscode_processingstatuscode = @processingstatus
			GROUP BY dim_mappingstatuscode_webstatuscode
		)
		
	SET @out = ISNULL(@out, @processingstatus)
	
	RETURN @out
END
GO
