USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractaffiliatLast4Secid]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractaffiliatLast4Secid]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractaffiliatLast4Secid]
    (@tipfirst              varchar(3))

returns nvarchar(max)

as

BEGIN

    declare @db         nvarchar(50)
    declare @sql        nvarchar(max)

    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    set @sql = 'select tipnumber, lastname, right( ltrim(rtrim(secid)), 4) mapvalue
                from ' + @db + '.dbo.affiliat
                where acctstatus != ''C'' '
                
    return @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingextractaffiliatLast4Secid ('803')

-- resulting @SQL value:
select tipnumber, lastname, right( ltrim(rtrim(secid)), 4) mapvalue                  from [803].dbo.affiliat                  where acctstatus != 'C' 

select *
from [803].dbo.affiliat


*/
GO
