USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_stmtGenerateRowMap_AllBouncedEmails]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_stmtGenerateRowMap_AllBouncedEmails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_stmtGenerateRowMap_AllBouncedEmails] (@jobid BIGINT)
RETURNS NVARCHAR(MAX)
AS

BEGIN
	DECLARE @sqlOut NVARCHAR(MAX)
	DECLARE @tipfirst VARCHAR(15) = (SELECT sid_dbprocessinfo_dbnumber FROM processingjob where sid_processingjob_id = @jobid)
	DECLARE @startDate VARCHAR(10)
	DECLARE @endDate VARCHAR(10)
	
	set @sqlOut = 
	REPLACE(
	'
		SELECT dim_emailwelcomekit_tipnumber AS tipnumber FROM rn1.rewardsnow.dbo.emailwelcomekit
		WHERE dim_emailwelcomekit_tipfirst = ''<DBNUMBER>'' AND dim_emailwelcomekit_sent = 2
	'	
	, '<DBNUMBER>', @tipfirst)
	
	RETURN @sqlOut

END
GO
