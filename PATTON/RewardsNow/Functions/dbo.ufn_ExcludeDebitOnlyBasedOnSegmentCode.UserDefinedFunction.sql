USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ExcludeDebitOnlyBasedOnSegmentCode]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ExcludeDebitOnlyBasedOnSegmentCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ExcludeDebitOnlyBasedOnSegmentCode] (@jobid BIGINT)
RETURNS @ExcludeTips TABLE (tipnumber          varchar(15) primary key)
        
AS

BEGIN
	declare @SQL varchar(max)
    DECLARE @sid_dbprocessinfo_dbnumber VARCHAR(3)
    set @sid_dbprocessinfo_dbnumber = (SELECT sid_dbprocessinfo_dbnumber FROM rewardsnow.dbo.processingjob where sid_processingjob_id = @jobid)

    set @sql = N'insert into @excludeTips (tipnumber)
				select distinct tipnumber
				from ' + Quotename(@sid_dbprocessinfo_dbnumber) + N'.dbo.customer
				where segmentcode=''D'' '
	exec sp_executesql @Sql
				
return 

END
GO
