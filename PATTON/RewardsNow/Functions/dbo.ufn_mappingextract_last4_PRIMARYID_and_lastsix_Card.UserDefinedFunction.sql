USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextract_last4_PRIMARYID_and_lastsix_Card]    Script Date: 09/30/2013 14:53:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_mappingextract_last4_PRIMARYID_and_lastsix_Card]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_mappingextract_last4_PRIMARYID_and_lastsix_Card]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_mappingextract_last4_PRIMARYID_and_lastsix_Card]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'  
create function [dbo].[ufn_mappingextract_last4_PRIMARYID_and_lastsix_Card]
    (@tipfirst              varchar(3))  
  
returns nvarchar(max)  
  
as  
  
BEGIN  
  
    declare @db         nvarchar(50)  
    declare @sql        nvarchar(max)  
  
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)  

    set @sql = ''select aff.tipnumber, isnull(lastname, ''''''''), (ssn.acctid + l6.acctid) mapvalue
				from [261].dbo.affiliat aff 

				join (select tipnumber, right(ltrim(rtrim(acctid)),4) acctid
					  from [261].dbo.affiliat
					  where accttypedesc like ''''%primaryid%'''') ssn
					  on aff.tipnumber = ssn.tipnumber

				join (select tipnumber, right(ltrim(rtrim(acctid)),6) acctid
					  from [261].dbo.affiliat
					  where accttypedesc like ''''%card%'''') l6
					  on aff.tipnumber = l6.tipnumber '' 
  
    return @sql  
END  
  
/*  

--Add to source

INSERT INTO MappingSource (dim_mappingsource_description, sid_mappingtable_id, sid_mappingsourcetype_id, dim_mappingsource_source, dim_mappingsource_active)
SELECT ''Last 4 of SSN Affliat (PRIMARYID)'', 3, 2, ''ufn_mappingextractlast4_PRIMARYID'', 1

Test harness  
  
select rewardsnow.dbo.ufn_mappingextractlast4_PRIMARYID (''248'')  
  
-- resulting @SQL value:  
select tipnumber, lastname, right( ltrim(rtrim(acctid)), 4) mapvalue                    from [248].dbo.affiliat                    where acctstatus != ''C''                   and accttype = ''PRIMARYID''
  
  
*/' 
END
GO
