USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_IsAlphaNumeric]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_IsAlphaNumeric]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ufn_IsAlphaNumeric] 
(
	-- Add the parameters for the function here
	@ValidationType varchar(3),  
	@Input varchar(max)			
)
RETURNS BIT
AS
BEGIN
/* 
	This function takes a validation 

	sample call
	 select dbo.ufn_IsAlphaNumeric('MID','11-22')
*/

	-- Declare the return variable here
	DECLARE @Result BIT=1, @NumRecs int=0

--alpha only. all characters
if 	@ValidationType ='A'
begin
	-- Add the T-SQL statements to compute the return value here
	if (PATINDEX('%[^a-z]%', @Input ) > 0) SET @Result=0
end

--alpha or numeric characters (no spaces
if 	@ValidationType ='AN'
begin
	-- Add the T-SQL statements to compute the return value here
	if (PATINDEX('%[^a-z,^0-9]%', @Input ) > 0) SET @Result=0
end	

--alphanumeric or empty
if 	@ValidationType ='ANE'
begin
	-- Add the T-SQL statements to compute the return value here
	if (PATINDEX('%[^a-z,^0-9,'' '']%', @Input ) > 0) SET @Result=0
end	
--valid MID (allow alphanumeric and dashes)
if 	@ValidationType ='MID'
begin
	-- Add the T-SQL statements to compute the return value here
	if (PATINDEX('%[^a-z,^0-9,^-]%', @Input ) > 0) SET @Result=0
end	
	


	-- Return the result of the function
	RETURN @Result

END
GO
