USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawDataReplaceEmptyString_01]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawDataReplaceEmptyString_01]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawDataReplaceEmptyString_01] (@tipfirst varchar(3), @Input varchar(max))  RETURNS varchar(Max)
AS
BEGIN
	declare @out varchar(max)

	If @input = '' 
		Begin  
				set @out = '01'
		End 
		Else	set @out = @Input
	
	return @out
END
GO
