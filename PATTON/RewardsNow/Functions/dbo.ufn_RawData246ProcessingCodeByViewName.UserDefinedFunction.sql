USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData246ProcessingCodeByViewName]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData246ProcessingCodeByViewName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData246ProcessingCodeByViewName] (@tipfirst varchar(3), @data varchar(max))  
RETURNS int 

AS  

BEGIN
	DECLARE @output int

    set @output = (select dim_rniprocessingparameter_value
                   from rewardsnow.dbo.rniprocessingparameter
                   where dim_rniprocessingparameter_key = 'vw_651_TRAN_SOURCE_1__PROCESSINGCODE'
                   and sid_dbprocessinfo_dbnumber = @tipfirst)
 
 RETURN @output  
END
GO
