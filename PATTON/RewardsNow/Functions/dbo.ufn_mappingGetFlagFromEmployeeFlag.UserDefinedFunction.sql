USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingGetFlagFromEmployeeFlag]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingGetFlagFromEmployeeFlag]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_mappingGetFlagFromEmployeeFlag]
    (@tipfirst              varchar(3))  
  
returns nvarchar(max)  
  
as  
  
BEGIN  
  
    declare @db         nvarchar(50)  
    declare @sql        nvarchar(max)  
  
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)  
  
    set @sql = 'select tipnumber, lastname, CASE WHEN ISNUMERIC(EmployeeFlag) = 1 THEN EmployeeFlag WHEN EmployeeFlag = ''Y'' THEN ''1'' WHEN EmployeeFlag = ''N'' THEN ''0'' ELSE ''0'' END mapvalue  
                from ' + @db + '.dbo.customer  
                where status != ''C'' '  
                  
    return @sql  
END  
  
  
/*  Test harness  
  
select rewardsnow.dbo.ufn_mappingGetFlagFromEmployeeFlag ('241')  
  
-- resulting @SQL value:  
select tipnumber, lastname, CASE WHEN ISNUMERIC(EmployeeFlag) = 1 THEN EmployeeFlag WHEN EmployeeFlag = 'Y' THEN '1' WHEN EmployeeFlag = 'N' THEN '0' ELSE '0' END mapvalue                    from [241].dbo.customer                    where status != 'C' 
  
  
*/
GO
