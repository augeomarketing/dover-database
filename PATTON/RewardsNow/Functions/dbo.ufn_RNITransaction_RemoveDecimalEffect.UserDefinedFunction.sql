USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNITransaction_RemoveDecimalEffect]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNITransaction_RemoveDecimalEffect]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RNITransaction_RemoveDecimalEffect] (@input DECIMAL(20,2))
RETURNS DECIMAL(20,2)
AS
BEGIN
	RETURN CONVERT(DECIMAL(20,2), LEFT(CONVERT(VARCHAR(255), @input / 100), LEN(@input) - 2))
END
GO
