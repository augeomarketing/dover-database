USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_ConvertCustomerStatus]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_ConvertCustomerStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_RawData_ConvertCustomerStatus]
    (@tipfirst              varchar(3), @statusstring varchar(max))
returns varchar(2)

AS

BEGIN
    declare @status         varchar(2) = '01'

if len(@statusstring) >= 1

BEGIN    

    if len(@statusstring) = 1 set @statusstring = ' ' + @statusstring

    -- Internal Status    
        if left(@statusstring,1) in (select dim_rniprocessingparameter_value 
                             from rewardsnow.dbo.RNIProcessingParameter
                             where sid_dbprocessinfo_dbnumber = @tipfirst and
                                   dim_rniprocessingparameter_key like 'CUSTOMER_INTSTS_CANNOTREDEEM%' and
                                   dim_rniprocessingparameter_active = 1)
            set @status = '02' -- Suspend status - earn points but can not redeem.  

    -- Now check External status - which overrides internal.  External Statuses will CLOSE THE ACCOUNT
        if right(@statusstring,1) in (select dim_rniprocessingparameter_value 
                             from rewardsnow.dbo.RNIProcessingParameter
                             where sid_dbprocessinfo_dbnumber = @tipfirst and
                                   dim_rniprocessingparameter_key like 'CUSTOMER_EXTSTS_CLOSED%' and
                                   dim_rniprocessingparameter_active = 1)
            set @status = '03' -- Closed status - closed acct shut it down
END

    return @status
END

/* TEST HARNESS


select dbo.ufn_RawData_ConvertCustomerStatus ('649', 'DZ')

*/
GO
