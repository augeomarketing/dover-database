USE [Rewardsnow]
GO
/****** Object:  UserDefinedFunction [dbo].[fnCalcBusinessDaysElapsed]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnCalcBusinessDaysElapsed] (@StartDate datetime, @EndDate datetime)
returns int

as

BEGIN

declare @DaysElapsed		int

set @startdate = cast(cast(year(@startdate) as varchar(4)) + '/' +
				 cast(month(@startdate) as varchar(2)) + '/' +
				 cast(day(@startdate) as varchar(2)) as datetime)

set @enddate = cast(cast(year(@enddate) as varchar(4)) + '/' +
				 cast(month(@enddate) as varchar(2)) + '/' +
				 cast(day(@enddate) as varchar(2)) + ' 23:59:59' as datetime)


set @DaysElapsed = (select count(*) - 1
				from dbo.calendar
				where calendardt between @startdate and @enddate
				and dayofweeknm not in('Saturday', 'Sunday')
				and IsBusinessHoliday = 'N')

return @DaysElapsed

END

--  select dbo.fnCalcBusinessDaysElapsed('2008-05-27 20:34:00', '2008-06-04 00:00:00')
GO
