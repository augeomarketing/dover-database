USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractaffiliatLeft4acctid]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractaffiliatLeft4acctid]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractaffiliatLeft4acctid]  
    (@tipfirst              varchar(3))  
  
returns nvarchar(max)  
  
as  
  
BEGIN  
  
    declare @db         nvarchar(50)  
    declare @sql        nvarchar(max)  
  
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)  
  
    set @sql = 'select tipnumber, lastname, left( ltrim(rtrim(acctid)),4) mapvalue  
                from ' + @db + '.dbo.affiliat  
                '  
                  
    return @sql  
END  
  
  
/*  Test harness  
  
select rewardsnow.dbo.ufn_mappingextractaffiliatLeft4acctid ('102')  
  
-- resulting @SQL value:  
select tipnumber, lastname, left( ltrim(rtrim(acctid)), 6) mapvalue                  from [102BCM].dbo.affiliat                  where acctstatus != 'C'   
  
  
*/
GO
