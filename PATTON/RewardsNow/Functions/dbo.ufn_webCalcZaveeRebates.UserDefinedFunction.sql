USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_webCalcZaveeRebates]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_webCalcZaveeRebates]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_webCalcZaveeRebates]
(
	@BeginDate DATETIME,
	@EndDate DATETIME,
	@Client VARCHAR(MAX)
)
RETURNS
@Rebates TABLE
(
	TransactionId INT,
	RNIRebate DECIMAL(18,4),
	ZaveeRebate DECIMAL(18,4)
)

AS
BEGIN

	DECLARE @tranid int,
			@tranamt decimal(18,4), 
			@agent varchar(20), 
			@immrebate as decimal(18,4), 
			@650rebate decimal(18,4), 
			@fiid varchar(3), 
			@clientrebate decimal(18,4)
	
	DECLARE BuildCursor CURSOR FOR 
		SELECT sid_ZaveeTransactions_Identity, dim_ZaveeTransactions_TransactionAmount, dim_ZaveeTransactions_AgentName, 
			   dim_ZaveeTransactions_IMMRebate, dim_ZaveeTransactions_650Rebate, dim_ZaveeTransactions_FinancialInstituionID, 
			   dim_ZaveeTransactions_ClientRebate
		FROM ZaveeTransactions
		WHERE (dim_ZaveeTransactions_PaidDate >= @BeginDate)
			  AND (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
			  AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
			  AND (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split(@Client,',')))
				--and dim_ZaveeTransactions_MerchantName like '%Kabob%'
				
	OPEN BuildCursor 
	FETCH NEXT FROM BuildCursor INTO @tranid, @tranamt, @agent, @immrebate, @650rebate, @fiid, @clientrebate

	declare @grossrebate decimal(18,2)
	declare @extrarebates decimal(18,2)
	declare @RNIRebate decimal(18,2)
	declare @ZaveeRebate decimal(18,2)
	
	WHILE @@FETCH_STATUS = 0 
	BEGIN 
		SET @grossrebate = FLOOR(@tranamt * dbo.ufn_CalcZaveeRebatePercentage(@agent)) / 100
		SET @extrarebates = (FLOOR(@immrebate * 100) / 100) + (FLOOR(@650rebate * 100) / 100) + (FLOOR((CASE WHEN @fiid = '241' THEN @clientrebate ELSE 0 END) * 100) / 100)

		IF ((@grossrebate - @extrarebates) * 100) % 2 > 0
			BEGIN
				SET @RNIRebate = FLOOR(((@grossrebate - @extrarebates) * 100) / 2 + 1) / 100
				SET @ZaveeRebate = FLOOR(((@grossrebate - @extrarebates) * 100) / 2) / 100
			END
		ELSE
			BEGIN
				SET @RNIRebate = FLOOR(((@grossrebate - @extrarebates) * 100) / 2) / 100
				SET @ZaveeRebate = FLOOR(((@grossrebate - @extrarebates) * 100) / 2) / 100
			END

		INSERT INTO @Rebates (TransactionId, RNIRebate,	ZaveeRebate)
		VALUES (@tranid, @RNIRebate, @ZaveeRebate)

		FETCH NEXT FROM BuildCursor INTO @tranid, @tranamt, @agent, @immrebate, @650rebate, @fiid, @clientrebate
	END 
	CLOSE BuildCursor 

	DEALLOCATE BuildCursor 

	-- Return the result of the function
	RETURN

END

	--declare @grossrebate decimal(18,2)
	--declare @extrarebates decimal(18,2)
	--	SELECT sid_ZaveeTransactions_Identity, dim_ZaveeTransactions_TransactionAmount, 
	--			FLOOR(dim_ZaveeTransactions_TransactionAmount * dbo.ufn_CalcZaveeRebatePercentage(dim_ZaveeTransactions_AgentName)) /100 as GrossRebate,
	--			dim_ZaveeTransactions_IMMRebate + dim_ZaveeTransactions_650Rebate + CASE WHEN dim_ZaveeTransactions_FinancialInstituionID = '241' THEN dim_ZaveeTransactions_ClientRebate ELSE 0 END as ExtraRebate,
	--			dim_ZaveeTransactions_AgentName, 
	--		   ROUND(dim_ZaveeTransactions_IMMRebate, 2, 1), dim_ZaveeTransactions_650Rebate, dim_ZaveeTransactions_FinancialInstituionID, 
	--		   FLOOR(dim_ZaveeTransactions_ClientRebate * 100) / 100
	--	FROM zzZaveeTransactions
	--	WHERE --(dim_ZaveeTransactions_PaidDate >= @BeginDate)
	--		  (dim_ZaveeTransactions_CancelledDate IS NULL OR dim_ZaveeTransactions_CancelledDate = '1900-01-01') 
	----		  --AND (dim_ZaveeTransactions_PaidDate < DATEADD(dd, 1, @EndDate))
	----		  --AND (LEFT(dim_ZaveeTransactions_MemberID, 3) IN ( select * from dbo.ufn_split(@Client,',')))
	--			and dim_ZaveeTransactions_MemberID = '241000000030819'
				
	--	IF ((@grossrebate - @extrarebates) * 100) % 2 > 0
	--		BEGIN
	--			SELECT ((@grossrebate - @extrarebates) * 100) % 2, ROUND((@grossrebate - @extrarebates) / 2 + 0.01, 2, 1),
	--			ROUND((@grossrebate - @extrarebates) / 2, 2, 1)
	--		END
	--	ELSE
	--		BEGIN
	--			SELECT ((@grossrebate - @extrarebates) * 100) % 2, ROUND((@grossrebate - @extrarebates) / 2, 2, 1),
	--			ROUND((@grossrebate - @extrarebates) / 2, 2, 1)
	--		END
GO
