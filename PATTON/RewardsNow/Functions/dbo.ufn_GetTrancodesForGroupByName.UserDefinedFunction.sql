USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetTrancodesForGroupByName]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetTrancodesForGroupByName]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetTrancodesForGroupByName] (@groupname VARCHAR(255))
RETURNS @tc table (sid_trantype_trancode VARCHAR(2))
AS
BEGIN
	INSERT INTO @tc (sid_trantype_trancode)
	SELECT DISTINCT (sid_trantype_trancode)
	FROM RewardsNow.dbo.vw_RNITrancodeMap
	WHERE dim_rnitrancodegroup_name = @groupname

	RETURN 
END
GO
