USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_TranslateFITranCodeToTransactionCode]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_TranslateFITranCodeToTransactionCode]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_RawData_TranslateFITranCodeToTransactionCode]
	(@tipfirst			varchar(3),
	 @data			varchar(255))
RETURNS varchar(2)

AS
/* test
select dbo.ufn_RawData_TranslateFITranCodeToTransactionCode('233','SP')
select * from TranType
*/
BEGIN

      declare @output varchar(2)

      --include a default just in case
      set @output = @data

      if @tipfirst = '233'
      begin
            set @output = case @data
                  when 'SP' then 0
                  when 'PP' then 0
                  when 'SR' then 1
                  when 'PR' then 1
            end
      end

      return @output


END
GO
