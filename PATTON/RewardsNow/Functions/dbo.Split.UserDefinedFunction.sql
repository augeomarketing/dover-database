USE [Rewardsnow]
GO
IF OBJECT_ID(N'Split') IS NOT NULL
	DROP FUNCTION Split
GO

CREATE FUNCTION dbo.Split
(
	@inputList VARCHAR(MAX)
	, @delim VARCHAR(1)
)
RETURNS @IDTable TABLE (Item VARCHAR(255))
AS
BEGIN
	DECLARE @list VARCHAR(MAX)
	DECLARE @item VARCHAR(MAX)
	DECLARE @continue BIT
	DECLARE @index INT = 0

	SET @continue = 0
	SET @list = @inputList

	WHILE @continue = 0
	BEGIN
		
		SET @index = CHARINDEX(@delim, @list)
		
		IF @index = 0
		BEGIN
			SET @item = @list
			SET @continue = 1
		END
		ELSE
		BEGIN
			SET @item = SUBSTRING(@list, 1, (@index - 1))
			SET @list = SUBSTRING(@list, @index+1, len(@list))
			SET @continue = 0
		END
		
		SET @item = LEFT(RTRIM(LTRIM(@item)), 255)
		IF ISNULL(@item, '') <> ''
			BEGIN
				INSERT INTO @IDTable(Item)
				VALUES(@item)
			END

	END
	
	RETURN
END
GO
/*

select * from dbo.Split('123;456;789;', ';')
select * from dbo.Split('123; 456; 789;', ';')
select * from dbo.Split('123 456 789', ' ')
select * from dbo.Split('123  ,456,789', ',')
select * from dbo.Split('123   |   456 |789 |  O12', '|')

*/