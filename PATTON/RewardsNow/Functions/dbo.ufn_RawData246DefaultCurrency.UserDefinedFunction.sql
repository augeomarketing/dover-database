USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData246DefaultCurrency]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData246DefaultCurrency]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData246DefaultCurrency] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(3)

AS  

BEGIN
	DECLARE @output varchar(3)

    set @output = (select dim_rniprocessingparameter_value
                   from rewardsnow.dbo.rniprocessingparameter
                   where dim_rniprocessingparameter_key = 'DEFAULT_TXNCURRENCY'
                   and sid_dbprocessinfo_dbnumber = CASE
                                                        when (select count(*) from rewardsnow.dbo.rniprocessingparameter where dim_rniprocessingparameter_key = 'DEFAULT_TXNCURRENCY' and sid_dbprocessinfo_dbnumber = @tipfirst) > 1 then @tipfirst
                                                        else 'RNI'
                                                    END)
 
 RETURN @output  
END
GO
