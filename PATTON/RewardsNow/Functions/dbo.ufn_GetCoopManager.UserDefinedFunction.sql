USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetCoopManager]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetCoopManager]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Function returns the point value from the Portal_Bonus_Codes or TranType table


CREATE FUNCTION [dbo].[ufn_GetCoopManager] (@guid varchar(512))  
RETURNS VARCHAR(50)
AS 
 
BEGIN 
	DECLARE @Name VARCHAR(50)
	BEGIN
		 SELECT   TOP 1 @Name = cust.name1 --  mgrName
		 from RN1.Coop.dbo.Account a1
		 inner join RN1.coop.dbo.account a2  on a1.MemberNumber = a2.MemberID
		 inner join RN1.coop.dbo.customer cust on  a1.tipnumber = cust.tipnumber
		 WHERE a2.membernumber = @guid
		
	END

	RETURN @Name

END
GO
