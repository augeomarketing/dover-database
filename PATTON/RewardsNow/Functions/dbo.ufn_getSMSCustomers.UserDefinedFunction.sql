USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_getSMSCustomers]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_getSMSCustomers]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_getSMSCustomers]
()
RETURNS
@customer TABLE
(  
	TipNumber varchar(15), 
	Name1 varchar(80), 
	Address1 varchar(255), 
	Address2 varchar(255), 
	city varchar(80), 
	stateabbr varchar(2), 
	ZipCode varchar(10), 
	Email varchar(80)
 )  

AS
BEGIN

	DECLARE @dbnumber nvarchar(3), @dbname varchar(20), @sql nvarchar(max)

	DECLARE tip_cursor CURSOR FOR 
	select DBNumber, DBNameNEXL from dbprocessinfo where LocalMerchantParticipant = 'Y'

	OPEN tip_cursor

	FETCH NEXT FROM tip_cursor 
	INTO @dbnumber, @dbname

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = N'INSERT INTO #customer(TipNumber, Name1, Address1, Address2, city, stateabbr, ZipCode, Email)
			SELECT s.TipNumber, c.Name1, c.Address1, c.Address2, c.city, c.state, c.ZipCode, s.Email 
			from RN1.' + QUOTENAME(@dbname) + '.dbo.[1Security] s 
			inner join RN1.' + QUOTENAME(@dbname) + '.dbo.Customer c on s.TipNumber = c.TipNumber'

		EXECUTE sp_executesql @sql
	
		--left outer join (select dim_RNICustomer_RNIId, min(dim_RNICustomer_CustomerCode) as CustomerCode, max(dim_RNICustomer_LastModified) as LastModified from RNICustomer where dim_RNICustomer_TipPrefix IN 
		--('241','248','611','617','631','615','270','603','643','644','645','646','647','648','649','651','653','658',
		--'637','650','654','274') group by dim_RNICustomer_RNIId) c on a.TipNumber = c.dim_RNICustomer_RNIId

		FETCH NEXT FROM tip_cursor 
		INTO @dbnumber, @dbname
	END 
	CLOSE tip_cursor
	DEALLOCATE tip_cursor

	-- Return the result of the function
	RETURN

END
GO
