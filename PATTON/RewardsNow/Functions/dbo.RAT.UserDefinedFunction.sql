USE [RewardsNow]
GO

/****** Object:  UserDefinedFunction [dbo].[RAT]    Script Date: 03/02/2011 10:20:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RAT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[RAT]
GO

USE [RewardsNow]
GO

/****** Object:  UserDefinedFunction [dbo].[RAT]    Script Date: 03/02/2011 10:20:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[RAT]  (@cSearchExpression nvarchar(4000), @cExpressionSearched  nvarchar(4000), @nOccurrence  smallint = 1 )
returns smallint
as
    begin
      if @nOccurrence > 0
         begin
            declare @i smallint, @length smallint, @StartingPosition  smallint
            select  @length  = datalength(@cExpressionSearched)/(case SQL_VARIANT_PROPERTY(@cExpressionSearched,'BaseType') when 'nvarchar' then 2  else 1 end) -- for unicode
            select  @cSearchExpression = reverse(@cSearchExpression), @cExpressionSearched = reverse(@cExpressionSearched)
            select  @i = 0, @StartingPosition  = -1 
            while @StartingPosition <> 0 and @nOccurrence > @i
               select  @i = @i + 1, @StartingPosition  = charindex(@cSearchExpression  COLLATE Latin1_General_BIN,
                                                                   @cExpressionSearched  COLLATE Latin1_General_BIN, @StartingPosition + 1)
            if @StartingPosition <> 0
              select @StartingPosition = 2 - @StartingPosition +  @length - datalength(@cSearchExpression)/(case SQL_VARIANT_PROPERTY(@cSearchExpression,'BaseType') when 'nvarchar' then 2  else 1 end) -- for unicode
         end
      else
         set @StartingPosition =  NULL
         
      return @StartingPosition
    end

GO


