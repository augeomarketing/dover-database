USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_splitWithIndex]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_splitWithIndex]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_splitWithIndex]  
/* This function is used to ufn_split up multi-value parameters */  
(  
@ItemList NVARCHAR(4000),  
@delimiter CHAR(1)  
)  
RETURNS @IDTable TABLE (ItemIndex bigint identity(1,1), Item VARCHAR(50))  
AS  
BEGIN  
DECLARE @tempItemList NVARCHAR(4000)  
SET @tempItemList = @ItemList  
  
DECLARE @i INT  
DECLARE @Item NVARCHAR(4000)  
  
SET @tempItemList = REPLACE (@tempItemList, @delimiter + ' ', @delimiter)  
SET @i = CHARINDEX(@delimiter, @tempItemList)  
  
WHILE (LEN(@tempItemList) > 0)  
BEGIN  
IF @i = 0  
SET @Item = @tempItemList  
ELSE  
SET @Item = LEFT(@tempItemList, @i - 1)  
  
INSERT INTO @IDTable(Item) VALUES(@Item)  
  
IF @i = 0  
SET @tempItemList = ''  
ELSE  
SET @tempItemList = RIGHT(@tempItemList, LEN(@tempItemList) - @i)  
  
SET @i = CHARINDEX(@delimiter, @tempItemList)  
END  
RETURN  
END
GO
