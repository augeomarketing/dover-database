USE [Rewardsnow]
GO
/****** Object:  UserDefinedFunction [dbo].[CAPICOMDecrypt]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CAPICOMDecrypt] (@TextToDecrypt varchar(4000), @Secret varchar(1000))
RETURNS varchar(4000)
AS
BEGIN
  DECLARE @DecryptedText varchar(4000)
  DECLARE @rc int
  DECLARE @object int
  DECLARE @Method_call varchar(4000)

  EXEC @rc = sp_OACreate 'CAPICOM.EncryptedData', @object OUT
  if @rc <> 0
    return NULL
  
  set @method_call = 'SetSecret("' + @Secret + '")'
  EXEC @rc=sp_OAMethod @Object, @method_call
  if @rc <> 0
    return NULL
  
  set @method_call = 'Decrypt("' + @TextToDecrypt + '")'
  EXEC @rc=sp_OAMethod @Object,@method_call
  if @rc <> 0
    return NULL

  EXEC @rc=sp_OAGetProperty @Object, 'Content',@DecryptedText out
  if @rc <> 0
    return NULL

  exec @rc = sp_OADestroy @object

  return @DecryptedText
END
GO
