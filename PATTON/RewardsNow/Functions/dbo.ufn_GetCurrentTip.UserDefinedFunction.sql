USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetCurrentTip]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetCurrentTip]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetCurrentTip] (@oldtip VARCHAR(15))
RETURNS VARCHAR(15)
AS
BEGIN
	DECLARE @newtip VARCHAR(15)
	
	IF (SELECT COUNT(*) FROM OnlineHistoryWork.dbo.New_Tiptracking WHERE OldTip = @oldtip and newtip <> @oldtip and newtip <> '0') = 0
	BEGIN
		SET @newtip = @oldtip
	END 
	ELSE
	BEGIN
		SELECT TOP 1 @newTip = RewardsNOW.dbo.[ufn_GetCurrentTip](NewTip) 
		FROM OnlineHistoryWork.dbo.New_Tiptracking 
		WHERE OldTip = @oldtip
			AND NewTip <> '0'
		ORDER BY TranDate DESC
	END

	RETURN @newtip

END
GO
