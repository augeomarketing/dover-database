USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractACCTID_REGISTRATION]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractACCTID_REGISTRATION]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractACCTID_REGISTRATION]
    (@tipfirst              varchar(3))  
  
returns nvarchar(max)  
  
as  
  
BEGIN  
  
    declare @db         nvarchar(50)  
    declare @sql        nvarchar(max)  
  
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)  
  
    set @sql = 'select tipnumber, lastname, ltrim(rtrim(acctid)) mapvalue  
                from ' + @db + '.dbo.affiliat  
                where acctstatus != ''C'' 
                and accttype = ''REGISTRATION''
                ' 
    return @sql  
END  
  
/*  

--Add to source

INSERT INTO MappingSource (dim_mappingsource_description, sid_mappingtable_id, sid_mappingsourcetype_id, dim_mappingsource_source, dim_mappingsource_active)
SELECT 'Registration-Acctid from Affliat (REGISTRATION)', 3, 2, 'ufn_mappingextractACCTID_REGISTRATION', 1

Test harness  
  
select rewardsnow.dbo.ufn_mappingextractlast4_PRIMARYID ('248')  
  
-- resulting @SQL value:  
select tipnumber, lastname, right( ltrim(rtrim(acctid)), 4) mapvalue                    from [248].dbo.affiliat                    where acctstatus != 'C'                   and accttype = 'PRIMARYID'
  
  
*/
GO
