USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractlast6_CREDIT]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractlast6_CREDIT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractlast6_CREDIT]
    (@tipfirst              varchar(3))    
    
returns nvarchar(max)    
    
as    
    
BEGIN    
    
    declare @db         nvarchar(50)    
    declare @sql        nvarchar(max)    
    
    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)    
    
    set @sql = 'select tipnumber, lastname, right( ltrim(rtrim(acctid)), 6) mapvalue    
                from ' + @db + '.dbo.affiliat    
                where acctstatus != ''C''   
                and accttype LIKE ''CREDIT%'''    
                    
    return @sql    
END
GO
