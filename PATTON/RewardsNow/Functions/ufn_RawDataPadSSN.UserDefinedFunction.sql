USE [Rewardsnow] 
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_PadSSN]    Script Date: 01/11/2012 08:32:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_RawDataPadSSN]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_RawDataPadSSN]
GO

/****** Object:  UserDefinedFunction [dbo].[ufn_PadSSN]    Script Date: 01/11/2012 08:32:24 ******/

-- =============================================
-- Author:		Rich 
-- Create date: 2011-10-30
-- Description:	pad column with leading zeros
-- =============================================
CREATE FUNCTION [dbo].[ufn_RawDataPadSSN]
(
		@TipFirst varchar(3), 
		@Data varchar(max)
)  
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Output VARCHAR(MAX)

	set @Output =  REPLICATE ( '0',9 - Len ( Rewardsnow.dbo.ufn_Trim( @Data ) ) ) + @Data

	RETURN @Output 

END

GO


