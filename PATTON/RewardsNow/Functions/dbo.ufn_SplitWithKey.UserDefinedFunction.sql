IF OBJECT_ID(N'ufn_SplitWithKey', 'TF') is not null
	drop function ufn_SplitWithKey
GO

CREATE FUNCTION ufn_SplitWithKey(@key varchar(max), @list varchar(max), @delim varchar(1))
RETURNS @splitwithkey TABLE (KeyItem varchar(max), ListElement varchar(max))
AS
BEGIN
	declare @keytable table (keyitem varchar(max));
	insert @keytable values (@key);
	
	INSERT INTO @splitwithkey (KeyItem, ListElement)
	select keyitem, item
	from @keytable
	cross join dbo.Split(@list, @delim);
	
	RETURN;
	
END
--select * from dbo.ufn_SplitWithKey('1', 'A,B,C,D', ',')
GO
