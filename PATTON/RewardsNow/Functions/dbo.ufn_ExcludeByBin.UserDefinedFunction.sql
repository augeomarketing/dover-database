USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ExcludeByBin]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_ExcludeByBin]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ExcludeByBin] (@jobid BIGINT) 
	RETURNS @ExcludeTips TABLE (tipnumber VARCHAR(15) PRIMARY KEY)          
AS  
BEGIN      
	DECLARE @sid_dbprocessinfo_dbnumber VARCHAR(3)     
	SET @sid_dbprocessinfo_dbnumber = (SELECT sid_dbprocessinfo_dbnumber FROM rewardsnow.dbo.processingjob WHERE sid_processingjob_id = @jobid)          

	IF @sid_dbprocessinfo_dbnumber = '229'
	BEGIN

		INSERT INTO @excludeTips (tipnumber)   
		SELECT DISTINCT dim_rnicustomer_rniid AS tipnumber   
		FROM RNICustomer rnic   
		WHERE sid_dbprocessinfo_dbnumber='229'
			AND dim_rnicustomer_cardnumber LIKE '493720%'
		
	END
	
	RETURN

END
GO
