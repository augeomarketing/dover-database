USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData241ForceSig]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData241ForceSig]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData241ForceSig] (@tipfirst varchar(3), @data varchar(max))
RETURNS varchar(1)
AS
BEGIN
	DECLARE @output VARCHAR(1)
	SET @output = '2'
	RETURN @output
END
GO
