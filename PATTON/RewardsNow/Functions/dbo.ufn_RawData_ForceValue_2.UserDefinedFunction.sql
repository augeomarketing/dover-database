USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_ForceValue_2]    Script Date: 08/12/2013 11:57:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_RawData_ForceValue_2]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[ufn_RawData_ForceValue_2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ufn_RawData_ForceValue_2]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[ufn_RawData_ForceValue_CCC] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(1)  
AS  
BEGIN  
 DECLARE @output VARCHAR(1)  
 SET @output = ''CCC''  
 RETURN @output  
END  ' 
END
GO
