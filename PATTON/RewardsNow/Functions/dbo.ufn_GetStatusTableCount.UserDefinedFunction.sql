USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetStatusTableCount]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetStatusTableCount]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicholas T. Parsons
-- Create date: 3.23.2015
-- Description:	Gets status table count for 
--              given query.
-- =============================================
CREATE FUNCTION [dbo].[ufn_GetStatusTableCount] 
(
	-- Add the parameters for the function here
	@StatusTableQuery NVARCHAR(MAX)
)
RETURNS INTEGER
AS
BEGIN
	-- Declare the return variable here
	DECLARE @TotalStageTables INTEGER

	-- Add the T-SQL statements to compute the return value here
	EXECUTE usp_GetStatusTableCount @StatusTableQuery, @TotalStageTables OUTPUT
	-- Return the result of the function
	RETURN @TotalStageTables

END
GO
