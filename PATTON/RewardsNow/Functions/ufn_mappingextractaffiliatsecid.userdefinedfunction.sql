use rewardsnow
GO

if object_id('ufn_mappingextractaffiliatsecid') is not null
    drop function dbo.ufn_mappingextractaffiliatsecid
GO


create function dbo.ufn_mappingextractaffiliatsecid
    (@tipfirst              varchar(3))

returns nvarchar(max)

as

BEGIN

    declare @db         nvarchar(50)
    declare @sql        nvarchar(max)

    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    set @sql = 'select tipnumber, lastname, secid mapvalue
                from ' + @db + '.dbo.affiliat
                where acctstatus != ''C'' '
                
    return @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingextractaffiliatsecid ('102')

-- resulting @SQL value:
select tipnumber, lastname, right( ltrim(rtrim(acctid)), 6) mapvalue                  from [102BCM].dbo.affiliat                  where acctstatus != 'C' 


*/