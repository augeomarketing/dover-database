USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData261_FormatMemberID]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData261_FormatMemberID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData261_FormatMemberID] (@TipFirst varchar(3), @data varchar(max)) 
RETURNS VARCHAR(10)
AS
BEGIN
	DECLARE @output VARCHAR(10)
	DECLARE @type INT
	
	SET @data = right(rtrim(@data),4) + substring(@data,11,6)
	SET @output = @data
	
	RETURN @output

END
GO
