USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_mappingextractCustomerMisc4]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_mappingextractCustomerMisc4]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_mappingextractCustomerMisc4]
    (@tipfirst              varchar(3))

returns nvarchar(max)

as

BEGIN

    declare @db         nvarchar(50)
    declare @sql        nvarchar(max)

    set @db = (select quotename(dbnamepatton) from rewardsnow.dbo.dbprocessinfo where dbnumber = @tipfirst)

    set @sql = 'select tipnumber, lastname, ltrim(rtrim(misc4)) mapvalue
                from ' + @db + '.dbo.customer
                where status != ''C'' '
                
    return @sql
END


/*  Test harness

select rewardsnow.dbo.ufn_mappingextractCustomerMisc4 ('219')

-- resulting @SQL value:
select tipnumber, lastname, ltrim(rtrim(misc4)) mapvalue                  from [219DeanBank].dbo.customer                  where status != 'C' 


*/
GO
