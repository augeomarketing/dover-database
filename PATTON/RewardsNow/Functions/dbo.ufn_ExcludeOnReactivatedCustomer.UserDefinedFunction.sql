USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ExcludeOnReactivatedCustomer]    Script Date: 03/25/2016 11:12:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ExcludeOnReactivatedCustomer] (@jobid BIGINT)
RETURNS @ExcludeTips TABLE (tipnumber          varchar(15) primary key)
        
AS

BEGIN

        insert into @excludeTips (tipnumber)
		select distinct cs.tipnumber 
		from [219DeanBank].dbo.customer cs
		join [219DeanBank].dbo.impCustomer_Reactivate ra
		on cs.misc4 = ra.PrimaryCustomerDDA


return 

END
GO
