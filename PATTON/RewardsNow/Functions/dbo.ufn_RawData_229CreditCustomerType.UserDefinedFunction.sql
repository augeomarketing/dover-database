USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_229CreditCustomerType]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_RawData_229CreditCustomerType]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_229CreditCustomerType] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(1)  
AS  
BEGIN  
 DECLARE @output VARCHAR(1)  
 
 SET @output = 
	CASE @data 
		WHEN '45200100493720' THEN 2
		WHEN '45208500408138' THEN 3
		ELSE 1
	END
 
	--Visa Signature Credit Card    sys/prin 4520/0100, BIN 493720
	--Our State Rewards Credit Card sys/prin 4520/8500, BIN 408138


 RETURN @output  
END
GO
