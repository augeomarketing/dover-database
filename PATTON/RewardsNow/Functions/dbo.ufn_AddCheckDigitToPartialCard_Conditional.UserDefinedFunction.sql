USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_AddCheckDigitToPartialCard_Conditional]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_AddCheckDigitToPartialCard_Conditional]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_AddCheckDigitToPartialCard_Conditional] (@tipfirst VARCHAR(3), @card VARCHAR(MAX))   
 RETURNS VARCHAR(MAX)   
 AS   
 BEGIN    
 DECLARE @OUTPUT VARCHAR(MAX)  
   
 SET @card = LTRIM(RTRIM(@card))  
 SET @OUTPUT = @card
 
 IF LEN(@card) = 15  
  SET @OUTPUT = @card + CONVERT(VARCHAR(1), REWARDSNOW.DBO.UFN_GetLuhnSum(@card))    
   
 return @output   
END
GO
