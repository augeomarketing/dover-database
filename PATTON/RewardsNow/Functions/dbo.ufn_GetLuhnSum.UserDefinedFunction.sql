USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_GetLuhnSum]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_GetLuhnSum]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetLuhnSum] (@s varchar(20) )
RETURNS int
AS
	BEGIN
	DECLARE @i int, @j int, @total int, @final int, @tmp int, @result bit, @testMsg varchar(50)
 	
	SELECT @i = 2
	SELECT @final = 0
	SELECT @total = 0
	SELECT @s =ltrim(@s)
	SELECT @s =rtrim(@s)
	WHILE @i < (len(@s) + 2)
	BEGIN
	SELECT @j=(ASCII(substring(@s,@i-1,1))-48)
	IF @i % 2 = 0
		SELECT @j = @j * 2
		IF @j > 9
			SELECT @j = @j - 9
	SELECT @tmp = @total
	SELECT @total=@tmp+@j
	SELECT @i=@i+1
	END
	IF @total % 10 = 0
		SELECT @final = 0
	ELSE
		SELECT @final = 10 - (@total % 10)
   	RETURN (@final)
END
GO
