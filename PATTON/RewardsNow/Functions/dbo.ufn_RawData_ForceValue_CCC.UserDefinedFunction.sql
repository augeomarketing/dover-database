USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_ForceValue_CCC]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_ForceValue_CCC]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_ForceValue_CCC] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(3)  
AS  
BEGIN  
 DECLARE @output VARCHAR(3)  
 SET @output = 'CCC'  
 RETURN @output  
END
GO
