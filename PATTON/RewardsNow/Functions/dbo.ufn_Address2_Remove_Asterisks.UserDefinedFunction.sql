USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_Address2_Remove_Asterisks]    Script Date: 10/06/2015 14:27:48 ******/
DROP FUNCTION [dbo].[ufn_Address2_Remove_Asterisks]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[ufn_Address2_Remove_Asterisks]
	(@tipfirst			varchar(3),
	 @address			varchar(255))
RETURNS varchar(50)

AS

BEGIN
	declare @address2	varchar(50)

	if @address is not NULL and @address !=''
			set @address2 = (Case When @address like '%[0-9a-Z]%'
							Then @address
							Else replace(@address,'*','') 
							END)
	else
		set @address2 = ''

return @address2
END
GO
