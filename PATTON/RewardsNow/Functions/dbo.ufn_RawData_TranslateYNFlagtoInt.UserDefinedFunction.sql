USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_TranslateYNFlagtoInt]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_TranslateYNFlagtoInt]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufn_RawData_TranslateYNFlagtoInt]
	(@tipfirst			varchar(3),
	 @YNFlag			varchar(255))
RETURNS varchar(2)

AS
/* test
select dbo.ufn_RawData_TranslateYNFlagtoInt('233','N')
*/
BEGIN
	declare @xlatestatus	varchar(2)
	
	set @YNFlag = 'YNFLAG_' + ltrim(rtrim(@YNFlag))
	
	set @xlatestatus = (select top 1 dim_rniprocessingparameter_value
						from rewardsnow.dbo.rniprocessingparameter
						where sid_dbprocessinfo_dbnumber = @tipfirst
						and dim_rniprocessingparameter_key = @YNFlag
						and dim_rniprocessingparameter_active = 1)
	return @xlatestatus

END
GO
