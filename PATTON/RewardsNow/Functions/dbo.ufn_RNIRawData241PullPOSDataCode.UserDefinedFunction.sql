USE [RewardsNow]
GO

IF OBJECT_ID(N'ufn_RNIRawData241PullPOSDataCode') IS NOT NULL
	DROP FUNCTION ufn_RNIRawData241PullPOSDataCode
GO

CREATE FUNCTION ufn_RNIRawData241PullPOSDataCode (@tipfirst VARCHAR(3), @data VARCHAR(MAX))
RETURNS VARCHAR(1)
AS
BEGIN
	DECLARE @output varchar(1)
	
	SET @output = CASE 
					WHEN SUBSTRING(@data, 33, 1) = 1 THEN 1
					ELSE 2
				  END
	
	RETURN @output
END