USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RawData_RemoveNonNumericFromPhone]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RawData_RemoveNonNumericFromPhone]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RawData_RemoveNonNumericFromPhone] (@tipfirst varchar(3), @data varchar(max))  
RETURNS varchar(10)  
AS  
BEGIN  
 DECLARE @output VARCHAR(10)  
 
 SET @output = isnull(replace(replace(replace(@data,'(',''),')',''),'-',''),'0000000000')
	
 RETURN @output  
END
GO
