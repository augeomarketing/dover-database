  
create function dbo.ufn_TranslateCustomerStatusWithNulls  
 (@tipfirst   varchar(3),  
  @status   varchar(255))  
RETURNS varchar(2)  
  
AS  
  
BEGIN  
 declare @xlatestatus varchar(2)  
 
 set @status = ltrim(rtrim(isnull(@status, '')))
 
 if @status = ''
 	set @status = 'NULL'
   
 set @status = 'CUSTOMERSTATUS_' + @status  
   
 set @xlatestatus = (select top 1 dim_rniprocessingparameter_value  
      from rewardsnow.dbo.rniprocessingparameter  
      where sid_dbprocessinfo_dbnumber = @tipfirst  
      and dim_rniprocessingparameter_key = @status  
      and dim_rniprocessingparameter_active = 1)  
 return @xlatestatus  
  
END