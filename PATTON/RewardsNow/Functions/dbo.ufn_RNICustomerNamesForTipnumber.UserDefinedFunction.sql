USE [RewardsNow]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_RNICustomerNamesForTipnumber]    Script Date: 10/06/2015 14:27:49 ******/
DROP FUNCTION [dbo].[ufn_RNICustomerNamesForTipnumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_RNICustomerNamesForTipnumber] (@sid_dbprocessinfo_dbnumber VARCHAR(3))
RETURNS @namelist TABLE
	(
		tipnumber varchar(15)
		, acctname1 varchar(100)
		, acctname2 varchar(100)
		, acctname3 varchar(100)
		, acctname4 varchar(100)
		, acctname5 varchar(100)
		, acctname6 varchar(100)
		, acctname7 varchar(100)
		, acctname8 varchar(100)
		, acctname9 varchar(100)
		, acctname10 varchar(100)
		, acctname11 varchar(100)
		, acctname12 varchar(100)
		, acctname13 varchar(100)
		, acctname14 varchar(100)
		, acctname15 varchar(100)
		, acctname16 varchar(100)
		, acctname17 varchar(100)
		, acctname18 varchar(100)
		, acctname19 varchar(100)
		, acctname20 varchar(100)
	)
AS
BEGIN

	DECLARE @namesort TABLE
	(
		sid_namesort_id BIGINT IDENTITY(1,1)
		, dim_rnicustomer_rniid VARCHAR(15)
		, dim_rnicustomer_name1 VARCHAR(40)
		, dim_rnicustomer_name2 VARCHAR(40)
		, dim_rnicustomer_name3 VARCHAR(40)
		, dim_rnicustomer_name4 VARCHAR(40)
	)

	INSERT INTO @namesort
	(
		dim_rnicustomer_rniid, dim_rnicustomer_name1, dim_rnicustomer_name2, dim_rnicustomer_name3, dim_rnicustomer_name4
	)
	select 
		rnic.dim_rnicustomer_rniid
		, UPPER(rewardsnow.dbo.ufn_RemoveSpecialCharacters(rnic.dim_RNICustomer_Name1)) AS dim_rnicustomer_name1
		, UPPER(rewardsnow.dbo.ufn_RemoveSpecialCharacters(rnic.dim_RNICustomer_Name2)) AS dim_rnicustomer_name2
		, UPPER(rewardsnow.dbo.ufn_RemoveSpecialCharacters(rnic.dim_RNICustomer_Name3)) AS dim_rnicustomer_name3
		, UPPER(rewardsnow.dbo.ufn_RemoveSpecialCharacters(rnic.dim_RNICustomer_Name4)) AS dim_rnicustomer_name4
		--, sid_RNICustomer_ID
	from RNICustomer rnic
	left outer join ufn_RNICustomerGetStatusByProperty('CAUSE_PURGE') prp
	ON convert(int, rnic.dim_rnicustomer_customercode) = prp.sid_rnicustomercode_id
	where rnic.sid_dbprocessinfo_dbnumber = @sid_dbprocessinfo_dbnumber
		and prp.dim_status_id IS NULL
	order by rnic.dim_rnicustomer_rniid, rnic.dim_RNICustomer_PrimaryIndicator desc, rnic.sid_RNICustomer_ID ASC

	DECLARE @names TABLE
	(
		sid_names_id INT IDENTITY(1,1) PRIMARY KEY
		, dim_names_tipnumber varchar(15)
		, dim_names_name VARCHAR(max)
	);

	DECLARE @names2 TABLE
	(
		sid_names_id INT PRIMARY KEY
		, dim_names_tipnumber varchar(15)
		, dim_names_name VARCHAR(max)
		, dim_names_rank VARCHAR(10)
	);

	INSERT INTO @names (dim_names_tipnumber, dim_names_name)
	SELECT dim_rnicustomer_rniid, listname
	FROM
		@namesort n
	UNPIVOT
	(
		listname FOR fieldlist in ([dim_RNICustomer_Name1], [dim_RNICustomer_Name2], [dim_RNICustomer_Name3], [dim_RNICustomer_Name4])
	) as unpvt
	WHERE listname <> ''

	--DEDUPE NAMES

	INSERT INTO @names2(sid_names_id, dim_names_tipnumber, dim_names_name, dim_names_rank)
	SELECT nms.sid_names_id, nms.dim_names_tipnumber, nms.dim_names_name
			, 'ACCTNAME' + CAST(RANK() OVER (PARTITION BY nms.dim_names_tipnumber ORDER BY (nms.sid_names_id)) AS VARCHAR) 
	FROM @names nms
	INNER JOIN
	(
		SELECT MIN(sid_names_id) sid_names_id, dim_names_tipnumber, dim_names_name
		FROM @names
		GROUP BY dim_names_tipnumber, dim_names_name
	) unq
	ON nms.sid_names_id = unq.sid_names_id
	ORDER BY nms.sid_names_id;

	INSERT INTO @namelist
	(
		TIPNUMBER
		, acctname1
		, acctname2
		, acctname3
		, acctname4
		, acctname5
		, acctname6
		, acctname7
		, acctname8
		, acctname9
		, acctname10
		, acctname11
		, acctname12
		, acctname13
		, acctname14
		, acctname15
		, acctname16
		, acctname17
		, acctname18
		, acctname19
		, acctname20
	)
	SELECT dim_names_tipnumber
		, [acctname1] AS acctname1
		, [acctname2] AS acctname2
		, [acctname3] AS acctname3
		, [acctname4] AS acctname4
		, [acctname5] AS acctname5
		, [acctname6] AS acctname6
		, [acctname7] AS acctname7
		, [acctname8] AS acctname8
		, [acctname9] AS acctname9
		, [acctname10] AS acctname10
		, [acctname11] AS acctname11
		, [acctname12] AS acctname12
		, [acctname13] AS acctname13
		, [acctname14] AS acctname14
		, [acctname15] AS acctname15
		, [acctname16] AS acctname16
		, [acctname17] AS acctname17
		, [acctname18] AS acctname18
		, [acctname19] AS acctname19
		, [acctname20] AS acctname20
	FROM
	(SELECT dim_names_tipnumber, dim_names_name, dim_names_rank FROM @names2) nms
	PIVOT (MIN (dim_names_name) FOR dim_names_rank IN ([acctname1], [acctname2], [acctname3], [acctname4], [acctname5], [acctname6], [acctname7], [acctname8], [acctname9], [acctname10], [acctname11], [acctname12], [acctname13], [acctname14], [acctname15], [acctname16], [acctname17], [acctname18], [acctname19], [acctname20])
	) AS PVT;
	
	RETURN;
END
GO
