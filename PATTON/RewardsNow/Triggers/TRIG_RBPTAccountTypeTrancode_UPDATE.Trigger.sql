/****** Object:  Trigger [TRIG_RBPTAccountTypeTrancode_UPDATE]    Script Date: 02/20/2009 14:57:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTAccountTypeTrancode_UPDATE] ON [dbo].[RBPTAccountTypeTranCode] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTAccountTypeTrancode_DateLastModified = getdate()
	FROM dbo.RBPTAccountTypeTrancode c JOIN deleted del
		ON c.sid_TipFirst = del.sid_TipFirst
			AND c.sid_RBPTAccountTYpe_ID = del.sid_RBPTAccountTYpe_ID
			AND c.sid_trantype_trancode = del.sid_trantype_trancode
 END
GO
