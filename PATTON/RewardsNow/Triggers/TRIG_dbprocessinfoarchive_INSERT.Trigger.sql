USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_dbprocessinfoarchive_insert]    Script Date: 01/31/2014 15:00:00 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_dbprocessinfoarchive_insert]'))
DROP TRIGGER [dbo].[TRIG_dbprocessinfoarchive_insert]
GO

/****** Object:  Trigger [dbo].[TRIG_dbprocessinfoarchive_insert]    Script Date: 01/31/2014 15:00:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_dbprocessinfoarchive_insert]
   ON  [RewardsNow].[dbo].[dbprocessinfo]
   AFTER Update
AS 
BEGIN
	SET NOCOUNT ON;

    INSERT INTO Rewardsnow.dbo.DBProcessInfoArchive (DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, DateJoined, MinRedeemNeeded, MaxPointsPerYear, LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName, GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, Logo, Landing, TermsPage, FaqPage, EarnPage, Business, StatementDefault, StatementNum, CustomerServicePhone, ServerName, TransferHistToRn1, CalcDailyExpire, hasonlinebooking, AccessDevParticipant, VesdiaParticipant, MinCatalogPointValue, MaxCatalogPointValue, ExpPointsDisplayPeriodOffset, ExpPointsDisplay, sid_editaddress_id, SSOExactMatch, ShoppingFlingExportFile, MDTAutoUpdateLiabilitySent, ParseFirstName, LocalMerchantParticipant, OnlineOffersParticipant, LocalMerchantChainsParticipant)
    SELECT DBNumber, DBNamePatton, DBNameNEXL, DBAvailable, ClientCode, ClientName, ProgramName, DBLocationPatton, DBLocationNexl, PointExpirationYears, DateJoined, MinRedeemNeeded, MaxPointsPerYear, LastTipNumberUsed, PointsExpireFrequencyCd, ClosedMonths, WelcomeKitGroupName, GenerateWelcomeKit, sid_FiProdStatus_statuscode, IsStageModel, ExtractGiftCards, ExtractCashBack, RNProgramName, PointsUpdated, TravelMinimum, TravelBottom, CashBackMinimum, Merch, AirFee, Logo, Landing, TermsPage, FaqPage, EarnPage, Business, StatementDefault, StatementNum, CustomerServicePhone, ServerName, TransferHistToRn1, CalcDailyExpire, hasonlinebooking, AccessDevParticipant, VesdiaParticipant, MinCatalogPointValue, MaxCatalogPointValue, ExpPointsDisplayPeriodOffset, ExpPointsDisplay, sid_editaddress_id, SSOExactMatch, ShoppingFlingExportFile, MDTAutoUpdateLiabilitySent, ParseFirstName, LocalMerchantParticipant, OnlineOffersParticipant, LocalMerchantChainsParticipant
	FROM deleted

END

GO