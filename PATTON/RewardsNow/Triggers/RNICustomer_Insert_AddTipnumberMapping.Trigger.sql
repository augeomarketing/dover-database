USE [RewardsNow]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Trigger [TRIG_dbProcessinfo_NoUpdateAllRows]    Script Date: 02/06/2014 16:46:50 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[RNICustomer_Insert_AddTipnumberMapping]'))
DROP TRIGGER [dbo].[RNICustomer_Insert_AddTipnumberMapping]
GO

CREATE TRIGGER [dbo].[RNICustomer_Insert_AddTipnumberMapping]
ON [dbo].[RNICustomer]

AFTER INSERT
AS

SET NOCOUNT ON
SET XACT_ABORT ON 
SET ARITHABORT ON 

DECLARE	
	@Tipnumber VARCHAR(15),
	@MemberId VARCHAR(50)

SELECT @Tipnumber = dim_RNICustomer_RNIId, @MemberId = dim_RNICustomer_Member FROM inserted	

IF NOT EXISTS(SELECT * FROM MemberIdToTip WHERE dim_memberidtotip_tipnumber = @Tipnumber)
	BEGIN
		INSERT INTO MemberIdToTip (dim_memberidtotip_memberid, dim_memberidtotip_tipnumber)
		VALUES (@MemberId, @Tipnumber)
	END

GO
