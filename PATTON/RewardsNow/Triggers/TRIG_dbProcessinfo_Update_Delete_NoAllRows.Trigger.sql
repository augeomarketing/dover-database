USE [RewardsNow]
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Trigger [TRIG_dbProcessinfo_NoUpdateAllRows]    Script Date: 02/06/2014 16:46:50 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_dbProcessinfo_Update_Delete_NoAllRows]'))
DROP TRIGGER [dbo].[TRIG_dbProcessinfo_Update_Delete_NoAllRows]
GO


CREATE TRIGGER [dbo].[TRIG_dbProcessinfo_Update_Delete_NoAllRows]
   ON  [RewardsNow].[dbo].[dbprocessinfo]
   FOR Update , Delete
AS 
BEGIN
     DECLARE @Count int
     SET @Count = @@ROWCOUNT;
         
     IF @Count >= (SELECT COUNT(*) FROM [Rewardsnow].dbo.[dbProcessInfo])
     BEGIN
         RAISERROR('Cannot Update/Delete ALL Rows. USE WHERE CLAUSE',16,1) 
         ROLLBACK TRANSACTION
         RETURN;
     END
END

GO


