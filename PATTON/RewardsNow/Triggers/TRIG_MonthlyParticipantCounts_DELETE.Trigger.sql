/****** Object:  Trigger [TRIG_MonthlyParticipantCounts_DELETE]    Script Date: 02/20/2009 14:57:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_MonthlyParticipantCounts_DELETE] ON [dbo].[MonthlyParticipantCounts] 
      FOR Delete
      AS 
 BEGIN 

	insert into dbo.MonthlyParticipantCounts_History
	(TipFirst, DBFormalName, TotCustomers, TotAffiliatCnt, TotCreditCards, TotDebitCards, 
	 TotEmailCnt, TotDebitWithDDA, TotDebitWithoutDDA, TotBillable, TotGroupedDebits, Rundate, DateDeleted, UpdateType, MonthEndDate, billedstat, billeddate, DateJoined)

	select TipFirst, DBFormalName, TotCustomers, TotAffiliatCnt, TotCreditCards, TotDebitCards, 
	TotEmailCnt, TotDebitWithDDA, TotDebitWithoutDDA, TotBillable, TotGroupedDebits, Rundate, getdate(), 'Deleted', MonthEndDate, billedstat, billeddate, DateJoined
	from deleted 

 END
GO
