USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNITransactionLoadColumn_BackupRec_DELETE]    Script Date: 10/10/2014 11:50:29 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNITransactionLoadColumn_BackupRec_DELETE]'))
DROP TRIGGER [dbo].[TRIG_RNITransactionLoadColumn_BackupRec_DELETE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNITransactionLoadColumn_BackupRec_DELETE]    Script Date: 10/10/2014 11:50:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 




CREATE TRIGGER [dbo].[TRIG_RNITransactionLoadColumn_BackupRec_DELETE] ON [dbo].[RNITransactionLoadColumn]   
      FOR DELETE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.RNITransactionLoadColumn
	insert into RN_CentralProcessingTables_HIST.dbo.RNITransactionLoadColumn
	Select *,getdate(),SUSER_NAME() from DELETED
	
 END  
  


GO
