USE [RewardsNow]
GO

CREATE TRIGGER TRG_RNITransaction_Insert
ON RNITransaction
FOR INSERT
AS

UPDATE rnit
SET sid_smstranstatus_id = -1
FROM RNITransaction rnit
INNER JOIN INSERTED ins 
	ON rnit.sid_rnitransaction_id = ins.sid_rnitransaction_id
INNER JOIN dbprocessinfo dbpi
	ON rnit.dim_rnitransaction_tipprefix = dbpi.dbnumber
WHERE
	ISNULL(dbpi.LocalMerchantParticipant, 'N') <> 'Y'

