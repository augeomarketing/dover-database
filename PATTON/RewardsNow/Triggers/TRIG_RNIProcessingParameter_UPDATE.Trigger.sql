USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNIProcessingParameter_UPDATE]    Script Date: 06/12/2013 17:29:18 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNIProcessingParameter_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_RNIProcessingParameter_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNIProcessingParameter_UPDATE]    Script Date: 06/12/2013 17:29:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_RNIProcessingParameter_UPDATE] ON [dbo].[RNIProcessingParameter] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
	SET dim_RNIProcessingParameter_lastmodified = getdate()
	FROM dbo.RNIProcessingParameter c 
	JOIN deleted del 
		ON c.sid_dbprocessinfo_dbnumber = del.sid_dbprocessinfo_dbnumber
			AND c.dim_rniprocessingparameter_key = del.dim_rniprocessingparameter_key

 END

GO
