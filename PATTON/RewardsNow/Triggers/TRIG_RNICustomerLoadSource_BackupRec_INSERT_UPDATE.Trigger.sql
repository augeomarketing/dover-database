USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNICustomerLoadSource_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:46:37 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNICustomerLoadSource_BackupRec_INSERT_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_RNICustomerLoadSource_BackupRec_INSERT_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNICustomerLoadSource_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:46:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE TRIGGER [dbo].[TRIG_RNICustomerLoadSource_BackupRec_INSERT_UPDATE] ON [dbo].[RNICustomerLoadSource]   
      FOR INSERT,UPDATE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.RNICustomerLoadSource
	insert into RN_CentralProcessingTables_HIST.dbo.RNICustomerLoadSource
	Select *,getdate(),SUSER_NAME() from deleted	
 END  
  
  





GO
