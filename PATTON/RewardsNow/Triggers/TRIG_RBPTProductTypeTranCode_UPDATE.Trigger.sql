/****** Object:  Trigger [TRIG_RBPTProductTypeTranCode_UPDATE]    Script Date: 02/20/2009 14:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTProductTypeTranCode_UPDATE] ON [dbo].[RBPTProductTypeTranCode] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTProductTypeTranCode_DateLastModified = getdate()
	FROM dbo.RBPTProductTypeTranCode c JOIN deleted del
		ON c.sid_RBPTProductType_id = del.sid_RBPTProductType_id
			AND c.dim_RBPTProductTypeTranCode_Tipfirst = del.dim_RBPTProductTypeTranCode_Tipfirst
			AND c.sid_trantype_trancode = del.sid_trantype_trancode
 END
GO
