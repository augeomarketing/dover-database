USE [RewardsNow]
GO

CREATE TRIGGER TRG_PROMOTIONALPOINTLEDGER_UPSERTDETAIL
ON PromotionalPointLedger
FOR INSERT, UPDATE
AS
BEGIN
	UPDATE ppld
	SET dim_promotionalpointledgerdetail_points = I.dim_promotionalpointledger_maxamount
		, dim_promotionalpointledgerdetail_entrydate = I.dim_promotionalpointledger_awarddate
	FROM PromotionalPointLedgerDetail ppld 
	INNER JOIN INSERTED I 
		ON I.sid_promotionalpointledger_id = ppld.sid_promotionalpointledger_id
			AND ppld.dim_promotionalpointledgerdetail_desc = 'OPENING BALANCE'
			AND 
			(
				I.dim_promotionalpointledger_maxamount <> ppld.dim_promotionalpointledgerdetail_points
				OR I.dim_promotionalpointledger_awarddate <> ppld.dim_promotionalpointledgerdetail_entrydate
			)
				
	
	INSERT INTO PromotionalPointLedgerDetail (sid_promotionalpointledger_id, sid_trantype_trancode, dim_promotionalpointledgerdetail_points, dim_promotionalpointledgerdetail_entrydate, dim_promotionalpointledgerdetail_desc)
	SELECT ppl.sid_promotionalpointledger_id,  '$+', dim_promotionalpointledger_maxamount, dim_promotionalpointledger_awarddate, 'OPENING BALANCE'
	FROM INSERTED ppl
	LEFT OUTER JOIN PromotionalPointLedgerDetail ppld
		ON ppl.sid_promotionalpointledger_id = ppld.sid_promotionalpointledger_id
			AND ppld.sid_trantype_trancode = '$+'
			AND ppld.dim_promotionalpointledgerdetail_desc = 'OPENING BALANCE'
	WHERE
		ppld.sid_promotionalpointledger_id is null
END			
GO

