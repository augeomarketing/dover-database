USE [RewardsNow]
GO

/****** Object:  Trigger [insSSISConfiguration_001]    Script Date: 11/02/2010 14:37:25 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[insSSISConfiguration_001]'))
DROP TRIGGER [dbo].[insSSISConfiguration_001]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[insSSISConfiguration_001]    Script Date: 11/02/2010 14:37:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[insSSISConfiguration_001]
ON [dbo].[SSIS Configurations]
FOR insert AS
INSERT INTO SSISConfigurationKeyName
(
      ConfigurationFilter
      , PackagePath
)
SELECT
      ins.ConfigurationFilter
      , ins.PackagePath
FROM
      inserted ins 
LEFT OUTER JOIN SSISConfigurationKeyName cfg
      ON ins.ConfigurationFilter = cfg.ConfigurationFilter
      AND ins.PackagePath = cfg.PackagePath
WHERE
      cfg.ConfigurationFilter IS NULL
      

GO


