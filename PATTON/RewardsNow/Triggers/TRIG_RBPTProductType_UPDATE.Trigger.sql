/****** Object:  Trigger [TRIG_RBPTProductType_UPDATE]    Script Date: 02/20/2009 14:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTProductType_UPDATE] ON [dbo].[RBPTProductType] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTProductType_DateLastModified = getdate()
	FROM dbo.RBPTProductType c JOIN deleted del
		ON c.sid_RBPTProductType_id = del.sid_RBPTProductType_id
 END
GO
