USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNIHouseholding_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:52:45 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNIHouseholding_BackupRec_INSERT_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_RNIHouseholding_BackupRec_INSERT_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNIHouseholding_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:52:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE TRIGGER [dbo].[TRIG_RNIHouseholding_BackupRec_INSERT_UPDATE] ON [dbo].[RNIHouseholding]   
      FOR INSERT,UPDATE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.RNIHouseholding
	insert into RN_CentralProcessingTables_HIST.dbo.RNIHouseholding
	Select *,getdate(),SUSER_NAME() from deleted	
 END  
  
  





GO
