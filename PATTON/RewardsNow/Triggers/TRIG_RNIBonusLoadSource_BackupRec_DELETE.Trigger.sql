
USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNIBonusLoadSource_BackupRec_DELETE]    Script Date: 10/10/2014 11:48:34 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNIBonusLoadSource_BackupRec_DELETE]'))
DROP TRIGGER [dbo].[TRIG_RNIBonusLoadSource_BackupRec_DELETE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNIBonusLoadSource_BackupRec_DELETE]    Script Date: 10/10/2014 11:48:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
 




CREATE TRIGGER [dbo].[TRIG_RNIBonusLoadSource_BackupRec_DELETE] ON [dbo].[RNIBonusLoadSource]   
      FOR DELETE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.RNIBonusLoadSource
	insert into RN_CentralProcessingTables_HIST.dbo.RNIBonusLoadSource
	Select *,getdate(),SUSER_NAME() from DELETED
	
 END  
  


GO
