USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNITransactionLoadSource_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:47:44 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNITransactionLoadSource_BackupRec_INSERT_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_RNITransactionLoadSource_BackupRec_INSERT_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNITransactionLoadSource_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:47:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE TRIGGER [dbo].[TRIG_RNITransactionLoadSource_BackupRec_INSERT_UPDATE] ON [dbo].[RNITransactionLoadSource]   
      FOR INSERT,UPDATE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.RNITransactionLoadSource
	insert into RN_CentralProcessingTables_HIST.dbo.RNITransactionLoadSource
	Select *,getdate(),SUSER_NAME() from deleted	
 END  
  
  





GO