/****** Object:  Trigger [truMonthStatementCycle]    Script Date: 02/20/2009 14:57:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[truMonthStatementCycle]
   ON  [dbo].[MonthStatementCycle]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update msc	
		set lastModified = getdate()
	from inserted ins join dbo.MonthStatementCycle msc
		on ins.MonthStatementCycle_Month = msc.MonthStatementCycle_Month

END
GO
