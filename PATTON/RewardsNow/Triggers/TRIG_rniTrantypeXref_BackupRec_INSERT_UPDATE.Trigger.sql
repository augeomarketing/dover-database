USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_rniTrantypeXref_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:51:51 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_rniTrantypeXref_BackupRec_INSERT_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_rniTrantypeXref_BackupRec_INSERT_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_rniTrantypeXref_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:51:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE TRIGGER [dbo].[TRIG_rniTrantypeXref_BackupRec_INSERT_UPDATE] ON [dbo].[rniTrantypeXref]   
      FOR INSERT,UPDATE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.rniTrantypeXref
	insert into RN_CentralProcessingTables_HIST.dbo.rniTrantypeXref
	Select *,getdate(),SUSER_NAME() from deleted	
 END  
  
  





GO