/****** Object:  Trigger [tru_RBPTCostPerPoint]    Script Date: 02/20/2009 14:57:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tru_RBPTCostPerPoint]
   ON  [dbo].[RBPTCostPerPoint]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update cpp
		set dim_RBPTCostPerPoint_DateLastModified = getdate()
	from inserted ins join dbo.RBPTcostperpoint cpp
		on ins.sid_tipfirst = cpp.sid_tipfirst
		and ins.sid_TranType_trancode = cpp.sid_TranType_trancode
		and ins.dim_RBPTcostperpoint_effectivedate = cpp.dim_RBPTcostperpoint_effectivedate
END
GO
