USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_StatementTranCode_UPDATE]    Script Date: 05/10/2011 11:34:45 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_StatementTranCode_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_StatementTranCode_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_StatementTranCode_UPDATE]    Script Date: 05/10/2011 11:34:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_StatementTranCode_UPDATE] ON [dbo].[StatementTranCode] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_StatementTranCode_lastmodified = getdate()
	FROM dbo.[StatementTranCode] c JOIN deleted del
		ON c.sid_StatementTranCode_id = del.sid_StatementTranCode_id
 END

GO


