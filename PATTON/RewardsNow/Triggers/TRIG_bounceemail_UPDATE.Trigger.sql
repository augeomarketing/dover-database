USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_bounceemail_UPDATE]    Script Date: 06/12/2013 17:29:18 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_bounceemail_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_bounceemail_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_bounceemail_UPDATE]    Script Date: 06/12/2013 17:29:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[TRIG_bounceemail_UPDATE] ON [dbo].[bounceemail] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
	SET dim_bounceemail_lastmodified = getdate()
	FROM dbo.bounceemail c 
	JOIN deleted del 
		ON c.sid_bounceemail_id = del.sid_bounceemail_id

 END

GO
