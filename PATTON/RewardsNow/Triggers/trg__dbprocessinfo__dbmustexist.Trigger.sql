USE [RewardsNow]
GO

IF OBJECT_ID(N'trg__dbprocessinfo__dbmustexist') IS NOT NULL
	DROP TRIGGER trg__dbprocessinfo__dbmustexist
GO

CREATE TRIGGER trg__dbprocessinfo__dbmustexist
ON dbprocessinfo
FOR INSERT, UPDATE
AS
BEGIN
	DECLARE @dbpattoncount TINYINT
	DECLARE @dbnexlcount TINYINT
	DECLARE @dbnamepatton varchar(50)
	DECLARE @dbnamenexl varchar(50)
	DECLARE @status varchar(1)
	
	
	SELECT @dbnamepatton = dbnamepatton
		, @dbnamenexl = dbnamenexl
		, @status = sid_FIProdStatus_statuscode
	FROM
		INSERTED
	
	IF @status = 'P'
	BEGIN
	
		SET @dbpattoncount = (select count(*) from sys.databases where [name] = @dbnamepatton)
		SET @dbnexlcount = (select count(*) from rn1.master.sys.databases where [name] = @dbnamenexl)
		
		IF ISNULL(@dbpattoncount, 0) = 0 OR ISNULL(@dbnexlcount, 0) = 0
		BEGIN
			RAISERROR('Cannot Insert/Update Record for Database that Does not Exist on Patton and/or RN1', 16, 10)
			ROLLBACK
		END
	END
END
GO
