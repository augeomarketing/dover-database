/****** Object:  Trigger [TRIG_RBPTPostingPeriod_UPDATE]    Script Date: 02/20/2009 14:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTPostingPeriod_UPDATE] ON [dbo].[RBPTPostingPeriod] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTPostingPeriod_DateLastModified = getdate()
	FROM dbo.RBPTPostingPeriod c JOIN deleted del
		ON c.sid_RBPTPostingPeriod_id = del.sid_RBPTPostingPeriod_id
 END
GO
