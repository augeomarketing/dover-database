/****** Object:  Trigger [TRIG_RBPTPriority_UPDATE]    Script Date: 02/20/2009 14:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TRIG_RBPTPriority_UPDATE] ON [dbo].[RBPTPriority] 
      FOR UPDATE 
      NOT FOR REPLICATION 
      AS 
 BEGIN 
	UPDATE c
		SET dim_RBPTPriority_DateLastModified = getdate()
	FROM dbo.RBPTPriority c JOIN deleted del
		ON c.sid_tipfirst = del.sid_tipfirst
			AND c.sid_RBPTAccountType_Id = del.sid_RBPTAccountType_Id
 END
GO
