
USE [RewardsNow]
GO

/****** Object:  Trigger [TRIG_RNIRawImportDataDefinition_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:44:43 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TRIG_RNIRawImportDataDefinition_BackupRec_INSERT_UPDATE]'))
DROP TRIGGER [dbo].[TRIG_RNIRawImportDataDefinition_BackupRec_INSERT_UPDATE]
GO

USE [RewardsNow]
GO

/****** Object:  Trigger [dbo].[TRIG_RNIRawImportDataDefinition_BackupRec_INSERT_UPDATE]    Script Date: 10/10/2014 11:44:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE TRIGGER [dbo].[TRIG_RNIRawImportDataDefinition_BackupRec_INSERT_UPDATE] ON [dbo].[RNIRawImportDataDefinition]   
      FOR INSERT,UPDATE
      AS   
 BEGIN   
 -- select * from RN_CentralProcessingTables_HIST.dbo.RNIRawImportDataDefinition
	insert into RN_CentralProcessingTables_HIST.dbo.RNIRawImportDataDefinition
	Select *,getdate(),SUSER_NAME() from deleted	
 END  
  
  





GO



