USE [RewardsNow]
GO

/****** Object:  Table [dbo].[wrkTipZip]    Script Date: 04/05/2016 15:22:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[wrkTipZip](
	[TipNumber] [varchar](20) NOT NULL,
	[ZipCode] [varchar](10) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


