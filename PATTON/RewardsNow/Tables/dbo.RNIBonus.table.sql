USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonus_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonus]'))
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [FK_RNIBonus_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonus_RNIBonusProcessType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonus]'))
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [FK_RNIBonus_RNIBonusProcessType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonus_RNIRawImport]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonus]'))
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [FK_RNIBonus_RNIRawImport]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonus_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonus]'))
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [FK_RNIBonus_TranType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonus__dim_rn__7D7D68F1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [DF__RNIBonus__dim_rn__7D7D68F1]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__RNIBonus__dim_rn__7E718D2A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [DF__RNIBonus__dim_rn__7E718D2A]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_RNIBonus_dim_rnibonus_bonuspoints]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonus] DROP CONSTRAINT [DF_RNIBonus_dim_rnibonus_bonuspoints]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonus]    Script Date: 04/22/2013 19:03:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonus]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonus]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonus]    Script Date: 04/22/2013 19:03:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonus](
	[sid_rnibonus_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_rnirawimport_id] [bigint] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[sid_rnibonusprocesstype_id] [bigint] NOT NULL,
	[dim_rnibonus_portfolio] [varchar](255) NULL,
	[dim_rnibonus_member] [varchar](255) NULL,
	[dim_rnibonus_primaryid] [varchar](255) NULL,
	[dim_rnibonus_RNIID] [varchar](15) NULL,
	[dim_rnibonus_processingcode] [int] NOT NULL,
	[dim_rnibonus_cardnumber] [varchar](16) NOT NULL,
	[dim_rnibonus_transactionamount] [decimal](18, 2) NOT NULL,
	[dim_rnibonus_bonuspoints] [int] NOT NULL,
	[dim_rnibonus_processingenddate] [date] NOT NULL,
	[sid_trantype_trancode] [nvarchar](2) NOT NULL,
 CONSTRAINT [PK__RNIBonus__65E38CFF7B95207F] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonus_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RNIBonus]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonus_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[RNIBonus] CHECK CONSTRAINT [FK_RNIBonus_dbprocessinfo]
GO

ALTER TABLE [dbo].[RNIBonus]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonus_RNIBonusProcessType] FOREIGN KEY([sid_rnibonusprocesstype_id])
REFERENCES [dbo].[RNIBonusProcessType] ([sid_rnibonusprocesstype_id])
GO

ALTER TABLE [dbo].[RNIBonus] CHECK CONSTRAINT [FK_RNIBonus_RNIBonusProcessType]
GO

ALTER TABLE [dbo].[RNIBonus]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonus_RNIRawImport] FOREIGN KEY([sid_rnirawimport_id])
REFERENCES [dbo].[RNIRawImport] ([sid_rnirawimport_id])
GO

ALTER TABLE [dbo].[RNIBonus] CHECK CONSTRAINT [FK_RNIBonus_RNIRawImport]
GO

ALTER TABLE [dbo].[RNIBonus]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonus_TranType] FOREIGN KEY([sid_trantype_trancode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[RNIBonus] CHECK CONSTRAINT [FK_RNIBonus_TranType]
GO

ALTER TABLE [dbo].[RNIBonus] ADD  CONSTRAINT [DF__RNIBonus__dim_rn__7D7D68F1]  DEFAULT ('') FOR [dim_rnibonus_cardnumber]
GO

ALTER TABLE [dbo].[RNIBonus] ADD  CONSTRAINT [DF__RNIBonus__dim_rn__7E718D2A]  DEFAULT ((0)) FOR [dim_rnibonus_transactionamount]
GO

ALTER TABLE [dbo].[RNIBonus] ADD  CONSTRAINT [DF_RNIBonus_dim_rnibonus_bonuspoints]  DEFAULT ((0)) FOR [dim_rnibonus_bonuspoints]
GO

