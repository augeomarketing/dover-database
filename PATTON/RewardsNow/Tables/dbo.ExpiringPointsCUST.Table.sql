USE [Rewardsnow]
GO
SET ANSI_PADDING ON
GO
DROP TABLE [dbo].[ExpiringPointsCUST]
GO
/****** Object:  Table [dbo].[ExpiringPointsCUST]    Script Date: 09/30/2009 14:10:43 ******/

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ExpiringPointsCUST](
	[sid_ExpiringPointsCUST_TIPNUMBER] [varchar](15) NOT NULL,
	[dim_ExpiringPointsCUST_POINTSTOEXPIRE] [int] NULL,
	[dim_ExpiringPointsCUST_DBNAMEONNEXL] [varchar](50) NULL,
	[dim_ExpiringPointsCUST_STATUS] [varchar](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ExpiringPointsCUST] ADD  CONSTRAINT [DF_CUSTOMER_RunAvailable]  DEFAULT (0) FOR [dim_ExpiringPointsCUST_POINTSTOEXPIRE]
GO
SET ANSI_PADDING OFF
GO
