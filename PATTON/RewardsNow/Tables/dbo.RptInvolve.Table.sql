USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptInvolve]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptInvolve](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[Category] [varchar](25) NOT NULL,
	[CatData] [numeric](27, 6) NULL,
	[RunDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
