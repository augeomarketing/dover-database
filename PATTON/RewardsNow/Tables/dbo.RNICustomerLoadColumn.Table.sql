USE RewardsNow
GO

IF OBJECT_ID(N'RNICustomerLoadColumn') IS NOT NULL
	DROP TABLE RNICustomerLoadColumn
GO

CREATE TABLE RNICustomerLoadColumn
(
	sid_rnicustomerloadcolumn_id BIGINT IDENTITY(1,1) PRIMARY KEY
	, sid_rnicustomerloadsource_id BIGINT NOT NULL
	, dim_rnicustomerloadcolumn_sourcecolumn VARCHAR(100)
	, dim_rnicustomerloadcolumn_targetcolumn VARCHAR(100)
	, dim_rnicustomerloadcolumn_fidbcustcolumn VARCHAR(100)
	, dim_rnicustomerloadcolumn_keyflag TINYINT NOT NULL DEFAULT(0)
	, dim_rnicustomerloadcolumn_loadtoaffiliat TINYINT NOT NULL DEFAULT(0)
	, dim_rnicustomerloadcolumn_affiliataccttype VARCHAR(100)
	, dim_rnicustomerloadcolumn_primaryorder TINYINT NOT NULL DEFAULT(0)
	, dim_rnicustomerloadcolumn_primarydescending TINYINT NOT NULL DEFAULT(0)
)

--NEED CHECK CONSTRAINT: IF LOADTOAFFILIAT = 1 AFFILIATACCTTYPE CANNOT BE NULL OR EMPTY
--NEED TRIGGER: CANNOT ENTER SOURCE COLUMN THAT DOESN'T EXIST IS SOURCE VIEW
--NEED TRIGGER: CANNOT ENTER TARGET COLUMN THAT DOESN'T EXIST IN RNICUSTOMER
--NEED TRIGGER: CANNOT ENTER FIDBCUSTCOLUMN THAT DOESN'T EXIST IN CUSTOMER
--NEED UNIQUE CONSTRAINT ON PRIMARY ORDER + SID_RNICUSTOMERLOADSOURCE_ID 

GO