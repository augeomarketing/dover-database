use [RewardsNow]
go
/*
CREATE TABLE RNICustomerCodeStatus
(
	sid_rnicustomercode_id INT NOT NULL PRIMARY KEY
	, dim_status_id VARCHAR(1)
	, dim_status_description VARCHAR(50)
)
go

INSERT INTO RNICustomerCodeStatus (sid_rnicustomercode_id, dim_status_id, dim_status_description)
SELECT 1, 'A', '[A] Active'
UNION SELECT 2, 'X', '[X] Unenrolled'
UNION SELECT 99, 'C', '[C] Closed'

*/
 -- PREVENT_INSERT = 1, CAUSE_PURGE = 2 (BOTH = 3)
 
INSERT INTO RNICustomerCodeStatus (sid_rnicustomercode_id, dim_status_id, dim_status_description)
SELECT 98, 'E', '[E] End of Program'
UNION SELECT 97, 'O', '[O] Opt Out'
 
 
ALTER TABLE RNICustomerCodeStatus
ADD dim_rnicustomercodestatus_propertysettings BIGINT default(0)

UPDATE RNICustomerCodeStatus
SET dim_rnicustomercodestatus_propertysettings = 0

UPDATE RNICustomerCodeStatus
SET dim_rnicustomercodestatus_propertysettings = 3
WHERE dim_status_id IN ('C', 'E', 'O')
