USE [Rewardsnow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_stmtValue_processingjob]') AND parent_object_id = OBJECT_ID(N'[dbo].[stmtValue]'))
ALTER TABLE [dbo].[stmtValue] DROP CONSTRAINT [FK_stmtValue_processingjob]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__stmtValue__dim_s__7539FB4F]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[stmtValue] DROP CONSTRAINT [DF__stmtValue__dim_s__7539FB4F]
END

GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtValue]    Script Date: 12/23/2011 12:28:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stmtValue]') AND type in (N'U'))
DROP TABLE [dbo].[stmtValue]
GO

USE [Rewardsnow]
GO

/****** Object:  Table [dbo].[stmtValue]    Script Date: 12/23/2011 12:28:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[stmtValue](
	[sid_processingjob_id] [bigint] NOT NULL,
	[dim_stmtvalue_column] [int] NOT NULL,
	[dim_stmtvalue_row] [int] NOT NULL,
	[dim_stmtvalue_value] [varchar](max) NULL,
	[dim_stmtvalue_dateadded] [datetime] NOT NULL,
	[dim_stmtvalue_lastmodified] [datetime] NULL,
	[dim_stmtvalue_lastmodifiedby] [varchar](255) NULL,
 CONSTRAINT [pk__stmtValue] PRIMARY KEY CLUSTERED 
(
	[sid_processingjob_id] ASC,
	[dim_stmtvalue_row] ASC,
	[dim_stmtvalue_column] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[stmtValue]  WITH CHECK ADD  CONSTRAINT [FK_stmtValue_processingjob] FOREIGN KEY([sid_processingjob_id])
REFERENCES [dbo].[processingjob] ([sid_processingjob_id])
GO

ALTER TABLE [dbo].[stmtValue] CHECK CONSTRAINT [FK_stmtValue_processingjob]
GO

ALTER TABLE [dbo].[stmtValue] ADD  DEFAULT (getdate()) FOR [dim_stmtvalue_dateadded]
GO

