USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaFinalFile]    Script Date: 09/04/2012 14:24:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaFinalFile]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaFinalFile]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaFinalFile]    Script Date: 09/04/2012 14:24:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaFinalFile](
	[Column1] [varchar](2) NOT NULL,
	[Column2] [varchar](15) NOT NULL,
	[Column3] [varchar](15) NOT NULL,
	[column4] [varchar](50) NULL,
	[column5] [varchar](30) NULL,
	[column6] [varchar](30) NULL,
	[column7] [varchar](32) NOT NULL,
	[column8] [varchar](40) NOT NULL,
	[column9] [varchar](1) NOT NULL,
	[column10] [varchar](8) NULL,
	[column11] [varchar](6) NOT NULL,
	[column12] [varchar](4) NOT NULL,
	[column13] [varchar](30) NULL,
	[column14] [varchar](30) NULL,
	[column15] [varchar](40) NULL,
	[column16] [varchar](40) NULL,
	[column17] [varchar](40) NULL,
	[column18] [varchar](2) NULL,
	[column19] [varchar](15) NULL,
	[column20] [varchar](20) NULL,
	[column21] [varchar](20) NULL,
	[column22] [varchar](50) NULL,
	[column23] [varchar](1) NULL,
	[column24] [varchar](1) NULL,
	[column25] [varchar](1) NULL,
	[column26] [varchar](15) NULL,
	[column27] [varchar](2) NULL
) ON [PRIMARY]

GO


