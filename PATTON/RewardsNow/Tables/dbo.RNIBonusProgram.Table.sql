USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusProgram_RNIBonusProgramType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusProgram]'))
ALTER TABLE [dbo].[RNIBonusProgram] DROP CONSTRAINT [FK_RNIBonusProgram_RNIBonusProgramType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RNIBonusProgram_TranType]') AND parent_object_id = OBJECT_ID(N'[dbo].[RNIBonusProgram]'))
ALTER TABLE [dbo].[RNIBonusProgram] DROP CONSTRAINT [FK_RNIBonusProgram_TranType]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgramExpirationDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgram_dim_BonusProgram_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgram] DROP CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgram]    Script Date: 08/09/2011 17:29:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgram]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgram]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgram]    Script Date: 08/09/2011 17:29:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProgram](
	[sid_rnibonusprogram_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_rnibonusprogram_description] [varchar](255) NOT NULL,
	[dim_rnibonusprogram_effectivedate] [date] NOT NULL,
	[dim_rnibonusprogram_expirationdate] [date] NOT NULL,
	[sid_rnibonusprogramtype_code] [varchar](1) NOT NULL,
	[sid_trantype_tranCode] [nvarchar](2) NOT NULL,
	[dim_rnibonusprogram_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogram_lastupdated] [datetime] NOT NULL,
 CONSTRAINT [PK_RNIBonusProgram] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogram_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[truRNIBonusProgram_LastUpdated]    Script Date: 08/09/2011 17:29:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[truRNIBonusProgram_LastUpdated]
	on [dbo].[RNIBonusProgram]
	AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bp
		set dim_RNIBonusProgram_LastUpdated = getdate()
	from inserted ins join dbo.RNIBonusProgram bp
		on ins.sid_RNIBonusProgram_ID = bp.sid_RNIBonusProgram_id

END

GO

ALTER TABLE [dbo].[RNIBonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusProgram_RNIBonusProgramType] FOREIGN KEY([sid_rnibonusprogramtype_code])
REFERENCES [dbo].[RNIBonusProgramType] ([sid_rnibonusprogramtype_code])
GO

ALTER TABLE [dbo].[RNIBonusProgram] CHECK CONSTRAINT [FK_RNIBonusProgram_RNIBonusProgramType]
GO

ALTER TABLE [dbo].[RNIBonusProgram]  WITH CHECK ADD  CONSTRAINT [FK_RNIBonusProgram_TranType] FOREIGN KEY([sid_trantype_tranCode])
REFERENCES [dbo].[TranType] ([TranCode])
GO

ALTER TABLE [dbo].[RNIBonusProgram] CHECK CONSTRAINT [FK_RNIBonusProgram_TranType]
GO

ALTER TABLE [dbo].[RNIBonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgramExpirationDate]  DEFAULT ('12/31/9999 23:59:59') FOR [dim_rnibonusprogram_expirationdate]
GO

ALTER TABLE [dbo].[RNIBonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_DateAdded]  DEFAULT (getdate()) FOR [dim_rnibonusprogram_dateadded]
GO

ALTER TABLE [dbo].[RNIBonusProgram] ADD  CONSTRAINT [DF_BonusProgram_dim_BonusProgram_LastUpdated]  DEFAULT (getdate()) FOR [dim_rnibonusprogram_lastupdated]
GO

