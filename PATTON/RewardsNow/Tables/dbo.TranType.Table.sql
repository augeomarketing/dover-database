USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[TranType]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TranType](
	[TranCode] [nvarchar](2) NOT NULL,
	[Description] [nvarchar](40) NULL,
	[IncDec] [nvarchar](1) NOT NULL,
	[CntAmtFxd] [nvarchar](1) NOT NULL,
	[Points] [float] NOT NULL,
	[Ratio] [float] NOT NULL,
	[TypeCode] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_TranType] PRIMARY KEY NONCLUSTERED 
(
	[TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [idx_TranType_TranCode] ON [dbo].[TranType] 
(
	[TranCode] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_TranType_RatioPoints] ON [dbo].[TranType] 
(
	[Points] ASC,
	[Ratio] ASC
)WITH FILLFACTOR = 90 ON [PRIMARY]
GO
