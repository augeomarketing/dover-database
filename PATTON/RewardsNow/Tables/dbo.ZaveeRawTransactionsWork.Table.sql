USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRawTransactionsWork]    Script Date: 04/04/2013 14:35:44 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeRawTransactionsWork]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeRawTransactionsWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeRawTransactionsWork]    Script Date: 04/04/2013 14:35:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeRawTransactionsWork](
	[dim_ZaveeRawTransactions_FinancialInstituionID] [varchar](3) NULL,
	[dim_ZaveeRawTransactions_MemberID] [varchar](15) NULL,
	[dim_ZaveeRawTransactions_MemberCard] [varchar](16) NULL,
	[dim_ZaveeRawTransactions_TransactionId] [varchar](50) NULL,
	[dim_ZaveeRawTransactions_TransactionDate] [date] NULL,
	[dim_ZaveeRawTransactions_TransactionAmount] [numeric](16, 2) NULL,
	[dim_ZaveeRawTransactions_MerchantId] [varchar](50) NULL,
	[dim_ZaveeRawTransactions_MerchantName] [varchar](255) NULL,
	[dim_ZaveeRawTransactions_TranType] [varchar](1) NULL,
	[dim_ZaveeRawTransactions_DateAdded] [datetime] NULL,
	[dim_ZaveeRawTransactions_DateProcessed] [datetime] NULL
) ON [PRIMARY]

GO


