USE [RewardsNow]
GO

IF OBJECT_ID(N'RNITransactionAuxKey') IS NOT NULL
	DROP TABLE RNITransactionAuxKey
GO

CREATE TABLE RNITransactionAuxKey
(
	sid_rnitransactionauxkey_id INT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, dim_rnitransactionauxkey_name VARCHAR(20) NOT NULL
	, dim_rnitransactionauxkey_desc VARCHAR(255) NULL
)
GO

