USE [RewardsNow]
GO

IF OBJECT_ID(N'RNICustomerLegacyMapSource') IS NOT NULL
	DROP TABLE RNICustomerLegacyMapSource
GO

CREATE TABLE RNICustomerLegacyMapSource
(
	sid_dbprocessinfo_dbnumber VARCHAR(50)
	, sid_rnicustomerlegacymapsource_id INT IDENTITY(1,1)
	, dim_rnicustomerlegacymapsource_source VARCHAR(255)
)
GO

