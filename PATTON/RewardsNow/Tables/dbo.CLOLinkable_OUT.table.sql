USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkable_OUT]    Script Date: 07/31/2015 15:29:42 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOLinkable_OUT]') AND type in (N'U'))
DROP TABLE [dbo].[CLOLinkable_OUT]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkable_OUT]    Script Date: 07/31/2015 15:29:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOLinkable_OUT](
	[sid_CLOLinkable_OUT_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_CLOLinkable_id] [bigint] NOT NULL,
	[AccountToken] [varchar](255) NULL,
	[SettlementId] [varchar](255) NULL,
	[SettlementAmount] [decimal](18, 2) NULL,
	[PurchaseTransactionID] [varchar](255) NULL,
	[PurchaseTransactionTimestamp] [varchar](255) NULL,
	[dim_RNITransaction_TipPrefix] [varchar](3) NOT NULL,
	[dim_RNITransaction_RNIId] [varchar](15) NOT NULL,
	[dim_RNITransaction_ProcessingCode] [int] NOT NULL,
	[dim_RNITransaction_CardNumber] [varchar](16) NULL,
	[dim_RNITransaction_TransactionDate] [datetime] NULL,
	[dim_RNITransaction_TransactionCode] [int] NOT NULL,
	[dim_RNITransaction_EffectiveDate] [date] NULL,
	[dim_RNITransaction_TransactionAmount] [decimal](18, 2) NULL,
	[dim_RNITransaction_TransactionDescription] [varchar](255) NULL,
	[dim_RNITransaction_MerchantID] [varchar](50) NULL,
	[dim_RNITransaction_PointsAwarded] [int] NULL,
	[sid_trantype_trancode] [nvarchar](2) NULL,
	[sid_dbprocessinfo_dbnumber]  AS ([dim_RNITransaction_TipPrefix]),
	[sid_smstranstatus_id] [int] NOT NULL,
	[OutDate] [varchar](14) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


