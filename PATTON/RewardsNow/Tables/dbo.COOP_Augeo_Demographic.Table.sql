USE [RewardsNow]
GO
/****** Object:  Table [dbo].[COOP_Augeo_Demographic]    Script Date: 11/16/2015 11:41:43 ******/
DROP TABLE [dbo].[COOP_Augeo_Demographic]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COOP_Augeo_Demographic](
	[SEQ] [nchar](1) NULL,
	[Data] [nchar](506) NULL
) ON [PRIMARY]
GO
