USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeCust__dim_Z__20CF3021]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeCustomer] DROP CONSTRAINT [DF__ZaveeCust__dim_Z__20CF3021]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeCust__dim_Z__21C3545A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeCustomer] DROP CONSTRAINT [DF__ZaveeCust__dim_Z__21C3545A]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeCust__dim_Z__22B77893]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeCustomer] DROP CONSTRAINT [DF__ZaveeCust__dim_Z__22B77893]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomer]    Script Date: 03/27/2013 11:37:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeCustomer]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeCustomer]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeCustomer]    Script Date: 03/27/2013 11:37:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeCustomer](
	[sid_ZaveeCustomer_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_ZaveeCustomer_TipPrefix] [varchar](3) NOT NULL,
	[dim_ZaveeCustomer_Tipnumber] [varchar](15) NOT NULL,
	[dim_ZaveeCustomer_FirstName] [varchar](40) NULL,
	[dim_ZaveeCustomer_LastName] [varchar](40) NULL,
	[dim_ZaveeCustomer_FullName] [varchar](40) NULL,
	[dim_ZaveeCustomer_Address1] [varchar](40) NULL,
	[dim_ZaveeCustomer_Address2] [varchar](40) NULL,
	[dim_ZaveeCustomer_City] [varchar](40) NULL,
	[dim_ZaveeCustomer_State] [varchar](2) NULL,
	[dim_ZaveeCustomer_ZipCode] [varchar](15) NULL,
	[dim_ZaveeCustomer_EmailAddress] [varchar](50) NULL,
	[dim_ZaveeCustomer_EmailPref] [varchar](1) NULL,
	[dim_ZaveeCustomer_Status] [varchar](1) NOT NULL,
	[dim_ZaveeCustomer_DateAdded] [datetime] NOT NULL,
	[dim_ZaveeCustomer_DateLastModified] [datetime] NULL,
	[dim_ZaveeCustomer_DateProcessed] [datetime] NULL,
 CONSTRAINT [PK_ZaveeCustomer_ID] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeCustomer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeCustomer]    Script Date: 03/27/2013 11:37:09 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeCustomer] ON [dbo].[ZaveeCustomer] 
(
	[dim_ZaveeCustomer_FullName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeCustomer_Tip]    Script Date: 03/27/2013 11:37:09 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeCustomer_Tip] ON [dbo].[ZaveeCustomer] 
(
	[dim_ZaveeCustomer_Tipnumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZaveeCustomer] ADD  DEFAULT ('A') FOR [dim_ZaveeCustomer_Status]
GO

ALTER TABLE [dbo].[ZaveeCustomer] ADD  DEFAULT (getdate()) FOR [dim_ZaveeCustomer_DateAdded]
GO

ALTER TABLE [dbo].[ZaveeCustomer] ADD  DEFAULT (getdate()) FOR [dim_ZaveeCustomer_DateLastModified]
GO


