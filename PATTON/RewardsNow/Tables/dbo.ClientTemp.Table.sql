USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[ClientTemp]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClientTemp](
	[tipfirst] [nchar](3) NOT NULL,
	[clientname] [varchar](50) NULL,
	[DBNAMEONPATTON] [varchar](50) NULL,
	[PointExpirationYears] [varchar](1) NULL,
	[datejoined] [datetime] NULL,
	[EXPFREQ] [char](2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
