USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIStandardPoints]    Script Date: 5/26/2015 3:31:27 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIStandardPoints]') AND type in (N'U'))
DROP TABLE [dbo].[RNIStandardPoints]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RNIStandardPoints](
	[RNIStandardPointID] [int] IDENTITY(1,1) NOT NULL,
	[HistDate] [datetime] NOT NULL,
	[Points] [decimal](18, 0) NOT NULL,
	[TipNumber] [varchar](15) NOT NULL,
	[sid_rnitrancodegroup_id] [bigint] NOT NULL,
	[Ratio] [float] NULL,
	[DBNumber] [varchar](3) NULL,
	[InsertDate] [datetime] NULL,
 CONSTRAINT [PK_RNIStandardPoints] PRIMARY KEY CLUSTERED 
(
	[RNIStandardPointID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



