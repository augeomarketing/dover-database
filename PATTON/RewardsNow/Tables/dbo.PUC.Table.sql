USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PUC_dim_PUC_EmailSent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PUC] DROP CONSTRAINT [DF_PUC_dim_PUC_EmailSent]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[PUC]    Script Date: 03/24/2010 12:13:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PUC]') AND type in (N'U'))
DROP TABLE [dbo].[PUC]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[PUC]    Script Date: 03/24/2010 12:13:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PUC](
	[sid_PUC_id] [int] IDENTITY(1,1) NOT NULL,
	[dim_PUC_Tipnumber] [varchar](15) NULL,
	[dim_PUC_DateDeleted] [varchar](10) NULL,
	[dim_PUC_EmailMsg] [varchar](8000) NULL,
	[dim_PUC_FIContactEmail] [varchar](200) NULL,
	[dim_PUC_RNContactEmail] [varchar](200) NULL,
	[dim_PUC_EmailSent] [int] NULL,
 CONSTRAINT [PK_PUC] PRIMARY KEY CLUSTERED 
(
	[sid_PUC_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PUC] ADD  CONSTRAINT [DF_PUC_dim_PUC_EmailSent]  DEFAULT ((0)) FOR [dim_PUC_EmailSent]
GO


