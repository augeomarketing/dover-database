USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MerchantF__dim_M__45E31B44]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[MerchantFundedCustomerCardWork] DROP CONSTRAINT [DF__MerchantF__dim_M__45E31B44]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerCardWork]    Script Date: 08/06/2013 11:01:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MerchantFundedCustomerCardWork]') AND type in (N'U'))
DROP TABLE [dbo].[MerchantFundedCustomerCardWork]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[MerchantFundedCustomerCardWork]    Script Date: 08/06/2013 11:01:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MerchantFundedCustomerCardWork](
	[sid_MerchantFundedCustomerCardWork_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[dim_MerchantFundedCustomerCardWork_Tipnumber] [varchar](15) NOT NULL,
	[dim_MerchantFundedCustomerCardWork_HashedTipnumberCard] [varchar](32) NULL,
	[dim_MerchantFundedCustomerCardWork_DateAdded] [datetime] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MerchantFundedCustomerCardWork] ADD  DEFAULT (getdate()) FOR [dim_MerchantFundedCustomerCardWork_DateAdded]
GO


