USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaFileSequence]    Script Date: 02/27/2013 11:18:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VisaFileSequence]') AND type in (N'U'))
DROP TABLE [dbo].[VisaFileSequence]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaFileSequence]    Script Date: 02/27/2013 11:18:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VisaFileSequence](
	[VisaFileSequenceIdentity] [int] IDENTITY(1,1) NOT NULL,
	[LastSeqNumber] [int] NOT NULL,
	[RunDate] [date] NULL,
 CONSTRAINT [PK_VisaFileSequenceIdentity] PRIMARY KEY CLUSTERED 
(
	[VisaFileSequenceIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


