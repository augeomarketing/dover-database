USE [RewardsNow]
GO

/*

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_fiprocessingstepuserexit_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[fiprocessingstepuserexit]'))
ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [FK_fiprocessingstepuserexit_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_fiprocessingstepuserexit_processingstep]') AND parent_object_id = OBJECT_ID(N'[dbo].[fiprocessingstepuserexit]'))
ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [FK_fiprocessingstepuserexit_processingstep]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_fiprocessingstepuserexit_enabled]') AND parent_object_id = OBJECT_ID(N'[dbo].[fiprocessingstepuserexit]'))
ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [CK_fiprocessingstepuserexit_enabled]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_enabled]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_enabled]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_lastmodified]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[fiprocessingstepuserexit]    Script Date: 01/20/2011 16:13:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fiprocessingstepuserexit]') AND type in (N'U'))
DROP TABLE [dbo].[fiprocessingstepuserexit]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[fiprocessingstepuserexit]    Script Date: 01/20/2011 16:13:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[fiprocessingstepuserexit](
	[sid_processingstep_id] [bigint] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_processingstepuserexit_sequence] [bigint] NOT NULL,
	[dim_fiprocessingstepuserexit_enabled] [int] NOT NULL,
	[dim_fiprocessingstepuserexit_ssispackagename] [varchar](max) NOT NULL,
	[dim_fiprocessingstepuserexit_created] [datetime] NOT NULL,
	[dim_fiprocessingstepuserexit_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_fiprocessingstepuserexit] PRIMARY KEY CLUSTERED 
(
	[sid_processingstep_id] ASC,
	[sid_dbprocessinfo_dbnumber] ASC,
	[dim_processingstepuserexit_sequence] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[fiprocessingstepuserexit]  WITH CHECK ADD  CONSTRAINT [FK_fiprocessingstepuserexit_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] CHECK CONSTRAINT [FK_fiprocessingstepuserexit_dbprocessinfo]
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit]  WITH CHECK ADD  CONSTRAINT [FK_fiprocessingstepuserexit_processingstep] FOREIGN KEY([sid_processingstep_id])
REFERENCES [dbo].[processingstep] ([sid_processingstep_id])
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] CHECK CONSTRAINT [FK_fiprocessingstepuserexit_processingstep]
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit]  WITH CHECK ADD  CONSTRAINT [CK_fiprocessingstepuserexit_enabled] CHECK  (([dim_fiprocessingstepuserexit_enabled]=(1) OR [dim_fiprocessingstepuserexit_enabled]=(0)))
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] CHECK CONSTRAINT [CK_fiprocessingstepuserexit_enabled]
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] ADD  CONSTRAINT [DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_enabled]  DEFAULT ((1)) FOR [dim_fiprocessingstepuserexit_enabled]
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] ADD  CONSTRAINT [DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_created]  DEFAULT (getdate()) FOR [dim_fiprocessingstepuserexit_created]
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] ADD  CONSTRAINT [DF_fiprocessingstepuserexit_dim_fiprocessingstepuserexit_lastmodified]  DEFAULT (getdate()) FOR [dim_fiprocessingstepuserexit_lastmodified]
GO

*/

ALTER TABLE [dbo].[fiprocessingstepuserexit] DROP CONSTRAINT [PK_fiprocessingstepuserexit]
GO

ALTER TABLE [dbo].[fiprocessingstepuserexit] ADD dim_processingstepuserexit_steporder INT NOT NULL DEFAULT(1)
GO

UPDATE [dbo].[fiprocessingstepuserexit] SET dim_processingstepuserexit_steporder = 1

ALTER TABLE [dbo].[fiprocessingstepuserexit] 
ADD CONSTRAINT [PK_fiprocessingstepuserexit] 
	PRIMARY KEY 
	(
		[sid_processingstep_id] ASC
		, [sid_dbprocessinfo_dbnumber] ASC
		, [dim_processingstepuserexit_sequence] ASC
		, [dim_processingstepuserexit_steporder] ASC
	)
GO
