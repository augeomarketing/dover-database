USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactions_OutLog]    Script Date: 01/18/2016 16:26:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeTransactions_OutLog](
	[sid_ZaveeTransactions_OutLog_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NULL,
	[ProcessDate] [datetime] NULL,
	[DateUploadedToPlatform] [datetime] NULL,
	[DateAddedToDB] [datetime] NULL,
 CONSTRAINT [PK_ZaveeTransactions_OutLog] PRIMARY KEY CLUSTERED 
(
	[sid_ZaveeTransactions_OutLog_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZaveeTransactions_OutLog] ADD  DEFAULT (getdate()) FOR [ProcessDate]
GO

ALTER TABLE [dbo].[ZaveeTransactions_OutLog] ADD  DEFAULT ('1900-01-01') FOR [DateUploadedToPlatform]
GO

ALTER TABLE [dbo].[ZaveeTransactions_OutLog] ADD  DEFAULT ('1900-01-01') FOR [DateAddedToDB]
GO
