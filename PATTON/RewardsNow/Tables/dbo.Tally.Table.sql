USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Tally]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tally](
	[N] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_Tally_N] PRIMARY KEY CLUSTERED 
(
	[N] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
