USE [RewardsNow]
GO

IF OBJECT_ID(N'RNIMappedValue') IS NOT NULL
	DROP TABLE RNIMappedValue
GO

CREATE TABLE RNIMappedValue
(
	sid_rnimappedvalue_id BIGINT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, sid_rnimappedvaluetype_id INT
	, dim_rnimappedvalue_value VARCHAR(255)
)
GO

