USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaFilesPulled]    Script Date: 01/08/2013 14:54:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaFilesPulled]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaFilesPulled]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaFilesPulled]    Script Date: 01/08/2013 14:54:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaFilesPulled](
	[FilePulledIdentity] [int] IDENTITY(1,1) NOT NULL,
	[FileType] [varchar](50) NOT NULL,
	[FileDate] [date] NOT NULL,
	[RunDate] [datetime] NULL,
 CONSTRAINT [PK_FilePulled] PRIMARY KEY CLUSTERED 
(
	[FilePulledIdentity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


