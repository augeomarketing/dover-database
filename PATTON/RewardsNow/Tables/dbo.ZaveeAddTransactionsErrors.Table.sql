USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeAddT__dim_Z__1A39A330]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeAddTransactionsErrors] DROP CONSTRAINT [DF__ZaveeAddT__dim_Z__1A39A330]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeAddTransactionsErrors]    Script Date: 07/03/2013 11:36:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeAddTransactionsErrors]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeAddTransactionsErrors]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeAddTransactionsErrors]    Script Date: 07/03/2013 11:36:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeAddTransactionsErrors](
	[sid_ZaveeAddTransactionsErrors_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NULL,
	[dim_ZaveeAddTransactionsErrors_FinancialInstituionID] [varchar](3) NULL,
	[dim_ZaveeAddTransactionsErrors_MemberID] [varchar](15) NULL,
	[dim_ZaveeAddTransactionsErrors_MemberCard] [varchar](16) NULL,
	[dim_ZaveeAddTransactionsErrors_MerchantId] [varchar](16) NULL,
	[dim_ZaveeAddTransactionsErrors_MerchantName] [varchar](250) NULL,
	[dim_ZaveeAddTransactionsErrors_TransactionId] [varchar](50) NULL,
	[dim_ZaveeAddTransactionsErrors_TransactionDate] [date] NULL,
	[dim_ZaveeAddTransactionsErrors_TransactionAmount] [numeric](16, 2) NULL,
	[dim_ZaveeAddTransactionsErrors_AwardAmount] [numeric](16, 2) NULL,
	[dim_ZaveeAddTransactionsErrors_AwardPoints] [int] NULL,
	[dim_ZaveeAddTransactionsErrors_TranType] [varchar](1) NULL,
	[dim_ZaveeAddTransactionsErrors_DateAdded] [datetime] NULL,
	[dim_ZaveeAddTransactionsErrors_PendingDate] [datetime] NULL,
	[dim_ZaveeAddTransactionsErrors_ProcessedDate] [datetime] NULL,
	[dim_ZaveeAddTransactionsErrors_InvoicedDate] [datetime] NULL,
	[dim_ZaveeAddTransactionsErrors_PaidDate] [datetime] NULL,
	[dim_ZaveeAddTransactionsErrors_ErrorHoldDate] [datetime] NULL,
	[dim_ZaveeAddTransactionsErrors_Comments] [varchar](500) NULL,
	[sid_ZaveeTransactionStatus_id] [bigint] NULL
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeAddTransactionsErrors_MemberId]    Script Date: 07/03/2013 11:36:38 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeAddTransactionsErrors_MemberId] ON [dbo].[ZaveeAddTransactionsErrors] 
(
	[dim_ZaveeAddTransactionsErrors_MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZaveeAddTransactionsErrors] ADD  DEFAULT (getdate()) FOR [dim_ZaveeAddTransactionsErrors_DateAdded]
GO


