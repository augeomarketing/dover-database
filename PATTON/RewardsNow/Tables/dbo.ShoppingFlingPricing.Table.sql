USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ShoppingF__dim_S__5BEF2A98]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ShoppingFlingPricing] DROP CONSTRAINT [DF__ShoppingF__dim_S__5BEF2A98]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ShoppingF__dim_S__5CE34ED1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ShoppingFlingPricing] DROP CONSTRAINT [DF__ShoppingF__dim_S__5CE34ED1]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingPricing]    Script Date: 11/22/2011 13:22:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ShoppingFlingPricing]') AND type in (N'U'))
DROP TABLE [dbo].[ShoppingFlingPricing]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ShoppingFlingPricing]    Script Date: 11/22/2011 13:22:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ShoppingFlingPricing](
	[Sid_ShoppingFlingPricing_Identity] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[dim_ShoppingFlingPricing_TIP] [varchar](3) NULL,
	[dim_ShoppingFlingPricing_RNI] [decimal](5, 2) NULL,
	[dim_ShoppingFlingPricing_Client] [decimal](5, 2) NULL,
	[dim_ShoppingFlingPricing_Cartera] [decimal](5, 2) NULL,
	[dim_ShoppingFlingPricing_4thParty] [decimal](5, 2) NULL,
	[dim_ShoppingFlingPricing_5thParty] [decimal](5, 2) NULL,
	[dim_ShoppingFlingPricing_TierBegin] [numeric](16, 0) NULL,
	[dim_ShoppingFlingPricing_TierEnd] [numeric](16, 0) NULL,
	[dim_ShoppingFlingPricing_CreateDate] [datetime] NULL,
	[dim_ShoppingFlingPricing_UpdateDate] [datetime] NULL,
	[dim_ShoppingFlingPricing_ReportGrouping] [int] NULL,
	[dim_ShoppingFlingPricing_Comments] [varchar](250) NULL,
 CONSTRAINT [Sid_ShoppingFlingPricing_Identity] PRIMARY KEY CLUSTERED 
(
	[Sid_ShoppingFlingPricing_Identity] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ShoppingFlingPricing] ADD  DEFAULT (getdate()) FOR [dim_ShoppingFlingPricing_CreateDate]
GO

ALTER TABLE [dbo].[ShoppingFlingPricing] ADD  DEFAULT (getdate()) FOR [dim_ShoppingFlingPricing_UpdateDate]
GO


