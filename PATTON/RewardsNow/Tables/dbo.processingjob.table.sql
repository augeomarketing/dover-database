USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_processingjob_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[processingjob]'))
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [FK_processingjob_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_processingjob_processingjobstatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[processingjob]'))
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [FK_processingjob_processingjobstatus]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_processingjob_processingstep]') AND parent_object_id = OBJECT_ID(N'[dbo].[processingjob]'))
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [FK_processingjob_processingstep]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_processingjob_stepparameterstartenddate]') AND parent_object_id = OBJECT_ID(N'[dbo].[processingjob]'))
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [CK_processingjob_stepparameterstartenddate]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_processingjob_sid_processingjobstatus_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [DF_processingjob_sid_processingjobstatus_id]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_processingjob_dim_processingjob_created]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [DF_processingjob_dim_processingjob_created]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_processingjob_dim_processingjob_lastmodified]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[processingjob] DROP CONSTRAINT [DF_processingjob_dim_processingjob_lastmodified]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingjob]    Script Date: 01/20/2011 16:12:43 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[processingjob]') AND type in (N'U'))
DROP TABLE [dbo].[processingjob]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[processingjob]    Script Date: 01/20/2011 16:12:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[processingjob](
	[sid_processingjob_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_processingstep_id] [bigint] NOT NULL,
	[sid_dbprocessinfo_dbnumber] [varchar](50) NOT NULL,
	[dim_processingjob_stepparameterstartdate] [date] NOT NULL,
	[dim_processingjob_stepparameterenddate] [date] NOT NULL,
	[dim_processingjob_jobstartdate] [datetime] NULL,
	[dim_processingjob_jobcompletiondate] [datetime] NULL,
	[sid_processingjobstatus_id] [bigint] NOT NULL,
	[dim_processingjob_created] [datetime] NOT NULL,
	[dim_processingjob_lastmodified] [datetime] NOT NULL,
 CONSTRAINT [PK_processingjob] PRIMARY KEY CLUSTERED 
(
	[sid_processingjob_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[processingjob]  WITH CHECK ADD  CONSTRAINT [FK_processingjob_dbprocessinfo] FOREIGN KEY([sid_dbprocessinfo_dbnumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[processingjob] CHECK CONSTRAINT [FK_processingjob_dbprocessinfo]
GO

ALTER TABLE [dbo].[processingjob]  WITH CHECK ADD  CONSTRAINT [FK_processingjob_processingjobstatus] FOREIGN KEY([sid_processingjobstatus_id])
REFERENCES [dbo].[processingjobstatus] ([sid_processingjobstatus_id])
GO

ALTER TABLE [dbo].[processingjob] CHECK CONSTRAINT [FK_processingjob_processingjobstatus]
GO

ALTER TABLE [dbo].[processingjob]  WITH CHECK ADD  CONSTRAINT [FK_processingjob_processingstep] FOREIGN KEY([sid_processingstep_id])
REFERENCES [dbo].[processingstep] ([sid_processingstep_id])
GO

ALTER TABLE [dbo].[processingjob] CHECK CONSTRAINT [FK_processingjob_processingstep]
GO

ALTER TABLE [dbo].[processingjob]  WITH CHECK ADD  CONSTRAINT [CK_processingjob_stepparameterstartenddate] CHECK  (([dim_processingjob_stepparameterenddate]>=[dim_processingjob_stepparameterstartdate]))
GO

ALTER TABLE [dbo].[processingjob] CHECK CONSTRAINT [CK_processingjob_stepparameterstartenddate]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'check end date is >= start date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'processingjob', @level2type=N'CONSTRAINT',@level2name=N'CK_processingjob_stepparameterstartenddate'
GO

ALTER TABLE [dbo].[processingjob] ADD  CONSTRAINT [DF_processingjob_sid_processingjobstatus_id]  DEFAULT ((1)) FOR [sid_processingjobstatus_id]
GO

ALTER TABLE [dbo].[processingjob] ADD  CONSTRAINT [DF_processingjob_dim_processingjob_created]  DEFAULT (getdate()) FOR [dim_processingjob_created]
GO

ALTER TABLE [dbo].[processingjob] ADD  CONSTRAINT [DF_processingjob_dim_processingjob_lastmodified]  DEFAULT (getdate()) FOR [dim_processingjob_lastmodified]
GO

