USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[Current_Month_Activity_wrk]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Current_Month_Activity_wrk](
	[tipnumber] [varchar](15) NOT NULL,
	[points] [decimal](38, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
