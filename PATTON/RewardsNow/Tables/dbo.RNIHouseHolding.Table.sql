USE [RewardsNow]
GO

IF OBJECT_ID(N'RNIHouseholding') IS NOT NULL
	DROP TABLE RNIHouseholding
GO

CREATE TABLE RNIHouseholding
(
	sid_dbprocessinfo_dbnumber VARCHAR(10) NOT NULL
	, dim_rnihouseholding_precedence INT NOT NULL
	, dim_rnihouseholding_field01 VARCHAR(50) NOT NULL
	, dim_rnihouseholding_field02 VARCHAR(50)
	, dim_rnihouseholding_field03 VARCHAR(50)
	, dim_rnihouseholding_field04 VARCHAR(50)
	, CONSTRAINT pk_RNIHouseholding__dbnumber_precedence PRIMARY KEY (sid_dbprocessinfo_dbnumber, dim_rnihouseholding_precedence)
)
GO
