USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[RptTrvlSumm]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RptTrvlSumm](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[CertValue] [decimal](12, 3) NOT NULL,
	[PointVal] [int] NOT NULL,
	[NumAccounts] [int] NOT NULL,
	[RunDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
