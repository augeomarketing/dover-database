USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramType_dim_BonusProgramType_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_BonusProgramType_dim_BonusProgramType_LastUpdated]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RNIBonusProgramType] DROP CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramType]    Script Date: 08/09/2011 17:30:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RNIBonusProgramType]') AND type in (N'U'))
DROP TABLE [dbo].[RNIBonusProgramType]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[RNIBonusProgramType]    Script Date: 08/09/2011 17:30:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RNIBonusProgramType](
	[sid_rnibonusprogramtype_code] [varchar](1) NOT NULL,
	[dim_rnibonusprogramtype_description] [varchar](255) NOT NULL,
	[dim_rnibonusprogramtype_dateadded] [datetime] NOT NULL,
	[dim_rnibonusprogramtype_lastupdated] [datetime] NOT NULL,
 CONSTRAINT [PK_RNIBonusProgramType] PRIMARY KEY CLUSTERED 
(
	[sid_rnibonusprogramtype_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Trigger [dbo].[truRNIBonusProgramType_LastUpdated]    Script Date: 08/09/2011 17:30:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

   
CREATE TRIGGER [dbo].[truRNIBonusProgramType_LastUpdated]
   ON  [dbo].[RNIBonusProgramType]
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;

	update bpt
		set dim_RNIBonusProgramType_LastUpdated = getdate()
	from inserted ins join dbo.RNIBonusProgramType bpt
		on ins.sid_RNIBonusProgramType_code = bpt.sid_RNIBonusProgramType_code

END


GO

ALTER TABLE [dbo].[RNIBonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_DateAdded]  DEFAULT (getdate()) FOR [dim_rnibonusprogramtype_dateadded]
GO

ALTER TABLE [dbo].[RNIBonusProgramType] ADD  CONSTRAINT [DF_BonusProgramType_dim_BonusProgramType_LastUpdated]  DEFAULT (getdate()) FOR [dim_rnibonusprogramtype_lastupdated]
GO

