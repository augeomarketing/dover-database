USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbprocessinfo_dbprocessinfo]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [FK_dbprocessinfo_dbprocessinfo]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbprocessinfo_editaddress]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [FK_dbprocessinfo_editaddress]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbprocessinfo_FiProdStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [FK_dbprocessinfo_FiProdStatus]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_dbprocessinfo_ExtractCashBack]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [CK_dbprocessinfo_ExtractCashBack]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_dbprocessinfo_hasonlinebooking]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [CK_dbprocessinfo_hasonlinebooking]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_dbprocessinfo_ParseFirstName]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [CK_dbprocessinfo_ParseFirstName]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_dbprocessinfo_VGC]') AND parent_object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]'))
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [CK_dbprocessinfo_VGC]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_GenerateWelcomeKit]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_GenerateWelcomeKit]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_IsStageModel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_IsStageModel]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_ExtractGiftCards]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_ExtractGiftCards]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_ExtractCashBack]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_ExtractCashBack]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_TransferHistToRn1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_TransferHistToRn1]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_CalcDailyExpire]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_CalcDailyExpire]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__dbprocess__hason__6A86975B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF__dbprocess__hason__6A86975B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_VesdiaParticipant]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_VesdiaParticipant]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_AccessDevelopment]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_AccessDevelopment]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_ExpPointsDisplayPeriodOffset]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_ExpPointsDisplayPeriodOffset]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_ExpPointsDisplay]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_ExpPointsDisplay]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_sid_editaddress_id]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_sid_editaddress_id]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_SSOExactMatch]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_SSOExactMatch]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_ShoppingFlingExportFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_ShoppingFlingExportFile]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__dbprocess__MDTAu__1F460F0B]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF__dbprocess__MDTAu__1F460F0B]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_dbprocessinfo_ParseFirstName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[dbprocessinfo] DROP CONSTRAINT [DF_dbprocessinfo_ParseFirstName]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[dbprocessinfo]    Script Date: 07/10/2012 10:09:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[dbprocessinfo]') AND type in (N'U'))
DROP TABLE [dbo].[dbprocessinfo]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[dbprocessinfo]    Script Date: 07/10/2012 10:09:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[dbprocessinfo](
	[DBNumber] [varchar](50) NOT NULL,
	[DBNamePatton] [varchar](50) NULL,
	[DBNameNEXL] [varchar](50) NULL,
	[DBAvailable] [char](1) NOT NULL,
	[ClientCode] [varchar](50) NULL,
	[ClientName] [varchar](256) NULL,
	[ProgramName] [varchar](256) NULL,
	[DBLocationPatton] [varchar](50) NULL,
	[DBLocationNexl] [varchar](50) NULL,
	[PointExpirationYears] [int] NULL,
	[DateJoined] [datetime] NULL,
	[MinRedeemNeeded] [int] NULL,
	[MaxPointsPerYear] [numeric](18, 0) NULL,
	[LastTipNumberUsed] [varchar](15) NULL,
	[PointsExpireFrequencyCd] [varchar](2) NULL,
	[ClosedMonths] [int] NULL,
	[WelcomeKitGroupName] [varchar](50) NULL,
	[GenerateWelcomeKit] [varchar](1) NOT NULL,
	[sid_FiProdStatus_statuscode] [varchar](1) NULL,
	[IsStageModel] [int] NULL,
	[ExtractGiftCards] [varchar](1) NOT NULL,
	[ExtractCashBack] [varchar](1) NOT NULL,
	[RNProgramName] [varchar](50) NULL,
	[PointsUpdated] [smalldatetime] NULL,
	[TravelMinimum] [int] NULL,
	[TravelBottom] [int] NULL,
	[CashBackMinimum] [int] NULL,
	[Merch] [int] NULL,
	[AirFee] [numeric](18, 0) NULL,
	[Logo] [varchar](50) NULL,
	[Landing] [varchar](255) NULL,
	[TermsPage] [varchar](50) NULL,
	[FaqPage] [varchar](20) NULL,
	[EarnPage] [varchar](20) NULL,
	[Business] [varchar](1) NULL,
	[StatementDefault] [int] NULL,
	[StatementNum] [int] NULL,
	[CustomerServicePhone] [varchar](50) NULL,
	[ContactPerson1] [varchar](50) NULL,
	[ContactPerson2] [varchar](50) NULL,
	[ContactPhone1] [varchar](12) NULL,
	[ContactPhone2] [varchar](12) NULL,
	[ContactEmail1] [varchar](50) NULL,
	[ContactEmail2] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[ServerName] [varchar](50) NULL,
	[TransferHistToRn1] [varchar](1) NOT NULL,
	[CalcDailyExpire] [varchar](1) NOT NULL,
	[hasonlinebooking] [int] NOT NULL,
	[VesdiaParticipant] [varchar](1) NOT NULL,
	[AccessDevParticipant] [varchar](1) NOT NULL,
	[ExpPointsDisplayPeriodOffset] [int] NULL,
	[ExpPointsDisplay] [int] NULL,
	[sid_editaddress_id] [int] NOT NULL,
	[SSOExactMatch] [int] NOT NULL,
	[ShoppingFlingExportFile] [varchar](1) NULL,
	[MDTAutoUpdateLiabilitySent] [varchar](1) NOT NULL,
	[ParseFirstName] [varchar](1) NOT NULL,
 CONSTRAINT [PK_dbprocessinfo_1] PRIMARY KEY CLUSTERED 
(
	[DBNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IDX_dbnumber_dbnamepatton]    Script Date: 07/10/2012 10:09:29 ******/
CREATE NONCLUSTERED INDEX [IDX_dbnumber_dbnamepatton] ON [dbo].[dbprocessinfo] 
(
	[DBNumber] ASC,
	[DBNamePatton] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'this is the TPM name' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'dbprocessinfo', @level2type=N'COLUMN',@level2name=N'RNProgramName'
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH CHECK ADD  CONSTRAINT [FK_dbprocessinfo_dbprocessinfo] FOREIGN KEY([DBNumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [FK_dbprocessinfo_dbprocessinfo]
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH CHECK ADD  CONSTRAINT [FK_dbprocessinfo_editaddress] FOREIGN KEY([sid_editaddress_id])
REFERENCES [dbo].[editaddress] ([sid_editaddress_id])
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [FK_dbprocessinfo_editaddress]
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH NOCHECK ADD  CONSTRAINT [FK_dbprocessinfo_FiProdStatus] FOREIGN KEY([sid_FiProdStatus_statuscode])
REFERENCES [dbo].[FiProdStatus] ([sid_FiProdStatus_statuscode])
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [FK_dbprocessinfo_FiProdStatus]
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH NOCHECK ADD  CONSTRAINT [CK_dbprocessinfo_ExtractCashBack] CHECK  (([ExtractCashBack]='N' OR [ExtractCashBack]='Y'))
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_ExtractCashBack]
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH CHECK ADD  CONSTRAINT [CK_dbprocessinfo_hasonlinebooking] CHECK  (([hasonlinebooking]=(1) OR [hasonlinebooking]=(0)))
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_hasonlinebooking]
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH CHECK ADD  CONSTRAINT [CK_dbprocessinfo_ParseFirstName] CHECK  (([ParseFirstName]='N' OR [ParseFirstName]='Y'))
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_ParseFirstName]
GO

ALTER TABLE [dbo].[dbprocessinfo]  WITH NOCHECK ADD  CONSTRAINT [CK_dbprocessinfo_VGC] CHECK  (([ExtractGiftCards]='N' OR [ExtractGiftCards]='Y'))
GO

ALTER TABLE [dbo].[dbprocessinfo] CHECK CONSTRAINT [CK_dbprocessinfo_VGC]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_GenerateWelcomeKit]  DEFAULT ('Y') FOR [GenerateWelcomeKit]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_IsStageModel]  DEFAULT ((0)) FOR [IsStageModel]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ExtractGiftCards]  DEFAULT ('N') FOR [ExtractGiftCards]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ExtractCashBack]  DEFAULT ('N') FOR [ExtractCashBack]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_TransferHistToRn1]  DEFAULT ('N') FOR [TransferHistToRn1]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_CalcDailyExpire]  DEFAULT ('N') FOR [CalcDailyExpire]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF__dbprocess__hason__6A86975B]  DEFAULT ((0)) FOR [hasonlinebooking]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_VesdiaParticipant]  DEFAULT ('N') FOR [VesdiaParticipant]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_AccessDevelopment]  DEFAULT ('N') FOR [AccessDevParticipant]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ExpPointsDisplayPeriodOffset]  DEFAULT ((0)) FOR [ExpPointsDisplayPeriodOffset]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ExpPointsDisplay]  DEFAULT ((0)) FOR [ExpPointsDisplay]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_sid_editaddress_id]  DEFAULT ((1)) FOR [sid_editaddress_id]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_SSOExactMatch]  DEFAULT ((0)) FOR [SSOExactMatch]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ShoppingFlingExportFile]  DEFAULT ('N') FOR [ShoppingFlingExportFile]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF__dbprocess__MDTAu__1F460F0B]  DEFAULT ('Y') FOR [MDTAutoUpdateLiabilitySent]
GO

ALTER TABLE [dbo].[dbprocessinfo] ADD  CONSTRAINT [DF_dbprocessinfo_ParseFirstName]  DEFAULT ('N') FOR [ParseFirstName]
GO

