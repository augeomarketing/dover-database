USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[BONUSDISTRIBUTION]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BONUSDISTRIBUTION](
	[ClientID] [char](3) NOT NULL,
	[Yr] [char](4) NOT NULL,
	[Mo] [char](5) NOT NULL,
	[MonthAsStr] [varchar](10) NULL,
	[BonusDelta] [numeric](18, 0) NOT NULL,
	[BonusBA] [numeric](18, 0) NOT NULL,
	[BonusBC] [numeric](18, 0) NOT NULL,
	[BonusBE] [numeric](18, 0) NOT NULL,
	[BonusBF] [numeric](18, 0) NOT NULL,
	[BonusBI] [numeric](18, 0) NOT NULL,
	[BonusBM] [numeric](18, 0) NOT NULL,
	[BonusBN] [numeric](18, 0) NOT NULL,
	[BonusBR] [numeric](18, 0) NOT NULL,
	[BonusBT] [numeric](18, 0) NOT NULL,
	[BonusNW] [numeric](18, 0) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
