USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOLinkableAccessToken]    Script Date: 01/18/2016 09:44:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOLinkableAccessToken](
	[sid_CLOLinkableAccessToken_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Tipnumber] [varchar](15) NOT NULL,
	[ConsumerToken] [varchar](255) NOT NULL,
	[AccessToken] [varchar](255) NOT NULL,
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


