USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualDetail]    Script Date: 12/14/2010 10:40:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrualDetail]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrualDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrualDetail]    Script Date: 12/14/2010 10:40:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrualDetail](
	[RecordType] [varchar](2) NOT NULL,
	[TranID] [varchar](14) NULL,
	[TranDt] [varchar](8) NOT NULL,
	[TranTime] [varchar](7) NULL,
	[CardNum] [varchar](32) NULL,
	[MemberID] [varchar](15) NOT NULL,
	[Tran_amt] [decimal](16, 4) NOT NULL,
	[CurrencyCode] [varchar](10) NULL,
	[ReferenceNum] [varchar](16) NULL,
	[MerchantID] [varchar](30) NULL,
	[MerchantName] [varchar](60) NULL,
	[TranType] [varchar](30) NOT NULL,
	[MemberReward] [numeric](16, 0) NOT NULL
) ON [PRIMARY]

GO


