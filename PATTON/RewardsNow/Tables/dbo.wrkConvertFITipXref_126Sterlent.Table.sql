USE [Rewardsnow]
GO
/****** Object:  Table [dbo].[wrkConvertFITipXref_126Sterlent]    Script Date: 09/30/2009 14:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wrkConvertFITipXref_126Sterlent](
	[OldTipNumber] [varchar](15) NOT NULL,
	[NewTipNumber] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
