USE [RewardsNow]
GO

IF OBJECT_ID(N'stmtExclusion') IS NOT NULL
	DROP TABLE stmtExclusion
GO

CREATE TABLE stmtExclusion
(
	sid_stmtexclusion_id INT IDENTITY(1,1) PRIMARY KEY
	, sid_stmtreportdefinition_id INT
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
	, dim_stmtexclusion_function VARCHAR(255)
	, dim_stmtexclusion_active BIT NOT NULL DEFAULT(1)
)
GO


