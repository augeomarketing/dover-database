USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrual]    Script Date: 11/04/2010 12:32:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VesdiaAccrual]') AND type in (N'U'))
DROP TABLE [dbo].[VesdiaAccrual]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VesdiaAccrual]    Script Date: 11/04/2010 12:32:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VesdiaAccrual](
	[Column0] [varchar](250) NULL,
	[Column1] [varchar](250) NULL,
	[Column2] [varchar](250) NULL,
	[Column3] [varchar](250) NULL,
	[Column4] [varchar](250) NULL,
	[Column5] [varchar](250) NULL,
	[Column6] [varchar](250) NULL,
	[Column7] [varchar](250) NULL,
	[Column8] [varchar](250) NULL,
	[Column9] [varchar](250) NULL,
	[Column10] [varchar](250) NULL,
	[Column11] [varchar](250) NULL,
	[Column12] [varchar](250) NULL
) ON [PRIMARY]

GO


