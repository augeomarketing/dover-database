USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__0EFCFAAE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__0EFCFAAE]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__0FF11EE7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__0FF11EE7]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__10E54320]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__10E54320]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__11D96759]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__11D96759]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__12CD8B92]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__12CD8B92]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__13C1AFCB]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__13C1AFCB]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ZaveeTran__dim_Z__6C5DC9E7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ZaveeTransactions] DROP CONSTRAINT [DF__ZaveeTran__dim_Z__6C5DC9E7]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactions]    Script Date: 10/16/2013 10:14:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ZaveeTransactions]') AND type in (N'U'))
DROP TABLE [dbo].[ZaveeTransactions]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[ZaveeTransactions]    Script Date: 10/16/2013 10:14:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZaveeTransactions](
	[sid_ZaveeTransactions_Identity] [bigint] IDENTITY(1,1) NOT NULL,
	[sid_RNITransaction_ID] [bigint] NULL,
	[dim_ZaveeTransactions_FinancialInstituionID] [varchar](3) NULL,
	[dim_ZaveeTransactions_MemberID] [varchar](15) NULL,
	[dim_ZaveeTransactions_MemberCard] [varchar](16) NULL,
	[dim_ZaveeTransactions_MerchantId] [varchar](16) NULL,
	[dim_ZaveeTransactions_MerchantName] [varchar](250) NULL,
	[dim_ZaveeTransactions_TransactionId] [varchar](50) NULL,
	[dim_ZaveeTransactions_TransactionDate] [date] NULL,
	[dim_ZaveeTransactions_TransactionAmount] [numeric](16, 2) NULL,
	[dim_ZaveeTransactions_AwardAmount] [numeric](16, 2) NULL,
	[dim_ZaveeTransactions_AwardPoints] [int] NULL,
	[dim_ZaveeTransactions_TranType] [varchar](1) NULL,
	[dim_ZaveeTransactions_DateAdded] [datetime] NULL,
	[dim_ZaveeTransactions_PendingDate] [datetime] NULL,
	[dim_ZaveeTransactions_ProcessedDate] [datetime] NULL,
	[dim_ZaveeTransactions_InvoicedDate] [datetime] NULL,
	[dim_ZaveeTransactions_PaidDate] [datetime] NULL,
	[dim_ZaveeTransactions_ErrorHoldDate] [datetime] NULL,
	[dim_ZaveeTransactions_Comments] [varchar](500) NULL,
	[sid_ZaveeTransactionStatus_id] [bigint] NULL,
	[dim_ZaveeTransactions_CancelledDate] [datetime] NULL,
	[dim_ZaveeTransactions_AgentName] [varchar](32) NULL,
	[dim_ZaveeTransactions_RNIRebate] [numeric](16, 4) NULL,
	[dim_ZaveeTransactions_ZaveeRebate] [numeric](16, 4) NULL,
	[dim_ZaveeTransactions_ClientRebate] [numeric](16, 4) NULL,
	[dim_ZaveeTransactions_EmailSent] [datetime] NULL
) ON [PRIMARY]

GO


USE [RewardsNow]
/****** Object:  Index [IX_ZaveeTransactions_MemberId]    Script Date: 10/16/2013 10:14:16 ******/
CREATE NONCLUSTERED INDEX [IX_ZaveeTransactions_MemberId] ON [dbo].[ZaveeTransactions] 
(
	[dim_ZaveeTransactions_MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 99) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT (getdate()) FOR [dim_ZaveeTransactions_DateAdded]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_ZaveeTransactions_PendingDate]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_ZaveeTransactions_ProcessedDate]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_ZaveeTransactions_InvoicedDate]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_ZaveeTransactions_PaidDate]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_ZaveeTransactions_ErrorHoldDate]
GO

ALTER TABLE [dbo].[ZaveeTransactions] ADD  DEFAULT ('1900-01-01') FOR [dim_ZaveeTransactions_EmailSent]
GO


