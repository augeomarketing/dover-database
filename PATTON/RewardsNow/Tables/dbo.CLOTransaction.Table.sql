USE [RewardsNow]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_CLOTransaction_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CLOTransaction] DROP CONSTRAINT [DF_CLOTransaction_DateAdded]
END

GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOTransaction]    Script Date: 07/31/2015 16:03:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CLOTransaction]') AND type in (N'U'))
DROP TABLE [dbo].[CLOTransaction]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CLOTransaction]    Script Date: 07/31/2015 16:03:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CLOTransaction](
	[sid_CLOTransaction_ID] [bigint] IDENTITY(1000000000000,1) NOT NULL,
	[MemberID] [varchar](20) NULL,
	[TranAmount] [varchar](10) NULL,
	[MerchantId] [varchar](50) NULL,
	[MerchantName] [varchar](200) NULL,
	[TransferCard] [varchar](25) NULL,
	[TranDate] [varchar](10) NULL,
	[CardBin] [varchar](6) NULL,
	[Last4] [varchar](4) NULL,
	[SICcode] [varchar](4) NULL,
	[MerchantStreet] [varchar](100) NULL,
	[MerchantCity] [varchar](50) NULL,
	[MerchantState] [varchar](2) NULL,
	[MerchantZipCode] [varchar](10) NULL,
	[CategoryCode] [varchar](10) NULL,
	[ExtTransactionID] [varchar](42) NULL,
	[TranType] [varchar](2) NULL,
	[TranSource] [varchar](2) NULL,
	[DateAdded] [datetime] NULL,
	[sid_CLOFileHistory_ID] [int] NULL,
	[RowNum] [int] NULL,
	[sid_smstranstatus_id] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[CLOTransaction] ADD  CONSTRAINT [DF_CLOTransaction_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO


