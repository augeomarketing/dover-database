USE [RewardsNow]
GO



/****** Object:  Table [dbo].[CentralProcessingStagingValidationResults]    Script Date: 10/29/2012 09:54:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CentralProcessingStagingValidationResults]') AND type in (N'U'))
DROP TABLE [dbo].[CentralProcessingStagingValidationResults]
GO

/****** Object:  Table [dbo].[CentralProcessingStagingValidationResults]    Script Date: 03/11/2015 15:44:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


CREATE TABLE [dbo].[CentralProcessingStagingValidationResults](
	[CentralProcessingStagingValidationResultsId] [int] IDENTITY(1,1) NOT NULL,
	[CentralProcessingStagingValidationTypesId] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DBNumber] [varchar](50) NOT NULL,
	[ProcessedValue] [int] NULL,
	[ValidationError] [bit] NOT NULL,
	[MonthEndDate] [datetime] NULL,
	[VarianceValue] [int] NULL,
 CONSTRAINT [PK_CentralProcessingStagingValidationResults] PRIMARY KEY CLUSTERED 
(
	[CentralProcessingStagingValidationResultsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[CentralProcessingStagingValidationResults]  WITH CHECK ADD  CONSTRAINT [FK_CentralProcessingStagingValidationResults_CentralProcessingStagingValidationTypes] FOREIGN KEY([CentralProcessingStagingValidationTypesId])
REFERENCES [dbo].[CentralProcessingStagingValidationTypes] ([CentralProcessingStagingValidationTypesId])
GO

ALTER TABLE [dbo].[CentralProcessingStagingValidationResults] CHECK CONSTRAINT [FK_CentralProcessingStagingValidationResults_CentralProcessingStagingValidationTypes]
GO

ALTER TABLE [dbo].[CentralProcessingStagingValidationResults]  WITH CHECK ADD  CONSTRAINT [FK_CentralProcessingStagingValidationResults_dbprocessinfo] FOREIGN KEY([DBNumber])
REFERENCES [dbo].[dbprocessinfo] ([DBNumber])
GO

ALTER TABLE [dbo].[CentralProcessingStagingValidationResults] CHECK CONSTRAINT [FK_CentralProcessingStagingValidationResults_dbprocessinfo]
GO

ALTER TABLE [dbo].[CentralProcessingStagingValidationResults] ADD  CONSTRAINT [DF_CentralProcessingStagingValidationResults_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO

ALTER TABLE [dbo].[CentralProcessingStagingValidationResults] ADD  CONSTRAINT [DF_CentralProcessingStagingValidationResults_ValidationError]  DEFAULT ((0)) FOR [ValidationError]
GO



