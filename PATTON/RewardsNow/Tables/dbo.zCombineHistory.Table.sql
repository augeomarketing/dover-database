USE [RewardsNOW]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_zCombineHistory_ErrDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[zCombineHistory] DROP CONSTRAINT [DF_zCombineHistory_ErrDate]
END

GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[zCombineHistory]    Script Date: 04/01/2010 15:06:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zCombineHistory]') AND type in (N'U'))
DROP TABLE [dbo].[zCombineHistory]
GO

USE [RewardsNOW]
GO

/****** Object:  Table [dbo].[zCombineHistory]    Script Date: 04/01/2010 15:06:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[zCombineHistory](
	[TipPri] [varchar](15) NOT NULL,
	[TipSec] [varchar](15) NULL,
	[transid] [varchar](50) NULL,
	[CombineDate] [datetime] NULL,
 CONSTRAINT [PK_zCombineHistory] PRIMARY KEY CLUSTERED 
(
	[TipPri] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[zCombineHistory] ADD  CONSTRAINT [DF_zCombineHistory_ErrDate]  DEFAULT (getdate()) FOR [CombineDate]
GO


