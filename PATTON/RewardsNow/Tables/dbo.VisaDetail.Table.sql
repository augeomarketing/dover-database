USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaDetail]    Script Date: 02/27/2013 11:18:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VisaDetail]') AND type in (N'U'))
DROP TABLE [dbo].[VisaDetail]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[VisaDetail]    Script Date: 02/27/2013 11:18:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VisaDetail](
	[RecordType] [char](1) NULL,
	[Action] [char](6) NULL,
	[VISAFeatureSelect] [char](8) NULL,
	[AccountNumber] [char](19) NULL,
	[AccountStatus] [char](2) NULL,
	[ReplacedAcctNbr] [char](19) NULL,
	[ZipCode] [char](5) NULL,
	[SSN] [char](4) NULL,
	[NamePrefix] [char](5) NULL,
	[FirstName] [char](15) NULL,
	[MiddleInitial] [char](1) NULL,
	[LastName] [char](25) NULL,
	[NameSuffix] [char](5) NULL,
	[CompanyName] [char](40) NULL,
	[Address1] [char](40) NULL,
	[Address2] [char](40) NULL,
	[City] [char](30) NULL,
	[State] [char](2) NULL,
	[Phone] [char](10) NULL,
	[Email] [char](100) NULL,
	[LinkReasonCode] [char](1) NULL,
	[UnlinkInd] [char](1) NULL,
	[PrimaryAcctFlag] [char](1) NULL,
	[LinkedAccount] [char](19) NULL,
	[Filler1] [char](2) NULL,
	[ElectronicCommConfirm] [char](1) NULL,
	[RewardProgramFlag] [char](1) NULL,
	[CompanyCode] [char](2) NULL,
	[ProductId] [char](2) NULL,
	[MethodOfContact] [char](1) NULL,
	[RPIN] [char](6) NULL,
	[Filler2] [char](16) NULL,
	[MobilePhone] [char](10) NULL,
	[AcctOpenedDate] [char](8) NULL,
	[Filler3] [char](2) NULL
) ON [PRIMARY]

GO


