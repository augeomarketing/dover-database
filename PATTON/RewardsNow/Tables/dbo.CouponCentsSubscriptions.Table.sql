USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CouponCentsSubscriptions]    Script Date: 05/04/2012 10:59:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CouponCentsSubscriptions]') AND type in (N'U'))
DROP TABLE [dbo].[CouponCentsSubscriptions]
GO

USE [RewardsNow]
GO

/****** Object:  Table [dbo].[CouponCentsSubscriptions]    Script Date: 05/04/2012 10:59:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CouponCentsSubscriptions](
	[TipNumber] [varchar](20) NULL,
	[NewTip] [varchar](20) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[SubscriptionTypeName] [varchar](50) NULL,
	[SubscriptionTypeDescription] [varchar](256) NULL,
	[DollarsSpent] [decimal](14, 2) NULL,
	[PointsSpent] [int] NULL,
	[SubscriptionTerm] [varchar](50) NULL,
	[StatusName] [varchar](50) NULL,
	[StatusDescription] [varchar](100) NULL,
	[NextExpirationDate] [date] NULL,
	[EmailAddress] [varchar](50) NULL,
	[Name1] [varchar](50) NULL,
	[ZipCode] [varchar](10) NULL,
	[TypeId] [int] NULL,
	[StatusId] [int] NULL
) ON [PRIMARY]

GO


