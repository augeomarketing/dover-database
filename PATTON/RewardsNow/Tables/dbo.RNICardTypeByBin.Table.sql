USE [RewardsNow]
GO

/* ONLY USE THE DROP DURING INITIAL TESTING AND FIRST DEPLOYMENT */
IF OBJECT_ID(N'RNICardTypeByBin') IS NOT NULL
	DROP TABLE RNICardTypeByBin
GO

CREATE TABLE RNICardTypeByBin
(
	sid_rnicardtypebybin_id BIGINT IDENTITY(1,1) PRIMARY KEY
	, dim_rnicardtypebybin_bin VARCHAR(6) NOT NULL
	, dim_rnicardtypebybin_cardtype VARCHAR(10) NOT NULL DEFAULT('OTHER')
	, dim_rnicardtypebybin_dateadded DATETIME NOT NULL DEFAULT(GETDATE())
	, dim_rnicardtypebybin_lastmodified DATETIME NOT NULL DEFAULT(GETDATE())
)

GO


