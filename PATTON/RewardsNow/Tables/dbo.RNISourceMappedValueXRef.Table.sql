USE [RewardsNow]
GO

IF OBJECT_ID(N'RNISourceMappedValueXRef') IS NOT NULL
	DROP TABLE RNISourceMappedValueXRef
GO

CREATE TABLE RNISourceMappedValueXRef
(
	sid_rnisourcemappedvaluexref_id BIGINT IDENTITY(1,1) PRIMARY KEY
	, sid_dbprocessinfo_dbnumber VARCHAR(50)
	, dim_rnisourcemappedvaluexref_sourcetable VARCHAR(255)
	, dim_rnisourcemappedvaluexref_sourcecolumn VARCHAR(255)
	, dim_rnisourcemappedvaluexref_startindex INT
	, dim_rnisourcemappedvaluexref_length INT
	, dim_rnisourcemappedvaluexref_sourcevalue VARCHAR(255)
	, sid_rnimappedvalue_id BIGINT
)
GO

